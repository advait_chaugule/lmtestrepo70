/*
 * EJ 4/26/2019
 * report.java - Based on example servlet code provided within JasperReports
 *
 * JasperReports - Free Java Reporting Library.
 * Copyright (C) 2001 - 2019 TIBCO Software Inc. All rights reserved.
 * http://www.jaspersoft.com
 *
 * Unless you have purchased a commercial license agreement from Jaspersoft,
 * the following license terms apply:
 *
 * This program is part of JasperReports.
 *
 * JasperReports is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JasperReports is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JasperReports. If not, see <http://www.gnu.org/licenses/>.
 */
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.*;




import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.export.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
/*import java.util.HashMap;
import java.util.Map;*/

import java.util.*;
import java.sql.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import net.sf.jasperreports.engine.JRConstants;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import net.sf.jasperreports.web.util.WebHtmlResourceHandler;


/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 */
public class ReportServlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;

    static Logger mainLogger = Logger.getLogger("reportservlet");

    @Override
    public void service(
        HttpServletRequest request,
        HttpServletResponse response
    ) throws IOException, ServletException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String jReportName = request.getParameter("report"); // the .jasper file       
        String orgid = request.getParameter("orgid"); // common param
        String docid = request.getParameter("docid"); // common param
        String itemid = request.getParameter("itemid"); // common param
        Connection conn = null;
        String PathToReports = "/usr/share/tomcat8/webapps/reportserver/WEB-INF/classes/"; // filesystem
        // String PathToReports = getServletContext().getRealPath("/reports/");	// app deployment location

        try {
            // Database connection information
            /*ServletContext context = this.getServletConfig().getServletContext();
            Context envContext = new InitialContext();
            Context initContext  = (Context) envContext.lookup("java:/comp/env");
            /*DataSource ds = (DataSource)initContext.lookup("jdbc/ACMT");*/

            /* Connection con = ds.getConnection();*/
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://acmt.ce9bcc1pq6dg.us-east-1.rds.amazonaws.com:3306/acmt_dev", "acmt", "Awc867MC");
            System.err.println(conn);

            // create a map of parameters to pass to the report.
            Map parameters = new HashMap();
            parameters.put("Report_Title", "Glossary");
            parameters.put("orgid", orgid);
            parameters.put("docid", docid);
            parameters.put("itemid", itemid);


            // Locate the compiled Jasper report
            File reportFile = new File(PathToReports + jReportName + ".jasper");
            if (!reportFile.exists())
                throw new JRRuntimeException("File " + jReportName + ".jasper not found. The report design must be compiled first.");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());

            // Build report parameter map
            /* Map < String, Object > params = new HashMap < String, Object > ();

             params.put("IS_IGNORE_PAGINATION", Boolean.TRUE); // JasperReports built-in parameter to disable pagination during report creation

             // our own parameters -- may or may not be present
             params.put("SUBREPORT_DIR", PathToReports); // required for reports that have subreports
             // mainLogger.log(Level.INFO, "servlet: "+ this.getServletName());
             params.put("servlet", this.getServletName());
             params.put("orgid", jorgid);
             params.put("docid", jdocid);
             params.put("itemid", jitemid);
             params.put("DocumentName", request.getParameter("DocumentName"));*/

            // Create report object
            JasperPrint jasperPrint =
                JasperFillManager.fillReport(
                    jasperReport,
                    parameters,
                    conn
                );

            // output report as HTML
            HtmlExporter exporter = new HtmlExporter();

            // EJ - Jasperreports use a session object to hold images from an HTML export (or save in a folder)
            // This allows the Jaspersoft ImageServlet to find the images.
            /* Image Servlet definition that needs to be put into the configuration
            	<servlet>
            		<servlet-name>image</servlet-name>
            		<servlet-class>net.sf.jasperreports.j2ee.servlets.ImageServlet</servlet-class>
            	</servlet>
            */
            //request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            SimpleHtmlExporterOutput output = new SimpleHtmlExporterOutput(out); // send the exported HTML to the servlet's output
            output.setImageHandler(new WebHtmlResourceHandler("image?image={0}")); // call the image servlet with the imageID
            exporter.setExporterOutput(output);

            exporter.exportReport();
        } catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
            sqle.printStackTrace(out);
        } catch (Exception e) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>ACMT Reporting..........</title>");
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
            out.println("</head>");

            out.println("<body bgcolor=\"white\">");

            out.println("<span class=\"bnew\">ACMT Reporting service encountered this error :</span>");
            out.println("<pre>");

            e.printStackTrace(out);

            out.println("</pre>");

            out.println("</body>");
            out.println("</html>");
        } finally {
            //close the connection.
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception ignored) {}
            }
        }
    }


}