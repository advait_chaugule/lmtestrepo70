(function(){
    'use strict';
    angular.module('app').controller('LoginController', ['$scope', '$location', '$rootScope', 'AuthenticationService', function($scope, $location, $rootScope, AuthenticationService){
        var vm = this;
        vm.login = login;
       console.log("login controller");
        (function initController() {
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            console.log("login initiated");

            AuthenticationService.Login(vm.username, vm.password, function (response) {
               $location.path('/main');
            }, function(response) {
                console.log('Not working');
            });
            console.log("Hi I am login function.")
        };
    }]);
})();