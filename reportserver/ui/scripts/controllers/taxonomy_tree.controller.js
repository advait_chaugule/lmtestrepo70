(function () {
    'use strict';
  
    angular.module('app')
      .controller('TaxonomyTreeController', ['$scope', function ($scope) {
        $scope.remove = function (scope) {
          scope.remove();
        };
  
        $scope.toggle = function (scope) {
          scope.toggle();
        };
  
        // $scope.moveLastToTheBeginning = function () {
        //   var a = $scope.data.pop();
        //   $scope.data.splice(0, 0, a);
        // };
  
        $scope.newSubItem = function (scope) {
          var nodeData = scope.$modelValue;
          nodeData.children.push({
            id: nodeData.id * 10 + nodeData.children.length,
            title: nodeData.title + '.' + (nodeData.children.length + 1),
            nodes: []
          });
        };
  
        $scope.collapseAll = function () {
          $scope.$broadcast('angular-ui-tree:collapse-all');
        };
  
        $scope.expandAll = function () {
          $scope.$broadcast('angular-ui-tree:expand-all');
        };
  
        $scope.data = [{
          "id": "a6b7567a-9d26-45fb-a017-b621faaae7fa",
          "parent_id": "",
          "cf_entity_type": "cf_doc",
          "uri": "http://www.cfdoc_1.com",
          "extra": "some extra value",
          "publisher_id": "535ee5c2-500d-4354-b63d-0aa4425ae025",
          "subject_id": "fd2ced14-38f5-41b3-8a99-8935a9c2a9ac",
          "organization_id": "471634e7-413e-4359-9805-3ca5d3cdcfb7",
          "item_type_id": "15bcdd87-bac4-4e9b-a4fb-2c719930db4f",
          "license_id": "39a9009b-4c11-49d2-a6af-0976dd34cc82",
          "concept_id": "a0528b2f-1bc0-446c-be29-f2157c6af3d6",
          "language_id": "c30a0cd2-3894-44e1-a324-81806954dbcf",
          "user_id": "dda7d273-da17-43b9-9a5e-3c9cfda4d3ae",
          "title": "Item Type 1",
          "creator": "creator_user",
          "official_source_url": "http://www.official_url.com",
          "url_name": "official",
          "version": "1.0",
          "description": "Description",
          "adoption_status": "adopted",
          "status_start_date": "2018-01-22 15:55:55",
          "status_end_date": "2018-01-22 15:55:55",
          "note": "Lorem Ipsum Dolor Text",
          "full_statement": "Full Statement",
          "human_coding_scheme": "represent parent child relationship in human readable format",
          "list_enumeration": "ennumeration list",
          "abbreviated_statement": "Abbreviated Statement",
          "education_level": "K1",
          "is_active": "1",
          "is_deleted": "0",
          "created_at": "2018-01-22 12:12:12",
          "updated_at": "2018-03-22 14:14:14",
          "children": [
            {
              "id": "a6b7567a-9d26-45fb-a017-b621faaae7fa",
              "parent_id": "75033007-d1ce-4515-be79-90a36e52a6c0",
              "cf_entity_type": "cf_item",
              "uri": "http://www.cfdoc_1.com",
              "extra": "some extra value",
              "publisher_id": "535ee5c2-500d-4354-b63d-0aa4425ae025",
              "subject_id": "fd2ced14-38f5-41b3-8a99-8935a9c2a9ac",
              "organization_id": "471634e7-413e-4359-9805-3ca5d3cdcfb7",
              "item_type_id": "15bcdd87-bac4-4e9b-a4fb-2c719930db4f",
              "license_id": "39a9009b-4c11-49d2-a6af-0976dd34cc82",
              "concept_id": "a0528b2f-1bc0-446c-be29-f2157c6af3d6",
              "language_id": "c30a0cd2-3894-44e1-a324-81806954dbcf",
              "user_id": "dda7d273-da17-43b9-9a5e-3c9cfda4d3ae",
              "title": "Item Type 1",
              "creator": "creator_user",
              "official_source_url": "http://www.official_url.com",
              "url_name": "official",
              "version": "1.0",
              "description": "Description",
              "adoption_status": "adopted",
              "status_start_date": "2018-01-22 15:55:55",
              "status_end_date": "2018-01-22 15:55:55",
              "note": "Lorem Ipsum Dolor Text",
              "full_statement": "Full Statement",
              "human_coding_scheme": "represent parent child relationship in human readable format",
              "list_enumeration": "ennumeration list",
              "abbreviated_statement": "Abbreviated Statement",
              "education_level": "K1",
              "is_active": "1",
              "is_deleted": "0",
              "created_at": "2018-01-22 12:12:12",
              "updated_at": "2018-03-22 14:14:14",
              "children": []
            },
            {
              "id": "a6b7567a-9d26-45fb-a017-b621faaae7fa",
              "parent_id": "75033007-d1ce-4515-be79-90a36e52a6c0",
              "cf_entity_type": "cf_item",
              "uri": "http://www.cfdoc_1.com",
              "extra": "some extra value",
              "publisher_id": "535ee5c2-500d-4354-b63d-0aa4425ae025",
              "subject_id": "fd2ced14-38f5-41b3-8a99-8935a9c2a9ac",
              "organization_id": "471634e7-413e-4359-9805-3ca5d3cdcfb7",
              "item_type_id": "15bcdd87-bac4-4e9b-a4fb-2c719930db4f",
              "license_id": "39a9009b-4c11-49d2-a6af-0976dd34cc82",
              "concept_id": "a0528b2f-1bc0-446c-be29-f2157c6af3d6",
              "language_id": "c30a0cd2-3894-44e1-a324-81806954dbcf",
              "user_id": "dda7d273-da17-43b9-9a5e-3c9cfda4d3ae",
              "title": "Item Type 1",
              "creator": "creator_user",
              "official_source_url": "http://www.official_url.com",
              "url_name": "official",
              "version": "1.0",
              "description": "Description",
              "adoption_status": "adopted",
              "status_start_date": "2018-01-22 15:55:55",
              "status_end_date": "2018-01-22 15:55:55",
              "note": "Lorem Ipsum Dolor Text",
              "full_statement": "Full Statement",
              "human_coding_scheme": "represent parent child relationship in human readable format",
              "list_enumeration": "ennumeration list",
              "abbreviated_statement": "Abbreviated Statement",
              "education_level": "K1",
              "is_active": "1",
              "is_deleted": "0",
              "created_at": "2018-01-22 12:12:12",
              "updated_at": "2018-03-22 14:14:14",
              "children": [
                {
                  "id": "a6b7567a-9d26-45fb-a017-b621faaae7fa",
                  "parent_id": "75033007-d1ce-4515-be79-90a36e52a6c0",
                  "cf_entity_type": "cf_item",
                  "uri": "http://www.cfdoc_1.com",
                  "extra": "some extra value",
                  "publisher_id": "535ee5c2-500d-4354-b63d-0aa4425ae025",
                  "subject_id": "fd2ced14-38f5-41b3-8a99-8935a9c2a9ac",
                  "organization_id": "471634e7-413e-4359-9805-3ca5d3cdcfb7",
                  "item_type_id": "15bcdd87-bac4-4e9b-a4fb-2c719930db4f",
                  "license_id": "39a9009b-4c11-49d2-a6af-0976dd34cc82",
                  "concept_id": "a0528b2f-1bc0-446c-be29-f2157c6af3d6",
                  "language_id": "c30a0cd2-3894-44e1-a324-81806954dbcf",
                  "user_id": "dda7d273-da17-43b9-9a5e-3c9cfda4d3ae",
                  "title": "Item Type 1",
                  "creator": "creator_user",
                  "official_source_url": "http://www.official_url.com",
                  "url_name": "official",
                  "version": "1.0",
                  "description": "Description",
                  "adoption_status": "adopted",
                  "status_start_date": "2018-01-22 15:55:55",
                  "status_end_date": "2018-01-22 15:55:55",
                  "note": "Lorem Ipsum Dolor Text",
                  "full_statement": "Full Statement",
                  "human_coding_scheme": "represent parent child relationship in human readable format",
                  "list_enumeration": "ennumeration list",
                  "abbreviated_statement": "Abbreviated Statement",
                  "education_level": "K1",
                  "is_active": "1",
                  "is_deleted": "0",
                  "created_at": "2018-01-22 12:12:12",
                  "updated_at": "2018-03-22 14:14:14",
                  "children": []
                },
                {
                  "id": "a6b7567a-9d26-45fb-a017-b621faaae7fa",
                  "parent_id": "75033007-d1ce-4515-be79-90a36e52a6c0",
                  "cf_entity_type": "cf_item",
                  "uri": "http://www.cfdoc_1.com",
                  "extra": "some extra value",
                  "publisher_id": "535ee5c2-500d-4354-b63d-0aa4425ae025",
                  "subject_id": "fd2ced14-38f5-41b3-8a99-8935a9c2a9ac",
                  "organization_id": "471634e7-413e-4359-9805-3ca5d3cdcfb7",
                  "item_type_id": "15bcdd87-bac4-4e9b-a4fb-2c719930db4f",
                  "license_id": "39a9009b-4c11-49d2-a6af-0976dd34cc82",
                  "concept_id": "a0528b2f-1bc0-446c-be29-f2157c6af3d6",
                  "language_id": "c30a0cd2-3894-44e1-a324-81806954dbcf",
                  "user_id": "dda7d273-da17-43b9-9a5e-3c9cfda4d3ae",
                  "title": "Item Type 1",
                  "creator": "creator_user",
                  "official_source_url": "http://www.official_url.com",
                  "url_name": "official",
                  "version": "1.0",
                  "description": "Description",
                  "adoption_status": "adopted",
                  "status_start_date": "2018-01-22 15:55:55",
                  "status_end_date": "2018-01-22 15:55:55",
                  "note": "Lorem Ipsum Dolor Text",
                  "full_statement": "Full Statement",
                  "human_coding_scheme": "represent parent child relationship in human readable format",
                  "list_enumeration": "ennumeration list",
                  "abbreviated_statement": "Abbreviated Statement",
                  "education_level": "K1",
                  "is_active": "1",
                  "is_deleted": "0",
                  "created_at": "2018-01-22 12:12:12",
                  "updated_at": "2018-03-22 14:14:14",
                  "children": []
                }
              ]
            }
          ]
        }
        ];
      }])
      
  
  }());