(function(){
    'use strict';

    angular.module('app').factory('AuthorizationService', ['$http', '$cookies', '$rootScope', '$timeout', function($http, $cookies, $rootScope, $timeout){
        var service = {};
        service.getLoggedInUser = getLoggedInUser;
        
        return service;

    	function getLoggedInUser () {
    		if ($rootScope.globals.currentUser&&$rootScope.globals) {
    			return $rootScope.globals.currentUser.user;
    		}
        };

    }]);
})();