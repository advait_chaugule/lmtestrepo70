#script to upload file to s3 artifactory bucket a $yo yo 
import boto3
import json
import pprint
import time
import os
import zipfile
import sys

console=boto3.session.Session(profile_name="test")
s3_client=console.client("s3")
s3_resource=console.resource('s3')

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file),os.path.relpath(os.path.join(root, file),os.path.join(path, '..')))
def zipit(dir_list, zip_name):
    zipf = zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED)
    for dir in dir_list:
        zipdir(dir, zipf)
    zipf.close()

list_bucket= s3_resource.buckets.all()
bucket_name='myartibuckectfornewaccount'
bucket_obj=s3_resource.Bucket(bucket_name)
list_of_object_keys=[] #null list
unique_folder_level1=set()#null set
unique_folder_level2=set()
jenkins_job_name=''#name of the jenkins job
jenkins_project=''
jenkins_env=''
jenkins_type=''
level1=''
level2=''
#print("Bucket Name: {} \nObject Name: {}".format(bucket_name,bucket_obj))

#extracting all objects and imaginary  level one folder in that bucket
for i in bucket_obj.objects.all():
    list_of_object_keys.append(i.key)
    unique_folder_level1.add(i.key.split("/")[0])
    unique_folder_level2.add(i.key.split("/")[1])

#extarcting jenkins_job_name
#jenkins_job_name='ACMT-Dev-Frontend'
jenkins_job_name=sys.argv[1]
jenkins_project=jenkins_job_name.split('-')[0]
jenkins_env=jenkins_job_name.split('-')[1]
jenkins_type=jenkins_job_name.split('-')[2]
#print('jenkins_project: {}\njenkins_env: {}\njenkins_type: {}'.format(jenkins_project,jenkins_env,jenkins_type))

for Imgfolder in unique_folder_level1:
    if jenkins_type.lower() in Imgfolder.lower():
        level1=Imgfolder

for Imgfolder in unique_folder_level2:
    if jenkins_env.lower() in Imgfolder.lower():
        level2=Imgfolder

#logic for backend
if jenkins_type.lower() == 'backend':
    # Initilization of Backend Variable
    #workspacepath="/var/lib/jenkins/workspace/sys.argv[1]"
    #zipfilepath="/var/lib/jenkins/workspace/zipfilepath/sys.argv[2]"
    #buildartifactpath="/usr/share/nginx/build-artifacts/lmp70"
    print('Creating the zip of api,env-files,faq and preparing to upload')
    path='/var/lib/jenkins/workspace/TEST-BOX/'+sys.argv[1]
    os.chdir(path)
    dir_list=['api','env-files','faq']
    zip_name='appcode-'+sys.argv[2]+'.zip'
    zipit(dir_list, zip_name)
    res=bucket_obj.upload_file(zip_name,level1+'/'+level2+'/'+zip_name)
#logic for backend
if jenkins_type.lower() == 'frontend':
     print('Ziping the generated dist folder during build')
     path='/var/lib/jenkins/workspace/TEST-BOX/'+sys.argv[1]+'/acmt-webapp'
     os.chdir(path)
     dir_list=['dist']
     zip_name='dist-'+sys.argv[2]+'.zip'
     zipit(dir_list, zip_name)
     res=bucket_obj.upload_file(zip_name,level1+'/'+level2+'/'+zip_name)


#res=s3_client.put_object(Bucket=bucket_name,Key='FRONTEND/DEV/Test.txt')
#res=s3_client.upload_file('C:/Users/advait.chaugule/windows_server _full.PNG',bucket_name,'FRONTEND/DEV/Test.PNG')
#res=bucket_obj.upload_file('C:/Users/advait.chaugule/windows_server _full.PNG','FRONTEND/DEV/Test.PNG')
#print(res)
