#!/bin/bash
set -e
echo "############### Setting up variables ###############"
declare -a remoteips=("$1" "$2")
workspacepath="/var/lib/jenkins/workspace/$3"
zipfilepath="/var/lib/jenkins/workspace/zipfilepath/$4"
buildartifactpath="/usr/share/nginx/build-artifacts/lmp70"

zip_code() {
    echo "############### Preparing build package on build server ###############"
    cd ${workspacepath};
    sudo zip -q -r ${zipfilepath}/appcode.zip api/ env-files/ faq/;
}
code_deployment() {
for remoteip in "${remoteips[@]}"; do
    echo "############### Deploying code to webserver ${remoteip}###############"
    sudo scp -o StrictHostKeyChecking=no -i /var/lib/jenkins/workspace/ACMT.pem ${zipfilepath}/appcode.zip ec2-user@${remoteip}:${buildartifactpath}/package/

    sudo ssh -o StrictHostKeyChecking=no -i /var/lib/jenkins/workspace/ACMT.pem ec2-user@${remoteip} <<'ENDSSH'
    sudo -s;
    set -e;
    documentroot="/usr/share/nginx/html/lmp70"
    buildartifactpath="/usr/share/nginx/build-artifacts/lmp70"

    echo "############### Taking down current site ###############"
    #sudo pm2 stop ${documentroot}/api/acmtnodeapi/index.js
    if [ -d "${documentroot}" ]; then
	    sudo rm -rf ${documentroot}/*;
    else
        echo ${documentroot} does not exist!
        exit 1;
    fi
    echo "############### Extracting build package into WebServer ###############"
    sudo unzip -q -o ${buildartifactpath}/package/appcode.zip -d ${documentroot};

    echo "############### Replacing platform specific files ###############"
    sudo cp ${documentroot}/env-files/ENV/.env ${documentroot}/api/v2/.env;
    sudo cp ${documentroot}/env-files/ENV/wp-config.php ${documentroot}/faq/;

    echo "############### Deleting zip from Webserver ###############"
    sudo rm -rf ${buildartifactpath}/package/*;

    echo "############### Regenerating autoload ###############"
    sudo composer install -d ${documentroot}/api/v2;
    sudo composer dump-autoload -d ${documentroot}/api/v2;

    echo "############### Executing php artisan migrate ###############"
    sudo php ${documentroot}/api/v2/artisan migrate;
    sudo php ${documentroot}/api/v2/artisan migrate --database=mysql_audit --path=/src/Services/Api/database/migrations/report;
    sudo touch ${documentroot}/api/v2/storage/logs/report_events_error.log;
    sudo php ${documentroot}/api/v2/artisan queue:restart;

    echo "############### Executing sudo php artisan l5-swagger:generate ###############"
    sudo php ${documentroot}/api/v2/artisan l5-swagger:generate;

    echo "############### Creating symbolic link ###############"
    cd ${documentroot}/api/v2/public
    sudo ln -s ../public/ server;

    echo "############### Giving permissions to Webserver ###############"
    sudo mkdir -p ${documentroot}/api/v2/storage/app/temporary/awslogs;
    sudo chown -R nginx:nginx ${documentroot}/api/v2/storage;
    sudo chown -R nginx:nginx ${documentroot}/api/v2/bootstrap/cache;
    sudo setfacl -Rdm u:nginx:rwx ${documentroot}/api/v2/bootstrap/cache;
    sudo chown -R nginx:nginx ${documentroot}/faq/wp-content/uploads;
    sudo setfacl -Rdm u:nginx:rwx ${documentroot}/faq/wp-content/uploads;

    echo "############### Node deployment ###############"
    cd /usr/share/nginx/html/lmp70/api/acmtnodeapi/
    sudo npm install
    sudo pm2 delete all
    sudo pm2 start /usr/share/nginx/html/lmp70/api/acmtnodeapi/index.js
    sudo pm2 save

    echo "############### Restarting Supervisor ###############"
    sudo service supervisord restart;
ENDSSH
done
}

delete_build(){
    echo "############### Deleting build package from build server ###############"
    sudo rm -rf ${zipfilepath}/appcode.zip
}

zip_code
code_deployment
delete_build