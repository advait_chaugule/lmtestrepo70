var showLineBarChartV4 = function (dataset) {
  var margin = {
      top: 25,
      right: 80,
      bottom: 30,
      left: 50
    },
    width = 1000 - margin.left - margin.right,
    height = 250 - margin.top - margin.bottom;

  var w = width + margin.left + margin.right;
  var h = height + margin.top + margin.bottom;

  var xScale = d3.scaleBand()
    .rangeRound([0, width])
    .padding(0.1)
    .domain(dataset.map(function (d) {
      return d[0];
    }));
  var yScale = d3.scaleLinear()
    .rangeRound([height, 0])
    .domain([0, d3.max(dataset, (function (d) {
      return d[2];
    }))]);
  d3.select("svg").remove();
  var svg = d3.select("#chart_container").append("svg")
    .attr("viewBox", "0 0 " + w + " " + h)
    .attr("preserveAspectRatio", "xMidYMid meet");

  var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  // axis-x
  svg.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(xScale));

  // axis-y
  g.append("g")
    .attr("class", "axis axis--y")
    .call(d3.axisLeft(yScale));

//   var YLeftAxis = d3.svg.axis().scale(YLine).orient("right").tickFormat(d3.format(".2s"));
//   g.append("g")
//     .attr("class", "y axis")
//     .attr("transform", "translate(" + (width) + ",0)")
//     .call(YLeftAxis);

  var bar = g.selectAll("rect")
    .data(dataset)
    .enter().append("g");

  // bar chart
  bar.append("rect")
    .attr("x", function (d) {
      return xScale(d[0]);
    })
    .attr("y", function (d) {
      return yScale(d[2]);
    })
    .attr("width", xScale.bandwidth())
    .attr("height", function (d) {
      return height - yScale(d[2]);
    })
    .attr("class", function (d) {
      var s = "bar ";
      if (d[1] < 400) {
        return s + "bar1";
      } else if (d[1] < 800) {
        return s + "bar2";
      } else {
        return s + "bar3";
      }
    });

  // labels on the bar chart
  bar.append("text")
    .attr("dy", "1.3em")
    .attr("x", function (d) {
      return xScale(d[0]) + xScale.bandwidth() / 2;
    })
    .attr("y", function (d) {
      return yScale(d[2]);
    })
    .attr("text-anchor", "middle")
    .attr("font-family", "sans-serif")
    .attr("font-size", "11px")
    .attr("fill", "black")
    .text(function (d) {
      return d[2];
    });

  // line chart
  var line = d3.line()
    .x(function (d, i) {
      return xScale(d[0]) + xScale.bandwidth() / 2;
    })
    .y(function (d) {
      return yScale(d[1]);
    })
    .curve(d3.curveMonotoneX);

  bar.append("path")
    .attr("class", "line") // Assign a class for styling
    .attr("d", line(dataset)); // 11. Calls the line generator

  bar.append("circle") // Uses the enter().append() method
    .attr("class", "dot") // Assign a class for styling
    .attr("cx", function (d, i) {
      return xScale(d[0]) + xScale.bandwidth() / 2;
    })
    .attr("cy", function (d) {
      return yScale(d[1]);
    })
    .attr("r", 5);
}
