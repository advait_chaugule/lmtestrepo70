/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare interface Role {
  _id?: string;
  role_id: string;
  name: string;
  description: string;
}

declare interface USER {
  _id?: string;
  first_name: string;
  last_name: string;
  email?: string;
  email_id: string;
  is_active: string;
  user_id: string;
  role_id: string;
  system_role: { name: string, role_id: string };
  password: string;
  organization_id: string;
  workflow_stage_role_id: string;
  user_name?: string;
  role_name?: string;
}
declare interface Workflow {
  workflow_id: string;
  name: string;
}

declare interface WorkflowDetails {
  name: string;
  organization_id: string;
  project_count: number;
  workflow_id: string;
}

declare interface Nodetype {
  node_type_id: string;
  name: string;
}
declare interface Metadata {
  metadata_id: string;
  name: string;
  field_type_id: number;
  field_possible_values: Object[];
  type: string;
}
