import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusPopupWindowComponent } from './status-popup-window.component';

describe('StatusPopupWindowComponent', () => {
  let component: StatusPopupWindowComponent;
  let fixture: ComponentFixture<StatusPopupWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusPopupWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusPopupWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
