import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseComponent } from './base.component';
import { SharedService } from '../shared.service';
import { CommonService } from '../common.service';
import { AuthenticateUserService } from '../authenticateuser.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { NgProgressModule } from 'ngx-progressbar';

describe('BaseComponent', () => {
  let component: BaseComponent;
  let fixture: ComponentFixture<BaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BaseComponent],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, HttpClientModule, NgProgressModule],
      providers: [SharedService, CommonService, AuthenticateUserService, ConfirmationDialogService,
        { provide: 'Window', useValue: window }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create BaseComponent', () => {
    expect(component).toBeTruthy();
  });
});
