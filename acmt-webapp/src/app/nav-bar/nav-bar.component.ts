import {
  Component,
  OnInit,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Observable
} from 'rxjs';
import {
  Router
} from '@angular/router';
import {
  CommonService
} from '../common.service';
import {
  GlobalSettings
} from '../global.settings';
import {
  AuthenticateUserService
} from './../authenticateuser.service';
import Utils from '../utils';
import {
  SharedService
} from '../shared.service';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  environment
} from '../../environments/environment';
import {
  AutoCompleteComponent
} from '../common/auto-complete/auto-complete.component';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss', './nav-bar-skip-main.scss', './nav-bar-logo-img.scss']

})

/// <reference path="../..assets/js/common.d.ts" />
export class NavBarComponent implements OnInit, OnChanges, OnDestroy {

  userName = '';
  nameInitials = '';
  pending = []; // pending request
  @Input() currentItem = 'Taxonomies';
  @Output() showNavEvent: EventEmitter<any>;
  navBarItems = [];
  serverPermission: boolean;
  intervalTimer = null;
  notifyCount = 0;
  orgCode;
  orgName = '';
  orgId = null;
  organizations = [];
  currentComponentEvent: Subscription;
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  orgNameChangeEvent: Subscription;
  showNav = true;
  dropdownSettings = {
    textField: 'organization_name',
    showLabel: false,
    buttonType: 'button'
  };
  selectedOrg: any;
  uniqueId = 'organization_id';
  selectType = 'single';
  autoCompleteTitle = 'organization';
  nameChangeSubscription: Subscription;
  hovered = false;
  btnFocused = false;

  @ViewChild('autoComplete', {
    static: false
  }) autoComplete: AutoCompleteComponent;

  constructor(
    private http: HttpClient,
    private router: Router,
    private service: CommonService,
    private sharedService: SharedService) {
    this.showNavEvent = new EventEmitter<any>();
    this.renamePacingGuide = environment.renamePacingGuide;
    this.getJSON().subscribe(data => {
      this.navBarItems = data;
      if (this.renamePacingGuide) {
        for (const i in this.navBarItems) {
          if (i) {
            if (this.navBarItems[i]['name'] === 'Pacing Guides') {
              this.navBarItems[i]['name'] = 'Unit Planner';
              break;
            }
          }
        }
      }
      this.sharedService.windowResizeEvent.subscribe((item: any) => {
        if (item) {
          if (window.devicePixelRatio >= 1.5) {
            this.toggleSideBar(false);
          }
        }
      });
      this.setTheCurrentItem(this.currentItem);
    },
      err => { });


    // this.sharedService.orgDetails.subscribe((event: any) => {
    //   if (event.orgCode !== undefined) {
    //     this.orgCode = event.orgCode;
    //   }
    // });

    this.nameChangeSubscription = this.sharedService.profileNameChangeEvent.subscribe(data => {
      if (data) {
        this.setUserName();
      }
    });

    if (localStorage.getItem('orgDetails')) {
      this.orgCode = JSON.parse(localStorage.getItem('orgDetails'))['orgCode'];
    }


  }

  public getJSON(): Observable<any> {
    try {
      return this.http.get('./assets/json/nav.json');
    } catch (err) { }
  }
  ngOnInit() {
    this.getAllProjectAccessRequest();
    this.setUserName();
    this.orgName = localStorage.getItem('org_name');
    if (localStorage.getItem('orgDetails')) {
      this.orgId = JSON.parse(localStorage.getItem('orgDetails'))['orgId'];
    }
    init_dropdownHover();
    this.getNotificationUpdatesCount();
    this.startNotificationUpdatesActivity();
    this.getOrganizationList();
    this.orgNameChangeEvent = this.sharedService.orgNameChangeEvent.subscribe((data: any) => {
      if (data) {
        const org = data.data;
        this.setOrgName(org.ogdId, org.newName);
      }
    });
    localStorage.setItem('isShowNav', JSON.stringify(this.showNav));
  }

  ngOnChanges() {
    this.setTheCurrentItem(this.currentItem);
  }

  ngOnDestroy() {
    if (this.intervalTimer) {
      clearInterval(this.intervalTimer);
    }
    if (this.currentComponentEvent) {
      this.currentComponentEvent.unsubscribe();
    }
    if (this.orgNameChangeEvent) {
      this.orgNameChangeEvent.unsubscribe();
    }
    if (this.nameChangeSubscription) {
      this.nameChangeSubscription.unsubscribe();
    }
  }

  setTheCurrentItem(item) {
    // Its quick fix need to work on it
    // tslint:disable-next-line:max-line-length
    this.currentItem = item;
    if (item === 'Team' || item === 'Workflows' || item === 'Metadata' || item === 'Node Template') {
      if (this.navBarItems[3]) {
        this.navBarItems[3].isExpanded = true;
      }
    }
  }

  getCurrentItem() {

  }

  onNavItemSelected(itemName) {
    this.currentItem = itemName;

  }

  onItemClick(item) {
    item.isExpanded = !item.isExpanded;
  }

  checkPermission(name) {
    if (localStorage.getItem('permissions')) {

      const res = JSON.parse(localStorage.getItem('permissions'));
      const projectPermission = JSON.parse(localStorage.getItem('project_role_permissions'));
      if (name === 'Taxonomies') {
        if (res && res.role_permissions && res.role_permissions.taxonomy_permissions.length > 0) {
          return true;
        }
      } else if (name === 'Projects') {
        if (res && res.role_permissions && res.role_permissions.project_permissions.length > 0) {
          return true;
        }
        // Override global permission with project permission
        if (projectPermission && projectPermission.length > 0) {
          // console.log('projectPermission', projectPermission);
          if (projectPermission[0].length > 0) {
            return true;
          }
        }
      } else if (name === 'Pacing Guides') {
        if (res && res.role_permissions && res.role_permissions.pacing_guide_permissions.length > 0) {
          return true;
        }
      } else
        if (name === 'Team') {
          if (res && res.role_permissions &&
            res.role_permissions.role_user_permissions &&
            res.role_permissions.role_user_permissions.length > 0) {
            return true;
          }
        } else if (name === 'Workflows') {
          if (res && res.role_permissions &&
            res.role_permissions.workflow_permissions &&
            res.role_permissions.workflow_permissions.length > 0) {
            return true;
          }
        } else if (name === 'Metadata') {
          if (res && res.role_permissions &&
            ((res.role_permissions.metadata_permissions &&
              res.role_permissions.metadata_permissions.length > 0) || (res.role_permissions.node_type_permissions &&
                res.role_permissions.node_type_permissions.length > 0))) {
            return true;
          }
        } else if (name === 'Node Template') {
          if (res && res.role_permissions &&
            res.role_permissions.node_template_permissions &&
            res.role_permissions.node_template_permissions.length > 0) {
            return true;
          }
        } else if (name === 'Notifications') {
          if (res && res.role_permissions &&
            res.role_permissions.notification_permissions &&
            res.role_permissions.notification_permissions.length > 0) {
            return true;
          }
        } else if (name === 'Settings') {
          if (res && (res.role_permissions &&
            res.role_permissions.taxonomy_permissions &&
            res.role_permissions.taxonomy_permissions.length > 0 &&
            this.hasPermission(res.role_permissions.taxonomy_permissions, 'Manage CASE servers')) ||
            (res.role_permissions.note_permissions &&
              res.role_permissions.note_permissions.length > 0)) {
            return true;
          }
        } else if (name === 'Public Review') {
          if (res && res.role_permissions &&
            res.role_permissions.public_review_permissions &&
            res.role_permissions.public_review_permissions.length > 0 &&
            (this.hasPermission(res.role_permissions.public_review_permissions, 'View Active Public Review Taxonomies') ||
              (this.hasPermission(res.role_permissions.public_review_permissions, 'View Archived Public Review Taxonomies')))) {
            return true;
          }
        } else if (name === 'Reports') {
          if (res && res.role_permissions &&
            res.role_permissions.role_user_permissions &&
            res.role_permissions.role_user_permissions.length > 0 &&
            (this.hasPermission(res.role_permissions.role_user_permissions, 'Reports'))) {
            return true;
          }
        } else if (name === 'Home') {
          if (localStorage.getItem('system_role_code') === 'PPR00') {
            return false;
          } else {
            if (res && res.role_permissions &&
              res.role_permissions.role_user_permissions &&
              res.role_permissions.role_user_permissions.length > 0 &&
              (this.hasPermission(res.role_permissions.role_user_permissions, 'View Home Screen') ||
                this.hasPermission(res.role_permissions.role_user_permissions, 'View Additional Statistics'))) {
              return true;
            } else {
              return false;
            }
          }
        } else {
          return true;
        }
    }
    return false;
  }

  checkAdministration(item) {

    if (item.name === 'Administration' && item.children) {
      for (let i = 0; i < item.children.length; i++) {
        if (this.checkPermission(item.children[i].name)) {
          return true;
        }
      }
    }
    return false;
  }

  logout() {
    const frontface_url = sessionStorage.getItem('frontface_url');
    if (localStorage.getItem('orgDetails')) {
      this.orgCode = JSON.parse(localStorage.getItem('orgDetails'))['orgCode'];
    }
    localStorage.clear();
    sessionStorage.clear();
    sessionStorage.setItem('frontface_url', frontface_url);
    this.navigate();
    document.title = 'ACMT :Academic Competencies Management Tool';
  }

  notifications() {
    // this.router.navigate([`/app/notifications`]);
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_NOTIFICATIONS;
    this.router.navigate([path]);
  }

  /* --------- Functionality for fetching the list of all request start --------- */

  getAllProjectAccessRequest() {
    this.pending = [];
    const url = GlobalSettings.PROJECT_ACCESS_REQUEST;
    this.service.getServiceData(url).then((res: any) => {
      if (Array.isArray(res)) {
        res.forEach(element => {
          if (element.status === 1) {
            this.pending.push(element); // pending
          }
        });
      }
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  hasPermission(array, name) {
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      if (element.display_name === name) {
        return true;
      }
    }
    return false;
  }

  /**
   * To check any notifications, in each 30 seconds interval
   */
  startNotificationUpdatesActivity() {
    this.intervalTimer = setInterval(() => this.getNotificationUpdatesCount(), 5000 * Utils.NOTIFICATION_INTERVAL_TIME);
  }

  /**
   * To get number of notifications
   */
  getNotificationUpdatesCount() {
    const url = GlobalSettings.NOTIFICATION_COUNT;
    this.service.getServiceData(url).then((res: any) => {
      // console.log('Notification Count ', res);
      const count = res.count;
      if (this.sharedService.newNotificationEvent) {
        if (this.notifyCount !== count) { // If new notifications, send 'true'
          this.getNotificationList();
          this.sharedService.newNotificationEvent.next(true);
        } else {
          this.sharedService.newNotificationEvent.next(false);
        }
      }
      this.notifyCount = count;
    }).catch((ex) => {
      console.log('Notification Count Ex', ex);
    });
  }


  navigate() {
    const nav = [];
    nav.push('/org/' + this.orgCode + '/home');
    this.router.navigate(nav);
    // if (sessionStorage.getItem('frontface_url')) {
    //   this.router.navigate([sessionStorage.getItem('frontface_url')]);
    // } else {
    //   this.router.navigate([nav]);
    // }
  }

  /**
   * To get organization list
   */
  getOrganizationList() {
    this.organizations = [];
    const url = GlobalSettings.ORGANIZATION_LIST;
    this.service.getServiceData(url).then((res: any) => {
      console.log('Organization List ', res);
      this.organizations = res;
      this.selectedOrg = this.organizations.find(x => x.organization_id === this.orgId);
    }).catch((ex) => {
      console.log('Organization List Ex', ex);
    });
  }

  /**
   * To set organization response in localstorage after switching organization
   * @param orgId
   */
  setOrganization(org: any) {
    const orgId = org.organization_id;
    if (orgId && (this.orgName !== org.organization_name)) {
      const url = GlobalSettings.SWITCH_ORGANIZATION;
      const body = {
        'organization_id': orgId
      };
      this.service.postService(url, body).then((res: any) => {
        console.log('setOrganization  ', res);
        this.service.setLoginResponseInStorage(res);
        this.sharedService.currentComponentEvent.next({
          component: null
        });

        this.sharedService.csvStatusUpdateEvent.next({ checkStatus: true });


        // this.router.navigate([sessionStorage.getItem('current_url')]) .then(() => window.location.reload());
        this.orgName = localStorage.getItem('org_name');
        this.orgId = JSON.parse(localStorage.getItem('orgDetails'))['orgId'];
        if ((sessionStorage.getItem('current_url')).indexOf('app/home') !== -1) {
          this.sharedService.currentComponentEvent.next({
            component: 'home'
          });
        } else {
          const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_HOME;
          sessionStorage.setItem('current_url', path);
        }
        this.router.navigate([sessionStorage.getItem('current_url')]);
        const self = this;
        this.getJSON().subscribe(data => {
          localStorage.setItem('permissions', '');
          this.service.getUserPermission(function () {
            self.navBarItems = data;
          });
        });
      }).catch((ex: any) => {
        console.log('on Set Organization ex ', ex);
      });
    }
  }

  goToHomePage() {
    this.onNavItemSelected('Home');
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_HOME;
    this.router.navigate([path]);
  }

  skipToMain() {
    if (document.getElementById('main_container')) {
      document.getElementById('main_container').focus();
    }
  }

  /**
   * To change organization name
   * @param orgId (organization name)
   * @param newName (organization's new name to be updated)
   */
  setOrgName(orgId: any, newName: string) {
    for (const org of this.organizations) {
      if (org.organization_id === orgId) {
        org.organization_name = newName;
        this.orgName = newName;
        localStorage.setItem('org_name', newName);
        break;
      }
    }
  }

  onClickedOutside(e) {
    this.toggleSideBar(false);
  }
  toggleSideBar(val = true) {
    this.showNav = !this.showNav;
    localStorage.setItem('isShowNav', '' + this.showNav);
    if (document.querySelector('#main_container')) {
      // Adding/removing animation class on left panel menu collapsed/expanded
      document.querySelector('#main_container').classList.remove('container-animation');
      document.querySelector('#main_container').classList.add('container-animation');
      setTimeout(() => {
        document.querySelector('#main_container').classList.remove('container-animation');
      }, 300);
    }
    // Utils.preloaderWidthAdjustment(val);
    this.showNavEvent.emit(this.showNav);
  }

  getNavHeight() {
    const head = document.getElementById('head').offsetHeight;
    const foot = document.getElementById('foot').offsetHeight;
    const sec = document.getElementById('mainNav').offsetHeight;
    const navHeight = (sec - (head + foot)) + 'px';
    return (navHeight);
  }

  onClickedOutsideList(e) {
    this.autoComplete.openPanel(false);
  }

  onUserProfileClick() {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_USER_PROFILE;
    this.router.navigate([path]);
  }

  setUserName() {
    if (localStorage.getItem('last_name') && localStorage.getItem('last_name') !== null && localStorage.getItem('last_name') !== 'null') {
      this.userName = localStorage.getItem('first_name') + ' ' + localStorage.getItem('last_name');
      this.nameInitials = localStorage.getItem('first_name')[0] + localStorage.getItem('last_name')[0];
    } else {
      this.userName = localStorage.getItem('first_name');
      this.nameInitials = localStorage.getItem('first_name')[0];
    }
  }

  focusNavBar(val) {
    this.hovered = val;
  }
  focustoggleBtn(val) {
    this.btnFocused = val;
  }

  getNotificationList() {
    const url = GlobalSettings.NOTIFICATION_LIST;

    this.service.getServiceData(url).then((res: any) => {
      if (res.length > 0) {
        Utils.sortDataArray(res, 'updated_at', false, false);
        // tslint:disable-next-line: max-line-length
        const versionNotificationList = res.filter(noti => noti.notification_category === 13);
        if (versionNotificationList && versionNotificationList.length) {
          this.sharedService.notificationListEvent.next(versionNotificationList);
        }
      }
    }).catch((ex) => {
      console.log('Notification List Ex', ex);
    });
  }
}
