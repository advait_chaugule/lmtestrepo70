import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseServersComponent } from './case-servers.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('CaseServersComponent', () => {
  let component: CaseServersComponent;
  let fixture: ComponentFixture<CaseServersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CaseServersComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [SharedService, CommonService, ConfirmationDialogService, AuthenticateUserService],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseServersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create CaseServersComponent', () => {
    expect(component).toBeTruthy();
  });
});
