import {
  Component,
  OnInit
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import {
  SharedService
} from '../../../shared.service';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../../help/walkthrough/walktrough.service';
import {
  WalkthroughComponent
} from '../../help/walkthrough/walkthrough.component';*/

@Component({
  selector: 'app-case-servers',
  templateUrl: './case-servers.component.html'
})
export class CaseServersComponent implements OnInit {

  serverList;
  serverName = '';
  serverURL = '';
  serverDescription = '';
  buttonDisabled = true;
  currentServerId = '';
  editServer;
  serverNameField;
  showError = false;
  flagTest = false;
  sourceUrlPattern = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

  tabItems = ['case servers', 'homepage blocks', 'document']; // contains all the tabs for the current view
  currentTab = this.tabItems[0].toLowerCase(); // selects the current tab as default view


  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'linked_name',
    class: ' ',
    type: 'text'
  },
  {
    name: 'Configured by',
    propName: 'updated_by',
    class: '',
    width: '15%',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Date',
    propName: 'date',
    class: '',
    width: '15%',
    type: 'text'
  },
  {
    name: 'Status',
    propName: 'connection_status',
    class: '',
    width: '10%',
    type: 'text',
    canFilter: true
  }

  ];
  optionList = [];
  form: FormGroup;
  cServerloader = false; // holds loading state for fetching case server list

  constructor(
    private sharedService: SharedService,
    private service: CommonService,
    private dialogService: ConfirmationDialogService,
    private authenticateUser: AuthenticateUserService
    /*  private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService*/
  ) {
    /* Search Event Subscription Started */
    // this.sharedService.searchEvent.subscribe((event: any) => {
    //   console.log('search by', event);
    //   if (event.type && event.type === 'Case Server') {
    //     this.onSearch(event.text);
    //   }
    // });
    /* Search Event Subscription Ended */
  }

  ngOnInit() {
    this.form = new FormGroup({
      serverName: new FormControl(null, [Validators.required]),
      serverURL: new FormControl(null, [Validators.required, Validators.pattern(this.sourceUrlPattern)]),
      serverDescription: new FormControl()
    });
    this.userRoleRight();
    this.initialseCaseServer();
    this.sharedService.faqEvent.next({
      name: 'case_server_list'
    });
  }

  initialseCaseServer() {
    this.getServerList();
    /*this.tourService.end();
    this.tourService.initialize(this.walkService.getWalkthroughData('case_server_list'));*/
    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit',
      type: 'editServerButton',
      value: true,
      modal: '#addCase',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'deleteServerButton',
      value: true,
      check: 'isDelete'
    }
    ];
  }

  getServerList() {
    this.cServerloader = true;
    const url = GlobalSettings.GET_SERVER_LIST;
    this.service.getServiceData(url).then((res: any) => {
      if (res && res.length) {
        this.serverList = res;
        this.serverList.forEach(server => {
          server.date = server.updated_at.split(' ')[0];
          server.isEditable = true;
          server.isDelete = true;
          server.showOptions = (!server.isEditable && !server.isDelete) ? false : true;
        });
      } else {
        this.serverList = [];
      }
      this.cServerloader = false;
    }).catch((ex) => {
      this.cServerloader = false;
    });
  }

  // onSearch(txt) {
  //   if (txt === undefined || txt === '' || txt.length < 3) {
  //     this.searchedServers = this.serverList;
  //   } else {
  //     this.searchedServers = this.serverList.filter(server => {
  //       if ((server.linked_name.toLowerCase()).includes(txt.toLowerCase())) {
  //         return server;
  //       }
  //     });
  //   }
  // }
  createServer() {
    this.editServer = false;
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'add_caseserver'
    });
  }

  checkValidation() {
    if (this.serverName === '') {
      this.showError = true;
    } else {
      this.showError = false;
    }
  }
  testConnectionServer() {
    this.serverAuthentication(0);
  }

  serverAuthentication(isSave) {
    let url;
    if (this.editServer) {
      url = GlobalSettings.UPDATE_SERVER_DETAILS + '/' + this.currentServerId;
    } else {
      url = GlobalSettings.CREATE_LINK_SERVER;
    }
    const body = {
      'linked_name': this.serverName,
      'server_url': this.serverURL,
      'description': this.serverDescription,
      'linked_type': 1,
      'is_save': isSave
    };
    this.service.postService(url, body).then((res: any) => {
      if (isSave === 0) {
        if (res.result === 'success') {
          if (this.serverName && (this.serverName).trim().length > 0) {
            this.buttonDisabled = false;
          }
          this.flagTest = true;
          this.sharedService.sucessEvent.next({
            type: 'test_connection_success'
          });
        } else {
          this.buttonDisabled = true;
          this.sharedService.sucessEvent.next({
            type: 'test_connection_failed'
          });
        }
      } else {
        if (this.editServer) {
          this.sharedService.sucessEvent.next({
            type: 'server_details_updated'
          });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'create_server'
          });
        }
      }
      this.getServerList();
    }).catch(ex => {
      if (ex.msg === 'Api url is already used in Current Tenant.') {
        this.sharedService.sucessEvent.next({
          type: 'server_url_exist'
        });
      } else if (ex.msg === 'Server name is already used in Current Tenant.') {
        this.sharedService.sucessEvent.next({
          type: 'server_name_exist'
        });
      } else if (ex.msg === 'Server name is mandatory.') {
        this.sharedService.sucessEvent.next({
          type: 'server_name_mandatory'
        });
      } else if (ex.msg === 'Server name is already used in Current Tenant.,Api url is already used in Current Tenant.') {
        this.sharedService.sucessEvent.next({
          type: 'servername_api_inuse'
        });
      }
    });

  }

  addServer() {
    this.serverAuthentication(1);
    this.getServerList();
    this.clearModalFields();
  }

  // ----------------for Common Data table----------------- //

  onOptionClicked(event) {
    console.log(event);
    switch (event.clickedOn) {
      case 'editServerButton':
        this.editServerDetails(event.data.linked_server_id);
        break;
      case 'deleteServerButton':
        this.deleteServer(event.data.linked_server_id, event.data.linked_name);
        break;
      default:
    }
  }
  editServerDetails(serverId) {
    this.editServer = true;
    this.buttonDisabled = false;
    const url = GlobalSettings.GET_SERVER_DETAILS + '/' + serverId;
    this.service.getServiceData(url).then((res: any) => {
      this.serverName = res[0].linked_name;
      this.serverURL = res[0].server_url;
      this.serverDescription = res[0].description;
      this.currentServerId = res[0].linked_server_id;
    });
  }

  disableButton() {
    // this.editServer && this.serverName === '' ? (this.buttonDisabled = true) : (this.buttonDisabled = false);
    if (this.editServer) {
      this.serverName.trim() === '' || this.serverURL === '' ? (this.buttonDisabled = true) : (this.buttonDisabled = false);
    } else {
      if (this.flagTest) {
        this.serverName.trim() === '' || this.serverURL === '' ? (this.buttonDisabled = true) : (this.buttonDisabled = false);
      }
    }
    this.checkValidation();
  }

  deleteServer(serverId, serverName) {
    this.dialogService.confirm('Confirm', 'Do you want to delete server ' + serverName + ' ?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.DELETE_SERVER_LINK + '/' + serverId;
          this.service.deleteServiceData(url).then((res: any) => {
            this.getServerList();
            this.sharedService.sucessEvent.next({
              type: 'server_deleted'
            });
          }).catch((ex) => {
            this.sharedService.sucessEvent.next({
              type: 'prevent_delete_role'
            });
            console.log('Error while deleting the role', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  clearModalFields() {
    this.serverName = '';
    this.serverURL = '';
    this.serverDescription = '';
    this.showError = false;
    this.flagTest = false;
    if (this.form) {
      this.form.reset();
    }
    setTimeout(() => {
      this.buttonDisabled = true;
    }, 0);
    this.sharedService.faqEvent.next({
      name: 'case_server_list'
    });
  }

  onTabSelected(tab) {
    this.currentTab = tab;
    if (tab === 'case servers') {
      this.initialseCaseServer();
    }
    // else if (tab === 'homepage blocks') {

    // }
    // setTimeout(() => {
    //   this.currentTab = tab;
    //   // this.router.navigate(['./app/', tab]); // navigation will commence as per tab title, default is metadata
    // }, 10);
  }

  userRoleRight() {

    this.tabItems = [];
    setTimeout(() => {
      if (this.authenticateUser.authenticTaxonomy('Manage CASE servers')) {
        this.tabItems.push('case servers');
      }
      if (this.authenticateUser.AuthenticateNoteDocData('Manage Website Notes')) {
        this.tabItems.push('homepage blocks');
      }
      if (this.authenticateUser.AuthenticateNoteDocData('Manage Website Documents')) {
        this.tabItems.push('document');
      }
      if (JSON.parse(localStorage.getItem('is_super_admin')) === 1) {
        this.tabItems.push('manage tenant');
      }
      this.currentTab = this.tabItems[0].toLowerCase(); // selects the current tab as default view
    }, 1000);
  }

}
