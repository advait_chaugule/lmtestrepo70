import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';

import {
  ImportFileComponent
} from '../../../common/import-file/import-file.component';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  CommonService
} from '../../../common.service';
import Utils from '../../../utils';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import { DataTableComponent } from 'src/app/common/data-table/data-table.component';

import {
  HttpClient
} from '@angular/common/http';

import {
  Observable
} from 'rxjs';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./../../../taxonomy/import-taxonomy/import-taxonomy.component.scss']
})
export class DocumentUploadComponent implements OnInit {

  documentList = []; // holds the documents added
  form: FormGroup; // form instance
  option = '';
  @ViewChild('dataTable', { static: false }) dataTable: DataTableComponent;

  editorConfig = {
    'editable': true,
    'spellcheck': true,
    'height': '250px',
    'minHeight': '0',
    'width': 'auto',
    'minWidth': '0',
    'translate': 'yes',
    'enableToolbar': true,
    'showToolbar': true,
    'placeholder': 'Enter text here...',
    'imageEndPoint': '',
    'toolbar': [
      ['bold', 'italic', 'underline', 'strikeThrough', 'superscript', 'subscript'],
      ['horizontalLine', 'orderedList', 'unorderedList']
    ]
  };


  titleData = ''; // holds title
  descData = ''; // holds description
  assetStatus; // holds asset status
  assetOrder; // holds asset order
  assetId; // holds asset id

  showtext = false; // used to show Getting added at the moment text
  intermediateData = []; // hold urls and data to be added after service call
  addedData = []; // hold added urls and uploaded data
  documents = [];
  documentsForDrag = [];
  orgId;
  listEndingData = {
    title: 'Getting added at the moment'
  };
  enableUpdate = false; // holds the value for update on reorder
  enableCreateButton = false; // holds value to enable confirm button
  enableUpdateButton = false; // holds value to enable update button on editing

  documentPermission = false; // holds permission for document
  valid = false;

  editorContent = '';
  documentLoader = false; // holds loading state for document list fetching
  docAddLoader = false; // holds loading state for document adding

  @ViewChild('importFileComponent', { static: false }) importFileComponent: ImportFileComponent;
  @Output() addDeleteEvent: EventEmitter<any>;

  itemExemplars = [];

  optionList = [{
    name: 'Edit',
    type: 'editDocumentButton',
    value: true,
    check: 'isEditable'
  },
  {
    name: 'Delete',
    type: 'deleteDocumentButton',
    value: true,
    check: 'isDelete'
  }, {
    name: 'Download',
    type: 'downloadDocument',
    value: true,
    propName: 'downloadUrl',
    url: true,
    check: 'isDownload'
  }
  ];

  listOfColumn = [{
    name: 'Name',
    propName: 'title',
    class: '',
    type: 'text'
  },
  {
    name: 'Status',
    propName: 'active_status',
    class: '',
    width: '10%',
    type: 'dropdown',
    canFilter: true
  },
  {
    name: 'Type',
    propName: 'extracted_asset_content_type',
    class: '',
    width: '30%',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Upload Date',
    propName: 'uploaded_at',
    class: '',
    width: '10%',
    type: 'date',
    dateType: 'amCalendar'
  }
  ];

  statusList = [{
    name: 'Active',
    value: '1',
    propName: 'asset_id'
  },
  {
    name: 'Inactive',
    value: '0',
    propName: 'asset_id'
  }
  ];

  fileFormats = []; // holds all the files formats

  constructor(private fb: FormBuilder, private service: CommonService, private sharedService: SharedService,
    private AuthenticateUser: AuthenticateUserService, private dialogService: ConfirmationDialogService, private http: HttpClient) {
    this.form = this.fb.group({
      title: [null, Validators.required],
      description: [null]
    });

    this.addDeleteEvent = new EventEmitter<any>();

    this.getJSON().subscribe(data => {
      this.fileFormats = data;
    });

  }

  ngOnInit() {
    if (localStorage.getItem('orgDetails')) {
      this.orgId = JSON.parse(localStorage.getItem('orgDetails'))['orgId'];
      // this.getDocuments();
      this.userRoleRight();
    }
  }


  /* --------- Capture the File object when selected on UI Functionality start --------- */

  onFileUploadEvent(data) {
    const type = data.file.name;
    this.showtext = true;
    const fileType = type.split('.').pop();
    if (fileType === 'exe') {
      this.sharedService.sucessEvent.next({
        type: 'exe_file_upload'
      });
      this.intermediateData = [];
      this.importFileComponent.setUploaedFile('');
    } else {
      this.intermediateData.push({
        'asset': data.file,
        'title': (this.titleData ? this.titleData : ''),
        'description': (this.editorContent ? this.editorContent : ''),
        'asset_name': data.file['name'],
        'type': fileType,
        'asset_linked_type': '5'
      });
    }
    console.log('this.intermediateData', this.intermediateData);
  }

  /* --------- Capture the File object when selected on UI Functionality end --------- */



  /* --------- POST Service Functionality start --------- */

  addDocument() {
    this.docAddLoader = true;
    let fileDetails = {};
    const index = this.intermediateData.length - 1;
    let obj: any;

    const fd = new FormData();
    fd.append('asset', this.intermediateData[index]['asset']);
    fileDetails = this.intermediateData[index]['asset'];
    fd.append('title', this.intermediateData[index]['title']);
    fd.append('description', this.intermediateData[index]['description']);
    fd.append('asset_name', this.intermediateData[index]['asset_name']);
    fd.append('asset_linked_type', '5');
    fd.append('item_linked_id', this.orgId);
    obj = fd;
    this.service.postService(GlobalSettings.FRONT_FACE_UPLOAD_DOC, obj).then((res) => {
      this.showtext = false;
      this.sharedService.sucessEvent.next({
        type: 'add_assets'
      });
      this.importFileComponent.setUploaedFile('');
      this.intermediateData[index]['item_association_id'] = res['asset_id'];
      this.intermediateData[index]['asset_id'] = res['asset_id'];
      this.fillObject(res, fileDetails);
      // this.checkValidation();
      this.intermediateData = [];
      this.form.reset();
      this.editorContent = '';
      this.docAddLoader = false;
      this.getDocuments();
    }).catch((ex) => {
      console.log('addAssets ex ', ex);
      this.docAddLoader = false;
    });
  }

  /* --------- POST Service Functionality end --------- */


  /* --------- Functionality to update the object to show in RHS start --------- */

  fillObject(res, fileDetails) {
    this.itemExemplars.unshift({
      'title': res['title'],
      'description': res['description'],
      'asset_id': res['asset_id'] ? res['asset_id'] : '',
      'association_id': res['asset_id'],
      'asset_file_name': fileDetails ? fileDetails['name'] : '',
      'asset_content_type': fileDetails ? fileDetails['type'] : '',
      'asset_size_in_bytes': fileDetails ? fileDetails['size'] : '',
      'asset_target_file_name': '',
      'asset_uploaded_date_time': '',
      'has_asset': 1
    });

    this.insertList(res, fileDetails);
  }



  /* --------- Functionality to update the object to show in RHS end --------- */


  /* --------- On Delete Uploaded item Functionality start --------- */

  onFileDeleteEvent(asset_id, action) {
    this.dialogService.confirm('Confirm', 'Are you sure to delete this document?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.FRONT_FACE_UPLOAD_DOC + '/' + asset_id;
          if (asset_id.length > 0) {
            this.service.deleteServiceData(url).then((res) => {
              this.sharedService.sucessEvent.next({
                type: 'delete_asset'
              });
              this.itemExemplars = this.itemExemplars.filter(obj => {
                return obj.asset_id !== asset_id;
              });
              if (action === 'getList') {
                this.popList(res);
              }
            }).catch((ex) => {
              console.log('onFileDeleteEvent document upload settings ', ex);
            });
          }
          this.checkValidation();
          // this.importFileComponent.setUploaedFile('');
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  onIntermediateDel() {
    this.intermediateData.pop();
    document.getElementById('title').focus();
    this.importFileComponent.setUploaedFile('');
    if (!this.form.value.title) {
      this.showtext = false;
    }
    this.onCancel(event);
  }

  /* --------- On Delete Uploaded item Functionality end --------- */


  onCancel(event) {
    // if (this.intermediateData.length > 0) {
    //   event.stopPropagation();
    //   event.preventDefault();
    //   this.sharedService.sucessEvent.next({
    //     type: 'unsaved_exempler_data'
    //   });
    // } else {
    this.addDeleteEvent.emit('');
    if (this.importFileComponent) {
      this.importFileComponent.setUploaedFile('');
    }
    this.form.reset();
    this.editorContent = '';
    this.intermediateData = [];
    this.addedData = [];
    this.showtext = false;
    // this.getDocuments();
    this.itemExemplars = [];
    // }
  }



  /* --------- Functionality to convert bytes to appropriate file size start --------- */

  formatBytes(bytes) {
    let fileSize = (Utils.formatBytes(bytes));
    let size = 0;
    let measure = '';
    if (fileSize.toLowerCase().includes('bytes')) {
      size = Math.round(Number(fileSize.substring(0, fileSize.length - 5)));
      measure = fileSize.substring(fileSize.length - 5, fileSize.length);
    } else {
      size = Math.round(Number(fileSize.substring(0, fileSize.length - 2)));
      measure = fileSize.substring(fileSize.length - 2, fileSize.length);
    }

    fileSize = size + ' ' + measure;
    // console.log(fileSize);
    return fileSize;
  }

  /* --------- Functionality to convert bytes to appropriate file size end --------- */

  checkValidation() {
    if (this.titleData && this.titleData.trim().length > 0) {
      this.valid = true;
    } else {
      this.valid = false;
    }
  }

  onValidateDesc() {
    // if (this.editorContent && this.editorContent.trim().length > 0) {
    //   this.valid = true;
    // } else {
    //   this.valid = false;
    // }
    this.valid = true;
  }

  getEditorData() {

  }
  onValidateTitle() {

  }


  /* --------- Fetch Notes List functionality Started --------- */

  getDocuments() {
    this.documents = [];
    this.documentsForDrag = [];
    this.documentLoader = true;
    const url = GlobalSettings.FRONT_FACE_UPLOAD_DOC + '/' + this.orgId;
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        res.forEach(element => {
          element['extracted_asset_content_type'] = Utils.getFileType(element['asset_name'].split('.').pop().toUpperCase(), this.fileFormats);
        });
        this.documents = res;
        this.documentsForDrag = JSON.parse(JSON.stringify(res));
        this.documents.forEach(element => {
          if (this.documentPermission) {
            element.showOptions = true;
          } else {
            element.showOptions = false;
          }
          const downloadUrl = GlobalSettings.BASE_URL + GlobalSettings.ASSET_ASSOCIATION + '/download/' + element.asset_target_file_name;
          element.downloadUrl = downloadUrl;
          element.active_status = element.status === 1 ? 'Active' : 'Inactive';
          element.isDownload = true;
          element.isEditable = true;
          element.isDelete = true;
        });
        console.log(this.documents);
        if (this.dataTable) {
          this.dataTable.setData(this.documents);
        }
      }
      this.documentLoader = false;
    }).catch((ex) => {
      console.log('Error caught while fetching notes list', ex);
      this.documentLoader = false;
    });
  }

  /* --------- Fetch Notes List functionality End --------- */


  /* --------- Fetch to select edit or delete functionality Start --------- */

  onOptionClicked(event) {
    const option = event.clickedOn;
    if (option === 'editDocumentButton') {
      if (document.getElementById('editDocument')) {
        document.getElementById('editDocument').click();
      }
      this.enableControls('edit');
      this.valid = false;
      this.titleData = event.data.title;
      this.editorContent = event.data.description;
      this.assetStatus = event.data.status;
      this.assetOrder = event.data.asset_order;
      this.assetId = event.data.asset_id;
    } else if (option === 'deleteDocumentButton') {
      this.assetId = event.data.asset_id;
      this.onFileDeleteEvent(this.assetId, 'getList');
    }
  }

  /* --------- Fetch to select edit or delete functionality End --------- */

  onNodeRearranged() {
    this.enableUpdate = true;
  }

  reorderNotes() {
    const tempArr = [];
    this.documentsForDrag.forEach(function (element, index) {
      tempArr.push({
        'asset_id': element.asset_id,
        'asset_order': index + 1
      });
    });
    const url = GlobalSettings.FRONT_FACE_UPLOAD_DOC + '/reorder';
    this.service.putService(url, {
      'file': tempArr
    }).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_document_block'
      });
      this.getDocuments();
      this.onCancel('');
    }).catch((ex) => {
      console.log('reorder notes error occured', ex);
    });
  }


  enableControls(control) {
    if (control === 'create') {
      this.enableCreateButton = true;
      this.enableUpdateButton = false;
    } else {
      this.enableCreateButton = false;
      this.enableUpdateButton = true;
    }
  }

  /* --------- Update Notes functionality Start --------- */

  updateDocument() {
    const body = {
      'title': this.titleData,
      'description': this.editorContent,
    };

    const url = GlobalSettings.FRONT_FACE_UPLOAD_DOC + '/' + this.assetId;
    this.service.putService(url, body).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_document_block'
      });
      this.getDocuments();
      // this.refreshList(res);
      this.onCancel('');
    }).catch((ex) => {
      console.log('update_document_block error occured', ex);
    });
  }

  /* --------- Update Notes functionality End --------- */


  /* --------- Change note status functionality Started --------- */

  changeStatus(status, assetId) {
    const body = {
      'status': status,
    };
    this.assetId = assetId;
    const url = GlobalSettings.FRONT_FACE_UPLOAD_DOC + '/' + assetId;
    this.service.putService(url, body).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_document_block'
      });
      // this.getDocuments();
      this.refreshList(res);
      this.onCancel('');
    }).catch((ex) => {
      console.log('createMetadata error occured', ex);
    });
  }

  /* --------- Change note status functionality Ended --------- */



  /* --------- Functionality to check permission start --------- */

  userRoleRight() {
    if (this.AuthenticateUser.AuthenticateNoteDocData('Manage Website Documents')) {
      this.documentPermission = true;
    } else {
      this.documentPermission = false;
    }
    this.documentLoader = true;
    setTimeout(() => {
      this.getDocuments();
    }, 500);

  }

  /* --------- Functionality to check permission end --------- */


  /* --------- Functionality to update title and description on the fly start --------- */
  titleChange() {
    if (this.intermediateData && this.intermediateData.length > 0) {
      this.intermediateData[this.intermediateData.length - 1]['title'] = this.titleData;
    }
  }

  descriptionChange() {
    if (this.intermediateData && this.intermediateData.length > 0) {
      this.intermediateData[this.intermediateData.length - 1]['description'] = this.editorContent;
    }
  }
  /* --------- Functionality to update title and description on the fly end --------- */




  /* --------- List update functionality Start --------- */

  insertList(res, fileDetails) {

    const downloadUrl = GlobalSettings.ASSET_ASSOCIATION + '/download/' + res.asset_target_file_name;
    this.documents.push({
      'title': res['title'],
      'description': res['description'],
      'asset_id': res['asset_id'] ? res['asset_id'] : '',
      'asset_target_file_name': fileDetails ? fileDetails['name'] : '',
      'asset_content_type': fileDetails ? fileDetails['type'] : '',
      'asset_size': fileDetails ? fileDetails['size'] : '',
      'asset_name': fileDetails ? fileDetails['name'] : '',
      'uploaded_at': res['uploaded_at'] ? res['uploaded_at'] : '',
      'status': res['status'],
      'active_status': res['status'] === 1 ? 'Active' : 'Inactive',
      'showOptions': true,
      'isEditable': true,
      'isDelete': true,
      'isDownload': true,
      'downloadUrl': downloadUrl
    });

    this.documentsForDrag.push({
      'title': res['title'],
      'description': res['description'],
      'asset_id': res['asset_id'] ? res['asset_id'] : '',
      'asset_target_file_name': fileDetails ? fileDetails['name'] : '',
      'asset_content_type': fileDetails ? fileDetails['type'] : '',
      'asset_size': fileDetails ? fileDetails['size'] : '',
      'asset_name': fileDetails ? fileDetails['name'] : '',
      'uploaded_at': res['uploaded_at'] ? res['uploaded_at'] : '',
      'status': res['status'],
      'active_status': res['status'] === 1 ? 'Active' : 'Inactive'
    });
  }

  refreshList(res) {
    this.documents.forEach(element => {
      if (element.asset_id === this.assetId) {
        element.title = res.title;
        element.description = res.description;
        element.active_status = res.status ? 'Active' : 'Inactive';
        element.asset_order = res.asset_order;
      }
    });
    this.documentsForDrag.forEach(element => {
      if (element.asset_id === this.assetId) {
        element.title = res.title;
        element.description = res.description;
        element.active_status = res.status ? 'Active' : 'Inactive';
        element.asset_order = res.asset_order;
      }
    });
  }

  popList(res) {
    for (let i = 0; i < this.documents.length; i++) {
      if (this.documents[i]['asset_id'] === this.assetId) {
        console.log('Match');
        this.documents.splice(i, 1);
      }
    }

    for (let i = 0; i < this.documentsForDrag.length; i++) {
      if (this.documentsForDrag[i]['asset_id'] === this.assetId) {
        console.log('Match');
        this.documentsForDrag.splice(i, 1);
      }
    }
  }

  /* --------- List update functionality End --------- */

  public getJSON(): Observable<any> {
    try {
      return this.http.get('./assets/json/fileFormat.json');
    } catch (err) { }
  }

}
