import { Component, OnInit } from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';
import { GlobalSettings } from '../../../global.settings';
import {
    CommonService
} from '../../../common.service';
import {
    SharedService
} from '../../../shared.service';
import { AuthenticateUserService } from '../../../authenticateuser.service';
import { ConfirmationDialogService } from '../../../confirmation-dialog/confirmation-dialog.service';
// import { TuiService } from 'ngx-tui-editor';
@Component({
    selector: 'app-homepage-blocks',
    templateUrl: './homepage-blocks.component.html'
})
export class HomepageBlocksComponent implements OnInit {


    editorConfig = {
        'editable': true,
        'spellcheck': true,
        'height': '250px',
        'minHeight': '0',
        'width': 'auto',
        'minWidth': '0',
        'translate': 'yes',
        'enableToolbar': true,
        'showToolbar': true,
        'placeholder': 'Enter text here...',
        'imageEndPoint': '',
        'toolbar': [
            ['bold', 'italic', 'underline', 'strikeThrough', 'superscript', 'subscript'],
            ['horizontalLine', 'orderedList', 'unorderedList']
        ]
    };



    options: {
        initialValue: '# Title of Project',
        initialEditType: 'wysiwyg',
        height: 'auto',
        minHeight: '500px',
        usageStatistics: false
    };
    template_id;
    selectCreateBlock = false; // holds if create new block is selected
    form: FormGroup; // holds form group object
    homePageBlockName = ''; // holds name
    homePageBlockContent = ''; // holds content
    note_status = ''; // holds active inactive status
    note_order = 1; // holds note order
    note_id = ''; // holds note id
    homePageBlocksList = []; // holds note list
    homePageBlocksListForDrag = []; // holds the object for dragula
    enableUpdate = false; // holds the value for update on reorder
    enableCreateButton = false; // holds value to enable confirm button
    enableUpdateButton = false; // holds value to enable update button on editing
    homePageBlocksPermission = false; // holds permission for home page blocks
    editorContent = '';
    valid = false;
    // ----------------for Common Data table----------------- //
    listOfColumn = [{
        name: 'Name',
        propName: 'title',
        class: '',
        type: 'text'
    },
    {
        name: 'Status',
        propName: 'status',
        class: '',
        width: '10%',
        type: 'dropdown'
    }];

    optionList = [{
        name: 'Edit',
        type: 'editHomePageTitleButton',
        value: true,
        check: 'isEditable'
    },
    {
        name: 'Delete',
        type: 'deleteHomePageTitleButton',
        value: true,
        check: 'isDelete'
    }];

    statusList = [{
        name: 'Active',
        value: 1,
        propName: 'note_id'
    },
    {
        name: 'Inactive',
        value: 0,
        propName: 'note_id'
    }
    ];
    hpBlockLoader = false; // holds loading state for fetching home page block data


    constructor(private fb: FormBuilder,
        private service: CommonService, private sharedService: SharedService, private dialogService: ConfirmationDialogService,
        private AuthenticateUser: AuthenticateUserService,
    ) {
        this.form = this.fb.group({
            homePageBlockName: [null, Validators.required]
        });
    }
    // setHtml() {
    //     this.editorService.setHtml('<h1>Hello World</h1>');
    // }
    ngOnInit() {
        // this.form = new FormGroup({
        //     homePageBlockName: new FormControl()
        // });
        this.userRoleRight();
    }

    onCancel() {
        // this.selectCreateBlock = false;
        this.form.reset();
        // this.getWorkFlowList();
        this.homePageBlockName = '';
        this.homePageBlockContent = '';
        this.editorContent = '';
    }

    checkValidation() {
        if (this.homePageBlockName && this.homePageBlockName.trim().length > 0) {
            this.valid = true;
        } else {
            this.valid = false;
        }
    }


    checkDescValidation() {
        this.valid = true;
    }


    createHomePageBlock() {
        console.log(this.form.value);
        const body = {
            'title': this.homePageBlockName,
            'description': this.editorContent,
            'note_status': 0,
            'note_order': 1,
        };

        const url = GlobalSettings.HOME_PAGE_BLOCK_NOTE;
        this.service.postService(url, body).then((res: any) => {
            this.sharedService.sucessEvent.next({
                type: 'create_home_page_block'
            });
            this.getHomePageBlocks();
            // this.insertList(res);
            this.onCancel();
            this.selectCreateBlock = false;
        }).catch((ex) => {
            console.log('createMetadata error occured', ex);
        });

    }


    /* --------- Fetch Notes List functionality Started --------- */

    getHomePageBlocks() {
        this.homePageBlocksList = [];
        this.homePageBlocksListForDrag = [];
        this.hpBlockLoader = true;
        const url = GlobalSettings.HOME_PAGE_BLOCK_NOTE;
        this.service.getServiceData(url).then((res: any) => {
            if (res) {
                this.homePageBlocksList = res;
                this.homePageBlocksListForDrag = JSON.parse(JSON.stringify(res));
                this.homePageBlocksList.forEach(element => {
                    if (this.homePageBlocksPermission) {
                        element.showOptions = true;
                    } else {
                        element.showOptions = false;
                    }
                    element.isEditable = true;
                    element.isDelete = true;
                    element.status = element.note_status === 1 ? 'Active' : 'Inactive';
                });
                console.log(this.homePageBlocksList);
            }
            this.hpBlockLoader = false;
        }).catch((ex) => {
            console.log('Error caught while fetching notes list', ex);
            this.homePageBlocksList = [];
            this.hpBlockLoader = false;
        });
    }

    /* --------- Fetch Notes List functionality End --------- */


    /* --------- Update Notes functionality Start --------- */

    updateHomePageBlock() {
        console.log(this.form.value);
        const body = {
            'title': this.homePageBlockName,
            'description': this.editorContent,
            'note_status': this.note_status,
            'note_order': this.note_order
        };

        const url = GlobalSettings.HOME_PAGE_BLOCK_NOTE + '/' + this.note_id;
        this.service.putService(url, body).then((res: any) => {
            this.sharedService.sucessEvent.next({
                type: 'update_home_page_block'
            });
            // this.getHomePageBlocks();
            this.refreshList(res);
            this.onCancel();
            this.selectCreateBlock = false;
        }).catch((ex) => {
            console.log('createMetadata error occured', ex);
        });
    }

    /* --------- Update Notes functionality End --------- */


    /* --------- Delete Notes functionality Start --------- */

    deleteHomePageBlock() {
        this.dialogService.confirm('Confirm', 'Are you sure to delete this note?')
            .then((confirmed) => {
                if (confirmed) {
                    const url = GlobalSettings.HOME_PAGE_BLOCK_NOTE + '/' + this.note_id;
                    this.service.deleteServiceData(url).then((res: any) => {
                        this.sharedService.sucessEvent.next({
                            type: 'delete_home_page_block'
                        });
                        // this.getHomePageBlocks();
                        this.popList(res);
                        this.onCancel();
                    }).catch((ex) => {
                        console.log('createMetadata error occured', ex);
                    });
                } else {
                    console.log('User cancel the dialog');
                }
            })
            .catch(() => {
                console.log('User dismissed the dialog');
            });
    }

    /* --------- Delete Notes functionality Start --------- */


    /* --------- Fetch metadata List functionality End --------- */


    /* --------- Fetch to select edit or delete functionality Start --------- */

    onOptionClicked(event) {
        console.log(event);
        const option = event.clickedOn;
        if (option === 'editHomePageTitleButton') {
            this.valid = false;
            this.enableControls('edit');
            this.selectCreateBlock = true;
            this.homePageBlockName = event.data.title;
            this.homePageBlockContent = event.data.description;
            this.editorContent = event.data.description;
            // setTimeout(() => {
            //     // this.editorService.setHtml(event.data.description);
            // }, 200);
            this.note_status = event.data.note_status;
            this.note_order = event.data.note_order;
            this.note_id = event.data.note_id;
        } else if (option === 'deleteHomePageTitleButton') {
            this.note_id = event.data.note_id;
            this.deleteHomePageBlock();
        }
    }

    /* --------- Fetch to select edit or delete functionality End --------- */



    /* --------- Change note status functionality Started --------- */

    changeStatus(status, note_id) {
        const body = {
            'note_status': status,
        };
        const url = GlobalSettings.HOME_PAGE_BLOCK_NOTE + '/' + note_id;
        this.service.putService(url, body).then((res: any) => {
            this.sharedService.sucessEvent.next({
                type: 'update_home_page_block'
            });
            this.getHomePageBlocks();
            this.onCancel();
        }).catch((ex) => {
            console.log('createMetadata error occured', ex);
        });
    }

    /* --------- Change note status functionality Ended --------- */




    onNodeRearranged() {
        this.enableUpdate = true;
    }

    reorderNotes() {
        const tempArr = [];
        this.homePageBlocksListForDrag.forEach(function (element, index) {
            tempArr.push({ 'note_id': element.note_id, 'note_order': index + 1 });
        });
        const url = GlobalSettings.HOME_PAGE_BLOCK_REORDER;
        this.service.putService(url, { 'note': tempArr }).then((res: any) => {
            this.sharedService.sucessEvent.next({
                type: 'update_home_page_block'
            });
            this.getHomePageBlocks();
            this.onCancel();
        }).catch((ex) => {
            console.log('reorder notes error occured', ex);
        });
    }

    enableControls(control) {
        if (control === 'create') {
            this.enableCreateButton = true;
            this.enableUpdateButton = false;
        } else {
            this.enableCreateButton = false;
            this.enableUpdateButton = true;
        }
    }


    /* --------- Functionality to check permission start --------- */

    userRoleRight() {
        if (this.AuthenticateUser.AuthenticateNoteDocData('Manage Website Notes')) {
            this.homePageBlocksPermission = true;
        } else {
            this.homePageBlocksPermission = false;
        }
        this.hpBlockLoader = true;
        setTimeout(() => {
            this.getHomePageBlocks();
        }, 500);

    }

    /* --------- Functionality to check permission end --------- */

    change(event) {
        // console.log(this.editorService.getHtml());
    }

    getEditorData() {
        console.log(this.editorContent);
    }


    /* --------- List update functionality Start --------- */

    insertList(res) {
        res.showOptions = true;
        res.isEditable = true;
        res.isDelete = true;
        res.status = res.note_status === 1 ? 'Active' : 'Inactive';
        this.homePageBlocksList.push(res);
        this.homePageBlocksListForDrag.push(res);
    }

    refreshList(res) {
        this.homePageBlocksList.forEach(element => {
            if (element.note_id === this.note_id) {
                element.title = res.title;
                element.description = res.description;
                element.note_status = res.note_status;
                element.note_order = res.note_order;
            }
        });
        this.homePageBlocksListForDrag.forEach(element => {
            if (element.note_id === this.note_id) {
                element.title = res.title;
                element.description = res.description;
                element.note_status = res.note_status;
                element.note_order = res.note_order;
            }
        });
    }

    popList(res) {
        if (this.homePageBlocksList) {
            for (let i = 0; i < this.homePageBlocksList.length; i++) {
                if (this.homePageBlocksList[i]['note_id'] === res['note_id']) {
                    console.log('Match');
                    this.homePageBlocksList.splice(i, 1);
                }
            }
        }

        if (this.homePageBlocksListForDrag) {
            for (let i = 0; i < this.homePageBlocksListForDrag.length; i++) {
                if (this.homePageBlocksListForDrag[i]['note_id'] === res['note_id']) {
                    console.log('Match');
                    this.homePageBlocksListForDrag.splice(i, 1);
                }
            }
        }
    }

    /* --------- List update functionality End --------- */

    onMouseCall($event) {

    }

}
