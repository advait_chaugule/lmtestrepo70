import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync
} from '@angular/core/testing';

import {
  TenantListComponent
} from './tenant-list.component';
import {
  SharedService
} from 'src/app/shared.service';
import {
  CommonService
} from 'src/app/common.service';
import {
  HttpClientModule,
  HttpClient
} from '@angular/common/http';
import {
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  ConfirmationDialogService
} from 'src/app/confirmation-dialog/confirmation-dialog.service';
import {
  Router
} from '@angular/router';
import {
  MockCommonService
} from 'src/app/administration/mock.common.service';
describe('TenantListComponent', () => {
  let component: TenantListComponent;
  let fixture: ComponentFixture < TenantListComponent > ;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [TenantListComponent],
        providers: [SharedService, {
          provide: CommonService,
          useClass: MockCommonService
        }, ConfirmationDialogService, {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }],
        imports: [HttpClientModule,
          FormsModule,
          ReactiveFormsModule
        ],
        schemas: [NO_ERRORS_SCHEMA]

      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantListComponent);
    // service = new MockCommonService();
    component = fixture.componentInstance;
    // spyOn(service, 'isAuthenticated').and.returnValue(false);

    fixture.detectChanges();
  });

  it('Should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * function the fakeAsync function executes the code inside it’s body in a special fake async test zone. 
   * This intercepts and keeps track of all promises created in it’s body.
   * The tick() function blocks execution and simulates the passage of time until all pending asynchronous activities complete.
   */
  it('Should load tenant list', fakeAsync(() => {
    component.getTenantList();
    tick(2);
    fixture.detectChanges();
    expect(component.tenantList.length).toBe(2);
  }));

  it('Should set link for Tenant', fakeAsync(() => {
    component.getTenantList();
    tick(2);
    component.setLinks(component.tenantList[0]);
    fixture.detectChanges();
    expect(component.viewLinkList.length).toBe(3);;
  }));

  it('Should return status by value 0/1', () => {
    expect(component.getStatusByValue(1)).toBe(component.ACTIVE_STATUS);
    expect(component.getStatusByValue(0)).toBe(component.INACTIVE_STATUS);
  });

  it('Should chagne status inactive to active', fakeAsync(() => {
    component.getTenantList();
    tick(2);
    // Initally status should be Inactive
    expect(component.tenantList[0].status).toBe(component.INACTIVE_VALUE);
    component.changeStatus({
      id: component.tenantList[0].organization_id,
      val: component.ACTIVE_VALUE
    });
    fixture.detectChanges();
    tick(2);
    expect(component.tenantList[0].status).toBe(component.ACTIVE_VALUE);
    expect(component.tenantList[0].active_status).toBe(component.ACTIVE_STATUS);
  }));

  it('Should show error msg', fakeAsync(() => {
    let outputValue = {
      type: ''
    };

    component.sharedService.sucessEvent.subscribe((value: any) => {
      outputValue = value;
    });
    component.getTenantList();
    fixture.detectChanges();
    tick(2);
    component.changeStatus({
      id: component.tenantList[0].organization_id,
      val: 2
    });
    tick(2);
    expect(outputValue.type).toBe('error');
  }));

  it('Should click edit teanat', () => {
    component.onEditTenantClick();
    expect(component.actionType).toBe(component.EDIT_ACTION);
  });

  it('Should click create teanat', () => {
    component.onCreateTenantClick();
    expect(component.actionType).toBe(component.CREATE_ACTION);
  });

  it('Should call eidt action complete', fakeAsync(() => {
    component.getTenantList();
    tick(2);
    fixture.detectChanges();
    component.tenantList[0].organization_name = 'ChangedName';
    component.onActionComplete({
      action: component.EDIT_ACTION,
      data: component.tenantList[0]
    });
    expect(component.tenantList[0].organization_name).toBe('ChangedName');
  }));

  it('Should call create action complete', fakeAsync(() => {
    component.onActionComplete({
      action: component.CREATE_ACTION,
      data: {}
    });
    tick(2);
    fixture.detectChanges();
    expect(component.tenantList.length).toBe(2);
  }));

});
