import { Component, OnInit } from '@angular/core';
import { GlobalSettings } from 'src/app/global.settings';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-tenant-list',
  templateUrl: './tenant-list.component.html'
})
export class TenantListComponent implements OnInit {

  ACTIVE_STATUS = 'Active';
  ACTIVE_VALUE = 1;
  INACTIVE_STATUS = 'Inactive';
  INACTIVE_VALUE = 0;
  CREATE_ACTION = 'create';
  EDIT_ACTION = 'update';

  modalId = 'createTenantModal'; // holds create/update tenant modal id
  tenantList = []; // holds list of tenants
  tenantsLoader = false; // holds loading state for fetching tenant list
  defaultSortBy: any; // holds default column of table based on which sorting will be
  selectedTenant: any = {}; // holds selected tenant from list
  viewLinkList = []; // holds link list (i.e. tenant, CASE API links) for tenant
  actionType: string = this.CREATE_ACTION;
  // ----------------for Common Data table----------------- //
  listOfColumn = [{ // column properties
    name: 'Organization Name',
    propName: 'organization_name',
    class: '',
    type: 'text'
  },
  {
    name: 'Short Code',
    propName: 'short_code',
    class: '',
    type: 'text'
  },
  {
    name: 'Date of Creation',
    propName: 'created_at',
    class: '',
    type: 'date',
    dateType: 'amCalendar'
  },
  {
    name: 'Status',
    propName: 'active_status',
    class: '',
    type: 'dropdown',
    canFilter: true
  },
  {
    name: 'Created By',
    propName: 'created_by',
    class: '',
    type: 'text',
    canFilter: true
  }
  ];
  optionList = []; // holds list of options which will be visible on table's more option(three dots) clicked
  statusList = [ // status column
    {
      name: this.ACTIVE_STATUS,
      value: this.ACTIVE_VALUE,
      propName: 'organization_id'
    },
    {
      name: this.INACTIVE_STATUS,
      value: this.INACTIVE_VALUE,
      propName: 'organization_id'
    }
  ];

  constructor(private service: CommonService, public sharedService: SharedService) { }

  ngOnInit() {
    this.getTenantList();
    if (this.listOfColumn[2]) {
      this.defaultSortBy = this.listOfColumn[2]; // setting 'Date of Creation' as default sorting
    }
    this.optionList = [
      {
        name: 'View links',
        type: 'viewLinks',
        value: true,
        modal: '#viewLinksModal',
        check: 'isViewLink'
      },
      {
        name: 'Edit',
        type: 'editTenant',
        value: true,
        modal: '#' + this.modalId,
        check: 'isEdit'
      }
    ];
  }

  /**
   * To get Tenant List
   */
  getTenantList() {
    const url = GlobalSettings.TENANT_LIST_URL;
    this.tenantList = [];
    this.tenantsLoader = true;
    this.service.getServiceData(url).then((res: any) => {
     // console.log('Tenant List===========>', res);
      if (res) {
        const response = res.organization;
        if (response && response.length > 0) {
          response.forEach((tenant: any) => {
            tenant.active_status = this.getStatusByValue(tenant.status);
            tenant.isViewLink = (tenant.tenant_link || tenant.case_api_link) ? true : false;
            tenant.isEdit = true;
            tenant.showOptions = tenant.isViewLink || tenant.isEdit;
            this.tenantList.push(tenant);
          });
        }
      }
      this.tenantsLoader = false;
    }).catch(ex => {
      this.tenantsLoader = false;
      console.log('getTenantList ex ', ex);
    });
  }

  // On table's more option clicked
  onOptionClicked(event: any) {
    this.selectedTenant = event.data;
    const type = event.clickedOn;
    if (type === 'viewLinks') { // If clicked on 'View Links'
      this.onOpenModal(type);
      this.setLinks(this.selectedTenant);
    } else if (type === 'editTenant') { // If clicked on 'Edit'
      this.onEditTenantClick();
    }
  }

  // Setting selected tenant's all links in list to show in modal
  setLinks(tenant: any) {
    this.viewLinkList = [];
    if (tenant) {
      this.selectedTenant = tenant;
      this.viewLinkList.push({ title: 'Organization Name', value: this.selectedTenant.organization_name, isCopy: false });
      this.viewLinkList.push({ title: 'Tenant Link', value: this.selectedTenant.tenant_link, isCopy: true });
      this.viewLinkList.push({ title: 'CASE API Link', value: this.selectedTenant.case_api_link, isCopy: true });
    }
  }

  /**
   * To change status Active/Inactive
   * @param event (status data)
   */
  changeStatus(event: any) {
    if (event.id) {
      const url = GlobalSettings.UPDATE_TENANT_URL;
      const body = {
        organization_id: event.id,
        status: event.val
      };
      this.service.putService(url, body, 0, true).then((res: any) => {
        if (res) {
          const response = res.data;
          if (response.result && (response.result.toLowerCase() === 'exist')) {
            this.showMessage('error', res.message);
          } else {
            const statusName = this.getStatusByValue(response.status);
            this.updateTenantTable(event.id, 'status', response.status);
            this.updateTenantTable(event.id, 'active_status', statusName);
            this.showMessage('tenant_updated', 'Tenant is ' + statusName.toLowerCase());
          }
        }
      }).catch((ex) => {
        this.showMessage('error', ex.msg);
      });
    }
  }

  /**
   * To update tenant list table with updated value of any specific property
   * @param tenantId (unique tenant id)
   * @param updateProp (which column proprty value needs to be updated)
   * @param updateValue (updated value)
   */
  updateTenantTable(tenantId: any, updateProp: any, updateValue: any) {
    for (const tenant of this.tenantList) {
      if (tenant.organization_id === tenantId) {
        tenant[updateProp] = updateValue;
        break;
      }
    }
  }

  // On copied after clicking on copy icon button
  onCopied(event: any) {
    console.log('Copied Value', event);
    this.sharedService.sucessEvent.next({
      type: 'url_copied'
    });
  }

  /**
   * After create/edit action completed, display updated value in table
   * @param event (event data)
   */
  onActionComplete(event: any) {
    const type = event.action.toLowerCase();
    const data = event.data;
    if (type === this.EDIT_ACTION.toLowerCase()) {
      this.updateTenantTable(data.organization_id, 'organization_name', data.organization_name);
    } else if (type === this.CREATE_ACTION.toLowerCase()) {
      this.getTenantList();
    }
  }

  // On create button click
  onCreateTenantClick() {
    this.onOpenModal('crtTenant');
    this.actionType = this.CREATE_ACTION;
    this.selectedTenant = {};
  }

  // On edit button click
  onEditTenantClick() {
    this.onOpenModal('editTenant');
    this.actionType = this.EDIT_ACTION;
    this.selectedTenant = JSON.parse(JSON.stringify(this.selectedTenant));
  }

  // On open modal
  onOpenModal(type: string) {
    let closeBtnId: any = '';
    if (type === 'crtTenant' || type === 'editTenant') { // create/edit tenant modal
      closeBtnId = 'close' + this.modalId;
    } else if (type === 'viewLinks') { // view links modal
      closeBtnId = 'closeviewLinksModal';
    }
    // Focus on close button
    setTimeout(() => {
      if (closeBtnId && document.getElementById(closeBtnId)) {
        document.getElementById(closeBtnId).focus();
      }
    }, 400);
  }

  // On cancel View links modal, clear selected data
  onCancelViewLinkModal() {
    this.selectedTenant = {};
    this.viewLinkList = [];
  }

  showMessage(msgType: string, msg: string) {
    this.sharedService.sucessEvent.next({
      customMsg: msg,
      type: msgType
    });
  }

  getStatusByValue(val: any) {
    if (val === this.ACTIVE_VALUE) {
      return this.ACTIVE_STATUS;
    } else {
      return this.INACTIVE_STATUS;
    }
  }

}
