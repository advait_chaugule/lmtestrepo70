import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTenantComponent } from './create-tenant.component';
import { ModalComponent } from 'src/app/common/modal/modal.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('CreateTenantComponent', () => {
  let component: CreateTenantComponent;
  let fixture: ComponentFixture<CreateTenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateTenantComponent, ModalComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, SharedService, ConfirmationDialogService],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create CreateTenantComponent', () => {
    expect(component).toBeTruthy();
  });
});
