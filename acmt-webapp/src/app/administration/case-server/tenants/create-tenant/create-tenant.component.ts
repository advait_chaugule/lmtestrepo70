import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GlobalSettings } from 'src/app/global.settings';

@Component({
  selector: 'app-create-tenant',
  templateUrl: './create-tenant.component.html'
})
export class CreateTenantComponent implements OnInit, OnChanges {

  CREATE_ACTION = 'create';
  EDIT_ACTION = 'update';

  modalId = 'createTenantModal'; // holds modal unique id
  form: FormGroup;
  orgName = '';
  tenantEmail: string;
  tenantAdmin: string;
  shortCode = '';
  tenantPassword: string;
  shortCodeRegex = /^(?=.*[a-z])(?=.*[0-9])/i;
  pwdRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,100})/;
  emailRegex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  crtTenantLoader = false; // holds loading state while creating tenant
  getTenantLoader = false; // holds loading state while getting any specific tenant
  originalOrgName = '';
  @Input() actionType = this.CREATE_ACTION; // create or update
  @Input() selectedTenant: any = {}; // For edit purpose
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(private service: CommonService, private sharedService: SharedService) { }

  ngOnInit() { }

  ngOnChanges() {
    if (this.actionType) {
      this.actionType = this.actionType.toLowerCase();
      if (this.actionType === this.CREATE_ACTION.toLowerCase()) { // create tenant
        // Form building
        this.form = this.getCreateForm();
        this.form.updateValueAndValidity();
      } else if (this.actionType === this.EDIT_ACTION.toLowerCase()) { // update tenant
        // Form building
        this.form = this.getEditForm();
        this.getTenantById(this.selectedTenant.organization_id); // fetching selected tenant details
      }
    }
  }

  // On create Tenant
  createTenant() {
    if (this.form.valid) {
      const url = GlobalSettings.CREATE_TENANT_URL;
      const body = {
        organization_name: this.orgName,
        admin_account: this.tenantEmail,
        admin_name: this.tenantAdmin,
        password: this.tenantPassword,
        shortcode: this.shortCode
      };
      this.crtTenantLoader = true;
      this.service.registerPostService(url, body).then((res: any) => {
        if (res) {
          if (res.data.result === 'exist') {
            this.showMessage('error', res.message);
          } else {
            this.showMessage('reg_success', res.message);
            this.clearForm();
            setTimeout(() => {
              this.closeModal();
            }, 150);
            this.actionEvent.emit({ data: res, action: this.actionType });
          }
        }
        this.crtTenantLoader = false;
      }).catch((ex) => {
        this.showMessage('error', ex.message);
        this.crtTenantLoader = false;
      });
    }
  }

  // On update Tenant
  updateTenant() {
    if (this.form.valid) {
      const url = GlobalSettings.UPDATE_TENANT_URL;
      const body = {
        organization_id: this.selectedTenant.organization_id,
        organization_name: this.orgName
      };
      this.crtTenantLoader = true;
      this.service.putService(url, body, 0, true).then((res: any) => {
        this.selectedTenant.organization_name = this.orgName;
        if (res) {
          const response = res.data;
          if (response.result && (response.result.toLowerCase() === 'exist')) {
            this.showMessage('error', res.message);
          } else {
            this.showMessage('tenant_updated', null);

            // If updated organization is current organization from where user is updating
            if ((localStorage.getItem('orgDetails')) &&
              (JSON.parse(localStorage.getItem('orgDetails'))['orgId'] === this.selectedTenant.organization_id)) {
              const obj = {
                ogdId: this.selectedTenant.organization_id,
                newName: this.orgName
              };

              // this is triggering for showing updated organization name in left sidenav
              this.sharedService.orgNameChangeEvent.next({
                data: obj
              });
            }
            this.clearForm();
            setTimeout(() => {
              this.closeModal();
            }, 150);
            this.actionEvent.emit({ data: this.selectedTenant, action: this.actionType });
          }
        }
        this.crtTenantLoader = false;
      }).catch((ex) => {
        this.showMessage('error', ex.msg);
        this.crtTenantLoader = false;
      });
    }
  }

  /**
   * To get tenant details by id
   * @param tenantId (organization/tenant id)
   */
  getTenantById(tenantId: any) {
    if (tenantId) {
      const url = GlobalSettings.GET_TENANT_URL + '?organization_id=' + tenantId;
      this.getTenantLoader = true;
      this.service.getServiceData(url, true, true).then((res: any) => {
        if (res) {
          const response = res.data;
          if (response.result && (response.result.toLowerCase() === 'exist')) {
            this.showMessage('error', res.message);
          } else {
            this.selectedTenant.admin_account = response.admin_account;
            this.selectedTenant.admin_name = response.admin_name;
            this.orgName = response.organization_name;
            this.originalOrgName = response.organization_name;
            this.tenantEmail = response.admin_account;
            this.tenantAdmin = response.admin_name;
            this.shortCode = response.short_code;
          }
        }
        this.getTenantLoader = false;
      }).catch(ex => {
        this.getTenantLoader = false;
        this.showMessage('error', ex.msg);
        console.log('getTenantById ex ', ex);
      });
    }
  }

  /**
   * Method to trigger create or edit tenant api calling based on type
   * @param actionType (create/edit)
   */
  tenantAction(actionType: string) {
    if (actionType === this.CREATE_ACTION.toLowerCase()) {
      this.createTenant();
    } else if (actionType === this.EDIT_ACTION.toLowerCase()) {
      this.updateTenant();
    }
  }

  // Form group for create tenant form
  getCreateForm() {
    return new FormGroup({
      orgName: new FormControl(null, [Validators.required]),
      tenantEmail: new FormControl(null, [Validators.required, Validators.pattern(this.emailRegex)]),
      tenantAdmin: new FormControl(null, [Validators.required]),
      shortCode: new FormControl(null, [Validators.required, Validators.pattern(this.shortCodeRegex)]),
      tenantPassword: new FormControl(null, [Validators.required, Validators.pattern(this.pwdRegex)])
    });
  }

  // Form group for edit tenant form
  getEditForm() {
    return new FormGroup({
      orgName: new FormControl(null, [Validators.required]),
      tenantEmail: new FormControl({ value: null, disabled: true }),
      tenantAdmin: new FormControl({ value: null, disabled: true }),
      shortCode: new FormControl({ value: null, disabled: true }),
      tenantPassword: new FormControl({ value: null, disabled: true })
    });
  }

  // On cancel modal
  onCancel(event: any) {
    this.clearForm();
  }

  // Clear form
  clearForm() {
    this.orgName = '';
    this.shortCode = '';
    this.tenantAdmin = '';
    this.tenantEmail = '';
    this.tenantPassword = '';
    this.originalOrgName = '';
    this.form.reset();
    this.form.enable();
  }

  closeModal() {
    if (document.getElementById('close' + this.modalId)) {
      document.getElementById('close' + this.modalId).click();
    }
  }

  showMessage(msgType: string, msg: string) {
    this.sharedService.sucessEvent.next({
      customMsg: msg,
      type: msgType
    });
  }

  /**
   * Custom Form Validation
   * 1) If no changes is done in Edit tenant name, then consider that invalid form
   */
  customFormValid() {
    if (this.actionType === this.EDIT_ACTION.toLowerCase()) { // Only for Edit
      if (this.orgName.toLowerCase().trim() === this.originalOrgName.toLowerCase().trim()) {
        return false; // not valid form
      } else {
        return true; // valid form
      }
    } else {
      return true; // Except Edit action, return true
    }
  }

}
