import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {
  RoleListingComponent
} from './team/role-listing/role-listing.component';

import {
  WorkflowComponent
} from './workflow/workflow/workflow.component';
import {
  PermissionsComponent
} from './team/permissions/permissions.component';
import {
  WorkflowListComponent
} from './workflow/workflowlist/workflowlist.component';
import {
  MetadataListingComponent
} from './metadata/metadata-listing/metadata-listing.component';
import {
  NodeTemplateComponent
} from './node/node-template/node-template.component';
import {
  CreateWorkflowComponent
} from './workflow/create-workflow/create-workflow.component';
import {
  ViewWorkflowComponent
} from './workflow/view-workflow/view-workflow.component';
import { CaseServersComponent } from './case-server/case-servers/case-servers.component';
import { ReportsComponent } from './jasper-reports/reports/reports.component';

const routes: Routes = [{
  path: 'team',
  component: RoleListingComponent
}, {
  path: 'workflow/list',
  component: WorkflowListComponent,
},
{
  path: 'workflow/:flag/:id',
  component: WorkflowComponent
},
{
  path: 'workflow/:flag',
  component: WorkflowComponent
},
{
  path: 'workflow/create',
  component: CreateWorkflowComponent
}, {
  path: 'permissions',
  component: PermissionsComponent
},
{
  path: 'permissions/:id/:name',
  component: PermissionsComponent
},
{
  path: 'metadata',
  component: MetadataListingComponent
},
{
  path: 'nodetemplate',
  component: NodeTemplateComponent
},
{
  path: 'caseservers',
  component: CaseServersComponent
},
{
  path: 'reports',
  component: ReportsComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
