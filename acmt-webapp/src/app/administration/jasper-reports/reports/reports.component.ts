import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonService } from '../../../common.service';
import { GlobalSettings } from '../../../global.settings';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';
import {
  saveAs
} from 'file-saver';
import { AutoCompleteComponent } from '../../../common/auto-complete/auto-complete.component';
import { SharedService } from '../../../shared.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  /* ***************** */
  /* All File Formats */
  HTML_FORMAT = 'html';
  PDF_FORMAT = 'pdf';
  CSV_FORMAT = 'csv';
  XLS_FORMAT = 'xls';
  DOCX_FORMAT = 'docx';
  RTF_FORMAT = 'rtf';
  ODT_FORMAT = 'odt';
  ODS_FORMAT = 'ods';
  XLSX_FORMAT = 'xlsx';
  PPTX_FORMAT = 'pptx';
  /* ***************** */
  PARAMETER_NUMBER = 5; /* Number of total parameter which will come in each report data based on which form elements
                          will be created dynamically. As example, 'parameter_1_display_name', 'parameter_2_display_name' etc.'*/

  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;

  loadingReportList = false;
  reportList: any = []; // holds list of report
  selectedReport: any = null; // holds data of selected report
  defaultSelectedFormat = this.XLS_FORMAT; // Default selected file format is 'xls'
  selectedFileFormat: any = this.defaultSelectedFormat; // selected file format name
  reportForm: FormGroup;
  formObj = {};
  formDetails = [];
  year; // holds year selected from date
  month; // holds month selected from date
  day; // holds day selected from date
  listOfColumn = [ // Table column property
    {
      name: 'Name',
      propName: 'title',
      class: '',
      type: 'link',
      redirect: true,
      loadPage: true
    }
  ];
  formatList = [ // holds all possible file format list
    this.HTML_FORMAT, this.PDF_FORMAT, this.CSV_FORMAT, this.XLS_FORMAT, this.DOCX_FORMAT, this.RTF_FORMAT,
    this.ODT_FORMAT, this.ODS_FORMAT, this.XLSX_FORMAT, this.PPTX_FORMAT
  ];
  publicReviewTaxonomyList = [];
  allTaxonomyList = [];
  draftTaxonomyList = [];
  myDatePickerOptionsStartDate: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    inline: false,
    editableDateField: false,
    openSelectorOnInputClick: true,
  };
  myDatePickerOptionsEndDate: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    inline: false,
    editableDateField: false,
    openSelectorOnInputClick: true,
    disableUntil: {
      year: this.year,
      month: this.month,
      day: this.day
    }
  };
  downloadLoader = false;

  constructor(private service: CommonService, private fb: FormBuilder, private sharedService: SharedService) {
    this.reportForm = this.fb.group(this.formObj); // initializing form froup
  }

  ngOnInit() {
    this.getReportList();
    this.getTaxonomyList();
    this.getPublicReviewTaxonomyList();
    this.sharedService.faqEvent.next({
      name: 'reports'
    });
  }

  /**
   * To get whole report list
   */
  getReportList() {
    const url = GlobalSettings.JASPER_REPORT_LIST;
    this.reportList = [];
    this.loadingReportList = true;
    this.selectedReport = null;
    this.service.getServiceData(url).then((res: any) => {
      if (res && res.length > 0) {
        res.forEach(element => {
          element.id = element.report_config_id;
          this.reportList.push(element);
        });
      }
      if (this.reportList.length > 0) {
        this.onReportSelected(this.reportList[0]); // selecting default report to first
      }
      this.loadingReportList = false;
      setTimeout(() => {
        jasperReportHeightCal();
      }, 200);
    }).catch((ex) => {
      if (ex['status'] === 400) { // If no report found
        this.loadingReportList = false;
        this.reportList = [];
      }
      console.log('getReportList ex ', ex);
    });
  }

  /**
   * Report download service calling
   */
  getReportData() {
    const URL = GlobalSettings.JASPER_REPORT_VIEW + this.getUrlParamsFromForm();

    let contentType = 'text/plain';
    this.downloadLoader = true;
    switch (this.selectedFileFormat) {
      case this.PDF_FORMAT:
        contentType = 'application/pdf';
        break;
      case this.CSV_FORMAT:
        contentType = 'text/html';
        break;
      case this.DOCX_FORMAT:
        contentType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        break;
      case this.RTF_FORMAT:
        contentType = 'application/rtf';
        break;
      case this.ODT_FORMAT:
        contentType = 'application/vnd.oasis.opendocument.text';
        break;
      case this.ODS_FORMAT:
        contentType = 'application/vnd.oasis.opendocument.spreadsheet';
        break;
      case this.XLS_FORMAT:
      case this.XLSX_FORMAT:
        contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        break;
      default:
        break;
    }
    this.service.downloadServiceData(URL, (this.selectedFileFormat ? this.selectedFileFormat.toUpperCase() : ''))
      .then((res: any) => {
        try {
          this.saveToFileSystem(res, this.selectedFileFormat, contentType);
        } catch (error) {
          console.log('onSaveToFileSystem error ', error);
        } finally {
          this.downloadLoader = false;
        }
      }).catch((ex) => {
        console.log('getReportData ', ex);
        this.downloadLoader = false;
      });
  }

  /**
   * On report selected from left side table and display form values in right side panel
   * @param data (Selected report data)
   */
  onReportSelected(data: any) {
    console.log(data);
    this.selectedReport = data;
    this.formDetails = [];
    this.formObj = {};
    if (this.autoComplete) {
      this.autoComplete.clearSelection();
    }
    this.resetArrayForMultiselect(this.draftTaxonomyList);
    for (let i = 0; i < this.PARAMETER_NUMBER; i++) {
      const internalName = data['parameter_' + (i + 1) + '_subtype'],
        elementType = data['parameter_' + (i + 1) + '_prebuild_type'],
        displayName = data['parameter_' + (i + 1) + '_display_name'];
      if (elementType !== 0) {
        this.formDetails.push({
          'label': displayName,
          'fieldType': elementType,
          'internalName': internalName
        });
        if (internalName === 'format') {
          this.formObj[internalName] = [this.defaultSelectedFormat, [Validators.required]];
        } else {
          this.formObj[internalName] = [null, [Validators.required]];
        }
      }
    }
    this.reportForm = this.fb.group(this.formObj);
  }

  /**
   * On submit report form to get report
   */
  onFormSubmit() {
    this.setValueInFormObject();
    this.getReportData();
  }

  onStartDateChanged(event) {
    this.year = event['date']['year'];
    this.month = event['date']['month'];
    this.day = event['date']['day'];
    this.myDatePickerOptionsEndDate = {
      dateFormat: 'yyyy-mm-dd',
      inline: false,
      editableDateField: false,
      openSelectorOnInputClick: true,
      disableUntil: {
        year: this.year,
        month: this.month,
        day: this.day
      }
    };
    this.reportForm.patchValue({
      to_date: ''
    });
  }

  /**
   * Creating URL params dynamically based on form fields
   */
  getUrlParamsFromForm() {
    let url = '';
    this.setValueInFormObject();
    url = url + '?report_type=' + this.selectedReport['internal_name'];
    for (let i = 0; i < this.formDetails.length; i++) {
      url = url + '&' + this.formDetails[i]['internalName'] + '=' + this.formDetails[i]['value'];
    }
    return url;
  }

  /**
   * To set value in form object from formgroup values
   */
  setValueInFormObject() {
    for (let i = 0; i < this.formDetails.length; i++) {
      if (this.formDetails[i].fieldType === 1) { // date picker value
        this.formDetails[i]['value'] = this.reportForm.value[this.formDetails[i]['internalName']].formatted;
      } else if ((this.formDetails[i].fieldType === 3) || ((this.formDetails[i].fieldType === 4) &&
        (this.formDetails[i].internalName !== 'source_document_id')) || (this.formDetails[i].fieldType === 5)) {
        if (this.formDetails[i]['internalName'] === 'target_document_id') {
          const quotedValue = this.getDoubleQuotedStringFromArray([this.reportForm.value[this.formDetails[i]['internalName']]['document_id']]);
          this.formDetails[i]['value'] = quotedValue;
        } else {
          this.formDetails[i]['value'] = this.reportForm.value[this.formDetails[i]['internalName']]['document_id'];
        }
      } else {
        this.formDetails[i]['value'] = this.reportForm.value[this.formDetails[i]['internalName']];
        if (this.formDetails[i]['internalName'] === 'format') {
          this.selectedFileFormat = this.reportForm.value[this.formDetails[i]['internalName']];
        }
      }
      // } else if (this.formDetails[i].fieldType === 4) { // Draft Taxonomy
      //   this.formDetails[i]['value'] = this.reportForm.value[this.formDetails[i]['internalName']].title;
      // } else if (this.formDetails[i].fieldType === 5) { // All Taxonomy
      //   this.formDetails[i]['value'] = this.reportForm.value[this.formDetails[i]['internalName']].document_id;
      // }
    }
  }

  private saveToFileSystem(response: any, type: any, contentType: any) {
    let blob = null;
    const FILE_NAME = this.selectedReport['internal_name'];
    blob = new Blob([response], {
      type: contentType
    });
    saveAs(blob, FILE_NAME + '.' + type.toLowerCase());
  }

  /**
   * To get public review taxonomy list
   */
  getPublicReviewTaxonomyList() {
    const url = GlobalSettings.PROJECT_LIST + '?project_type=3';
    this.service.getServiceData(url).then((res: any) => {
      this.publicReviewTaxonomyList = res === null ? [] : res.projects;
    }).catch(ex => {
      console.log('getPublicReviewTaxonomyList ex ', ex);
    });
  }

  /**
   * To get taxonomy list
   */
  getTaxonomyList() {
    const url = GlobalSettings.TAXONOMY_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.allTaxonomyList = [];
      this.draftTaxonomyList = [];
      if (res && res.length > 0) {
        res.forEach((element: any) => {
          this.allTaxonomyList.push(element);
          if ((element.status !== 5) && (element.status !== 3)) {
            this.draftTaxonomyList.push(element);
          }
        });
      }
    }).catch(ex => {
      console.log('getTaxonomyList ex ', ex);
    });
  }

  onClickedOutside(e: Event) {
    if (this.autoComplete) {
      this.autoComplete.openPanel(false);
    }
  }

  /**
   * To get multiselect values from autocomplete component
   * @param event (multiselect data)
   * @param controls (form controls details)
   */
  updateValueFromAutocomplete(event: any, controls: any) {
    this.setMultiValuesInForm(event, controls);
  }

  /* Setting multiple document ids as array in form element */
  setMultiValuesInForm(arr: any[], controls: any) {
    const temp = [];
    for (const elem of arr) {
      temp.push(elem.document_id);
    }
    this.reportForm.controls[controls.internalName].setValue(this.getDoubleQuotedStringFromArray(temp));
  }

  /**
   * Converting string of array to double quoted string, (e.g. ['a', 'b', 'c'] => '"a","b","c"')
   * @param arr (array)
   */
  getDoubleQuotedStringFromArray(arr: any[]): string {
    let str = '';
    for (let i = 0; i < arr.length; i++) {
      str = str + '\"' + arr[i] + '\"';
      if (i < (arr.length - 1)) {
        str = str + ',';
      }
    }
    return str;
  }

  /**
   * To reset array of elements for multiselect dropdown i.e. deleting 'is_selected' attribute if any
   * @param arr (array elements)
   */
  resetArrayForMultiselect(arr: any[]) {
    for (const elem of arr) {
      delete elem.is_selected;
    }
  }
}
