import {
  CommonService
} from "../common.service";
import {
  HttpClient
} from '@angular/common/http';
import {
  ConfirmationDialogService
} from '../confirmation-dialog/confirmation-dialog.service';
import {
  Router
} from '@angular/router';
import {
  SharedService
} from '../shared.service';
import {
  GlobalSettings
} from '../global.settings';

export class MockCommonService extends CommonService {
  authenticated = false;
  tenantlist = `{
      "organization": [
        {
          "organization_id": "00998d1d-0197-4eeb-8255-a6e2582c74b2",
          "organization_name": "Accounting",
          "short_code": "ORGA0",
          "created_at": "2019-01-24 12:34:58",
          "status": 0,
          "created_by": "None",
          "tenant_link": "https:\/\/comet-stg.learningmate.com\/#\/org\/ORGA0",
          "case_api_link": "https:\/\/api.comet-stg.learningmate.com\/server\/api\/v1\/ORGA0\/ims\/case\/v1p0"
        },
        {
          "organization_id": "07774391-0f7a-441e-9ff4-38213dff56a5",
          "organization_name": "Organization 6",
          "short_code": "ORG06",
          "created_at": "2018-10-23 14:13:49",
          "status": 1,
          "created_by": "None",
          "tenant_link": "https:\/\/comet-stg.learningmate.com\/#\/org\/ORG06",
          "case_api_link": "https:\/\/api.comet-stg.learningmate.com\/server\/api\/v1\/ORG06\/ims\/case\/v1p0"
        }
      ]
    }`;
  permissionsList = `{
    "system_permissions": {
      "project_permissions": [
        {
          "permission_id": "145fd813-3a8e-4877-b547-ddf3746f08ba",
          "parent_permission_id": "",
          "display_name": "View Project List",
          "display_order": 1
        }
      ],
      "taxonomy_permissions": [
        {
          "permission_id": "b9eed119-3933-4d47-bd60-405816992d7e",
          "parent_permission_id": "",
          "display_name": "View Draft Taxonomies",
          "display_order": 1
        }
      ],
      "comment_permissions": [
        {
          "permission_id": "6e679922-800e-456e-adec-5bb7e8e0df05",
          "parent_permission_id": "",
          "display_name": "View My Comments",
          "display_order": 1
        }
      ],
      "metadata_permissions": [
        {
          "permission_id": "817f8f2c-82db-46e6-a45d-56b667755b51",
          "parent_permission_id": "",
          "display_name": "List Metadata",
          "display_order": 1
        }
      ],
      "node_type_permissions": [
        {
          "permission_id": "899eef91-5045-4846-b3fe-f9e0c52296cc",
          "parent_permission_id": "",
          "display_name": "List NodeType",
          "display_order": 1
        }
      ],
      "node_template_permissions": [
        {
          "permission_id": "035add13-aa92-48ff-a279-dbcbb351e455",
          "parent_permission_id": "",
          "display_name": "View Node Templates",
          "display_order": 1
        }
      ],
      "workflow_permissions": [
        {
          "permission_id": "8f0f17d1-7ec7-4c1d-b97b-ff6c9a012ae0",
          "parent_permission_id": "8f0f17d1-7ec7-4c1d-b97b-ff6c9a012ae0",
          "display_name": "View Workflow",
          "display_order": 1
        }
      ],
      "role_user_permissions": [
        {
          "permission_id": "1bf38065-34e3-48c0-ae3b-f58ea59cecd3",
          "parent_permission_id": "",
          "display_name": "List Roles",
          "display_order": 1
        }
      ],
      "notification_permissions": [
        {
          "permission_id": "fd3c70eb-b6a2-453d-9812-70331eeebe28",
          "parent_permission_id": "fbb30451-3287-46a5-9daa-7c9ab0ec96c0",
          "display_name": "Manage Project Access requests",
          "display_order": 1
        }
      ],
      "public_review_permissions": [
        {
          "permission_id": "268362a6-1791-44a0-8629-cc465866d082",
          "parent_permission_id": "268362a6-1791-44a0-8629-cc465866d082",
          "display_name": "View Active Public Review Taxonomies",
          "display_order": 1
        }
      ],
      "note_permissions": [
        {
          "permission_id": "a8b835b9-72e6-404a-8d21-c183aa2be50e",
          "parent_permission_id": "a8b835b9-72e6-404a-8d21-c183aa2be50e",
          "display_name": "Manage Website Notes",
          "display_order": 1
        }
      ],
      "pacing_guide_permissions": [
        {
          "permission_id": "de776eee-4327-4211-9dbc-3e22fbd105af",
          "parent_permission_id": "",
          "display_name": "Add Container",
          "display_order": 1
        }
      ]
    },
    "role_permissions": {
      "project_permissions": [
        {
          "permission_id": "145fd813-3a8e-4877-b547-ddf3746f08ba",
          "parent_permission_id": "",
          "display_name": "View Project List",
          "display_order": 1,
          "internal_name": "ProjectController@showAll"
        }
      ],
      "comment_permissions": [
        {
          "permission_id": "6e679922-800e-456e-adec-5bb7e8e0df05",
          "parent_permission_id": "",
          "display_name": "View My Comments",
          "display_order": 1,
          "internal_name": "CommentController@index"
        }
      ],
      "metadata_permissions": [
        {
          "permission_id": "817f8f2c-82db-46e6-a45d-56b667755b51",
          "parent_permission_id": "",
          "display_name": "List Metadata",
          "display_order": 1,
          "internal_name": "MetadataController@index"
        }
      ],
      "node_type_permissions": [
        {
          "permission_id": "899eef91-5045-4846-b3fe-f9e0c52296cc",
          "parent_permission_id": "",
          "display_name": "List NodeType",
          "display_order": 1,
          "internal_name": "NodeTypeController@index"
        }
      ],
      "node_template_permissions": [
        {
          "permission_id": "035add13-aa92-48ff-a279-dbcbb351e455",
          "parent_permission_id": "",
          "display_name": "View Node Templates",
          "display_order": 1,
          "internal_name": "NodeTemplateController@index"
        }
      ],
      "workflow_permissions": [
        {
          "permission_id": "8f0f17d1-7ec7-4c1d-b97b-ff6c9a012ae0",
          "parent_permission_id": "8f0f17d1-7ec7-4c1d-b97b-ff6c9a012ae0",
          "display_name": "View Workflow",
          "display_order": 1,
          "internal_name": "WorkflowController@viewWorkflow"
        }
      ],
      "role_user_permissions": [
        {
          "permission_id": "1bf38065-34e3-48c0-ae3b-f58ea59cecd3",
          "parent_permission_id": "",
          "display_name": "List Roles",
          "display_order": 1,
          "internal_name": "RoleController@index"
        }
      ],
      "notification_permissions": [
        {
          "permission_id": "fd3c70eb-b6a2-453d-9812-70331eeebe28",
          "parent_permission_id": "fbb30451-3287-46a5-9daa-7c9ab0ec96c0",
          "display_name": "Manage Project Access requests",
          "display_order": 1,
          "internal_name": "ProjectController@getAllProjectAccessRequests"
        }
      ],
      "public_review_permissions": [
        {
          "permission_id": "6e795157-01cc-495d-b26f-3499d2075d51",
          "parent_permission_id": "6e795157-01cc-495d-b26f-3499d2075d51",
          "display_name": "View Archived Public Review Taxonomies",
          "display_order": 2,
          "internal_name": "View_archived_public_review_taxonomies"
        }
      ],
      "note_permissions": [
        {
          "permission_id": "a8b835b9-72e6-404a-8d21-c183aa2be50e",
          "parent_permission_id": "a8b835b9-72e6-404a-8d21-c183aa2be50e",
          "display_name": "Manage Website Notes",
          "display_order": 1,
          "internal_name": "Manage_Website_Notes"
        }
      ],
      "pacing_guide_permissions": [
        {
          "permission_id": "de776eee-4327-4211-9dbc-3e22fbd105af",
          "parent_permission_id": "",
          "display_name": "Add Container",
          "display_order": 1,
          "internal_name": "CFItemController@create"
        }
      ]
    }
  }`;
  taxonomyList = `[
    {
      "document_id": "ad4dd2db-6bad-143f-35e6-763c4c8de49e",
      "title": "taxonomy builder",
      "title_html": "",
      "document_type": 1,
      "created_at": "2019-12-31 11:05:03",
      "associated_projects_count": 1,
      "last_active_date": "2019-12-31 11:08:00",
      "updated_by": "shweta ",
      "adoption_status": 1,
      "status": 1,
      "import_type": 2,
      "creator": "Shweta Gujjeti",
      "imported_by": "shweta ",
      "uri": "https:\/\/api.acmt-qa.learningmate.com\/server\/api\/v1\/Shw02\/ims\/case\/v1p0\/CFDocuments\/ad4dd2db-6bad-143f-35e6-763c4c8de49e",
      "source_document_id": "ad4dd2db-6bad-143f-35e6-763c4c8de49e",
      "groups": [
        
      ]
    }
  ]`;
  nodeTypeList = `{
    "nodetype": [
      {
        "node_type_id": "1e2d1f0c-0f33-4dd2-896e-3c6b2deaaa48",
        "title": "Benchmark",
        "is_document": 0,
        "is_deleted": 0,
        "is_default": 0,
        "created_by": "shweta ",
        "updated_at": "2019-12-24T05:33:38+00:00"
      },
      {
        "node_type_id": "69a89f27-e66f-43f6-875c-a11158b46656",
        "title": "Child 1 Node Type",
        "is_document": 0,
        "is_deleted": 0,
        "is_default": 0,
        "created_by": "shweta ",
        "updated_at": "2019-12-31T11:57:15+00:00"
      },
      {
        "node_type_id": "c7a26130-a242-45ac-aed5-34821a938ddf",
        "title": "Child 1.1 Node Type",
        "is_document": 0,
        "is_deleted": 0,
        "is_default": 0,
        "created_by": "shweta ",
        "updated_at": "2019-12-31T11:57:15+00:00"
      },
      
    ]
  }`;
  metadataList = `{
    "metadata": [
      {
        "metadata_id": "8f2fb2c1-08ba-461a-a8ef-2a4f82faf1cb",
        "parent_metadata_id": "",
        "name": "Abbreviated Statement",
        "internal_name": "abbreviated_statement",
        "type": "TEXT",
        "field_type_id": 1,
        "field_possible_values": "",
        "order": 21,
        "is_custom": 0,
        "is_document": 0,
        "is_mandatory": 0,
        "is_active": 1,
        "updated_by": "",
        "updated_at": "2019-12-31T12:03:36+00:00",
        "usage_count": 3544
      },
      {
        "metadata_id": "84278a4b-3fd2-4fec-b2b4-596d0ce187f6",
        "parent_metadata_id": "",
        "name": "Alternative Label",
        "internal_name": "alternative_label",
        "type": "TEXT",
        "field_type_id": 1,
        "field_possible_values": "",
        "order": 18,
        "is_custom": 0,
        "is_document": 0,
        "is_mandatory": 0,
        "is_active": 1,
        "updated_by": "",
        "updated_at": "2019-12-31T12:03:36+00:00",
        "usage_count": 3544
      }
    ]
  }`;
  workflowList = `{
    "WorkflowList": {
      "workflows": [
        {
          "workflow_id": "6fa1d5ce-47f4-4f7c-a2ba-93aa714ee8d3",
          "organization_id": "e187e2b2-9a65-4ea6-9328-76f9cea381dc",
          "name": "Default workflow",
          "workflow_code": null,
          "is_default": 1,
          "is_active": 1,
          "is_deleted": 0,
          "updated_by": "System",
          "created_at": "2019-12-24T05:21:15+00:00",
          "updated_at": "2019-12-24T05:21:15+00:00"
        },
        {
          "workflow_id": "fdd3851c-ed0a-475a-a48d-5dadd9579c33",
          "organization_id": "e187e2b2-9a65-4ea6-9328-76f9cea381dc",
          "name": "Public Review",
          "workflow_code": "WPR00",
          "is_default": 0,
          "is_active": 1,
          "is_deleted": 0,
          "updated_by": "System",
          "created_at": "2019-12-24T05:21:18+00:00",
          "updated_at": "2019-12-24T05:21:18+00:00"
        }
      ],
      "count": 2
    },
    "WorkflowUsageCount": {
      "6fa1d5ce-47f4-4f7c-a2ba-93aa714ee8d3": 9,
      "fdd3851c-ed0a-475a-a48d-5dadd9579c33": 1
    }
  }`;
  complinceReport = `{
    "case-compliance": {
      "overall-percentage": 60,
      "case-filled-nodes": 3,
      "case-total-nodes": 5,
      "fields": [
        {
          "key": "full_statement",
          "value": 2
        }
      ],
      "nodes": [
        {
          "key": "Default",
          "value": 2
        }
      ]
    },
    "coverage-compliance": {
      "overall-percentage": 100,
      "custom-filled-nodes": 5,
      "custom-total-nodes": 5,
      "fields": [
        
      ],
      "nodes": [
        
      ]
    }
  }`;
  userList = `{
    "UserList": {
      "users": [
        {
          "user_id": "df7fc2e4-b94b-42f3-8132-fa3817f25601",
          "email": "kamal.asawara+1@learningmate.com",
          "first_name": "kamal",
          "last_name": "",
          "is_active": 1,
          "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
          "system_role": {
            "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
            "name": "Admin"
          }
        },
        {
          "user_id": "34b36478-b756-4487-ac53-3666d9b7b60e",
          "email": "kamal.asawara@learningmate.com",
          "first_name": "kamal",
          "last_name": "asawara",
          "is_active": 1,
          "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
          "system_role": {
            "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
            "name": "Admin"
          }
        },
        {
          "user_id": "e916c2c0-59c7-4efa-b3c2-e7254c4997f5",
          "email": "kamal.asawara+4@learningmate.com",
          "first_name": "kamal",
          "last_name": "asawara",
          "is_active": 0,
          "role_id": "bcc55512-a782-4dc2-a456-f77076ded2e6",
          "system_role": {
            "role_id": "bcc55512-a782-4dc2-a456-f77076ded2e6",
            "name": "Platform review- Global role"
          }
        },
        {
          "user_id": "a76d5803-98fd-47c7-ac7a-ea93d73d2ae7",
          "email": "gincy.aleyamma+11@learningmate.com",
          "first_name": "",
          "last_name": "",
          "is_active": 1,
          "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
          "system_role": {
            "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
            "name": "Admin"
          }
        },
        {
          "user_id": "39eeaf40-128c-4144-b295-229c9d4f30e3",
          "email": "gincy.aleyamma+101@learningmate.com",
          "first_name": "Gincy",
          "last_name": "Mathai",
          "is_active": 1,
          "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
          "system_role": {
            "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
            "name": "Admin"
          }
        }
      ],
      "count": 5,
      "totalUsers": 5
    },
    "UserProjectCount": {
      "df7fc2e4-b94b-42f3-8132-fa3817f25601": 0,
      "34b36478-b756-4487-ac53-3666d9b7b60e": 0,
      "e916c2c0-59c7-4efa-b3c2-e7254c4997f5": 0,
      "a76d5803-98fd-47c7-ac7a-ea93d73d2ae7": 0,
      "39eeaf40-128c-4144-b295-229c9d4f30e3": 0
    }
  }`;
  roleList = `{
    "RoleList": {
      "roles": [
        {
          "role_id": "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5",
          "name": "Admin",
          "description": "Admin",
          "is_active": 1,
          "role_code": "DAR00"
        },
        {
          "role_id": "bcc55512-a782-4dc2-a456-f77076ded2e6",
          "name": "Platform review- Global role",
          "description": "Platform review- Global role",
          "is_active": 1,
          "role_code": "PPR00"
        },
        {
          "role_id": "d01337bc-50ba-4d3a-9a9d-121e19753f90",
          "name": "Platform review- Taxonomy role",
          "description": "Platform review- Taxonomy role",
          "is_active": 1,
          "role_code": "PPR01"
        }
      ],
      "count": 3
    },
    "RoleUsageCount": {
      "0f10fad1-f7c1-4148-b3a9-4488ae6a48d5": 4,
      "bcc55512-a782-4dc2-a456-f77076ded2e6": 1,
      "d01337bc-50ba-4d3a-9a9d-121e19753f90": 0
    }
  }`;
  constructor(httpClient: HttpClient,
    dialogService: ConfirmationDialogService,
    router: Router,
    shareService: SharedService) {
    super(httpClient, router, shareService);
  }
  getServiceData(url: String) {
    return new Promise((resolve, reject) => {
      if (url.indexOf(GlobalSettings.GET_PERMISSION) > -1) {
        console.log('GET PERMISSION');
        resolve(JSON.parse(this.permissionsList));
      } else if (url.indexOf(GlobalSettings.TAXONOMY_LIST) > -1) {
        resolve(JSON.parse(this.taxonomyList));
      } else if (url.indexOf(GlobalSettings.GET_NODETYPE_LIST) > -1) {
        resolve(JSON.parse(this.nodeTypeList));
      } else if (url.indexOf(GlobalSettings.METADATA_SEARCH) > -1) {
        resolve(JSON.parse(this.metadataList));
      } else if (url.indexOf(GlobalSettings.TENANT_LIST_URL) > -1) {
        resolve(JSON.parse(this.tenantlist));
      } else if (url.indexOf(GlobalSettings.WORKFLOW_LIST) > -1) {
        resolve(JSON.parse(this.workflowList));
      } else if (url.indexOf(GlobalSettings.COMPLIANCE_REPORTS) > -1) {
        console.log('complinceReport************');
        resolve(JSON.parse(this.complinceReport));
      }else if (url.indexOf(GlobalSettings.GET_USERS) > -1) {
        console.log('complinceReport************');
        resolve(JSON.parse(this.userList));
      }else if(url.indexOf(GlobalSettings.GET_ROLE)>-1){
        resolve(JSON.parse(this.roleList));
      }
    });
  }
  putService(url, body, index, allRes) {
    return new Promise((resolve, reject) => {
      const data = JSON.parse(`{
            "status": 200,
            "data": {
              "organization_name": "Accounting",
              "admin_account": "ajay.mendes@learningmate.com",
              "admin_name": "Ajay",
              "short_code": "ORG15",
              "status": 1
            },
            "message": "Data found.",
            "success": ""
          }`);
      if (body.status === 2) {
        data.data.result = 'exist';
        data.message = 'Data exist';
      } else {
        data.status = body.status;
      }
      resolve(data);
    });
  }
}
