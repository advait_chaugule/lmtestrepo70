import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'src/app/common.service';
import { GlobalSettings } from 'src/app/global.settings';
import { AutoCompleteComponent } from 'src/app/common/auto-complete/auto-complete.component';
import { SharedService } from 'src/app/shared.service';
import Utils from 'src/app/utils';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-association-presets',
  templateUrl: './association-presets.component.html',
  styleUrls: ['./association-presets.component.scss', '../metadata-set/metadata-set.component.scss']
})
export class AssociationPresetsComponent implements OnInit, OnDestroy {

  CREATE_ACTION = 'create';
  EDIT_ACTION = 'update';
  DELETE_ACTION = 'delete';
  VIEW_ACTION = 'view';

  associationPresets = []; // holds association preset list
  taxonomyTypes = []; // holds taxonomy type list
  associationTypes = []; // holds association type list
  metadataList = []; // holds metadata list
  assoPresetForm: FormGroup;
  presetName: string; // holds preset name
  sTaxonomyType: any = null; // holds source taxonomy type
  aTaxonomyType: any = null; // holds association taxonomy type
  assoType: any; // holds association type
  selectedMetadata = []; // holds selected metadata/metadata set
  selectedPreset: any; // holds selected preset
  operation = this.CREATE_ACTION; // holds operation name (create/edit/delete)
  presetLoading = false; // association preset loading state
  assoTypeLoading = false; // association type loading state
  metadataLoading = false; // metadata loading state
  actionLoading = false; // add/edit/delete loading state
  createPermission = false; // holds create permission value
  editPermission = false; // holds edit permission value
  deletePermission = false; // holds delete permission value
  windowResizeSubscription: Subscription;
  listOfColumn = [{
    name: 'Name',
    propName: 'title',
    class: '',
    type: 'text'
  },
  {
    name: 'Source Taxonomy Type',
    propName: 'source_document_name',
    class: '',
    type: 'text'
  },
  {
    name: 'Association Taxonomy Type',
    propName: 'destination_document_name',
    class: '',
    type: 'text'
  },
  {
    name: 'Association Type',
    propName: 'item_association_name',
    class: '',
    type: 'text'
  }
  ];
  optionList = [];

  @ViewChild('autoCompleteAsso', { static: false }) autoCompleteAsso: AutoCompleteComponent;
  @ViewChild('autoCompleteMeta', { static: false }) autoCompleteMeta: AutoCompleteComponent;

  constructor(private fb: FormBuilder, private service: CommonService, private sharedService: SharedService,
    private dialogService: ConfirmationDialogService, private authenticateUser: AuthenticateUserService) { }

  ngOnInit() {
    this.assoPresetForm = this.fb.group({ // form group definition
      presetName: [null, Validators.compose([Validators.required, this.service.nospaceValidator])],
      sTaxoType: [null, Validators.compose([Validators.required])],
      aTaxoType: [null, Validators.compose([Validators.required])],
      assoType: [null, Validators.compose([Validators.required])],
      metadata: [null, Validators.compose([Validators.required])]
    });
    this.userRoleRight();
    this.getAssociationPresetList(); // getting association preset list
    this.getAssociationType(); // getting association type list
    this.getTaxonomyTypes(); // getting taxonomy type list
    this.getAllMetadata(); // get all metadata list
    this.presetHeightCalculation();
    this.windowResizeSubscription = this.sharedService.windowResizeEvent.subscribe((item: any) => {
      if (item) {
        this.presetHeightCalculation();
      }
    });
  }

  ngOnDestroy() {
    if (this.windowResizeSubscription) {
      this.windowResizeSubscription.unsubscribe();
    }
  }

  /**
   * To get list of assocition presets
   */
  getAssociationPresetList() {
    const url = GlobalSettings.ASSOCIATION_PRESET_URL;
    this.presetLoading = true;
    this.associationPresets = [];
    this.service.getServiceData(url).then((res: any) => {
      this.associationPresets = res;
      this.associationPresets.forEach((data, index) => {
        data.isEditable = this.editPermission;
        data.isDelete = this.deletePermission;
        data.isView = true;
        data.showOptions = (!data.isEditable && !data.isDelete && !data.isView) ? false : true;
        data.id = 'preset' + (index + 1);
        data.metadataValues = [];
        this.updateMetadataValuesField(data);
      });
      if (this.associationPresets.length) {
        Utils.sortDataArray(this.associationPresets, 'title', false, true);
        this.onPresetActionSelect(this.associationPresets[0]);
      }
      this.presetLoading = false;
      setTimeout(() => {
        this.presetHeightCalculation();
      }, 100);
    }).catch((ex) => {
      console.log('Error caught while fetching association preset list', ex);
      this.presetLoading = false;
    });
  }

  /**
   * To get association type list
   */
  getAssociationType() {
    if (localStorage.getItem('access_token')) {
      const url = GlobalSettings.GET_CASE_ASSOCIATION_TYPES;
      this.associationTypes = [];
      this.assoTypeLoading = true;
      this.service.getServiceData(url).then((res: any) => {
        res.forEach(element => {
          if (element.type_name !== 'exemplar') {
            this.associationTypes.push(element);
          }
        });
        this.assoTypeLoading = false;
      }).catch(ex => {
        console.log('list of association types ex ', ex);
        this.assoTypeLoading = false;
      });
    }
  }

  /**
   * To get taxonomy type list
   */
  getTaxonomyTypes() {
    if (localStorage.getItem('access_token')) {
      this.taxonomyTypes = [];
      // For getting any metadata's field values with internal name, we have to send any random id as per backend logic
      const url = GlobalSettings.GET_METADATA_LIST + '/random_id' + '?internal_name=Taxonomy Type';
      this.service.getServiceData(url).then((res: any) => {
        if (res && res.length > 0) {
          res.forEach((element: any) => {
            this.taxonomyTypes.push(
              {
                label: element.value,
                value: element.key
              }
            );
          });
        }
      }).catch((ex) => {
        console.log('Error caught while fetching taxonomy types', ex);
      });
    }
  }

  /**
   * To get metadata list
   */
  getAllMetadata() {
    this.metadataList = [];
    this.metadataLoading = true;
    const url = GlobalSettings.GET_METADATA_LIST;
    this.service.getServiceData(url).then((res: any) => {
      if (res.metadata) {
        res.metadata.forEach((element: any) => {
          if (element.metadata_id) {
            this.metadataList.push({
              metadata_id: element.metadata_id,
              sort_order: element.order,
              name: element.name,
              is_custom: element.is_custom,
              is_mandatory: 0
            });
          }
        });
      }
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  /**
   * Create association-preset API calling method
   */
  createAssociationPreset() {
    if (this.assoPresetForm.valid) {
      this.actionLoading = true;
      const metadataSet = [];
      for (let i = 0; i < this.selectedMetadata.length; i++) {
        this.selectedMetadata[i]['sort_order'] = i + 1;
        const obj = {
          metadata_id: this.selectedMetadata[i]['metadata_id'],
          is_mandatory: this.selectedMetadata[i]['is_mandatory'],
          sort_order: i + 1
        };
        metadataSet.push(obj);
      }
      const url = GlobalSettings.ASSOCIATION_PRESET_URL;
      const body = {
        name: this.presetName,
        source_taxonomy_type_id: this.sTaxonomyType.value,
        target_taxonomy_type_id: this.aTaxonomyType.value,
        association_type: this.assoType.type_id,
        metadata: metadataSet
      };

      this.service.postService(url, body, 0, true).then((res: any) => {
        if (res) {
          if (res.success) { // success
            this.showMessage(res.message ? res.message.toString() : res.message);
            this.getAssociationPresetList();
            this.onCancel();
            this.closeModal(); // close opened modal
          } else { // error
            this.showMessage((res.message ? res.message.toString() : res.message), 'error');
          }
        }
        this.actionLoading = false;
      }).catch((ex: any) => {
        console.log('createAssociationPreset  ', ex);
        this.showMessage((ex.msg ? ex.msg.toString() : 'Failed to create. Please try later.'), 'error');
        this.actionLoading = false;
      });
    }
  }

  /**
   * Update association-preset API calling method
   */
  updateAssociationPreset() {
    if (this.assoPresetForm.valid) {
      this.dialogService.confirm('Confirm', 'Making this change will affect previously added associations. Are you confirm to update?')
        .then((confirmed) => {
          if (confirmed) {
            this.actionLoading = true;
            const metadataSet = [];
            for (let i = 0; i < this.selectedMetadata.length; i++) {
              this.selectedMetadata[i]['sort_order'] = i + 1;
              const obj = {
                metadata_id: this.selectedMetadata[i]['metadata_id'],
                is_mandatory: this.selectedMetadata[i]['is_mandatory'],
                sort_order: i + 1
              };
              metadataSet.push(obj);
            }
            const url = GlobalSettings.ASSOCIATION_PRESET_URL;
            const body = {
              name: this.presetName,
              source_taxonomy_type_id: this.sTaxonomyType.value,
              target_taxonomy_type_id: this.aTaxonomyType.value,
              association_type: this.assoType.type_id,
              metadata: metadataSet,
              node_type_id: this.selectedPreset.node_type_id
            };

            this.service.putService(url, body, 0, true).then((res: any) => {
              if (res) {
                if (res.success) { // success
                  this.showMessage(res.message ? res.message.toString() : res.message);
                  this.updatePresetListInView();
                  this.onCancel();
                  this.closeModal(); // close opened modal
                } else { // error
                  this.showMessage((res.message ? res.message.toString() : res.message), 'error');
                }
              }
              this.actionLoading = false;
            }).catch((ex: any) => {
              console.log('updateAssociationPreset  ', ex);
              this.showMessage((ex.msg ? ex.msg.toString() : 'Failed to update. Please try later.'), 'error');
              this.actionLoading = false;
            });
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    }
  }

  /**
   * Delete association preset API calling method
   * @param sourceTaxoTypeId (source taxonomy type value)
   * @param targetTaxotypeId (destination taxonomy type value)
   * @param assotypeId (association type value)
   */
  deleteAssociationPreset(sourceTaxoTypeId: any, targetTaxotypeId: any, assotypeId: any) {
    if (sourceTaxoTypeId && targetTaxotypeId && assotypeId) {
      this.dialogService.confirm('Confirm', 'Do you want to delete ' + this.selectedPreset.title + ' ?')
        .then((confirmed) => {
          if (confirmed) {
            this.actionLoading = true;
            const url = GlobalSettings.ASSOCIATION_PRESET_URL;
            const body = {
              source_taxonomy_type_id: sourceTaxoTypeId,
              target_taxonomy_type_id: targetTaxotypeId,
              association_type: assotypeId
            };

            this.service.deleteServiceData(url, true, body).then((res: any) => {
              if (res) {
                if (res.success) { // success
                  this.showMessage(res.message ? res.message.toString() : res.message);
                  this.removePresetListInView();
                  this.onCancel();
                  if (this.associationPresets.length) {
                    this.onPresetActionSelect(this.associationPresets[0]);
                  }
                } else { // error
                  this.showMessage((res.message ? res.message.toString() : res.message), 'error');
                }
              }
              this.actionLoading = false;
            }).catch((ex: any) => {
              console.log('deleteAssociationPreset  ', ex);
              this.showMessage((ex.msg ? ex.msg.toString() : 'Failed to delete. Please try later.'), 'error');
              this.actionLoading = false;
            });
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    } else {
      this.showMessage('This preset can not be deleted. Some data is missing.', 'error');
    }
  }

  /**
   * To get association preset
   * @param sourceTaxoTypeId (source taxonomy type value)
   * @param targetTaxotypeId (destination taxonomy type value)
   * @param assotypeId (association type value)
   */
  getPreset(sourceTaxoTypeId: any, targetTaxotypeId: any, assotypeId: any) {
    if (sourceTaxoTypeId && targetTaxotypeId && assotypeId) {
      const url = GlobalSettings.ASSOCIATION_PRESET_URL + '/' + sourceTaxoTypeId + '/' + targetTaxotypeId + '/' + assotypeId;
      this.service.getServiceData(url).then((res: any) => {
        this.selectedPreset = res;
      }).catch((ex) => {
        console.log('Error caught while fetching association preset', ex);
      });
    }
  }

  userRoleRight() {
    if (this.authenticateUser.AuthenticateMetadata('Create association configurations')) {
      this.createPermission = true;
    } else {
      this.createPermission = false;
    }
    if (this.authenticateUser.AuthenticateMetadata('Edit association configuration')) {
      this.editPermission = true;
    } else {
      this.editPermission = false;
    }
    if (this.authenticateUser.AuthenticateMetadata('Delete association configuration')) {
      this.deletePermission = true;
    } else {
      this.deletePermission = false;
    }

    this.optionList = [
      {
        name: 'View',
        type: this.VIEW_ACTION,
        value: true,
        modal: '#asso-preset-modal',
        check: 'isView'
      },
      {
        name: 'Edit',
        type: this.EDIT_ACTION,
        value: this.editPermission,
        modal: '#asso-preset-modal',
        check: 'isEditable'
      },
      {
        name: 'Delete',
        type: this.DELETE_ACTION,
        value: this.deletePermission,
        modal: '',
        check: 'isDelete'
      }
    ];
  }

  // Method for create or update preset using API calling based on operation
  assoPresetAction() {
    if (this.operation === this.CREATE_ACTION) { // Create preset
      this.createAssociationPreset();
    } else if (this.operation === this.EDIT_ACTION) { // Edit preset
      this.updateAssociationPreset();
    }
  }

  // On create button click
  onCreateBtnClick() {
    this.operation = this.CREATE_ACTION;
    this.focusOnCloseBtn();
  }

  // On edit option click
  onEditBtnClick() {
    this.operation = this.EDIT_ACTION;
    this.focusOnCloseBtn();
    this.setSelectedPresetValues(this.selectedPreset);
  }

  // On delete option click
  onDeleteBtnClick() {
    this.operation = this.DELETE_ACTION;
    this.deleteAssociationPreset(this.selectedPreset.source_document_type, this.selectedPreset.destination_document_type,
      this.selectedPreset.item_association_type);
  }

  // On view option click
  onViewBtnClick() {
    this.operation = this.VIEW_ACTION;
    this.focusOnCloseBtn();
  }

  // On preset select from left side preset list
  onPresetActionSelect(data: any, actiontype = this.VIEW_ACTION) {
    this.selectedPreset = data;
    this.operation = actiontype;
    switch (actiontype) {
      case this.EDIT_ACTION: // edit
        this.onEditBtnClick();
        break;
      case this.DELETE_ACTION: // delete
        this.onDeleteBtnClick();
        break;
      case this.VIEW_ACTION: // view
        this.onViewBtnClick();
        break;
      default:
    }
  }

  /**
   * On assotion type dropdown select
   * @param event (selected association data)
   */
  onAssoTypeSelect(event: any) {
    this.assoType = event;
    // setting value in form group
    if (event) {
      this.assoPresetForm.controls['assoType'].setValue(event);
    } else {
      this.assoPresetForm.controls['assoType'].setValue(null);
    }
  }


  /**
   * On metadata dropdown select
   * @param event (selected metadata data)
   */
  onMetadataSelect(event: any) {
    if (event.length) {
      for (const element of event) {
        if (this.selectedMetadata.find(elem => elem.metadata_id === element.metadata_id) === undefined) {
          element.sort_order = (this.selectedMetadata.length + 1);
          this.selectedMetadata.push(element);
        }
      }
    }
    this.assoPresetForm.controls['metadata'].setValue(this.selectedMetadata);
  }

  onClickedOutside(event: any, type: string) {
    if (type === 'autoCompleteAsso' && this.autoCompleteAsso) { // association type dropdown
      this.autoCompleteAsso.openPanel(false);
    } else if (type === 'autoCompleteMeta' && this.autoCompleteMeta) { // metadata dropdown
      this.autoCompleteMeta.openPanel(false);
    }
  }

  // On more options click from table
  onOptionClicked(event: any) {
    this.operation = event.clickedOn;
    this.onPresetActionSelect(event.data, event.clickedOn);
  }

  onNodeRearranged(event: any) {
    // console.log(this.selectedMetadata);
  }

  // Remove metadata from selected list in UI
  deleteMetadata(index: number, metadata: any) {
    const metadataId = metadata.metadata_id;
    metadata.is_mandatory = 0;
    metadata.is_selected = 0;
    this.selectedMetadata.splice(index, 1);
    this.assoPresetForm.controls['metadata'].setValue(this.selectedMetadata); // updating form group
    this.updateDropdownSelectionList(metadataId);
  }

  // update selection of options in metadata dropdown
  updateDropdownSelectionList(metadataId: string) {
    this.metadataList.forEach(obj => {
      if (obj['metadata_id'] === metadataId) {
        obj['is_selected'] = 0;
        this.autoCompleteMeta.unselectObjInList(obj);
      }
    });
  }

  // Set selected Association Preset data in variables and fromgroup
  setSelectedPresetValues(data: any) {
    this.presetName = data.title;
    this.sTaxonomyType = this.getTaxotypeById(data.source_document_type);
    this.aTaxonomyType = this.getTaxotypeById(data.destination_document_type);
    this.assoType = this.getAssotypeById(data.item_association_type);
    this.selectedMetadata = JSON.parse(JSON.stringify(data.metadata));
    this.assoPresetForm.controls['presetName'].setValue(this.presetName);
    this.assoPresetForm.controls['sTaxoType'].setValue(this.sTaxonomyType);
    this.assoPresetForm.controls['aTaxoType'].setValue(this.aTaxonomyType);
    this.assoPresetForm.controls['assoType'].setValue(this.assoType);
    this.assoPresetForm.controls['metadata'].setValue(this.selectedMetadata);
  }

  // Updating preset list for any updated data after successful update
  updatePresetListInView() {
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'title', this.presetName);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'source_document_type', this.sTaxonomyType.value);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'source_document_name', this.sTaxonomyType.label);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'destination_document_type', this.aTaxonomyType.value);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'destination_document_name', this.aTaxonomyType.label);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'item_association_type', this.assoType.type_id);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'item_association_name', this.assoType.display_name);
    Utils.updateArray(this.selectedPreset.id, this.associationPresets, 'id', 'metadata', this.selectedMetadata);
    for (const element of this.associationPresets) { // updating matadata values by comma separating string
      if (element['id'] === this.selectedPreset.id) {
        this.updateMetadataValuesField(element);
        break;
      }
    }
  }

  // Removing selected preset from list
  removePresetListInView() {
    const index = this.associationPresets.findIndex(elem => elem.id === this.selectedPreset.id);
    if (index > -1) {
      this.associationPresets.splice(index, 1);
    }
  }

  // To get taxonomy type value from taxonomy type list by id
  getTaxotypeById(id: any) {
    let foundElem = null;
    if (this.taxonomyTypes) {
      foundElem = this.taxonomyTypes.find(elem => elem.value === id);
    }
    return foundElem;
  }

  // To get association type value from association type list by id
  getAssotypeById(id: any) {
    let foundElem = null;
    if (this.associationTypes) {
      foundElem = this.associationTypes.find(elem => elem.type_id === id);
    }
    return foundElem;
  }

  // To set metadata list as comma separated string values
  updateMetadataValuesField(presetData: any) {
    if (presetData) {
      presetData.metadataValues = [];
      if (presetData.metadata) {
        presetData.metadata.forEach((element: any) => {
          presetData.metadataValues.push(element.name);
        });
      }
      presetData.metadataValues = presetData.metadataValues.toString().replace(/,/gi, ', '); // replacing ',' with ', ';
      presetData.metadataValues = presetData.metadataValues.length ? presetData.metadataValues : 'No metadata found';
    }
  }

  onCancel() {
    this.assoPresetForm.reset();
    this.clearValues();
  }

  closeModal() {
    setTimeout(() => {
      if (document.getElementById('closeasso-preset-modal')) {
        document.getElementById('closeasso-preset-modal').click();
      }
    }, 150);
  }

  // Clearing values for association preset's variables
  clearValues() {
    this.presetName = '';
    this.sTaxonomyType = null;
    this.aTaxonomyType = null;
    this.assoType = null;
    this.selectedMetadata = [];
    // this.selectedPreset = {};
    if (this.autoCompleteAsso) {
      this.autoCompleteAsso.clearSelection();
    }
    if (this.autoCompleteMeta) {
      this.autoCompleteMeta.clearSelection();
    }
  }

  focusOnCloseBtn() {
    setTimeout(() => {
      if (document.getElementById('closeasso-preset-modal')) {
        document.getElementById('closeasso-preset-modal').focus();
      }
    }, 700);
  }

  showMessage(msg: string, msgType?: string) {
    if (msgType) {
      this.sharedService.sucessEvent.next({
        customMsg: Utils.getFirstCapitalLetterString(msg),
        type: msgType
      });
    } else {
      this.sharedService.sucessEvent.next({
        customMsg: Utils.getFirstCapitalLetterString(msg)
      });
    }
  }

  presetHeightCalculation() {
    const windowHeight = window.innerHeight;
    let tabHeight = 0;
    let titleHeight = 0;
    let presetBtnSectionHeight = 0;
    if (document.querySelectorAll('.tab-items-section') && document.querySelectorAll('.tab-items-section')[0]) {
      tabHeight = document.querySelectorAll('.tab-items-section')[0].clientHeight;
    }
    if (document.querySelectorAll('#preset-btn-section') && document.querySelectorAll('#preset-btn-section')[0]) {
      presetBtnSectionHeight = document.querySelectorAll('#preset-btn-section')[0].clientHeight;
    }
    if (document.getElementsByClassName('authoring-header') && document.getElementsByClassName('authoring-header')[0]) {
      titleHeight = document.getElementsByClassName('authoring-header')[0].clientHeight;
    }
    const extraHeight = 58;
    const panelHeight = windowHeight - (tabHeight + titleHeight + presetBtnSectionHeight + extraHeight);
    if (document.getElementById('association-preset-left')) {
      document.getElementById('association-preset-left').style.height = panelHeight + 'px';
    }
    if (document.getElementById('association-preset-right')) {
      document.getElementById('association-preset-right').style.height = panelHeight + 'px';
    }
  }

}
