import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import {
  Observable
} from 'rxjs';
import {
  HttpClient
} from '@angular/common/http';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../../help/walkthrough/walktrough.service';*/
import {
  format
} from 'date-fns';
import {
  AutoCompleteComponent
} from '../../../common/auto-complete/auto-complete.component';
import Utils from 'src/app/utils';
@Component({
  selector: 'app-metadata-set',
  templateUrl: './metadata-set.component.html',
  styleUrls: ['./metadata-set.component.scss']
})
export class MetadataSetComponent implements OnInit {

  nodetypeList;
  metadataList = [];
  metadataObj = [];
  originalObj = [];
  originalTitle;
  NodeTypeMetadata = [];
  name = '';
  public nodetype: Nodetype = <Nodetype>{};
  selectedMetadata = null;
  formValidation = false;
  form: FormGroup;
  searchText = true; // flag to determine the text search has populated any data in metadatalist array
  tab = 'metadataset';
  editMetadata;
  createMetadata; // holds boolean value related to permission for create metadata
  deleteMetadata; // holds boolean value related to permission for delete metadata
  duplicateMetadata; // holds boolean value related to permission for duplicate metadata
  mandatoryImsCaseFields = []; // holds list of all mandatory IMS case fields
  searchListReult;
  selectType = 'single'; // holds the dropdown selection criteria as in multi or single
  uniqueId = 'metadata_id'; // holds the unique id for the auto complete items
  autoCompleteTitle = 'Select Metadata'; // title to be sent for auto complete dropdown
  textToSearch = '';
  searchedNodeType = [];
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'title',
    class: '',
    type: 'text'
  },
  {
    name: 'Creator',
    propName: 'created_by',
    class: '',
    width: '20%',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Updated at',
    propName: 'updated_at',
    class: '',
    width: '20%',
    type: 'text'
  }
  ];
  optionList = [];
  @ViewChild('editMetadataSet', { static: false }) editMetadataSet;
  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;

  constructor(
    private service: CommonService,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    private fb: FormBuilder,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService,*/
    private AuthenticateUser: AuthenticateUserService) {
    /* Search Event Subscription Started */
    this.sharedService.searchEvent.subscribe((event: any) => {
      if (event.type && event.type === 'Metadata' && this.tab === 'metadataset') {
        this.onSearchTextChange(event.text);
        this.textToSearch = event.text;
      } else {
        this.textToSearch = '';
      }
    });
    /* Search Event Subscription Ended */
  }

  ngOnInit() {
    this.form = this.fb.group({
      metadatasetname: [null, Validators.compose([Validators.required])],
    });
    this.userRoleRight();
    setTimeout(() => {
      this.getAllNodeTypes();
    }, 1000);
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('metadataset_list'));*/
  }

  /* --------- Functionality to fetch all node types start --------- */

  getAllNodeTypes() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.nodetypeList = res.nodetype;
      this.nodetypeList.forEach(element => {
        if (element.created_by === '') {
          element.created_by = 'System';
        }
        element.updated_at = this.formateDate(element.updated_at);
        element.isEditable = this.editMetadata;
        element.isDelete = element.is_document === 1 || element.is_default === 1 ? false : this.deleteMetadata;
        element.isDuplicate = this.duplicateMetadata;
        element.showOptions = (!element.isEditable && !element.isDelete && !element.isDuplicate) ? false : true;
        // if (element.is_default) {
        //   element.isDelete = false;
        // }
      });
      if (this.nodetypeList.length > 0) {
        this.searchText = false;
      } else {
        this.searchText = true;
      }
      // this.userRoleRight();
    }).catch((ex) => {
      console.log('Error caught while fetching metadata set list', ex);
    });
  }

  /* --------- Functionality to fetch all node types end --------- */


  /* --------- Functionality to populate metadata dropdown start --------- */

  getAllMetadata(control, nodeData) {
    this.mandatoryImsCaseFields = [];
    const url = GlobalSettings.GET_METADATA_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.metadataList = res.metadata;
      const tempObj = [];
      // tempObj.push(this.getDefaultMetaData());
      this.metadataList.forEach(element => {
        element['is_selected'] = 0;
        if (element.is_active === 1 && control === 'create') {
          if (element.is_document === 0 || element.is_document === 2) {
            tempObj.push(element);
            if (element.is_mandatory === 1) {
              this.updateMandatoryImsFieldList(element.name);
            }
          }
        } else if (element.is_active === 1 && control === 'edit') {
          if (nodeData.title === 'Document' && nodeData.is_document === 1) {
            if (element.is_document === 1 || element.is_document === 2 || element.is_custom === 1) {
              tempObj.push(element);
              if (element.is_mandatory === 1) {
                this.updateMandatoryImsFieldList(element.name);
              }
            }
          } else {
            if (element.is_document === 0 || element.is_document === 2) {
              tempObj.push(element);
              if (element.is_mandatory === 1) {
                this.updateMandatoryImsFieldList(element.name);
              }
            }
          }
        }
        this.metadataList = tempObj;
        this.searchListReult = tempObj;
        this.selectedMetadata = this.metadataList[0];
      });
      if (control === 'create') {
        this.addCaseFields();
      }
      if (control === 'edit') {
        this.getNodeTypeMetadata(this.nodetype.node_type_id);
      }
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  updateMandatoryImsFieldList(field) {
    this.mandatoryImsCaseFields.push(field);
  }

  /* --------- Functionality to populate metadata dropdown end --------- */


  /* --------- Functionality to add case fields by default start --------- */

  addCaseFields() {
    for (const i in this.metadataList) {
      if (this.metadataList[i]['is_custom'] === 0) {
        this.metadataList[i]['is_selected'] = 1;
        this.metadataObj.push({
          'id': this.metadataList[i]['metadata_id'],
          'order': i,
          'name': this.metadataList[i]['name'],
          'is_custom': this.metadataList[i]['is_custom'],
          'is_mandatory': this.metadataList[i]['is_mandatory']
        });
      }
    }
  }

  /* --------- Functionality to add case fields by default end --------- */


  /* --------- Functionality to select metadata on click of + button start --------- */

  selectMetadata() {
    let childflag = 0;
    let parentflag = 0;
    const metadata_id = this.selectedMetadata.metadata_id;
    this.metadataList.forEach(element => {
      if (element.metadata_id === metadata_id) {

        this.metadataObj.forEach(item => {
          if (item.id === metadata_id) {
            childflag = 1;
          }
        });
        if (childflag === 0) {
          element['is_selected'] = 1;
          this.metadataObj.push({
            'id': metadata_id,
            'order': this.metadataObj.length + 1,
            'name': element.name,
            'is_custom': element.is_custom,
            'is_mandatory': element.is_mandatory
          });
          this.checkValidation();
          this.autoComplete.openPanel(false);

        } else {
          // console.log('child already present');
          // this.sharedService.sucessEvent.next({
          //   type: 'duplicate_metadata'
          // });
        }

        const parent_id = element.parent_metadata_id;
        if (parent_id.length > 0) {
          this.metadataList.forEach(value => {
            if (value.metadata_id === parent_id) {
              this.metadataObj.forEach(item => {
                if (item.id === parent_id) {
                  parentflag = 1;
                }
              });
              if (parentflag === 0) {
                value['is_selected'] = 1;
                this.metadataObj.push({
                  'id': parent_id,
                  'order': this.metadataObj.length + 1,
                  'name': value.name,
                  'is_custom': value.is_custom,
                  'is_mandatory': value.is_mandatory
                });
              } else {
                // console.log('Parent already present');
              }
            }
          });
        }
      }
    });
  }

  /* --------- Functionality to select metadata on click of + button end --------- */


  /* --------- Functionality on update or create button click start --------- */

  updateMetadataSetName(event, type) {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    if (this.nodetype.node_type_id === undefined) {
      this.service.postService(url, this.nodetype, 0, true).then((res: any) => {
        if (res) {
          if (!res.success) {
            this.sharedService.sucessEvent.next({
              customMsg: res.message[0],
              type: 'error'
            });
          } else {
            if (res.data) {
              this.getAllNodeTypes();
              this.saveNodeTypeMetadata(event, type, res.data['node_type_id']);
            }
          }
        }
      }).catch(ex => {
        console.log('create nodetype', ex);
      });
    } else {
      this.service.putService(url + '/' + this.nodetype.node_type_id, this.nodetype, 0, true).then((res: any) => {
        if (res) {
          if (!res.success) {
            this.sharedService.sucessEvent.next({
              customMsg: res.message[0],
              type: 'error'
            });
          } else {
            if (res.data) {
              this.getAllNodeTypes();
              // console.log(this.nodetype.node_type_id, this.nodetypeList, 'node_type_id', 'title', this.nodetype.name);
              // Utils.updateArray(this.nodetype.node_type_id, this.nodetypeList, 'node_type_id', 'title', this.nodetype.name);
              this.saveNodeTypeMetadata(event, type, res.data['node_type_id']);
            }
          }
        }
      }).catch(ex => {
        console.log('update nodetype', ex);
      });
    }
  }
  saveNodeTypeMetadata(event, type, nodetypeID) {
    // const nodetypeID = this.nodetype['node_type_id'];
    const url = GlobalSettings.SAVE_NODETYPE_METADATA + '/' + nodetypeID;
    for (let i = 0; i < this.metadataObj.length; i++) {
      this.metadataObj[i]['order'] = i + 1;
    }
    const flag = this.checkImsCaseFields();

    if (flag === 0) {

      const message = 'This metadata set does not contain all mandatory fields as per IMS CASE" are you sure you want to continue?';
      this.dialogService.confirm('Confirm', message)
        .then((confirmed) => {
          if (confirmed) {
            this.postService(url, type);
          } else {
            console.log('User cancel the dialog while confirming');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog while confirming');
        });
    } else {
      this.postService(url, type);
    }
  }

  postService(url, type) {
    const data = {
      'metadata': this.metadataObj
    };
    this.service.postService(url, data).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: type
      });
      this.onCancel();
      setTimeout(() => {
        this.closeModal();
      }, 200);
      this.metadataObj = [];
    }).catch(ex => {
      console.log('create nodetype', ex);
    });
  }

  checkImsCaseFields() {
    console.log('this.mandatoryImsCaseFields', this.mandatoryImsCaseFields);
    let flag = 1;
    for (const mandatoryField in this.mandatoryImsCaseFields) {
      if (mandatoryField) {
        for (const metadata in this.metadataObj) {
          if (metadata) {
            if (this.mandatoryImsCaseFields[mandatoryField] === this.metadataObj[metadata]['name']) {
              if (this.metadataObj[metadata]['is_mandatory'] === 0) {
                flag = 0;
              } else {
                flag = 1;
              }
              break;
            }
          }
        }
        if (flag === 0) {
          break;
        }
      }
    }
    return flag;
  }

  /* --------- Functionality on update or create button click end --------- */


  /* --------- Functionality to fetch existing metadata of particular node type start --------- */

  getNodeTypeMetadata(nodetypeID) {
    this.metadataObj = [];
    const url = GlobalSettings.SAVE_NODETYPE_METADATA + '/' + this.nodetype.node_type_id;
    this.service.getServiceData(url).then((res: any) => {
      this.NodeTypeMetadata = res;
      this.NodeTypeMetadata.forEach(element => {
        if (element.metadata_id) {
          this.metadataObj.push({
            'id': element.metadata_id,
            'order': element.order,
            'name': element.name,
            'is_custom': element.is_custom,
            'is_mandatory': element.is_mandatory
          });
        }
        this.updateDropdownSelectionList(element.metadata_id, 'edit');
      });
      this.originalObj = JSON.parse(JSON.stringify(this.metadataObj));
    }).catch((ex) => {

    });
  }

  /* --------- Functionality to fetch existing metadata of particular node type end --------- */


  /* --------- Functionality call on create or edit button click on listing start --------- */

  onEditNodeType(data) {
    this.autoComplete.initialiseSelectedArray();
    this.autoComplete.openPanel(false);
    if (data && data !== undefined) {
      // this.editMetadataSet.nativeElement.click();
      this.getAllMetadata('edit', data);
      this.nodetype = data;
      this.nodetype.name = data.title;
      this.originalTitle = JSON.parse(JSON.stringify(data)).title;
      // this.getNodeTypeMetadata(this.nodetype.node_type_id);
    } else {
      this.getAllMetadata('create', '');
      this.nodetype = <Nodetype>{};
      this.NodeTypeMetadata = [];
      this.metadataObj = [];
      this.originalTitle = '';
    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'create_metadataset'
    });
  }

  /* --------- Functionality call on create or edit button click on listing start --------- */


  /* --------- Functionality to delete the node type from listing start --------- */

  deleteNodeType(nodetypeId, nodeTitle, index) {

    this.dialogService.confirm('Confirm', 'Do you want to delete metadataset  ' + nodeTitle + ' ?')
      .then((confirmed) => {
        if (confirmed) {
          // this.nodetypeList.splice(index,1);
          const url = GlobalSettings.GET_NODETYPE + '/' + nodetypeId;
          this.service.deleteServiceData(url).then((res: any) => {
            if (res && res.node_usage === 1) {
              this.sharedService.sucessEvent.next({
                type: 'prevent_delete_metadataset'
              });
            } else {
              // this.getAllNodeTypes();
              this.deleteMetadataSetFromTable(nodetypeId);
              this.sharedService.sucessEvent.next({
                type: 'delete_metadataset'
              });
            }
          }).catch((ex) => {
            console.log('Error while deleting the role', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }

  /* --------- Functionality to delete the node type from listing end --------- */


  /* --------- Functionality to delete the metadata from list preview start --------- */

  deleteMetadataValue(index, metadata_id, parent_metadata_id) {
    const tempObj = [];
    this.metadataObj.splice(index, 1);
    if (this.metadataObj.length < 1) {
      this.formValidation = false;
    } else {
      this.formValidation = true;
    }
    this.checkValidation();
    this.updateDropdownSelectionList(metadata_id, 'delete');
  }

  /* --------- Functionality to delete the metadata from list preview end --------- */


  /* --------- Functionality to update the dropdown based on the list preview start --------- */

  updateDropdownSelectionList(metadata_id, operation) {
    this.metadataList.forEach(obj => {
      if (operation === 'delete') {
        if (obj['metadata_id'] === metadata_id) {
          obj['is_selected'] = 0;
          this.autoComplete.unselectObjInList(obj);
        }
      }
      if (operation === 'edit') {
        if (obj['metadata_id'] === metadata_id) {
          obj['is_selected'] = 1;
        }
      }
    });
  }

  /* --------- Functionality to update the dropdown based on the list preview end --------- */


  /* --------- Functionality called on cancle button start --------- */

  onCancel() {
    this.form.controls.metadatasetname.reset();
    if (this.metadataList && this.metadataList.length > 0) {
      this.selectedMetadata = this.metadataList[0];
    }
    this.formValidation = false;
    this.autoComplete.clearSelection();
    this.sharedService.faqEvent.next({
      name: 'metadata_list'
    });
  }

  /* --------- Functionality called on cancle button end --------- */

  formateDate(date) {
    try {
      return format(new Date(date), 'YYYY-MM-DD');
    } catch (error) {
      console.log('formateDate  ', error);
    }

  }

  /* --------- Search functionality Started --------- */
  onSearchTextChange(txt) {
    // if (txt !== undefined && txt !== '' && txt.length > 2) {
    //   const url = GlobalSettings.NODETYPE_SEARCH + txt;
    //   this.service.getServiceData(url).then((res: any) => {
    //     this.nodetypeList = res.nodetype;
    //   }).catch(ex => {
    //     console.log('list of taxonomies ex ', ex);
    //   });
    // } else {
    //   this.getAllNodeTypes();
    // }

    if (this.nodetypeList) {

      if (txt === undefined || txt === '' || txt.length < 3) {
        this.searchedNodeType = this.nodetypeList;
      } else {
        this.searchedNodeType = this.nodetypeList.filter(obj => {
          if ((obj.title.toLowerCase()).includes(txt.toLowerCase())) {
            return obj;
          }
        });
      }
    }
  }
  /* --------- Search functionality Ended --------- */

  /* --------- List all metadata set list on close of alert start --------- */

  // onClose() {
  //   this.getAllNodeTypes();
  // }

  /* --------- List all metadata set list on close of alert end --------- */


  /* --------- Check permission functionality start --------- */

  userRoleRight() {
    if (this.AuthenticateUser.AuthenticateMetadataSet('Edit NodeType')) {
      this.editMetadata = true;
    } else {
      this.editMetadata = false;
    }
    if (this.AuthenticateUser.AuthenticateMetadataSet('Create NodeType')) {
      this.createMetadata = true;
    } else {
      this.createMetadata = false;
    }
    if (this.AuthenticateUser.AuthenticateMetadataSet('Delete NodeType')) {
      this.deleteMetadata = true;
    } else {
      this.deleteMetadata = false;
    }
    if (this.AuthenticateUser.AuthenticateMetadataSet('Duplicate NodeType')) {
      this.duplicateMetadata = true;
    } else {
      this.duplicateMetadata = false;
    }

    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit',
      type: 'editMetadata',
      value: this.editMetadata,
      modal: '#add-metadataset',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'deleteMetadata',
      value: this.deleteMetadata,
      modal: '',
      check: 'isDelete'
    },
    {
      name: 'Duplicate',
      type: 'duplicateMetadata',
      value: this.duplicateMetadata,
      modal: '',
      check: 'isDuplicate'
    }
    ];
  }

  getDefaultMetaData() {
    return {
      name: 'Select Metadata',
      metadata_id: undefined
    };
  }

  /* --------- Check permission functionality end --------- */


  /* --------- On excape key press function start --------- */
  mouseLeave(event) {
    if (event && event.keyCode === 27) {
      this.onCancel();
    } else {
      const self = this;
      const modal = document.getElementById('add-metadataset');
      window.onclick = function (evt) {
        if (evt.target === modal) {
          self.onCancel();
        }
      };
    }
  }
  /* --------- On excape key press function end --------- */


  /* --------- Duplicate node type node type functionality start --------- */
  duplicateNodeType(node_type_id) {
    const url = GlobalSettings.DUPLICATE_NODE_TYPE + '/' + node_type_id;
    this.service.postService(url, '').then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'duplicate_metadata'
      });
      this.getAllNodeTypes();
    }).catch((ex) => {
      console.log('createMetadata error occured', ex);
    });
  }

  /* --------- Duplicate node type node type functionality end --------- */


  /* --------- Functionality on nodes rearranged start --------- */
  onNodeRearranged(evt) {
    this.formValidation = true;
    this.checkValidation();
  }
  /* --------- Functionality on nodes rearranged end --------- */


  /* --------- Functionality to check validation for buttons start --------- */

  checkValidation() {
    if ((this.nodetype.name === this.originalTitle) &&
      (JSON.stringify(this.originalObj) === JSON.stringify(this.metadataObj))) {
      this.formValidation = false;
    } else {
      if (this.nodetype && this.nodetype.name) {
        const title = this.nodetype.name.trim();
        if (this.metadataObj.length > 0 && (title && title.length > 0 && title !== '')) {
          this.formValidation = true;
        } else {
          this.formValidation = false;
        }
      }
    }
  }
  /* --------- Functionality to check validation for buttons start --------- */


  /* --------- Functionality to identify the target element and toggle state of autocomplete dropdown start --------- */
  toggleOpen(val) {
    if (val.id) {
      if (val.id === 'metadataSetDropdown') {
        this.autoComplete.openPanel(true);
      } else {
        this.autoComplete.openPanel(false);
      }
    } else {
      console.log(val);
    }
  }
  /* --------- Functionality to identify the target element and toggle state of autocomplete dropdown end --------- */


  /* --------- Functionality to update the selected object from autocomplete start --------- */
  updateSelectedObject(selectedMetadata) {
    this.selectedMetadata = selectedMetadata;
    if (selectedMetadata.hasOwnProperty('is_selected')) {
      this.selectMetadata();
    }
  }
  /* --------- Functionality to update the selected object from autocomplete end --------- */


  /* --------- Functionality to detect is clicked outside autocomplete component start --------- */

  onClickedOutside(e: Event) {
    this.autoComplete.openPanel(false);
  }

  onOptionClicked(event) {
    console.log(event);
    switch (event.clickedOn) {
      case 'editMetadata':
        this.nodetype.node_type_id = event.data.node_type_id;
        this.onEditNodeType(event.data);
        break;
      case 'deleteMetadata':
        this.deleteNodeType(event.data.node_type_id, event.data.title, event.index);
        break;
      case 'duplicateMetadata':
        this.duplicateNodeType(event.data.node_type_id);
        break;
      default:
    }
  }

  /* --------- Functionality to detect is clicked outside autocomplete component end --------- */

  closeModal() {
    if (document.getElementById('cancel-modal-btn-add-metadataset')) {
      document.getElementById('cancel-modal-btn-add-metadataset').click();
    }
  }

  /**
   * To remove metadata set from table list
   * @param id (metadata set id)
   */
  deleteMetadataSetFromTable(id: any) {
    if (this.nodetypeList && this.nodetypeList.length) {
      this.nodetypeList = this.nodetypeList.filter(metadataSet => {
        return (metadataSet.node_type_id !== id);
      });
    }
  }

}
