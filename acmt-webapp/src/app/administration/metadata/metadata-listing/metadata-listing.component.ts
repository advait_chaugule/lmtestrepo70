import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  DoCheck
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import {
  Observable
} from 'rxjs';
import {
  HttpClient
} from '@angular/common/http';
import Utils from '../../../utils';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../../help/walkthrough/walktrough.service';*/

@Component({
  selector: 'app-metadata-listing',
  templateUrl: './metadata-listing.component.html',
  styleUrls: ['./metadata-listing.component.scss']

})
export class MetadataListingComponent implements OnInit {

  public metadata: Metadata = <Metadata>{};
  form: FormGroup; // form declaration
  createMetadataForm: FormGroup; // form declaration
  metadataList; // Array for all metadata list items
  metadata_id: string; // unique metadata identifier
  tabItems = ['metadata', 'metadataset', 'association presets']; // contains all the tabs for the current view
  currentTab = this.tabItems[0].toLowerCase(); // selects the current tab as default view
  editMetadata: boolean; // flag for displaying edit button based on permission
  viewMetadata: boolean; // // flag for displaying data based on permission
  delMetadata: boolean; // flag for displaying data based on permission
  viewMetaConfig: boolean; // flag for displaying association preset configuration list
  metaDataTab = true;
  metadatasetTab = false;
  searchText: boolean; // flag to determine the text search has populated any data in metadatalist array
  @ViewChild('updateMetadata', { static: false }) updateMetadata;
  metadataValue = [];
  associatedMetadata: any[] = [];
  listValueName = [];
  selectName = '';
  selectCode = '';
  language_id = '';
  listType;
  valid = false;
  metadataName;
  initialListData;
  initialCustomListData;
  listTypeSelected = false;
  editAddedListItem = false;
  createMetadataBtn = false;
  editObj = {};
  customList = [''];
  customListOriginal = [''];
  createMetadataObj;
  validateCreate = false;
  editCustom = false;
  customMetadataListItem;
  editAddedCustomListItem = false;
  editCustomMetadataListIndex;
  updatedData;
  inputTypeValue;
  curentItem;
  textToSearch = '';
  searchedMeta = [];
  disableButton = false;
  caseField = ''; // holds original case field value from util array
  metaFieldValLoading = false; // loading state for metadata field list fetching
  tempFieldList = [];
  updateLoading = false; // loading state for add/update metadata
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'name',
    class: '',
    type: 'text'
  },
  {
    name: 'Type',
    propName: 'type',
    class: '',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Status',
    propName: 'isActive',
    class: '',
    type: 'dropdown',
    canFilter: true
  },
  {
    name: 'Usage',
    propName: 'usage_count',
    class: '',
    type: 'text'
  },
  {
    name: 'Defined By',
    propName: 'defined_by',
    class: '',
    width: '12%',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Case field',
    propName: 'caseField',
    class: '',
    width: '15%',
    type: 'text',
    canFilter: true
  }
  ];
  optionList = [];
  statusList = [{
    name: 'Active',
    value: '1',
    propName: 'metadata_id'
  },
  {
    name: 'Inactive',
    value: '0',
    propName: 'metadata_id'
  }
  ];

  roleStatusButton = false;
  disableAddingListItem = true; // holds boolean to disable adding items in list only for source_type metadata

  constructor(
    private sharedService: SharedService,
    private service: CommonService,
    private http: HttpClient,
    private router: Router,
    private AuthenticateUser: AuthenticateUserService,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService,*/
    private fb: FormBuilder,
    private dialogService: ConfirmationDialogService) {

    /* Search Event Subscription Started */
    this.sharedService.searchEvent.subscribe((event: any) => {
      if (event.type && event.type === 'Metadata' && this.metaDataTab === true) {
        this.onSearchTextChange(event.text);
        this.textToSearch = event.text;
      } else {
        this.textToSearch = '';
      }
    });
    /* Search Event Subscription Ended */


  }

  ngOnInit() {
    this.form = this.fb.group({
      metadataname: [null, Validators.compose([Validators.required])],
      shortCode: [null],
      languageName: [null]
    });

    this.createMetadataForm = this.fb.group({
      customMetadataOptions: [null, Validators.compose([Validators.required])],
      customMetadataName: [null, Validators.compose([Validators.required])]
    });


    console.log('Metadata init called');
    this.getAllMetadata();
    this.userRoleRight();
    // this.tourService.end();
    // this.tourService.initialize(this.walkService.getWalkthroughData('metadata_list'));
    this.sharedService.faqEvent.next({
      name: 'metadata_list'
    });
  }

  /* --------- Fetch metadata List functionality Started --------- */

  getAllMetadata() {
    const url = GlobalSettings.GET_METADATA_LIST + '?show_count=1';
    // const url = './assets/json/getAllMetadata.json';
    this.service.getServiceData(url).then((res: any) => {
      this.metadataList = res.metadata;
      this.metadataList.forEach(data => {
        data.defined_by = data.is_custom === 0 ? 'CASE' : 'Custom';
        data.isEditable = this.editMetadata;
        // data.field_type_id = 7 means 'Dictionary' type
        data.isDelete = (data.is_custom === 0 || data.field_type_id === 7) ? false : this.delMetadata;
        data.isActive = (data.field_type_id === 7) ? '' : (data.is_active === 1 ? 'Active' : 'Inactive');
        data.caseField = Utils.caseField[data.internal_name] ? Utils.caseField[data.internal_name] : '';
        data.showOptions = (!data.isEditable && !data.isDelete) ? false : true;
      });
      if (this.metadataList.length > 0) {
        this.searchText = false;
      } else {
        this.searchText = true;
      }
      this.userRoleRight();
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  /* --------- Fetch metadata List functionality Ended --------- */

  /* ---------- Create Metadata Functionality Started ----------- */
  onSubmit() {
    const url = GlobalSettings.GET_METADATA_LIST;
    if (this.metadata.metadata_id === undefined) {

    } else {
      if (this.listType === true && this.editCustom) {
        this.updatedData = {
          name: this.metadata.name,
          field_type: this.metadata.field_type_id,
          field_possible_values: (this.metadata.field_type_id === 7) ? this.tempFieldList : this.customList,
        };
      } else {
        this.updatedData = {
          name: this.metadata.name,
          field_type: this.metadata.field_type_id,
          field_possible_values: this.listValueName,
        };
      }
      // console.log('updatedData', JSON.stringify(updatedData));
      // tslint:disable-next-line:max-line-length
      this.dialogService.confirm('Confirm', 'Change in the metadata value would affect all existing draft and published taxonomies using this metadata field')
        .then((confirmed) => {
          if (confirmed) {
            this.updateLoading = true;
            this.service.putService(url + '/' + this.metadata.metadata_id, this.updatedData).then((res: any) => {
              // this.getAllMetadata();
              this.sharedService.sucessEvent.next({
                type: 'update_metadata'
              });
              this.updateMetadataTable(this.metadata.metadata_id, 'name', this.metadata.name);
              this.updateLoading = false;
              this.selectName = '';
              this.selectCode = '';
              this.language_id = '';
              this.onCancel();
              setTimeout(() => {
                if (document.getElementById('cancel-modal-btn-edit-metadata')) {
                  document.getElementById('cancel-modal-btn-edit-metadata').click();
                }
              }, 300);
            }).catch(ex => {
              if (ex.status === 400) {
                this.sharedService.sucessEvent.next({
                  customMsg: ex.msg ? ex.msg : 'This metadata name with field type already exists.',
                  type: 'error'
                });
              }
              this.updateLoading = false;
              console.log('Error while updating metdata', ex);
            });
          } else {
            console.log('User cancel the dialog while confirming');
          }
        })
        .catch(() => {
          console.log('Exception occured while confirming the dialog while confirming');
        });
    }
  }

  /* ---------- Create Metadata Functionality Ended ----------- */

  getMetadataValue(value, short_code) {
    const temObj = {
      language_id: '',
      name: value,
      short_code: short_code
    };
    if (this.editAddedListItem) {
      this.editObj['name'] = this.selectName;
      this.editObj['short_code'] = this.selectCode;
    } else {
      this.listValueName.push(temObj);
    }
    this.selectName = '';
    this.selectCode = '';
    this.form.controls.languageName.reset();
    this.form.controls.shortCode.reset();
    this.language_id = '';
    this.valid = true;
    this.editAddedListItem = false;
  }

  /* --------- Change metadata status functionality Started --------- */

  changeStatus(status, metadata_id) {
    const url = GlobalSettings.CHANGE_STATUS + '/' + metadata_id;
    const tempObj = {
      is_active: status,
    };
    this.service.putService(url, tempObj).then((res: any) => {
      this.getAllMetadata();
      this.sharedService.sucessEvent.next({
        type: 'active_status'
      });
    }).catch(ex => {
      console.log('error caught in update status for metadata', ex);
    });
  }
  /* --------- Change metadata status functionality Ended --------- */

  /* --------- Get metadata functionality Started --------- */
  getMetadata(metadata_id) {
    this.listValueName = [];
    const url = GlobalSettings.GET_METADATA_LIST + '/' + metadata_id;
    this.metaFieldValLoading = true;
    this.service.getServiceData(url).then((res: any) => {
      const tempList = [];
      let data: any;
      this.tempFieldList = [];
      if (res.field_type_id === 7) { // 'Dictionary' type metadata
        this.tempFieldList = res.field_possible_values;
        for (const obj of res.field_possible_values) {
          tempList.push(obj.value);
        }
        this.listValueName = JSON.parse(JSON.stringify(tempList));
        data = tempList;
        this.initialListData = tempList;
      } else { // Other metedata except 'Dictionary' type
        this.listValueName = JSON.parse(JSON.stringify(res.field_possible_values));
        data = res.field_possible_values;
        this.initialListData = res.field_possible_values;
      }
      this.customList = data.toString().split(',');
      this.customListOriginal = data.toString().split(',');
      this.initialCustomListData = data.toString().split(',');
      this.metaFieldValLoading = false;
    }).catch((ex) => {
      console.log('getMetadata exception', ex);
    });
  }
  /* --------- Get metadata functionality Ended --------- */

  // / On click Menu option

  onOptionClicked(event) {
    this.caseField = null;
    switch (event.clickedOn) {
      case 'editMetadata':
        if (event.data['internal_name']) {
          this.caseField = this.getCaseField(event.data['internal_name']);
        }
        this.onEditMetadata(event.data);
        break;
      case 'delMetadata':
        if (event.data.is_custom === 1) {
          this.deleteMetadata(event.data);
        }
        break;
      default:
    }
  }

  /* Delete functionality Started */

  deleteMetadataValue(index) {

    this.listValueName.splice(index, 1);
    if (JSON.stringify(this.initialListData) === JSON.stringify(this.listValueName)) {
      this.valid = false;
    } else {
      this.valid = true;
    }
  }
  /* --------- Delete function Ended --------- */

  /* ------------ Edit Metadata Functionality Started ----------*/
  onEditMetadata(data) {
    this.curentItem = data;
    this.valid = false;
    this.form.controls.languageName.reset();
    this.form.controls.shortCode.reset();
    // data.field_type_id=7 means, 'Dictionary' type which will be list type values
    if (data.type === 'LIST' || data.field_type_id === 7) {
      this.disableAddingListItem = true;
      this.listType = true;
      if (data.is_custom === 1) {
        this.editCustom = true;
      } else {
        this.editCustom = false;
      }
    } else {
      this.listType = false;
    }
    if (data !== undefined) {
      this.metadataName = data.name;
      const newData = JSON.stringify(data);
      this.metadata = JSON.parse(newData);
      this.getMetadata(this.metadata.metadata_id);
    } else {
      this.metadata = <Metadata>{};
    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'edit_metadata'
    });
  }
  /* ------------ Edit Metadata Functionality Ended ----------*/

  /* Tab Selection functionality Started */

  onTabSelected(tab) {
    console.log('onTabSelected==>', tab);
    this.currentTab = tab;
    if (tab === 'metadata') {
      this.metaDataTab = true;
      this.metadatasetTab = false;
      this.getAllMetadata();
    } else if (tab === 'metadataset') {
      this.metadatasetTab = true;
      this.metaDataTab = false;

    }
    setTimeout(() => {
      this.currentTab = tab;
      // this.router.navigate(['./app/', tab]); // navigation will commence as per tab title, default is metadata
    }, 10);
  }

  /* Tab Selection functionality Ended */

  /* Search functionality Started */
  onSearchTextChange(txt) {
    // if (txt !== undefined && txt !== '' && txt.length > 2) {
    //   const url = GlobalSettings.METADATA_SEARCH + txt;
    //   this.service.getServiceData(url).then((res: any) => {
    //     this.metadataList = res.metadata;
    //   }).catch(ex => {
    //     console.log('list of taxonomies ex ', ex);
    //   });
    // } else {
    //   this.getAllMetadata();
    // }
    if (this.metadataList) {

      if (txt === undefined || txt === '' || txt.length < 3) {
        this.searchedMeta = this.metadataList;
      } else {
        this.searchedMeta = this.metadataList.filter(obj => {
          if ((obj.name.toLowerCase()).includes(txt.toLowerCase())) {
            return obj;
          }
        });
      }
    }
  }
  /* Search functionality Ended */



  /* Check Permission functionality Started */

  userRoleRight() {

    if (this.AuthenticateUser.AuthenticateMetadata('Create Metadata')) {
      this.createMetadataBtn = true;
    } else {
      this.createMetadataBtn = false;
    }
    if (this.AuthenticateUser.AuthenticateMetadata('Edit Metadata')) {
      this.editMetadata = true;
    } else {
      this.editMetadata = false;
    }
    if (this.AuthenticateUser.AuthenticateMetadata('List Metadata')) {
      this.viewMetadata = true;
    } else {
      this.viewMetadata = false;
    }
    if (this.AuthenticateUser.AuthenticateMetadata('Delete Metadata')) {
      this.delMetadata = true;
    } else {
      this.delMetadata = false;
    }
    const listMetadataSet = this.AuthenticateUser.AuthenticateMetadataSet('List NodeType');
    if (listMetadataSet) {
      this.tabItems = ['metadata', 'metadataset'];
      this.metaDataTab = true;
      this.metadatasetTab = true;
    } else {
      this.tabItems = ['metadata'];
      this.metaDataTab = true;
      this.metadatasetTab = false;
      this.currentTab = 'metadata';
    }
    if (!this.viewMetadata) {
      if (listMetadataSet) {
        this.tabItems = ['metadataset'];
        this.metaDataTab = false;
        this.metadatasetTab = true;
        this.currentTab = 'metadataset';
      }
    }
    this.tabItems.push('association presets');

    if (this.AuthenticateUser.AuthenticateMetadata('View list of association configuration')) {
      this.viewMetaConfig = true;
    } else {
      this.viewMetaConfig = false;
      this.removeTab('association presets');
    }

    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit Metadata',
      type: 'editMetadata',
      value: this.editMetadata,
      modal: '#edit-metadata',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'delMetadata',
      value: this.delMetadata,
      modal: '',
      check: 'isDelete'
    }
    ];
  }

  removeTab(tabName: any) {
    if (this.tabItems && this.tabItems.length > 0) {
      this.tabItems = this.tabItems.filter((item: any) => {
        return (item !== tabName);
      });
    }
  }

  /* Check Permission functionality Ended */

  /* List all metadata list on close of alert start */

  // onClose() {
  //   this.getAllMetadata();
  // }

  /* List all metadata list on close of alert end */

  onCancel() {
    this.language_id = '';
    this.selectName = '';
    this.selectCode = '';
    // this.getAllMetadata();
    this.customList = [''];
    this.customListOriginal = [''];
    if (document.getElementById('0')) {
      document.getElementById('0').innerHTML = '';
    }
    this.createMetadataForm.reset();
    this.form.reset();
    this.customMetadataListItem = '';
    this.listTypeSelected = false;
    this.valid = false;
    this.tempFieldList = [];
    this.sharedService.faqEvent.next({
      name: 'metadata_list'
    });
    this.disableAddingListItem = true;
  }

  editList(data) {
    this.selectName = data.name;
    this.selectCode = data.short_code;
    this.language_id = data.language_id;
    this.editAddedListItem = true;
    this.editObj = data;
  }

  checkValidation(e) {
    if (this.listType === true && !this.editCustom) {
      if (JSON.stringify(this.initialListData) === JSON.stringify(this.listValueName)) {
        this.valid = false;
      } else {
        this.valid = true;
      }
    } else if (this.listType === true && this.editCustom) {
      if (JSON.stringify(this.initialCustomListData) === JSON.stringify(this.customList)) {
        this.valid = false;
      } else {
        this.valid = true;
      }
    }

  }

  onModelChange(e) {
    if (this.metadataName === e) {
      this.valid = false;
    } else {
      this.valid = true;
    }
  }

  selectedOption(value) {
    this.inputTypeValue = value;
    if (value === '3') {
      this.listTypeSelected = true;
      this.validateCreate = false;
      this.disableButton = true;
    } else {
      this.listTypeSelected = false;
      this.validateCreate = true;
      this.disableButton = false;
    }
  }
  createMetadata() {
    this.disableButton = true;
    const name = this.createMetadataForm.value.customMetadataName;
    const field_type = this.createMetadataForm.value.customMetadataOptions;
    this.createMetadataObj = {
      'name': name,
      'field_type': field_type,
      'field_possible_values': ''
    };
    if (this.listTypeSelected) {
      this.createMetadataObj.field_possible_values = this.customList.splice(0, this.customList.length - 1);
    }
    const url = GlobalSettings.CREATE_CUSTOM_METADATA;
    this.service.postService(url, this.createMetadataObj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'create_metadata'
      });
      this.disableButton = false;
      this.onCancel();
      this.getAllMetadata();
      setTimeout(() => {
        if (document.getElementById('closecreate-metadata')) {
          document.getElementById('closecreate-metadata').click();
        }
      }, 300);
    }).catch((ex) => {
      this.disableButton = false;
      if (ex.status === 400) {
        this.sharedService.sucessEvent.next({
          customMsg: ex.msg ? ex.msg : 'This metadata name with field type already exists.',
          type: 'error'
        });
      }
      console.log('createMetadata error occured', ex);
    });
  }


  addCustomMetaList(evt, index) {
    this.customMetadataListItem = '';
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    if (this.customList[index] === '') {
      if (elt.innerText.length > 0) {
        this.customList[index] = elt.innerText;
        this.customList[index + 1] = '';
        document.getElementById(index).innerHTML = '';
      }
    } else {
      this.customList[index] = elt.innerText;
    }
    if (this.customList.length > 0 && this.customList[0].length > 0) {
      this.validateCreate = true;
      this.disableButton = false;
    } else {
      this.validateCreate = false;
      this.disableButton = true;
    }
  }

  editCustomMetaList(data: any, fieldTypeId: any) {
    const lowerCaseNames = this.customList.map(value => {
      return value.toLowerCase();
    });
    if (data && (lowerCaseNames.indexOf(data.toLowerCase()) < 0)) {
      if (this.editAddedCustomListItem) {
        this.customList[this.editCustomMetadataListIndex] = data;
        if (fieldTypeId === 7 && this.tempFieldList[this.editCustomMetadataListIndex]) {
          this.tempFieldList[this.editCustomMetadataListIndex].value = data;
        }
      } else {
        if (this.metadata.name !== 'Source Type') {
          this.customList.push(data);
        }

        if (fieldTypeId === 7) {
          const len = this.tempFieldList.length;
          if (this.tempFieldList[len - 1]) {
            this.tempFieldList.push({ key: (this.tempFieldList[len - 1].key + 1), value: data });
          }
        }
      }
      this.customMetadataListItem = '';
      this.editAddedCustomListItem = false;
    }
    this.disableAddingListItem = true;
  }

  editCustomMetadataList(data, index) {
    this.disableAddingListItem = false;
    this.customMetadataListItem = data;
    this.editAddedCustomListItem = true;
    this.editCustomMetadataListIndex = index;
  }

  deleteCustomMetadataList(index, item) {

    const data = this.curentItem;
    if (index > (this.customListOriginal.length - 1)) {
      // If deleting element is not saved in database i.e. newly added item, don't call API for them
      this.deleteCustomFromArray(index);
    } else {
      const itemValue = (data.field_type_id === 7 && this.tempFieldList[index]) ? this.tempFieldList[index].key : item;
      const url = GlobalSettings.CHECK_META_DATA_USAGE + '/' + data.metadata_id + '/' + itemValue;
      this.service.getServiceData(url).then((res: any) => {
        if (res > 0 && (data.field_type_id === 3 || data.field_type_id === 7)) {
          this.sharedService.sucessEvent.next({
            type: 'metadata_usage'
          });
        } else {
          // console.log(this.customList, '---deleteCustomMetadataList----', this.initialCustomListData);
          this.deleteCustomFromArray(index);
        }
      }).catch((ex) => {
        console.log('Error caught while fetching metadata list', ex);
        if (this.customList.length && data.field_type_id !== 3) {
          this.deleteCustomFromArray(index);
        }
      });
    }
  }

  deleteCustomFromArray(index: number) {
    this.customList.splice(index, 1);
    this.customListOriginal.splice(index, 1);
    if (this.tempFieldList.length) {
      this.tempFieldList.splice(index, 1);
    }
    this.checkValidation(index);
  }

  onKeyPress(evt) {
    const key = evt.which || evt.keyCode || evt.charCode;
    if (key === 13) {
      evt.preventDefault();
    }
  }

  checkValidationCustMetadata(data, index) {
    if (data.target.innerHTML === '' && this.customList.length - 1 !== index) {
      this.validateCreate = false;
    }
  }

  deleteCustomMetaList(index) {
    if (this.customList[index].length > 0) {
      this.dialogService.confirm('Confirm', 'Are you sure you want to delete the List item?')
        .then((confirmed) => {
          if (confirmed) {
            this.customList.splice(index, 1);
            if (this.customList[0] === '') {
              this.validateCreate = false;
            }
          } else {
            console.log('User cancel the dialog while confirming');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog while confirming');
        });
    }
  }




  /* --------- Delete Metadata from the list functionality start --------- */

  deleteMetadata(data) {
    // tslint:disable-next-line:max-line-length
    // console.log(JSON.stringify(data));
    // tslint:disable-next-line:max-line-length
    this.dialogService.confirm('Confirm', 'Change in the metadata value would affect all existing draft taxonomies using this metadata field. Are you sure you want to delete ' + data['name'] + ' metadata?')
      .then((confirmed) => {
        if (confirmed) {
          this.metadataList = this.metadataList.filter(items => items.metadata_id !== data.metadata_id);
          const url = GlobalSettings.GET_METADATA_LIST + '/' + data['metadata_id'];
          this.service.deleteServiceData(url).then((res: any) => {
            this.deleteMetadataFromTable(data.metadata_id);
            this.sharedService.sucessEvent.next({
              type: 'delete_metadata'
            });
            // this.getAllMetadata();
          }).catch((ex) => {
            console.log('Error while deleting the metadata', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }
  /* --------- Delete Metadata from the list functionality end --------- */


  /* --------- On excape key press function start --------- */
  mouseLeave(event) {
    if (event && event.keyCode === 27) {
      this.onCancel();
    } else {
      const self = this;
      const modal = document.getElementById('create-metadata');
      window.onclick = function (evt) {
        if (evt.target === modal) {
          self.onCancel();
        }
      };
    }
  }
  /* --------- On excape key press function end --------- */

  onOpenMetadataCreateModal() {
    // clear data
    this.customList = [''];
    this.editCustom = false;
    this.customMetadataListItem = '';
    this.listValueName = [];
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'create_metadata'
    });
  }

  /* --------- Functionality for copying value to clipboard start --------- */

  copyCaseFieldValue() {
    // window.clipboardData.setData('Text', text);
    // const caseField: HTMLInputElement = document.getElementById('caseField') as HTMLInputElement;
    // document.addEventListener('copy', listener, false)
    // document.execCommand('copy');
    // document.removeEventListener('copy', listener, false);
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (this.caseField));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }

  /* --------- Functionality for copying value to clipboard end --------- */


  /* --------- Functionality for fetching Case Field name from Internal name start --------- */

  getCaseField(internal_name) {
    return Utils.caseField[internal_name];
  }

  /* --------- Functionality for fetching Case Field name from Internal name end --------- */

  /**
   * To update metadata list table with updated value of any specific property
   * @param id (unique id)
   * @param updateProp (which column proprty value needs to be updated)
   * @param updateValue (updated value)
   */
  updateMetadataTable(id: any, updateProp: any, updateValue: any) {
    Utils.updateArray(id, this.metadataList, 'metadata_id', updateProp, updateValue);
  }

  /**
   * To remove metadata from table list
   * @param id (metadata id)
   */
  deleteMetadataFromTable(id: any) {
    if (this.metadataList && this.metadataList.length) {
      this.metadataList = this.metadataList.filter(metadata => {
        return (metadata.metadata_id !== id);
      });
    }
  }
}
