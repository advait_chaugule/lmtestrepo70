import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  CommonService
} from '../../../common.service';
import {
  AutoCompleteComponent
} from '../../../common/auto-complete/auto-complete.component';
import {
  Router
} from '@angular/router';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';
import Utils from '../../../utils';
import { ConfirmationDialogService } from '../../../confirmation-dialog/confirmation-dialog.service';
@Component({
  selector: 'app-create-workflow',
  templateUrl: './create-workflow.component.html',
  styleUrls: ['./create-workflow.component.scss', './../workflow/workflow-step.scss', './overide.scss']
})
export class CreateWorkflowComponent implements OnInit {

  form: FormGroup; // form instance
  workflow_id; // holds current workflow id
  @Input() selectedRoles = []; // holds all selected role object
  @Input() workflowStepName; // holds workflow step name
  @Input() workflowDesc; // holds workflow step desc
  @Input() workflow_stage_id; // holds workflow stage id used while editing stage
  selectType = 'multi'; // holds the dropdown selection criteria as in multi or single
  workflowName; // holds workflow name
  uniqueId = 'role_id'; // holds the unique id for the auto complete items
  autoCompleteTitle = 'Roles'; // title to be sent for auto complete dropdown
  workflowStageListing = []; // holds the list of all stages
  formValidate = false; // validation flag for done button

  @Output() createWorkflowStatus: EventEmitter<any>; // emits the stage creation status if true or false to toggle between list and create
  @Output() setWorkflowId: EventEmitter<any>; // emits workflow id once the 1st step is created in workflow
  @Output() workflowStageList: EventEmitter<any>; // emits the workflowStageListing object which holds all stages
  @Input() roles; // holds all roles to populate in dropdown for auto complete
  @Input() createNewWorkflow; // check if its the first step while workflow is created
  @Input() workflowIdInput; // check if its the first step while workflow is created
  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;
  @Input() stages; // collect the stages added in the workflow
  @Output() editWorkflowStatus: EventEmitter<any>; // emits the workflowStageListing object which holds all stages
  @Output() showStagesStatus: EventEmitter<any>; // emits the whether showStagesStatus(milestone) is needed to display
  @Output() updateProtipStatus: EventEmitter<any>; // emits the protip status to hide or display
  @Input() editStage; // holds boolean if clicked on stage to edit
  prevStageValues; // holds previous stage values before editing, used to compare old vs new value for button validation
  constructor(
    private service: CommonService,
    private router: Router,
    private fb: FormBuilder,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService) {
    this.createWorkflowStatus = new EventEmitter<any>();
    this.setWorkflowId = new EventEmitter<any>();
    this.workflowStageList = new EventEmitter<any>();
    this.editWorkflowStatus = new EventEmitter<any>();
    this.showStagesStatus = new EventEmitter<any>();
    this.updateProtipStatus = new EventEmitter<any>();
  }

  ngOnInit() {
    this.form = this.fb.group({
      workflowName: [null],
      workflowStepName: [null, Validators.compose([Validators.required])],
      workflowDesc: [null]
    });

    this.workflow_id = this.workflowIdInput;
    this.initialRoles();
    this.workflowStageListing = this.stages;
    if (this.createNewWorkflow) {
      this.sharedService.faqEvent.next({
        name: 'create_workflow'
      });
    }
  }

  /* --------- Functionality to initialise all role to not selected state start --------- */

  initialRoles() {
    if (this.roles) {
      this.roles.forEach(element => {
        element.is_selected = 0;
      });
    }
  }

  /* --------- Functionality to initialise all role to not selected state end --------- */


  /* --------- Functionality to identify the target element and toggle state of autocomplete dropdown start --------- */
  toggleOpen(val) {
    if (val.id) {
      if (val.id === 'workflowRole') {
        this.autoComplete.openPanel(true);
      } else {
        this.autoComplete.openPanel(false);
      }
    } else {
      this.autoComplete.openPanel(false);
    }
  }
  /* --------- Functionality to identify the target element and toggle state of autocomplete dropdown end --------- */


  /* --------- Functionality to update the selected object from autocomplete start --------- */

  updateSelectedObject(selectedRole) {
    // this.selectedRoles = [];
    selectedRole.forEach(element => {
      this.selectedRoles.push({ 'role_id': element.role_id, 'name': element.name });
    });
    this.checkValidation();
    this.addRole(selectedRole);
  }

  /* --------- Functionality to update the selected object from autocomplete end --------- */


  /* --------- Functionality to detect is clicked outside autocomplete component start --------- */

  onClickedOutside(e: Event) {
    this.autoComplete.openPanel(false);
  }

  /* --------- Functionality to detect is clicked outside autocomplete component end --------- */


  /* --------- Functionality to create workflow stage start --------- */

  createWorkflowStage() {
    const url = GlobalSettings.CREATE_WORKFLOW_STAGE + '/' + this.workflow_id;
    const obj = {
      'workflow_stages_role': this.selectedRoles,
      'stage_order': this.workflowStageListing.length + 1,
      'stage_name': this.workflowStepName.trim(),
      'stage_description': this.workflowDesc ? this.workflowDesc : ''
    };
    this.service.postService(url, obj).then((res: any) => {
      res['roles'] = this.selectedRoles;
      this.workflowStageListing.push(res);
      this.createWorkflowStatus.emit(false);
      this.editWorkflowStatus.emit(true);
      this.showStagesStatus.emit(true);
      this.updateProtipStatus.emit(false);
      this.workflowStageList.emit(this.workflowStageListing);
      this.sharedService.sucessEvent.next({
        type: 'workflow_step_added'
      });
    }).catch(ex => {
      if (ex.msg === 'Stage name already exists in current workflow.') {
        this.sharedService.sucessEvent.next({
          type: 'workflow_step_exist'
        });
      } else {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg
        });
      }
      console.log('create nodetype', ex);
    });
  }

  /* --------- Functionality to create workflow stage end --------- */


  /* --------- Functionality to create new workflow start --------- */

  createWorkflow() {
    if (this.createNewWorkflow) {
      const url = GlobalSettings.CREATE_WORKFLOW;
      const obj = {
        'name': this.workflowName
      };
      this.service.postService(url, obj).then((res: any) => {
        this.workflow_id = res.workflow_id;
        this.setWorkflowId.emit(this.workflow_id);
        this.createWorkflowStage();
      }).catch(ex => {
        if (ex.msg === 'Workflow name already exists.') {
          this.sharedService.sucessEvent.next({
            type: 'workflow_exist'
          });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: ex.msg
          });
        }
        console.log('create nodetype', ex);
      });
    } else {
      this.createWorkflowStage();
    }
    this.sharedService.faqEvent.next({
      name: 'workflow_list'
    });
  }


  /* --------- Functionality to create new workflow end --------- */


  /* --------- Functionality on click of cancle button start --------- */

  cancle() {
    if (this.createNewWorkflow) {
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_WORKFLOW_LIST;
      this.router.navigate([path]);
      this.sharedService.faqEvent.next({
        name: 'workflow_list'
      });
    } else {
      this.editWorkflowStatus.emit(true);
      this.sharedService.faqEvent.next({
        name: 'edit_stage_workflow'
      });
    }
    this.createWorkflowStatus.emit(false);
  }

  /* --------- Functionality on click of cancle button end --------- */


  /* --------- Functionality on check validation start --------- */

  checkValidation() {
    if (this.editStage) {
      if (this.form.valid && this.prevStageValues) {
        if (this.prevStageValues.stage_name === this.workflowStepName && this.prevStageValues.stage_description === this.workflowDesc) {
          this.formValidate = false;
        } else {
          // this.formValidate = true;
          if (this.workflowStepName.trim().length > 0) {
            this.formValidate = true;
          }
        }
      } else {
        this.formValidate = false;
      }
    } else {
      if (this.form.valid) {
        if (this.createNewWorkflow) {
          if (this.workflowName.trim().length > 0 && this.workflowStepName.trim().length > 0) {
            this.formValidate = true;
          } else {
            this.formValidate = false;
          }
        } else {
          if (this.workflowStepName.trim().length > 0) {
            this.formValidate = true;
          }
        }
      } else {
        this.formValidate = false;
      }
    }

  }

  /* --------- Functionality on check validation end --------- */


  /* --------- Functionality on click of delete icon from selection list start --------- */

  deleteRole(data) {
    if (this.workflow_stage_id) { // If workflow exists, then only call delete api
      this.deleteRoleService(data);
    } else { // otherwise remove only from UI
      this.removeRoleFromList(data);
    }
  }

  // This remove is only for UI purpose removing
  removeRoleFromList(data: any) {
    this.selectedRoles = this.selectedRoles.filter(element => {
      return element[this.uniqueId] !== data[this.uniqueId];
    });
    this.roles.forEach(element => {
      if (element[this.uniqueId] === data[this.uniqueId]) {
        element.is_selected = 0;
      }
    });
    this.autoComplete.unselectObjInList(data);
  }

  /* --------- Functionality on click of delete icon from selection list end --------- */

  setEditWorkflowStageValues() {
    this.selectedRoles.forEach(element => {
      for (const i in this.roles) {
        if (i) {
          if (this.roles[i].role_id === element.role_id) {
            this.roles[i].is_selected = 1;
            break;
          }
        }
      }
    });
    this.prevStageValues = JSON.parse(JSON.stringify({
      workflow_stages_role: this.selectedRoles,
      stage_order: this.workflowStageListing.length + 1,
      stage_name: this.workflowStepName.trim(),
      stage_description: this.workflowDesc ? this.workflowDesc : ''
    }));
  }



  /* --------- Functionality on click of done button while editing start --------- */


  onUpdate() {
    if (this.prevStageValues.stage_name === this.workflowStepName) {
      this.updateService();
    } else {
      // tslint:disable-next-line:max-line-length
      this.dialogService.confirm('Confirm', 'Editing the stage name will make the changes to all projects that utilize this workflow. Are you sure you want to edit the stage name?')
        .then((confirmed) => {
          if (confirmed) {
            this.updateService();
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    }

  }


  updateService() {
    const tempObj = {
      workflow_stage_id: this.workflow_stage_id,
      stage_name: this.workflowStepName.trim(),
      stage_description: this.workflowDesc ? this.workflowDesc : ''
    };
    const url = GlobalSettings.UPDATE_WORKFLOW_STAGES;
    this.service.postService(url + '' + this.workflow_stage_id, tempObj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_stage'
      });
      this.createWorkflowStatus.emit(false);
      this.editWorkflowStatus.emit(true);
      this.showStagesStatus.emit(true);
    }).catch(ex => {
      console.log('create role', ex);
    });
  }


  /* --------- Functionality on click of done button while editing end --------- */


  /* --------- Functionality to add role while editing stage start --------- */

  addRole(data) {
    let counter = 0;
    data.forEach(element => {
      if (this.editStage) {
        if (data && data !== 'null') {
          const url = GlobalSettings.UPDATE_WORKFLOW_STAGES;
          const tempObj = {
            workflow_stage_id: this.workflow_stage_id,
            stage_name: this.workflowStepName.trim(),
            role_id: element.role_id,
            stage_description: this.workflowDesc ? this.workflowDesc : ''
          };
          counter = counter + 1;
          this.service.postService(url + '' + this.workflow_stage_id, tempObj).then((res: any) => {
          }).catch(ex => {
            console.log('create workflow', 'ex===>>', ex.msg);
            if (ex.msg === 'Role mapping exists') {
              this.sharedService.sucessEvent.next({
                type: 'role_mapping_exists'
              });
            }
          });
        }
      }
    });
    if (counter > 0) {
      this.sharedService.sucessEvent.next({
        type: 'add_workflow_stage_role'
      });
    }
  }

  /* --------- Functionality to add role while editing stage end --------- */


  /* --------- Functionality to delete role while editing stage start --------- */

  deleteRoleService(data) {
    const url = GlobalSettings.DELETE_WORKFLOW_STAGES_ROLE + '/' + this.workflow_stage_id + '/' + data.role_id;
    this.service.deleteServiceData(url).then((res: any) => {
      this.removeRoleFromList(data);
      this.sharedService.sucessEvent.next({
        type: 'delete_workflow_stage_role'
      });
    }).catch((ex) => {
      if (ex.status === 400) {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg ? ex.msg : ex.error.message
        });
      }
    });

  }

  /* --------- Functionality to delete role while editing stage end --------- */

}
