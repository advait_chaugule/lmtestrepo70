import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  OnDestroy,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  CommonService
} from '../../../common.service';
import {
  SharedService
} from '../../../shared.service';
import {
  ActivatedRoute,
  Params
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  CreateWorkflowComponent
} from '../create-workflow/create-workflow.component';
import {
  Router
} from '@angular/router';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import Utils from '../../../utils';
import {
  Subscription
} from 'rxjs/Subscription';
@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss', './workflow-timeline.scss', './workflow-step.scss']
})

export class WorkflowComponent implements OnInit, OnDestroy {
  workFlowList: any[] = [];
  editing = {};
  roleList: any[] = [];
  roleData: any[] = [];
  tempRoleData: any[] = [];
  stage_name: string;
  workflow_stage_id: string;
  roleId: any[] = [];
  viewWorkFlow: boolean;
  editWorkflow = false;
  workflow_id: string;
  index: string;
  form: FormGroup;
  listWorkflowStages: boolean;
  editWorkflowStages: boolean;
  deleteWorkflowStages: boolean;
  selectedRole = null;
  formValidation = false;
  workflow_desciption = '';
  buttonTitle = 'Update';
  createWorkflowFlag = true; // check if its the first step while workflow is created
  createNewWorkflow = true; // check if its the first step while workflow is created 
  roles = []; // holds all roles to populate in dropdown for auto complete
  workflowStageList = [];
  prev_obj;
  type = 'workflow';
  showProtip = false;
  showStages = false;
  editStage = false; // holds boolean if stage us edited
  searchTextEvent: Subscription; // holds subscription for search
  searchEnabled = false; // holds boolean to determine if search is enabled
  workflowStageListSearchResult = []; // holds search result object to list
  searchTriggered = false; // holds boolean if search panel is expanded to apply css
  @ViewChild('editworkFlowBtn', { static: false }) editworkFlow;
  @ViewChild('createWorkflow', { static: false }) createWorkflow: CreateWorkflowComponent;
  faqTitle = 'workflow_list';
  constructor(private AuthenticateUser: AuthenticateUserService,
    private activatedRoute: ActivatedRoute,
    private service: CommonService,
    private sharedService: SharedService,
    private router: Router,
    private dialogService: ConfirmationDialogService) {

    this.searchTextEvent = this.sharedService.searchTextEvent.subscribe((res: any) => {
      if (res.text && res.text.length) {
        this.searchEnabled = true;
        this.filterStage(res.text);
      } else {
        this.searchEnabled = false;
      }
      if (res.trigger) {
        this.searchTriggered = true;
      } else {
        this.searchTriggered = false;
      }
    });

  }

  /* --------- Functionality to filter workflow stages from list on search start --------- */

  filterStage(searchText) {
    this.workflowStageListSearchResult = this.workflowStageList.filter(obj => {
      obj.step = 'Stage ' + obj.order;
      // tslint:disable-next-line:max-line-length
      if (obj.stage_description.toLowerCase().includes(searchText.toLowerCase()) || obj.stage_name.toLowerCase().includes(searchText.toLowerCase())
        || this.getRolesName(obj.roles).toLowerCase().includes(searchText.toLowerCase()) || JSON.stringify(obj.step).toLowerCase().includes(searchText.toLowerCase())) {
        return obj;
      }
    });
  }

  /* --------- Functionality to filter workflow stages from list on search end --------- */

  ngOnInit() {
    this.getRoles();
    this.getAllRoles();
    this.form = new FormGroup({
      workflow_role: new FormControl(),
      workflow_desc: new FormControl(),
      workflow_stage: new FormControl(),
    });

    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['flag'] === 'edit') {
        this.viewWorkFlow = false;
        this.createWorkflowFlag = false;
        this.editWorkflow = true;
        this.workflow_id = params['id'];
        this.getWorkFlowList();
      } else if (params['flag'] === 'view') {
        this.viewWorkFlow = true;
        this.editWorkflow = false;
        this.createWorkflowFlag = false;
        this.workflow_id = params['id'];
        this.getWorkFlowList();
      } else {
        this.createWorkflowFlag = true;
        this.editWorkflow = false;
        this.viewWorkFlow = false;
      }
    });
    this.sharedService.faqEvent.next({
      name: 'edit_stage_workflow'
    });
  }


  getWorkFlowList() {
    const url = GlobalSettings.GET_WORKFLOW + '/' + this.workflow_id;
    this.service.getServiceData(url).then((res: any) => {
      this.workflowStageList = res.workflow.stages;
      if (this.workflowStageList && this.workflowStageList.length === 0) {
        this.showProtip = true;
      } else {
        this.showStages = true;
        this.showProtip = false;
      }
    }).catch((ex) => {

    });
  }

  updateValue(event, cell, rowIndex, data) {
    this.editing[rowIndex + '-' + cell] = false;
    this.workFlowList[rowIndex][cell] = event.target.value;

    const url = GlobalSettings.UPDATE_WORKFLOW_STAGES;

    const tempObj = {
      workflow_stage_id: data.workflow_stage_id,
      stage_name: this.workFlowList[rowIndex][cell],
      role_id: null,
      stage_description: this.workflow_desciption ? this.workflow_desciption : ''
    };

    this.service.postService(url + '' + data.workflow_stage_id, tempObj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_stage_name'
      });
    }).catch(ex => {
      console.log('create role', ex);
    });
  }

  getRoles() {
    const url = GlobalSettings.GET_ROLE;
    this.service.getServiceData(url).then((res: any) => {
      // this.roleList = res.RoleList.roles;
      res.RoleList.roles.forEach(element => {
        if (element.is_active === 1) {
          this.roleList.push(element);
          this.onCancel();

        }
      });
    }).catch((ex) => {

    });
  }


  onCancel() {
    this.form.reset();
    // this.getWorkFlowList();
  }



  selectRole(data) {
    if (data && data !== 'null') {
      this.selectObjInList(data);
      const tempData = [];
      this.roleData.forEach(r => {
        tempData.push(r);
      });
      const url = GlobalSettings.UPDATE_WORKFLOW_STAGES;
      const tempObj = {
        workflow_stage_id: this.workflow_stage_id,
        stage_name: this.stage_name,
        role_id: data,
        stage_description: this.workflow_desciption ? this.workflow_desciption : ''
      };

      this.service.postService(url + '' + this.workflow_stage_id, tempObj).then((res: any) => {
        this.roleData = res.workflow_stage_role;
        this.sharedService.sucessEvent.next({
          type: 'add_workflow_stage_role'
        });
        this.getWorkFlowList();
      }).catch(ex => {
        console.log('create workflow', 'ex===>>', ex.msg);
        if (ex.msg === 'Role mapping exists') {
          this.sharedService.sucessEvent.next({
            type: 'role_mapping_exists'
          });
        }
      });
    }
  }

  deleteRole(index, data) {

    const url = GlobalSettings.DELETE_WORKFLOW_STAGES_ROLE + '/' + this.workflow_stage_id + '/' + data.role_id;
    this.service.deleteServiceData(url).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'delete_workflow_stage_role'
      });
      const rows = [...this.roleData];
      rows.splice(index, 1);
      this.roleData = [...rows];
      const tempRows = [...this.tempRoleData];
      tempRows.splice(index, 1);
      this.tempRoleData = [...tempRows];
      this.getWorkFlowList();
      this.unselectObjInList(data.role_id);
    }).catch((ex) => {
      this.sharedService.sucessEvent.next({
        type: 'prevent_delete_role'
      });

    });

  }






  // Used functions

  mouseLeave() {

  }

  /* --------- Functionality to check validation on update button start --------- */

  checkValidation() {
    const tempObj = {
      workflow_stage_id: this.workflow_stage_id,
      stage_name: this.stage_name,
      stage_description: this.workflow_desciption ? this.workflow_desciption : '',
    };
    if (JSON.stringify(tempObj) === JSON.stringify(this.prev_obj)) {
      this.formValidation = false;
    } else {
      if (this.stage_name && this.stage_name.trim().length > 0) {
        this.formValidation = true;
      } else {
        this.formValidation = false;
      }
    }
  }

  /* --------- Functionality to check validation on update button end --------- */


  /* --------- Functionality to update on drag start --------- */

  onNodeRearranged(evt) {
    if (this.editWorkflow) {
      const obj = {
        'workflow_stages': []
      };

      for (let index = 0; index < this.workflowStageList.length; index++) {
        const element = this.workflowStageList[index];
        const ele = {
          workflow_stage_id: element.workflow_stage_id,
          order: index + 1
        };
        obj.workflow_stages.push(ele);
      }
      this.updateWorkFlowStages(obj);
    }
    // console.log('onNodeRearranged ', this.workflowStageList);
  }

  updateWorkFlowStages(obj) {
    if (this.editWorkflow) {
      const url = GlobalSettings.GET_WORKFLOW + '/' + this.workflow_id;
      this.service.postService(url, obj).then((res) => {
        console.log('updateWorkFlowStages ', res);
      }).catch((ex) => {
        console.log('updateWorkFlowStages ex ', ex);
      });
    }
  }


  /* --------- Functionality to update on drag end --------- */


  /* --------- Functionality to on click of update button on edit modal start --------- */


  onUpdate(event, index) {
    event.stopPropagation();
    event.preventDefault();
    if (this.prev_obj.stage_name === this.stage_name) {
      this.updateService(index);
    } else {
      // tslint:disable-next-line:max-line-length
      this.dialogService.confirm('Confirm', 'Editing the stage name will make the changes to all projects that utilize this workflow. Are you sure you want to edit the stage name?')
        .then((confirmed) => {
          if (confirmed) {
            this.updateService(index);
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    }

  }


  updateService(index) {
    const tempObj = {
      workflow_stage_id: this.workflow_stage_id,
      stage_name: this.stage_name,
      stage_description: this.workflow_desciption ? this.workflow_desciption : ''
    };
    const url = GlobalSettings.UPDATE_WORKFLOW_STAGES;
    this.service.postService(url + '' + this.workflow_stage_id, tempObj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_stage'
      });
      this.onCancel();
      setTimeout(() => {
        document.getElementById('closeedit-workflow-modal').click();
      }, 500);
      this.getWorkFlowList();
    }).catch(ex => {
      console.log('create role', ex);
    });
  }

  /* --------- Functionality to on click of update button on edit modal end --------- */


  /* --------- Functionality to on click of edit icon to open modal start --------- */

  editWorkFlowPopUp(data, index) {
    setTimeout(() => {
      if (this.createWorkflow) {
        this.createWorkflow.checkValidation();
      }
      if (this.createWorkflow) {
        this.createWorkflow.setEditWorkflowStageValues();
      }
    }, 50);

    // console.log(JSON.stringify(data));
    if (data !== undefined) {
      this.editworkFlow.nativeElement.click();
      const temp = JSON.parse(JSON.stringify(data.roles));
      this.roleData = [];
      this.unselectAllInList();
      temp.forEach(element => {
        if (element !== null) {
          this.selectObjInList(element.role_id);
          this.roleData.push(element);
        }
      });
      this.stage_name = data.stage_name;
      this.workflow_stage_id = data.workflow_stage_id;
      this.index = index;
      this.workflow_desciption = data.stage_description ? data.stage_description : '';
      this.prev_obj = {
        'workflow_stage_id': this.workflow_stage_id,
        'stage_name': this.stage_name,
        'stage_description': this.workflow_desciption ? this.workflow_desciption : '',
        // 'roleData': this.roleData
      };
    }
  }

  /* --------- Functionality to on click of edit icon to open modal end --------- */


  /* --------- Functionality to display role names comma seperated start --------- */

  getRolesName(roles) {
    const names = [];
    roles.forEach(element => {
      if (element) {
        names.push(element.name);
      }
    });
    return '' + names.join(', ');
  }

  /* --------- Functionality to display role names comma seperated end --------- */


  /* --------- Functionality to populate all roles to be used in create workflow start --------- */

  getAllRoles() {
    const url = GlobalSettings.GET_ROLE;
    this.service.getServiceData(url).then((res: any) => {
      res = res['RoleList']['roles'];
      res.forEach(element => {
        if (element.is_active === 1) {
          this.roles.push(
            {
              'name': element.name,
              'role_id': element.role_id,
              'is_selected': 0
            });
        }
      });
    }).catch((ex) => {

    });
  }

  /* --------- Functionality to populate all roles to be used in create workflow end --------- */


  /* --------- Functionality to capture if workflow has been created to toggle between list and create functionality start --------- */

  createWorkflowStatus(flag) {
    this.createWorkflowFlag = flag;
    if (this.workflowStageList.length === 0) {
      this.showProtip = true;
    }
    if (this.createNewWorkflow && this.workflowStageList.length === 0) {
      this.faqTitle = 'create_workflow';
      this.sharedService.faqEvent.next({
        name: this.faqTitle
      });
    } else {
      this.faqTitle = 'edit_stage_workflow';
      this.sharedService.faqEvent.next({
        name: this.faqTitle
      });
    }
  }

  /* --------- Functionality to capture if workflow has been created to toggle between list and create functionality end --------- */


  /* --------- Functionality to capture stageStatus on add of step functionality start --------- */

  showStagesStatus(flag) {
    this.showStages = flag;
  }

  /* --------- Functionality to capture stageStatus on add of step functionality end --------- */


  /* --------- Functionality to update protip status start --------- */

  updateProtipStatus(flag) {
    this.showProtip = flag;
  }

  /* --------- Functionality to update protip status end --------- */

  editWorkflowStatus(flag) {
    this.editWorkflow = flag;
    this.editStage = false;
    this.getWorkFlowList();
  }

  /* --------- Functionality to capture and store workflow id which is sent back to create compoent start --------- */


  /* --------- Functionality to capture if workflow stage has been created and make edit mode true start --------- */

  setWorkflowId(evt) {
    this.workflow_id = evt;
  }

  /* --------- Functionality to capture if workflow stage has been created and make edit mode true end --------- */


  /* --------- Functionality to capture and store workflow id which is sent back to create compoent end --------- */


  /* --------- Functionality to capture the workflow stages from create component to populate in list start --------- */

  setWorkflowStageList(data) {
    this.workflowStageList = data;
  }

  /* --------- Functionality to capture the workflow stages from create component to populate in list end --------- */


  /* --------- Functionality to save the workflow and navigate to listing start --------- */

  saveWorkflow() {
    this.workflowStageList = [];
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_WORKFLOW_LIST;
    this.router.navigate([path]);
  }

  /* --------- Functionality to save the workflow and navigate to listing end --------- */


  /* --------- Functionality to delete workflow stage start --------- */

  deleteStage(item) {
    if (this.workflowStageList.length !== 1) {
      const url = GlobalSettings.DELETE_WORKFLOW_STAGE + '/' + item.workflow_stage_id;
      this.service.deleteServiceData(url).then((res) => {
        this.sharedService.sucessEvent.next({
          type: 'delete_workflow_stage'
        });
        this.workflowStageList = this.workflowStageList.filter(obj => {
          return JSON.stringify(obj) !== JSON.stringify(item);
        });
        if (this.workflowStageList.length === 0) {
          this.showProtip = true;
          this.showStages = false;
        }
      }).catch((ex) => {
        this.sharedService.sucessEvent.next({
          type: 'prevent_delete_stage'
        });
        console.log('delete_workflow_stage ', ex);
      });
    } else {
      this.sharedService.sucessEvent.next({
        type: 'prevent_last_delete_stage'
      });
    }
  }

  /* --------- Functionality to delete workflow stage end --------- */
  // Used functions

  unselectObjInList(objId: any) {
    if (this.roleList && this.roleList.length) {
      for (const obj of this.roleList) {
        if (obj['role_id'] === objId) {
          obj['isSelected'] = false;
          break;
        }
      }
    }
  }

  unselectAllInList() {
    if (this.roleList && this.roleList.length) {
      this.roleList.forEach(element => {
        element['isSelected'] = false;
      });
    }
  }

  selectObjInList(objId: any) {
    if (this.roleList && this.roleList.length) {
      for (const obj of this.roleList) {
        if (obj['role_id'] === objId) {
          obj['isSelected'] = true;
          break;
        }
      }
    }
  }

  /* --------- Functionality to clear pre populated values from edit step during add step start --------- */

  resetWorkflowStage() {
    this.sharedService.faqEvent.next({
      name: this.faqTitle
    });
    this.roleData = [];
    this.stage_name = '';
    this.workflow_desciption = '';
  }

  /* --------- Functionality to clear pre populated values from edit step during add step end --------- */

  ngOnDestroy() {
    this.sharedService.searchTextEvent.next({
      text: '',
      trigger: false
    });
    if (this.searchTextEvent) {
      this.searchTextEvent.unsubscribe();
    }
  }

}
