import {
  Component,
  OnInit,
  Input,
  OnChanges,
  OnDestroy
} from '@angular/core';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  SharedService
} from '../../../shared.service';

@Component({
  selector: 'app-view-workflow',
  templateUrl: './view-workflow.component.html',
  styleUrls: ['./view-workflow.component.scss', './../workflow/workflow.component.scss', './../workflow/workflow-timeline.scss', './../workflow/workflow-step.scss']
})
export class ViewWorkflowComponent implements OnInit, OnChanges, OnDestroy {

  @Input() workflowStageList = []; // holds the list of workflow stages
  noDataFound = false;
  searchEnabled = false; // holds boolean to determine if search is enabled
  workflowStageListSearchResult = []; // holds search result object to list
  searchTextEvent: Subscription; // holds subscription for search
  constructor(private sharedService: SharedService) {
    this.searchTextEvent = this.sharedService.searchTextEvent.subscribe((res: any) => {
      if (res.text && res.text.length) {
        this.searchEnabled = true;
        if (this.workflowStageList && this.workflowStageList.length) {
          this.filterStage(res.text);
        }
      } else {
        this.searchEnabled = false;
      }
    });
  }

  /* --------- Functionality to filter workflow stages from list on search start --------- */

  filterStage(searchText) {
    this.workflowStageListSearchResult = this.workflowStageList.filter(obj => {
      obj.step = 'Stage ' + obj.order;
      // tslint:disable-next-line:max-line-length
      if (obj.stage_description.toLowerCase().includes(searchText.toLowerCase()) || obj.stage_name.toLowerCase().includes(searchText.toLowerCase()) ||
        this.getRolesName(obj.roles).toLowerCase().includes(searchText.toLowerCase()) || JSON.stringify(obj.step).toLowerCase().includes(searchText.toLowerCase())) {
        return obj;
      }
    });
  }

  /* --------- Functionality to filter workflow stages from list on search end --------- */

  ngOnInit() {
    setTimeout(() => {
      this.setNoDataFound();
    }, 1500);
  }

  ngOnChanges() {
    setTimeout(() => {
      if (this.workflowStageList) {
        this.setNoDataFound();
      }
    }, 1500);
  }

  /* --------- Functionality to check if no stages are present start --------- */

  setNoDataFound() {
    if (this.workflowStageList.length === 0) {
      this.noDataFound = true;
    } else {
      this.noDataFound = false;
    }
  }

  /* --------- Functionality to check if no stages are present end --------- */


  /* --------- Functionality to display role names comma seperated start --------- */

  getRolesName(roles) {
    const names = [];
    roles.forEach(element => {
      if (element) {
        names.push(element.name);
      }
    });
    return '' + names.join(', ');
  }

  /* --------- Functionality to display role names comma seperated end --------- */

  ngOnDestroy() {
    this.sharedService.searchTextEvent.next({
      text: '',
      trigger: false
    });
    if (this.searchTextEvent) {
      this.searchTextEvent.unsubscribe();
    }
  }

}
