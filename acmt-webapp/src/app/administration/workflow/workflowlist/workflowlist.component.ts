import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl

} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../../help/walkthrough/walktrough.service';*/
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import Utils from '../../../utils';
@Component({
  selector: 'app-workflows',
  templateUrl: './workflowlist.component.html',
})
export class WorkflowListComponent implements OnInit, AfterViewInit {
  workflowList: any[] = [];
  usageCount: any[] = [];
  listWorkflow = true;
  // searchedWorflows;
  createWorkflowPer;
  viewWorkflowPer;
  deleteWorkflowPer;
  editWorkflowPer;
  duplicateWorkflowPer;
  workflowTitle = ''; // holds current workflow title
  workflow_id; // holds current workflow id
  prevTitle; // holds current title
  valid = false;
  isDupliModal = false;
  listOfColumn = [{
    name: 'Name',
    propName: 'name',
    class: '',
    type: 'text'
  },
  {
    name: 'Created By',
    propName: 'updated_by',
    class: '',
    width: '15%',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Usage',
    propName: 'usage',
    class: '',
    width: '10%',
    type: 'text'
  }
  ];
  optionList = [];
  form: FormGroup;
  wfLoader = false; // holds loading state for fetching workflow list

  constructor(
    private authenticateUser: AuthenticateUserService,
    private service: CommonService,
    private router: Router,
    private fb: FormBuilder,
    private sharedService: SharedService,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService,*/
    private dialogService: ConfirmationDialogService) {
    /* Search Event Subscription Started */
    // this.sharedService.searchEvent.subscribe((event: any) => {
    //   console.log('search by', event);
    //   if (event.type && event.type === 'Workflows') {
    //     this.onSearch(event.text);
    //   }
    // });
    /* Search Event Subscription Ended */
  }

  ngOnInit() {
    this.form = new FormGroup({
      workflow_title: new FormControl(null, [Validators.required])
    });
    this.userRoleRight();
    this.getWorkFlowDetails();
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('workflow_list'));*/
    this.sharedService.faqEvent.next({
      name: 'workflow_list'
    });
  }

  ngAfterViewInit() {
    // init_rippleEffect();
  }

  getWorkFlowDetails() {
    const url = GlobalSettings.GET_WORKFLOW;
    this.wfLoader = true;
    this.service.getServiceData(url).then((res: any) => {
      // Re-checking permissions
      this.userRoleRight();
      this.workflowList = res.WorkflowList.workflows;
      // this.searchedWorflows = this.workflowList;
      this.workflowList.forEach(work => {
        work.isView = this.viewWorkflowPer;
        work.isTitleEditable = this.editWorkflowPer;
        work.isStageEditable = this.editWorkflowPer;
        work.isDuplicable = this.duplicateWorkflowPer;
        if (work.workflow_code === 'WPR00') {
          work.isDelete = false;
        } else {
          work.isDelete = this.deleteWorkflowPer;
        }
        work.showOptions = (!work.isView && !work.isTitleEditable && !work.isStageEditable && !work.isDelete) ? false : true;
      });
      this.usageCount = res.WorkflowUsageCount;
      const data1 = [];
      const data2 = [];
      for (const u in this.usageCount) {
        if (u) {
          for (const wf of this.workflowList) {
            if (u === wf.workflow_id) {
              data1.push(u);
              data2.push(this.usageCount[u]);
              wf['usage'] = this.usageCount[u];
            }
          }
        }
      }
      this.wfLoader = false;
    }).catch((ex) => {
      this.wfLoader = false;
    });

  }

  // onSearch(txt) {
  //   if (txt === undefined || txt === '' || txt.length < 3) {
  //     this.searchedWorflows = this.workflowList;
  //   } else {
  //     this.searchedWorflows = this.workflowList.filter(workflow => {
  //       if ((workflow.name.toLowerCase()).includes(txt.toLowerCase())) {
  //         return workflow;
  //       }
  //     });
  //   }
  // }

  onOptionClicked(event) {
    console.log(event);
    switch (event.clickedOn) {
      case 'viewWorkflowPer':
        this.viewWorkFlow('view', event.data.workflow_id);
        break;
      case 'editWorkflowPer_title':
        this.isDupliModal = false;
        this.workflowTitle = event.data.name;
        this.workflow_id = event.data.workflow_id;
        this.setTitle();
        // this.deleteRole(event.data.user_id, event.data.first_name);
        break;
      case 'editWorkflowPer_stage':
        this.editWorkFlow('edit', event.data.workflow_id);
        break;
      case 'deleteWorkflowPer':
        this.deleteWorkFlow(event.data, event.data.workflow_id);
        break;
      case 'duplicateWorkflowPer':
        this.isDupliModal = true;
        this.workflowTitle = event.data.name + '_copy';
        this.workflow_id = event.data.workflow_id;
        this.checkValidation();
        break;
      default:
    }
    this.sharedService.searchTextEvent.next({
      text: '',
      trigger: false
    });
  }

  editWorkFlow(edit, workflow_id) {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_WORKFLOW + `/${edit}/${workflow_id}`;
    this.router.navigate([path]);
  }

  viewWorkFlow(view, workflow_id) {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_WORKFLOW + `/${view}/${workflow_id}`;
    this.router.navigate([path]);
  }

  userRoleRight() {
    if (this.authenticateUser.AuthenticateWorkFlow('Edit Workflow')) {
      this.editWorkflowPer = true;
    }
    if (this.authenticateUser.AuthenticateWorkFlow('Delete Workflow')) {
      this.deleteWorkflowPer = true;
    }
    if (this.authenticateUser.AuthenticateWorkFlow('View Workflow')) {
      this.viewWorkflowPer = true;
    }
    if (this.authenticateUser.AuthenticateWorkFlow('Create Workflow')) {
      this.createWorkflowPer = true;
    }
    if (this.authenticateUser.AuthenticateWorkFlow('Duplicate workflow')) {
      this.duplicateWorkflowPer = true;
    }

    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'View',
      type: 'viewWorkflowPer',
      value: this.viewWorkflowPer,
      modal: '',
      check: 'isView'
    },
    {
      name: 'Edit Title',
      type: 'editWorkflowPer_title',
      value: this.editWorkflowPer,
      modal: '#editTitle',
      check: 'isTitleEditable'
    },
    {
      name: 'Edit Stage',
      type: 'editWorkflowPer_stage',
      value: this.editWorkflowPer,
      modal: '',
      check: 'isStageEditable'
    },
    {
      name: 'Delete',
      type: 'deleteWorkflowPer',
      value: this.deleteWorkflowPer,
      modal: '',
      check: 'isDelete'
    },
    {
      name: 'Duplicate',
      type: 'duplicateWorkflowPer',
      value: this.duplicateWorkflowPer,
      modal: '#editTitle',
      check: 'isDuplicable'
    }
    ];
  }

  createWorkflow(create) {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_WORKFLOW_CREATE;
    this.router.navigate([path]);
  }

  /* --------- Functionality to update workflow title start --------- */


  updateWorkflowTitle() {
    const url = GlobalSettings.EDIT_WORKFLOW_STAGE + '/' + this.workflow_id;
    const obj = {
      'name': this.workflowTitle
    };
    this.service.postService(url, obj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'edit_workflow_title'
      });
      // this.getWorkFlowDetails();
      this.updateWorkflowTable(this.workflow_id, 'name', this.workflowTitle);
      this.onCancel();
    }).catch(ex => {
      console.log('updateWorkflowTitle exception', ex);
      if (ex.msg === 'Workflow Name already exists in current Tenant.') {
        this.sharedService.sucessEvent.next({
          type: 'workflow_exist'
        });
      }
    });
  }

  /* --------- Functionality to update workflow title end --------- */


  setTitle() {
    this.prevTitle = this.workflowTitle;
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
  }
  checkValidation() {
    if (this.isDupliModal) {
      if (this.workflowTitle.trim().length !== 0) {
        this.valid = true;
      } else {
        this.valid = false;
      }
    } else {
      if (this.prevTitle !== this.workflowTitle && this.workflowTitle.trim().length !== 0) {
        this.valid = true;
      } else {
        this.valid = false;
      }
    }

  }

  onCancel() {
    this.valid = false;
    this.form.reset();
  }

  deleteWorkFlow(data, id) {
    this.dialogService.confirm('Confirm', 'Are you sure you want to delete workflow - ' + data.name + ' ?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.DELETE_WORKFLOW + '/' + id;
          this.service.deleteServiceData(url).then((res: any) => {
            // this.getWorkFlowDetails();
            const length = res.split(',');
            if (res.length === 0) {
              this.sharedService.sucessEvent.next({
                type: 'delete_workflow'
              });
              this.deleteWorkflowFromTable(id);
            } else {
              this.sharedService.sucessEvent.next({
                customMsg: 'This workflow cannot be deleted as it is currently utilised in project(s) ' + res,
                type: 'workflow_associated'
              });
            }

          }).catch((ex) => {
            console.log('Error while deleting the workflow', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  duplicateWorkflow(event) {
    event.stopPropagation();
    event.preventDefault();
    const url = GlobalSettings.DUPLICATE_WORKFLOW + '/' + this.workflow_id,
      body = {
        'name': this.workflowTitle
      };

    this.service.postService(url, body).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'duplicate_workflow_created'
      });
      this.getWorkFlowDetails();
      this.onCancel();
      setTimeout(() => {
        document.getElementById('closeeditTitle').click();
      }, 100);
    }).catch(ex => {
      if (ex.msg === 'Workflow Name already exists in current Tenant.') {
        this.sharedService.sucessEvent.next({
          customMsg: ex.msg,
          type: 'workflow_exist'
        });
      }
    });
  }

  /**
   * To update workflow list table with updated value of any specific property
   * @param id (unique id)
   * @param updateProp (which column proprty value needs to be updated)
   * @param updateValue (updated value)
   */
  updateWorkflowTable(id: any, updateProp: any, updateValue: any) {
    Utils.updateArray(id, this.workflowList, 'workflow_id', updateProp, updateValue);
  }

  /**
   * To remove workflow from table list
   * @param id (workflow id)
   */
  deleteWorkflowFromTable(id: any) {
    if (this.workflowList && this.workflowList.length) {
      this.workflowList = this.workflowList.filter(workflow => {
        return (workflow.workflow_id !== id);
      });
    }
  }
}
