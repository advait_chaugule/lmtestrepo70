import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { GlobalSettings } from '../../../global.settings';
import { CommonService } from '../../../common.service';

@Component({
  selector: 'app-view-workflow-modal',
  templateUrl: './view-workflow-modal.component.html',
  styleUrls: ['./../workflow/workflow.component.scss', './../workflow/workflow-step.scss', './../workflow/workflow-timeline.scss', './view-workflow-modal.component.scss']
})
export class ViewWorkflowModalComponent implements OnInit, OnChanges {
  @Input() workflowId;
  @Output() cancelEvent: EventEmitter<any>;

  workflowData: any[] = [];

  constructor(private service: CommonService) {
    this.cancelEvent = new EventEmitter<any>();
  }

  ngOnInit() {}

  ngOnChanges() {
    this.getWorkFlowById(this.workflowId);
  }

  /**
   * To get workflow data by id
   * @param workflowId
   */
  getWorkFlowById(workflowId) {
    if (workflowId) {
      const url = GlobalSettings.GET_WORKFLOW + '/' + workflowId;
      this.service.getServiceData(url).then((res: any) => {
        this.workflowData = res.workflow.stages;
        console.log(res);
        setTimeout(() => {
          focusOnModalCloseBtn();
        }, 500);
      }).catch((ex) => {
        console.log('Get workflow by id ex', ex);
      });
    } else {
      this.workflowData = [];
    }
  }

  onCancelPreviewWorkflow() {
    this.workflowData = [];
    this.cancelEvent.emit('');
  }

}
