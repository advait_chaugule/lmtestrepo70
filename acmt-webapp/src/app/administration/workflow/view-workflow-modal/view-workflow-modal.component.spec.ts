import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWorkflowModalComponent } from './view-workflow-modal.component';
import { ModalComponent } from 'src/app/common/modal/modal.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedService } from 'src/app/shared.service';

describe('ViewWorkflowModalComponent', () => {
  let component: ViewWorkflowModalComponent;
  let fixture: ComponentFixture<ViewWorkflowModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewWorkflowModalComponent, ModalComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, ConfirmationDialogService, SharedService],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWorkflowModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ViewWorkflowModalComponent', () => {
    expect(component).toBeTruthy();
  });
});
