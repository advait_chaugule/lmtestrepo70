import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import Utils from '../../../utils';
@Component({
  selector: 'app-template-preview',
  templateUrl: './template-preview.component.html'
})
export class TemplatePreviewComponent implements OnInit {

  constructor() {}
  @Input() nodeTemplate;

  ngOnInit() {}

  calculatePadding(level) {
    return Utils.calculatePadding(level, 'treeview');
  }

}
