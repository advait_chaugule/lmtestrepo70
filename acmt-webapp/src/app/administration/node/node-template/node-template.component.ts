import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';
import {
  from
} from 'rxjs';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  Router
} from '@angular/router';
import {
  filter
} from 'rxjs/operators';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import Utils from '../../../utils';
import { AutoCompleteComponent } from 'src/app/common/auto-complete/auto-complete.component';
/*import { NgxBootstrapProductTourService } from 'ngx-bootstrap-product-tour';*/
/*import { WalktroughService } from '../../../help/walkthrough/walktrough.service';*/
@Component({
  selector: 'app-node-template',
  templateUrl: './node-template.component.html',

})
export class NodeTemplateComponent implements OnInit, OnChanges {

  constructor(
    private authenticateUser: AuthenticateUserService,
    private service: CommonService,
    private router: Router,
    private fb: FormBuilder,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService*/) {
    /* Search Event Subscription Started */

    // this.sharedService.searchEvent.subscribe((event: any) => {
    //   if (event.type && event.type === 'Node Template') {
    //     this.onSearchTextChange(event.text);
    //     this.textToSearch = event.text;
    //   } else {
    //     this.textToSearch = '';
    //   }
    // });
    /* Search Event Subscription Ended */
  }

  nodeTemplate = []; // list to be previewed
  templateName; // Template name
  form: FormGroup;
  nodeTypes;
  selectedNodeType = null;
  delNodeTemplate;
  createNodeTemplate;
  editNodeTemplate;
  nodeTemplateLength = false;
  nodeTemplateList;
  createObj; // Object send to api
  parentObj; // Parent obj as document
  searchText: boolean; // flag to determine the text search has populated any data in metadatalist array
  template_id = null; // stores template id used to determine if add / edit is trigger on modal
  enableUpdate: Boolean = false;
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Template Name',
    propName: 'name',
    class: '',
    type: 'text'
  },
  {
    name: 'Usage Count',
    propName: 'usage',
    class: '',
    width: '15%',
    type: 'text'
  }
  ];
  optionList = [];
  textToSearch = '';
  searchedNodeType = [];

  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;

  ngOnInit() {
    this.form = this.fb.group({
      templateName: [null, Validators.compose([Validators.required])],
      type: [null, Validators.compose([Validators.required])]
    });
    this.userRoleRight();
    this.getNodeTemplateList();
    this.getAllNodeTypes();
    /*this.tourService.end();
    this.tourService.initialize(this.walkService.getWalkthroughData('nodetemplate_list'));*/
    this.sharedService.faqEvent.next({
      name: 'node_template_list'
    });
  }

  ngOnChanges() { }

  /* --------- Node Template List padding function start --------- */

  calculatePadding(level) {
    return Utils.calculateNodeTemplatePadding(level, 'treeview');
  }

  /* --------- Node Template List padding function end --------- */

  /* --------- Fetch Node Templates function start --------- */

  getNodeTemplateList() {
    const url = GlobalSettings.NODE_TEMPLATE;
    this.service.getServiceData(url).then((res: any) => {
      this.nodeTemplateList = res;
      this.nodeTemplateList.forEach(node => {
        node.isEditable = true;
        node.isDelete = true;
        node.showOptions = (!node.isEditable && !node.isDelete) ? false : true;
      });
      if (this.nodeTemplateList.length > 0) {
        this.searchText = false;
      } else {
        this.searchText = true;
      }
      // console.log(JSON.stringify(this.nodeTemplateList));
      this.userRoleRight();
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  /* --------- Fetch Node Templates function end --------- */

  /* --------- Node Type dropdown function service call start --------- */

  getAllNodeTypes() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.nodeTypes = res.nodetype.filter(function (obj) {
        return obj.title !== 'Document';
      });
      this.parentObj = res.nodetype.filter(function (obj) {
        return obj.title === 'Document';
      });
      // console.log(JSON.stringify(this.nodeTypes));
    });
  }

  /* --------- Node Type dropdown function service call end --------- */


  /* --------- Node Type selection function start --------- */

  selectNodeType(value) {
    let check = true;
    let name, id;
    [name, id] = value.split('#||#');
    const selectNodeTypeObj = {
      'title': name,
      'node_type_id': id
    };
    from(this.nodeTemplate)
      .filter((obj: any) => obj.node_type_id === selectNodeTypeObj.node_type_id)
      .subscribe(result => {
        check = false;
      });
    if (check) {
      if (selectNodeTypeObj.title !== 'null') {
        this.nodeTemplate.push(selectNodeTypeObj);
        if (this.form.controls['type']) {
          this.form.controls['type'].setValue(this.nodeTemplate);
        }
        this.enableUpdate = true;
        this.nodeTemplateLength = true;
        this.onModelChange();
      }
    } else {
      this.sharedService.sucessEvent.next({
        type: 'node_template_exists'
      });
    }
  }

  /* --------- Node Type selection function end --------- */

  /* --------- Node Template Object creation function start --------- */

  createNodeTemplateObj(operation) {
    this.createObj = {
      'name': this.templateName,
      'node_types': []
    };
    const tempArr = [];
    let parent_node_type_id;
    for (const obj in this.nodeTemplate) {
      if (obj) {
        const tempObj = {};
        if (obj === '0') {
          tempObj['parent_node_type_id'] = this.parentObj[0]['node_type_id'];
        } else {
          tempObj['parent_node_type_id'] = parent_node_type_id;
        }
        parent_node_type_id = this.nodeTemplate[obj]['node_type_id'];
        tempObj['node_type_id'] = this.nodeTemplate[obj]['node_type_id'];
        tempObj['sort_order'] = Number(obj) + 2;
        this.createObj['node_types'].push(tempObj);
      }
    }
    this.createObj['node_types'].unshift({
      'parent_node_type_id': '',
      'node_type_id': this.parentObj[0]['node_type_id'],
      'sort_order': 1
    });
    // console.log('nodeTemplate ==> ' + JSON.stringify(this.createObj));
    if (operation === 'add') {
      this.saveNodeTemplate();
    } else {
      this.updateNodeTemplate();
    }
  }

  /* --------- Node Template Object creation function end --------- */


  /* --------- Save Node Template Service call function start --------- */

  saveNodeTemplate() {

    const url = GlobalSettings.NODE_TEMPLATE;
    this.service.postService(url, this.createObj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'node_template_add'
      });
      this.onCancel();
      this.getNodeTemplateList();
    }).catch((ex) => {
      console.log('createNodeTemplate error occured', ex);
    });
  }

  /* --------- Save Node Template Service call function end --------- */

  // On click Menu option

  onOptionClicked(event) {
    console.log(event);
    switch (event.clickedOn) {
      case 'editNodeTemplate':
        this.onEditNodeTemplate(event.data);
        break;
      case 'delNodeTemplate':
        this.deleteNodeTemplate(event.data);
        break;
      default:
    }
  }

  /* --------- Get the node template on click of edit button functionality start --------- */

  onEditNodeTemplate(data) {
    this.template_id = data.node_template;
    this.templateName = data.name;
    this.nodeTemplateLength = true;
    const url = GlobalSettings.NODE_TEMPLATE + '/' + data['node_template'];
    this.service.getServiceData(url).then((res: any) => {
      this.nodeTemplate = res.node_types.splice(1, res.node_types.length);
    }).catch((ex) => {
      console.log('Error caught in Node template while editing', ex);
    });
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
  }

  /* --------- Get the node template on click of edit button functionality end --------- */

  /* --------- Update Node Template Service call function start --------- */
  updateNodeTemplate() {
    const url = GlobalSettings.NODE_TEMPLATE + '/' + this.template_id;
    this.service.putService(url, this.createObj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'node_template_update'
      });
      this.onCancel();
      this.getNodeTemplateList();
    }).catch((ex) => {
      console.log('updateNodeTemplate error occured', ex);
    });
  }
  /* --------- Update Node Template Service call function end --------- */

  /* --------- Delete Node Type from the draggable listed functionality start --------- */

  deleteNodeType(value) {
    this.dialogService.confirm('Confirm', 'Do you want to delete the node type?')
      .then((confirmed) => {
        if (confirmed) {
          this.nodeTemplate = this.nodeTemplate.filter(items => items !== value);
          if (this.form.controls['type']) {
            this.form.controls['type'].setValue(this.nodeTemplate);
          }
          this.enableUpdate = true;
          this.onModelChange();
          if (this.nodeTemplate.length === 0) {
            this.nodeTemplateLength = false;
            this.enableUpdate = false;
            if (document.getElementById('nodeType')) {
              document.getElementById('nodeType').getElementsByTagName('option')[0].selected = true;
            }
          }
          if (this.autoComplete) {
            this.autoComplete.deleteObj(value, { target: { id: 'deleteSelectedElement' } });
          }
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  /* --------- Delete Node Type from the draggable listed functionality end --------- */

  /* --------- Check Permission functionality Started --------- */

  userRoleRight() {
    if (this.authenticateUser.AuthenticateNodeTemplate('Create Node Templates')) {
      this.createNodeTemplate = true;
    } else {
      this.createNodeTemplate = false;
    }
    if (this.authenticateUser.AuthenticateNodeTemplate('Delete Node Templates')) {
      this.delNodeTemplate = true;
    } else {
      this.delNodeTemplate = false;
    }
    if (this.authenticateUser.AuthenticateNodeTemplate('Edit Node Templates')) {
      this.editNodeTemplate = true;
    } else {
      this.editNodeTemplate = false;
    }
    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit',
      type: 'editNodeTemplate',
      value: this.editNodeTemplate,
      modal: '#add-nodetemplate',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'delNodeTemplate',
      value: this.delNodeTemplate,
      modal: '',
      check: 'isDelete'
    }
    ];
  }

  /* --------- Check Permission functionality Ended --------- */

  /* --------- Cancel function start --------- */

  onCancel() {
    console.log('cancle called');
    this.form.reset();
    this.form.controls.type.reset();
    this.createObj = {};
    this.nodeTemplate = [];
    this.nodeTemplateLength = false;
    this.enableUpdate = false;
    this.template_id = '';
    if (this.autoComplete) {
      this.autoComplete.clearSelection();
    }
    this.sharedService.faqEvent.next({
      name: 'node_template_list'
    });
  }

  /* --------- Cancel function end --------- */





  /* --------- Edit function start --------- */

  deleteNodeTemplate(data) {
    this.dialogService.confirm('Confirm', 'Are you sure you want to delete ' + data['name'] + ' template?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.NODE_TEMPLATE + '/' + data['node_template'];
          this.service.deleteServiceData(url).then((res: any) => {
            this.sharedService.sucessEvent.next({
              type: 'node_template_delete'
            });
            this.getNodeTemplateList();
          }).catch((ex) => {
            console.log('Error while deleting the template', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }

  /* --------- Edit function end --------- */


  /* --------- Search functionality Started --------- */
  onSearchTextChange(txt) {
    /*if (txt.length > 2) {
      const url = GlobalSettings.NODE_TEMPLATE_SEARCH + txt;
      this.service.getServiceData(url).then((res: any) => {
        this.nodeTemplateList = res;
      }).catch(ex => {
        console.log('list of node templates ex ', ex);
      });
    } else {
      this.getNodeTemplateList();
    }*/
    if (this.nodeTemplateList) {

      if (txt === undefined || txt === '' || txt.length < 3) {
        this.searchedNodeType = this.nodeTemplateList;
      } else {
        this.searchedNodeType = this.nodeTemplateList.filter(obj => {
          if ((obj.name.toLowerCase()).includes(txt.toLowerCase())) {
            return obj;
          }
        });
      }
    }
  }
  /* --------- Search functionality Ended --------- */

  /* --------- List all node template list on close of alert for search functionality start --------- */

  onClose() {
    this.getNodeTemplateList();
  }

  /* --------- List all node template list on close of alert for search functionality end --------- */


  /* --------- On excape key press function start --------- */
  onMouseCall(event) {
    if (event.keyCode === 27) {
      this.onCancel();
    }
  }
  /* --------- On excape key press function end --------- */

  onNodeRearranged(event) {
    this.enableUpdate = true;
    this.onModelChange();
  }

  onModelChange() {
    if (this.form.controls.templateName.valid && this.nodeTemplateLength) {
      this.enableUpdate = true;
    } else {
      this.enableUpdate = false;
    }
  }

  onOpenNodetemplateCreateModal() {
    this.sharedService.faqEvent.next({
      name: 'create_node_template'
    });
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
  }

  changeStatus(val1, val2) {

  }

  onClickedOutside(e) {
    if (this.autoComplete) {
      this.autoComplete.openPanel(false);
    }
  }
}
