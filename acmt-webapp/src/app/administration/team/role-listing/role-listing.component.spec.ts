import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  RoleListingComponent
} from './role-listing.component';
import {
  ModalComponent
} from 'src/app/common/modal/modal.component';
import {
  NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  AuthenticateUserService
} from 'src/app/authenticateuser.service';
import {
  CommonService
} from 'src/app/common.service';
import {
  SharedService
} from 'src/app/shared.service';
import {
  ConfirmationDialogService
} from 'src/app/confirmation-dialog/confirmation-dialog.service';
import {
  HttpClientModule
} from '@angular/common/http';
import {
  RouterTestingModule
} from '@angular/router/testing';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  MockCommonService
} from '../../mock.common.service';

describe('RoleListingComponent', () => {
  let component: RoleListingComponent;
  let fixture: ComponentFixture < RoleListingComponent > ;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [RoleListingComponent, ModalComponent],
        imports: [HttpClientModule, RouterTestingModule, FormsModule, ReactiveFormsModule],
        providers: [AuthenticateUserService, {
          provide: CommonService,
          useClass: MockCommonService
        }, SharedService, ConfirmationDialogService],
        schemas: [NO_ERRORS_SCHEMA]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create RoleListingComponent', () => {
    expect(component).toBeTruthy();
  });

  it('Should get user list', async () => {
   await component.getUsers();
    expect(component.userList.length).toBe(5);
  });

  it('Should get role list', async () => {
   await component.getRoles();
    expect(component.roleList.length).toBe(3);
  });
});
