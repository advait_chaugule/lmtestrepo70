import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl

} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';

import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import Utils from '../../../utils';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../../help/walkthrough/walktrough.service';*/

@Component({
  selector: 'app-role-listing',
  templateUrl: './role-listing.component.html'
})
export class RoleListingComponent implements OnInit, AfterViewInit {
  public role: Role = <Role>{};
  roleList: any[] = [];
  usageCount: any[] = [];
  userList: any[] = [];
  // searchedRoles;
  form: FormGroup;
  createRoleButton = false;
  deleteRoleButton = false;
  editRoleButton = false;
  viewRoleButton = false;
  viewRolePermission = false;
  listRoleButton = false;
  roleStatusButton = false;
  userListCount = false;
  showBtn = false; // to solve the UI issue while loading the createRoleButton
  tabItems = ['roles', 'users']; // contains all the tabs for the current view
  currentTab = this.tabItems[0].toLowerCase(); // selects the current tab as default view
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'name',
    class: '',
    type: 'text'
  },
  {
    name: 'Status',
    propName: 'active_status',
    class: '',
    width: '8%',
    type: 'dropdown',
    canFilter: true
  },
  {
    name: 'Usage',
    propName: 'usage',
    class: '',
    width: '2%',
    type: 'text',
    canFilter: true
  }
  ];
  optionList = [];
  statusList = [{
    name: 'Active',
    value: '1',
    propName: 'role_id'
  },
  {
    name: 'Inactive',
    value: '0',
    propName: 'role_id'
  }
  ];
  @ViewChild('myname', { static: false }) input;

  constructor(
    private AuthenticateUser: AuthenticateUserService,
    private service: CommonService,
    private router: Router,
    private fb: FormBuilder,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService*/
  ) {

    /* Search Event Subscription Started */
    // this.sharedService.searchEvent.subscribe((event: any) => {
    //   console.log('search by', event);
    //   if (event.type && event.type === 'Team') {
    //     this.onSearch(event.text);
    //   }
    // });
    /* Search Event Subscription Ended */

  }

  ngOnInit() {

    this.getRoles();
    this.getUsers();
    this.form = this.fb.group({
      rolename: [null, Validators.compose([Validators.required, this.service.nospaceValidator])],
      description: [null]
    });
    this.userRoleRight();
    /*  this.tourService.end();
      this.tourService.initialize(this.walkService.getWalkthroughData('role_list'));*/
    this.sharedService.faqEvent.next({
      name: 'role_list'
    });
  }

  ngAfterViewInit() {
    // init_rippleEffect();
  }

  getUsers() {
    const url = GlobalSettings.GET_USERS;
    this.service.getServiceData(url).then((res: any) => {
      // console.log(' res.UserList.users', res.UserList.users);
      this.userList = res.UserList.users;
      this.userRoleRight();
    }).catch((ex) => {

    });
  }

  getRoles() {
    this.sharedService.searchText.next({
      'searchText': 'clear'
    });
    const url = GlobalSettings.GET_ROLE;
    this.service.getServiceData(url).then((res: any) => {
      this.roleList = res.RoleList.roles;
      // this.searchedRoles = this.roleList;
      this.roleList.forEach(role => {
        role.isView = this.viewRoleButton;
        role.isEditable = this.editRoleButton;
        role.isPermitted = this.viewRolePermission;
        role.active_status = role.is_active === 1 ? 'Active' : 'Inactive';
        if (role.role_code === 'PPR01' || role.role_code === 'PPR00') {
          role.isDelete = false;
        } else {
          role.isDelete = this.deleteRoleButton;
        }
        role.showOptions = (!role.isView && !role.isEditable && !role.isPermitted && !role.isDelete) ? false : true;
      });
      this.usageCount = res.RoleUsageCount;
      this.userRoleRight();
      // const data1 = [];
      // const data2 = [];
      for (const u in this.usageCount) {
        if (u) {
          for (const r of this.roleList) {
            if (u === r.role_id) {
              // data1.push(u);
              // data2.push(this.usageCount[u]);
              r['usage'] = this.usageCount[u];
            }
          }
        }
      }
    }).catch((ex) => {

    });
  }

  // onSearch(txt) {
  //   if (txt === undefined || txt === '' || txt.length < 3) {
  //     this.searchedRoles = this.roleList;
  //   } else {
  //     this.searchedRoles = this.roleList.filter(role => {
  //       if ((role.name.toLowerCase()).includes(txt.toLowerCase())) {
  //         return role;
  //       }
  //     });
  //   }
  // }

  // onClose() {
  //   this.onSearch('');
  // }

  onSubmit() {
    const url = GlobalSettings.CREATE_ROLE;
    if (this.role.role_id === undefined) {
      this.role.name = this.role.name.trim();
      const tempData = {
        name: this.role.name,
        description: this.role.description = this.role.description === undefined ? null : this.role.description,
      };
      this.service.postService(url, this.role, 0, true).then((res: any) => {
        if (res.success) {
          this.getRoles();
          this.onCancel();
          this.sharedService.sucessEvent.next({
            type: 'create_role'
          });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: res.message,
          });
        }
      }).catch(ex => {
        console.log('create role', ex);
      });
    } else {
      this.service.putService(url + '/' + this.role.role_id, this.role, 0, true).then((res: any) => {
        if (res.success) {
          this.getRoles();
          this.sharedService.sucessEvent.next({
            type: 'update_role'
          });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: res.message,
          });
        }
      }).catch(ex => {
        console.log('update role', ex);
      });
    }
  }

  onCancel() {
    this.form.reset();
    this.sharedService.faqEvent.next({
      name: 'role_list'
    });
  }

  // ----------------for Common Data table----------------- //

  onOptionClicked(event) {
    console.log(event);
    switch (event.clickedOn) {
      case 'editRoleButton':
        this.onEditUser(event.data);
        break;
      case 'viewRolePermission':
        this.editPermissions(event.data.role_id, event.data.name);
        break;
      case 'deleteRoleButton':
        this.deleteRole(event.data.role_id, event.data.name);
        break;
      default:
    }
  }

  editPermissions(roleId, name) {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_PERMISSIONS + `/${roleId}/${name}`;
    this.router.navigate([path]);
  }

  deleteRole(roleId, roleName) {

    this.dialogService.confirm('Confirm', 'Do you want to delete role ' + roleName + ' ?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.CREATE_ROLE + '/' + roleId;
          this.service.deleteServiceData(url).then((res: any) => {
            this.getRoles();
            this.sharedService.sucessEvent.next({
              type: 'delete_role'
            });
          }).catch((ex) => {
            this.sharedService.sucessEvent.next({
              type: 'prevent_delete_role'
            });
            console.log('Error while deleting the role', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }

  onTabSelected(tab) {
    this.currentTab = tab;
    if (this.currentTab === 'users') {
      this.getUsers();
      this.sharedService.faqEvent.next({
        name: 'create_user'
      });
    } else {
      this.getRoles();
      this.sharedService.faqEvent.next({
        name: 'role_list'
      });
    }
  }

  onEditUser(data) {
    if (data && data !== undefined) {
      // console.log(' userModal ', this.input, data);
      this.input.nativeElement.click();
      const newData = JSON.stringify(data);
      this.role = JSON.parse(newData);
      // console.log(' userModal sss', this.input, data);
    } else {
      this.role = <Role>{};

    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'create_role'
    });
  }

  userRoleRight() {
    this.showBtn = true;
    if (this.AuthenticateUser.AuthenticateUserRole('Create Role')) {
      this.createRoleButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('Delete Role')) {
      this.deleteRoleButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('Edit Role')) {
      this.editRoleButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('View Role')) {
      this.viewRoleButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('Edit Role Permission')) {
      this.viewRolePermission = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('List Roles')) {
      this.listRoleButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('Change Role Status')) {
      this.roleStatusButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('List User')) {
      this.userListCount = true;
    }
    if (this.listRoleButton === false) {
      if (this.userListCount) {
        this.currentTab = 'users';
      }
    }

    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'View',
      type: 'viewRoleButton',
      value: this.viewRoleButton,
      check: 'isView'
    },
    {
      name: 'Edit Role',
      type: 'editRoleButton',
      value: this.editRoleButton,
      check: 'isEditable'
    },
    {
      name: 'Edit Permissions',
      type: 'viewRolePermission',
      value: this.viewRolePermission,
      check: 'isPermitted'
    },
    {
      name: 'Delete',
      type: 'deleteRoleButton',
      value: this.deleteRoleButton,
      check: 'isDelete'
    }
    ];


  }

  mouseLeave() {
    this.getRoles();
  }

  changeStatus(status, role_id) {
    const url = GlobalSettings.CREATE_ROLE + '/' + 'status' + '/' + role_id;
    const tempObj = {
      is_active: status,
    };
    this.service.postService(url, tempObj).then((res: any) => {
      this.getRoles();
      this.sharedService.sucessEvent.next({
        type: 'update_role'
      });
    }).catch(ex => {
      console.log('update role', ex);
    });
  }

  /**
   * To get user list from user list component through output event whenever user listing api calling
   * @param event
   */
  onGetUserList(event) {
    if (event && event.users) {
      this.userList = event.users;
    }
  }

}
