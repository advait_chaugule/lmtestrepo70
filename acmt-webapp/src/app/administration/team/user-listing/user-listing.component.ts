import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  ViewChildren,
  QueryList
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormArray

} from '@angular/forms';
import {
  SharedService
} from '../../../shared.service';

import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import {
  CustomValidators
} from 'ng2-validation';
import {
  from
} from 'rxjs';
import {
  RoleListingComponent
} from '../role-listing/role-listing.component';
import {
  ConfirmationDialogService
} from '../../../confirmation-dialog/confirmation-dialog.service';
import { AutoCompleteComponent } from '../../../common/auto-complete/auto-complete.component';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../../help/walkthrough/walktrough.service';*/


@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit {

  ADD_ACTION = 'add';
  UPDATE_ACTION = 'edit';

  public user: USER = <USER>{};
  @ViewChild('myname', { static: false }) input;
  @ViewChildren('autoComplete') autoCompletes: QueryList<AutoCompleteComponent>;
  @Output() userListEvent: EventEmitter<any> = new EventEmitter<any>();
  form: FormGroup;
  userList: any[];
  roleList: any[] = [];
  // searchedUsers;
  system_role;
  createUserButton: boolean;
  editUserButton: boolean;
  deleteUserButton: boolean;
  userDetailsButton: boolean;
  userListButton: boolean;
  validEmail: boolean;
  roleStatusButton: boolean;
  resendEmailButton: boolean;
  selectedRole = null;
  formValidation = false;
  selectType = 'single';
  uniqueId = 'role_id';
  autoCompleteTitle = 'Platform Role';
  addedUser: any[] = []; // holds added user list while creating another user
  currentIndex = -1; // holds currently user index in list to be added or delete in add user screen
  action = this.ADD_ACTION; // it denotes 'Add' or 'Delete' operation
  submitLoader = false;
  addLocation = 'create'; // 'another'
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'name',
    class: '',
    type: 'text'
  },
  {
    name: 'Email',
    propName: 'email',
    class: '',
    type: 'text'
  },
  {
    name: 'Platform Role',
    propName: 'platform_role',
    class: '',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Status',
    propName: 'active_status',
    class: '',
    type: 'dropdown',
    canFilter: true
  },
  {
    name: 'Projects',
    propName: 'usage',
    class: '',
    type: 'text'
  }
  ];
  optionList = [];
  statusList = [{
    name: 'Active',
    value: '1',
    propName: 'user_id'
  },
  {
    name: 'Inactive',
    value: '0',
    propName: 'user_id'
  }
  ];


  constructor(
    private AuthenticateUser: AuthenticateUserService,
    private roleListingComponent: RoleListingComponent,
    private service: CommonService,
    private fb: FormBuilder,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
   /* private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService*/) {
    /* Search Event Subscription Started */
    // this.sharedService.searchEvent.subscribe((event: any) => {
    //   console.log('search by', event);
    //   if (event.type && event.type === 'Team') {
    //     this.onSearch(event.text);
    //   }
    // });
    /* Search Event Subscription Ended */
  }

  ngOnInit() {
    this.getUsers();
    this.getRoles();

    this.form = new FormGroup({
      userFArray: new FormArray([
        this.fb.group({
          email: [null, Validators.compose([Validators.required, Validators.email])],
          role: [null, Validators.compose([Validators.required])]
        })
      ])
    });

    this.userRoleRight();
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('user_list'));*/
    this.sharedService.searchText.next({
      'searchText': 'clear'
    });
    this.sharedService.faqEvent.next({
      name: 'create_user'
    });
  }

  // For getting users as Form array of multiple dynamically added user in reactive form
  get users() {
    return this.form.get('userFArray') as FormArray;
  }

  // For add user purpose in add user screen on 'Add another user' button click
  addUserForm() {
    /* If user is already added in database (checking based on 'isAdded'), then don't call API for that added user, just
     * simply add new row for another user. Or row will will be added after API call response if user does not exists in database
    */

    if (this.form.get('userFArray') && (this.form.get('userFArray') as FormArray).controls[this.currentIndex] &&
      (this.form.get('userFArray') as FormArray).controls[this.currentIndex].valid) { // For valid form checking

      if ((this.addedUser.length - 1 > -1) && !(this.addedUser[this.addedUser.length - 1].isAdded)) { // api call for not exists user
        this.onSubmit(this.addedUser.length - 1, true);
      } else {
        this.addUserInList();
      }
    } else {
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: 'Please fill all mandatory fields.'
      });
    }
  }

  /**
   * To add user with form group in added list while adding using 'Add another user' button in add user screen
   */
  addUserInList(isFormCreate = true) {
    if (isFormCreate) {
      this.users.push( // pushing new form group for newly added user
        this.fb.group({
          email: [null, Validators.compose([Validators.required, CustomValidators.email])],
          role: [null, Validators.compose([Validators.required])]
        })
      );
    }
    const user: any = {} as USER;
    user.user_id = null;
    user.email_id = '';
    user.role_id = '';
    user.roleList = JSON.parse(JSON.stringify(this.roleList));
    user.emailExistsErr = false;
    user.isAdded = false;

    this.addedUser.push(user); // pushing new User object
    this.system_role = { name: 'Select Platform Role' };
    this.currentIndex = this.currentIndex + 1;
  }

  // For delete user purpose in add user screen
  deleteUserForm(index: any) {
    if (this.addedUser[index] && this.addedUser[index].isAdded) {
      // If user added in database, then call delete api
      this.deleteUserApiCall(this.addedUser[index].user_id, 'modal', index);
    } else { // else remove user just from UI
      this.removeUserFromList(index);
    }
  }

  /**
   * To remove user with form group from added list while deleting in add user screen
   * @param index (user index in list)
   */
  removeUserFromList(index: number) {
    this.currentIndex = this.currentIndex - 1;
    this.users.removeAt(index); // remove from form group
    this.addedUser.splice(index, 1);
    if (this.addedUser.length < 1) {
      this.addUserInList();
    }
  }

  getUsers() {
    this.userRoleRight();
    const url = GlobalSettings.GET_USERS;
    this.service.getServiceData(url).then((res: any) => {
      this.userList = res.UserList.users;
      // this.searchedUsers = this.userList;
      this.userListEvent.emit({ 'users': this.userList });
      this.userList.forEach(user => {
        user.name = user.first_name + ' ' + (user.last_name ? user.last_name : '');
        user.platform_role = user.system_role.name;
        user.active_status = user.is_active === 1 ? 'Active' : 'Inactive';
        user.isView = this.userDetailsButton;
        user.isEditable = this.editUserButton;
        user.isDelete = this.deleteUserButton;
        user.isResend = this.resendEmailButton && !user.is_register ? true : false;
        user.showOptions = (!user.isView && !user.isEditable && !user.isDelete) ? false : true;
      });
      const data1 = [],
        data2 = [];
      for (const u in res.UserProjectCount) {
        if (u) {
          for (const r of this.userList) {
            if (u === r.user_id) {
              data1.push(u);
              data2.push(res.UserProjectCount[u]);
              r['usage'] = res.UserProjectCount[u];
            }
          }
        }
      }

    }).catch((ex) => {

    });
  }

  getRoles() {
    const url = GlobalSettings.GET_ROLE;
    this.service.getServiceData(url).then((res: any) => {
      res.RoleList.roles.forEach(element => {
        if (element.is_active === 1) {
          this.roleList.push(element);
        }
      });
      if (this.addedUser && this.addedUser[0]) {
        this.addedUser[0].roleList = JSON.parse(JSON.stringify(this.roleList));
      }
    }).catch((ex) => {

    });
  }

  // onSearch(txt) {
  //   if (this.userList) {

  //     if (txt === undefined || txt === '' || txt.length < 3) {
  //       this.searchedUsers = this.userList;
  //     } else {
  //       this.searchedUsers = this.userList.filter(user => {
  //         if ((user.name.toLowerCase()).includes(txt.toLowerCase()) || (user.email.toLowerCase()).includes(txt.toLowerCase())) {
  //           return user;
  //         }
  //       });
  //     }
  //   }
  // }

  onOptionClicked(event) {
    switch (event.clickedOn) {
      case 'editUserButton':
        this.onEditUser(event.data);
        break;
      case 'deleteUserButton':
        this.deleteRole(event.data.user_id, event.data.first_name);
        break;
      case 'resendEmailButton':
        this.resendEmail(event.data.email, event.data.first_name);
        break;
      default:
    }
  }

  resendEmail(emailAddress, firstName) {
    const url = GlobalSettings.RESEND_EMAIL;
    const body = {
      email: emailAddress
    };
    this.service.postService(url, body).then((res: any) => {
      this.sharedService.sucessEvent.next({
        customMsg: 'Email resent successfully.',
        type: 'success'
      });
    }).catch(ex => {
      console.log('Error while send email', ex);
    });
  }

  deleteRole(roleId, roleName) {

    this.dialogService.confirm('Confirm', 'Do you want to delete user ' + roleName + ' ?')
      .then((confirmed) => {
        if (confirmed) {
          this.deleteUserApiCall(roleId);
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  deleteUserApiCall(userId: any, callLocation = 'table', index?: number) {
    const url = GlobalSettings.GET_USERS + '/' + userId;
    this.service.deleteServiceData(url).then((res: any) => {
      if (callLocation === 'modal') {
        this.removeUserFromList(index);
      }
      this.getUsers();
      this.roleListingComponent.getUsers();
      this.sharedService.sucessEvent.next({
        type: 'delete_user'
      });
    }).catch((ex) => {
      console.log('Error while deleting the User ', ex);
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: ex.msg
      });
    });
  }

  selectRole(roleId, userIndex: number) {
    if (this.addedUser && this.addedUser[userIndex]) {
      this.addedUser[userIndex].role_id = roleId;
      if (this.form.get('userFArray') && (this.form.get('userFArray') as FormArray).controls[userIndex]) {
        ((this.form.get('userFArray') as FormArray).controls[userIndex] as FormArray).controls['role'].setValue(roleId);
      }
    }
  }

  // API calling method for user create or update
  onSubmit(index: number, isNewRowReq = false) {
    const url = GlobalSettings.GET_USERS;
    const user = this.addedUser[index];
    this.submitLoader = true;
    if (user.user_id === undefined || user.user_id === null) { // User adding
      this.service.postService(url, user).then((res: any) => {
        if (res.result === 'deleted') {
          this.sharedService.sucessEvent.next({
            type: 'user_deleted'
          });
        } else if (res.result === 'exist') {
          this.sharedService.sucessEvent.next({
            type: 'user_exist'
          });
        } else if (res.result === 'deactive') {
          this.sharedService.sucessEvent.next({
            type: 'user_deactive'
          });
        } else {
          this.getUsers();
          this.addedUser[index].user_id = res.user_id;
          this.addedUser[index].isAdded = true;
          this.sharedService.sucessEvent.next({
            type: 'create_user'
          });
          if (isNewRowReq) {
            this.addUserInList(); // push new row in Add User screen while successfully user added
          }
        }
        this.submitLoader = false;
      }).catch(ex => {
        console.log('err', ex);
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg
        });
        this.submitLoader = false;
      });
    } else { // User updating
      this.service.putService(url + '/' + user.user_id, user).then((res: any) => {
        this.getUsers();
        this.sharedService.sucessEvent.next({
          type: 'update_user'
        });
        this.submitLoader = false;
      }).catch(ex => {
        console.log('err', ex);
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg
        });
        this.submitLoader = false;
      });
    }
    // this.roleListingComponent.getUsers();
  }

  onCancel() {
    this.form.reset();
    this.autoCompletes.toArray().forEach(element => {
      element.clearSelection();
    });
    this.addedUser = [];
    this.currentIndex = -1;
    for (let i = 0; i < this.users.length; i++) { // Clearing each created form group
      if (i !== 0) { // Not clearing first form group for further later actions (add/edit)
        this.users.removeAt(i);
      }
    }
    // this.autoCompletes.clearSelection();
  }


  // For Add new User or Edit User
  onEditUser(data) {
    this.validEmail = false;
    this.addedUser = [];
    this.currentIndex = -1;
    if (data && data !== undefined) { // Edit user
      this.action = this.UPDATE_ACTION;
      const strData = JSON.stringify(data);
      const newData = JSON.parse(strData);
      const user = newData;
      user.email_id = newData.email;
      user.organization_id = user.organization_id === undefined ? null : user.organization_id;
      user.password = user.password === undefined ? null : user.password;
      user.roleList = JSON.parse(JSON.stringify(this.roleList));
      user.isAdded = true;
      this.system_role = user.system_role;
      this.addedUser.push(user);
      this.currentIndex = this.currentIndex + 1;

      // assigning user's role in form group's 'role' controller
      if (this.form.get('userFArray') && (this.form.get('userFArray') as FormArray).controls[this.currentIndex]) {
        ((this.form.get('userFArray') as FormArray).controls[this.currentIndex] as FormArray).controls['role'].setValue(user.system_role);
      }
    } else { // Add user
      this.action = this.ADD_ACTION;
      this.addUserInList(false);
    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
  }

  userRoleRight() {
    if (this.AuthenticateUser.AuthenticateUserRole('Create User')) {
      this.createUserButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('Edit User')) {
      this.editUserButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('Delete User')) {
      this.deleteUserButton = true;
    }

    if (this.AuthenticateUser.AuthenticateUserRole('List User')) {
      this.userListButton = true;
    }
    if (this.AuthenticateUser.AuthenticateUserRole('Change User Status')) {
      this.roleStatusButton = true;
    }
    if (this.AuthenticateUser.AuthenticateUserRole('Admin setup for resend email')) {
      this.resendEmailButton = true;
    }

    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'View',
      type: 'userDetailsButton',
      value: this.userDetailsButton,
      check: 'isView'
    },
    {
      name: 'Edit User',
      type: 'editUserButton',
      value: this.editUserButton,
      check: 'isEditable',
      modal: '#add-user'
    },
    {
      name: 'Delete',
      type: 'deleteUserButton',
      value: this.deleteUserButton,
      check: 'isDelete'
    },
    {
      name: 'Resend Email',
      type: 'resendEmailButton',
      value: this.resendEmailButton,
      check: 'isResend'
    }
    ];
  }

  mouseLeave() {
    // this.getUsers();
  }

  checkValidEmail(event: any, userIndex: number) {
    const value = event.target.value;
    this.validEmail = false; // email not exists
    from(this.userList)
      .filter((w: any) => w.email === value)
      .subscribe(result => {
        if (result !== undefined) {
          this.validEmail = true; // email already exists i.e. added for any user
        }
      });
    this.addedUser[userIndex].emailExistsErr = this.validEmail;
  }


  changeStatus(status, user_id) {
    const url = GlobalSettings.CREATE_USER + '/' + 'status' + '/' + user_id;
    const tempObj = {
      is_active: status,
    };
    let is_register = 0;
    for (let i = 0; i < this.userList.length; i++) {
      if (user_id === this.userList[i]['user_id']) {
        if (this.userList[i]['is_register']) {
          is_register = 1;
          break;
        }
      }
    }
    if (is_register) {
      this.service.postService(url, tempObj).then((res: any) => {
        this.getUsers();
        this.sharedService.sucessEvent.next({
          type: 'update_user'
        });
      }).catch(ex => {
        console.log('update user', ex);
      });
    } else {
      this.sharedService.sucessEvent.next({
        type: 'unregistered_user'
      });
    }

  }

  onClickedOutside(e, index: number) {
    // this.autoComplete.openPanel(false);
    for (let i = 0; i < this.autoCompletes.toArray().length; i++) {
      if (i === index) {
        this.autoCompletes.toArray()[i].openPanel(false);
        break;
      }
    }
  }

  // On Create button click for user create
  onCreateBtnClick() {
    if (this.form.get('userFArray') && (this.form.get('userFArray') as FormArray).controls[this.currentIndex] &&
      (this.form.get('userFArray') as FormArray).controls[this.currentIndex].valid) { // For valid form checking

      if ((this.addedUser.length - 1 > -1) && !(this.addedUser[this.addedUser.length - 1].isAdded)) { // api call for not exists user
        this.onSubmit(this.addedUser.length - 1);
      }
    } else {
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: 'Please fill all mandatory fields.'
      });
    }
  }

}
