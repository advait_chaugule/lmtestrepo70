import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListingComponent } from './user-listing.component';
import { ModalComponent } from 'src/app/common/modal/modal.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { RoleListingComponent } from '../role-listing/role-listing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('UserListingComponent', () => {
  let component: UserListingComponent;
  let fixture: ComponentFixture<UserListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserListingComponent, ModalComponent],
      imports: [HttpClientModule, RouterTestingModule, FormsModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AuthenticateUserService, CommonService, SharedService, ConfirmationDialogService, RoleListingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create UserListingComponent', () => {
    expect(component).toBeTruthy();
  });
});
