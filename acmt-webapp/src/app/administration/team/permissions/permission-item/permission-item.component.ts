import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-permission-item',
  templateUrl: './permission-item.component.html',
  styleUrls: ['./permission-item.component.scss']
})
export class PermissionItemComponent implements OnInit, OnChanges {

  @Input() itemName: string; // item display name for which permission is setting
  @Input() selectAll = false; // indicates that all permission checkbox for item is selected or not
  @Input() itemPermissions = []; // holds all permission list for a particular item
  @Input() disabledCheckbox = false;

  @Output() allSelectedEvent: EventEmitter<any> = new EventEmitter<any>(); // event on all checkbox selected
  @Output() permissionSelectedEvent: EventEmitter<any> = new EventEmitter<any>(); // event on single permission selected

  itemLowCaseName; // its used for creating unique id field for some DOM element
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected

  constructor() {
    this.renamePacingGuide = environment.renamePacingGuide;
  }

  ngOnInit() {
  }

  ngOnChanges() {
    const name = this.itemName ? (this.itemName.split(' ').length > 1 ? (this.itemName.split(' ')[0] + this.itemName.split(' ')[1])
      : this.itemName.split(' ')[0]) : '';
    this.itemLowCaseName = name.toLowerCase();
  }

  onSelectAllPermission(event) {
    this.allSelectedEvent.emit({ 'event': event, 'data': this.selectAll });
  }

  onSinglePermissionSelect(event, permission, index) {
    const data = {
      'event': event,
      'permission': permission,
      'index': index
    };
    this.permissionSelectedEvent.emit(data);
  }

  /* --------- Functionality to replace pacing guide str with unit planner based on config in env start --------- */

  getDisplayPermissionName(permissionName) {
    if (this.renamePacingGuide && permissionName.indexOf('Pacing Guide') !== -1) {
      permissionName = permissionName.replace('Pacing Guide', 'Unit Planner');
    }
    return permissionName;
  }

  /* --------- Functionality to replace pacing guide str with unit planner based on config in env end --------- */
}
