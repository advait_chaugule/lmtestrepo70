import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionItemComponent } from './permission-item.component';
import { FormsModule } from '@angular/forms';

describe('PermissionItemComponent', () => {
  let component: PermissionItemComponent;
  let fixture: ComponentFixture<PermissionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermissionItemComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create PermissionItemComponent', () => {
    expect(component).toBeTruthy();
  });
});
