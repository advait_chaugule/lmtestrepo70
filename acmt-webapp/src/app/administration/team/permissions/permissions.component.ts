import {
  Component,
  OnInit
} from '@angular/core';
import {
  CommonService
} from '../../../common.service';
import {
  GlobalSettings
} from '../../../global.settings';
import {
  ActivatedRoute,
  Params,
  Router
} from '@angular/router';
import * as Rx from 'rxjs';
import {
  SharedService
} from '../../../shared.service';
import {
  AuthenticateUserService
} from '../../../authenticateuser.service';
import * as _ from "lodash";
import Utils from '../../../utils';
import { environment } from '../../../../environments/environment';
import { find } from 'rxjs/operators';

@Component({
  selector: 'app-permissions-listing',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {
  PROJECT = 'Projects';
  TAXONOMY = 'Taxonomies';
  METADATA = 'Metadata';
  WORKFLOW = 'Workflow';
  USER_ROLE = 'User & Role Management';
  COMMENT = 'Comment';
  NODE_TEMPLATE = 'Node Template';
  NODE_TYPE = 'Node Type';
  NOTIFICATION = 'Notification';
  PUBLIC_REVIEW = 'Public Review';
  PACING_GUIDE = 'Pacing Guide Comments';
  WEBSITE_NOTES = 'Website notes & documents';

  permission_set: any[] = [];
  role_id: string;
  rolePermissions: any;
  systemPermissions: any;
  userRoleID: string;
  setPermissionButton: boolean;
  projectAdministrator: string;
  submitButton: boolean;
  uniqData: any;
  selectAllItems = []; // holds array of selectedAll permission check value for each block.
  permissionListContainer = []; // holds array of each block's permission list
  loadingData = false;
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  showLoader = false; // holds flag value to show loader while saving permissions

  constructor(private service: CommonService,
    private AuthenticateUser: AuthenticateUserService,
    private activatedRoute: ActivatedRoute, private router: Router, private sharedService: SharedService,
    private commonService: CommonService) {
    this.renamePacingGuide = environment.renamePacingGuide;
    if (this.renamePacingGuide) {
      this.PACING_GUIDE = 'Unit Planner Comments';
    }
  }

  ngOnInit() {
    /* Initializing all permission checkboxes as selected */
    this.selectAllItems[this.PROJECT] = true;
    this.selectAllItems[this.TAXONOMY] = true;
    this.selectAllItems[this.METADATA] = true;
    this.selectAllItems[this.WORKFLOW] = true;
    this.selectAllItems[this.USER_ROLE] = true;
    this.selectAllItems[this.COMMENT] = true;
    this.selectAllItems[this.NODE_TEMPLATE] = true;
    this.selectAllItems[this.NODE_TYPE] = true;
    this.selectAllItems[this.NOTIFICATION] = true;
    this.selectAllItems[this.PUBLIC_REVIEW] = true;
    this.selectAllItems[this.PACING_GUIDE] = true;
    this.selectAllItems[this.WEBSITE_NOTES] = true;


    this.userRoleRight();

    this.activatedRoute.params.subscribe((params: Params) => {

      console.log('ngOnInit activatedRoute');
      if (params['id'] !== undefined) {
        this.role_id = params['id'];
        this.projectAdministrator = params['name'];

        this.sharedService.setTitleEvent.next({
          type: 'team',
          title: params['name'] + '-' + 'Permission'
        });

      } else {
        const path = this.commonService.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_TEAM;
        this.router.navigate([path]);

      }
    });

    this.loadingData = true;
    const url = GlobalSettings.GET_PERMISSION + '' + this.role_id;
    this.service.getServiceData(url).then((res: any) => {
      this.rolePermissions = res.role_permissions;
      this.systemPermissions = res.system_permissions;

      // Project permissions
      this.initPermissionData(this.systemPermissions.project_permissions, this.rolePermissions.project_permissions, this.PROJECT);

      // Taxonomy permissions
      this.initPermissionData(this.systemPermissions.taxonomy_permissions, this.rolePermissions.taxonomy_permissions, this.TAXONOMY);

      // Metadata permissions
      this.initPermissionData(this.systemPermissions.metadata_permissions, this.rolePermissions.metadata_permissions, this.METADATA);

      // Workflow permissions
      this.initPermissionData(this.systemPermissions.workflow_permissions, this.rolePermissions.workflow_permissions, this.WORKFLOW);

      // User & Role management permissions
      this.initPermissionData(this.systemPermissions.role_user_permissions, this.rolePermissions.role_user_permissions, this.USER_ROLE);

      // Comment permissions
      this.initPermissionData(this.systemPermissions.comment_permissions, this.rolePermissions.comment_permissions, this.COMMENT);

      // Node template permissions
      this.initPermissionData(this.systemPermissions.node_template_permissions, this.rolePermissions.node_template_permissions,
        this.NODE_TEMPLATE);

      // Node type permissions
      this.initPermissionData(this.systemPermissions.node_type_permissions, this.rolePermissions.node_type_permissions, this.NODE_TYPE);

      // Notification permissions
      this.initPermissionData(this.systemPermissions.notification_permissions, this.rolePermissions.notification_permissions,
        this.NOTIFICATION);

      // Public review permissions
      this.initPermissionData(this.systemPermissions.public_review_permissions, this.rolePermissions.public_review_permissions,
        this.PUBLIC_REVIEW);

      // Pacing guide permissions
      this.initPermissionData(this.systemPermissions.pacing_guide_permissions, this.rolePermissions.pacing_guide_permissions,
        this.PACING_GUIDE);

      // Website notes permissions
      this.initPermissionData(this.systemPermissions.note_permissions, this.rolePermissions.note_permissions, this.WEBSITE_NOTES);

      this.userRoleRight();
      this.loadingData = false;
    }).catch((ex) => {
      console.log(ex);
    });
    this.sharedService.faqEvent.next({
      name: 'edit_permission'
    });
  }

  initPermissionData(systemPermissions, rolePermissions, type) {
    const permissionData = [];
    Rx.from(systemPermissions)
      .subscribe((d: any) => {
        Rx.from(rolePermissions)
          .pipe(find((e: any) => e.permission_id === d.permission_id))
          .subscribe(data => {
            if (data !== undefined) {
              d['checked'] = true;
              permissionData.push(d);
              this.permission_set.push(d.permission_id);
              this.submitButton = true;
            } else {
              d['checked'] = false;
              permissionData.push(d);
              this.selectAllItems[type] = false;
            }
          });
      });


    this.permissionListContainer[type] = permissionData;
    if (this.permissionListContainer[type].length === 0) {
      this.permissionListContainer[type] = systemPermissions;
    }
  }

  /**
   * On selection of individual permission of each block like 'project', 'taxonomy' etc.
   * @param eventData (event emitter data)
   * @param type (block type for which permissions are setting)
   */
  onPermissionSelected(eventData, type) {
    this.checkPermission(eventData, this.permissionListContainer[type], type);
  }

  /**
   * On select all permission of each block
   * @param eventData (event emitter data)
   * @param type (block type for which all permissions are setting)
   */
  onAllPermissionSelected(eventData, type) {
    this.permissionListContainer[type] = this.checkAllPermission(eventData, this.permissionListContainer[type], type);
  }

  /**
   * To manipulate individual permission selection value
   * @param eventData
   * @param list (permission list which holds permission values of a block)
   * @param type (block type, e.g. 'projects', 'taxonomy', 'metadata' etc.)
   */
  checkPermission(eventData, list, type) {
    const event = eventData.event,
      permissionId = eventData.permission.permission_id,
      dataObj = eventData.permission,
      rolIndex = eventData.index,
      tempObj = {};
    for (let i = 0; i < list.length; i++) {
      tempObj[i] = list[i];
    }
    if (event.target.checked === true) {
      this.permission_set.push(permissionId);
      list[rolIndex]['checked'] = true;
      if (dataObj.parent_permission_id !== '') {
        this.permission_set.push(dataObj.parent_permission_id);
        for (let i = 0; i < Object.keys(tempObj).length; i++) {
          if (dataObj.parent_permission_id === (tempObj[i]['permission_id'])) {
            list[i]['checked'] = true;
          }
        }
      }
    } else {
      list[rolIndex]['checked'] = false;
      this.permission_set.splice(this.permission_set.indexOf(permissionId), 1);
    }
    if (this.permission_set.length > 0) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
    }
    this.isAllPermissionSelected(list, type);
  }

  isAllPermissionSelected(list, type) {
    Rx.from(list)
      .pipe(find((e: any) => e.checked === false))
      .subscribe(data => {
        if (data === undefined) {
          this.selectAllItems[type] = true;
        } else {
          this.selectAllItems[type] = false;
        }
      });
  }

  /**
   * To manipulate individual permission selection value
   * @param eventData
   * @param list (permission list to be manipulated)
   * @param type (block type, e.g. 'projects', 'taxonomy', 'metadata' etc.)
   */
  checkAllPermission(eventData, list, type) {
    const event = eventData.event;
    this.selectAllItems[type] = eventData.data;
    let mainList = []; // total permission list which holds permission values of a block
    switch (type) {
      case this.PROJECT:
        mainList = this.systemPermissions.project_permissions;
        break;
      case this.TAXONOMY:
        mainList = this.systemPermissions.taxonomy_permissions;
        break;
      case this.METADATA:
        mainList = this.systemPermissions.metadata_permissions;
        break;
      case this.WORKFLOW:
        mainList = this.systemPermissions.workflow_permissions;
        break;
      case this.USER_ROLE:
        mainList = this.systemPermissions.role_user_permissions;
        break;
      case this.COMMENT:
        mainList = this.systemPermissions.comment_permissions;
        break;
      case this.NODE_TEMPLATE:
        mainList = this.systemPermissions.node_template_permissions;
        break;
      case this.NODE_TYPE:
        mainList = this.systemPermissions.node_type_permissions;
        break;
      case this.NOTIFICATION:
        mainList = this.systemPermissions.notification_permissions;
        break;
      case this.PUBLIC_REVIEW:
        mainList = this.systemPermissions.public_review_permissions;
        break;
      case this.PACING_GUIDE:
        mainList = this.systemPermissions.pacing_guide_permissions;
        break;
      case this.WEBSITE_NOTES:
        mainList = this.systemPermissions.note_permissions;
        break;
      default:
        break;
    }
    if (event.target.checked === true) {
      list = [];
      this.submitButton = true;
      mainList.forEach(element => {
        element['checked'] = true;
        list.push(element);
        this.permission_set.push(element.permission_id);
      });
      this.permission_set = _.uniqBy(this.permission_set, function (permissionId) {
        return permissionId;
      });
    } else {
      list.forEach(element => {
        element['checked'] = false;
        this.permission_set.splice(this.permission_set.indexOf(element.permission_id), 1);
      });
    }
    return list;
  }

  onSubmit() {
    if (this.permission_set.length > 0) {
      this.submitButton = true;
      this.showLoader = true;
      const tempObj = {
        permission_set: this.permission_set.join(',')
      };
      const url = GlobalSettings.SET_PERMISSION + '' + this.role_id;
      this.service.postService(url, tempObj).then((res: any) => {
        this.showLoader = false;
        // this.sharedService.sucessEvent.next({
        //   type: 'update_permissions'
        // });
        this.getUserPermission();
      }).catch(ex => {
        console.log('create role', ex);
      });

    } else {
      this.submitButton = false;
    }
  }


  getUserPermission() {
    const url = GlobalSettings.GET_PERMISSION + '' + localStorage.getItem('role_id');
    this.service.getServiceData(url).then((res: any) => {
      // console.log(' getUerPemfsfsfsfs ', res);
      localStorage.setItem('permissions', JSON.stringify(res));
      this.service.role_user_permissions = res.role_permissions.role_user_permissions;
      this.service.project_permissions = res.role_permissions.project_permissions;
      this.service.taxonomy_permissions = res.role_permissions.taxonomy_permissions;
      this.service.workflow_permissions = res.role_permissions.workflow_permissions;
      this.service.metadata_permissions = res.role_permissions.metadata_permissions;
      this.service.note_permissions = res.role_permissions.note_permissions;
      this.service.comment_permissions = res.role_permissions.comment_permissions;
      this.service.node_template_permissions = res.role_permissions.node_template_permissions;
      this.service.node_type_permissions = res.role_permissions.node_type_permissions;
      this.service.notification_permissions = res.role_permissions.notification_permissions;
      this.sharedService.sucessEvent.next({
        type: 'update_permissions'
      });
      const path = this.commonService.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_TEAM;
      this.router.navigate([path]);
    });
  }

  userRoleRight() {
    /*if (this.AuthenticateUser.AuthenticateUserRole('Set Permission')) {
      this.setPermissionButton = true;
    }*/
    this.setPermissionButton = true;
  }

  backToRole() {
    const path = this.commonService.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_TEAM;
    this.router.navigate([path]);
  }

  navigateToTeam() {
    const path = this.commonService.getOrgDetails() + '/' + Utils.ROUTE_ADMIN_TEAM;
    this.router.navigate([path]);
  }
}
