import {
  NgModule,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  AdministrationRoutingModule
} from './administration-routing.module';
import {
  WorkflowListComponent
} from './workflow/workflowlist/workflowlist.component';
import {
  WorkflowComponent
} from './workflow/workflow/workflow.component';
import {
  PermissionsComponent
} from './team/permissions/permissions.component';
import {
  TeamDetailComponent
} from '../project/team-details/team-details.component';
import {
  RoleListingComponent
} from './team/role-listing/role-listing.component';
import {
  NodeTemplateComponent
} from './node/node-template/node-template.component';
import {
  MetadataListingComponent
} from './metadata/metadata-listing/metadata-listing.component';
import {
  MetadataSetComponent
} from './metadata/metadata-set/metadata-set.component';
import {
  UserListingComponent
} from './team/user-listing/user-listing.component';
import {
  CreateWorkflowComponent
} from './workflow/create-workflow/create-workflow.component';
import {
  DragulaModule,
  DragulaService
} from 'ng2-dragula';
// import {
//   AutoCompleteComponent
// } from '../common/auto-complete/auto-complete.component';

import {
  Shared2Module
} from '../shared/shared2/shared2.module';

import { Shared3Module } from '../shared/shared3/shared3.module';
import { CaseServersComponent } from './case-server/case-servers/case-servers.component';
import { HomepageBlocksComponent } from './case-server/homepage-blocks/homepage-blocks.component';
import { DocumentUploadComponent } from './case-server/document-upload/document-upload.component';
import { SharedModule } from '../shared/shared.module';
// import { TuiModule } from 'ngx-tui-editor';
import { NgxEditorModule } from 'ngx-editor';
import { PublicModule } from '../shared/public.module';
import { PermissionItemComponent } from './team/permissions/permission-item/permission-item.component';
import { ReportsComponent } from './jasper-reports/reports/reports.component';
import { CreateTenantComponent } from './case-server/tenants/create-tenant/create-tenant.component';
import { TenantListComponent } from './case-server/tenants/tenant-list/tenant-list.component';
import { AssociationPresetsComponent } from './metadata/association-presets/association-presets.component';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';
@NgModule({
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Shared2Module,
    Shared3Module,
    SharedModule,
    // TuiModule
    NgxEditorModule,
    PublicModule,
    PreLoaderModule
  ],
  declarations: [RoleListingComponent,
    WorkflowListComponent,
    WorkflowComponent,
    PermissionsComponent,
    MetadataListingComponent,
    MetadataSetComponent,
    UserListingComponent,
    CreateWorkflowComponent,
    // AutoCompleteComponent,
    CaseServersComponent,
    HomepageBlocksComponent,
    DocumentUploadComponent,
    PermissionItemComponent,
    ReportsComponent,
    CreateTenantComponent,
    TenantListComponent,
    AssociationPresetsComponent
  ],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AdministrationModule { }
