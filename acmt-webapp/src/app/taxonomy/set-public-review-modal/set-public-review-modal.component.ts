import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {
  format
} from 'date-fns';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-set-public-review-modal',
  templateUrl: './set-public-review-modal.component.html'
})
export class SetPublicReviewModalComponent implements OnInit, OnChanges {
  startDate: any;
  endDate: any;
  selectedDateType = 'with_end_date';
  form: FormGroup;
  tempTaxonomyTitle: any;

  @Input() taxonomyTitle;

  @Output() setTaxonomyEvent = new EventEmitter <any> ();
  @Output() modalCancelEvent = new EventEmitter <any> ();

  public dateTimeRange: Date[];

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      selectedDateType: new FormControl(null, [Validators.required]),
      dateRange: new FormControl(null, [Validators.required])
    });
  }

  ngOnChanges() {
    this.tempTaxonomyTitle = this.taxonomyTitle;
  }

  onCancel() {
    this.form.reset();
    this.taxonomyTitle = this.tempTaxonomyTitle;
    this.resetData();
    this.modalCancelEvent.emit();
  }

  getVal(event) {
    if (this.selectedDateType === 'with_end_date') {
      this.startDate = event.value[0] ? format(event.value[0], 'YYYY-MM-DD HH:mm:ss') : '';
      this.endDate = event.value[1] ? format(event.value[1], 'YYYY-MM-DD HH:mm:ss') : '';
    } else {
      this.startDate = event.value ? format(event.value, 'YYYY-MM-DD HH:mm:ss') : '';
      this.endDate = '';
    }
  }

  onToggleOption() {
    this.resetDateRange();
  }

  resetDateRange() {
    this.dateTimeRange = undefined;
    this.startDate = undefined;
    this.endDate = undefined;
  }
  resetData() {
    this.selectedDateType = 'with_end_date';
    if (this.form) {
      if (this.form.controls['name']) {
        this.form.controls['name'].setValue(this.taxonomyTitle);
      }
      if (this.form.controls['selectedDateType']) {
        this.form.controls['selectedDateType'].setValue('with_end_date');
      }
    }
    this.resetDateRange();
  }
  setTaxonomyForPublicReview() {
    this.setTaxonomyEvent.emit({
      start: this.startDate,
      end: this.endDate
    });
    setTimeout(() => {
      this.onCancel();
    }, 500);
  }
}
