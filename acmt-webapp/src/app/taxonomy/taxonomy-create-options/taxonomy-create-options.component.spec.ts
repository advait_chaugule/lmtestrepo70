import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyCreateOptionsComponent } from './taxonomy-create-options.component';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { CommonService } from 'src/app/common.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedService } from 'src/app/shared.service';

describe('TaxonomyCreateOptionsComponent', () => {
  let component: TaxonomyCreateOptionsComponent;
  let fixture: ComponentFixture<TaxonomyCreateOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxonomyCreateOptionsComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [AuthenticateUserService, CommonService, ConfirmationDialogService, SharedService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomyCreateOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create TaxonomyCreateOptionsComponent', () => {
    expect(component).toBeTruthy();
  });
});
