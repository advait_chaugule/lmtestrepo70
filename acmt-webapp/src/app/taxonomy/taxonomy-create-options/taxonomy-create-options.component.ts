import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-taxonomy-create-options',
  templateUrl: './taxonomy-create-options.component.html',
  styleUrls: ['./taxonomy-create-options.component.scss']
})
export class TaxonomyCreateOptionsComponent implements OnInit {

  @Input() currentOption = 'import';
  @Output() selectedOption: EventEmitter<any>;
  private importTaxonomy: boolean;
  @Input() steps = 'Step 1 of 3';
  @Input() isCreateClicked = false;
  @Input() options = [{
    title: 'Import files',
    subTitle: 'Import taxonomy standards like CASE',
    value: 'import'
  }, {
    title: 'Build your own',
    subTitle: 'Use the taxonomy builder and build from scratch',
    value: 'build'
  }];

  constructor(private AuthenticateUser: AuthenticateUserService, private sharedService: SharedService) {
    this.selectedOption = new EventEmitter<any>();
  }

  ngOnInit() {
    try {
      this.userRoleRight();
      // this.selectedOption.emit(this.currentOption);
      this.onSelection(this.currentOption);
    } catch (err) { }
    // this.currentOption = 'import';
    /*if (localStorage.getItem('selectedOption')) {
      this.selectedOption = localStorage.getItem('selectedOption');
    }*/
  }

  onSelection(type) {
    this.currentOption = type;
    this.selectedOption.emit(type);
    if (this.isCreateClicked) {
      this.sharedService.faqEvent.next({
        name: type + '_taxonomy'
      });
    }
    console.log('onSelection  ', type);
  }

  onContinueClick() {

    /*switch (this.selectedOption) {
      case 'import':
        this.router.navigate(['/app/importtaxonomies']);
        break;
      case 'build':
        this.router.navigate(['/app/buildtaxonomy']);
        break;
      default:
    }*/
  }

  userRoleRight() {
    try {
      if (this.AuthenticateUser.authenticTaxonomy('Import Taxonomy')) {
        this.importTaxonomy = true;
      }
    } catch (err) { }
  }
}
