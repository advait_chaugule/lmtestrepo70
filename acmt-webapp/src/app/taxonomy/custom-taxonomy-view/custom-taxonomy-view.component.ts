import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  OnDestroy,
  ElementRef
} from '@angular/core';
import {
  ActivatedRoute,
  Params
} from '@angular/router';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  GlobalSettings
} from '../../global.settings';
import Utils from '../../utils';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  CommonService
} from '../../common.service';
import {
  SharedService
} from '../../shared.service';

@Component({
  selector: 'app-custom-taxonomy-view',
  templateUrl: './custom-taxonomy-view.component.html',
  styleUrls: ['./custom-taxonomy-view.component.scss']
})
export class CustomTaxonomyViewComponent implements OnInit, OnDestroy, AfterViewInit {
  acivatedRouteEvent: Subscription;
  nodeSelectedEvent: Subscription;
  taxonomyId;
  taxonomyData = null;
  taxonomyTitle = null;
  loadingReport = true;
  itemDetail = null;
  selectedNode;
  viewMore = false;
  @ViewChild('reportElement', { static: false }) reportElementDiv: ElementRef;
  view_element: any;
  constructor(private acivatedRoute: ActivatedRoute,
    private treeService: TreeDataService,
    private service: CommonService,
    private sharedService: SharedService) {
    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      if (item) {
        if (item.id) {
          this.onTreeNodeSelected(item);
          this.getSelectedTreeItemDetail(item.id, item['is_document']);
        }
      }
    });
  }

  ngOnInit() {
    this.acivatedRouteEvent = this.acivatedRoute.params.subscribe((params: Params) => {
      if (params['id'] !== undefined) {
        this.taxonomyId = params['id'];
        localStorage.setItem('taxonomy_id', this.taxonomyId);
      }
    });

  }

  ngAfterViewInit() {
    customTaxonomyHeightCal();
    // Utils.initResizablePanel();
    this.getTaxonomyData();

    document.getElementById('custom-view-rightPanel').style.overflow = 'hidden';
  }
  getTaxonomyData() {
    if (this.taxonomyId) {
      this.taxonomyData = null;
      const url = GlobalSettings.GET_TREE_VIEW_ENHANCED + this.taxonomyId + '?is_customview=1';
      this.treeService.getTreeData(url, true, Utils.EXPAND_LEVEL).then((response: any) => {
        const res = response.parsedTreeNodes;
        if (res && res.children) {
          Utils.sortData(res.children);

          // code to check if node allow_expand !==yes then remove its all children so in UI it will show bullet
          try {
            res.children[0].children.forEach(element => {
              if (element.custom_view_data) {
                if (JSON.parse(element.custom_view_data).allow_expand !== 'yes') {
                  element.children = [];
                }
              }
            });

          } catch (ex) {
            console.log('Exception ', ex);
          }
          this.taxonomyData = res;
          if (res.children.length > 0) {
            this.taxonomyTitle = res.children[0].full_statement;
            this.sharedService.setTitleEvent.next({
              'type': 'taxonomy',
              'title': Utils.extractContentFromHtml(this.taxonomyTitle),
              'document_id': this.taxonomyId
            });
            if (this.taxonomyData.children[0].children[0]) {
              this.selectedNode = this.taxonomyData.children[0].children[0];
              setTimeout(() => {
                if (document.getElementById(this.selectedNode['id'] + '-node')) {
                  document.getElementById(this.selectedNode['id'] + '-node').click();
                }
              }, 500);
            }
          }
        }
        customTaxonomyHeightCal();
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });
    }
  }

  onTreeNodeSelected(item) {
    this.getReport(item.id);
  }

  getReport(itemId) {
    if (this.reportElementDiv && this.reportElementDiv.nativeElement) {
      this.loadingReport = true;
      this.viewMore = false;
      this.view_element = this.reportElementDiv.nativeElement;
      this.view_element.innerHTML = '';

      const URL = GlobalSettings.CUSTOM_TAXONOMY_REPORT_URL + this.taxonomyId + '/' + itemId + '?t=' + (new Date()).getTime();
      const self = this;
      this.service.downloadServiceData(URL, 'HTML').then((res: any) => {
        const fragment = document.createRange().createContextualFragment(res);
        this.view_element.appendChild(fragment);
        this.loadingReport = false;
        setTimeout(() => {
          customTaxonomyHeightCal();
        }, 500);
        // this.reportElementDiv.nativeElement.querySelector('td[class]').style.backgroundColor = "red";
        const elements = this.reportElementDiv.nativeElement.querySelectorAll('.CustomView');

        elements.forEach(element => {
          element.style.cursor = 'pointer';

          element.addEventListener('click', function (event) {
            if (navigator.userAgent.indexOf('Edge') >= 0) {
              if (event.currentTarget.id) {
                self.getSelectedTreeItemDetail(event.currentTarget.id, 0);
              }
            } else {
              if (event.path && event.path.length > 0 && event.path[0].localName === 'span') {
                self.getSelectedTreeItemDetail(event.path[1].id, 0);
              } else {
                self.getSelectedTreeItemDetail(event.target.id, 0);
              }
            }

            elements.forEach(el => {
              // el.style.backgroundColor = 'white';
              el.classList.remove('active');
              /* el.style.cursor = 'hand';
               if (el.children && el.children.length > 0) {
                 el.children[0].classList.remove('active');
               }*/
            });
            element.classList.add('active');
            // event.target.style.backgroundColor = '#3d5afe';

          });
        });
      }).catch((ex) => {
        this.loadingReport = false;
        console.log('getJasperReport  ', ex);
      });
    }
  }

  getSelectedTreeItemDetail(item_id, is_document) {
    let url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + item_id;
    if (is_document === 1) {
      url = GlobalSettings.GET_CFDOC_ITEM_DETAIL + '/' + item_id;
    } else {
      url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + item_id;
    }
    this.itemDetail = null;
    this.service.getServiceData(url).then((res: any) => {
      console.log(' getSelectedTreeItemDetail ', res);
      this.itemDetail = res;
      setTimeout(() => {
        customTaxonomyHeightCal();
      }, 500);
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
    });
  }

  ngOnDestroy() {
    if (this.acivatedRouteEvent) {
      this.acivatedRouteEvent.unsubscribe();
    }
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    Utils.removeBodyScrollClass();
    document.removeEventListener('mousemove', null, false);
  }

  closeDetails() {
    const element = this.reportElementDiv.nativeElement;
    element.classList.remove('col-lg-7');
    element.classList.add('col-lg-10');
    Utils.addBodyScrollClass();
    if (document.getElementById('right-panel')) {
      document.getElementById('right-panel').style.right = '-35%';
    }
  }

  /* condition for association show/hide
   * show all associations except isChildOf relation and description='customView' */
  isShowAssociation(association) {
    let show = false;
    if (association.association_type_name && association.association_type_name !== 'isChildOf') {
      show = true;
    } else if (association.association_type.display_name !== 'isChildOf') {
      show = true;
    }
    // If description is 'customView', hide that association
    if (association.description && association.description.trim() === 'customView') {
      show = false;
    }
    return show;
  }

  onViewMoreClick(value) {

    this.viewMore = value;
    setTimeout(() => {
      customTaxonomyHeightCal();
    }, 10);

  }
  ondragStart() {
    console.log('Hello');
    customTaxonomyHeightCal();
  }
}
