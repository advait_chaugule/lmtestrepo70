import {
  Component,
  OnInit,
  Input,
  ViewChild,
  AfterViewInit,
  OnDestroy

} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  SharedService
} from '../../shared.service';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  ImportTaxonomyComponent
} from '../import-taxonomy/import-taxonomy.component';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import {
  BuildTaxonomyComponent
} from '../build-taxonomy/build-taxonomy.component';
import {
  count
} from 'rxjs/operator/count';
import Utils from '../../utils';
import {
  SortListComponent
} from '../../common/sort-list/sort-list.component';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  FilterListComponent
} from '../../common/filter-list/filter-list.component';
import {
  TableConfigurationComponent
} from 'src/app/common/table-configuration/table-configuration.component';
import { TaxonomyGroupsComponent } from '../taxonomy-groups/taxonomy-groups.component';
import { DataTableComponent } from 'src/app/common/data-table/data-table.component';

const DRAFT_TAXONOMY = 'Draft';
const PUBLIC_REVIEW_TAXONOMY = 'Public Review';
const PUBLISHED_TAXONOMY = 'Published';

@Component({
  selector: 'app-taxonomy-list',
  templateUrl: './taxonomy-list.component.html',
  styleUrls: ['./taxonomy-list.component.scss', '../taxonomy-create-options/taxonomy-create-options.component.scss']
})
export class TaxonomyListComponent implements OnInit, AfterViewInit, OnDestroy {
  FILTER_BY_LABEL = 'Filter by';
  SORT_BY_LABEL = 'Sort by';
  JSON_TYPE = 'CASE JSON';
  CSV_TYPE = 'CSV';

  @Input() taxonomyList = [];
  steps = [false, true, true];
  currentStep = 0;
  currentOption = 'import';
  continueDisabled = false;
  proceedDisabled = false;
  uploadFinished = false; // indicates whether uploading is finished or not for CSV
  children_taxonomies_count = 0;
  associations_count = 0;
  taxonomy_count = 0;
  createTaxonomyButton: boolean;
  viewTaxonomyButton: boolean;
  taxonomyListButton: boolean;
  taxonomyDeleteButton: boolean;
  taxonomyEditButton = false;
  searchText: boolean;
  type = 'taxonomylist';
  showProtip = false;
  resetTree = 0;
  saveTaxonomy = false;
  selectedNodeTemplate = [];
  selectTemplateName = '';
  emptyFields = false;
  isFirstTime = true;
  // selectedSortOption = '--Select--';
  selectedFileType = this.JSON_TYPE;
  modalHeader = 'Create New Taxonomy';
  updated_node_count = 0;
  blank_updated_node_count = 0;
  updated_metadata_count = 0;
  blank_metadata_count = 0;
  new_metadata_count = 0;
  updatedItems = [];
  updatedMetadata = [];
  errorData = []; // to display error report for CSV import in summary
  document: any = {
    'document_id': null,
    'document_title': null
  };
  searchTrigger = false;
  searchResult = [];
  minItemPerPage;
  editTaxonomy = false;
  document_id = '';
  startIndex;
  lastIndex;
  createdOrUpdated;
  new_node_count;
  delete_node_count;
  viewType = 'listview';
  permissions = {};
  timerStopped = false; // To track whether timer is stopped for JSON file upload
  showSummaryDetails = true; // to show summary details or to show alert message for JSON file upload
  errorInUpload = false;
  totalTaxonomyCountToUpload = 0;
  fileUpload = false; // check if upload service has been triggered, used to change text of continue button
  taxonomyTypeList = []; // holds list of taxonomy type
  groupsLoading = false; // loading state for taxonomy groups fetching from API response
  // ----------------for Common Data table----------------- //
  listOfColumn = [];
  defaultColumnList;
  currentColumnList;

  columnsForError = [{ // table columns for CSV error report
    name: 'Error',
    propName: 'Error',
    class: '',
    type: 'text'
  },
  {
    name: 'Row',
    propName: 'Row',
    class: '',
    type: 'text'
  }];


  sortOptions = [{
    'type': 'title',
    'isDate': false,
    'isAsce': true,
    'display': 'Name (A-Z)'
  }, {
    'type': 'title',
    'isDate': false,
    'isAsce': false,
    'display': 'Name (Z-A)'
  }, {
    'type': 'created_at',
    'isDate': true,
    'isAsce': true,
    'display': 'Active Since (Ascending)'
  }, {
    'type': 'created_at',
    'isDate': true,
    'isAsce': false,
    'display': 'Active Since (Descending)'
  }, {
    'type': 'associated_projects_count',
    'isDate': false,
    'isAsce': true,
    'display': 'Project Count (Low - High)'
  }, {
    'type': 'associated_projects_count',
    'isDate': false,
    'isAsce': false,
    'display': 'Project Count (High - Low)'
  }];

  filterByOptions = [{
    name: 'All',
    value: 'All'
  }, {
    name: DRAFT_TAXONOMY,
    value: DRAFT_TAXONOMY
  },
  {
    name: PUBLISHED_TAXONOMY,
    value: PUBLISHED_TAXONOMY // 3
  }
  ];
  visibleFilterByOptions = [];
  selectedFilterOption = null;
  taxonomyType: any; // holds value based on routing url param
  optionList = [];
  STATUS_DRAFT = Utils.STATUS_DRAFT;
  STATUS_PUBLIC_REVIEW = Utils.STATUS_PUBLIC_REVIEW;
  STATUS_PUBLISHED = Utils.STATUS_PUBLISHED;
  filteredTaxonomyList;
  nodeOrdered = false;
  listOfDetails = [];
  dropdownObject = {};
  filterType = '';
  defaultSortByColumn: any;
  defaultFilterOptions = [];
  pageDetail: any;
  userDefault: any; // holds flag value of user selection if user has set current configured table as default
  allUserDefault: any; // holds fag value of user selection if user has set current configured table as default for all
  docNodetypeId: any; // ID of Document node type for metadata set
  additionalMetadataList = []; // list of additional columns
  canConfirureTable = false; // holds permission if user can configure table column list
  canSetDefaultForAll = false; // holds permission if user can set default list view for all
  userSelection: any = {}; // holds user-default and all-user-default flag values
  currentDataSet: any; // current dataset for taxonomy-list view
  tableConfigured = false;
  isCopy = false; // holds additional options to append or append with child nodes
  isCopyAssociation = false; // holds additional options to append or append with child nodes
  taxoToBeCpoied: any; // holds the taxonomy to be copied
  copyTaxonomyName = ''; // holds the name of copied taxonomy
  showGroupSection = false; // to display copy set button with toggling (hiding) taxonomy create button section
  groupManagePermission = false;
  checkedTaxonomyIds = [];
  selectedTaxonomies = [];
  taxonomyGroups = [];
  firstTime: any;
  dropdownData: any = {
    uniqueId: 'metadata_id',
    propName: 'name'
  };
  enableSaveButton = false;
  tableViewList = [
    {
      name: 'Default',
      code_name: 'default',
      isActive: false,
      showTable: true
    }, {
      name: 'Custom',
      code_name: 'configured',
      isActive: false,
      showTable: false
    }
  ];
  uploadType = 'Desktop';
  // tslint:disable-next-line:max-line-length
  @ViewChild('taxonomyBuilder', {
    static: false
  }) taxonomyBuilder: BuildTaxonomyComponent;
  @ViewChild('importComponent', {
    static: false
  }) importComponent: ImportTaxonomyComponent;
  @ViewChild('sortListRef', {
    static: false
  }) sortListRef: SortListComponent;
  @ViewChild('configTableRef', {
    static: false
  }) configTableRef: TableConfigurationComponent;
  @ViewChild('grpTaxonomy', { static: false }) grpTaxonomy: TaxonomyGroupsComponent;
  @ViewChild('dataTable', { static: false }) dataTable: DataTableComponent;

  searchResultEvent: Subscription;
  position;
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  loaderOnDelete = false; // holds boolean to determine is delete action is triggered on taxonomy
  activateSessionBoolean = false; // holds boolean to determine whether to keep session active
  isCreateClicked = false;
  loadConfiguration = false;
  searchingText;
  filterData = [];
  filterDataEvent: Subscription;
  defaultList = [];
  constructor(
    private service: CommonService,
    private AuthenticateUser: AuthenticateUserService,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    private router: Router,
    private acivatedRoute: ActivatedRoute,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService*/
  ) {
    this.minItemPerPage = Utils.PAGINATION_LOWER_LIMIT;
    this.searchResultEvent = this.sharedService.searchResultEvent.subscribe((item: any) => {
      if (this.isFirstTime) {
        this.isFirstTime = false;
        return;
      }
      if (!this.isFirstTime) {
        if (item) {
          this.searchResult = item['searchData'];
          if (item['showSearchList']) {
            this.searchTrigger = true;
          } else {
            this.searchTrigger = false;
          }
        }
      }
      // console.log('Item received', JSON.stringify(this.searchResult));

    });
    this.acivatedRoute.queryParams.subscribe((params) => {
      if (params['taxonomyType'] !== undefined) {
        if (params['taxonomyType'].toLowerCase().trim() === 'published' &&
          this.AuthenticateUser.authenticTaxonomy('View Published Taxonomies')) {
          this.taxonomyType = PUBLISHED_TAXONOMY;
        } else {
          this.taxonomyType = null;
        }
      }
    });

    this.acivatedRoute.params.subscribe((param: any) => {
      if (param.type) {
        if (param.type === 'taxonomies_created_by_me') {
          this.filterType = 'created_by_me';
        }
        if (param.type === 'published_taxonomies') {
          this.filterType = 'published_taxonomies';
        }
      }
    });

    this.sharedService.searchTextEvent.subscribe((res: any) => {
      this.searchingText = res.text;
    });

    this.filterDataEvent = this.sharedService.filterDataTable.subscribe((res: any) => {
      this.filterData = res.data;
    });
  }

  ngOnInit() {
    this.searchingText = '';
    this.position = 3;
    this.currentStep = 0;
    this.firstTime = localStorage.getItem('taxonomy_list_first_time');
    this.userRoleRight();
    this.getTaxonomyGroups(); // getting taxonomy groups

    if (document.querySelector('body')) {
      const element = document.querySelector('body');
      element.classList.remove('hide-vScroll');
    }
    this.getTableConfig();

    Utils.removeShowClass();
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('taxonomy_list'));*/
    this.sharedService.faqEvent.next({
      name: 'taxonomy_list'
    });


  }

  ngOnDestroy() {
    if (this.filterDataEvent) {
      this.filterDataEvent.unsubscribe();
    }
  }

  setDefaultPagination() {
    this.pageDetail = {
      start: 0,
      last: Utils.PAGINATION_LOWER_LIMIT,
      pageIndex: 1,
      itemPerPage: Utils.PAGINATION_LOWER_LIMIT
    };
    this.startIndex = 0;
    this.lastIndex = Utils.PAGINATION_LOWER_LIMIT;
  }
  setPagination() {
    if (localStorage.getItem('taxonomy_list_page_state')) {
      const pageDetail = JSON.parse(localStorage.getItem('taxonomy_list_page_state')).pageDetail;
      this.pageDetail = pageDetail;
    }
  }

  setFilterList() {
    if (localStorage.getItem('taxonomy_list_page_state')) {

      const filterOptions = JSON.parse(localStorage.getItem('taxonomy_list_page_state')).filterList;
      this.defaultFilterOptions = [];
      for (const item of filterOptions) {
        this.defaultFilterOptions.push({
          propName: 'label',
          value: item.label
        });
      }
    }
  }

  ngAfterViewInit() {
    // init_rippleEffect();
  }

  onCreateNewTaxonomy() {
    console.log('onCreateNewTaxonomy ');
    this.onCancleClick();
    this.currentStep = 0;
    this.continueDisabled = false;
    this.proceedDisabled = false;
    this.uploadFinished = false;
    this.saveTaxonomy = false;
    this.isCreateClicked = true;
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'create_taxonomy'
    });
  }

  listTaxonomies() {
    // After fetching taxonomy type list, call taxonomy list api
    this.getDictionaryMetadata();
  }

  callTaxonomyListApi() {
    this.searchText = true;
    const url = GlobalSettings.TAXONOMY_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.taxonomyList = [];
      if (res && res.length > 0) {
        res.forEach(element => {
          if (element.status !== Utils.STATUS_PUBLIC_REVIEW_COPIED) {
            if (element.adoption_status === Utils.STATUS_PUBLIC_REVIEW) {
              element.status = Utils.STATUS_PUBLIC_REVIEW;
            }
            this.taxonomyList.push(element);
          }
        });
        this.taxonomyList.forEach(item => {
          item.current_status = item.status === 5 ? PUBLIC_REVIEW_TAXONOMY :
            (item.status === 3 ? PUBLISHED_TAXONOMY : DRAFT_TAXONOMY);
          item.taxonomy_type = this.getTypeNameByValue(item.document_type);
          item.isEditable = item.actual_import_type === 1 ? false : this.taxonomyEditButton;
          item.isDelete = this.taxonomyDeleteButton;
          item.showOptions = item.status === 3 ? false : (!item.isEditable && !item.isDelete) ? false : true;
          item.imported_as = Utils.getImportType(item.actual_import_type, item.source_type_id);
          item.showCreator = true;
          item.showDetail = true;
          item.canCopy = true;
          item.isAccessible = true;
          item.groupNames = [];
          if (item.groups && item.groups.length) { // taxonomy groups
            item.groups.forEach((grp: any) => {
              item.groupNames.push(grp.group_name);
            });
          }
          item.groupNames = item.groupNames.toString().replace(/,/gi, ', '); // replacing ',' with ', '
          item.groupNames = item.groupNames ? item.groupNames : '-'; // - means empty
        });
        this.setFilterList();

        if (this.taxonomyList.length > 0) {
          this.searchText = false;
          this.showProtip = false;
        } else {
          this.searchText = true;
          this.showProtip = true;
        }
        // this.sortListRef.sortDefalut();
        // this.filteredTaxonomyList = this.taxonomyList;
        if (this.viewType === 'listview') {
          this.filteredList(this.taxonomyList);
        }
        if (this.taxonomyType) { // 'taxonomyType' holds value based on routing param
          // If it holds value, clear this param value by routing again
          const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_TAXONOMY_LIST;
          this.router.navigate([path]);
        } else {
          setTimeout(() => {
            if (this.sortListRef) {
              this.sortListRef.sortDefalut(this.sortOptions[3].display);
            }
          }, 75);
        }
        if (this.filterType && this.filterType.length > 0) {
          this.filterTableData(this.filterType);
        }
      } else if (res && res.length === 0) {
        this.filteredTaxonomyList = res;
      }
    }).catch(ex => {
      console.log('list of taxonomies ex ', ex);
    });
  }

  filteredList(data) {
    this.filteredTaxonomyList = data;
    if (this.filteredTaxonomyList.length > 0 && this.filteredTaxonomyList[0]['status'] === 3) {
      const item = this.listOfColumn.find((col: any) => col.name === 'Last Change');
      if (item) {
        item.name = 'Published On';
      }
    } else {
      const item = this.listOfColumn.find((col: any) => col.name === 'Published On');
      if (item) {
        item.name = 'Last Change';
      }
    }
  }


  onTaxonomySelected(item) {
    console.log('onTaxonomySelected ', item, this.viewTaxonomyButton);
    if (this.viewTaxonomyButton) {
      this.sharedService.setTitleEvent.next({
        type: 'taxonomy',
        title: Utils.extractContentFromHtml(item.title),
        title_html: item['title_html'],
        document_id: item.document_id,
        source_document_id: item['source_document_id']
      });
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_TAXONOMY_DETAIL;
      this.router.navigate([path, item.document_id]);
    }

  }

  searchTaxonomies(search) {

    if (search.length > 2) {
      // const url = GlobalSettings.TAXONOMY_SEARCH + search;
      // this.service.getServiceData(url).then((res: any) => {
      //   this.taxonomyList = res;
      // }).catch(ex => {
      //   console.log('list of taxonomies ex ', ex);
      // });
      this.sharedService.setSearchKeyword.next({
        type: 'search',
        keyword: search
      });
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_SEARCH;
      this.router.navigate([path]);

    } else {
      this.listTaxonomies();
    }
    /* var params = {};
     params.Authorization = sessionService.get('access_token');
     // params.exclude_role_ids = angular.isDefined($scope.exclude_role_ids) ? JSON.stringify($scope.exclude_role_ids) : '[]';
     var deferred = $q.defer();

     $http({
       method: 'GET',
       headers: params,
       url: serviceEndPoint + 'api/v1/taxonomy/filter/title?search_key=' + search,
       params: params
     }).success(function (data, status, headers, config) {
       console.log(data);
       deferred.resolve(data); //Return the roles list
     }).error(function (data, status, headers, config) {
       // console.log('Not Working');
       $rootScope.unauthorised_redirection(data.status);
     });
     return deferred.promise;*/
  }

  changeView(type) {
    this.viewType = type;
    this.setPagination();
    if (this.viewType === 'cardview') {
      this.sortByCardView();
    }
  }

  // ----------------for Common Data table----------------- //

  onOptionClicked(item) {
    console.log(item.event);
    switch (item.clickedOn) {
      case 'editTaxonomy':
        this.editTaxanomy(item.event, item.data);
        break;
      case 'delTaxonomy':
        this.deleteTaxanomy(item.data);
        break;
      case 'showTaxoDetails':
        this.setSourceTaxonomyDetails(item.data);
        break;
      case 'copyTaxonomy':
        this.onCopyTaxonomy(item.data);
        break;
      default:
    }
  }
  onOptionSelected(option) {
    console.log('onOptoinSelected  ', option);
    this.currentOption = option;
    this.continueDisabled = false;
    this.proceedDisabled = false;
    this.uploadFinished = false;
  }

  onFileUploadEvent(event) {
    console.log('onFileUploadEvent  ', event);
    if (event.type === 'start') {
      this.continueDisabled = true;
      this.proceedDisabled = true;
      this.uploadFinished = false;
    } else {
      this.continueDisabled = !event.successUpload;
      this.proceedDisabled = !event.successUpload;
      this.uploadFinished = event.successUpload;
      this.uploadType = event.uploadType;
      if (this.timerStopped) {
        /* If timer is stopped and still files are not uploaded in time, then redirect the user automatically to third step by
          clicking on continue button. So if event.uploadedInTime return true, that means files are uploaded in time
          and we don't need to redirect automatically. Just continue button will get enabled and summary will be visible
          after clicking continue button on third step. */
        this.showSummaryDetails = event.uploadedInTime;
        this.errorInUpload = this.importComponent.isAllFilesInError();
        if (!this.showSummaryDetails) {
          // Redirecting to third step automatically
          this.onContinueClick();
        }
      }
    }

    if (event.filetype !== 'CSV') {
      this.children_taxonomies_count = event.taxonomies_count;
      this.associations_count = event.associations_count;
      this.taxonomy_count = event.taxonomy_count;
      this.totalTaxonomyCountToUpload = event.totalTaxonomyInUploadState;
      this.document.document_id = event.document_id ? event.document_id : null;
      this.document.document_title = event.document_title ? event.document_title : null;
    } else {
      this.updated_node_count = event.updated_node_count;
      this.blank_updated_node_count = event.blank_updated_node_count;
      this.updated_metadata_count = event.updated_metadata_count;
      this.blank_metadata_count = event.blank_metadata_count;
      this.new_metadata_count = event.new_metadata_count;
      this.new_node_count = event.new_node_count;
      this.createdOrUpdated = event.createdOrUpdated;
      this.delete_node_count = event.delete_node_count;
      this.associations_count = event.associations_count;
      this.updatedItems = [];
      this.updatedMetadata = event.updatedMetadata;
      this.errorData = event.errorData;
      this.totalTaxonomyCountToUpload = event.totalTaxonomyInUploadState;
      if (this.createdOrUpdated === 'Taxonomy Created') {
        this.modalHeader = 'Create New Taxonomy';
      } else {
        this.modalHeader = 'Update Taxonomy';
      }
      this.updatedItems = event.updatedItems;
    }

  }

  onFileTypeEvent(event) {
    this.selectedFileType = event;
    if (this.selectedFileType === this.CSV_TYPE) {
      this.modalHeader = 'Update Taxonomy';
    } else {
      this.modalHeader = 'Create New Taxonomy';
    }
  }

  onCancleClick() {
    this.fileUpload = false;
    this.steps = [false, true, true];
    if (this.importComponent) {
      this.importComponent.clearFiles();
      this.importComponent.resetForm();
      this.importComponent.setDefalutSelection();
    }
    this.listTaxonomies();
    if (this.resetTree === 0) {
      this.resetTree = 1;
    } else {
      this.resetTree = 0;
    }
    this.currentOption = 'import';
    this.onFileTypeEvent(this.JSON_TYPE);
    this.updated_node_count = 0;
    this.blank_updated_node_count = 0;
    this.updated_metadata_count = 0;
    this.blank_metadata_count = 0;
    this.new_metadata_count = 0;
    this.totalTaxonomyCountToUpload = 0;
    this.updatedItems = [];
    this.updatedMetadata = [];
    this.errorData = [];
    this.document = {
      'document_id': null,
      'document_title': null
    };
    this.nodeOrdered = false;
    this.showSummaryDetails = true;
    this.errorInUpload = false;
    this.timerStopped = false;
    this.isCreateClicked = false;
    this.sharedService.faqEvent.next({
      name: 'taxonomy_list'
    });
  }
  onContinueClick() {
    if (this.currentStep === 0 && this.currentOption !== 'build') {
      this.fileUpload = true;
    } else {
      this.fileUpload = false;
    }
    if ((this.currentStep === 1) && this.importComponent &&
      !this.importComponent.isAllFilesUploaded() && this.showSummaryDetails) {
      // Stopping to go next step from step 2 to 3 for JSON file upload on continue click
      this.continueDisabled = true;
      this.importComponent.startUploadProcessForAllFiles();
      if (this.uploadType === 'URL' || this.selectedFileType === this.CSV_TYPE) {
        this.currentStep = 3;
        this.showSummaryDetails = false;
        this.errorInUpload = false;
        this.steps[1] = true;
        this.steps[2] = false;
      }
      return;
    }
    this.currentStep++;
    let index = this.steps.indexOf(false);

    if (index === 2) {
      console.log('Index 2');
    } else {
      this.steps[index] = true;
      index++;
      this.steps[index] = false;
    }

    if (this.currentStep === 1) {

      if (this.currentOption === 'import') {
        if (this.importComponent) {
          this.importComponent.clearFiles();
        }
        this.continueDisabled = true;
      } else {
        if (this.taxonomyBuilder) {
          this.taxonomyBuilder.cleartaxonomy();
        }
        this.continueDisabled = true;
      }
    }
    if (this.currentStep === 2) {
      if (this.selectedFileType === this.CSV_TYPE) {
        this.uploadFinished = false;
      } else {
        this.uploadFinished = true;
      }
      if (this.taxonomyBuilder) {
        this.taxonomyBuilder.treeInitialize();
        this.taxonomyBuilder.saveDocument();
        setTimeout(() => {
          if (this.taxonomyBuilder.form) {
            this.taxonomyBuilder.form.reset();
          }
        }, 800);
      }
    }
  }

  onTryAgainClick() {
    this.steps = [true, false, true];
    this.currentStep = 1;
    this.saveTaxonomy = false;
    this.timerStopped = false;
    if (this.importComponent) {
      this.importComponent.onFileTypeSelected();
    }
    if (this.currentStep === 1) {
      if (this.currentOption === 'import') {
        if (this.importComponent) {
          this.importComponent.clearFiles();
        }
        this.continueDisabled = true;
      } else {
        if (this.taxonomyBuilder) {
          this.taxonomyBuilder.cleartaxonomy();
        }
      }
    }
  }

  onProceedWithImportedData() {
    this.importComponent.onConfirmClicked(0);
  }

  userRoleRight() {
    this.visibleFilterByOptions = [];
    this.filterByOptions.forEach(data => {
      this.visibleFilterByOptions.push(data);
    });
    if (this.AuthenticateUser.authenticTaxonomy('Create Taxonomy')) {
      this.createTaxonomyButton = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('Import Taxonomy')) {
      this.createTaxonomyButton = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('View Taxonomy Hierarchy')) {
      this.viewTaxonomyButton = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('View Draft Taxonomies')) {
      this.taxonomyListButton = true;
    } else {
      this.visibleFilterByOptions = this.removeObjectFromArray(DRAFT_TAXONOMY, this.visibleFilterByOptions, 'name');
    }
    if (this.AuthenticateUser.authenticTaxonomy('Delete Taxonomy')) {
      this.taxonomyDeleteButton = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('Edit Taxonomy')) {
      this.taxonomyEditButton = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('Update taxonomy list view')) {
      this.canConfirureTable = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('Admin setup for taxonomy list view')) {
      this.canSetDefaultForAll = true;
    }
    if (this.AuthenticateUser.authenticTaxonomy('View Published Taxonomies')) {
      this.taxonomyListButton = true;
    } else {
      this.visibleFilterByOptions = this.removeObjectFromArray(PUBLISHED_TAXONOMY, this.visibleFilterByOptions, 'name');
    }
    if (this.taxonomyType) { // 'taxonomyType' holds value based on routing url param
      this.selectedFilterOption = this.taxonomyType;
    } else {
      this.selectedFilterOption = (this.visibleFilterByOptions.length > 0) ? this.visibleFilterByOptions[0].name : null;
    }
    const self = this;
    this.AuthenticateUser.authenticatePublicReview('View Active Public Review Taxonomies', function (val) {
      if (val) {
        self.visibleFilterByOptions.push({
          name: PUBLIC_REVIEW_TAXONOMY,
          value: PUBLIC_REVIEW_TAXONOMY // 5
        });
      }
    });

    if (this.AuthenticateUser.authenticTaxonomy('Manage group')) {
      this.groupManagePermission = true;
    }

    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit',
      type: 'editTaxonomy',
      value: this.taxonomyEditButton,
      modal: '#createTaxonomy',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'delTaxonomy',
      value: this.taxonomyDeleteButton,
      modal: '',
      check: 'isDelete'
    },
    {
      name: 'Source taxonomy details',
      type: 'showTaxoDetails',
      value: true,
      modal: '#show-details',
      check: 'showDetail'
    },
    {
      name: 'Create a copy',
      type: 'copyTaxonomy',
      value: true,
      modal: '#associationSettings',
      check: 'canCopy'
    }
    ];
    this.listTaxonomies();
  }

  onClose() {
    this.listTaxonomies();
  }


  deleteTaxanomy(data) {
    if (data.status === 5) {
      this.sharedService.sucessEvent.next({
        type: 'prevent_taxonomy_delete'
      });
    } else {
      const url = GlobalSettings.CHECK_DELETE_TAXONOMY + '/' + data.document_id;
      this.service.getServiceData(url).then((res: any) => {
        if (res.projects.length > 0) {
          this.sharedService.sucessEvent.next({
            type: 'delete_taxonomy_permission',
            count: res.projects.length
          });
        } else {
          this.dialogService.confirm('Confirm', 'Are you sure you want to delete this taxonomy ?')
            .then((confirmed) => {
              if (confirmed) {
                this.loaderOnDelete = true;
                const urlDelete = GlobalSettings.DELETE_TAXONOMY + '/' + data.document_id;
                this.service.deleteServiceData(urlDelete).then((resDelete: any) => {
                  this.checkPagination();
                  this.loaderOnDelete = false;
                  this.sharedService.sucessEvent.next({
                    type: 'delete_taxanomy'
                  });
                }).catch((ex) => {
                  this.loaderOnDelete = false;
                  // this.sharedService.sucessEvent.next({
                  //   type: 'delete_taxanomy_failure'
                  // });
                  console.log('Error while deleting the User ', ex);
                });
              } else {
                console.log('User cancel the dialog');
              }
            })
            .catch(() => {
              console.log('User dismissed the dialog');
            });

        }
      }).catch((ex) => {
        console.log('Error while deleting the User ', ex);
      });
    }
  }

  checkPagination() {
    let taxonomy_list_page_state = {};
    if (localStorage.getItem('taxonomy_list_page_state')) {
      taxonomy_list_page_state = JSON.parse(localStorage.getItem('taxonomy_list_page_state'));
      const pageDetail = JSON.parse(localStorage.getItem('taxonomy_list_page_state')).pageDetail;
      this.pageDetail = pageDetail;
    }
    const totalCount = this.taxonomyList && this.taxonomyList.length ? this.taxonomyList.length : null;
    let modulas = 0;
    if (this.pageDetail) {
      modulas = totalCount % Number(this.pageDetail.itemPerPage);
    }
    if (this.pageDetail && this.pageDetail['pageIndex'] === this.pageDetail['maxPages'] && modulas === 1) {
      taxonomy_list_page_state['pageDetail']['last'] = this.pageDetail['last'] - 1;
      taxonomy_list_page_state['pageDetail']['pageIndex'] = this.pageDetail['pageIndex'] - 1;
      taxonomy_list_page_state['pageDetail']['maxPages'] = this.pageDetail['maxPages'] - 1;
      localStorage.removeItem('taxonomy_list_page_state');
      localStorage.setItem('taxonomy_list_page_state', JSON.stringify(taxonomy_list_page_state));
    } else {
      this.setPagination();
    }
    this.listTaxonomies();
  }

  editTaxanomy(event, taxonomy) {
    this.modalHeader = 'Update Taxonomy';
    if (taxonomy.status === 5) {
      event.stopPropagation();
      event.preventDefault();
      this.sharedService.sucessEvent.next({
        type: 'prevent_taxonomy_edit'
      });
    } else {
      this.editTaxonomy = true;
      this.steps = [true, true, true];
      this.currentStep = 2;
      this.currentOption = 'edit';
      this.document_id = taxonomy['document_id'];
      this.resetTree = 2;
    }
    this.sharedService.faqEvent.next({
      name: 'build_taxonomy'
    });
  }


  onTaxonomyBuildCancelEvent() {
    console.log('onTaxonomyBuildCancelEvent ');
    this.steps = [false, true, true];
    this.currentStep = 0;
    if (this.taxonomyBuilder) {
      this.taxonomyBuilder.cleartaxonomy();
    }
    this.editTaxonomy = false;
    this.saveTaxonomy = false;
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 600);
  }

  saveTaxonomyEvent() {
    console.log('Taxonomy created event fired');
    if (this.taxonomyBuilder) {
      this.taxonomyBuilder.savetaxonomy();
    }
    if (!this.emptyFields) {
      document.getElementById('closeButton').click();
      this.saveTaxonomy = false;
      this.listTaxonomies();
    }
  }
  onTaxonomyUpdateEvent(val) {
    console.log('Taxonomy created event fired', val);
    this.saveTaxonomy = val;
  }

  onTamplatedSelected(data) {
    this.selectedNodeTemplate = data.filter(function (elem) {
      return elem.title !== 'Document';
    });
  }

  /* --------- Check empty fields in build taxonomy event capture functionality start --------- */
  emptyFieldsAlert(event) {
    this.emptyFields = event;
  }
  /* --------- Check empty fields in build taxonomy event capture functionality end --------- */


  /* --------- functionality on alert close start ---------*/
  onCloseAlert() {
    this.emptyFields = false;
  }
  /* --------- functionality on alert close end ---------*/

  /* --------- functionality to add class and remove class on focus and blur for individual card layout start ---------*/
  onFocus(event, index) {
    const element = document.getElementById('card-' + index);
    element.classList.add('tn-focus');

  }

  onBlur(event, index) {
    const element = document.getElementById('card-' + index);
    element.classList.remove('tn-focus');

  }
  // sortData(event, type, isDate, isAsce) {
  //   console.log('sort event on ... ', event.target.innerHTML);
  //   this.selectedSortOption = event.target.innerHTML;
  //   return Utils.sortDataArray(this.taxonomyList, type, isDate, isAsce);
  // }
  /* --------- functionality to add class and remove class on focus and blur for individual card layout end ---------*/

  onFinishClick() {
    // Navigating to taxonomy details page only for single file upload
    if (this.totalTaxonomyCountToUpload === 1 && this.importComponent.isAllFilesUploaded() && this.document.document_id) {
      const item = {
        'title': this.document.document_title,
        'document_id': this.document.document_id
      };
      this.onTaxonomySelected(item);
    }
    if (this.importComponent) {
      this.importComponent.closeTaxonomyBuildModal();
    }
    this.onCancleClick();
  }

  // changeDateToLocalTimeZone(date) {
  //   const newDate = new Date(date + ' UTC');
  //   return newDate.toString();
  // }
  changeDateToLocalTimeZone(date) {
    return Utils.changeDateToLocalTimeZone(date);
  }
  showDataInRange(event) {
    this.startIndex = event.start - 1;
    this.lastIndex = event.last;
    if (this.viewType === 'cardview') {
      localStorage.setItem('taxonomy_list_page_state', JSON.stringify({ pageDetail: event }));
    }
    // localStorage.setItem('pagination_details', JSON.stringify(event));
  }

  removeObjectFromArray(value: any, list: any, prop: string) {
    if (list && list.length > 0) {
      list = list.filter(function (item) {
        return (item[prop] !== value);
      });
    }
    return list;
  }

  onNodeReordered(event) {
    this.nodeOrdered = event.reArrenged;
  }

  setSourceTaxonomyDetails(data) {
    console.log(data);
    this.listOfDetails = [{
      title: 'Creator',
      value: data.creator,
      type: 'text'
    },
    {
      title: 'Taxonomy Name',
      value: data.title,
      type: 'text'
    },
    {
      title: 'GUID',
      value: data.source_document_id,
      type: 'text'
    },
    {
      title: 'URI',
      value: data.uri,
      type: 'link'
    }
    ];

  }

  onTimerStopped(event: any) {
    if (event['stopped']) {
      this.timerStopped = true;
    } else {
      this.timerStopped = false;
    }
  }

  filterTableData(type) {
    const myName = localStorage.getItem('first_name') + ' ' + localStorage.getItem('last_name');
    this.defaultFilterOptions = [];
    if (this.taxonomyList.length) {
      if (type === 'created_by_me') {
        // this.taxonomyList = this.taxonomyList.filter((proj: any) => proj.imported_by === myName);
        this.defaultFilterOptions = [{
          propName: 'label',
          value: myName
        }];
      }
    }
  }

  getChanges(event) {
    console.log('change events', event.pageIndex);
    if (event.pageDetail && event.pageDetail['pageIndex'] > event.pageDetail['maxPages']) {
      if (document.getElementById('previousPage')) {
        document.getElementById('previousPage').click();
      }
    } else {
      localStorage.setItem('taxonomy_list_page_state', JSON.stringify(event));
    }
  }

  setListView(isDefault: boolean) {
    this.listOfColumn = [];
    const list = JSON.parse(localStorage.getItem('taxonomy_list'));
    const tableConfigured = JSON.parse(localStorage.getItem('tableConfigured'));
    if (isDefault) {
      if (this.currentColumnList.is_user_default || this.currentColumnList.default_for_all_user) {
        this.currentDataSet = JSON.parse(JSON.stringify(this.currentColumnList));
        this.loadTable('configured');
      } else {
        this.currentDataSet = JSON.parse(JSON.stringify(this.defaultColumnList));
        this.loadTable('default');
      }
    } else {
      this.currentDataSet = JSON.parse(JSON.stringify(tableConfigured ? this.currentColumnList : this.defaultColumnList));
      this.loadTable(tableConfigured ? 'configured' : 'default');
    }
    this.listOfColumn = tableConfigured && !this.currentDataSet.is_user_default ? list : this.currentDataSet.taxonomy_list_setting;
  }

  saveChanges() {
    this.configTableRef.saveChanges();
  }

  updateListView(configVal) {
    this.tableConfigured = true;
    this.listOfColumn = configVal.columnList;
    this.userDefault = configVal.userDefault;
    this.allUserDefault = configVal.allUserDefault;
    localStorage.setItem('tableConfigured', JSON.stringify(this.tableConfigured));
    localStorage.setItem('taxonomy_list', JSON.stringify(configVal.columnList));
    this.setTableConfig(configVal.columnList);
  }

  // To fetch 'Dictionary' type metadata's field values list
  // After fetching taxonomy types, call project list api to set taxonomy type for each taxonomy
  getDictionaryMetadata() {
    if (localStorage.getItem('access_token')) {
      this.taxonomyTypeList = [];
      // For getting any metadata's field values with internal name, we have to send any random id as per backend logic
      const url = GlobalSettings.GET_METADATA_LIST + '/random_id' + '?internal_name=Taxonomy Type';
      this.service.getServiceData(url).then((res: any) => {
        if (res && res.length > 0) {
          this.taxonomyTypeList = res;
        }
        this.callTaxonomyListApi();
      }).catch((ex) => {
        this.taxonomyTypeList = [];
        this.taxonomyList = [];
        console.log('Error caught while fetching dictionary type metadata', ex);
      });
    }
  }

  // To get field type name of Taxonomy type against value
  getTypeNameByValue(value: any) {
    if (this.taxonomyTypeList.length) {
      const index = this.taxonomyTypeList.findIndex(element => {
        return element.key === value;
      });
      if (index > -1) { // If key value found
        return this.taxonomyTypeList[index].value;
      } else { // If not found
        return '';
      }
    } else {
      return '';
    }
  }
  sortByCardView() {
    this.defaultSortByColumn = JSON.parse(localStorage.getItem('taxonomy_list_page_state')).lastSortBy;
    this.sortOptions.forEach(data => {
      if (this.defaultSortByColumn.propName.trim() === data.type.trim()) {
        const index = this.sortOptions.indexOf(data);
        this.position = this.defaultSortByColumn.isAsc === true ? index - 1 : index;
      }
    });
  }
  sortByListView(event) {
    this.listOfColumn.forEach(element => {
      if (event.type === element.propName) {
        this.defaultSortByColumn.isAsc = event.isAsce;
        this.defaultSortByColumn.name = element.name;
      }
    });
  }

  cancelOptionsToCopy() {
    this.isCopy = false;
    this.isCopyAssociation = false;
  }

  change() {
    console.log(this.isCopy);
  }

  onCopyTaxonomy(data) {
    this.taxoToBeCpoied = data;
    this.document_id = data.document_id;
    this.copyTaxonomyName = data.title;
  }

  createCopy() {
    const url = GlobalSettings.COPY_TAXONOMY;
    const body = {
      documentId: this.document_id,
      taxonomyName: this.copyTaxonomyName,
      isCopy: this.isCopy,
      isCopyAsso: this.isCopyAssociation
    };
    this.service.postService(url, body, 0, true).then((res: any) => {
      this.sharedService.sucessEvent.next({
        customMsg: res.message.join(' '),
        type: res.success ? 'success' : 'error'
      });
      this.callTaxonomyListApi();
      this.cancelOptionsToCopy();
    }).catch((ex) => {
      console.log('ERROR ##>>', ex);
    });
  }

  /******* TAXONOMY GROUP FUNCTIONALITY STARTS *******/
  // On table row's checkbox clicked
  onRowChecked(event: any) {
    this.selectedTaxonomies = JSON.parse(JSON.stringify(event.data));
    this.checkedTaxonomyIds = [];
    if (event.data && event.data.length) { // If any rows selected, then show group section and hide create taxonomy section
      this.showGroupSection = true;
      this.storeTaxonomyIds();
    } else {
      this.showGroupSection = false;
    }
  }

  // On group name click from dropdown, copy selected taxonomy to the group
  onGroupNameClick(grp: any) {
    if (this.grpTaxonomy) {
      this.grpTaxonomy.setName = grp.group_name; // setting as selected group name
      this.grpTaxonomy.setAction('c'); // copy action
      this.grpTaxonomy.onTaxonomyCopyToGroup(grp.group_id); // copy taxonomy api calling
    }
  }

  // On create set option click, get and pass selected taxonomy ids
  onCreateSetOptionClick() {
    if (this.grpTaxonomy) {
      this.grpTaxonomy.setName = '';
      this.grpTaxonomy.setAction('c'); // copy action
    }
    setTimeout(() => {
      if (document.getElementById('closecreateSetModal')) {
        document.getElementById('closecreateSetModal').focus();
      }
    }, 700);
  }

  // On manage set option click
  onManageSetOptionClick() {
    this.grpTaxonomy.setAction('m'); // manage action
    setTimeout(() => {
      if (document.getElementById('closemanageSetModal')) {
        document.getElementById('closemanageSetModal').focus();
      }
    }, 700);
  }

  storeTaxonomyIds() {
    // this.checkedTaxonomyIds = [];
    this.selectedTaxonomies.forEach(elem => {
      this.checkedTaxonomyIds.push(elem.document_id);
    });
  }

  /**
   * To get all taxonomy groups
   */
  getTaxonomyGroups() {
    this.taxonomyGroups = [];
    this.groupsLoading = true;
    const url = GlobalSettings.TAXONOMY_GROUP_URL;
    this.service.getServiceData(url).then((res: any) => {
      this.taxonomyGroups = res;
      if (this.taxonomyGroups.length) {
        Utils.sortDataArray(this.taxonomyGroups, 'group_name', false, true);
      }
      this.groupsLoading = false;
    }).catch((ex: any) => {
      console.log(ex);
      this.groupsLoading = false;
    });
  }

  onTaxonomyGroupAction(event: any) {
    if ((event.type === 'cancel' || event.type === 'create' || event.type === 'copy') && this.dataTable) {
      this.dataTable.clearAllSelection();
    }
    if (event.type === 'create') { // on group create, call group list api
      this.getTaxonomyGroups();
    }
    if (event.type === 'update' || event.type === 'delete' || event.type === 'copy') {
      // on group update, taxonomy copy, group delete, call taxonomy list api
      this.listTaxonomies();
      if (event.type === 'copy') { // only on taxonomy copy with group create, call group list api
        this.getTaxonomyGroups();
      }
    }
  }
  /******* TAXONOMY GROUP FUNCTIONALITY ENDS *******/

  caseServerChangeEvent(evt) {
    this.fileUpload = evt;
  }

  getTableConfig() {
    const featureId = 1;
    const settingId = 1;
    const url = GlobalSettings.TABLE_CONFIG + '/' + featureId + '/' + settingId;

    this.service.getServiceData(url, true, true).then((res: any) => {
      console.log('DP', res);
      this.defaultColumnList = res.data.super_default_json;
      this.currentColumnList = res.data.json_config_value;
      this.docNodetypeId = res.data.document_node_type_id;
      this.userSelection = {
        userDefault: res.data.is_user_default,
        allUserDefault: res.data.default_for_all_user
      };
      this.constructColumnListData(this.defaultColumnList.taxonomy_list_setting, true);
      this.constructColumnListData(this.currentColumnList.taxonomy_list_setting, false);
      this.defaultList = JSON.parse(JSON.stringify(this.defaultColumnList.taxonomy_list_setting));
      const index = this.defaultColumnList.taxonomy_list_setting.findIndex(data => !data.is_default);
      this.defaultColumnList.taxonomy_list_setting.splice(index, 1);
      if (this.currentColumnList.taxonomy_list_setting.length) {
        const configTable = this.tableViewList.find(table => table.code_name === 'configured');
        configTable.showTable = true;
      }
      const firstTime = localStorage.getItem('taxonomy_list_first_time');
      if (!firstTime) {
        this.setListView(true);
        this.defaultSortByColumn = this.listOfColumn[4];
        this.setDefaultPagination();
        localStorage.setItem('taxonomy_list_first_time', '1');
      } else {
        this.setListView(false);
        this.setPagination();
        this.setFilterList();
        this.defaultSortByColumn = JSON.parse(localStorage.getItem('taxonomy_list_page_state')).lastSortBy;
      }
      this.sharedService.taxonomyColumnListEvent.next({
        columnList: this.listOfColumn,
        defaultColumnList: this.defaultColumnList.taxonomy_list_setting,
        hasPermission: this.canConfirureTable
      });
      this.getDocMetadatList();
    }).catch((ex) => {

    });
  }

  constructColumnListData(columnList: any, setWidth: any) {
    let defaultColWidth;
    if (setWidth) {
      const tableWidth = document.getElementById('dataTable').offsetWidth;
      const optionColWidth = document.getElementById('optionColumn').offsetWidth;
      const dataColWidth = tableWidth - optionColWidth;
      const noOfCol = columnList.length;
      defaultColWidth = (dataColWidth / noOfCol);
    }

    columnList.forEach(col => {
      col.name = col.display_name;
      if (setWidth) {
        col.width = Math.round(defaultColWidth);
      }

      switch (col.display_name) {
        case 'Name':
          col.propName = 'title';
          col.class = '';
          col.type = 'link';
          col.redirect = true;
          col.loadPage = true;
          col.canRemove = false;
          break;
        case 'Type':
          col.propName = 'taxonomy_type';
          col.class = ' px-1';
          col.type = 'text';
          col.canRemove = false;
          col.canFilter = true;
          break;
        case 'Status':
          col.propName = 'current_status';
          col.class = ' px-1';
          col.type = 'text';
          col.canRemove = false;
          col.canFilter = true;
          break;
        case 'Projects':
          col.propName = 'associated_projects_count';
          col.class = ' px-1';
          col.type = 'text';
          col.canRemove = false;
          break;
        case 'Last Change':
          col.propName = 'last_active_date';
          col.class = ' px-1';
          col.type = 'date';
          col.dateType = 'amCalendar';
          col.canRemove = false;
          break;
        case 'Active since':
          col.propName = 'created_at';
          col.class = ' px-1';
          col.type = 'date';
          col.dateType = 'amCalendar';
          col.canRemove = false;
          break;
        case 'Import type':
          col.propName = 'imported_as';
          col.class = ' px-1';
          col.type = 'text';
          col.canFilter = true;
          col.canRemove = false;
          break;
        case 'Imported by':
          col.propName = 'imported_by';
          col.class = ' px-1 created-by';
          col.type = 'text';
          col.canFilter = true;
          col.canRemove = false;
          break;
        case 'Group':
          col.propName = 'groupNames';
          col.class = ' px-1';
          col.type = 'text';
          col.canRemove = true;
          col.canFilter = true;
          break;
        case 'Source Type':
          col.propName = 'source_type';
          col.class = ' px-1';
          col.type = 'text';
          col.canRemove = true;
          col.canFilter = true;
          break;
        default:
      }
    });
  }

  setTableConfig(list) {
    const url = GlobalSettings.TABLE_CONFIG;
    const body = {
      feature_id: 1,
      setting_id: 1,
      json_config_value: {
        version: 1,
        is_user_default: this.userDefault,
        default_for_all_user: this.allUserDefault,
        taxonomy_list_setting: list
      }
    };
    this.service.putService(url, body, 0, true).then((res: any) => {
      console.log(res);
      if (res.success) {
        this.getTableConfig();
        this.setListView(false);
        this.callTaxonomyListApi();
        this.sharedService.sucessEvent.next({
          type: 'success',
          customMsg: res.message
        });
      }
    }).catch((ex) => {
      console.log(ex);
    });
  }

  setToDefaultList(val: any) {
    // this.listOfColumn = this.defaultColumnList;
    this.setListView(true);

  }

  getDocMetadatList() {
    this.additionalMetadataList = [];
    const url = GlobalSettings.SAVE_NODETYPE_METADATA + '/' + this.docNodetypeId;
    this.service.getServiceData(url).then((res: any) => {
      const idx = res.findIndex((col: any) => col.name === 'Title');
      if (idx > -1) {
        res.splice(idx, 1);
      }
      this.additionalMetadataList = this.defaultList.concat(res);
    }).catch((ex) => {
    });

  }

  checkSaveValidation(event) {
    this.enableSaveButton = event;

  }

  clearData() {
    this.loadConfiguration = false;
    this.configTableRef.cancelChanges();
  }

  resetModal() {
    this.loadConfiguration = true;
    this.currentDataSet = this.currentColumnList.taxonomy_list_setting.length ? this.currentColumnList : this.defaultColumnList;
    this.enableSaveButton = this.currentDataSet.is_user_default || this.currentDataSet.default_for_all_user;
  }

  loadTable(val) {
    if (val === 'default') {
      this.currentDataSet = JSON.parse(JSON.stringify(this.defaultColumnList));
    } else {
      this.currentDataSet = JSON.parse(JSON.stringify(this.currentColumnList));
    }
    this.listOfColumn = JSON.parse(JSON.stringify(this.currentDataSet.taxonomy_list_setting));
    this.tableViewList.forEach((table: any) => {
      if (table.code_name === val) {
        table.isActive = true;
      } else {
        table.isActive = false;
      }
    });
  }

  activateSession(evt) {
    this.activateSessionBoolean = evt;
    const url = GlobalSettings.REFRESH_TOCKEN;
    if (this.activateSessionBoolean) {
      const self = this;
      var refreshId = setInterval(function () {
        self.service.putService(url, {}).then((res: any) => {
          console.log('Notification Count ');
        }).catch((ex) => {
          console.log('Notification Count Ex', ex);
        });
        if (!self.activateSessionBoolean) {
          clearInterval(refreshId);
        }
      }, 300000);
    }
  }

  deactivateSession() {
    this.activateSessionBoolean = false;
  }

  getColumnFilterObj() {
    const obj = {};
    obj['title'] = this.searchingText;
    return obj;
  }

}
