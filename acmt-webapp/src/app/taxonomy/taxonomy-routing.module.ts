import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {
  TaxonomyListComponent
} from './taxonomy-list/taxonomy-list.component';
import {
  TaxonomyDetailComponent
} from './taxonomy-detail/taxonomy-detail.component';
import { CustomTaxonomyViewComponent } from './custom-taxonomy-view/custom-taxonomy-view.component';
const routes: Routes = [{
  path: 'list',
  component: TaxonomyListComponent
}, {
  path: 'list/:type',
  component: TaxonomyListComponent
}, {
  path: 'detail/:id',
  component: TaxonomyDetailComponent
}, {
  path: 'detail/:id/:parentId/:itemId',
  component: TaxonomyDetailComponent
}, {
  path: 'detail/:id/:itemId',
  component: TaxonomyDetailComponent
}, {
  path: 'detail/:id/:parentId/:itemId/:location',
  component: TaxonomyDetailComponent
}, {
  path: 'customview/:id',
  component: CustomTaxonomyViewComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxonomyRoutingModule { }
