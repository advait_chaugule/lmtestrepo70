import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyGroupsComponent } from './taxonomy-groups.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('TaxonomyGroupsComponent', () => {
  let component: TaxonomyGroupsComponent;
  let fixture: ComponentFixture<TaxonomyGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxonomyGroupsComponent],
      imports: [HttpClientModule, FormsModule, ReactiveFormsModule, RouterTestingModule],
      providers: [SharedService, CommonService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomyGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create TaxonomyGroupsComponent', () => {
    expect(component).toBeTruthy();
  });
});
