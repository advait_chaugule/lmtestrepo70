import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { GlobalSettings } from 'src/app/global.settings';
import Utils from 'src/app/utils';

@Component({
  selector: 'app-taxonomy-groups',
  templateUrl: './taxonomy-groups.component.html',
  styleUrls: ['./taxonomy-groups.component.scss']
})
export class TaxonomyGroupsComponent implements OnInit {

  GROUP_RENAME_OPERATION = 'rename';
  TAXO_GROUP_COPY_OPERATION = 'copy';
  MANAGE_GROUP_OPERATION = 'manage';
  CREATE_SET_MODAL_ID = 'createSetModal';
  MANAGE_SET_MODAL_ID = 'manageSetModal';

  @Input() taxonomyIds = []; // selected taxonomy ids for which set will be created or managed. this will come from parent component
  @Input() taxonomyGroups = []; // holds user tenants' all available groups
  @Output() actionEvent: EventEmitter<any>; // event emitter for any add/update/delete operation

  createSetForm: FormGroup; // create set form group
  setName: string; // holds group name in create/update form
  setCrtLoading = false; // loading state for create/update group
  action: string; // holds action name for group copy, rename, manage
  isGroupOpened = false; // to know whether we clicked any group in manage sets for seeing its associated taxonomies name
  selectedGroup: any; // selected group
  selectedGrpTaxonomies = []; // taxonomy list for selected group
  taxonomyLoading = false; // loading state when fetching taxonomy list of a group
  deleteLoading = false; // loading state for delete

  constructor(private sharedService: SharedService, private service: CommonService) {
    this.actionEvent = new EventEmitter<any>();
  }

  ngOnInit() {
    this.createSetForm = new FormGroup({
      setName: new FormControl(null, [Validators.required])
    });
  }

  /**
   * To create any taxonomy group (or set) with or without taxonomy copied to it
   * @param copyReq (if true, then selected taxonomy will be copied to that group otherwise only group will be created)
   */
  onCreateSet(copyReq = false) {
    if (this.createSetForm.valid) {
      this.setCrtLoading = true;
      const url = GlobalSettings.TAXONOMY_GROUP_URL;
      const obj = {
        group_name: this.setName
      };
      this.service.postService(url, obj, 0, true).then((res: any) => {
        if (res.success) {
          const data = res.data[0];
          if (!copyReq) {
            this.sharedService.sucessEvent.next({
              customMsg: 'Group created successfully.'
            });
            this.onCancel();
            this.setCrtLoading = false;
            this.closeModal('create', false);
            this.actionEvent.emit({ type: 'create', data });
          } else {
            this.onTaxonomyCopyToGroup(data.group_id);
          }
        } else {
          this.setCrtLoading = false;
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: res.message ? res.message.toString() : res.message
          });
        }
      }).catch((ex) => {
        console.log('onCreateSet ex ', ex);
        this.setCrtLoading = false;
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg ? ex.msg.toString() : 'Failed to create group. Please try later.'
        });
      });
    } else {
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: 'Please fill out required fields.'
      });
    }
  }

  /**
   * To update name for any group
   */
  onUpdateSet() {
    if (this.createSetForm.valid && this.selectedGroup) {
      this.doUpdateAPICall('name');
    } else {
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: 'Please fill out required fields.'
      });
    }
  }

  /**
   * Update API calling method for name, set default changing
   * @param type ('default' for Set default, 'name' for name changing)
   */
  doUpdateAPICall(type: string) {
    const url = GlobalSettings.TAXONOMY_GROUP_URL;
    this.setCrtLoading = true;
    let obj = {};
    let msg = '';
    if (type === 'default') { // group default update
      obj = {
        group_id: this.selectedGroup.group_id,
        is_default: this.selectedGroup.is_default // (0=false / 1=true)
      };
      msg = this.selectedGroup.is_default ? 'Group is accessible to team.' : 'Group is not accessible to team.';
    } else if (type === 'name') { // group name update
      obj = {
        group_id: this.selectedGroup.group_id,
        group_name: this.setName
      };
      msg = 'Group name updated successfully.';
    }
    this.service.putService(url, obj, 0, true).then((res: any) => {
      this.setCrtLoading = false;
      if (res.success) {
        const data = res.data[0];
        if (type === 'default') {
          this.updateGroupArray(this.selectedGroup.group_id, 'is_default', this.selectedGroup.is_default);
        } else if (type === 'name') {
          this.updateGroupArray(this.selectedGroup.group_id, 'group_name', this.setName);
          this.onCancel();
          this.closeModal('create', false);
        }
        this.sharedService.sucessEvent.next({
          customMsg: msg
        });
        data.type = type;
        this.actionEvent.emit({ type: 'update', data });
      } else {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: res.message ? res.message.toString() : res.message
        });
        if (type === 'default') {
          this.selectedGroup.is_default = !this.selectedGroup.is_default;
          this.updateGroupArray(this.selectedGroup.group_id, 'is_default', this.selectedGroup.is_default);
        }
      }
    }).catch((ex) => {
      console.log('onUpdateSet ex ', ex);
      this.setCrtLoading = false;
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: ex.msg ? ex.msg.toString() : 'Failed to update group. Please try later.'
      });
      if (type === 'default') {
        this.selectedGroup.is_default = !this.selectedGroup.is_default;
        this.updateGroupArray(this.selectedGroup.group_id, 'is_default', this.selectedGroup.is_default);
      }
    });
  }

  /**
   * To copy selected taxonomies to any particular group
   * @param groupId (group id where taxonomy will be copied)
   */
  onTaxonomyCopyToGroup(groupId: any) {
    if (groupId && this.taxonomyIds) {
      const url = GlobalSettings.ADD_TAXONOMY_TO_GROUP_URL;
      const obj = {
        group_id: groupId,
        document_id: this.taxonomyIds
      };
      this.service.postService(url, obj, 0, true).then((res: any) => {
        if (res.success) {
          this.sharedService.sucessEvent.next({
            customMsg: this.setName ? ('Taxonomy copied successfully to group ' + this.setName.toUpperCase()) : res.message
          });
          this.onCancel();
          this.closeModal('create', false);
          this.actionEvent.emit({ type: 'copy', data: res.data });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: res.message ? res.message.toString() : res.message
          });
        }
        this.setCrtLoading = false;
      }).catch((ex) => {
        console.log('onTaxonomyCopyToGroup ex ', ex);
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg ? ex.msg.toString() : 'Failed to copy. Please try later.'
        });
        this.setCrtLoading = false;
      });
    }
  }

  /**
   * To get available taxonomy list for any group
   * @param groupId (group id)
   */
  getTaxonomyFromGroup(groupId: any) {
    this.selectedGrpTaxonomies = [];
    this.taxonomyLoading = true;
    const url = GlobalSettings.GET_ALL_TAXONOMY_FROM_GROUP_URL + '?group_id=' + groupId;
    this.service.getServiceData(url).then((res: any) => {
      this.selectedGrpTaxonomies = res;
      this.taxonomyLoading = false;
    }).catch((ex: any) => {
      console.log(ex);
      this.taxonomyLoading = false;
    });
  }

  /**
   * To delete any group
   * @param groupId (group id)
   * @param index (index in array)
   */
  onDeleteGroup(groupId: any, index: number) {
    if (groupId) {
      this.deleteLoading = true;
      const url = GlobalSettings.TAXONOMY_GROUP_URL + '?group_id=' + groupId;
      this.service.deleteServiceData(url, true).then((res: any) => {
        if (res.success) {
          this.taxonomyGroups.splice(index, 1);
          this.sharedService.sucessEvent.next({
            customMsg: 'Group deleted successfully.'
          });
          res.data.type = 'default';
          this.actionEvent.emit({ type: 'delete', data: res.data });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: res.message ? res.message.toString() : res.message
          });
        }
        this.deleteLoading = false;
      }).catch((ex: any) => {
        console.log(ex);
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg ? ex.msg.toString() : 'Failed to delete group. Please try later.'
        });
        this.deleteLoading = false;
      });
    } else {
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: 'Group id is not found. Please try again.'
      });
    }
  }

  // Method for selecting group to fetch its taxonomies
  onGroupSelected(selectedGrp: any) {
    this.selectedGroup = selectedGrp;
    this.isGroupOpened = true;
    this.getTaxonomyFromGroup(this.selectedGroup.group_id);
  }

  /**
   * To delete any taxonomy from a group
   * @param groupId (group id from where taxonomy will be removed)
   * @param taxonomyId (taxonomy id to be removed)
   * @param index (taxonomy index in array)
   */
  deleteTaxonomyFromGroup(groupId: any, taxonomyId: string, index: number) {
    if (groupId && taxonomyId) {
      this.deleteLoading = true;
      const url = GlobalSettings.DELETE_TAXONOMY_TO_GROUP_URL;
      const obj = {
        group_id: groupId,
        document_id: [taxonomyId]
      };
      this.service.deleteServiceData(url, true, obj).then((res: any) => {
        if (res.success) {
          this.selectedGrpTaxonomies.splice(index, 1);
          this.sharedService.sucessEvent.next({
            customMsg: 'Taxonomy removed from group successfully.'
          });
          res.data.type = 'default';
          this.actionEvent.emit({ type: 'delete', data: res.data });
        } else {
          this.sharedService.sucessEvent.next({
            type: 'error',
            customMsg: res.message ? res.message.toString() : res.message
          });
        }
        this.deleteLoading = false;
      }).catch((ex: any) => {
        console.log(ex);
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: ex.msg ? ex.msg.toString() : 'Failed to delete taxonomy. Please try later.'
        });
        this.deleteLoading = false;
      });
    } else {
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: 'Group Id or Taxonomy id is not found. Please try again.'
      });
    }
  }

  // On rename button click, set selected group, rename action
  onRenameClick(selectedGrp: any) {
    this.setAction('r');
    this.selectedGroup = selectedGrp;
    this.setName = selectedGrp.group_name;
    setTimeout(() => {
      if (document.getElementById('close' + this.CREATE_SET_MODAL_ID)) {
        document.getElementById('close' + this.CREATE_SET_MODAL_ID).focus();
      }
    }, 700);
  }

  // On default setting toggle button click
  onDefaultSetClick(selectedGrp: any) {
    selectedGrp.is_default = !selectedGrp.is_default;
    this.selectedGroup = selectedGrp;
    this.selectedGroup.is_default = this.selectedGroup.is_default ? 1 : 0; // conversion boolean to number
    if (this.selectedGroup) {
      this.doUpdateAPICall('default');
    }
  }

  // On focus group row element, select focused row group
  onFocusGroupName(selectedGrp: any) {
    this.selectedGroup = selectedGrp;
  }

  // On back button click
  onBackClick() {
    this.isGroupOpened = false;
  }

  onCancel() {
    this.setName = '';
    this.createSetForm.reset();
  }

  setAction(type: string) {
    if (type.toLowerCase() === 'r') {
      this.action = this.GROUP_RENAME_OPERATION; // rename operation
    } else if (type.toLowerCase() === 'c') {
      this.action = this.TAXO_GROUP_COPY_OPERATION; // copy taxonomy operation
    } else if (type.toLowerCase() === 'm') {
      this.action = this.MANAGE_GROUP_OPERATION; // manage group operation
    }
  }

  // Method for close modal manually
  closeModal(type: string, isReqEventEmit = true) {
    setTimeout(() => {
      if (type === 'create' && document.getElementById('close' + this.CREATE_SET_MODAL_ID)) {
        document.getElementById('close' + this.CREATE_SET_MODAL_ID).click();
      } else if (type === 'manage' && document.getElementById('close' + this.MANAGE_SET_MODAL_ID)) {
        document.getElementById('close' + this.MANAGE_SET_MODAL_ID).click();
      }
    }, 150);
    this.isGroupOpened = false;
    this.selectedGroup = {};
    if (isReqEventEmit) {
      this.actionEvent.emit({ type: 'cancel' });
    }
  }

  // Method will be called on modal cancelled
  onModalClose(event?: any) {
    this.actionEvent.emit({ type: 'cancel' });
    this.isGroupOpened = false;
    this.selectedGroup = {};
    this.onCancel();
  }

  /**
   * To update group list array with updated value of any specific property
   * @param id (unique id)
   * @param updateProp (which column proprty value needs to be updated)
   * @param updateValue (updated value)
   */
  updateGroupArray(uniqueValue: any, updateProp: any, updateValue: any) {
    Utils.updateArray(uniqueValue, this.taxonomyGroups, 'group_id', updateProp, updateValue);
  }

}
