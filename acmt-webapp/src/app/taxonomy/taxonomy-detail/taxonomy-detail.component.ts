import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  ActivatedRoute
} from '@angular/router';
import {
  SharedService
} from '../../shared.service';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  saveAs
} from 'file-saver';
import Utils from '../../utils';
/*import {
  WalktroughService
} from '../../help/walkthrough/walktrough.service';
import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';*/
import {
  NgProgress
} from 'ngx-progressbar';
import {
  Router
} from '@angular/router';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  NodeDetailsComponent
} from '../../project/node-details/node-details.component';
import {
  TreeAccordianComponent
} from '../../tree-accordian/tree-accordian.component';
import {
  from,
  Observable
} from 'rxjs';
import {
  Angular5Csv
} from 'angular5-csv/dist/Angular5-csv';
import {
  HttpClient
} from '@angular/common/http';
import {
  ItemDetailsComponent
} from '../../common/item-details/item-details.component';
import {
  PublicReviewComponent
} from '../../common/public-review/public-review.component';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  TreeComponent
} from '../../common/tree/tree.component';
import {
  TabularTreeViewComponent
} from '../../common/tabular-tree-view/tabular-tree-view.component';
import {
  ProjectMappedNodeService
} from '../../project-mapped-node.service';
import { TableConfigurationComponent } from 'src/app/common/table-configuration/table-configuration.component';
import { TreeSearchComponent } from '../../common/tree-search/tree-search.component';
import { CustomTableViewComponent } from '../../common/custom-table-view/custom-table-view.component';
import { NodeTypeTableViewComponent } from '../../common/node-type-table-view/node-type-table-view.component';

/// <reference path='../../..assets/js/common.d.ts'>
@Component({
  selector: 'app-taxonomy-detail',
  templateUrl: './taxonomy-detail.component.html',
  styleUrls: ['./taxonomy-detail.component.scss', '../taxonomy-list/taxonomy-list.component.scss']
})

export class TaxonomyDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  JSON_WITH_FORMAT = 'JSON_WITH_FORMATTED';
  JSON_WITHOUT_FORMAT = 'JSON_WITHOUT_FORMATTED';
  PDF_FORMAT = 'PDF';
  CSV_FORMAT = 'CSV';
  SAMPLE_CSV_FORMAT = 'SAMPLE-CSV';
  JSON_WITH_CUSTOM = 'JSON_WITH_CUSTOM';
  CUSTOM_PDF_FORMAT = 'CUSTOM_PDF';
  DOCX_FORMAT = 'DOCX';
  LEFT_MIN_SIZE = Utils.LEFT_PANEL_MIN_SIZE; // Left panel min size
  LEFT_MAX_SIZE = Utils.LEFT_PANEL_MAX_SIZE; // Left panel max size

  workflowList = [];
  taxonomyList = [];
  taxonomyData: any;
  selectedWorkFlow = null;
  selectedTaxonomy = null;
  textLength = 50;
  selectedTaxonomyNodeIs = [];
  nodes: any;
  relations: any;
  treeResponseData: any;
  selectedTaxonomyId = null;
  selectedTaxonomySourceId = null;
  treeNode: any;
  tabItems: any[] = [];
  currentTab = 'taxonomy';
  currentTreeTab = 'detailview';
  isFirstTime = true;
  showAssocciation = false;
  showExemplar = false;
  list_enumeration;
  sequence_number;
  education_level;
  item_associations;
  nodeSelectedEvent: Subscription;
  taxonomyTitleEvent: Subscription;
  type = 'taxonomydetail';
  showProtip = true;
  workFlowStagesList: any[] = [];
  stageNameTitle: string;
  taxonomyTitle = '';
  taxonomyTitleHtml = '';
  treeGraphData = {
    nodes: [],
    links: []
  };
  serviceUrl;
  showExportButton = false;
  view = 'taxonomy-detail';
  itemAssociations: any;
  itemExemplars = [];
  itemAssets = [];
  itemAdditionalMetadata = [];
  itemLinkedNodes = [];
  showAddMetadatas = false;
  nodetypeData;
  selectedNodeType = {
    title: '',
    node_type_id: ''
  };
  defaultNodeType = 'Default';
  urlParams: any;
  parentNodeIds = [];
  taxonomyId: any;
  reportTabPermission = false; // 'Reports' tab permission
  canPublicReview = false;
  canPublishTaxonomy = false;
  canUnpublishTaxonomy = false;
  itemComplianceReports = [];
  viewLocation = 'taxonomyDetails';
  titleForm: FormGroup;
  treeTabItems = ['detailview', 'tableview'];
  showJasperReport = false;
  exportData = [];
  @ViewChild('nodeDetailsComponent', {
    static: false
  }) nodeDetailsComponent: NodeDetailsComponent;
  @ViewChild('treeAccordianRef', {
    static: false
  }) treeAccordianComponet: TreeAccordianComponent;
  @ViewChild('tree', {
    static: false
  }) tree: TreeComponent;
  @ViewChild('itemDetailsComponent', {
    static: false
  }) itemDetailsComponent: ItemDetailsComponent;
  @ViewChild('appPublicReview', {
    static: false
  }) publicReviewComponent: PublicReviewComponent;
  @ViewChild('element', {
    static: false
  }) viewElement: ElementRef;
  @ViewChild('tabularView', {
    static: false
  }) tabularView: TabularTreeViewComponent;
  @ViewChild('configTableRef', {
    static: false
  }) configTableRef: TableConfigurationComponent;
  @ViewChild('treeSearch', {
    static: false
  }) treeSearch: TreeSearchComponent;
  @ViewChild('customTable', {
    static: false
  }) customTable: CustomTableViewComponent;

  @ViewChild('nodeTypeTableView', {
    static: false
  }) nodeTypeTableView: NodeTypeTableViewComponent;

  view_element: any;
  searchResult = [];
  searchTrigger = false;
  searchResultEvent: Subscription;
  isLoaded = true;
  preventEdit = false;
  isEditable = false;
  isSearch = false;
  import_type = 0;
  customMetadata = [];
  showNavigation = true; // holds boolean value for showing up navigation arrow
  adoptionStatus = '';
  publishStatus: any;
  listOfColumn = [];
  colHeaders = ['full_statement', 'human_coding_scheme']; // holds internal names for column headers
  metadataList = []; // holds metadata list used for table view headers
  caseMetadataList = [];
  associationList = []; // holds associations list for csv export
  exportLoading = false; // Loading state for taxonomy export
  importCsvPermission = false; // holds permission for import CSV taxonomy from Taxonomy details page

  /*
  ,
  {
    name: '',
    propName: 'compStatus',
    class: 'col-sm-1 col-lg-1 col-xl-1',
  }
  */

  dynamicClass = 'col-6';

  listOfClass = [

    {
      class: 'col-2',
    },

    {
      class: 'col-3',
    },
    {
      class: 'col-1',
    }
  ];
  windowResizeSubscription: Subscription;
  canConfirureTable = true;
  listOfTableData = [];
  currentDataSet: any;
  defaultColumnList: any;
  additionalColumnList = [];
  canConfigureTableView = false;
  canSetDefaultForAll = false;
  initialiseSearch = true;
  tableViewList = [
    {
      name: 'Default',
      code_name: 'supper_default_table',
      isActive: false
    }, {
      name: 'Metadata',
      code_name: 'metadata_table',
      isActive: false
    }, {
      name: 'Nodetype',
      code_name: 'node_type_table',
      isActive: false
    }
  ];
  defaultNodeTypelist = [];
  additionalMetadata = [];
  additionalNodes = [];
  dropdownData: any = {
    uniqueId: 'metadata_id',
    propName: 'display_name'
  };
  configureTab = 'Metadata Fields';
  defaultmetadataArray = [];
  selectedTable = 'supper_default_table';
  enableButton = false;
  isCompatible = false;
  canEdit = false;
  isCalled = 0;
  searchResultList = [];
  filterCriteriaList = [];
  arrayToSearch = [];
  searchText = '';
  isFilterApplied = false;
  isLoading = false;
  isSearching = false;
  worker: any;
  isReset = false;
  showUserFriendlyPage = false;
  loadVersionModal = false;
  accessToken = '';
  taxonomyDetailFromList = {};
  comparisonId = '';
  showLoader = false;
  showVersionBtn = false;

  constructor(
    private service: CommonService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private treeService: TreeDataService,
    private treeService1: ProjectMappedNodeService,

    /*private walkService: WalktroughService,
    private tourService: NgxBootstrapProductTourService,*/
    private authenticateUser: AuthenticateUserService,
    private http: HttpClient) {
    this.getMetadataList();

    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      if (item) {
        if (item.id) {
          this.onTreeNodeSelected(item);
        }
      }
    });
    this.taxonomyTitleEvent = this.sharedService.setTitleEvent.subscribe((event: any) => {

      if (event && event.title) {
        switch (event.type) {
          case 'taxonomy':
            this.taxonomyTitle = event.title;
            this.taxonomyTitleHtml = event['title_html'];
            this.selectedTaxonomySourceId = event['source_document_id'];
            break;
          default:
            break;
        }

      }
    });

    this.windowResizeSubscription = this.sharedService.windowResizeEvent.subscribe((item: any) => {
      if (item) {
        this.taxonomyDetailsHeightCalculation();
        this.nodeTypeViewHeightCalculation();
      }
    });

    this.searchResultEvent = this.sharedService.searchResultEvent.subscribe((item: any) => {
      if (this.isLoaded) {
        this.isLoaded = false;
        return;
      }
      if (!this.isLoaded) {
        if (item) {
          this.searchResult = item['searchData'];
          if (item['showSearchList']) {
            this.searchTrigger = true;
          } else {
            this.searchTrigger = false;
            this.expendSelectedNode();
          }
          this.isFilterApplied = false;
        }
      }
    });
  }

  treeNodeSelectedEvent(item) {
    if (item.detail) {
      if (item.detail.id) {
        this.onTreeNodeSelected(item.detail);
      }
    }
  }
  ngOnInit() {
    this.titleForm = new FormGroup({
      title: new FormControl(null, [Validators.required])
    });
    this.sharedService.faqEvent.next({
      name: 'taxonomy_detail'
    });
    // this.getWorkFlowList();
    this.isEditable = false;
    console.log('ngOnInit TaxonomyDetail');
    this.route.params.subscribe(params => {
      this.urlParams = params;
      console.log('ngOnInit TaxonomyDetail subscribe', params);
      this.isSearch = false;
      if (params.location === 'search') {
        this.isSearch = true;
        this.currentTreeTab = 'detailview';
      }
      this.getAllMetadata();
      this.onTaxonomySelected(params.id);
      this.taxonomyId = params.id;
    });
    this.sharedService.notificationListEvent.subscribe((list: any) => {
      if (list && list.length) {
        const updateNoti = list.find(noti => noti.target_context.document_id === this.taxonomyId);
        if (updateNoti) {
          this.showVersionBtn = updateNoti.new_version_notification;
        }
      }
    });
    this.getDataList();
    this.accessToken = localStorage.getItem('access_token');
    // this.getNodeTypeDetails('metadata');
    // this.getNodeTypeDetails('node_type');
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('taxonomy_detail'));*/
  }

  getWorkFlowList() {
    const url = GlobalSettings.GET_WORKFLOW;
    this.service.getServiceData(url).then((res: any) => {
      this.getWorkFlowStagesList(res.WorkflowList.workflows[0].workflow_id);
    }).catch((ex) => {
      console.log('error in WorkflowList', ex);
    });
  }
  getWorkFlowStagesList(workflowId) {
    const url = GlobalSettings.GET_WORKFLOW + '/' + workflowId;
    this.service.getServiceData(url).then((res: any) => {
      this.workFlowStagesList = res.workflow.stages;
      this.stageNameTitle = res.workflow.stages[0].stage_name;
    }).catch((ex) => {
      console.log('error in workFlowStagesList', ex);
    });
  }

  onTaxonomySelected(id) {
    if (id) {
      // this.progressServie.start();
      this.taxonomyData = null;
      this.selectedTaxonomyId = id;
      this.treeTabItems = ['detailview', 'tableview'];
      this.serviceUrl = GlobalSettings.GET_TREE_VIEW_ENHANCED_V5 + id;
      // this.serviceUrl = GlobalSettings.GET_TREE_VIEW_ENHANCED + id;
      this.service.getServiceData(this.serviceUrl).then((response: any) => {
        // this.treeService.getTreeData(this.serviceUrl, true, Utils.EXPAND_LEVEL).then((response: any) => {
        const res = response;
        // const res = response.parsedTreeNodes;
        this.import_type = res.import_type;
        this.canEdit = true;
        if (this.import_type === 1) {
          this.canEdit = false;
        }
        if (this.treeService.isDeleted) {
          this.sendMsgNoSuchTaxonomy();
          return;
        }
        // console.log('GET_TREE_VIEW ', res);
        if (res && res.children) {
          // console.log('before order : ', res);
          // Utils.sortData(res.children);
          this.taxonomyDetailFromList['source_document_id'] = res.children[0].source_document_id;
          this.taxonomyDetailFromList['uri'] = res.children[0].uri;
          this.taxonomyData = res;
          this.arrayToSearch = this.taxonomyData.children;
          // if (!this.isFilterApplied) {
          //   this.searchResultList = this.taxonomyData.children;
          // }
          if (res.children.length > 0) {
            this.taxonomyTitle = res.children[0].full_statement;
            this.taxonomyTitleHtml = res.children[0].title_html;
            this.treeNode = this.taxonomyData.children[0];
            this.sharedService.setTitleEvent.next({
              type: 'taxonomy',
              title: Utils.extractContentFromHtml(this.taxonomyTitle),
              document_id: this.selectedTaxonomyId,
              title_html: this.taxonomyTitleHtml,
              isEditable: this.import_type === 1 ? false : true,
              source_document_id: this.selectedTaxonomySourceId
            });

            if (res.children[0].status && res.children[0].status === 5 || res.children[0].status && res.children[0].status === 3) {
              this.preventEdit = true;
              this.sharedService.preventEdit.next({
                preventEdit: this.preventEdit,
                importTypeEdit: this.canEdit,
              });
            } else {
              this.preventEdit = false;
              this.sharedService.preventEdit.next({
                preventEdit: this.preventEdit,
                importTypeEdit: this.canEdit
              });
              this.getItemsComplianceReport(); // compliance icon setting only for draft taxonomy
            }
          }
          init_detailsViewClick();
          this.expendSelectedNode();
          this.userRoleRight();
          this.onTreeTabSelected(this.currentTreeTab);

          // If custom view configured, then show custom view tab
          if (this.taxonomyData.children[0].custom_view_visibility === 1) { // 1 for show, 0 for hide
            this.treeTabItems.push('customview');
          }
          if (this.tabularView && this.currentTreeTab === 'tableview') {
            this.tabularView.setTableData(this.taxonomyData);
          }
          // if (this.nodeTypeTableView) {
          //   this.nodeTypeTableView.configTable(this.taxonomyData);
          // }
          setTimeout(() => {
            if (this.treeSearch) {
              this.treeSearch.getFilterData(this.taxonomyData, true);
            }
          }, 500);
        }
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });
    }

  }
  expendSelectedNode() {

    setTimeout(() => {
      if (this.tree) {
        // this.treeAccordianComponet.expendNode(this.treeNode.id);
        // if (!this.urlParams.parentId) {
        //   this.treeAccordianComponet.onNodeSelected(this.treeNode);
        // }
        if (this.urlParams.parentId && this.urlParams.itemId) {
          const node = {
            id: this.urlParams.itemId,
            is_document: 0,
            parent_id: this.urlParams.parentId
          };
          this.expandTreeTillSelectedNode(node);
        }
      }
    }, 1000);
  }

  expandTreeTillSelectedNode(node) {
    this.parentNodeIds = [];
    // Utils.expandTreeTillSelectedNode(node, this.taxonomyData);
    this.callExpandTreeFromWorker(node);
    this.tree.onNodeSelected(node);
    this.currentTreeTab = 'detailview';
    this.onTreeTabSelected(this.currentTreeTab);
  }
  onTreeNodeSelected(item) {
    console.log('onTreeNodeSelected ', item);
    this.treeNode = item;
    // this.selectedNode = item;
    this.getSelectedTreeItemDetail(item);
  }
  onTabSelected(tab) {
    Utils.removeBodyScrollClass();
    if (tab.toLowerCase() === 'public review') {
      this.type = 'public_review_comment';
      this.showProtip = false;
    }
    if (tab.toLowerCase() === 'jasper report') {
      this.getJasperReport();
    } else {
      this.type = 'taxonomydetail';
      this.showProtip = true;
    }
    setTimeout(() => {
      this.currentTab = tab;
    }, 10);
    if ((tab.toLowerCase() === 'taxonomy' && this.currentTreeTab === 'detailview') || tab === 'reports') {
      Utils.addBodyScrollClass();
    }
    if (tab.toLowerCase() === 'taxonomy') {
      this.sharedService.faqEvent.next({
        name: 'taxonomy_detail'
      });
    } else {
      this.sharedService.faqEvent.next({
        name: tab.toLowerCase().replace(/ /gi, '_') + '_taxonomy'
      });
    }
    if (tab !== 'taxonomy' && tab !== 'reports') {
      Utils.addBodyScroll();
      Utils.removeBodyScrollClass();
    }
  }

  onTreeTabSelected(tab) {
    this.currentTreeTab = tab;
    // console.log(this.treeNode.id, ' ', document.getElementById('01'));
    Utils.removeBodyScrollClass();
    // Utils.addBodyScroll();
    if (tab === 'detailview' && !this.isSearch) {
      Utils.addBodyScrollClass();
      setTimeout(() => {
        // const a = document.getElementsByTagName('acmt-tree')[0].shadowRoot.innerHTML;
        // if (this.taxonomyData && this.taxonomyData.children[0] && document.getElementById(this.taxonomyData.children[0]['id'] + '-node'))
        if (this.taxonomyData && this.taxonomyData.children[0]) {
          // document.getElementById(this.taxonomyData.children[0]['id'] + '-node').click();
          this.onTreeNodeSelected(this.taxonomyData.children[0]);
          this.taxonomyDetailsHeightCalculation();
        }
      }, 1200);
    }
    if (this.currentTreeTab === 'graphview') {
      this.showTreeGraphView();
    }

    if (this.currentTreeTab === 'tableview' && this.selectedTable === 'node_type_table') {
      setTimeout(() => {
        Utils.removeBodyScroll();
        this.nodeTypeViewHeightCalculation();
      }, 50);
      setTimeout(() => {
        if (this.nodeTypeTableView) {
          this.nodeTypeTableView.configTable(this.taxonomyData);
        }
      }, 50);
    }
    console.log('this.currentTreeTab', this.currentTreeTab);

    if (this.tabularView && this.currentTreeTab === 'tableview') {
      // Utils.addBodyScroll();
      Utils.removeBodyScroll();
      if (this.isCalled < 2) {
        this.getNodeTypeDetails('metadata');
        this.getNodeTypeDetails('node_type');
      }
      if (this.selectedTable === 'super_default_table') {
        setTimeout(() => {
          this.setDefaultWidth();
        }, 500);
      }
      this.tabularView.setTableData(this.taxonomyData);
    }
    if (tab === 'customview') {
      Utils.addBodyScroll();
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_TAXONOMY_CUSTOM_VIEW;
      localStorage.setItem('taxonomy_id', this.taxonomyId);
      this.router.navigate([path, this.taxonomyId]);
    }
  }
  getSelectedTreeItemDetail(item) {

    let url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + item.id;
    if (item['is_document'] === 1) {
      url = GlobalSettings.GET_CFDOC_ITEM_DETAIL + '/' + item.id;
    }
    this.itemAssociations = [];
    this.itemExemplars = [];
    this.itemAssets = [];
    this.itemAdditionalMetadata = [];
    this.itemLinkedNodes = [];
    this.service.getServiceData(url).then((res: any) => {
      console.log(' getSelectedTreeItemDetail ', res);
      if (this.treeNode['is_document'] === 1) {
        this.selectedTaxonomySourceId = res.source_document_id;
        // this.showAddMetadatas = false;
        res['is_editable'] = 1;
        if (!res.node_type_id) {
          res['node_type_id'] = this.treeNode.node_type_id;
          res['node_type'] = this.treeNode.title;
        }
        res['source_uuid_id'] = res['source_document_id'];
        this.adoptionStatus = res.adoption_status;
        this.publishStatus = res.status;
      } else {
        res['source_uuid_id'] = res['source_item_id'];
      }

      if (res.node_type_id) {
        this.setNodeTypeById(res.node_type_id);
      }

      if (this.itemDetailsComponent) {
        this.itemDetailsComponent.generateFormData(res, this.selectedNodeType.title);
      }
      this.list_enumeration = res.list_enumeration;
      this.sequence_number = res.sequence_number;
      this.education_level = res.education_level;
      this.item_associations = res.item_associations;
      // Move scroll to top
      const rightContainer = document.getElementById('right_container_id');
      if (rightContainer) {
        rightContainer.scrollTop = 0;
      }

      setTimeout(() => {
        this.showAssocciation = true;
        this.showExemplar = item.node_type === 'Document' ? false : true;
        this.itemAssociations = res.item_associations;
        if (res.exemplar_associations) {
          this.itemExemplars = res.exemplar_associations;
        }
        if (res.assets) {
          this.itemAssets = res.assets;
        }
        if (res.linked_item) {
          this.itemLinkedNodes = res.linked_item;
        }
        if (res.custom_metadata) {
          // this.itemAdditionalMetadata = res.custom_metadata;
          this.itemAdditionalMetadata = [];
          if (res.custom_metadata) {
            res.custom_metadata.forEach(obj => {
              if (obj.is_additional_metadata === 1) {
                this.itemAdditionalMetadata.push(obj);
              }
            });
          }
        }
        this.showAddMetadatas = true;

      }, 2000);
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
    });
  }

  ngAfterViewInit() {
    init_detailsViewClick();
    init_treeViewClick();
    // taxonomyDetailsHeightCalculation();
  }

  ngOnDestroy() {
    Utils.addBodyScroll();
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    if (this.taxonomyTitleEvent) {
      this.taxonomyTitleEvent.unsubscribe();
    }
    if (this.windowResizeSubscription) {
      this.windowResizeSubscription.unsubscribe();
    }
    Utils.addBodyScroll();
  }

  getAllMetadata() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.nodetypeData = res.nodetype;
    }).catch((err: any) => { });
  }
  /**
   * Set selectedNodeType by node_type_id
   * @param  {} node_type_id
   */
  setNodeTypeById(node_type_id) {
    if (this.nodetypeData) {
      from(this.nodetypeData)
        .filter((w: any) => w.node_type_id === node_type_id)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
          // console.log('Hello', this.selectedNodeType);
          // this.nodeType
        });
    }
  }


  selectStageName(data) {
    this.stageNameTitle = data.stage_name;
  }

  onExportTaxonomy(type) {
    let baseURL = GlobalSettings.EXPORT_TAXONOMY;
    let contentType = 'text/plain';
    this.exportLoading = true;
    switch (type) {
      case this.JSON_WITH_FORMAT:
      case this.JSON_WITHOUT_FORMAT:
        baseURL = GlobalSettings.EXPORT_TAXONOMY;
        contentType = 'text/plain';
        break;
      case this.PDF_FORMAT:
        baseURL = GlobalSettings.EXPORT_TAXONOMY_PDF;
        contentType = 'application/pdf';
        break;
      case this.CSV_FORMAT:
        baseURL = GlobalSettings.EXPORT_TAXONOMY_CSV;
        contentType = 'text/html';
        this.getExportTaxonomyTreeData();
        break;
      case this.JSON_WITH_CUSTOM:
        baseURL = GlobalSettings.EXPORT_TAXONOMY;
        contentType = 'text/plain';
        break;
      case this.SAMPLE_CSV_FORMAT:
        let sampleData;
        this.getJSON().subscribe(sample_data => {
          sampleData = sample_data;
          this.exportCSV(sampleData, 'sample_csv');
        });
        break;
      case 'PUBLIC_REVIEW':
        break;
      case this.CUSTOM_PDF_FORMAT:
        baseURL = GlobalSettings.EXPORT_CUSTOM_VIEW_URL + this.selectedTaxonomyId + '/' + 'pdf';
        contentType = 'application/pdf';
        break;
      case this.DOCX_FORMAT:
        baseURL = GlobalSettings.EXPORT_CUSTOM_VIEW_URL + this.selectedTaxonomyId + '/' + 'docx';
        contentType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        break;
      default:
        baseURL = GlobalSettings.EXPORT_TAXONOMY;
        break;
    }
    if (type !== this.CSV_FORMAT && type !== this.SAMPLE_CSV_FORMAT && type !== 'PUBLIC_REVIEW') {
      let url: any, url2: any;
      if (type === this.CUSTOM_PDF_FORMAT || type === this.DOCX_FORMAT) {
        url = baseURL;
      } else {
        url = baseURL + '/' + this.selectedTaxonomyId;
        if (type === this.JSON_WITH_FORMAT) {
          url = url + '?is_html=1';
        } else if (type === this.JSON_WITHOUT_FORMAT || type === this.JSON_WITH_CUSTOM) {
          url = url + '?is_html=0';
        }
      }
      if (type === this.JSON_WITH_CUSTOM) {
        const orgCode = this.service.getOrgDetails().replace('/org/', '');
        const newURL = GlobalSettings.VERSION + orgCode + '/ims/case/v1p0/CustomMetadata/';
        url2 = newURL + this.selectedTaxonomySourceId + '?document_id=' + this.selectedTaxonomyId;
      }
      const customJSONtype = type === this.JSON_WITH_CUSTOM ? (url.includes('case') ? 'custom' : 'case') : '';
      this.service.downloadServiceData(url, type === this.CUSTOM_PDF_FORMAT ? this.PDF_FORMAT : type).then((res: any) => {
        try {
          this.saveToFileSystem(res, type === this.CUSTOM_PDF_FORMAT ? this.PDF_FORMAT : type, contentType, customJSONtype);
          if (type === this.JSON_WITH_CUSTOM) {
            this.downloadCustomJSON(url2, type, contentType);
          }
        } catch (error) {
          console.log('onExportTaxonomy error ', error);
          this.exportLoading = false;
        }
      }).catch((ex) => {
        console.log('onExportTaxonomy ', ex);
        this.exportLoading = false;
      });
    }
  }

  public getJSON(): Observable<any> {
    try {
      return this.http.get('./assets/json/sample_csv.json');
    } catch (err) { }
  }


  private saveToFileSystem(response, type, contentType, urlType?: string) {

    let blob = null;
    if (type === this.PDF_FORMAT || type === this.CSV_FORMAT || type === this.DOCX_FORMAT) {
      blob = new Blob([response], {
        type: contentType
      });
    } else {
      if (urlType === 'custom') {
        blob = new Blob([JSON.stringify(response)], {
          type: contentType
        });
      } else {
        blob = new Blob([JSON.stringify(response.data)], {
          type: contentType
        });
      }

    }
    let title = this.taxonomyTitle;
    if (!this.taxonomyTitle) {
      title = this.selectedTaxonomyId;
    }
    if (type === this.JSON_WITHOUT_FORMAT || type === this.JSON_WITH_FORMAT) {
      saveAs(blob, title + '.' + type.replace('_WITHOUT_FORMATTED', '').replace('_WITH_FORMATTED', '').toLowerCase());
    } else if (type === this.JSON_WITH_CUSTOM) {
      saveAs(blob, title + '_' + urlType + '.' + type.replace('_WITH_CUSTOM', '').toLowerCase());
    } else {
      saveAs(blob, title + '.' + type.toLowerCase());
    }
    this.exportLoading = false;

  }

  downloadCustomJSON(url, type, contentType) {
    this.service.downloadServiceData(url, type).then((res: any) => {
      try {
        this.saveToFileSystem(res, type, contentType, 'custom');
      } catch (error) {
        console.log('onExportTaxonomy error ', error);
      }
    }).catch((ex) => {
      console.log('onExportTaxonomy ', ex);
      this.exportLoading = false;
    });
  }

  createCSVData(csvData, data) {

    data.forEach(element => {
      /* '' is used for treating string of numbers as string, otherwise string of numbers is saving
        as number in file*/
      const obj = {
        action: '',
        node_id: element.id ? ('' + element.id) : '',
        'parent/destination_node_id': element.parent_id ? ('' + element.parent_id) : '',
        case_association_type: ''
      };
      this.caseMetadataList.forEach(item => {
        if (element && element.case_metadata && element.case_metadata.length > 0) {

          const value = element.case_metadata[0][item.metadata_name];
          obj['case_' + item.metadata_name] = value ? ('' + value) : '';
          if (element.tree_association !== undefined) {
            if (element.tree_association === 0) {
              obj['case_' + item.metadata_name] = '';
              obj['case_association_type'] = element.association_type ? ('' + element.association_type) : '';
            }
          }
        }
      });
      this.customMetadata.forEach(metadata => {
        if (element && element.metadata_detail) {
          const metadataItem = element.metadata_detail.find(x => {
            if (metadata.name === x.name.toLowerCase()) {
              return x;
            }
          });
          obj[metadata.name + '(' + Utils.getCustomMetadataType(metadata.field_type) + ')'] = metadataItem ? metadataItem.metadata_value : '';
        }

      });
      if ((obj['parent/destination_node_id']) || element.document_id) {
        csvData.push(obj);
      }
      // This code is not requried, now assocation data is coming in relation array
      // pushing associations for node if any
      if (element.tree_association === 1) {
        const associations = this.findAssoByNodeId(element);
        if (associations.length > 0) {
          for (const asso of associations) {
            const assoObj = {
              action: '',
              node_id: asso.child_id ? ('' + asso.child_id) : '',
              'parent/destination_node_id': asso.parent_id ? ('' + asso.parent_id) : '',
              case_association_type: asso.association_type ? ('' + asso.association_type) : ''
            };
            csvData.push(assoObj);
          }
        }
      }

      if (element.children && element.children.length > 0) {
        this.createCSVData(csvData, element.children);
      }
    });

  }

  /**
   * To find corresponding associations for a node
   * @param nodeId (selected node id)
   */
  findAssoByNodeId(node: any) {
    const result = [];
    for (const asso of this.associationList) {
      if (asso.child_id === node.id) {
        if (asso.association_type !== 'isChildOf' || asso.parent_id !== node.parent_id) {
          result.push(asso);
        }
      }
    }
    Utils.sortDataArray(result, 'sequence_number', false, true);
    return result;
  }

  exportCSV(data, name) {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: ['action', 'node_id', 'parent/destination_node_id', 'case_association_type']
    };

    if (name === 'sample_csv') {
      options.headers.push('case_title', 'case_full_statement', 'case_human_coding_scheme',
        'case_list_enumeration', 'case_node_type', 'case_education_level');
    }
    this.caseMetadataList.forEach(metadata => {
      options.headers.push('case_' + metadata.metadata_name);
    });

    this.customMetadata.forEach(item => {
      options.headers.push(item.name + '(' + Utils.getCustomMetadataType(item.field_type) + ')');
    });

    console.log('CSV headers', options.headers);
    new Angular5Csv(data, name, options);
    this.exportLoading = false;

  }

  checkNumber(value) {
    console.log('checkNumber ', parseInt(value, 10));
    return parseInt(value, 10);
  }

  editTaxonomyName() {
    const url = GlobalSettings.GET_DOCUMENT_DETAIL + '/' + 'edit' + '/' + this.selectedTaxonomyId;
    const tempObj = {
      title: this.taxonomyTitle,
      title_html: this.taxonomyTitleHtml,
    };
    // this.taxonomyTitle = tempObj.title;
    this.service.putService(url, tempObj).then((res: any) => {

      this.sharedService.setTitleEvent.next({
        type: 'taxonomy',
        title: res.title,
        title_html: res.title_html ? res.title_html : this.taxonomyTitleHtml,
        document_id: this.selectedTaxonomyId,
        isEditable: this.import_type === 1 ? false : true,
        source_document_id: this.selectedTaxonomySourceId
      });

      this.taxonomyData.title = res.title;
      // Update the title in taxonomy tree
      if (this.taxonomyData && this.taxonomyData.children && this.taxonomyData.children.length > 0) {
        this.taxonomyData.children[0].title = res.title;
        this.taxonomyData.children[0].title_html = res.title_html ? res.title_html : this.taxonomyTitleHtml;
        this.taxonomyData.children[0].full_statement = res.title;
        this.taxonomyData.children[0].full_statement_html = res.title_html ? res.title_html : this.taxonomyTitleHtml;
      }
      this.sharedService.sucessEvent.next({
        type: 'taxonomy_title'
      });
    }).catch(ex => {
      console.log('error in taxonomy title', ex);
      this.onCancel();
    });
  }

  onCancel() {
    this.titleForm.reset();
    if (this.taxonomyData && this.taxonomyData.children && this.taxonomyData.children.length > 0) {
      this.taxonomyTitle = this.taxonomyData.children[0].full_statement;
      this.taxonomyTitleHtml = JSON.parse(JSON.stringify(this.taxonomyData.children[0].title_html));
      if (this.titleForm.controls['title']) {
        this.titleForm.controls['title'].setValue(this.taxonomyTitle);
      }
    }
  }
  showTreeGraphView() {
    /*this.service.getServiceData(this.serviceUrl).then((res: any) => {
      console.log('treeGraphData  ', res);
      this.treeGraphData.nodes = [];
      this.treeGraphData.links = [];
      res.nodes.forEach(element => {
        const obj = {
          id: element.id,
          name: element.title !== '' ? element.title : element.human_coding_scheme
        };
        this.treeGraphData.nodes.push(obj);
      });
      res.relations.forEach(element => {
        const obj = {
          source: element.parent_id,
          target: element.child_id
        };
        this.treeGraphData.links.push(obj);
      });

    }).catch((ex) => {

    });*/
    showTreeGraphV3(this.taxonomyData);
  }

  userRoleRight() {

    this.tabItems = ['taxonomy', 'reports', 'public review', 'comments'];

    if (this.authenticateUser.authenticTaxonomy('Unpublish Taxonomy')) {
      this.canUnpublishTaxonomy = true;
    }

    if (this.authenticateUser.authenticTaxonomy('Export Taxonomy')) {
      this.tabItems.push('export');
      this.showExportButton = true;
    }
    if (this.authenticateUser.authenticTaxonomy('Publish Taxonomy')) {
      this.tabItems.push('publish');
      this.canPublishTaxonomy = true;
    }

    if (this.authenticateUser.authenticTaxonomy('Update taxonomy table view')) {
      this.canConfigureTableView = true;
    }

    if (this.authenticateUser.authenticTaxonomy('Admin setup for table view')) {
      this.canSetDefaultForAll = true;
    }

    this.reportTabPermission = this.authenticateUser.authenticTaxonomy('Taxonomy Reports');
    if (this.reportTabPermission === false) { // If no permission for 'Reports' tab, remove it
      this.removeTab('reports');
    }

    this.canPublicReview = false;
    const self = this;
    this.authenticateUser.authenticatePublicReview('Publish for public review', function (val) {
      self.canPublicReview = val;
      console.log('canPublicReview', self.canPublicReview);
    });

    if (this.authenticateUser.authenticTaxonomy('Import Updated CSV')) {
      this.importCsvPermission = true;
    }

    const publicReviewTabPermission = this.authenticateUser.authenticTaxonomy('Publish Taxonomy For Public');
    if (publicReviewTabPermission === false) {
      this.removeTab('public review');
    }
    if (this.showJasperReport) {
      this.tabItems.push('jasper report');
    }

  }

  removeTab(tabName: any) {
    if (this.tabItems && this.tabItems.length > 0) {
      this.tabItems = this.tabItems.filter(function (item) {
        return (item !== tabName);
      });
    }
  }

  /**
   * Setting taxonomy as public review taxonomy
   */
  setTaxonomyForPublicReview(event) {
    if (this.selectedTaxonomyId) {
      const url = GlobalSettings.SET_PUBLIC_REVIEW + '/' + this.selectedTaxonomyId;
      const body = {
        title: this.taxonomyTitle,
        start_date: event.start,
        end_date: event.end
      };
      this.service.postService(url, body).then((res: any) => {
        console.log(' setTaxonomyForPublicReview ', res);
        this.sharedService.sucessEvent.next({
          type: 'set_public_review_success'
        });
        this.onCancel();
        if (this.publicReviewComponent) {
          this.publicReviewComponent.getPublicReviewHistory();
        }
      }).catch((ex: any) => {
        console.log('setTaxonomyForPublicReview ex:  ', ex);
        this.sharedService.sucessEvent.next({
          type: 'duplicate_review_create',
          customMsg: ex.msg ? ex.msg : 'Some error occurred! Please try later.'
        });
      });
    }
  }

  changeProtipDisplay(e) {
    this.showProtip = e;
  }

  /**
   * To get items compliance report
   */
  getItemsComplianceReport() {
    if (this.taxonomyId) {
      this.itemComplianceReports = [];
      const url = GlobalSettings.ITEM_COMPLIANCE_REPORT + '/' + this.taxonomyId + '/1';
      this.service.getServiceData(url).then((res: any) => {
        this.itemComplianceReports = res;
        if (this.taxonomyData && this.taxonomyData.children) {
          for (let i = 0; i < this.taxonomyData.children.length; i++) {
            this.setComplianceStatusForAllNode(this.taxonomyData.children[i]); // setting compliance icon
          }
        }
      }).catch((ex) => {
        console.log('getItemsComplianceReport ', ex);
      });
    }
  }

  /**
   * To set compliance status for each taxonomy node
   */
  setComplianceStatusForAllNode(data: any) {
    if (data) {
      if (data.children.length > 0) {
        data.compStatus = this.getComplianceStatusForItem(data.id); // 'compStatus' will hold compliance status value
        for (let i = 0; i < data.children.length; i++) {
          this.setComplianceStatusForAllNode(data.children[i]);
        }
      } else {
        data.compStatus = this.getComplianceStatusForItem(data.id);
        this.taxonomyDetailsHeightCalculation();
      }
    }
  }

  /**
   * To get compliance status value for respective item id (status values: 0,1,2)
   * @param itemId
   */
  getComplianceStatusForItem(itemId) {
    if (itemId) {
      for (let i = 0; i < this.itemComplianceReports.length; i++) {
        if (itemId === this.itemComplianceReports[i].item_id) {
          return this.itemComplianceReports[i].status;
        }
      }
    }
    return null;
  }

  getJasperReport() {
    this.view_element = this.viewElement.nativeElement;
    const URL = GlobalSettings.JASPER_REPORT_URL + this.taxonomyId;
    this.service.downloadServiceData(URL, 'HTML').then((res: any) => {
      const fragment = document.createRange().createContextualFragment(res);
      this.view_element.appendChild(fragment);
      // this.viewElement.nativeElement.querySelector('span[title]').style.backgroundColor = 'red';
      this.viewElement.nativeElement.querySelector('span[title]').addEventListener('click', function (event) {
        alert(event.target.title.split(';').join('\n'));
      });
    }).catch((ex) => {
      console.log('getJasperReport  ', ex);
    });
  }

  getExportTaxonomyTreeData() {
    const url = GlobalSettings.CSV_EXPORT_BACKGROUND + '/' + this.selectedTaxonomyId;
    this.service.getServiceData(url).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'export_background'
      });
      this.exportLoading = false;
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });

    // const url = GlobalSettings.CSV_EXPORT_TAXONOMY + '/' + this.selectedTaxonomyId;
    // this.treeService1.getTreeData(url, Utils.EXPAND_LEVEL, true).then((response: any) => {
    //   const res = response;
    //   this.exportData = this.treeService1.getNodes();
    //   this.associationList = this.treeService1.externalRelations;
    //   this.exportData.forEach(csvdata => {
    //     const caseMetadata = csvdata.case_metadata[0];
    //     for (const metadata in caseMetadata) {
    //       if (this.caseMetadataList.findIndex(x => x.metadata_name === metadata.toLowerCase()) === -1) {
    //         this.caseMetadataList.push({
    //           metadata_name: metadata
    //         });
    //       }
    //     }
    //     csvdata.metadata_detail.forEach(item => {
    //       if (this.customMetadata.findIndex(x => x.name === item.name.toLowerCase()) === -1) {
    //         this.customMetadata.push({
    //           name: item.name.toLowerCase(),
    //           field_type: item.field_type
    //         });
    //       }
    //     });
    //   });
    //   const data = [];
    //   // Utils.sortData(res.children);
    //   this.createCSVData(data, res);
    //   this.exportCSV(data, this.taxonomyTitle);
    // }).catch(ex => {
    //   console.log('Export CSV error ', ex);
    //   this.exportLoading = false;
    // });
  }

  nodeTypeViewHeightCalculation() {
    if (this.nodeTypeTableView) {
      this.nodeTypeTableView.nodeTypeViewHeightCalculation();
    }
  }

  tabularViewHeightCalculation() {
    if (this.tabularView) {
      this.tabularView.tabularViewHeightCalculation();
    }
  }


  taxonomyDetailsHeightCalculation() {
    const windowHeight = window.innerHeight;
    let tabHeight;
    let titleHeight;
    if (document.getElementById('treeTabContainer') && document.getElementById('tabContainer')) {
      tabHeight = document.getElementById('treeTabContainer').offsetHeight + document.getElementById('tabContainer').offsetHeight;
    }
    if (document.getElementsByClassName('authoring-header') && document.getElementsByClassName('authoring-header')[0]) {
      titleHeight = document.getElementsByClassName('authoring-header')[0].clientHeight;
    }
    // tslint:disable-next-line:max-line-length
    const protipHeight = document.getElementById('taxonomyProtip') && document.getElementById('taxonomyProtip').offsetHeight ? document.getElementById('taxonomyProtip').offsetHeight : 0;
    const extraHeight = 69;
    const panelHeight = windowHeight - (tabHeight + titleHeight + protipHeight + extraHeight);
    if (document.getElementById('treeDetailsView')) {
      document.getElementById('treeDetailsView').style.height = panelHeight + 'px';
    }
    // document.getElementById('right_container_id').style.height = panelHeight + 'px';
    if (document.getElementById('right_container_id')) {
      document.getElementById('right_container_id').style.height = panelHeight + 'px';
    }
  }

  onNextNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  onPreviousNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  selectNodeOnNavigation(node: any) {
    this.treeNode = node;
    Utils.expandTreeTillSelectedNode(this.treeNode, this.taxonomyData);
    this.tree.onNodeSelected(this.treeNode);
    setTimeout(() => {
      const id = this.treeNode['id'] ? this.treeNode['id'] : this.treeNode['item_id'];
      if (document.getElementById(id + '-node')) {
        document.getElementById(id + '-node').focus();
      }
    }, 100);
  }

  updateTitleFromEditor(event) {
    this.taxonomyTitle = event['value'];
    this.taxonomyTitleHtml = event['htmlValue'];
  }


  /* --------- Fetch metadata List functionality Started --------- */

  getMetadataList() {
    const url = GlobalSettings.GET_METADATA_LIST + '?is_custom=0';
    this.service.getServiceData(url).then((res: any) => {
      this.metadataList = res.metadata;

      this.colHeaders.forEach(header => {
        this.listOfColumn.forEach(element => {
          if (element.propName === header) {
            element.name = this.extractMetadataName(header);
          }
        });
      });
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  /* --------- Fetch metadata List functionality Ended --------- */


  /* --------- Functionality to fetch metadata name from internal name start --------- */

  extractMetadataName(interal_name) {
    for (const i in this.metadataList) {
      if (i && this.metadataList[i]['internal_name'] === interal_name) {
        return this.metadataList[i]['name'];
      }
    }
  }

  /* --------- Functionality to fetch metadata name from internal name end --------- */

  sendMsgNoSuchTaxonomy() {
    this.sharedService.sucessEvent.next({
      type: 'no_taxonomy'
    });
    setTimeout(() => {
      this.redirectToTaxonomyList();
    }, 300);
  }

  redirectToTaxonomyList() {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_TAXONOMY_LIST;
    this.router.navigate([path]);
  }

  getLeftPanelWidth() {
    let leftMenuWidth;
    let menuButtonWidth;
    const showNav = JSON.parse(localStorage.getItem('isShowNav'));
    const windowWidth = window.innerWidth;
    const actualWholePanelWidth = document.getElementById('treeViewContainer').offsetWidth;
    const rightPanelWidth = document.getElementById('right_container_id').offsetWidth;
    if (document.getElementById('mainNav')) {
      leftMenuWidth = document.getElementById('mainNav').offsetWidth;
    }
    if (document.getElementById('menuButton')) {
      menuButtonWidth = document.getElementById('menuButton').offsetWidth;
    }
    const navbarWidth = showNav ? leftMenuWidth : menuButtonWidth;
    const withoutNavbarWidth = windowWidth - navbarWidth;
    const extraWidth = withoutNavbarWidth - actualWholePanelWidth;
    let leftPanelWidth;
    leftPanelWidth = (actualWholePanelWidth - rightPanelWidth) + extraWidth;
    return (leftPanelWidth + 'px');
  }

  // On csv import modal close
  onImportModalClose() {
    this.onTaxonomySelected(this.taxonomyId);
  }

  // On import button click
  onImportTaxoClick() {
    setTimeout(() => {
      if (document.getElementById('closeimportTaxonomyModal')) {
        document.getElementById('closeimportTaxonomyModal').focus();
      }
    }, 700);
  }

  getDataList() {
    this.isLoading = true;
    this.isFirstTime = true;
    this.defaultmetadataArray = [];
    const featureId = 3;
    const settingId = 3;
    const url = GlobalSettings.TABLE_CONFIG + '/' + featureId + '/' + settingId + '?document_id=' + this.taxonomyId;
    this.service.getServiceData(url, true, true).then((res: any) => {
      console.log('tabular view get data', res);
      this.listOfTableData = res.data.taxonomy_table_views;
      const defaultTable = res.data.taxonomy_table_views[0];
      defaultTable.table_config.forEach(item => {
        item.metadata.forEach(data => {
          data.internal_name = data.internal_name.toLowerCase().replace(' ', '_');
          if (data.internal_name !== 'full_statement') {
            data.canRemove = true;
          }
          if (data.internal_name === 'node_type') {
            data.metadata_id = 'node_type_1234';
          }
          if (this.defaultmetadataArray.indexOf(data) === -1) {
            this.defaultmetadataArray.push(data);
          }
        });
      });
      if (this.listOfTableData.length > 1) {
        if (res.data.last_table_view) {
          this.loadTable(res.data.last_table_view);
        }
      } else {
        this.loadTable('supper_default_table');
      }
      this.extractFilterCriteriaList();
      this.isLoading = false;
      this.isFirstTime = false;
    }).catch((ex) => {
      console.log('error', ex);
    });
  }

  openConfigurationPage() {
    this.configureTab = 'Metadata Fields';
    this.toggleColumnView(this.configureTab);
  }

  getNodeTypeDetails(viewType) {
    this.isCalled += 1;
    const type = 1;
    const returnOrder = 'return_order=1';
    const url = GlobalSettings.TAXONOMY_NODE_TYPE_DETAILS + '/' + this.taxonomyId + '/' + type + '/' + viewType + '?' + returnOrder;
    this.service.getServiceData(url, true, true).then((res: any) => {
      console.log('taxonomy node type detail response #########', res.data);
      if (res.success) {
        if (viewType === 'metadata') {
          this.additionalMetadata = res.data.metadata;
          const typeMetadata = this.defaultmetadataArray.find(data => data.internal_name === 'node_type');
          this.additionalMetadata.push(typeMetadata);
        } else {
          this.defaultNodeTypelist = [];
          this.additionalNodes = [];
          this.isCompatible = res.data.compatibility;
          res.data.nodeTypes.forEach(element => {
            element.selectedMetadataList = [];
            element.metadata.forEach(item => {
              item.selectDisplay = false;
            });
          });
          this.constructData(res.data.nodeTypes, 'node_type');
          this.additionalNodes = res.data.nodeTypes;
          res.data.defaultNodeTypesOrder.forEach(element => {
            if (element && (this.defaultNodeTypelist.findIndex(data => data.node_type_id === element) === -1)) {
              this.defaultNodeTypelist.push(res.data.nodeTypes.find((node: any) => node.node_type_id === element));
            }
          });
          this.currentDataSet = {
            is_user_default: this.defaultColumnList.is_user_default,
            default_for_all_user: this.defaultColumnList.default_for_all_user,
            taxonomy_list_setting: this.defaultNodeTypelist
          };
        }
      }

    }).catch((ex) => {

    });
  }

  loadTable(type, disableSave?: boolean, configTable = false) {
    console.log('type', type);
    if (type === 'node_type_table' && configTable) {
      setTimeout(() => {
        Utils.removeBodyScroll();
        this.nodeTypeViewHeightCalculation();
        if (this.nodeTypeTableView) {
          // this.nodeTypeTableView.resetData();
          this.nodeTypeTableView.configTable(this.taxonomyData);
        }
      }, 50);
    } else {
      setTimeout(() => {
        this.tabularViewHeightCalculation();
      }, 50);
      Utils.removeBodyScroll();
    }
    this.colHeaders = [];
    this.listOfColumn = [];
    if (!disableSave) {
      this.selectedTable = type;
    }
    this.tableViewList.forEach((table: any) => {
      if (table.code_name === type) {
        table.isActive = true;
      } else {
        table.isActive = false;
      }
    });

    if (type === 'supper_default_table' || (this.listOfTableData && this.listOfTableData.length === 1)) {
      this.setDefaultColumnList(type);
    } else {
      const currentTable = this.listOfTableData.find((view: any) => view.table_name.toLowerCase().trim() === type.toLowerCase().trim());
      if (currentTable) {
        this.currentDataSet = {
          is_user_default: currentTable.is_user_default,
          default_for_all_user: currentTable.default_for_all_user,
          taxonomy_list_setting: currentTable.table_config
        };
      } else {
        this.setDefaultColumnList(type);
      }
    }
    this.constructData(this.currentDataSet.taxonomy_list_setting, type.replace('_table', ''));
    this.listOfColumn = JSON.parse(JSON.stringify(this.currentDataSet.taxonomy_list_setting));
    if (type !== 'node_type_table') {
      this.listOfColumn.push({
        name: '',
        display_name: '',
        propName: 'compStatus',
        internal_name: 'compStatus',
        class: 'col-sm-1 col-lg-1 col-xl-1',
      });
    }

    this.listOfColumn.forEach((colData: any) => {

      if (colData.internal_name !== 'node_type' || colData.internal_name !== 'compStatus') {
        this.colHeaders.push(colData.internal_name);
      }

    });
    this.enableButton = this.currentDataSet.is_user_default || this.currentDataSet.default_for_all_user;
  }

  setDefaultColumnList(type) {
    this.currentDataSet = {
      is_user_default: 1,
      default_for_all_user: 0,
      taxonomy_list_setting: type === 'node_type_table' ? this.defaultNodeTypelist : this.defaultmetadataArray
    };

  }
  constructData(array, type) {
    if (type === 'metadata' || type === 'supper_default') {
      array.forEach(data => {
        data.name = data.display_name;
        data.internal_name = data.internal_name === 'compStatus' ? 'compStatus' : data.internal_name.toLowerCase().replace(/ /gi, '_');
        // data.class = 'col';
        data.propName = data.internal_name;
        if (data.internal_name !== 'full_statement') {
          data.canRemove = true;
        } else {
          data.canRemove = false;
        }
      });

    } else {
      array.forEach(element => {
        element.selectedMetadataList = [];
        element.canRemove = true;
        element.metadata.forEach(item => {
          if (!element.metadataSelected && (item.internal_name === 'full_statement' || item.internal_name === 'human_coding_scheme')) {
            item.is_selected = 1;
            item.selectDisplay = true;
          } else {
            item.is_selected = 0;
          }
          if (item.selectDisplay) {
            item.propName = 'display_name';
            item.value = item.display_name;
            element.selectedMetadataList.push(item);
          }
        });
      });
    }

  }

  toggleColumnView(tab) {
    this.configureTab = tab;

    switch (tab) {
      case 'Metadata Fields':
        this.dropdownData = {
          uniqueId: 'metadata_id',
          propName: 'display_name'
        };
        this.loadTable(this.tableViewList[1].code_name, true);
        this.loadColumnList('metadata');
        break;
      case 'Node Type':
        this.dropdownData = {
          uniqueId: 'node_type_id',
          propName: 'display_name'
        };
        this.loadTable(this.tableViewList[2].code_name, true);
        this.loadColumnList('nodeType');
        break;
      default:
    }
    this.defaultColumnList = {
      is_user_default: 1,
      default_for_all_user: 0,
      taxonomy_list_setting: tab === 'Metadata Fields' ? this.defaultmetadataArray.slice(0) : this.defaultNodeTypelist.slice(0)
    };
  }

  saveChanges() {
    this.configTableRef.saveChanges();
  }
  updateListView(e) {
    console.log(e);
    this.setTableViewDataList(e);
  }

  setTableViewDataList(changedData: any) {
    this.constructData(changedData.columnList, this.configureTab === 'Metadata Fields' ? 'metadata' : 'node_type');
    const url = GlobalSettings.TABLE_CONFIG;
    const body = {
      document_id: this.taxonomyId,
      setting_id: 3,
      feature_id: 3,
      table_name: this.configureTab === 'Metadata Fields' ? 'metadata_table' : 'node_type_table',
      json_data: {
        version: 1,
        feature_id: 3,
        setting_id: 3,
        last_table_view: this.configureTab === 'Metadata Fields' ? 'metadata_table' : 'node_type_table',
        taxonomy_table_views:
          [{
            table_name: this.configureTab === 'Metadata Fields' ? 'metadata_table' : 'node_type_table',
            is_user_default: changedData.userDefault,
            default_for_all_user: changedData.allUserDefault,
            view_type: this.configureTab === 'Metadata Fields' ? 'metadata' : 'node_type',
            table_config: changedData.columnList
          }]

      }
    };
    this.service.putService(url, body, 0, true).then((res: any) => {
      if (res.success) {
        this.sharedService.sucessEvent.next({
          type: 'success',
          customMsg: res.message
        });
        this.clearFields();
        this.getDataList();
        this.onTaxonomySelected(this.urlParams.id);
      } else {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: res.message
        });
      }
    }).catch((ex) => {
      console.log(ex);
    });
  }

  loadColumnList(type) {
    if (type === 'metadata') {
      this.additionalColumnList = this.additionalMetadata;
    } else {
      this.additionalColumnList = this.additionalNodes;
    }

  }

  cancelChanges() {
    this.loadTable(this.selectedTable, true);
    this.clearFields();
    if (this.loadVersionModal) {
      this.loadVersionModal = false;
    }
  }

  clearFields() {
    this.configTableRef.onCancel();
  }

  checkValidity(tableName) {
    if (this.listOfTableData.find((table: any) => table.table_name === tableName.code_name)) {
      return true;
    } else {
      return false;
    }
  }

  checkSaveValidation(val) {
    this.enableButton = val;
  }

  setDefaultWidth() {
    if (document.getElementById('tablePanel')) {
      const wholeTableWidth = document.getElementById('tablePanel').offsetWidth;
      const lastColWidth = document.getElementById('tablePanel').lastChild['offsetWidth'];
      const actualTableWidth = wholeTableWidth - lastColWidth;
      const indivColWidth = actualTableWidth / this.defaultmetadataArray.length;
      this.defaultmetadataArray.forEach(col => {
        col.width = indivColWidth;
      });
    }

  }

  captureSearchEvent(event) {
    this.isFilterApplied = event.isFilterApplied;
    this.searchResultList = event.filterData;
    this.taxonomyData = event.treeData;
    if (this.isFilterApplied) {
      this.isReset = false;
    }
    if (!this.isFilterApplied && !this.isReset) {
      this.isReset = true;
      // this.onTaxonomySelected(this.taxonomyId);
    }
    if (this.isReset) {
      this.resetTree();
    }
    if (this.tabularView && !this.isReset) {
      this.tabularView.setTableData(this.taxonomyData);
    }
  }

  extractFilterCriteriaList() {
    this.filterCriteriaList = [];
    const index = this.listOfTableData.findIndex(table => table.table_name === 'metadata_table');
    if (index > -1) {
      const filterArray = this.defaultmetadataArray.concat(this.listOfTableData[index].table_config);
      filterArray.forEach(item => {
        if (!this.filterCriteriaList.find(metadata => metadata.display_name === item.display_name)) {
          this.filterCriteriaList.push(item);
        }
      });

    } else {
      this.filterCriteriaList = JSON.parse(JSON.stringify(this.defaultmetadataArray));
    }
  }

  setLoaderEvent(val) {
    this.isFirstTime = val['isFirstTime'];
    // if (!this.initialiseSearch) {
    this.isSearching = val['isLoading'];
    // }
    this.initialiseSearch = false;
  }

  callExpandTreeFromWorker(node) {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker('../../treeService.worker', { type: 'module' });
    }
    if (this.worker) {
      this.worker.onmessage = ({ data }) => {
        this.taxonomyData = null;
        console.log('data received from worker in taxonomy', data);
        if (data) {
          this.taxonomyData = data.taxonomyData;
        }
      };
      this.worker.postMessage({
        data: this.taxonomyData,
        location: 'utils',
        parameters: {
          selectedNode: node,
          isMultipleCall: false
        }
      });
    }
  }

  exportTable() {
    this.nodeTypeTableView.export();
  }

  // FUNCTION to reset tree data after search reset

  resetTree() {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker('../../treeService.worker', { type: 'module' });
    }
    if (this.worker) {
      this.worker.onmessage = ({ data }) => {
        this.taxonomyData = null;
        if (data) {
          this.taxonomyData = data.taxonomyData;
          // console.log('data received from worker in resettree', this.taxonomyData);
          if (this.tabularView) {
            this.tabularView.setTableData(this.taxonomyData);
          }
        }
      };
      this.worker.postMessage({
        data: this.taxonomyData,
        location: 'utils',
        parameters: {
          level: 1,
          reset: true
        }
      });
    }
  }

  loadCoverageReport(obj) {
    this.callCompareVersion(obj.firstObj, obj.secondObj);
  }

  callCompareVersion(v1 = '', v2 = '') {
    this.loadVersionModal = true;
    this.showLoader = true;
    // tslint:disable-next-line: no-string-literal
    const idx = this.taxonomyDetailFromList['uri'].indexOf('ims/case/');
    // tslint:disable-next-line: no-string-literal
    const serverUri = this.taxonomyDetailFromList['uri'].substr(0, idx);
    const url = serverUri + 'compareVersion';
    const body = {
      // tslint:disable-next-line: no-string-literal
      document_id: this.taxonomyDetailFromList['source_document_id'],
      first_version: v1,
      second_version: v2
    };
    this.service.postService(url, body, 0, true, false).then((res: any) => {
      if (res.success) {
        this.comparisonId = res.data.comparison_id;
      } else {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: res.message
        });
      }
      this.showLoader = false;
    }).catch(ex => {
      console.log(ex);
      this.showLoader = false;
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: ex.msg
      });
    });
  }
}
