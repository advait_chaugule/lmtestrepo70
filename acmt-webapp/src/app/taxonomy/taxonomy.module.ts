import {
  NgModule,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';

import {
  TaxonomyRoutingModule
} from './taxonomy-routing.module';
import {
  SharedModule
} from '../shared/shared.module';
import {
  PublicModule
} from '../shared/public.module';
import {
  TaxonomyListComponent
} from './taxonomy-list/taxonomy-list.component';
import {
  TaxonomyDetailComponent
} from './taxonomy-detail/taxonomy-detail.component';
import {
  ImportTaxonomyComponent
} from './import-taxonomy/import-taxonomy.component';
import {
  TaxonomyCreateOptionsComponent
} from './taxonomy-create-options/taxonomy-create-options.component';
import {
  Shared2Module
} from '../shared/shared2/shared2.module';
import {
  Shared3Module
} from '../shared/shared3/shared3.module';
import {
  MyDateRangePickerModule
} from 'mydaterangepicker';
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule
} from 'ng-pick-datetime';
import {
  SetPublicReviewModalComponent
} from './set-public-review-modal/set-public-review-modal.component';
import { CustomTaxonomyViewComponent } from './custom-taxonomy-view/custom-taxonomy-view.component';
import { UploadingListComponent } from './import-taxonomy/uploading-list/uploading-list.component';
// import { AngularSplitModule } from 'angular-split';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';
import { TaxonomySummaryComponent } from './import-taxonomy/taxonomy-summary/taxonomy-summary.component';
import { TaxonomyCsvImportModalComponent } from './taxonomy-csv-import-modal/taxonomy-csv-import-modal.component';
import { TaxonomyGroupsComponent } from './taxonomy-groups/taxonomy-groups.component';
// import '../../js/acmt-tree.js';
import '../../assets/js/taxonomy-version.js';
@NgModule({
  imports: [
    CommonModule,
    TaxonomyRoutingModule,
    SharedModule,
    Shared2Module,
    Shared3Module,
    PublicModule,
    MyDateRangePickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    // AngularSplitModule.forRoot(),
    PreLoaderModule
  ],
  declarations: [
    TaxonomyListComponent,
    TaxonomyDetailComponent,
    TaxonomyCreateOptionsComponent,
    ImportTaxonomyComponent,
    SetPublicReviewModalComponent,
    CustomTaxonomyViewComponent,
    UploadingListComponent,
    TaxonomySummaryComponent,
    TaxonomyCsvImportModalComponent,
    TaxonomyGroupsComponent,
  ],
  exports: [SharedModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TaxonomyModule { }
