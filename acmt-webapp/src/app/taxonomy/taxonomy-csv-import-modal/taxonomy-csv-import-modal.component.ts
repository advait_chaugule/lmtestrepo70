import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ImportTaxonomyComponent } from '../import-taxonomy/import-taxonomy.component';

@Component({
  selector: 'app-taxonomy-csv-import-modal',
  templateUrl: './taxonomy-csv-import-modal.component.html',
  styleUrls: ['./taxonomy-csv-import-modal.component.scss']
})
export class TaxonomyCsvImportModalComponent implements OnInit {

  CSV_TYPE = 'CSV';
  selectedFileType = this.CSV_TYPE;
  steps = [false, true];
  currentStep = 0;
  continueDisabled = true;
  proceedDisabled = false;
  uploadFinished = false; // indicates whether uploading is finished or not for CSV
  showSummaryDetails = false;
  document: any = {
    'document_id': null,
    'document_title': null
  };
  children_taxonomies_count = 0;
  associations_count = 0;
  taxonomy_count = 0;
  totalTaxonomyCountToUpload = 0;
  updated_node_count = 0;
  blank_updated_node_count = 0;
  updated_metadata_count = 0;
  blank_metadata_count = 0;
  new_metadata_count = 0;
  new_node_count;
  delete_node_count;
  updatedItems = [];
  updatedMetadata = [];
  errorData = []; // to display error report for CSV import in summary
  createdOrUpdated: string;

  @Input() documentId: any; // selected taxonomy id from where CSV is being importing
  @Output() modalClosedEvent: EventEmitter<any> = new EventEmitter<any>(); // modal close event emitter
  @ViewChild('importComponent', {
    static: false
  }) importComponent: ImportTaxonomyComponent;

  constructor() { }

  ngOnInit() {
  }

  onFileUploadEvent(event) {
    if (event.type === 'start') {
      this.continueDisabled = true;
      this.proceedDisabled = true;
      this.uploadFinished = false;
    } else {
      this.continueDisabled = !event.successUpload;
      this.proceedDisabled = !event.successUpload;
      this.uploadFinished = event.successUpload;
    }

    this.updated_node_count = event.updated_node_count;
    this.blank_updated_node_count = event.blank_updated_node_count;
    this.updated_metadata_count = event.updated_metadata_count;
    this.blank_metadata_count = event.blank_metadata_count;
    this.new_metadata_count = event.new_metadata_count;
    this.new_node_count = event.new_node_count;
    this.createdOrUpdated = event.createdOrUpdated;
    this.delete_node_count = event.delete_node_count;
    this.associations_count = event.associations_count;
    this.updatedItems = [];
    this.updatedMetadata = event.updatedMetadata;
    this.errorData = event.errorData;
    this.totalTaxonomyCountToUpload = event.totalTaxonomyInUploadState;
    this.updatedItems = event.updatedItems;
  }

  onFileTypeEvent(event) {
    this.selectedFileType = event;
  }

  onTryAgainClick() {
    this.steps = [false, true];
    this.currentStep = 0;
    this.continueDisabled = true;
    if (this.importComponent) {
      this.importComponent.onFileTypeSelected();
    }
  }

  onFinishClick() {
    setTimeout(() => {
      this.onCancelClick();
    }, 50);
  }

  onCancelClick() {
    this.steps = [false, true];
    if (this.importComponent) {
      this.importComponent.clearFiles();
      this.importComponent.resetForm();
      // this.importComponent.setDefalutSelection();
    }
    this.onFileTypeEvent(JSON.parse(JSON.stringify(this.CSV_TYPE)));
    this.updated_node_count = 0;
    this.blank_updated_node_count = 0;
    this.updated_metadata_count = 0;
    this.blank_metadata_count = 0;
    this.new_metadata_count = 0;
    this.totalTaxonomyCountToUpload = 0;
    this.associations_count = 0;
    this.new_node_count = 0;
    this.updatedItems = [];
    this.updatedMetadata = [];
    this.errorData = [];
    this.document = {
      'document_id': null,
      'document_title': null
    };
    this.continueDisabled = true;
    this.proceedDisabled = false;
    this.uploadFinished = false;
    this.currentStep = 0;
    this.modalClosedEvent.emit({ action: 'closed' });
  }

  onContinueClick() {
    this.currentStep++;
    let index = this.steps.indexOf(false);

    if (index === 2) {
      console.log('Index 2');
    } else {
      this.steps[index] = true;
      index++;
      this.steps[index] = false;
    }
    if (this.currentStep === 1) {
      this.uploadFinished = false;
    }
    this.onProceedWithImportedData();
  }

  onProceedWithImportedData() {
    this.importComponent.onConfirmClicked(0);
  }

}
