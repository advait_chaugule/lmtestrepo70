import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyCsvImportModalComponent } from './taxonomy-csv-import-modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('TaxonomyCsvImportModalComponent', () => {
  let component: TaxonomyCsvImportModalComponent;
  let fixture: ComponentFixture<TaxonomyCsvImportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxonomyCsvImportModalComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomyCsvImportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
