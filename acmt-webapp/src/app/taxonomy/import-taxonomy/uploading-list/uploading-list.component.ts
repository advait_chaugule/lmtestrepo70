import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import Utils from '../../../utils';

@Component({
  selector: 'app-uploading-list',
  templateUrl: './uploading-list.component.html',
  styleUrls: ['../import-taxonomy.component.scss']
})
export class UploadingListComponent implements OnInit {

  UPLOAD_NOT_STARTED = Utils.UPLOAD_NOT_STARTED;
  UPLOAD_IN_PROGRESS = Utils.UPLOAD_IN_PROGRESS;

  @Input() uploadList = []; // upload file list
  @Input() showDeleteButton = true; // holds boolean value whether to show/hide delete button
  @Input() isUploading = false; // holds boolean value whether uploading is in progress or not
  @Input() hideImportOptions = false; // holds to show import type dropdown or not
  @Input() isImportLoader; // holds boolean value whether to Enable/Diseble option
  @Input() showSubscribeBtn = false; // holds boolean value whether to show subscribe option or not

  @Output() importTypeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter<any>();
  importType = null;
  importNoChangesText = 'Import with no changes';

  constructor() { }

  ngOnInit() {
  }

  formatBytes(bytes: any) {
    return Utils.formatBytes(bytes);
  }

  onImportTypeChange(id: any) {
    this.importTypeEvent.emit({ 'value': this.importType, 'documentId': id });
  }

  removeFile(index: number) {
    const deletedData = this.uploadList[index];
    deletedData.is_selected = 0;
    this.uploadList.splice(index, 1);
    this.setIndexForAll();
    this.deleteEvent.emit({ operation: 'deleted', data: deletedData });
  }

  setIndexForAll() {
    for (let i = 0; i < this.uploadList.length; i++) {
      this.uploadList[i].index = i;
    }
  }

}