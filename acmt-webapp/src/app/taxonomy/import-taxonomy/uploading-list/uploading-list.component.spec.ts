import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadingListComponent } from './uploading-list.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

describe('UploadingListComponent', () => {
  let component: UploadingListComponent;
  let fixture: ComponentFixture<UploadingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploadingListComponent],
      imports: [FormsModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create UploadingListComponent', () => {
    expect(component).toBeTruthy();
  });
});
