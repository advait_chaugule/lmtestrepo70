import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomySummaryComponent } from './taxonomy-summary.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('TaxonomySummaryComponent', () => {
  let component: TaxonomySummaryComponent;
  let fixture: ComponentFixture<TaxonomySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxonomySummaryComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create TaxonomySummaryComponent', () => {
    expect(component).toBeTruthy();
  });
});
