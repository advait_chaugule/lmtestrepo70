import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-taxonomy-summary',
  templateUrl: './taxonomy-summary.component.html',
  styleUrls: ['./taxonomy-summary.component.scss']
})
export class TaxonomySummaryComponent implements OnInit {

  JSON_TYPE = 'CASE JSON';
  CSV_TYPE = 'CSV';
  TAXO_LIST_LOCATION = 'taxolist';
  TAXO_DETAIL_LOCATION = 'taxodetail';

  @Input() showSummaryDetails;
  @Input() totalTaxonomyCountToUpload;
  @Input() selectedFileType: string;
  @Input() createdOrUpdated: string;
  @Input() childrenTaxonomiesCount: number; // children_taxonomies_count
  @Input() associationsCount: number; // associations_count
  @Input() newNodeCount: number; // new_node_count
  @Input() updatedNodeCount: number; // updated_node_count
  @Input() deleteNodeCount: number; // delete_node_count
  @Input() newMetadataCount: number; // new_metadata_count
  @Input() updatedItems = [];
  @Input() updatedMetadata = [];
  @Input() errorData = [];
  @Input() viewLocation = this.TAXO_LIST_LOCATION;
  @Input() currentStep: number;
  @Input() totalStepLength: number;
  @Input() totalStep: number;
  @Input() proceedDisabled = false;
  @Input() errorInUpload = false;

  columnsForError = [{ // table columns for CSV error report
    name: 'Error',
    propName: 'Error',
    class: '',
    type: 'text'
  },
  {
    name: 'Row',
    propName: 'Row',
    class: '',
    type: 'text'
  }];

  constructor() { }

  ngOnInit() {
  }

}
