import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Output,
  EventEmitter,
  OnDestroy,
  Input
} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import Utils from '../../utils';
import {
  SharedService
} from '../../shared.service';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  Observable, Subscription
} from 'rxjs';
import {
  HttpClient
} from '@angular/common/http';
import {
  Angular5Csv
} from 'angular5-csv/dist/Angular5-csv';
import { AutoCompleteComponent } from 'src/app/common/auto-complete/auto-complete.component';
const IMPORT_NO_CHANGES = 'Import with no changes'; // 1
const CREATE_COPY = 'Create a copy'; // 2
const IMPORT_AND_UPDATE = 'Import and update URIs to the current server'; // 3
const UPDATE_EXISTING_TAXONOMY = 'Update the existing taxonomy'; // 4
const JSON_TYPE = 'CASE JSON';
const CSV_TYPE = 'CSV';

@Component({
  selector: 'app-import-taxonomy',
  templateUrl: './import-taxonomy.component.html',
  styleUrls: ['./import-taxonomy.component.scss', '../taxonomy-create-options/taxonomy-create-options.component.scss']
})
export class ImportTaxonomyComponent implements OnInit, OnDestroy {

  TAXO_LIST_LOCATION = 'taxolist';
  TAXO_DETAIL_LOCATION = 'taxodetail';
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @Input() viewLocation = this.TAXO_LIST_LOCATION;
  @Input() documentId: any;
  @Input() currentStep: number;
  @Input() totalStepLength: number;
  @Input() csvUpdateOnly = false; /* whether only for CSV update or all option. If true,
                                  // then only taxonomy update otherwise create, update etc. */
  @Output() fileUploadEvent: EventEmitter<any>;
  @Output() fileTypeEvent: EventEmitter<any>;
  @Output() timeStopEvent: EventEmitter<any>; // event to fire when timer is stopped
  @Output() caseServerChangeEvent: EventEmitter<any>; // event to fire when timer is stopped

  selectedFiles: any = [];
  messages = [];
  children_taxonomies_count = 0;
  associations_count = 0;
  sucessUploadCount = 0;
  fileCount = 0;
  updated_node_count = 0;
  blank_updated_node_count = 0;
  updated_metadata_count = 0;
  blank_metadata_count = 0;
  new_metadata_count = 0;
  updatedItems: any = [];
  updatedMetadata: any = [];
  jsonType = JSON_TYPE;
  csvType = CSV_TYPE;
  @Input() selectedFileType = this.jsonType;
  selectedUploadType = 'Desktop';
  serverList = [];
  taxonomyList = [];
  document: any = {
    'document_id': null,
    'document_title': null
  };
  selectedServer: any = null;
  selectedTaxonomy: any = null;
  selectedTaxonomyList = [];
  currentServerId = '';
  CF_status;
  createdOrUpdated;
  new_node_count;
  delete_node_count;
  form: FormGroup;
  importOptions = [];
  importNoChangesText = IMPORT_NO_CHANGES;
  progressEventRef: Subscription;
  uploadDisabled = false; // disable upload while uploading process in progress
  timer: any; // to hold setinterval timer value
  uploadedInTime = false; // to track whether all taxonomy files(JSON) are uploaded in specific times
  counterForTime = 0; // to count timing value in second
  errorData = []; // to display error report for CSV import in summary
  serverTaxonomyLoading = false; // loading state to fetch taxonomy list from server
  disableServer = false;
  customMetadata = [{
    'name': 'Custom metadata Text',
    'field_type': 1
  },
  {
    'name': 'Custom metadata Long Text',
    'field_type': 2
  },
  {
    'name': 'Custom metadata List',
    'field_type': 3
  },
  {
    'name': 'Custom metadata Number',
    'field_type': 6
  },
  {
    'name': 'Custom metadata Date',
    'field_type': 4
  },
  {
    'name': 'Custom metadata Time',
    'field_type': 5
  }
  ];
  @ViewChild('autoCompleteTaxoUrl', { static: false }) autoCompleteTaxoUrl: AutoCompleteComponent;
  @ViewChild('autoCompleteServer', { static: false }) autoCompleteServer: AutoCompleteComponent;
  isImportLoader: boolean;
  loadingServerList = false; // flag to display loader for server list dropdown
  @Output() activateSessionObj: EventEmitter<any>;
  loadingCSV = false;
  constructor(private service: CommonService, private sharedService: SharedService, private http: HttpClient) {
    this.fileUploadEvent = new EventEmitter<any>();
    this.fileTypeEvent = new EventEmitter<any>();
    this.timeStopEvent = new EventEmitter<any>();
    this.caseServerChangeEvent = new EventEmitter<any>();
    this.activateSessionObj = new EventEmitter<any>();
    this.progressEventRef = this.sharedService.progressEvent.subscribe((event: any) => {
      if (event && this.selectedFiles[event.index]) {
        this.selectedFiles[event.index].uploadProgress = event.percentDone;
        /* When all files only uploaded 100% in server without processing work, then after a 5sec
          delay file import status checking API will be called */
        if (event.percentDone === 100 && event.resReceived) {
          this.selectedFiles[event.index].uploadedInServer = true;
          if (this.isAllFilesUploaded(true) && !this.isAllFilesUploaded()) {
            this.timeStopEvent.emit({ 'stopped': false });
            setTimeout(() => {
              this.setUploadStatusForAll();
            }, (Utils.UPLOAD_COUNTER * 1000));
          }
        }
      }
    });
  }

  ngOnInit() {
    this.form = new FormGroup({
      selectUploadType: new FormControl(null),
      fileType: new FormControl(null),
      server: new FormControl(null),
      taxonomy: new FormControl(null),
      importType: new FormControl(null)
    });
  }

  ngOnDestroy() {
    if (this.progressEventRef) {
      this.progressEventRef.unsubscribe();
    }
  }

  clearFiles() {
    this.importOptions = [];
    this.selectedTaxonomyList = [];
    this.taxonomyList = [];
    this.selectedTaxonomy = null;
    this.selectedFiles = [];
    this.selectedServer = null;
    this.clearCount();
    if (this.autoCompleteTaxoUrl) {
      this.autoCompleteTaxoUrl.clearSelection();
    }
    if (this.autoCompleteServer) {
      this.autoCompleteServer.clearSelection();
    }
    this.uploadDisabled = false;
    this.disableServer = false;
    this.isImportLoader = false;
    this.loadingCSV = false;
  }

  clearCount() {
    this.children_taxonomies_count = 0;
    this.associations_count = 0;
    this.sucessUploadCount = 0;
    this.fileCount = 0;
  }

  resetForm() {
    this.form.reset();
  }

  setDefalutSelection() {
    this.selectedFileType = this.jsonType;
    setTimeout(() => {
      if (this.form.controls['fileType']) {
        this.form.controls['fileType'].setValue(this.selectedFileType);
      }
      this.selectedUploadType = 'Desktop';
    }, 500);
  }

  onUploadTypeSelected(value) {
    this.clearFiles(); // clearing all selections
    this.selectedFileType = this.jsonType;
    this.fileTypeEvent.emit(this.selectedFileType);
    this.sendUploadEvent('end', false);
    if (value === 'URL') { // If URL is selected, fetch server list by api calling
      this.getServerList();
    }
  }
  onFilesSelection() {
    if (this.fileInput) {
      const inputFile: HTMLInputElement = this.fileInput.nativeElement as HTMLInputElement;
      console.log(' onFilesSelection  ', inputFile);
      if (this.selectedFileType === this.csvType) {
        // this.selectedFiles = [];
        this.fileCount = this.selectedFiles.length;
      }
      for (let index = 0; index < inputFile.files.length; index++) {
        const file = inputFile.files[index];
        const typeName = this.selectedFileType === this.csvType ? this.csvType : 'JSON';
        const showDelete = true;
        this.initFile(file, null, typeName, showDelete);
        this.selectedFiles.push(file);
      }
      inputFile.value = '';
    }

    if (this.fileCount === this.selectedFiles.length) {
      console.log('user click cancle button');
    } else {
      const length = this.selectedFiles.length;
      this.fileCount = length;
      if (length > 0) {
        this.initProcessForUpload();
      }
    }
  }

  /**
   * First time file initializing before pushing into array
   * @param file (File object)
   * @param name (file name)
   * @param typeName (type name i.e, JSON, CSV etc.)
   * @param showDelete (to show delete button, by default button is hidden)
   * @param importOptions (import options list, by default list is empty)
   * @param importType (selected import type value, by default null)
   * @param identifier (identifier for JSON file, by default null)
   * @param isUploaded (to track file is uploaded or not, by default false)
   * @param uploadProgress (upload progress percentage, by default 0)
   * @param uploadStatus (upload status, by default 'upload not started' status)
   * @param hasError (whether upload error is found or not, by default false)
   * @param message (message for error, by default empty)
   */
  initFile(file: any, name?: string, typeName?: string, showDelete = false, importOptions: any = [], importType: any = null,
    identifier: any = null, isUploaded = false, uploadProgress = 0, uploadStatus = Utils.UPLOAD_NOT_STARTED,
    hasError = false, message = '', importCheck = 'not started', import_identifier: any = null) {

    if (name) {
      file['name'] = name;
    }
    file['typeName'] = typeName ? typeName : null;
    file['message'] = message;
    file['importOptions'] = importOptions; // import type dropdown options initializing
    file['importType'] = importType;
    file['identifier'] = identifier;
    file['isUploaded'] = isUploaded;
    file['showDelete'] = showDelete; // delete button show condition
    file['hasError'] = hasError; // file has error or not
    file['uploadProgress'] = uploadProgress; // upload percentage
    file['uploadStatus'] = uploadStatus; // upload progress status (not started)
    file['importCheck'] = importCheck;
    file['import_identifier'] = import_identifier ? import_identifier : null; // For JSON upload only for checking upload status
    file['index'] = this.selectedFiles.length;
    file['uploadedInServer'] = false;
    file['file'] = file;
  }

  /* Initial function for uploading which will check file type and process type(i.e. using file or URL) and
    based on that it will call respective functions */
  initProcessForUpload() {
    this.disableServer = true;
    this.sendUploadEvent('start', false);
    for (let i = 0; i < this.selectedFiles.length; i++) {
      const file = this.selectedFiles[i];
      if (!file.isUploaded && !file.hasError) {
        if (this.selectedUploadType === 'Desktop' && this.selectedFileType === this.jsonType) { // For JSON
          this.readFile(file);
        } else { // For CSV or URL taxonomy upload
          this.uploadFiles(file, false, null);
        }
      }
    }
  }

  /* File reading for JSON type */
  readFile(file: any) {
    const reader: any = new FileReader();
    reader.onload = () => {
      try {
        const obj = JSON.parse(reader.result);
        const identifier = obj['CFDocument']['identifier'];
        if (identifier) {
          file.identifier = identifier;
          this.uploadFiles(file, false, identifier);
        }
      } catch (ex) {
        file.message = 'Please upload a valid JSON file.';
        file.hasError = true;
        this.sendUploadEvent('end', (this.isAllFilesUploaded() || this.isAllselected()));
        console.log('readFile-------------------- ', ex);
      }
    };
    reader.readAsText(file);
  }

  /**
   * Upload file method
   * @param file (File object)
   * @param index (file index in list)
   * @param isConfirm (boolean value of file going to be final storing process or fetching import options
   *  i.e. [is_ready_to_commit_changes = 1/0])
   * @param identifier (identifier for JSON file, else pass null)
   */
  uploadFiles(file: any, isConfirm: boolean, identifier: any) {
    const fd = new FormData();
    let importType = 0;
    let url = GlobalSettings.IMPORT_CASE_JSON;

    if (this.selectedUploadType === 'Desktop') {
      // CSV file
      if (this.selectedFileType === this.csvType) {
        url = GlobalSettings.UPLOAD_CSV;
        // default sending as '0' for CSV if not selected
        importType = 1;
        fd.append('csvFile', file);
        fd.append('import_pass', '2');
        if (this.viewLocation === this.TAXO_DETAIL_LOCATION) {
          fd.append('document_id', this.documentId);
        }
      } else { // JSON file
        // default sending as '1' for JSON if not selected
        importType = 1;
        if (identifier) {
          fd.append('source_identifier', identifier);
        } else {
          fd.append('case_json', file);
        }
        if (file.import_identifier) { // 'import_identifier' will be needed only for JSON file
          fd.append('import_identifier', file.import_identifier);
        }
      }
    } else {
      url = GlobalSettings.IMPORT_CF_PACKAGE_API_URL;
      importType = 1;
      fd.append('linked_server_id', this.currentServerId);
      fd.append('document_id', file.document_id);
      fd.append('import_identifier', file.import_identifier);
    }
    fd.append('import_type', file.importType ? file.importType : importType);
    fd.append('is_ready_to_commit_changes', isConfirm ? '1' : '0');

    if (isConfirm) { // For final API calling i.e. file data will be stored in database (with 'is_ready_to_commit_changes' as 1)
      file['uploadStatus'] = Utils.UPLOAD_IN_PROGRESS;
      this.service.uploadService(url, fd, file.index).then((res: any) => {
        file['importType'] = null;
        file['isUploaded'] = (this.selectedUploadType === 'Desktop' && this.selectedFileType === this.jsonType) ? false : true;
        file['showDelete'] = false;
        file['hasError'] = false;
        file['importOptions'] = [];
        file['uploadStatus'] = (this.selectedUploadType === 'Desktop' && this.selectedFileType === this.jsonType) ?
          Utils.UPLOAD_IN_PROGRESS : Utils.UPLOAD_FINISHED;
        if (res) {
          if (this.selectedFileType !== this.csvType) {
            this.updateSummaryFromResponse(res);
          } else {
            this.setCSVData(res, file.index); // set import options to upload CSV
          }
        }

        // Checking if all selected files are uploaded successfully, then show message
        let successUpload = false;
        this.uploadedInTime = false;

        if (this.isAllFilesUploaded()) {
          if (this.selectedUploadType === 'Desktop' && this.selectedFileType === this.csvType) { // upload file (csv)
            successUpload = true;
            // For CSV, show success message if created/updated
            this.sharedService.sucessEvent.next({
              customMsg: res.createdOrUpdated ? (res.createdOrUpdated + ' ' + 'Successfully') : null,
              type: 'update_taxonomy'
            });
            this.sendUploadEvent('end', successUpload);
          } else if (this.selectedUploadType === 'URL') { // Import using URL
            successUpload = true;
            this.sendUploadEvent('end', successUpload);
            // this.sharedService.sucessEvent.next({
            //   customMsg: (this.getFilesCountForUploading() > 1 ? 'Taxonomies' : 'Taxonomy') + ' Imported Successfully'
            // });
          }
        }
        this.uploadDisabled = !successUpload;
      }).catch((ex: any) => {
        console.log('Ex ----', ex.msg);
        file['message'] = ex.msg;
        file['hasError'] = true;
        file['importOptions'] = [];
        this.actionAfterJsonImport();
      });
    } else {
      /* To fetch import type options list for first time file uploading */
      this.loadingCSV = this.selectedFileType === this.csvType ? true : false;
      this.service.postService(url, fd, file.index, true).then((response: any) => {
        const res = response.data;

        this.importOptions = [];
        if (res.import_type && res.import_type.length > 0) {
          res.import_type.forEach(element => {
            this.importOptions.push({
              name: this.findImportOptionName(element),
              value: element
            });
            file['hide'] = (element === 1 && this.selectedFileType === this.csvType) ? true : false;
          });
        }
        file['importOptions'] = this.importOptions;
        file['hasError'] = (this.selectedFileType === this.csvType) ? (response.success ? false : true) : false;
        file['import_identifier'] = res.import_identifier;
        if (document.querySelectorAll('.custom-select.import-type').length > 0) {
          setTimeout(() => {
            if (document.querySelectorAll('.custom-select.import-type')[0]) {
              (document.querySelectorAll('.custom-select.import-type')[0] as HTMLElement).focus();
            }
          }, 500);
        }
        if (this.selectedFileType !== this.csvType) {
          this.updateSummaryFromResponse(res);
        } else {
          file['uploadFinished'] = true;
          this.setCSVData(response, file.index);
          if (res.package_exist && !this.csvUpdateOnly) {
            this.sharedService.sucessEvent.next({
              customMsg: 'This taxonomy already exists',
              type: 'error'
            });
          }
          if (file.hide || this.csvUpdateOnly) {
            file['importType'] = file.hide ? 1 : 4;
            this.onImportTypeSelected('');
          }
          if (file.hasError) {
            this.sendUploadEvent('end', this.isAllselected());
          }
          const successUpload = file['importOptions'].length > 0 ? (file['importType'] !== null ? true : false) : true;

        }
        if (file['importOptions'].length === 0 && file['importType'] === 1 && this.isAllFilesUploaded()) {
          // this.sendUploadEvent('end',true)
        }
        // For CSV file, show alert if taxonomy already exists
      }).catch((ex: any) => {
        console.log('Ex ----', ex.msg);
        file['message'] = ex.msg;
        file['hasError'] = true;
        this.sendUploadEvent('end', (this.isAllFilesUploaded() || this.isAllselected()));
      });
    }

  }

  /* To start final file import process which actually will store data in database */
  startUploadProcessForAllFiles() {
    this.uploadDisabled = true;
    this.disableServer = false;
    for (let i = 0; i < this.selectedFiles.length; i++) {
      if (!this.selectedFiles[i].hasError && !this.selectedFiles[i].isUploaded) {
        if (this.selectedFileType === this.csvType) {
          // call pass 3 API for uploading CSV
          const body = {
            import_pass: 3,
            batch_id: this.selectedFiles[i].batch_id,
            import_type: this.selectedFiles[i].importType ? this.selectedFiles[i].importType : 1
          };
          this.callUploadCSV(body);
          this.sharedService.csvStatusUpdateEvent.next({ checkStatus: true });
        } else {
          this.uploadFiles(this.selectedFiles[i], true, null);
        }
        if (this.selectedFiles[i].subscription) {
          this.callSubscribeAPI(this.selectedFiles[i]);
        }
      }
    }
  }

  /**
   * To checking all files uploaded from valid files
   * @param onlyInServer (boolean value: to inicate whether to check all files uploaded totally or only in server
   * If true, that means to check only in server, otherwise total upload with all processing)
   */
  isAllFilesUploaded(onlyInServer = false) {
    if (this.selectedFiles.length > 0) {
      const filteredList = this.selectedFiles.filter(element => { // excluding errored files
        return !element['hasError'];
      });
      if (filteredList.length > 0) {
        if (onlyInServer) { // If only uploaded in server without any processing
          return filteredList.every(element => {
            return (element['uploadedInServer'] === true);
          });
        } else { // uploaded with processing complete
          return filteredList.every(element => {
            return (element['isUploaded'] === true);
          });
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /* To check all import type dropdown selected or not for valid files */
  isAllselected(): boolean {
    if (this.selectedFiles.length > 0) {
      const filteredList = this.selectedFiles.filter(element => { // excluding uploaded and errored files
        // if (this.selectedFileType === this.csvType) {
        //   return (!element['hasError'] && element['isUploaded']);
        // }
        return (!element['hasError'] && !element['isUploaded']);
      });
      if (filteredList.length > 0) {
        return filteredList.every(element => {
          return (element['importType'] !== null);
        });
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * To check all files are in import status checking stage for synchronously handling of API
   */
  isAllImportCheckingFinished(): boolean {
    if (this.selectedFiles.length > 0) {
      const filteredList = this.selectedFiles.filter(element => { // excluding errored files
        return (!element['hasError']);
      });
      if (filteredList.length > 0) {
        return filteredList.every(element => {
          return (element['importCheck'] === 'finished');
        });
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getFilesCountForUploading(): number {
    let count = 0;
    if (this.selectedFiles.length > 0) {
      const filteredList = this.selectedFiles.filter(element => { // excluding errored files
        return (!element['hasError']);
      });
      count = filteredList.length;
    }
    return count;
  }

  /**
   * To check whether all uploaded files are in errored state
   */
  isAllFilesInError(): boolean {
    if (this.selectedFiles.length > 0) {
      return this.selectedFiles.every(element => {
        return (element['hasError'] === true);
      });
    } else {
      return false;
    }
  }

  /* To click on 'proceed with import data' button for CSV file import */
  onConfirmClicked(index) {
    this.selectedFiles[index].importType = this.selectedFiles[index].importType ? this.selectedFiles[index].importType : 2;
    this.sendUploadEvent('start', false);
    this.startUploadProcessForAllFiles();
  }

  sendUploadEvent(type, isSuccess?) {
    if (this.selectedFileType !== this.csvType) {
      this.fileUploadEvent.emit({
        type,
        uploadType: this.selectedUploadType,
        filetype: this.selectedFileType,
        taxonomies_count: this.children_taxonomies_count,
        associations_count: this.associations_count,
        taxonomy_count: this.sucessUploadCount,
        document_id: this.document.document_id,
        document_title: this.document.document_title,
        successUpload: isSuccess,
        uploadedInTime: this.uploadedInTime,
        totalTaxonomyInUploadState: this.getFilesCountForUploading()
      });
    } else {
      // this.isImportLoader = isSuccess === false && type === 'start' ? true : false;
      this.fileUploadEvent.emit({
        type,
        uploadType: this.selectedUploadType,
        filetype: this.selectedFileType,
        updated_node_count: this.updated_node_count,
        blank_updated_node_count: this.blank_updated_node_count,
        updatedItems: this.updatedItems,
        updatedMetadata: this.updatedMetadata,
        updated_metadata_count: this.updated_metadata_count,
        blank_metadata_count: this.blank_metadata_count,
        new_metadata_count: this.new_metadata_count,
        createdOrUpdated: this.createdOrUpdated,
        new_node_count: this.new_node_count,
        delete_node_count: this.delete_node_count,
        associations_count: this.associations_count,
        taxonomy_count: this.sucessUploadCount,
        document_id: this.document.document_id,
        document_title: this.document.document_title,
        successUpload: isSuccess,
        totalTaxonomyInUploadState: this.getFilesCountForUploading(),
        errorData: this.errorData
      });
    }
  }
  formatBytes(bytes) {
    return Utils.formatBytes(bytes);
  }

  onFileTypeSelected() {
    this.clearFiles();
    this.fileTypeEvent.emit(this.selectedFileType);
  }

  // On import type dropdown change event
  onImportTypeSelected(event?: any) {
    if (this.selectedUploadType === 'Desktop' || this.selectedUploadType === 'URL') { // upload file (json/csv)
      this.sendUploadEvent('end', this.isAllselected());
    }
  }

  getServerList() {
    const url = GlobalSettings.GET_SERVER_LIST;
    this.loadingServerList = true;
    this.service.getServiceData(url).then((res: any) => {
      this.serverList = res;
      this.loadingServerList = false;
    });
  }

  getServerTaxonomyList() {
    this.caseServerChangeEvent.emit(true);
    this.taxonomyList = [];
    this.selectedTaxonomy = null;
    this.selectedTaxonomyList = [];
    this.currentServerId = null;

    if (this.selectedServer && this.selectedServer.linked_server_id) {
      this.serverTaxonomyLoading = true;
      this.currentServerId = this.selectedServer.linked_server_id;
      const url = GlobalSettings.GET_SERVER_TAXONOMY_LIST + '/' + this.currentServerId;
      this.service.getServiceData(url).then((res: any) => {
        if (res instanceof Array) {
          this.taxonomyList = res;
        }
        this.serverTaxonomyLoading = false;
      }).catch(err => {
        this.serverTaxonomyLoading = false;
        console.log(err);
      });
    }
  }

  /**
   * First time, default importType and isConfirm will be sent as 1 and 0 respectively.
   * isConfirm is sent as 0 for getting import type option values. Then selected import type will be sent as importType and
   * along with isConfirm will be 1 for importing finally.
   * @param importType (import type value)
   * @param isConfirm (value to permanently store (0/1))
   */
  onTaxonomySelected(isConfirm: boolean, selectedTaxoList: any[]) {
    this.selectedFileType = this.jsonType;
    let newTaxonomyFound = false;
    if (!isConfirm) { // when taxonomy is selected for first time to know import options
      selectedTaxoList.forEach(element => {
        this.initFile(element, element.taxonomy_name, 'API', true);
        if (this.selectedFiles.find((file: any) => file.document_id === element.document_id) === undefined) {
          this.selectedFiles.push(element);
          newTaxonomyFound = true;
        }
      });
    }
    if (newTaxonomyFound) {
      // If any new taxonomy added from dropdown, then process for api call
      this.initProcessForUpload();
    }
  }

  findImportOptionName(value: number) {
    if (value === 1) {
      return IMPORT_NO_CHANGES;
    } else if (value === 2) {
      return CREATE_COPY;
    } else if (value === 3) {
      return IMPORT_AND_UPDATE;
    } else if (value === 4) {
      return UPDATE_EXISTING_TAXONOMY;
    } else {
      return null;
    }
  }

  downloadSample(evt) {

    let sampleData;
    this.getJSON().subscribe(sample_data => {
      sampleData = sample_data;
      this.exportCSV(sampleData, 'sample_csv');
    });


  }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/json/sample_csv.json');
  }

  exportCSV(data, name) {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: ['action', 'node_id', 'parent/destination_node_id', 'case_association_type']
    };

    if (name === 'sample_csv') {
      options.headers.push('case_title', 'case_full_statement', 'case_human_coding_scheme',
        'case_list_enumeration', 'case_node_type', 'case_education_level');
    }

    this.customMetadata.forEach(item => {
      options.headers.push(item.name + '(' + Utils.getCustomMetadataType(item.field_type) + ')');
    });
    let csvObj = new Angular5Csv(data, name, options);
    csvObj = null;
  }

  closeTaxonomyBuildModal() {
    if (document.getElementById('closecreateTaxonomy')) {
      document.getElementById('closecreateTaxonomy').click();
    }
  }

  // File deleted event
  onFileDeleted(event: any) {
    if (this.autoCompleteTaxoUrl) {
      this.autoCompleteTaxoUrl.unselectObjInList(event.data);
    }
    if (this.selectedFileType === this.csvType) {
      this.cancelCSVImport(event.data.batch_id, event.data.index);
    }
    this.sendUploadEvent('deleted', (this.isAllFilesUploaded() || this.isAllselected()));
    this.fileCount = this.selectedFiles.length;
  }

  // For JSON/CSV file upload
  updateSummaryFromResponse(res: any) {
    let updateDoc = []; // for document changes in CSV
    let updateItems = []; // for item changes in CSV
    this.new_node_count = 0;
    this.delete_node_count = 0;
    this.createdOrUpdated = 0;
    this.updated_metadata_count = 0;
    this.blank_metadata_count = 0;
    this.new_metadata_count = 0;
    this.updated_node_count = 0;
    this.blank_updated_node_count = 0;
    this.updatedItems = []; // it includes total documents and items changes
    this.updatedMetadata = [];
    this.errorData = [];
    if (res.new_node_count) {
      this.new_node_count = res.new_node_count;
    }
    if (res.delete_node_count) {
      this.delete_node_count = res.delete_node_count;
    }
    if (res.createdOrUpdated) {
      this.createdOrUpdated = res.createdOrUpdated;
    }
    if (res.updated_metadata_count) {
      this.updated_metadata_count = res.updated_metadata_count;
    }
    if (res.blank_metadata_count) {
      this.blank_metadata_count = res.blank_metadata_count;
    }
    if (res.new_metadata_count) {
      this.new_metadata_count = res.new_metadata_count;
    }

    this.document.document_id = null;
    this.document.document_title = null;
    if (res.children_taxonomies_count) {
      this.children_taxonomies_count += res.children_taxonomies_count;
      this.associations_count += res.associations_count;
      this.sucessUploadCount += 1;
      // setting document id and document title only for one file upload
      if (res.document_id && res.document_title) {
        this.document.document_id = res.document_id;
        this.document.document_title = res.document_title;
      }
    }
    if (res.updated_node_count || res.blank_updated_node_count || res.blank_updated_count) {
      this.updated_node_count = res.updated_node_count;
      this.blank_updated_node_count = res.blank_updated_node_count ? res.blank_updated_node_count : res.blank_updated_count;

      /* Building changed attributes value dynamically for document and items */
      if (res.document && !(res.document instanceof Array)) {
        updateDoc = this.getReConstructedArrayAsKeyValue(res.document);
      }
      if (res.item && !(res.item instanceof Array)) {
        updateItems = this.getReConstructedArrayAsKeyValue(res.item);
      } else {
        // This should not be the case, becasue when update item update count is 0 then res.item is array otherwise its object
      }

      this.updatedItems = updateDoc.concat(updateItems);
    }

    // if (this.selectedFileType === this.csvType) {
    if (res.new_associations_count) {
      this.associations_count = res.new_associations_count;
    }
    if (res.metadata) {
      this.updatedMetadata = res.metadata;
    }
    if (res.errorData) {
      this.errorData = res.errorData;
    }
  }

  /**
   * To re-structure object as proper key value pairing
   * @param object (object type value)
   */
  getReConstructedArrayAsKeyValue(object: any) {
    const newArray = [];
    if (object) {
      for (const id of Object.keys(object)) {
        if (object[id] && !(object[id] instanceof Array)) {
          if (Object.keys(object[id]).length > 0) {
            Object.keys(object[id]).forEach(attribute => {
              if (typeof object[id][attribute] === 'object') {
                object[id][attribute]['key'] = attribute; // setting each key in one parameter ('key')
              }
              if (object[id][attribute]['old_' + attribute] || object[id][attribute]['new_' + attribute]) {
                // If old or new any one value exists, push it in the array
                newArray.push(object[id][attribute]);
              }
            });
          }
        }
      }
    }
    return newArray;
  }

  /**
   * To check and set upload status for JSON file baesd on import identifier value
   * @param file (selected file of which status will be set)
   */
  checkImportStatus(file: any) {
    const requestId = file.import_identifier;
    const url = GlobalSettings.TAXONOMY_IMPORT_STATUS + '/' + requestId;
    if (!file.isUploaded && requestId) {
      file.importCheck = 'started';
      this.service.getServiceData(url).then((res: any) => {
        const response = res;
        if (response) {
          if (response.status === Utils.UPLOAD_FINISHED) { // Upload successful
            file.isUploaded = true;
            file.uploadStatus = Utils.UPLOAD_FINISHED;
            file.hasError = false;
          } else if (response.status === Utils.UPLOAD_ERROR) { // Error in upload
            file.isUploaded = false;
            file.hasError = true;
          } else if (response.status === Utils.UPLOAD_IN_PROGRESS) { // Upload work in progress
            file.isUploaded = false;
            file.hasError = false;
            file.uploadStatus = Utils.UPLOAD_IN_PROGRESS;
          } else if (response.status === Utils.UPLOAD_NOT_STARTED) { // Upload not started
            file.isUploaded = false;
            file.hasError = false;
            file.uploadStatus = Utils.UPLOAD_NOT_STARTED;
          }
        }
        file.importCheck = 'finished';
        this.actionAfterJsonImport();
      }).catch(ex => {
        file.isUploaded = false;
        file.hasError = true;
        this.actionAfterJsonImport();
        console.log('Taxonomy import status ex ', ex);
      });
    }
  }

  setUploadStatusForAll() {
    if (this.isAllImportCheckingFinished()) {
      this.actionAfterJsonImport();
    } else {
      for (const elem of this.selectedFiles) {
        if (elem.importCheck !== 'started' && elem.importCheck !== 'finished') {
          this.checkImportStatus(elem);
        }
      }
    }
  }

  /**
   * Method called to be after JSON upload for any success or failure actions
   */
  actionAfterJsonImport() {
    let successUpload = false;
    if (this.isAllImportCheckingFinished()) {
      if (this.isAllFilesUploaded()) {
        this.uploadedInTime = true;
        successUpload = true;
        this.sharedService.sucessEvent.next({
          customMsg: (this.getFilesCountForUploading() > 1 ? 'Taxonomies' : 'Taxonomy') + ' Imported Successfully'
        });
      } else {
        this.uploadedInTime = false;
      }
      this.timeStopEvent.emit({ stopped: true });
    }
    this.sendUploadEvent('end', successUpload);
    this.uploadDisabled = !successUpload;
  }

  // Clicking outside of auto complete dropdown, close opened dropdown
  onClickedOutside(e: any, type: string) {
    if (type === 'taxonomy') { // Taxonomy dropdown
      if (this.autoCompleteTaxoUrl) {
        this.autoCompleteTaxoUrl.openPanel(false);
      }
    } else if (type === 'server') { // server dropdown
      if (this.autoCompleteServer) {
        this.autoCompleteServer.openPanel(false);
      }
    }
  }

  activateSession() {
    this.activateSessionObj.emit(true);
  }

  setCSVData(res, index) {

    console.log('upload csv response', res);
    const file = this.selectedFiles[index];
    if (!res.success && res.message.length) {
      file.message = res.message.join('. ');
    }
    file.batch_id = res.data.batch_id;
    const isAllUploaded = this.selectedFiles.every((data: any) => data['uploadFinished']);
    if (isAllUploaded) {
      this.loadingCSV = false;
      this.uploadDisabled = false;
    }
  }


  callUploadCSV(bodyObj) {
    const url = GlobalSettings.UPLOAD_CSV;
    this.service.postService(url, bodyObj, 0, true).then((response: any) => {
      if (!(response.success)) {
        this.sharedService.sucessEvent.next({
          customMsg: response.message,
          type: 'error'
        });
      }
      // console.log(response);
    });
  }

  cancelCSVImport(batchId, index) {
    const url = GlobalSettings.CANCEL_CSV_IMPORT + '/' + batchId;
    const body = {};
    this.service.deleteServiceData(url).then((res: any) => {
      console.log('CSV import cancelled', res);
    });
  }

  callSubscribeAPI(item) {
    const idx = this.selectedServer.server_url.indexOf('ims/case/');
    const serverURL = this.selectedServer.server_url.substr(0, idx);
    const url = serverURL + 'subscription';
    const body = {
      document_id: item.document_id
    };
    this.service.postService(url, body, 0, true, false).then(res => {
      console.log('subscribed', res);
    });
  }

}
