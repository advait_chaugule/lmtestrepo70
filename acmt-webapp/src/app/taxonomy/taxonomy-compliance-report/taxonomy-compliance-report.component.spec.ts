import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyComplianceReportComponent } from './taxonomy-compliance-report.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedService } from 'src/app/shared.service';

describe('TaxonomyComplianceReportComponent', () => {
  let component: TaxonomyComplianceReportComponent;
  let fixture: ComponentFixture<TaxonomyComplianceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxonomyComplianceReportComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, ConfirmationDialogService, SharedService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomyComplianceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create TaxonomyComplianceReportComponent', () => {
    expect(component).toBeTruthy();
  });
});
