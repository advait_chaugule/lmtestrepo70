import { TestBed, inject } from '@angular/core/testing';

import { CommonService } from './common.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedService } from './shared.service';

describe('CommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, SharedService]
    });
  });

  it('should be created CommonService', inject([CommonService], (service: CommonService) => {
    expect(service).toBeTruthy();
  }));
});
