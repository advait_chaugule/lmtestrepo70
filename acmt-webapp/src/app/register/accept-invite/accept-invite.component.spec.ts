import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptInviteComponent } from './accept-invite.component';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';

describe('AcceptInviteComponent', () => {
  let component: AcceptInviteComponent;
  let fixture: ComponentFixture<AcceptInviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AcceptInviteComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [CommonService, SharedService, ConfirmationDialogService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptInviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create AcceptInviteComponent', () => {
    expect(component).toBeTruthy();
  });
});
