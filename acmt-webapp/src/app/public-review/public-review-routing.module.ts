import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {
  ReviewListComponent
} from './review-list/review-list.component';
import { ReviewCommentComponent } from './review-comment/review-comment.component';

const routes: Routes = [{
  path: 'list',
  component: ReviewListComponent
},
{
  path: 'review/:id',
  component: ReviewCommentComponent
}, {
  path: 'reviewed/:id',
  component: ReviewCommentComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicReviewRoutingModule { }
