import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicReviewRoutingModule } from './public-review-routing.module';
import { ReviewListComponent } from './review-list/review-list.component';
import { ReviewCommentComponent } from './review-comment/review-comment.component';
import { SharedModule } from '../shared/shared.module';
import { PublicModule } from '../shared/public.module';
import { Shared2Module } from '../shared/shared2/shared2.module';
import { Shared3Module } from '../shared/shared3/shared3.module';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';

@NgModule({
  imports: [
    CommonModule,
    PublicReviewRoutingModule,
    SharedModule,
    Shared2Module,
    Shared3Module,
    PublicModule,
    PreLoaderModule
  ],
  declarations: [
    ReviewListComponent,
    ReviewCommentComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PublicReviewModule { }
