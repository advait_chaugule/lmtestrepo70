import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewCommentComponent } from './review-comment.component';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { TreeDataService } from 'src/app/tree-data.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';

describe('ReviewCommentComponent', () => {
  let component: ReviewCommentComponent;
  let fixture: ComponentFixture<ReviewCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewCommentComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [CommonService, SharedService, TreeDataService, AuthenticateUserService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ReviewCommentComponent', () => {
    expect(component).toBeTruthy();
  });
});
