import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {
  Subscription,
} from 'rxjs/Subscription';
import {
  from,
} from 'rxjs';

import {
  ProjectNotesComponent
} from '../../project/projects-notes/projects-notes.component';
import {
  CommonService
} from '../../common.service';
import {
  ActivatedRoute,
  Router,
  Params,
  NavigationEnd
} from '@angular/router';
import {
  SharedService
} from '../../shared.service';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  GlobalSettings
} from '../../global.settings';
import Utils from '../../utils';
import {
  ItemDetailsComponent
} from '../../common/item-details/item-details.component';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import { TreeComponent } from '../../common/tree/tree.component';


@Component({
  selector: 'app-review-comment',
  templateUrl: './review-comment.component.html'
  // styleUrls: ['./review-comment.component.scss']
})
export class ReviewCommentComponent implements OnInit, OnDestroy {

  NODE_ENABLE_NUMBER = 2; /* indicates the document node's children node postion upto which respective
   nodes will remain enabled and rest as disabled until all enabled nodes will contain self comments */

  isAllNode = true;
  isFirstTime = true;
  isEditable = false;
  commentNotification = false;
  viewCommentList = true;
  projectId;
  taxonomyData;
  associationTypes: any;
  itemAdditionalMetadata: any;
  itemAssociations: any = [];
  itemLinkedNodes: any = [];
  itemExemplars: any = [];
  itemAssets: any = [];
  selectedNodeDetail: any;
  projectDetail: any;
  defaultNodeType = 'Default';
  rootNodeType = 'Document';
  selectNodeItemID: string;
  intervalTimer = null;
  nodetypeData;
  selectedNodeType = {
    title: '',
    node_type_id: ''
  };
  nodeSelectedEvent: Subscription;
  projectTreeViewEvent: Subscription;
  acivatedRouteEvent: Subscription;
  selectedNode: any = {
    item_id: '',
    id: ''
  };
  selectedNodeId: any;
  view = 'project-authoring';
  showAssocciation = false;
  showExemplar = false;
  showDocument = false;
  showAdditionalMetadata = false;
  protipType = 'publicreview';
  showProtip = false;
  commentData: any[] = [];
  nodesCommented = true;
  comment_total_nodes;
  comment_done_nodes;
  review_completion_percentage;
  list_pending_nodes = [];
  commentsLoaded = false;
  // submitReview = false;
  editComments = true;
  submitPublicReview = false;
  showNoComment = false;
  showNav = {
    value: false
  };
  showNavigation = true; // holds boolean value for showing up navigation arrow
  lastNode;
  @ViewChild('projectNotesComponentId', { static: false }) projectNotesComponent: ProjectNotesComponent;
  @ViewChild('itemDetailsComponent', { static: false }) itemDetailsComponent: ItemDetailsComponent;
  @ViewChild('tree', { static: false }) tree: TreeComponent;
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring
  constructor(
    private service: CommonService,
    private acivatedRoute: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService,
    private treeDataService: TreeDataService, private AuthenticateUser: AuthenticateUserService) {


    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: any) => {

      if (event.url.indexOf('reviewed') !== -1) {
        this.editComments = false;

      }
    });

    // Subscripbe to tree node selected event
    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      if (item && item.location === 'public_review') {
        if (item.item_id || item.id) {
          this.selectedNode = item;
          /*if (item['is_editable']) {
            this.isEditable = item['is_editable'] === (0 || 2) ? false : true;
          } else {
            this.isEditable = false;
          }*/
          this.getSelectedTreeItemDetail(item.item_id ? item.item_id : item.id);
        }
      }
    });

    this.sharedService.getCommentReport.subscribe((event: any) => {
      if (event.getCommentReport !== undefined) {
        this.getCommentReport();
      }
    });
    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.selectedStageId = event.currentWorkflowStage;
          this.userRoleRight();
        }, 1000);
      }
    });
  }

  /**
   * Componet Life Cycle Method
   */
  ngOnInit() {
    this.isEditable = false;
    this.showProtip = true;
    this.nodesCommented = true;
    // Get the project id from url
    this.acivatedRouteEvent = this.acivatedRoute.params.subscribe((params: Params) => {
      if (params['id'] !== undefined) {
        this.projectId = params['id'];
        localStorage.setItem('project_id', this.projectId);
        this.getProjectDetail();
        this.getAllNodes(Utils.EXPAND_LEVEL);
        this.userRoleRight();
        // this.startLogProjectActivity();
        this.getAllMetadata();
        Utils.removeBodyScroll();
      }
    });

    // Subscribe event for  Complete & Assigned tab (defined in base component)
    this.projectTreeViewEvent = this.sharedService.projectTreeViewEvent.subscribe((type) => {
      if (this.isFirstTime === false) {
        this.taxonomyData = null;
        if (type === 'inactive') {
          this.isAllNode = true;
          this.selectedNode = {
            item_id: '',
            id: ''
          };
          this.getAllNodes(Utils.EXPAND_LEVEL);
        }
      } else {
        this.isFirstTime = false;
        // this.isEditable = true;
      }
    });
  }

  /**
   * Componet Life Cycle Method
   */
  ngOnDestroy() {
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    if (this.projectTreeViewEvent) {
      this.projectTreeViewEvent.unsubscribe();
    }
    if (this.acivatedRouteEvent) {
      this.acivatedRouteEvent.unsubscribe();
    }
    if (this.intervalTimer) {
      clearInterval(this.intervalTimer);
    }
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
    Utils.addBodyScroll();
  }

  /**
   * Get Project Detail
   */
  getProjectDetail() {
    const url = GlobalSettings.PROJECT_LIST + '/' + this.projectId;
    this.service.getServiceData(url).then((res) => {
      this.projectDetail = res;
      // Emit event to base component to update the title
      this.sharedService.setTitleEvent.next({
        'type': 'public_review',
        'title': this.projectDetail.project_name
      });
      // if (res['review_status'] && res['review_status'] === 3) {
      //   this.editComments = false;
      // } else {
      //   this.editComments = true;
      // }
    }).catch((ex) => {
      console.log('getProjectDetail ', ex);
    });
  }

  /**
   * Get Project Tree Data With All Nodes
   */
  getAllNodes(level) {
    this.isAllNode = true;
    const url = GlobalSettings.GET_MAPPED_NODES_ALL + '/' + this.projectId;
    this.treeDataService.getTreeData(url, true, level).then((res: any) => {
      if (res) {
        // Sort the tree according to list enumaration
        // Utils.sortData(res.parsedTreeNodes['children']);
        this.taxonomyData = res.parsedTreeNodes;
        if (this.taxonomyData.children && this.taxonomyData.children.length > 0) {
          this.selectedNode = this.taxonomyData.children[0];
          this.selectedNodeId = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
          this.getDocumentComments(this.selectedNodeId);
        }
        setTimeout(() => {
          // if (document.getElementById('01')) {
          // document.getElementById('01').click();
          // if (this.treeAccordianComponet) {
          //   this.treeAccordianComponet.expendNode(this.selectedNode.id);
          //   this.treeAccordianComponet.onNodeSelected(this.selectedNode);
          // }
          // }
          if (document.getElementById(this.taxonomyData.children[0]['id'] + '-node')) {
            document.getElementById(this.taxonomyData.children[0]['id'] + '-node').click();
          }
          publicReviewHeightCalculation();
        }, 1000);

      }
    }).catch((ex) => {
      console.log('getProjectMappedNodes ', ex);
    });
  }

  /**
   * Get The Selected Node Detail By ItemId
   * @param  {string} itemId
   */
  getSelectedTreeItemDetail(itemId: string) {

    let url;
    if (this.selectedNode['is_document'] === 1) {
      url = GlobalSettings.GET_CFDOC_ITEM_DETAIL + '/' + itemId + '?project_id=' + this.projectId;
    } else {
      url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + itemId + '?project_id=' + this.projectId;
    }
    this.itemAssociations = [];
    this.itemLinkedNodes = [];
    this.itemExemplars = [];
    this.itemAssets = [];
    this.itemAdditionalMetadata = [];
    this.showAssocciation = false;
    this.showAdditionalMetadata = false;
    this.showExemplar = false;
    this.showDocument = false;
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        // If Selected Node Is Document Node
        if (this.selectedNode['is_document'] === 1) {
          if (!res.node_type_id) {
            this.setNodeTypeByTitle(this.rootNodeType);
            res['node_type_id'] = this.selectedNodeType.node_type_id;
            res['node_type'] = this.selectedNodeType.title;
          }
          res['source_uuid_id'] = res['source_document_id'];
        } else {
          res['source_uuid_id'] = res['source_item_id'];
        }
        // Check the node node_type and set it into UI
        if (res.node_type_id && res.node_type_id.length > 0) {
          console.log('OnNodeSeltected IF ', res.node_type_id);
          this.setNodeTypeById(res.node_type_id);
        } else {
          console.log('OnNodeSeltected ELSE ', res.node_type_id, this.defaultNodeType);
          // If node_type is undefined then set it to defaultNodeType
          from(this.nodetypeData)
            .filter((w: any) => w.title === this.defaultNodeType)
            .subscribe(result => {
              this.selectedNodeType = JSON.parse(JSON.stringify(result));
              res['node_type_id'] = result.node_type_id;
              res['node_type'] = result.title;
            });
        }

        if (this.itemDetailsComponent) {
          this.itemDetailsComponent.generateFormData(res, this.selectedNodeType.title);
        }
        this.selectedNodeDetail = res;
        this.selectNodeItemID = itemId;
        if (this.projectNotesComponent) {
          this.projectNotesComponent.getItemComments(itemId);
        }
        setTimeout(() => {
          if (res.item_associations) {
            this.itemAssociations = res.item_associations;
            this.showAssocciation = true;
          }
          if (res.linked_item) {
            this.itemLinkedNodes = res.linked_item;
          }
          if (res.exemplar_associations) {
            this.itemExemplars = res.exemplar_associations;
            this.showExemplar = true;
          }
          if (res.assets) {
            this.itemAssets = res.assets;
            this.showDocument = true;
          }
          if (res.custom_metadata) {
            this.itemAdditionalMetadata = [];
            if (res.custom_metadata) {
              res.custom_metadata.forEach(obj => {
                if (obj.is_additional_metadata === 1) {
                  this.itemAdditionalMetadata.push(obj);
                }
              });
            }
            if (this.itemAdditionalMetadata.length > 0) {
              this.showAdditionalMetadata = true;
            }
          }
        }, 600);
        const rightContainer = document.getElementById('right_container_id');
        if (rightContainer) {
          rightContainer.scrollTop = 0;
        }
        document.getElementById('mySidenav').style.right = '-250%';
      }
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
    });
  }

  userRoleRight() {
    // this.viewCommentList = this.AuthenticateUser.AuthenticateComment('View Comments');
    // const viewComments = Utils.checkProjectPermission(this.projectId, 'View Comments', 'comment_permissions',this.selectedStageId);
    // if (viewComments.hasProject) {
    //   this.viewCommentList = viewComments.valid;
    // } Submit Public Review
    const self = this;
    this.AuthenticateUser.authenticatePublicReview('Submit Public Review', function (val) {
      self.submitPublicReview = val;
      console.log('Global Create comment', val);
      const submitReview = Utils.checkProjectPermission(self.projectId, 'Submit Public Review', 'public_review_permissions', self.selectedStageId);
      if (submitReview.hasProject) {
        self.submitPublicReview = submitReview.valid;
        console.log('Submit Public Review', submitReview.valid);
      }
    });

  }

  setCommentNotificationStatus(status) {
    this.commentNotification = status;
  }

  selectedNodeTop(top) {
    console.log('selectedNodeTop  ', top);
    const treeContainer = document.getElementById('tree_container_id');
    if (treeContainer) {
      const height = window.innerHeight;
      let diff = top - height;
      if (diff > 0) {
        diff += 50;
      }
      treeContainer.scrollTop += diff;
    }
  }

  /*startLogProjectActivity() {
    this.intervalTimer = setInterval(() => this.logProjectSessionActivity(), 1000 * 60);
  }*/

  logProjectSessionActivity() {
    const url = GlobalSettings.SESSION_ACITIVITY + '/' + this.projectId;
    this.service.postService(url, {}).then((res) => { }).catch((ex) => {
      console.log(' Log Activity Catch', ex);
    });

  }

  getAllMetadata() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.nodetypeData = res.nodetype;
    })
      .catch((err: any) => { });
  }

  /**
   * Set selectedNodeType by title
   * @param  {} title
   */
  setNodeTypeByTitle(title) {
    if (this.nodetypeData) {
      from(this.nodetypeData)
        .filter((w: any) => w.title === title)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
        });
    }
  }

  /**
   * Set selectedNodeType by node_type_id
   * @param  {} node_type_id
   */
  setNodeTypeById(node_type_id) {
    if (this.nodetypeData) {
      from(this.nodetypeData)
        .filter((w: any) => w.node_type_id === node_type_id)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
        });
    }
  }

  /**
   * Getting document comment
   * @param documentId
   */

  // kunal
  getDocumentComments(documentId) {
    if (documentId) {
      const url = GlobalSettings.USER_DOCUMENT_COMMENTS + '/' + documentId;
      this.service.getServiceData(url).then((res: any) => {
        if (res) {
          this.commentData = JSON.parse(JSON.stringify(res));
          if (this.taxonomyData.children[0] && this.taxonomyData.children[0].children[0]) {
            this.taxonomyData.children[0].hasComment = true; // taxonomyData.children[0] indicates root taxonomy
            this.setHasCommentForAllNode(this.taxonomyData.children[0], false); // For first level i.e. children[0]
            this.taxonomyData.children[0]['expand'] = true;
            for (let i = 0; i < this.NODE_ENABLE_NUMBER; i++) { // setting hasComment
              if (this.taxonomyData.children[0].children[i]) {
                this.setHasComment(this.taxonomyData.children[0].children[i]);
              }
            }
            this.nodesCommented = true;
            for (let i = 0; i < this.NODE_ENABLE_NUMBER; i++) { // checking whether all enabled nodes are commented or not
              if (this.taxonomyData.children[0].children[i]) {
                this.checkAllNodesCommented(this.taxonomyData.children[0].children[i]);
              }
            }
          }

          // Finding last node of tree based on enabled nodes commented
          if (this.taxonomyData.children[0].children.length > this.NODE_ENABLE_NUMBER && !this.nodesCommented) {
            this.lastNode = this.findLastNode(this.taxonomyData.children[0].children[this.NODE_ENABLE_NUMBER - 1]);
          } else {
            this.lastNode = this.findLastNode(this.taxonomyData);
          }
          console.log('First Level All Nodes commented: ' + this.nodesCommented);
        }
        // console.log('getDocumentComments: ', this.commentData);
      }).catch(ex => {
        console.log('getDocumentComments ex: ', ex);
      });
    }
  }

  setHasCommentForAllNode(data: any, expand) {
    if (data) {
      if (data.children.length > 0) {
        // data.expand = expand;
        data.isCommented = this.isCommentAvailableForItem(data.id);
        for (let i = 0; i < data.children.length; i++) {
          this.setHasCommentForAllNode(data.children[i], expand);
        }
      } else {
        // data.expand = expand;
        data.isCommented = this.isCommentAvailableForItem(data.id);
      }
    }
  }

  /**
   * To check item has comment or not by current user
   * @param itemId
   */
  isCommentAvailableForItem(itemId) {
    let flag = false;
    if (itemId) {
      for (let i = 0; i < this.commentData.length; i++) {
        if ((itemId === this.commentData[i].thread_source_id) && (this.commentData[i].created_by_id === localStorage.getItem('user_id'))) {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  /**
   * Setting comment is present or not
   * @param data (taxonomy data)
   */
  setHasComment(data: any) {
    if (data) {
      if (data.children.length > 0) {
        data.hasComment = this.isCommentAvailableForItem(data.id);
        for (let i = 0; i < data.children.length; i++) {
          this.setHasComment(data.children[i]);
        }
      } else {
        data.hasComment = this.isCommentAvailableForItem(data.id);
      }
    }
  }

  /**
   * To check selected node and its children all have comment
   * @param nodes
   */
  checkAllNodesCommented(nodes) {
    if (nodes) {
      if (nodes.children && nodes.children.length > 0) {
        for (let i = 0; i < nodes.children.length; i++) {
          this.checkAllNodesCommented(nodes.children[i]);
        }
        if (nodes.hasComment === false) {
          this.nodesCommented = nodes.hasComment;
          return;
        }
      } else {
        if (nodes.hasComment === false) {
          this.nodesCommented = nodes.hasComment;
          return;
        }
      }
    }
  }

  onAction(event) {
    this.getDocumentComments(this.selectedNodeId);
  }


  getCommentReport() {
    // this.submitReview = false;
    this.commentsLoaded = false;
    const url = GlobalSettings.GET_COMMENT_REPORT + '/' + this.projectId;
    this.service.getServiceData(url).then((res) => {
      if (res) {
        this.commentsLoaded = true;
        this.comment_total_nodes = res['comment_total_nodes'];
        this.comment_done_nodes = res['comment_done_nodes'];
        this.review_completion_percentage = res['review_completion_percentage'];
        this.list_pending_nodes = res['list_pending_nodes'];
      }
    }).catch((ex) => {
      console.log('getProjectDetail ', ex);
    });


  }

  mouseLeave() {

  }

  submitReviewComments() {
    const url = GlobalSettings.SUBMIT_REVIEW_COMMENTS;
    this.service.postService(url, {
      'project_id': this.projectId
    }).then((res) => {
      this.sharedService.sucessEvent.next({
        type: 'review_comments_submit'
      });
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PUBLIC_REVIEW_LIST;
      this.router.navigate([path]);
    }).catch((ex) => {
      console.log(' Exception submit review', ex);
    });
  }

  closeSubmitModal() {
    if (this.nodesCommented) {
      //      this.getAllNodes(15);
      this.setHasCommentForAllNode(this.taxonomyData.children[0], true);
      this.showNoComment = true;
    }
  }

  openCommentSec() {
    if (this.projectNotesComponent) {
      this.projectNotesComponent.showHideNav(true);
    }
  }

  onNextNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  onPreviousNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  selectNodeOnNavigation(node: any) {
    this.selectedNode = node;
    Utils.expandTreeTillSelectedNode(this.selectedNode, this.taxonomyData.children[0]);
    this.tree.onNodeSelected(this.selectedNode);
    setTimeout(() => {
      const id = this.selectedNode['id'] ? this.selectedNode['id'] : this.selectedNode['item_id'];
      if (document.getElementById(id + '-node')) {
        document.getElementById(id + '-node').focus();
      }
    }, 100);
  }

  findLastNode(node: any) {
    if (node.children && node.children.length > 0) {
      return this.findLastNode(node.children[node.children.length - 1]);
    } else if (node && node.length > 0) {
      return this.findLastNode(node[node.length - 1]);
    } else {
      return JSON.parse(JSON.stringify(node));
    }
  }

}
