import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy
} from '@angular/core';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  SortListComponent
} from '../../common/sort-list/sort-list.component';
import {
  CommonService
} from '../../common.service';
import {
  Router
} from '@angular/router';
import {
  SharedService
} from '../../shared.service';
// import {
//   WalktroughService
// } from '../../help/walkthrough/walktrough.service';
import {
  GlobalSettings
} from '../../global.settings';
import Utils from '../../utils';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html'
})
export class ReviewListComponent implements OnInit, OnDestroy {
  SORT_BY_LABEL = 'Sort by';

  searchText: boolean;
  selectedSortOption = '--Select--';
  searchResult = [];
  searchTrigger = false;
  searchResultEvent: Subscription;
  isFirstTime = true;
  startIndex = 0;
  lastIndex = 10;
  REIVEW_NOT_STARTED = 1;
  REIVEW_INPROGRESS = 2;
  REIVEW_COMPLETED = 3;
  minItemPerPage;
  viewType = '';
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'taxonomy_name',
    class: '',
    type: 'link',
    redirect: true,
    loadPage: true
  },
  {
    name: 'Status',
    propName: 'status',
    class: '',
    width: '15%',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Active Since',
    propName: 'created_at',
    class: '',
    width: '15%',
    type: 'date',
    dateType: 'amCalendar'
  }
  ];

  sortOptions = [{
    type: 'project_name',
    isDate: false,
    isAsce: true,
    display: 'Name (A-Z)'
  }, {
    type: 'project_name',
    isDate: false,
    isAsce: false,
    display: 'Name (Z-A)'
  }, {
    type: 'created_at',
    isDate: true,
    isAsce: true,
    display: 'Active Since (Asc)'
  }, {
    type: 'created_at',
    isDate: true,
    isAsce: false,
    display: 'Active Since (Desc)'
  }];
  activeReviewPermission = true; // permission for showing active public review
  archivedReviewPermission = true; // permission for showing completed public review
  reviewTempList = [];
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring
  searchingText; // holds text input to search in list
  filterData = []; // holds result array of search
  filterDataEvent: Subscription;
  reviewListLoaded = false; // boolean to determine if review list is loaded
  @ViewChild('sortListRef', { static: false }) sortListRef: SortListComponent;


  projects = [];
  newlyCreatedProjectId = null;
  constructor(
    private AuthenticateUser: AuthenticateUserService,
    private service: CommonService,
    private router: Router,
    private sharedService: SharedService
  ) {
    this.minItemPerPage = Utils.PAGINATION_LOWER_LIMIT;
    this.searchResultEvent = this.sharedService.searchResultEvent.subscribe((item: any) => {
      if (this.isFirstTime) {
        this.isFirstTime = false;
        return;
      }
      if (!this.isFirstTime) {
        if (item) {
          this.searchResult = item['searchData'];
          if (item['showSearchList']) {
            this.searchTrigger = true;
          } else {
            this.searchTrigger = false;
          }
        }
      }
    });
    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.selectedStageId = event.currentWorkflowStage ? event.currentWorkflowStage : '';
          this.userRoleRight();
        }, 1000);
      }
    });

    this.sharedService.searchTextEvent.subscribe((res: any) => {
      this.searchingText = res.text;
    });

    this.filterDataEvent = this.sharedService.filterDataTable.subscribe((res: any) => {
      this.filterData = res.data;
    });
  }

  ngOnInit() {
    this.searchingText = '';
    this.getProjects();
    this.sharedService.faqEvent.next({
      name: 'public_review'
    });
  }

  ngOnDestroy() {
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
  }


  getProjects() {
    this.searchText = true;
    this.reviewListLoaded = false;
    const url = GlobalSettings.PROJECT_LIST + '?project_type=3';
    this.service.getServiceData(url).then((res: any) => {
      this.reviewTempList = res === null ? [] : res.projects;
      if (this.reviewTempList.length > 0) {
        this.searchText = false;
      } else {
        this.searchText = true;
      }
      this.reviewTempList.forEach(item => {
        switch (item.review_status) {
          case 1:
            item.status = 'Review not yet started';
            break;
          case 2:
            item.status = 'Review in progress';
            break;
          case 3:
            item.status = 'Completed & submitted the review';
            break;
          default:
        }
      });
      this.projects = [];
      this.userRoleRight();
      this.sortListRef.sortDefalut('Active Since (Desc)');
      this.reviewListLoaded = true;
    }).catch(ex => {
      console.log('list of projects ex ', ex);
      this.reviewListLoaded = true;
    });
  }

  onProjectSelected(project) {
    this.sharedService.setTitleEvent.next({
      'type': 'project',
      'title': project.project_name
    });
    console.log('kljjkl');
    if (project.review_status === this.REIVEW_COMPLETED) {
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PUBLIC_REVIEW_DONE;
      this.router.navigate([path, project.project_id]);
    } else {
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PUBLIC_REVIEW_PENDING;
      this.router.navigate([path, project.project_id]);
    }
  }

  onSearchTextChange(txt) {
    if (txt.length > 2) {
      const url = GlobalSettings.PROJECT_SEARCH + txt;
      this.service.getServiceData(url).then((res: any) => {
        this.projects = res.projects;
      }).catch(ex => {
        console.log('list of projects ex ', ex);
      });
    } else {
      this.getProjects();
    }
  }

  checkProjectCanBeClickable(project_id, selectedStageId) {
    let isClickable = false;
    this.AuthenticateUser.authenticatePublicReview('View public review nodes', function (val) {
      isClickable = val;
      const editProject = Utils.checkProjectPermission(project_id, 'View public review nodes', 'project_permissions', selectedStageId);
      if (editProject.hasProject) {
        isClickable = editProject.valid;
      }
    });

    console.log('checkProjectCanBeClickable ', isClickable);
    return true;
  }

  onClose() {
    this.getProjects();
  }
  /* --------- functionality to add class and remove class on focus and blur for individual card layout start ---------*/
  onFocus(event, index) {
    const element = document.getElementById('card-' + index);
    element.classList.add('tn-focus');

  }

  onBlur(event, index) {
    const element = document.getElementById('card-' + index);
    element.classList.remove('tn-focus');

  }
  /* --------- functionality to add class and remove class on focus and blur for individual card layout end ---------*/

  /**
   * Function to sort data
   */
  sortData(event, type, isDate, isAsce) {
    this.selectedSortOption = event.target.innerHTML;
    Utils.sortDataArray(this.projects, type, isDate, isAsce);
  }

  changeDateToLocalTimeZone(date) {
    return Utils.changeDateToLocalTimeZone(date);
  }

  showDataInRange(event) {
    this.startIndex = event.start - 1;
    this.lastIndex = event.last;
  }

  changeView(type) {
    console.log('View type', type);
    this.viewType = type;
  }

  // Permission setup
  userRoleRight() {
    const self = this;
    this.activeReviewPermission = false;
    this.AuthenticateUser.authenticatePublicReview('View Active Public Review Taxonomies', function (val) {
      self.activeReviewPermission = val;
      self.filterReviewList(self.reviewTempList, 2); // 2 for active public review
    });

    this.archivedReviewPermission = false;
    this.AuthenticateUser.authenticatePublicReview('View Archived Public Review Taxonomies', function (val) {
      self.archivedReviewPermission = val;
      self.filterReviewList(self.reviewTempList, 3); // 3 for archieved public review
    });
  }

  /**
   * To filter review list based on permission (type: public review type)
   */
  filterReviewList(list, type) {
    if (list && list.length > 0) {
      list.forEach(item => {
        if (item.public_review_status === type) {
          this.projects.push(item);
        }
      });
    }

    this.projects.forEach(element => {
      element.is_clickable = this.checkProjectCanBeClickable(element.project_id, element.workflow['current_stage_id']) ? true : false;
    });
  }

  // Function to set search object for data list
  getColumnFilterObj() {
    const obj = {};
    obj['taxonomy_name'] = this.searchingText;
    return obj;
  }

}
