import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolemodalComponent } from './role-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('RoleModalComponent', () => {
  let component: RolemodalComponent;
  let fixture: ComponentFixture<RolemodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RolemodalComponent],
      imports: [FormsModule, ReactiveFormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolemodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create RolemodalComponent', () => {
    expect(component).toBeTruthy();
  });
});
