import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewComponent } from './tree-view.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedService } from '../shared.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('TreeViewComponent', () => {
  let component: TreeViewComponent;
  let fixture: ComponentFixture<TreeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TreeViewComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [SharedService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create TreeViewComponent', () => {
    expect(component).toBeTruthy();
  });
});
