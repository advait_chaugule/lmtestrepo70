import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFriendlyPageComponent } from './user-friendly-page.component';

describe('UserFriendlyPageComponent', () => {
  let component: UserFriendlyPageComponent;
  let fixture: ComponentFixture<UserFriendlyPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFriendlyPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFriendlyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
