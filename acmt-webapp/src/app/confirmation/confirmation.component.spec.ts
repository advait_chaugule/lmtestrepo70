import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationComponent } from './confirmation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from '../common.service';
import { SharedService } from '../shared.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';

describe('ConfirmationComponent', () => {
  let component: ConfirmationComponent;
  let fixture: ComponentFixture<ConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmationComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [CommonService, SharedService, ConfirmationDialogService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ConfirmationComponent', () => {
    expect(component).toBeTruthy();
  });
});
