/// <reference lib="webworker" />
addEventListener('message', ({ data }) => {
    // const response = `worker response to ${data}`;
    // console.log('data in worker ', data);
    const treeData = data.data;
    const location = data.location;
    const treeDataService = new TreeDataService();
    const treeSearch = new SetSearchFilter();
    const tabularView = new SetTableData();
    const configureTreeData = new ConfigureTreeDataForNodeTypeTable();
    const utilsFunction = new SetUtilsFunction();
    const iterateTreeMergeData = new MergeHumanCodeFullStatementClass();
    if (location === 'tree-search') {
        treeSearch.setParameters(data.parameters);
        if (!data.parameters['search']) {
            treeSearch.setFilter(treeData);
        } else {
            treeSearch.setFilter(treeData);
            treeSearch.searchNode(data.parameters['searchText'], data.parameters['isReset']);
        }
        if (data.parameters['getFilterResult']) {
            treeSearch.getFilterResult(data.parameters['dataForGetFilterResult']);
        }
        if (data.parameters['clearMarkNode']) {
            treeSearch.clearMarkNode(treeData.children);
        }
        // treeSearch.getFilterResult(treeData);
        postMessage({
            treeData: treeSearch.treeData,
            treeNodes: treeSearch.treeNodes,
            arrayToSearch: treeSearch.arrayToSearch,
            searchResultList: treeSearch.searchResultList,
            searchText: treeSearch.searchText,
            filterListObj: treeSearch.filterListObj,
            filterResult: treeSearch.filterResult,
            filterObj: treeSearch.filterObj
        });
        close();
    } else if (location === 'tabular-view') {
        tabularView.setTableData(treeData);
        postMessage({ 'items': tabularView.items, 'getProjectComments': tabularView.getProjectComments });
    } else if (location === 'node-type-table-view') {
        configureTreeData.setParameters(data.parameters);
        configureTreeData.configureNodeTypeTableData(treeData);
        if (data.generateVirualItems) {
            configureTreeData.createData(treeData);
        }
        postMessage({ 'nodeTypeData': configureTreeData.nodeTypeData, 'virtualRenderItems': configureTreeData.virtualRenderItems, 'columns': configureTreeData.columns });
        close();
    } else if (location === 'utils') {
        utilsFunction.setParameters(data);
        if (data.parameters['reset']) {
            utilsFunction.callResetExpansion(treeData, data.parameters['level']);
        } else {
            utilsFunction.callExpandTreeNodeFunction(treeData, data.parameters['isMultipleCall']);
        }
        postMessage({ taxonomyData: utilsFunction.taxonomyData });
    } else if (location === 'mergeHumanCodeFullStatement') {
        iterateTreeMergeData.setParameters(data.parameters);
        iterateTreeMergeData.iterate(treeData[0], 0);
        postMessage(treeData);
        close();
    } else {
        treeDataService.setParameters(data.parameters);
        treeDataService.setTreeResponse(treeData);
        treeDataService.setNodes();
        treeDataService.setRelations();
        treeDataService.findTopmostOrphan(); // Finding list of topmost orphan nodes
        treeDataService.setAssociations();
        postMessage(treeDataService.parseTreeData());
    }
    close();

});

export class TreeDataService {

    DUMMY_ID_FOR_ORPHANLABEL = 'orphan_label_node_id';

    projectName = '';
    projectDesc = '';
    workflowList = [];
    taxonomyList = [];
    taxonomyData = {
        children: []
    };
    selectedWorkFlow = null;
    selectedTaxonomy = null;
    textLength = 50;
    selectedTaxonomyNodeIs = [];
    nodes = [];
    relations: any;
    associations: any = [];
    treeResponseData: any;
    selectedTaxonomyId = null;
    addWrapperNode = true;
    expandLevel = 1;
    isPacingGuide = false;
    import_type = 0;
    isDeleted = false; // holds flag for taxonomy deleted or not
    rootNodeId: any; // holds root node i.e. document node id
    orphanNodeList = []; // holds list of orphan node list if any
    topMostOrphanNodes = []; // holds list of top most orphan nodes
    worker: any;

    constructor() { }

    setParameters(parameters) {
        this.isPacingGuide = parameters.isPacingGuide;
        this.expandLevel = parameters.level;
        this.addWrapperNode = parameters._addWrapperNode;
        this.import_type = 0;
        this.isDeleted = false;
        this.DUMMY_ID_FOR_ORPHANLABEL = parameters.DUMMY_ID_FOR_ORPHANLABEL;
    }

    setTreeResponse(data: any): void {
        this.treeResponseData = data;
    }

    setNodes(): void {
        const apiResponse: any = this.treeResponseData;
        this.nodes = apiResponse.nodes;
    }

    getNodes(): any {
        return this.nodes;
    }

    setRelations(): void {
        const apiResponse: any = this.treeResponseData;
        this.relations = apiResponse.relations;
    }

    getRelations(): any {
        return this.relations;
    }

    setAssociations(): void {
        const apiResponse: any = this.treeResponseData;
        this.associations = [];
        if (apiResponse && apiResponse.associations) {
            this.associations = apiResponse.associations;
        }
    }

    getAssociations(): any {
        return this.associations;
    }

    parseTreeData() {

        const treeNodes: any = this.getNodes();
        const documentNode: any = treeNodes[0] ? treeNodes[0] : treeNodes;
        this.rootNodeId = documentNode.id;
        const parsedTree = documentNode;
        if (parsedTree) {
            parsedTree.level = 1;
        }
        if (parsedTree && documentNode && this.addWrapperNode) {
            parsedTree.full_statement = documentNode.title;
            parsedTree.full_statement_html = documentNode.title_html ? documentNode.title_html : documentNode.title;
            parsedTree.is_document = 1;
            parsedTree.title = documentNode.title;
            parsedTree.expand = true;
            parsedTree.cut = 0;
            parsedTree.paste = 0;
            parsedTree.reorder = 0;
            parsedTree.isFirst = true;
            parsedTree.list_enumeration = parsedTree.list_enumeration;
            parsedTree.sequence_number = '' + parsedTree.sequence_number;
        }
        parsedTree.children = this.createTreeStructure(documentNode.id, 1);
        this.sortData(parsedTree.children);
        /* Orphan lable node appending under document node STARTS */
        // this.orphanNodeList = this.getOrphanNodes();
        // if (this.orphanNodeList.length > 0) {
        this.buildOrphanLableNode(parsedTree);
        // }
        /* Orphan lable node appending under document node ENDS */

        const wrapperNode = {
            children: [parsedTree],
        };
        if (this.addWrapperNode) {
            return {
                parsedTreeNodes: wrapperNode,
                relations: this.relations
            };
        } else {
            return {
                parsedTreeNodes: parsedTree,
                relations: this.relations
            };
        }
    }

    createTreeStructure(nodeId: string, level) {
        const data: any[] = [];
        const childrenNodes = this.extractChildren(nodeId);
        if (childrenNodes.length > 0) {
            level = level + 1;
            for (const key in childrenNodes) {
                if (childrenNodes[key]) {
                    const childNode = childrenNodes[key];
                    if (childNode) {
                        nodeId = childNode.id;
                        childNode.children = [];
                        childNode.expand = level <= this.expandLevel ? true : false;
                        childNode.cut = 0;
                        childNode.paste = 0;
                        childNode.reorder = 0;
                        childNode.level = level;
                        childNode.list_enumeration = '' + childNode.list_enumeration;
                        childNode.sequence_number = '' + childNode.relation_sequence_number;
                        const nodeChildren = this.createTreeStructure(nodeId, level);
                        if (nodeChildren.length > 0) {
                            childNode.children = nodeChildren;
                        }
                        data.push(childNode);
                    }
                }
            }
        }
        return data;
    }

    extractChildren(nodeId: string): any {
        const treeNodes: any = this.getNodes();
        const nodeRelations: any = this.getRelations();
        const data: any[] = [];
        for (const key in nodeRelations) {
            if (nodeRelations[key]) {

                const relation: any = nodeRelations[key];
                if (relation.child_id !== relation.parent_id) {
                    const parentId: string = relation.parent_id;
                    // console.log(' extractChildren  ', relation, '  ', parentId);
                    if (nodeId === parentId) {
                        const childId: string = relation.child_id;
                        const childNode = this.extractNode(childId, parentId);
                        childNode['relation_sequence_number'] = relation.sequence_number;
                        if (childNode) {
                            childNode.parent_id = parentId;
                            // These properties used in Export CSV to show association and association type
                            if (nodeRelations[key].tree_association !== undefined) {
                                childNode.tree_association = nodeRelations[key].tree_association;
                                childNode.association_type = nodeRelations[key].association_type;
                            }
                            data.push(childNode);
                        }
                    }
                }
            }
        }
        return data;
    }

    extractNode(nodeIdToSearchWith: string, parentId): any {
        const treeNodes: any = this.getNodes();
        let nodeFound: any;
        for (const key in treeNodes) {
            if (treeNodes[key]) {
                const node: any = treeNodes[key];
                if (this.isPacingGuide) {
                    // This to handle if hierarchy_id is null
                    if (node.hierarchy_id == null) {
                        node.hierarchy_id = treeNodes[0].id;
                    }
                    if (nodeIdToSearchWith === node.id && parentId === node.hierarchy_id) {
                        nodeFound = Object.assign({}, node);
                        break;
                    }
                } else {
                    if (nodeIdToSearchWith === node.id) {
                        nodeFound = Object.assign({}, node);
                        break;
                    }
                }
            }
        }
        return nodeFound;
    }

    /**
     * To get orphan node list
     */
    /*getOrphanNodes() {
      const nodes = this.getNodes();
      const orphanNodes = [];
      for (const node of nodes) {
        // Check node is orphan or not skipping root node
        if ((node.id !== this.rootNodeId) && this.isNodeOrphan(node)) {
          node.isOrphan = true;
          node.cut = 0;
          node.paste = 0;
          node.reorder = 0;
          node.parent_id = '';
          node.children = [];
          // node.list_enumeration = node.sequence_number ? '' + node.sequence_number : node.list_enumeration;
          orphanNodes.push(node);
        }
      }
      return orphanNodes;
    }*/

    /**
     * To check node is orphan or not by giving node object as parameter
     * @param node (selected node object)
     */
    /*isNodeOrphan(node: any) {
      const nodeId = node.id;
      const nodeRelations = this.getRelations();
      let flag = false;
      let parentId: any;
      for (const key in nodeRelations) {
        if (nodeRelations[key]) {
          const relation: any = nodeRelations[key];
          if ((relation.child_id === nodeId) && relation.parent_id) { // If node exists in relation, set flag=true
            parentId = relation.parent_id;
            flag = true;
            break;
          }
        }
      }
      if (flag) { // If flag is true, then check its parent is orphan or not recursively
        if (parentId !== this.rootNodeId) { // excluding root node checking
          node.firstRootOrphan = false;
          return this.isNodeOrphan({
            id: parentId
          });
        } else {
          return false; // not orphan
        }
      } else {
        // If node exists in relation as only parent_id but not exists in nodes list, don't cosider the node as orphan
        const orphan = !this.findNodeById(nodeId) ? false : true;
        node.firstRootOrphan = orphan;
        return orphan; // orphan
      }
    }*/

    /**
     * To find list of top most orphan root nodes
     */
    findTopmostOrphan() {
        const nodes = this.getNodes();
        this.topMostOrphanNodes = [];
        for (const node of nodes) {
            if (node.is_orphan === 1) { // top most orphan node
                this.topMostOrphanNodes.push(node);
            }
        }
    }

    /**
     * To create Orphan level root node which will be non-clickable i.e. only a text, under which orphan node list
     * will remain. This root node can be identified by attribute 'isOrphanLabel' which will be true
     * @param parentNode (Node of which one children will be orphan lable node i.e. orphan lable node will be one
     * of children of given parameter node- 'parentNode')
     */
    buildOrphanLableNode(parentNode: any) {
        let orphanFound = false;
        const nodes = this.getNodes();
        const orphanLableNode: any = {}; // 'orphanLableNode' is one dummy node
        const title = 'Nodes with no linked parent';
        orphanLableNode.full_statement = title;
        orphanLableNode.full_statement_html = title;
        orphanLableNode.human_coding_scheme = '';
        orphanLableNode.is_document = 0;
        // orphanLableNode.title = title;
        orphanLableNode.cut = 0;
        orphanLableNode.paste = 0;
        orphanLableNode.reorder = 0;
        orphanLableNode.children = [];
        orphanLableNode.sequence_number = '';
        orphanLableNode.isOrphan = true;
        orphanLableNode.isOrphanLabel = true;
        orphanLableNode.parent_id = parentNode.id;
        orphanLableNode.id = this.DUMMY_ID_FOR_ORPHANLABEL; // assigning dummy id
        orphanLableNode.list_enumeration = '' + (this.getMaxLE(parentNode.children));
        orphanLableNode.sequence_number = '' + (this.getMaxLE(parentNode.children));
        orphanLableNode.level = parentNode.level + 1;
        // orphanLableNode.expand = orphanLableNode.level <= this.expandLevel ? true : false;
        orphanLableNode.expand = true;

        // for (const node of nodes) {
        for (const node of this.topMostOrphanNodes) {
            orphanFound = true;
            // const node = this.findNodeById(orphanNode.id);
            node.cut = 0;
            node.paste = 0;
            node.reorder = 0;
            node.parent_id = this.DUMMY_ID_FOR_ORPHANLABEL;
            node.level = orphanLableNode.level + 1;
            node.expand = node.level <= this.expandLevel ? true : false;
            node.children = this.createTreeStructure(node.id, node.level);
            orphanLableNode.children.push(node);
        }
        // console.log('Orphan node tree =========================>', orphanLableNode);
        if (orphanFound) {
            parentNode.children.push(orphanLableNode);
        }
    }

    /**
     * To fetch node from node list by node id
     * @param nodeId (selected node id)
     */
    /*findNodeById(nodeId: any) {
      const nodeList = this.getNodes();
      for (const node of nodeList) {
        if (node.id.trim() === nodeId.trim()) {
          return node;
        }
      }
    }*/

    sortData(data, sortBy: string = 'human_coding_scheme') {
        let self = this;
        if (data instanceof Array) {
            if (data.length > 0) {
                sortBy = this.checkSortType(data);
            }
        } else {
            if (data.children && data.children.length > 0) {
                sortBy = this.checkSortType(data.children);
            }
        }
        data.sort(function (a, b) {

            if (sortBy === 'sequence_number' &&
                self.checkNumber(a[sortBy]) && self.checkNumber(b[sortBy])) {
                return (parseInt(a[sortBy], 10) < parseInt(b[sortBy], 10) ? -1 : 1) * (1);
            } else {
                const valA = a[sortBy].toUpperCase();
                const valB = b[sortBy].toUpperCase();

                let comparison = 0;
                if (valA > valB) {
                    comparison = 1;
                } else if (valA < valB) {
                    comparison = -1;
                }
                return comparison;
            }

        });

        data.forEach(element => {
            if (element.children && element.children.length > 0) {
                sortBy = this.checkSortType(element.children);
                this.sortData(element.children, sortBy);
            }
        });
    }


    checkSortType(children) {
        if (children[0].sequence_number && children[0].sequence_number.trim() !== '' && children[0].sequence_number !== '0'
            && children[0].sequence_number !== 'undefined') {
            return 'sequence_number';
        } else {
            if (children[0].human_coding_scheme && children[0].human_coding_scheme.trim() !== '') {
                return 'human_coding_scheme';
            } else {
                return 'full_statement';
            }
        }
    }

    checkNumber(value) {
        return parseInt(value, 10);
    }

    getMaxLE(nodes: any[]) {
        const listEnumerations = [];
        for (const node of nodes) {
            listEnumerations.push(node.sequence_number);
        }
        return (Math.max(...listEnumerations) + 1);
    }

    sortDataArray(array, sortby, isDate, isAsce) {
        array.sort(function (a, b) {
            let val1, val2;
            if (!isDate && typeof (a[sortby]) === 'number') {
                val1 = a[sortby];
                val2 = b[sortby];
            } else {
                val1 = a[sortby].toUpperCase();
                val2 = b[sortby].toUpperCase();
            }
            if (isDate) {
                val1 = new Date(a[sortby].replace(/-/g, '/'));
                val2 = new Date(b[sortby].replace(/-/g, '/'));
            }
            if (val1 === val2) {
                return 0;
            }
            if (val1 > val2) {
                return isAsce ? 1 : -1;
            }
            if (val1 < val2) {
                return isAsce ? -1 : 1;
            }
        });
    }

}



export class SetTableData {

    rawTreeData; // holds tree object from details view
    dataWithAllComments; // holds complete object re assigned from raw tree object
    assignedToMe;
    items; // object used in rendering table
    getProjectComments = false;
    setTableData(data) {
        this.rawTreeData = null;
        this.rawTreeData = data;
        if (this.rawTreeData) {
            const rawTreeDataWithoutParent = [];
            if (!this.rawTreeData.children && this.rawTreeData.length > 1) {
                // rawTreeDataWithoutParent.push({ id: '', children: [] });
                this.rawTreeData.forEach(item => {
                    // rawTreeDataWithoutParent.push(item.children[0]);
                    if (item.children && item.children.length) {
                        item.children.forEach(child => {
                            rawTreeDataWithoutParent.push(child);
                        });
                    }
                });
            }

            this.dataWithAllComments = this.rawTreeData.children ? this.rawTreeData.children :
                (this.rawTreeData.length === 1 ? this.rawTreeData[0].children : rawTreeDataWithoutParent);
            this.onChangeCommentAssigned();
        }


    }


    onChangeCommentAssigned() {
        if (this.assignedToMe === 'assignedToMe') {
            this.getProjectComments = true;
            // this.getProjectComments(this.taxonomyId); // comment icon setting for 'comments assigned to me'
        } else { // comment icon setting for 'open commented node'
            if (this.dataWithAllComments) {
                this.items = this.dataWithAllComments;
            }
        }

    }

}


export class SetSearchFilter {

    treeData;
    filterListObj = {}; // holds formatted filter object with values
    selectedFilterList = []; // holds selected values to filter tree
    filterResult = []; // holds filtered tree nodes
    treeNodes: any;
    arrayToSearch = [];
    isSearchApplied = false; // holds flag if any filter or search is applied on tree data
    searchResultList = [];
    filterCriteriaList;
    searchText = ''; // holds text input in search box
    isReset = false;
    filterObj = [];
    showFilter = true;
    setParameters(param) {
        this.filterCriteriaList = param.filterCriteriaList;
        this.searchText = param.searchText;
        this.isReset = param.isReset;
        this.selectedFilterList = param.selectedFilterList;
        this.filterResult = param.filterResult;
        this.filterObj = param.filterObj;
        this.showFilter = param.showFilter;
        this.searchResultList = param.searchResultList;
    }
    setFilter(treeData) {
        this.treeData = treeData;
        if (this.treeData) {
            if (this.treeData instanceof Array) {
                if (this.showFilter) {
                    this.treeData.forEach(tree => {
                        this.getFilterObj(tree.children);
                    });
                }
                this.treeNodes = this.treeData;
                this.arrayToSearch = this.treeData;
            } else if (this.treeData instanceof Object) {
                if (this.showFilter) {
                    this.getFilterObj(this.treeData.children);
                }
                this.treeNodes = this.treeData.children;
                this.arrayToSearch = this.treeData.children;
            }
            if (!this.isSearchApplied) {
                this.clearMarkNode(this.treeNodes);
            }
        }
    }


    // FUNCTION to extract filter object from tree data
    getFilterObj(dataSet) {
        dataSet.forEach(data => {
            this.filterCriteriaList.forEach(col => {
                const property = col.hasOwnProperty('propName') ? 'propName' : 'internal_name';
                const propStr = col[property].toLowerCase().replace(/ /gi, '_');
                if (propStr !== 'human_coding_scheme' && propStr !== 'full_statement') {
                    const element = data[propStr];
                    if ((element || element === 0)) {
                        if (!this.filterListObj[propStr]) {
                            this.filterListObj[propStr] = [{
                                colName: propStr,
                                label: element.trim(),
                                is_selected: 0
                            }];
                        } else {
                            if (!this.filterListObj[propStr].find((obj) => element.trim() === (obj.label))) {
                                this.filterListObj[propStr].push({
                                    colName: propStr,
                                    label: element.trim(),
                                    is_selected: 0
                                });
                            }
                        }
                    }
                }
            });

            if (data.children && data.children.length) {
                this.getFilterObj(data.children);
            }
        });
    }

    clearMarkNode(treeArr) {
        if (treeArr) {
            treeArr.forEach(node => {
                if (this.filterResult && this.searchResultList) {
                    const index1 = this.searchResultList.findIndex(item => item.id === node.id);
                    const index2 = this.filterResult.findIndex(item => item.id === node.id);
                    if (index1 === -1 && index2 === -1) {
                        node.isHighlighted = false;
                        if (!node.is_document) {
                            node.isSearchResult = false;
                        }
                    } else {
                        node.isHighlighted = true;
                    }

                    if (node.children && node.children.length) {
                        this.clearMarkNode(node.children);
                    }
                }
            });
        } else {
            // console.log('Exception in clear mark node ####');
        }
    }


    // FUNCTION to search user input text in tree data
    searchNode(text, isReset: boolean = false) {
        this.searchResultList = [];
        this.searchText = text;
        if (this.searchText.length) {
            if (this.filterResult && this.filterResult.length) {
                this.arrayToSearch = this.filterResult;
            }
            this.getSearchResult(this.arrayToSearch, text);
        } else {
            if (this.selectedFilterList && this.selectedFilterList.length && !isReset) {
                this.getFilterResult(this.arrayToSearch);
                this.searchResultList = this.filterResult;
            } else {

                this.searchResultList = [];
                this.arrayToSearch = this.treeNodes;
                // if (this.treeData instanceof Array) {
                //   this.treeData.forEach(tree => {
                //     this.searchResultList = tree.children;
                //   });
                // } else if (this.treeData instanceof Object) {
                //   this.searchResultList = this.treeData.children;
                // }
            }
        }

    }

    getSearchResult(treeData: any, text) {
        treeData.forEach(element => {
            if (element.level) {
                element.isSearchResult = false;
                element.isHighlighted = false;
                if ((element.full_statement && element.full_statement.toLowerCase().includes(text.toLowerCase())) ||
                    (element.human_coding_scheme && element.human_coding_scheme.toLowerCase().includes(text.toLowerCase()))) {
                    element.isSearchResult = true;
                    element.isHighlighted = true;
                    if (!this.searchResultList.find(data => data.id === element.id) ||
                        (this.searchResultList.find(data => data.id === element.id) &&
                            !this.searchResultList.find(data => data.parent_id === element.parent_id))) {
                        this.searchResultList.push(element);
                    }
                }
            }
            if (element.children && element.children.length) {
                this.getSearchResult(element.children, text);
            }
        });
    }

    groupByType(array, type) {
        const propArr = [];
        const dataArr = [];
        array.forEach(item => {
            if (item[type] && propArr.indexOf(item[type]) === -1) {
                propArr.push(item[type]);
            }
        });

        propArr.forEach(prop => {
            const obj = {};
            obj['key'] = prop;
            obj['value'] = array.filter(data => {
                return (data[type] === prop);
            });
            dataArr.push(obj);
        });
        this.sortDataArray(dataArr, 'key', false, true);
        return dataArr;
    }

    sortDataArray(array, sortby, isDate, isAsce) {
        array.sort(function (a, b) {
            let val1, val2;
            if (!isDate && typeof (a[sortby]) === 'number') {
                val1 = a[sortby];
                val2 = b[sortby];
            } else {
                val1 = a[sortby].toUpperCase();
                val2 = b[sortby].toUpperCase();
            }
            if (isDate) {
                val1 = new Date(a[sortby].replace(/-/g, '/'));
                val2 = new Date(b[sortby].replace(/-/g, '/'));
            }
            if (val1 === val2) {
                return 0;
            }
            if (val1 > val2) {
                return isAsce ? 1 : -1;
            }
            if (val1 < val2) {
                return isAsce ? -1 : 1;
            }
        });
    }



    // FUNCTION to parse tree data with selected filters
    getFilterResult(treeData: any) {
        let dataSet = treeData;
        const searchResult = [];
        this.filterObj = this.groupByType(this.selectedFilterList, 'colName');
        for (const obj of this.filterObj) {
            dataSet.forEach(data => {
                data.isSearchResult = false;
                if (obj.value.find(filter => data[obj.key] === filter.label)) {
                    data.isSearchResult = true;
                    data.isHighlighted = true;
                    if (searchResult.indexOf(data) < 0 && this.filterResult.indexOf(data) < 0) {
                        searchResult.push(data);
                    }
                } else {
                    const idx = searchResult.indexOf(data);
                    data.isSearchResult = false;
                    data.isHighlighted = false;
                    if (idx > -1) {
                        searchResult.splice(idx, 1);
                    }
                }

                if (data.children && data.children.length) {
                    this.getFilterResult(data.children);
                }
            });
            dataSet = searchResult;
        }

        this.filterResult = this.filterResult.concat(searchResult);

        // console.log('##### FILTER ====>', this.filterResult);

    }

}

export class ConfigureTreeDataForNodeTypeTable {
    nodeTypeData: any = {};
    align = false;
    Object = Object;
    document: any;
    nodeTypeArray = [];
    setParameters(param) {
        this.nodeTypeData = param.nodeTypeData;
        this.align = param.alignTable;
        this.document = param.document;
        this.nodeTypeArray = param.nodeTypeArray;
        if (this.align) {
            this.alignTable();
        }
    }

    configureNodeTypeTableData(treeData) {
        treeData.forEach(data => {
            const level = data.level;
            if (level && data.node_type && data.node_type !== 'Document') {
                if (this.nodeTypeData[level]) {
                    const index = this.nodeTypeData[level].findIndex(item => item.key === data.node_type);
                    if (index !== -1) {
                        if (!this.nodeTypeData[level][index].value.find(node => node.id === data.id)) {
                            this.nodeTypeData[level][index].value.push(data);
                        }
                    } else {
                        const group = this.groupByType(treeData, 'node_type');
                        group.forEach(type => {
                            if (!this.nodeTypeData[level].find(nodeType => nodeType.key === type.key)) {
                                this.nodeTypeData[level].push(type);
                            }
                        });
                    }
                } else {
                    this.nodeTypeData[level] = this.groupByType(treeData, 'node_type');
                }
                // this.addBlankChild(this.nodeTypeData[level][0].value, level);
            }
            if (data.is_orphan !== 1 && data.children && data.children.length > 0) {
                this.configureNodeTypeTableData(data.children);
            }
        });
    }

    checkChildExistence(array) {
        let childNodeType;
        if (array && array.length) {
            const hasChild = array.some(node => {
                if (node.children && node.children.length) {
                    childNodeType = node.children[0].node_type;
                    return true;
                }
            });
            const obj = {
                nodeType: childNodeType,
                hasChild
            };
            return obj;
        }
    }

    addBlankChild(array, currentLevel) {
        let checkedData;
        let childNodeType = '';
        let arrayWithChild;
        checkedData = this.checkChildExistence(array);
        arrayWithChild = checkedData.hasChild;
        childNodeType = checkedData.nodeType;

        if (!arrayWithChild) {
            this.nodeTypeData[currentLevel - 1][0].value.forEach(element => {
                checkedData = this.checkChildExistence(element.children);
                if (checkedData && checkedData.length) {
                    arrayWithChild = checkedData.hasChild;
                    childNodeType = checkedData.nodeType;
                    if (arrayWithChild) {
                        return;
                    }
                }
            });
        }
        if (arrayWithChild) {
            for (const node of array) {
                if (node.id && node.children && !node.children.length) {
                    node.children.push({
                        id: node.id + '_blank_child',
                        parent_id: node.id,
                        node_type: childNodeType,
                        level: node.level + 1,
                        isBlankChild: true,
                        full_statement: '',
                        human_coding_sceme: ''
                    });
                }
            }
        }

    }

    /* Group an array by value */

    groupByType(array, type) {
        // console.log('item[type]', JSON.stringify(array));
        const propArr = [];
        const dataArr = [];
        array.forEach(item => {
            if (item[type] && propArr.indexOf(item[type]) === -1) {
                propArr.push(item[type]);
            }
        });

        propArr.forEach(prop => {
            const obj = {};
            obj['key'] = prop;
            obj['value'] = array.filter(data => {
                return (data[type] === prop);
            });
            dataArr.push(obj);
        });
        this.sortDataArray(dataArr, 'key', false, true);
        return dataArr;
    }


    sortDataArray(array, sortby, isDate, isAsce) {
        array.sort(function (a, b) {
            let val1: any = '';
            let val2: any = '';
            if (!isDate && typeof (a[sortby]) === 'number') {
                val1 = a[sortby];
                val2 = b[sortby];
            } else {
                val1 = a[sortby] ? a[sortby].toUpperCase() : '';
                val2 = b[sortby] ? b[sortby].toUpperCase() : '';
            }
            if (isDate) {
                val1 = new Date(a[sortby].replace(/-/g, '/'));
                val2 = new Date(b[sortby].replace(/-/g, '/'));
            }
            if (val1 === val2) {
                return 0;
            }
            if (val1 > val2) {
                return isAsce ? 1 : -1;
            }
            if (val1 < val2) {
                return isAsce ? -1 : 1;
            }
        });
    }


    alignTable() {
        Object.keys(this.nodeTypeData).forEach(column => {
            if (this.nodeTypeData[column][0].display) {
                this.nodeTypeData[column][0].value.forEach(node => {
                    this.calculateHeight(node, this.nodeTypeData[column][0].metadataList);
                });
            }
        });
    }
    calculateHeight(node: any, metadataList: any) {
        let totalHeight = 0;
        if (node.children && node.children.length) {
            node.children.forEach(child => {
                if (!child.isBlankChild) {
                    const elemId = child.id + '-node-0';
                    if (this.document.getElementById(elemId)) {
                        const elemHeight = this.document.getElementById(elemId).offsetHeight;
                        totalHeight += elemHeight;
                    }
                }
            });
        } else {
            if (node.isBlankChild) {
                const elemId = node.parent_id + '-node-0';
                if (this.document.getElementById(elemId)) {
                    const elemHeight = this.document.getElementById(elemId).offsetHeight;
                    totalHeight = elemHeight;
                }
            }
        }
        node.height = totalHeight ? totalHeight + 'px' : this.setBiggerHeight(node, metadataList) + 'px';
        // this.checkTextHeight(node, metadataList);
    }

    checkTextHeight(node, metadataList) {
        const elemId = node.id;
        let textHeight = 0;
        for (let i = 0; i < metadataList.length; i++) {
            const domId = elemId + '-button-' + i;
            const element = this.document.getElementById(domId);
            if (element) {
                textHeight = element.offsetHeight;
                node.height = textHeight > JSON.parse(node.height.replace('px', '')) ? textHeight + 'px' : node.height;
            }
        }

    }

    setBiggerHeight(node, metadataList) {
        const elemeId = node.id;
        let biggerHeight = 0;
        for (let i = 0; i < metadataList.length; i++) {
            const domId = elemeId + '-node-' + i;
            const element = this.document.getElementById(domId);
            if (element) {
                const currentHeight = element.offsetHeight;
                biggerHeight = currentHeight > biggerHeight ? currentHeight : biggerHeight;
            }
        }
        return biggerHeight;
    }


    object = new Object();
    rowArray = [];
    rowSpanArray = [];
    columns = {};
    virtualRenderItems = [];
    count = 0;
    metadataObj = {};
    createData(treeData) {
        this.rowArray = [];
        this.object = new Object();

        treeData.forEach(data => {
            const level = data.level;
            let obj = new Object();
            this.createData_(data, 2);
        });
        // console.log('Finalarray ', JSON.stringify(this.rowArray));
        // console.log(' this.metadataObj ', JSON.stringify(this.metadataObj));
        // console.log(' this.metadataObj ', JSON.stringify(this.columns));

        this.generateVirtualScrollArr();
    }

    createData_(treeData, level) {
        // console.log('treeData', treeData, this.nodeTypeData, level);
        if (treeData.node_type && treeData.node_type !== 'Document') {
            return;
        }
        treeData = treeData.children ? treeData.children : treeData;

        treeData.forEach((data) => {
            level = data.level;
            if (this.nodeTypeData[level] && this.nodeTypeData[level][0] && this.nodeTypeData[level][0].metadataList) {
                this.object[this.nodeTypeData[level][0].key + '_' + level] = {};
                this.nodeTypeData[level][0].metadataList.forEach(element => {
                    this.metadataObj[this.nodeTypeData[level][0].key + '_' + level + '_' + element.internal_name] = "-";
                    this.object[this.nodeTypeData[level][0].key + '_' + level][element.internal_name] = data[element.internal_name + '_html'] ? data[element.internal_name + '_html'] : data[element.internal_name];
                    this.object[this.nodeTypeData[level][0].key + '_' + level]['generateCell'] = true;
                    this.object[this.nodeTypeData[level][0].key + '_' + level]['level'] = level;
                    this.object[this.nodeTypeData[level][0].key + '_' + level]['item_id'] = data['id'];
                    this.columns[this.nodeTypeData[level][0].key + '_' + level + '#||#' + element.display_name + '#||#' + element.internal_name]
                        = this.nodeTypeData[level][0].width;
                });
                this.object['rowspan_' + this.nodeTypeData[level][0].key + '_' + level + '#||#' + level] = 0;
            }
            // console.log('Finalarray Cr_ ', this.object, level, data);
            if (data.children && data.children.length > 0) {
                const length = data.children.length;
                // if (this.nodeTypeData && this.nodeTypeData[level] && length > 1) {
                //     this.object['rowspan_' + this.nodeTypeData[level][0].key + '#||#' + level] = length;
                // }
                // if (this.nodeTypeData[level - 1]) {
                //     this.object['rowspan_' + this.nodeTypeData[level - 1][0].key + '#||#' + (level - 1)] = this.object['rowspan_' + this.nodeTypeData[level - 1][0].key + '#||#' + (level - 1)] + length;
                // }
                this.count = 0;
                // console.log('############ DATA calling checkAllLeafNodeCount');
                this.checkAllLeafNodeCount(data);
                if (this.nodeTypeData[level] && this.nodeTypeData[level][0]) {
                    this.object['rowspan_' + this.nodeTypeData[level][0].key + '_' + level + '#||#' + level] = this.count;
                }


                this.createData_(data.children, level);

            } else {
                // this.updateRowSpan(this.object);
                this.rowArray.push(this.object);
                // this.count = 1;
                this.object = new Object();

                // console.log('array ********* ', JSON.stringify(this.object));

            }

        });
    }

    checkAllLeafNodeCount(data) {

        // this.count = 1;
        if (data.children && data.children.length) {
            // this.count += (data.children.length);
            data.children.forEach(child => {
                // this.count += (child.children.length);
                //console.log('############ DATA', this.count)
                if (child.children && child.children.length) {
                    // console.log('############ DATA recursive calling checkAllLeafNodeCount', child.children)
                    this.checkAllLeafNodeCount(child);
                } else {

                    this.count = this.count + 1;
                    // console.log('############ DATA inner else', this.count)
                }
            });
        }
    }

    updateRowSpan(obj) {
        let tempArr = [];
        Object.keys(obj).forEach(key => {
            if (key.includes('rowspan_')) {
                tempArr.push(key + '#||#' + obj[key]);
            }
        });
        // console.log('#########', tempArr);
        if (tempArr && tempArr.length) {
            tempArr.forEach((element, i) => {

                // console.log('OBJ', element, i);
                let values = element.split('#||#');
                let key = values[0];
                let column = values[1] - 1;
                let value = Number(values[2]);
                obj[key] = value;
                // let columnCount = tempArr.length + 1;
                let columnCount = this.nodeTypeArray.length;
                let totalSpanCount = Number(value);
                if (value === 0 && column !== columnCount) {
                    // console.log('VALUE IF', value);
                    for (let j = column; j <= tempArr.length; j++) {
                        if (tempArr[j]) {
                            // console.log('tempArr', tempArr, j);
                            let tempvalues = tempArr[j].split('#||#');
                            let newValue = Number(tempvalues[2]);
                            if (newValue !== 0) {
                                obj[key] = newValue;
                                break;
                            }
                        }
                    }
                } else {
                    if (column !== columnCount) {
                        for (let j = column; j <= tempArr.length; j++) {
                            if (tempArr[j]) {
                                // console.log('tempArr', tempArr, j);
                                let tempvalues = tempArr[j].split('#||#');
                                let newValue = Number(tempvalues[2]);
                                totalSpanCount = totalSpanCount + newValue;
                            }
                        }
                        let calculatedSpanValue = totalSpanCount - (columnCount - column - 1);
                        obj[key] = calculatedSpanValue;
                    }
                }
                // console.log('$$$$$$$$$$$$$', tempArr, key, 'COLUMN ', column, 'CURRENT SPAN ', value, 'TOTAL COLUMNS ', columnCount, 'totalSpanCount ', totalSpanCount);
            });
        }
    }

    generateVirtualScrollArr() {
        this.virtualRenderItems = [];
        this.rowArray.forEach(row => {
            let tableRow = {};
            this.nodeTypeArray.forEach(data => {
                if (row[data]) {
                    Object.keys(row[data]).forEach(key => {
                        if (row[data][key] !== true && key !== 'item_id' && key !== 'level') {
                            tableRow[data + '_' + key] = (row[data][key] ? row[data][key] : '') + '#||#' +
                                (row['rowspan_' + data + '#||#' + row[data]['level']] ? row['rowspan_' + data + '#||#' + row[data]['level']] : 1) + '#||#' + row[data]['level'] + '#||#' + row[data]['item_id'];
                            // tableRow['rowspan_' + data] = row['rowspan_' + data] ? row['rowspan_' + data] : 1;
                        }
                    });
                }
            });
            this.virtualRenderItems.push(tableRow)
        });
        // console.log('virtualRenderItems', JSON.stringify(this.virtualRenderItems));
        this.generateColSpan();
        // console.log('virtualRenderItems UPDATED', JSON.stringify(this.virtualRenderItems));
    }


    generateColSpan() {

        // document is considered as column 1

        const maximumColumns = this.nodeTypeArray.length + 1;
        // console.log('maximumColumns', maximumColumns, this.nodeTypeArray);
        this.virtualRenderItems.forEach(element => {
            let last_value = Object.values(element)[Object.values(element).length - 1] + '';
            const data = last_value.split('#||#');
            const currentColumnNo = Number(data[2]);
            let colSpan = 1;
            if (currentColumnNo < maximumColumns) {
                // console.log('last_value', last_value, currentColumnNo, maximumColumns);
                const nextColumnNo = currentColumnNo + 1
                for (let i = nextColumnNo; i <= maximumColumns; i++) {
                    // colSpan = colSpan + this.nodeTypeData[i] ? this.nodeTypeData[i][0]['metadataList'].length : 0;
                    if (this.nodeTypeData[i] && this.nodeTypeData[i][0] && this.nodeTypeData[i][0]['metadataList']) {
                        for (let j = 0; j < this.nodeTypeData[i][0]['metadataList'].length; j++) {
                            element[this.nodeTypeData[i][0]['key'] + '_' + i + '_' + this.nodeTypeData[i][0]['metadataList'][j]['internal_name']] = '';
                        }
                    }
                }
                // element[Object.keys(element)[Object.keys(element).length - 1]] = last_value + '#||#' + colSpan;
            }
        });
    }


}

export class SetUtilsFunction {

    taxonomyData;
    dataSet;
    selectedNode;
    objProp;

    setParameters(obj) {
        if (obj.parameters.reset) {
            this.taxonomyData = obj.data;
        }
        if (obj.parameters.propName) {
            this.objProp = obj.parameters.propName;
        }
        if (obj.parameters.isMultipleCall) {
            if (obj.parameters.searchResultList) {
                this.dataSet = obj.parameters.searchResultList;
            }
            if (obj.parameters.selectedNode) {
                this.selectedNode = obj.parameters.selectedNode;
            }
        } else {
            this.dataSet = null;
            this.selectedNode = obj.parameters.selectedNode;
        }
    }

    callExpandTreeNodeFunction(treeData, isMultipleCall: boolean) {
        // console.log(this.dataSet);
        if (isMultipleCall) {
            if (treeData instanceof Array) {
                treeData.forEach(tree => {
                    this.expandTreeTillSelectedNode(this.selectedNode, tree, this.objProp);
                });
            } else {
                this.dataSet.forEach(node => {
                    this.expandTreeTillSelectedNode(node, treeData, this.objProp);
                });
            }

        } else {
            this.expandTreeTillSelectedNode(this.selectedNode, treeData, this.objProp);
        }
    }
    /********************************************************/
    /** To expand node of any taxonomy tree data upto selected node
     * @param selectedNode (upto which node tree needs to be expanded)
     * @param taxonomydata (main taxonomy tree data)
     */
    expandTreeTillSelectedNode(selectedNode: any, taxonomydata: any, propName?: any) {
        this.taxonomyData = taxonomydata;
        this.getAllParents(selectedNode, propName);
    }
    getAllParents(node: any, propName) {
        if (node) {
            if (node.parent_id && node.is_document !== 1) {
                this.iterate(this.taxonomyData, node, propName);
            } else if (node.is_document === 1) {
                node['expand'] = true;
                if (propName) {
                    node[propName] = true;
                }
            }
        }
    }
    iterate(current: any, node: any, propName) {
        const id = node.parent_id;
        if (current && current.children) {
            const children = current.children;
            if (current.id === id) {
                current['expand'] = true;
                if (propName) {
                    current[propName] = true;
                }
                this.getAllParents(current, propName);
            } else if (propName && current.id === node.id) {
                current[propName] = true;
                // this.getAllParents(current, propName);
            }
            for (let i = 0, len = children.length; i < len; i++) {
                this.iterate(children[i], node, propName);
            }
        }
    }
    /********************************************************/

    callResetExpansion(dataArray, level) {
        this.taxonomyData = dataArray;
        this.expandTreeTillSelectedLevel(this.taxonomyData, level);
    }

    expandTreeTillSelectedLevel(dataArray, level) {
        // console.log('received tree data', JSON.stringify(dataArray));
        if (dataArray) {
            const array = dataArray.children ? dataArray.children : dataArray;
            if (array) {
                array.forEach(node => {
                    if (node) {
                        if (node.level && node.level === level) {
                            node.expand = true;
                        } else {
                            node.expand = false;
                        }

                        if (node.children && node.children.length) {
                            this.expandTreeTillSelectedLevel(node.children, level);
                        }

                    }
                });
            }
        }
    }
}

export class MergeHumanCodeFullStatementClass {
    expandLevel = 0;
    textLength = 300;

    setParameters(parameters) {
        this.expandLevel = parameters.expandLevel;
        this.textLength = parameters.textLength;
    }


    iterate(current, depth) {
        if (current && current.children) {
            const children = current.children;
            if (current.id) {
                current.value = current.id + '::' + current.parent_id;;
                current.checked = false;
                current.disabled = false;
                // current.collapsed = depth < Utils.EXPAND_LEVEL ? false : true;
                current.expand = depth <= this.expandLevel ? true : false;
                // current.collapsed = false;
            }
            if (current.title) {
                const maxLengthExceed = current.title.length > this.textLength ? true : false;
                current.text = current.title.substr(0, this.textLength);
                maxLengthExceed ? current.text = current.text + ' ...' : current.text = current.text + '';
            }
            if (!current.title) {
                if (current.full_statement) {
                    const maxLengthExceed = current.full_statement.length > this.textLength ? true : false;
                    current.text = current.human_coding_scheme + ' ' + current.full_statement.substr(0, this.textLength);
                    maxLengthExceed ? current.text = current.text + ' ...' : current.text = current.text + '';
                } else {
                    current.text = current.human_coding_scheme;
                }
            }
            // console.log('------------ ', current['text']);
            for (let i = 0, len = children.length; i < len; i++) {
                this.iterate(children[i], depth + 1);
            }
        }
    }
}
