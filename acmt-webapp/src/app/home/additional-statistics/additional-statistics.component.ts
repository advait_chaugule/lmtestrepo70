import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  from
} from 'rxjs';
import {
  IMyDrpOptions,
  IMyDateRangeModel
} from 'mydaterangepicker';
import {
  Subscription
} from 'rxjs/Subscription';
import Utils from '../../utils';

@Component({
  selector: 'app-additional-statistics',
  templateUrl: './additional-statistics.component.html',
  styleUrls: ['./additional-statistics.component.scss']
})
export class AdditionalStatisticsComponent implements OnInit, OnDestroy {

  activityLog = [];
  dashboardData: any;
  projects_created;
  projects_deleted;
  taxonomy_nodes_added;
  taxonomy_nodes_removed;
  noactivityMsg = false;
  user_session_details = [];
  startMonth;
  dateRange;
  top_contributor_list = [];
  temp_top_contributor_list = [];
  projectsList = [];
  loading = false;
  dateRangePickerOptions: IMyDrpOptions = {
    // other options...
    width: '100%',
    dateFormat: 'yyyy-mm-dd'
  };
  currentTab = 'Day';
  startDate: any;
  endDate: any;
  MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  // Initialized to specific date (09.10.2018).

  dateModel: any = {
    beginDate: {
      year: 2018,
      month: 10,
      day: 9
    },
    endDate: {
      year: 2018,
      month: 10,
      day: 19
    }
  };
  currentComponentEvent: Subscription;
  selectedProjectName = 'All Projects';
  selectedProjectId = 'All';
  showHomeScreen = null;
  showAdditionalStatistics = null;

  constructor(
    private service: CommonService,
    private sharedService: SharedService,
    private authenticateUser: AuthenticateUserService
  ) { }

  ngOnInit() {
    const date = new Date();
    date.setDate(date.getDate() - 7);
    this.startDate = this.getFormattedDate(date.getFullYear(), (date.getMonth() + 1), date.getDate());
    const currentDate = new Date();
    this.endDate = this.getFormattedDate(currentDate.getFullYear(), (currentDate.getMonth() + 1), currentDate.getDate());
    const dateRange = 'activity_log_start_date=' + this.startDate +
      '&activity_log_end_date=' + this.endDate;
    this.dateModel = {
      beginDate: {
        year: date.getFullYear(),
        month: (date.getMonth() + 1),
        day: date.getDate()
      },
      endDate: {
        year: currentDate.getFullYear(),
        month: (currentDate.getMonth() + 1),
        day: currentDate.getDate()
      }
    };
    this.loading = true;
    this.getAcitvitLog(dateRange);
    this.getProjects();

    this.sharedService.currentComponentEvent.subscribe((data: any) => {
      if (data && data.component === 'home') {
        this.loading = true;
        this.getAcitvitLog(dateRange);
        this.getProjects();
      }
    });
  }

  getProjects() {
    const url = GlobalSettings.PROJECT_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.projectsList = res.projects;
    }).catch(ex => {
      console.log('list of project ex ', ex);
    });
  }

  onTabSelected(tab) {
    this.currentTab = tab;
    const dateRange = 'activity_log_start_date=' + this.startDate +
      '&activity_log_end_date=' + this.endDate;
    this.getAcitvitLog(dateRange);
  }

  getAcitvitLog(daterange) {
    const url = GlobalSettings.GET_DASHBOARD_DATA + '?' + daterange + '&calendar_group_by_type=' + this.currentTab.toLowerCase();
    this.top_contributor_list = [];
    this.temp_top_contributor_list = [];
    this.loading = true;
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        console.log(' getServiceData  ', res);
        this.dashboardData = res;
        this.activityLog = this.dashboardData.activity_list;
        this.user_session_details = this.dashboardData.user_session_details;

        this.dashboardData.top_contributor_list.forEach(element => {
          element.total_time_spent_in_seconds = (element.total_time_spent_in_seconds * 1.0) / 3600.00;
          this.top_contributor_list.push(element);
        });

        this.temp_top_contributor_list = this.dashboardData.top_contributor_list;

        if (this.activityLog.length === 0) {
          this.noactivityMsg = true;
        } else {
          this.noactivityMsg = false;
        }
        // loadChart();

        this.fetchMetrics();
        this.showGraph();
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  // getAcitvitLog(daterange) {
  //   const dateRangeParam = '?' + daterange + '&calendar_group_by_type=' + this.currentTab.toLowerCase();
  //   this.getUserSessionDetails(dateRangeParam);
  // }

  showGraph() {
    const data = [];
    if (this.user_session_details && this.user_session_details.length > 0) {

      let viewType = 'date';
      if (this.currentTab === 'Day') {
        viewType = 'date';
      } else {
        viewType = this.currentTab.toLowerCase();
      }

      this.user_session_details.forEach(element => {
        element['sortedDate'] = new Date(element.date);
      });

      this.user_session_details.sort((a, b) => {
        const key1 = a.sortedDate;
        const key2 = b.sortedDate;

        if (key1 < key2) {
          return -1;
        } else if (key1 === key2) {
          return 0;
        } else {
          return 1;
        }
      });

      this.user_session_details.forEach(element => {
        let date = '';
        if (this.currentTab === 'Day') {
          date = element.date;
        }
        if (this.currentTab === 'Week') {
          date = element.start_date + '- ' + element.end_date;
        }
        if (this.currentTab === 'Month') {
          date = element.month_name;
        }
        const obj = {
          Date: date,
          Categories: [{
            Name: 'cat1',
            Value: 0
          }, {
            Name: this.currentTab,
            Value: element.user_visits
          }, {
            Name: 'cat3',
            Value: 0
          }],
          LineCategory: [{
            Name: 'minutes',
            Value: element.time_spent.minutes
          }]
        };
        // const obj = [date, element.user_visits, element.time_spent.minutes];
        // console.log('showGraph', obj);
        data.push(obj);
      });
    }
    showLineBarChart(data, data.length);

  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    console.log(' onDateRangeChanged ', event);
    this.startMonth = this.MONTHS[event.beginDate.month - 1];
    if (event.beginDate.month !== event.endDate.month) {
      this.dateRange = event.beginDate.day + ' - ' + event.endDate.day + ' ' + this.MONTHS[event.endDate.month - 1];
    } else {
      this.dateRange = event.beginDate.day + ' - ' + event.endDate.day;
    }
    this.startDate = this.getFormattedDate(event.beginDate.year, event.beginDate.month, event.beginDate.day);
    this.endDate = this.getFormattedDate(event.endDate.year, event.endDate.month, event.endDate.day);
    const dateRange = 'activity_log_start_date=' + this.startDate +
      '&activity_log_end_date=' + this.endDate;
    this.getAcitvitLog(dateRange);

  }

  fetchMetrics() {
    this.projects_created = this.dashboardData.count_metrices.projects_created;
    this.projects_deleted = this.dashboardData.count_metrices.projects_deleted;
    this.taxonomy_nodes_added = this.dashboardData.count_metrices.taxonomy_nodes_added;
    this.taxonomy_nodes_removed = this.dashboardData.count_metrices.taxonomy_nodes_removed;
  }

  // fetchMetrics(response) {
  //   this.projects_created = response.projects_created;
  //   this.projects_deleted = response.projects_deleted;
  //   this.taxonomy_nodes_added = response.taxonomy_nodes_added;
  //   this.taxonomy_nodes_removed = response.taxonomy_nodes_removed;
  // }

  getFormattedDate(year, month, day) {
    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }
    return year + '-' + month + '-' + day;
  }

  getUserSessionDetails(dateRange) {
    const url = GlobalSettings.GET_DASHBOARD_DATA + dateRange + '&type=user_session_details';
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.user_session_details = res.user_session_details;
        this.getCountMmetrices(dateRange);
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  getCountMmetrices(dateRange) {
    const url = GlobalSettings.GET_DASHBOARD_DATA + dateRange + '&type=count_metrices';
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.fetchMetrics();
        this.getTopContributorList(dateRange);
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  getTopContributorList(dateRange) {
    this.top_contributor_list = [];
    this.temp_top_contributor_list = [];
    const url = GlobalSettings.GET_DASHBOARD_DATA + dateRange + '&type=top_contributor_list';
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        res.top_contributor_list.forEach(element => {
          element.total_time_spent_in_seconds = (element.total_time_spent_in_seconds * 1.0) / 3600.00;
          this.top_contributor_list.push(element);
        });
        this.temp_top_contributor_list = res.top_contributor_list;
        this.getActivityList(dateRange);
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  getActivityList(dateRange) {
    const url = GlobalSettings.GET_DASHBOARD_DATA + dateRange + '&type=activity_list';
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.activityLog = res.activity_list;
        if (this.activityLog.length === 0) {
          this.noactivityMsg = true;
        } else {
          this.noactivityMsg = false;
        }

        this.showGraph();
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  selectProjectId(projectId, projectName) {
    Utils.focusOnElement('selectedProjectBtn'); // element id: 'selectedProjectBtn'
    this.selectedProjectName = projectName;
    this.selectedProjectId = projectId;
    if (projectId !== 'All') {
      this.top_contributor_list = [];
      if (this.temp_top_contributor_list) {
        from(this.temp_top_contributor_list)
          .subscribe(result => {
            from(result.contributed_projects)
              .filter((w: any) => w.project_id === projectId)
              .subscribe(data => {
                if (data !== undefined) {
                  const tempObj = {};
                  const time: any = Math.ceil(data.time_spent_seconds / 60);
                  tempObj['user_display_name'] = result.user_display_name;
                  tempObj['projects_display_name'] = data.project_display_name;
                  tempObj['total_time_spent_in_seconds'] = time;
                  tempObj['total_action_count'] = data.action_count;
                  this.top_contributor_list.push(tempObj);
                }
              });
          });
      }
    } else {
      this.top_contributor_list = this.temp_top_contributor_list;
    }
  }

  ngOnDestroy() {
    if (this.currentComponentEvent) {
      this.currentComponentEvent.unsubscribe();
    }
  }

}
