import {
  NgModule, NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';

import {
  HomeRoutingModule
} from './home-routing.module';
import {
  HomeComponent
} from './home.component';
import {
  MyDatePickerModule
} from 'mydatepicker';
import {
  MyDateRangePickerModule
} from 'mydaterangepicker';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { Shared3Module } from '../shared/shared3/shared3.module';
import { DefaultHomePageComponent } from './default-home-page/default-home-page.component';
import { AdditionalStatisticsComponent } from './additional-statistics/additional-statistics.component';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    MyDatePickerModule,
    MyDateRangePickerModule,
    FormsModule,
    ReactiveFormsModule,
    Shared3Module,
    PreLoaderModule
  ],
  declarations: [HomeComponent, DefaultHomePageComponent, AdditionalStatisticsComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class HomeModule { }
