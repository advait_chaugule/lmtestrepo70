import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  HttpClient
} from '@angular/common/http';
import {
  from
} from 'rxjs';
import {
  IMyDrpOptions,
  IMyDateRangeModel
} from 'mydaterangepicker';
import {
  filter
} from 'rxjs/operator/filter';
import {
  Subscription
} from 'rxjs/Subscription';
import Utils from '../../utils';
import {
  ActivatedRoute,
  Params,
  Router
} from '@angular/router';

@Component({
  selector: 'app-default-home-page',
  templateUrl: './default-home-page.component.html',
  styleUrls: ['./default-home-page.component.scss']
})
export class DefaultHomePageComponent implements OnInit, OnDestroy {

  newCountMetrices: any;
  Object = Object;
  listOfProjects = [];
  projectList = [];
  startDate: any;
  endDate: any;
  loading = true;
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'project_name',
    class: '',
    type: 'text'
  },
  {
    name: 'Issues',
    propName: 'issue',
    class: '',
    type: 'link',
    redirect: true,
    loadPage: true
  },
  {
    name: 'Status',
    propName: 'workflow_stage',
    class: '',
    type: 'text'
  },
  {
    name: 'Created By',
    propName: 'used_by',
    class: '',
    type: 'text'
  },
  {
    name: 'Role Assigned',
    propName: 'access_roles',
    class: '',
    type: 'text'
  },
  {
    name: 'Top Contributor',
    propName: 'top_contributor',
    class: '',
    type: 'text'
  }
  ];

  filterOptions = ['All', 'Created By Me', 'Assigned To Me'];
  selectedFilterOption = this.filterOptions[0];
  orgCode: any;
  showMore = false;
  changeOrganizationEvent: Subscription;

  constructor(
    private service: CommonService,
    private sharedService: SharedService,
    private router: Router,
    private commonService: CommonService) { }

  ngOnInit() {
    const date = new Date();
    this.startDate = '2019-07-12';
    this.endDate = this.getFormattedDate(date.getFullYear(), date.getMonth(), date.getDate());
    // tslint:disable-next-line:max-line-length
    const dateRange = '?activity_log_start_date=' + this.startDate + '&activity_log_end_date=' + this.endDate + '&calendar_group_by_type=day';
    this.getNewCountMetrices(dateRange);
    this.getProjectList(dateRange);
    this.orgCode = this.commonService.getOrgDetails() + '/';

    this.changeOrganizationEvent = this.sharedService.currentComponentEvent.subscribe((data: any) => {
      if (data && data.component === 'home') {
        this.loading = true;
        this.getNewCountMetrices(dateRange);
        this.getProjectList(dateRange);
      }
    });
  }

  getNewCountMetrices(dateRange) {
    const url = GlobalSettings.GET_DASHBOARD_DATA + dateRange + '&type=new_count_metrices';
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.newCountMetrices = res.new_count_metrices;
        console.log('new count metrics', this.newCountMetrices);
        for (const key in this.newCountMetrices) {
          if (true) {
            const count = this.newCountMetrices[key];
            this.newCountMetrices[key] = {
              value: count,
              property: key,
              unit: key.split('_')[0],
              display_name: key.replace('_', ' ').split(' ')[1].replace(/_/g, ' ')
            };
          }

        }
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  getProjectList(dateRange) {
    const url = GlobalSettings.GET_DASHBOARD_DATA + dateRange + '&type=project_list';
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.listOfProjects = res.project_list;
        console.log('project list', this.listOfProjects);
        const currentUser = localStorage.getItem('first_name') + '' + localStorage.getItem('last_name');
        this.listOfProjects.forEach(project => {
          const roles = []; // holds project roles

          // project created by
          project.used_by = (project.created_by_name && project.created_by_name === currentUser) ? 'me' : project.created_by_name;

          // project roles
          if (project.assigned_role && project.assigned_role.length > 0) {
            project.assigned_role.forEach((r: any) => {
              roles.push(r.name);
            });
          } else {
            roles.push('No Role');
          }

          project.access_roles = roles.join(', ');
        });

        this.filterProjectList(this.selectedFilterOption);
      }
      this.loading = false;
    }).catch((ex) => {
      console.log('catch ', ex);
      this.loading = false;
    });
  }

  getFormattedDate(year, month, day) {
    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }
    return year + '-' + month + '-' + day;
  }

  openDetails(data: any) {
    let path: string;
    const type = data.property;
    switch (true) {
      case type.includes('projects'):
        path = this.orgCode + Utils.ROUTE_PROJECT_LIST;
        break;
      case type.includes('taxonomies'):
        path = this.orgCode + Utils.ROUTE_TAXONOMY_LIST;
        break;
      case type.includes('comments'):
        path = this.orgCode + Utils.ROUTE_NOTIFICATIONS;
        break;
      default:
        path = this.orgCode + '/';
    }
    if (type === 'published_taxonomies') {
      this.router.navigate([path], {
        queryParams: {
          taxonomyType: 'published'
        }
      });
    } else {
      this.router.navigate([path, type]);
    }
  }

  goToOpenComments(data) {
    console.log(data);
    this.sharedService.setTitleEvent.next({
      type: 'project',
      title: data.project_name
    });
    const path = this.orgCode + Utils.ROUTE_PROJECT_DETAIL + '/' + data.project_id;
    this.router.navigate([path, 'open_comments']);
  }

  filterProjectList(option: any) {
    let filteredList = [];
    this.selectedFilterOption = option;
    if (option === 'All') {
      filteredList = this.listOfProjects;
    } else {
      if (option === 'Created By Me') {
        filteredList = this.listOfProjects.filter((proj: any) => {
          return proj.used_by === 'me';
        });
      }
      if (option === 'Assigned To Me') {
        filteredList = this.listOfProjects.filter((proj: any) => {
          return proj.used_by !== 'me';
        });
      }
    }

    if (filteredList.length > 10) {
      this.showMore = true;
      for (let i = 0; i < 10; i++) {
        this.projectList.push(this.listOfProjects[i]);
      }
    } else {
      this.showMore = false;
      this.projectList = filteredList;
    }
  }

  openProjectList() {
    const path = this.orgCode + Utils.ROUTE_PROJECT_LIST;
    const type = 'projects_related_to_me';
    this.router.navigate([path, type]);
  }

  ngOnDestroy() {
    if (this.changeOrganizationEvent) {
      this.changeOrganizationEvent.unsubscribe();
    }
  }
}
