import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import {
  CommonService
} from '../common.service';
import {
  GlobalSettings
} from '../global.settings';
import {
  HttpClient
} from '@angular/common/http';
import {
  from
} from 'rxjs';
import {
  IMyDrpOptions,
  IMyDateRangeModel
} from 'mydaterangepicker';
import {
  filter
} from 'rxjs/operator/filter';
import {
  SharedService
} from '../shared.service';
import {
  Subscription
} from 'rxjs/Subscription';
import Utils from '../utils';
import { AuthenticateUserService } from '../authenticateuser.service';

/// <reference path='../..assets/js/common.d.ts'>
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {

  activityLog = [];
  dashboardData: any;
  projects_created;
  projects_deleted;
  taxonomy_nodes_added;
  taxonomy_nodes_removed;
  noactivityMsg = false;
  user_session_details = [];
  startMonth;
  dateRange;
  top_contributor_list = [];
  temp_top_contributor_list = [];
  projectsList = [];
  currentTab = 'Day';
  loading = false;
  dateRangePickerOptions: IMyDrpOptions = {
    // other options...
    width: '100%',
    dateFormat: 'yyyy-mm-dd'
  };
  startDate: any;
  endDate: any;
  MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  // Initialized to specific date (09.10.2018).

  dateModel: any = {
    beginDate: {
      year: 2018,
      month: 10,
      day: 9
    },
    endDate: {
      year: 2018,
      month: 10,
      day: 19
    }
  };
  currentComponentEvent: Subscription;
  selectedProjectName = 'All Projects';
  showHomeScreen = true;
  showAdditionalStatistics = true;
  constructor(
    private authenticateUser: AuthenticateUserService,
    private sharedService: SharedService) {

  }

  ngOnInit() {
    this.userRoleRight();
    this.sharedService.faqEvent.next({
      name: 'home_page'
    });
  }

  selectProjectId(projectId, projectName) {
    Utils.focusOnElement('selectedProjectBtn'); // element id: 'selectedProjectBtn'
    this.selectedProjectName = projectName;
    if (projectId !== 'All') {
      this.top_contributor_list = [];
      from(this.temp_top_contributor_list)
        .subscribe(result => {
          from(result.contributed_projects)
            .filter((w: any) => w.project_id === projectId)
            .subscribe(data => {
              if (data !== undefined) {
                const tempObj = {};
                const time: any = Math.ceil(data.time_spent_seconds / 60);
                tempObj['user_display_name'] = result.user_display_name;
                tempObj['projects_display_name'] = data.project_display_name;
                tempObj['total_time_spent_in_seconds'] = time;
                tempObj['total_action_count'] = data.action_count;
                this.top_contributor_list.push(tempObj);
              }
            });
        });
    } else {
      this.top_contributor_list = this.temp_top_contributor_list;
    }
  }

  ngOnDestroy() {
    if (this.currentComponentEvent) {
      this.currentComponentEvent.unsubscribe();
    }
  }

  userRoleRight() {
    if (this.authenticateUser.AuthenticateUserRole('View Home Screen')) {
      this.showHomeScreen = true;
    } else {
      this.showHomeScreen = false;
    }
    if (this.authenticateUser.AuthenticateUserRole('View Home Screen')) {
      this.showHomeScreen = true;
    } else {
      this.showHomeScreen = false;
    }
    if (this.authenticateUser.AuthenticateUserRole('View Additional Statistics')) {
      this.showAdditionalStatistics = true;
    } else {
      this.showAdditionalStatistics = false;
    }
  }

}
