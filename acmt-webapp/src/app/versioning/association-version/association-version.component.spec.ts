import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociationVersionComponent } from './association-version.component';

describe('AssociationVersionComponent', () => {
  let component: AssociationVersionComponent;
  let fixture: ComponentFixture<AssociationVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociationVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociationVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
