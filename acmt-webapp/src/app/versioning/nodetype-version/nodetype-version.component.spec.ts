import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodetypeVersionComponent } from './nodetype-version.component';

describe('NodetypeVersionComponent', () => {
  let component: NodetypeVersionComponent;
  let fixture: ComponentFixture<NodetypeVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodetypeVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodetypeVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
