import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nodetype-version',
  templateUrl: './nodetype-version.component.html',
  styleUrls: ['./nodetype-version.component.scss']
})
export class NodetypeVersionComponent implements OnInit {

  listOfColumn = []; // holds list of column headers in version-summary table
  dataArray = []; // holds the data array to be shown in version-summary table
  defaultSortByColumn: any; // holds the column object by which the table data will be sorted by default
  pageDetail: any; // holds object for pagination details

  constructor() { }

  ngOnInit() {
    this.listOfColumn = [
      {
        name: 'Updates to',
        propName: 'title',
        class: '',
        type: 'text'
      },
      {
        name: 'Type',
        propName: 'title',
        class: '',
        type: 'text'
      },
      {
        name: 'New',
        propName: 'title',
        class: '',
        type: 'text'
      },
      {
        name: 'Updated',
        propName: 'title',
        class: '',
        type: 'text'
      },
      {
        name: 'Deleted',
        propName: 'title',
        class: '',
        type: 'text'
      }
    ];
    this.defaultSortByColumn = this.listOfColumn[2];
  }

  // FUNCTION to change pagination
  getChanges(evt) {
  }

}
