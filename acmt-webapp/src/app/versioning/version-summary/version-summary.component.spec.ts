import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionSummaryComponent } from './version-summary.component';

describe('VersionSummaryComponent', () => {
  let component: VersionSummaryComponent;
  let fixture: ComponentFixture<VersionSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
