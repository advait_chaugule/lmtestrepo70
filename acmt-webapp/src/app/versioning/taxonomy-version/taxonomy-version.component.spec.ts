import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyVersionListComponent } from './taxonomy-version-list.component';

describe('TaxonomyVersionListComponent', () => {
  let component: TaxonomyVersionListComponent;
  let fixture: ComponentFixture<TaxonomyVersionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxonomyVersionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomyVersionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
