import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy
} from '@angular/core';
import {
  Params,
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  ProjectMappedNodeService
} from '../../project-mapped-node.service';
import {
  SharedService
} from '../../shared.service';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';

import Utils from '../../utils';
/*import {
  WalktroughService
} from '../../help/walkthrough/walktrough.service';
import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';*/
import {
  TreeAccordianComponent
} from '../../tree-accordian/tree-accordian.component';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  TabularTreeViewComponent
} from '../../common/tabular-tree-view/tabular-tree-view.component';
import {
  from
} from 'rxjs';
import {
  ItemDetailsComponent
} from '../../common/item-details/item-details.component';
import { TreeComponent } from '../../common/tree/tree.component';
import { TableConfigurationComponent } from 'src/app/common/table-configuration/table-configuration.component';
import { TreeSearchComponent } from '../../common/tree-search/tree-search.component';
import { NodeTypeTableViewComponent } from '../../common/node-type-table-view/node-type-table-view.component';
/// <reference path="../../..assets/js/common.d.ts">

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['../../taxonomy/taxonomy-list/taxonomy-list.component.scss', './project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

  LEFT_MIN_SIZE = Utils.LEFT_PANEL_MIN_SIZE; // Left panel min size
  LEFT_MAX_SIZE = Utils.LEFT_PANEL_MAX_SIZE; // Left panel max size

  @ViewChild('tabularView', { static: false }) tabularView: TabularTreeViewComponent;
  @ViewChild('tree', { static: false }) tree: TreeComponent;
  @ViewChild('itemDetailsComponent', { static: false }) itemDetailsComponent: ItemDetailsComponent;
  @ViewChild('configTableRef', {
    static: false
  }) configTableRef: TableConfigurationComponent;
  @ViewChild('treeSearch', {
    static: false
  }) treeSearch: TreeSearchComponent;
  @ViewChild('nodeTypeTableView', {
    static: false
  }) nodeTypeTableView: NodeTypeTableViewComponent;
  projectId;
  projectDetail;
  taxonomyData;
  isAllNode = false;
  currentTreeTab = 'detailview';
  treeTabItems = ['detailview', 'tableview'];
  // tabItems = ['Home', 'Activity', 'Comments', 'Assets', 'Team'];
  tabItems = ['Home'];
  currentTab;
  editUserPermission = false;
  addUserPermission = false;
  selectedNode = null;
  projectReportPermission = false;
  searchResult = [];
  searchTrigger = false;
  searchResultEvent: Subscription;
  isFirstTime = true;
  isFirstSearchInitialise = true;
  preventEdit = false;
  itemComplianceReports = [];
  assignedToMe = 'all';
  isEditable = false;
  nodeSelectedEvent: Subscription;
  treeNode: any;
  itemAssociations: any;
  itemExemplars = [];
  itemAssets = [];
  itemAdditionalMetadata = [];
  itemLinkedNodes = [];
  nodetypeData;
  selectedNodeType = {
    title: '',
    node_type_id: ''
  };
  windowResizeSubscription: Subscription;
  list_enumeration;
  sequence_number;
  education_level;
  item_associations;
  showAddMetaData = true;
  showAssocciation = true;
  showDocument = true;
  showExemplar = true;
  viewLocation = 'taxonomyDetails';
  showNavigation = true; // holds boolean value for showing up navigation arrow
  listOfColumn = [];
  isSearching = false;
  dynamicClass = 'col-md-5 col-lg-4 col-xl-5';
  isLoading = false;
  listOfClass = [{
    class: 'col-md-2 col-lg-2 col-xl-2',
  },
  {
    class: 'col-md-3 col-lg-3 col-xl-3',
  },
  {
    class: 'col-md-1 col-lg-1 col-xl-1',
  },
  {
    class: 'col-md-1 adjust-last-col',
  }
  ];

  colHeaders = ['full_statement', 'human_coding_scheme']; // holds internal names for column headers
  metadataList = []; // holds metadata list used for table view headers
  defaultCommentType = 'all'; // holds default comment type
  canConfirureTable = true;
  listOfTableData = [];
  currentDataSet: any;
  defaultColumnList: any;
  additionalColumnList = [];
  canConfigureTableView = false;
  canSetDefaultForAll = false;
  tableViewList = [
    {
      name: 'Default',
      code_name: 'supper_default_table',
      isActive: false
    }, {
      name: 'Metadata',
      code_name: 'metadata_table',
      isActive: false
    }, {
      name: 'Nodetype',
      code_name: 'node_type_table',
      isActive: false
    }
  ];
  defaultNodeTypelist = [];
  additionalMetadata = [];
  additionalNodes = [];
  dropdownData: any = {
    uniqueId: 'metadata_id',
    propName: 'display_name'
  };
  configureTab = 'Metadata Fields';
  defaultmetadataArray = [];
  selectedTable = 'supper_default_table';
  enableButton = false;
  isCompatible = false;
  saveTable = false;
  selectedStageId = ''; // holds selected stage from project authoring
  isCalled = 0;
  searchResultList = [];
  filterCriteriaList = [];
  arrayToSearch = [];
  searchText = '';
  isFilterApplied = false;
  initialiseSearch = true;
  worker: any;
  isReset = false;
  showUserFriendlyPage = false;
  constructor(
    private acivatedRoute: ActivatedRoute,
    private router: Router,
    private service: CommonService,
    private mappedNodeService: ProjectMappedNodeService,
    private sharedService: SharedService,
    private authenticateUser: AuthenticateUserService
    /*,
        private walkService: WalktroughService,
        private tourService: NgxBootstrapProductTourService*/
  ) {
    this.getMetadataList();
    this.acivatedRoute.params.subscribe((params: Params) => {
      if (params['id'] !== undefined) {
        this.projectId = params['id'];
        this.getProjectDetail();
        // this.getProjectMappedNodes();
        // this.getAllNodes();
        this.getProjectAssociatedNodes();
        // this.userRoleRight();

        if (params.type && params.type === 'open_comments') {
          this.defaultCommentType = 'opened';
          this.onTabSelected('comments');
        } else {
          this.currentTab = this.tabItems[0].toLowerCase();
        }
      } else {
        const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_LIST;
        this.router.navigate([path]);
      }
    });
    this.searchResultEvent = this.sharedService.searchResultEvent.subscribe((item: any) => {
      if (this.isFirstTime) {
        this.isFirstTime = false;
        return;
      }
      if (!this.isFirstTime) {
        if (item) {
          this.searchResult = item['searchData'];
          if (item['showSearchList']) {
            this.searchTrigger = true;
          } else {
            this.searchTrigger = false;
          }
          this.isFilterApplied = false;
        }
      }
    });



    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      if (item) {
        if (item.id) {
          this.onTreeNodeSelected(item);
        }
      }
    });

    this.windowResizeSubscription = this.sharedService.windowResizeEvent.subscribe((item: any) => {
      if (item) {
        this.nodeTypeViewHeightCalculation();
        this.taxonomyDetailsHeightCalculation();
        this.getLeftPanelWidth();
      }
    });

    this.sharedService.faqEvent.next({
      name: 'project_detail'
    });

  }

  ngOnInit() {
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('project_detail'));*/
    this.getAllMetadata();
    this.getDataList();
    // this.getNodeTypeDetails('metadata');
    // this.getNodeTypeDetails('node_type');
  }

  getProjectDetail() {
    const url = GlobalSettings.PROJECT_LIST + '/' + this.projectId;
    this.service.getServiceData(url).then((res) => {
      this.projectDetail = res;
      localStorage.setItem('project_details', JSON.stringify({ id: this.projectId, title: this.projectDetail.project_name }));
      if (res['project_type']) {
        localStorage.setItem('project_type', res['project_type']);
      }
      if (res && res['current_workflow_stage_id']) {
        this.selectedStageId = res['current_workflow_stage_id'].split('||')[1];
      }
      this.sharedService.workFlowId.next({
        'workFlowId': res['workflow_id'],
        'projectId': this.projectId,
        'currentWorkflowStage': res['current_workflow_stage_id'].split('||')[1]
      });
      if (res['document_status'] === 5 || res['document_status'] === 3) {
        this.preventEdit = true;
      } else {
        this.preventEdit = false;
      }
      this.userRoleRight(res['current_workflow_stage_id'].split('||')[1]);
    }).catch((ex) => {
      console.log('getProjectDetail ', ex);
    });
  }

  getProjectAssociatedNodes() {
    this.isAllNode = false;
    const url = GlobalSettings.GET_PROJECT_TREE_VIEW_ENHANCED + this.projectId;
    this.mappedNodeService.getTreeData(url, Utils.EXPAND_LEVEL).then((res: any) => {
      if (res) {
        console.log(' getProjectAssociatedNodes  ', res);
        // Utils.sortData(res);
        this.taxonomyData = res;
        this.arrayToSearch = this.taxonomyData[0].children[0];
        if (this.taxonomyData && this.taxonomyData[0]) {
          this.selectedNode = this.taxonomyData[0].children[0];
        }
        // this.userRoleRight();
        this.getItemsComplianceReport(); // For compliance icon setting
        this.onTreeTabSelected(this.currentTreeTab);
        if (this.tabularView && this.currentTreeTab === 'tableview') {
          console.log('Called 1');
          this.tabularView.setTableData(this.taxonomyData);
        }
        setTimeout(() => {
          if (this.treeSearch) {
            this.treeSearch.getFilterData(this.taxonomyData, true);
            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        }, 500);
      } else {
        this.sharedService.sucessEvent.next({
          type: 'no_project'
        });
        setTimeout(() => {
          this.redirectToProjectList();
        }, 500);
      }
      console.log(' getProjectAssociatedNodes ', res);
    }).catch((ex) => {
      console.log('getProjectAssociatedNodes ', ex);
      if (ex && ex.status === 400) { // If no project nodes found, redirect to project list
        this.sharedService.sucessEvent.next({
          type: 'no_project'
        });
        setTimeout(() => {
          this.redirectToProjectList();
        }, 500);
      }
    });
  }
  onTabSelected(tab) {
    if (tab.toLowerCase() === 'documents' || tab.toLowerCase() === 'team') {
      this.sharedService.faqEvent.next({
        name: tab.toLowerCase() + '_project'
      });
    } else {
      this.sharedService.faqEvent.next({
        name: 'project_detail'
      });
    }

    if (tab.toLowerCase() === 'home') {
      // this.onTreeTabSelected(this.currentTreeTab);
      Utils.removeBodyScrollClass();
      Utils.removeBodyScroll();
      console.log('this.currentTreeTab', this.currentTreeTab, this.selectedTable)
      setTimeout(() => {
        if (this.currentTreeTab === 'detailview') {
          this.taxonomyDetailsHeightCalculation();
        } else if (this.currentTreeTab === 'tableview' && this.selectedTable === 'node_type_table') {
          // this.nodeTypeViewHeightCalculation();
          this.onTreeTabSelected(this.currentTreeTab);
        }
      }, 500);
    }
    if (tab.toLowerCase() !== 'home') {
      Utils.addBodyScroll();
      Utils.removeBodyScrollClass();
    }
    setTimeout(() => {
      this.currentTab = tab;
    }, 10);
  }

  onTreeTabSelected(tab) {
    console.log('Called 2');
    this.currentTreeTab = tab;
    Utils.removeBodyScrollClass();
    init_treeViewClick();
    if (this.tabularView && this.currentTreeTab === 'tableview') {
      Utils.removeBodyScroll();
      if (this.isCalled < 2) {
        this.getNodeTypeDetails('metadata');
        this.getNodeTypeDetails('node_type');
      }

      if (this.selectedTable === 'node_type_table') {
        setTimeout(() => {
          // Utils.removeBodyScroll();
          this.nodeTypeViewHeightCalculation();
        }, 50);
        setTimeout(() => {
          if (this.nodeTypeTableView) {
            this.nodeTypeTableView.configTable(this.taxonomyData);
          }
        }, 50);
      } else {
        this.tabularView.setTableData(this.taxonomyData);
        // Utils.removeBodyScrollClass();
      }
    }
    if (tab === 'detailview') {
      Utils.addBodyScrollClass();
      setTimeout(() => {
        let clickableNode: any;
        if (this.taxonomyData && this.taxonomyData[0] && this.taxonomyData[0].children[0]) {
          if (this.taxonomyData[0].children[0]['isOrphanLabel']) {
            clickableNode = this.taxonomyData[0].children[0].children[0];
          } else {
            clickableNode = this.taxonomyData[0].children[0];
          }
          this.selectNodeOnNavigation(clickableNode);
          this.taxonomyDetailsHeightCalculation();
          // if (document.getElementById(clickableNode['id'] + '-node')) {
          //   document.getElementById(clickableNode['id'] + '-node').click();
          //   // taxonomyDetailsHeightCalculation();
          //   this.taxonomyDetailsHeightCalculation();
          // }
        }
      }, 2000);
    }
  }

  onEditClick() {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_AUTHORING;
    localStorage.setItem('project_type', '1');
    localStorage.setItem('filterCriteriaList', JSON.stringify(this.filterCriteriaList));
    this.router.navigate([path, this.projectId]);
  }

  openSettings() {
    const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_TABLE_CONFIGURATION;
    localStorage.setItem('project_type', '1');
    this.router.navigate([path, this.projectId]);
  }

  userRoleRight(selectedStageId) {
    this.selectedStageId = selectedStageId;
    this.tabItems = ['Home', 'Documents', 'Team', 'Reports', 'Comments'];
    /*if (this.authenticateUser.AuthenticProjectPermissions('Project Team')) {
      // this.tabItems = ['Home', 'Team'];
      this.tabItems = ['Home', 'Documents', 'Team', 'Reports'];
    } else {
      this.tabItems = ['Home'];
    }*/
    let project_teamGlobal = this.authenticateUser.AuthenticProjectPermissions('Project Team');
    const project_team = Utils.checkProjectPermission(this.projectId, 'Project Team', 'project_permissions', this.selectedStageId);
    if (project_team.hasProject) {
      project_teamGlobal = project_team.valid;
    }
    if (!project_teamGlobal) {
      this.removeTab('Team');
    }

    this.addUserPermission = this.authenticateUser.AuthenticProjectPermissions('Create Project User');
    const addUser = Utils.checkProjectPermission(this.projectId, 'Create Project User', 'project_permissions', this.selectedStageId);
    if (addUser.hasProject) {
      this.addUserPermission = addUser.valid;
    }

    this.editUserPermission = this.authenticateUser.AuthenticProjectPermissions('Manage Project User Role');
    const editUser = Utils.checkProjectPermission(this.projectId, 'Manage Project User Role', 'project_permissions', this.selectedStageId);
    if (editUser.hasProject) {
      this.editUserPermission = editUser.valid;
    }

    this.projectReportPermission = this.authenticateUser.AuthenticProjectPermissions('Project Reports');
    const projectReport = Utils.checkProjectPermission(this.projectId, 'Project Reports', 'project_permissions', this.selectedStageId);
    if (projectReport.hasProject) {
      this.projectReportPermission = projectReport.valid;
    }
    if (!this.projectReportPermission) {
      this.removeTab('Reports');
    }

    this.canConfigureTableView = this.authenticateUser.AuthenticProjectPermissions('Update project table view');
    const configureTableView = Utils.checkProjectPermission(this.projectId, 'Update project table view', 'project_permissions', this.selectedStageId);
    if (configureTableView.hasProject) {
      this.canConfigureTableView = configureTableView.valid;
    }

    // if (this.authenticateUser.AuthenticProjectPermissions('Update project table view')) {
    //   this.canConfigureTableView = true;
    // }

    this.canSetDefaultForAll = this.authenticateUser.AuthenticProjectPermissions('Admin setup for project table view');
    const canSetDefaultForAll = Utils.checkProjectPermission(this.projectId, 'Admin setup for project table view', 'project_permissions', this.selectedStageId);
    if (canSetDefaultForAll.hasProject) {
      this.canSetDefaultForAll = canSetDefaultForAll.valid;
    }

  }

  removeTab(tabName: any) {
    if (this.tabItems && this.tabItems.length > 0) {
      this.tabItems = this.tabItems.filter(function (item) {
        return (item !== tabName);
      });
    }
  }

  checkProjectCanBeDeleted(project_id) {

    let isDeletable = this.authenticateUser.AuthenticProjectPermissions('Delete Project');
    const editProject = Utils.checkProjectPermission(project_id, 'Delete Project', 'project_permissions', this.selectedStageId);
    if (editProject.hasProject) {
      isDeletable = editProject.valid;
    }
    return isDeletable;
  }

  redirectToProjectList() {
    let path;
    if (localStorage.getItem('project_type') === '2') {
      path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PACING_GUIDE_LIST;
    } else {
      path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_LIST;
    }
    this.router.navigate([path]);
  }
  /*public checkProjectPermission(project_id, permissionName) {
    let valid = false;
    let hasProjectPermission = false;
    const projectsPermission = JSON.parse(localStorage.getItem('project_role_permissions'));
    if (projectsPermission && projectsPermission.length > 0) {
      for (let i = 0; i < projectsPermission.length; i++) {
        const element = projectsPermission[i];
        if (element.project_id === project_id) {
          hasProjectPermission = true;
          if (element.project_permissions !== undefined) {
            Rx.from(element.project_permissions)
              .filter((x: any) => x.display_name === permissionName)
              .subscribe(data => {
                valid = true;
              });
          }
        }
      }
    } else {
      // console.log('There is no project permission');
    }

    return {
      'valid': valid,
      'hasProject': hasProjectPermission
    };
  }*/

  /**
   * To get items compliance report
   */
  getItemsComplianceReport() {
    if (this.projectId) {
      this.itemComplianceReports = [];
      const url = GlobalSettings.ITEM_COMPLIANCE_REPORT + '/' + this.projectId + '/2';
      this.service.getServiceData(url).then((res: any) => {
        this.itemComplianceReports = res;
        for (let i = 0; i < this.taxonomyData.length; i++) {
          this.setComplianceStatusForAllNode(this.taxonomyData[i]); // setting compliance icon
        }
      }).catch((ex) => {
        console.log('getItemsComplianceReport ', ex);
      });
    }
  }

  /**
   * To set compliance status for each taxonomy node
   */
  setComplianceStatusForAllNode(data: any) {
    if (data) {
      if (data.children.length > 0) {
        data.compStatus = this.getComplianceStatusForItem(data.id); // 'compStatus' will hold compliance status value
        for (let i = 0; i < data.children.length; i++) {
          this.setComplianceStatusForAllNode(data.children[i]);
        }
      } else {
        data.compStatus = this.getComplianceStatusForItem(data.id);
      }
    }
  }

  /**
   * To get compliance status value for respective item id (status values: 0,1,2)
   * @param itemId
   */
  getComplianceStatusForItem(itemId) {
    if (itemId) {
      for (let i = 0; i < this.itemComplianceReports.length; i++) {
        if (itemId === this.itemComplianceReports[i].item_id) {
          return this.itemComplianceReports[i].status;
        }
      }
    }
    return null;
  }

  onTreeNodeSelected(item) {
    console.log('onTreeNodeSelected ', item);
    this.treeNode = item;
    this.getSelectedTreeItemDetail(item);
  }

  getSelectedTreeItemDetail(item) {

    let url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + item.id;
    if (item['is_document'] === 1) {
      url = GlobalSettings.GET_CFDOC_ITEM_DETAIL + '/' + item.id;
    } else {
      url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + item.id;
    }
    this.itemAssociations = [];
    this.itemExemplars = [];
    this.itemAssets = [];
    this.itemAdditionalMetadata = [];
    this.itemLinkedNodes = [];
    this.service.getServiceData(url).then((res: any) => {
      console.log(' getSelectedTreeItemDetail ', res);
      if (this.treeNode['is_document'] === 1) {
        // this.showAddMetaData = false;
        res['is_editable'] = 1;
        if (!res.node_type_id) {
          res['node_type_id'] = this.treeNode.node_type_id;
          res['node_type'] = this.treeNode.title;
        }
        res['source_uuid_id'] = res['source_document_id'];
      } else {
        res['source_uuid_id'] = res['source_item_id'];
      }

      if (res.node_type_id) {
        this.setNodeTypeById(res.node_type_id);
      }

      if (this.itemDetailsComponent) {
        this.itemDetailsComponent.generateFormData(res, this.selectedNodeType.title);
      }
      this.list_enumeration = res.list_enumeration;
      this.sequence_number = res.sequence_number;
      this.education_level = res.education_level;
      this.item_associations = res.item_associations;
      // Move scroll to top
      const rightContainer = document.getElementById('right_container_id');
      if (rightContainer) {
        rightContainer.scrollTop = 0;
      }
      setTimeout(() => {
        this.showAssocciation = true;
        this.showExemplar = item.node_type === 'Document' ? false : true;
        this.itemAssociations = res.item_associations;
        if (res.exemplar_associations) {
          this.itemExemplars = res.exemplar_associations;
        }
        if (res.assets) {
          this.itemAssets = res.assets;
        }
        if (res.linked_item) {
          this.itemLinkedNodes = res.linked_item;
        }
        if (res.custom_metadata) {
          this.itemAdditionalMetadata = [];
          if (res.custom_metadata) {
            res.custom_metadata.forEach(obj => {
              if (obj.is_additional_metadata === 1) {
                this.itemAdditionalMetadata.push(obj);
              }
            });
          }
        }
        this.showAddMetaData = true;
        console.log('itemExemplars', this.itemExemplars);
      }, 2000);
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
    });

  }

  setNodeTypeById(node_type_id) {
    if (this.nodetypeData) {
      from(this.nodetypeData)
        .filter((w: any) => w.node_type_id === node_type_id)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
        });
    }
  }

  getAllMetadata() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.nodetypeData = res.nodetype;
    });
  }

  ngOnDestroy() {
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    if (this.windowResizeSubscription) {
      this.windowResizeSubscription.unsubscribe();
    }
    Utils.addBodyScroll();
  }


  taxonomyDetailsHeightCalculation() {
    const windowHeight = window.innerHeight;
    let tabHeight;
    let titleHeight;
    if (document.getElementById('treeTabContainer') && document.getElementById('tabContainer')) {
      tabHeight = document.getElementById('treeTabContainer').offsetHeight + document.getElementById('tabContainer').offsetHeight;
    }
    if (document.getElementsByClassName('authoring-header')) {
      titleHeight = document.getElementsByClassName('authoring-header')[0].clientHeight;
    }
    // tslint:disable-next-line:max-line-length
    const protipHeight = document.getElementById('taxonomyProtip') && document.getElementById('taxonomyProtip').offsetHeight ? document.getElementById('taxonomyProtip').offsetHeight : 0;
    const extraHeight = 69;
    const panelHeight = windowHeight - (tabHeight + titleHeight + protipHeight + extraHeight);
    if (document.getElementById('treeDetailsView')) {
      document.getElementById('treeDetailsView').style.height = panelHeight + 'px';
    }
    // document.getElementById('right_container_id').style.height = panelHeight + 'px';
    if (document.getElementById('right_container_id')) {
      document.getElementById('right_container_id').style.height = panelHeight + 'px';
    }
  }

  onNextNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  onPreviousNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  selectNodeOnNavigation(node: any) {
    this.treeNode = node;
    if (this.taxonomyData) {
      this.callExpandTreeFromWorker(node);

      for (const tree of this.taxonomyData) {
        Utils.expandTreeTillSelectedNode(this.treeNode, tree);
      }
    }
    if (this.tree) {
      this.tree.onNodeSelected(this.treeNode);
    }
    setTimeout(() => {
      const id = this.treeNode['id'] ? this.treeNode['id'] : this.treeNode['item_id'];
      if (document.getElementById(id + '-node')) {
        document.getElementById(id + '-node').focus();
      }
    }, 100);
  }

  /* --------- Fetch metadata List functionality Started --------- */

  getMetadataList() {
    const url = GlobalSettings.GET_METADATA_LIST + '?is_custom=0';
    this.service.getServiceData(url).then((res: any) => {
      this.metadataList = res.metadata;

      this.colHeaders.forEach(header => {
        this.listOfColumn.forEach(element => {
          if (element.propName === header) {
            element.name = this.extractMetadataName(header);
          }
        });
      });
    }).catch((ex) => {
      console.log('Error caught while fetching metadata list', ex);
    });
  }

  /* --------- Fetch metadata List functionality Ended --------- */


  /* --------- Functionality to fetch metadata name from internal name start --------- */

  extractMetadataName(interal_name) {
    for (const i in this.metadataList) {
      if (i && this.metadataList[i]['internal_name'] === interal_name) {
        return this.metadataList[i]['name'];
      }
    }
  }

  /* --------- Functionality to fetch metadata name from internal name end --------- */

  getLeftPanelWidth() {
    let leftMenuWidth;
    let menuButtonWidth;
    let actualWholePanelWidth;
    let rightPanelWidth;
    const showNav = JSON.parse(localStorage.getItem('isShowNav'));
    const windowWidth = window.innerWidth;
    if (document.getElementById('treeViewContainer')) {
      actualWholePanelWidth = document.getElementById('treeViewContainer').offsetWidth;
    }
    if (document.getElementById('right_container_id')) {
      rightPanelWidth = document.getElementById('right_container_id').offsetWidth;
    }
    if (document.getElementById('mainNav')) {
      leftMenuWidth = document.getElementById('mainNav').offsetWidth;
    }
    if (document.getElementById('menuButton')) {
      menuButtonWidth = document.getElementById('menuButton').offsetWidth;
    }
    const navbarWidth = showNav ? leftMenuWidth : menuButtonWidth;
    const withoutNavbarWidth = windowWidth - navbarWidth;
    const extraWidth = withoutNavbarWidth - actualWholePanelWidth - 15;
    let leftPanelWidth;
    leftPanelWidth = (actualWholePanelWidth - rightPanelWidth) + extraWidth;
    return (leftPanelWidth + 'px');
  }

  // Table View Configuration STARTS

  getDataList() {
    this.isLoading = true;
    this.isFirstSearchInitialise = true;
    this.defaultmetadataArray = [];
    const featureId = 4;
    const settingId = 4;
    const url = GlobalSettings.TABLE_CONFIG + '/' + featureId + '/' + settingId + '?project_id=' + this.projectId;
    this.service.getServiceData(url, true, true).then((res: any) => {
      console.log('tabular view get data', res);
      if (JSON.parse(res.success)) {
        this.listOfTableData = res.data.project_table_views;
        const defaultTable = res.data.project_table_views[0];
        defaultTable.table_config.forEach(item => {
          item.metadata.forEach(data => {
            data.internal_name = data.internal_name.toLowerCase().replace(' ', '_');
            if (data.internal_name !== 'full_statement') {
              data.canRemove = true;
            }
            if (data.internal_name === 'node_type') {
              data.metadata_id = 'node_type_1234';
            }
            if (this.defaultmetadataArray.indexOf(data) === -1) {
              this.defaultmetadataArray.push(data);
            }
          });
        });
        if (this.listOfTableData.length > 1) {
          if (res.data.last_table_view) {
            this.loadTable(res.data.last_table_view);
          }
        } else {
          this.loadTable('supper_default_table');
        }

      } else {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: res.message
        });
      }
      this.extractFilterCriteriaList();
      // this.isLoading = false;
      this.isFirstSearchInitialise = false;
    }).catch((ex) => {
      console.log('error', ex);
    });
  }

  getNodeTypeDetails(viewType) {
    this.isCalled += 1;
    const type = 2;
    const returnOrder = 'return_order=1';
    const url = GlobalSettings.TAXONOMY_NODE_TYPE_DETAILS + '/' + this.projectId + '/' + type + '/' + viewType + '?' + returnOrder;
    this.service.getServiceData(url, true, true).then((res: any) => {
      console.log('taxonomy node type detail response #########', res.data);
      if (res.success) {

        if (viewType === 'metadata') {
          this.additionalMetadata = res.data.metadata;
          const typeMetadata = this.defaultmetadataArray.find(data => data.internal_name === 'node_type');
          this.additionalMetadata.push(typeMetadata);
        } else {
          this.defaultNodeTypelist = [];
          this.additionalNodes = [];
          this.isCompatible = res.data.compatibility;
          res.data.nodeTypes.forEach(element => {
            element.selectedMetadataList = [];
            element.metadata.forEach(item => {
              item.selectDisplay = false;
            });
          });
          this.constructData(res.data.nodeTypes, 'node_type');
          this.additionalNodes = res.data.nodeTypes;
          res.data.defaultNodeTypesOrder.forEach(element => {
            if (element && (this.defaultNodeTypelist.findIndex(data => data.node_type_id === element) === -1)) {
              this.defaultNodeTypelist.push(res.data.nodeTypes.find((node: any) => node.node_type_id === element));
            }
          });
          this.currentDataSet = {
            is_user_default: this.defaultColumnList.is_user_default,
            default_for_all_user: this.defaultColumnList.default_for_all_user,
            taxonomy_list_setting: this.defaultNodeTypelist
          };
        }

      }

    }).catch((ex) => {

    });
  }

  openConfigurationPage() {
    this.configureTab = 'Metadata Fields';
    this.toggleColumnView(this.configureTab);
  }

  loadTable(type, disableSave?: boolean, configTable = false) {
    if (type === 'node_type_table' && configTable) {
      setTimeout(() => {
        Utils.removeBodyScroll();
        this.nodeTypeViewHeightCalculation();
        if (this.nodeTypeTableView) {
          // this.nodeTypeTableView.resetData();
          this.nodeTypeTableView.configTable(this.taxonomyData);
        }
      }, 50);
    } else {
      setTimeout(() => {
        this.tabularViewHeightCalculation();
      }, 50);
      Utils.removeBodyScroll();
    }
    this.colHeaders = [];
    this.listOfColumn = [];
    if (!disableSave) {
      this.selectedTable = type;
    }
    this.tableViewList.forEach((table: any) => {
      if (table.code_name === type) {
        table.isActive = true;
      } else {
        table.isActive = false;
      }
    });

    if (type === 'supper_default_table' || (this.listOfTableData && this.listOfTableData.length === 1)) {
      this.setDefaultColumnList(type);
    } else {
      const currentTable = this.listOfTableData.find((view: any) => view.table_name.toLowerCase().trim() === type.toLowerCase().trim());
      if (currentTable) {
        this.currentDataSet = {
          is_user_default: currentTable.is_user_default,
          default_for_all_user: currentTable.default_for_all_user,
          taxonomy_list_setting: currentTable.table_config
        };
      } else {
        this.setDefaultColumnList(type);
      }
    }
    this.constructData(this.currentDataSet.taxonomy_list_setting, type.replace('_table', ''));
    this.listOfColumn = JSON.parse(JSON.stringify(this.currentDataSet.taxonomy_list_setting));
    if (type !== 'node_type_table') {
      this.listOfColumn.push({
        name: '',
        display_name: '',
        propName: 'compStatus',
        internal_name: 'compStatus',
        class: 'col-md-1 col-lg-1 col-xl-1',
      }, {
        name: '',
        display_name: '',
        propName: 'edit',
        internal_name: 'edit',
        class: 'col-md-1 text-right',
      });
    }

    this.listOfColumn.forEach((colData: any) => {

      if (colData.internal_name !== 'node_type' || colData.internal_name !== 'compStatus' || colData.internal_name !== 'edit') {
        this.colHeaders.push(colData.internal_name);
      }

    });
    this.enableButton = this.currentDataSet.is_user_default || this.currentDataSet.default_for_all_user;
  }

  setDefaultColumnList(type) {
    this.currentDataSet = {
      is_user_default: 1,
      default_for_all_user: 0,
      taxonomy_list_setting: type === 'node_type_table' ? this.defaultNodeTypelist : this.defaultmetadataArray
    };

  }

  constructData(array, type) {
    if (type === 'metadata' || type === 'supper_default') {
      array.forEach(data => {
        data.name = data.display_name;
        data.internal_name = data.internal_name === 'compStatus' ? 'compStatus' : data.internal_name.toLowerCase().replace(/ /gi, '_');
        data.class = 'col';
        data.propName = data.internal_name;
        if (data.internal_name !== 'full_statement') {
          data.canRemove = true;
        } else {
          data.canRemove = false;
        }
      });

    } else {
      array.forEach(element => {
        element.selectedMetadataList = [];
        element.canRemove = true;
        element.metadata.forEach(item => {
          if (!element.metadataSelected && (item.internal_name === 'full_statement' || item.internal_name === 'human_coding_scheme')) {
            item.is_selected = 1;
            item.selectDisplay = true;
          } else {
            item.is_selected = 0;
          }
          if (item.selectDisplay) {
            item.propName = 'display_name';
            item.value = item.display_name;
            element.selectedMetadataList.push(item);
          }
        });
      });
    }

  }

  toggleColumnView(tab) {
    this.configureTab = tab;

    switch (tab) {
      case 'Metadata Fields':
        this.dropdownData = {
          uniqueId: 'metadata_id',
          propName: 'display_name'
        };
        this.loadTable(this.tableViewList[1].code_name, true);
        this.loadColumnList('metadata');
        break;
      case 'Node Type':
        this.dropdownData = {
          uniqueId: 'node_type_id',
          propName: 'display_name'
        };
        this.loadTable(this.tableViewList[2].code_name, true);
        this.loadColumnList('nodeType');
        break;
      default:
    }
    this.defaultColumnList = {
      is_user_default: 1,
      default_for_all_user: 0,
      taxonomy_list_setting: tab === 'Metadata Fields' ? this.defaultmetadataArray.slice(0) : this.defaultNodeTypelist.slice(0)
    };
  }

  saveChanges() {
    this.saveTable = true;
    this.configTableRef.saveChanges();
  }
  updateListView(e) {
    this.setTableViewDataList(e);
  }

  setTableViewDataList(changedData: any) {
    const url = GlobalSettings.TABLE_CONFIG;
    const body = {
      project_id: this.projectId,
      setting_id: 4,
      feature_id: 4,
      table_name: this.configureTab === 'Metadata Fields' ? 'metadata_table' : 'node_type_table',
      json_data: {
        version: 1,
        feature_id: 4,
        setting_id: 4,
        last_table_view: this.configureTab === 'Metadata Fields' ? 'metadata_table' : 'node_type_table',
        project_table_views:
          [{
            table_name: this.configureTab === 'Metadata Fields' ? 'metadata_table' : 'node_type_table',
            is_user_default: changedData.userDefault,
            default_for_all_user: changedData.allUserDefault,
            view_type: this.configureTab === 'Metadata Fields' ? 'metadata' : 'node_type',
            table_config: changedData.columnList
          }]

      }
    };
    this.service.putService(url, body, 0, true).then((res: any) => {
      if (res.success) {
        this.sharedService.sucessEvent.next({
          type: 'success',
          customMsg: res.message
        });
        this.clearFields();
        this.getDataList();
        this.isLoading = true;
        this.getProjectAssociatedNodes();
      } else {
        this.sharedService.sucessEvent.next({
          type: 'error',
          customMsg: res.message
        });
      }
    }).catch((ex) => {
      console.log(ex);
    });
  }

  loadColumnList(type) {
    if (type === 'metadata') {
      this.additionalColumnList = this.additionalMetadata;
    } else {
      this.additionalColumnList = this.additionalNodes;
    }

  }

  clearFields() {
    this.configTableRef.onCancel();
  }

  cancelChanges() {
    this.loadTable(this.selectedTable, true);
    this.clearFields();
  }

  checkValidity(tableName) {
    if (this.listOfTableData && this.listOfTableData.find((table: any) => table.table_name === tableName.code_name)) {
      return true;
    } else {
      return false;
    }
  }

  checkSaveValidation(val) {
    this.enableButton = val;
  }

  setDefaultWidth() {
    if (document.getElementById('tablePanel')) {
      const wholeTableWidth = document.getElementById('tablePanel').offsetWidth;
      const lastColWidth = document.getElementById('tablePanel').lastChild['offsetWidth'];
      const actualTableWidth = wholeTableWidth - lastColWidth;
      const indivColWidth = actualTableWidth / this.defaultmetadataArray.length;
      this.defaultmetadataArray.forEach(col => {
        col.width = indivColWidth;
      });
    }

  }


  // search functionality STARTS
  captureSearchEvent(event) {
    this.isFilterApplied = event.isFilterApplied;
    this.searchResultList = (event.filterData);
    this.taxonomyData = event.treeData;
    if (this.isFilterApplied) {
      this.isReset = false;
    }
    if (!this.isFilterApplied && !this.isReset) {
      this.isReset = true;
    }

    if (this.isReset) {
      this.resetTree();
    } else {
      if (this.tabularView) {
        this.tabularView.setTableData(this.taxonomyData);
      }
    }

  }

  extractFilterCriteriaList() {
    this.filterCriteriaList = [];
    const index = this.listOfTableData.findIndex(table => table.table_name === 'metadata_table');
    if (index > -1) {
      const filterArray = this.defaultmetadataArray.concat(this.listOfTableData[index].table_config);
      filterArray.forEach(item => {
        if (!this.filterCriteriaList.find(metadata => metadata.display_name === item.display_name)) {
          this.filterCriteriaList.push(item);
        }
      });

    } else {
      this.filterCriteriaList = JSON.parse(JSON.stringify(this.defaultmetadataArray));
    }
  }

  setLoaderEvent(val) {
    this.isFirstSearchInitialise = val['isFirstTime'];
    // if (!this.initialiseSearch) {
    this.isSearching = val['isLoading'];
    // }
    this.initialiseSearch = false;
  }

  callExpandTreeFromWorker(node) {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker('../../treeService.worker', { type: 'module' });
    }
    if (this.worker) {
      this.worker.onmessage = ({ data }) => {
        this.taxonomyData = [];
        // console.log('data received from worker in project', data);
        if (data) {
          this.taxonomyData.push(data.taxonomyData);
        }
      };
      this.worker.postMessage({
        data: this.taxonomyData,
        location: 'utils',
        parameters: {
          selectedNode: node,
          isMultipleCall: true
        }
      });
    }
  }

  nodeTypeViewHeightCalculation() {
    if (this.nodeTypeTableView) {
      this.nodeTypeTableView.nodeTypeViewHeightCalculation();
    }
  }

  tabularViewHeightCalculation() {
    if (this.tabularView) {
      this.tabularView.tabularViewHeightCalculation();
    }
  }

  exportTable() {
    this.nodeTypeTableView.export();
  }

  // FUNCTION to reset tree data after search reset

  resetTree() {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker('../../treeService.worker', { type: 'module' });
    }
    if (this.worker) {
      this.worker.onmessage = ({ data }) => {
        this.taxonomyData = null;
        if (data) {
          this.taxonomyData = data.taxonomyData;
          // console.log('data received from worker in project', this.treeNodes);
          if (this.tabularView) {
            this.tabularView.setTableData(this.taxonomyData);
          }
        }
      };
      this.worker.postMessage({
        data: this.taxonomyData,
        location: 'utils',
        parameters: {
          level: 1,
          reset: true
        }
      });
    }
  }


}
