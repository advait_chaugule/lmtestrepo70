import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRequestModalComponent } from './project-request-modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('ProjectRequestModalComponent', () => {
  let component: ProjectRequestModalComponent;
  let fixture: ComponentFixture<ProjectRequestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectRequestModalComponent],
      imports: [HttpClientModule, RouterTestingModule, FormsModule, ReactiveFormsModule],
      providers: [SharedService, CommonService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectRequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
