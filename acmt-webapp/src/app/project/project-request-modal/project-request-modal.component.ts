import {
  Component,
  OnInit,
  Input,
  OnChanges
} from '@angular/core';
import {
  GlobalSettings
} from '../../global.settings';
import {
  CommonService
} from '../../common.service';
import {
  SharedService
} from '../../shared.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-project-request-modal',
  templateUrl: './project-request-modal.component.html'
})
export class ProjectRequestModalComponent implements OnInit, OnChanges {

  @Input() projects: any[] = [];
  projectList = [];
  roleList: any;
  selectedProject = null;
  selectedRoles: any[] = [];
  selectedRole = null;
  requestDisabled = true;
  form: FormGroup;
  constructor(private service: CommonService, private sharedService: SharedService) { }

  ngOnChanges() {
    this.projectList = [];
    this.requestDisabled = true;
    // this.projectList.push(this.getDefaultProject());
    // this.setDefaultProject();
    if (this.projects) {
      this.projects.forEach(element => {
        this.projectList.push(element);
      });
    }
  }
  ngOnInit() {
    this.form = new FormGroup({
      project: new FormControl(null, Validators.required),
      permission: new FormControl(null, Validators.required)
    });
    // this.setDefaultRole();
  }

  getRoles() {
    this.setDefaultRole();
    const url = GlobalSettings.GET_All_WORKFLOW_ROLES + '/' + this.selectedProject.workflow_id;
    this.service.getServiceData(url).then((res: any) => {
      res.forEach(element => {
        this.roleList.push(element);
      });
    }).catch((ex) => {
      console.log('getRole ', ex);
    });
  }

  onProjectSelected() {

    this.requestDisabled = true;
    if (this.selectedProject && this.selectedProject.workflow_id) {
      this.getRoles();
    } else {
      this.setDefaultRole();
    }
  }

  onRoleSelected() {
    if (this.selectedRoles && this.selectedRole && this.selectedRoles.indexOf(this.selectedRole) === -1
      && this.selectedRole.workflow_stage_role_id) {
      this.selectedRoles.push(this.selectedRole);
    }
    this.requestDisabled = this.selectedRoles.length === 0 ? true : false;
  }

  onDelete(index) {
    this.selectedRoles.splice(index, 1);
    this.requestDisabled = this.selectedRoles.length === 0 ? true : false;
    if (this.selectedRoles.length === 0) {
      this.selectedRole = null;
    }
  }
  onCancel() {
    if (this.form) {
      this.form.reset();
    }
    this.setDefaultRole();
    this.setDefaultProject();
    this.requestDisabled = true;
  }

  onRequestAccess() {

    const roles = [];
    this.selectedRoles.forEach(element => {
      roles.push(element.workflow_stage_role_id);
    });
    const url = GlobalSettings.PROJECT_ACCESS_REQUEST,
      body = {
        project_id: this.selectedProject.project_id,
        workflow_stage_role_ids: roles.join(',')
      };
    this.service.postService(url, body).then((res) => {
      console.log('res ', res);
      this.requestDisabled = true;
      // this.setDefaultRole();
      // this.setDefaultProject();
      this.onCancel();
      this.sharedService.sucessEvent.next({
        type: 'project_access_request'
      });

    }).catch((ex) => {
      console.log('Ex ', ex);
    });

  }

  /*getDefaultProject() {

    return {
      project_name: 'Select Project',
      project_id: undefined,
      workflow_id: undefined
    };
  }

  getDefaultRole() {

    return {
      role_name: 'Select Role',
      workflow_stage_role_id: undefined
    };

  }*/

  setDefaultRole() {
    this.roleList = [];
    // this.roleList.push(this.getDefaultRole());
    // this.selectedRole = this.roleList[0];
    this.selectedRoles = [];
    this.selectedRole = null;
  }

  setDefaultProject() {
    /*if (this.projectList && this.projectList.length > 0) {
      this.selectedProject = this.projectList[0];
    }*/
    this.selectedProject = null;
  }

}
