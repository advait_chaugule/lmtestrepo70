import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  ProjectComplianceReportComponent
} from './project-compliance-report.component';
import {
  CommonService
} from 'src/app/common.service';
import {
  ConfirmationDialogService
} from 'src/app/confirmation-dialog/confirmation-dialog.service';
import {
  HttpClientModule
} from '@angular/common/http';
import {
  RouterTestingModule
} from '@angular/router/testing';
import {
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  SharedService
} from 'src/app/shared.service';
import {
  MockCommonService
} from 'src/app/administration/mock.common.service';

describe('ProjectComplianceReportComponent', () => {
  let component: ProjectComplianceReportComponent;
  let fixture: ComponentFixture < ProjectComplianceReportComponent > ;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [ProjectComplianceReportComponent],
        imports: [HttpClientModule, RouterTestingModule],
        providers: [{
          provide: CommonService,
          useClass: MockCommonService
        }, ConfirmationDialogService, SharedService],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectComplianceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should load generate report UI', () => {
    component.projectId = 'projectId';
    component.getComplianceReport();
    fixture.detectChanges();

  });
});
