import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GlobalSettings } from '../../global.settings';
import { CommonService } from '../../common.service';

const caseCompliance = 'CASE Compliance';
const coverage = 'Coverage/Completion';

@Component({
  selector: 'app-project-compliance-report',
  templateUrl: './project-compliance-report.component.html',
  styleUrls: ['./project-compliance-report.component.scss']
})
export class ProjectComplianceReportComponent implements OnInit, OnChanges {
  caseCompliance: string = caseCompliance;
  coverage: string = coverage;
  caseComplianceData: any = {
    filledNodes: 0,
    totalNodes: 0,
    progressPercentage: 0,
    fields: [],
    nodes: []
  };
  coverageComplianceData: any = {
    filledNodes: 0,
    totalNodes: 0,
    progressPercentage: 0,
    fields: [],
    nodes: []
  };
  reportFound: boolean;

  @Input() projectId;

  constructor(private service: CommonService) { }

  ngOnInit() {
    this.reportFound = false;
  }

  ngOnChanges() {
    this.getComplianceReport();
  }

  /**
   * To get compliance report for project
   */
  getComplianceReport() {
    if (this.projectId) {
      const url = GlobalSettings.COMPLIANCE_REPORTS + '/' + this.projectId + '/2';
      this.service.getServiceData(url).then((res) => {
        if (res === 'No Compliance Report Found') {
          this.reportFound = false;
        } else {
          this.reportFound = true;
          this.setComplianceData('case-compliance', res);
          this.setComplianceData('coverage-compliance', res);
        }
        console.log('getComplianceReport ', res);
      }).catch((ex) => {
        console.log('getComplianceReport ', ex);
      });
    }
  }

  setComplianceData(complianceType: any, res: any) {
    const data: any = {
      filledNodes: 0,
      totalNodes: 0,
      progressPercentage: 0,
      fields: [],
      nodes: []
    };

    if (res) {
      data.filledNodes = (complianceType === 'case-compliance') ? res[complianceType]['case-filled-nodes'] :
        res[complianceType]['custom-filled-nodes'];
      data.totalNodes = (complianceType === 'case-compliance') ? res[complianceType]['case-total-nodes'] :
        res[complianceType]['custom-total-nodes'];
      data.progressPercentage = res[complianceType]['overall-percentage'];
      data.fields = [];
      res[complianceType]['fields'].forEach(element => {
        data.fields.push({ 'name': element.key, 'number': element.value });
      });
      data.nodes = [];
      res[complianceType]['nodes'].forEach(element => {
        data.nodes.push({ 'name': element.key, 'number': element.value });
      });
    }

    if (complianceType === 'case-compliance') {
      this.caseComplianceData = data;
    } else if (complianceType === 'coverage-compliance') {
      this.coverageComplianceData = data;
    }
  }

}
