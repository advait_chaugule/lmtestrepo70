import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTreeAccordianComponent } from './project-tree-accordian.component';
import { SharedService } from 'src/app/shared.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ProjectTreeAccordianComponent', () => {
  let component: ProjectTreeAccordianComponent;
  let fixture: ComponentFixture<ProjectTreeAccordianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectTreeAccordianComponent],
      providers: [SharedService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTreeAccordianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
