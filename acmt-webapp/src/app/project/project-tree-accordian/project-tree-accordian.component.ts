import {
  Component,
  OnInit,
  EventEmitter,
  OnChanges,
  OnDestroy,
  Input,
  Output,
  AfterViewChecked
} from '@angular/core';

import {
  Subscription
} from 'rxjs/Subscription';
import {
  SharedService
} from '../../shared.service';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import Utils from '../../utils';

import {
  GlobalSettings
} from '../../../../src/app/global.settings';
import {
  CommonService
} from '../../common.service';


@Component({
  selector: 'app-project-tree-accordian',
  templateUrl: './project-tree-accordian.component.html',
  styleUrls: ['./project-tree-accordian.component.scss']
})
export class ProjectTreeAccordianComponent implements OnInit, OnChanges, OnDestroy, AfterViewChecked {
  @Input() treeData: any;
  @Input() level: 0;
  @Output() selectedEvent: EventEmitter<any>;
  @Input() selectedNodeId = null;
  @Input() viewLocation = 'project_auth';
  @Input() treeType = 'treeview';
  @Input() isAllNode = true;
  @Input() isAddNode = false;
  @Input() isDeleteNode = false;
  @Input() levelLimit = -1; // this is for setting the tree level ristriction
  @Output() addNodeEvent: EventEmitter<any>;
  @Output() deleteNodeEvent: EventEmitter<any>;
  @Output() selectedNodeTop: EventEmitter<any>;
  @Output() human_coding_scheme = new EventEmitter();
  @Output() full_statement = new EventEmitter();
  @Output() taxonomy_parent_obj = new EventEmitter();
  @Output() document_title = new EventEmitter();
  @Output() metadataType = new EventEmitter();
  @Output() nodeTypeIdEvent = new EventEmitter();
  @Input() allNodeTypeList;
  @Input() nodetypeList = [];
  @Input() allCommented;
  @Input() showNoComment = false;
  charLimit = 220;
  keyPress = false;
  humancode;
  fullstatement;
  documenttitle;
  select_option_type: string;
  currentNode;
  // allNodeTemplateList = [];
  nodeSelectedEvent: Subscription;
  prevHumanCodeVal = 'undefined';
  prevFullStatementVal = 'undefined';
  prevTitleVal = '';
  addNodeFired = true;


  constructor(private sharedService: SharedService, private dialogService: ConfirmationDialogService) {
    this.selectedEvent = new EventEmitter<any>();
    this.addNodeEvent = new EventEmitter<any>();
    this.deleteNodeEvent = new EventEmitter<any>();
    this.selectedNodeTop = new EventEmitter<any>();
    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {

      if (item && item.location === this.viewLocation) {
        if (item && item.item_id) {
          this.selectedNodeId = item.item_id;
        }
        if (item && item.id) {
          this.selectedNodeId = item.id;
        }
        const self = this;
        // Execute only when new node added
        if (item.full_statement === 'undefined') {
          setTimeout(function () {

            const elementCollection = document.getElementsByClassName('activated');
            if (elementCollection && elementCollection.length > 0) {
              const rect = elementCollection[0].getBoundingClientRect();
              if (rect) {
                self.selectedNodeTop.emit(rect.top);
              }
            }
          }, 300);
        }
      }
    });

  }


  ngOnInit() {
    setTimeout(() => {

      this.treeData.forEach(element => {
        if (element.children && element.children.length > 0) {
          if (element.expand) {
            const elems: any = document.getElementsByClassName(element.id + '-atag');
            for (let i = 0; i < elems.length; i++) {
              if (elems[i].getAttribute('aria-expanded') === 'false') {
                elems[i].click();
              }
            }
          }
        }
      }, 1000);
    });
  }
  calculatePadding(level) {
    if (this.viewLocation === 'taxonomy_builder') {
      return Utils.calculateTaxonomyBuilderPadding(level, this.treeType);
    } else {
      return Utils.calculatePadding(level, this.treeType);
    }
  }

  ngOnChanges() {

  }
  ngAfterViewChecked() { }

  onNodeSelected(item) {
    if (item.id) {
      this.selectedNodeId = item.id;
    }
    if (item.item_id) {
      this.selectedNodeId = item.item_id;
    }
    item.location = this.viewLocation;
    // console.log('onNodeSelected tree-accordian ------------- ', item);
    // this.selectedEvent.emit(item);
    this.sharedService.treeNodeSelectedEvent.next(item);
  }

  expendNode(id) {
    // console.log('expendNode ', document.getElementById(id + '-atag'));
    const elems: any = document.getElementsByClassName(id + '-atag');
    /*if (document.getElementById(id + '-atag')) {
      if (document.getElementById(id + '-atag').getAttribute('aria-expanded') === 'false') {
        document.getElementById(id + '-atag').click();
      }
    }*/
    for (let i = 0; i < elems.length; i++) {
      if (elems[i].getAttribute('aria-expanded') === 'false') {
        elems[i].click();
      }
    }
  }

  focusNode(id) {
    if (document.getElementById(id + '-span')) {
      document.getElementById(id + '-span').focus();
    }
  }

  addNode(item) {
    item.location = this.viewLocation;
    item.level = this.level;
    console.log('addNode tree-accordian ------------- ', item);
    // this.selectedEvent.emit(item);
    // this.sharedService.treeAddNodeEvent.next(item);
    if (item.location === 'taxonomy_builder' && item.parent !== 'root') {
      if (item.full_statement.length > 0) {
        this.addNodeEvent.emit(item);
      }
    } else {
      this.addNodeEvent.emit(item);
    }
    this.keyPress = false;
  }
  deleteNode(item) {
    if (this.viewLocation !== 'taxonomy_builder') {
      this.dialogService.confirm('Confirm', 'Do you want to delete the node?')
        .then((confirmed) => {
          if (confirmed) {
            // console.log('deleteNode tree-accordian ', item);
            item.location = this.viewLocation;
            // this.sharedService.treeDeleteNodeEvent.next(item);
            this.deleteNodeEvent.emit(item);
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    } else {
      this.deleteNodeEvent.emit(item);
    }


  }

  addNodeEventFun(item) {

    this.addNodeEvent.emit(item);
  }

  deleteNodeEventFun(item) {

    this.deleteNodeEvent.emit(item);
  }

  blurHumanCodingScheme(item, evt) {
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    const human_coding_scheme = elt.innerText;
    // if (this.addNodeFired === false && this.prevHumanCodeVal !== human_coding_scheme.trim())
    if (this.prevHumanCodeVal !== human_coding_scheme.trim()) {
      this.prevHumanCodeVal = human_coding_scheme;
      const tempObj = [item, human_coding_scheme];
      this.human_coding_scheme.emit(tempObj);
    }
    this.addNodeFired = false;
  }

  blurFullStatement(item, evt) {
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    const full_statement = elt.innerText;
    if (this.prevFullStatementVal !== full_statement.trim()) {
      this.prevFullStatementVal = full_statement;
      const tempObj = [item, full_statement];
      this.full_statement.emit(tempObj);
    }
  }

  selectMetaData(item, evt, nodetypeid) {
    const metadataType = evt;
    const tempObj = [item, metadataType];

    this.metadataType.emit(tempObj);
    this.getNodeTypeId(item, nodetypeid);
  }

  getNodeTypeId(item, nodetypeid) {
    const node_type_id = nodetypeid;
    const tempObj = [item, node_type_id];
    this.nodeTypeIdEvent.emit(tempObj);
    // console.log('111', tempObj);
  }

  blurDocumentTitle(item, evt) {
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    const document_title = elt.innerText;
    if (this.prevTitleVal !== document_title.trim()) {
      this.prevTitleVal = document_title;
      const tempObj = [item, document_title];
      this.document_title.emit(tempObj);
    }
  }

  setHumanCodingSchemeEvent(val) {
    this.human_coding_scheme.emit(val);
  }

  setFullStatementEvent(val) {
    this.full_statement.emit(val);
  }

  setMetadataTypeEvent(val) {
    this.metadataType.emit(val);
  }

  setNodeTypeIdEvent(val) {
    this.nodeTypeIdEvent.emit(val);
  }

  setDocumentTitleEvent(val) {
    this.document_title.emit(val);
  }

  editNode(obj) {
    this.humancode = obj.human_coding_scheme;
    this.fullstatement = obj.full_statement;
    this.documenttitle = obj.document_title;
    // console.log('Edit Node Called' + JSON.stringify(obj));
  }

  updateParentNodeObj() {
    console.log('Update button clicked');
    const parentObj = {
      human_coding_scheme: this.humancode,
      full_statement: this.fullstatement,
      document_title: this.documenttitle
    };
    this.taxonomy_parent_obj.emit(parentObj);
  }

  ngOnDestroy() {
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
  }

  onFocus(item) {
    this.currentNode = item;
    this.prevHumanCodeVal = item['human_coding_scheme'];
  }

  onKeyPress(item, event, control) {
    const key = event.which || event.keyCode || event.charCode;
    const parentId = this.currentNode.parent_id;
    const itemId = this.currentNode.id;
    if (key === 13 && event.shiftKey) {
      event.preventDefault();
      if (this.level !== this.levelLimit) {
        if (this.keyPress === false) {
          this.keyPress = true;
          if (control === 'humanCodeScheme') {
            this.blurHumanCodingScheme(item, event);
          }
          if (control === 'fullStatement') {
            this.blurFullStatement(item, event);
          }
          if (control === 'document') {
            this.blurDocumentTitle(item, event);
          }
          this.addButtonEvent(itemId);
        }
      }
    } else if (key === 13) {
      event.preventDefault();
      if (this.keyPress === false) {
        this.keyPress = true;
        if (control === 'humanCodeScheme') {
          this.blurHumanCodingScheme(item, event);
        }
        if (control === 'fullStatement') {
          this.blurFullStatement(item, event);
        }
        if (control === 'document') {
          this.blurDocumentTitle(item, event);
        }
        this.addButtonEvent(parentId);
      }
    }
  }

  onKeyUp(event) {
    this.keyPress = false;
  }
  addButtonEvent(buttonId) {
    if (document.getElementById(buttonId + '-add')) {
      document.getElementById(buttonId + '-add').click();
    }
  }
}
