import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import {
  SharedService
} from '../../shared.service';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  HttpClient
} from '@angular/common/http';
import Utils from '../../utils';
import {
  ImportFileComponent
} from '../../common/import-file/import-file.component';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  isNullOrUndefined
} from 'util';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import { AutoCompleteComponent } from '../../common/auto-complete/auto-complete.component';
import { TreeViewComponent } from '../../tree-view/tree-view.component';
import { TreeComponent } from '../../common/tree/tree.component';

export class ItemNode {
  children: ItemNode[];
  text: string;
  value: string;
}

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss', './../../taxonomy/import-taxonomy/import-taxonomy.component.scss']

})

export class CreateProjectComponent implements OnInit, OnChanges, OnDestroy {

  projectName = '';
  projectDesc = '';
  workflowList = [];
  workflowData: any[] = [];
  taxonomyList = [];
  taxonomyData: any;
  selectedWorkFlow = null;
  tempSelectedWorkFlow = null;
  selectedTaxonomy = null;
  tempSelectedTaxonomy = null;
  form: FormGroup;
  wflow: FormControl;
  selectedDraftTaxonomy: FormControl;
  textLength = 50;
  selectedTaxonomyNodeIs = [];
  selectedParentNodeIds = [];
  nodes: any;
  relations: any;
  treeResponseData: any;
  selectedTaxonomyId = null;
  type = 'CreateProject';
  inputControlsDetails;
  showUploadingPanel = false;
  documentList: any;
  messages = [];
  currentProjectId: any;
  progressEventRef: Subscription;
  fileUploadEventRef: Subscription;
  progressPercentage: any;
  currentDocumentIndex;
  totalStep: number;
  taxonomyLoaded = true;
  selectType = 'single';
  uniqueId = 'document_id';
  autoCompleteTitle = 'Taxonomy';
  uniqueId_workflow = 'workflow_id';
  autoCompleteTitle_workflow = 'Workflow';
  validWorkflow = false;
  workflowPrevLoader = false;
  crtProjLoader = false; // holds loading state as boolean for project create
  docLoader = false; // holds loading state as boolean for project document upload
  selectedNodeIds = [];
  selectedPartialIds = [];

  @Input() currentMode = 'create';
  @Input() projectData = null;
  @Input() disabledButton = false;
  @Input() currentStep: number;
  @Output() projectCreatedEvent: EventEmitter<any>;
  @Output() documentCreatedEvent: EventEmitter<any>;
  @Output() closePopupEvent: EventEmitter<any>;

  @ViewChild('importFileComponent', { static: false }) importFileComponent: ImportFileComponent;
  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;
  @ViewChild('autoComplete_workflow', { static: false }) autoComplete_workflow: AutoCompleteComponent;
  @ViewChild('tree', { static: false }) tree: TreeComponent;

  constructor(
    private service: CommonService,
    private sharedService: SharedService,
    private treeService: TreeDataService,
    private dialogService: ConfirmationDialogService) {
    this.projectCreatedEvent = new EventEmitter<any>();
    this.documentCreatedEvent = new EventEmitter<any>();
    this.closePopupEvent = new EventEmitter<any>();

    this.service.getServiceData(GlobalSettings.INPUTCONTROLS_JSON_URL).then((data: any) => {
      if (data) {
        console.log('Type');
        for (const item of data) {
          if (this.type === item.type) {
            this.inputControlsDetails = item;
          }
        }
      } else {
        console.log('ProtipComponent data is null');
      }
    }).catch((ex) => {
      console.log('ProtipComponent ', ex);
    });

    /*this.progressEventRef = this.sharedService.progressEvent.subscribe((event: any) => {
      if (event) {
        this.progressPercentage = event.percentDone;
        if (this.documentList && this.documentList.length > 0) {
          this.documentList[this.currentDocumentIndex].uploadPercentage = this.progressPercentage;
        }
      }
    });*/
    this.fileUploadEventRef = this.fileUploadSubscription();
  }
  ngOnInit() {
    this.getWorkFlow();
    this.listTaxonomies();
    this.createProjectFormValidation();
    this.form = new FormGroup({
      project_name: new FormControl(null, [Validators.required]),
      project_workflow: new FormControl(),
      project_taxonomy: new FormControl(),
      project_desc: new FormControl(),
      temp_project_workflow: new FormControl(),

    });
    this.documentList = [];
    this.progressPercentage = 0;
    this.totalStep = 2;
  }

  ngOnChanges() {
    if (this.projectData) {
      this.projectName = this.projectData.project_name;
      this.projectDesc = this.projectData.description;
      if (this.projectData.workflow && this.projectData.workflow.title) {
        this.tempSelectedWorkFlow = this.projectData.workflow;
      } else {
        this.tempSelectedWorkFlow = {
          title: 'Default workflow'
        };
      }
      if (this.projectData.taxonomy_name) {
        this.tempSelectedTaxonomy = {
          title: this.projectData.taxonomy_name
        };
        if (this.form.controls.project_taxonomy) {
          this.form.controls.project_taxonomy.setValue(this.projectData.taxonomy_name);
        }
      }
      if (this.currentMode === 'update') {
        this.showUploadingPanel = true;
        this.currentProjectId = this.projectData.project_id;
        this.selectedTaxonomyId = this.projectData.document_id;
        this.callAssetListApi(this.projectData.project_id);
        this.getTaxonomyNodes();
      }
    }
  }

  ngOnDestroy() {
    if (this.progressEventRef) {
      this.progressEventRef.unsubscribe();
    }
    if (this.fileUploadEventRef) {
      this.fileUploadEventRef.unsubscribe();
    }
  }

  getWorkFlow() {
    const url = GlobalSettings.WORKFLOW_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.workflowList = []; // res.WorkflowList.workflows;
      // this.workflowList.push(this.getDefaultWorkFlow());
      if (res && res.WorkflowList && res.WorkflowList.workflows) {
        res.WorkflowList.workflows.forEach(element => {
          this.workflowList.push(element);
        });
      }
      if (this.workflowList.length > 0) {
        this.selectedWorkFlow = null;
      }
    }).catch(ex => {
      console.log('list of taxonomies ex ', ex);
    });
  }


  listTaxonomies() {
    if (localStorage.getItem('access_token')) {
      this.taxonomyList = [];
      // this.taxonomyList.push(this.getDefaultTaxonomy());
      const url = GlobalSettings.TAXONOMY_LIST + '?document_list_type=1&document_list_type=2';
      this.service.getServiceData(url).then((res: any) => {
        if (res) {
          res.forEach(element => {
            if (element.import_type !== undefined && element.import_type !== 1 && element.status !== 3) {
              this.taxonomyList.push(element);
            }
          });

        }
        if (this.taxonomyList.length > 0) {
          this.selectedTaxonomy = this.taxonomyList[0];
        }
        // this.taxonomyList = res;
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });
    }
  }

  onTaxonomySelected(taxonomy) {

    if (taxonomy) {
      this.taxonomyData = null;
      this.selectedTaxonomyNodeIs = [];
      this.selectedTaxonomy = taxonomy;
      if (this.selectedTaxonomy) {
        this.selectedTaxonomyId = this.selectedTaxonomy.document_id;
      }
      console.log('selectedTaxonomy  ', this.selectedTaxonomyId);
      if (this.selectedTaxonomyId) {
        this.taxonomyLoaded = false;
        const newUrl = GlobalSettings.GET_TREE_VIEW_ENHANCED + this.selectedTaxonomyId;
        this.treeService.getTreeData(newUrl, false, Utils.EXPAND_LEVEL).then((response: any) => {
          const res = response.parsedTreeNodes;
          if (res) {
            res.expand = true;
            // console.log('GET_TREE_VIEW ', res);
            if (res && res.children) {
              Utils.sortData(res.children);
            }
            this.taxonomyData = {
              children: [res]
            };
            this.iterate(this.taxonomyData, 0);
            this.taxonomyLoaded = true;
          }
        }).catch(ex => {
          console.log('list of taxonomies ex ', ex);
        });
      } else {
        console.log('onTaxonomySelected id is null');
      }
    } else {
      this.taxonomyData = null;
    }

  }

  // create project form validation

  createProjectFormValidation() {
    this.form = new FormGroup({
      project_name: new FormControl(null, Validators.required),
      wflow: new FormControl(),
      selectedDraftTaxonomy: new FormControl()
    });
  }
  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  createProject(event) {
    console.log(this.form.valid);
    if (this.form.valid) {
      console.log('form submitted');
      if (this.selectedTaxonomyNodeIs.length > 0) {
        this.callCreateProjectAPI();
      } else {
        console.log('Plese select at least on node');
      }
      if (event) {
        console.log(event);
        event.preventDefault();
        event.stopPropagation();
      }
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  onCancel() {
    this.formReset();
    this.documentList = [];
    this.currentProjectId = null;
    this.showUploadingPanel = false;
    // this.currentStep = 1;
    this.closePopupEvent.emit({});
    this.clearDropdown();
    this.sharedService.faqEvent.next({
      name: 'project_list'
    });
  }

  clearDropdown() {
    if (this.autoComplete_workflow) {
      this.autoComplete_workflow.clearSelection();
    }
    if (this.autoComplete) {
      this.autoComplete.clearSelection();
    }
  }

  formReset() {
    if (this.form) {
      this.form.reset();
    }
  }

  updateProject(event) {
    this.tree.emitSelectedNodes(); // new tree
    const url = GlobalSettings.CREATE_PROJECT;
    const tempObj = {
      project_name: this.projectName,
      workflow_id: this.projectData.workflow_id,
      description: this.projectDesc
    };
    event.preventDefault();
    event.stopPropagation();

    // Getting selected node from taxonomy tree for editing
    let i = 0;
    let foundDocumentId = false;
    this.selectedTaxonomyNodeIs.forEach(element => {
      if (element === this.selectedTaxonomyId) {
        this.selectedTaxonomyNodeIs.splice(i, 1);
        foundDocumentId = true;
      }
      i++;
    });
    // if user select the whole tree then pass the document (taxonomy id )as parent id
    if (foundDocumentId && this.selectedParentNodeIds.length === 0) {
      this.selectedParentNodeIds.push(this.selectedTaxonomyId);
    }

    this.service.putService(url + '/' + this.projectData.project_id, tempObj).then((res: any) => {
      // this.projectCreatedEvent.emit();
      const obj = {
        project_id: res.project_id,
        item_id: this.selectedTaxonomyNodeIs.join(','),
        document_id: this.selectedTaxonomyId,
        parent_id: this.selectedParentNodeIds.join(','),
        root_selected_status: foundDocumentId === true ? 1 : 0
      };
      // Nodes mapping with project
      this.callProjectTaxonomyMappingAPI(obj, this.currentMode);
      this.currentStep = 2;
    }).catch(ex => {
      console.log('update project', ex);
    });
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  callCreateProjectAPI() {
    this.tree.emitSelectedNodes(); // new tree
    const url = GlobalSettings.CREATE_PROJECT,
      body = {
        project_name: this.projectName,
        workflow_id: this.selectedWorkFlow.workflow_id,
        // workflow_id: this.selectedWorkFlow,
        description: this.projectDesc,
        project_type: 1
      };
    let i = 0;
    let foundDocumentId = false;
    this.selectedTaxonomyNodeIs.forEach(element => {
      if (element === this.selectedTaxonomyId) {
        this.selectedTaxonomyNodeIs.splice(i, 1);
        foundDocumentId = true;
      }
      i++;
    });
    // if user select the whole tree then pass the document (taxonomy id )as parent id
    if (foundDocumentId && this.selectedParentNodeIds.length === 0) {
      this.selectedParentNodeIds.push(this.selectedTaxonomyId);
    }
    /*let j = 0;
    this.selectedParentNodeIds.forEach(element => {
      if (element === this.selectedTaxonomyId) {
        this.selectedParentNodeIds.splice(j, 1);
      }
      j++;
    });*/
    this.crtProjLoader = true;
    this.service.postService(url, body).then((res: any) => {
      console.log('callCreateProjectAPI  create', this.selectedTaxonomyId);
      const obj = {
        project_id: res.project_id,
        item_id: this.selectedTaxonomyNodeIs.length ? this.selectedTaxonomyNodeIs.join(',') :
          foundDocumentId ? this.selectedTaxonomyId : '',
        document_id: this.selectedTaxonomyId,
        parent_id: this.selectedParentNodeIds.join(','),
        root_selected_status: foundDocumentId === true ? 1 : 0
      };
      // Nodes mapping with project
      this.callProjectTaxonomyMappingAPI(obj, this.currentMode);
      if (res.project_id) {
        this.clearDropdown();
        this.showUploadingPanel = true;
        this.currentProjectId = res.project_id;
        this.currentStep = 2;
      }
    }).catch((ex) => {
      console.log('callCreateProjectAPI ex ', ex);
    });
  }

  callProjectTaxonomyMappingAPI(obj: any, actionType: string) {
    const url = GlobalSettings.MAPPED_PROJECT_TAXONOMY;
    this.service.postService(url, obj).then((res: any) => {
      console.log('callProjectTaxonomyMappingAPI  ', res);
      this.projectCreatedEvent.emit(res);
      if (actionType === 'create') {
        // When project is creating
        this.sharedService.sucessEvent.next({
          type: 'create_project'
        });
      } else {
        // When project is updating
        this.sharedService.sucessEvent.next({
          type: 'update_project'
        });
      }
      this.crtProjLoader = false;
    }).catch((ex) => {
      console.log('callProjectTaxonomyMappingAPI ex ', ex);
    });
    // this.formReset();
  }

  onTaxonomyTreeNodeSelected(obj: any) {

    this.selectedTaxonomyNodeIs = obj.nodes;
    this.selectedParentNodeIds = obj.partialNodes;
    // console.log(' onTaxonomyTreeNodeSelected  ', this.selectedTaxonomyNodeIs);
  }

  iterate(current, depth) {
    if (current && current.children) {
      const children = current.children;
      if (current.id) {
        current['value'] = current.id + '::' + current.parent_id; // + '::' + current.parent_id // new tree
        current['checked'] = false;
        current['disabled'] = true;
        current['collapsed'] = depth < Utils.EXPAND_LEVEL ? false : true;
        if (this.currentMode === 'create') { // For create project
          if (current.project_enabled) {
            if (parseInt(current.project_enabled, 10) === 1) {
              current['disabled'] = true;
              // this is for partially selected node
              if (current.is_enabled === 0) {
                current['disabled'] = false;
              }
            } else {
              current['disabled'] = false;
            }
          } else {
            current['disabled'] = false;
          }
        } else if (this.currentMode === 'update') { // For edit project
          current['disabled'] = false;
          if (current.is_editable === 1 && current.project_enabled === '1') {
            if (current.id !== 'orphan_label_node_id' && current.id !== 'undefined') {
              this.selectedNodeIds.push(current.id);
            }
            if (current.parent_id !== 'orphan_label_node_id' && current.parent_id !== 'undefined') {
              this.selectedParentNodeIds.push(current.parent_id);
            }
          }
          if (current.is_editable) {
            if (current.is_editable === 1) {
              // If nodes are used in current project, keep nodes as disabled and checked
              current['checked'] = true;
              current['disabled'] = true;
              // this is for partially selected node
              if (current.is_editable === 0 || current.is_editable === 2) {
                current['checked'] = false;
              }
            } else {
              if (current.is_editable === 0 || current.is_editable === 2) {
                current['checked'] = false;
              }
            }
          } else {
            current['checked'] = false;
          }
          // If nodes are associated with any other projects, keep nodes as disabled
          if (current.project_enabled) {
            if (parseInt(current.project_enabled, 10) === 1) {
              current['disabled'] = true;
            }
          }
        }
      }
      if (current.title) {
        current['text'] = current.title.substr(0, this.textLength);
      }
      if (!current.title) {
        if (current.full_statement) {

          current['text'] = current.human_coding_scheme + ' ' + current.full_statement.substr(0, this.textLength);
        } else {
          current['text'] = current.human_coding_scheme;
        }
      }
      // console.log('------------ ', current['text']);

      for (let i = 0, len = children.length; i < len; i++) {
        this.iterate(children[i], depth + 1);
      }
    }
  }

  getDefaultWorkFlow() {
    return {
      name: 'Select Workflow',
      workflow_id: undefined
    };
  }

  getDefaultTaxonomy() {
    return {
      'document_id': undefined,
      'title': 'Select Taxonomy'
    };
  }

  clearAllData() {
    this.projectName = '';
    this.projectDesc = '';
    this.taxonomyData = null;
    if (this.workflowList && this.workflowList.length > 0) {
      this.selectedWorkFlow = null;
    }
    if (this.taxonomyList.length > 0) {
      this.selectedTaxonomy = this.taxonomyList[0];
    }
    this.selectedTaxonomyNodeIs = [];
  }
  resetSteps(mode) {
    this.currentStep = 1;
    if (mode === 'update') {
      setTimeout(() => {
        if (this.tree) {
          this.tree.updatedExistingIds(this.selectedNodeIds, this.selectedParentNodeIds); // new tree
        }
      }, 1000);
    }
  }

  onFileUploadEvent(data) {
    console.log(data);
    console.log('Project Id for selected file: ' + this.currentProjectId);
    let obj, isAllFileUploaded = 1; // 1 for true, 0 for false. Indicates all files uploaded or not except selected one
    const type = data.file.name;
    const document = {
      'asset': data.file,
      'asset_linked_type': '4', // 4 for project
      'title': data.file.name ? data.file.name : '',
      'size': data.file.size,
      'description': '',
      'type': type.split('.').pop(),
      'item_linked_id': this.currentProjectId, // Project id
      'uploadStatus': 0, // 0 for not uploaded and 1 for uploaded successfully
      'uploadPercentage': 0,
      'asset_id': null
    };
    this.documentList.push(document);
    const index = this.documentList.length - 1; // last file index
    obj = this.generateFormData(index);

    if (index === 0) { // upload first file
      this.callUploadFileAPI(index, obj);
    } else {
      for (let i = 0; i < this.documentList.length - 1; i++) {
        if (this.documentList[i].uploadStatus === 0) { // atleast one file is pending for upload
          isAllFileUploaded = 0;
          break;
        }
        if (i === this.documentList.length - 2) { // checking with previous one of lastly added
          if (isAllFileUploaded === 1) {
            this.callUploadFileAPI(index, obj); // calling api to upload current index document in list
          }
        }
      }
    }
  }

  // Subscription for upload document's api calling
  fileUploadSubscription(): Subscription {
    let obj;
    return this.sharedService.uploadFileIndexEvent.subscribe(
      data => {
        data = data + 1; // data = index
        if (this.documentList && this.documentList.length > 0 && (isNullOrUndefined(this.documentList[data]) === false)) {
          obj = this.generateFormData(data);
          this.callUploadFileAPI(data, obj); // calling api to upload
        }
      }, err => {
        console.error(err);
      });
  }

  /**
   * To generate form data for POST service
   * @param index (File index)
   */
  generateFormData(index): any {
    const fd = new FormData();
    fd.append('asset', this.documentList[index].asset);
    // fileDetails = this.documentList[index]['a;
    fd.append('title', this.documentList[index].title);
    fd.append('description', this.documentList[index].description);
    fd.append('asset_linked_type', this.documentList[index].asset_linked_type);
    fd.append('item_linked_id', this.documentList[index].item_linked_id);
    return fd;
  }

  /**
   * Api call function for uploading document
   * @param index (document index in list)
   * @param obj (form data of document to be uploaded)
   */
  callUploadFileAPI(index, obj) {
    this.docLoader = true;
    this.documentList[index].uploadStart = 1;
    this.currentDocumentIndex = index;
    this.service.uploadService(GlobalSettings.ASSET_ASSOCIATION, obj).then((res) => {
      this.sharedService.sucessEvent.next({
        type: 'add_assets'
      });
      this.documentList[index].uploadStatus = 1; // changing current file's status to uploded successfully
      this.sharedService.uploadFileIndexEvent.next(index);
      this.importFileComponent.setUploaedFile('');
      this.documentList[index]['asset_id'] = res['asset_id'];
      this.documentCreatedEvent.emit(res);
      console.log(res);
      this.docLoader = false;
    }).catch((ex) => {
      console.log('addDocument ex ', ex);
      this.docLoader = false;
    });
  }

  /* --------- Functionality to convert bytes to appropriate file size start --------- */
  formatBytes(bytes) {
    let fileSize = (Utils.formatBytes(bytes));
    let size = 0;
    let measure = '';
    if (fileSize.toLowerCase().includes('bytes')) {
      size = Math.round(Number(fileSize.substring(0, fileSize.length - 5)));
      measure = fileSize.substring(fileSize.length - 5, fileSize.length);
    } else {
      size = Math.round(Number(fileSize.substring(0, fileSize.length - 2)));
      measure = fileSize.substring(fileSize.length - 2, fileSize.length);
    }

    fileSize = size + ' ' + measure;
    // console.log(fileSize);
    return fileSize;
  }
  /* --------- Functionality to convert bytes to appropriate file size end --------- */


  /**
   * To delete document
   * @param asset_id
   */
  onFileDeleteEvent(asset_id) {
    this.dialogService.confirm('Confirm', 'Are you sure you want to delete this document?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.ASSET_ASSOCIATION + '/' + asset_id;
          if (asset_id && asset_id.length > 0) {
            this.service.deleteServiceData(url).then((res) => {
              this.documentList = this.documentList.filter(obj => {
                return obj.asset_id !== asset_id;
              });
              this.sharedService.sucessEvent.next({
                type: 'delete_document'
              });
            }).catch((ex) => {
              console.log('onFileDeleteEvent  ', ex);
            });
          }
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  /**
   * Api to list assets of a specific project
   * @param project_id
   */
  callAssetListApi(project_id) {
    const url = GlobalSettings.ASSET_ASSOCIATION + '/' + project_id;

    if (project_id && project_id.length > 0) {
      this.documentList = [];
      this.service.getServiceData(url).then((res: any) => {
        if (res) {
          res.forEach(element => {
            const document = {
              'title': element.title ? element.title : '',
              'description': element.description ? element.description : '',
              'type': element.title ? element.title.split('.').pop() : '',
              'item_linked_id': element.item_linked_id,
              'uploadStatus': 1, // 0 for not uploaded and 1 for uploaded successfully
              'uploadPercentage': 100,
              'size': element.asset_size,
              'asset_id': element.asset_id
            };
            this.documentList.push(document);
          });
        }
      }).catch(ex => {
        console.log('list of assets for project ex ', ex);
      });
    }
  }

  /**
   * On preview of selected workflow
   */
  onPreviewClick() {
    let workflowId;
    if (this.currentMode === 'update' && this.tempSelectedWorkFlow && this.tempSelectedWorkFlow.workflow_id) {
      workflowId = this.tempSelectedWorkFlow.workflow_id;
    } else if (this.currentMode === 'create' && this.selectedWorkFlow && this.selectedWorkFlow.workflow_id) {
      workflowId = this.selectedWorkFlow.workflow_id;
    }
    this.getWorkFlowById(workflowId);
  }

  /**
   * To get workflow data by id
   * @param workflowId
   */
  getWorkFlowById(workflowId) {
    if (workflowId) {
      this.workflowPrevLoader = true;
      const url = GlobalSettings.GET_WORKFLOW + '/' + workflowId;
      this.service.getServiceData(url).then((res: any) => {
        this.workflowData = res.workflow.stages;
        setTimeout(() => {
          focusOnModalCloseBtn();
        }, 500);
        this.workflowPrevLoader = false;
      }).catch((ex) => {
        this.workflowPrevLoader = false;
        this.onCancelPreviewWorkflow();
        console.log('Get workflow by id ex', ex);
      });
    }
  }

  onCancelPreviewWorkflow() {
    this.workflowData = [];
  }

  /**
   * To get taxonomy's mapped nodes of a project on edit project
   */
  getTaxonomyNodes() {
    this.selectedNodeIds = [];
    this.selectedPartialIds = [];
    this.taxonomyData = null;
    if (this.currentProjectId) {
      this.taxonomyLoaded = false;
      const url = GlobalSettings.GET_MAPPED_NODES_ALL + '/' + this.currentProjectId;
      this.treeService.getTreeData(url, false).then((response: any) => {
        const res = response.parsedTreeNodes;
        if (res) {
          res.expand = true;
          if (res && res.children) {
            Utils.sortData(res.children);
          }
          this.taxonomyData = { children: [res] };
          this.iterate(this.taxonomyData, 0);
          this.taxonomyLoaded = true;
        }
      }).catch(ex => {
        console.log('getTaxonomyNodes ex ', ex);
      });
    }
  }
  onClickedOutsideWorkflow(event, id) {
    this.autoComplete_workflow.openPanel(false);
  }
  onClickedOutside(event, id) {
    if (event.srcElement.id !== 'workflowRole') {
      this.autoComplete.openPanel(false);
    }
  }
  updateSelectedObject(event) {
    this.selectedWorkFlow = event;
  }

  checkValidation(type) {
    switch (type) {
      case 'workflow':
        if (this.selectedWorkFlow) {
          this.validWorkflow = true;
        } else {
          this.validWorkflow = false;
        }
        break;
      case 'taxonomy':
        if (this.selectedTaxonomy) {
          this.validWorkflow = true;
        } else {
          this.validWorkflow = false;
        }
    }
  }


}
