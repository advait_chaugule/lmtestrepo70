import {
  Component,
  OnInit,
  Input,
  OnChanges,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {
  GlobalSettings
} from '../../global.settings';
import {
  CommonService
} from '../../common.service';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import {
  SharedService
} from '../../shared.service';
/*import { WalktroughService } from '../../help/walkthrough/walktrough.service';
import { NgxBootstrapProductTourService } from 'ngx-bootstrap-product-tour';*/
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import Utils from '../../utils';
import {
  Subscription
} from 'rxjs/Subscription';
import { DataTableComponent } from '../../common/data-table/data-table.component';

@Component({
  selector: 'app-project-document',
  templateUrl: './project-document.component.html'
})
export class ProjectDocumentComponent implements OnInit, OnChanges, OnDestroy {

  @Input() projectId;
  @ViewChild('dataTable', { static: false }) dataTable: DataTableComponent;
  documentList: any;
  projectMode: any;
  selectedProject: any;
  listOfColumn = [{
    name: 'Document',
    propName: 'title',
    class: '',
    type: 'text'
  },
  {
    name: 'Update Date',
    propName: 'uploaded_at',
    class: '',
    width: '15%',
    type: 'text'
  },
  {
    name: '',
    propName: 'viewUrl',
    value: 'Preview',
    class: '',
    type: 'link',
    redirect: true
  }
  ];
  optionList = [];
  preventEdit = false;
  editPermission = false;
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring

  constructor(
    private service: CommonService,
    private dialogService: ConfirmationDialogService,
    private sharedService: SharedService,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService,*/
    private AuthenticateUser: AuthenticateUserService) {
    this.sharedService.preventEdit.subscribe((event: any) => {
      if (event.preventEdit !== undefined) {
        this.preventEdit = event.preventEdit;
      }
    });
    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.selectedStageId = event.currentWorkflowStage;
          if (this.projectId) {
            this.checkProjectEditPermission(this.projectId, this.selectedStageId);
          }
        }, 1000);
      }
    });
  }

  ngOnInit() {
    this.documentList = [];
    // ----------------for Common Data table----------------- //
    this.optionList = [{
      name: 'Download',
      type: 'downloadDocument',
      value: true,
      propName: 'downloadUrl',
      url: true,
      check: 'isDownload'
    }];

    if (!this.preventEdit) {
      this.optionList.push({
        name: 'Delete',
        type: 'deleteDocument',
        value: true,
        modal: '#accept-request',
        url: false,
        check: 'isDelete'
      });
    }
    this.getListOfDocument(this.projectId);
    /*this.tourService.end();
    this.tourService.initialize(this.walkService.getWalkthroughData('project_document'));*/
  }

  ngOnChanges() {
    if (this.projectId) {
      this.editPermission = this.checkProjectEditPermission(this.projectId, this.selectedStageId);
    }
  }

  // On click Menu option

  onOptionClicked(event) {
    switch (event.clickedOn) {
      case 'deleteDocument':
        this.onDeleteDocument(event.data.asset_id);
        break;
      default:
    }
  }

  /**
   * To list documents
   * @param project_id
   */
  getListOfDocument(project_id) {
    const url = GlobalSettings.ASSET_ASSOCIATION + '/' + project_id;
    if (project_id && project_id.length > 0) {
      this.documentList = [];
      this.service.getServiceData(url).then((res: any) => {
        if (res) {
          res.forEach(element => {
            const document = {
              'title': element.title ? element.title : '',
              'description': element.description ? element.description : '',
              'uploaded_at': element.uploaded_at,
              'asset_id': element.asset_id,
              'viewUrl': GlobalSettings.BASE_URL + GlobalSettings.ASSET_ASSOCIATION + '/preview/' + element.asset_target_file_name,
              'downloadUrl': GlobalSettings.BASE_URL + GlobalSettings.ASSET_ASSOCIATION + '/download/' + element.asset_target_file_name,
              'isDelete': this.editPermission,
              'isDownload': true,
              'showOptions': true
            };
            this.documentList.push(document);
          });
          if (this.dataTable) {
            this.dataTable.setData(this.documentList);
          }
        }
      }).catch(ex => {
        console.log('list of assets for project ex ', ex);
      });
    }
  }

  /**
   * To delete document
   * @param asset_id
   */
  onDeleteDocument(asset_id) {
    this.dialogService.confirm('Confirm', 'Are you sure you want to delete this document?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.ASSET_ASSOCIATION + '/' + asset_id;
          if (asset_id && asset_id.length > 0) {
            this.service.deleteServiceData(url).then((res) => {
              this.documentList = this.documentList.filter(obj => {
                return obj.asset_id !== asset_id;
              });
              this.sharedService.sucessEvent.next({
                type: 'delete_document'
              });
            }).catch((ex) => {
              console.log('onDeleteDocument  ', ex);
            });
          }
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  editProjectPopUp() {
    this.projectMode = 'update';
    const url = GlobalSettings.GET_PROJECT_DETAIL + '/' + this.projectId;

    this.service.getServiceData(url).then((res) => {
      const tempData = res;
      this.selectedProject = JSON.parse(JSON.stringify(tempData));
    }).catch((ex) => {
      console.log('onFetchProject  ', ex);
    });

    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 800);
  }

  onDocumentCreateEvent(event) {
    this.getListOfDocument(this.projectId);
  }

  onPopupCloseEvent() {
    this.getListOfDocument(this.projectId);
  }

  checkProjectEditPermission(project_id, selectedStageId) {
    let isEditable = this.AuthenticateUser.AuthenticProjectPermissions('Edit Project');
    const editProject = Utils.checkProjectPermission(project_id, 'Edit Project', 'project_permissions', selectedStageId);
    if (editProject.hasProject) {
      isEditable = editProject.valid;
    }
    return isEditable;
  }

  ngOnDestroy() {
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
  }

}
