import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDocumentComponent } from './project-document.component';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('ProjectDocumentComponent', () => {
  let component: ProjectDocumentComponent;
  let fixture: ComponentFixture<ProjectDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectDocumentComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, SharedService, AuthenticateUserService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
