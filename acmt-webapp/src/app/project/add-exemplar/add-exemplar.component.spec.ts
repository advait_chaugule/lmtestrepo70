import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddExemplarComponent } from './add-exemplar.component';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('AddExemplarComponent', () => {
  let component: AddExemplarComponent;
  let fixture: ComponentFixture<AddExemplarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddExemplarComponent],
      imports: [FormsModule, HttpClientModule, ReactiveFormsModule, RouterTestingModule],
      providers: [CommonService, SharedService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddExemplarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
