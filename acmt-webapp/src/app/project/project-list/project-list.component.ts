import {
  Component,
  OnInit,
  ViewChild,
  Input,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  SharedService
} from '../../shared.service';
import {
  AuthenticateUserService
} from '../../../../src/app/authenticateuser.service';
import {
  CreateProjectComponent
} from '../../project/create-project/create-project.component';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../help/walkthrough/walktrough.service';*/
import Utils from '../../utils';
import {
  SortListComponent
} from './../../common/sort-list/sort-list.component';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  format
} from 'date-fns';
import {
  environment
} from '../../../environments/environment';
import { TableConfigurationComponent } from 'src/app/common/table-configuration/table-configuration.component';
@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, AfterViewInit, OnDestroy {
  SORT_BY_LABEL = 'Sort by';

  createProjectButton: true;
  deleteProjectButton: true;
  editProjectButton: true;
  viewProjectButton: true;
  projectMode = 'create';
  selectedProject = null;
  searchText: boolean;
  type = 'projects';
  showProtip = false;
  // selectedSortOption = '--Select--';
  searchResult = [];
  searchTrigger = false;
  searchResultEvent: Subscription;
  isFirstTime = true;
  startIndex = 0;
  lastIndex = 10;
  minItemPerPage;
  viewType = '';
  filterType = '';
  taxonomyTypeList = []; // holds list of taxonomy type
  // ----------------for Common Data table----------------- //
  listOfColumn = [];
  optionList = [];

  sortOptions = [{
    type: 'project_name',
    isDate: false,
    isAsce: true,
    display: 'Name (A-Z)'
  }, {
    type: 'project_name',
    isDate: false,
    isAsce: false,
    display: 'Name (Z-A)'
  }, {
    type: 'created_at',
    isDate: true,
    isAsce: true,
    display: 'Active Since (Asc)'
  }, {
    type: 'created_at',
    isDate: true,
    isAsce: false,
    display: 'Active Since (Desc)'
  }];
  defaultFilterOptions = [];
  defaultSortByColumn: any;
  pageDetail: any;

  @ViewChild('createProject', {
    static: false
  }) createProject: CreateProjectComponent;
  @ViewChild('creatProjectButton', {
    static: false
  }) creatProjectButton;
  @ViewChild('sortListRef', {
    static: false
  }) sortListRef: SortListComponent;
  @ViewChild('configTableRef', { static: false }) configTableRef: TableConfigurationComponent;


  projects;
  newlyCreatedProjectId = null;
  filterMode = 'AND';
  position;
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  searchingText;
  filterData = [];
  filterDataEvent: Subscription;

  constructor(
    private AuthenticateUser: AuthenticateUserService,
    private service: CommonService,
    private router: Router,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    private acivatedRoute: ActivatedRoute
    /*,
        private tourService: NgxBootstrapProductTourService,
        private walkService: WalktroughService*/
  ) {
    this.renamePacingGuide = environment.renamePacingGuide;
    this.minItemPerPage = Utils.PAGINATION_LOWER_LIMIT;
    this.searchResultEvent = this.sharedService.searchResultEvent.subscribe((item: any) => {
      if (this.isFirstTime) {
        this.isFirstTime = false;
        return;
      }
      if (!this.isFirstTime) {
        if (item) {
          this.searchResult = item['searchData'];
          if (item['showSearchList']) {
            this.searchTrigger = true;
          } else {
            this.searchTrigger = false;
          }
        }
      }
    });

    this.acivatedRoute.params.subscribe((param: any) => {
      if (param.type) {
        if (param.type === 'projects_created_by_me') {
          this.filterType = 'created_by_me';
        }
        if (param.type === 'projects_assigned_to_me') {
          this.filterType = 'assigned_by_me';
        }
        if (param.type === 'projects_related_to_me') {
          this.filterType = 'related_to_me';
        }
      }
    });

    this.sharedService.searchTextEvent.subscribe((res: any) => {
      this.searchingText = res.text;
    });

    this.filterDataEvent = this.sharedService.filterDataTable.subscribe((res: any) => {
      this.filterData = res.data;
    });

  }

  ngOnInit() {
    this.searchingText = '';
    this.position = 3;
    this.getProjects();
    this.userRoleRight();
    /*this.tourService.end();
    this.tourService.initialize(this.walkService.getWalkthroughData('project_list'));*/
    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit',
      type: 'editProject',
      value: this.editProjectButton,
      modal: '#createProject',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'delProject',
      value: this.deleteProjectButton,
      modal: '',
      check: 'isDelete'
    }
    ];

    this.sharedService.faqEvent.next({
      name: 'project_list'
    });
    const firstTime = localStorage.getItem('project_list_first_time');
    if (!firstTime) {
      this.setListView(true);
      this.setDefaultPagination();
      this.defaultSortByColumn = this.listOfColumn[3];
      localStorage.setItem('project_list_first_time', '1');
    } else {
      this.setListView(false);
      this.setPagination();
      this.setFilterList();
      this.defaultSortByColumn = JSON.parse(localStorage.getItem('project_list_page_state')).lastSortBy;
    }
  }

  ngAfterViewInit() {
    // init_rippleEffect();
  }

  ngOnDestroy() {
    if (this.filterDataEvent) {
      this.filterDataEvent.unsubscribe();
    }
  }
  changeView(type) {
    console.log('View type', type);
    this.viewType = type;
    this.setPagination();
    if (this.viewType === 'cardview') {
      this.sortByCardView();
    }
  }
  getProjects() {
    // After fetching taxonomy type list, call project list api
    this.getDictionaryMetadata();
  }

  callProjectListApi() {
    this.searchText = true;
    const url = GlobalSettings.PROJECT_LIST + '?project_type=1';
    this.service.getServiceData(url).then((res: any) => {
      this.projects = [];
      res.projects.forEach(project => {

        // if (this.checkProjectCanBeViewed(project.project_id, project.workflow['current_stage_id'])) {
        const roles_arr = [];
        project.project_status = project.taxonomy_status === 3 ? 'Inactive' : project.project_status;
        // project.updated_at_new = moment().format('MMMM Do YYYY, h:mm:ss a');
        project.isEditable = project.taxonomy_status !== 5 && project.taxonomy_status !== 3 ?
          this.checkProjectCanBeEdited(project.project_id, project.workflow['current_stage_id']) : false;
        project.isDelete = project.taxonomy_status !== 5 && project.taxonomy_status !== 3 ?
          this.checkProjectCanBeDeleted(project.project_id, project.workflow['current_stage_id']) : false;
        project.showOptions = (!project.isEditable && !project.isDelete) ? false : true;
        // project.project_taxonomy_type = project.taxonomy_type === 1 ? 'Generic' :
        project.project_taxonomy_type = this.getTypeNameByValue(project.taxonomy_type);
        project.groupNames = [];
        if (project.groups && project.groups.length) { // taxonomy groups
          project.groups.forEach((grp: any) => {
            project.groupNames.push(grp.group_name);
          });
        }
        project.groupNames = project.groupNames.toString().replace(/,/gi, ', '); // replacing ',' with ', '
        project.groupNames = project.groupNames ? project.groupNames : '-'; // - means empty
        this.projects.push(project);
        if (project.access_right.length > 0) {
          project.access_right.forEach(r => {
            roles_arr.push(r.name);
          });
        } else {
          roles_arr.push('No Role');
        }
        project.access_roles = roles_arr.join(', ');
        // project.access_roles = 'Author, Admin, Debarati';
        // }
      });
      if (this.projects.length > 0) {
        this.searchText = false;
        this.showProtip = false;
      } else {
        this.searchText = true;
        this.showProtip = true;
      }
      this.userRoleRight();
      if (this.filterType && this.filterType.length > 0) {
        this.filterTableData(this.filterType);
      }
      if (this.sortListRef) {
        this.sortListRef.sortDefalut(this.sortOptions[3].display);
      }
    }).catch(ex => {
      console.log('list of taxonomies ex ', ex);
    });
  }

  onProjectSelected(project) {
    console.log('onProjectSelected  ', this.checkProjectCanBeClickable(project.project_id, project.workflow['current_stage_id']));
    if (this.checkProjectCanBeClickable(project.project_id, project.workflow['current_stage_id'])) {
      this.sharedService.setTitleEvent.next({
        'type': 'project',
        'title': project.project_name
      });
      /*const url = GlobalSettings.CG_PACKAGE + project.project_id;
      this.service.getServiceData(url).then((res: any) => {
        this.projects = res.projects;
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });*/
      // [routerLink]="['/app/projectauthoring',project.project_id]"
      // this.router.navigate(['./app/projectauthoring', project.project_id]);
      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_DETAIL;
      this.router.navigate([path, project.project_id]);
    } else {
      this.sharedService.sucessEvent.next({
        type: 'project_permission_denied'
      });
    }

  }

  onSearchTextChange(txt) {
    if (txt.length > 2) {
      const url = GlobalSettings.PROJECT_SEARCH + txt;
      this.service.getServiceData(url).then((res: any) => {
        this.projects = res.projects;
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });
    } else {
      this.getProjects();
    }
  }

  onProjectCreateEvent(event) {
    console.log('onProjectCreateEvent  ', event);
    if (event) {
      this.newlyCreatedProjectId = event.project_id;
      const intervalId = setInterval(() => {
        this.newlyCreatedProjectId = null;
        if (intervalId) {
          clearInterval(intervalId);
        }
      }, 3 * 1000);
    }

    this.getProjects();
  }

  // ----------------for Common Data table----------------- //

  onOptionClicked(event) {
    console.log(event);
    switch (event.clickedOn) {
      case 'editProject':
        this.editProjectPopUp(event.data);
        break;
      case 'delProject':
        this.deleteProject(event.data.project_id, event.data.project_name);
        break;
      default:
    }
  }
  deleteProject(projectId, projectName) {

    this.dialogService.confirm('Confirm', 'Do you want to delete project ' + projectName + '?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.PROJECT_LIST + '/' + projectId;
          this.service.deleteServiceData(url).then((res) => {
            this.sharedService.sucessEvent.next({
              type: 'delete_project'
            });
            this.getProjects();
          }).catch((ex) => {
            console.log('deleteProject  ', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }


  userRoleRight() {

    if (this.AuthenticateUser.AuthenticProjectPermissions('Create Project')) {
      this.createProjectButton = true;
    }

    if (this.AuthenticateUser.AuthenticProjectPermissions('Delete Project')) {
      this.deleteProjectButton = true;
    }

    if (this.AuthenticateUser.AuthenticProjectPermissions('Edit Project')) {
      this.editProjectButton = true;
    }
    if (this.AuthenticateUser.AuthenticProjectPermissions('View Associated Nodes')) {
      this.viewProjectButton = true;
    }
  }

  onCreateProject() {
    this.projectMode = 'create';
    this.selectedProject = null;
    if (this.createProject) {
      this.createProject.clearAllData();
      this.createProject.resetSteps('create');
    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 800);
    this.sharedService.faqEvent.next({
      name: 'create_project'
    });
  }

  editProjectPopUp(data) {
    this.projectMode = 'update';
    // this.selectedProject = data;
    const tempData = data;
    this.selectedProject = JSON.parse(JSON.stringify(tempData));
    if (this.createProject) {
      this.createProject.resetSteps('update');
    }

    // this.creatProjectButton.nativeElement.click();
    // this.createProject.projectData(data  );
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 800);
    this.sharedService.faqEvent.next({
      name: 'edit_project'
    });
  }

  //
  checkProjectCanBeViewed(project_id, selectedStageId) {

    let canView = this.AuthenticateUser.AuthenticProjectPermissions('View Project List');
    const view_project = Utils.checkProjectPermission(project_id, 'View Project List', 'project_permissions', selectedStageId);
    if (view_project.hasProject) {
      canView = view_project.valid;
    }
    return canView;

  }

  checkProjectCanBeEdited(project_id, selectedStageId) {

    let isEditable = this.AuthenticateUser.AuthenticProjectPermissions('Edit Project');
    const editProject = Utils.checkProjectPermission(project_id, 'Edit Project', 'project_permissions', selectedStageId);
    if (editProject.hasProject) {
      isEditable = editProject.valid;
    }
    return isEditable;
  }

  checkProjectCanBeDeleted(project_id, selectedStageId) {

    let isDeletable = this.AuthenticateUser.AuthenticProjectPermissions('Delete Project');
    const editProject = Utils.checkProjectPermission(project_id, 'Delete Project', 'project_permissions', selectedStageId);
    if (editProject.hasProject) {
      isDeletable = editProject.valid;
    }
    return isDeletable;
  }

  checkProjectCanBeClickable(project_id, selectedStageId) {
    let isClickable = this.AuthenticateUser.AuthenticProjectPermissions('View Associated Nodes');
    const editProject = Utils.checkProjectPermission(project_id, 'View Associated Nodes', 'project_permissions', selectedStageId);
    if (editProject.hasProject) {
      isClickable = editProject.valid;
    }
    return isClickable;
  }

  onClose() {
    this.getProjects();
  }
  /* --------- functionality to add class and remove class on focus and blur for individual card layout start ---------*/
  onFocus(event, index) {
    const element = document.getElementById('card-' + index);
    element.classList.add('tn-focus');

  }

  onBlur(event, index) {
    const element = document.getElementById('card-' + index);
    element.classList.remove('tn-focus');

  }
  /* --------- functionality to add class and remove class on focus and blur for individual card layout end ---------*/

  changeDateToLocalTimeZone(date) {
    return Utils.changeDateToLocalTimeZone(date);
  }

  showDataInRange(event) {
    this.startIndex = event.start - 1;
    this.lastIndex = event.last;

    if (this.viewType === 'cardview') {
      localStorage.setItem('project_list_page_state', JSON.stringify({ pageDetail: event }));
    }
  }

  filterTableData(type) {
    const myName = localStorage.getItem('first_name') + ' ' + localStorage.getItem('last_name');
    const roleArr = this.getAssignedRoles();
    this.defaultFilterOptions = [];
    this.filterMode = 'AND';

    if (this.projects.length) {

      switch (type) {
        case 'created_by_me':
          this.defaultFilterOptions = [{
            propName: 'label',
            value: myName
          }];
          break;
        case 'assigned_by_me':
          roleArr.forEach(role => {
            this.defaultFilterOptions.push({
              propName: 'label',
              value: role
            });
          });
          break;
        case 'related_to_me':
          this.filterMode = 'OR';
          this.defaultFilterOptions = [{
            propName: 'label',
            value: myName
          }];

          roleArr.forEach(role => {
            this.defaultFilterOptions.push({
              propName: 'label',
              value: role
            });
          });
      }
    }
  }



  getAssignedRoles() {
    const rolesArr = [];
    this.projects.forEach(element => {
      const role = element.access_roles.split(',');
      role.forEach(pr => {
        if (pr.trim() !== 'No Role' && (!rolesArr.find(prole => prole.trim() === pr.trim()))) {
          rolesArr.push(pr);
        }
      });
    });
    return rolesArr;
  }

  getChanges(event) {
    console.log('change events', event);
    localStorage.setItem('project_list_page_state', JSON.stringify(event));
  }
  setDefaultPagination() {
    this.pageDetail = {
      start: 0,
      last: Utils.PAGINATION_LOWER_LIMIT,
      pageIndex: 1,
      itemPerPage: Utils.PAGINATION_LOWER_LIMIT
    };
    this.startIndex = 0;
    this.lastIndex = Utils.PAGINATION_LOWER_LIMIT;
  }
  setPagination() {
    if (localStorage.getItem('project_list_page_state')) {
      const pageDetail = JSON.parse(localStorage.getItem('project_list_page_state')).pageDetail;
      this.pageDetail = pageDetail;
    }
  }

  setFilterList() {
    if (localStorage.getItem('project_list_page_state')) {

      const filterOptions = JSON.parse(localStorage.getItem('project_list_page_state')).filterList;
      this.defaultFilterOptions = [];
      for (const item of filterOptions) {
        this.defaultFilterOptions.push({
          propName: 'label',
          value: item.label
        });
      }
    } else {
      console.log('setFilter localstoarge project_list_page_state is null');
    }
  }

  setListView(isDefault: boolean) {
    const list = JSON.parse(localStorage.getItem('project_list'));
    if (isDefault || !list) {
      this.listOfColumn = [{
        name: 'Name',
        propName: 'project_name',
        class: '',
        width: '20%',
        type: 'link',
        redirect: true,
        loadPage: true
      },
      {
        name: 'Type',
        propName: 'project_taxonomy_type',
        class: '',
        type: 'text',
        canRemove: true,
        canFilter: true
      },
      {
        name: 'Workflow Status',
        propName: 'project_status',
        class: '',
        width: '15%',
        type: 'text',
        canRemove: true,
        canFilter: true
      },
      {
        name: 'Groups',
        propName: 'groupNames',
        class: '',
        type: 'text',
        canRemove: true,
        canFilter: true
      },
      {
        name: 'Last Change',
        propName: 'updated_at',
        class: '',
        width: '10%',
        type: 'date',
        dateType: 'amCalendar'
      },
      {
        name: 'Active since',
        propName: 'created_at',
        class: '',
        width: '10%',
        type: 'date',
        dateType: 'amCalendar'
      },
      {
        name: 'Draft Taxonomy',
        propName: 'taxonomy_name',
        class: '',
        width: '15%',
        type: 'text',
        canFilter: true
      },
      {
        name: 'Created By',
        propName: 'created_by',
        class: '',
        width: '12%',
        type: 'text',
        canFilter: true,
        canRemove: true
      },
      {
        name: 'Roles',
        propName: 'access_roles',
        class: '',
        type: 'text',
        canFilter: true
      }
      ];
    } else {
      this.listOfColumn = list;
    }
  }

  saveChanges() {
    this.configTableRef.saveChanges();
  }

  updateListView(list) {
    this.listOfColumn = list.columnList;
    localStorage.setItem('project_list', JSON.stringify(list.columnList));
  }

  // To fetch 'Dictionary' type metadata's field values list
  // After fetching taxonomy types, call project list api to set taxonomy type for each taxonomy
  getDictionaryMetadata() {
    if (localStorage.getItem('access_token')) {
      this.taxonomyTypeList = [];
      // For getting any metadata's field values with internal name, we have to send any random id as per backend logic
      const url = GlobalSettings.GET_METADATA_LIST + '/random_id' + '?internal_name=Taxonomy Type';
      this.service.getServiceData(url).then((res: any) => {
        if (res && res.length > 0) {
          this.taxonomyTypeList = res;
        }
        this.callProjectListApi();
      }).catch((ex) => {
        console.log('Error caught while fetching dictionary type metadata', ex);
      });
    }
  }

  // To get field type name of Taxonomy type against value
  getTypeNameByValue(value: any) {
    if (this.taxonomyTypeList.length) {
      const index = this.taxonomyTypeList.findIndex(element => {
        return element.key === value;
      });
      if (index > -1) { // If key value found
        return this.taxonomyTypeList[index].value;
      } else { // If not found
        return '';
      }
    } else {
      return '';
    }
  }
  sortByCardView() {
    this.defaultSortByColumn = JSON.parse(localStorage.getItem('project_list_page_state')).lastSortBy;
    this.sortOptions.forEach(data => {
      if (this.defaultSortByColumn.propName.trim() === data.type.trim()) {
        const index = this.sortOptions.indexOf(data);
        this.position = this.defaultSortByColumn.isAsc === true ? index - 1 : index;
      }
    });
  }
  sortByListView(event) {
    this.listOfColumn.forEach(element => {
      if (event.type === element.propName) {
        this.defaultSortByColumn.isAsc = event.isAsce;
        this.defaultSortByColumn.name = element.name;
      }
    });
  }

  onCloseModal() {
    this.sharedService.faqEvent.next({
      name: 'project_list'
    });

  }

  getColumnFilterObj() {
    const obj = {};
    obj['project_name'] = this.searchingText;
    obj['taxonomy_name'] = this.searchingText;
    return obj;
  }
}
