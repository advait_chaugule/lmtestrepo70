import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  AfterViewInit
} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  ActivatedRoute,
  Params,
  Router
} from '@angular/router';
import {
  GlobalSettings
} from '../../global.settings';
import {
  SharedService
} from '../../shared.service';
import {
  AssociationComponent
} from './association/association.component';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  ProjectMappedNodeService
} from '../../project-mapped-node.service';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';

import {
  ProjectTreeAccordianComponent
} from '../project-tree-accordian/project-tree-accordian.component';
import Utils from '../../utils';
import {
  NodeDetailsComponent
} from '../../project/node-details/node-details.component';
import {
  AdditionalMetadataComponent
} from '../../project/additional-metadata/additional-metadata.component';
import {
  from
} from 'rxjs';
import {
  ProjectNotesComponent
} from '../projects-notes/projects-notes.component';
import {
  BuildTaxonomyComponent
} from '../../taxonomy/build-taxonomy/build-taxonomy.component';
import {
  TreeComponent
} from '../../common/tree/tree.component';
import * as _ from 'lodash';
import {
  AcmtSearchComponent
} from '../../common/acmt-search-block/acmt-search/acmt-search.component';
import {
  SelectTaxonomyComponent
} from '../../common/select-taxonomy/select-taxonomy.component';
import {
  SplitComponent
} from 'angular-split';
import { AssociationSettingsComponent } from '../association-settings/association-settings.component';
import { TreeSearchComponent } from 'src/app/common/tree-search/tree-search.component';


@Component({
  selector: 'app-project-authoring',
  templateUrl: './project-authoring.component.html',
  styleUrls: ['./project-authoring.component.scss']
})
export class ProjectAuthoringComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  LEFT_MIN_SIZE = Utils.LEFT_PANEL_MIN_SIZE; // Left panel min size
  LEFT_MAX_SIZE = Utils.LEFT_PANEL_MAX_SIZE; // Left panel max size

  @Input() step = 1; // holds the step for create association modal
  selectedTaxonomy; // holds selected taxonomy from create association modal
  valid = false;
  invalidUrl = false;
  isAllNode = true;
  isFirstTime = true;
  isEditable = false;
  isAddAssociations = false;
  deleteAssociations = false;
  updateAssociations = false;
  createNodePermission = false;
  deleteNodePermission = false;
  editNodePermission = false;
  commentNotification = false;
  showAssocciation = false;
  viewCommentList = true;
  isAllFieldFilled = false;
  createAdditionalMetadataPermission = false;
  projectId;
  taxonomyData = null;
  taxonomyOriginalData = null;
  taxonomyList: any;
  associationTypes: any;
  itemAdditionalMetadata: any;
  itemAssociations: any = [];
  itemExemplars: any = [];
  itemAssets: any = [];
  itemLinkedNodes: any = [];
  selectedNodeDetail: any;
  human_coding: any;
  full_statement: any;
  list_enumeration: any;
  education_level: any;
  projectDetail: any;
  formData;
  selectItemID = 'Master';
  defaultNodeType = 'Default';
  rootNodeType = 'Document';
  selectNodeItemID: string;
  parentNode = null;
  intervalTimer = null;
  nodetypeData;
  selectNodeTypeId;
  selectedNodeType = {
    title: '',
    node_type_id: ''
  };
  nodeSelectedEvent: Subscription;
  projectTreeViewEvent: Subscription;
  acivatedRouteEvent: Subscription;
  selectedNode: any = {
    item_id: '',
    id: '',
    isFirst: true,
    is_document: 1
  };
  view = 'project-authoring';
  showExempler = false;
  option = ''; // store the option selected exempler or asset
  currentIndex;
  description = '';
  additionalMetadataObj = [];
  addedAdditionalMetadata = [];
  fetchedMetadata: any = {};
  action;
  showAdditionaMetaData = true;
  preventEdit = false;
  tempNode;
  lastNode;
  firstNode;
  firstItemNode: any;
  tempTaxonomyData;
  itemId;
  showCommentPopup = 'false';
  projectComments = null;
  assignedToMe = 'all';
  isactive: boolean;
  project_type: any;
  itemComplianceReports = [];
  canEditContainer = true;
  document_id = null;
  enableDND = false;
  dragDropData = null;
  oldData = null;
  savingData = false;
  // documentCreated = false;
  postAPIData = [];
  old_itemsData = [];
  updateTaxonomyEvent: Subscription;
  treeRearrenged = false;
  buildTaxonomyData = null; // taxonomy data for build taxonomy screen when updating taxonomy for project
  pacingGuideNodes = [];
  reOrder = false; // holds if reorder option is selected or not
  editorEnabled = false;
  reorderComplete = true; // holds if re order functionality is complete, used for pre loader
  urlParams: any;
  parentNodeIds = [];
  nodeLoading = false;
  tabItems = ['Search Taxonomy', 'Browse Taxonomy']; // contains all the tabs for the current view
  currentTab = this.tabItems[0].toLowerCase(); // selects the current tab as default view
  selectedNodeIds = []; // holds all selected node ids from create association modal
  originNodeId = ''; // holds origin node id on which association is getting added
  originNode = null; // holds origin node on which association is getting added
  selectedAssociationType = null; // holds selected association from drop down
  addedAssociationCount = 0; // holds count of assciations added on modal before closing
  copyCount = 0; // holds count of copies appended on modal before closing
  disableAssociation = true; // holds boolean whether to disable save button
  requiredFullHierarchyFlag = null; // holds requiredFullHierarchyFlag to identify if append or append with childs is selected
  loaderOnAssociation = false; // holds boolean to display loader on new add association modal
  associationLoaderData = ''; // holds text to display loader on new add association modal
  reLoadTree = false; // holds boolean to determine if to reload tree on close of association modal
  itemNodetypeList = []; // holds nodetype list for each node
  disableOptionList = []; // holds option list to disable
  @ViewChild('assocationModal', {
    static: false
  }) assocationModal: AssociationComponent;
  @ViewChild('treeAccordian', {
    static: false
  }) treeAccordianComponet: ProjectTreeAccordianComponent;
  @ViewChild('tree', {
    static: false
  }) tree: TreeComponent;
  @ViewChild('nodeDetailsComponent', {
    static: false
  }) nodeDetailsComponent: NodeDetailsComponent;
  @ViewChild('additionalMetadataComponent', {
    static: false
  }) additionalMetadataComponent: AdditionalMetadataComponent;
  @ViewChild('projectNotesComponentId', {
    static: false
  }) projectNotesComponent: ProjectNotesComponent;
  @ViewChild('taxonomyBuilder', {
    static: false
  }) taxonomyBuilder: BuildTaxonomyComponent;
  @ViewChild('acmtSearch', {
    static: false
  }) acmtSearch: AcmtSearchComponent;
  @ViewChild('selectTaxonomy', {
    static: false
  }) selectTaxonomy: SelectTaxonomyComponent;
  @ViewChild('split', {
    static: false
  }) splitElem: SplitComponent;
  @ViewChild('associationSettings', {
    static: false
  }) associationSettings: AssociationSettingsComponent;
  @ViewChild('treeSearch', {
    static: false
  }) treeSearch: TreeSearchComponent;
  isLoading = false;
  isCopy = false; // holds additional options to append or append with child nodes
  isCopyAssociation = false; // holds additional options to append or append with child nodes
  optionsToCopySaved = false; // holds falg save is clicked on modal for options to append or append with child nodes
  relations = []; // holds relation from taxonomy hierarchy
  nodeHierarchy = []; // holds node hierarchy to append
  selectedTaxonomyData = []; // holds parsed taxonomy selected from association modal
  selectedDocumentId = ''; // holds parsed taxonomy's document id selected from association modal
  // copyStateChange = false;
  saveButtonClickedForCopy = false; // holds boolean if save button is clicked
  displayText = ''; // display text to be sent and updated on save button click
  displayOption = ''; // display text to be sent and updated on save button click for append and append to child
  copy = false; // copy with exact match option to be sent and updated on save button click
  copyAssociation = false; // copy association option to be sent and updated on save button click
  textLength = Utils.textLength; // holds the length to be set for each node's text limit to display
  breadCrumbLength = Utils.breadCrumbLength; // holds the length to be set for each node's breadcrumb text limit to display
  associationModalClose = false; // holds boolean if association modal is in open or close state
  associationModalOpen = false; // holds boolean if association modal is in open state, help to load static filter
  eventSubscription: Subscription;
  windowResizeSubscription: Subscription;
  associatedNodes = [];
  combinedAssociationArr = [];
  rightPaneSub: Subscription;
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring
  openAssociationList = false;
  searchResultList = [];
  filterCriteriaList = [];
  arrayToSearch = [];
  searchText = '';
  isFilterApplied = false;
  sourceDocumentId = '';
  worker: any;
  isReset = false;
  isSearching = false;
  initialiseSearch = true;
  isFirstSearchInitialise = true;
  constructor(
    private service: CommonService,
    private acivatedRoute: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService,
    private treeDataService: TreeDataService,
    private mappedNodeService: ProjectMappedNodeService,
    private authenticateUser: AuthenticateUserService,
    private dialogService: ConfirmationDialogService
  ) {

    // Subscripbe to tree node selected event
    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      if (item && item.location === 'project_auth') {
        if (item.item_id || item.id) {
          this.selectedNode = item;
          if (item['is_editable']) {
            this.isEditable = item['is_editable'] === (0 || 2) ? false : true;
          } else {
            this.isEditable = item['item_type'] === 'container' ? true : false;
          }
          this.getSelectedTreeItemDetail(item.item_id ? item.item_id : item.id);
        }
      }
    });
    this.project_type = localStorage.getItem('project_type');
    this.filterCriteriaList = JSON.parse(localStorage.getItem('filterCriteriaList'));

    this.eventSubscription = this.sharedService.openTree.subscribe((events: any) => {
      if (events.addAssociation) {
        this.addAssociation(this.selectedNode);
      }
    });

    this.windowResizeSubscription = this.sharedService.windowResizeEvent.subscribe((item: any) => {
      if (item) {
        this.projectAuthHeightCalculation();
        this.resizeRightPanelWidth();
      }
    });
    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          if (event.currentWorkflowStage) {
            this.selectedStageId = event.currentWorkflowStage;
            this.userRoleRight();
          }
        }, 1000);
      }
    });
  }

  ngOnInit() {
    // Get the project id from url
    this.acivatedRouteEvent = this.acivatedRoute.params.subscribe((params: Params) => {
      this.urlParams = params;
      if (params['itemId'] !== undefined) {
        this.itemId = params['itemId'];
      } else {
        this.itemId = null;
      }
      this.projectAuthHeightCalculation();
      if (params['id'] !== undefined) {
        this.projectId = params['id'];
        localStorage.setItem('project_id', this.projectId);
        this.getProjectDetail();
        this.getProjectAssociatedNodes();
        this.userRoleRight();
        this.startLogProjectActivity();
        this.getAllMetadata();
      } else {
        const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_LIST;
        this.router.navigate([path]);
      }
    });

    this.acivatedRoute.queryParams.subscribe((params) => {
      if (params['showCommentPopup'] !== undefined) {
        this.showCommentPopup = params['showCommentPopup'];
      } else {
        this.showCommentPopup = 'false';
      }
    });

    // Subscribe event for  Complete & Assigned tab (defined in base component)
    this.projectTreeViewEvent = this.sharedService.projectTreeViewEvent.subscribe((type) => {
      if (this.isFirstTime === false) {
        this.taxonomyData = null;
        if (type === 'inactive') { // complete nodes
          this.isAllNode = true;
          this.reOrder = false;
          this.initializeSelectednode();
          this.getAllNodes();
        } else { // assigned nodes
          this.initializeSelectednode();
          this.isAllNode = false;
          this.getProjectAssociatedNodes();
        }
      } else {
        this.isFirstTime = false;
        this.isEditable = true;
      }
    });

    this.updateTaxonomyEvent = this.sharedService.updateTaxonomyEvent.subscribe((data: any) => {
      this.reOrder = false;
      if (data && data.update) {
        this.onReorderModalOpen();
      }
      if (data && data.reorder && !this.isAllNode) {
        this.reOrder = true;
      } else if (data && data.reorder && this.isAllNode) {
        this.sharedService.sucessEvent.next({
          type: 'reorder_error'
        });
      }
    });

    // Subscripbe to tree node selected event
    // this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
    //   if (item && item.location === 'project_auth') {
    //     if (item.item_id || item.id) {
    //       this.selectedNode = item;
    //       if (item['is_editable']) {
    //         this.isEditable = item['is_editable'] === (0 || 2) ? false : true;
    //       } else {
    //         this.isEditable = false;
    //       }
    //       this.getSelectedTreeItemDetail(item.item_id ? item.item_id : item.id);
    //     }
    //   }
    // });

    this.resizeRightPanelWidth();
    this.sharedService.faqEvent.next({
      name: 'project_authoring'
    });
  }

  ngOnChanges() {

  }

  ngAfterViewInit() {
    this.rightPaneSub = this.splitElem.dragProgress$.subscribe((split: any) => {
      this.resizeRightPanelWidth();
    });
  }

  /**
   * Get Project Detail
   */
  getProjectDetail() {
    const url = GlobalSettings.PROJECT_LIST + '/' + this.projectId;
    this.service.getServiceData(url).then((res) => {
      this.projectDetail = res;
      if (res['project_type']) {
        localStorage.setItem('project_type', res['project_type']);
      }
      this.sharedService.workFlowId.next({
        'workFlowId': res['workflow_id'],
        'projectId': this.projectId,
        'currentWorkflowStage': res['current_workflow_stage_id'].split('||')[1]
      });
      console.log('#### KUNAL', res['document_status']);
      if (res['document_status'] === 5 || res['document_status'] === 3) {
        this.preventEdit = true;
        this.sharedService.preventEdit.next({
          'preventEdit': this.preventEdit,
        });
      } else {
        this.preventEdit = false;
        this.sharedService.preventEdit.next({
          'preventEdit': this.preventEdit,
        });
      }
      // Emit event to base component to update the title
      this.sharedService.setTitleEvent.next({
        'type': 'project',
        'title': this.projectDetail.project_name
      });
    }).catch((ex) => {
      console.log('getProjectDetail ', ex);
    });
  }

  /**
   * Get Project Tree Data With All Nodes
   */
  getAllNodes(callUpdate = false) {
    if (callUpdate) {
      this.reorderComplete = false;
    }
    this.isAllNode = true;
    // const url = GlobalSettings.GET_MAPPED_NODES_ALL + '/' + this.projectId;
    const url = GlobalSettings.GET_MAPPED_NODES_ALL_ENHANCED + '/' + this.projectId;
    this.treeDataService.getTreeData(url, true, Utils.EXPAND_LEVEL).then((res: any) => {
      if (res) {
        // Sort the tree according to list enumaration
        // Utils.sortData(res.parsedTreeNodes['children']);
        this.taxonomyData = res.parsedTreeNodes;
        if (callUpdate) {
          this.update();
        }
        if (this.taxonomyData.children && this.taxonomyData.children.length > 0) {
          this.selectedNode = this.taxonomyData.children[0];
          this.tempTaxonomyData = this.taxonomyData.children;
          this.firstNode = this.selectedNode;
          this.firstItemNode = this.selectedNode;
          if (this.selectedNode['is_document'] === 1) {
            this.firstItemNode = this.loadNextNode(this.selectedNode);
          }
          this.lastNode = this.findLastNode(this.taxonomyData);
          // const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
          // this.getSelectedTreeItemDetail(id);
        }
        if (this.assignedToMe === 'assignedToMe') {
          this.getProjectComments(this.projectId); // comment icon setting for 'comments assigned to me'
        } else {
          for (let i = 0; i < this.taxonomyData.children.length; i++) {
            this.setOpenCommentForAllNode(this.taxonomyData.children[i]); // comment icon setting for 'open commented node'
          }
        }
        this.getItemsComplianceReport(); // For compliance icon setting
        this.clickSelectedNode();
        setTimeout(() => {
          this.projectAuthHeightCalculation();
          this.resizeRightPanelWidth(19);
        }, 100);
        setTimeout(() => {
          if (this.treeSearch) {
            this.treeSearch.getFilterData(this.taxonomyData, true);
            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        }, 500);
      }
    }).catch((ex) => {
      console.log('getProjectMappedNodes ', ex);
      if (ex && ex.status === 400) { // If no project nodes found, redirect to project list
        this.sendMsgNoSuchProject();
      }
    });
  }

  clickSelectedNode() {
    setTimeout(() => {
      if (document.getElementById(this.selectedNode['id'] + '-node')) {
        document.getElementById(this.selectedNode['id'] + '-node').click();
      }
    }, 1000);
  }

  /**
   * Get The Project Tree Data Only For Assigned Nodes or Associated Nodes
   */
  getProjectAssociatedNodes(callUpdate = false) {
    if (callUpdate) {
      this.reorderComplete = false;
    }
    this.isAllNode = false;
    if (localStorage.getItem('project_type') === '2') {
      this.getPacingGuideNodes();
    } else {
      const url = GlobalSettings.GET_PROJECT_TREE_VIEW_ENHANCED + this.projectId;
      this.mappedNodeService.getTreeData(url, Utils.EXPAND_LEVEL).then((res) => {
        if (res) {
          this.taxonomyData = null;
          this.taxonomyOriginalData = null;
          this.buildTaxonomyData = {};
          // Sort the tree according to list enumaration
          // Utils.sortData(res);
          this.taxonomyData = res;
          this.taxonomyOriginalData = JSON.parse(JSON.stringify(res));
          if (callUpdate) {
            this.update();
          }
          if (this.taxonomyData.length > 0) {
            this.buildTaxonomyData['children'] = [];
            this.taxonomyData.forEach(element => {
              if (element.children && element.children.length) {
                element.children.forEach((child: any) => {
                  this.buildTaxonomyData['children'].push(JSON.parse(JSON.stringify(child)));
                });
              }
            });
            this.tempTaxonomyData = this.taxonomyData;
            if (this.taxonomyData[0]) {
              if (this.taxonomyData[0].children.length > 0) {
                if (this.taxonomyData[0].children[0]['isOrphanLabel']) { // skipping orphan node lable if any
                  this.firstNode = this.taxonomyData[0].children[0].children[0];
                } else {
                  this.firstNode = this.taxonomyData[0].children[0];
                }
                this.firstItemNode = this.firstNode;
                if (this.firstNode['is_document'] === 1) {
                  this.firstItemNode = this.loadNextNode(this.firstNode);
                }
                this.lastNode = this.findLastNode(this.taxonomyData);
                if (this.itemId) {
                  this.selectedNode['id'] = this.itemId;
                  this.getParentNodeObject(this.selectedNode['id']);
                  this.selectedNode = this.parentNode;
                } else {
                  this.selectedNode = this.taxonomyData[0].children[0];
                }
                // const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
                // this.getSelectedTreeItemDetail(id);

              }
            }
          }
          if (this.assignedToMe === 'assignedToMe') {
            this.getProjectComments(this.projectId); // comment icon setting for 'comments assigned to me'
          } else {
            for (let i = 0; i < this.taxonomyData.length; i++) {
              this.setOpenCommentForAllNode(this.taxonomyData[i]); // comment icon setting for 'open commented node'
            }
          }
          this.getItemsComplianceReport(); // For compliance icon setting
          // this.expendSelectedNode();

          if (!this.reLoadTree) {
            setTimeout(() => {
              if (this.itemId) {
                if (document.getElementById(this.itemId + '-node')) {
                  document.getElementById(this.itemId + '-node').click();
                  // document.getElementById(this.itemId + '-atag').click();
                }
              } else {
                let clickableNode: any;
                if (this.taxonomyData[0]) {
                  if (this.taxonomyData[0].children[0]['isOrphanLabel']) {
                    clickableNode = this.taxonomyData[0].children[0].children[0];
                  } else {
                    clickableNode = this.taxonomyData[0].children[0];
                  }
                  this.expandTreeTillSelectedNode(clickableNode);
                }
                // if (document.getElementById(this.taxonomyData[0].children[0]['id'] + '-node')) {
                //   document.getElementById(this.taxonomyData[0].children[0]['id'] + '-node').click();
                // }
              }
            }, 1150);
          }
          if (this.reLoadTree) {
            setTimeout(() => {
              const node = {
                id: this.itemId,
                is_document: 0,
                parent_id: this.itemId
              };
              this.expandTreeTillSelectedNode(node);

            }, 1000);
            setTimeout(() => {
              if (this.itemId) {
                if (document.getElementById(this.itemId + '-node')) {
                  document.getElementById(this.itemId + '-node').click();
                }
              }
            }, 1000);
          }
          setTimeout(() => {
            this.projectAuthHeightCalculation();
            this.resizeRightPanelWidth(19);
          }, 100);
          setTimeout(() => {
            if (this.treeSearch) {
              this.treeSearch.getFilterData(this.taxonomyData, true);
              this.isLoading = false;
            } else {
              this.isLoading = false;
            }
          }, 500);
        }
        this.reLoadTree = false;
      }).catch((ex) => {
        console.log('getProjectAssociatedNodes ', ex);
        if (ex && ex.status === 400) { // If no project nodes found, redirect to project list
          this.sendMsgNoSuchProject();
        }
      });
    }
  }

  sendMsgNoSuchProject() {
    this.sharedService.sucessEvent.next({
      type: 'no_project'
    });
    setTimeout(() => {
      this.redirectToProjectList();
    }, 500);
  }
  /**
   * Get The Selected Node Detail By ItemId
   * @param  {string} itemId
   */
  getSelectedTreeItemDetail(itemId: string) {

    let url;
    this.nodeLoading = true;
    if (this.selectedNode['is_document'] === 1) {
      this.showExempler = false;
      url = GlobalSettings.GET_CFDOC_ITEM_DETAIL + '/' + itemId + '?project_id=' + this.projectId;
    } else {
      this.showExempler = true;
      url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + itemId + '?project_id=' + this.projectId;
    }
    this.itemAssociations = [];
    this.itemExemplars = [];
    this.itemAssets = [];
    this.itemAdditionalMetadata = [];
    this.itemLinkedNodes = [];
    this.selectedNode['open_comment_count'] = 0;
    this.itemNodetypeList = [];
    this.disableOptionList = [];
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.sourceDocumentId = res.source_document_id;
        // this.showAdditionaMetaData = true;
        // If Selected Node Is Document Node
        if (this.selectedNode['is_document'] === 1) {
          // res['is_editable'] = 1;
          // this.showAdditionaMetaData = false;
          if (!res.node_type_id) {
            this.setNodeTypeByTitle(this.rootNodeType);
            res['node_type_id'] = this.selectedNodeType.node_type_id;
            res['node_type'] = this.selectedNodeType.title;
          }
          res['source_uuid_id'] = res['source_document_id'];
        } else {
          res['source_uuid_id'] = res['source_item_id'];
        }
        // Check the node node_type and set it into UI
        if (res.node_type_id && res.node_type_id.length > 0) {
          console.log('OnNodeSeltected IF ', res.node_type_id);
          setTimeout(() => {
            this.setNodeTypeById(res.node_type_id);
          }, 1000);
        } else {
          console.log('OnNodeSeltected ELSE ', res.node_type_id, this.defaultNodeType);
          // If node_type is undefined then set it to defaultNodeType
          if (this.nodetypeData) {
            from(this.nodetypeData)
              .filter((w: any) => w.title === this.defaultNodeType)
              .subscribe(result => {
                this.selectedNodeType = JSON.parse(JSON.stringify(result));
                res['node_type_id'] = result.node_type_id;
                res['node_type'] = result.title;
              });
          }
        }
        this.itemNodetypeList = this.selectedNode.nodetypeList;
        if (this.itemNodetypeList && this.itemNodetypeList.length) {
          this.disableOptionList.push(this.itemNodetypeList.find(type => type.title === 'Document'));
        }
        setTimeout(() => {
          if (this.nodeDetailsComponent) {
            // console.log('this.selectedNodeType.title', this.selectedNodeType.title);
            this.nodeDetailsComponent.generateForm(res, this.selectedNodeType.title);
          }
        }, 1000);
        this.addedAdditionalMetadata = [];
        if (res.custom_metadata) {
          res.custom_metadata.forEach(obj => {
            if (obj.is_additional_metadata === 1) {
              this.addedAdditionalMetadata.push(obj);
            }
          });
        }
        this.isAllFieldFilled = false;
        setTimeout(() => {
          if (!this.valid && this.selectedNode['is_editable'] === 1) {
            this.isAllFieldFilled = true;
          }
        }, 1200);
        this.selectedNodeDetail = res;
        this.selectNodeItemID = itemId;
        if (this.projectNotesComponent) {
          this.projectNotesComponent.getItemComments(itemId);
          //  this.selectNodeItemID = res.item_id;
        }
        setTimeout(() => {
          this.showAssocciation = true;
          if (res.item_associations) {
            // this.addAssociationsToTreeNode(res.item_associations);
            this.itemAssociations = res.item_associations;
          }
          if (res.exemplar_associations) {
            this.itemExemplars = res.exemplar_associations;
          }
          if (res.assets) {
            this.itemAssets = res.assets;
          }
          if (res.linked_item) {
            this.itemLinkedNodes = res.linked_item;
          }
          if (res.custom_metadata) {
            this.itemAdditionalMetadata = res.custom_metadata;
          }

          if ((this.itemAssociations && this.itemAssociations.length > 0) ||
            (this.itemLinkedNodes && this.itemLinkedNodes.length > 0)) {

            // modifying association array
            if (this.itemAssociations && this.itemAssociations.length) {
              this.itemAssociations.forEach(element => {
                element['link_type'] = 'association';
                element['target_node'] = element.destination_node;
                element['taxonomy_name'] = element.target_node.document.document_title;
              });
            }

            // modifying linked nodes array
            if (this.itemLinkedNodes && this.itemLinkedNodes.length) {
              this.itemLinkedNodes.forEach(element => {
                element['link_type'] = 'linked node';
                element['target_node'] = element.source_node;
                element['taxonomy_name'] = element.target_node.document.document_title;
              });
            }

            // combining association and linked nodes in a single array
            this.combinedAssociationArr = this.itemAssociations.concat(this.itemLinkedNodes);

            // taking association_type_name at outer level for grouping the array
            this.combinedAssociationArr.forEach(node => {
              node['association_type_name'] = node.association_type.type_name;
            });

            // grouping the combined array by taxonomy name
            this.associatedNodes = Utils.groupByType(this.combinedAssociationArr, 'taxonomy_name');

            // grouping the modified array by association type
            this.associatedNodes.forEach(item => {
              item.value = Utils.groupByType(item.value, 'association_type_name', 'sequence_number');
            });
          }
        }, 600);
        const rightContainer = document.getElementById('right_container_id');
        if (rightContainer) {
          rightContainer.scrollTop = 0;
        }
        if (document.getElementById('mySidenav')) {
          document.getElementById('mySidenav').style.right = '-250%';
        }
        if (this.showCommentPopup === 'true' && this.isEditable && document.getElementById('btnComment')) {
          document.getElementById('btnComment').click();
          this.showCommentPopup = 'false';
        }
        if (res.open_comments_count) {
          this.selectedNode['open_comment_count'] = res.open_comments_count;
        }
      }
      this.nodeLoading = false;
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
      setTimeout(() => {
        if (this.nodeDetailsComponent) {
          // console.log('this.selectedNodeType.title', this.selectedNodeType.title);
          this.nodeDetailsComponent.generateForm([], this.selectedNodeType.title);
        }
      }, 1000);
    });
  }


  /**
   * Get Document Node Detail
   * @param  {any} destination_node
   */
  getDocumentDetail(destination_node: any) {
    const url = GlobalSettings.GET_DOCUMENT_DETAIL + '/' + destination_node.document_id;
    this.service.getServiceData(url).then((res: any) => {
      destination_node.title = res.title;
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
    });
  }


  /**
   * Add Node Event
   * @param  {} item
   */
  addNodeEvent(item) {
    if (item && item.location === 'project_auth') {
      if (item.item_id || item.id) {
        this.selectedNode = item;
        this.addNode();
      }
    }
  }

  addNode() {
    const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
    this.setNewNodeNodeType();
    let list_enumeration = 1;
    let sequence_number = 1;
    if (this.selectedNode && this.selectedNode['children']) {
      list_enumeration = this.selectedNode['children'].length + 1;
      sequence_number = this.selectedNode['children'].length + 1;
    }
    const body = {
      parent_id: id,
      document_id: this.projectDetail.document_id,
      full_statement: '',
      full_statement_html: '',
      human_coding_scheme: '',
      human_coding_scheme_html: '',
      list_enumeration: '' + list_enumeration,
      sequence_number: '' + sequence_number,
      abbreviated_statement: '',
      education_level: '',
      note: '',
      concept_keywords: '',
      language_id: '',
      node_type_id: this.selectedNodeType.node_type_id,
      alternative_label: '',
      project_id: this.projectId
    };

    const url = GlobalSettings.CREATE_NODE;
    this.service.postService(url, body).then((res: any) => {
      if (this.selectedNode) {
        const level = this.selectedNode['level'];
        const parentId = this.selectedNode['id'];

        if (!this.selectedNode['children']) {
          this.selectedNode['children'] = [];
        }

        this.selectedNode['children'].push(res);
        this.selectedNode['expand'] = true;
        res.breadCrumb = this.selectedNode.breadCrumb + ' > ';
        res.breadCrumbArray = res.breadCrumb.split('>');

        // Reset the selectedNod object
        this.initializeSelectednode();
        // Now assign the newly created object to selected Object
        this.selectedNode = res;
        this.selectedNode['children'] = [];
        this.selectedNode['id'] = res.item_id;
        this.selectedNode['is_editable'] = 1;
        this.selectedNode['node_type'] = this.selectedNodeType['title'];
        this.selectedNode['metadataType'] = this.selectedNodeType['title'];
        this.selectedNode['parent_id'] = parentId;
        this.selectedNode['level'] = level + 1;
        this.selectedNode['nodetypeList'] = [];
        this.selectedNode['cut'] = 0;
        this.selectedNode['paste'] = 0;
        this.selectedNode['reorder'] = 0;
        this.nodetypeData.forEach(type => {
          this.selectedNode['nodetypeList'].push({
            node_type_id: type.node_type_id,
            title: type.title,
            is_selected: (this.selectedNode.node_type === type.title ? 1 : 0)
          });
        });
        if (this.tree) {
          this.tree.onNodeSelected(this.selectedNode);
        }
        this.sharedService.sucessEvent.next({
          type: 'create_node'
        });
        this.updateTaxonomyAfterTreeNodeUpdate(this.selectedNode, this.buildTaxonomyData['children'], 'add');
      }
      // this.getProjectMappedNodes();
    }).catch((ex) => {
      console.log('createNode ', ex);
    });

  }

  /**
   * Delete Node Event
   * @param  {} item
   */
  deleteNodeEvent(item) {
    if (item && item.location === 'project_auth') {
      if (item.item_id || item.id) {
        this.selectedNode = item;
        this.deleteNode();
      }
    }
  }

  deleteNode() {
    if (this.selectedNode.item_id || this.selectedNode['id']) {
      const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
      const url = GlobalSettings.DELETE_NODE + '/' + id + '?project_id=' + this.projectId,
        body = {
          project_id: this.projectId
        };

      this.service.deleteServiceData(url, false, body).then((res) => {
        this.getParentNodeObject(this.selectedNode['parent_id']);
        this.deleteObjectFromTree(id);
        this.updateTaxonomyAfterTreeNodeUpdate(this.selectedNode, this.buildTaxonomyData['children'], 'delete');
        this.tree.getTreeData(this.taxonomyData[0].children);
        this.sharedService.sucessEvent.next({
          type: 'delete_node'
        });
      }).catch((ex) => {
        console.log('deleteNode ', ex);
        /*this.getParentNodeObject(this.selectedNode['parent_id']);
        this.deleteObjectFromTree(id);
        this.sharedService.sucessEvent.next({
          type: 'delete_node'
        });*/
        // this.getProjectData();
      });
    }
  }


  getProjectData(callUpdate = false) {
    if (this.isAllNode) {
      this.getAllNodes(callUpdate);
    } else {
      this.getProjectAssociatedNodes(callUpdate);
    }
  }

  editNode() {
    if (this.nodeDetailsComponent) {
      this.editorEnabled = this.nodeDetailsComponent.checkEditorStatus();
    }
    if (this.editorEnabled) {
      this.sharedService.sucessEvent.next({
        type: 'rte_update_missing'
      });
      // do something editor open not updated
    } else {
      this.nodeDetailsComponent.checkValidation();
      if (this.valid) {
        console.log('this.valid', this.valid);
        this.isAllFieldFilled = false;
      } else {
        console.log('this.invalid', this.valid);
        this.isAllFieldFilled = true;
      }
      this.nodeDetailsComponent.getFormData();
      // this.additionalMetadataComponent.getAdditionMetadata();
      // if (this.additionalMetadataObj.length > 0) {
      //   this.additionalMetadataObj.forEach(obj => {
      //     this.formData['custom'].push(obj);
      //   });
      // }
      this.formData['parent_id'] = this.selectedNode['parent_id'];
      this.formData['document_id'] = this.projectDetail.document_id;
      this.formData['projectId'] = this.projectId;
      console.log('dateformat ********* ', this.formData);
      if (this.formData['status_start_date']) {
        if (this.formData['status_start_date']['formatted']) {
          this.formData['status_start_date'] = this.formData['status_start_date']['formatted'];
        } else {
          if (this.formData['status_start_date']['date']) {
            this.formData['status_start_date'] = this.formData['status_start_date']['date']['year'] +
              '-' + this.formateNumber(this.formData['status_start_date']['date']['month']) +
              '-' + this.formateNumber(this.formData['status_start_date']['date']['day']);
          }
        }
      }
      if (this.formData['status_end_date']) {
        if (this.formData['status_end_date']['formatted']) {
          this.formData['status_end_date'] = this.formData['status_end_date']['formatted'];
        } else {
          if (this.formData['status_end_date']['date']) {
            this.formData['status_end_date'] = this.formData['status_end_date']['date']['year'] +
              '-' + this.formateNumber(this.formData['status_end_date']['date']['month']) +
              '-' + this.formateNumber(this.formData['status_end_date']['date']['day']);
          }
        }
      }
      if (this.formData['language_name']) {
        this.formData['language_name'] = this.formData['language_name']['short_code'];
      }
      const body = this.formData;
      let url;
      const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
      if (this.selectedNode['is_document'] === 1) {
        url = GlobalSettings.SAVE_CFDOC_NODE + '/' + this.projectDetail.document_id;
      } else {
        url = GlobalSettings.EDIT_NODE + '/' + id;
      }
      this.service.putService(url, body).then((res: any) => {
        this.updateBreadCrumb(res);

        this.selectedNode['human_coding_scheme'] = res.human_coding_scheme;
        this.selectedNode['full_statement'] = res.full_statement;
        // this.selectedNode['human_coding_scheme_html'] = res.human_coding_scheme_html;
        this.selectedNode['full_statement_html'] = res.full_statement_html;
        this.selectedNode['education_level'] = res.education_level;
        this.selectedNode['list_enumeration'] = res.list_enumeration;
        this.selectedNode['sequence_number'] = res.sequence_number;
        if (this.selectedNode['is_document'] === 1) {
          this.selectedNode['full_statement'] = res.title;
          this.selectedNode['full_statement_html'] = res.title_html;
        }
        this.sharedService.sucessEvent.next({
          type: 'edit_node'
        });
        this.getParentNodeObject(body.parent_id);
        if (this.parentNode && this.parentNode.children) {
          Utils.sortData(this.parentNode.children);
        }
        this.getItemsComplianceReport();
        // setTimeout(() => {
        //   if (this.treeSearch) {
        //     this.treeSearch.getFilterData(this.taxonomyData, true);
        //     this.isLoading = false;
        //   } else {
        //     this.isLoading = false;
        //   }
        // }, 500);
      }).catch((ex) => {
        console.log('createNode ', ex);
      });
      // } else {
      //   this.sharedService.sucessEvent.next({
      //     type: 'node_details_missing'
      //   });
      //   this.isAllFieldFilled = true;
      // }
    }


  }

  formateNumber(num) {
    return ('0' + num).slice(-2);
  }

  onAssocationCreated(evt) {
    const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
    this.getSelectedTreeItemDetail(id);
    if (evt && evt.selectedAssociationType) {
      this.selectedAssociationType = evt.selectedAssociationType;
    }
  }
  onAddDelteExemplar(evt) {
    const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
    this.getSelectedTreeItemDetail(id);
  }

  deleteAssociation(item) {

    this.dialogService.confirm('Confirm', 'Do you want to delete the association?')
      .then((confirmed) => {
        if (confirmed) {
          // console.log(' deleteAssociation ', item);
          if (item.item_association_id) {
            const url = GlobalSettings.DELETE_ITEM_ASSOCIATION + '/' + item.item_association_id;
            this.service.deleteServiceData(url).then((res) => {
              this.sharedService.sucessEvent.next({
                type: 'delete_association'
              });
              const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
              this.getSelectedTreeItemDetail(id);
              // this.getProjectData();
              if (item.association_type.type_name === 'isChildOf' || item.association_type.type_name === 'isPeerOf') {
                this.reloadTree(id);
              }
            }).catch((ex) => {
              console.log('deleteAssociation ', ex);
              this.sharedService.sucessEvent.next({
                type: 'not_delete_child_association'
              });
            });
          }
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }

  getIndex(idx) {
    console.log(idx);
    this.currentIndex = idx;
    this.description = this.getItem(idx, 'description');
  }
  getItem(idx, type) {
    if (idx !== undefined) {
      if (type === 'description') {
        // this.description = this.description;
        return this.itemAssociations[idx].description;
      }
      return this.itemAssociations[this.currentIndex];
    }
  }
  editAssociation(item) {

    if (item.item_association_id) {
      const url = GlobalSettings.EDIT_ITEM_ASSOCIATION + '/' + item.item_association_id,
        body = {
          origin_node_id: item.origin_node.item_id,
          destination_node_id: item.destination_node.item_id,
          association_type: item.association_type.type_id,
          description: this.description
        };

      this.service.putService(url, body).then((res) => {
        const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
        this.getSelectedTreeItemDetail(id);
        this.sharedService.sucessEvent.next({
          type: 'edit_association_desc'
        });
      }).catch((ex) => {
        console.log('editAssociation ', ex);
      });
    }
  }

  userRoleRight() {

    if (this.selectedStageId) {
      this.createNodePermission = this.authenticateUser.AuthenticProjectPermissions('Create Node');
      const createNode = Utils.checkProjectPermission(this.projectId, 'Create Node', 'project_permissions', this.selectedStageId);
      if (createNode.hasProject) {
        this.createNodePermission = createNode.valid;
      }

      this.editNodePermission = this.authenticateUser.AuthenticProjectPermissions('Edit Node');
      const editNode = Utils.checkProjectPermission(this.projectId, 'Edit Node', 'project_permissions', this.selectedStageId);
      if (editNode.hasProject) {
        this.editNodePermission = editNode.valid;
      }

      this.deleteNodePermission = this.authenticateUser.AuthenticProjectPermissions('Delete Node');
      const deleteNode = Utils.checkProjectPermission(this.projectId, 'Delete Node', 'project_permissions', this.selectedStageId);
      if (deleteNode.hasProject) {
        this.deleteNodePermission = deleteNode.valid;
      }

      this.isAddAssociations = this.authenticateUser.AuthenticProjectPermissions('Create Associations');
      const createAsso = Utils.checkProjectPermission(this.projectId, 'Create Associations', 'project_permissions', this.selectedStageId);
      if (createAsso.hasProject) {
        this.isAddAssociations = createAsso.valid;
      }

      this.updateAssociations = this.authenticateUser.AuthenticProjectPermissions('Update Associations');
      const updateAsso = Utils.checkProjectPermission(this.projectId, 'Update Associations', 'project_permissions', this.selectedStageId);
      if (updateAsso.hasProject) {
        this.updateAssociations = updateAsso.valid;
      }

      this.deleteAssociations = this.authenticateUser.AuthenticProjectPermissions('Delete Associations');
      const deleteAsso = Utils.checkProjectPermission(this.projectId, 'Delete Associations', 'project_permissions', this.selectedStageId);
      if (deleteAsso.hasProject) {
        this.deleteAssociations = deleteAsso.valid;
      }

      this.viewCommentList = true;
      const globViewComment = this.authenticateUser.AuthenticateComment('View My Comments', 'project');
      const globViewOthrComment = this.authenticateUser.AuthenticateComment('View Others Comments', 'project');
      const viewComments = Utils.checkProjectPermission(this.projectId, 'View My Comments', 'comment_permissions', this.selectedStageId);
      const viewOtrComment = Utils.checkProjectPermission(this.projectId, 'View Others Comments', 'comment_permissions', this.selectedStageId);
      if (!globViewComment && !globViewOthrComment) {
        this.viewCommentList = false;
        if (viewComments.hasProject || viewOtrComment.hasProject) {
          this.viewCommentList = ((viewComments.hasProject ? viewComments.valid : false) ||
            (viewOtrComment.hasProject ? viewOtrComment.valid : false));
        }
      } else {
        if (viewComments.hasProject && viewOtrComment.hasProject) {
          this.viewCommentList = (viewComments.valid || viewOtrComment.valid);
        } else if (!viewComments.hasProject && !viewOtrComment.hasProject) {
          this.viewCommentList = true;
        } else if (viewComments.hasProject && !viewOtrComment.hasProject) {
          this.viewCommentList = viewComments.valid;
        } else if (!viewComments.hasProject && viewOtrComment.hasProject) {
          this.viewCommentList = viewOtrComment.valid;
        }
      }


      this.createAdditionalMetadataPermission = this.authenticateUser.AuthenticProjectPermissions('Add Additional Metadata');
      const creatAdditionaleMetadata = Utils.checkProjectPermission(this.projectId, 'Add Additional Metadata', 'project_permissions', this.selectedStageId);
      if (creatAdditionaleMetadata.hasProject) {
        this.createAdditionalMetadataPermission = creatAdditionaleMetadata.valid;
      }

      if (localStorage.getItem('project_type') === '2') {
        this.editNodePermission = this.authenticateUser.AuthenticateComment('Edit Container', 'pacing_guide');
        const editNode1 = Utils.checkProjectPermission(this.projectId, 'Edit Container', 'pacing_guide_permissions', this.selectedStageId);
        if (editNode1.hasProject) {
          this.editNodePermission = editNode1.valid;
        }
        let workflowGlobalPermission = this.authenticateUser.AuthenticateComment('Change Workflow Stage', 'pacing_guide');
        const workFlowPGPermission = Utils.checkProjectPermission(this.projectId, 'Change Workflow Stage', 'pacing_guide_permissions', this.selectedStageId);
        if (workFlowPGPermission.hasProject) {
          workflowGlobalPermission = workFlowPGPermission.valid;
        }
        let editPGGlobalPermission = this.authenticateUser.AuthenticateComment('Edit Pacing Guide', 'pacing_guide');
        const editPGPermission = Utils.checkProjectPermission(this.projectId, 'Edit Pacing Guide', 'pacing_guide_permissions', this.selectedStageId);
        if (editPGPermission.hasProject) {
          editPGGlobalPermission = editPGPermission.valid;
        }
        // If edit pacing guide permission is false, then disable workflow edit i.e. override workflow permission with false
        if (!editPGGlobalPermission) {
          workflowGlobalPermission = false;
        }
        this.sharedService.workFlowPermission.next({
          'is_editable': !workflowGlobalPermission,
        });
      }
    }


  }

  deleteObjectFromTree(id) {
    if (this.parentNode) {
      if (this.parentNode.children) {
        let nodeIndex = -1;
        for (let index = 0; index < this.parentNode.children.length; index++) {
          const child = this.parentNode.children[index];
          if (child.id === id) {
            nodeIndex = index;
            break;
          }
        }
        if (nodeIndex > -1) {
          // Delete Node from parent children array using slice method
          if (Array.isArray(this.taxonomyData)) {
            Utils.delNodeOccurance(this.taxonomyData, id);
          } else {
            Utils.delNodeOccurance([{
              children: this.taxonomyData.children
            }], id);
          }
          // this.parentNode.children.splice(nodeIndex, 1);

          if (nodeIndex === 0) {
            if (this.tree) {
              setTimeout(() => {
                this.tree.selectedNodeId = this.parentNode.id;
                this.tree.onNodeSelected(this.parentNode);
              }, 100);
            }
          } else {
            if (this.tree) {
              const prevNode = this.parentNode.children[nodeIndex - 1];
              if (prevNode) {
                setTimeout(() => {
                  this.tree.onNodeSelected(prevNode);
                }, 100);
              }
            }
          }
        } else {
          console.log('deleteObjectFromTree Deleted node not found');
        }
      } else {
        console.log('deleteObjectFromTree ParentNode children is null');
      }

    } else {
      console.log('deleteObjectFromTree ParentNode is null');
    }
  }

  getParentNodeObject(parentId) {

    if (this.isAllNode === false) {
      this.taxonomyData.forEach(element => {
        this.getParent(element, parentId);
      });
    } else {
      this.taxonomyData.children.forEach(element => {
        this.getParent(element, parentId);
      });
    }

  }

  getParent(node, parentId) {
    if (node.id === parentId) {
      this.parentNode = node;
      // console.log('getParent*********** ', node.id, parentId);
    } else {
      if (node.children.length > 0) {
        node.children.forEach(element => {
          this.getParent(element, parentId);
        });
      }
    }
  }

  setCommentNotificationStatus(status) {
    this.commentNotification = status;
  }
  selectedNodeTop(top) {
    console.log('selectedNodeTop  ', top);
    const treeContainer = document.getElementById('tree_container_id');
    if (treeContainer) {
      const height = window.innerHeight;
      let diff = top - height;
      if (diff > 0) {
        diff += 50;
      }
      treeContainer.scrollTop += diff;
    }
  }

  startLogProjectActivity() {

    this.intervalTimer = setInterval(() => this.logProjectSessionActivity(), 1000 * 60);
  }

  logProjectSessionActivity() {
    const url = GlobalSettings.SESSION_ACITIVITY + '/' + this.projectId;
    this.service.postService(url, {}).then((res) => {
      // console.log(' Log Activity ', res);
    }).catch((ex) => {
      console.log(' Log Activity Catch', ex);
    });

  }

  getAllMetadata() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.nodetypeData = res.nodetype;
    });
  }
  /**
   * UI Event
   * @param  {} data
   */
  onNodeTypeSelected(data) {
    this.selectedNode['node_type_id'] = data.node_type_id;
    this.selectedNodeType = data;
  }
  receiveFormObj(event) {
    this.formData = event;
  }

  receiveAddonMetaObj(event) {
    console.log('Received', event);
    if (event.list) {
      this.additionalMetadataObj = event.list;
      this.addedAdditionalMetadata = event.list;
    }
    this.updateNodeDetail(event.response, event.value);
    /*const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
    this.getSelectedTreeItemDetail(id);*/
  }

  updateNodeDetail(response, value) {
    if (response) {
      if (this.selectedNode) {

        if (response.internal_name === 'human_coding_scheme') {
          this.selectedNode['human_coding_scheme'] = value;
        }
        if (response.internal_name === 'full_statement') {
          this.selectedNode['full_statement'] = value;
        }
        if (response.internal_name === 'list_enumeration' || response.internal_name === 'sequence_number') {
          this.selectedNode['list_enumeration'] = value;
          this.selectedNode['sequence_number'] = value;
          if (this.parentNode && this.parentNode.children) {
            Utils.sortData(this.parentNode.children);
          }
        }
      }
    }
  }

  formValidate(event) {
    this.valid = event;
  }

  formObject(event) {
    if (event.controls['official_source_url'] && event.controls['official_source_url'].value.length > 0) {
      if (event.controls['official_source_url'].valid === false) {
        this.invalidUrl = true;
      } else {
        this.invalidUrl = false;
      }
    } else {
      this.invalidUrl = false;
    }
  }

  /**
   * Delete Project Resource like Exemplar, Assets, Additional metadata
   */
  onDeleteProjectResource(item_association_id, resourceType) {
    let msg, url, successEventType;
    if (resourceType === 'Document') {
      msg = 'Are you sure you want to delete this document?';
      url = GlobalSettings.ASSET_ASSOCIATION + '/' + item_association_id;
      successEventType = 'delete_asset';
    } else if (resourceType === 'Exemplar') {
      msg = 'Do you want to delete the exemplar?';
      url = GlobalSettings.EXEMPLER_ASSOCIATION + '/' + item_association_id;
      successEventType = 'delete_exemplar';
    } else if (resourceType === 'AdditionalMetadata') {
      msg = 'Do you want to delete this additional metadata?';
      let nodeId;
      /* For pacing guide node id field will be 'item_id' and for project node id field will be 'id' */
      if (localStorage.getItem('project_type') === '2') { // pacing guide authoring checking
        nodeId = this.selectedNode.item_id;
      } else { // project authoring checking
        nodeId = this.selectedNode.id;
      }
      if (this.selectedNode['is_document'] === 1) { // Document level selection
        url = GlobalSettings.DOCUMENT_LEVEL_ADDITIONAL_METADATA + '/' + nodeId + '/' + item_association_id;
        // item_association_id => metadata id
      } else if (this.selectedNode['is_document'] === 0) { // Item level selection
        url = GlobalSettings.ITEM_LEVEL_ADDITIONAL_METADATA + '/' + nodeId + '/' + item_association_id;
        // item_association_id => metadata id
      }
      successEventType = 'delete_additional_metadata';
      // this.updateNodeDetail()
    }

    this.dialogService.confirm('Confirm', msg)
      .then((confirmed) => {
        if (confirmed) {
          this.service.deleteServiceData(url).then((res) => {
            console.log('onDeleteProjectresourceEvent  ', item_association_id);
            if (resourceType === 'Document') {
              this.itemAssets = this.removeProjectResourceFromList(this.itemAssets, item_association_id, resourceType);
            } else if (resourceType === 'Exemplar') {
              this.itemExemplars = this.removeProjectResourceFromList(this.itemExemplars, item_association_id, resourceType);
            } else if (resourceType === 'AdditionalMetadata') {
              // this.onAddDelteExemplar();
              this.itemAdditionalMetadata = this.removeProjectResourceFromList(this.itemAdditionalMetadata,
                item_association_id,
                resourceType);
              this.addedAdditionalMetadata = this.removeProjectResourceFromList(this.addedAdditionalMetadata,
                item_association_id,
                resourceType);
              if (this.additionalMetadataComponent &&
                this.additionalMetadataComponent.metadataList &&
                this.additionalMetadataComponent.metadataList.length > 0) {
                this.additionalMetadataComponent.getNodeTypeMetadata();
              } else {
                this.additionalMetadataComponent.getAllMetadata();
              }
              setTimeout(() => {
                additionalMetadataPanelHeight();
              }, 100);
            }
            this.sharedService.sucessEvent.next({
              type: successEventType
            });
          }).catch((ex) => {
            console.log('onDeleteProjectresourceEvent  ', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  /**
   * Removing resource (Exemplar, Assets) item from array
   * @param list[]
   * @param item_association_id
   * @param resourceType
   */
  removeProjectResourceFromList(list, item_association_id, resourceType) {
    let modifiedList = [];
    if (list) {
      let index = 0;
      let foundIndex = -1;
      if (resourceType === 'Document') {
        list.forEach(element => { // For Assets, unique field is asset_id
          if (item_association_id === element.asset_id) {
            foundIndex = index;
          }
          index++;
        });
      } else if (resourceType === 'Exemplar') {
        list.forEach(element => { // For Exemplar, unique field is association_id
          if (item_association_id === element.association_id) {
            foundIndex = index;
          }
          index++;
        });
      } else if (resourceType === 'AdditionalMetadata') {
        list.forEach(element => { // For AdditionalMetadata, unique field is metadata_id
          if (item_association_id === element.metadata_id) {
            foundIndex = index;
          }
          index++;
        });
      }
      if (foundIndex > -1) {
        if (list.length > foundIndex) {
          list.splice(foundIndex, 1);
        }
      }
      modifiedList = list;
    }
    return modifiedList;
  }

  /**
   * Set selectedNodeType by title
   * @param  {} title
   */
  setNodeTypeByTitle(title) {
    if (this.nodetypeData) {
      from(this.nodetypeData)
        .filter((w: any) => w.title === title)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
        });
    }
  }

  /**
   * Set selectedNodeType by node_type_id
   * @param  {} node_type_id
   */
  setNodeTypeById(node_type_id) {
    if (this.nodetypeData) {
      from(this.nodetypeData)
        .filter((w: any) => w.node_type_id === node_type_id)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
          // this.nodeType
        });
    }
  }

  /**
   * Set selected node type
   */
  setNewNodeNodeType() {
    if (this.selectedNode && this.selectedNode['children'] && this.selectedNode['children'].length > 0) {
      if (this.selectedNode['children'][0]['node_type_id']) {

        this.setNodeTypeById(this.selectedNode['children'][0]['node_type_id']);
      } else {
        this.setNodeTypeByTitle(this.defaultNodeType);
      }
    } else {
      this.setNodeTypeByTitle(this.defaultNodeType);
    }
  }
  /**
   * Componet Life Cycle Method
   */
  ngOnDestroy() {
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    if (this.projectTreeViewEvent) {
      this.projectTreeViewEvent.unsubscribe();
    }
    if (this.acivatedRouteEvent) {
      this.acivatedRouteEvent.unsubscribe();
    }
    if (this.updateTaxonomyEvent) {
      this.sharedService.updateTaxonomyEvent.next({
        update: false
      });
      this.updateTaxonomyEvent.unsubscribe();
    }
    if (this.intervalTimer) {
      clearInterval(this.intervalTimer);
    }
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }
    if (this.windowResizeSubscription) {
      this.windowResizeSubscription.unsubscribe();
    }
    if (this.rightPaneSub) {
      this.rightPaneSub.unsubscribe();
    }
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
  }

  optionSelected(data) {
    this.option = data;
    // Resizing height of right panel box of add-exemplar modal body
    setTimeout(() => {
      exemplarAssetPanelHeight();
      focusOnModalCloseBtn();
    }, 500);

    console.log(this.option);
  }

  onAddAssociation() {
    // Resizing height of right panel box of adding association modal body
    setTimeout(() => {
      assetPanelHeight();
      focusOnModalCloseBtn();
    }, 500);
  }

  onAddMetadata() {
    this.action = 'add';
    this.fetchedMetadata = new Object();
    // Resizing height of right panel box of adding additional-metadata modal body
    setTimeout(() => {
      additionalMetadataPanelHeight();
      focusOnModalCloseBtn();
    }, 500);
  }

  /**
   * On edit additional metadata associated to an item
   * @param obj {metadata_id, metadata_value}
   */
  onEditAdditionalMetadata(obj) {
    this.fetchedMetadata = new Object();
    const url = GlobalSettings.GET_METADATA_LIST + '/' + obj.metadata_id;

    // Fetching metadata value againgst metadata id and combining the value associated to the selected item
    this.service.getServiceData(url).then((res: any) => {
      this.fetchedMetadata = res;
      if (typeof this.fetchedMetadata.field_possible_values === 'string') {
        this.fetchedMetadata.field_possible_values = this.fetchedMetadata.field_possible_values.split(',');
      }
      this.fetchedMetadata.metadata_value = obj.metadata_value;
      this.fetchedMetadata.metadata_value_html = obj.metadata_value_html;
      this.action = 'edit';
    }).catch((ex) => {
      console.log('Error caught while fetching metadata', ex);
    });
  }

  /**
   * On next button click
   */
  onTreeNavigateNextClick() {
    this.selectedNode = this.loadNextNode(this.selectedNode);
    if (this.selectedNode.isOrphanLabel) { // Don't navigate to orphan node label
      this.selectedNode.expand = true;
      this.onTreeNavigateNextClick();
    }
    this.expandTreeTillSelectedNode(this.selectedNode);
  }

  /**
   * On previous button click
   */
  onTreeNavigatePreviousClick() {
    this.loadPreviousNode(this.selectedNode);
    if (this.tempNode.isOrphanLabel) { // Don't navigate to orphan node label
      this.selectedNode = this.tempNode;
      this.onTreeNavigatePreviousClick();
    }
    this.selectedNode = this.tempNode;
    this.expandTreeTillSelectedNode(this.selectedNode);
  }

  /**
   * To load next node of current node
   * @param selectedNode (current node)
   */
  loadNextNode(selectedNode) {
    if (selectedNode) {
      if (selectedNode.children && selectedNode.children.length > 0) { // If node has children
        return selectedNode.children[0];
      } else { // If no children
        this.getAvailableNode(selectedNode);
        return this.tempNode;
      }
    } else {
      return null;
    }
  }

  /**
   * To get next available node
   * @param currNode
   */
  getAvailableNode(currNode) {
    let parent, childList = [],
      position = 0;
    if (currNode.parent_id) {
      this.getParentNodeObject(currNode.parent_id);
      parent = this.parentNode;
      childList = parent.children;
      if (childList.length > 0) {
        position = this.findIndexInList(currNode, childList);
        if (position < childList.length - 1) { // If node is not in last position
          this.tempNode = childList[position + 1];
        } else { // If last position
          this.getAvailableNode(parent);
        }
      }
    } else {
      if (this.tempTaxonomyData && this.tempTaxonomyData.length > 1) {
        position = this.findIndexInList(currNode, this.tempTaxonomyData);
        this.tempNode = this.tempTaxonomyData[position + 1].children[0];
      } else {
        this.tempNode = this.firstNode;
      }
    }
  }

  /**
   * To load previous node of current node
   * @param selectedNode (current node)
   */
  loadPreviousNode(selectedNode) {
    let parent, childList = [],
      position = 0;
    if (selectedNode && selectedNode.parent_id) {
      this.getParentNodeObject(selectedNode.parent_id);
      parent = this.parentNode;
      childList = parent.children;
      if (childList.length > 0) {
        position = this.findIndexInList(selectedNode, childList);
        if (position > 0) { // If node is not in first position
          this.tempNode = childList[position - 1];
          if (this.tempNode.children.length > 0) {
            this.tempNode = this.findLastNode(this.tempNode.children[this.tempNode.children.length - 1]);
          }
        } else { // in first position
          if (parent && parent.parent_id) {
            this.tempNode = parent;
          } else {
            this.loadPreviousNode(parent);
          }
        }
      }
    } else {
      if (this.tempTaxonomyData && this.tempTaxonomyData.length > 1) {
        position = this.findIndexInList(selectedNode, this.tempTaxonomyData);
        const childPosition = this.tempTaxonomyData[position - 1].children.length - 1;
        this.tempNode = this.findLastNode(this.tempTaxonomyData[position - 1].children[childPosition]);
      } else {
        this.tempNode = this.tempTaxonomyData.length === 1 ? this.firstNode : this.lastNode;
      }
    }
  }

  /**
   * To fond position of element in a list
   * @param element
   * @param list
   */
  findIndexInList(element, list) {
    return list.findIndex(obj => ((obj.id === element.item_id) || (obj.id === element.id)));
  }

  /**
   * To find last node of tree
   * @param node
   */
  findLastNode(node: any) {
    if (node.children && node.children.length > 0) {
      return this.findLastNode(node.children[node.children.length - 1]);
    } else if (node && node.length > 0) {
      return this.findLastNode(node[node.length - 1]);
    } else {
      return JSON.parse(JSON.stringify(node));
    }
  }

  /**
   * Getting project comment
   * @param projectId
   */

  getProjectComments(projectId) {
    if (projectId) {
      const url = GlobalSettings.PROJECT_COMMENTS + '/' + projectId;
      this.service.getServiceData(url).then((res: any) => {
        if (res) {
          console.log(res);
          this.projectComments = JSON.parse(JSON.stringify(res));
          if (this.isAllNode === false) {
            for (let i = 0; i < this.taxonomyData.length; i++) {
              this.setMyCommentForAllNode(this.taxonomyData[i], Utils.EXPAND_LEVEL);
            }
          } else {
            for (let i = 0; i < this.taxonomyData.children.length; i++) {
              this.setMyCommentForAllNode(this.taxonomyData.children[i], Utils.EXPAND_LEVEL);
            }
          }
        }
      }).catch(ex => {
        console.log('getProjectComments ex: ', ex);
      });
    }
  }

  /**
   * To set commented for current user
   * @param data (taxonomy data)
   * @param expand
   */
  setMyCommentForAllNode(data: any, expand) {
    if (data) {
      if (data.children.length > 0) {
        // data.expand = expand;
        data.isCommented = this.isCommentAvailableForItem(data.id);
        for (let i = 0; i < data.children.length; i++) {
          this.setMyCommentForAllNode(data.children[i], expand);
        }
      } else {
        // data.expand = expand;
        data.isCommented = this.isCommentAvailableForItem(data.id);
      }
    }
  }

  /**
   * To check item has comment or not by current user
   * @param itemId
   */
  isCommentAvailableForItem(itemId) {
    let flag = false;
    if (itemId) {
      for (let i = 0; i < this.projectComments.length; i++) {
        if ((itemId === this.projectComments[i].thread_source_id) &&
          (this.projectComments[i].assign_to_id === localStorage.getItem('user_id'))) {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  /**
   * To set commented for open comment node
   * @param data (taxonomy data)
   */
  setOpenCommentForAllNode(data: any) {
    if (data) {
      if (data.children.length > 0) {
        data.isCommented = (data.open_comment_count && data.open_comment_count > 0) ? true : false;
        for (let i = 0; i < data.children.length; i++) {
          this.setOpenCommentForAllNode(data.children[i]);
        }
      } else {
        data.isCommented = (data.open_comment_count && data.open_comment_count > 0) ? true : false;
      }
    }
  }

  /**
   * On project comment's action
   */
  onCommentAction(event: any) {
    const status = event.toLowerCase();

    switch (status) {
      case 'create_notes':
      case 're-open':
        this.selectedNode['isCommented'] = true;
        this.selectedNode['open_comment_count'] = this.selectedNode['open_comment_count'] ?
          (this.selectedNode['open_comment_count'] + 1) : 1;
        break;
      case 'no_comments': // get called when api return blank array for comments
        this.selectedNode['isCommented'] = false;
        this.selectedNode['open_comment_count'] = 0;
        break;
      case 'delete_notes':
      case 'closed':
        this.selectedNode['open_comment_count'] = this.selectedNode['open_comment_count'] ?
          (this.selectedNode['open_comment_count'] - 1) : 0;
        break;
    }
  }

  onChangeCommentAssigned() {
    if (this.assignedToMe === 'assignedToMe') {
      this.getProjectComments(this.projectId); // comment icon setting for 'comments assigned to me'
    } else if (this.assignedToMe === 'complete') {
      this.sharedService.projectTreeViewEvent.next('inactive');
    } else { // comment icon setting for 'open commented node'
      this.sharedService.projectTreeViewEvent.next('active');
      if (this.isAllNode === false) {
        for (let i = 0; i < this.taxonomyData.length; i++) {
          this.setOpenCommentForAllNode(this.taxonomyData[i]);
        }
      } else {
        for (let i = 0; i < this.taxonomyData.children.length; i++) {
          this.setOpenCommentForAllNode(this.taxonomyData.children[i]);
        }
      }
    }

  }
  openCommentSec() {
    if (this.projectNotesComponent) {
      this.projectNotesComponent.showHideNav(true);
    }
  }

  onArrowClicked(event) {
    const elem = event.target;
    if (elem.getAttribute('aria-expanded') === 'true') {
      event.target.parentElement.parentElement.parentElement.classList.remove('active');
    } else {
      event.target.parentElement.parentElement.parentElement.classList.add('active');
    }
  }

  /**
   * To get items compliance report
   */
  getItemsComplianceReport() {
    if (this.projectId) {
      this.itemComplianceReports = [];
      const url = GlobalSettings.ITEM_COMPLIANCE_REPORT + '/' + this.projectId + '/2';
      this.service.getServiceData(url).then((res: any) => {
        this.itemComplianceReports = res;
        // if (this.isAllNode === false) {
        //   for (let i = 0; i < this.taxonomyData.length; i++) {
        //     this.setComplianceStatusForAllNode(this.taxonomyData[i]); // setting compliance icon for project taxonomy
        //   }
        // } else {
        //   for (let i = 0; i < this.taxonomyData.children.length; i++) {
        //     this.setComplianceStatusForAllNode(this.taxonomyData.children[i]); // setting compliance icon for complete taxonomy
        //   }
        // }
      }).catch((ex) => {
        console.log('getItemsComplianceReport ', ex);
      });
    }
  }

  redirectToProjectList() {
    let path;
    if (localStorage.getItem('project_type') === '2') {
      path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PACING_GUIDE_LIST;
    } else {
      path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_LIST;
    }
    this.router.navigate([path]);
  }

  /**
   * To set compliance status for each taxonomy node
   */
  setComplianceStatusForAllNode(data: any) {
    if (data) {
      if (data.children.length > 0) {
        data.compStatus = this.getComplianceStatusForItem(data.id); // 'compStatus' will hold compliance status value
        for (let i = 0; i < data.children.length; i++) {
          this.setComplianceStatusForAllNode(data.children[i]);
        }
      } else {
        data.compStatus = this.getComplianceStatusForItem(data.id);
      }
    }
  }

  /**
   * To get compliance status value for respective item id (status values: 0,1,2)
   * @param itemId
   */
  getComplianceStatusForItem(itemId) {
    if (itemId) {
      for (let i = 0; i < this.itemComplianceReports.length; i++) {
        if (itemId === this.itemComplianceReports[i].item_id) {
          return this.itemComplianceReports[i].status;
        }
      }
    }
    return null;
  }

  getPacingGuideNodes() {
    const url = GlobalSettings.PACING_GUIDES_AUTHORING + '/' + this.projectId;
    this.treeDataService.getTreeData(url, true, Utils.EXPAND_LEVEL, true, true).then((res: any) => {
      if (res) {
        Utils.sortData(res.parsedTreeNodes.children);
        this.pacingGuideNodes = res.parsedTreeNodes.children;
        Utils.changePacingGuideIds(this.pacingGuideNodes, '');
        this.taxonomyData = this.pacingGuideNodes;
        if (this.taxonomyData.length > 0) {
          this.tempTaxonomyData = this.taxonomyData;
          if (this.taxonomyData[0]) {
            if (this.taxonomyData[0].children.length > 0) {
              if (this.taxonomyData[0].children[0]['isOrphanLabel']) { // skipping orphan node lable if any
                this.firstNode = this.taxonomyData[0].children[0].children[0];
              } else {
                this.firstNode = this.taxonomyData[0].children[0];
              }
              this.firstItemNode = this.firstNode;
              if (this.firstNode['is_document'] === 1) {
                this.firstItemNode = this.loadNextNode(this.firstNode);
              }
              this.lastNode = this.findLastNode(this.taxonomyData);
              if (this.itemId) {
                this.selectedNode['id'] = this.itemId;
                this.getParentNodeObject(this.selectedNode['id']);
                this.selectedNode = this.parentNode;
              } else {
                this.selectedNode = this.taxonomyData[0].children[0];
              }
              // const id = this.selectedNode.item_id ? this.selectedNode.item_id : this.selectedNode['id'];
              // this.getSelectedTreeItemDetail(id);

            }
          }
        }
        if (this.assignedToMe === 'assignedToMe') {
          this.getProjectComments(this.projectId); // comment icon setting for 'comments assigned to me'
        } else {
          for (let i = 0; i < this.taxonomyData.length; i++) {
            this.setOpenCommentForAllNode(this.taxonomyData[i]); // comment icon setting for 'open commented node'
          }
        }
        this.getItemsComplianceReport(); // For compliance icon setting

        setTimeout(() => {
          let clickableNode: any;
          if (this.taxonomyData[0]) {
            if (this.taxonomyData[0].children[0]['isOrphanLabel']) {
              clickableNode = this.taxonomyData[0].children[0].children[0];
              this.expandTreeTillSelectedNode(clickableNode);
            } else {
              this.clickSelectedNode();
            }
          }
        }, 500);
        setTimeout(() => {
          this.projectAuthHeightCalculation();
          this.resizeRightPanelWidth(19);
        }, 100);
      } else {
        this.sendMsgNoSuchProject();
      }

    }).catch((ex) => {
      console.log('Error ', ex);
    });
  }

  /**
   * On reorder taxonomy update modal open
   */
  onReorderModalOpen() {
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.document_id = this.document_id || this.projectDetail['document_id'];
  }

  /**
   * On reorder taxonomy update modal close
   */
  onReorderModalClose() {
    this.treeRearrenged = false;
    this.document_id = null;
    this.assignedToMe = 'all';
    this.getProjectAssociatedNodes();
  }

  // On tree data re-ordered
  onTreeUpdate(event) {
    this.treeRearrenged = event.reArrenged;
  }

  onCancel(event) {

  }

  mouseLeave() {
    console.log('mouse leave');
  }

  onSubmit() {
    console.log('onSubmit');
  }

  saveUpdatedTaxonomy() {
    this.onUpdate();
  }


  onUpdate() {
    this.getProjectData(true);
  }

  update() {
    this.postAPIData = [];
    this.old_itemsData = [];
    // let list_enumeration = 1;
    let sequence_number = 1;
    // let arraySequence = [];
    if (this.tree) {
      this.taxonomyData = this.tree.getTreeData();
    }
    this.taxonomyData.forEach(element => {
      this.createData(element, sequence_number, false);
      sequence_number++;
      // arraySequence.push(element.sequence_number);
    });

    // arraySequence.sort();
    // this.taxonomyData.forEach((element,index )=> {
    //   this.createData(element, arraySequence[index], false);
    // });


    // let old_list_enumeration = 1;
    // let arraySequenceOriginal = [];

    let old_sequence_number = 1;
    this.taxonomyOriginalData[0]['children'].forEach(element => {
      this.createData(element, old_sequence_number, true);
      old_sequence_number++;
      // arraySequenceOriginal.push(element.sequence_number);
    });

    // arraySequenceOriginal.sort();
    // this.taxonomyOriginalData[0]['children'].forEach((element,index )=> {
    //   this.createData(element, arraySequenceOriginal[index], true);
    // });


    this.savingData = true;
    let body: any = null;

    body = {
      items: this.postAPIData,
      old_items: this.old_itemsData,
      type: 'project',
      project_id: this.projectId
    };

    this.service.postService(GlobalSettings.SET_TREE_HIERARCHY_ENHANCED + '/' + this.projectDetail.document_id, body).then((res) => {
      this.reorderComplete = true;
      this.taxonomyData = null;
      this.taxonomyOriginalData = null;
      this.savingData = false;
      this.taxonomyData = this.dragDropData;
      this.taxonomyOriginalData = JSON.parse(JSON.stringify(this.dragDropData));
      this.sharedService.sucessEvent.next({
        type: 'taxonomy_reorder_update'
      });
      this.getProjectAssociatedNodes();
      this.onCancelDragAndDrop();
    }).catch((ex) => {
      this.reorderComplete = true;
      this.savingData = false;
      this.onCancelDragAndDrop();
      console.log('Set Tree ', ex);
    });
  }

  createData(element, list_enumeration, isOld) {
    // Setting empty parent id of a node if node contains dummy parent id
    if (element.parent_id === Utils.DUMMY_ID_FOR_ORPHANLABEL) {
      element.parent_id = '';
    }
    const obj = {
      item_id: element.id,
      parent_id: element.parent_id ? element.parent_id : '',
      id: element.id,
      list_enumeration: '' + list_enumeration,
      sequence_number: '' + list_enumeration
    };
    if ((obj.id !== obj.parent_id) && (!element.isOrphanLabel)) {
      if (!isOld) {
        this.postAPIData.push(obj);
      } else {
        this.old_itemsData.push(obj);
      }
    }

    // let arraySequence = [];


    if (element.children && element.children.length > 0) {
      for (let index = 0; index < element.children.length; index++) {
        const child = element.children[index];
        // arraySequence.push(child.sequence_number);
        this.createData(child, index + 1, isOld);
      }
    }

    // arraySequence.sort();

    // for (let index = 0; index < element.children.length; index++) {
    //   const child = element.children[index];
    //   this.createData(child, arraySequence[index], isOld);
    // }

  }

  onCancelDragAndDrop() {
    this.reOrder = false;
    this.tree.cancleReorder();
    this.taxonomyData = null;
    this.getProjectData();
    // this.taxonomyData = JSON.parse(JSON.stringify(this.taxonomyOriginalData));
  }

  expendSelectedNode() {

    setTimeout(() => {
      if (this.tree) {
        // this.treeAccordianComponet.expendNode(this.treeNode.id);
        // if (!this.urlParams.parentId) {
        //   this.treeAccordianComponet.onNodeSelected(this.treeNode);
        // }
        if (this.urlParams.parentId && this.urlParams.itemId) {
          const node = {
            'id': this.urlParams.itemId,
            'is_document': 0,
            'parent_id': this.urlParams.parentId
          };
          this.expandTreeTillSelectedNode(node);
        }
      }
    }, 1000);
  }

  expandTreeTillSelectedNode(node) {
    this.parentNodeIds = [];
    if (localStorage.getItem('project_type') === '2') { // pacing guide authoring
      // Utils.expandTreeTillSelectedNode(node, this.taxonomyData[0]);
      this.callExpandTreeFromWorker(node);
    } else { // project authoring
      if (this.isAllNode === false && this.taxonomyData) {
        for (const dataObj of this.taxonomyData) {
          Utils.expandTreeTillSelectedNode(node, dataObj);
        }
      } else if (this.isAllNode === true && this.taxonomyData && this.taxonomyData.children) {
        for (const treeData of this.taxonomyData.children) {
          Utils.expandTreeTillSelectedNode(node, treeData);
        }
      }
    }
    if (this.tree) {
      this.tree.onNodeSelected(node);
    }
    setTimeout(() => {
      const id = node['id'] ? node['id'] : node['item_id'];
      if (document.getElementById(id + '-node')) {
        document.getElementById(id + '-node').focus();
      }
    }, 100);
  }

  /**
   * To update taxonomy data after add/delete from project authoring tree
   * @param selectedNode (node to be added/deleted)
   * @param taxonomy (taxonomy builder screen data)
   * @param operation (opration name: add/delete)
   */
  updateTaxonomyAfterTreeNodeUpdate(selectedNode, taxonomy, operation) {
    taxonomy.forEach(element => {
      this.iterateToUpdateTaxonomy(element, selectedNode, operation);
    });
  }
  // iterating taxonomy for updating according to add/delete
  iterateToUpdateTaxonomy(node, selectedNode, operation) {
    if (node['id'] === selectedNode['parent_id']) {
      if (operation === 'add') { // If a node is added, push it's parent's children
        if (!node['children']) {
          node['children'] = [];
        }
        node['children'].push(selectedNode);
        node['children'] = _.uniqBy(node['children'], function (id) {
          return id;
        });
      } else if (operation === 'delete') { // If a node is deleted, splice it from its parent's children list
        const index = this.findIndexInList(this.selectedNode, node['children']);
        node['children'].splice(index, 1);
      }
    } else {
      if (node.children.length > 0) {
        node.children.forEach(element => {
          this.iterateToUpdateTaxonomy(element, selectedNode, operation);
        });
      }
    }
  }

  /* Node navigation function */
  nodeNavigation(event: any) {
    if (event.navigation === 'next') {
      this.onTreeNavigateNextClick();
    } else if (event.navigation === 'previous') {
      this.onTreeNavigatePreviousClick();
    }
  }

  initializeSelectednode() {
    this.selectedNode = {
      item_id: '',
      id: '',
      isFirst: true,
      is_document: 1
    };
  }

  /* open taxonomy in new tab */
  navigateToTaxonomy(node: any, associationId) {
    let url = '';
    const urlPrefix = GlobalSettings.BASE_URL.replace('server/', '#').replace('api.', '') + this.service.getOrgDetails();
    if (node) {
      const docId = node.document.document_id;
      const parentId = node.parent_id;
      const nodeId = node.item_id;
      const projId = node.project_id;
      const itemAssoId = node.item_association_id; // for pacing guide
      if (node.project_type === 2 && projId) { // If pacing guide and project id exists
        if (parentId && nodeId && itemAssoId) { // If pacing guide's item id found
          url = urlPrefix + '/' + Utils.ROUTE_PACING_GUIDE_DETAIL + '/' + projId + '/' + parentId + '/' +
            nodeId + '/' + itemAssoId + '/search';
        } else {
          url = urlPrefix + '/' + Utils.ROUTE_PACING_GUIDE_DETAIL + '/' + projId + '/search';
        }
      } else if (docId) { // If not pacing guide and document id exists
        if (nodeId && parentId) {
          url = urlPrefix + '/' + Utils.ROUTE_TAXONOMY_DETAIL + '/' + docId + '/' + parentId + '/' + nodeId + '/search';
        } else if (parentId) {
          url = urlPrefix + '/' + Utils.ROUTE_TAXONOMY_DETAIL + '/' + docId + '/' + parentId;
        } else {
          url = urlPrefix + '/' + Utils.ROUTE_TAXONOMY_DETAIL + '/' + docId;
        }
      } else { // If nothing exists, set empty url and disabled navigation
        url = '';
      }
    }
    return url;
  }

  openNewTab(event, id) {
    // event.preventDefault();
    // if (document.getElementById(id)) {
    //   document.getElementById(id).dispatchEvent(new MouseEvent('click', {
    //     ctrlKey: true,
    //     cancelable: true
    //   }));
    // }
  }

  openInNewTab(node, id) {
    const currentUrl = sessionStorage.getItem('current_url');
    const frontFaceUrl = sessionStorage.getItem('frontface_url');
    sessionStorage.clear();
    const url = this.navigateToTaxonomy(node, id);
    var win = window.open(url, '_blank');
    sessionStorage.setItem('current_url', currentUrl);
    sessionStorage.setItem('frontface_url', frontFaceUrl);
    win.focus();
    event.preventDefault();
  }

  /**
   * To decide whether to show Node's full statement/human coding scheme or Uri or empty
   * @param node (destination node from association list)
   */
  isNodeOrUri(node: any) {
    let showItem = '';

    if (node) {

      if (node.is_external === 1 || (node.full_statement === '' && node.human_coding_scheme === '')) {
        showItem = 'uri';
      } else {
        if ((node.full_statement || node.human_coding_scheme) && node.item_id) {
          showItem = 'node';
        } else if (node.destination_node_url) {
          showItem = 'uri';
        } else if (node.node_type_title && (node.node_type_title.toLowerCase().trim() === 'document')) {
          showItem = 'node';
        } else {
          showItem = '';
        }
      }

    }

    return showItem;
  }


  /* --------- Functionality to switch and set current tab start --------- */

  onTabSelected(tab) {
    this.currentTab = tab;
    if (this.currentTab === 'search taxonomy') {
      this.sharedService.faqEvent.next({
        name: 'association_search'
      });
    } else {
      this.sharedService.faqEvent.next({
        name: 'association_browse'
      });
    }
  }

  /* --------- Functionality to switch and set current tab end --------- */


  /* --------- Functionality on selecting taxonomy from create association modal start --------- */

  setSelectedStep(event) {
    this.step = event.step;
    if (event.selectedTaxonomy) {
      this.selectedTaxonomy = event.selectedTaxonomy;
    }
    if (this.step === 1) {
      this.disableAssociation = true;
    }
  }

  /* --------- Functionality on selecting taxonomy from create association modal end --------- */


  /* --------- Functionality to set details captured from associatin modal start --------- */

  associationModalObj(selectedNodeIds) {
    this.selectedNodeIds = selectedNodeIds.node;
    this.selectedAssociationType = selectedNodeIds.associationType;
    this.requiredFullHierarchyFlag = selectedNodeIds.requiredFullHierarchyFlag;
    this.relations = selectedNodeIds.relations;
    this.selectedTaxonomyData = selectedNodeIds.selectedTaxonomyData;
    this.selectedDocumentId = selectedNodeIds.documentId;
    if (this.selectedNodeIds && this.selectedNodeIds.length > 0 && this.selectedAssociationType) {
      if (this.selectedAssociationType.type_name === 'isParentOf' && this.optionsToCopySaved) {
        this.disableAssociation = false;
      } else if (this.selectedAssociationType.type_name === 'isParentOf' && !this.optionsToCopySaved) {
        this.disableAssociation = true;
      } else if (this.selectedAssociationType.type_name !== 'isParentOf') {
        this.disableAssociation = false;
        this.optionsToCopySaved = false;
        this.copy = false;
        this.copyAssociation = false;
      }

      // else {
      //   this.disableAssociation = true;
      //   this.optionsToCopySaved = false;
      //   this.copy = false;
      // }
    } else {
      this.disableAssociation = true;
    }
  }

  /* --------- Functionality to set details captured from associatin modal end --------- */


  /* --------- Functionality to set current node captured from tree while adding associations, hit add association modal start --------- */

  addAssociation(originNode) {
    this.openAssociations(false);
    this.originNodeId = originNode.id;
    this.originNode = originNode;
    this.currentTab = this.tabItems[0].toLowerCase(); // selects the current tab as default view
    this.associationModalOpen = true;
    if (this.acmtSearch) {
      this.acmtSearch.resetSearch(true);
    }
    if (this.associationSettings) {
      this.associationSettings.setOriginNodeId(this.originNodeId);
    }
    this.sharedService.faqEvent.next({
      name: 'association_search'
    });
  }

  /* --------- Functionality to set current node captured from tree while adding associations, hit add association modal  end --------- */


  /* --------- Functionality to create association start from add associatin modal start --------- */

  createAssociations() {
    this.loaderOnAssociation = true;
    if (this.selectedAssociationType.type_name !== 'isParentOf') {
      this.associationLoaderData = 'Creating Associations';
      const body = {
        origin_node_id: this.originNodeId,
        destination_node_ids: this.selectedNodeIds,
        project_type: 1,
        association_type: this.selectedAssociationType.type_id,
        project_id: this.projectId
      };
      const url = GlobalSettings.CREATE_CFITEM_CASE_ASSOCIATION;
      this.service.postService(url, body).then((res: any) => {
        this.addedAssociationCount = this.addedAssociationCount + res[0].destination_node.length;
        this.sharedService.sucessEvent.next({
          type: 'created_association'
        });
        this.loaderOnAssociation = false;
        this.getSelectedTreeItemDetail(this.originNodeId);
      }).catch((ex) => {
        this.loaderOnAssociation = false;
        this.sharedService.sucessEvent.next({
          type: 'error_association'
        });
      });
    } else {
      this.associationLoaderData = 'Copying Nodes';
      this.appendChild();
    }
  }

  /* --------- Functionality to create association start from add associatin modal end --------- */


  /* --------- Functionality on close of association modal start --------- */

  closeAssociationModal($event) {
    this.addedAssociationCount = 0;
    this.copyCount = 0;
    this.step = 1;
    if (this.selectedAssociationType && this.selectedAssociationType.type_name !== 'isParentOf' &&
      this.selectedAssociationType.type_name !== 'isChildOf') {
      this.getSelectedTreeItemDetail(this.originNodeId);
      // console.log('other ####');
    } else if (this.selectedAssociationType && this.selectedAssociationType.type_name === 'isChildOf') {
      // console.log('isChildOf ####');
      // this.getProjectAssociatedNodes();
      this.reloadTree(this.selectedNode.id);
    } else {
      // console.log('isParentOf ####');
      this.itemId = this.originNodeId;
      // const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_AUTHORING + '/';
      // this.router.navigate([path, this.projectId, this.originNodeId, this.originNodeId]);
      this.reloadTree(this.originNodeId);
    }
    this.optionsToCopySaved = false;
    this.copy = false;
    this.copyAssociation = false;
    this.associationModalClose = true;
    this.associationModalOpen = false;
    if (this.acmtSearch) {
      this.acmtSearch.resetSearch(true);
    }
    this.sharedService.faqEvent.next({
      name: 'project_authoring'
    });
  }

  /* --------- Functionality on close of association modal end --------- */


  /* --------- Functionality on close of inner popup for slecting option for copy start --------- */


  cancleOptionsToCopy(event) {
    this.isCopy = false;
    this.isCopyAssociation = false;
    // this.copy = false;
    // this.disableAssociation = true;
    // this.optionsToCopySaved = false;
  }

  /* --------- Functionality on close of inner popup for slecting option for copy end --------- */


  /* --------- Functionality on save of inner popup for slecting option for copy start --------- */

  optionsToCopy() {
    this.optionsToCopySaved = true;
    this.saveButtonClickedForCopy = true;
    if (this.selectedAssociationType.type_name === 'isParentOf' && this.optionsToCopySaved && this.selectedNodeIds &&
      this.selectedNodeIds.length > 0) {
      this.disableAssociation = false;
    } else {
      this.disableAssociation = true;
    }

    this.copy = this.isCopy;
    this.copyAssociation = this.isCopyAssociation;

    if (this.isCopy && !this.isCopyAssociation && this.saveButtonClickedForCopy) {
      this.displayText = 'Settings: Exact Match with Original';
    } else if (!this.isCopy && this.isCopyAssociation && this.saveButtonClickedForCopy) {
      this.displayText = 'Settings: Association copied';
    } else if (this.isCopy && this.isCopyAssociation && this.saveButtonClickedForCopy) {
      this.displayText = 'Settings: Association copied & Exact Match with Original';
    } else {
      this.displayText = '';
      this.copy = false;
      this.copyAssociation = false;
    }

    if (this.requiredFullHierarchyFlag === 0) {
      this.displayOption = 'Append';
    } else if (this.requiredFullHierarchyFlag === 1) {
      this.displayOption = 'Append with child nodes';
    }
    this.selectTaxonomy.taxonomyDetailsHeightCalculation(-59);
  }

  /* --------- Functionality on save of inner popup for slecting option for copy end --------- */


  /* --------- Functionality to create hierarchy of children and POST service for copy start --------- */

  appendChild() {
    this.nodeHierarchy = [];
    if (this.relations) {
      this.relations.forEach(relation => {
        relation.present = 0;
      });

      this.selectedNodeIds.forEach(nodeId => {
        this.relations.forEach(relation => {
          // relation.present = 0;
          // relation.item_id = relation.child_id;
          if (relation.child_id) {
            delete Object.assign(relation, {
              ['item_id']: relation.child_id
            }).child_id;
          }
          if ((nodeId === relation.item_id || nodeId === relation.parent_id) && relation.present === 0) {
            relation.present = 1;
            this.nodeHierarchy.push(relation);
          }
        });
      });
      this.generateObj(this.relations, this.nodeHierarchy);
    }

    if (this.currentTab === 'search taxonomy' && this.step !== 2) {
      this.nodeHierarchy = [];
    }
    const body = {
      origin_node_id: this.originNodeId,
      document_id: this.selectedDocumentId ? this.selectedDocumentId : this.projectId,
      node_ids_in_csv: this.selectedNodeIds,
      node_hierarchy: this.requiredFullHierarchyFlag === 1 ? this.nodeHierarchy : [],
      project_type: 1,
      required_full_hierarchy_copy: this.requiredFullHierarchyFlag,
      project_id: this.projectId,
      isCopy: this.copy ? 1 : 0,
      isCopyAsso: this.copyAssociation ? 1 : 0,
    };
    const url = GlobalSettings.APPEND_NODES;
    this.service.postService(url, body).then((res: any) => {
      if (this.selectedAssociationType.type_name !== 'isParentOf') {
        this.addedAssociationCount = this.addedAssociationCount + this.selectedNodeIds.length;
      } else {
        this.copyCount = this.copyCount + this.selectedNodeIds.length;
      }
      this.sharedService.sucessEvent.next({
        type: 'copy_nodes'
      });
      if (document.getElementById('removeAppendChild')) {
        document.getElementById('removeAppendChild').click();
      }
      this.loaderOnAssociation = false;
    });

    // console.log('nodeHierarchy', JSON.stringify(this.nodeHierarchy));
  }


  generateObj(relations, intermdediateAddedChild) {
    const tempChild = [];
    relations.forEach(relation => {
      intermdediateAddedChild.forEach(hierarchyChild => {
        if (hierarchyChild.item_id === relation.parent_id && relation.present === 0) {
          relation.present = 1;
          this.nodeHierarchy.push(relation);
          tempChild.push(relation);
        }
      });
    });

    if (tempChild.length > 0) {
      this.generateObj(relations, tempChild);
    }
  }

  /* --------- Functionality to create hierarchy of children and POST service for copy end --------- */

  change() {
    // this.copyStateChange = true;
    this.saveButtonClickedForCopy = false;
  }

  settingsModalOpen(event: any) {
    // console.log('copyAssociation', this.copyAssociation);
    this.isCopy = this.copy;
    this.isCopyAssociation = this.copyAssociation;
  }


  /* --------- Functionality on click of remove selection for append child option start --------- */

  removeSelectionForCopy() {
    this.optionsToCopySaved = false;
    this.disableAssociation = true;
    this.copy = false;
    this.copyAssociation = false;
  }

  /* ---------POC TO SHOW EXTERNAL ASSOCIATION OF NODES --------- */

  addAssociationsToTreeNode(associationArray: any) {
    if (this.selectedNode.is_document !== 1 && this.selectedNode.children && !this.selectedNode.associationAdded) {
      associationArray.forEach(element => {
        if (element.origin_node.parent_id !== element.destination_node.item_id) {
          element.destination_node.isAssociationNode = true;
          element.destination_node.children = [];
          element.destination_node.level = this.selectedNode.level + 1;
          element.destination_node.id = element.destination_node.item_id;
          element.destination_node.association_type = element.association_type.display_name;
          this.selectedNode.children.push(element.destination_node);
          this.selectedNode.associationAdded = true;
        }
      });
    }
  }
  /* --------- Functionality to add associations as children of tre node in view only ends --------- */

  assModalClosed(evt) {
    // const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_AUTHORING + '/';
    // this.router.navigate([path, this.projectId, this.selectedNode.item_id, this.selectedNode.item_id]);
    // this.getProjectAssociatedNodes();
    // this.reloadTree(this.selectedNode.id);
    if (this.selectedAssociationType && this.selectedAssociationType.type_name !== 'isChildOf') {
      this.getSelectedTreeItemDetail(this.selectedNode.id);
    } else {
      this.reloadTree(this.selectedNode.id);
    }
  }

  /* --------- Functionality to update breadcrumb on edit node start --------- */


  updateBreadCrumb(res) {
    let data;
    if (res.hasOwnProperty('title')) {
      data = res.title;
    } else {
      data = res.human_coding_scheme && res.human_coding_scheme.length > 0 ? res.human_coding_scheme : res.full_statement;
    }
    let index = 0;
    if (this.selectedNode && this.selectedNode.breadCrumbArray && this.selectedNode.breadCrumbArray[0].trim() === 'Orphan node') {
      index = this.selectedNode.level - 2;
    } else {
      index = this.selectedNode.level - 1;
    }
    if (this.selectedNode && this.selectedNode.breadCrumbArray) {
      this.selectedNode.breadCrumbArray[index] = data;
    }
    if (this.selectedNode.children && this.selectedNode.children.length > 0) {
      this.updatedChildBreadCrumb(this.selectedNode, index, data);
    }


  }

  updatedChildBreadCrumb(currentNode, index, data) {
    currentNode.children.forEach(node => {
      if (node.breadCrumbArray) {
        node.breadCrumbArray[index] = data;
      }
      if (node.children.length > 0) {
        this.updatedChildBreadCrumb(node, index, data);
      }
    });
  }

  /* --------- Functionality to update breadcrumb on edit node end --------- */

  reloadTree(nodeId) {
    this.taxonomyData = null;
    this.reLoadTree = true;
    this.itemId = nodeId;
    this.getProjectAssociatedNodes();

  }

  // Authoring page height calculation
  projectAuthHeightCalculation() {
    const windowHeight = window.innerHeight;
    let titleHeight = 0;
    const extraHeight = 1;
    if (document.getElementsByClassName('authoring-header') && document.getElementsByClassName('authoring-header')[0]) {
      titleHeight = document.getElementsByClassName('authoring-header')[0].clientHeight;
    }
    const panelHeight = windowHeight - (titleHeight + extraHeight);
    if (document.getElementById('tree_container_id')) {
      document.getElementById('tree_container_id').style.height = panelHeight + 'px';
    }
    if (document.getElementById('right_container_id')) {
      document.getElementById('right_container_id').style.height = panelHeight + 'px';
    }
  }

  // resize right panel's navigation button static div tag based on right pane resize width
  resizeRightPanelWidth(size = 0) {
    let panelWidth = 0;
    if (document.querySelector('#right_container_id')) {
      panelWidth = document.querySelector('#right_container_id').clientWidth - size;
    }
    if (document.getElementById('right-preview-panel')) {
      document.getElementById('right-preview-panel').style.width = panelWidth + 'px';
    }
  }

  openAssociations(val) {
    this.openAssociationList = val;
  }

  openSettings(node) {
    if (this.associationSettings) {
      this.associationSettings.showHideNav(true);
      this.associationSettings.setData(node);
    }
  }

  captureSearchEvent(event) {
    this.isFilterApplied = event.isFilterApplied;
    this.searchResultList = (event.filterData);
    this.taxonomyData = event.treeData;
    if (this.isFilterApplied) {
      this.isReset = false;
    }
    if (!this.isFilterApplied && !this.isReset) {
      // this.getProjectAssociatedNodes();
      this.isReset = true;
    }
  }

  callExpandTreeFromWorker(node) {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker('../../treeService.worker', { type: 'module' });
    }
    if (this.worker) {
      this.worker.onmessage = ({ data }) => {
        this.taxonomyData = [];
        console.log('data received from worker in taxonomy', data);
        if (data) {
          this.taxonomyData.push(data.taxonomyData);
        }
      };
      this.worker.postMessage({
        data: this.taxonomyData[0],
        location: 'utils',
        parameters: {
          selectedNode: node,
          isMultipleCall: false
        }
      });
    }
  }

  setLoaderEvent(val) {
    this.isFirstSearchInitialise = val['isFirstTime'];
    // if (!this.initialiseSearch) {
    this.isSearching = val['isLoading'];
    // }
    this.initialiseSearch = false;
  }

}
