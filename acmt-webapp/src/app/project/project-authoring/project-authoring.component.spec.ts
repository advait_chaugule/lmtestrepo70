import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectAuthoringComponent } from './project-authoring.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { TreeDataService } from 'src/app/tree-data.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { ProjectMappedNodeService } from 'src/app/project-mapped-node.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AngularSplitModule } from 'angular-split';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

describe('ProjectAuthoringComponent', () => {
  let component: ProjectAuthoringComponent;
  let fixture: ComponentFixture<ProjectAuthoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectAuthoringComponent],
      imports: [RouterTestingModule, AngularSplitModule, FormsModule, HttpClientModule],
      providers: [
        CommonService,
        SharedService,
        TreeDataService,
        ProjectMappedNodeService,
        AuthenticateUserService,
        ConfirmationDialogService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectAuthoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
