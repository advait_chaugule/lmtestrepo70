import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {
  ProjectListComponent
} from './project-list/project-list.component';
import {
  ProjectDetailComponent
} from './project-detail/project-detail.component';
import {
  ProjectAuthoringComponent
} from './project-authoring/project-authoring.component';

const routes: Routes = [{
  path: 'list',
  component: ProjectListComponent
}, {
  path: 'list/:type',
  component: ProjectListComponent
}, {
  path: 'detail/:id',
  component: ProjectDetailComponent
}, {
  path: 'detail/:id/:type',
  component: ProjectDetailComponent
}, {
  path: 'authoring/:id',
  component: ProjectAuthoringComponent
}, {
  path: 'authoring/:id/:parentId/:itemId',
  component: ProjectAuthoringComponent
}, {
  path: 'authoring/:id/:itemId',
  component: ProjectAuthoringComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
