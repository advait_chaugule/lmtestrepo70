import {
  NgModule,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  ProjectRoutingModule
} from './project-routing.module';
import {
  ProjectListComponent
} from './project-list/project-list.component';
import {
  CreateProjectComponent
} from './create-project/create-project.component';
import {
  TreeviewModule
} from 'ngx-treeview';
import {
  ProjectDetailComponent
} from './project-detail/project-detail.component';
import {
  ProjectAuthoringComponent
} from './project-authoring/project-authoring.component';
import {
  ProjectNotesComponent
} from './projects-notes/projects-notes.component';

import {
  ProjectRequestModalComponent
} from './project-request-modal/project-request-modal.component';
import {
  AssociationComponent
} from './project-authoring/association/association.component';
import {
  SharedModule
} from '../shared/shared.module';
import { PublicModule } from '../shared/public.module';
import {
  ProjectDocumentComponent
} from './project-document/project-document.component';
import {
  Shared2Module
} from '../shared/shared2/shared2.module';
import { AddExemplarComponent } from './add-exemplar/add-exemplar.component';
import { AdditionalMetadataComponent } from './additional-metadata/additional-metadata.component';
import { DynamicFormFieldComponent } from '../common/dynamic-form-field/dynamic-form-field.component';
import { Shared3Module } from '../shared/shared3/shared3.module';
import { ProjectComplianceReportComponent } from './project-compliance-report/project-compliance-report.component';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';
@NgModule({
  imports: [
    ProjectRoutingModule,
    TreeviewModule,
    SharedModule,
    Shared2Module,
    Shared3Module,
    PublicModule,
    PreLoaderModule
  ],
  declarations: [
    ProjectListComponent,
    CreateProjectComponent,
    ProjectDetailComponent,
    ProjectAuthoringComponent,
    ProjectRequestModalComponent,
    ProjectDocumentComponent,
    ProjectComplianceReportComponent
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ProjectModule { }
