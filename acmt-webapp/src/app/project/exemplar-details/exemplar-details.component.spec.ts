import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExemplarDetailsComponent } from './exemplar-details.component';
import { SharedService } from 'src/app/shared.service';

describe('ExemplarDetailsComponent', () => {
  let component: ExemplarDetailsComponent;
  let fixture: ComponentFixture<ExemplarDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExemplarDetailsComponent],
      imports: [],
      providers: [SharedService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExemplarDetailsComponent);
    component = fixture.componentInstance;
    component.exemplarData = {};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
