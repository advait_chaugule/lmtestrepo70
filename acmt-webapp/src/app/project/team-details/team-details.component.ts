import {
  Component,
  OnInit,
  ViewChild,
  Input
} from '@angular/core';
import {
  Params,
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import {
  ProjectMappedNodeService
} from '../../project-mapped-node.service';
import {
  SharedService
} from '../../shared.service';
import {
  CustomValidators
} from 'ng2-validation';

import {
  FormGroup,
  FormBuilder,
  Validators

} from '@angular/forms';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import { AutoCompleteComponent } from 'src/app/common/auto-complete/auto-complete.component';
/*import {
  WalktroughService
} from '../../help/walkthrough/walktrough.service';
import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';*/

/// <reference path="../../..assets/js/common.d.ts">

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-details.component.html'
})
export class TeamDetailComponent implements OnInit {
  public user: USER = <USER>{};
  form: FormGroup;
  userList: any[] = [];
  roleList: any[] = [];
  projectUserList: any[];
  @ViewChild('myname', { static: false }) input;
  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;
  @ViewChild('autoCompleteRole', { static: false }) autoCompleteRole: AutoCompleteComponent;
  @ViewChild('autoCompleteEmail', { static: false }) autoCompleteEmail: AutoCompleteComponent;
  system_role: any;
  system_email: string;
  projectId: String;
  workflow_id = '';
  tempWorkFloeUserId: string;
  @Input() canAddUser = false;
  @Input() canEditUser = false;
  @Input() canDeleteUser = false;
  selectedUser = null;
  selectedRole = null;
  formValidation = false;
  listOfColumn = [{
    name: 'Email',
    propName: 'user_email',
    class: '',
    type: 'text'
  },
  {
    name: 'Role',
    propName: 'role_name',
    class: '',
    width: '15%',
    type: 'text'
  }
  ];
  optionList = [];
  preventEdit = false;
  // addnewuser = true;
  constructor(private acivatedRoute: ActivatedRoute,
    private router: Router,
    private service: CommonService,
    private mappedNodeService: ProjectMappedNodeService,
    private AuthenticateUser: AuthenticateUserService,
    private fb: FormBuilder,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    /*private tourService: NgxBootstrapProductTourService,
    private walkService: WalktroughService*/) {

    this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.setWorkflowId(event.workFlowId, event.projectId);
        }, 1000);

      }
    });

    this.sharedService.preventEdit.subscribe((event: any) => {
      if (event.preventEdit !== undefined) {
        this.preventEdit = event.preventEdit;
      }
    });

  }


  setWorkflowId(id, projectId) {
    this.workflow_id = id;
    this.projectId = projectId;
    this.getRoles();
  }


  ngOnInit() {
    this.optionList = [{
      name: 'Edit',
      type: 'editTeam',
      value: true,
      modal: '#add-user',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'deleteTeam',
      value: true,
      modal: '#accept-request',
      check: 'isDelete'
    }
    ];
    this.form = this.fb.group({
      // email: [null, Validators.compose([Validators.required, Validators.email])],
      email: [null, Validators.compose([Validators.required])],
      role: [null, Validators.compose([Validators.required])],
    });

    this.acivatedRoute.params.subscribe((params: Params) => {
      if (params['id'] !== undefined) {
        this.projectId = params['id'];
        // this.getAllOrganizationWorkflows();
        this.getUsers();
        this.getAllProjectUser();
      } else {
        //  this.router.navigate(['./app/projects']);
      }
    });
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('user_list'));*/
  }
  getAllProjectUser() {
    const url = GlobalSettings.GET_All_PROJECT_USER + '/' + this.projectId;
    this.service.getServiceData(url).then((res: any) => {
      this.projectUserList = res;
      this.projectUserList.forEach(user => {
        user.isEditable = this.canEditUser;
        user.isDelete = this.canDeleteUser;
        user.showOptions = (!user.isEditable && !user.isDelete) ? false : true;
      });
    }).catch((ex) => {

    });
  }

  // On click Menu option

  onOptionClicked(event) {
    switch (event.clickedOn) {
      case 'editTeam':
        this.onEditUser(event.data);
        break;
      case 'deleteTeam':
        this.deleteRole(event.data);
        break;
      default:
    }
  }


  // getAllOrganizationWorkflows() {
  //   const url = GlobalSettings.GET_WORKFLOW;
  //   this.service.getServiceData(url).then((res: any) => {

  //     this.workflow_id = res.WorkflowList.workflows[0].workflow_id;

  //     this.getRoles();

  //   }).catch((ex) => {

  //   });
  // }

  getUsers() {
    const url = GlobalSettings.GET_USERS;
    this.service.getServiceData(url).then((res: any) => {
      // this.userList = res.users;
      res.UserList.users.forEach(element => {
        if (element.is_active === 1) {
          this.userList.push(element);
        }
      });
    }).catch((ex) => {

    });
  }

  getRoles() {
    const url = GlobalSettings.GET_All_WORKFLOW_ROLES + '/' + this.workflow_id;
    this.service.getServiceData(url).then((res: any) => {
      this.roleList = res;
    }).catch((ex) => {

    });
  }
  deleteRole(userData) {

    this.dialogService.confirm('Confirm', 'Do you want to delete the user?')
      .then((confirmed) => {
        if (confirmed) {
          const url = GlobalSettings.UNASSIGN_PROJECT_USER + '/' + this.projectId;
          const tempObj = {
            user_id: userData.user_id,
            workflow_stage_role_id: userData.workflow_stage_role_id
          };

          this.service.postService(url, tempObj).then((res: any) => {
            this.getAllProjectUser();
            this.sharedService.sucessEvent.next({
              type: 'delete_user'
            });
          }).catch(ex => {
            console.log('err', ex);
          });
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });


  }

  selectRole(roleId) {
    this.user.workflow_stage_role_id = roleId;

    // tslint:disable-next-line:max-line-length
    if (this.user.workflow_stage_role_id === undefined || this.user.user_id === undefined || this.user.workflow_stage_role_id === 'null' || this.user.user_id === 'null'
      || this.user.workflow_stage_role_id === null || this.user.user_id === null) {
      this.formValidation = false;
    } else {
      this.formValidation = true;
    }
    console.log('role id ' + this.user.workflow_stage_role_id + 'userID ' + this.user.user_id + this.formValidation);
  }
  selectUserId(userId) {
    this.user.user_id = userId;

    // tslint:disable-next-line:max-line-length
    if (this.user.workflow_stage_role_id === undefined || this.user.user_id === undefined || this.user.workflow_stage_role_id === 'null' || this.user.user_id === 'null'
      || this.user.workflow_stage_role_id === null || this.user.user_id === null) {
      this.formValidation = false;
    } else {
      this.formValidation = true;
    }
    console.log('role id ' + this.user.workflow_stage_role_id + 'userID ' + this.user.user_id + this.formValidation);
  }

  onSubmit() {
    const url = GlobalSettings.ASSIGN_PROJECT_USER + '/' + this.projectId;

    const tempObj = {
      user_id: this.user.user_id,
      workflow_stage_role_id: this.user.workflow_stage_role_id
    };

    // if (this.user.user_id === undefined) {
    this.service.postService(url, tempObj).then((res: any) => {
      this.getAllProjectUser();
      this.onCancel();
      if (res[0].user_exists) {
        this.sharedService.sucessEvent.next({
          type: 'user_exists'
        });
      } else {
        this.sharedService.sucessEvent.next({
          type: 'create_user'
        });
      }
    }).catch(ex => {
      console.log('err', ex);
    });

  }
  onCancel() {
    this.formValidation = false;
    // this.form.reset();
    if (this.user.user_name) {
      console.log('True');
      this.system_email = '';
      this.system_role = { name: 'Select Role' };
    } else {
      console.log('False');
      this.form.reset();
    }
    this.clearAutoComplete();
  }


  onEditUser(data) {
    // console.log('#' + JSON.stringify(data) + marker);
    if (data && data !== undefined) {

      // this.input.nativeElement.click();
      const temp = JSON.parse(JSON.stringify(data));
      this.user = temp;
      this.system_email = temp.user_email;
      this.system_role = temp;
      this.tempWorkFloeUserId = this.user.workflow_stage_role_id;

      console.log('this.system_email' + this.system_email + 'this.system_role' + this.system_role);
    } else {


      this.user = <USER>{};
      // this.system_email = null;
      this.system_role = { name: 'Select Role' };
      this.system_email = 'Select User';
    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
  }



  mouseLeave() {
    // this.system_email = '';
    // this.system_role = { name: 'Select Role' };
    this.getAllProjectUser();
  }

  unassignProjectUser() {
    const url = GlobalSettings.UNASSIGN_PROJECT_USER + '/' + this.projectId;
    const tempObj = {
      user_id: this.user.user_id,
      workflow_stage_role_id: this.tempWorkFloeUserId,
    };

    this.service.postService(url, tempObj).then((res: any) => {
      this.changeUserStatus();
      this.sharedService.sucessEvent.next({
        type: 'update_user'
      });
    }).catch(ex => {
      console.log('err', ex);
    });
  }

  changeUserStatus() {
    const url = GlobalSettings.ASSIGN_PROJECT_USER + '/' + this.projectId;

    const tempObj = {
      user_id: this.user.user_id,
      workflow_stage_role_id: this.user.workflow_stage_role_id
    };

    // if (this.user.user_id === undefined) {
    this.service.postService(url, tempObj).then((res: any) => {
      this.getAllProjectUser();
      this.onCancel();
    }).catch(ex => {
      console.log('err', ex);
    });
  }

  onClickedOutside(e, type: string) {
    if (type === 'autoCompleteRole') {
      this.autoCompleteRole.openPanel(false);
    } else if (type === 'autoCompleteEmail') {
      this.autoCompleteEmail.openPanel(false);
    } else {
      this.autoComplete.openPanel(false);
    }
  }

  clearAutoComplete() {
    if (this.autoCompleteRole) {
      this.autoCompleteRole.clearSelection();
    }
    if (this.autoCompleteEmail) {
      this.autoCompleteEmail.clearSelection();
    }
    if (this.autoComplete) {
      this.autoComplete.clearSelection();
    }
  }

}
