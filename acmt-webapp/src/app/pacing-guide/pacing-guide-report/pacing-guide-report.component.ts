import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-pacing-guide-report',
  templateUrl: './pacing-guide-report.component.html',
  styleUrls: ['./../../taxonomy/taxonomy-compliance-report/taxonomy-compliance-report.component.scss']
})
export class PacingGuideReportComponent implements OnInit, OnChanges {

  @Input() taxonomyData;

  listOfColumn = [ // Table column property
    {
      name: 'Name',
      propName: 'title',
      class: '',
      type: 'link',
      redirect: true,
      loadPage: true
    }
  ];
  menuList = [
    {
      id: 1,
      title: 'Coverage Report'
    }
  ];
  selectedMenu = this.menuList[0];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    setTimeout(() => {
      reportTabHeightCalculation();
    }, 50);
  }

  onMenuSelected(event: any) {
    this.selectedMenu = event;
  }

  onExportBtnClick() {
    setTimeout(() => {
      if (document.getElementById('closecoverageReport')) {
        document.getElementById('closecoverageReport').focus();
      }
    }, 400);
    this.taxonomyData = JSON.parse(JSON.stringify(this.taxonomyData));
  }

}
