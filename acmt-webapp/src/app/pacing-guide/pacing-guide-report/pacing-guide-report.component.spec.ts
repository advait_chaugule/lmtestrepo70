import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacingGuideReportComponent } from './pacing-guide-report.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('PacingGuideReportComponent', () => {
  let component: PacingGuideReportComponent;
  let fixture: ComponentFixture<PacingGuideReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PacingGuideReportComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacingGuideReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create PacingGuideReportComponent', () => {
    expect(component).toBeTruthy();
  });
});
