import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  SharedService
} from '../../shared.service';
import {
  CommonService
} from '../../common.service';
import {
  Router,
  ActivatedRoute,
  Params
} from '@angular/router';
import Utils from '../../utils';
import {
  GlobalSettings
} from '../../global.settings';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  Subscription
} from 'rxjs';
import {
  from
} from 'rxjs';
import {
  ItemDetailsComponent
} from '../../common/item-details/item-details.component';
import {
  TreeAccordianComponent
} from '../../tree-accordian/tree-accordian.component';
import { TreeComponent } from '../../common/tree/tree.component';
import {
  environment
} from '../../../environments/environment';
/// <reference path='../../..assets/js/common.d.ts'>
@Component({
  selector: 'app-pacing-guide-detail',
  templateUrl: './pacing-guide-detail.component.html'
})
export class PacingGuideDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  HOME_TAB = 'Pacing Guide';
  TEAM_TAB = 'Team';
  REPORT_TAB = 'Reports';
  TABLE_VIEW = 'tableview';
  GRAPH_VIEW = 'graphview';
  TREE_VIEW = 'treeview';
  DETAIL_VIEW = 'detailview';

  pacingGuideId;
  defaultTreeTab = this.TABLE_VIEW; // holds default selected tree tab value under home(pacing guide) tab
  currentTreeTab = this.defaultTreeTab;
  tabItems = [];
  currentTab = null;
  taxonomyData: any;
  isEditable = false;
  itemAdditionalMetadata: any;
  itemAssociations: any = [];
  itemExemplars: any = [];
  itemAssets: any = [];
  itemLinkedNodes: any = [];
  pacingGuideDetail: any;
  nodetypeData;
  selectedNodeType = {
    title: '',
    node_type_id: ''
  };
  nodeSelectedEvent: Subscription;
  acivatedRouteEvent: Subscription;
  selectedNode = {
    item_id: '',
    id: ''
  };
  view = 'taxonomy-detail';
  preventEdit = false;
  project_type: any;
  showExemplar = false;
  showAdditionalMetaData = false;
  showAssocciation = false;
  showAssets = true;
  viewLocation = 'taxonomyDetails';
  viewUserPermission = false;
  editUserPermission = false;
  addUserPermission = false;
  deleteUserPermission = false;
  viewPacingGuidePermission = false;
  taxonomyArrayData = []; // holds formatted data from tree to array data
  treeTabItems = [this.TABLE_VIEW, this.DETAIL_VIEW];
  listOfColumn = []; // holds table's column properties
  maxColnumber = 0; // table's max column number
  tableViewCompatible = false; // holds boolean value whether table view will be displayed or not based on some scenarios
  pacingGuideNodes: any;
  isTableContainStandard = false;
  showNavigation = true; // holds boolean value for showing up navigation arrow
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  itemId: any = null;
  parentId: any = null;
  itemAssociationId: any = null;
  isSearch = false;
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring
  @ViewChild('itemDetailsComponent', { static: false }) itemDetailsComponent: ItemDetailsComponent;
  @ViewChild('tree', { static: false }) tree: TreeComponent;

  constructor(
    private acivatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CommonService,
    private sharedService: SharedService,
    private authenticateUser: AuthenticateUserService,
    private treeDataService: TreeDataService) {
    this.renamePacingGuide = environment.renamePacingGuide;
    if (this.renamePacingGuide) {
      this.HOME_TAB = 'Unit Planner';
    }

    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      if (item && item.location === 'taxonomydetail') {
        if (item.item_id || item.id) {
          this.selectedNode = item;
          this.getSelectedTreeItemDetail(item.item_id ? item.item_id : item.id);
        }
      }
    });
    this.project_type = localStorage.getItem('project_type');

    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.selectedStageId = event.currentWorkflowStage;
          this.userRoleRight();
        }, 1000);
      }
    });
  }

  ngOnInit() {
    this.acivatedRouteEvent = this.acivatedRoute.params.subscribe((params: Params) => {
      this.parentId = params['parentId'];
      this.itemAssociationId = params['itemAsso'];
      this.isSearch = false;
      if (params['location'] === 'search') { // setting isSearch for Detail View tree tab selection
        this.isSearch = true;
      }
      if (params['id'] !== undefined) {
        if (params['itemId']) {
          this.itemId = params.itemId;
          // this.defaultTreeTab = this.DETAIL_VIEW;
          this.currentTreeTab = this.DETAIL_VIEW;
          setTimeout(() => {
            if (document.getElementById(this.itemId + '-node')) {
              document.getElementById(this.itemId + '-node').click();
            }
            pacingGuideDetailsHeightCalculation();
          }, 2000);
        }
        this.pacingGuideId = params['id'];
        localStorage.setItem('project_id', this.pacingGuideId);
        this.getAllMetadata();
        this.getPacingGuideDetail();
        this.getPacingGuideNodes();
        setTimeout(() => {
          this.userRoleRight();
        }, 500);
      } else {
        const org = this.commonService.getOrgDetails();
        if (org) {
          const path = org + '/' + Utils.ROUTE_PACING_GUIDE_LIST;
          this.router.navigate([path]);
        }
      }

      /* this.tourService.end();
       this.tourService.initialize(this.walkService.getWalkthroughData('project_authoring'));*/
    });
  }

  ngOnDestroy() {
    if (this.acivatedRouteEvent) {
      this.acivatedRouteEvent.unsubscribe();
    }
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
    Utils.removeBodyScrollClass();
  }

  ngAfterViewInit() {
    init_detailsViewClick();
    init_treeViewClick();
  }

  getPacingGuideDetail() {
    const url = GlobalSettings.PROJECT_LIST + '/' + this.pacingGuideId;
    this.commonService.getServiceData(url).then((res) => {
      this.pacingGuideDetail = res;
      if (res['project_type']) {
        localStorage.setItem('project_type', res['project_type']);
      }
      if (res && res['current_workflow_stage_id']) {
        this.selectedStageId = res['current_workflow_stage_id'].split('||')[1];
      }
      this.sharedService.workFlowId.next({
        'workFlowId': res['workflow_id'],
        'projectId': this.pacingGuideId,
        'currentWorkflowStage': res['current_workflow_stage_id'].split('||')[1]
      });

      this.sharedService.setTitleEvent.next({
        'type': 'project',
        'title': this.pacingGuideDetail.project_name
      });
      console.log(' getProjectDetail ', res);
      if (res['document_status'] === 5 || res['document_status'] === 3) {
        this.preventEdit = true;
        this.sharedService.preventEdit.next({
          'preventEdit': this.preventEdit,
        });
      } else {
        this.preventEdit = false;
        this.sharedService.preventEdit.next({
          'preventEdit': this.preventEdit,
        });
      }
    }).catch((ex) => {
      console.log('getPacingGuideDetail ex ', ex);
    });
  }

  onTabSelected(tab) {
    setTimeout(() => {
      this.currentTab = tab;
      if (tab === this.HOME_TAB.toLowerCase()) {
        this.onTreeTabSelected(this.defaultTreeTab);
      }
    }, 10);
  }

  onTreeTabSelected(tab) {
    this.currentTreeTab = tab;
    if (tab === this.DETAIL_VIEW) {
      if ((this.itemId === null || this.itemId === undefined) ||
        (this.itemAssociationId === null || this.itemAssociationId === undefined)) {
        Utils.addBodyScrollClass();
        setTimeout(() => {
          if (this.taxonomyData && this.taxonomyData[0] && document.getElementById(this.taxonomyData[0]['id'] + '-node')) {
            document.getElementById(this.taxonomyData[0]['id'] + '-node').click();
          }
          pacingGuideDetailsHeightCalculation();
        }, 500);
      } else {
        this.expandSelectedNode();
      }
    } else {
      Utils.removeBodyScrollClass();
    }
  }

  onEditClick() {
    const path = this.commonService.getOrgDetails() + '/' + Utils.ROUTE_PROJECT_AUTHORING;
    this.router.navigate([path, this.pacingGuideId]);
  }

  getPacingGuideNodes() {
    const url = GlobalSettings.PACING_GUIDES_AUTHORING + '/' + this.pacingGuideId;
    this.treeDataService.getTreeData(url, true, 3, true, true).then((res: any) => {
      if (res) {
        Utils.sortData(res.parsedTreeNodes.children);
        // this.taxonomyData = res.children;
        this.pacingGuideNodes = res.parsedTreeNodes.children;
        Utils.changePacingGuideIds(this.pacingGuideNodes, '');
        this.taxonomyData = this.pacingGuideNodes;
        if (this.taxonomyData.length > 0) {
          if (this.taxonomyData[0]) {
            this.selectedNode = this.taxonomyData[0];
          }
          console.log('getPacingGuideNodes  ', this.taxonomyData);
          this.makeTreeToArrayStructure(this.taxonomyData[0].children);
          // If browser parameter get 'search' value as location params, select details view tab otherwise default tab
          const tabToSelect = this.isSearch ? this.DETAIL_VIEW : this.defaultTreeTab;
          this.onTreeTabSelected(tabToSelect);
        }
        this.userRoleRight();
      }
      init_detailsViewClick();

    }).catch((ex) => {
      console.log('Error ', ex);
      if (ex && ex.status === 400) { // If no nodes found, redirect to pacingguide list
        this.sharedService.sucessEvent.next({
          type: 'no_project'
        });
        setTimeout(() => {
          const org = this.commonService.getOrgDetails();
          if (org) {
            const path = org + '/' + Utils.ROUTE_PACING_GUIDE_LIST;
            this.router.navigate([path]);
          }
        }, 500);
      }
    });
  }

  /**
   * Get The Selected Node Detail By ItemId
   * @param  {string} itemId
   */
  getSelectedTreeItemDetail(itemId: string) {
    let url;
    if (this.selectedNode['is_document'] === 1) {
      this.showExemplar = false;
      url = GlobalSettings.GET_CFDOC_ITEM_DETAIL + '/' + itemId + '?project_id=' + this.pacingGuideId;
    } else {
      this.showExemplar = true;
      url = GlobalSettings.GET_TREE_ITEM_DETAIL + '/' + itemId + '?project_id=' + this.pacingGuideId;
    }
    this.itemAssociations = [];
    this.itemExemplars = [];
    this.itemAssets = [];
    this.itemAdditionalMetadata = [];
    this.itemLinkedNodes = [];
    this.selectedNode['open_comment_count'] = 0;
    this.commonService.getServiceData(url).then((res: any) => {
      if (this.selectedNode['is_document'] === 1) {
        res['is_editable'] = 1;
        if (!res.node_type_id) {
          res['node_type_id'] = this.selectedNodeType.node_type_id;
          res['node_type'] = this.selectedNodeType.title;
        }
        res['source_uuid_id'] = res['source_document_id'];
      } else {
        res['source_uuid_id'] = res['source_item_id'];
      }

      if (res.node_type_id) {
        this.setNodeTypeById(res.node_type_id);
      }

      if (this.itemDetailsComponent) {
        this.itemDetailsComponent.generateFormData(res, this.selectedNodeType.title);
      }
      // this.itemAssociations = res.item_associations;
      // Move scroll to top
      const rightContainer = document.getElementById('right_container_id');
      if (rightContainer) {
        rightContainer.scrollTop = 0;
      }

      setTimeout(() => {
        if (res.item_associations) {
          this.itemAssociations = res.item_associations;
        }
        if (res.exemplar_associations) {
          this.itemExemplars = res.exemplar_associations;
        }
        if (res.assets) {
          this.itemAssets = res.assets;
        }
        if (res.custom_metadata) {
          this.itemAdditionalMetadata = [];
          if (res.custom_metadata) {
            res.custom_metadata.forEach(obj => {
              if (obj.is_additional_metadata === 1) {
                this.itemAdditionalMetadata.push(obj);
              }
            });
          }
        }
        if (res.linked_item) {
          this.itemLinkedNodes = res.linked_item;
        }
        this.showExemplar = res.node_type === 'Document' ? false : true;
        if (this.itemAssociations.length > 0) {
          this.showAssocciation = true;
        }
        if (this.itemExemplars.length > 0) {
          this.showExemplar = true;
        }
        if (this.itemAssets.length > 0) {
          this.showAssets = true;
        }
        if (this.itemAdditionalMetadata.length > 0) {
          this.showAdditionalMetaData = true;
        }
        pacingGuideDetailsHeightCalculation();
      }, 2000);
    }).catch((ex) => {
      console.log('getSelectedTreeItemDetail ', ex);
    });
  }

  /**
   * Set selectedNodeType by node_type_id
   * @param  {} node_type_id
   */
  setNodeTypeById(node_type_id) {
    if (this.nodetypeData && this.nodetypeData.length > 0) {
      from(this.nodetypeData)
        .filter((w: any) => w.node_type_id === node_type_id)
        .subscribe(result => {
          this.selectedNodeType = JSON.parse(JSON.stringify(result));
        });
    }
  }

  getAllMetadata() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.commonService.getServiceData(url).then((res: any) => {
      this.nodetypeData = res.nodetype;
    })
      .catch((err: any) => { });
  }

  userRoleRight() {

    this.tabItems = [this.HOME_TAB, this.TEAM_TAB, this.REPORT_TAB];

    this.viewPacingGuidePermission = this.authenticateUser.AuthenticateComment('View Pacing Guide', 'pacing_guide');
    const view_pg_tab = Utils.checkProjectPermission(this.pacingGuideId, 'View Pacing Guide', 'pacing_guide_permissions', this.selectedStageId);
    if (view_pg_tab.hasProject) {
      this.viewPacingGuidePermission = view_pg_tab.valid;
    }
    // Removing Pacing Guide tab if not permission
    if (!this.viewPacingGuidePermission) {
      this.removeTab(this.HOME_TAB);
    }

    this.viewUserPermission = this.authenticateUser.AuthenticateComment('View Users', 'pacing_guide');
    const viewUser = Utils.checkProjectPermission(this.pacingGuideId, 'View Users', 'pacing_guide_permissions', this.selectedStageId);
    if (viewUser.hasProject) {
      this.viewUserPermission = viewUser.valid;
    }
    // Removing Team tab if not permission
    if (!this.viewUserPermission) {
      this.removeTab(this.TEAM_TAB);
    }
    this.addUserPermission = this.authenticateUser.AuthenticateComment('Add Users', 'pacing_guide');
    const addUser = Utils.checkProjectPermission(this.pacingGuideId, 'Add Users', 'pacing_guide_permissions', this.selectedStageId);
    if (addUser.hasProject) {
      this.addUserPermission = addUser.valid;
    }

    this.editUserPermission = this.authenticateUser.AuthenticateComment('Update Users', 'pacing_guide');
    const editUser = Utils.checkProjectPermission(this.pacingGuideId, 'Update Users', 'pacing_guide_permissions', this.selectedStageId);
    if (editUser.hasProject) {
      this.editUserPermission = editUser.valid;
    }

    this.deleteUserPermission = this.authenticateUser.AuthenticateComment('Delete Users', 'pacing_guide');
    const deleteUser = Utils.checkProjectPermission(this.pacingGuideId, 'Delete Users', 'pacing_guide_permissions', this.selectedStageId);
    if (deleteUser.hasProject) {
      this.deleteUserPermission = deleteUser.valid;
    }

    if (this.tabItems.length > 0) {
      this.currentTab = this.tabItems[0].toLowerCase();
    } else {
      this.currentTab = null;
    }
  }

  removeTab(tabName: any) {
    if (this.tabItems && this.tabItems.length > 0) {
      this.tabItems = this.tabItems.filter(function (item) {
        return (item !== tabName);
      });
    }
  }


  /*********************** Pacing Guide Table View data formation starts ***********************/
  /**
   * To make taxonomy tree data to array structure
   * @param treeData
   */
  makeTreeToArrayStructure(treeData: any) {
    this.isTableContainStandard = false;

    if (treeData && treeData.length > 0) {
      for (let i = 0; i < treeData.length; i++) {
        if (treeData[i]['item_type'] === 'standard') {
          // If any table row's root node is standard type, then table view will not be displayed
          this.tableViewCompatible = false;
          break;
        } else {
          this.tableViewCompatible = true;
        }
        this.taxonomyArrayData[i] = [];
        this.formatToArray(treeData[i], i, 0, true);
      }
    }
    if (this.tableViewCompatible) {
      for (let i = 0; i < treeData.length; i++) {
        this.findPossibleMaxColumn(treeData[i], i, 0); // calculating column number
      }
      this.reformatArrayAsCompatible(); // reformatting table as compatible
      this.makeDynamicColumnProperty(); // creating table column properties dynamically
      this.tableViewCompatible = this.checkTableViewCompatibility();
    }
    console.log('Pacing Guide Table View Compatibility: ' + this.tableViewCompatible);
    console.log(this.taxonomyArrayData);
    console.log(this.maxColnumber);

  }

  /**
   * Format tree data to array
   * @param node (selected node)
   * @param rowIndex (row index in table structure)
   * @param colIndex (column index in table structure)
   * @param isFirstNode (true/false: To check, if first node of children, then initialize column array)
   */
  formatToArray(node, rowIndex, colIndex, isFirstNode: boolean) {
    if (node.item_type === 'standard') {
      this.isTableContainStandard = true;
    }
    this.maxColnumber = (colIndex + 1) > this.maxColnumber ? (colIndex + 1) : this.maxColnumber;
    if (isFirstNode && !this.taxonomyArrayData[rowIndex]['column' + colIndex]) {
      this.taxonomyArrayData[rowIndex]['colCount'] = colIndex + 1;
      this.taxonomyArrayData[rowIndex]['column' + colIndex] = [];
    }
    if (node.children && node.children.length > 0) {
      this.taxonomyArrayData[rowIndex]['column' + colIndex].push(node);
      node.children.forEach((element, i) => {
        this.formatToArray(element, rowIndex, (colIndex + 1), (i === 0));
      });
    } else {
      this.taxonomyArrayData[rowIndex]['column' + colIndex].push(node);
    }
  }

  /**
   * To calculate possible column number of table view
   * @param node (selected node)
   * @param rowIndex (row index in table structure)
   * @param colIndex (column index in table structure)
   */
  findPossibleMaxColumn(node, rowIndex, colIndex) {
    this.maxColnumber = (colIndex + 1) > this.maxColnumber ? (colIndex + 1) : this.maxColnumber;
    if (node.children && node.children.length > 0) {
      node.children.forEach((element, i) => {
        this.findPossibleMaxColumn(element, rowIndex, (colIndex + 1));
      });
    } else {
      if (node.item_type === 'container' && (colIndex === this.maxColnumber - 1) && this.isTableContainStandard) {
        this.maxColnumber = this.maxColnumber + 1;
        this.taxonomyArrayData[rowIndex]['maxColLabel'] = true;
      }
    }
  }

  /**
   * Reformatting array as compatible view like: to keep standars at right most column
   */
  reformatArrayAsCompatible() {
    this.taxonomyArrayData.forEach((element, i) => {
      for (let j = 0; j < element['colCount']; j++) {
        if (this.taxonomyArrayData[i]['column' + j]) {
          for (let k = 0; k < this.taxonomyArrayData[i]['column' + j].length; k++) {
            if (this.taxonomyArrayData[i]['column' + j][k].item_type === 'standard' && (j !== this.maxColnumber - 1)) {
              if (!this.taxonomyArrayData[i]['column' + (this.maxColnumber - 1)]) {
                this.taxonomyArrayData[i]['column' + (this.maxColnumber - 1)] = [];
                this.taxonomyArrayData[i]['colCount'] = this.taxonomyArrayData[i]['colCount'] + 1;
              }
              this.taxonomyArrayData[i]['column' + (this.maxColnumber - 1)].push(this.taxonomyArrayData[i]['column' + j][k]);
              this.taxonomyArrayData[i]['column' + j].splice(k, 1);
              --k;
            }
          }
          if (this.taxonomyArrayData[i]['column' + j].length === 0) {
            delete this.taxonomyArrayData[i]['column' + j];
            this.taxonomyArrayData[i]['colCount'] = this.taxonomyArrayData[i]['colCount'] - 1;
          }
        }
      }
    });
  }

  /**
   * To create table column properties dynamically
   */
  makeDynamicColumnProperty() {
    let node;
    this.listOfColumn = [];
    for (let i = 0; i < this.taxonomyArrayData.length; i++) {
      if (this.taxonomyArrayData[i]['colCount'] === this.maxColnumber || this.taxonomyArrayData[i]['maxColLabel']) {
        node = this.taxonomyArrayData[i];
        break;
      }
    }
    if (node) {
      for (let i = 0; i < this.maxColnumber; i++) {
        this.listOfColumn.push({
          name: (i !== this.maxColnumber - 1 || (node['column' + i] && node['column' + i][0].item_type !== 'standard')) ?
            (node['column' + i] && node['column' + i][0].metadataType) : 'Standards',
          propName: 'column' + i,
          type: 'array',
          // attributes are array object's property which needs to print in table
          attributes: (i !== this.maxColnumber - 1 || (node['column' + i] && node['column' + i][0].item_type !== 'standard')) ?
            [{ name: 'human_coding_scheme', class: '' }, { name: 'full_statement', class: '' }]
            : [{ name: 'human_coding_scheme', class: '' }, { name: 'full_statement', class: '' },
            { name: 'document_title', class: 'taxonomy-title' }]
          // class: 'col-sm-' + Math.floor(12 / this.maxColnumber) + ' col-xl-' + Math.floor(12 / this.maxColnumber)
        });
      }
    }
  }

  /**
   * To check table view compatibility i.e. if different rows hold different type containers, then table view
   * will not be displayed
   */
  checkTableViewCompatibility() {
    const containers: any = [];
    let compatibility = false;
    for (let j = 0; j < this.maxColnumber; j++) {
      containers[j] = [];
    }
    this.taxonomyArrayData.forEach((element, i) => {
      for (let j = 0; j < this.maxColnumber; j++) {
        if (this.taxonomyArrayData[i]['column' + j]) {
          for (let k = 0; k < this.taxonomyArrayData[i]['column' + j].length; k++) {
            if (this.taxonomyArrayData[i]['column' + j][k].item_type !== 'standard') {
              containers[j].push(this.taxonomyArrayData[i]['column' + j][k].metadataType);
            }
          }
        }
      }
    });
    outer: for (let i = 0; i < containers.length; i++) {
      if (containers[i].length === 1) {
        compatibility = true;
        continue outer;
      }
      for (let j = 0; j < containers[i].length; j++) {
        for (let k = 0; k < containers[i].length; k++) {
          if (j !== k) {
            if (containers[i][j] !== containers[i][k]) { // If different container type found in different rows
              compatibility = false;
              break outer;
            } else {
              compatibility = true;
            }
          }
        }
      }
    }
    return compatibility;
  }
  /*********************** Pacing Guide Table View data formation ends ***********************/

  onNextNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  onPreviousNavigationClick(event: any) {
    this.selectNodeOnNavigation(event.node);
  }

  selectNodeOnNavigation(node: any) {
    this.selectedNode = node;
    Utils.expandTreeTillSelectedNode(this.selectedNode, this.taxonomyData[0]);
    this.tree.onNodeSelected(this.selectedNode);
    setTimeout(() => {
      const id = node['id'] ? node['id'] : node['item_id'];
      if (document.getElementById(id + '-node')) {
        document.getElementById(id + '-node').focus();
      }
    }, 100);
  }

  expandSelectedNode() {
    setTimeout(() => {
      if (this.tree) {
        if (this.parentId && this.itemId && this.itemAssociationId) {
          const node = {
            'id': this.itemAssociationId,
            'item_id': this.itemId,
            'is_document': 0,
            'parent_id': this.parentId
          };
          this.selectNodeOnNavigation(node);
        }
      }
    }, 1000);
  }

  getLeftPanelWidth() {
    let leftMenuWidth;
    let menuButtonWidth;
    const showNav = JSON.parse(localStorage.getItem('isShowNav'));
    const windowWidth = window.innerWidth;
    const actualWholePanelWidth = document.getElementById('treeViewContainer').offsetWidth;
    const rightPanelWidth = document.getElementById('right_container_id').offsetWidth;
    if (document.getElementById('mainNav')) {
      leftMenuWidth = document.getElementById('mainNav').offsetWidth;
    }
    if (document.getElementById('menuButton')) {
      menuButtonWidth = document.getElementById('menuButton').offsetWidth;
    }
    const navbarWidth = showNav ? leftMenuWidth : menuButtonWidth;
    const withoutNavbarWidth = windowWidth - navbarWidth;
    const extraWidth = withoutNavbarWidth - actualWholePanelWidth;
    let leftPanelWidth;
    leftPanelWidth = (actualWholePanelWidth - rightPanelWidth) + extraWidth;
    return (leftPanelWidth + 'px');
  }

}
