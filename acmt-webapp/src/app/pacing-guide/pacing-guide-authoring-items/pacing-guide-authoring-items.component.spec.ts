import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacingGuideAuthoringItemsComponent } from './pacing-guide-authoring-items.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/common.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { SharedService } from 'src/app/shared.service';

describe('PacingGuideAuthoringItemsComponent', () => {
  let component: PacingGuideAuthoringItemsComponent;
  let fixture: ComponentFixture<PacingGuideAuthoringItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PacingGuideAuthoringItemsComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, ConfirmationDialogService, SharedService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacingGuideAuthoringItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create PacingGuideAuthoringItemsComponent', () => {
    expect(component).toBeTruthy();
  });
});
