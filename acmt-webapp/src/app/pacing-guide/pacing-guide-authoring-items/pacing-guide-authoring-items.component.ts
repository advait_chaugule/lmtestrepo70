import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../common.service';
import Utils from '../../utils';

@Component({
  selector: 'app-pacing-guide-authoring-items',
  templateUrl: './pacing-guide-authoring-items.component.html'
})
export class PacingGuideAuthoringItemsComponent implements OnInit {
  @Input() authoringItems;
  @Output() deleteClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router, private service: CommonService) { }

  ngOnInit() {
  }

  onItemClick(item) {
    console.log(item);
    if (item.type.trim() === 'Standard') {
      this.navigateToTaxonomyView(item);
    }
  }

  // Delete click event of item
  onDeleteClick(data) {
    this.deleteClicked.emit(data);
  }

  /**
   * Navigating to taxonomy details page
   * @param item (authored item)
   */
  navigateToTaxonomyView(item) {
    if (item && item['document_id'] && item['parent_id'] && item['item_id']) {
      // this.router.navigate(['app/taxonomy/detail/' + item['document_id'] + '/' +
      //   item['parent_id'] + '/' + item['item_id']]);

      const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_TAXONOMY_DETAIL;
      this.router.navigate([path + item['document_id'] + '/' +
        item['parent_id'] + '/' + item['item_id']]);
    }
  }

}
