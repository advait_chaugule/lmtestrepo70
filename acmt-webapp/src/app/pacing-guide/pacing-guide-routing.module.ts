import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePacingGuideComponent } from './create-pacing-guide/create-pacing-guide.component';
import { PacingGuideAuthoringComponent } from './pacing-guide-authoring/pacing-guide-authoring.component';
import { PacingGuideListComponent } from './pacing-guide-list/pacing-guide-list.component';
import { PacingGuideDetailComponent } from './pacing-guide-detail/pacing-guide-detail.component';

const routes: Routes = [
  {
    path: 'list',
    component: PacingGuideListComponent
  },
  {
    path: 'create',
    component: CreatePacingGuideComponent
  },
  {
    path: 'authoring/:id',
    component: PacingGuideAuthoringComponent
  },
  {
    path: 'detail/:id',
    component: PacingGuideDetailComponent
  },
  {
    path: 'detail/:id/:location',
    component: PacingGuideDetailComponent
  },
  {
    path: 'detail/:id/:parentId/:itemId',
    component: PacingGuideDetailComponent
  },
  {
    path: 'detail/:id/:parentId/:itemId/:itemAsso/:location',
    component: PacingGuideDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacingGuideRoutingModule { }
