import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ViewChild,
  AfterViewInit,

} from '@angular/core';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  ActivatedRoute,
  Params,
  Router
} from '@angular/router';
import {
  GlobalSettings
} from '../../global.settings';
import {
  CommonService
} from '../../common.service';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  SharedService
} from '../../shared.service';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import Utils from '../../utils';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import {
  TreeReorderComponent
} from '../../common/tree-reorder/tree-reorder.component';
import {
  ProjectMappedNodeService
} from '../../project-mapped-node.service';
import {
  ProjectNotesComponent
} from '../../project/projects-notes/projects-notes.component';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  AutoCompleteComponent
} from '../../common/auto-complete/auto-complete.component';
import { PacingGuideTreeReorderComponent } from 'src/app/common/pacing-guide-tree-reorder/pacing-guide-tree-reorder.component';




export class Node {
  id;
  full_statement;
  children = [];
  parent_id = null;
  description = '';
  type = '';
  constructor(_id, _name, _child) {
    this.id = _id;
    this.full_statement = _name;
    this.children = _child;

  }
}

@Component({
  selector: 'app-pacing-guide-authoring',
  templateUrl: './pacing-guide-authoring.component.html',
  styleUrls: ['./pacing-guide-authoring.component.scss', './pacing-guide-authoring-body.component.scss', './pacing-guide-authoring-comment.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class PacingGuideAuthoringComponent implements OnInit, OnDestroy, AfterViewInit {
  acivatedRouteEvent: Subscription;
  nodeSelectedEvent: Subscription;
  workFlowStagesEvent: Subscription;
  pacingGuideId: any;
  taxonomyList: any[] = [];
  nodeList: any[] = [];
  containerTypeList: any[] = [];
  selectedTaxonomy: any = null;
  taxonomyData: any = null;
  selectedTreeNode: any = null;
  selectedTaxonomyId: any = null;
  selectedTaxonmyTitle: any = null;
  pacingGuideTaxonomyId: any = null;
  selectedContainerType: any = null;
  containerText: any = '';
  addedAuthoringList: any[];
  authoredNodes = [];
  standardForm: FormGroup;
  containerForm: FormGroup;
  authoringItem: any = {
    'item_id': null,
    'parent_id': null,
    'document_id': null,
    'type': null,
    'title': null,
    'description': null,
    'list_enumeration': null,
    'sequence_number': null
  };
  authItemsLoading = false;
  updateItem = false;
  postAPIData = [];
  workFlowStageId = '';
  isFirstTime = true;
  selectedNode;
  showNav = {
    value: false
  };
  isTaxonomyExpanded = false;
  expandedNode: any;
  parentNode: any;
  currentTaxonomy: any;
  canCreateContainer;
  canDeleteContainer;
  selectType = 'single';
  uniqueId = 'item_id';
  autoCompleteTitle = 'Container Type';
  validContainerType = false;
  calculatedHeight = '400px';
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring
  @ViewChild('dragElement', {
    static: false
  }) dragElement: HTMLElement;
  @ViewChild('treeViewRef', {
    static: false
  }) treeViewRef: PacingGuideTreeReorderComponent;
  @ViewChild('projectNotesComponentId', {
    static: false
  }) projectNotesComponent: ProjectNotesComponent;
  @ViewChild('autoComplete', {
    static: false
  }) autoComplete: AutoCompleteComponent;
  constructor(private acivatedRoute: ActivatedRoute,
    private router: Router,
    private service: CommonService,
    private treeDataService: TreeDataService,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    private mappedNodeService: ProjectMappedNodeService,
    private authenticateUser: AuthenticateUserService) {
    this.acivatedRouteEvent = this.acivatedRoute.params.subscribe((params: Params) => {
      if (params['id'] !== undefined) {
        this.pacingGuideId = params['id'];
        this.getPacingGuideDetail();
        setTimeout(() => {
          this.getAuthoredItemList();
          this.userRoleRight();
        }, 1500);
      } else {
        const path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PACING_GUIDE_LIST;
        this.router.navigate([path]);
      }
    });
    this.nodeSelectedEvent = this.sharedService.treeNodeSelectedEvent.subscribe((item: any) => {
      // console.log('associated nodes -------', item);
      if (item && item.id && item.location === 'pacing_guide_authoring') {
        item.parent_id = null;
        this.selectedTreeNode = item;
      }
    });
    this.standardForm = this.createStandardFormGroup();
    this.containerForm = this.createContainerFormGroup();

    this.workFlowStagesEvent = this.sharedService.workFlowStages.subscribe((event: any) => {
      if (event.workFlowStageId && event.workFlowStageId !== undefined) {
        this.setWorkflowId(event.workFlowStageId);
      }
    });

    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.selectedStageId = event.currentWorkflowStage;
          this.userRoleRight();
        }, 1000);
      }
    });
  }

  ngOnInit() {
    this.isFirstTime = false;
    this.getTaxonomyList();
    this.getContainerTypes();
    this.addedAuthoringList = [];
    this.workFlowStageId = null;

  }

  ngAfterViewInit() {
    if (document.querySelector('body')) {
      const element = document.querySelector('body');
      element.classList.remove('hide-vScroll');
    }
    setTimeout(() => {
      this.getheight();
    }, 100);
  }

  ngOnDestroy() {
    if (this.acivatedRouteEvent) {
      this.acivatedRouteEvent.unsubscribe();
    }
    if (this.nodeSelectedEvent) {
      this.nodeSelectedEvent.unsubscribe();
    }
    if (this.workFlowStagesEvent) {
      this.workFlowStagesEvent.unsubscribe();
    }
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
    this.addedAuthoringList = [];
    this.isFirstTime = true;
  }

  createStandardFormGroup() {
    // Form structure creation for Standard
    const group = new FormGroup({
      selectedTaxonomy: new FormControl(null, [<any>Validators.required]),
      selectStandards: new FormControl(null)
    });
    return group;
  }

  createContainerFormGroup() {
    // Form structure creation for Container
    const group = new FormGroup({
      // selectedContainerType: new FormControl(null, [ < any > Validators.required]),
      containerText: new FormControl(null, [<any>Validators.required])
    });
    return group;
  }

  /**
   * To get associated taxonomy list of a particular pacing guide
   */
  getTaxonomyList() {
    this.taxonomyList = [];
    this.nodeList = [];
    if (this.pacingGuideId) {
      const url = GlobalSettings.PACING_GUIDES + '/' + this.pacingGuideId;
      this.service.getServiceData(url).then((res: any) => {
        const documents = res['project_documents_details'];
        this.pacingGuideTaxonomyId = res.document_id;
        if (documents && documents.length > 0) {
          documents.forEach(element => {
            element.is_document = 1;
            element.is_taxonomy = true;
            this.taxonomyList.push(element);
          });
        }
        this.taxonomyList.forEach(element => {
          this.nodeList.push(element);
        });

      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });
    }
  }

  /**
   * To get container type list
   */
  getContainerTypes() {
    const url = GlobalSettings.GET_NODETYPE_LIST;
    this.service.getServiceData(url).then((res: any) => {
      // console.log('Container types: ', res);
      res.nodetype.forEach(nodeType => {
        if (nodeType && nodeType.is_document !== 1) {
          this.containerTypeList.push(nodeType);
        }
      });
    }).catch((ex) => {
      console.log('Error caught while fetching metadata set list', ex);
    });
  }

  getPacingGuideDetail() {
    const url = GlobalSettings.PROJECT_LIST + '/' + this.pacingGuideId;
    this.service.getServiceData(url).then((res) => {
      if (res['project_type']) {
        localStorage.setItem('project_type', res['project_type']);
      }
      if (res && res['project_name']) {
        this.sharedService.setTitleEvent.next({
          'type': 'project',
          'title': res['project_name']
        });

        this.sharedService.workFlowStages.next({
          'selectedFilterOption': res['current_workflow_stage']['current_workflow_stage_name']
        });
        this.getWorkFlowStagesList(res['workflow_id']);
      }
    }).catch((ex) => {
      console.log('getPacingGuideDetail ', ex);
    });
  }

  getAuthoredItemList() {
    this.taxonomyList = [];
    if (this.pacingGuideId) {
      this.authItemsLoading = true;
      const url = GlobalSettings.PACING_GUIDES_AUTHORING + '/' + this.pacingGuideId;
      this.addedAuthoringList = [];
      this.treeDataService.getTreeData(url, false, 2, true, true).then((res: any) => {

        if (res) {
          Utils.sortData(res.parsedTreeNodes.children);
          console.log('Before getAuthoredItemList  ', JSON.parse(JSON.stringify(res.parsedTreeNodes.children)));
          this.authoredNodes = JSON.parse(JSON.stringify(res.parsedTreeNodes.children));
          Utils.changePacingGuideIds(this.authoredNodes, '');
          this.addedAuthoringList = this.authoredNodes;
          // Set the outer nodes parent_id to null
          this.addedAuthoringList.forEach(element => {
            element.parent_id = null;
          });
          console.log('After getAuthoredItemList  ', this.addedAuthoringList);
          this.authItemsLoading = false;
          this.selectedNode = this.addedAuthoringList[0];
          if (this.selectedNode) {
            this.selectedNode['selected'] = true;
            this.loadComments(this.selectedNode.item_id);
          }
          // this.sortAuthoringList();
        }
      }).catch((ex) => {
        console.log('onTaxonomySelected error ', ex);
      });
    }
  }

  /**
   * On selecting of taxonomy from dropdown
   * @param event (selected taxonomy)
   */
  onTaxonomySelected(event) {
    console.log('onTaxonomySelected ', event);

    if (event && event.document_id) {
      let newArr = [];
      this.taxonomyData = null;
      this.selectedTreeNode = null;
      this.currentTaxonomy = event;
      this.nodeList = [];
      this.selectedTaxonomyId = event.document_id;
      this.selectedTaxonmyTitle = event.title;
      // const url = GlobalSettings.GET_TREE_VIEW + this.selectedTaxonomyId;
      const url = GlobalSettings.GET_PROJECT_ASSOCIATED_NODES + '/' + this.pacingGuideId + '?document_id=' +
        this.selectedTaxonomyId;
      this.mappedNodeService.getTreeData(url, 10).then((res) => {
        if (res) {
          console.log(' getProjectAssociatedNodes ', res);
          if (res) {
            Utils.sortData(res);
          }
          this.taxonomyData = res;
          if (this.taxonomyData.length) {
            newArr = this.taxonomyData[0].title ? this.taxonomyData : this.taxonomyData[0].children;
            newArr.forEach(element => {
              if (element.item_usage_count === undefined) {
                element.item_usage_count = 0;
              }
              this.nodeList.push(element);
            });
          } else {
            this.nodeList = [];
          }

        }
        console.log('onTaxonomySelected: ------', this.taxonomyData);
      }).catch((ex) => {
        console.log('onTaxonomySelected error ', ex);
      });
    }
  }

  /**
   * API calling on Standard create
   */
  onCreateStandard(parentId) {
    this.authoringItem = {};
    if (this.pacingGuideTaxonomyId && this.selectedTreeNode) {
      console.log('Pacing guide TaxonomyId ', this.selectedTaxonomyId);
      console.log('selectedTreeNode ', this.selectedTreeNode);
      const url = GlobalSettings.CREATE_CASE_ASSOCIATION;

      const body = {
        'origin_node_id': this.selectedTreeNode.id, // selected node from taxonomy tree nodes
        'destination_node_ids': parentId ? parentId : this.pacingGuideTaxonomyId, // pacing guide's default taxonomy
        'association_type': 1,
        'project_type': 2,
        'description': '',
        'list_enumeration': '' + (this.addedAuthoringList.length + 1),
        'sequence_number': '' + (this.addedAuthoringList.length + 1),
        'project_id': this.pacingGuideId
      };
      this.service.postService(url, body).then((res: any) => {

        if (res === 'No new associations to create.') {
          // TODO: console.log('--------------- ',res);
          /*if (this.treeViewRef) {
            this.treeViewRef.updateStandard(this.authoringItem,false);
          }*/
        } else {

          this.sharedService.sucessEvent.next({
            type: 'created_standard'
          });
          const data = res[0]['origin_node'];

          this.authoringItem = this.getAuthoredItemObj(data.item_id, data.parent_id,
            data.document_id, 'Standard', data.human_coding_scheme, data.full_statement);
          this.authoringItem['children'] = [];
          this.authoringItem['parent_id'] = null;
          this.authoringItem['item_type'] = 'Standard';
          this.authoringItem.list_enumeration = this.addedAuthoringList.length + 1;
          this.authoringItem.sequence_number = this.addedAuthoringList.length + 1;
          this.authoringItem['document_title'] = this.selectedTaxonmyTitle;
          this.authoringItem['item_association_id'] = res[0]['item_association_id'];
          if (this.treeViewRef) {
            this.treeViewRef.updateStandard(this.authoringItem, true);
          }
          this.updateItemCount(this.selectedTreeNode, true);
          // this.addedAuthoringList.push(this.authoringItem);
          // this.sortAuthoringList();
          console.log(' getAuthoredItemList ', this.addedAuthoringList);
          this.onCancel();
        }
      }).catch((ex: any) => {
        console.log('onCreateStandard  ', ex);
        this.onCancel();
      });
    }
  }

  /**
   * API calling on Container create
   */
  onCreateContainer() {
    console.log('Container type: ', this.selectedContainerType);
    this.authoringItem = {};
    if (this.selectedContainerType && this.selectedContainerType.node_type_id) {
      const url = GlobalSettings.CREATE_CONTAINER;
      const body = {
        'parent_id': this.pacingGuideTaxonomyId, // pacing guide's default taxonomy
        'document_id': this.pacingGuideTaxonomyId, // pacing guide's default taxonomy
        'full_statement': this.containerText,
        'node_type_id': this.selectedContainerType.node_type_id, // selected container type's id
        'project_type': 2,
        'list_enumeration': '' + (this.addedAuthoringList.length + 1),
        'sequence_number': '' + (this.addedAuthoringList.length + 1)
      };
      this.service.postService(url, body).then((res: any) => {
        console.log('Response onCreateContainer ', res);
        this.sharedService.sucessEvent.next({
          type: 'created_container'
        });
        this.authoringItem = this.getAuthoredItemObj(res.item_id, res.parent_id,
          res.document_id, 'container', this.selectedContainerType.title, res.full_statement);
        this.authoringItem['children'] = [];
        this.authoringItem['parent_id'] = null;
        this.authoringItem['item_association_id'] = res.item_association_id;
        this.authoringItem.list_enumeration = '' + this.addedAuthoringList.length + 1;
        this.authoringItem.sequence_number = '' + this.addedAuthoringList.length + 1;
        this.authoringItem['node_type'] = this.selectedContainerType.title;
        this.authoringItem['expand'] = true;
        this.authoringItem['cut']= 0;
        this.authoringItem['paste']= 0;
        this.authoringItem['reorder']= 0;
        this.addedAuthoringList.push(this.authoringItem);
        // this.sortAuthoringList();
        console.log('getAuthoredItemList ', this.addedAuthoringList);
        this.onCancel();
      }).catch((ex: any) => {
        console.log('onCreateContainer  ', ex);
        this.onCancel();
      });
    }
  }

  // Form validation
  checkFormValidation(formName: any): boolean {
    this.checkValidation();
    if (formName === 'standard' && this.standardForm && this.selectedTreeNode && this.selectedTreeNode.id &&
      (this.selectedTreeNode.is_document === 0)) {
      return this.standardForm.valid;
    } else if (formName === 'container' && this.containerForm && this.containerText && (this.containerText.trim() !== '') && this.validContainerType) {
      return this.containerForm.valid;
    } else {
      return false;
    }
  }

  /**
   * @param item_id
   * @param parent_id
   * @param document_id
   * @param type ('container' or 'standard')
   * @param title (full_statement value)
   * @param description (human_coding_scheme value)
   */
  getAuthoredItemObj(item_id: any, parent_id: any,
    document_id: any, type: any, title: any, description: any) {

    const obj: any = {};
    obj.item_id = item_id;
    obj.id = item_id;
    obj.parent_id = parent_id;
    obj.document_id = document_id;
    obj.item_type = type;
    obj.title = title;
    obj.full_statement = description;
    obj.human_coding_scheme = title;
    obj.description = description;
    obj.node_type = title;
    return obj;
  }



  sortAuthoringList() {
    if (this.addedAuthoringList && this.addedAuthoringList.length > 0) {
      Utils.sortDataArray(this.addedAuthoringList, 'sequence_number', false, true);
    }
  }

  /**
   * On Add Standard modal opened
   */
  onAddStandardModalOpen() {
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.onCancel();
    this.getTaxonomyList();
  }

  /**
   * On Add Container modal opened
   */
  onAddContainerModalOpen() {
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.onCancel();
    this.getContainerTypes();
  }

  onCancel() {
    this.containerForm.reset();
    this.standardForm.reset();
    // this.clearTaxonomyData();
    this.clearContainerData();
    this.authoringItem = {};
    if (this.autoComplete) {
      this.autoComplete.clearSelection();
    }
  }

  onKeydown(event) {
    if (event.keyCode === 27) { // On Escape press
      this.onCancel();
    }
  }

  clearTaxonomyData() {
    this.selectedTaxonomy = null;
    this.taxonomyData = null;
    this.selectedTreeNode = null;
    this.selectedTaxonomyId = null;
  }

  clearContainerData() {
    this.selectedContainerType = null;
    this.containerText = '';
  }

  onTreeUpdate(dataList) {
    // this.addedAuthoringList = dataList;
    this.postAPIData = [];
    console.log('onTreeUpdate  before ', this.addedAuthoringList);
    let list_enumeration = 1;
    let sequence_number = 1;
    dataList.forEach(element => {
      this.createData(element, sequence_number, element.item_type);
      list_enumeration++;
      sequence_number++;
    });
    this.updateItem = true;
    console.log('onTreeUpdate ', this.postAPIData);
    const url = GlobalSettings.PACING_GUIDE_SET_TREE_HIERARCHY + '/' + this.pacingGuideTaxonomyId;

    this.service.postService(url, {
      items: this.postAPIData
    }).then((res) => {
      console.log('onTreeUpdate res ', res);
      this.updateItem = false;
    }).catch((ex) => {
      console.log('Set Tree ', ex);
      this.updateItem = false;
    });
  }

  createData(element, list_enumeration, parentType) {

    console.log('createData  ', element);
    // Setting empty parent id of a node if node contains dummy parent id
    if (element.parent_id === Utils.DUMMY_ID_FOR_ORPHANLABEL) {
      element.parent_id = '';
    }
    const obj = {
      item_id: element.item_id,
      parent_id: element.parent_id ? element.new_parent_id : '',
      id: element.item_id,
      list_enumeration: '' + list_enumeration,
      sequence_number: '' + list_enumeration,
      item_type: element.item_type ? element.item_type.toLowerCase() : '',
      parent_item_type: parentType ? parentType.toLowerCase() : '',
      item_association_id: element.item_association_id,
      title: element.full_statement ? element.full_statement : element.human_coding_scheme
    };
    if (!element.isOrphanLabel) {
      this.postAPIData.push(obj);
    }
    if (element.children && element.children.length > 0) {
      for (let index = 0; index < element.children.length; index++) {
        const child = element.children[index];
        this.createData(child, index + 1, element.item_type);
      }
    }

  }

  onDeleteEvent(node) {
    let url = '',
      body = null;
    if (node.item_type.toLowerCase().trim() === 'standard') {
      url = GlobalSettings.DELETE_ITEM_ASSOCIATION + '/' + node.item_association_id;
      body = null;
    } else if (node.item_type.toLowerCase().trim() === 'container') {
      url = GlobalSettings.DELETE_NODE + '/' + node.item_id + '?project_id=' + this.pacingGuideId;
      body = {
        project_id: this.pacingGuideId
      };
    }

    this.dialogService.confirm('Confirm', 'Are you sure you want to delete the selected node?')
      .then((confirmed) => {
        if (confirmed) {
          if (node.id) {
            this.service.deleteServiceData(url, body).then((res) => {
              if (this.treeViewRef) {
                this.treeViewRef.onNodeDeleted(node.id);
              }
              this.sharedService.sucessEvent.next({
                type: 'delete_node'
              });
              this.updateItemCount(node, false);
            }).catch((ex) => {
              console.log('deleteNode ', ex);
            });
          }
        } else {
          console.log('User cancel the dialog');
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });

  }

  getWorkFlowStagesList(workflowid) {
    const url = GlobalSettings.GET_WORKFLOW + '/' + workflowid;
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        this.sharedService.workFlowStages.next({
          'workFlowStages': res.workflow.stages,
        });
      }
    }).catch((ex) => {
      console.log('error in workFlowStagesList', ex);
    });
  }


  setWorkflowId(id) {
    this.workFlowStageId = id;
    if (!this.isFirstTime && this.workFlowStageId) {
      this.updateWorkflow();
    }
  }

  updateWorkflow() {
    const url = GlobalSettings.PACING_GUIDES + '/' + this.pacingGuideId + '/workflowStage';
    const body = {
      'project_id': this.pacingGuideId,
      'workflow_stage_id': this.workFlowStageId
    };
    this.service.postService(url, body).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'pacing_guide_status'
      });

    }).catch((ex) => {
      console.log('createNodeTemplate error occured', ex);
    });
  }

  loadComments(itemId) {
    if (this.projectNotesComponent) {
      this.projectNotesComponent.getItemComments(itemId);
    }
  }
  getCurrentItem(item) {
    if (this.selectedNode === item) {
      return;
    }
    console.log('pacing guide received', item);
    this.selectedNode = item;
    this.loadComments(item.item_id);
    console.log(this.selectedNode);
  }
  openCommentSec() {
    if (this.projectNotesComponent) {
      this.projectNotesComponent.showHideNav(true);
    }
  }
  getheight() {
    const containerElem = document.getElementById('pacing_guide_container');
    const headerElem = document.getElementsByClassName('pacing-guide-authoring-header')[0];
    const height = containerElem ? containerElem.scrollHeight : 0;
    const headerHeight = headerElem ? (headerElem.scrollHeight + 1) : 0;
    const windowHeight = window.innerHeight;
    const panelheight = windowHeight - headerHeight;
    this.calculatedHeight = (height > panelheight ? height : panelheight) + 'px';
  }

  userRoleRight() {

    this.canCreateContainer = this.authenticateUser.AuthenticateComment('Add Container', 'pacing_guide');
    const createContainer = Utils.checkProjectPermission(this.pacingGuideId, 'Add Container', 'pacing_guide_permissions', this.selectedStageId);
    if (createContainer.hasProject) {
      this.canCreateContainer = createContainer.valid;
    }
    console.log('userRoleRight ', this.canCreateContainer);
    this.canDeleteContainer = this.authenticateUser.AuthenticateComment('Delete Container', 'pacing_guide');
    const deleteContainer = Utils.checkProjectPermission(this.pacingGuideId, 'Delete Container', 'pacing_guide_permissions', this.selectedStageId);
    if (deleteContainer.hasProject) {
      this.canDeleteContainer = deleteContainer.valid;
    }
    console.log('userRoleRight ', this.canDeleteContainer);

    // Pacing Guide Comments
  }
  getParentNodeObject(parentId) {

    this.taxonomyData.forEach(element => {
      this.getParent(element, parentId);
    });

  }

  getParent(node, parentId) {
    if (node.id === parentId) {
      this.parentNode = node;
      // console.log('getParent*********** ', node.id, parentId);
    } else {
      if (node.children.length > 0) {
        node.children.forEach(element => {
          this.getParent(element, parentId);
        });
      }
    }
  }

  getNodeObject(id) {

  }

  loadNext(event) {
    console.log('onTaxonomySelected  ', event);
    this.isTaxonomyExpanded = true;
    this.nodeList = [];
    this.expandedNode = event;

    if (event.is_taxonomy) {
      this.onTaxonomySelected(event);
    } else {
      event.children.forEach(element => {
        console.log('loadNext   ', element.item_usage_count);
        if (element.item_usage_count === undefined) {
          element.item_usage_count = 0;
        }
        this.nodeList.push(element);
      });
    }
  }

  loadPrevious(node) {
    console.log(node);
    if (!node.is_taxonomy) {
      if (node.isFirst) {
        this.onTaxonomySelected(this.currentTaxonomy);
        this.expandedNode = this.currentTaxonomy;
      } else {
        this.getParentNodeObject(node.parent_id);
        this.expandedNode = this.parentNode;
        this.nodeList = this.parentNode.children;
      }
    } else {
      this.getTaxonomyList();
      this.isTaxonomyExpanded = false;
    }
    // if (node.is_document === )
  }

  getLeftPanelHeight() {
    const headerElem = document.getElementsByClassName('pacing-guide-authoring-header')[0];
    const headerHeight = headerElem ? (headerElem.scrollHeight + 1) : 0;
    // const footerHeight = document.getElementById('footer').offsetHeight;
    const windowHeight = window.innerHeight;
    const leftPanelHeight = windowHeight - (headerHeight);
    return leftPanelHeight + 'px';
  }

  getInnerPanelHeight() {
    let height;
    const panelHeight = document.getElementById('left_panel').offsetHeight;
    const upperPanelHeight = document.getElementById('add_standard').offsetHeight;

    height = panelHeight - upperPanelHeight;

    return height + 'px';
  }


  drag(ev) {
    /*   localStorage.setItem('dragStarted', 'true');
      ev.dataTransfer.setData('text', ev.target.id);*/
    this.treeViewRef.drag(ev, true);
  }
  drop(ev) {
    if (ev && ev.dragData) {
      ev.dragData.item_usage_count = 1;
    }
    this.treeViewRef.drop(ev);
  }

  onAddStandard(data) {
    console.log(' loadNext onAddStandard   ', data.obj.item_usage_count);
    this.selectedTreeNode = data.obj;
    this.onCreateStandard(data.parentId);
  }

  updateItemCount(item, isAdd) {
    this.nodeList.forEach(element => {
      if (item && item.item_id === element.id) {
        if (isAdd) {
          element.item_usage_count += 1;
        } else {
          element.item_usage_count -= 1;
        }
      }
    });
  }

  onInvalidMoveEvent(msg) {
    this.sharedService.sucessEvent.next({
      type: 'pacing-guid-invalid-move',
      msgType: this.sharedService.MSG_TYPE_WARNING
    });
  }
  checkValidation() {
    if (this.selectedContainerType) {
      this.validContainerType = true;
    } else {
      this.validContainerType = false;
    }
  }
  updateSelectedObject(obj) {
    this.selectedContainerType = obj;
  }
  onClickedOutsideTaxonomy(e) {
    this.autoComplete.openPanel(false);
  }
}
