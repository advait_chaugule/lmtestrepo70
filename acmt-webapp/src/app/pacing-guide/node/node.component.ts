import {
  Component,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-node',
  templateUrl: './node.component.html'
})


export class NodeComponent implements OnChanges {
  @Input() name: string;
  @Input() item: any;
  @Input() isChild = false;
  @Input() level = 0;
  @Input() isdragStart = 'false';
  @Input() selectedParentId = null;
  @Output() deleteEvent: EventEmitter<any>;
  @Output() selectedNodeItem: EventEmitter<any>;

  constructor() {
    this.deleteEvent = new EventEmitter<any>();
    this.selectedNodeItem = new EventEmitter<any>();
  }
  ngOnChanges() {
    this.isdragStart = localStorage.getItem('dragStarted');
  }

  onDeleteEvent(id) {
    this.deleteEvent.emit(id);
  }

  onItemClick(item) {
    console.log(item);
    this.selectedNodeItem.emit(item);
  }

}
