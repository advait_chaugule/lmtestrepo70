import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacingGuideRoutingModule } from './pacing-guide-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PublicModule } from '../shared/public.module';
import { CreatePacingGuideComponent } from './create-pacing-guide/create-pacing-guide.component';
import { PacingGuideAuthoringComponent } from './pacing-guide-authoring/pacing-guide-authoring.component';
import { Shared2Module } from '../shared/shared2/shared2.module';
import { PacingGuideListComponent } from './pacing-guide-list/pacing-guide-list.component';
import { Shared3Module } from '../shared/shared3/shared3.module';
import { PacingGuideAuthoringItemsComponent } from './pacing-guide-authoring-items/pacing-guide-authoring-items.component';
import { NodeComponent } from './node/node.component';
import { PacingGuideDetailComponent } from './pacing-guide-detail/pacing-guide-detail.component';
import { ItemDetailModule } from '../shared/item-detail/item-detail.module';
import { PacingGuideReportComponent } from './pacing-guide-report/pacing-guide-report.component';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';


@NgModule({
  imports: [
    CommonModule,
    PacingGuideRoutingModule,
    SharedModule,
    PublicModule,
    Shared2Module,
    Shared3Module,
    ItemDetailModule,
    PreLoaderModule
  ],
  declarations: [
    CreatePacingGuideComponent,
    PacingGuideAuthoringComponent,
    PacingGuideListComponent,
    PacingGuideAuthoringItemsComponent,
    NodeComponent,
    PacingGuideDetailComponent,
    PacingGuideReportComponent
  ],
  exports: [SharedModule, ItemDetailModule],
  schemas: [NO_ERRORS_SCHEMA],
})
export class PacingGuideModule { }
