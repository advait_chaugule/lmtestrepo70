import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy
} from '@angular/core';
import {
  CommonService
} from '../../common.service';
import {
  GlobalSettings
} from '../../global.settings';
import Utils from '../../utils';
import {
  SortListComponent
} from '../../common/sort-list/sort-list.component';
/*import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
import {
  WalktroughService
} from '../../help/walkthrough/walktrough.service';*/
import {
  CreatePacingGuideComponent
} from '../create-pacing-guide/create-pacing-guide.component';
import {
  Router
} from '@angular/router';
import {
  SharedService
} from '../../shared.service';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  ConfirmationDialogService
} from '../../confirmation-dialog/confirmation-dialog.service';
import {
  ParseTreeResult
} from '@angular/compiler';
import {
  AuthenticateUserService
} from '../../authenticateuser.service';
import {
  TreeDataService
} from '../../tree-data.service';
import {
  ProjectMappedNodeService
} from '../../project-mapped-node.service';
import {
  environment
} from '../../../environments/environment';
@Component({
  selector: 'app-pacing-guide-list',
  templateUrl: './pacing-guide-list.component.html'
})
export class PacingGuideListComponent implements OnInit, OnDestroy {
  SORT_BY_LABEL = 'Sort by';

  loading = false;
  startIndex = 0;
  lastIndex = 10;
  actionMode: string;
  selectedPacingGuide: any = null;
  minItemPerPage;
  pacingGuides;
  viewType = '';
  createPacingGuidePermission = false;
  btnText = 'Create New Pacing Guide'; // holds the text for create button
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  selectedWorkFlowStageEvent: Subscription;
  selectedStageId = ''; // holds selected stage from project authoring
  // ----------------for Common Data table----------------- //
  listOfColumn = [{
    name: 'Name',
    propName: 'project_name',
    // class: 'col-2 pr-2',
    type: 'link',
    redirect: true,
    loadPage: true
  },
  {
    name: 'Status',
    propName: 'status',
    // class: 'col px-1',
    type: 'text'
  },
  {
    name: 'Last Change',
    propName: 'updated_at',
    // class: 'col pl-1 pr-2',
    type: 'date',
    dateType: 'amCalendar'
  },
  {
    name: 'Active Since',
    propName: 'created_at',
    // class: 'col px-1',
    type: 'date',
    dateType: 'amCalendar'
  },
  {
    name: 'Created By',
    propName: 'created_by',
    // class: 'col pl-1 pr-2 created-by',
    type: 'text',
    canFilter: true
  },
  {
    name: 'Roles',
    propName: 'access_roles',
    // class: 'col px-1',
    type: 'text',
    canFilter: true
  }
  ];
  optionList = [];

  sortOptions = [{
    type: 'project_name',
    isDate: false,
    isAsce: true,
    display: 'Name (A-Z)'
  }, {
    type: 'project_name',
    isDate: false,
    isAsce: false,
    display: 'Name (Z-A)'
  }, {
    type: 'created_at',
    isDate: true,
    isAsce: true,
    display: 'Active Since (Asc)'
  }, {
    type: 'created_at',
    isDate: true,
    isAsce: false,
    display: 'Active Since (Desc)'
  }];
  searchingText; // holds text input to search in list
  filterData = []; // holds result array of search
  filterDataEvent: Subscription;

  @ViewChild('sortListRef', { static: false }) sortListRef: SortListComponent;
  @ViewChild('createPacingGuide', { static: false }) createPacingGuide: CreatePacingGuideComponent;

  searchResult = [];
  searchTrigger = false;
  searchResultEvent: Subscription;
  isFirstTime = true;

  constructor(
    private service: CommonService,
    private treeService: TreeDataService,
    /* private tourService: NgxBootstrapProductTourService,
     private walkService: WalktroughService,*/
    private router: Router,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService,
    private authenticateUser: AuthenticateUserService) {
    this.renamePacingGuide = environment.renamePacingGuide;
    if (this.renamePacingGuide) {
      this.btnText = 'Create new Unit Planner';
    }
    this.minItemPerPage = Utils.PAGINATION_LOWER_LIMIT;
    this.searchResultEvent = this.sharedService.searchResultEvent.subscribe((item: any) => {
      if (this.isFirstTime) {
        this.isFirstTime = false;
        return;
      }
      if (!this.isFirstTime) {
        if (item) {
          this.searchResult = item['searchData'];
          if (item['showSearchList']) {
            this.searchTrigger = true;
          } else {
            this.searchTrigger = false;
          }
        }
      }
    });

    this.selectedWorkFlowStageEvent = this.sharedService.workFlowId.subscribe((event: any) => {
      if (event.workFlowId !== undefined) {
        setTimeout(() => {
          this.selectedStageId = event.currentWorkflowStage;
          this.userRoleRight();
        }, 1000);
      }
    });

    this.sharedService.searchTextEvent.subscribe((res: any) => {
      this.searchingText = res.text;
    });

    this.filterDataEvent = this.sharedService.filterDataTable.subscribe((res: any) => {
      this.filterData = res.data;
    });
  }

  ngOnInit() {
    this.searchingText = '';
    this.getPacingGuides();
    setTimeout(() => {
      this.userRoleRight();
    }, 500);
    /* this.tourService.end();
     this.tourService.initialize(this.walkService.getWalkthroughData('pacing_guide_list'));*/
    // ----------------for Common Data table----------------- //

    this.optionList = [{
      name: 'Edit',
      type: 'editPacingGuide',
      value: true,
      modal: '#createPacingGuide',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'delPacingGuide',
      value: true,
      modal: '',
      check: 'isDelete'
    }
    ];
    this.sharedService.faqEvent.next({
      name: 'pacing_guide_list'
    });
  }

  ngOnDestroy() {
    if (this.selectedWorkFlowStageEvent) {
      this.selectedWorkFlowStageEvent.unsubscribe();
    }
  }

  /**
   * To get pacing guide list
   */
  getPacingGuides() {
    const url = GlobalSettings.PACING_GUIDES + '?project_type=2';
    this.loading = true;
    this.service.getServiceData(url).then((res: any) => {
      // console.log('List of pacing guides:', res.projects);
      this.pacingGuides = [];
      res.projects.forEach(item => {
        if (this.checkPacingGuideCanBeViewed(item.project_id, item.workflow['current_stage_id'])) {
          const roles_arr = [];
          item.project_status = item.taxonomy_status === 3 ? 'Inactive' : item.project_status;
          item['status'] = item['project_status'];
          item.isEditable = item.taxonomy_status !== 5 && item.taxonomy_status !== 3 ?
            this.checkPacingGuideCanCanBeEdited(item.project_id, item.workflow['current_stage_id']) : false;
          item.isDelete = item.taxonomy_status !== 5 && item.taxonomy_status !== 3 ?
            this.checkPacingGuideCanCanBeDeleted(item.project_id, item.workflow['current_stage_id']) : false;
          item.showOptions = (!item.isEditable && !item.isDeletable) ? false : true;
          item.showOptions = (!item.isEditable && !item.isDelete) ? false : true;
          this.pacingGuides.push(item);
          if (item.access_right.length > 0) {
            item.access_right.forEach(r => {
              roles_arr.push(r.name);
            });
          } else {
            roles_arr.push('No Role');
          }
          item.access_roles = roles_arr.join(', ');
          // item.access_roles = 'Author, Admin, Debarati';
        }
      });
      if (this.pacingGuides.length > 0) {
        this.sortListRef.sortDefalut('Active Since (Desc)');
      } else {
        console.log('No pacing guide found.');
      }
      this.userRoleRight();
      this.loading = false;
    }).catch(ex => {
      this.pacingGuides = [];
      console.log('list of pacing guides ex ', ex);
    });
  }

  onPacingGuideCreateBtnClick() {
    this.actionMode = 'Create';
    this.selectedPacingGuide = null;
    if (this.createPacingGuide) {
      this.createPacingGuide.getAllTaxonomyList();
    }
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'create_pacing'
    });
  }

  onEvent(event) {
    if (event.msg === 'success' && (event.action === 'edit' || event.action === 'create')) {
      this.getPacingGuides();
    }
  }

  /**
   * Pacing Guide delete api
   * @param pacingGuideId
   * @param pacingGuideName
   */
  onPacingGuideDelete(pacingGuideId, pacingGuideName?) {
    if (pacingGuideId) {
      this.dialogService.confirm('Confirm', 'Do you want to delete pacing guide ' + pacingGuideName + '?')
        .then((confirmed) => {
          if (confirmed) {
            const url = GlobalSettings.PACING_GUIDES + '/' + pacingGuideId;
            this.service.deleteServiceData(url).then((res: any) => {
              this.sharedService.sucessEvent.next({
                type: 'delete_pacing_guide'
              });
              this.pacingGuides = this.removePacingGuideFromList(res.project_id, this.pacingGuides);
            }).catch((ex) => {
              console.log('onPacingGuideDelete ', ex);
            });
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    }
  }

  /**
   * To remove pacing guide from list
   * @param pacingGuideId
   * @param list
   */
  removePacingGuideFromList(pacingGuideId, list) {
    let modifiedList = list;
    if (list) {
      let index = 0;
      let foundIndex = -1;
      list.forEach(element => {
        if (pacingGuideId === element.project_id) {
          foundIndex = index;
        }
        index++;
      });
      if (foundIndex > -1) {
        if (list.length > foundIndex) {
          list.splice(foundIndex, 1);
          modifiedList = list;
        }
      }
    }
    return modifiedList;
  }

  onPacingGuideEditBtnClick(pgData) {
    this.actionMode = 'Edit';
    const tempData = pgData;
    this.selectedPacingGuide = JSON.parse(JSON.stringify(tempData));
    setTimeout(() => {
      focusOnModalCloseBtn();
    }, 500);
    this.sharedService.faqEvent.next({
      name: 'create_pacing'
    });
  }

  getLocalTimeZoneDate(date) {
    return Utils.changeDateToLocalTimeZone(date);
  }

  onPacingguideSelected(pacingguide) {
    if (pacingguide && pacingguide.project_id) {
      this.sharedService.setTitleEvent.next({
        'type': 'project',
        'title': pacingguide.project_name
      });
      let path;
      const url = GlobalSettings.PACING_GUIDES_AUTHORING + '/' + pacingguide.project_id;
      this.treeService.getTreeData(url, true).then((response: any) => {
        const res = response.parsedTreeNodes;
        const data = res ? res.children[0].children : [];
        if (data.length > 0) {
          path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PACING_GUIDE_DETAIL;
          localStorage.setItem('project_type', pacingguide.project_type);
        } else {
          path = this.service.getOrgDetails() + '/' + Utils.ROUTE_PACING_GUIDE_AUTHORING;
        }
        this.router.navigate([path, pacingguide.project_id]);
      }).catch((ex) => {
        console.log('Error ', ex);
      });
    }
  }

  showDataInRange(event) {
    this.startIndex = event.start - 1;
    this.lastIndex = event.last;
  }

  changeView(type) {
    console.log('View type', type);
    this.viewType = type;
  }

  // ----------------for Common Data table----------------- //

  onOptionClicked(item) {
    console.log(item.event);
    switch (item.clickedOn) {
      case 'editPacingGuide':
        this.onPacingGuideEditBtnClick(item.data);
        break;
      case 'delPacingGuide':
        this.onPacingGuideDelete(item.data.project_id, item.data.project_name);
        break;
      default:
    }
  }

  userRoleRight() {
    this.createPacingGuidePermission = this.authenticateUser.AuthenticateComment('Create Pacing Guide', 'pacing_guide');
  }

  checkPacingGuideCanBeViewed(pacingGuideId, selectedStageId) {

    let canView = this.authenticateUser.AuthenticateComment('View Pacing Guide', 'pacing_guide');
    const view_project = Utils.checkProjectPermission(pacingGuideId, 'View Pacing Guide', 'pacing_guide_permissions', selectedStageId);
    if (view_project.hasProject) {
      canView = view_project.valid;
    }
    return canView;

  }

  checkPacingGuideCanCanBeEdited(pacingGuideId, selectedStageId) {

    let isEditable = this.authenticateUser.AuthenticateComment('Edit Pacing Guide', 'pacing_guide');
    const editProject = Utils.checkProjectPermission(pacingGuideId, 'Edit Pacing Guide', 'pacing_guide_permissions', selectedStageId);
    if (editProject.hasProject) {
      isEditable = editProject.valid;
    }
    return isEditable;
  }

  checkPacingGuideCanCanBeDeleted(pacingGuideId, selectedStageId) {

    let isDeletable = this.authenticateUser.AuthenticateComment('Delete Pacing Guide', 'pacing_guide');
    const editProject = Utils.checkProjectPermission(pacingGuideId, 'Delete Pacing Guide', 'pacing_guide_permissions', selectedStageId);
    if (editProject.hasProject) {
      isDeletable = editProject.valid;
    }
    return isDeletable;
  }

  /*checkPacingGuideCanCanBeClickable(pacingGuideId) {
    let isClickable = this.authenticateUser.AuthenticateComment('View Associated Nodes', 'pacing_guide');
    const editProject = Utils.checkProjectPermission(pacingGuideId, 'View Associated Nodes', 'pacing_guide_permissions',this.selectedStageId);
    if (editProject.hasProject) {
      isClickable = editProject.valid;
    }
    return isClickable;
  }*/

  // Function to set search object for data list
  getColumnFilterObj() {
    const obj = {};
    obj['project_name'] = this.searchingText;
    return obj;
  }

}
