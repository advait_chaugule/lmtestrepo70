import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  ViewChild
} from '@angular/core';
import {
  GlobalSettings
} from '../../global.settings';
import {
  CommonService
} from '../../common.service';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  TreeDataService
} from '../../tree-data.service';
import Utils from '../../utils';
import {
  SharedService
} from '../../shared.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { AutoCompleteComponent } from '../../common/auto-complete/auto-complete.component';
import {
  environment
} from '../../../environments/environment';

const CREATE_MODE = 'create';
const EDIT_MODE = 'edit';

@Component({
  selector: 'app-create-pacing-guide',
  templateUrl: './create-pacing-guide.component.html',
  styleUrls: ['./create-pacing-guide.component.scss']
})
export class CreatePacingGuideComponent implements OnInit, OnChanges {
  @Input() currentStep;
  @Input() actionMode; // 'create' or 'edit'
  @Input() pacingGuideData;
  @Output() createdEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() updatedEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() canceledEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent;
  @ViewChild('autoComplete_taxonomy', { static: false }) autoComplete_taxonomy: AutoCompleteComponent;
  workflowList: any[] = [];
  taxonomyList: any[] = [];
  pacingGuideForm: FormGroup; // form instance
  pacingGuideName: string;
  description: string;
  selectedWorkflow: any = null;
  selectedTaxonomy: any = null;
  currentPacingGuideId: any;
  taxonomyData: any = null;
  textLength = 50;
  selectedTaxonomyNodeIs: any;
  selectedParentNodeIds: any;
  addedTaxonomyList: any[];
  selectedTaxonomyNodeDetail: any[];
  selectedTaxonomyId: any;
  selectedWorkflowId: any;
  disabledAddTaxonomyNodeBtn: boolean;
  selectedTaxonomyObj: any = {
    taxonomy: null,
    nodes: null
  };
  eventObj: any = {
    'data': null,
    'msg': null,
    'action': null
  };
  addSelectionTxt = 'Add Selection';
  currentSelectedIndex = 0;
  createMode: string;
  editMode: string;
  selectType = 'single';
  uniqueId = 'workflow_id';
  uniqueId_taxo = 'document_id';
  autoCompleteTitle = 'Workflow';
  autoCompleteTitle_taxo = 'Taxonomy';
  validWorkflow = false;
  renamePacingGuide = false; // holds boolean to show if PG needs to be renamed based on environment selected
  taxonomyLoader = false; // holds loading state for taxonomy selection

  constructor(private service: CommonService,
    private treeService: TreeDataService,
    private sharedService: SharedService,
    private dialogService: ConfirmationDialogService) {
    this.pacingGuideForm = this.createFormGroup();
    this.renamePacingGuide = environment.renamePacingGuide;
  }

  ngOnInit() {
    this.disabledAddTaxonomyNodeBtn = true;
    this.createMode = CREATE_MODE;
    this.editMode = EDIT_MODE;
    this.selectedTaxonomyNodeDetail = [];
    this.addedTaxonomyList = [];
    this.resetStep();
    this.getWorkFlowList();
  }

  ngOnChanges() {
    this.actionMode = this.actionMode ? this.actionMode.toLowerCase() : this.actionMode;

    if (this.pacingGuideData && (this.actionMode === EDIT_MODE)) {
      this.pacingGuideName = this.pacingGuideData.project_name;
      this.currentPacingGuideId = this.pacingGuideData.project_id;
      this.description = this.pacingGuideData.description;
      if (this.pacingGuideData.workflow && this.pacingGuideData.workflow.workflow_id) {
        this.selectedWorkflow = {
          'workflow_id': this.pacingGuideData.workflow.workflow_id,
          'name': this.pacingGuideData.workflow.title
        };
      } else {
        this.selectedWorkflow = {
          'workflow_id': null,
          'name': null
        };
      }
      this.getPacingGuideTaxonomyList();
      this.disabledAddTaxonomyNodeBtn = true;
    }
  }

  /**
   * To get workflow list
   */
  getWorkFlowList() {
    const url = GlobalSettings.WORKFLOW_LIST;
    this.service.getServiceData(url).then((res: any) => {
      this.workflowList = [];
      if (res && res.WorkflowList && res.WorkflowList.workflows) {
        res.WorkflowList.workflows.forEach(element => {
          this.workflowList.push(element);
        });
      }
    }).catch(ex => {
      console.log('list of workflow ex ', ex);
    });
  }

  /**
   * To get taxonomy list
   */
  getAllTaxonomyList() {
    this.taxonomyList = [];
    const url = GlobalSettings.TAXONOMY_LIST;
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        res.forEach(element => {
          element.is_disabled = false;
          if (element.status !== Utils.STATUS_PUBLIC_REVIEW && element.status !== Utils.STATUS_PUBLIC_REVIEW_COPIED) {
            this.taxonomyList.push(element);
          }
        });
      }
    }).catch(ex => {
      console.log('list of taxonomies ex ', ex);
    });
  }

  getPacingGuideTaxonomyList() {
    this.taxonomyList = [];
    this.addedTaxonomyList = [];
    const url = GlobalSettings.PACING_GUIDES + '/' + this.currentPacingGuideId;
    this.service.getServiceData(url).then((res: any) => {
      if (res) {
        const documents = res['project_documents_details'];
        if (documents && documents.length > 0) {
          documents.forEach(element => {
            this.selectedTaxonomyObj = {};
            this.selectedTaxonomyObj.taxonomy = JSON.parse(JSON.stringify(element));
            this.taxonomyList.push(JSON.parse(JSON.stringify(element)));
            this.addedTaxonomyList.push(JSON.parse(JSON.stringify(this.selectedTaxonomyObj)));
          });
          this.selectedTaxonomy = documents[0];
          this.getAllNodes(this.selectedTaxonomy, 0);
        }
        console.log(this.addedTaxonomyList);
      }
    }).catch(ex => {
      console.log('list of pacing guide taxonomies ex ', ex);
    });
  }

  createFormGroup() {
    // Form structure creation
    const group = new FormGroup({
      pacingGuideName: new FormControl(null, [<any>Validators.required]),
      description: new FormControl(null),
      selectedWorkflow: new FormControl(null),
      selectedTaxonomy: new FormControl(null)
    });
    return group;
  }

  /**
   * Clicking on continue button to create pacing guide
   * @param event
   */
  onContinueClick(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.actionMode === CREATE_MODE) {
      this.onPacingGuideCreate();
    } else if (this.actionMode === EDIT_MODE) {
      this.onPacingGuideEdit();
    }
    this.sharedService.faqEvent.next({
      name: 'build_pacing'
    });
  }

  resetStep() {
    this.currentStep = 1;
  }

  /**
   * Clicking on cancel button
   */
  onCancelClick() {
    setTimeout(() => {
      this.resetStep();
      this.clearAllData();
      this.eventObj.msg = 'canceled';
      this.eventObj.data = null;
      this.eventObj.action = 'cancel';
      this.canceledEvent.emit(this.eventObj);
      if (this.autoComplete) {
        this.autoComplete.clearSelection();
      }
      if (this.autoComplete_taxonomy) {
        this.autoComplete_taxonomy.clearSelection();
      }
    }, 200);
    this.sharedService.faqEvent.next({
      name: 'pacing_guide_list'
    });
  }

  /**
   * On preview of selected workflow
   */
  onPreviewClick() {
    if (this.selectedWorkflow && this.selectedWorkflow.workflow_id) {
      this.selectedWorkflowId = this.selectedWorkflow.workflow_id;
    }
  }

  onCancelPreviewWorkflow() {
    this.selectedWorkflowId = null;
  }

  /**
   * Calling pacing guide create api
   */
  onPacingGuideCreate() {
    const url = GlobalSettings.CREATE_PACING_GUIDES,
      body = {
        project_name: this.pacingGuideName,
        description: this.description ? this.description : '',
        workflow_id: this.selectedWorkflow.workflow_id,
        project_type: 2 // For pacing guide, project_type = 2
      };
    this.service.postService(url, body).then((res: any) => {
      if (res && res.project_id) {
        this.sharedService.sucessEvent.next({
          type: 'create_pacing_guide'
        });
        this.currentPacingGuideId = res.project_id;
        this.currentStep = this.currentStep + 1;
        this.eventObj.msg = 'success';
        this.eventObj.data = res;
        this.eventObj.action = CREATE_MODE;
        this.createdEvent.emit(this.eventObj);
        // if (this.currentStep === 2) {
        //   this.sharedService.faqEvent.next({
        //     name: 'build_pacing'
        //   });
        // }
      }
    }).catch((ex) => {
      console.log('onPacingGuideCreate ex ', ex);
      this.eventObj.msg = 'failure';
      this.eventObj.data = ex;
      this.eventObj.action = CREATE_MODE;
      this.createdEvent.emit(this.eventObj);
    });
  }

  /**
   * On select taxonomy from dropdown
   * @param event
   */
  onTaxonomySelected(event) {
    this.selectedTaxonomyId = null;
    this.taxonomyData = null;
    this.addSelectionTxt = 'Add Selection';
    this.disabledAddTaxonomyNodeBtn = true;
    if (this.selectedTaxonomy && this.selectedTaxonomy.document_id) {
      this.taxonomyLoader = true;
      this.selectedTaxonomyId = this.selectedTaxonomy.document_id;
      this.selectedTaxonomyObj.taxonomy = this.selectedTaxonomy;
      const newUrl = GlobalSettings.GET_TREE_VIEW_ENHANCED + this.selectedTaxonomyId;
      this.treeService.getTreeData(newUrl, false).then((response: any) => {
        const res = response.parsedTreeNodes;
        // console.log('GET_TREE_VIEW ', res);
        // if (res && res.children) {
        //   Utils.sortData(res.children);
        // }
        this.taxonomyData = res;
        this.iterate(this.taxonomyData, 0);
        this.taxonomyLoader = false;
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
        this.taxonomyLoader = false;
      });
    } else {
      this.clearTaxonomyData();
    }
    console.log('selectedTaxonomy  ', this.selectedTaxonomy);
  }

  iterate(current, depth) {
    if (current && current.children) {
      const children = current.children;
      if (current.id) {
        current['value'] = current.id;
        current['checked'] = false;
        current['collapsed'] = depth < Utils.EXPAND_LEVEL ? false : true;
        if (this.actionMode === EDIT_MODE) {
          current['disabled'] = true;
        } else {
          current['disabled'] = false;
        }
        if (current.is_editable) {
          if (current.is_editable === 1) {
            current['checked'] = true;
            // this is for partially selected node
            if (current.is_editable === 0 || current.is_editable === 2) {
              current['checked'] = false;
            }
          } else {
            current['checked'] = false;
          }
        } else {
          current['checked'] = false;
        }
      }
      if (current.title) {
        current['text'] = current.title.substr(0, this.textLength);
      }
      if (!current.title) {
        if (current.full_statement) {

          current['text'] = current.human_coding_scheme + ' ' + current.full_statement.substr(0, this.textLength);
        } else {
          current['text'] = current.human_coding_scheme;
        }
      }
      // console.log('------------ ', current['text']);

      for (let i = 0, len = children.length; i < len; i++) {
        this.iterate(children[i], depth + 1);
      }
    }
  }

  /**
   * On select of associated nodes of selected taxonomy
   * @param obj
   */
  onTaxonomyTreeNodeSelectedEvent(obj: any) {
    console.log(' onTaxonomyTreeNodeSelectedEvent  ', obj);
    this.selectedTaxonomyNodeIs = [];
    this.selectedParentNodeIds = [];
    this.selectedTaxonomyNodeDetail = [];
    obj.partialNodes.forEach(item => {
      this.selectedParentNodeIds.push(item.id);
      this.selectedTaxonomyNodeDetail.push(item);
    });
    obj.nodes.forEach(item => {
      this.selectedTaxonomyNodeIs.push(item.id);
      this.selectedTaxonomyNodeDetail.push(item);
    });
    if (this.selectedTaxonomyNodeDetail && this.selectedTaxonomyNodeDetail.length > 0 && this.actionMode === CREATE_MODE) {
      this.disabledAddTaxonomyNodeBtn = false;
      // Utils.sortDataArray(this.selectedTaxonomyNodeDetail, 'name', false, true);
      this.selectedTaxonomyObj.nodes = this.selectedTaxonomyNodeDetail;
    } else {
      this.disabledAddTaxonomyNodeBtn = true;
      /*if (this.addSelectionTxt === 'Update Selection') {
        this.disabledAddTaxonomyNodeBtn = false;
      }*/
    }
    console.log(this.selectedTaxonomyNodeDetail);
  }

  /**
   * To add selected nodes
   * @param event
   */
  addSelectedTaxonomyTreeNode(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.selectedTaxonomyId && this.currentPacingGuideId) {
      this.callPacingguideTaxonomyMappingAPI();
    }
  }

  /**
   * Api calling for adding taxonomy nodes to pacing guide
   */
  callPacingguideTaxonomyMappingAPI() {
    const url = GlobalSettings.MAPPED_PROJECT_TAXONOMY;
    let i = 0,
      foundDocumentId = false;
    this.selectedTaxonomyNodeIs.forEach(element => {
      if (element === this.selectedTaxonomyId) {
        this.selectedTaxonomyNodeIs.splice(i, 1);
        foundDocumentId = true;
      }
      i++;
    });
    // if user select the whole tree then pass the document (taxonomy id ) as parent id
    if (foundDocumentId && this.selectedParentNodeIds.length === 0) {
      this.selectedParentNodeIds.push(this.selectedTaxonomyId);
    }

    const obj = {
      project_id: this.currentPacingGuideId,
      item_id: this.selectedTaxonomyNodeIs.join(','),
      document_id: this.selectedTaxonomyId,
      parent_id: this.selectedParentNodeIds.join(','),
      root_selected_status: foundDocumentId ? 1 : 0
    };
    this.service.postService(url, obj).then((res: any) => {
      console.log('callPacingguideTaxonomyMappingAPI  ', res);
      if (res) {
        if (this.addSelectionTxt === 'Add Selection') {
          this.addedTaxonomyList.push(this.selectedTaxonomyObj);
          this.sharedService.sucessEvent.next({
            type: 'selection_added'
          });
          this.setTaxonomyOptionDisabled(this.selectedTaxonomyId, true);
          setTimeout(() => {
            pacingGuideHeightCalculation();
          }, 200);
        } else {
          // this.addedTaxonomyList = [];
          if (this.currentSelectedIndex <= this.addedTaxonomyList.length) {
            this.addedTaxonomyList[this.currentSelectedIndex] = this.selectedTaxonomyObj;
          }
          this.sharedService.sucessEvent.next({
            type: 'selection_updated'
          });
        }
        this.selectedTaxonomyObj = {};
        console.log(this.addedTaxonomyList);
      }
      this.clearTaxonomyData(false);
    }).catch((ex) => {
      console.log('callPacingguideTaxonomyMappingAPI ex ', ex);
      this.sharedService.sucessEvent.next({
        type: 'error',
        customMsg: ex.msg ? ex.msg : 'Error occured in system.'
      });
    });
  }

  // Form validation
  checkFormValidation(): boolean {
    this.checkValidation();
    if (this.pacingGuideForm && (this.pacingGuideName && this.pacingGuideName.trim().length > 0) && this.validWorkflow) {
      return this.pacingGuideForm.valid;
    }
    return false;
  }

  clearAllData() {
    this.pacingGuideForm.reset();
    this.pacingGuideName = '';
    this.description = '';
    this.selectedWorkflow = null;
    this.addedTaxonomyList = [];
    this.clearTaxonomyData();
    if (this.autoComplete) {
      this.autoComplete.clearSelection();
    }
  }

  clearTaxonomyData(clearDisable = true) {
    this.selectedTaxonomy = null;
    this.taxonomyData = null;
    this.selectedTaxonomyObj = {};
    this.selectedTaxonomyNodeDetail = [];
    this.disabledAddTaxonomyNodeBtn = true;
    if (this.autoComplete_taxonomy) {
      this.autoComplete_taxonomy.clearSelection(clearDisable);
    }
  }

  /**
   * Get Pacing Guide Tree Data With All Nodes
   */
  getAllNodes(taxonomy, index) {
    this.selectedTaxonomy = taxonomy;
    this.selectedTaxonomyObj.taxonomy = this.selectedTaxonomy;
    this.currentSelectedIndex = index;
    const id = taxonomy.document_id;
    this.selectedTaxonomyId = id;
    if (id && this.currentPacingGuideId) {
      this.addSelectionTxt = 'Update Selection';
      const url = GlobalSettings.GET_PACINGGUIDE_ASSOCIATED_NODES + '/' + this.currentPacingGuideId + '?document_id=' + id;
      this.treeService.getTreeData(url, false).then((response: any) => {
        const res = response.parsedTreeNodes;
        // console.log('GET_TREE_VIEW ', res);
        // if (res && res.children) {
        //   Utils.sortData(res.children);
        // }
        this.taxonomyData = res;
        this.iterate(this.taxonomyData, 0);
      }).catch(ex => {
        console.log('list of taxonomies ex ', ex);
      });
    }
  }

  /**
   * To disabled selected taxonomy option
   * @param documentId (selected taxonomy id)
   */
  setTaxonomyOptionDisabled(documentId: any, disabled: boolean) {
    if (documentId) {
      this.taxonomyList.forEach(element => {
        if (element.document_id === documentId) {
          element.is_disabled = disabled;
        }
      });
    }
  }

  /**
   * Delete API for associated taxonomy nodes with pacing guide
   * @param documentId
   */
  deleteAddedTaxonomy(documentId: any) {
    if (this.actionMode === CREATE_MODE) {
      this.selectedTaxonomyId = documentId;
      this.dialogService.confirm('Confirm', 'Are you sure to delete this taxonomy?')
        .then((confirmed) => {
          if (confirmed) {
            if (documentId && this.currentPacingGuideId) {
              const url = GlobalSettings.DELETE_PACINGGUIDE_ASSOCIATED_TAXONOMY_NODES + '/' + this.currentPacingGuideId +
                '?document_id=' + documentId;
              this.service.deleteServiceData(url).then((res) => {
                this.setTaxonomyOptionDisabled(documentId, false);
                this.deleteTaxonomyFromAddedList(documentId);
                this.clearTaxonomyData();
                this.sharedService.sucessEvent.next({
                  type: 'delete_asso_taxonomy'
                });
              }).catch((ex) => {
                console.log('deleteAddedTaxonomy ', ex);
              });
            }
          } else {
            console.log('User cancel the dialog');
          }
        })
        .catch(() => {
          console.log('User dismissed the dialog');
        });
    }
  }

  /**
   * Delete added taxonomy from list
   * @param documentId
   */
  deleteTaxonomyFromAddedList(documentId: any) {
    if (this.addedTaxonomyList && this.addedTaxonomyList.length > 0) {
      this.addedTaxonomyList = this.addedTaxonomyList.filter(function (item) {
        return (item.taxonomy.document_id !== documentId);
      });
    }
    setTimeout(() => {
      pacingGuideHeightCalculation();
    }, 200);
  }

  /**
   * Calling pacing guide edit api
   */
  onPacingGuideEdit() {
    const url = GlobalSettings.CREATE_PACING_GUIDES;
    const obj = {
      project_name: this.pacingGuideName,
      workflow_id: this.selectedWorkflow.workflow_id,
      description: this.description
    };
    this.service.putService(url + '/' + this.currentPacingGuideId, obj).then((res: any) => {
      this.sharedService.sucessEvent.next({
        type: 'update_pacing_guide'
      });
      this.currentStep = this.currentStep + 1;
      this.eventObj.msg = 'success';
      this.eventObj.data = res;
      this.eventObj.action = EDIT_MODE;
      this.updatedEvent.emit(this.eventObj);
      // this.sharedService.faqEvent.next({
      //   name: 'build_pacing'
      // });
    }).catch(ex => {
      console.log('onPacingGuideEdit', ex);
      this.eventObj.msg = 'failure';
      this.eventObj.data = ex;
      this.eventObj.action = EDIT_MODE;
      this.updatedEvent.emit(this.eventObj);
    });
  }
  updateSelectedObject(event, type) {
    switch (type) {
      case 'workflow':
        this.selectedWorkflow = event;
        break;
      case 'taxonomy':
        this.selectedTaxonomy = event;
        this.onTaxonomySelected(event);
        break;
    }
  }

  onClickedOutside(e) {
    this.autoComplete.openPanel(false);
  }
  onClickedOutsideTaxonomy(e) {
    this.autoComplete_taxonomy.openPanel(false);
  }
  checkValidation() {
    if (this.selectedWorkflow) {
      this.validWorkflow = true;
    } else {
      this.validWorkflow = false;
    }
  }
}
