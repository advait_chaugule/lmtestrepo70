import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  TabComponent
} from './tab.component';
import {
  SharedService
} from '../shared.service';

describe('TabComponent', () => {

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [TabComponent],
        providers: [SharedService]

      })
      .compileComponents();
  }));

  describe(':', () => {

    function setup() {
      const fixture = TestBed.createComponent(TabComponent);
      const component = fixture.componentInstance;
      component.tabItems = ['tab1','tab2'];
      const sharedService = fixture.debugElement.injector.get(SharedService);
      return {
        fixture,
        component,
        sharedService
      };
    }

    it('should create', () => {
      const {
        component
      } = setup();
      console.log('Should Create');
      expect(component).toBeTruthy();
      
    });

    it('should select tab', () => {

      console.log('select tab');
      const { fixture, component, sharedService } = setup();
      fixture.detectChanges();
      expect(component.currentTab).toBe(component.tabItems[0]);
      component.onTabSelected(component.tabItems[1]);
      fixture.detectChanges();
      expect(component.currentTab).toBe(component.tabItems[1]);
    });
  });


});
