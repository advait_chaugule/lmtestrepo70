import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges
} from '@angular/core';
import {
  SharedService
} from '../shared.service';
import { GlobalSettings } from '../global.settings';
@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit, OnChanges {

  @Input() tabItems = [];
  @Input() counts = [];
  @Input() showCheckbox;
  @Input() enableEmail;
  @Input() currentTab;
  @Input() viewLocation;
  @Input() showVersionBtn = false;


  @Output() tabSelectedEvent: EventEmitter<any>;
  @Output() checkboxClickEvent: EventEmitter<any>;
  @Output() loadVersionSummaryEvent: EventEmitter<any>;

  constructor(private sharedService: SharedService) {
    // this.tabItems = ['Taxonomy', 'Projects', 'Activity', 'Publish'];
    this.tabSelectedEvent = new EventEmitter<any>();
    this.checkboxClickEvent = new EventEmitter<any>();
    this.loadVersionSummaryEvent = new EventEmitter<any>();
  }
  ngOnInit() {
    if (this.tabItems && this.tabItems.length > 0) {
      if (this.currentTab === undefined) {
        this.currentTab = this.tabItems[0];
      }
    }
  }

  ngOnChanges() {
    // if (this.tabItems && this.tabItems.length > 0) {
    //   // this.currentTab = this.tabItems[0];
    // }
  }

  onTabSelected(tab) {
    this.currentTab = tab;
    this.tabSelectedEvent.emit(this.currentTab.toLowerCase());
    this.sharedService.searchText.next({
      'searchText': 'clear'
    });
  }

  setOption() {
    console.log(this.enableEmail);
    this.checkboxClickEvent.next(this.enableEmail);
  }

  loadVersionReport() {
    this.loadVersionSummaryEvent.emit();
  }

}
