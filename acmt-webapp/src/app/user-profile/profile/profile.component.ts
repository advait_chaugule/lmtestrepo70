import { Component, OnInit, ViewChild } from '@angular/core';
import Utils from 'src/app/utils';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { GlobalSettings } from 'src/app/global.settings';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  GRADE_LEVEL_LEST = Utils.GRADE_LEVEL_LIST; // grade list
  SUBJECT_AREAS = Utils.SUBJECT_AREA_LIST; // subject list
  LANGUAGE_LIST = Utils.LANGUAGE_LIST; // language list
  GRADE_LEVEL_TYPE = 'gradeLevel';
  SUBJECT_TYPE = 'subject';
  LANGUAGE_TYPE = 'language';
  PRO_MODAL_TYPE = 'profile';
  PASS_MODAL_TYPE = 'password';
  // TEACHER_TYPE = 1;
  // ADMINISTRATOR_TYPE = 2;
  // HIGHER_EDUCATION_TYPE = 3;

  profileForm: FormGroup; // edit profile form
  chngPassForm: FormGroup; // change password form
  profileLoading = false; // loading state for user profile fetching
  profileUpdateLoading = false; // loading state for user profile update
  userProfileObj: any;
  firstName: string;
  lastName: string;
  email: string;
  userType: any;
  grades = [];
  subjects = [];
  languages = [];
  otherLanguage: string;
  password: string;
  newPassword: string;
  confPassword: string;
  showOtherLang = false; // whether to show other language field or not
  // Password must contain a mixture of uppercase and lowercase letters, numbers and symbols
  pwdPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,100})/; // password pattern
  dropdownItemsRoles = [];
  dropdownSettings: {};
  selectedLangs = [];
  typeOfuser: any;
  valid = true;
  showErrorMsg = false;
  constructor(private service: CommonService, private sharedService: SharedService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.profileForm = new FormGroup({
      firstName: new FormControl(null, [Validators.required, this.service.nospaceValidator]),
      lastName: new FormControl(null, [Validators.required, this.service.nospaceValidator]),
      userType: new FormControl(null),
      gradeLevels: new FormControl(null),
      subjects: new FormControl(null),
      languages: new FormControl(null),
      otherLang: new FormControl(null),
    });
    this.chngPassForm = new FormGroup({
      currentPass: new FormControl(null, [Validators.required, this.service.nospaceValidator]),
      passwords: this.formBuilder.group({
        newPass: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(12),
        Validators.pattern(this.pwdPattern), this.service.nospaceValidator]],
        confNewPass: ['', [Validators.required, this.service.nospaceValidator]],
      }, { validator: this.passwordConfirming })
    });
    this.grades = JSON.parse(JSON.stringify(this.GRADE_LEVEL_LEST));
    this.subjects = JSON.parse(JSON.stringify(this.SUBJECT_AREAS));
    this.languages = JSON.parse(JSON.stringify(this.LANGUAGE_LIST));
    this.getUserProfile(); // fetching user profile details
    this.dropdownItemsRoles = [
      {
        option: 'Are you a K-12 teacher?',
        value: "TEACHER_TYPE"
      },
      {
        option: 'Are you a K-12 administrator?',
        value: "ADMINISTRATOR_TYPE"
      },
      {
        option: 'Do you work in Higher Education?',
        value: "HIGHER_EDUCATION_TYPE"
      }
    ]
    this.dropdownSettings = {
      textField: 'label',
      showLabel: false,
      buttonType: 'plusCircle'
    };
  }
  // Password and confirm password mismatch checking validators
  private passwordConfirming(control: AbstractControl): { mismatch: boolean } {
    if (control.get('newPass').value !== control.get('confNewPass').value) {
      return { mismatch: true };
    }
  }

  // To fetch logged in user's profile data
  getUserProfile() {
    const url = GlobalSettings.USER_PROFILE_URL;
    this.profileLoading = true;
    this.service.getServiceData(url).then((res: any) => {
      this.userProfileObj = JSON.parse(JSON.stringify(res));
      this.setProfileInfoInVar(); // setting response data into local variables and formgroup
      this.profileLoading = false;
    }).catch(ex => {
      this.profileLoading = false;
      console.log('getUserProfile ex ', ex);
    });
  }

  // To update profile info
  editProfile() {
    if (this.profileForm.valid) { // valid form submission
      this.profileUpdateLoading = true;
      const url = GlobalSettings.USER_PROFILE_URL;
      // if (this.userType && (parseInt(this.userType, 10) !== this.TEACHER_TYPE)) {
      //   // for user type selection other than teacher, clear grades and subjects
      //   this.profileForm.controls['gradeLevels'].setValue('');
      //   this.profileForm.controls['subjects'].setValue('');
      // }
      this.dropdownItemsRoles.forEach(element => {
        const typeOfuser = this.profileForm.controls['userType'].value;
        const index = this.dropdownItemsRoles.indexOf(element) + 1;
        if (element.value === typeOfuser.value) {
          this.profileForm.controls['userType'].setValue(index);
        }
      });
      const body = {
        first_name: this.profileForm.controls['firstName'].value,
        last_name: this.profileForm.controls['lastName'].value,
        reg_user_type: this.profileForm.controls['userType'].value,
        reg_teacher_grades: this.profileForm.controls['gradeLevels'].value,
        reg_teacher_subjects: this.profileForm.controls['subjects'].value,
        reg_admin_languages: this.profileForm.controls['languages'].value,
        reg_admin_other_languages: this.profileForm.controls['otherLang'].value
      };
      this.service.putService(url, body, 0, true).then((res: any) => {
        if (res) {
          // const response = res.data;
          if (res.success) { // success response
            this.updateLocalProfileObj(); // update local profile object after update from api response
            this.setNameInOtherPlaces(); // event triggering for affecting changed name in other places like localstorage, navbar component
            this.showMessage(res.message ? res.message.toString() : res.message);
            this.onCancel();
            this.closeModal(this.PRO_MODAL_TYPE); // close opened modal
          } else { // error
            this.showMessage((res.message ? res.message.toString() : res.message), 'error');
            this.setCheckboxInfo(this.GRADE_LEVEL_TYPE, this.userProfileObj.reg_teacher_grades);
            this.setCheckboxInfo(this.SUBJECT_TYPE, this.userProfileObj.reg_teacher_subjects);
          }
        }
        this.profileUpdateLoading = false;
      }).catch((ex) => {
        this.showMessage((ex.msg ? ex.msg.toString() : 'Failed to update profile. Please try later.'), 'error');
        this.profileUpdateLoading = false;
        this.setCheckboxInfo(this.GRADE_LEVEL_TYPE, this.userProfileObj.reg_teacher_grades);
        this.setCheckboxInfo(this.SUBJECT_TYPE, this.userProfileObj.reg_teacher_subjects);
      });
    } else {
      this.showMessage('Please fill out required fields.', 'error');
    }
  }

  // Change password api calling method
  editPassword() {
    if (this.chngPassForm.valid) { // valid form submission
      this.profileUpdateLoading = true;
      const url = GlobalSettings.USER_PASSWORD_URL;
      const body = {
        currentPassword: this.password,
        newPassword: this.newPassword,
        confirmNewPassword: this.confPassword
      };
      this.service.putService(url, body, 0, true).then((res: any) => {
        if (res) {
          // const response = res.data;
          if (res.success) { // success response
            this.showMessage(res.message ? res.message.toString() : res.message);
            this.onCancel();
            this.closeModal(this.PASS_MODAL_TYPE);
          } else { // error
            this.showMessage((res.message ? res.message.toString() : res.message), 'error');
          }
        }
        this.profileUpdateLoading = false;
      }).catch((ex) => {
        this.showMessage((ex.msg ? ex.msg.toString() : 'Failed to update password. Please try later.'), 'error');
        this.profileUpdateLoading = false;
      });
    } else {
      this.showMessage('Please fill out all valid inputs.', 'error');
    }
  }

  // On edit profile click
  onEditBtnClick() {
    this.setProfileInfoInVar();
    setTimeout(() => {
      if (document.getElementById('closeeditProfile')) {
        document.getElementById('closeeditProfile').focus();
      }
    }, 500);
  }

  // On change password option click
  onChangePasswordBtnClick() {
    setTimeout(() => {
      if (document.getElementById('closechangePassword')) {
        document.getElementById('closechangePassword').focus();
      }
    }, 500);
  }
  // To set profile information in local variables and formgroup, from response object
  setProfileInfoInVar() {
    if (this.userProfileObj) {
      this.showOtherLang = false;
      this.firstName = this.userProfileObj.first_name;
      this.lastName = this.userProfileObj.last_name;
      this.otherLanguage = this.userProfileObj.reg_admin_other_languages;
      if (this.otherLanguage && this.otherLanguage.length > 0) { // If other language found, show text field for other language
        this.showOtherLang = true;
      }
      this.userType = this.userProfileObj.reg_user_type;
      this.dropdownItemsRoles.forEach(element => {
        const index = this.dropdownItemsRoles.indexOf(element) + 1;
        if (index === this.userProfileObj.reg_user_type && this.userProfileObj.reg_user_type !== undefined) {
          this.userType = element;
          this.typeOfuser = element.value;
        }
      });
      this.email = this.userProfileObj.email;
      this.setCheckboxInfo(this.GRADE_LEVEL_TYPE, this.userProfileObj.reg_teacher_grades);
      this.setCheckboxInfo(this.SUBJECT_TYPE, this.userProfileObj.reg_teacher_subjects);
      this.setCheckboxInfo(this.LANGUAGE_TYPE, this.userProfileObj.reg_admin_languages);
      this.profileForm.controls['firstName'].setValue(this.firstName);
      this.profileForm.controls['lastName'].setValue(this.lastName);
      this.profileForm.controls['userType'].setValue(this.userType);
      this.profileForm.controls['otherLang'].setValue(this.otherLanguage);
      this.userProfileObj.initialNames = this.getInitialNames(this.firstName, this.lastName);
    }
  }

  // To update local response profile object from formgroup after profile update successfully
  updateLocalProfileObj() {
    this.userProfileObj.first_name = this.profileForm.controls['firstName'].value;
    this.userProfileObj.last_name = this.profileForm.controls['lastName'].value;
    this.userProfileObj.reg_user_type = this.profileForm.controls['userType'].value;
    this.userProfileObj.reg_teacher_grades = this.profileForm.controls['gradeLevels'].value;
    this.userProfileObj.reg_teacher_subjects = this.profileForm.controls['subjects'].value;
    this.userProfileObj.reg_admin_languages = this.profileForm.controls['languages'].value;
    this.userProfileObj.reg_admin_other_languages = this.profileForm.controls['otherLang'].value;
    this.userProfileObj.initialNames = this.getInitialNames(this.userProfileObj.first_name, this.userProfileObj.last_name);
  }

  // To set updated first name and last name in other places like local storage, navbar component etc.
  setNameInOtherPlaces() {
    localStorage.setItem('first_name', this.userProfileObj.first_name);
    localStorage.setItem('last_name', this.userProfileObj.last_name);
    this.sharedService.profileNameChangeEvent.next({
      firstName: this.userProfileObj.first_name,
      lastName: this.userProfileObj.last_name
    });
  }

  // To get initial letters of first name and last name
  getInitialNames(firstName: string, lastName: string) {
    return Utils.getInitialNames(firstName, lastName);
  }

  /**
   * To set checked true/false based on selected values for checkbox type array list
   * @param type (checkbox type)
   * @param values (selected values as string, including comma separated for multiple values)
   */
  setCheckboxInfo(type: string, values: string) {
    const selectedValues = values.split(',');
    if (type === this.GRADE_LEVEL_TYPE) {
      this.grades.forEach((grade: any) => {
        grade.checked = false;
        if (selectedValues.find((val: any) => grade.id === val)) {
          grade.checked = true;
        }
      });
      this.profileForm.controls['gradeLevels'].setValue(values);
    } else if (type === this.SUBJECT_TYPE) {
      this.subjects.forEach((sub: any) => {
        sub.checked = false;
        if (selectedValues.find((val: any) => sub.id === val)) {
          sub.checked = true;
        }
      });
      this.profileForm.controls['subjects'].setValue(values);
    } else if (type === this.LANGUAGE_TYPE) {
      const defaultSelectedLang = [];
      this.languages.forEach((lang: any) => {
        lang.is_selected = 0;
        if (selectedValues.find((val: any) => lang.id === val)) {
          lang.is_selected = 1;
          if (lang.label === 'Other language') {
            this.showOtherLang = true;
          }
          defaultSelectedLang.push(lang);
        }
      });
      this.selectedLangs = defaultSelectedLang;
      this.profileForm.controls['languages'].setValue(values);
    }
  }

  // On check box click, update form group also
  onCheckboxClick(type: string) {
    const values = [];
    if (type === this.GRADE_LEVEL_TYPE) {
      this.grades.forEach((grade: any) => {
        if (grade.checked) {
          values.push(grade.id);
        }
      });
      this.profileForm.controls['gradeLevels'].setValue(values.toString());
    } else if (type === this.SUBJECT_TYPE) {
      this.subjects.forEach((sub: any) => {
        if (sub.checked) {
          values.push(sub.id);
        }
      });
      this.profileForm.controls['subjects'].setValue(values.toString());
    } else if (type === this.LANGUAGE_TYPE) {
      this.languages.forEach((lang: any) => {
        if (lang.is_selected) {
          values.push(lang.id);
        }
      });
      this.profileForm.controls['languages'].setValue(values.toString());
    }
  }

  // If other language option unchecked, clear language field
  onOthrLangCheck() {
    if (!this.showOtherLang) {
      this.otherLanguage = '';
      this.profileForm.controls['otherLang'].setValidators(null);
      this.profileForm.controls['otherLang'].setValue('');
    } else {
      this.profileForm.controls['otherLang'].setValidators([Validators.required, this.service.nospaceValidator]);
      setTimeout(() => {
        if (document.getElementById('other-lang-field')) {
          document.getElementById('other-lang-field').focus();
        }
      }, 150);
    }
    this.profileForm.controls['otherLang'].updateValueAndValidity();
  }

  onCancel(event?: any) {
    this.profileForm.reset();
    this.chngPassForm.reset();
    this.clearFormValues();
    this.showOtherLang = false;
    this.getUserProfile();
  }

  clearFormValues() {
    this.firstName = '';
    this.lastName = '';
    this.otherLanguage = '';
    this.userType = '';
    this.grades = JSON.parse(JSON.stringify(this.GRADE_LEVEL_LEST));
    this.subjects = JSON.parse(JSON.stringify(this.SUBJECT_AREAS));
    this.languages = JSON.parse(JSON.stringify(this.LANGUAGE_LIST));
    this.password = '';
    this.newPassword = '';
    this.confPassword = '';
  }

  // modal close manually
  closeModal(type: string) {
    if (type === this.PRO_MODAL_TYPE) {
      setTimeout(() => {
        if (document.getElementById('closeeditProfile')) {
          document.getElementById('closeeditProfile').click();
        }
      }, 150);
    } else if (type === this.PASS_MODAL_TYPE) {
      setTimeout(() => {
        if (document.getElementById('closechangePassword')) {
          document.getElementById('closechangePassword').click();
        }
      }, 150);
    }
  }

  showMessage(msg: string, msgType?: string) {
    if (msgType) {
      this.sharedService.sucessEvent.next({
        customMsg: Utils.getFirstCapitalLetterString(msg),
        type: msgType
      });
    } else {
      this.sharedService.sucessEvent.next({
        customMsg: Utils.getFirstCapitalLetterString(msg)
      });
    }
  }
  // On checkbox button click for user type, set selected value in local variable
  onDropdownclick() {
    const typeOfuser = this.profileForm.controls['userType'].value;
    this.typeOfuser = typeOfuser.value;
  }
  onCheckboxClickOption(event, type) {
    this.selectedLangs = event.selectedData;
    this.onCheckboxClick(type);
    // let otherLangFound = false;
    this.showOtherLang = false;
    if (event.selectedData.length > 0) {
      event.selectedData.forEach(element => {
        if (element.label === 'Other language' && element.is_selected === 1) {
          this.showOtherLang = true;
          // otherLangFound = true;
        }
      });
    } else {
      this.showOtherLang = false;
    }
    this.onOthrLangCheck();
    this.checkValidation();
  }

  checkValidation() {
    this.languages.forEach(obj => {
      if (obj.value === 'other' && obj.is_selected === 1) {
        if (this.otherLanguage.length > 0) {
          this.valid = true;
          this.showErrorMsg = false;
        } else {
          this.showErrorMsg = true;
          this.valid = false;
        }
      } else {
        this.showErrorMsg = false;
        this.valid = true;
      }
    });

  }
}
