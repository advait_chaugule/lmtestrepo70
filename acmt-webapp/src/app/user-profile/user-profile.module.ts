import {
    NgModule,
    NO_ERRORS_SCHEMA
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    UserProfileRoutingModule
} from './user-profile-routing.module';
import {
    SharedModule
} from '../shared/shared.module';
import {
    PublicModule
} from '../shared/public.module';
import {
    Shared2Module
} from '../shared/shared2/shared2.module';
import {
    Shared3Module
} from '../shared/shared3/shared3.module';
import { PreLoaderModule } from '../shared/preloader/pre-loader.module';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
    imports: [
        CommonModule,
        UserProfileRoutingModule,
        SharedModule,
        Shared2Module,
        Shared3Module,
        PublicModule,
        PreLoaderModule
    ],
    declarations: [
        ProfileComponent,
    ],
    exports: [SharedModule],
    schemas: [NO_ERRORS_SCHEMA]
})
export class UserProfileModule { }
