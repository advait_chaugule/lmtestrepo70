import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeAccordianComponent } from './tree-accordian.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedService } from '../shared.service';

describe('TreeAccordianComponent', () => {
  let component: TreeAccordianComponent;
  let fixture: ComponentFixture<TreeAccordianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TreeAccordianComponent],
      providers: [SharedService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeAccordianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create TreeAccordianComponent', () => {
    expect(component).toBeTruthy();
  });
});
