import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontFacingNavBarComponent } from './front-facing-nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedService } from 'src/app/shared.service';

describe('FrontFacingNavBarComponent', () => {
  let component: FrontFacingNavBarComponent;
  let fixture: ComponentFixture<FrontFacingNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrontFacingNavBarComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [SharedService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontFacingNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create FrontFacingNavBarComponent', () => {
    expect(component).toBeTruthy();
  });
});
