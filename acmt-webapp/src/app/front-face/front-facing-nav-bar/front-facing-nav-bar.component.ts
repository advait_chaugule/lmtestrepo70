import {
  Component,
  OnInit,
  AfterViewInit,
  AfterViewChecked,
  AfterContentInit,
  AfterContentChecked,
  OnDestroy
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  HttpClient
} from '@angular/common/http';
import {
  SharedService
} from '../../shared.service';
import {
  NavigationEnd,
  Router,
  ActivatedRoute,
  Params,
  NavigationStart
} from '@angular/router';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  environment
} from '../../../environments/environment';
@Component({
  selector: 'app-front-facing-nav-bar',
  templateUrl: './front-facing-nav-bar.component.html',
  styleUrls: ['./front-facing-nav-bar.component.scss', './../../nav-bar/nav-bar-skip-main.scss']
})
export class FrontFacingNavBarComponent implements OnInit, AfterViewChecked, OnDestroy {

  isPublic = false;
  title = ''; // holds the title to be displayed based on route path
  selectedTopMenu = ''; // holds the selected tab based on route path
  currentURL = ''; // holds the current url
  taxonomyTitleEvent: Subscription;
  showPublishedTaxonomy = true; // holds boolean to show if published taxonomy needs to be viewed based on environment selected

  navBarItems = [];
  orgCode = '';
  constructor(private http: HttpClient, private router: Router, private sharedService: SharedService) {
    this.getNavJson();
    if (localStorage.getItem('orgDetails')) {
      this.orgCode = JSON.parse(localStorage.getItem('orgDetails'))['orgCode'];
    }

    // this.sharedService.orgDetails.subscribe((event: any) => {
    //   if (event.orgCode !== undefined) {
    //     this.orgCode = event.orgCode;
    //   }
    // });
  }

  getNavJson() {
    this.navBarItems = [];
    this.getJSON().subscribe(data => {
      this.navBarItems = data;
      this.navBarItems.forEach(navItem => {
        navItem.active = false;
        if (!this.showPublishedTaxonomy && navItem.name === 'Taxonomies') {
          this.navBarItems.splice(this.navBarItems.indexOf(navItem), 1);
        }
      });
      // console.log('####', this.navBarItems);
    });
  }


  ngOnInit() {
    // for the taxonomy details heading
    console.log('#### front face nav bar initialised');
    for (const i in environment.tenant) {
      if (i && this.router.url.split('/')[2] === environment.tenant[i]) {
        this.showPublishedTaxonomy = false;
        break;
      } else {
        this.showPublishedTaxonomy = environment.showPublishedTaxonomy;
      }
    }
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: any) => {

      if (localStorage.getItem('orgDetails')) {
        this.orgCode = JSON.parse(localStorage.getItem('orgDetails'))['orgCode'];
      }
      this.currentURL = event.url;
      const url = event.url;
      switch (url) {
        case '/org/' + this.orgCode:
          this.title = '';
          this.isPublic = true;
          this.selectedTopMenu = this.title;
          break;
        case '/org/' + this.orgCode + '/home':
          this.title = '';
          this.isPublic = true;
          this.selectedTopMenu = this.title;
          break;
        case '/org/' + this.orgCode + '/taxonomies':
          this.title = 'Taxonomies';
          this.isPublic = true;
          this.selectedTopMenu = this.title;
          break;
        case '/org/' + this.orgCode + '/pacingguides':
          this.title = 'Pacing Guide';
          this.isPublic = true;
          this.selectedTopMenu = this.title;
          break;
        case '/org/' + this.orgCode + '/documents':
          this.title = 'Documents';
          this.isPublic = true;
          this.selectedTopMenu = this.title;
          break;
        default:
          console.log('default');
          this.isPublic = false;
          break;
      }
      document.title = 'ACMT : ' + this.title;
      if (this.currentURL.indexOf('/taxonomies/detail') >= 0) {
        this.taxonomyTitleEvent = this.sharedService.setTitleEvent.subscribe((titleEvent: any) => {
          if (titleEvent && titleEvent.title) {
            switch (titleEvent.type) {
              case 'taxonomy':
                this.title = titleEvent.title;
                break;
              default:
                break;
            }
          }
        });
      }
    });

    // this.router.navigate(['/home']);
    if (sessionStorage.getItem('frontface_url')) {
      setTimeout(() => {
        const pathArr = sessionStorage.getItem('frontface_url').split('/');
        const path = pathArr[3];
        if (path === 'taxonomies') {
          if (document.getElementById('taxonomies-button')) {
            document.getElementById('taxonomies-button').click();
          }
        } else if (path === 'pacingguides') {
          if (document.getElementById('pacingguides-button')) {
            console.log('$$$$ id to click', path);
            document.getElementById('pacingguides-button').click();
          }
        } else if (path === 'documents') {
          if (document.getElementById('documents-button')) {
            document.getElementById('documents-button').click();
          }
        } else {
          if (document.getElementById('Home-button')) {
            document.getElementById('Home-button').click();
          }
        }
      }, 150);
    } else {
      setTimeout(() => {
        if (document.getElementById('Home-button')) {
          // document.getElementById('Home-button').click();
        }
      }, 1000);
    }
    // sessionStorage.setItem('frontface_url', this.router.url);
    if (this.router.url !== '/') {
      sessionStorage.setItem('frontface_url', this.router.url);
    }
  }

  ngAfterViewChecked() {
    this.sharedService.activeLink.next({
      'activeLink': true
    });

  }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/json/front-facing-nav.json');
  }

  navigate(item) {
    this.getNavJson();
    if (localStorage.getItem('orgDetails')) {
      this.orgCode = JSON.parse(localStorage.getItem('orgDetails'))['orgCode'];
    }
    if (item.path !== 'login') {
      const nav = [];
      const path = 'org/' + this.orgCode + '/' + item.path;
      // console.log('#### path front-facing-nav-bar', path);
      // console.log('#### this.navBarItems', this.navBarItems);
      setTimeout(() => {
        if (this.navBarItems && this.navBarItems.length > 0) {
          this.navBarItems.forEach(navItem => {
            if (navItem.path === item.path) {
              navItem.active = true;
            } else {
              navItem.active = false;
            }
          });
        }
      }, 160);
      nav.push('/' + path);
      this.router.navigate(nav);
      // sessionStorage.setItem('frontface_url', this.router.url);
      if (this.router.url !== '/') {
        sessionStorage.setItem('frontface_url', this.router.url);
      }
    } else {
      const nav = [];
      const path = 'org/' + this.orgCode + '/login';
      nav.push('/' + path);
      this.router.navigate(nav);
      // sessionStorage.setItem('frontface_url', this.router.url);
      if (this.router.url !== '/') {
        sessionStorage.setItem('frontface_url', this.router.url);
      }
    }
  }

  onHelpClick() {

  }

  ngOnDestroy() {
    if (this.taxonomyTitleEvent) {
      this.taxonomyTitleEvent.unsubscribe();
    }
  }

  skipToMain() {
    if (document.getElementById('mainContent')) {
      document.getElementById('mainContent').focus();
    }
  }
}
