import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontFacingTaxonomyDetailComponent } from './front-facing-taxonomy-detail.component';
import { CommonService } from 'src/app/common.service';
import { TreeDataService } from 'src/app/tree-data.service';
import { SharedService } from 'src/app/shared.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';

describe('FrontFacingTaxonomyDetailComponent', () => {
  let component: FrontFacingTaxonomyDetailComponent;
  let fixture: ComponentFixture<FrontFacingTaxonomyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrontFacingTaxonomyDetailComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [CommonService, TreeDataService, SharedService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontFacingTaxonomyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
