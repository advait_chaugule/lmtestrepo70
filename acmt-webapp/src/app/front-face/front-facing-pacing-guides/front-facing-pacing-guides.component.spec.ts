import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontFacingPacingGuidesComponent } from './front-facing-pacing-guides.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('FrontFacingPacingGuidesComponent', () => {
  let component: FrontFacingPacingGuidesComponent;
  let fixture: ComponentFixture<FrontFacingPacingGuidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrontFacingPacingGuidesComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontFacingPacingGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create FrontFacingPacingGuidesComponent', () => {
    expect(component).toBeTruthy();
  });
});
