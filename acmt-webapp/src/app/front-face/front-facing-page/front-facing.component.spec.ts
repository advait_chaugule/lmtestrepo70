import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontFacingComponent } from './front-facing.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';

describe('FrontFacingComponent', () => {
  let component: FrontFacingComponent;
  let fixture: ComponentFixture<FrontFacingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrontFacingComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, SharedService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontFacingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create FrontFacingComponent', () => {
    expect(component).toBeTruthy();
  });
});
