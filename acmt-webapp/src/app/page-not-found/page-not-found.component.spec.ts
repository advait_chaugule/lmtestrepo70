import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  PageNotFoundComponent
} from './page-not-found.component';
import {
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  RouterTestingModule
} from '@angular/router/testing';
describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageNotFoundComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear localstorage ', () => {
    localStorage.setItem('test', 'test');
    component.onClick();
    expect(localStorage.getItem('test')).toBeNull();
  });
});
