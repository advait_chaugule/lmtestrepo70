import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  TreeTabComponent
} from './tree-tab.component';
import {
  SharedService
} from '../shared.service';

describe('TreeTabComponent', () => {
  let component: TreeTabComponent;
  let fixture: ComponentFixture < TreeTabComponent > ;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [TreeTabComponent],
        providers: [SharedService]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeTabComponent);
    component = fixture.componentInstance;
    component.treeTabItems = ['treeview', 'tableview'];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select tree tab', () => {
    component.onTreeTabSelected('treeview');
    expect(component.currentTreeTab).toBe('treeview');
  });

  it('should emit  current selected tree tab', () => {
    let emitedvalue = null;
    component.treeTabSelectedEvent.subscribe((value) => {
      emitedvalue = value;
    });
    component.onTreeTabSelected(component.treeTabItems[0]);
    fixture.detectChanges();
    expect(emitedvalue).toBe(component.treeTabItems[0]);
  });

  it('should emit give type', () => {
    let emitedvalue = null;
    component.exportTaxonomy.subscribe((value) => {
      emitedvalue = value;
    });
    component.onExportClick('treeview');
    fixture.detectChanges();
    expect(emitedvalue).toBe('treeview');

  });
});
