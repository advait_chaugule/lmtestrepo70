import {
  Injectable
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse
} from '@angular/common/http';
import {
  GlobalSettings
} from '../global.settings';

import {
  CommonService
} from '../../../src/app/common.service';

export class User {
  email: string;
  password: any;
  accountid: any;
  refreshtoken: any;
  accesstoken: any;
  loginFromCookie: boolean;

  constructor(public _email: string,
    public _password: string,
    public _accountid: string,
    public _refreshtoken: string,
    public _accesstoken: string,
    public _loginfromcookie: boolean) {
    this.email = _email;
    this.password = _password;
    this.accountid = _accountid;
    this.refreshtoken = _refreshtoken;
    this.accesstoken = _accesstoken;
    this.loginFromCookie = _loginfromcookie;
  }
}


@Injectable()
export class LoginService {
  public user: User;
  public authdetail: any;
  constructor(private _router: Router, private http: HttpClient, private service: CommonService) {

  }

  navigateTo(path) {
    this._router.navigate(['home/' + path]);
  }

  login(user: User) {
    this.user = user;
    if (localStorage.getItem('orgDetails')) {
      this.user['organization_id'] = JSON.parse(localStorage.getItem('orgDetails'))['orgId'];
    }
    const url =  this.service.baseURL + GlobalSettings.LOGIN_URL,
      headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
      options = {
        headers: headers
      },
      self = this;
      console.log('login ',url);
    return new Promise((resolve, reject) => {
      
      this.http.post(url, this.user)
        .subscribe(
          (response: any) => {
            console.log('loing111', response);
            if (response !== null) {
              const data: any = response;
              console.log('loing ', response);
              if (data.status === 200) {
                console.log('active_access_token ', data);

                if (data.message === 'Login successfull.') {
                  localStorage.setItem('access_token', data.data.active_access_token);
                  localStorage.setItem('user_id', data.data.user_id);
                  localStorage.setItem('role_id', data.data.system_role.role_id);
                  localStorage.setItem('first_name', data.data.first_name);
                  localStorage.setItem('last_name', data.data.last_name);
                  localStorage.setItem('is_super_admin', data.data.is_super_admin);
                  localStorage.setItem('system_role_code', data.data.system_role.role_code);
                  localStorage.setItem('org_name', data.data.organization.name);
                  // localStorage.setItem('system_role_permissions', data.data.system_role_permissions.role_user_permissions);
                  console.log('loing===>', data.data.system_role_permissions.role_user_permissions);
                  this.service.role_user_permissions = data.data.system_role_permissions.role_user_permissions;
                  this.service.project_permissions = data.data.system_role_permissions.project_permissions;
                  this.service.taxonomy_permissions = data.data.system_role_permissions.taxonomy_permissions;
                  this.service.workflow_permissions = data.data.system_role_permissions.workflow_permissions;
                  this.service.metadata_permissions = data.data.system_role_permissions.metadata_permissions;
                  this.service.note_permissions = data.data.system_role_permissions.note_permissions;
                  this.service.comment_permissions = data.data.system_role_permissions.comment_permissions;
                  this.service.node_template_permissions = data.data.system_role_permissions.node_template_permissions;
                  this.service.notification_permissions = data.data.system_role_permissions.notification_permissions;
                  localStorage.setItem('project_role_permissions', JSON.stringify(data.data.project_role_permissions));
                  resolve(response.data);
                }
              } else {
                reject(data.message);
              }
            }
          }, error => {

            reject(error);
          }
        );
    });

  }


  logout() {
    // localStorage.clear();
    const body = {
      'accountid': localStorage.getItem('account_id'),
      'refreshtoken': localStorage.getItem('refresh_token')
    },
      url =  this.service.baseURL + GlobalSettings.LOGOUT_URL + '/' + body.accountid + '/token/' + body.refreshtoken,
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token'),
        'refreshtoken': localStorage.getItem('refresh_token'),
        'accountid': localStorage.getItem('account_id')
      }),
      options = {
        headers: headers
      },
      self = this;
    return new Promise((resolve, reject) => {
      this.http.post(url, body, options)
        .subscribe(
          response => {
            if (response !== null) {
              const data: any = response;
              if (data.error) {

                reject(data);
              } else {

                resolve();
              }
            }
          }, error => {
            reject(error);
          }
        );
    });
  }

}
