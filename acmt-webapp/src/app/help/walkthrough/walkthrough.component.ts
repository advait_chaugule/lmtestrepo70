/*import {
  Component,
  OnInit,
  AfterViewInit
} from '@angular/core';
import {
  NgxBootstrapProductTourService
} from 'ngx-bootstrap-product-tour';
@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.component.html',
  styleUrls: './walkthrough.component.scss'
})
export class WalkthroughComponent implements OnInit, AfterViewInit {
  totalSteps = 0;
  constructor(private tourService: NgxBootstrapProductTourService) { }

  ngOnInit() { }
  ngAfterViewInit() {
    this.tourService.initialize$.subscribe((steps: any[]) => {
      this.totalSteps = steps.length;
    });
  }

  // Clicking on next button
  clickOnNextButton() {
    this.tourService.next();
    setTimeout(() => {
      if (document.getElementById('prevTourBtn')) {
        document.getElementById('prevTourBtn').focus();
      }
    }, 200);
  }

  // Clicking on previous button
  clickOnPrevButton() {
    this.tourService.prev();
    setTimeout(() => {
      if (this.tourService.hasNext(this.tourService.currentStep) && this.tourService.hasPrev(this.tourService.currentStep)) {
        if (document.getElementById('prevTourBtn')) {
          document.getElementById('prevTourBtn').focus();
        }
      } else {
        if (document.getElementById('nextTourBtn')) {
          document.getElementById('nextTourBtn').focus();
        }
      }
    }, 200);
  }

}
*/