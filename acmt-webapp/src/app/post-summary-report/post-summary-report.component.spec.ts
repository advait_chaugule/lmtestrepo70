import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSummaryReportComponent } from './post-summary-report.component';

describe('PostSummaryReportComponent', () => {
  let component: PostSummaryReportComponent;
  let fixture: ComponentFixture<PostSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
