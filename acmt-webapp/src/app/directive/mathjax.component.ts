import { Component, ElementRef, OnChanges, Input } from '@angular/core';

declare var MathJax: {
    Hub: {
        Queue: (param: Object[]) => void;
    }
};

@Component({
    selector: '[mathJax]',
    template: ''
})
export class MathJaxComponent implements OnChanges {

    @Input() mathJax: string;

    constructor(private element: ElementRef) { }

    ngOnChanges() {
        setTimeout(() => {
            const de = /{{ '{' }}/gi;
            this.element.nativeElement.innerHTML = !!this.mathJax ? this.mathJax.replace(de, "{") : '';
            MathJax.Hub.Queue(['Typeset', MathJax.Hub, this.element.nativeElement]);
        }, 300);
    }
}