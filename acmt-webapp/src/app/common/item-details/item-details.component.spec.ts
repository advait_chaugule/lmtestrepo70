import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  ItemDetailsComponent
} from './item-details.component';
import {
  RouterTestingModule
} from '@angular/router/testing';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  AuthenticateUserService
} from 'src/app/authenticateuser.service';
import {
  CommonService
} from 'src/app/common.service';
import {
  ConfirmationDialogService
} from 'src/app/confirmation-dialog/confirmation-dialog.service';
import {
  SharedService
} from 'src/app/shared.service';
import {
  ActivatedRoute
} from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

describe('ItemDetailsComponent', () => {
  let component: ItemDetailsComponent;
  let fixture: ComponentFixture<ItemDetailsComponent>;

  const fakeActivatedRoute = {
    params: {
      id: '6c203f9e-597a-4cd2-8bed-6d3641a24984',
      subscribe: jasmine.createSpy('subscribe')
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ItemDetailsComponent
      ],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
      providers: [AuthenticateUserService, CommonService, ConfirmationDialogService, SharedService,
        {
          provide: ActivatedRoute,
          useValue: fakeActivatedRoute
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
