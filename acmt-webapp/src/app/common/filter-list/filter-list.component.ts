import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
  OnChanges
} from '@angular/core';
import {
  isNullOrUndefined
} from 'util';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html'
})
export class FilterListComponent implements OnInit, OnChanges {

  @Input() listToFilter = [];
  @Input() filterOptions = []; // holds filter dropdown list
  @Input() selectedFilterOption; // selected filter option
  @Input() filterAttribute; // attribute name of object from list to be filtered
  @Output() filteredList: EventEmitter < any > ; // emits the selected object from the dropdown
  @Input() labelFor: string;

  constructor() {
    this.filteredList = new EventEmitter < any > ();
  }

  ngOnInit() {
    if (isNullOrUndefined(this.selectedFilterOption)) {
      if (this.filterOptions.length > 0) {
        this.selectedFilterOption = this.filterOptions[0].name; // default selection to first option
      } else {
        this.selectedFilterOption = 'No option available'; // If no option list found
      }
    }
  }

  ngOnChanges() {
    if (this.selectedFilterOption && this.listToFilter && this.listToFilter.length > 0) {
      const optionValue = this.findValueByName(this.selectedFilterOption);
      this.filterData(this.selectedFilterOption, optionValue);
    }
  }

  /**
   * To filter list based on filter option
   * @param optionName (filter option name)
   * @param optionValue (filter option value)
   */
  filterData(optionName, optionValue) {
    if (document.getElementById('filterByBtn')) {
      document.getElementById('filterByBtn').focus();
    }
    this.selectedFilterOption = optionName;
    let filteredData = [];
    if (optionName !== 'All') {
      if (this.listToFilter && this.listToFilter.length > 0) {
        filteredData = this.listToFilter.filter(obj => {
          return obj[this.filterAttribute] === optionValue;
        });
        // console.log('filteredData ####', filteredData);
      }
    } else {
      filteredData = this.listToFilter;
    }
    this.filteredList.emit(filteredData);
  }

  /**
   * To find option value to respective filter name from filter options
   * @param name
   */
  findValueByName(name) {
    for (let i = 0; i < this.filterOptions.length; i++) {
      if (this.filterOptions[i].name === name) {
        return this.filterOptions[i].value;
      }
    }
  }

}
