import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionComponent } from './accordion.component';
import { SharedService } from 'src/app/shared.service';

describe('AccordionComponent', () => {
  let component: AccordionComponent;
  let fixture: ComponentFixture<AccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionComponent],
      providers: [SharedService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionComponent);
    component = fixture.componentInstance;
    component.title = 'test title';
    component.dataLength = 2;
    component.accordionNo = 2;
    component.expanded = false;
    component.addModalId = 'test_modal';
    component.showAddButton = true;
    component.currentTreeTab = 'TreeTab';
    fixture.detectChanges();
  });

  it('should create accordion component', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger the expand/collapse button click', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.btn.btn-clear.arrow-toggle');
    const event = {
      target: button
    };
    component.onArrowClicked(event);
    fixture.detectChanges();
    if (button.getAttribute('aria-expanded') === 'true' || button.getAttribute('aria-expanded') === true) {
      expect(component.expanded).toBe(true);
    } else {
      expect(component.expanded).toBe(false);
    }
  });

  it('should trigger the add button click', () => {
    const event = 'add-association';
    component.onAddButtonClick(event);
    fixture.detectChanges();
  });

});
