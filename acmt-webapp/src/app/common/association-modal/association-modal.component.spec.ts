import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociationModalComponent } from './association-modal.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { ModalComponent } from '../modal/modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { SharedService } from 'src/app/shared.service';

describe('AssociationModalComponent', () => {
  let component: AssociationModalComponent;
  let fixture: ComponentFixture<AssociationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssociationModalComponent, ModalComponent],
      providers: [ConfirmationDialogService, SharedService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [ModalComponent] } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
