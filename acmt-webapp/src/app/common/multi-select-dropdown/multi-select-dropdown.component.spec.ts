import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  MultiSelectDropdownComponent
} from './multi-select-dropdown.component';
import { FormsModule } from '@angular/forms';
import { SimpleChange } from '@angular/core';
import Utils from 'src/app/utils';

describe('MultiSelectDropdownComponent', () => {
  let component: MultiSelectDropdownComponent;
  let fixture: ComponentFixture<MultiSelectDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MultiSelectDropdownComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectDropdownComponent);
    component = fixture.componentInstance;
    component.label = 'Name Selection';
    component.data = [{
      display_name: 'Status Start Date',
      name: 'Status Start Date',
      internal_name: 'status_start_date',
      value: 'status_start_date',
      metadata_id: '60177dc3-77e5-4636-9cf3-67c7f7b1140d',
      is_custom: 0,
      propName: 'display_name'
    }, {
      display_name: 'Full Statement',
      name: 'Full Statement',
      internal_name: 'full_statement',
      value: 'full_statement',
      metadata_id: '7c9b6712-4c9d-421d-88bb-00d80b3cc79a',
      is_custom: 0,
      propName: 'display_name'
    }, {
      display_name: 'Human Coding Scheme',
      name: 'Human Coding Scheme',
      internal_name: 'human_coding_scheme',
      value: 'human_coding_scheme',
      metadata_id: '88d285e3-ba24-4ca7-9959-ac610a9894fa',
      is_custom: 0,
      propName: 'display_name'
    }, {
      display_name: 'Language Name',
      name: 'Language Name',
      internal_name: 'language_name',
      value: 'language_name',
      metadata_id: 'b2522d60-b4ec-4066-880e-47154d6529a3',
      is_custom: 0,
      propName: 'display_name'
    }, {
      display_name: 'Status End Date',
      name: 'Status End Date',
      internal_name: 'status_end_date',
      value: 'status_end_date',
      metadata_id: 'b565ff4d-f4c6-4286-8a59-918614b6d303',
      is_custom: 0,
      propName: 'display_name'
    }, {
      display_name: 'List Enumeration',
      name: 'List Enumeration',
      internal_name: 'list_enumeration',
      value: 'list_enumeration',
      metadata_id: 'dac9a281-48c2-4f18-9901-a95563faf03c',
      is_custom: 0,
      propName: 'display_name'
    }];
    component.id = 'multi-select';
    component.viewLocation = '';
    component.selectedOptionList = [{
      display_name: 'Full Statement',
      name: 'Full Statement',
      internal_name: 'full_statement',
      metadata_id: '7c9b6712-4c9d-421d-88bb-00d80b3cc79a',
      is_custom: 0,
      value: 'full_statement',
      propName: 'display_name'
    }, {
      display_name: 'Human Coding Scheme',
      name: 'Human Coding Scheme',
      internal_name: 'human_coding_scheme',
      metadata_id: '88d285e3-ba24-4ca7-9959-ac610a9894fa',
      is_custom: 0,
      value: 'human_coding_scheme',
      propName: 'display_name'
    }];
    component.isNewLine = false;
    component.isMandatory = true;
    component.customCol = false;
    component.selectedData = {

    };
    component.dropdownSettings = {
      textField: 'display_name',
      showLabel: false,
      buttonType: 'icon'
    };
    component.ngOnChanges({
      data: new SimpleChange(null, component.data, true),
      selectedOptionList: new SimpleChange(null, component.selectedOptionList, true)
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should de-select option', () => {
    const selectedOption = {
      display_name: 'List Enumeration',
      name: 'List Enumeration',
      internal_name: 'list_enumeration',
      value: 'list_enumeration',
      metadata_id: 'dac9a281-48c2-4f18-9901-a95563faf03c',
      is_custom: 0,
      propName: 'display_name',
      is_selected: 1
    };
    component.onOptionSelect(selectedOption);
    expect(selectedOption.is_selected).toBe(0);
  });

  it('should select option', () => {
    const selectedOption = {
      display_name: 'List Enumeration',
      name: 'List Enumeration',
      internal_name: 'list_enumeration',
      value: 'list_enumeration',
      metadata_id: 'dac9a281-48c2-4f18-9901-a95563faf03c',
      is_custom: 0,
      propName: 'display_name',
      is_selected: 0
    };
    component.onOptionSelect(selectedOption);
    expect(selectedOption.is_selected).toBe(1);
  });

  it('should select all options', () => {
    component.doCheckBoxesSelection(true);
    expect(component.isAllSelected).toBeTruthy();
  });

  it('should select all options', () => {
    component.doCheckBoxesSelection(false);
    expect(component.isAllSelected).toBeFalsy();
  });

  it('should toggle drop-down', () => {
    component.showDropdown = true;
    component.toggleDropdown();
    expect(component.showDropdown).toBeFalsy();
  });

  it('should clear search text', () => {
    component.onClearSearchText();
    expect(component.searchKeyword).toBe('');
  });

  it('should close drop-down on outside click', () => {
    component.onClickOutSide();
    expect(component.showDropdown).toBeFalsy();
  });

  it('should select all options', () => {
    component.selectAll();
    expect(component.isAllSelected).toBeTruthy();
  });

  it('should de-select all options', () => {
    component.clearAllSelect();
    expect(component.isAllSelected).toBeFalsy();
  });

  it('should search on basis of selected options', () => {
    component.uniqueProp = component.dropdownSettings.textField;
    component.originalList = component.data;
    component.searchKeyword = 'list';
    const searchResult = [{
      display_name: 'List Enumeration',
      name: 'List Enumeration',
      internal_name: 'list_enumeration',
      value: 'list_enumeration',
      metadata_id: 'dac9a281-48c2-4f18-9901-a95563faf03c',
      is_custom: 0,
      propName: 'display_name'
    }];
    component.onSearch(component.uniqueProp);
    expect(component.data).toEqual(searchResult);
  });

  it('should add user selection', () => {
    spyOn(component.SelectEvent, 'emit');
    fixture.detectChanges();
    const userInput = true;
    component.addSelection(userInput);
    expect(component.SelectEvent.emit).toHaveBeenCalled();
  });
});
