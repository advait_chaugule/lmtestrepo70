import { Component, OnInit, Input, Output, EventEmitter, OnChanges, Inject } from '@angular/core';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { SharedService } from '../../shared.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnChanges {

  @Input() modalId; // modal's unique id
  @Input() modalContentId;
  @Input() modalSize = 'small'; // (values: big/small)
  @Input() showCancelBtn = true; // cancel button will be show or not in modal footer
  @Input() showDoneBtn = false;
  @Input() showCloseAlert = false; // to check whether alert will be shown or not
  @Input() closeAlertMsg = 'You have unsaved changes. Are you sure you want to leave this page?'; // alert message
  @Input() showErrorMsg = false;
  @Input() showFooter = true;

  @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() mouseLeaveEvent: EventEmitter<any> = new EventEmitter<any>();


  preventRequired = true; // for prevent event of modal is required or not
  alertActive = false; // to check whether close alert is opened or not
  emitCount = 0;
  faqUrls: any;
  faqTitle = '';

  constructor(
    private dialogService: ConfirmationDialogService,
    private sharedService: SharedService,
    private http: HttpClient,
    @Inject('Window') private window: Window
  ) {

    this.getJSON().subscribe(data => {
      this.faqUrls = data;
    });
    this.sharedService.faqEvent.subscribe((event: any) => {
      if (event && event.name) {
        this.faqTitle = event.name;
      }
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.setPreventRequired();
  }

  /**
   * On cancel modal
   */
  onCancel(event) {
    if (this.showCloseAlert && this.preventRequired) { // If close alert is required
      event.stopPropagation();
      event.preventDefault();
      if (!this.alertActive) {
        this.alertActive = true;
        this.alertForClose(event); // show alert to close
      }
    } else if (this.showErrorMsg) {
      event.stopPropagation();
      event.preventDefault();
      this.showErrorAlert();
    } else { // If close alert is not required, directly close modal without alert
      this.closeModal(event);
      this.setPreventRequired();
      this.emitCount = 0;
    }
  }

  /**
   * On escape key over modal
   * @param event
   */
  onModalKeydown(event) {
    if (event.keyCode === 27) { // '27' for Escape button
      event.stopPropagation();
      event.preventDefault();
      this.onCancel(event);
    }
  }

  onMouseLeave(event) {
    this.mouseLeaveEvent.emit(event);
  }

  /**
   * Confirmation alert showing for close modal
   */
  alertForClose(event) {
    this.preventRequired = true;
    this.dialogService.confirm('Confirm', this.closeAlertMsg, false)
      .then((confirmed) => {
        this.alertActive = false;
        if (confirmed) { // If yes, close modal
          this.preventRequired = false;
          this.closeModal(event);
        }
      })
      .catch(() => {
        console.log('User dismissed the dialog');
      });
  }

  // Closing modal by click event
  closeModal(event) {
    if (this.emitCount < 1) {
      this.closeEvent.emit({ 'event': event, 'msg': 'Closed' });
      this.emitCount = this.emitCount + 1;
    }
    if (document.getElementById('close' + this.modalId)) {
      document.getElementById('close' + this.modalId).click();
    }
  }

  /**
   * Setting prevent event required or not
   */
  setPreventRequired() {
    if (this.showCloseAlert) {
      this.preventRequired = true;
    } else {
      this.preventRequired = false;
    }
  }

  showErrorAlert() {
    this.sharedService.sucessEvent.next({
      type: 'unsaved_exempler_data'
    });
  }

  onHelpClick() {
    const base_url = 'https://api.' + this.window.location.hostname + '/faq/';
    const fAqUrL = base_url + this.faqUrls[this.faqTitle];
    window.open(fAqUrL, '_blank');
  }
  public getJSON(): Observable<any> {
    return this.http.get('../assets/json/helpFAQ.json');
  }
}
