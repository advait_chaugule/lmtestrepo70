// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { ModalComponent } from './modal.component';
// import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
// import { ConfirmationDialogComponent } from 'src/app/confirmation-dialog/confirmation-dialog.component';
// import { SharedService } from 'src/app/shared.service';
// import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// describe('ModalComponent', () => {
//   let component: ModalComponent;
//   let fixture: ComponentFixture<ModalComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ModalComponent, ConfirmationDialogComponent],
//       imports: [NgbModule],
//       providers: [ConfirmationDialogService, SharedService]
//     })
//       .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [ConfirmationDialogComponent] } })
//       .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ModalComponent);
//     component = fixture.componentInstance;
//     component.modalId = 'testModalId';
//     component.modalSize = 'small';
//     component.showCancelBtn = false;
//     component.showDoneBtn = false;
//     component.showCloseAlert = true;
//     component.closeAlertMsg = 'Saving alert';
//     component.showErrorMsg = false;
//     component.showFooter = true;
//     component.ngOnChanges();
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('should call onChanges', () => {
//     expect(component.preventRequired).toBe(true);
//     expect(component.modalId).toBe('testModalId');
//     expect(component.modalSize).toBe('small');
//     expect(component.showCancelBtn).toBe(false);
//     expect(component.showErrorMsg).toBe(false);
//   });

//   it('should call onCancel', () => {
//     const event: any = {};
//     event.stopPropagation = () => { };
//     event.preventDefault = () => { };
//     // component.onCancel(event);
//     fixture.detectChanges();
//     expect(component.alertActive).toBe(false);
//     expect(component.emitCount).toBe(0);
//   });

//   it('should call onModalKeydown for Escape button', () => {
//     const event: any = {};
//     event.stopPropagation = () => { };
//     event.preventDefault = () => { };
//     event.keyCode = 27;
//     component.onModalKeydown(event);
//     fixture.detectChanges();
//   });

//   it('should call closeModal()', () => {
//     const event: any = {};
//     event.stopPropagation = () => { };
//     event.preventDefault = () => { };
//     component.emitCount = 0;
//     component.closeModal(event);
//     fixture.detectChanges();
//     expect(component.emitCount).toBe(1);
//   });

//   // it('should call showErrorAlert()', () => {
//   //   component.showErrorAlert();
//   // });

//   it('should call onMouseLeave()', () => {
//     const event: any = {};
//     component.onMouseLeave(event);
//   });

// });
