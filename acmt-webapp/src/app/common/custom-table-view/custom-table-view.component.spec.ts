import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomTableViewComponent } from './custom-table-view.component';

describe('CustomTableViewComponent', () => {
  let component: CustomTableViewComponent;
  let fixture: ComponentFixture<CustomTableViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomTableViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
