import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { DataTableComponent } from './data-table.component';
import { PreloaderComponent } from '../preloader/preloader.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { SimpleChange } from '@angular/core';
import { MultiSelectDropdownComponent } from '../multi-select-dropdown/multi-select-dropdown.component';
import { CustomFilterPipe } from '../custom-filter.pipe';
import { MomentModule } from 'angular2-moment';
import { PaginationComponent } from '../pagination/pagination.component';
import { FormsModule } from '@angular/forms';
import { SharedService } from 'src/app/shared.service';
import Utils from 'src/app/utils';
import { By } from '@angular/platform-browser';

describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DataTableComponent, PreloaderComponent, MultiSelectDropdownComponent, PaginationComponent, CustomFilterPipe],
      imports: [MomentModule, FormsModule],
      providers: [SharedService]
    })
      .overrideModule(BrowserDynamicTestingModule,
        { set: { entryComponents: [PreloaderComponent, MultiSelectDropdownComponent, PaginationComponent] } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    component.showMenu = true;
    component.columnList = [{
      name: 'Name',
      propName: 'project_name',
      class: 'col-2',
      type: 'link',
      redirect: true,
      loadPage: true
    },
    {
      name: 'Type',
      propName: 'project_taxonomy_type',
      class: 'col px-1',
      type: 'text',
      canRemove: true,
      canFilter: true
    },
    {
      name: 'Workflow Status',
      propName: 'project_status',
      class: 'col px-1',
      type: 'text',
      canRemove: true,
      canFilter: true
    },
    {
      name: 'Groups',
      propName: 'groupNames',
      class: 'col px-1',
      type: 'text',
      canRemove: true,
      canFilter: true
    },
    {
      name: 'Last Change',
      propName: 'updated_at',
      class: 'col px-1',
      type: 'date',
      dateType: 'amCalendar'
    },
    {
      name: 'Active since',
      propName: 'created_at',
      class: 'col px-1',
      type: 'date',
      dateType: 'amCalendar'
    },
    {
      name: 'Draft Taxonomy',
      propName: 'taxonomy_name',
      class: 'col px-1 draft-taxonomy',
      type: 'text',
      canFilter: true
    },
    {
      name: 'Created By',
      propName: 'created_by',
      class: 'col px-1 created-by',
      type: 'text',
      canFilter: true,
      canRemove: true
    },
    {
      name: 'Roles',
      propName: 'access_roles',
      class: 'col px-1',
      type: 'text',
      canFilter: true
    }];
    component.dataSet = [{
      project_id: 'ea81d471-8fd8-4879-96b4-faa92b1ff43d',
      project_name: 'Test orphan',
      description: '',
      workflow_id: '3791ac0a-b204-4fdd-96ff-f7c6c6d5226d',
      project_type: 1,
      created_at: '2019-12-11 06:38:30',
      updated_at: '2019-12-11 10:43:35',
      workflow: {
        workflow_id: '3791ac0a-b204-4fdd-96ff-f7c6c6d5226d',
        title: 'Test Workflow'
      },
      taxonomy_name: 'Orphan bachcha \'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incid',
      taxonomy_status: 1,
      taxonomy_type: 3,
      project_status: 'Test',
      document_id: '4d34fcb2-9861-0541-41bb-fb1105671743',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      group: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'k',
      groupNames: '-',
      access_roles: 'No Role'
    },
    {
      project_id: '3fa92f67- 8499 - 42c7 - b0e1 - 70f106f6a0a3',
      project_name: 'Crazy Projo',
      description: '',
      workflow_id: '461beae0 - 9c36 - 4064 - b6b1 - ba774de5775d',
      project_type: 1,
      created_at: '2019 - 12 - 11 05: 11: 26',
      updated_at: '2019 - 12 - 11 05: 11: 27',
      workflow: {
        workflow_id: '461beae0 - 9c36 - 4064 - b6b1 - ba774de5775d',
        title: 'WF1'
      },
      taxonomy_name: 'Taxo paxo',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'xjashxjah',
      document_id: 'ac2292f0 - 0525 - 31b9 - 8d87 - 2b63e5124a92',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    },
    {
      project_id: '222e1169 - 4108 - 420a- 9b7c - 5cf9869f6cfc',
      project_name: 'More Chocolaty',
      description: '',
      workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
      project_type: 1,
      created_at: '2019 - 12 - 10 07: 24: 33',
      updated_at: '2019 - 12 - 10 07: 24: 34',
      workflow: {
        workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
        title: 'Test Workflow'
      },
      taxonomy_name: 'Chocolaty Taxo',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'Test',
      document_id: '8def4b1e - 2cc7 - 7fdf - fc18 - 292c77d2095c',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    },
    {
      project_id: '9e5427de- 567b - 405a - a762 - ae25fe5dc8eb',
      project_name: '1',
      description: '1',
      workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
      project_type: 1,
      created_at: '2019 - 11 - 18 12: 23: 14',
      updated_at: '2019 - 11 - 18 12: 23: 57',
      workflow: {
        workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
        title: 'Test Workflow'
      },
      taxonomy_name: '1',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'Test',
      document_id: '52505a97 - 0c39 - be29 - 4195 - 756ea11d28b7',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    },
    {
      project_id: '24aa6ec0- ab9f - 4b88 - ab14 - 13dcfbc48b41',
      project_name: 'test new proj',
      description: '',
      workflow_id: '461beae0 - 9c36 - 4064 - b6b1 - ba774de5775d',
      project_type: 1,
      created_at: '2019 - 11 - 14 12: 58: 59',
      updated_at: '2019 - 11 - 18 11: 25: 58',
      workflow: {
        workflow_id: '461beae0 - 9c36 - 4064 - b6b1 - ba774de5775d',
        title: 'WF1'
      },
      taxonomy_name: 'Taxo to copy',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'xjashxjah',
      document_id: '8cbcba5b - 3988 - 1d62 - e8b6 - bef5f5539426',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    }];
    component.optionList = [{
      name: 'Edit',
      type: 'editProject',
      value: true,
      modal: '#createProject',
      check: 'isEditable'
    },
    {
      name: 'Delete',
      type: 'delProject',
      value: true,
      modal: '',
      check: 'isDelete'
    }];
    component.defaultSortBy = {
      name: 'Last Change',
      propName: 'updated_at',
      class: 'col px-1',
      type: 'date',
      dateType: 'amCalendar'
    };
    component.sortRequired = true;
    component.pagination = {
      start: 0,
      last: Utils.PAGINATION_LOWER_LIMIT,
      pageIndex: 1,
      itemPerPage: Utils.PAGINATION_LOWER_LIMIT
    };
    component.selectedOptionList = [{
      propName: 'label',
      value: 'Debarati Purkait'
    }];
    component.uniqueField = 'project_id';
    component.dropdownItems = [{
      name: 'Active',
      value: '1',
      propName: 'role_id'
    },
    {
      name: 'Inactive',
      value: '0',
      propName: 'role_id'
    }];
    component.ngOnChanges({
      dataSet: new SimpleChange([], component.dataSet, true)
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change status', () => {
    spyOn(component.clickedStatus, 'emit');
    fixture.detectChanges();
    component.changeStatus(component.dropdownItems[0].value, '24aa6ec0- ab9f - 4b88 - ab14 - 13dcfbc48b41');
    expect(component.clickedStatus.emit).toHaveBeenCalled();
  });

  it('should open detail page', () => {
    spyOn(component.linkClicked, 'emit');
    fixture.detectChanges();
    const data = {
      project_id: '9e5427de- 567b - 405a - a762 - ae25fe5dc8eb',
      project_name: '1',
      description: '1',
      workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
      project_type: 1,
      created_at: '2019 - 11 - 18 12: 23: 14',
      updated_at: '2019 - 11 - 18 12: 23: 57',
      workflow: {
        workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
        title: 'Test Workflow'
      },
      taxonomy_name: '1',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'Test',
      document_id: '52505a97 - 0c39 - be29 - 4195 - 756ea11d28b7',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    };
    component.onProjectSelected(data);
    expect(component.linkClicked.emit).toHaveBeenCalled();
  });

  it('should work according the option clicked', () => {
    spyOn(component.clickedOption, 'emit');
    fixture.detectChanges();
    const event = new MouseEvent('click');
    const data = {
      project_id: '9e5427de- 567b - 405a - a762 - ae25fe5dc8eb',
      project_name: '1',
      description: '1',
      workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
      project_type: 1,
      created_at: '2019 - 11 - 18 12: 23: 14',
      updated_at: '2019 - 11 - 18 12: 23: 57',
      workflow: {
        workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
        title: 'Test Workflow'
      },
      taxonomy_name: '1',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'Test',
      document_id: '52505a97 - 0c39 - be29 - 4195 - 756ea11d28b7',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    };
    component.onOptionClicked(event, component.optionList[0].type, data, 0);
    expect(component.clickedOption.emit).toHaveBeenCalled();
  });

  it('should get current request details', () => {
    spyOn(component.showData, 'emit');
    fixture.detectChanges();
    const data = {
      project_id: '9e5427de- 567b - 405a - a762 - ae25fe5dc8eb',
      project_name: '1',
      description: '1',
      workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
      project_type: 1,
      created_at: '2019 - 11 - 18 12: 23: 14',
      updated_at: '2019 - 11 - 18 12: 23: 57',
      workflow: {
        workflow_id: '3791ac0a - b204 - 4fdd - 96ff - f7c6c6d5226d',
        title: 'Test Workflow'
      },
      taxonomy_name: '1',
      taxonomy_status: 1,
      taxonomy_type: 1,
      project_status: 'Test',
      document_id: '52505a97 - 0c39 - be29 - 4195 - 756ea11d28b7',
      last_changed_by: 'Debarati Purkait',
      created_by: 'Debarati Purkait',
      access_right: [

      ],
      groups: [

      ],
      isEditable: true,
      isDelete: true,
      showOptions: true,
      project_taxonomy_type: 'Generic',
      groupNames: ' - ',
      access_roles: 'No Role'
    };
    component.getCurrentRequestDetails(data);
    expect(component.showData.emit).toHaveBeenCalled();
  });

  it('should show data within selected range', () => {
    const pageEvent = {
      start: 1,
      last: 10,
      itemPerPage: 12,
      pageIndex: 1,
      maxPages: 2
    };
    const checkedPages = [false, false];
    component.showDataInRange(pageEvent);
    expect(component.checkedPages).toEqual(checkedPages);
  });

  it('should filter the data list based on blank filter list', () => {
    const filterArray = [];
    const column = {
      name: 'Type',
      propName: 'project_taxonomy_type',
      class: 'col px-1',
      type: 'text',
      canRemove: true,
      canFilter: true
    };
    component.filterDataList(filterArray, column);
    expect(component.dataSet.length).toEqual(component.originalDataSet.length);
  });

  it('should filter the data list based on selected filters', () => {
    const filterArray = [{
      colName: 'project_taxonomy_type',
      label: 'Generic',
      is_selected: 1
    }, {
      colName: 'created_by',
      label: 'Debarati Purkait',
      is_selected: 1
    }];
    const column = {
      name: 'Type',
      propName: 'project_taxonomy_type',
      class: 'col px-1',
      type: 'text',
      canRemove: true,
      canFilter: true
    };
    component.filterDataList(filterArray, column);
    expect(component.dataSet.length).not.toEqual(component.originalDataSet);
  });

  it('should select filter options', () => {
    const filterArray = [{
      colName: 'project_taxonomy_type',
      label: 'Generic',
      is_selected: 1
    }, {
      colName: 'created_by',
      label: 'Debarati Purkait',
      is_selected: 1
    }];
    const selectedDataList = {
      selectedData: filterArray
    };
    component.onFilterOptionSelect(selectedDataList, component.columnList[1]);
    expect(component.filterArray).toEqual(filterArray);
  });

  it('should select filter options', () => {
    component.filterOperationType = 'OR';
    const filterArray = [{
      colName: 'project_taxonomy_type',
      label: 'Generic',
      is_selected: 1
    }, {
      colName: 'created_by',
      label: 'Debarati Purkait',
      is_selected: 1
    }];
    const selectedDataList = {
      selectedData: filterArray
    };
    component.onFilterOptionSelect(selectedDataList, component.columnList[1]);
    expect(component.filterArray).toEqual(filterArray);
  });

  it('should check table rows for single type', () => {
    fixture.detectChanges();
    const data = component.dataSet[0];
    data.checked = true;
    const elem = fixture.debugElement.query(By.css('input[type=checkbox]'));
    const event = {
      target: elem.nativeElement
    };
    const type = 'single';
    component.onRowChecked(type, event, data);
    expect(data.checked).toBe(elem.nativeElement.checked);
  });

  it('should check table rows for paginatedAll type', () => {
    fixture.detectChanges();
    const data = component.dataSet[0];
    data.checked = true;
    const elem = fixture.debugElement.query(By.css('input[type=checkbox]'));
    const event = {
      target: elem.nativeElement
    };
    const type = 'paginatedAll';
    component.onRowChecked(type, event, data);
    expect(component.checkedPages[component.pageIndex]).toBe(elem.nativeElement.checked);
  });

  it('should return the index of checked row', () => {
    component.checkedRows = component.dataSet;
    const value = component.dataSet[1];
    const result = component.getIndexOfChecked(value);
    expect(result).toBe(1);
  });

  it('should clear all selections', () => {
    component.maxPages = 1;
    component.clearAllSelection();
    expect(component.checkedPages[0]).toBeFalsy();
  });

  it('should set focus on selected data', () => {
    const data = component.dataSet[0];
    component.onFocus(data);
    expect(component.selectedData).toEqual(data);
  });

  it('should set width of column', () => {
    const column = {
      name: 'Name',
      propName: 'project_name',
      class: 'col-2',
      type: 'link',
      redirect: true,
      loadPage: true,
      width: 200
    };
    const width = column.width + 'px';
    const result = component.setWidth(column);
    expect(result).toMatch(width);
  });

  it('should check single row', () => {
    const data = component.dataSet[2];
    data.checked = true;
    component.doCheckForSingle(data);
    expect(component.checkedRows).toContain(data);
  });

  it('should uncheck single row', () => {
    component.checkedRows = [component.dataSet[2], component.dataSet[5]];
    const data = component.dataSet[2];
    data.checked = false;
    component.doCheckForSingle(data);
    expect(component.checkedRows).not.toContain(data);
  });

  it('should check all page data', () => {
    component.startIndex = 0;
    component.lastIndex = 4;
    component.checkedRows = [];
    component.doCheckForPagedData(true);
    expect(component.checkedRows.length).toBeGreaterThan(0);
  });

  it('should uncheck all page data', () => {
    component.startIndex = 0;
    component.lastIndex = 5;
    component.checkedRows = JSON.parse(JSON.stringify(component.dataSet));
    component.doCheckForPagedData(false);
    expect(component.checkedRows.length).toBe(0);
  });
});
