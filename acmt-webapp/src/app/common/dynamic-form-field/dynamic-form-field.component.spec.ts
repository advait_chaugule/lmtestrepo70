import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFormFieldComponent } from './dynamic-form-field.component';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { NgxEditorModule } from 'ngx-editor';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DynamicFormFieldComponent', () => {
  let component: DynamicFormFieldComponent;
  let fixture: ComponentFixture<DynamicFormFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicFormFieldComponent],
      imports: [FormsModule, MyDatePickerModule, NgxEditorModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormFieldComponent);
    component = fixture.componentInstance;
    component.selectedMetadata = 'test metadata';
    component.field_type_id = 'id';
    component.clearFieldValue = true;
    component.disabled = false;
    component.ngOnChanges();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onChanges', () => {
    expect(component.value).toBe('');
  });

  it('should call formatDateValue', () => {
    const date = '2019-12-17';
    component.formatDateValue(date);
  });

  it('should call onCustomDateChanged', () => {
    const event: any = {};
    component.onCustomDateChanged(event);
  });

  it('should call registerOnChange', () => {
    const fn: any = {};
    component.registerOnChange(fn);
  });

  it('should call registerOnTouched', () => {
    const fn: any = {};
    component.registerOnTouched(fn);
  });

  it('should call writeValue', () => {
    const value = 'test';
    component.writeValue(value);
  });

  it('should call updateFieldFromEditor', () => {
    const event: any = {};
    component.updateFieldFromEditor(event);
  });

  it('should call isNumber', () => {
    const event: any = {};
    component.isNumber(event);
  });
});
