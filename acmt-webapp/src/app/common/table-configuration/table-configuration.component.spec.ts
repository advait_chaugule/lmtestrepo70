import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableConfigurationComponent } from './table-configuration.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

describe('TableConfigurationComponent', () => {
  let component: TableConfigurationComponent;
  let fixture: ComponentFixture<TableConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableConfigurationComponent],
      imports: [FormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableConfigurationComponent);
    component = fixture.componentInstance;
    component.dropdownData = {}
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
