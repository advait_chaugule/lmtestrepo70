/// <reference lib="webworker" />

addEventListener('message', ({ data }) => {
  // const response = `worker response to ${data}`;
  // console.log('data in worker ', data);
  const treeData = data.data;
  test(treeData[0].children, data.isExpand);
  postMessage(treeData);
  close();

});

function test(children, isExpand) {
  children.forEach(element => {
    element.expand = isExpand;
    element.worker = 'add';
    if (element.children && element.children.length > 0) {
      test(element.children, isExpand);
    }

  });
}
