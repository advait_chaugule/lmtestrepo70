import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectTreeComponent } from './create-project-tree.component';
import { FormsModule } from '@angular/forms';

describe('CreateProjectTreeComponent', () => {
  let component: CreateProjectTreeComponent;
  let fixture: ComponentFixture<CreateProjectTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateProjectTreeComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProjectTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});