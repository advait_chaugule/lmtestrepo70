import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicReviewComponent } from './public-review.component';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

describe('PublicReviewComponent', () => {
  let component: PublicReviewComponent;
  let fixture: ComponentFixture<PublicReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PublicReviewComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService, SharedService, ConfirmationDialogService, AuthenticateUserService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
