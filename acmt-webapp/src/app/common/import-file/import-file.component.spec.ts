import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportFileComponent } from './import-file.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { PreloaderComponent } from '../preloader/preloader.component';

describe('ImportFileComponent', () => {
  let component: ImportFileComponent;
  let fixture: ComponentFixture<ImportFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImportFileComponent, PreloaderComponent]
    })
      .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [PreloaderComponent] } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
