import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  PaginationComponent
} from './pagination.component';
import {
  FormsModule
} from '@angular/forms';
import Utils from 'src/app/utils';
import {
  SimpleChange
} from '@angular/core';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture < PaginationComponent > ;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [PaginationComponent],
        imports: [FormsModule]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    component.dataLength = 50;
    component.defaultPageRange = {
      itemPerPage: Utils.PAGINATION_LOWER_LIMIT,
      last: Utils.PAGINATION_LOWER_LIMIT,
      start: 0,
      pageIndex: 1
    };
    component.ngOnChanges({
      dataLength: new SimpleChange(null, component.dataLength, true),
      defaultPageRange: new SimpleChange(null, {
        itemPerPage: Utils.PAGINATION_LOWER_LIMIT,
        last: Utils.PAGINATION_LOWER_LIMIT,
        start: 0,
        pageIndex: 1
      }, true)
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onChanges', () => {
    expect(component.startIndex).toBe(1);
  });

  it('OnNextClick', () => {
    let outputValue;
    component.displayItemRangeEvent.subscribe((value) => {
      outputValue = value;
    });
    component.onNextClicked();
    fixture.detectChanges();
    expect(outputValue.pageIndex).toBe(component.pageIndex);
  });

  it('OnPreviousClick', () => {
    let outputValue;
    component.pageIndex = 2;
    component.displayItemRangeEvent.subscribe((value) => {
      outputValue = value;
    });
    component.onPreviousClicked();
    fixture.detectChanges();
    expect(outputValue.pageIndex).toBe(component.pageIndex);
  });

  it('Check if data length less than no. of item per page ',()=>{
    component.pageIndex = 2;
    component.noOfItemPerPage = 10;
    component.updatePagination(5);
    expect(component.pageIndex).toBe(1);
  });
});
