// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { CoverageReportModalComponent } from './coverage-report-modal.component';
// import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
// import { ModalComponent } from '../modal/modal.component';
// import { AutoCompleteComponent } from '../auto-complete/auto-complete.component';
// import { TreeviewComponent } from 'ngx-treeview';
// import { TabComponent } from 'src/app/tab/tab.component';
// import { PreloaderComponent } from '../preloader/preloader.component';
// import { CommonService } from 'src/app/common.service';
// import { SharedService } from 'src/app/shared.service';
// import { TreeDataService } from 'src/app/tree-data.service';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { HttpClientModule } from '@angular/common/http';
// import { RouterTestingModule } from '@angular/router/testing';
// import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';

// describe('CoverageReportModalComponent', () => {
//   let component: CoverageReportModalComponent;
//   let fixture: ComponentFixture<CoverageReportModalComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         CoverageReportModalComponent,
//         ModalComponent,
//         AutoCompleteComponent,
//         TreeviewComponent,
//         TabComponent,
//         PreloaderComponent],
//       imports: [HttpClientModule, RouterTestingModule],
//       providers: [CommonService, SharedService, TreeDataService, ConfirmationDialogService],
//       schemas: [NO_ERRORS_SCHEMA]
//     })
//       .overrideModule(BrowserDynamicTestingModule,
//         {
//           set: {
//             entryComponents: [
//               ModalComponent,
//               AutoCompleteComponent,
//               TreeviewComponent,
//               TabComponent,
//               PreloaderComponent
//             ]
//           }
//         })
//       .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(CoverageReportModalComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
