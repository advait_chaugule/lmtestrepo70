import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabularTreeSidePaneComponent } from './tabular-tree-side-pane.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/common.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedService } from 'src/app/shared.service';

describe('TabularTreeSidePaneComponent', () => {
  let component: TabularTreeSidePaneComponent;
  let fixture: ComponentFixture<TabularTreeSidePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabularTreeSidePaneComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [CommonService, ConfirmationDialogService, SharedService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabularTreeSidePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
