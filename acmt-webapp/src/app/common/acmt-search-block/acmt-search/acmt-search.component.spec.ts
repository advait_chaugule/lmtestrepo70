// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { AcmtSearchComponent } from './acmt-search.component';
// import { CommonService } from 'src/app/common.service';
// import { SearchFilterDataService } from '../SearchFilterDataService';
// import { RouterTestingModule } from '@angular/router/testing';
// import { SharedService } from 'src/app/shared.service';
// import { MultiSelectDropdownComponent } from '../../multi-select-dropdown/multi-select-dropdown.component';
// import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AcmtSearchListComponent } from '../acmt-search-list/acmt-search-list.component';
// import { PreloaderComponent } from '../../preloader/preloader.component';
// import { SelectAssociationComponent } from '../../select-association/select-association.component';
// import { PaginationComponent } from '../../pagination/pagination.component';
// import { HttpClientModule } from '@angular/common/http';
// import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';


// describe('AcmtSearchComponent', () => {
//   let component: AcmtSearchComponent;
//   let fixture: ComponentFixture<AcmtSearchComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         AcmtSearchComponent,
//         MultiSelectDropdownComponent,
//         AcmtSearchListComponent,
//         PreloaderComponent,
//         SelectAssociationComponent,
//         PaginationComponent
//       ],
//       imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
//       providers: [CommonService, SearchFilterDataService, SharedService, ConfirmationDialogService]
//     })
//       .overrideModule(BrowserDynamicTestingModule,
//         {
//           set: {
//             entryComponents: [
//               MultiSelectDropdownComponent,
//               AcmtSearchListComponent,
//               PreloaderComponent,
//               SelectAssociationComponent,
//               PaginationComponent
//             ]
//           }
//         })
//       .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(AcmtSearchComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
