import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcmtSearchListComponent } from './acmt-search-list.component';
import { SharedService } from 'src/app/shared.service';
import { SelectAssociationComponent } from '../../select-association/select-association.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { PaginationComponent } from '../../pagination/pagination.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('AcmtSearchListComponent', () => {
  let component: AcmtSearchListComponent;
  let fixture: ComponentFixture<AcmtSearchListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AcmtSearchListComponent, SelectAssociationComponent, PaginationComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [SharedService]
    })
      .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [SelectAssociationComponent, PaginationComponent] } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcmtSearchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
