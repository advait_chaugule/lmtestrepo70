// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { BrowseTaxonomyListComponent } from './browse-taxonomy-list.component';
// import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
// import { DataTableComponent } from '../data-table/data-table.component';
// import { CommonService } from 'src/app/common.service';
// import { HttpClientModule } from '@angular/common/http';
// import { RouterTestingModule } from '@angular/router/testing';
// import { PreloaderComponent } from '../preloader/preloader.component';
// import { MultiSelectDropdownComponent } from '../multi-select-dropdown/multi-select-dropdown.component';
// import { PaginationComponent } from '../pagination/pagination.component';
// import { CustomFilterPipe } from '../custom-filter.pipe';
// import { MomentModule } from 'angular2-moment';
// import { FormsModule } from '@angular/forms';
// import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
// import { SharedService } from 'src/app/shared.service';

// describe('BrowseTaxonomyListComponent', () => {
//   let component: BrowseTaxonomyListComponent;
//   let fixture: ComponentFixture<BrowseTaxonomyListComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         BrowseTaxonomyListComponent,
//         DataTableComponent,
//         PreloaderComponent,
//         MultiSelectDropdownComponent,
//         PaginationComponent,
//         CustomFilterPipe
//       ],
//       imports: [HttpClientModule, RouterTestingModule, MomentModule, FormsModule],
//       providers: [CommonService, ConfirmationDialogService, SharedService]
//     })
//       .overrideModule(BrowserDynamicTestingModule,
//         {
//           set: {
//             entryComponents: [
//               DataTableComponent,
//               PreloaderComponent,
//               MultiSelectDropdownComponent,
//               PaginationComponent]
//           }
//         })
//       .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(BrowseTaxonomyListComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
