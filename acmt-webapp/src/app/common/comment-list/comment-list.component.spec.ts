import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentListComponent } from './comment-list.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { PreloaderComponent } from '../preloader/preloader.component';
import { PaginationComponent } from '../pagination/pagination.component';
import { CommonService } from 'src/app/common.service';
import { SharedService } from 'src/app/shared.service';
import { AuthenticateUserService } from 'src/app/authenticateuser.service';
import { FormsModule } from '@angular/forms';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('CommentListComponent', () => {
  let component: CommentListComponent;
  let fixture: ComponentFixture<CommentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentListComponent, PreloaderComponent, PaginationComponent],
      imports: [FormsModule, HttpClientModule, RouterTestingModule],
      providers: [CommonService, SharedService, AuthenticateUserService, ConfirmationDialogService]
    })
      .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [PreloaderComponent, PaginationComponent] } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
