import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import {
  ComplianceProgressComponent
} from './compliance-progress.component';
import { SimpleChange } from '@angular/core';

describe('ComplianceProgressComponent', () => {
  let component: ComplianceProgressComponent;
  let fixture: ComponentFixture < ComplianceProgressComponent > ;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [ComplianceProgressComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplianceProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChange',()=>{
    component.ngOnChanges({
      progressPercentage: new SimpleChange(null, undefined, true),
      progressTotal: new SimpleChange(null, undefined, true),
      progressColor: new SimpleChange(null, undefined, true),
    });
    fixture.detectChanges();
    expect(component.progressColor).toBe('r');
  });

  it('setProgressColor', () => {

    component.progressPercentage = 20;
    component.setProgressColor();
    expect(component.progressColor).toBe('r');

    component.progressPercentage = 50;
    component.setProgressColor();
    expect(component.progressColor).toBe('y');

    component.progressPercentage = 78;
    component.setProgressColor();
    expect(component.progressColor).toBe('g');
    fixture.detectChanges();
  });

  it('setDefaultColor', () => {
    component.setDefaultColor();
    component.convertToLowercase();
    expect(component.progressColor).toBe('y');
  });
  it('setDefaultPercentage', () => {
    component.setDefaultPercentage();
    expect(component.progressPercentage).toBe(0);
  });
  it('setDefaultItemsCompleted', () => {
    component.setDefaultItemsCompleted();
    expect(component.progressDone).toBe(0);
    expect(component.progressTotal).toBe(0);
  });
});
