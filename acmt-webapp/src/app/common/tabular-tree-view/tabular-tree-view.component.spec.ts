import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabularTreeViewComponent } from './tabular-tree-view.component';
import { SharedService } from 'src/app/shared.service';
import { CommonService } from 'src/app/common.service';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('TabularTreeViewComponent', () => {
  let component: TabularTreeViewComponent;
  let fixture: ComponentFixture<TabularTreeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabularTreeViewComponent],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [SharedService, CommonService, ConfirmationDialogService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabularTreeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
