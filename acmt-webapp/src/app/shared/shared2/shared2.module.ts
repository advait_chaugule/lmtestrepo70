import {
  NgModule, NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  ViewWorkflowComponent
} from '../../administration/workflow/view-workflow/view-workflow.component';
/*import {
  WalktroughService
} from '../../help/walkthrough/walktrough.service';*/

import { ViewWorkflowModalComponent } from '../../administration/workflow/view-workflow-modal/view-workflow-modal.component';
import {
  MomentModule
} from 'angular2-moment';
import {
  NodeTemplateComponent
} from '../../administration/node/node-template/node-template.component';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { DragulaModule, DragulaService } from 'ng2-dragula';
import { Shared3Module } from '../shared3/shared3.module';
import { TemplatePreviewComponent } from '../../administration/node/template-preview/template-preview.component';
import { SharedModule } from '../shared.module';
import { TeamDetailComponent } from '../../project/team-details/team-details.component';

@NgModule({
  imports: [
    CommonModule,
    MomentModule,
    FormsModule,
    ReactiveFormsModule,
    DragulaModule,
    Shared3Module,
    SharedModule
  ],
  declarations: [
    ViewWorkflowComponent,
    ViewWorkflowModalComponent,
    NodeTemplateComponent,
    TemplatePreviewComponent,
    TeamDetailComponent
  ],
  exports: [
    ViewWorkflowComponent,
    ViewWorkflowModalComponent,
    MomentModule,
    NodeTemplateComponent,
    DragulaModule,
    TemplatePreviewComponent,
    TeamDetailComponent
  ],
  providers: [DragulaService],
  schemas: [NO_ERRORS_SCHEMA]

})
export class Shared2Module { }
