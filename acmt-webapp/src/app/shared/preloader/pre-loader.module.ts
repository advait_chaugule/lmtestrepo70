import {
    NgModule,
    NO_ERRORS_SCHEMA
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PreloaderComponent } from 'src/app/common/preloader/preloader.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [PreloaderComponent],
    exports: [PreloaderComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PreLoaderModule { }
