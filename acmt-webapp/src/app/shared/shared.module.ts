import {
  NgModule,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';

import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';

import {
  ProjectTreeAccordianComponent
} from '../project/project-tree-accordian/project-tree-accordian.component';

// import {
//   SortListComponent
// } from '../common/sort-list/sort-list.component';
import {
  FilterListComponent
} from '../common/filter-list/filter-list.component';


import {
  ImportFileComponent
} from './../common/import-file/import-file.component';
import { Shared3Module } from './shared3/shared3.module';
import { PublicModule } from './public.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Shared3Module,
    PublicModule
  ],
  exports: [
    CommonModule, FormsModule,
    ReactiveFormsModule,

    ProjectTreeAccordianComponent,
    FilterListComponent

  ],
  declarations: [

    ProjectTreeAccordianComponent,
    FilterListComponent

  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class SharedModule { }
