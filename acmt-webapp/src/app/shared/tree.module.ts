import {
    NgModule,
    NO_ERRORS_SCHEMA
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';
import { TreeComponent } from '../common/tree/tree.component';
import { CreateProjectTreeComponent } from '../common/create-project-tree/create-project-tree.component';
import { AutoCompleteModule } from './auto-complete/auto-complete.module';
import { ClickOutsideModule } from 'ng-click-outside';
import { MathJaxModule } from './mathJax/mathJax.module';

@NgModule({
    imports: [CommonModule, FormsModule,
        ReactiveFormsModule, AutoCompleteModule, ClickOutsideModule, MathJaxModule
    ],
    exports: [
        TreeComponent,
        CreateProjectTreeComponent,
        MathJaxModule
    ],
    declarations: [
        TreeComponent,
        CreateProjectTreeComponent
    ],
    schemas: [NO_ERRORS_SCHEMA],
})
export class TreeModule { }
