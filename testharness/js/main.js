// main.js

var ADMINSERVICES = ["validateLogin",];
/*
** LEGENDS FOR "fieldNamesJSON":
	'#' -> 1st hash signifies that after this hash, all the things are for plotting of input fields
	'#n#' -> means not mandatory field for service call
	'||s@value>displayname,value>displayname' -> means this needs a dropdown list. It contains its display names and its values.
	'||ppd@value' -> means this field has a pre defined value that needs to be assign to its text input field.
	
**
*/
var fieldNamesJSON = {
	"validateLogin" : ["username","password"],
        //["AppType #--||s@ios>ios,webclient>webclient,andrd>andrd,wintab>wintab,chromeapp>chromeapp","AppVersionNumber","PassCode #--||ppd@1234","LastTokenID #--#n#"],
	/*"ValidateLoginForStudent" : ["TokenID","DeviceTimeStamp","UserName","Password","IsCallerProjectionApp ('y' for Projection app, else select 'n')#--||s@n>no,y>yes","CallerAppType","CurrentAppVersionNo","CurrentAppZipRevisionNo","MasterProductType#--||s@ilit>ilit,myeld>myeld,wtw>wtw,demo>(demo) for Reset iLit only #--#n#"],
	"ValidateLoginForStudentSAML" : ["TokenID","DeviceTimeStamp","RumbaTicketID","ServiceURL","IsCallerProjectionApp ('y' for Projection app, else select 'n')#--||s@n>no,y>yes","CallerAppType","MasterProductType#--||s@ilit>ilit,myeld>myeld,wtw>wtw #--#n#"],
	"ValidateLoginForInstructor" : ["TokenID","DeviceTimeStamp","UserName","Password","IsCallerProjectionApp ('y' for Projection app, else select 'n')#--||s@n>no,y>yes","CallerAppType","CurrentAppVersionNo","CurrentAppZipRevisionNo","MasterProductType#--||s@ilit>ilit,myeld>myeld,wtw>wtw,demo>(demo) for Reset iLit only #--#n#"],
	"ValidateLoginForInstructorUsingSAML" : ["TokenID","DeviceTimeStamp","RumbaTicketID","ServiceURL","IsCallerProjectionApp ('y' for Projection app, else select 'n')#--||s@n>no,y>yes","CallerAppType","MasterProductType#--||s@ilit>ilit,myeld>myeld,wtw>wtw #--#n#"],
	"SetSession" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","SessionState #--||s@Start>Start,Stop>Stop"],
	"GetGradeInfoInDetail" : ["TokenID","DeviceTimeStamp","ProductGradeID","CallerClassID","CallerUserID"],
	"GetClassStatus" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID"],
	"GetTOCItems" : ["TokenID","DeviceTimeStamp","ProductGradeID","CallerClassID","CallerUserID"],
	"AssignBookForClass" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID","ItemID"],
	"GetCurrentLessonForClass" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID"],
	"SetCurrentLessonForClass" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID","ItemID"],
	"SetSurvey" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID","UserType #--||s@I>Instructor,S>Student","SurveyType #--||s@LessonSurvey>LessonSurvey,AdHocPoll>AdHocPoll","ActionType #--||s@Start>Start,Stop>Stop,Attempt>Attempt","QuestionID","QuestionInfo","UserResponseItemNo #--#n#"],
	"SaveAttemptDataForGradeableItem" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID (this should be Student's ID)","IAID","ItemID #--#n#","StudentAttemptData #--#n#","StudentAttemptSummary #--#n#","SystemScore #--#n#","FinalScore #--#n#","ItemCompleted (pass '1' if completed, else pass '0')#--||s@0>0 (false),1>1 (true)","IsStudentScored","OralFluencyData","MaxScore"],
	"GetAttemptDataForGradeableItem" : ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID","IAID"],
	"SetBulkLog" : ["TokenID","DeviceTimeStamp","CallerAppType","LogDetails (JSONArray)"],
	"SaveNote" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ShortNoteText #--#n#","NoteID #--#n#","NoteType #--||s@journal>journal,wordbank>wordbank,classnotes>classnotes","NoteTitle","NoteText","NoteRefID #--#n#","RefUnitNumber","RefOtherData"],
	"DeleteNote" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","NoteID"],
	"GetNoteList" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","NoteType #--||s@journal>journal,wordbank>wordbank,classnotes>classnotes","NoteRefID #--#n#","DataRangeType #--||s@Full>Full,Delta>Delta","MaxAttemptRevisionID (mandatory if Delta) #--#n#"],
	"GetNoteInfo" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","NoteID"],
	"GetConferenceStudentData" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID","ItemID","ConfType"],
	"SaveConferenceStudentData" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID","ItemID","ConfType","ConferenceData #--#n#","FinalScore #--#n#","ConferenceTitle #--#n#"],
	"GetListOfConferenceStudentData" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ConfType","StudentID #--#n#"],
	"SetCurrentBookForStudent" : ["TokenID","DeviceTimeStamp","CallerClassID","StudentID","ItemID"],
	"GetCurrentBookForStudent" : ["TokenID","DeviceTimeStamp","CallerClassID","StudentID"],
	"GetLibraryProgress" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID","ItemID"],
	"SaveLibraryProgress" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID","ItemID","ProgressDataSummary","ProgressDataDetails","TotalNumberOfWordsRead","TotalNumberOfSecondsSpent","BookLexileLevel"],
	"Logout" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetIWTTotalWordCount" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetGradebookForStudent" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","DataRangeType #--||s@Full>Full,Delta>Delta", "MaxAttemptRevisionID (mandatory if Delta) #--#n#"],
	"GetGradebookAttemptDataForStudApp" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","DataRangeType #--||s@Full>Full,Delta>Delta", "Item_Attempt_IDs (mandatory if Delta) #--#n#"],
	"AssignGradeableItem" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentItemsJsonArray"],
	"ReAssignGradeableItem" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","IAID","Comment #--#n#"," PKTOralFluencyResults #--#n#"],
	"RemoveGradeableItem" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","IAIDs #--#n#"],
	"GetClassUserDetails" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetRosterForClass" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetGradebookForInstructor" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","DataRangeType #--||s@Full>Full,Delta>Delta", "MaxAttemptRevisionID (mandatory if Delta) #--#n#"],
	"GetGradebookAttemptData" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","DataRangeType #--||s@Full>Full,Delta>Delta", "Item_Attempt_IDs (mandatory if Delta) #--#n#"],
	"GetSurveyResult" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","QuestonID"],
	"SetScoreForGradeableItem" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","IAID","InstructorScore","InstructorScoreRubric #--#n#","WordCount #--#n#","FinalScore","Comments #--#n#","PKTOralFluencyResults"],
	"SetUserLevel" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID","LexileLevel #--#n#","ReadingLevel #--#n#","UserLexileLevelDetails #--#n#","UserReadingLevelDetails #--#n#"],
	"paragraphfeedbackRequest" : ["TokenID","DeviceTimeStamp","prologue:clientId #--||ppd@ilit","prologue:clientPassword #--||ppd@tkptili","prologue:transactionId #--||ppd@c17a43e1-46a6-11e1-a0cd-c82a1416fe11","prologue:studentId #--||ppd@123XYZ","promptId #--||ppd@prompt123","promptType #--||ppd@narrative","instructionalLevel #--||ppd@L12","paragraphType #--||ppd@introduction","paragraph #--||ppd@The quick brown fox jumps teh over lazy grey dog. The dog looks up. The fox darts away. And the dog looks up."],
	"essayScoreRequest" : ["TokenID","DeviceTimeStamp","prologue:clientId #--||ppd@ilit","prologue:clientPassword #--||ppd@tkptili","prologue:transactionId #--||ppd@c17a43e1-46a6-11e1-a0cd-c82a1416fe11","prologue:studentId #--||ppd@123XYZ","promptId #--||ppd@prompt123","essay #--||ppd@The quick brown fox jumps teh over lazy grey dog. The dog looks up. The fox darts away. And the dog looks up."],
	"essayFeedbackRequest" : ["TokenID","DeviceTimeStamp","prologue:clientId #--||ppd@ilit","prologue:clientPassword #--||ppd@tkptili","prologue:transactionId #--||ppd@a838c99e-46a6-11e1-a9a8-c82a1416fe11","prologue:studentId #--||ppd@123XYZ","instructionalLevel #--||ppd@L8","essay #--||ppd@The quick brown fox jumps teh over lazy grey dog. The dog looks up. The fox darts away. And the dog looks up."],
	"scoreSummaryRequest" : ["TokenID","DeviceTimeStamp","prologue:clientId #--||ppd@ilit","prologue:clientPassword #--||ppd@tkptili","prologue:transactionId #--||ppd@c17a43e1-46a6-11e1-a0cd-c82a1416fe11","prologue:studentId #--||ppd@123XYZ","readingId #--||ppd@a564a45fef6f4649908265adccb1fc78","summary #--||ppd@The quick brown fox jumps teh over lazy grey dog. The dog looks up. The fox darts away. And the dog looks up."],
	"summaryFeedbackRequest" : ["TokenID","DeviceTimeStamp","prologue:clientId #--||ppd@ilit","prologue:clientPassword #--||ppd@tkptili","prologue:transactionId #--||ppd@a838c99e-46a6-11e1-a9a8-c82a1416fe11","prologue:studentId #--||ppd@123XYZ","readingId #--||ppd@a564a45fef6f4649908265adccb1fc78","summary #--||ppd@The quick brown fox jumps teh over lazy grey dog. The dog looks up. The fox darts away. And the dog looks up."],
	"SetGradeableItemList" : ["ProductgradeId","ContentbaseUrl","JsonzipUrl","AssignmentPath"],
	"GetLibraryProgressSummary" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetUserLevel" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetMessageList" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ProductCode","AppType #--||ppd@0"],
	"SetBroadcast": ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ActionType  #--||s@Start>Start,Stop>Stop,Update>Update","MediaType #--||s@Image>Image,Video>Video,eBook>eBook,AssignmentOrAssessment>AssignmentOrAssessment","MediaID #--#n#","MediaFullUrl #--#n#","ScribbleData #--#n#","MediaActionType #--#n#||s@NA>NA,Play>Play,Pause>Pause,Stop>Stop,Seek>Seek ","QuestionInformation #--#n#","StudentIDs #--#n#","ScreenMetadata #--#n#"],
	"SetProjection": ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ActionType  #--||s@Start>Start,Stop>Stop,Update>Update","ProjectItemType #--||s@Image>Image,Video>Video,Question>Question, TextNAudio>TextNAudio","MediaID #--#n#","MediaFullUrl #--#n#","ScribbleData #--#n#","MediaActionType #--#n#||s@NA>NA,Play>Play,Pause>Pause,Stop>Stop,Seek>Seek ","QuestionInformation #--#n#"],
	"GetLibraryProgressDetailForClass": ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetskillbasedreportdatabyWeekRange": ["TokenID","DeviceTimeStamp","CallerClassID","CallerUserID"],
	"GetSkillTaxonomyInformation": ["TokenID","DeviceTimeStamp"],
	"SetScribbleData":["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ResetAll #--||s@N>N,Y>Y","Ref_MediaID ","ScribbleDataRecords:ScribbleID #--#n#","ScribbleDataRecords:ScribbleData #--#n#","ScribbleDataRecords:ScribbleDisplayStatus #--#n#||s@1>1(Show),2>2(Hide)","ScribbleDataRecords:ScribbleChangeType #--#n#||s@D>D(for Data insertion),S>S(for Status change)"],
	"iPortal_Login" : ["TokenID","DeviceTimeStamp","UserName","Password","IsPearsonUser #--#n#"],
	"iPortal_Login_For_SAML" : ["TokenID","DeviceTimeStamp","RumbaTicketID","ServiceURL"],
	"iPortal_AdminUser_List" : ["TokenID","DeviceTimeStamp","CallerUserID","DistrictID #--#n#","SchoolID #--#n#","UserRole #--#n#"],
	"iPortal_Product_CreateOrUpdate" : ["TokenID","DeviceTimeStamp","ProductID #--#n#","ProductName","ProductCode","RumbaProductISBN","RumbaProductAuthContextID","MasterProductType"],
	"iPortal_Product_List" : ["TokenID","DeviceTimeStamp"],
	"iPortal_ProductGrade_CreateOrUpdate" : ["TokenID","DeviceTimeStamp","ProductGradeID #--#n#","ProductID","GradeID","DisplayName","RevisionNumber  #--#n#","GradeItemInfoPath","TotalUnits  #--#n#","TotalWeeksWithInGrade  #--#n#","TotalLessonsWithInGrade  #--#n#"],
	"iPortal_ProductGrade_List" : ["TokenID","DeviceTimeStamp","CallerUserID","ProductID"],
	"iPortal_School_List" : ["TokenID","DeviceTimeStamp","CallerUserID","RumbaOrganizationIDForDistrict","GetFromLocal #--||s@0>0,1>1","ShowMySchoolListForInstructor #--||s@n>No (n),y>Yes (y)"],
	"iPortal_Grade_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","ProductID  #--#n#"],
	"iPortal_District_List" : ["IsAllDistrict #--||s@0>0,1>1"],
	"iPortal_User_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","SchoolID  #--#n#","CallerClassID  #--#n#","IsClassRoster   #--#n#--||s@Y>Y,N>N","UserRole  #--#n#--||s@ >All,S>S,I>I","ShowOnlyMyClassesUsers   #--#n#--||s@n>No (n),y>Yes (y)"],
	"iPortal_User_CreateOrUpdate" :  [ "TokenID","DeviceTimeStamp","DistrictID #--#n#","SchoolID #--#n#","UserID #--#n#","StudentID #--#n#","RumbaUserID #--#n#","UserRole #--||s@PA>Pearson Admin,PU>Pearson User,PS>Pearson Sale,DA>District Admin,SCA>School Admin,AT>Assistant Teacher,I>Instructor,S>Student,CT>Co Teacher","RumbaOrganizatioIDForDistrict #--#n#","RumbaOrganizatioIDForSchool #--#n#","UserFullName","UserMetaDataJSONString #--#n#","UserName","Password ","ProductType","InstructorIDforCoTeacher #--#n#","ListOfMonitorClassIDForCoTeacher #--#n#","UserEmailID #--#n#"],
	"iPortal_Class_CreateOrUpdate" :  [ "TokenID","DeviceTimeStamp","DistrictID","SchoolID","ClassID #--#n#","CallerUserID","ProductGradeID","ClassName","DeploymentOptionNumber #--||s@A>A,B>B,C>C","LocalServerID #--#n#","SchoolSession #--#n#","PeriodName #--#n#","ClassDuration #--#n#","ClassStatus #--||s@1>Planned,2>Active,3>Inactive","DemoClass #--||s@Y>Y,N>N","ProductID #--#n#","InstructorID #--#n#","TargetGradeCode #--#n#","IsMasterClass #--||s@Y>Y,N>N","LinkedClassesForMasterClass","IsMasterClassIntegratedClass #--||s@Y>Y,N>N","SuffixForCopiedClass #--||s@>None (blank),Q1>Q1,Q2>Q2,Q3>Q3,Q4>Q4,S1>S1,S2>S2"],
	"iPortal_Class_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","SchoolID  #--#n#","ShowMyClassesOnly #--||s@0>No (0),1>Yes (1)","SourceClassID #--#n#","ShowOnlyMoveableClasses#--||s@0>No (0),1>Yes (1)"],
	"iPortal_Class_Roster_Update" :  [ "TokenID","DeviceTimeStamp","ClassID","UserRole #--||s@I>Instructor,S>Student,CT>Co-Teacher","UserIDcsv #--#n#","SourceClassIDStudentIDMappingForStudDataMove #--#n#"],
	"iPortal_Class_Roster_List" : [ "TokenID","DeviceTimeStamp","ClassID","GetInactiveStudentsForDataCopy  #--#n#","CallerClassID  #--#n#"],
	"iPortal_Grade_CreateOrUpdate" : [ "TokenID","DeviceTimeStamp","GradeID  #--#n#","GradeName","GradeCode","Description"],
	"iPortal_GetSysConfigList" : [ "TokenID","DeviceTimeStamp","CallerUserID"],
	"iPortal_GetSysConfig" : [ "TokenID","DeviceTimeStamp","Key"],
	"iPortal_SetSysConfig" : [ "TokenID","DeviceTimeStamp","Key","Value"],
	"iPortal_DeleteSysConfig" : [ "TokenID","DeviceTimeStamp","Key"],
	"RumbaGetDistrictList" :[ "ProductType  #--||s@cai45>CA 45,cae45>CA ELL, cap45>CA Premium, ilit>National 90 ILIT"],
	"OralfluencyGetScoreData": [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","TestInstanceId"],
	"OralfluencySubmitScoreRequest": [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","TestInstanceId","ItemType #--#n#","ItemId #--#n#","ContentName #--#n#","ContentURL #--#n#"],
	"OralfluencyReservation" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"iPortal_ProductGrade_Info" : [ "TokenID","DeviceTimeStamp","ProductGradeID"],
	"iPortal_ProductGrade_Delete" : [ "TokenID","DeviceTimeStamp","ProductGradeID"],
	"iPortal_User_Info": [ "TokenID","DeviceTimeStamp","UserID"],
	"iPortal_Class_Delete" : ["TokenID","DeviceTimeStamp","CallerClassID"],
	"iPortal_Class_Info" : ["TokenID","DeviceTimeStamp","CallerClassID"],
	"iPortal_Class_Summary" : ["TokenID","DeviceTimeStamp","CallerClassID"],
	"iPortal_District_Servers_CreateOrUpdate" : ["TokenID","DeviceTimeStamp","LocalServerID #--#n#","DisplayName #--#n#","ServerIPAddress #--#n#","LocalServerServicePath #--#n#","DistrictID","SchoolID"],
	"iPortal_District_Servers_List" : ["TokenID","DeviceTimeStamp","DistrictID #--#n#"],
	"iPortal_District_Servers_Delete" : ["TokenID","DeviceTimeStamp","LocalServerID"],
	"iPortal_District_Servers_Info" : ["TokenID","DeviceTimeStamp","LocalServerID"],
	"iPortal_ClassRoster_Disable" : ["TokenID","DeviceTimeStamp","ClassID #--#n#","UserID","Active #--||s@Y>Y,N>N"],
	"iPortal_ResetClassProgressData" : ["TokenID","DeviceTimeStamp","ClassID","CallerUserID"],
	"iPortal_DBLoadSummaryReport" : ["TokenID","DeviceTimeStamp","PassCode","getActualCounts #--||s@0>N,1>Y"],
	"iPortal_Remove_ClassRoster" : ["TokenID","DeviceTimeStamp","UserIDsCSV","ClassID"],
	"iPortal_CheckBulkUserValidation" : ["TokenID","DeviceTimeStamp","UploadFileID"],
	"iPortal_CreateSubscribeBulkUsers" : ["TokenID","DeviceTimeStamp","UploadFileID","SchoolID","DistrictID","ProductCode","RegisterUserIntoClassView #--||s@true>Y,false>N","SubscribeUserIntoProduct #--||s@true>Y,false>N","ClassID #--#n#"],
	"iPortal_ClassRoster_MoveUser" : ["TokenID","DeviceTimeStamp","NewClassID","OldClassID","StudentIDsCSV"],
	"iPortal_UsersCountReportByUserRole" : ["PassCode"],
	"GetskillbasedreportdatabyDateRange" : ["TokenID","DeviceTimeStamp","CallerClassID #--#n#","AccYearID","StartDate","EndDate","SkillReportTabNumber #--#n#","DistrictID","SchoolIDs #--#n#","ClassIDs #--#n#"],
	"iPortal_DBL_GetTotalUsersOnline" : ["TokenID","PassCode","IsOnlineOnToday #--||s@true>Y,false>N"],
	"SyncSkillTaxonomy" : ["PassCode"],
	"GetClassUserLevel" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetBuzzCmtDetails" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","TestInstanceId","ItemType #--#n#","ItemId #--#n#","ContentName #--#n#","ContentURL #--#n#"],
	"SetBuzzComment" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ResetCmtForClass #--#n#","IsCommentForClass #--#n#","StudentID #--#n#","StarCount #--#n#","CMTForBuzz #--#n#"],
	"GetPollList" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetPollInfo" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","PollID #--#n#"],
	"UpdatePoll" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","PollID #--#n#","PollTitle #--#n#","PollQuestion #--#n#","PollChoices #--#n#","DeletePoll #--#n#"],
	"iPortal_District_Info" : [ "TokenID","DeviceTimeStamp","DistrictID"],
	"iPortal_District_AddUpdateProducts" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","ProductIDs #--#n#","ProductISBNs #--#n#","IsEdCloudDistrict #--#n#","DistrictSuffix #--#n#","SectionIDs #--#n#","State #--#n#","City #--#n#","Notes #--#n#","InvalidSectionIDs #--#n#","DeploymentOptionChoice #--#n#||s@A>A,B>B","LocalServerID #--#n#", "StudentEnrollmentModel #--#n#||s@1>1 (manual),2>2 (auto),3>3 (EssayBridge)","EasyBridgeDistrictType #--#n#"],
	"MigrateUsersUsingCSV" : [ "PassCode","UserCSVPath"],
	"RumbaCreateOrUpdateUser" : ["RumbaUserId #--#n#","UserName","Password","FirstName","MiddleName #--#n#","LastName","Title #--#n#","Suffix #--#n#","Gender #--#n#","DateOfBirth #--#n#","Email #--#n#","WorkPhone:IsPrimary #--#n#||s@false>N,true>Y","WorkPhone:PhoneNumber #--#n#","WorkPhone:CountryCode #--#n#", "CellPhone:IsPrimary #--#n#||s@false>N,true>Y","CellPhone:PhoneNumber #--#n#","CellPhone:CountryCode #--#n#","BusinessRuleSet #--||ppd@CG","UserType #--||s@T>Instructor (T),S>Student (S),PA>Pearson Admin (PA - for Pearson Admin / Pearson User / Classview Manager),CA>Customer Admin (CA - for District Admin / School Admin)","CreatedBy #--||ppd@ffffffff52fa5204e4b012499eea19b0","DistrictID #--#n#","OrganizationId #--#n#"],
	"RumbaSubscription" : [ "UserId","ProductType #--||s@cai45>CA 45,cae45>CA ELL, cap45>CA Premium, ilit>National 90 ILIT","CreatedBy #--||ppd@ffffffff52fa5204e4b012499eea19b0","UserType #--||s@T>Instructor,S>Student","OrganizationId"],
	"CheckUserNameAvailability" : [ "UserName", "DistrictID"],
	"ValidatePassword" : [ "Password", "UserRole #--||s@T>teacher,S>Student"],
	"RumbaCreateUserInBatch" : [ "TokenID","DeviceTimeStamp","BatchID #--#n#","UserDetails"],
	"RumbaGetUserBatchStatus" : [ "TokenID","DeviceTimeStamp","BatchID"],
	"iPortal_PurgeAllTokens" : [ "TokenID","PassCode"],
	"iPortal_ReplaceSchoolSession" : [ "PassCode","OldSSID","NewSSID"],
	"iRPT_District_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","ProductIDs #--#n#"],
	"iRPT_School_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","ProductIDs #--#n#","TeacherIDs"],
	"iRPT_Product_List" : [ "TokenID","DeviceTimeStamp","CallerUserID"],
	"iRPT_Class_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","SchoolID","UserID #--#n#","ShowMyClassesOnly #--#n#","ProductIDs #--#n#"],
	"iRPT_Class_Roster_Details" : [ "TokenID","DeviceTimeStamp","CallerUserID","ClassID"],
	"iRPT_LRS_Logs" : [ "TokenID","DeviceTimeStamp","CallerUserID","Date","ProductID #--#n#","DistrictID #--#n#","SchoolID #--#n#","ClassID #--#n#","VerbID #--#n#","UserID #--#n#", "ReportType #--||s@1>Only Summary Info,3>Summary + Details"],
	"iRPT_SYS_Logs" : [ "TokenID","DeviceTimeStamp","CallerUserID","Date","ClassID #--#n#","LogType #--||s@ALL>All,INFO>Info,WARNING>Warning,ERROR>Error","ProductID #--#n#","DistrictID #--#n#","SchoolID #--#n#","VerbID #--#n#","UserID #--#n#","CallerTypeID #--#n#", "ReportType #--||s@1>Only Summary Info,3>Summary + Details"],
	"iRPT_Verb_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","VerbType #--||s@LRS>LRS,SYS>SYS"],
	"iRPT_Caller_App_List" : [ "TokenID","DeviceTimeStamp","CallerUserID"],
	"iRPT_DR_GroupWiseSummary" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","GradeIDs #--#n#","ProductIDs #--#n#","DistrictID #--#n#","SchoolID #--#n#","ClassID #--#n#","GroupType #--#n#" ],
	"iRPT_DR_ReadingCompreDetail" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","GradeIDs #--#n#","ProductIDs #--#n#","DistrictID #--#n#","SchoolID #--#n#","ClassID #--#n#" ],
	"iRPT_DR_Reading_Growth" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","GradeIDs #--#n#","ProductIDs #--#n#","DistrictID #--#n#","SchoolID #--#n#","ClassID #--#n#"],
	"iRPT_DR_LA_Summary" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","GradeIDs #--#n#","ProductIDs #--#n#","DistrictID #--#n#","SchoolID #--#n#","ClassID #--#n#"],
	"iRPT_GR_Summary" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","GradeIDs #--#n#","ProductIDs #--#n#","DistrictID","SchoolID #--#n#","ClassID #--#n#"],
	"iRPT_Grade_List" : [ "TokenID","DeviceTimeStamp","CallerUserID"],
	"iRPT_User_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","SchoolID #--#n#","UserRole"],
	"iRPT_Product_Grade_List" : [ "TokenID","DeviceTimeStamp","CallerUserID","GradeIDs #--#n#","ProductIDs #--#n#" ],
	"GetClassList" : [ "TokenID","DeviceTimeStamp","CallerUserID","MasterProductType#--||s@ilit>ilit,myeld>myeld,wtw>wtw #--#n#"],
	"SaveUserSettings" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","PersonalSettings"],
	"GetUserSettings" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"iRPT_GR_Summary" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","GradeIDs #--#n#","ProductIDs #--#n#","DistrictID","SchoolID #--#n#","ClassID #--#n#"],
	"iPortal_IncrementClassesRevisionIds" : [ "TokenID","DeviceTimeStamp","CallerUserID","PassCode"],
	"SaveClassSettings" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","SaveSettingsToAllNewClasses #--||s@n>n,y>y","AssignmentSendingMode #--#n#","AcceptOralFluencyScore #--#n#","ShowCriticalResponse #--#n#","ClassGraphSnapshot #--#n#","StudentDataSnapshot #--#n#","ProjectSnapshot #--#n#","AvatarLibrary #--#n#","AcceptLibraryResponseScore #--#n#","AcceptNarrativeEssayResponseScore #--#n#"],
	"SaveClassCalendarSettings" : [  "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","SaveSettingsToAllNewClasses #--||s@n>n,y>y","AcademicCalendarJSON #--#n#" ],
	"GetClassSettings" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID" ],
	"SetCurrentWeekForClass" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","WeekNo"],
	"GetCurrentWeekForClass" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"CheckAssignAutoScoreGradeableItems" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"SaveInterestInventory" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","SurveyResponse","StudenInterestInventory","InventoryTerm #--||s@B>B,M>M,E>E"],
	"iPortal_GetUserSettings" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID #--#n#"],
	"iPortal_SaveUserSettings" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID #--#n#","PersonalSettings"],
	"iPortal_UpdateEdCloudLinkedClass" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ProductGradeID","ClassId","ClassName","ClassPeriod","ClassState","SchoolSession","DeploymentModel","TargetGradeCode #--#n#"],
	"SaveBookReviewFeedback" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ItemID","Rating #--#n#","Comments #--#n#","CSVFeebackTags #--#n#"],
	"GetBookReviewFeedback" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ItemIDs","StudentID"],
	"GetItemSkillMapping" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ProductGradeID"],
	"ReserveOrUnreserveBook" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ItemID","Action #--||s@0>0 (means reserve),1>1 (means unreserve)"],
	"GetListOfReservedBooks" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetDistrictList" : [ "TokenID","DeviceTimeStamp","ProductType"],
	"iPortal_GetDistrictSuffixFromEdcloud" : [ "TokenID","DeviceTimeStamp","CallerUserID","RumbaDistrictID"],
	"iPortal_GetListofEdCloudClasses" : [ "TokenID","DeviceTimeStamp","CallerUserID","ClassState #--||s@--->---,Pending>Pending","StudentEnrollementModel #--||s@2>2 (for EdCloud Class Queue),3>3 (for EasyBridge Pending Classes)","CSVDistrictIDs #--#n#","CSVSchoolIDs #--#n#"],
	"GetUserFeedbackforAllBooks" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"iPortal_ValidateEdcloudSections" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","CSVofSectionIDs"],
	"iPortal_CreateEdcloudClasses" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","CSVofValidSectionIDs"],
	"iPortal_FindAndSyncRosterForEdcloudLinkedClasses" : ["PassCode","FullSync #--||s@Y>Y (Yes),N>N (No)","SyncDurationInMinutes"],
	"GetRecommendedBooks" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID"],
	"iRPT_Class_Roster_DetailsForDistrict" : [ "TokenID","DeviceTimeStamp","CallerUserID","DistrictID","CSVofSchoolIDs #--#n#","CSVofClassIDs #--#n#","UserRole #--#n#"],
	"RumbaGetOrganizationInfo" : ["OrganizationId"],
	"iPortal_Class_Roster_List_CSV":["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID"],
	"GetInterestInventory" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","StudentID"],
	"iRPT_BookReviewSummaryForClass" : [ "TokenID","DeviceTimeStamp","ClassId"],
	"iRPT_GetInterestInventoryForClass" : [ "TokenID","DeviceTimeStamp","ClassId"],
	"iRPT_AllDistrict_Stats" : [ "TokenID","DeviceTimeStamp"],
	"iRPT_GetEdcloudSyncLogs" :  [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","DistrictID","SchoolID #--#n#","ClassID #--#n#","FromDate #--#n#","ToDate #--#n#"],
	"iPortal_DeleteClassviewUser" : ["TokenID","DeviceTimeStamp","CallerUserID","UserID"],
	"iPortal_CreateClassWithMultipleUsers" : ["TokenID","DeviceTimeStamp","CallerUserID","ProductGradeID","TotalStudentsInClass","TotalClassesForThatUser #--#n#","UserPassword","SchoolSessionYear","TargetGradeCode #--#n#","DistrictId","SchoolId"],
	"iPortal_CreateMultipleClassesWithOneStudent" : [ "TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","PGIDAndTargetGradeCodesCSV","TotalClassesForThatUser","UserPassword","SchoolSessionYear","DistrictID","SchoolID"],
	"iRPT_InterestInventoryAnalysisSummary" : [ "TokenID","DeviceTimeStamp","CallerUserID","AccYearID","DistrictID #--#n#","SchoolID #--#n#" ],
	"iRPT_GetUserList" : ["TokenID","DeviceTimeStamp","CallerUserID","AccYearID","SearchText","CSV_UserRoles #--#n#","UserID #--#n#","ShowOnlyMyClassesUsers #--||s@0>0,1>1"],
	"iRPT_DistrictSummary" : ["TokenID","DeviceTimeStamp","CallerUserID","AccYearID","DistrictID"],
	"DO2_List_Of_Content_Available_for_Cache" : ["PassCode","NetworkServerID","IPAddress","MacAddress #--#n#"],
	"DO2_GetGradeInfoDetails" : ["PassCode","ProductGradeID"],
	"GetRumbaUserInfo" : ["TokenID","DeviceTimeStamp","CallerUserID","RumbaUserId"],
	"RumbaValidateLogin" : ["TokenID","DeviceTimeStamp","Username","Password"],
	"iPortal_CheckForMovementOfUsers" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID #--#n#","UserIDs","DestinationClassID"],
	"UpdateUserDistrictSuffixInRumba" : ["TokenID","PassCode","RumbaUserIDs","DistrictSuffix"],
	"iRPT_ProductUsed" :  ["TokenID","AccYearID"],
	"iRPT_TotalUsersProductWise" : ["TokenID","PassCode","AccYearID"],
	"iRPT_AutoRosterDetail" : ["TokenID","DeviceTimeStamp","CallerUserID","AccYearID","DistrictID","SchoolID","ClassID"],
	"iRPT_AutoRosterUserConflictLogs" : ["TokenID","DeviceTimeStamp","CallerUserID","AccYearID","DistrictID #--#n#","FromDate (dd-mm-yyyy) #--#n#","ToDate (dd-mm-yyyy) #--#n#","ReportType #--||s@''>Select,CR>Create,SN>Sync"],
	"iPortal_PVTRegisterDemoUserAndClass" : ["TokenID #--#n#","DeviceTimeStamp #--#n#","CallerUserID #--#n#","IntAppPassCode","EmailID","FirstName","LastName","ProductType #--||s@supplemental>Supplemental,literacy intervention>Literacy Intervention,english language development>English Language Development,words their way>Words Their Way","GradeCode #--||s@4>4,7>7,9>9","ExistingCustomer #--||s@0>0 (No),1>1 (Yes)"],
	"iRPT_DistrictOnelineSummary" : ["TokenID","AccYearID"],
	"SubPubSubscriptionRequest" : ["CallBackUrl","tags"],
	"GetSectionNotifications" : ["SectionID"],
	"GetProductInfoOfClasses" : ["SectionID"],
	"GetSubPubSubscriptionDetails" : ["SubscriptionID"],
	"GetSubPubSubscriptionDetailsForPrincipal": [],
	"DeleteSubPubSubscription" : ["SubscriptionID"],
	"SaveClassesPreferences": ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID #--#n#","ClassPreferences"],
	"iPortal_Orphan_User_List": ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID #--#n#","DistrictID","SchoolID #--#n#"],
	"iRPT_DistrictSummaryGrade":["TokenID","DeviceTimeStamp","CallerUserID","AccYearID"],
	"GetClassesPreferences":["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ClassID_CSV"],
	"iPortal_DoProcessingOfPendingEasyBridgeSyncingClasses":["PassCode"],
	"iRPT_StudentUsageReportAsCSV":["TokenID","DeviceTimeStamp","CallerUserID","AccYearID","DistrictID","SchoolID","CSVofClassID #--#n#"],
	"iPortal_Class_StatusUpdate":["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID","ClassID","ClassStatus"],
	"GetPendingClassList" : ["TokenID","DeviceTimeStamp","CallerUserID","CallerClassID #--#n#","MasterProductType #--#n#"],
	"GetClassInfo" : ["TokenID","DeviceTimeStamp","CallerUserID","ClassID"],
	"GetProductGradeList" : ["TokenID","DeviceTimeStamp","CallerUserID","ProductID"],
	"UpdatePendingClassNotification" :  ["TokenID","DeviceTimeStamp","CallerUserID","ClassID "],
	"UpdateClassForEasyBridgeLinkedClass" :  ["TokenID","DeviceTimeStamp","CallerUserID","ProductGradeID","ClassID","ClassName","ClassPeriod","TargetGradeCode #--#n#"],
	"GetGradeList" :  ["TokenID","DeviceTimeStamp","CallerUserID"],
	"ResetClassDataSetForEasyBridgeLinkedClass" : ["TokenID","DeviceTimeStamp","CallerUserID","ClassID"],
	"iPortal_ConfigureUserRoleForDistrictOrSchoolAdmin" : ["RumbaUserID", "UserRole", "SchoolID #--#n#"],
	"AdminLogin" : ["TokenID","DeviceTimeStamp","RumbaUserID"],
	"GetAutoOrPlusAdminURL" : ["DistrictID","CallBackURL #--||ppd@https://iseriesqa-bts2017.learningmate.com/webclient/instructor.html"],
	"iPortal_GetDistrictEBTypeAndSSOLoginURL" : ["TokenID","DeviceTimeStamp","DistrictID","CallbackURL"],
	"iPortal_MigrateBTS2016Districts" : ["PassCode","ENVPrefixForDynamodb"],
	"GetRumbaLicensedProductsForDistrict" : ["OrganizationID","GetAll #--||s@all>All,onlylicensed>Only Setup Products"],
	"iPortal_ResetAndMigrateBTS2016PearsonRoleUsers" : ["PassCode","ENVPrefixForDynamodb"],
	"iPortal_DeleteEverythingForDistirct" : ["PassCode","DistrictID"],
	"iPortal_DisableLogin" : ["TokenID","DeviceTimeStamp","CallerUserID","UserID","DisableLogin#--||s@1>Y,0>N"],
	"Temp_iPortal_UpdateScoreForWordReadingIssue" : ["PassCode","JsonStructure"],
	"iPortal_CopyClassData" : ["TokenID","DeviceTimeStamp","CallerUserID","SourceClassID","DestinationClassID","CSV_StudentIDsOfSourceClass #--#n#","SuffixForCopiedClass #--#n#"],
	"iPortal_CheckStatusBulkCopyClassDataCSV": ["TokenID","DeviceTimeStamp","CallerUserID","JobId"],
	"iPortal_District_Setting_Reset": ["TokenID","DeviceTimeStamp","CallerUserID","DistrictID"],
	"iPortal_CompareUAICache": ["TokenID","DeviceTimeStamp","CallerUserID #--#n#","CallerClassID","PassCode"],
	"iPortal_Temp_NormalizeStudentIDs": ["TokenID","PassCode"],
	"iPortal_Temp_UpdateAllEBStudentIDs": ["TokenID","PassCode"]*/
};

$(document).ready(function(e) {
	var  serviceTokenData = {"AppType":"webclient","AppVersionNumber":"1","LastTokenID":localStorage.getItem("TOKENID"),"PassCode":"9999"};
	var serviceTokenURL = SERVICEBASEURL+'GetServiceTokenID';
	//AjaxCall(serviceTokenURL,"post",serviceTokenData,sucessCallBackserviceToken, errorCallBack);
	
	function sucessCallBackserviceToken(responce) {
		var  responceJSON = JSON.parse(responce);
		if(responceJSON.Status == "200" || ((responceJSON.Status == "500") && (responceJSON.Error.ErrorCode == 'U1019'))){
			localStorage.setItem("TOKENID", responceJSON.Content.TokenID);
			localStorage.setItem("DATADEVICETIMESTAMP", responceJSON.Content.ExpiryDate);
			bindLogin();
		}
	} 
	function errorCallBack(){
	}
	bindLogin();
	
});

// FUNCTION TO BIND LOGIN Button
function bindLogin() {
	$('#btnLogin').unbind('click').bind('click', function(e) {
		var userName = $("#txtUserNm").val();
		var Password = $("#txtPassword").val();
		if(userName=='' || Password==''){
			alert('Enter Username or password');
		}else{
                    if(userName=='admin' || Password=='admin'){
                        setLocalStorageItem('loginTestHarness', 'done');
				$('#loginSection').hide();
				$('#testharness').show();
				bindEvents();
                    }
                }
		/*var LoginObj = {"TokenID":localStorage.getItem("TOKENID"),"DeviceTimeStamp":localStorage.getItem("DATADEVICETIMESTAMP"),"UserName":userName,"Password":Password};
		var url=SERVICEBASEURL+'/testharnessAdminLogin' ;
		AjaxCall(url,"post",LoginObj,sucessCallBackLogin, errorCallBack);
		}*/
	});
	function sucessCallBackLogin(responce){
		var responceJSON = JSON.parse(responce);
		if(responceJSON.Status == "200" || ((responceJSON.Status == "500") && (responceJSON.Error.ErrorCode == 'U1019'))){
			
			if(responceJSON.Content.Userinfo.UserRole == 'SA'){
				setLocalStorageItem('loginTestHarness', 'done');
				$('#loginSection').hide();
				$('#testharness').show();
				bindEvents();
			}else
				alert('Only SuperAdmin is allowed');
		}else
			alert(responceJSON.Error.ErrorUserDescription);
	}
	function errorCallBack(){
	}
	
}

// 	FUNCTION TO BIND ALL THE INTERACTIVES ELEMENTS ON THE PAGE
function bindEvents() {
	$('#servcTypeSelect').unbind('change').bind('change', function(e) {
		var val = $(this).val();
		var servicesArr = [];
		//servicesArr.push('##');
		$('#inputBlockWrapper').hide();
		$('#inputBlock').html('');	
		switch(val.toLowerCase()) {
			case 'acmt_admin':
				servicesArr.push.apply(servicesArr, ADMINSERVICES);
				break;
			
		}
		if(servicesArr.length > 0) {
			$('#servicesListSelect').html('').prop("disabled", false);
			for(var i =0; i < servicesArr.length; i++){
				if(servicesArr[i] ==  '##') {
					//$('#servicesListSelect').append('<option value="'+servicesArr[i]+'" disabled>---------common services end here---------</option>');
				} else {
					$('#servicesListSelect').append('<option value="'+servicesArr[i]+'">'+servicesArr[i].split(':::')[0]+'</option>');
				}
			}
		} else {
			$('#servicesListSelect').html('<option>No Services found for this Service Type!</option>').prop("disabled", true);
		}
		$('#servicesListSelect').trigger('change');
		return false;
	});
	
	$('#servicesListSelect').unbind('change').bind('change', function(e) {
		var val = $(this).val().trim();
		if(val.indexOf(':::')!=-1)
			val = val.split(':::')[0];
		var fieldArr = [];
		var htmlStr = '';
		fieldArr = fieldNamesJSON[val];
		$('#outputblock').hide();
		if(fieldArr == undefined) {
			if( val.split(':::')[0]== 'iPortal_District_List' ){
				$('#serviceNameLabel').html(val.split(':::')[0]);
				$('#inputBlockWrapper').show();
				$('#inputBlock').html(htmlStr);
				return true;
			}
			else{
				$('#outputblock').hide();
				alert('Service definition not found. Please check.');
				return false;
			}
			
		}
		$('#serviceNameLabel').html(val);
		var appType = $('#servcTypeSelect').val().toLowerCase().trim();
		for(var i =0; i < fieldArr.length; i++) {
			if(fieldArr[i].indexOf('#n#') > -1) {
				htmlStr+= '<div class="fieldBlock">';

					if(appType =='pkt' || $('#servicesListSelect').val()=='SetScribbleData'){
						var fieldLabelStr1 = fieldArr[i].split('#--')[0];
						if(fieldLabelStr1.indexOf(':')!= -1){
							var fieldL = fieldLabelStr1.split(':')[1];
							htmlStr+= '	<label class="label-left">'+'<span id="subfield">'+ fieldLabelStr1.split(':')[0]+':</span>'+fieldL+'</label>';
						}else
							htmlStr+= '	<label class="label-left">'+fieldLabelStr1+'</label>';
					}else
						htmlStr+= '	<label class="label-left">'+fieldArr[i].split('#--')[0]+'</label>';
			}else {
				htmlStr+= '<div class="fieldBlock" mandatory>';
				if(appType =='pkt'|| $('#servicesListSelect').val()=='SetScribbleData'){
					var fieldLabelStr1 = fieldArr[i].split('#--')[0];
					if(fieldLabelStr1.indexOf(':')!= -1){
						var fieldL = fieldLabelStr1.split(':')[1];
						htmlStr+= '	<label class="label-left">'+'<span id="subfield">'+ fieldLabelStr1.split(':')[0]+':</span>'+fieldL+'<span class="mandatoryClass">*</span></label>';
					}else
						htmlStr+= '	<label class="label-left">'+fieldLabelStr1+'<span class="mandatoryClass">*</span></label>';
				}else
					htmlStr+= '	<label class="label-left">'+fieldArr[i].split('#--')[0]+'<span class="mandatoryClass">*</span></label>';
			}
			htmlStr+= '	<div class="clear"></div>';
			var fieldLabelStr1 = fieldArr[i].split('#--')[0];
			var id = fieldLabelStr1.split(':')[0];
			//console.log('id:'+id.trim());
			if(fieldArr[i].indexOf('||s@') == -1) {
				if(fieldArr[i].indexOf('||ppd@') > -1) {
					htmlStr+= '	<input type="text" id="'+id.trim()+'" value="' + fieldArr[i].split('||ppd@')[1] + '" />';
				} else {
					htmlStr+= '	<input type="text" id="'+id.trim()+'" />';
				}
			} else {
				htmlStr+= '	<select>';
				var selOptStr = fieldArr[i].split('||s@')[1];
				var selOptArr = selOptStr.split(',');
				var optArr = [];
				for(var j = 0; j < selOptArr.length; j++){
					optArr = selOptArr[j].split('>');
					htmlStr+= ' <option id="'+id.trim()+'" value="'+optArr[0]+'">'+optArr[1]+'</option>';
				}
				htmlStr+= '	</select>';
			}
			htmlStr+= '	<div class="clear"></div>';
			htmlStr+= '</div>';
		}
		$('#inputBlockWrapper').show();
		$('#inputBlock').html(htmlStr);
		
		$('input#TokenID').val(localStorage.getItem("TOKENID"));	
		$('input#DeviceTimeStamp').val(localStorage.getItem("DATADEVICETIMESTAMP"));
		

		return false;
	});
	
	$('#btnSubmit').unbind('click').bind('click', function(e) {
		var mandatoryFieldArr = [];
		var allMandatoryCheckFlag = true;
		var appType = $('#servcTypeSelect').val().toLowerCase().trim();
		mandatoryFieldArr = $('#inputBlock .fieldBlock[mandatory]');
		for(var i = 0; i < mandatoryFieldArr.length; i++) {
			var elemMandate = $(mandatoryFieldArr[i]).find('input,select')[0];
			var elemVal = $(elemMandate).val().trim();
			if(elemVal == '') {
				allMandatoryCheckFlag = false;
				break;
			}
		}
		if(allMandatoryCheckFlag) {
			if(appType == 'pkt') {
				makeServiceCallForPKT();
			}/*  else if(appType == 'rumba') {
				
			}  */else {
				makeServiceCallForApp();
			}
		} else {
			alert('Please fill all mandatory fields and try again.');
		}
	});
	
	$('#btnLogout').unbind('click').bind('click', function(e) {
		//clearSessionStorage();
		clearLocalStorage();
		window.location.reload(true);
	});
	
	if(!ISRELEASEONDEV){
		$("#servcTypeSelect option[value='inst']").remove();
		$("#servcTypeSelect option[value='stud']").remove();
		$("#servcTypeSelect option[value='pkt']").remove();
		$("#servcTypeSelect option[value='clsv']").remove();
		$("#servcTypeSelect option[value='irtp']").remove();
		
		$('#servcTypeSelect').trigger('change');
	}else
		$('#servcTypeSelect').trigger('change');
}

// FUNCTION TO DO VALIDATION, POST OBJECT FORMATION AND PASS TO AJAX CALL
function makeServiceCallForPKT() {
	var inputBlockArr = [], postObj = {}, prologueObj={}, serviceValArr = [], serviceName = '', methodType = '', url = '';
	inputBlockArr = $('#inputBlock .fieldBlock');
	serviceValArr = $('#servicesListSelect').val().trim().split(':::');
	serviceName = serviceValArr[0];
	methodType = (serviceValArr[1] == undefined) ? 'POST' : 'GET';
	for(var i = 0; i < inputBlockArr.length; i++) {
		var inputField = $(inputBlockArr[i]).find('input,select')[0];
		var fieldName = $(inputBlockArr[i]).find('label').text().trim();
		if(fieldName.indexOf('prologue:')!= -1)
		{
			var subfieldName = fieldName.split(':')[1];
			var fieldName = fieldName.split(':')[0];
			prologueObj[subfieldName.replace(/\*/g, '')]=$(inputField).val().trim();
			postObj[fieldName.replace(/\*/g, '')] = prologueObj;
		}else{
			fieldName = String(fieldName.split(' ')[0]).trim();
			postObj[fieldName.replace(/\*/g, '')] = $(inputField).val().trim();
		}
	}
	$('#outputblock p').html('Loading.........');
	$('#outputblock').show();
	console.log(JSON.stringify(postObj, null, 4));
	url = SERVICEBASEURL + serviceName;
	AjaxCall(url, methodType, postObj, displayResponse);
}


// FUNCTION TO DO VALIDATION, POST OBJECT FORMATION AND PASS TO AJAX CALL
function makeServiceCallForApp() {
	var inputBlockArr = [], postObj = {},Getdata= '',ScribbleDataRecordsObj={}, serviceValArr = [], serviceName = '', methodType = '', url = '';
	inputBlockArr = $('#inputBlock .fieldBlock');
	serviceValArr = $('#servicesListSelect').val().trim().split(':::');
	serviceName = serviceValArr[0];
	methodType = (serviceValArr[1] == undefined) ? 'POST' : 'GET';
	if (serviceName == 'SetScribbleData'){
		for(var i = 0; i < inputBlockArr.length; i++) {
			var inputField = $(inputBlockArr[i]).find('input,select')[0];
			var fieldName = $(inputBlockArr[i]).find('label').text().trim();
			if(fieldName.indexOf('ScribbleDataRecords:')!= -1)
			{
				var subfieldName = fieldName.split(':')[1];
				var fieldName = fieldName.split(':')[0];
				ScribbleDataRecordsObj[subfieldName.replace(/\*/g, '')]=$(inputField).val().trim();
				postObj[fieldName.replace(/\*/g, '')] = ScribbleDataRecordsObj;
			}else{
				fieldName = String(fieldName.split(' ')[0]).trim();
				postObj[fieldName.replace(/\*/g, '')] = $(inputField).val().trim();
			}
		}

	}else {
		for(var i = 0; i < inputBlockArr.length; i++) {
			var inputField = $(inputBlockArr[i]).find('input,select')[0];
			var fieldName = $(inputBlockArr[i]).find('label').text().trim();
			fieldName = String(fieldName.split(' ')[0]).trim();
			if(methodType == 'POST')
				postObj[fieldName.replace(/\*/g, '')] = $(inputField).val().trim();
			else
				Getdata+='/'+$(inputField).val().trim();
		}
	}
	
	var beforeSendHeaderInfo = null;//AjaxCallWithHeader
	//collect header info for specific services
	if (serviceName == 'iPortal_PVTRegisterDemoUserAndClass')
	{
		var encodedPassCode = btoa(postObj.IntAppPassCode);
		beforeSendHeaderInfo = function (xhr) { xhr.setRequestHeader('IntAppPassCode', encodedPassCode); };
	}
	
	$('#outputblock p').html('Loading.........');
	$('#outputblock').show();
	console.log(JSON.stringify(postObj, null, 4));
	var appType = $('#servcTypeSelect').val().toLowerCase().trim();

	if(methodType === 'POST'){
		if(appType == 'clsv' || appType == 'analy' || appType == 'other' || appType == 'irpt' || appType == 'rediflit'){
			if( serviceName== 'SetGradeableItemList' || serviceName== 'GetDistrictList')
				var url = SERVICEBASEURL +serviceName;
			else
				var url = SERVICEBASEURL +'CCA/'+ serviceName;
		}
		else if(appType == 'rumba')
			var url = SERVICEBASEURL +'Rumba/'+ serviceName;
		else if(appType == 'ebserv')
			var url = SERVICEBASEURL +'EasyBridge/'+ serviceName;
		else
			var url = SERVICEBASEURL + serviceName;
		
		if (appType == 'rediflit' && serviceName== 'iPortal_PVTRegisterDemoUserAndClass')
			AjaxCallWithHeader(url, methodType, postObj, displayResponse, beforeSendHeaderInfo);
		else
			AjaxCall(url, methodType, postObj, displayResponse);
	}
	else{
		if(appType == 'clsv' || appType == 'irpt')
				var url = SERVICEBASEURL +'CCA/'+ serviceName + Getdata;	
		else if(appType == 'rumba')
			var url = SERVICEBASEURL +'Rumba/'+ serviceName + Getdata;
		else
			var url = SERVICEBASEURL + serviceName + Getdata;
		AjaxCall(url, methodType, "", displayResponse);
	}
}


// FUNCTION TO DISPLAY RESPONSE OF THE SERVICE CALL MADE
function displayResponse(response) {
	try{
		var obj = JSON.parse(response);
	}catch(e){}
	
	if(obj!=null || obj!=undefined)
		$('#outputblock p').html(JSON.stringify(obj, null, 4));
	else
		$('#outputblock p').html(JSON.stringify(response, null, 4));
}

// GENERIC AJAX ASYNC FUNCTION CALL
function AjaxCall(DestinationUrl, MethodType, Data, CallBack) {
    var DTO;
    if (MethodType === "GET" || MethodType === "DELETE") {
        DTO = null;
    }
    else { DTO = JSON.stringify(Data); }

	try {
		$.ajax({
			url: DestinationUrl,
			cache: false,
			type: MethodType,
			contentType: 'application/json; charset=utf-8',
			data: DTO,
			success: function (data) {
				if (CallBack && (typeof CallBack == "function")) {
					CallBack(data);
				}
				else { DTO = data; }
			},
			error: function (err, txtStatus, errorThrown) {
				alert("Error! " + txtStatus);
			}
		});
	} catch(e) {}

    return DTO;
}

function AjaxCallWithHeader(DestinationUrl, MethodType, Data, CallBack, beforeSendHeaderCall) {
    var DTO;
    if (MethodType === "GET" || MethodType === "DELETE") {
        DTO = null;
    }
    else { DTO = JSON.stringify(Data); }

	try {
		$.ajax({
			url: DestinationUrl,
			cache: false,
			beforeSend: beforeSendHeaderCall, //function (xhr) { xhr.setRequestHeader('IntAppPassCode', encodedPassCode); },
			type: MethodType,
			contentType: 'application/json; charset=utf-8',
			data: DTO,
			success: function (data) {
				if (CallBack && (typeof CallBack == "function")) {
					CallBack(data);
				}
				else { DTO = data; }
			},
			error: function (err, txtStatus, errorThrown) {
				alert("Error! " + txtStatus);
			}
		});
	} catch(e) {}

    return DTO;
}

// FUNCTION TO GET VALUES FROM SESSIONSTORAGE OF THE KEYS PASSED AS PARAMS
function getSessionStorageItem(itemName) {
	var itemVal = sessionStorage.getItem(itemName);
    return itemVal;
}

// FUNCTION TO SET VALUES IN SESSIONSTORAGE AGAINST THE KEYS PASSED IN PARAMS
function setSessionStorageItem(itemName, itemVal) {
	sessionStorage.setItem(itemName, itemVal);
}

// REMOVE THE KEY VALUES PAIR FROM SESSIONSTORAGE WITH KEYNAME AS PARAM
function removeSessionStorageItem(itemName) {
	sessionStorage.removeItem(itemName);
}

// FUNCTION TO CLEAR SESSIONSTORAGE OF ITS KEY VALUE PAIRS
function clearSessionStorage() {
	sessionStorage.clear();
}

// FUNCTION TO GET VALUES FROM LOCALSTORAGE OF THE KEYS PASSED AS PARAMS
function getLocalStorageItem(itemName) {
	var itemVal = localStorage.getItem(itemName);
    return itemVal;
}

// FUNCTION TO SET VALUES IN LOCALSTORAGE AGAINST THE KEYS PASSED IN PARAMS
function setLocalStorageItem(itemName, itemVal) {
	localStorage.setItem(itemName, itemVal);
}

// REMOVE THE KEY VALUES PAIR FROM LOCALSTORAGE WITH KEYNAME AS PARAM
function removeLocalStorageItem(itemName) {
	localStorage.removeItem(itemName);
}

// FUNCTION TO CLEAR LOCALSTORAGE OF ITS KEY VALUE PAIRS
function clearLocalStorage() {
	localStorage.clear();
}
