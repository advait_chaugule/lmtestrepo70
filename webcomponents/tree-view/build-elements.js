const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const jsFiles = [
    './dist/acmt-treeview/runtime.js',
    './dist/acmt-treeview/polyfills.js',
    './dist/acmt-treeview/scripts.js',
    './dist/acmt-treeview/main.js'
  ];
  //const cssFiles = ['./dist/acmt-treeview/styles.scss'];

  await fs.ensureDir('elements');
  await concat(jsFiles, 'elements/acmt-tree.js');
 // await concat(cssFiles, 'elements/style.scss');
})();