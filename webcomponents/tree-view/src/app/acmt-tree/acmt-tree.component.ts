import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  OnChanges
} from '@angular/core';

@Component({
  selector: 'app-acmt-tree',
  templateUrl: './acmt-tree.component.html',
  styleUrls: ['./acmt-tree.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AcmtTreeComponent implements OnInit, AfterViewInit, OnChanges {
  CUSTOM_VIEW_LOCATION = 'customview';
  PUBLIC_REVIEW_LOCATION = 'public_review';
  PROJECT_AUTH_LOCATION = 'project_auth';

  /* Related to taxonomu builder */

  TAXONOMY_BUILDER = 'taxonomy_builder';
  prevTitleVal = ''; // holds document title from taxonomy builder view
  @Output() document_title = new EventEmitter(); // emit document title from taxonomy builder view
  keyPress = false;
  currentNode; // holds the current focused node
  @Input() level: 0;
  @Input() levelLimit = -1; // this is for setting the tree level ristriction
  prevHumanCodeVal = 'undefined'; // holds human code from taxonomy builder view
  prevFullStatementVal = 'undefined'; // holds full statement from taxonomy builder view
  @Output() human_coding_scheme = new EventEmitter(); // emit human code scheme from taxonomy builder view
  @Output() full_statement = new EventEmitter(); // emit full statement from taxonomy builder view
  @Output() metadataType = new EventEmitter(); // holds the metadata type from the dropdown list to emit
  @Output() nodeTypeIdEvent = new EventEmitter(); // holds the node type id from the dropdown list to emit
  @Input() nodetypeList = []; // holds the list of node types to populate in dropdown
  @Input() data: any;
  /* Related to taxonomu builder */

  constructor() { }
  @Input() treeNodes: any; // holds all the tree nodes
  @Input() selectedNodeId; // holds the current selected node id
  @Input() viewLocation; // holds the location informarion where the component is applied
  @Input() showComplaince; // hold the flag to determine whether or not to show complaince status
  charLimit = 100; // holds character limit to trim hc and full statement
  copyPasteFlag = 0; // holds value to display copy icon or paste icon
  sourceElement; // holds copied element
  targetElement; // holds paste destination element
  sourceElementSiblings; // holds siblings for copied element, used to iterate and remove copied element after paste
  @Input() reOrder = false; // holds condition if to show re order icons
  @Input() showNoComment = false; // holds boolean value to denote if close button is hit on submit review modal for public review screen
  @Input() allCommented = false; // holds boolean value to denote if first 2 levels comments are filled for public review screen
  @Input() isAllNode = false; // holds if complete tree option is selected in authoring screen
  @Input() isAddNode = false; // holds if add node button will be shown or not
  @Input() isDeleteNode = false; // holds if delete node button will be shown or not

  @Output() addNodeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteNodeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() addAssociationEvent: EventEmitter<any> = new EventEmitter<any>(); // holds current node data emiited as event
  firstNode; // holds the first single node
  @Output() treeNodeSelectedEvent: EventEmitter<any> = new EventEmitter<any>(); // holds current node of tree emiited as event
  ngOnChanges() {
    console.log('ngOnchange', this.data);
    this.treeNodes = this.data.children;
  }
  ngOnInit() {
    console.log('ngOnIntit');
    // var data = `{"children":[
    //   {"id":"0d08d9bc-4f00-97c3-0233-bf383c2a6b3a","title":"1","title_html":"","human_coding_scheme":"","list_enumeration":"","sequence_number":"","full_statement":"1","status":1,"document_type":1,"adoption_status":1,"node_type":"Document","metadataType":"Document","node_type_id":"ed9807d1-63ed-4703-ba67-e4103addc29e","project_enabled":0,"project_name":"","is_document":1,"custom_view_visibility":0,"is_orphan":0,"full_statement_html":"1","expand":true,"cut":0,"paste":0,"reorder":0,"level":1,"isFirst":true,
    //   "children":[
    //     {"id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","title":"","human_coding_scheme":"1","human_coding_scheme_html":"1","list_enumeration":"1","sequence_number":"","full_statement":"1","full_statement_html":"1","node_type":"Standard","metadataType":"Standard","node_type_id":"fa65eb18-d6d4-4480-a127-e4583814bc2c","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"0d08d9bc-4f00-97c3-0233-bf383c2a6b3a","document_id":"","document_title":"","is_orphan":0,"children":[{"id":"99283265-190c-4b41-9d28-82635b3719a0","title":"","human_coding_scheme":"11","human_coding_scheme_html":"11","list_enumeration":"1","sequence_number":"","full_statement":"11","full_statement_html":"11","node_type":"Indicator","metadataType":"Indicator","node_type_id":"fb16ebce-80b5-4e36-9353-ac1f9f340b1c","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"1ac9ce76-0268-4a53-a41f-a20568405798","title":"","human_coding_scheme":"","human_coding_scheme_html":"","list_enumeration":"2","sequence_number":"","full_statement":"","full_statement_html":"","node_type":"Content Standard","metadataType":"Content Standard","node_type_id":"00b04261-bed7-4f4b-95a1-44407a72a1ef","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"86afb854-8a24-408b-bbcc-1c4627408f4f","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"3","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Element","metadataType":"Element","node_type_id":"3de7b7bf-87e7-4f1b-bd41-ef3323c3cb12","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"18d7a893-f040-4abf-884f-8b6894b0db4c","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"4","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Strand","metadataType":"Strand","node_type_id":"4ce62a1d-dd37-4fbf-b61e-95cce7493452","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"c3dc60c4-7ca3-4291-9568-db203624a0a1","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"5","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Child 23 Node Type","metadataType":"Child 23 Node Type","node_type_id":"2289d417-3011-4767-b06d-1b13b35f1878","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"ecba5a8a-f03c-4bfd-ab9d-f4e86ce38c58","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"6","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Unit","metadataType":"Unit","node_type_id":"72a2c650-3122-42cb-99e8-7182f5292c49","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"8a270180-d44b-4b33-ac49-f58d3254ffce","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"7","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Benchmark","metadataType":"Benchmark","node_type_id":"78dc7b24-e1d0-4970-ae0e-ad058ec8a44e","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"d4a5c9d0-cb6d-48b4-b441-b99dd798436b","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"8","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Grade Level","metadataType":"Grade Level","node_type_id":"a76e00f1-1e5d-4b75-9a77-ddd942a1d97f","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3},{"id":"8fb197ad-aea5-46e0-8f03-fc33ef31f315","title":"","human_coding_scheme":"","human_coding_scheme_html":null,"list_enumeration":"9","sequence_number":"","full_statement":"","full_statement_html":null,"node_type":"Indicator","metadataType":"Indicator","node_type_id":"fb16ebce-80b5-4e36-9353-ac1f9f340b1c","item_type":"","project_enabled":1,"project_name":"","is_document":0,"parent_id":"fd0dfccf-2f2f-42a0-ae3b-3174e594a304","document_id":"","document_title":"","is_orphan":0,"children":[],"expand":false,"cut":0,"paste":0,"reorder":0,"level":3}],"expand":false,"cut":0,"paste":0,"reorder":0,"level":2}],"location":"taxonomydetail","compStatus":0}]}`;
    // this.treeNodes = JSON.parse(data).children;
  }
  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    setTimeout(() => {
      if (this.viewLocation === this.TAXONOMY_BUILDER && document.getElementById('' + this.treeNodes[0]['id'] + '-node')) {
        document.getElementById('' + this.treeNodes[0]['id'] + '-node').click();
      }
    }, 1000);
    if (this.treeNodes && this.treeNodes.length === 1) {
      this.firstNode = this.treeNodes[0];
    } else {
      this.firstNode = {
        id: ''
      };
    }
  }




  /* --------- Functionality to emit the current selected node object start --------- */

  onNodeSelected(item) {
    if (!item.isOrphanLabel) {
      if (item.id) {
        this.selectedNodeId = item.id;
      }
      // if (item.item_id) {
      //   this.selectedNodeId = item.item_id;
      // }
      item.location = this.viewLocation;
      // this.sharedService.treeNodeSelectedEvent.next(item);
      this.treeNodeSelectedEvent.emit(item);
    }
  }

  /* --------- Functionality to emit the current selected node object end --------- */


  /* --------- Functionality to add padding on tree nodes start --------- */


  calculatePadding(level) {
    // if (this.viewLocation === this.CUSTOM_VIEW_LOCATION) {
    //   return 0;
    // } else {
    // return Utils.calculatePadding(level, 'treeview');
    // }


    if (this.viewLocation === 'taxonomy_builder') {
      return this.calculateTaxonomyBuilderPadding(level, '');
    } else {
      return this.calculatePadding_(level, 'treeview');
    }
  }
  calculateNodeTemplatePadding(level, treeType) {

    if (treeType === 'treeview') {
      if (level === 1) {
        return '1rem';
      }
    }
    return (1 + (level - 1) * 1.2) + 'rem';
  }

  calculateTaxonomyBuilderPadding(level, treeType) {

    if (level === 1) {
      return '1.25rem';
    }
    let padding = 1.78 + (level - 1);
    if (level > 2) {
      padding = padding + (level - 2) * .50;
    }
    return padding + 'rem';
  }

  calculatePadding_(level, treeType) {

    if (treeType === 'treeview') {
      if (level === 1) {
        return '.5rem';
      }
    }
    return (.5 + (level - 1) * 1.5) + 'rem';
  }

  /* --------- Functionality to add padding on tree nodes end --------- */


  /* --------- Functionality to re order sibliings under same parent start --------- */

  reOrderSiblings(siblings, index, itemId) {
    // console.log('items siblings', JSON.stringify(siblings));
    let sourceElement;
    let targetElement;
    for (const i in siblings) {
      if (siblings[i]['id'] === itemId) {
        sourceElement = siblings[Number(i)];
        sourceElement['reorder'] = 1;
        // this.updateCutReorderValue(sourceElement['children'], 'reorder', 1);
        targetElement = siblings[Number(i) + index];
        siblings[Number(i)] = targetElement;
        siblings[Number(i) + index] = sourceElement;
        break;
      }
    }
    setTimeout(() => {
      sourceElement['reorder'] = 0;
      // this.updateCutReorderValue(sourceElement['children'], 'reorder', 0);
    }, 1300);
    // console.log('target element', targetElement);
    // console.log('source element', sourceElement);
  }

  /* --------- Functionality to re order sibliings under same parent end --------- */


  /* --------- Functionality to cut and paste any node start --------- */

  copySourceElement(item, siblings) {
    this.sourceElement = item;
    this.sourceElementSiblings = siblings;
    item.cut = 1;
    this.updateCutReorderValue(item['children'], 'cut', 1);
  }

  pasteTargetElement(item) {
    this.targetElement = item;
    this.sourceElement.parent_id = this.targetElement.id;
    this.sourceElement.level = this.targetElement.level + 1;
    if (this.sourceElement['children'] && this.sourceElement['children'].length > 0) {
      this.updatePadding(this.sourceElement['children'], this.targetElement.level + 2);
    }
    this.removeFromList(this.sourceElementSiblings, this.sourceElement.id);
    item.children.push(this.sourceElement);
    this.targetElement['paste'] = 1;
    this.sourceElement['cut'] = 0;
    this.updateCutReorderValue(this.sourceElement['children'], 'cut', 0);
    setTimeout(() => {
      this.targetElement['paste'] = 0;
      // this.updateCutReorderValue(sourceElement['children'], 'reorder', 0);
    }, 1300);
  }

  /* --------- Functionality to cut and paste any node end --------- */


  /* --------- Functionality to remove the pasted element from previous node start --------- */

  removeFromList(arr, id) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id === id) {
        arr.splice(i, 1);
      }
    }
  }

  /* --------- Functionality to remove the pasted element from previous node end --------- */


  /* --------- Functionality to update the padding after pasting new node start --------- */

  updatePadding(sourceElement, level) {
    sourceElement.forEach(element => {
      element.level = level;
      if (element.children && element.children.length > 0) {
        this.updatePadding(element.children, level + 1);
      }
    });
  }


  /* --------- Functionality to update the padding after pasting new node end --------- */


  /* --------- Functionality to update the key for cut or paste used for updating css for cut and paste start --------- */

  updateCutReorderValue(items, key, val) {
    items.forEach(element => {
      element[key] = val;
      if (element.children && element.children.length > 0) {
        this.updateCutReorderValue(element.children, key, val);
      }
    });
  }

  /* --------- Functionality to update the key for cut or paste used for updating css for cut and paste end --------- */


  /* --------- Functionality to cancle re order functionality start --------- */

  cancleReorder() {
    this.copyPasteFlag = 0;
    this.sourceElement = null;
    this.targetElement = null;
  }

  /* --------- Functionality to cancle re order functionality end --------- */

  /* --------- Event emitter to add node -------- */
  addNode(item) {
    // if (item.id) {
    //   this.selectedNodeId = item.id;
    // }
    // item.location = this.viewLocation;
    // console.log('addNode tree-accordian ------------- ', item);
    // this.addNodeEvent.emit(item);

    item.location = this.viewLocation;
    item.expand = true;
    // item.level = this.level; // kunal level
    console.log('addNode tree-accordian ------------- ', item);
    // this.selectedEvent.emit(item);
    // this.sharedService.treeAddNodeEvent.next(item);
    if (item.location === 'taxonomy_builder' && item.parent !== 'root') {
      if (item.full_statement.length > 0) {
        this.addNodeEvent.emit(item);
      }
    } else {
      this.addNodeEvent.emit(item);
    }
    this.keyPress = false;
  }

  /* --------- Event emitter to delete node -------- */
  deleteNode(item) {
    console.log('deleteNode tree-accordian ', item);
    const delMsg = 'Deleting this node will remove it from all the nodes it is referred to. Are you sure you want to continue?';
    if (this.viewLocation !== 'taxonomy_builder') {
      this.deleteNodeEvent.emit({ node: item, msg: delMsg, dialog: true });
    } else {
      this.deleteNodeEvent.emit({ node: item, msg: '', dialog: false });
    }
    // if (this.viewLocation !== 'taxonomy_builder') {
    //   this.dialogService.confirm('Confirm', delMsg)
    //     .then((confirmed) => {
    //       if (confirmed) {
    //         // console.log('deleteNode tree-accordian ', item);
    //         item.location = this.viewLocation;
    //         // this.sharedService.treeDeleteNodeEvent.next(item);
    //         this.deleteNodeEvent.emit(item);
    //       } else {
    //         console.log('User cancel the dialog');
    //       }
    //     })
    //     .catch(() => {
    //       console.log('User dismissed the dialog');
    //     });
    // } else {
    //   this.deleteNodeEvent.emit(item);
    // }
  }

  // parsing style data as Object for binding into ngStyle
  calculateStyle(viewData) {
    if (viewData && this.viewLocation === this.CUSTOM_VIEW_LOCATION) {
      return JSON.parse(viewData).style;
    } else {
      return '';
    }
  }

  /* Show/hide expand button */
  showExpandBtn(item) {
    let show = false;
    show = item.children.length > 0; // default condition for all locations tree except customview taxonomy

    if (this.viewLocation === this.CUSTOM_VIEW_LOCATION) {
      if (item.custom_view_data) { // condition for customview taxonomy
        show = show && JSON.parse(item.custom_view_data).allow_expand.toLowerCase() === 'yes';
      }
    }

    return show;
  }

  /* Show/hide children container */
  showChildrenContainer(item) {
    let show = false;
    show = item.children && item.expand; // default condition for all locations tree except customview taxonomy

    if (this.viewLocation === this.CUSTOM_VIEW_LOCATION) {
      if (item.custom_view_data) { // condition for customview taxonomy
        show = show && JSON.parse(item.custom_view_data).allow_expand.toLowerCase() === 'yes';
      }
    }

    return show;
  }

  // To show title from custom view taxonomy node
  getCustomViewTitle(viewData) {
    if (viewData && this.viewLocation === this.CUSTOM_VIEW_LOCATION) {
      return JSON.parse(viewData).title;
    } else {
      return '';
    }
  }

  /* --------- Not used -------- */

  checkHeight() {
    let elemHeight = 0;
    const maxHeight = 80;
    if (document.getElementById('html-content')) {
      const elem = document.getElementById('html-content');
      elemHeight = maxHeight - elem.clientHeight + elem.offsetHeight;
    }
    if (elemHeight > 32) {
      return true;
    } else {
      return false;
    }
  }

  /* --------- Not used -------- */


  /* --------- Functionality to update the document node on focus shift start -------- */

  blurDocumentTitle(item, evt) {
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    const document_title = elt.innerText;
    if (this.prevTitleVal !== document_title.trim()) {
      this.prevTitleVal = document_title;
      const tempObj = [item, document_title];
      this.document_title.emit(tempObj);
    }
  }

  /* --------- Functionality to update the document node on focus shift end -------- */

  onKeyUp(event) {
    this.keyPress = false;
  }


  /* --------- Functionality to emit HC,FS,Doc node for keyboard functionality start -------- */

  onKeyPress(item, event, control) {
    const key = event.which || event.keyCode || event.charCode;
    const parentId = this.currentNode.parent_id;
    const itemId = this.currentNode.id;
    if (key === 13 && event.shiftKey) {
      event.preventDefault();
      if (item.level !== this.levelLimit) {
        if (this.keyPress === false) {
          this.keyPress = true;
          if (control === 'humanCodeScheme') {
            this.blurHumanCodingScheme(item, event);
          }
          if (control === 'fullStatement') {
            this.blurFullStatement(item, event);
          }
          if (control === 'document') {
            this.blurDocumentTitle(item, event);
          }
          this.addButtonEvent(itemId);
        }
      }
    } else if (key === 13) {
      event.preventDefault();
      if (this.keyPress === false) {
        this.keyPress = true;
        if (control === 'humanCodeScheme') {
          this.blurHumanCodingScheme(item, event);
        }
        if (control === 'fullStatement') {
          this.blurFullStatement(item, event);
        }
        if (control === 'document') {
          this.blurDocumentTitle(item, event);
        }
        this.addButtonEvent(parentId);
      }
    }
  }

  /* --------- Functionality to emit HC,FS,Doc node for keyboard functionality end -------- */


  /* --------- Functionality to update the HC on focus shift start -------- */

  blurHumanCodingScheme(item, evt) {
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    const human_coding_scheme = elt.innerText;
    // if (this.addNodeFired === false && this.prevHumanCodeVal !== human_coding_scheme.trim())
    if (this.prevHumanCodeVal !== human_coding_scheme.trim()) {
      this.prevHumanCodeVal = human_coding_scheme;
      const tempObj = [item, human_coding_scheme];
      this.human_coding_scheme.emit(tempObj);
    }
    // this.addNodeFired = false;
  }

  /* --------- Functionality to update the HC on focus shift end -------- */


  /* --------- Functionality to update the FS on focus shift start -------- */

  blurFullStatement(item, evt) {
    const elt = evt.target;
    elt.innerText = elt.innerText.replace(/\n/g, ' ');
    const full_statement = elt.innerText;
    if (this.prevFullStatementVal !== full_statement.trim()) {
      this.prevFullStatementVal = full_statement;
      const tempObj = [item, full_statement];
      this.full_statement.emit(tempObj);
    }
  }

  /* --------- Functionality to update the FS on focus shift end -------- */


  /* --------- Functionality to emit current node on click of add start -------- */

  addButtonEvent(buttonId) {
    if (document.getElementById(buttonId + '-add')) {
      document.getElementById(buttonId + '-add').click();
    }
  }

  /* --------- Functionality to emit current node on click of add end -------- */


  /* --------- Functionality to check previous value on current node to decide event emission on focus start -------- */

  onFocus(item) {
    this.currentNode = item;
    this.prevHumanCodeVal = item['human_coding_scheme'];
    this.prevFullStatementVal = item['full_statement'];
  }

  /* --------- Functionality to check previous value on current node to decide event emission on focus end -------- */


  /* --------- Functionality to focus on current node start -------- */

  focusNode(id) {
    if (document.getElementById(id + '-span')) {
      document.getElementById(id + '-span').focus();
    }
  }

  /* --------- Functionality to focus on current node end -------- */


  /* --------- Functionality to update the emit the metadata set and item for current node start -------- */

  selectMetaData(item, evt, nodetypeid) {
    const metadataType = evt;
    const tempObj = [item, metadataType];

    this.metadataType.emit(tempObj);
    this.getNodeTypeId(item, nodetypeid);
  }

  getNodeTypeId(item, nodetypeid) {
    const node_type_id = nodetypeid;
    const tempObj = [item, node_type_id];
    this.nodeTypeIdEvent.emit(tempObj);
    // console.log('111', tempObj);
  }

  /* --------- Functionality to update the emit the metadata set and item for current node end -------- */


  /* --------- Functionality to emit current node while adding associations start -------- */

  addAssociation(item) {
    if (item.id) {
      this.onNodeSelected(item);
    }
    this.addAssociationEvent.emit(item);
  }

  /* --------- Functionality to emit current node while adding associations end -------- */
}
