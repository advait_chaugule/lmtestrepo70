import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcmtTreeComponent } from './acmt-tree.component';

describe('AcmtTreeComponent', () => {
  let component: AcmtTreeComponent;
  let fixture: ComponentFixture<AcmtTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcmtTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcmtTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
