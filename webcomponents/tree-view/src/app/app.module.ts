import {
  BrowserModule
} from '@angular/platform-browser';
import {
  NgModule,
  Injector,
  NO_ERRORS_SCHEMA
} from '@angular/core';

import {
  AcmtTreeComponent
} from './acmt-tree/acmt-tree.component';
import {
  createCustomElement
} from '@angular/elements';

@NgModule({
  declarations: [
    AcmtTreeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  schemas:[NO_ERRORS_SCHEMA],
  entryComponents: [AcmtTreeComponent] // use entryComponents array instaed of bootstrap because it will be added dynamically.

})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    // using createCustomElement from angular package it will convert angular component to stander web component
    const el = createCustomElement(AcmtTreeComponent, {
      injector: this.injector
    });
    // using built in the browser to create your own custome element name
    customElements.define('acmt-tree', el);

    // const el = createCustomElement( MultiSelectDropdownComponent, { injector: this.injector });
    // customElements.define('acmt-multi-select-dropdown', el);
  }

}
