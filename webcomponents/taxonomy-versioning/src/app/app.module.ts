import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { TaxonomyVersioningComponent } from './Versioning/taxonomy-versioning/taxonomy-versioning.component';
import { AutoCompleteComponent } from './common/auto-complete/auto-complete.component';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'angular2-moment';
import { VersionSummaryComponent } from './Versioning/version-summary/version-summary.component';
import { DataTableComponent } from './common/data-table/data-table.component';
import { PaginationComponent } from './common/pagination/pagination.component';
import { PreloaderComponent } from './common/preloader/preloader.component';
import { MultiSelectDropdownComponent } from './common/multi-select-dropdown/multi-select-dropdown.component';
import {
  HttpClientModule
} from '@angular/common/http';
import {
  SharedService
} from './services/shared.service';
import {
  CommonService
} from './services/common.service';
import {
  ClickOutsideModule
} from 'ng-click-outside';
import { MetadataVersionComponent } from './Versioning/metadata-version/metadata-version.component';
import { TabComponent } from './common/tab/tab.component';
import { AssociationVersionReportComponent } from './Versioning/association-version-report/association-version-report.component';

@NgModule({
  declarations: [
    TaxonomyVersioningComponent,
    AutoCompleteComponent,
    VersionSummaryComponent,
    DataTableComponent,
    PaginationComponent,
    PreloaderComponent,
    MultiSelectDropdownComponent,
    MetadataVersionComponent,
    TabComponent,
    AssociationVersionReportComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MomentModule,
    HttpClientModule,
    ClickOutsideModule
  ],
  providers: [SharedService, CommonService],
  entryComponents: [TaxonomyVersioningComponent]
})
export class AppModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap() {
    const element = createCustomElement(TaxonomyVersioningComponent, {
      injector: this.injector
    });
    customElements.define('taxonomy-version', element);
  }
}
