import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadataVersionComponent } from './metadata-version.component';

describe('MetadataVersionComponent', () => {
  let component: MetadataVersionComponent;
  let fixture: ComponentFixture<MetadataVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetadataVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetadataVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
