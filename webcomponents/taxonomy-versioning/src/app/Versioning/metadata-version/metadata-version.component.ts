import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DataTableComponent } from 'src/app/common/data-table/data-table.component';
import { TabComponent } from 'src/app/common/tab/tab.component';

@Component({
  selector: 'app-metadata-version',
  templateUrl: './metadata-version.component.html',
  styleUrls: ['./metadata-version.component.scss']
})
export class MetadataVersionComponent implements OnInit {

  listOfColumn = []; // holds list of column headers in version-summary table
  dataArray = []; // holds the data array to be shown in version-summary table
  defaultSortByColumn: any; // holds the column object by which the table data will be sorted by default
  pageDetail: any; // holds object for pagination details
  tabItems = ['new', 'updated', 'deleted']; // holds list of tab items to display versioning
  newData = []; // holds  data for new tab
  updatedData = []; // holds data for updated tab
  deletedData = []; // holds data for deleted tab
  counts = [0, 0, 0]; // holds array of data counts for each tab
  currentTable = ''; // holds type of current table
  metadata = ''; // holds current metadata (for metadata table only)

  @Input() currentTab = 'new'; // holds currently selected tab

  @ViewChild('dataTable', { static: false }) dataTable: DataTableComponent; // holds reference of data-table component
  @ViewChild('tab', { static: false }) tab: TabComponent; // holds reference of Tab component

  constructor() { }

  ngOnInit() {
  }


  // FUNCTION to select tab item
  onTabSelected(tab) {
    this.currentTab = tab;
    this.setTable();
  }

  // FUNCTION to set columns according to current tab
  setTable() {
    switch (this.currentTable) {
      case 'metadata':
        this.setMetadataTable();
        this.defaultSortByColumn = this.listOfColumn[0];
        break;
      case 'nodetype':
        this.setNodetypeTable();
        this.defaultSortByColumn = this.listOfColumn[1];
        break;
      case 'document':
        this.setDocTable();
        this.defaultSortByColumn = this.listOfColumn[0];
        break;
    }
    this.dataTable.setData(this.dataArray, this.listOfColumn);
  }

  // FUNCTION to set columns according to current tab
  setMetadataTable() {
    switch (this.currentTab) {
      case 'new':
        this.listOfColumn = [
          {
            name: 'Node',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: this.metadata,
            propName: 'new',
            class: '',
            type: 'text'
          },
          {
            name: 'Path',
            propName: 'title',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.newData;
        break;
      case 'updated':
        this.listOfColumn = [
          {
            name: 'Node',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: 'Updates to ' + this.metadata + ' in New Version',
            propName: 'new',
            class: '',
            type: 'text'
          },
          {
            name: this.metadata + ' in Current Version',
            propName: 'old',
            class: '',
            type: 'text'
          },
          {
            name: 'Path',
            propName: 'title',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.updatedData;
        break;
      case 'deleted':
        this.listOfColumn = [
          {
            name: 'Node',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: 'Deleted',
            propName: 'delete',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.deletedData;
        break;
      default:
    }
  }

  setNodetypeTable() {
    switch (this.currentTab) {
      case 'new':
        this.listOfColumn = [
          {
            name: 'Node',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: 'Metadata',
            propName: 'metadata_name',
            class: '',
            type: 'text'
          },
          {
            name: 'New Metadata',
            propName: 'new',
            class: '',
            type: 'text'
          },
          {
            name: 'Path',
            propName: 'title',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.newData;
        break;
      case 'updated':
        this.listOfColumn = [
          {
            name: 'Node',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: 'Metadata',
            propName: 'metadata_name',
            class: '',
            type: 'text'
          },
          {
            name: 'Updates to Metadata in New Version',
            propName: 'new',
            class: '',
            type: 'text'
          },
          {
            name: 'Metadata in Current Version',
            propName: 'old',
            class: '',
            type: 'text'
          },
          {
            name: 'Path',
            propName: 'title',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.updatedData;
        break;
      case 'deleted':
        this.listOfColumn = [
          {
            name: 'Node',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: 'Metadata',
            propName: 'metadata_name',
            class: '',
            type: 'text'
          },
          {
            name: 'Value',
            propName: 'delete',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.deletedData;
        break;
      default:
    }
  }

  setDocTable() {
    switch (this.currentTab) {
      case 'new':
        this.listOfColumn = [
          {
            name: 'Metadata',
            propName: 'metadata_name',
            class: '',
            type: 'text'
          },
          {
            name: 'Value',
            propName: 'new',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.newData;
        break;
      case 'updated':
        this.listOfColumn = [
          {
            name: 'Metadata',
            propName: 'metadata_name',
            class: '',
            type: 'text'
          },
          {
            name: 'Updates in New Version',
            propName: 'new',
            class: '',
            type: 'text'
          },
          {
            name: 'Value in Current Version',
            propName: 'old',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.updatedData;
        break;
      case 'deleted':
        this.listOfColumn = [
          {
            name: 'Metadata',
            propName: 'metadata_name',
            class: '',
            type: 'text'
          },
          {
            name: 'Deleted Value',
            propName: 'delete',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.deletedData;
        break;
      default:
    }
  }

  // FUNCTION to construct data for data-table
  extractTableData(rawData) {
    this.counts = [];
    this.newData = [];
    this.updatedData = [];
    this.deletedData = [];
    this.currentTable = rawData.type;
    if (rawData.type === 'metadata') {
      this.metadata = rawData.metadata_name;
      this.extractMetadataSummary(rawData);
    }
    if (rawData.type === 'nodetype') {
      this.extractNodetypeSummary(rawData);
    }
    if (rawData.type === 'document') {
      this.extractDocVersionSummary(rawData);
    }
    this.counts.push(this.newData.length ? this.newData.length : 0);
    this.counts.push(this.updatedData.length ? this.updatedData.length : 0);
    this.counts.push(this.deletedData.length ? this.deletedData.length : 0);
    this.onTabSelected(this.currentTab);
    this.tab.setData(this.counts, this.currentTab);
  }


  // FUNCTION to construct data for METADATA
  extractMetadataSummary(rawData) {
    if (rawData.new) {
      for (const key in rawData.new) {
        if (key) {
          this.newData.push(rawData.new[key]);
        }
      }
    }

    if (rawData.edit) {
      for (const key in rawData.edit) {
        if (key) {
          this.updatedData.push(rawData.new[key]);
        }
      }
    }

    if (rawData.delete) {
      for (const key in rawData.delete) {
        if (key) {
          this.deletedData.push(rawData.new[key]);
        }
      }
    }

  }


  // FUNCTION to construct data for NODETYPE
  extractNodetypeSummary(rawData) {
    if (rawData.new) {
      for (const key in rawData.new) {
        if (rawData.new[key].length) {
          rawData.new[key].forEach(element => {
            this.newData.push(element);
          });
        }
      }
    }

    if (rawData.edit) {
      for (const key in rawData.edit) {
        if (rawData.edit[key].length) {
          rawData.edit[key].forEach(element => {
            this.updatedData.push(element);
          });
        }
      }
    }

    if (rawData.delete) {
      for (const key in rawData.delete) {
        if (rawData.delete[key].length) {
          rawData.delete[key].forEach(element => {
            this.deletedData.push(element);
          });
        }
      }
    }

  }

  // FUNCTION to extract data for DOCUMENT table
  extractDocVersionSummary(rawData) {
    if (rawData.new) {
      for (const key in rawData.new) {
        if (key) {
          this.newData.push({
            metadata_name: key,
            new: rawData.new[key].new
          });
        }
      }
    }
    if (rawData.edit) {
      for (const key in rawData.edit) {
        if (key) {
          this.updatedData.push({
            metadata_name: key,
            old: rawData.edit[key].old,
            new: rawData.edit[key].new
          });
        }
      }
    }
    if (rawData.delete) {
      for (const key in rawData.delete) {
        if (key) {
          this.deletedData.push({
            metadata_name: key,
            delete: rawData.delete[key].delete
          });
        }
      }
    }
  }
}
