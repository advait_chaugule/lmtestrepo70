import { Component, OnInit, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { AutoCompleteComponent } from 'src/app/common/auto-complete/auto-complete.component';
import { VersionSummaryComponent } from '../version-summary/version-summary.component';
import { GlobalSettings } from 'src/app/global-files/global-settings';
import { CommonService } from 'src/app/services/common.service';
import { SharedService } from 'src/app/services/shared.service';
import { MetadataVersionComponent } from '../metadata-version/metadata-version.component';
import { AssociationVersionReportComponent } from '../association-version-report/association-version-report.component';

@Component({
  selector: 'app-taxonomy-versioning',
  templateUrl: './taxonomy-versioning.component.html',
  styleUrls: ['./taxonomy-versioning.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class TaxonomyVersioningComponent implements OnInit, OnChanges {

  versionCriteriaList = []; // holds list of criteria whose versions are displayed for the taxonomy
  autoCompleteTitle = 'version comparison criteria'; // holds title to show for auto-complete dropdown
  selectType = 'single'; // holds type of selection to be made in auto-complete dropdown (single-select / multi-select)
  uniqueId = 'id'; // holds unique id to identify list-items in auto-complete
  propName = 'title'; // holds property name (in data object) of which the value will be displayed in auto-complete dropdown list
  defaultCriteria: any; // holds version criteria to be selected by default
  selectedCriteria: any; // holds currently selected criteria
  rawVersionData: any; // holds raw data array of version report

  metadataArray: any;
  nodetypeArray: any;
  docVersionSummary: any;
  associationArray: any;
  currentTab = 'new'; // holds currently selected tab to display (sending via input)

  @Input() comparisonId = 'bf41abed-3013-4fc8-b2a3-c6dcb3491dcd'; // holds comparison ID of published taxonomy
  @Input() domain = ''; // holds environment domain url

  @Output() changeCriteriaEvent: EventEmitter<any>;

  @ViewChild('autoComplete', { static: false }) autoComplete: AutoCompleteComponent; // holds reference of auto-complete component
  @ViewChild('summaryReport', { static: false }) summaryReport: VersionSummaryComponent; // holds reference of version-summary component
  // tslint:disable-next-line: max-line-length
  @ViewChild('metadataVersion', { static: false }) metadataVersion: MetadataVersionComponent; // holds reference of metadata version component
  // tslint:disable-next-line: max-line-length
  @ViewChild('associationVersion', { static: false }) associationVersion: AssociationVersionReportComponent; // holds reference of metadata version component


  constructor(
    private service: CommonService,
    private sharedService: SharedService
  ) {
    this.changeCriteriaEvent = new EventEmitter<any>();
  }

  ngOnInit() {
    this.versionCriteriaList = [
      {
        id: 1,
        title: 'Summary Report',
        type: 'summary'
      }
    ];
    this.defaultCriteria = this.versionCriteriaList[0];
    this.selectedCriteria = this.defaultCriteria;
    this.onCriteriaSelected(this.defaultCriteria);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.comparisonId) {
      this.onCriteriaSelected(this.defaultCriteria);
    }
  }

  // FUNCTION to select new criteria on which taxonomy versions will be shown
  onCriteriaSelected(val) {
    this.selectedCriteria = val;
    this.changeVersionReport(val);
  }

  // FUNCTION to load version report as per selected criteria
  changeVersionReport(criteria) {
    switch (criteria.type) {
      case 'summary':
        this.getSummaryReport();
        break;
      case 'metadata':
        this.getmetadataVersionSummary();
        break;
      case 'nodetype':
        this.getNodetypeVersionSummary();
        break;
      case 'document':
        this.getDocumentVersionSummary();
        break;
      case 'associations':
        this.getAssociationVersionSummary();
        break;
      default:
    }
  }

  // FUNCTION to get version summary data
  getSummaryReport() {
    this.sharedService.showLoader.next(true);
    if (this.domain) {
      GlobalSettings.env = this.domain;
    }
    const url = GlobalSettings.VERSION_SUMMARY_REPORT + '?comparison_id=' + this.comparisonId + '&is_internal=false';

    this.service.getServiceData(url, true, true).then((res: any) => {
      if (res.success) {
        this.rawVersionData = res.data;
        this.getdropdownData(res.data);
        this.loadReport(this.selectedCriteria.type);
      } else {
        this.rawVersionData = [];
        this.sharedService.showLoader.next(false);
      }
    }).catch(ex => {
      this.sharedService.showLoader.next(false);
    });
  }

  // FUNCTION to add options to drop-down list
  getdropdownData(data) {
    if (this.versionCriteriaList.length > 1) {
      this.versionCriteriaList.splice(1, this.versionCriteriaList.length);
    }
    if (data.document) {
      this.versionCriteriaList.push(
        {
          id: 2,
          title: 'Document',
          type: 'document'
        }
      );
    }

    if (data.associations) {
      this.versionCriteriaList.push(
        {
          id: 3,
          title: 'Associations',
          type: 'associations'
        }
      );
    }

    if (data.metadata) {
      data.metadata.items.forEach(item => {
        this.versionCriteriaList.push({
          id: item.internal_name,
          title: item.type,
          type: data.metadata.displayName.toLowerCase(),
        });
      });
    }
    if (data.nodetype) {
      data.nodetype.items.forEach(item => {
        this.versionCriteriaList.push({
          id: item.node_type_id,
          title: item.type,
          type: data.nodetype.displayName.toLowerCase(),
        });
      });
    }
  }

  // FUNCTION to get metadata version report
  getmetadataVersionSummary() {
    this.sharedService.showLoader.next(true);
    this.metadataArray = {
      'type': 'metadata',
      'metadata_name': 'Status Start Date',
      'internal_name': 'status_start_date',
      'new': {
        'b09659aa-4cb3-4db7-bcb1-814b41ede6dc': {
          'old': '',
          'new': '2020-06-01',
          'full_statement': 'Node A'
        },
        '668b24ff-e0ef-4b4e-b2ce-3d99d2a8f515': {
          'old': '',
          'new': '2020-06-02',
          'full_statement': 'Node B'
        }
      },
      'edit': {
        '668b24ff-e0ef-4b4e-b2ce-3d99d2a8f515': {
          'old': '2020-06-02',
          'new': '2020-06-03',
          'full_statement': 'Node B'
        }
      }
    };
    setTimeout(() => {
      this.loadReport(this.selectedCriteria.type);
    }, 1000);
  }

  // FUNCTION to get nodetype version report
  getNodetypeVersionSummary() {
    this.sharedService.showLoader.next(true);
    this.nodetypeArray = {
      'type': 'nodetype',
      'node_type_id': 'f5c54498-6760-4439-9c5e-038ca54cd500',
      'new': {
        'b09659aa-4cb3-4db7-bcb1-814b41ede6dc': [
          {
            'old': '',
            'new': '2020-06-01',
            'full_statement': 'Node 1',
            'metadata_name': 'Status Start Date',
            'internal_name': 'status_start_date',

          },
          {
            'old': 'X',
            'new': 'X1',
            'full_statement': 'Node 1',
            'metadata_name': 'Full Statement',
            'internal_name': 'full_statement',

          }
        ],
        '668b24ff-e0ef-4b4e-b2ce-3d99d2a8f515': [
          {
            'old': '',
            'new': '2020-06-02',
            'full_statement': 'Node 2',
            'metadata_name': 'Status Start Date',
            'internal_name': 'status_start_date',

          }
        ]
      },
      'edit': {
        '668b24ff-e0ef-4b4e-b2ce-3d99d2a8f515': [
          {
            'old': '2020-06-02',
            'new': '2020-06-03',
            'full_statement': 'Node 2',
            'metadata_name': 'Status Start Date',
            'internal_name': 'status_start_date',

          }
        ],
        'ceb79049-9d12-4bed-b5a2-418a1ace6c6b': [
          {
            'old': 'C',
            'new': 'C1',
            'full_statement': 'Node 3',
            'metadata_name': 'Full Statement',
            'internal_name': 'full_statement',

          }
        ]
      }
    };
    setTimeout(() => {
      this.loadReport(this.selectedCriteria.type);
    }, 1000);
  }


  // FUNCTION to get Document version summary data
  getDocumentVersionSummary() {
    this.sharedService.showLoader.next(true);
    this.docVersionSummary = {
      'type': 'document',
      'new': {
        'CustomText': {
          'old': '',
          'new': 'Test'
        }
      },
      'edit': {
        'Creator': {
          'old': 'Avalon',
          'new': 'Avalon_'
        },
        'Subject Title': {
          'old': 'Ti',
          'new': 'Title'
        }
      }
    };
    setTimeout(() => {
      this.loadReport(this.selectedCriteria.type);
    }, 1000);
  }
  // FUNCTION to get Document version summary data
  getAssociationVersionSummary() {
    this.sharedService.showLoader.next(true);
    this.associationArray = {
      'type': 'associations',
      'new': {
        'f6e132ee-e4d1-4f49-8771-b96a84ab40f5': [{
          'identifier': 'f6e132ee-e4d1-4f49-8771-b96a84ab40f5',
          'associationType': 'isPeerOf',
          'CFDocumentURI': {
            'identifier': 'b1731727-7800-4374-be53-d2e3f7dad3d4',
            // tslint:disable-next-line: max-line-length
            'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFDocuments/b1731727-7800-4374-be53-d2e3f7dad3d4',
            'title': 'TestVersioning'
          },
          'sequenceNumber': 2,
          // tslint:disable-next-line: max-line-length
          'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFAssociations/f6e132ee-e4d1-4f49-8771-b96a84ab40f5',
          'originNodeURI': {
            'identifier': '806b48d9-770c-45af-9887-4b7242764d79',
            'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFItems/806b48d9-770c-45af-9887-4b7242764d79',
            'title': 'TestVersioning:Chapter1:B1:B1'
          },
          'destinationNodeURI': {
            'identifier': 'af67de05-0a4a-4846-b2dc-4ac2dfbd24a4',
            'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFItems/af67de05-0a4a-4846-b2dc-4ac2dfbd24a4',
            'title': 'TestVersioning:Course:B:B'
          },
          'lastChangeDateTime': '2020-05-29T12:33:42+00:00',
          'full_statement': 'Node 1234',
          'destinationNode': ''
        },
        {
          'identifier': 'f6e132ee-e4d1-4f49-8771-b96a84ab40f5',
          'associationType': 'isChildOf',
          'CFDocumentURI': {
            'identifier': 'b1731727-7800-4374-be53-d2e3f7dad3d4',
            // tslint:disable-next-line: max-line-length
            'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFDocuments/b1731727-7800-4374-be53-d2e3f7dad3d4',
            'title': 'TestVersioning'
          },
          'sequenceNumber': 1,
          // tslint:disable-next-line: max-line-length
          'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFAssociations/f6e132ee-e4d1-4f49-8771-b96a84ab40f5',
          'originNodeURI': {
            'identifier': '806b48d9-770c-45af-9887-4b7242764d79',
            'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFItems/806b48d9-770c-45af-9887-4b7242764d79',
            'title': 'TestVersioning:Chapter1:B1:B1'
          },
          'destinationNodeURI': {
            'identifier': 'af67de05-0a4a-4846-b2dc-4ac2dfbd24a4',
            'uri': 'https://api.acmt-dev.learningmate.com/server/api/v1/AV001/ims/case/v1p0/CFItems/af67de05-0a4a-4846-b2dc-4ac2dfbd24a4',
            'title': 'TestVersioning:Course:B:B'
          },
          'lastChangeDateTime': '2020-05-29T12:33:42+00:00',
          'full_statement': 'Node 1234',
          'destinationNode': ''
        }]
      },
      'edit': {
        '294057b0-2f48-4a5a-b0b3-dd542d509809': [
          {
            'originNodeURI': {
              'old': 'TestVersioning:Chapter:A.1:A.1',
              'new': 'TestVersioning:Chapter1:A.1:A.1',
              'associationType': 'isChildOf',
              'full_statement': 'Node 9876',
              'destinationNode': ''
            }
          }
        ]
      }
    };
    setTimeout(() => {
      this.loadReport(this.selectedCriteria.type);
    }, 1000);
  }

  // FUNCTION to load table data
  loadReport(criteria) {
    switch (criteria) {
      case 'summary':
        this.summaryReport.extractSummaryData(this.rawVersionData);
        this.sharedService.showLoader.next(false);
        break;
      case 'metadata':
        this.metadataVersion.extractTableData(this.metadataArray);
        this.sharedService.showLoader.next(false);
        break;
      case 'nodetype':
        this.metadataVersion.extractTableData(this.nodetypeArray);
        this.sharedService.showLoader.next(false);
        break;
      case 'document':
        this.metadataVersion.extractTableData(this.docVersionSummary);
        this.sharedService.showLoader.next(false);
        break;
      case 'associations':
        this.associationVersion.extractTableData(this.associationArray);
        this.sharedService.showLoader.next(false);
        break;
    }
  }

  // FUNCTION to close auto-complete on clicking outside
  onClickedOutside(event) {
    this.autoComplete.openPanel(false);
  }

  //  FUNCTION to load detail Table
  loadDetail(data) {
    // tslint:disable-next-line: max-line-length
    this.selectedCriteria = this.versionCriteriaList.find(item => item.title === data.displayName && item.type.toLowerCase() === data.type.toLowerCase());
    this.currentTab = data.clickedOn;
    this.autoComplete.selectObject(this.selectedCriteria);
  }

}
