import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyVersioningComponent } from './taxonomy-versioning.component';

describe('TaxonomyVersioningComponent', () => {
  let component: TaxonomyVersioningComponent;
  let fixture: ComponentFixture<TaxonomyVersioningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxonomyVersioningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonomyVersioningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
