import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociationVersionReportComponent } from './association-version-report.component';

describe('AssociationVersionReportComponent', () => {
  let component: AssociationVersionReportComponent;
  let fixture: ComponentFixture<AssociationVersionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociationVersionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociationVersionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
