import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DataTableComponent } from 'src/app/common/data-table/data-table.component';
import { TabComponent } from 'src/app/common/tab/tab.component';
import Utils from 'src/app/global-files/utils';

@Component({
  selector: 'app-association-version-report',
  templateUrl: './association-version-report.component.html',
  styleUrls: ['./association-version-report.component.scss']
})
export class AssociationVersionReportComponent implements OnInit {

  listOfColumn = []; // holds list of column headers in version-summary table
  dataArray = []; // holds the data array to be shown in version-summary table
  defaultSortByColumn: any; // holds the column object by which the table data will be sorted by default
  pageDetail: any; // holds object for pagination details
  tabItems = ['new', 'updated', 'deleted']; // holds list of tab items to display versioning
  newData = []; // holds  data for new tab
  updatedData = []; // holds data for updated tab
  deletedData = []; // holds data for deleted tab
  counts = [0, 0, 0]; // holds array of data counts for each tab

  @Input() currentTab = 'new'; // holds currently selected tab

  @ViewChild('dataTable', { static: false }) dataTable: DataTableComponent; // holds reference of data-table component
  @ViewChild('tab', { static: false }) tab: TabComponent; // holds reference of Tab component

  constructor() { }

  ngOnInit() {
  }

  // FUNCTION to select tab item
  onTabSelected(tab) {
    this.currentTab = tab;
    this.setTable();
  }

  // FUNCTION to set columns according to current tab
  setTable() {
    switch (this.currentTab) {
      case 'new':
        this.listOfColumn = [
          {
            name: 'Full Statement',
            propName: 'key',
            class: '',
            type: 'text'
          },
          {
            name: 'Associations',
            propName: 'value',
            class: '',
            type: 'multi',
            keys: ['associationType', 'destinationNode']
          }
        ];
        this.dataArray = this.newData;
        break;
      case 'updated':
        this.listOfColumn = [
          {
            name: 'Full Statement',
            propName: 'key',
            class: '',
            type: 'text'
          },
          {
            name: 'Associations in New Version',
            propName: 'value',
            class: '',
            type: 'multi',
            keys: ['associationType', 'new']
          },
          {
            name: 'Associations in Current Version',
            propName: 'value',
            class: '',
            type: 'multi',
            keys: ['associationType', 'old']
          },
        ];
        this.dataArray = this.updatedData;
        break;
      case 'deleted':
        this.listOfColumn = [
          {
            name: 'Full Statement',
            propName: 'full_statement',
            class: '',
            type: 'text'
          },
          {
            name: 'Associations Deleted',
            propName: 'delete',
            class: '',
            type: 'text'
          },
          {
            name: '',
            propName: 'title',
            class: '',
            type: 'text'
          }
        ];
        this.dataArray = this.deletedData;
        break;
      default:
    }
  }

  extractTableData(rawData) {
    this.counts = [];
    this.newData = [];
    this.updatedData = [];
    this.deletedData = [];
    this.configureDataToDisplay(rawData);
    this.counts.push(this.newData.length ? this.newData.length : 0);
    this.counts.push(this.updatedData.length ? this.updatedData.length : 0);
    this.counts.push(this.deletedData.length ? this.deletedData.length : 0);
    this.onTabSelected(this.currentTab);
  }


  configureDataToDisplay(rawData) {
    if (rawData.new) {
      for (const key in rawData.new) {
        if (rawData.new[key].length) {
          const tempArr = [];
          rawData.new[key].forEach(element => {
            tempArr.push({
              full_statement: element.full_statement,
              associationType: element.associationType,
              sequence_number: element.sequenceNumber,
              destinationNode: element.destinationNodeURI.title
            });
          });
          this.newData = Utils.groupByType(tempArr, 'full_statement', 'sequence_number');
        }
      }
    }

    if (rawData.edit) {
      for (const key in rawData.edit) {
        if (rawData.edit[key].length) {
          const tempArr = [];
          rawData.edit[key].forEach(element => {
            tempArr.push({
              full_statement: element.originNodeURI.full_statement,
              associationType: element.originNodeURI.associationType,
              new: element.originNodeURI.new,
              old: element.originNodeURI.old
            });
          });
          this.updatedData = Utils.groupByType(tempArr, 'full_statement');
        }
      }
    }

  }
}
