import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { DataTableComponent } from 'src/app/common/data-table/data-table.component';

@Component({
  selector: 'app-version-summary',
  templateUrl: './version-summary.component.html',
  styleUrls: ['./version-summary.component.scss']
})
export class VersionSummaryComponent implements OnInit {

  listOfColumn = []; // holds list of column headers in version-summary table
  summaryData = []; // holds the data array to be shown in version-summary table
  defaultSortByColumn: any; // holds the column object by which the table data will be sorted by default
  pageDetail: any; // holds object for pagination details

  @Input() rawData: any; // holds raw input data for version summary report

  @Output() loadDetailEvent: EventEmitter<any>; // holds event to be emmitted while link column is clicked

  @ViewChild('dataTable', { static: false }) dataTable: DataTableComponent; // holds reference of data-table component

  constructor() {
    this.loadDetailEvent = new EventEmitter<any>();
  }

  ngOnInit() {
    this.listOfColumn = [
      {
        name: 'Updates to',
        propName: 'displayName',
        class: '',
        type: 'text'
      },
      {
        name: 'Type',
        propName: 'type',
        class: '',
        type: 'text',
        canFilter: true,
        isAsc: true
      },
      {
        name: 'New',
        propName: 'new',
        class: '',
        type: 'link',
        tabName: 'new'
      },
      {
        name: 'Updated',
        propName: 'edit',
        class: '',
        type: 'link',
        tabName: 'updated'
      },
      {
        name: 'Deleted',
        propName: 'delete',
        class: '',
        type: 'link',
        tabName: 'deleted'
      }
    ];
    this.defaultSortByColumn = this.listOfColumn[1];
  }

  // FUNCTION to extract version summary data from raw data to display in the table
  extractSummaryData(rawData) {
    const data = rawData;
    this.summaryData = [];
    let associationObj;
    if (data.document) {
      this.summaryData.push(data.document);
    }
    if (data.associations) {
      associationObj = {
        displayName: data.associations.displayName,
        type: data.associations.displayName,
        delete: 0,
        edit: 0,
        new: 0
      };
      data.associations.items.forEach(item => {
        associationObj.delete += item.delete;
        associationObj.edit += item.edit;
        associationObj.new += item.new;
      });
      this.summaryData.push(associationObj);
    }
    if (data.metadata) {
      data.metadata.items.forEach(item => {
        if (this.summaryData.length <= 25) {
          this.summaryData.push({
            displayName: item.type,
            type: data.metadata.displayName,
            delete: item.delete,
            edit: item.edit,
            new: item.new
          });
        }
      });
    }
    if (data.nodetype) {
      data.nodetype.items.forEach(item => {
        if (this.summaryData.length <= 25) {
          this.summaryData.push({
            displayName: item.type,
            type: data.nodetype.displayName,
            delete: item.delete,
            edit: item.edit,
            new: item.new
          });
        }
      });
    }
    this.dataTable.setData(this.summaryData);
  }

  // FUNCTION to load details
  onDetailsClicked(data) {
    this.loadDetailEvent.emit(data);
  }
}
