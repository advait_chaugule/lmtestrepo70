import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() tabItems = [];
  @Input() counts = [];
  @Input() currentTab;
  @Input() viewLocation;

  @Output() tabSelectedEvent: EventEmitter<any>;
  @Output() checkboxClickEvent: EventEmitter<any>;
  @Output() loadVersionSummaryEvent: EventEmitter<any>;

  constructor(private sharedService: SharedService, private cd: ChangeDetectorRef) {
    this.tabSelectedEvent = new EventEmitter<any>();
    this.checkboxClickEvent = new EventEmitter<any>();
    this.loadVersionSummaryEvent = new EventEmitter<any>();
  }

  ngOnInit() {
    if (this.tabItems && this.tabItems.length > 0) {
      if (this.currentTab === undefined) {
        this.currentTab = this.tabItems[0];
      }
    }
  }

  onTabSelected(tab) {
    this.currentTab = tab;
    this.tabSelectedEvent.emit(this.currentTab.toLowerCase());
  }

  setData(counts, currentTab?: string, tabItems?: any) {
    if (currentTab) {
      this.currentTab = currentTab;
    }
    if (tabItems) {
      this.tabItems = tabItems;
    }
    this.counts = counts;
    this.cd.detectChanges();
  }

}
