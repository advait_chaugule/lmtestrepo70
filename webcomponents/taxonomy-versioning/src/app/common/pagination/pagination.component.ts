import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import Utils from '../../global-files/utils';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {

  noOfItemsOptionList; // holds option items for page size
  initialPaginamtionRange = {
    start: 0,
    last: Utils.PAGINATION_LOWER_LIMIT,
    itemPerPage: Utils.PAGINATION_LOWER_LIMIT,
    pageIndex: 1
  }; // holds initial pagination object
  itemNumber; // holds selected page size object
  pageIndex; // holfs current page index
  noOfItemPerPage = Utils.PAGINATION_LOWER_LIMIT; // holds number of items per page
  maxPage = 0; // holds maximum page number
  startIndex; // holds starting index of data according current pagination
  lastIndex; // holds last index of data according current pagination

  @Input() dataLength = 0; // holds total length of data
  @Input() defaultPageRange: any; // holds default range of pagination

  @Output() displayItemRangeEvent: EventEmitter<any>; // event to be emmitted while pagination is changed
  @Output() maxPageCountEvent: EventEmitter<any>; // event to be emmitted while max page count changes


  constructor() {
    this.noOfItemsOptionList = [{
      value: 10,
      isSelected: false
    },
    {
      value: 15,
      isSelected: false
    },
    {
      value: 20,
      isSelected: false
    },
    {
      value: 25,
      isSelected: false
    },
    {
      value: 50,
      isSelected: false
    },
    {
      value: 100,
      isSelected: false
    }
    ];

    this.displayItemRangeEvent = new EventEmitter<any>();
    this.maxPageCountEvent = new EventEmitter<any>();
  }

  ngOnInit() {
    this.noOfItemsOptionList.forEach(item => {
      item.isSelected = false;
      if (this.defaultPageRange.itemPerPage === item.value) {
        this.itemNumber = item;
        this.itemNumber.isSelected = true;
      }
    });
    this.calculateMaxPage();
  }

  ngOnChanges() {
    if (this.defaultPageRange === undefined) {
      this.defaultPageRange = this.initialPaginamtionRange;
    }
    this.startIndex = this.defaultPageRange.start;
    this.lastIndex = this.defaultPageRange.last;
    this.noOfItemPerPage = this.defaultPageRange.itemPerPage;
    this.pageIndex = this.defaultPageRange.pageIndex;
    this.calculateMaxPage();
    this.updatePagination(this.dataLength);
  }
  showItems(resetPageIndex?: boolean) {
    this.noOfItemsOptionList.forEach(item => {
      item.isSelected = false;
    });
    if (this.itemNumber) {
      this.itemNumber.isSelected = true;
      this.noOfItemPerPage = this.itemNumber.value;
    }
    if (resetPageIndex) {
      this.pageIndex = 1;
    }
    this.calculateMaxPage();
    this.getStartIndex();
    this.getLastIndex();
    this.displayItemRangeEvent.emit({
      start: this.startIndex,
      last: this.lastIndex,
      itemPerPage: this.noOfItemPerPage,
      pageIndex: this.pageIndex,
      maxPages: this.maxPage
    });
  }

  onNextClicked() {
    if (this.pageIndex !== this.maxPage) {
      this.pageIndex++;
      this.getStartIndex();
      this.getLastIndex();
      this.displayItemRangeEvent.emit({
        start: this.startIndex,
        last: this.lastIndex,
        itemPerPage: this.noOfItemPerPage,
        pageIndex: this.pageIndex,
        maxPages: this.maxPage
      });
    }

  }

  onPreviousClicked() {
    if (this.pageIndex !== 1) {
      this.pageIndex--;
      this.getStartIndex();
      this.getLastIndex();
      this.displayItemRangeEvent.emit({
        start: this.startIndex,
        last: this.lastIndex,
        itemPerPage: this.noOfItemPerPage,
        pageIndex: this.pageIndex,
        maxPages: this.maxPage
      });
    }

  }

  calculateMaxPage() {
    this.maxPage = parseInt((this.dataLength / this.noOfItemPerPage) + '', 10) + ((this.dataLength % this.noOfItemPerPage) > 0 ? 1 : 0);
    this.maxPageCountEvent.emit(this.maxPage);
  }

  getStartIndex() {
    if (this.dataLength > this.noOfItemPerPage) {
      return this.startIndex = this.pageIndex === 1 ? 1 : (this.noOfItemPerPage * (this.pageIndex - 1)) + 1;
    } else {
      return this.startIndex = 1;
    }
  }

  getLastIndex() {
    this.lastIndex = ((this.noOfItemPerPage * (this.pageIndex - 1)) + this.noOfItemPerPage);
    if (this.lastIndex > this.dataLength) {
      this.lastIndex = this.dataLength;
    }
    return this.lastIndex;
  }

  updatePagination(dataLength) {
    if (dataLength <= this.noOfItemPerPage) {
      this.pageIndex = 1;
    }
    this.showItems();

  }

}
