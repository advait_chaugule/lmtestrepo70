import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  SimpleChanges,
  OnChanges,
  OnDestroy,
  ChangeDetectorRef,
  HostListener
} from '@angular/core';
import { Subscription } from 'rxjs';
import { PaginationComponent } from '../pagination/pagination.component';
import Utils from '../../global-files/utils';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, OnChanges, OnDestroy {

  Object = Object;
  currentDataList = []; // holds the current set of data according to filter or other criteria
  selectedLength: any;
  originalDataSet = []; // holds the whole data set
  noOfPages; // holds total number of pages of table data
  startIndex; // holds starting index of table data according current page
  lastIndex; // holds last index of table data according current page
  isFirstTime = true;
  minItemPerPage; // holds minimum number of items to be displayed per page
  filterData = []; // holds search result (coming from custom-filter pipe)
  filterList = {}; // holds object for filter list
  filterArray = []; // holds filtered data array
  lastSortBy: any; // holds column by which the table is sorted for the last time
  checkedRows = []; // holds list of checked rows
  pageIndex = -1; // holds current page index
  checkedPages = []; // holds list of checked/unchecked pages where all rows are selected or not
  maxPages = -1; // holds number of maximum pages
  itemPerPage = 0; // holds number of items to be displayed in a single page
  disabledAllCheckbox = false; // holds disabled for all checkbox
  showAllCheckBox = false; // to show or hide all checkbox
  dropdownSettings = {
    textField: 'label',
    showLabel: false,
    buttonType: 'icon'
  };
  filterOperationType = 'AND'; // holds filter operation type between multiple columns
  isConfigurable = false; // holds flag if the table columns are configurable
  showLoader = false; // holds flag to show loader
  filterDataEvent: Subscription;
  searchTextEvent: Subscription;

  @Input() tableCategory = ''; // holds table category
  @Input() columnList; // holds list of column headers
  @Input() dataSet = []; // holds the data array to be displayed in table
  @Input() defaultSortBy; // holds column by which table is sorted by default
  @Input() pagination: any; // holds object for pagination
  @Input() selectedData; // holds currently selected data in table
  @Input() selectedOptionList; // holds primarily selected options (if any)
  @Input() searchText = ''; // holds text to search in table
  @Input() sortRequired = true; // holds flag to show sort icon

  @Output() linkClicked: EventEmitter<any>; // holds event to be emmitted while link column is clicked
  @Output() pageChangeEvent: EventEmitter<any>; // holds event to be emmitted while changing pagination

  @ViewChild('paginationRef', { static: false }) paginationRef: PaginationComponent; // holds reference of pagination component


  constructor(private sharedService: SharedService, private _elem: ElementRef, private cd: ChangeDetectorRef) {
    this.linkClicked = new EventEmitter<any>();
    this.pageChangeEvent = new EventEmitter<any>();
    this.minItemPerPage = Utils.PAGINATION_LOWER_LIMIT;
    this.filterDataEvent = this.sharedService.filterDataTable.subscribe((res: any) => {
      this.filterData = res.data;
    });

    this.sharedService.showLoader.subscribe(val => {
      this.showLoader = val;
    });

  }

  ngOnInit() {
    this.startIndex = this.pagination && this.pagination.start > 1 ? this.pagination.start : 0;
    this.lastIndex = this.pagination ? this.pagination.last : Utils.PAGINATION_LOWER_LIMIT;
    if (this.dataSet && this.dataSet.length <= this.minItemPerPage) {
      this.lastIndex = this.dataSet.length;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.dataSet && this.dataSet.length) {
      this.parseTableData();
    }
  }

  parseTableData() {
    this.originalDataSet = [];
    this.dataSet.forEach(element => {
      element.checked = false; // initializing row checked as false
      this.originalDataSet.push(element);
    });
    this.columnList.forEach(col => {
      if (this.defaultSortBy && col.name === this.defaultSortBy.name) {
        col.isAsc = this.defaultSortBy.isAsc;
      } else {
        col.isAsc = false;
      }
      if (col.canFilter && !this.filterList[col['propName']]) {
        this.filterList[col['propName']] = [];
        this.dataSet.forEach(data => {
          if (data[col['propName']] || data[col['propName']] === 0) {
            data[col['propName']].toString().split(', ').forEach(element => {
              if (!this.filterList[col['propName']].find((obj) => element.trim() === (obj.label))) {
                this.filterList[col['propName']].push({
                  colName: col['propName'],
                  label: element.trim(),
                  is_selected: 0
                });
              }
            });
          }
          if (this.filterList[col['propName']].length > 1) {
            col['showFilter'] = true;
          } else {
            col['showFilter'] = false;
          }
        });
        if (this.filterList[col['propName']].length) {
          Utils.sortDataArray(this.filterList[col['propName']], 'label', false, true);
        }
      }
    });
    if (this.sortRequired) {
      if (this.defaultSortBy) {
        this.columnList.forEach(element => {
          if (element.name === this.defaultSortBy.name) {
            element.isAsc = !this.defaultSortBy.isAsc;
            this.sortData(element);
          }
        });
      } else {
        this.defaultSortBy = this.columnList[0];
        this.sortData(this.columnList[0]);
      }
    }
    if (this.dataSet.length < this.minItemPerPage) {
      this.selectedLength = this.dataSet.length;
    } else {
      this.selectedLength = this.minItemPerPage;
    }
    if (this.dataSet.length <= this.minItemPerPage) {
      this.lastIndex = this.dataSet.length;
    }
    this.resizeDataTable();
    this.showLoader = false;
    this.cd.detectChanges();
  }

  ngOnDestroy() {
    if (this.filterDataEvent) {
      this.filterDataEvent.unsubscribe();
    }
  }
  sortData(col) {
    if (col) {
      this.columnList.forEach(column => {
        column.isSorted = false;
      });
      col.isAsc = !col.isAsc;
      col.isSorted = true;
      Utils.sortDataArray(this.dataSet, col.propName, col.type === 'date', col.isAsc);
    }
  }

  onDetailsClicked(data, col) {
    this.selectedData = data;
    if (col.tabName) {
      data.clickedOn = col.tabName;
    }
    this.linkClicked.emit(data);
  }

  changeDateToLocalTimeZone(date) {
    return Utils.changeDateToLocalTimeZone(date);
  }

  resizeDataTable() {
    const length = this.selectedLength < this.dataSet.length ? this.selectedLength : this.dataSet.length;
    this.currentDataList = [];
    for (let i = 0; i < length; i++) {
      this.currentDataList.push(this.dataSet[i]);
    }
  }

  showDataInRange(event) {
    this.startIndex = event.start - 1;
    this.lastIndex = event.last;
    this.maxPages = event.maxPages; // getting maximum pages count
    this.pageIndex = event.pageIndex - 1;
    this.itemPerPage = event.itemPerPage;
    this.checkedPages = [];
    for (let i = 0; i < this.maxPages; i++) {
      this.checkedPages[i] = false;
    }
    this.checkedPages[this.pageIndex] = this.isAllChecked(this.startIndex, this.lastIndex);
  }

  getColumnFilterObj() {
    const obj = {};
    this.columnList.forEach(column => {
      obj[column.propName] = this.searchText;
    });
    return obj;
  }

  /**
   * Function to filter data based on selected filters 
   * It applied OR condition withing same coulumn filters and AND condition withing different column filters
   * @param data selected filter
   * @param column current column
   */
  filterDataList(data, column) {
    let newArr = [];
    this.filterArray = data;
    let hasFilter = false; // to check any filter selected from column
    // First filter on current column filters and (OR condition ) add all the matched object to newArr 
    this.filterArray.forEach(option => {
      if (column.propName === option.colName && option.is_selected) {
        hasFilter = true;
        newArr = newArr.concat(this.filter(option, this.originalDataSet));
      }
    });
    // Get Unique obj array
    newArr = Array.from(new Set(newArr));
    // Now filter over other filters and use newArr as data set (AND condition)
    let arr = [];
    newArr = hasFilter ? newArr : this.originalDataSet;
    hasFilter = false;
    this.filterArray.forEach(option => {

      if (column.propName !== option.colName && option.is_selected) {
        hasFilter = true;
        arr = arr.concat(this.filter(option, newArr));
      }
    });
    // Get Unique obj array
    arr = Array.from(new Set(arr));
    if (hasFilter) {
      newArr = arr;
    }
    this.dataSet = this.filterArray.length ? newArr : this.originalDataSet;
    if (this.dataSet.length > 1) {
      Utils.sortDataArray(this.dataSet, this.defaultSortBy.propName, this.defaultSortBy.type === 'date', this.defaultSortBy.isAsc);
    }
    if (this.dataSet && this.dataSet.length <= this.minItemPerPage) {
      this.startIndex = 0;
      this.lastIndex = this.dataSet.length;
    }
    if (this.paginationRef) {
      this.paginationRef.updatePagination(this.dataSet.length);
    }

  }

  /**
   * TODO need to fix lint issue
   * @param option
   * @param dataSet
   */
  filter(option, dataSet) {
    const dataSetArr = [];
    dataSet.filter(obj => {
      const arr = obj[option['colName']] || (obj[option['colName']] === 0) ? obj[option['colName']].toString().split(',') : [];

      arr.forEach(element => {
        if (element && element.trim().toLowerCase() === option['label'].toLowerCase()) {
          dataSetArr.push(obj);
        }
      });

    });
    return dataSetArr;
  }

  /**
   * On filtered options selected
   * @param event (filtered options selected data)
   * @param column (current column from where filter is triggered)
   */
  onFilterOptionSelect(event: any, column: any) {
    let filterCriteria = [];

    if (this.filterOperationType === 'OR' && this.selectedOptionList && this.selectedOptionList.length > 0) {
      // add selected options to the filter list
      event.selectedData.forEach(option => {
        if (!this.filterArray.find((obj) => obj.label.trim() === option.label.trim())) {
          this.filterArray.push(option);
        }
      });

      // remove unselected options from the filter list
      this.filterArray = this.filterArray.filter(obj => obj.is_selected === 1);

      this.filterDataList_OR();
    } else {
      this.filterArray = [];
      event.selectedData.forEach(option => {
        if (!this.filterArray.find((obj) => obj.label.trim() === option.label.trim())) {
          this.filterArray.push(option);
        }
      });

      // remove unselected options from the filter list
      this.filterArray = this.filterArray.filter(obj => obj.is_selected === 1);
      filterCriteria = this.filterArray;
      this.filterDataList(filterCriteria, column);
    }

  }

  filterDataList_OR() {
    const newArr = [];
    this.filterArray.forEach(option => {
      this.originalDataSet.filter(obj => {
        if (obj[option.colName].toLowerCase().includes(option.label.toLowerCase().trim())) {
          if (newArr.indexOf(obj) === -1) {
            newArr.push(obj);
          }
        }
      });
    });
    this.dataSet = newArr.length ? newArr : this.originalDataSet;
    Utils.sortDataArray(this.dataSet, this.defaultSortBy.propName, this.defaultSortBy.type === 'date', this.defaultSortBy.isAsc);
  }

  // To check all rows checked or unchecked between specific range for page (i.e. startindex and lastindex)
  isAllChecked(startIndex: number, lastIndex: number) {
    let checked = true;
    for (let i = startIndex; i < lastIndex; i++) {
      if (this.dataSet[i] && this.dataSet[i].checked) {
        continue;
      } else {
        checked = false;
        break;
      }
    }
    return checked;
  }

  // On focus any row's button, select that data
  onFocus(data: any) {
    this.selectedData = data;
  }

  setWidth(column) {
    if (this.isConfigurable) {
      return column.width ? (column.width + 'px') : this.calWidth();
    } else {
      return column.width;
    }
  }

  setData(data, columns?: any) {
    if (columns) {
      this.columnList = columns;
    }
    this.dataSet = data;
    if (this.dataSet && this.dataSet.length <= this.minItemPerPage) {
      this.lastIndex = this.dataSet.length;
    }
    this.parseTableData();
  }

  calWidth() {
    const tableWidth = document.getElementById('dataTable') ? document.getElementById('dataTable').offsetWidth : 0;
    const optionColWidth = document.getElementById('optionColumn') ? document.getElementById('optionColumn').offsetWidth : 0;
    const dataColWidth = tableWidth - optionColWidth;
    const noOfCol = this.columnList.length;
    const defaultColWidth = (dataColWidth / noOfCol);
    return defaultColWidth + 'px';
  }

}
