import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import { GlobalSettings } from '../global-files/global-settings';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  baseURL = GlobalSettings.BASE_URL;
  isDialogVisible = false;

  constructor(private httpClient: HttpClient) { }

  getServiceData(url, isHeader = true, fullRes = false) {
    url = this.baseURL + url;
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders({
      });
      if (isHeader === false) {
        headers = new HttpHeaders({});
      }
      const requestOptions = {
        headers: headers
      };
      this.httpClient.get(url, requestOptions)
        .subscribe(
          response => {
            if (response !== null) {
              this.isDialogVisible = false;
              const data: any = response;
              if (data.status === 200 || data.status === 201) {
                resolve(fullRes ? data : data.data);
              } else {
                reject(data.message);
              }
            }
          }, error => {
            reject(error);
          }
        );
    });

  }
}
