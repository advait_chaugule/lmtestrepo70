import {
  Injectable
} from '@angular/core';
import {
  BehaviorSubject
} from 'rxjs/BehaviorSubject';
import {
  Router
} from '@angular/router';

@Injectable()
export class SharedService {
  sucessEvent: BehaviorSubject<Object> = new BehaviorSubject({});
  filterDataTable: BehaviorSubject<Object> = new BehaviorSubject({});
  showLoader: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() { }

}
