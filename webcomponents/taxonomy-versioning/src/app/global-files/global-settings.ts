
export class GlobalSettings {
    static env = 'acmt-dev';
    public static BASE_URL = 'https://api.' + GlobalSettings.env + '.learningmate.com/server/';
    public static AUTH_URL = '';
    public static VERSION = 'api/v1/';

    /** TAXONOMY VERSION */
    public static VERSION_SUMMARY_REPORT = GlobalSettings.VERSION + 'comparison-summary-report';

}
