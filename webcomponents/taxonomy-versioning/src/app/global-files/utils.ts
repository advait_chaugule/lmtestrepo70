import {
    from
} from 'rxjs';

export default class Utils {

    static PAGINATION_LOWER_LIMIT = 25;
    static STATUS_DRAFT = 1;
    static STATUS_PUBLIC_REVIEW = 5;
    static STATUS_PUBLISHED = 3;
    static STATUS_PUBLIC_REVIEW_COPIED = 6;
    static NOTIFICATION_INTERVAL_TIME = 30; // in seconds
    static MAX_CACHE_AGE = 60000; // (in milliseconds) holds time for which api data will be stored in cache

    /**
     * Method to sort Array of objects
     * @param  array: Array that need to be sort
     * @param  sortby: object property name
     * @param  isDate: true/false if property is date type
     * @param  isAsce: true/false sort order
     */
    static sortDataArray(array, sortby, isDate, isAsce) {
        array.sort(function (a, b) {
            let val1: any = '';
            let val2: any = '';
            if (!isDate && typeof (a[sortby]) === 'number') {
                val1 = a[sortby];
                val2 = b[sortby];
            } else {
                val1 = a[sortby] ? a[sortby].toUpperCase() : '';
                val2 = b[sortby] ? b[sortby].toUpperCase() : '';
            }
            if (isDate) {
                val1 = new Date(a[sortby].replace(/-/g, '/'));
                val2 = new Date(b[sortby].replace(/-/g, '/'));
            }
            if (val1 === val2) {
                return 0;
            }
            if (val1 > val2) {
                return isAsce ? 1 : -1;
            }
            if (val1 < val2) {
                return isAsce ? -1 : 1;
            }
        });
    }

    static changeDateToLocalTimeZone(date) {
        let newDate: any;
        if (date === '') {
            return '-';
        }
        if (date) {
            let dateTrimed;
            if (date.length > 10) {
                dateTrimed = date.substr(0, date.length - 3);
            } else {
                dateTrimed = date;
            }
            newDate = new Date(dateTrimed.replace(/-/g, '/') + ' UTC');
        }
        return newDate ? (newDate.toString()) : '';
    }

    static getDateWithoutTime(date: string) {
        let d;
        let formatted = '';
        d = new Date(date.replace(/-/g, '/') + ' UTC');
        formatted = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
        return formatted;
    }



    /***************************** Group an array by value *********************/

    static groupByType(array: any, type: string, sortBy?: string) {
        const propArr = [];
        const dataArr = [];
        array.forEach(item => {
            if (item[type] && propArr.indexOf(item[type]) === -1) {
                propArr.push(item[type]);
            }
        });

        propArr.forEach(prop => {
            const obj = {};
            obj['key'] = prop;
            obj['value'] = array.filter(data => {
                return (data[type] === prop);
            });
            if (sortBy) {
                this.sortDataArray(obj['value'], sortBy, false, true);
            }
            dataArr.push(obj);
        });
        this.sortDataArray(dataArr, 'key', false, true);
        return dataArr;
    }

    /**********************************************************/

}
