const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/Taxonomy-Versioning/runtime.js',
    './dist/Taxonomy-Versioning/polyfills.js',
    './dist/Taxonomy-Versioning/scripts.js',
    './dist/Taxonomy-Versioning/main.js'
  ];

  await fs.ensureDir('elements');
  await concat(files, 'elements/taxonomy-version.js');
})();
