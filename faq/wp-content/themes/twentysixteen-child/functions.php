<?php 
	 add_action( 'wp_enqueue_scripts', 'twentysixteen_child_enqueue_styles' );
	 function twentysixteen_child_enqueue_styles() {
		$parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'child-style',
			get_stylesheet_directory_uri() . '/style.css',
			array( $parent_style ),
			wp_get_theme()->get('Version')
		);

		wp_enqueue_style( 'product_tour_css', get_stylesheet_directory_uri() . '/bootstraptour/css/bootstrap-tour-standalone.min.css' );

		wp_register_script( 
			'product_tour_js', 
			get_stylesheet_directory_uri() . '/bootstraptour/js/bootstrap-tour-standalone.min.js', 
			array( 'jquery' )
		);

		wp_enqueue_script( 'product_tour_js', get_stylesheet_directory_uri() . '/bootstraptour/js/bootstrap-tour-standalone.min.js');		

		} 
		   

	// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
 
// Creating the widget 
class wpb_widget extends WP_Widget {
 
function __construct() {
parent::__construct(
 
// Base ID of your widget
'wpb_widget', 
 
// Widget name will appear in UI
__('FAQ Category Widget', 'wpb_widget_domain'), 
 
// Widget description
array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
 
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

$post_id= get_the_ID();

$posts= get_post($post_id);

$category= get_the_terms($post_id,'epkb_post_type_1_category');

$term_id=$category[0]->term_taxonomy_id;
echo '<h2>'.$category[0]->name.'</h2>';


$articles = get_posts(array(
	'post_type' => 'epkb_post_type_1',
	'numberposts' => -1,
	'orderby'        => 'post_title',
	'order' => 'ASC',
	'tax_query' => array(
	  array(
		'taxonomy' => 'epkb_post_type_1_category',
		'field' => 'id',
		'terms' => $term_id, // Where term_id of Term 1 is "1".
		'include_children' => false
	  )
	)
  ));

  
	echo '<ul>';
	foreach($articles as $article ){
		$class='';
		//show current article selected
		if($article->ID==$post_id)
			$class='class="active-article"';

		echo '<li><a '.$class.'  href="'.get_permalink( $article->ID).'" >';
		echo  $article->post_title;
		echo '</a></li>';
	}
	echo '</ul>';


// This is where you run the code and display the output
//echo __( 'Hello, World!', 'wpb_widget_domain' );
echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here


function get_tax_navigation( $taxonomy = 'category' ) 
{
    // Make sure we are on a taxonomy term/category/tag archive page, if not, bail
	/* if ( 'category' === $taxonomy ) {
        if ( !is_category() )
            return false;
    } elseif ( 'post_tag' === $taxonomy ) {
        if ( !is_tag() )
            return false;
    } else {
        if ( !is_tax( $taxonomy ) )
            return false;
	} */
	//echo $taxonomy.' before if <br>';
    // Make sure the taxonomy is valid and sanitize the taxonomy
    if (    'category' !== $taxonomy 
         || 'post_tag' !== $taxonomy
    ) {
		$taxonomy = filter_var( $taxonomy, FILTER_SANITIZE_STRING );
		//echo $taxonomy.'<br>';
        if ( !$taxonomy ){
			//echo 'test1<br>';
			return false;
		}

        if ( !taxonomy_exists( $taxonomy ) ){
			//echo 'test2<br>';
			return false;
		}
    }
	
    // Get the current term object
	//$current_term = get_term( $GLOBALS['wp_the_query']->get_queried_object() );
	
	$post_id= get_the_ID();
	//echo $post_id.' post_id after if <br>';
	//$posts= get_post($post_id);

	$current_term= get_the_terms($post_id,'epkb_post_type_1_category');

	//echo '<pre>';		print_r($current_term);
    // Get all the terms ordered by slug 
	$terms = get_terms( $taxonomy, ['orderby' => 'slug','parent' => $current_term[0]->parent] );	
	
	//print_r($terms);
    // Make sure we have terms before we continue
    if ( !$terms ) 
        return false;

    // Because empty terms stuffs around with array keys, lets reset them
    $terms = array_values( $terms );

    // Lets get all the term id's from the array of term objects
    $term_ids = wp_list_pluck( $terms, 'term_id' );

    /**
     * We now need to locate the position of the current term amongs the $term_ids array. \
     * This way, we can now know which terms are adjacent to the current one
     */
	$current_term_position = array_search( $current_term[0]->term_id, $term_ids );
	/* print_r($term_ids);
	echo '<br> current term id '.$current_term[0]->term_id.'<br>';
	echo 'current term position '.$current_term_position.'<br>';  */ 
    // Set default variables to hold the next and previous terms
    $previous_term = '';
    $next_term     = '';

    // Get the previous term
    if ( 0 !== $current_term_position ){ 
		//echo '<br> previous term position '.($current_term_position - 1) .'<br>';
		$previous_term = $terms[$current_term_position - 1];
		$previous_term_id=$previous_term->term_id;
		$previous_term_topic = get_first_article_href($previous_term_id);
	}

    // Get the next term
    if ( intval( count( $term_ids ) - 1 ) !== $current_term_position ){ 
		//echo '<br> next term position '. ($current_term_position + 1).'<br>';
		$next_term = $terms[$current_term_position + 1];
		$next_term_id=$next_term->term_id;
		$next_term_topic = get_first_article_href($next_term_id);
	}

	//echo '<br> next term id '.$next_term_id.'<br>';
	//echo 'previous term id '.$previous_term->term_id.'<br>'; 



    // Build the links
	
		$pre_str='<div class="navigation hide_on_page" >
		<p class="coming-next-heading">
			<strong>Up next</strong>';
		
		if($previous_term_id)
			$pre_str.='<a href="' . esc_url( $previous_term_topic ) . '" class="nav prev"> <img src="'.get_stylesheet_directory_uri().'/images/prev.gif" >  </a>';
		
		if($next_term_id)		
			$pre_str.='<a href="' . esc_url( $next_term_topic ) . '" class="nav next" ><img src="'.get_stylesheet_directory_uri().'/images/next.gif" >  </a> <br>';
	
		$pre_str.='</p>';

		$pre_str.='<div class="topic">
		<p class="view-topic-heading"><strong>'.$next_term->name .'</strong></p>
		<p>'.$next_term->description.'</p>
		<p class="view-topic-btn" >
		<a href="' . esc_url( $next_term_topic ) . '"  class="view-topic"> View Topic</a>
		</p>
		</div>		
		</div>';
		
		
		

	return $pre_str;
   
}

function get_first_article_href($term_id){

	$articles = get_posts(array(
		'post_type' => 'epkb_post_type_1',
		'numberposts' => -1,
		'tax_query' => array(
		  array(
			'taxonomy' => 'epkb_post_type_1_category',
			'field' => 'id',
			'terms' => $term_id, // Where term_id of Term 1 is "1".
			'include_children' => false
		  )
		)
	  ));
	
	  return get_permalink($articles[0]->ID);	
	
}



	function wpforo_search_form( $html ) {

		$html = str_replace( 'placeholder="Search ', 'placeholder="Search for Frequently Asked Questions ', $html );
		
		$html = str_replace( '<label>', '<h2 style="color: #fff;">How can we help you?</h2><label>', $html );		
		
        return $html;
	}
	add_filter( 'get_search_form', 'wpforo_search_form' );

	function product_tour(){
		?> 
		<script>
var steps= [
	{
		element: ".search-field",
		title: "Search",
		content: "Type to search",
		placement:"top"
	},
	{
		element: ".eckb-categories-list",
		title: "FAQ category",
		content: "Contents FAQ",
		placement:"top"
	}
	,
	{
		element: ".eckb-categories-list",
		title: "FAQ category 3 ",
		content: "Contents FAQ 3",
		placement:"top"
	}		
	];
var stepsCount = steps.length;

		// Instance the tour
	var tour = new Tour({
		backdrop:true,
		template: fn_template, 

  /* template: "<div class='popover tour'>"+
    "<div class='arrow'></div>"+
    "<h3 class='popover-title'></h3>"+
    "<div class='popover-content'></div>"+
    "<div class='popover-navigation'>"+
        "<button class='btn btn-default' data-role='prev'>Prev</button>"+
        "<span data-role='separator'>|</span>"+
        "<button class='btn btn-default' data-role='next'>Next</button>"+
    "</div>"+
    "<button class='btn btn-default' data-role='end'>X</button>"+
  "</div>", */

	steps: steps});

// Initialize the tour
tour.init();
tour.start();

function fn_start_tour(){ 
// re-start the tour
 tour.restart();
}

function fn_template(step,total){

	var step_text=  eval(step+1)+' of '+stepsCount;

	var template= '<div class="popover tour">'+
		 '<div class="arrow" ></div>'+ 
		 '<h3 class="popover-title"></h3>'+ 
		 '<div class="popover-content"></div>'+ 
		 '<div class="popover-navigation">'+ 
		 '<p class="mb-0 text-muted">'+step_text+'</p>'+
		 '<div class="btn-group">'+ 
		 '<button class="btn btn-sm btn-default" data-role="prev">Prev</button>'+ 
		 '<button class="btn btn-sm btn-default" data-role="next" >Next</button>'+
		  '</div>'+ 
		 '<button class="btn btn-sm btn-default" data-role="end">X</button> </div> </div>';

	return template;

}

		</script>
		<?php
		
	}  

	function show_question_image(){
		echo '<a href="javascript:void(0)" id="start_tour" onclick="fn_start_tour();"  > 
		<img alt="Help" class="img-fluid ic-question" src="'.get_stylesheet_directory_uri() . '/images/question.gif"></a>';

	}

	

function my_the_content_filter($content) {


$date_time = "<p>Last modified on ".get_the_modified_date('M j, Y')."</p> "; 
$content = str_replace( '<div class="helpful helpful-theme-base">', $date_time.'<div class="helpful helpful-theme-base">', $content );
	return $content;
  }
  
  add_filter( 'the_content', 'my_the_content_filter' );
 ?>