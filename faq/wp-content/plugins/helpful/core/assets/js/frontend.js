(function($) {

  class HelpfulPlugin {
    constructor() {
      var self = this;
      self.helpful = $('.helpful');
      self.proButton = $('.helpful-pro');
      self.conButton = $('.helpful-con');

      self.initPro();
      self.initContra();
    }

    // on pro
    initPro() {
      var self = this;

      $(self.proButton).on('click tap', function(e) {
        e.preventDefault();

    		var ajaxData = {}

    		ajaxData['action'] = 'helpful_ajax_pro';
    		ajaxData['post_id'] = $(this).data('id');
    		ajaxData['user'] = $(this).data('user');
    		ajaxData['pro'] = $(this).data('pro');
    		ajaxData['contra'] = $(this).data('contra');

        var currentRequest = self.ajaxRequest(ajaxData);

        currentRequest.done(function(response) {
    			$(self.helpful).html( response );
        });

    		currentRequest.always(function(response) {
    			self.contactForm7();
    		});

        return false;
      });
    }

    // on contra
    initContra() {
      var self = this;

      $(self.conButton).on('click tap', function(e) {
        e.preventDefault();

    		var ajaxData = {}

    		ajaxData['action'] = 'helpful_ajax_contra';
    		ajaxData['post_id'] = $(this).data('id');
    		ajaxData['user'] = $(this).data('user');
    		ajaxData['pro'] = $(this).data('pro');
    		ajaxData['contra'] = $(this).data('contra');

        var currentRequest = self.ajaxRequest(ajaxData);

        currentRequest.done(function(response) {
    			$(self.helpful).html( response );
        });

    		currentRequest.always(function(response) {
    			self.contactForm7();
    		});

        return false;
      });
    }

    // contact form 7 support
    contactForm7() {
  		if( $('.wpcf7').length ) {
  			var wpcf7Elm = $( '.wpcf7' );
  			var actionUrl = $(wpcf7Elm).find('form').attr('action').split('#');

  			$(wpcf7Elm).find('form').attr('action', "#" + actionUrl[1]);

   			$(wpcf7Elm).find('form').each( function() {
  				var $form = $( this );
  				wpcf7.initForm( $form );
  				if ( wpcf7.cached ) {
  					wpcf7.refill( $form );
  				}
  			});
  		}
    }

    ajaxRequest(data) {
      return $.ajax({
  			url : helpful.ajax_url,
        data : data,
      });
    }
  }

  $(function() {
    new HelpfulPlugin();
  });

})(jQuery)
