(function($){
  if( $('#helpful_widget').length ) {
    $('#helpful_widget').find('strong').each(function(){
      var headline = $(this),
          parent = $(headline).parent('div'),
          list = $(parent).find('ul');

      $(headline).addClass('clickable')
      $(list).hide()
      $(headline).toggle(function(){
        $(list).show()
      }, function() {
        $(list).hide()
      })
    })
  }
})(jQuery)
