(function($) {

  // Colorpickers
  // for future
  // $('.my-color-field').wpColorPicker();

  // Codemirror
  var HelpfulEditor = document.getElementById('helpful_css');
  /*
  var editor = CodeMirror.fromTextArea(HelpfulEditor, {
    lineNumbers: true
  });
  */

  var editor = CodeMirror.fromTextArea(HelpfulEditor, {
    lineNumbers: true,
    mode: "text/javascipt",
    theme: "blackboard"
  });

})(jQuery)
