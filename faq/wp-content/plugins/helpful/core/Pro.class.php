<?php
/**
 * Helpful Pro Class
 *
 * All options are only in Helpful Pro!
 * (now in free version too!)
 *
 * @author Devhats
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Init class
new HelpfulPro;

class HelpfulPro
{
	/**
	 * Class Constructor
	 */
	public function __construct()
	{

		// Register Settings
		add_action( 'admin_init', array( $this, 'settings' ) );

		// Themes
		add_filter( 'helpful-themes', array( $this, 'themes' ) );

		// Admin Enqueue
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue' ), 1 );

		// Frontend add to head (CSS)
		add_action( 'wp_head', array( $this, 'add_css_to_head' ) );

		// Add General Settings
		add_action( 'helpful_general_settings', array( $this, 'general_settings' ) );

		// After Pro Message
		add_filter( 'helpful_after_pro', array( $this, 'after_pro' ), 1 );

		// After Contra Message
		add_filter( 'helpful_after_contra', array( $this, 'after_contra' ), 1 );
	}

	/**
	 * Register Settings
	 */
	public function settings()
	{
		// Register general options
		register_setting( 'helpful-general-settings-group', 'helpful_after_pro' );
		register_setting( 'helpful-general-settings-group', 'helpful_after_contra' );

		register_setting( 'helpful-general-settings-group', 'helpful_form_status_pro' );
		register_setting( 'helpful-general-settings-group', 'helpful_form_email_pro' );
		register_setting( 'helpful-general-settings-group', 'helpful_form_status_contra' );
		register_setting( 'helpful-general-settings-group', 'helpful_form_email_contra' );

		// Register design options
		register_setting( 'helpful-design-settings-group', 'helpful_theme' );
		register_setting( 'helpful-design-settings-group', 'helpful_css' );
		register_setting( 'helpful-design-settings-group', 'helpful_color' );
	}

	/**
	 * Register General Settings
	 */
	public function general_settings()
	{
		// Editor Settings
		$settings = array( 'teeny' => true, 'media_buttons' => false, 'textarea_rows' => 5 );

		echo '<table class="form-table">';

		// Editor for after submit: pro
		echo '<tr valign="top">';
		echo '<th scope="row"><label for="helpful_after_pro">' . _x('After voting (pro)', 'message after a positive voting', 'helpful') . '</label></th>';
		echo '<td>';
		wp_editor( get_option('helpful_after_pro'), 'helpful_after_pro', $settings );
		echo '<p class="description">';
		_e('The text that is displayed, after a positive vote (shortcodes <b>without Ajax</b> are also possible!)','helpful');
		echo '</p>';
		echo '</td>';
		echo '</tr>';

		// Editor for after submit: contra
		echo '<tr valign="top">';
		echo '<th scope="row"><label for="helpful_after_contra">' . _x('After voting (contra)', 'message after a negative voting', 'helpful') . '</label></th>';
		echo '<td>';
		wp_editor( get_option('helpful_after_contra'), 'helpful_after_contra', $settings );
		echo '<p class="description">';
		_e('The text that is displayed, after a negative vote (shortcodes <b>without Ajax</b> are also possible!)','helpful');
		echo '</p>';
		echo '</td>';
		echo '</tr>';

		echo '</table>';

		echo '<hr />';
	}

	/**
	 * Message After Pro
   * Updated 20.09.2018
	 */
	public function after_pro()
	{
		$after = get_option( 'helpful_after_pro' ) ? do_shortcode( get_option( 'helpful_after_pro' ) ) : __( 'Thank you for voting.', 'helpful' );
		return $after;
	}

	/**
	 * Message After Contra
   * Updated 20.09.2018
	 */
	public function after_contra()
	{
		$after = get_option( 'helpful_after_contra' ) ? do_shortcode( get_option( 'helpful_after_contra' ) ) : __( 'Thank you for voting.', 'helpful' );;
		return $after;
	}

	/**
	 *  Register Themes
	 */
	public function themes( $themes )
	{
		// set theme array
		$themes = array(
			'base' 		=> _x( 'Base', 'theme name', 'helpful' ),
			'dark' 		=> _x( 'Dark', 'theme name', 'helpful' ),
			'minimal' => _x( 'Minimal', 'theme name', 'helpful' ),
			'flat' 		=> _x( 'Flat', 'theme name', 'helpful' ),
			'theme'		=> _x( 'Theme', 'theme name', 'helpful' )
		);

		return $themes;
	}

	/**
	 * Admin enqueue
	 */
	public function admin_enqueue()
	{
		// current screen is helpful
		if( is_helpful() ) {

			// Register theme for preview
			foreach ( glob( plugin_dir_path( HELPFUL_FILE ) . 'core/assets/themes/*.css' ) as $theme ) {
				$name = str_replace( array('.css'), '', basename( $theme, PHP_EOL ) );
				$file = plugin_dir_path( HELPFUL_FILE ) . 'core/assets/themes/' . $name . '.css';

				if( file_exists( $file ) ) {
					wp_enqueue_style(
						'helpful-preview-' . $name,
						plugins_url( 'core/assets/themes/' . $name . '.css', HELPFUL_FILE ),
						false
					);
				}
			}

			wp_enqueue_style(
				'helpful-design',
				plugins_url( 'core/assets/css/tab-design.css', HELPFUL_FILE ),
				false
			);

      // Enqueue code editor and settings for manipulating HTML.
      $settings = wp_enqueue_code_editor( array( 'type' => 'css' ) );

      // Bail if user disabled CodeMirror.
      if ( false !== $settings ) {
        wp_add_inline_script(
          'code-editor',
          sprintf(
            'jQuery( function() { wp.codeEditor.initialize( "helpful_css", %s ); } );',
            wp_json_encode( $settings )
          )
        );
      }
		}
	}

	/**
	 * Add custom css to wp_head
	 */
	public function add_css_to_head()
	{
		if( get_option( 'helpful_css' ) ):

		echo '
		<!-- HELPFUL: CUSTOM CSS -->
		<style>' . get_option('helpful_css') . '</style>
		<!-- end HELPFUL: CUSTOM CSS -->
		';

		endif;
	}

} // end HelpfulPro Class
