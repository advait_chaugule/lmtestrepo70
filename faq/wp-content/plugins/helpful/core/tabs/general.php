<?php
/**
 * Tab: General Options
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

global $helpful;

if( $helpful['tab'] == 'general' ) :

// wp_editor settings
$settings = array( 'teeny' => true, 'media_buttons' => false, 'textarea_rows' => 5 );

?>

<h3><?php _ex( 'General', 'tab name', 'helpful' ); ?></h3>

<p><?php _ex( 'In this area, you can change general settings. You can use <code>{pro}</code> for outputting positive and <code>{contra}</code> for negative votes.', 'tab description', 'helpful' ); ?></p>

<hr />

<form method="post" action="options.php">

	<?php settings_fields( 'helpful-general-settings-group' ); ?>
	<?php do_settings_sections( 'helpful-general-settings-group' ); ?>

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="helpful_heading"><?php _ex( 'Headline', 'option name', 'helpful' ); ?></label></th>
			<td>
				<input type="text" id="helpful_heading" name="helpful_heading" class="regular-text" value="<?php echo esc_attr( get_option('helpful_heading') ); ?>"/>
				<p class="description"><?php _ex( 'Here you can define your own headline.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_content"><?php _ex( 'Content', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php wp_editor( get_option('helpful_content'), 'helpful_content', $settings ); ?>
				<p class="description"><?php _ex( 'Here you can define your own content.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_exists"><?php _ex('Already voted', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php wp_editor( get_option('helpful_exists'), 'helpful_exists', $settings ); ?>
				<p class="description"><?php _ex( 'This text will appear if the user has already voted.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
	</table>

	<hr />

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="helpful_pro"><?php _ex( 'Button (pro)', 'option name', 'helpful' ); ?></label></th>
			<td>
				<input type="text" id="helpful_pro" name="helpful_pro" class="regular-text" value="<?php echo esc_attr( get_option('helpful_pro') ); ?>"/>
				<p class="description"><?php _ex( 'Here you can define your own text for the pro button.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_contra"><?php _ex( 'Button (contra)', 'option name', 'helpful' ); ?></label></th>
			<td>
				<input type="text" id="helpful_contra" name="helpful_contra" class="regular-text" value="<?php echo esc_attr( get_option('helpful_contra') ); ?>"/>
				<p class="description"><?php _ex( 'Here you can define your own text for the contra button.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
	</table>

	<hr />

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="helpful_column_pro"><?php _ex('Column (pro)', 'option name', 'helpful' ); ?></label></th>
			<td>
				<input type="text" id="helpful_column_pro" name="helpful_column_pro" class="regular-text" value="<?php echo esc_attr( get_option('helpful_column_pro') ); ?>"/>
				<p class="description"><?php _ex( 'Here you can define your own text for the pro column in the post edit list.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_column_contra"><?php _ex( 'Column (contra)', 'option name', 'helpful' ); ?></label></th>
			<td>
				<input type="text" id="helpful_column_contra" name="helpful_column_contra" class="regular-text" value="<?php echo esc_attr( get_option('helpful_column_contra') ); ?>"/>
				<p class="description"><?php _ex( 'Here you can define your own text for the contra column in the post edit list.', 'helpful' ); ?></p>
			</td>
		</tr>
	</table>

	<hr />

	<?php do_action( 'helpful_general_settings' ); ?>

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="helpful_post_types"><?php _ex( 'Post types', 'option name', 'helpful' ); ?></label></th>
			<td class="helpful-checkbox">

				<?php $post_types = get_post_types( array( 'public' => true ) ); ?>

				<?php if( $post_types ) : foreach( $post_types as $post_type ) : ?>

				<?php if( get_option('helpful_post_types') ) : ?>

					<?php $checked = in_array( $post_type, get_option('helpful_post_types') ) ? 'checked="checked"' : ''; ?>

					<label>
						<input type="checkbox" name="helpful_post_types[]" id="helpful_post_types[]" value="<?php echo $post_type; ?>" <?php echo $checked; ?>/>
						<?php echo $post_type; ?>
					</label>

				<?php else : ?>

					<label>
						<input type="checkbox" name="helpful_post_types[]" id="helpful_post_types[]" value="<?php echo $post_type; ?>"/>
						<?php echo $post_type; ?>
					</label>

				<?php endif; ?>

				<?php endforeach; endif; ?>

				<p class="description"><?php _ex('Here you can choose the post types, where helpful should appear.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_exists_hide"><?php _ex( 'Already voted', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_exists_hide') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_exists_hide" type="checkbox" name="helpful_exists_hide" <?php echo $checked; ?> /> <?php _ex( 'hide', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Hide Helpful, if the user has already voted.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_count_hide"><?php _ex( 'Votes', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_count_hide') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_count_hide" type="checkbox" name="helpful_count_hide" <?php echo $checked; ?> /> <?php _ex( 'hide', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Hide vote counters.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_credits"><?php _ex( 'Credits', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_credits') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_credits" type="checkbox" name="helpful_credits" <?php echo $checked; ?> /> <?php _ex( 'show', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Support us and show your visitors that Helpful is from us.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="helpful_hide_in_content"><?php _ex( 'Helpful', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_hide_in_content') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_hide_in_content" type="checkbox" name="helpful_hide_in_content" <?php echo $checked; ?> /> <?php _ex('hide', 'label', 'helpful'); ?>
				</label>
				<p class="description"><?php _ex( 'Hide Helpful in your content. These option is useful by using the shortcode (<code>[helpful]</code>).', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
	</table>

	<hr />

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="helpful_widget"><?php _ex( 'Dashboard widget', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_widget') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_widget" type="checkbox" name="helpful_widget" <?php echo $checked; ?> /> <?php _ex( 'hide', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Hide the Helpful dashboard widget.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
    <tr valign="top">
			<th scope="row"><label for="helpful_widget_pro"><?php _ex( 'Most helpful', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_widget_pro') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_widget_pro" type="checkbox" name="helpful_widget_pro" <?php echo $checked; ?> /> <?php _ex( 'show', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Show most helpful entries.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
    <tr valign="top">
			<th scope="row"><label for="helpful_widget_contra"><?php _ex( 'Least helpful', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_widget_contra') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_widget_contra" type="checkbox" name="helpful_widget_contra" <?php echo $checked; ?> /> <?php _ex( 'show', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Show least helpful entries.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
    <tr valign="top">
			<th scope="row"><label for="helpful_widget_pro_recent"><?php _ex( 'Recently helpful', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_widget_pro_recent') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_widget_pro_recent" type="checkbox" name="helpful_widget_pro_recent" <?php echo $checked; ?> /> <?php _ex( 'show', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Show recently helpful entries.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
    <tr valign="top">
			<th scope="row"><label for="helpful_widget_contra_recent"><?php _ex( 'Recently unhelpful', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $checked = ( get_option('helpful_widget_contra_recent') ? 'checked="checked"' : '' ); ?>
				<label>
					<input id="helpful_widget_contra_recent" type="checkbox" name="helpful_widget_contra_recent" <?php echo $checked; ?> /> <?php _ex( 'show', 'label', 'helpful' ); ?>
				</label>
				<p class="description"><?php _ex( 'Show recently unhelpful entries.', 'option info', 'helpful' ); ?></p>
			</td>
		</tr>
    <tr valign="top">
			<th scope="row"><label for="helpful_widget_amount"><?php _ex( 'Number of entries', 'option name', 'helpful' ); ?></label></th>
			<td>
				<?php $number = get_option('helpful_widget_amount') ? get_option('helpful_widget_amount') : 5; ?>
				<label>
					<input id="helpful_widget_amount" type="number" name="helpful_widget_amount" value="<?php echo $number; ?>" min="1" />
				</label>
			</td>
		</tr>
	</table>

	<hr />

	<?php submit_button(); ?>

</form>

<?php endif; ?>
