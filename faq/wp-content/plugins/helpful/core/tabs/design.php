<?php
/** 
 * Tab: Design Options
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

global $helpful;

if( $helpful['tab'] == 'design' ) : 

?>

<h3><?php _ex('Design', 'tab name', 'helpful'); ?></h3>

<p><?php _ex('In this section, you can change settings for the design.', 'tab description', 'helpful'); ?></p>

<hr />

<form method="post" action="options.php">

	<?php settings_fields( 'helpful-design-settings-group' ); ?>
	<?php do_settings_sections( 'helpful-design-settings-group' ); ?>
	
	<table class="form-table">
		<tr valign="top">
			<th scope="row">
				<label for="helpful_theme">
					<?php _ex('Theme', 'option name', 'helpful'); ?>
				</label>
			</th>
			<td>
				<label class="helpful-select">
					<select id="helpful_theme" name="helpful_theme" class="regular-text">
						
						<?php $themes = apply_filters( 'helpful-themes', $themes ); ?>
						
						<?php if( $themes ) : foreach( $themes as $id => $label ) : ?>
						
							<?php $selected = ( $id == get_option('helpful_theme') ) ? 'selected="selected"' : ''; ?>
						
							<option value="<?php echo $id; ?>" <?php echo $selected; ?>>
								<?php echo $label; ?>
							</option>
						
						<?php endforeach; endif; ?>	
						
					</select>
				</label>
				
				<div id="theme-preview">
					
					<div id="theme-preview-device">
					
						<div id="hf-prev" class="helpful helpful-theme-base">		
							<div class="helpful-heading">
								<?php echo get_option( 'helpful_heading' ); ?>
							</div>
							<div class="helpful-content">
								<?php echo get_option( 'helpful_content' ); ?>
							</div>
							<div class="helpful-controls">
								<div class="helpful-pro" data-id="131" data-user="3b9790e04b447d819c209024eb0357ed" data-pro="1" data-contra="0">
									<?php echo get_option( 'helpful_pro' ); ?> <span class="counter">0</span>
								</div>		
								<div class="helpful-con" data-id="131" data-user="3b9790e04b447d819c209024eb0357ed" data-pro="0" data-contra="1">
									<?php echo get_option( 'helpful_contra' ); ?> <span class="counter">0</span>
								</div>	
							</div>	
						</div>
						
					</div>
					
					<div id="theme-preview-controls">
						<span class="dashicons dashicons-laptop l"></span>
						<span class="dashicons dashicons-tablet t"></span>
						<span class="dashicons dashicons-smartphone m"></span>
						<span class="dashicons dashicons-no-alt c"></span>
					</div>
					
				</div>
					
				<script>
					jQuery(document).ready( function() {
						jQuery('#helpful_theme').on( 'change', function() {
							var ThemePreview = jQuery('#theme-preview');
							var ThisValue = jQuery(this).val();
							if( ThisValue == 'theme' ) {
								ThemePreview.slideUp();
							} else {
								jQuery('#theme-preview #hf-prev').removeClass().addClass('helpful helpful-theme-' + ThisValue);
								jQuery('#theme-preview-device').css({'width':'100%'}).removeClass().addClass('hfd');
								ThemePreview.slideDown();
							}
						});
						jQuery('#theme-preview-controls .l').on( 'click', function( event ) {
							event.preventDefault();
							jQuery('#theme-preview-device').animate({'width':'100%'}).removeClass().addClass('hfd');
						});
						jQuery('#theme-preview-controls .t').on( 'click', function( event ) {
							event.preventDefault();
							jQuery('#theme-preview-device').animate({'width':'768px'}).removeClass().addClass('hft');
						});
						jQuery('#theme-preview-controls .m').on( 'click', function( event ) {
							event.preventDefault();
							jQuery('#theme-preview-device').animate({'width':'360px'}).removeClass().addClass('hfm');
						});
						jQuery('#theme-preview-controls .c').on( 'click', function( event ) {
							event.preventDefault();
							jQuery('#theme-preview').slideUp();
						});
					});
				</script>
			</td>
		</tr>
	</table>
	
	<hr />
	
	<?php do_action( 'helpful_design_settings' ); ?>

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><label for="helpful_css"><?php _ex('Custom CSS', 'option name', 'helpful'); ?></label></th>
			<td class="helpful-code-editor">
				<textarea id="helpful_css" name="helpful_css" class="regular-text" rows="15"><?php echo get_option('helpful_css'); ?></textarea>
			</td>
		</tr>
	</table>
	
	<hr />

	<?php submit_button(); ?>

</form>

<?php endif; ?>