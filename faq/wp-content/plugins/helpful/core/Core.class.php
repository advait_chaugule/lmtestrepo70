<?php
/**
 * Helpful Core Class
 *
 * @author Devhats
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Init class
new HelpfulCore;

class HelpfulCore
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		global $jal_db_version;
		$jal_db_version = '1.0';

		// Install database table
		register_activation_hook( HELPFUL_FILE, array( $this, 'install' ) );

		// Register menu
		add_action( 'admin_menu', array( $this, 'menu' ) );

		// Register settings for settings page
		add_action( 'plugins_loaded', array( $this, 'settings' ) );

		// Register tab general
		add_action( 'helpful_tabs', array( $this, 'register_tabs' ), 1 );

		// Register tabs content
		add_action( 'helpful_tabs_content', array( $this, 'tabs_content' ) );

		// Add after content
		add_filter( 'the_content', array( $this , 'add_to_content' ) );

		// Enqueue backend Scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'backend_enqueue' ) );

		// Enqueue frontend scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_enqueue' ) );

		// Ajax requests: pro
		add_action( 'wp_ajax_helpful_ajax_pro', array( $this, 'helpful_ajax_pro' ) );
		add_action( 'wp_ajax_nopriv_helpful_ajax_pro', array( $this, 'helpful_ajax_pro' ) );

		// Ajax requests: contra
		add_action( 'wp_ajax_helpful_ajax_contra', array( $this, 'helpful_ajax_contra' ) );
		add_action( 'wp_ajax_nopriv_helpful_ajax_contra', array( $this, 'helpful_ajax_contra' ) );

		// Frontend helpers
		add_filter( 'helpful_helpers', array( $this, 'frontend_helpers' ) );

		// Backend sidebar
		add_action( 'helpful_sidebar', array( $this, 'sidebar' ), 1 );

		// Error Message
		add_filter( 'helpful_error', array( $this, 'error' ), 1 );

		// After Messages
    // Removed 20.09.2018
		// add_filter( 'helpful_after_pro', array( $this, 'after_message' ), 1 );
		// add_filter( 'helpful_after_contra', array( $this, 'after_message' ), 1 );

		// Register columns
		$this->register_columns();

		// Register columns content
		$this->register_columns_content();

		// Register sortable columns
		$this->register_sortable_columns();

		// Make columns values sortable in query
		add_action( 'pre_get_posts', array( $this, 'make_sortable_columns' ), 1 );

		// Shortcode
		add_shortcode( 'helpful', array( $this, 'shortcode' ) );

		// Dashboard widget
		$this->register_widget();

		// Truncate table (uninstall function)
		$this->truncate();
	}

	/**
	 * Register Menu
	 */
	public function menu()
	{
		// add submenu on options
		add_submenu_page(
			'options-general.php',
			__( 'Helpful', 'helpful' ),
			__( 'Helpful', 'helpful' ),
			'manage_options',
			'helpful',
			array( $this, 'admin_page_callback' )
		);
	}

	/**
	 * Register Settings
	 */
	public function settings()
	{
		// general
		register_setting( 'helpful-general-settings-group', 'helpful_credits' );
		register_setting( 'helpful-general-settings-group', 'helpful_hide_in_content' );
		register_setting( 'helpful-general-settings-group', 'helpful_heading' );
		register_setting( 'helpful-general-settings-group', 'helpful_content' );
		register_setting( 'helpful-general-settings-group', 'helpful_pro' );
		register_setting( 'helpful-general-settings-group', 'helpful_exists' );
		register_setting( 'helpful-general-settings-group', 'helpful_post_types' );
		register_setting( 'helpful-general-settings-group', 'helpful_contra' );
		register_setting( 'helpful-general-settings-group', 'helpful_exists_hide' );
		register_setting( 'helpful-general-settings-group', 'helpful_count_hide' );
		register_setting( 'helpful-general-settings-group', 'helpful_widget' );
		register_setting( 'helpful-general-settings-group', 'helpful_widget_amount' );
		register_setting( 'helpful-general-settings-group', 'helpful_widget_pro' );
		register_setting( 'helpful-general-settings-group', 'helpful_widget_contra' );
		register_setting( 'helpful-general-settings-group', 'helpful_widget_pro_recent' );
		register_setting( 'helpful-general-settings-group', 'helpful_widget_contra_recent' );
		register_setting( 'helpful-general-settings-group', 'helpful_column_pro' );
		register_setting( 'helpful-general-settings-group', 'helpful_column_contra' );

		// system
		register_setting( 'helpful-system-settings-group', 'helpful_uninstall' );
	}

	/**
	 * Default options
	 */
	public function default_options( $bool = false )
	{
		if( $bool == true ):

		global $helpful;

		// default options
		update_option(
			'helpful_heading',
			_x( 'Was this post helpful?', 'default headline', 'helpful' )
		);

		update_option(
			'helpful_content',
			_x( 'Let us know if you liked the post. That’s the only way we can improve.', 'default description', 'helpful' )
		);

		update_option(
			'helpful_exists',
			_x( 'You have already voted for this post.', 'already voted', 'helpful' )
		);

		update_option(
			'helpful_success',
			_x( 'Thank you for voting.', 'text after voting', 'helpful' )
		);

		update_option(
			'helpful_error',
			_x( 'Sorry, an error has occurred.', 'error after voting', 'helpful' )
		);

		update_option(
			'helpful_pro',
			_x( 'Yes', 'text pro button', 'helpful' )
		);

		update_option(
			'helpful_contra',
			_x( 'No', 'text contra button', 'helpful' )
		);

		update_option(
			'helpful_column_pro',
			_x( 'Pro', 'column name', 'helpful' )
		);

		update_option(
			'helpful_column_contra',
			_x( 'Contra', 'column name', 'helpful' )
		);

		update_option(
			'helpful_post_types',
			array('post')
		);

		update_option(
			'helpful_count_hide',
			false
		);

		update_option(
			'helpful_credits',
			true
		);

		update_option(
			'helpful_widget',
			false
		);

		update_option(
			'helpful_uninstall',
			false
		);

		endif;
	}

	/**
	 * Admin page callback
	 */
	public function admin_page_callback()
	{
		include( plugin_dir_path( HELPFUL_FILE ) . 'templates/backend.php' );
	}

	/**
	 * Register Tabs
	 * Added in v2.4.3
   * Updated 09.08.2018
   */
	public function register_tabs()
	{
		global $helpful;

    $tabs = array();
    $tabs = array(
      'general' => array(
        'class' => $helpful['tab'] == 'general' ? 'helpful-tab helpful-tab-active' : 'helpful-tab',
        'href'  => '?page=helpful&tab=general',
        'name'  => _x( 'General', 'tab name', 'helpful' ),
      ),
      'design' => array(
        'class' => $helpful['tab'] == 'design' ? 'helpful-tab helpful-tab-active' : 'helpful-tab',
        'href'  => '?page=helpful&tab=design',
        'name'  => _x( 'Design', 'tab name', 'helpful' ),
      ),
      'system' => array(
        'class' => $helpful['tab'] == 'system' ? 'helpful-tab helpful-tab-active' : 'helpful-tab',
        'href'  => '?page=helpful&tab=system',
        'name'  => _x( 'System', 'tab name', 'helpful' ),
      ),
    );

    if( isset( $tabs ) ) {
      foreach( $tabs as $tab ) {
    		echo '<li class="' . $tab['class'] . '">';
    		echo sprintf(
          '<a href="%s" class="helpful-tab-link">%s</a>',
    			$tab['href'],
          $tab['name']
    		);
    		echo '</li>';
      }
    }
	}

	/**
	 * Register tabs content
	 */
	public function tabs_content()
	{
		foreach ( glob( plugin_dir_path( HELPFUL_FILE ) . "core/tabs/*.php" ) as $file ) {
			include_once $file;
		}
	}

	/**
	 * Add after content
	 */
	public function add_to_content($content)
	{
		if( get_option('helpful_hide_in_content') ) {
			return $content;
		}

		// is single
		if( get_option('helpful_post_types') && is_singular() ) {

			global $post;
			$current = get_post_type( $post );

			if( in_array( $current, get_option('helpful_post_types') ) ):

			ob_start();

			// custom frontend exists?
			if( file_exists( get_template_directory() . '/helpful/frontend.php' ) ) {
				include( get_template_directory() . '/helpful/frontend.php' );
			}
			else {
				include( plugin_dir_path( HELPFUL_FILE ) . 'templates/frontend.php' );
			}

			$helpful_poll = ob_get_contents();
			ob_end_clean();

			// add content after post content
			$content = $content . $helpful_poll;

			endif;

		}

		// return the new content
		return $content;
	}

	/**
	 * Add Shortcode
	 * Added in v2.0
   * Updated 11.05.2018
   */
	public function shortcode()
	{
		// is single
		if( get_option('helpful_post_types') && is_singular() ) {

			global $post;
			$current = get_post_type( $post );

			if( in_array( $current, get_option('helpful_post_types') ) ):

			ob_start();

			// custom frontend exists?
			if( file_exists( get_template_directory() . '/helpful/frontend.php' ) ) {
				include( get_template_directory() . '/helpful/frontend.php' );
			}
			else {
				include( plugin_dir_path( HELPFUL_FILE ) . 'templates/frontend.php' );
			}

			$helpful = ob_get_contents();
			ob_end_clean();

			endif;
		}

		// return the new content
		return $helpful;
	}

	/**
	 * Ajax callback: pro
   * Updated: 07.11.2018
	 */
	public function helpful_ajax_pro()
	{
		// like it
		if( $_REQUEST['pro'] == 1 ) {

			// do request if defined
			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

				// set args for insert command
				$args = array(
					'post_id' => $_REQUEST['post_id'],
					'user'		=> $_REQUEST['user'],
					'pro' 		=> $_REQUEST['pro'],
					'contra'	=> $_REQUEST['contra']
				);

				// do and check insert command
        $result = $this->insert( $args );

        if( $result == true ) {
          $after = apply_filters( 'helpful_after_pro', '' );
          echo $this->str_to_helpful( $after, $_REQUEST['post_id'] );
				}

				else {
					echo apply_filters( 'helpful_error', '' );
				}
			}

			// if request not definied do redirect
			else {
				wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
				exit();
			}

		}

    wp_die();
  }

	/**
	 * Ajax callback: contra
   * Updated: 07.11.2018
	 */
	public function helpful_ajax_contra()
	{
    // dont like it
    if( $_REQUEST['contra'] == 1 ) {

      // do requeset if defined
      if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

        // set args for insert command
        $args = array(
          'post_id' => $_REQUEST['post_id'],
          'user'		=> $_REQUEST['user'],
          'pro' 		=> $_REQUEST['pro'],
          'contra'	=> $_REQUEST['contra']
        );

        // do and check insert command
        $result = $this->insert( $args );

        if( $result == true ) {
          $after = apply_filters( 'helpful_after_contra', '' );
          echo $this->str_to_helpful( $after, $_REQUEST['post_id'] );
        }

        else {
          echo apply_filters( 'helpful_error', '' );
        }
      }

      // if request not defined do an redirect
      else {
        wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
        exit();
      }
    }

    wp_die();
  }

	/**
	 * Backend enqueue scripts
   * Updated: 09.08.2018
	 */
	public function backend_enqueue()
	{
		// Register charts
		wp_register_style(
			'helpful-charts',
			plugins_url( 'core/assets/css/widget.css', HELPFUL_FILE )
		);

    // Register scripts
  	wp_register_script(
  		'helpful-widget',
  		plugins_url( 'core/assets/js/widget.js', HELPFUL_FILE ),
  		'',
      true
  	);

		// current screen is helpful
		if( is_helpful() ) {

			// Backend CSS
			wp_enqueue_style (
				'helpful-backend',
				plugins_url( 'core/assets/css/admin.css', HELPFUL_FILE )
			);
		}
	}

	/**
	 * Enqueue scripts
	 */
	public function frontend_enqueue()
	{
		if( get_option('helpful_post_types') && is_singular() ) {

			global $post;
			$current = get_post_type( $post );

			// if $current post type is in the helpful post type array
			if( in_array( $current, get_option('helpful_post_types') ) ):

				// Frontend CSS
				if( !get_option( 'helpful_theme' ) ) {
					update_option('helpful_theme','base');
				}

        // default path css
        $theme_name = 'base';
        $theme_url = plugins_url( 'core/assets/themes/' . get_option( 'helpful_theme' ) . '.css', HELPFUL_FILE );

        // custom path css
        // located wordpress-theme/helpful/theme.css
        if( get_option('helpful_theme') == 'theme' ) {

          // custom css theme
          $file = get_stylesheet_directory_uri() . '/helpful/theme.csss';

          // check if theme exists
          if( file_exists($file) ) {
            $theme_url = $file;
          } else {
            $theme_url = plugins_url( "core/assets/themes/$theme_name.css", HELPFUL_FILE );
          }
        }

				wp_enqueue_style ( 'helpful-frontend', $theme_url, false );

				// Frontend Ajax
				wp_enqueue_script(
					'helpful-frontend',
					plugins_url( 'core/assets/js/frontend.js', HELPFUL_FILE ),
					array('jquery'),
					'1.0',
					true
				);

				// Frontend Ajax (wp)
				wp_localize_script(
					'helpful-frontend',
					'helpful',
					array(
						'ajax_url' => admin_url( 'admin-ajax.php' )
					)
				);

			endif;
		}
	}

	/**
	 * Frontend helpers
	 */
	public function frontend_helpers( $content )
	{
		global $post, $helpful;
		$post_id = $post->ID;

		$credits_text = sprintf(
			'<div class="helpful-credits">' . _x( 'Powered by %s', 'helpful credits', 'helpful' ) . '</div>',
			sprintf(
				'<a href="%s" target="_blank" rel="nofollow">%s</a>',
				'https://devhats.de',
				'Devhats'
			)
		);

    // class
    $class = get_option('helpful_theme') ? 'helpful helpful-theme-' . get_option('helpful_theme') : 'helpful';

    // custom css theme
    $file = get_stylesheet_directory_uri() . '/helpful/theme.csss';

    // check if custom theme exists
    if( get_option('helpful_theme') == 'theme' && !file_exists($file) ) {
      $class = 'helpful helpful-theme-base';
    }

		// options
		$credits = get_option( 'helpful_credits' ) ? $credits_text : '';
		$heading = get_option( 'helpful_heading' );
		$content = get_option( 'helpful_content' );
		$pro = get_option( 'helpful_pro' );
		$contra = get_option( 'helpful_contra' );
		$hide_counts = get_option( 'helpful_count_hide' );

		// md5 IP
		$user = md5($_SERVER['REMOTE_ADDR']);

		// get counts
		$count_pro = get_post_meta( $post_id, 'helpful-pro', true ) ? get_post_meta( $post_id, 'helpful-pro', true ) : 0;
		$count_con = get_post_meta( $post_id, 'helpful-contra', true ) ? get_post_meta( $post_id, 'helpful-contra', true ) : 0;

		$count_pro = get_option( 'helpful_count_hide' ) ? '' : '<span class="counter">' . $count_pro . '</span>';
		$count_con = get_option( 'helpful_count_hide' ) ? '' : '<span class="counter">' . $count_con . '</span>';

		// markup btn pro
		$btn_pro = '<div class="helpful-pro" ';
		$btn_pro .= 'data-id="' . $post_id . '" ';
		$btn_pro .= 'data-user="' . $user . '" ';
		$btn_pro .= 'data-pro="1" ';
		$btn_pro .= 'data-contra="0">';
		$btn_pro .= $pro . $count_pro;
		$btn_pro .= '</div>';

		// markup btn contra
		$btn_con = '<div class="helpful-con" ';
		$btn_con .= 'data-id="' . $post_id . '" ';
		$btn_con .= 'data-user="' . $user . '" ';
		$btn_con .= 'data-pro="0" ';
		$btn_con .= 'data-contra="1">';
		$btn_con .= $contra . $count_con;
		$btn_con .= '</div>';

		// set array for frontend template
		$content = array(
			'class' 		=> $class,
			'credits' 		=> $credits,
			'heading' 		=> $heading,
			'content' 		=> nl2br( $this->str_to_helpful( $content, $post_id ) ),
			'button-pro' 	=> $btn_pro,
			'button-contra' => $btn_con,
			'exists'		=> $this->check( $post_id, $user ),
			'exists-text'	=> nl2br( $this->str_to_helpful( get_option('helpful_exists'), $post_id ) ),
			'exists-hide'	=> ( get_option( 'helpful_exists_hide' ) ? true : false ),
		);

		return $content;
	}

	/**
	 * String to Helpful (Helper)
   * Updated: 07.11.2018
	 */
	public function str_to_helpful( $string, $post_id )
	{
		$pro = get_post_meta( $post_id, 'helpful-pro', true );
		$pro = ( $pro ? $pro : 0 );
		$contra = get_post_meta( $post_id, 'helpful-contra', true );
		$contra = ( $contra ? $contra : 0 );
		$new = str_replace( '{pro}', $pro, $string );
		$new = str_replace( '{contra}', $contra, $new );
    $new = str_replace( '{permalink}', esc_url(get_permalink($post_id)), $new );
		return $new;
	}

	/**
	 * String to Helpful Pro (Helper)
	 */
	public function str_to_pro( $string, $post_id )
	{
		$pro = get_post_meta( $post_id, 'helpful-pro', true );
		$pro = ( $pro ? $pro : 0 );
		return str_replace( '{pro}', $pro, $string );
	}

	/**
	 * String to Helpful Contra (Helper)
	 */
	public function str_to_contra( $string, $post_id )
	{
		$contra = get_post_meta( $post_id, 'helpful-contra', true );
		$contra = ( $contra ? $contra : 0 );
		return str_replace( '{contra}', $contra, $string );
	}

	/**
	 * Install Database Table
	 */
	public function install()
	{
		global $wpdb;
		global $jal_db_version;

		// table name
		$table_name = $wpdb->prefix . 'helpful';

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			user varchar(55) NOT NULL,
			pro mediumint(1) NOT NULL,
			contra mediumint(1) NOT NULL,
			post_id mediumint(9) NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		add_option( 'jal_db_version', $jal_db_version );

		$this->default_options(true);
	}

	/**
	 * Insert row
	 */
	public function insert( $args = null )
	{
		// check args
		if( $args == null) return false;
		if( !$args['user'] ) return false;
		if( !$args['post_id'] ) return false;

		$user = $args['user'];
		$pro = ( $args['pro'] == 1 ? 1 : 0 );
		$contra = ( $args['contra'] == 1 ? 1 : 0 );
		$post_id = $args['post_id'];

		global $wpdb;

		$table_name = $wpdb->prefix . 'helpful';

		$check = $wpdb->get_results("SELECT post_id,user FROM $table_name WHERE user = '$user' AND post_id = $post_id");
    if( $check ) return true;

		$wpdb->insert(
			$table_name,
			array(
				'time' 		=> current_time( 'mysql' ),
				'user' 		=> $user,
				'pro' 		=> $pro,
				'contra' 	=> $contra,
				'post_id' => $post_id,
			)
		);

		// insert pro in post meta
		if( $pro == 1 ):
			$current = (int) get_post_meta( $post_id, 'helpful-pro', true );
			$current = isset($current) ? $current+1 : 1;
			update_post_meta( $post_id, 'helpful-pro', $current );
		endif;

		// insert contra in post meta
		if( $contra == 1 ):
			$current = (int) get_post_meta( $post_id, 'helpful-contra', true );
			$current = isset($current) ? $current+1 : 1;
			update_post_meta( $post_id, 'helpful-contra', $current );
		endif;

		return true;
	}

	/**
	 * Check user
	 */
	public function check( $post_id, $user )
	{
		if( !$post_id ) return false;
		if( !$user ) return false;

		global $wpdb;

		// table
		$table_name = $wpdb->prefix . 'helpful';

		$result = $wpdb->get_row( "SELECT * FROM $table_name WHERE post_id = $post_id AND user = '$user'" );

		if( $result ) return true;

		return $result;
	}

	/**
	 * Truncate Table and delete post metas
	 */
	public function truncate()
	{
		if( get_option('helpful_uninstall') ):

		global $wpdb, $helpful;

		$table_name = $wpdb->prefix . 'helpful';
		$wpdb->query("TRUNCATE TABLE $table_name");
		update_option( 'helpful_uninstall', false );

		$posts = get_posts( array( 'post_type' => 'any', 'posts_per_page' => -1 ) );

		foreach( $posts as $post ) {
			if( get_post_meta( $post->ID, 'helpful-pro' ) ) :
				delete_post_meta( $post->ID, 'helpful-pro' );
			endif;

			if( get_post_meta( $post->ID, 'helpful-contra' ) ) :
				delete_post_meta( $post->ID, 'helpful-contra' );
			endif;
		}

    // removed in 2.4.2
		// $this->default_options(true);

		$helpful['system'] = __( 'The database table was successfully reset!', 'helpful' );

		endif;
	}

	/**
	 * Backend informations container
	 */
	public function sidebar()
	{
		global $helpful;

		$html  = '<h4>' . _x( 'Links & Support', 'headline sidebar options page', 'helpful' ) . '</h4>';
		$html .= '<p>' . _x( 'You have an question?', 'description sidebar options page', 'helpful' ) . '</p>';
		$html .= '<ul>';

		$html .= sprintf(
			'<li><a href="%s" target="_blank">%s</a></li>',
			'https://wordpress.org/plugins/helpful/#developers',
			_x( 'Changelogs', 'link text sidebar options page', 'helpful' )
		);

		$html .= sprintf(
			'<li><a href="%s" target="_blank">%s</a></li>',
			'https://wordpress.org/support/plugin/helpful',
			_x( 'Support', 'link text sidebar options page', 'helpful' )
		);

		$html .= sprintf(
			'<li><a href="%s" target="_blank">%s</a></li>',
			'https://wordpress.org/support/plugin/helpful/reviews/#new-post',
			_x( 'Rate this plugin', 'link text sidebar options page', 'helpful' )
		);

		echo $html;
	}

	/**
	 * Register widget
	 */
	public function register_widget()
	{
		if( !get_option('helpful_widget') ) {
			add_action( 'wp_dashboard_setup', array( $this, 'widget' ), 1 );
		}
	}

	/**
	 * Widget
	 */
	public function widget()
	{
		global $wp_meta_boxes;

		wp_add_dashboard_widget(
			'helpful_widget',
			_x( 'Helpful', 'headline dashboard widget', 'helpful' ),
			array( $this, 'widget_callback'),
      null,
      array( '__block_editor_compatible_meta_box' => false )
		);

		$dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

		$helpful_widget = array( 'helpful_widget' => $dashboard['helpful_widget'] );

		unset( $dashboard['helpful_widget'] );

		$sorted_dashboard = array_merge( $helpful_widget, $dashboard );

		$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
	}

	/**
	 * Widget callback
	 */
	public function widget_callback()
	{
		global $post,$wpdb,$helpful;

		wp_enqueue_style('helpful-charts');
		wp_enqueue_script('helpful-widget');

		$html = '';
		$url  = admin_url('options-general.php?page=helpful');
		$post_types = get_option('helpful_post_types');
    $number = get_option('helpful_widget_amount') ? get_option('helpful_widget_amount') : 5;

		$table_name = $wpdb->prefix . 'helpful';

		// Pros
		$p = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name WHERE pro = 1" );

		// Contras
		$c = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name WHERE contra = 1" );

    // sum
    $sum = $c + $p;

    // pro percentage
    if( $p == 0 ) {
      $pp = 0;
    } else {
      $pp = ( $p / $sum ) * 100;
    }

    // contra percentage
    if( $c == 0 ) {
      $cc = 0;
    } else {
      $cc = ( $c / $sum ) * 100;
    }

		// Pro Counter
		$html .= '<div class="helpful-counter-pro">';
		$html .= sprintf( '<span>%s <small>(%s%%)</small></span>', $p, round($pp,2) );
    $html .= '<div class="helpful-counter-info">' . get_option('helpful_column_pro') . '</div>';
		$html .= '</div>';

		// Contra Counter
		$html .= '<div class="helpful-counter-contra">';
		$html .= sprintf( '<span>%s <small>(%s%%)</small></span>', $c, round($cc,2) );
    $html .= '<div class="helpful-counter-info">' . get_option('helpful_column_contra') . '</div>';
		$html .= '</div>';

    $html .= '<hr />';

    // most helpful posts
    if( get_option('helpful_widget_pro') ) {

      $html .= '<div>';
      $html .= '<strong>' . _x('Most helpful','widget headline','helpful') . '</strong>';

      $pro_query = new WP_Query(array(
        'post_type' => 'any',
        'posts_per_page' => $number,
        'meta_key' => 'helpful-pro',
        'orderby' => array( 'meta_value_num' => 'DESC' ),
      ));

      if( $pro_query->have_posts() ) {

        $html .= '<ul>';

        while( $pro_query->have_posts() ) { $pro_query->the_post();
          $html .= sprintf(
            '<li><a href="%s">%s</a> <span>(%s)</span></li>',
            get_the_permalink(),
            $this->trimtext(get_the_title(),0,45),
            get_post_meta( get_the_ID(), 'helpful-pro', true )
          );
        } wp_reset_postdata();

        $html .= '</ul>';

      } else {
        $html .= '<p>' . __('No entries found.','helpful') . '</p>';
      }

      $html .= '</div>';
      $html .= '<hr />';
    }

    // least helpful posts
    if( get_option('helpful_widget_contra') ) {

      $html .= '<div>';
      $html .= '<strong>' . _x('Least helpful','widget headline','helpful') . '</strong>';

      $pro_query = new WP_Query(array(
        'post_type' => 'any',
        'posts_per_page' => $number,
        'meta_key' => 'helpful-contra',
        'orderby' => array( 'meta_value_num' => 'DESC' ),
      ));

      if( $pro_query->have_posts() ) {

        $html .= '<ul>';

        while( $pro_query->have_posts() ) { $pro_query->the_post();
          $html .= sprintf(
            '<li><a href="%s">%s</a> <span>(%s)</span></li>',
            get_the_permalink(),
            $this->trimtext(get_the_title(),0,45),
            get_post_meta( get_the_ID(), 'helpful-contra', true )
          );
        } wp_reset_postdata();

        $html .= '</ul>';

      } else {
        $html .= '<p>' . __('No entries found.','helpful') . '</p>';
      }

      $html .= '</div>';
      $html .= '<hr />';
    }

    // most helpful recent posts
    if( get_option('helpful_widget_contra_recent') ) {

      // results
      $recent_cons = $wpdb->get_results("SELECT post_id,time FROM $table_name WHERE pro = 1 ORDER BY time DESC LIMIT $number");

      $html .= '<div>';
      $html .= '<strong>' . _x('Recently helpful','widget headline','helpful') . '</strong>';

      if( isset( $recent_cons ) ) {

      $html .= '<ul>';

        foreach( $recent_cons as $p ) {

          $time = strtotime($p->time);

          $html .= sprintf(
            '<li><a href="%s">%s</a> <span>(%s)</span></li>',
            get_the_permalink($p->post_id),
            $this->trimtext(get_the_title($p->post_id),0,45),
            date( get_option('date_format'), $time )
          );
        }

      $html .= '</ul>';

      } else {
        $html .= '<p>' . __('No entries found.','helpful') . '</p>';
      }

      $html .= '</div>';
      $html .= '<hr />';
    }

    // least helpful recent posts
    if( get_option('helpful_widget_contra_recent') ) {

      // results
      $recent_cons = $wpdb->get_results("SELECT post_id,time FROM $table_name WHERE contra = 1 ORDER BY time DESC LIMIT $number");

      $html .= '<div>';
      $html .= '<strong>' . _x('Recently unhelpful','widget headline','helpful') . '</strong>';

      if( isset( $recent_cons ) ) {

      $html .= '<ul>';

        foreach( $recent_cons as $p ) {

          $time = strtotime($p->time);

          $html .= sprintf(
            '<li><a href="%s">%s</a> <span>(%s)</span></li>',
            get_the_permalink($p->post_id),
            $this->trimtext(get_the_title($p->post_id),0,45),
            date( get_option('date_format'), $time )
          );
        }

      $html .= '</ul>';

      } else {
        $html .= '<p>' . __('No entries found.','helpful') . '</p>';
      }

      $html .= '</div>';
      $html .= '<hr />';
    }

    $html .= '<div class="helpful-footer">';

		// Credits Link
		if( get_option( 'helpful_credits' ) ):
		$html .= sprintf(
			'<div class="helpful-credits">%s</div>',
			'<a href="https://devhats.de" target="_blank" rel="nofollow">Devhats</a>'
		);
		endif;

		// Settings Link
		$html .= '<div class="helpful-settings">';
		$html .= '<a href="' . $url . '" title="' . _x( 'Settings', 'link title dashboard widget', 'helpful' ) . '">';
		$html .= '<span class="dashicons dashicons-admin-generic"></span>';
		$html .= '</a>';
		$html .= '</div>';

    $html .= '</div>';

		echo $html;
	}

	/**
	 * Register columns
	 */
	public function register_columns()
	{
		$post_types = get_option('helpful_post_types');

		if( $post_types ) {

			foreach( $post_types as $post_type ) {
				add_filter( 'manage_edit-' . $post_type . '_columns', array( $this, 'columns' ), 10 );
			}
		}
	}

	/**
	 * Columns
	 */
	public function columns( $defaults )
	{
		global $helpful;

		$columns = array();
		foreach ($defaults as $key => $value) {
			$columns[$key] = $value;
			if ( $key == 'title' ) {
				$columns['helpful-pro'] = get_option('helpful_column_pro') ? get_option('helpful_column_pro') : _x( 'Pro', 'column name', 'helpful' );
				$columns['helpful-contra'] = get_option('helpful_column_contra') ? get_option('helpful_column_contra') : _x( 'Contra', 'column name', 'helpful' );
			}
		}
    	return $columns;
	}

	/**
	 * Register columns content
	 */
	public function register_columns_content()
	{
		$post_types = get_option('helpful_post_types');

		if( $post_types ) {

			foreach( $post_types as $post_type ) {
				add_action( 'manage_' . $post_type . '_posts_custom_column', array( $this, 'columns_content' ), 10, 2 );
			}
		}
	}

	/**
	 * Columns content
	 */
	public function columns_content( $column_name, $post_id )
	{
		if ( 'helpful-pro' == $column_name ) {
			$pros = get_post_meta( $post_id, 'helpful-pro', true );
			echo intval( $pros );
		}

		if ( 'helpful-contra' == $column_name ) {
			$cons = get_post_meta($post_id, 'helpful-contra', true );
			echo intval( $cons );
		}
	}

	/**
	 * Register sortable columns
	 */
	public function register_sortable_columns()
	{
		$post_types = get_option('helpful_post_types');
		if( $post_types ) {
			foreach( $post_types as $post_type ) {
				add_filter( 'manage_edit-' . $post_type . '_sortable_columns', array( $this, 'sortable_columns' ) );
			}
		}
	}

	/**
	 * Sortable columns
	 */
	public function sortable_columns( $sortable_columns )
	{
		$sortable_columns[ 'helpful-pro' ] = 'helpful-pro';
   	$sortable_columns[ 'helpful-contra' ] = 'helpful-contra';
		return $sortable_columns;
	}

	/**
	 * Make columns values sortable in query
	 */
	public function make_sortable_columns( $query )
	{
		if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {

			switch( $orderby ) {
				case 'helpful-pro':
				$query->set( 'meta_key', 'helpful-pro' );
				$query->set( 'orderby', 'meta_value' );
        break;
				case 'helpful-contra':
				$query->set( 'meta_key', 'helpful-contra' );
				$query->set( 'orderby', 'meta_value' );
        break;
      }
		}
	}

	/**
	 * After Message (old)
   * Updated 20.09.2018
	 */
	public function after_message( $value )
	{
		global $helpful;
		$value = __( 'Thank you for voting.', 'helpful' );
		return $value;
	}

	/**
	 * Error
	 */
	public function error()
	{
		global $helpful;
		echo __( 'Sorry, an error has occurred.', 'helpful' );
	}

  /**
   * Trim Text
   */
  public function trimtext($text, $start, $length) {
    if( strlen($text) > $length ) {
      return substr($text, $start, $length) . '...';
    } else {
      return substr($text, $start, $length);
    }
  }
}
