=== Helpful ===
Contributors: devhats, pixelbart
Donate link: https://www.paypal.me/834rd
Tags: helpful, poll, feedback, reviews, vote, review, voting
Requires at least: 4.6
Tested up to: 5.0
Stable tag: 2.4.7
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Add a fancy feedback form under your posts or post types and ask your visitors a question. Give them the ability to vote with yes or no.

== Description ==

Add a fancy feedback form under your posts or post types and ask your visitors a simple question: Was this helpful?
The plugin give them the ability to vote with yes or no. If it is not enough, you can look in your post list to get
an overview about your votes (pros and cons). With the integrated dashboard widget you only need to login, to find
what you need. Simply change your form theme or add your own css in the options.

Languages: English, German
Demo (German): https://klakki.me/

**Features**

1. fully changeable texts and questions
1. custom post type support
1. disable for users who already voted
1. dashboard statistics
1. page, cpt overview statistics
1. custom css
1. 3 additional themes
1. custom feedback box after vote

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/helpful` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Helpful screen to configure the

== Frequently Asked Questions ==

= Can i show votes after voting? =

You can. Simply use {pro} and {contra} in your texts to show the vote number.

= Can i use a custom css file? =

You can. Choose `theme` as theme in the helpful options. Then create an `helpful` folder and an `theme.css` file in it. Fill the `theme.css` file with your CSS and you're done.

== Changelog ==

= 2.4.7 =
* Fixed frontend scripts

= 2.4.6 =
* Fixed frontend scripts

= 2.4.5 =
* Tested with WordPress 5.0
* Fixed a issue in after message
* Fixed translation in dashboard widget (now uses the columns translation)
* {permalink} added for outputting the permalink

= 2.4.4 =
* WPML Support

= 2.4.3 =
* Cleaned code
* Added custom theme
* Fixed themes with border-box
* Fixed undefined variables
* Using codemirror from wordpress core
* Fixed widget css (after removing credits)

= 2.4.2 =
* Sidebar voting link added
* Settings no longer reset (only votes)
* Recently helpful and unhelpful entries (widget)
* Number of entries (widget)

= 2.4.1 =
* Fixed column option
* Accurate percentages in the widget

= 2.4.0 =
* Better but simple statistics (thx to @caspero and @anefarious1)
* Improvements (thx to @faterson)
* Bug fixes

= 2.3.2 =
* Fixed Shortcode

= 2.3.1 =
* Fixed admin enqueue (codemirror)

= 2.3 =
* Contact Form 7 Support
* Code optimization

= 2.2 =
* Refreshed languages
* Changed default language to en_US
* Added new de_DE and en_US language files
* Code optimization

= 2.1 =
* Refreshed languages

= 2.0 =
* Converted pro to free version
* Added shortcode `[helpful]`
* Added option, to hide helpful in content for better use with shortcode

= 1.1 =
* 4.9 Tested and fixed

= 1.0.4 =
* Fixed a bug within post metas

= 1.0.2 =
* Fixed a bug in dashboard widget

= 1.0.1 =
* WordPress 4.8 Tested

= 1.0.0 =
* Release
