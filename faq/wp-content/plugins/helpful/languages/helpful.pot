#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Helpful\n"
"POT-Creation-Date: 2018-01-15 00:03+0100\n"
"PO-Revision-Date: 2018-01-15 00:02+0100\n"
"Last-Translator: Kevin Pliester <kevin@devhats.de>\n"
"Language-Team: Kevin Pliester <kevin@devhats.de>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.13\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: helpful.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#. Plugin Name of the plugin/theme
#: core/Core.class.php:99 core/Core.class.php:100 templates/backend.php:12
msgid "Helpful"
msgstr ""

#: core/Core.class.php:143
msgctxt "default headline"
msgid "Was this post helpful?"
msgstr ""

#: core/Core.class.php:148
msgctxt "default description"
msgid ""
"Let us know, if you liked the post. Only in this way, we can improve us."
msgstr ""

#: core/Core.class.php:153
msgctxt "already voted"
msgid "You have already voted for this post."
msgstr ""

#: core/Core.class.php:158
msgctxt "text after voting"
msgid "Thank you for voting."
msgstr ""

#: core/Core.class.php:163
msgctxt "error after voting"
msgid "Sorry, an error has occurred."
msgstr ""

#: core/Core.class.php:168
msgctxt "text pro button"
msgid "Yes"
msgstr ""

#: core/Core.class.php:173
msgctxt "text contra button"
msgid "No"
msgstr ""

#: core/Core.class.php:178 core/Core.class.php:869
msgctxt "column name"
msgid "Pro"
msgstr ""

#: core/Core.class.php:183 core/Core.class.php:870
msgctxt "column name"
msgid "Contra"
msgstr ""

#: core/Core.class.php:234 core/tabs/general.php:18
msgctxt "tab name"
msgid "General"
msgstr ""

#: core/Core.class.php:251 core/tabs/system.php:15
msgctxt "tab name"
msgid "System"
msgstr ""

#: core/Core.class.php:502
#, php-format
msgctxt "helpful credits"
msgid "Powered by %s"
msgstr ""

#: core/Core.class.php:730
msgid "The database table was successfully reset!"
msgstr ""

#: core/Core.class.php:742
msgctxt "headline sidebar options page"
msgid "Links & Support"
msgstr ""

#: core/Core.class.php:743
msgctxt "description sidebar options page"
msgid "You have an question?"
msgstr ""

#: core/Core.class.php:749
msgctxt "link text sidebar options page"
msgid "Changelogs"
msgstr ""

#: core/Core.class.php:755
msgctxt "link text sidebar options page"
msgid "Support"
msgstr ""

#: core/Core.class.php:779
msgctxt "headline dashboard widget"
msgid "Helpful Stats"
msgstr ""

#: core/Core.class.php:835
msgctxt "link title dashboard widget"
msgid "Settings"
msgstr ""

#: core/Core.class.php:956
msgid "Thank you for voting."
msgstr ""

#: core/Core.class.php:966
msgid "Sorry, an error has occurred."
msgstr ""

#: core/Pro.class.php:65 core/tabs/design.php:15
msgctxt "tab name"
msgid "Design"
msgstr ""

#: core/Pro.class.php:96
msgctxt "message after a positive voting"
msgid "After voting (pro)"
msgstr ""

#: core/Pro.class.php:100
msgid ""
"The text that is displayed, after a positive vote (shortcodes <b>without "
"Ajax</b> are also possible!)"
msgstr ""

#: core/Pro.class.php:107
msgctxt "message after a negative voting"
msgid "After voting (contra)"
msgstr ""

#: core/Pro.class.php:111
msgid ""
"The text that is displayed, after a negative vote (shortcodes <b>without "
"Ajax</b> are also possible!)"
msgstr ""

#: core/Pro.class.php:144
msgctxt "theme name"
msgid "Base"
msgstr ""

#: core/Pro.class.php:145
msgctxt "theme name"
msgid "Dark"
msgstr ""

#: core/Pro.class.php:146
msgctxt "theme name"
msgid "Minimal"
msgstr ""

#: core/Pro.class.php:147
msgctxt "theme name"
msgid "Flat"
msgstr ""

#: core/Pro.class.php:148
msgctxt "theme name"
msgid "Theme"
msgstr ""

#: core/tabs/design.php:17
msgctxt "tab description"
msgid "In this section, you can change settings for the design."
msgstr ""

#: core/tabs/design.php:30
msgctxt "option name"
msgid "Theme"
msgstr ""

#: core/tabs/design.php:125
msgctxt "option name"
msgid "Custom CSS"
msgstr ""

#: core/tabs/general.php:20
msgctxt "tab description"
msgid ""
"In this area, you can change general settings. You can use <code>{pro}</"
"code> for outputting positive and <code>{contra}</code> for negative votes."
msgstr ""

#: core/tabs/general.php:31
msgctxt "option name"
msgid "Headline"
msgstr ""

#: core/tabs/general.php:34
msgctxt "option info"
msgid "Here you can define your own headline."
msgstr ""

#: core/tabs/general.php:38
msgctxt "option name"
msgid "Content"
msgstr ""

#: core/tabs/general.php:41
msgctxt "option info"
msgid "Here you can define your own content."
msgstr ""

#: core/tabs/general.php:45 core/tabs/general.php:128
msgctxt "option name"
msgid "Already voted"
msgstr ""

#: core/tabs/general.php:48
msgctxt "option info"
msgid "This text will appear if the user has already voted."
msgstr ""

#: core/tabs/general.php:57
msgctxt "option name"
msgid "Button (pro)"
msgstr ""

#: core/tabs/general.php:60
msgctxt "option info"
msgid "Here you can define your own text for the pro button."
msgstr ""

#: core/tabs/general.php:64
msgctxt "option name"
msgid "Button (contra)"
msgstr ""

#: core/tabs/general.php:67
msgctxt "option info"
msgid "Here you can define your own text for the contra button."
msgstr ""

#: core/tabs/general.php:76
msgctxt "option name"
msgid "Column (pro)"
msgstr ""

#: core/tabs/general.php:79
msgctxt "option info"
msgid ""
"Here you can define your own text for the pro column in the post edit list."
msgstr ""

#: core/tabs/general.php:83
msgctxt "option name"
msgid "Column (contra)"
msgstr ""

#: core/tabs/general.php:86
msgctxt "helpful"
msgid ""
"Here you can define your own text for the contra column in the post edit "
"list."
msgstr ""

#: core/tabs/general.php:97
msgctxt "option name"
msgid "Post types"
msgstr ""

#: core/tabs/general.php:124
msgctxt "option info"
msgid "Here you can choose the post types, where helpful should appear."
msgstr ""

#: core/tabs/general.php:132 core/tabs/general.php:142
#: core/tabs/general.php:162 core/tabs/general.php:177
msgctxt "label"
msgid "hide"
msgstr ""

#: core/tabs/general.php:134
msgctxt "option info"
msgid "Hide Helpful, if the user has already voted."
msgstr ""

#: core/tabs/general.php:138
msgctxt "option name"
msgid "Votes"
msgstr ""

#: core/tabs/general.php:144
msgctxt "option info"
msgid "Hide vote counters."
msgstr ""

#: core/tabs/general.php:148
msgctxt "option name"
msgid "Credits"
msgstr ""

#: core/tabs/general.php:152
msgctxt "label"
msgid "show"
msgstr ""

#: core/tabs/general.php:154
msgctxt "option info"
msgid "Support us and show your visitors that Helpful is from us."
msgstr ""

#: core/tabs/general.php:158
msgctxt "option name"
msgid "Helpful"
msgstr ""

#: core/tabs/general.php:164
msgctxt "option info"
msgid ""
"Hide Helpful in your content. These option is useful by using the shortcode "
"(<code>[helpful]</code>)."
msgstr ""

#: core/tabs/general.php:173
msgctxt "option name"
msgid "Dashboard widget"
msgstr ""

#: core/tabs/general.php:179
msgctxt "option info"
msgid "Hide the Helpful dashboard widget."
msgstr ""

#: core/tabs/system.php:17
msgctxt "tab description"
msgid ""
"Here you can reset Helpful. This affects the database table, all options and "
"also the stored values. The adjustments to the design remain. <b class="
"\"danger\">This process can not be undone!</b>"
msgstr ""

#: core/tabs/system.php:28
msgctxt "option name"
msgid "Reset plugin"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Add a fancy feedback form under your posts or post-types and ask your "
"visitors a question. Give them the abbility to vote with yes or no."
msgstr ""

#. Author of the plugin/theme
msgid "Devhats"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://devhats.de"
msgstr ""
