<?php
/**
 * Helpful Simple Config File
 *
 * @author  Devhats
 * @version 1.0
 */

// Set global variable
global $helpful;
		
// Current tab
$helpful['tab'] = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';