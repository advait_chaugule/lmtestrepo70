<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Handle operations on knowledge base such as adding, deleting and updating FAQ
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Handler {

	// name of FAQ shortcode
	const FAQ_MAIN_PAGE_SHORTCODE_NAME = 'epkb-faq'; // changing this requires db update

	// Prefix for custom post type name associated with given FAQ; this will never change
	const FAQ_POST_TYPE_PREFIX = 'epkb_faq_';  // changing this requires db update
	const FAQ_CATEGORY_TAXONOMY_SUFFIX = '_category';  // changing this requires db update; do not translate
	const FAQ_TAG_TAXONOMY_SUFFIX = '_tag'; // changing this requires db update; do not translate

	/**
	 * Get FAQ slug based on default FAQ name and ID. Default FAQ has slug without ID.
	 *
	 * @param $faq_id
	 *
	 * @return string
	 */
	public static function get_default_slug( $faq_id ) {
		/* translators: do NOT change this translation again. It will break links !!! */
		return sanitize_title_with_dashes( _x( 'FAQ', 'slug', 'echo-knowledge-base' ) . ( $faq_id == EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID ? '' : '-' . $faq_id ) );
	}

	/**
	 * Create a new FAQ Knowledge Base using default configuration when:
	 *  a) plugin is installed and activated
	 *  b) user clicks on 'Add FAQ' button (requires Multiple FAQs add-on)
	 * First default knowledge base has name 'Knowledge Base' with ID 1
	 * Add New FAQ will create FAQ with pre-set name 'Knowledge Base 2' with ID 2 and so on.
	 *
	 * @param int $new_faq_id - ID of the new FAQ
	 * @param $new_faq_main_page_title
	 * @param string $new_faq_main_page_slug
	 *
	 * @return array|WP_Error - the new FAQ configuration or WP_Error
	 */
	public static function add_new_faq_knowledge_base( $new_faq_id, $new_faq_main_page_title, $new_faq_main_page_slug='' ) {

		// use default FAQ configuration for a new FAQ
		$update_faq_config = true;

		// use default FAQ configuration ONLY if none exists
		$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $new_faq_id );
		if ( is_wp_error( $faq_config ) || ! is_array($faq_config) ) {
			$faq_config = EPKB_FAQ_Config_Specs::get_default_faq_config( EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID );
		} else {
			$update_faq_config = false;
		}

		// 1. register custom post type for this knowledge base
		$error = EPKB_FAQ_Articles_CPT_Setup::register_custom_post_type( $faq_config, $new_faq_id );
		if ( is_wp_error( $error ) ) {
			EPKB_Logging::add_log("Could not register post type when adding a new FAQ", $new_faq_id, $error);
			// ignore error and try to continue
		}

		// 2. Add a sample category with two articles in each if no category exists
		$all_faq_terms = EPKB_FAQ_Utilities::get_faq_categories( $new_faq_id );
		if ( empty($all_faq_terms) ) {
			self::create_sample_categories( $new_faq_id );
		}

		// 3. Add FAQ Main Page
		$faq_main_pages = $faq_config['faq_main_pages'];
		if ( empty($faq_main_pages) ) {

			// we add new FAQ Page here so remove hook
			remove_filter('save_post', 'epkb_save_any_page', 10 );
			// do not process FAQ shortcode during FAQ creation
			remove_shortcode( self::FAQ_MAIN_PAGE_SHORTCODE_NAME );

			$my_post = array(
				'post_title'    => $new_faq_main_page_title,
				'post_name'     => $new_faq_main_page_slug,
				'post_type'     => 'page',
				'post_content'  => '[' . self::FAQ_MAIN_PAGE_SHORTCODE_NAME . ' id=' . $new_faq_id . ']',
				'post_status'   => 'publish',
				'comment_status' => 'closed'
				// current user or 'post_author'   => 1,
			);
			$post_id = wp_insert_post( $my_post );
			if ( is_wp_error( $post_id ) || empty($post_id) ) {
				EPKB_Logging::add_log("Could not insert new post", $new_faq_id, $post_id);
			} else {
				$post = WP_Post::get_instance( $post_id );
				$faq_config['faq_name'] = $post->post_title;
				$faq_main_pages[ $post_id ] = $post->post_title;
				$faq_config['faq_main_pages'] = $faq_main_pages;
				$update_faq_config = true;
			}
		}

		// 5. save new/updated FAQ configuration
		if ( $update_faq_config ) {
			$result = epkb_get_instance()->faq_config_obj->update_faq_configuration( $new_faq_id, $faq_config );
			if ( is_wp_error( $result ) ) {
				EPKB_Logging::add_log( "Could not save configuration in the new FAQ", $new_faq_id, $result );
				return $result;
			}
		}

		// let add-ons know we have a new FAQ; does not apply to default FAQ because at that time add-on is not even active
		if ( $new_faq_id !== EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID ) {
			do_action( 'eckb_new_knowledge_base_added', $new_faq_id );
		}

		return $faq_config;
	}

	private static function create_sample_categories( $new_faq_id ) {

		$faq_category_taxonomy_name = self::get_category_taxonomy_name( $new_faq_id );

		// create first category with two articles
		$sample_category_name = __( 'Category 1', 'echo-knowledge-base' );
		$sample_article_title1 = __( 'Question and Answer 1', 'echo-knowledge-base' );
		$sample_article_title2 = __( 'Question and Answer 1', 'echo-knowledge-base' );
		$sample_cat = self::create_sample_category_and_articles( $new_faq_id, $faq_category_taxonomy_name, $sample_category_name, $sample_article_title1, $sample_article_title2 );
		if ( empty($sample_cat) ) {
			return;
		}

		// save category sequence data
		$cat_seq_meta = empty($sample_sub_cat) ? array( $sample_cat['category_id'] => array() )
			: array( $sample_cat['category_id'] => array( $sample_sub_cat['category_id'] => array() ) );
		EPKB_FAQ_Utilities::save_faq_option( $new_faq_id, EPKB_FAQ_Categories_Admin::FAQ_CATEGORIES_SEQ_META, $cat_seq_meta, true );

		// save articles sequence data
		$articles_array = $sample_cat['articles_array'];

		EPKB_FAQ_Utilities::save_faq_option( $new_faq_id, EPKB_FAQ_Articles_Admin::FAQ_ARTICLES_SEQ_META, $articles_array, true );
	}

	private static function create_sample_category_and_articles( $new_faq_id, $faq_category_taxonomy_name, $category_name, $post_title1, $post_title2 ) {

		// insert category
		$term_id_array = wp_insert_term( $category_name, $faq_category_taxonomy_name );
		if ( is_wp_error($term_id_array) ) {
			EPKB_Logging::add_log( 'Failed to insert category for new FAQ. cat name: ' . $category_name . ', taxonomy: ' . $faq_category_taxonomy_name . ' FAQ id: ' . $new_faq_id, $term_id_array );
			return null;
		}
		if ( ! isset($term_id_array['term_id']) ) {
			EPKB_Logging::add_log( 'Failed to insert category for new FAQ. cat name: ' . $category_name . ', taxonomy: ' . $faq_category_taxonomy_name . ' FAQ id: ' . $new_faq_id );
			return null;
		}

		$faq_term_id = $term_id_array['term_id'];

		$my_post1 = array(
			'post_title'    => $post_title1,
			'post_type'     => self::get_post_type( $new_faq_id ),
			'post_content'  => __( 'Coming Soon.', 'echo-knowledge-base' ),
			'post_status'   => 'publish',
			// current user or 'post_author'   => 1,
		);

		// create article under category
		$post1_id = wp_insert_post( $my_post1 );
		if ( is_wp_error( $post1_id ) || empty($post1_id) ) {
			$wp_error = is_wp_error( $post1_id ) ? $post1_id : new WP_Error(124, "post_id is emtpy");
			EPKB_Logging::add_log( 'Cound not insert post for new FAQ', $new_faq_id, $wp_error );
			return null;
		}

		$result = wp_set_object_terms( $post1_id, $faq_term_id, $faq_category_taxonomy_name );
		if ( is_wp_error($result) ) {
			EPKB_Logging::add_log( 'Could not insert default category for new FAQ. post id: ' . $post1_id . ' term id: ' . $faq_term_id . ', taxonomy: ' . $faq_category_taxonomy_name, $new_faq_id, $result );
			return null;
		}

		$my_post2 = array(
				'post_title'    => $post_title2,
				'post_type'     => EPKB_FAQ_Handler::get_post_type( $new_faq_id ),
				'post_content'  => __( 'Coming Soon.', 'echo-knowledge-base' ),
				'post_status'   => 'publish',
			// current user or 'post_author'   => 1,
		);

		// create article under category
		$post2_id = wp_insert_post( $my_post2 );
		if ( is_wp_error( $post2_id ) || empty($post2_id) ) {
			$wp_error = is_wp_error( $post2_id ) ? $post2_id : new WP_Error(124, "post_id is emtpy");
			EPKB_Logging::add_log( 'Cound not insert post for new FAQ', $new_faq_id, $wp_error );
			return null;
		}

		$result = wp_set_object_terms( $post2_id, $faq_term_id, $faq_category_taxonomy_name );
		if ( is_wp_error($result) ) {
			EPKB_Logging::add_log( 'Could not insert default category for new FAQ. post id: ' . $post2_id . ' term id: ' . $faq_term_id . ', taxonomy: ' . $faq_category_taxonomy_name, $new_faq_id, $result );
			return null;
		}

		$articles_array = array( $faq_term_id => array( '0' => $category_name, '1' => '', $post1_id => $post_title1, $post2_id => $post_title2 ) );

		return array( 'category_id' => $faq_term_id, 'articles_array' => $articles_array );
	}

	/**

	/**
	 * Retrieve current FAQ ID based on post_type value in URL based on user request etc.
	 *
	 * @return String | <empty> if not found
	 */
	public static function get_current_faq_id() {
		global $current_screen, $eckb_faq_id;

		if ( ! empty($eckb_faq_id) ) {
			return $eckb_faq_id;
		}

		// 1. retrieve current post being used and if user selected a tab for specific FAQ
		$faq_id = new WP_Error('unknown FAQ ID.');
		$faq_post_type = empty($_REQUEST['post_type']) ? '' : preg_replace('/[^A-Za-z0-9 \-_]/', '', $_REQUEST['post_type']); // sanitize_text_field( $_REQUEST['post_type'] );
		if ( ! empty($faq_post_type) && $faq_post_type != 'page' ) {
			$faq_id = self::get_faq_id_from_post_type( $faq_post_type );
		}

		$epkb_taxonomy = empty($_REQUEST['taxonomy']) ? '' : preg_replace('/[^A-Za-z0-9 \-_]/', '', $_REQUEST['taxonomy']);

		if ( is_wp_error( $faq_id ) && ! empty($epkb_taxonomy) && ! in_array($epkb_taxonomy, array('category', 'tag', 'post_tag')) ) {
			$faq_id = self::get_faq_id_from_category_taxonomy_name( $epkb_taxonomy );
		}

		if ( is_wp_error( $faq_id ) && ! empty($epkb_taxonomy) && ! in_array($epkb_taxonomy, array('category', 'tag', 'post_tag')) ) {
			$faq_id = self::get_faq_id_from_tag_taxonomy_name( $epkb_taxonomy );
		}

		if ( is_wp_error( $faq_id ) && isset($current_screen->post_type) && ! empty($current_screen->post_type) && ! in_array($current_screen->post_type, array('page', 'attachment', 'post') ) ) {
			$faq_id = self::get_faq_id_from_post_type( $current_screen->post_type );
		}

		$epkb_action = empty($_REQUEST['action']) ? '' : preg_replace('/[^A-Za-z0-9 \-_]/', '', $_REQUEST['action']);

		// e.g. when adding category within FAQ article
		if ( is_wp_error( $faq_id ) && ! empty($epkb_action) && strpos( $epkb_action, self::FAQ_POST_TYPE_PREFIX ) !== false ) {
			$found_faq_id = str_replace('add-', '', $epkb_action);
			$found_faq_id = EPKB_FAQ_Handler::get_faq_id_from_category_taxonomy_name( $found_faq_id );
			if ( ! empty($found_faq_id) && ! is_wp_error($found_faq_id) && EPKB_FAQ_Utilities::is_positive_int( $found_faq_id ) ) {
				$faq_id = $found_faq_id;
			}
		}

		$epkb_faq_id = empty($_REQUEST['epkb_faq_id']) ? '' : preg_replace('/\D/', '', $_REQUEST['epkb_faq_id']);
		if ( is_wp_error( $faq_id ) && ! empty($epkb_faq_id) && EPKB_FAQ_Utilities::is_positive_int( $epkb_faq_id )) {
			$faq_id = $epkb_faq_id;
		}

		$epkb_post = empty($_REQUEST['post']) ? '' : preg_replace('/\D/', '', $_REQUEST['post']);

		// when editing article
		if ( is_wp_error( $faq_id ) && ! empty($epkb_action) && $epkb_action == 'edit' && ! empty($epkb_post) && EPKB_FAQ_Utilities::is_positive_int( $epkb_post )) {
			$post = EPKB_FAQ_Utilities::get_faq_post_secure( $epkb_post );
			if ( ! empty($post) ) {
				$faq_id = self::get_faq_id_from_post_type( $post->post_type );
			}
		}

		// REST API
		if ( is_wp_error( $faq_id ) && ! empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/wp-json/wp/') !== false && strpos($_SERVER['REQUEST_URI'], '/' . self::FAQ_POST_TYPE_PREFIX) !== false ) {
			$faq_id = self::get_faq_id_from_rest_endpoint( $_SERVER['REQUEST_URI'] );
		}

		if ( empty($faq_id) || is_wp_error( $faq_id ) || ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			return '';
		}

		// 2. check if the "current id" belongs to one of the existing FAQs
		if ( $faq_id != EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID ) {
			$db_faq_config = new EPKB_FAQ_Config_DB();
			$faq_ids = $db_faq_config->get_faq_ids();
			if ( ! in_array( $faq_id, $faq_ids ) ) {
				EPKB_Logging::add_log("Found current FAQ ID to be unknown", $faq_id);
				return '';
			}
		}

		$eckb_faq_id = $faq_id;

		return $faq_id;
	}

	/**
	 * Is this FAQ post type?
	 *
	 * @param $post_type
	 * @return bool
	 */
	public static function is_faq_post_type( $post_type ) {
		if ( empty($post_type) || ! is_string($post_type)) {
			return false;
		}
		// we are only interested in FAQ articles
		return strncmp($post_type, self::FAQ_POST_TYPE_PREFIX, strlen(self::FAQ_POST_TYPE_PREFIX)) == 0;
	}

	/**
	 * Is this FAQ taxonomy?
	 *
	 * @param $taxonomy
	 * @return bool
	 */
	public static function is_faq_taxonomy( $taxonomy ) {
		if ( empty($taxonomy) || ! is_string($taxonomy) ) {
			return false;
		}
		// we are only interested in FAQ articles
		return strncmp($taxonomy, self::FAQ_POST_TYPE_PREFIX, strlen(self::FAQ_POST_TYPE_PREFIX)) == 0;
	}

	/**
	 * Does request have FAQ taxonomy or post type ?
	 *
	 * @return bool
	 */
	public static function is_faq_request() {

		$faq_post_type = empty($_REQUEST['post_type']) ? '' : preg_replace('/[^A-Za-z0-9 \-_]/', '', $_REQUEST['post_type']);
		$is_faq_post_type = empty($faq_post_type) ? false : self::is_faq_post_type( $faq_post_type );
		if ( $is_faq_post_type ) {
			return true;
		}

		$faq_taxonomy = empty($_REQUEST['taxonomy']) ? '' : preg_replace('/[^A-Za-z0-9 \-_]/', '', $_REQUEST['taxonomy']);
		$is_faq_taxonomy = empty($faq_taxonomy) ? false : self::is_faq_taxonomy( $faq_taxonomy );

		return $is_faq_taxonomy;
	}

	/**
	 * Retrieve current FAQ post type based on post_type value in URL based on user request etc.
	 *
	 * @return String | <empty> if valid post type not found
	 */
	public static function get_current_faq_post_type() {
		$faq_id = self::get_current_faq_id();
		if ( empty( $faq_id ) ) {
			return '';
		}
		return self::get_post_type( $faq_id );
	}

	/**
	 * Retrieve FAQ post type name e.g. ep faq_post_type_1
	 *
	 * @param $faq_id - assumed valid id
	 *
	 * @return string
	 */
	public static function get_post_type( $faq_id ) {
		$faq_id = EPKB_FAQ_Utilities::sanitize_int($faq_id, EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID );
		return self::FAQ_POST_TYPE_PREFIX . $faq_id;
	}

	/**
	 * Retrieve FAQ post type name e.g. <post type>_1
	 *
	 * @return string | <empty> when FAQ id cannot be determined
	 */
	public static function get_post_type2() {
		$faq_id = self::get_current_faq_id();
		if ( empty( $faq_id ) ) {
			return '';
		}
		return self::FAQ_POST_TYPE_PREFIX . $faq_id;
	}

	/**
	 * Return category name e.g. ep faq_post_type_1_category
	 *
	 * @param $faq_id - assumed valid id
	 *
	 * @return string
	 */
	public static function get_category_taxonomy_name( $faq_id ) {
		return self::get_post_type( $faq_id ) . self::FAQ_CATEGORY_TAXONOMY_SUFFIX;
	}

	/**
	 * Return category name e.g. <post type>_1_category
	 *
	 * @return string | <empty> when FAQ id cannot be determined
	 */
	public static function get_category_taxonomy_name2() {
		$faq_id = self::get_current_faq_id();
		if ( empty( $faq_id ) ) {
			return '';
		}
		return self::get_post_type( $faq_id ) . self::FAQ_CATEGORY_TAXONOMY_SUFFIX;
	}

	/**
	 * Return tag name e.g. ep faq_post_type_1_tag
	 *
	 * @param $faq_id - assumed valid id
	 *
	 * @return string
	 */
	public static function get_tag_taxonomy_name( $faq_id ) {
		return self::get_post_type( $faq_id ) . self::FAQ_TAG_TAXONOMY_SUFFIX;
	}

	/**
	 * Retrieve FAQ ID from category taxonomy name
	 *
	 * @param $category_name
	 *
	 * @return int | WP_Error
	 */
	public static function get_faq_id_from_category_taxonomy_name( $category_name ) {
		if ( empty($category_name) || in_array($category_name, array('category', 'tag', 'post_tag')) || ! is_string($category_name) ) {
			return new WP_Error('40', "faq_id not found");
		}

		$faq_id = str_replace( self::FAQ_POST_TYPE_PREFIX, '', $category_name );
		if ( empty($faq_id) ) {
			return new WP_Error('41', "faq_id not found");
		}

		$faq_id = str_replace( self::FAQ_CATEGORY_TAXONOMY_SUFFIX, '', $faq_id );
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			return new WP_Error('42', "faq_id not valid");
		}

		return $faq_id;
	}

	/**
	 * Retrieve FAQ ID from tag taxonomy name
	 *
	 * @param $tag_name
	 *
	 * @return int | WP_Error
	 */
	public static function get_faq_id_from_tag_taxonomy_name( $tag_name ) {
		if ( empty($tag_name) || in_array($tag_name, array('category', 'tag', 'post_tag')) || ! is_string($tag_name) ) {
			return new WP_Error('50', "faq_id not found");
		}

		$faq_id = str_replace( self::FAQ_POST_TYPE_PREFIX, '', $tag_name );
		if ( empty($faq_id) ) {
			return new WP_Error('51', "faq_id not found");
		}

		$faq_id = str_replace( self::FAQ_TAG_TAXONOMY_SUFFIX, '', $faq_id );
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			return new WP_Error('52', "faq_id not valid");
		}

		return $faq_id;
	}

	/**
	 * Retrieve FAQ ID from article type name
	 *
	 * @param String $post_type is post or post type
	 *
	 * @return int | WP_Error if no faq_id found
	 */
	public static function get_faq_id_from_post_type( $post_type ) {
		if ( empty($post_type) || in_array($post_type, array('page', 'attachment', 'post')) || ! is_string($post_type) ) {
			return new WP_Error('35', "faq_id not found");
		}

		$faq_id = str_replace( self::FAQ_POST_TYPE_PREFIX, '', $post_type );
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			return new WP_Error('36', "faq_id not valid");
		}

		return $faq_id;
	}

	/**
	 * Retrieve FAQ ID from REST API
	 * @param $endpoint
	 * @return int|WP_Error
	 */
	public static function get_faq_id_from_rest_endpoint( $endpoint ) {

		$parts = explode('?', $endpoint);
		if ( empty($parts) ) {
			return new WP_Error('37', "faq_id not valid");
		}

		$parts = explode('/', $parts[0]);
		if ( empty($parts) ) {
			return new WP_Error('37', "faq_id not valid");
		}

		$faq_id = new WP_Error('38', "faq_id not valid");
		foreach( $parts as $part ) {
			if ( ! self::is_faq_post_type( $part ) ) {
				continue;
			}

			if ( strpos( $part, self::FAQ_CATEGORY_TAXONOMY_SUFFIX ) !== false ) {
				$faq_id = self::get_faq_id_from_category_taxonomy_name( $part );
				break;
			} else if ( strpos( $part, self::FAQ_TAG_TAXONOMY_SUFFIX ) !== false ) {
				$faq_id = self::get_faq_id_from_tag_taxonomy_name( $part );
				break;
			} else {
				$faq_id = self::get_faq_id_from_post_type( $part );
				break;
			}
		}

		return $faq_id;
	}

	/**
	 * Determine if the current page is FAQ main page i.e. it contains FAQ shortcode and return its FAQ ID if any
	 * @param null $the_post - either pass post to the method or use current post
	 * @return int|null return FAQ ID if current page is FAQ main page otherwise null
	 */
	public static function get_faq_id_from_faq_main_shortcode( $the_post=null ) {
		/** @var $wpdb Wpdb */
		global $wpdb;

		$global_post = empty($GLOBALS['post']) ? '' : $GLOBALS['post'];
		$apost = empty($the_post) ? $global_post : $the_post;
		if ( empty($apost) || ! $apost instanceof WP_Post ) {
			return null;
		}

		// quick first pass
		if ( false === strpos( $apost->post_content, self::FAQ_MAIN_PAGE_SHORTCODE_NAME ) ) {
			return false;
		}

		// ensure WP knows about the shortcode
		add_shortcode( self::FAQ_MAIN_PAGE_SHORTCODE_NAME, array( 'EPKB_FAQ_Layouts_Setup', 'output_faq_page_shortcode' ) );

		// determine whether this page contains this plugin shortcode
		$content = '';
		if ( has_shortcode( $apost->post_content, self::FAQ_MAIN_PAGE_SHORTCODE_NAME ) ) {
			$content = $apost->post_content;
		} else if ( isset($apost->ID) ) {
			$content = $wpdb->get_var( "SELECT meta_value FROM $wpdb->postmeta " .
			                           "WHERE post_id = {$apost->ID} and meta_value LIKE '%%" . self::FAQ_MAIN_PAGE_SHORTCODE_NAME . "%%'" );
		}

		return self::get_faq_id_from_shortcode( $content );
	}

	/**
	 * Retrieve FAQ ID from post content - shortcode
	 *
	 * @param String $content should have the shortcode with FAQ ID
	 *
	 * @return int|null returns FAQ ID if found
	 */
	private static function get_faq_id_from_shortcode( $content ) {

		if ( empty($content) || ! is_string($content) ) {
			return null;
		}

		$start = strpos($content, self::FAQ_MAIN_PAGE_SHORTCODE_NAME);
		if ( empty($start) || $start < 0 ) {
			return null;
		}

		$end = strpos($content, ']', $start);
		if ( empty($start) || $start < 1 ) {
			return null;
		}

		$shortcode = substr($content, $start, $end);
		if ( empty($shortcode) || strlen($shortcode) < strlen(self::FAQ_MAIN_PAGE_SHORTCODE_NAME)) {
			return null;
		}

		preg_match_all('!\d+!', $shortcode, $number);
		$number = empty($number[0][0]) ? 0 : $number[0][0];
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $number ) ) {
			return null;
		}

		return (int)$number;
	}

    /**
     * Return all FAQ Main pages that we know about. Also remove old ones.
     *
     * @param $faq_config
     * @return array a list of FAQ Main Pages titles and links
     */
	public static function get_faq_main_pages( $faq_config) {

		$faq_main_pages = $faq_config['faq_main_pages'];
		$faq_main_pages_info = array();
		foreach ( $faq_main_pages as $post_id => $post_title ) {

			$post_status = get_post_status( $post_id );

			// remove previous page versions
			if ( empty( $post_status ) || $post_status == 'inherit' || $post_status == 'trash' ) {
				unset( $faq_main_pages[ $post_id ] );
				continue;
			}

			$post = get_post( $post_id );
			if ( empty( $post ) || is_array( $post ) || ! $post instanceof WP_Post ) {
				unset( $faq_main_pages[ $post_id ] );
				continue;
			}

			// remove page that does not contain FAQ shortcode any more
			$faq_id = self::get_faq_id_from_faq_main_shortcode( $post );
			if ( empty( $faq_id ) || $faq_id != $faq_config['id'] ) {
				unset( $faq_main_pages[ $post_id ] );
				continue;
			}

			$faq_post_slug = get_page_uri($post_id);  // includes PARENT directory slug
			if ( is_wp_error( $faq_post_slug ) || empty($faq_post_slug) || is_array($faq_post_slug) ) {
				$faq_post_slug = EPKB_FAQ_Handler::get_default_slug( $faq_id );
			}

			$faq_main_pages_info[$post_id] = array( 'post_title' => $post_title, 'post_status' => EPKB_FAQ_Utilities::get_post_status_text( $post_status ), 'post_slug' => urldecode($faq_post_slug) );
		}

		// we need to remove pages that are revisions
		if ( count( $faq_config['faq_main_pages'] ) != count($faq_main_pages) ) {
			$faq_config['faq_main_pages'] = $faq_main_pages;
			epkb_get_instance()->faq_config_obj->update_faq_configuration( $faq_config['id'], $faq_config );
		}

		return $faq_main_pages_info;
	}

    /**
     * Find FAQ Main Page that is not in trash and get its URL.
     *
     * @param $faq_config
     * @return string|<empty>
     */
	public static function get_first_faq_main_page_url( $faq_config ) {
		$first_page_id = '';
		$faq_main_pages = $faq_config['faq_main_pages'];
		foreach ( $faq_main_pages as $post_id => $post_title ) {
			$first_page_id = $post_id;
			break;
		}

		$first_page_url = empty($first_page_id) ? '' : get_permalink( $first_page_id );

		return is_wp_error( $first_page_url ) ? '' : $first_page_url;
	}
}
