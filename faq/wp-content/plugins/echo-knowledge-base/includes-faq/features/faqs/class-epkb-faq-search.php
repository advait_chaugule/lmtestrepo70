<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Search Knowledge Base
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Search {

	public function __construct() {
		add_action( 'wp_ajax_epkb-search-faq', array($this, 'search_faq' ) );
		add_action( 'wp_ajax_nopriv_epkb-search-faq', array($this, 'search_faq' ) );
	}

	/**
	 * Process AJAX search request
	 */
	public function search_faq() {

		// we don't need nonce and permission check here

		$faq_id = EPKB_FAQ_Utilities::sanitize_get_id( $_GET['epkb_faq_id'] );
		if ( is_wp_error( $faq_id ) ) {
			wp_die( json_encode( array( 'status' => 'success', 'search_result' => esc_html__( 'Error occurred. Please try again later.', 'echo-knowledge-base' ) ) ) );
		}

		$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config_or_default( $faq_id );

		// remove question marks
		$search_terms = EPKB_FAQ_Utilities::get( 'search_words' );
		$search_terms = stripslashes( $search_terms );
		$search_terms = str_replace('?', '', $search_terms);
		$search_terms = str_replace( array( "\r", "\n" ), '', $search_terms );

		// require minimum size of search word(s)
		if ( empty($search_terms) ) {
			wp_die( json_encode( array( 'status' => 'success', 'search_result' => esc_html( $faq_config['min_search_word_size_msg'] ) ) ) );
		}

		// search for given keyword(s)
		$result = $this->execute_search( $faq_id, $search_terms  );

		if ( empty($result) ) {
			$search_result = $faq_config['no_results_found'];

		} else {
			// ensure that links have https if the current schema is https
			set_current_screen('front');

			$search_result = '<h3>' . esc_html( $faq_config['search_results_msg'] ) . ' ' . $search_terms . '</h3>';
			$search_result .= '<ul>';

			$title_style = '';
			$icon_style  = '';
			if ( $faq_config['search_box_results_style'] == 'on' ) {
				$title_style = EPKB_FAQ_Utilities::get_inline_style( 'color:: article_font_color' , $faq_config);
				$icon_style = EPKB_FAQ_Utilities::get_inline_style( 'color:: article_icon_color' , $faq_config);
			}

			// display one line for each search result
			foreach( $result as $post ) {

				$article_url = get_permalink( $post->ID );
				if ( empty($article_url) || is_wp_error( $article_url )) {
					continue;
				}

				// linked articles have their own icon
				$article_title_icon = 'ep_font_icon_document';
				if ( has_filter( 'eckb_single_article_filter' ) ) {
					$article_title_icon = apply_filters( 'eckb_article_icon_filter', $article_title_icon, $post->ID );
					$article_title_icon = empty( $article_title_icon ) ? 'fa-file-text-o' : $article_title_icon;
				}

				$search_result .=
					'<li>' .
						'<a href="' .  esc_url( $article_url ) . '" class="epkb-ajax-search" data-faq-article-id="' . $post->ID . '">' .
							'<span class="eckb-article-title" ' . $title_style . '>' .
	                            '<i class="eckb-article-title-icon fa ' . esc_attr($article_title_icon) . ' ' . $icon_style . '"></i>' .
								'<span>' . esc_html($post->post_title) . '</span>' .
							'</span>' .
						'</a>' .
					'</li>';
			}
			$search_result .= '</ul>';
		}

		// we are done here
		wp_die( json_encode( array( 'status' => 'success', 'search_result' => $search_result ) ) );
	}

	/**
	 * Call WP query to get matching terms (any term OR match)
	 *
	 * @param $faq_id
	 * @param $search_terms
	 * @return array
	 */
	private function execute_search( $faq_id, $search_terms ) {

		// add-ons can adjust the search
		if ( has_filter( 'eckb_execute_search_filter' ) ) {
			$result = apply_filters('eckb_execute_search_filter', '', $faq_id, $search_terms );
			if ( is_array($result) ) {
				return $result;
			}
		}

		$result = array();
		$search_params = array(
				's' => $search_terms,
				'post_type' => EPKB_FAQ_Handler::get_post_type( $faq_id ),
				'post_status' => 'publish',      /** only PUBLISHED results */
				'ignore_sticky_posts' => true,  // sticky posts will not show at the top
				'posts_per_page' => 20,         // limit search results
				'no_found_rows' => true,        // query only posts_per_page rather than finding total nof posts for pagination etc.
				'cache_results' => false,       // don't need that for mostly unique searches
				'orderby' => 'relevance'
		);

		$found_posts_obj = new WP_Query( $search_params );
		if ( ! empty($found_posts_obj->posts) ) {
			$result = $found_posts_obj->posts;
			wp_reset_postdata();
		}

		return $result;
	}
}
