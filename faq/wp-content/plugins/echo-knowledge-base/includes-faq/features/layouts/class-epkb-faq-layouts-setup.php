<?php  if ( ! defined( 'ABSPATH' ) ) exit;

class EPKB_FAQ_Layouts_Setup {

	static $demo_mode = false;

	public function __construct() {
		add_filter( 'the_content', array( $this, 'get_faq_page_output_hook' ), 99999 ); // must be high priority
		add_shortcode( EPKB_FAQ_Handler::FAQ_MAIN_PAGE_SHORTCODE_NAME, array( 'EPKB_FAQ_Layouts_Setup', 'output_faq_page_shortcode' ) );
	}

	/**
	 * Current Theme / FAQ template  ==>  the_content()  ==> get article (this method)
	 *
	 * @param $content
	 * @return string
	 */
	public function get_faq_page_output_hook( $content ) {

		// ignore if not post, is archive or current theme with any layout
		// KEEP performance optimized
		$post = empty($GLOBALS['post']) ? '' : $GLOBALS['post'];
		if ( empty($post) || ! $post instanceof WP_Post || empty($post->post_type) || is_archive() || ! is_main_query() ) {
			return $content;
		}

		// continue if NOT FAQ Article URL; KEEP performance optimized
		if ( ! EPKB_FAQ_Handler::is_faq_post_type( $post->post_type ) ) {
			return $content;
		}

		// we have FAQ Article
		$faq_id = EPKB_FAQ_Handler::get_faq_id_from_post_type( $post->post_type );
		if ( is_wp_error($faq_id) ) {
			$faq_id = EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID;
		}

		// initialize FAQ config to be accessible to templates
		$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config_or_default( $faq_id );

		// retrieve article content and features
		$content = EPKB_FAQ_Articles_Setup::get_article_content_and_features( $post, $content, $faq_config );

		return $content;
	}

	/**
	 * Output layout based on FAQ Shortcode.
	 *
	 * @param array $shortcode_attributes are shortcode attributes that the user added with the shortcode
	 * @return string of HTML output replacing the shortcode itself
	 */
	public static function output_faq_page_shortcode( $shortcode_attributes ) {
        $faq_config = self::get_faq_config( $shortcode_attributes );

		return self:: output_main_page( $faq_config );
	}

	/**
	 * Show FAQ Main page i.e. knowledge-base/ url or FAQ Article Page in case of SBL.
	 *
	 * @param bool $is_builder_on
	 * @param null $faq_config
	 * @param array $article_seq
	 * @param array $categories_seq
	 *
	 * @return string
	 */
	public static function output_main_page( $faq_config, $is_builder_on=false, $article_seq=array(), $categories_seq=array() ) {

		// do not display Main Page of Archived FAQ
		if ( $faq_config['id'] !== EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID && EPKB_FAQ_Utilities::is_faq_archived( $faq_config['status'] ) ) {
			return __( 'This knowledge base was archived.', 'echo-knowledge-base' );
		}

		// let layout class display the FAQ main page
		$layout = empty($faq_config['faq_main_page_layout']) ? '' : $faq_config['faq_main_page_layout'];

		$layout_output = '';
		if ( ! self::is_core_layout( $layout ) ) {
			ob_start();
			apply_filters( 'epkb_' . strtolower($layout) . '_layout_output', $faq_config, $is_builder_on, $article_seq, $categories_seq );
			$layout_output = ob_get_clean();

			// use FAQ Layout if the current layout is missing
			$layout = empty($layout_output) ? EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME : $layout;
		}

		// if this is core layout then generate it; if this is add-on layout but it is missing then use FAQ Layout
		if ( empty($layout_output) ) {
			$layout_class_name = 'EPKB_FAQ_Layout_' . ucfirst($layout);
			$layout_class = class_exists($layout_class_name) ? new $layout_class_name() : new EPKB_FAQ_Layout_FAQ();
			ob_start();
			$layout_class->display_faq_main_page( $faq_config, $is_builder_on, $article_seq, $categories_seq );
			$layout_output = ob_get_clean();
		}

		return $layout_output;
	}

	private static function is_core_layout( $layout ) {
		return $layout == EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME;
	}

	/**
	 * Check that the layout exists and is properly configured
	 *
	 * @param array $shortcode_attributes
	 *
	 * @return array return the FAQ configuration
	 */
	private static function get_faq_config( $shortcode_attributes ) {

		$faq_id = empty($shortcode_attributes['id']) ? EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID : $shortcode_attributes['id'] ;
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			EPKB_Logging::add_log( "FAQ ID in shortcode is invalid. Using FAQ ID 1 instead of: ", $faq_id );
			$faq_id = EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID;
		}

		if ( count( $shortcode_attributes ) > 1 ) {
			EPKB_Logging::add_log( "FAQ with ID " . $faq_id . ' has too many shortcode attributes', $shortcode_attributes );
		}

		//retrieve FAQ config
		$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config_or_default( $faq_id );

		return $faq_config;
	}
}
