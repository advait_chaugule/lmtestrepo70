<?php

/**
 *  Outputs the FAQs Layout for knowledge base main page.
 *
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Layout_FAQ extends EPKB_FAQ_Layout {

	/**
	 * Generate content of the FQ main page
	 */
	public function generate_faq_main_page() {

		$class2 = $this->get_css_class( '::width' );

		if ( $this->faq_config['css_version'] == 'css-current' ) {
			$main_container_class = 'epkb-css-full-reset epkb-basic-template';
		} else {
			$main_container_class = 'epkb-basic-template-legacy';
		}	    ?>

		<div id="epkb-main-page-container" class="<?php echo $main_container_class; ?>">
			<div <?php echo $class2; ?>>  <?php

				//  FAQ Search form
				$this->get_search_form();

				//  Knowledge Base Layout
				$style1 = $this->get_inline_style( 'background-color:: background_color' );				?>
				<div id="epkb-content-container" <?php echo $style1; ?> >

					<!--  Main Page Content -->
					<div class="epkb-section-container">
						<?php $this->display_main_page_content(); ?>
					</div>

				</div>
			</div>
		</div>   <?php
	}

	/**
	 * Display FAQ Main Page content
	 */
	private function display_main_page_content() {

		$class0 = $this->get_css_class('::section_box_shadow, epkb-top-category-box');
		$style0 = $this->get_inline_style( 
				'border-radius:: section_border_radius,
				 border-width:: section_border_width,
				 border-color:: section_border_color, ' .
				'background-color:: section_body_background_color, border-style: solid' );

		$class_section_head = $this->get_css_class( 'section-head' . ($this->faq_config[ 'section_divider' ] == 'on' ? ', section_divider' : '' ) );
		$style_section_head = $this->get_inline_style(
					'border-bottom-width:: section_divider_thickness,
					background-color:: section_head_background_color, ' .
					'border-top-left-radius:: section_border_radius,
					border-top-right-radius:: section_border_radius,
					border-bottom-color:: section_divider_color,
					padding-top:: section_head_padding_top,
					padding-bottom:: section_head_padding_bottom,
					padding-left:: section_head_padding_left,
					padding-right:: section_head_padding_right'
		);
		$style3 = $this->get_inline_style(
					'color:: section_head_font_color,
					 text-align::section_head_alignment'
		);
		$style4 = $this->get_inline_style(
					'color:: section_head_description_font_color,
					 text-align::section_head_alignment'
		);
		$style5 = 'border-bottom-width:: section_border_width,
					padding-top::    section_body_padding_top,
					padding-bottom:: section_body_padding_bottom,
					padding-left::   section_body_padding_left,
					padding-right::  section_body_padding_right,';

		if ( $this->faq_config['section_box_height_mode'] == 'section_min_height' ) {
			$style5 .= 'min-height:: section_body_height';
		} else if ( $this->faq_config['section_box_height_mode'] == 'section_fixed_height' ) {
			$style5 .= 'overflow: auto, height:: section_body_height';
		}

		// for each CATEGORY display: a) its articles and b) top-level SUB-CATEGORIES with its articles

		$class1 = $this->get_css_class( ' ::nof_columns, ::section_font_size, eckb-categories-list' );

		$categories_icons = EPKB_FAQ_Utilities::get_faq_option( $this->faq_config['id'], EPKB_FAQ_Icons::CATEGORIES_ICONS, array(), true );
		$header_icon_style = $this->get_inline_style( 'color:: section_head_category_icon_color, font-size:: section_head_category_icon_size' );	?>

		<div <?php echo $class1; //Classes that are controlled by config settings ?> >   <?php

			/** DISPLAY FAQ CATEGORIES */
			foreach ( $this->category_seq_data as $faq_category_id ) {

				$category_name = isset($this->articles_seq_data[$faq_category_id][0]) ?	$this->articles_seq_data[$faq_category_id][0] : '';
				if ( empty($category_name) ) {
					continue;
				}

				$icon_name = empty($categories_icons[$faq_category_id]) ? EPKB_FAQ_Icons::DEFAULT_CATEGORY_ICON : $categories_icons[$faq_category_id];
				$icon_location = empty($this->faq_config['section_head_category_icon_location']) ? '' : $this->faq_config['section_head_category_icon_location'];

				$category_desc = isset($this->articles_seq_data[$faq_category_id][1]) && $this->faq_config['section_desc_text_on'] == 'on' ? $this->articles_seq_data[$faq_category_id][1] : '';
				$faq_category_data = $this->is_builder_on ? 'data-faq-category-id=' . $faq_category_id . ' data-faq-type=category ' : '';  			?>

				<!-- Section Container ( Category FAQ ) -->
				<section <?php echo $class0 . ' ' . $style0; ?> >

					<!-- Section Head -->
					<div <?php echo $class_section_head . ' ' . $style_section_head; ?> >

						<!-- Category Name + Icon -->
						<div class="epkb-category-level-1" <?php echo $faq_category_data . ' ' . $style3; ?> >

							<!-- Icon Top / Left -->	                            <?php
							if ( in_array( $icon_location, array('left', 'top') ) ) {
							    $top_icon_class = $icon_location == 'top' ? 'epkb-top-cat-icon' : '';      ?>
							    <span class="epkb-cat-icon fa <?php echo $top_icon_class . ' ' . $icon_name; ?>" data-faq-category-icon="<?php echo $icon_name; ?>" <?php echo $header_icon_style; ?>></span>     <?php
							}       ?>

							<span class="epkb-cat-name"><?php echo $category_name; ?></span>

							<!-- Icon Right -->     <?php
							if ( $icon_location == 'right' ) {     ?>
							    <span class="epkb-cat-icon fa <?php echo $icon_name; ?>" data-faq-category-icon="<?php echo $icon_name; ?>" <?php echo $header_icon_style; ?>></span>     <?php
							}       ?>

						</div>

						<!-- Category Description -->						<?php
						if ( $category_desc ) {   ?>
						    <p <?php echo $style4; ?> >
						        <?php echo $category_desc; ?>
						    </p>						<?php
						} ?>
					</div>


					<!-- Section Body -->
					<div class="epkb-section-body" <?php echo $this->get_inline_style( $style5 ); ?> >   			<?php

                        /** DISPLAY TOP-CATEGORY ARTICLES LIST */
						$this->display_articles_list( 1, $faq_category_id ); ?>

					</div><!-- Section Body End -->

				</section><!-- Section End -->  <?php

			}  ?>

			</div>       <?php
	}

	/**
	 * Display list of articles that belong to given subcategory
	 *
	 * @param $level
	 * @param $category_id
	 */
	private function display_articles_list( $level, $category_id ) {

		// retrieve articles belonging to given (sub) category if any
		$articles_list = array();
		if ( isset($this->articles_seq_data[$category_id]) ) {
			$articles_list = $this->articles_seq_data[$category_id];
			unset($articles_list[0]);
			unset($articles_list[1]);
		}

		// return if we have no articles and will not show 'Articles coming soon' message
		$articles_coming_soon_msg = $this->faq_config['category_empty_msg'];
		if ( empty($articles_list) && empty($articles_coming_soon_msg) ) {
			return;
		}

		$sub_category_styles = 'padding-left:: article_list_margin';
		if ( $level == 1 ) {
			$data_faq_type = 'article';
			$sub_category_styles .= 'padding-left:: sidebar_article_list_margin,';
		} else if ( $level == 2 ) {
			$data_faq_type = 'sub-article';
		} else {
			$data_faq_type = 'sub-sub-article';
		}

		$style = 'class="' . ( $level == 1 ? 'epkb-main-category ' : '' ) .  'epkb-articles eckb-articles-ordering"';		?>

		<ul <?php echo $style . ' ' . $this->get_inline_style( $sub_category_styles ); ?>> <?php

			if ( empty($articles_list) ) {
				echo '<li class="epkb-articles-coming-soon">' . esc_html( $articles_coming_soon_msg ) . '</li>';
			}

			$article_num = 0;
			$article_data = '';
			$nof_articles_displayed = $this->faq_config['nof_articles_displayed'];
			foreach ( $articles_list as $article_id => $article_title ) {
				$article_num++;
				$hide_class = $article_num > $nof_articles_displayed ? 'epkb-hide-elem' : '';
				if ( $this->is_builder_on ) {
					$article_data = $this->is_builder_on ? 'data-faq-article-id=' . $article_id . ' data-faq-type=' . $data_faq_type : '';
				}

				/** DISPLAY ARTICLE LINK */         ?>
				<li class="epkb-article-level-<?php echo $level . ' ' . $hide_class; ?>" <?php echo $article_data; ?> <?php echo $this->get_inline_style( 'padding-bottom:: article_list_spacing,padding-top::article_list_spacing' ); ?> >   <?php
					$this->single_article_link( $article_title, $article_id ); ?>
				</li> <?php
			}

			// if article list is longer than initial article list size then show expand/collapse message
			if ( count($articles_list) > $nof_articles_displayed ) { ?>
				<span class="epkb-show-all-articles">
					<span class="epkb-show-text">
						<?php echo esc_html( $this->faq_config['show_all_articles_msg'] ) . ' ( ' . ( count($articles_list) - $nof_articles_displayed ); ?> )
					</span>
				<span class="epkb-hide-text epkb-hide-elem"><?php echo esc_html( $this->faq_config['collapse_articles_msg'] ); ?></span> <?php
			}  ?>

		</ul> <?php
	}
}