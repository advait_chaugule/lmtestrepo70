<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Query article data in the database
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Articles_DB {

	// TODO NEXT RELEASE wp_cache_get, wp_cache_set etc. and set_transient/get_transient

	/**
	 * Get PUBLISHED articles related to a given category OR sub-category
	 *
	 * @param $faq_id
	 * @param $sub_or_category_id
	 * @param string $order_by
	 * @param int $nof_articles
	 * @param bool $include_children
	 *
	 * @return array of matching articles or empty array
	 */
	function get_published_articles_by_sub_or_category( $faq_id, $sub_or_category_id, $order_by='date', $nof_articles=200, $include_children=false ) {
		/** @var $wpdb Wpdb */
		global $wpdb;
		
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			EPKB_Logging::add_log( 'Invalid faq id', $faq_id );
			return array();
		}

		if ( ! EPKB_FAQ_Utilities::is_positive_int($sub_or_category_id) ) {
			EPKB_Logging::add_log( 'Invalid category id', $sub_or_category_id );
			return array();
		}

		$order = $order_by == 'title' ? 'ASC' : 'DESC';

		$query_args = array(
			'post_type' => EPKB_FAQ_Handler::get_post_type( $faq_id ),
			'post_status' => 'publish',  // we want only published articles
			'posts_per_page' => $nof_articles,
			'orderby' => $order_by,
			'order'=> $order,
			'tax_query' => array(
				array(
					'taxonomy' => EPKB_FAQ_Handler::get_category_taxonomy_name( $faq_id ),
					'terms' => $sub_or_category_id,
					'include_children' => $include_children
				)
			)
		);

		if ( EPKB_FAQ_Utilities::is_wpml_enabled() ) {
			$order_by = $order_by == 'title' ? 'post_title' : 'post_modified';
			$articles = $wpdb->get_results( " SELECT * " .
			                                " FROM $wpdb->posts p " .
			                                " WHERE p.ID in " .
			                                "   (SELECT object_id FROM $wpdb->term_relationships tr INNER JOIN $wpdb->term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id " .
			                                "    WHERE tt.term_id = " . $sub_or_category_id . " AND tt.taxonomy = '" . EPKB_FAQ_Handler::get_category_taxonomy_name( $faq_id ) . "') " .
			                                "   AND post_type = '" . EPKB_FAQ_Handler::get_post_type( $faq_id ) . "' AND post_status in ('publish')
		                              ORDER BY " . $order_by . ' ' . $order );  // Get only Published articles
			return $articles;
		}

		return get_posts( $query_args );  /** @secure 02.17 */
	}

	/**
	 * Retrieve all FAQ articles but do not count articles in Trash
	 *
	 * @param $faq_id
	 *
	 * @return number of all posts
	 */
	static function get_count_of_all_faq_articles( $faq_id ) {
		/** @var $wpdb Wpdb */
		global $wpdb;

		$faq_id = EPKB_FAQ_Utilities::sanitize_int( $faq_id, EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID );

		// parameters sanitized
		$posts = $wpdb->get_results( " SELECT * " .
									 " FROM $wpdb->posts " . /** @secure 02.17 */
		                             " WHERE post_type = '" . EPKB_FAQ_Handler::get_post_type( $faq_id ) . "' AND post_status in ('publish') ");
		if ( empty( $posts ) || ! is_array( $posts ) ) {
			return 0;
		}

		return empty( $posts ) ? 0 : count( $posts );
	}

	/**
	 * Retrieve all PUBLISHED articles that do not have either category or subcategory
	 *
	 * @param $faq_id
	 *
	 * @return array of posts
	 */
	function get_orphan_published_articles( $faq_id ) {
		/** @var $wpdb Wpdb */
		global $wpdb;

		// sanitize FAQ ID
		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			EPKB_Logging::add_log( 'Invalid faq id', $faq_id );
			return array();
		}

		// parameters sanitized
		$posts = $wpdb->get_results( "SELECT * FROM " .
		                             "   $wpdb->posts p LEFT JOIN " .  /** @secure 02.17 */
	                                 "   (SELECT object_id FROM $wpdb->term_relationships tr INNER JOIN $wpdb->term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id " .
		                                        " WHERE tt.taxonomy = '" . EPKB_FAQ_Handler::get_category_taxonomy_name( $faq_id ) . "') AS ta " .
		                             "ON ta.object_id = p.ID " .
		                             "WHERE post_type = '" . EPKB_FAQ_Handler::get_post_type( $faq_id ) . "' AND object_id IS NULL AND post_status in ('publish') ");  // Get only Published articles

		if ( empty( $posts ) || ! is_array( $posts ) ) {
			return array();
		}

		return $posts;
	}
}