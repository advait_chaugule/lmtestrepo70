<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Register a new CUSTOM POST TYPE + category + tag for a given instance of KNOWLEDGE BASE.
 *
 * This FAQ articles will have their post_type set to this newly registered custom post type.
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Articles_CPT_Setup {

	public function __construct() {
		add_action( 'init', array( $this, 'register_knowledge_base_post_types'), 10 );
	}

	/**
	 * Read configuration and create configured custom post types, each representing a knowledge base
	 */
	public function register_knowledge_base_post_types() {

		if ( ! EPKB_FAQ_Utilities::is_faq_enabled() ) {
			return;
		}

		$current_id = EPKB_FAQ_Handler::get_current_faq_id();
		foreach ( epkb_get_instance()->faq_config_obj->get_faq_configs() as $faq_config ) {

			$result = self::register_custom_post_type( $faq_config, $current_id );
			if ( is_wp_error( $result ) ) {
				EPKB_Logging::add_log("Could not register custom post type.", $faq_config['id'], $result);
			}
		}

		/* flush rules on plugin activation after CPTs were registered
		$is_flush_rewrite_rules = get_option( 'epkb_flush_rewrite_rules' );
		if ( ! empty($is_flush_rewrite_rules) && ! is_wp_error( $is_flush_rewrite_rules ) ) {
			delete_option( 'epkb_flush_rewrite_rules' );
			flush_rewrite_rules( false );
		} */
	}

	/**
	 * Register custom post type, including taxonomies (category, tag) and other constructs.
	 *
	 * @param array $faq_config
	 * @param int|string $current_id
	 * @return bool|WP_Error
	 */
	public static function register_custom_post_type( $faq_config, $current_id ) {

		$faq_id = $faq_config['id'];
		
		// do not register Archived FAQ
		if ( $faq_id !== EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID && EPKB_FAQ_Utilities::is_faq_archived( $faq_config['status'] ) ) {
			return true;
		}
		
		$faq_post_type = EPKB_FAQ_Handler::get_post_type( $faq_id );
		$faq_articles_common_path = empty( $faq_config['faq_articles_common_path'] ) ?
									EPKB_FAQ_Handler::get_default_slug( $faq_id ) : $faq_config['faq_articles_common_path'];

		// determine if this custom post type will be registered for user selected FAQ; if yes make it visible in admin UI
		$current_id = empty($current_id) ? EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID : $current_id;
		$show_post_in_ui = ( $faq_id == $current_id ) || ! is_admin();  // true if front-end (like admin bar)

		// first we need to setup CATEGORY taxonomy so that its rules are above 'attachments' links from its post type

		/** setup Category taxonomy */

		$taxonomy_name = EPKB_FAQ_Handler::get_category_taxonomy_name( $faq_id );
		$labels = array(
				'name'              => _x( 'FAQ Categories', 'taxonomy general name', 'echo-knowledge-base' ),
				'singular_name'     => _x( 'FAQ Category', 'taxonomy singular name', 'echo-knowledge-base' ),
				'search_items'      => __( 'Search FAQ Categories', 'echo-knowledge-base' ),
				'all_items'         => __( 'All FAQ Categories', 'echo-knowledge-base' ),
				'parent_item'       => __( 'Parent FAQ Category', 'echo-knowledge-base' ),
				'parent_item_colon' => __( 'Parent FAQ Category:', 'echo-knowledge-base' ),
				'edit_item'         => __( 'Edit FAQ Category', 'echo-knowledge-base' ),
				'update_item'       => __( 'Update FAQ Category', 'echo-knowledge-base' ),
				'add_new_item'      => __( 'Add New FAQ Category', 'echo-knowledge-base' ),
				'new_item_name'     => __( 'New FAQ Category Name', 'echo-knowledge-base' ),
				'menu_name'         => __( 'FAQ Categories', 'echo-knowledge-base' ),
		);
		$args = array(
				'hierarchical'          => false,
				'labels'                => $labels,
				'show_ui'               => $show_post_in_ui,
				'show_admin_column'     => $show_post_in_ui,
				'show_in_nav_menus'     => $show_post_in_ui,
				'publicly_queryable'    => false,
				'query_var'             => $taxonomy_name,
				'show_in_rest'          => false,
				'rewrite'               => array(
												/* translators: do NOT change this translation again. It will break links !!! */
												'slug'         => $faq_articles_common_path . '/' . _x( 'category', 'taxonomy singular name', 'echo-knowledge-base' ),  // TODO FUTURE overwrite with config
												'with_front'   => false,
												'hierarchical' => false
											),
		);
		$result = register_taxonomy( $taxonomy_name, array( $faq_post_type ), $args );
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		/** setup Tag taxonomy */
		$tag_name = EPKB_FAQ_Handler::get_tag_taxonomy_name( $faq_id );
		$labels = array(
				'name'                       => _x( 'FAQ Tags', 'taxonomy general name', 'echo-knowledge-base' ),
				'singular_name'              => _x( 'FAQ Tag', 'taxonomy singular name', 'echo-knowledge-base' ),
				'search_items'               => __( 'Search FAQ Tags', 'echo-knowledge-base' ),
				'all_items'                  => __( 'All FAQ Tags', 'echo-knowledge-base' ),
				'parent_item'                => __( 'Parent FAQ Tag', 'echo-knowledge-base' ),
				'parent_item_colon'          => __( 'Parent FAQ Tag:', 'echo-knowledge-base' ),
				'edit_item'                  => __( 'Edit FAQ Tag', 'echo-knowledge-base' ),
				'update_item'                => __( 'Update FAQ Tag', 'echo-knowledge-base' ),
				'view_item'                  => __( 'View FAQ Tag', 'echo-knowledge-base' ),
				'separate_items_with_commas' => __( 'Separate FAQ Tags with commas', 'echo-knowledge-base' ),
				'add_or_remove_items'        => __( 'Add or remove FAQ Tags', 'echo-knowledge-base' ),
				'add_new_item'               => __( 'Add New FAQ Tag', 'echo-knowledge-base' ),
				'new_item_name'              => __( 'New FAQ Tag Name', 'echo-knowledge-base' ),
				'menu_name'                  => __( 'FAQ Tags', 'echo-knowledge-base' )
		);
		$args = array(
				'hierarchical'          => false,
				'labels'                => $labels,
				'show_ui'               => $show_post_in_ui,
				'show_admin_column'     => $show_post_in_ui,
				'show_in_nav_menus'     => $show_post_in_ui,
				'publicly_queryable'    => false,
				'show_tagcloud'         => true,
				'query_var'             => $tag_name,
				'show_in_rest'          => false,
				'rewrite'               => array(
												/* translators: do NOT change this translation again. It will break links !!! */
												'slug'         => $faq_articles_common_path . '/' . _x( 'tag', 'taxonomy singular name', 'echo-knowledge-base' ),  // TODO FUTURE override with config
												'with_front'   => false,
												'hierarchical' => false
											),
		);
		$result = register_taxonomy( $tag_name, array( $faq_post_type ), $args );
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		/** setup Custom Post Type */
		$post_type_name = 'FAQ: ' . $faq_config['faq_name'];
		$labels = array(
				'name'               => _x( $post_type_name, 'post type general name', 'echo-knowledge-base' ),
				'singular_name'      => _x( 'FAQ Article', 'post type singular name', 'echo-knowledge-base' ),
				'add_new'            => _x( 'Add New FAQ Article', 'Articles', 'echo-knowledge-base' ),
				'add_new_item'       => __( 'Add New FAQ Article', 'echo-knowledge-base' ),
				'edit_item'          => __( 'Edit FAQ Article', 'echo-knowledge-base' ),
				'new_item'           => __( 'New FAQ Article', 'echo-knowledge-base' ),
				'all_items'          => __( 'All FAQ Articles', 'echo-knowledge-base' ),
				'view_item'          => __( 'View FAQ Article', 'echo-knowledge-base' ),
				'search_items'       => __( 'Search in FAQ Articles', 'echo-knowledge-base' ),
				'not_found'          => __( 'No FAQ Articles found', 'echo-knowledge-base' ),
				'not_found_in_trash' => __( 'No FAQ Articles found in Trash', 'echo-knowledge-base' ),
				'parent_item_colon'  => '',
				'menu_name'          => _x( 'FAQ', 'admin menu', 'echo-knowledge-base' )
		);
		$args = array(
				'labels'             => $labels,
				'public'             => true,
				'show_ui'            => true,
				'show_in_menu'       => $show_post_in_ui,
				'publicly_queryable' => false,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => $faq_articles_common_path, 'with_front' => false ),
				'capability_type'    => 'post',
				'map_meta_cap'       => true,
				'has_archive'        => false,
				'hierarchical'       => false,
				'show_in_rest'       => false,
				'menu_position'      => 5,    // below Posts menu
				'menu_icon'          => 'dashicons-welcome-learn-more',
				'supports'           => array(
											'title',
											'thumbnail',
											'excerpt',
											'revisions',
											'author'
				),
		);
		$result = register_post_type( $faq_post_type, $args );
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		/** tie taxonomies to the post type */

		$result = register_taxonomy_for_object_type( $taxonomy_name, $faq_post_type );
		if ( ! $result ) {
			return new WP_Error( 'register_object_for_tax_failed', "Failed to register taxonomy '$taxonomy_name' for post type '$faq_post_type' for FAQ ID: $faq_id" );
		}

		$result = register_taxonomy_for_object_type( $tag_name, $faq_post_type );
		if ( ! $result ) {
			return new WP_Error( 'register_object_for_tax_failed', "Failed to register taxonomy '$tag_name' for post type '$faq_post_type' for FAQ ID: $faq_id" );
		}

		return true;
	}
}

