<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Query categories data in the database
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Categories_DB {

	/**
	 * Get all top-level categories
	 *
	 * @param $faq_id
	 * @param string $hide_choice - if 'hide_empty' then do not return empty categories
	 *
	 * @return array or empty array on error
	 *
	 */
	function get_top_level_categories( $faq_id, $hide_choice='hide_empty' ) {

		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			EPKB_Logging::add_log( 'Invalid faq id', $faq_id );
			return array();
		}

		$args = array(
				'parent'        => '0',
				'hide_empty'    => $hide_choice === 'hide_empty' // whether to return categories without articles
		);

		$terms = get_terms( EPKB_FAQ_Handler::get_category_taxonomy_name( $faq_id ), $args );
		if ( is_wp_error( $terms ) ) {
			EPKB_Logging::add_log( 'cannot get terms for faq_id', $faq_id, $terms );
			return array();
		} else if ( empty($terms) || ! is_array($terms) ) {
			return array();
		}

		return array_values($terms);   // rearrange array keys
	}

	/**
	 * Get all categories that belong to given parent
	 *
	 * @param $faq_id
	 * @param int $parent_id is parent category we use to find children
	 * @param string $hide_choice
	 *
	 * @return array or empty array on error
	 */
	function get_child_categories( $faq_id, $parent_id, $hide_choice='hide_empty' ) {

		if ( ! EPKB_FAQ_Utilities::is_positive_int( $faq_id ) ) {
			EPKB_Logging::add_log( 'Invalid faq id', $faq_id );
			return array();
		}

		if ( ! EPKB_FAQ_Utilities::is_positive_int( $parent_id ) ) {
			EPKB_Logging::add_log( 'Invalid parent id', $parent_id );
			return array();
		}

		$args = array(
				'child_of'      => $parent_id,
				'parent'        => $parent_id,
				'hide_empty'    => $hide_choice === 'hide_empty'
		);

		$terms = get_terms( EPKB_FAQ_Handler::get_category_taxonomy_name( $faq_id ), $args );
		if ( is_wp_error( $terms ) ) {
			EPKB_Logging::add_log( 'failed to get terms for faq_id: ' . $faq_id . ', parent_id: ' . $parent_id, $terms );
			return array();
		}

		if ( empty( $terms ) || ! is_array( $terms ) ) {
			return array();
		}

		return array_values($terms);
	}
}