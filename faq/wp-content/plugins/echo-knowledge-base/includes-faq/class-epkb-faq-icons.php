<?php

/**
 * Store Icons in an array
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Icons {

	const CATEGORIES_ICONS = 'epkb_categories_icons';
	const DEFAULT_CATEGORY_ICON = 'ep_font_icon_document';

	public static function get_common_icons() {
		return EPKB_Icons::get_common_icons();
	}

	public static function get_other_icons() {
		return EPKB_Icons::get_other_icons();
	}

	public static function get_all_icons(){
		return EPKB_Icons::get_all_icons();
	}

	public static function format_font_awesome_icon_name( $value ){
		return EPKB_Icons::format_font_awesome_icon_name( $value );
	}
}