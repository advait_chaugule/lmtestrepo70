<?php

/**
 * Handle loading EP templates
 
 * @copyright   Copyright (C) 2018, Echo Plugins 
 * Some code adapted from code in EDD/WooCommmerce (Copyright (c) 2017, Pippin Williamson) and WP.
 */
class EPKB_FAQ_Templates {

	public function __construct() {
   		// TODO add_filter( 'template_include', array( __CLASS__, 'template_loader' ) );
	}

	/**
	 * Load article templates. Templates are in the 'templates' folder.
	 *
	 * Templates can be overriden in /theme/knowledgebase/ folder.
	 *
	 * @param mixed $template
	 * @return string
	 */
	public static function template_loader( $template ) {
		/** @var WP_Query $wp_query */
        global $wp_query, $eckb_faq_id, $eckb_is_faq_main_page;

		// ignore non-page/post conditions
        if ( ! self::is_post_page() ) {
            return $template;
        }

		// get current post
		$post = isset($GLOBALS['post']) ? $GLOBALS['post'] : '';
		if ( empty($post) || ! $post instanceof WP_Post ) {
			return $template;
		}

		// ignore search results page
		if ( $wp_query->is_search() ) {
			return $template;
		}

		// is FAQ enabled?
		if ( ! EPKB_FAQ_Utilities::is_faq_enabled() ) {
			return $template;
		}

        // is this FAQ Main Page ?
        $faq_id = EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID;
		$eckb_is_faq_main_page = false;
        $all_faq_configs = epkb_get_instance()->faq_config_obj->get_faq_configs( true );
        foreach ( $all_faq_configs as $one_faq_config ) {
            if ( ! empty($one_faq_config['faq_main_pages']) && is_array($one_faq_config['faq_main_pages']) &&
                 in_array($post->ID, array_keys($one_faq_config['faq_main_pages']) ) ) {
	            $eckb_is_faq_main_page = true;
                $faq_id = $one_faq_config['id'];
                break;  // found matching FAQ Main Page
            }
        }

        // is this FAQ Article Page ?
        if ( ! $eckb_is_faq_main_page ) {
            $faq_id = EPKB_FAQ_Handler::get_faq_id_from_post_type( $post->post_type );
            if ( is_wp_error( $faq_id ) ) {
                return $template;
            }
        }

		$eckb_faq_id = $faq_id;

		// continue only if we are using FAQ templates
		$temp_config = empty($all_faq_configs[$faq_id]) ? array() : $all_faq_configs[$faq_id];
        if ( ! self::is_faq_template_active( $temp_config ) ) {
            return $template;
        }

		// get the layout name
		$layout_config_name = $eckb_is_faq_main_page ? 'faq_main_page_layout' : 'faq_article_page_layout';
		$default_layout_name = EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME;
		$layout_name =  empty($all_faq_configs[$faq_id][$layout_config_name]) ? $default_layout_name : $all_faq_configs[$faq_id][$layout_config_name];

		// locate FAQ template
		$template_name = self::get_template_name( $layout_name );
		if ( empty($template_name) ) {
			return $template;
		}

		// if we can't locate any FAQ template then return the default WP template
		$located_template = self::locate_template( $template_name );
		if ( empty($located_template) ) {
			return $template;
		}

		//$epkb_faq_templates_on = true;

		return $located_template;
	}

	private static function is_faq_template_active( $faq_config=array() ) {

		if ( empty($faq_config) ) {
			$taxonomy = empty($GLOBALS['taxonomy']) ? '' : $GLOBALS['taxonomy'];
			$faq_id = EPKB_FAQ_Handler::get_faq_id_from_category_taxonomy_name( $taxonomy );
			if ( is_wp_error($faq_id) ) {
				return false;
			}

			$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
			if ( is_wp_error($faq_config) ) {
				return false;
			}
		}

		return ! empty($faq_config['templates_for_faq']) && ( $faq_config['templates_for_faq'] == 'faq_templates' );
	}

	/**
	 * Get the default filename for a template.
	 *
	 * @param $layout_name
	 * @return string
	 */
	private static function get_template_name( $layout_name ) {

		$layout_name = strtolower( $layout_name );
		if ( $layout_name == 'article' ) {
			return 'single-article.php';
		} else if ( in_array( $layout_name, array('faq') ) ) {
            return 'layout-' . $layout_name . '.php';
        }

		return '';
	}

	/**
	 * Retrieve the name of the highest priority template file that exists.
	 *
	 * Searches in the STYLESHEETPATH before TEMPLATEPATH so that CHILD THEME which
	 * inherit from a PARENT THEME can just overload one file. If the template is
	 * not found in either of those, it looks in FAQ template folder last
	 *
	 * Taken from bbPress
	 *
	 * @param string|array $template_names Template file(s) to search for, in order.
	 * @return false|string The template filename if one is located.
	 */
	public static function locate_template( $template_names ) {

		// No file found yet
		$located = false;

		// loop through hierarchy of template names
		foreach ( (array) $template_names as $template_name ) {

			// Continue if template is empty
			if ( empty( $template_name ) ) {
				continue;
			}

			// Trim off any slashes from the template name
			$template_name = ltrim( $template_name, '/' );

			// loop through hierarchy of template file locations ( child -> parent -> our theme )
			foreach( self::get_theme_template_paths() as $template_path ) {
				if ( file_exists( $template_path . $template_name ) ) {
					$located = $template_path . $template_name;
					break;
				}
			}

			if ( $located ) {
				break;
			}
		}

		return $located;
	}

	/**
	 * Returns a list of paths to check for template locations:
	 * 1. Child Theme
	 * 2. Parent Theme
	 * 3. FAQ Theme
	 *
	 * @return array
	 */
	private static function get_theme_template_paths() {

		$template_dir = self::get_theme_template_dir_name();

		$file_paths = array(
			1 => trailingslashit( get_stylesheet_directory() ) . $template_dir,
			10 => trailingslashit( get_template_directory() ) . $template_dir,
			100 => self::get_templates_dir()
		);

		$file_paths = apply_filters( 'epkb_template_paths', $file_paths );

		// sort the file paths based on priority
		ksort( $file_paths, SORT_NUMERIC );

		return array_map( 'trailingslashit', $file_paths );
	}

	/**
	 * Retrieves a template part
	 *
	 * Taken from bbPress
	 *
	 * @param string $slug
	 * @param string $name Optional. Default null
	 * @param $faq_config - used in templates
	 * @param $article
	 * @param bool $load
	 *
	 * @return string
	 */
	public static function get_template_part( $slug, $name = null, /** @noinspection PhpUnusedParameterInspection */ $faq_config,
		/** @noinspection PhpUnusedParameterInspection */$article, $load = true ) {
		// Execute code for this part
		do_action( 'epkb_get_template_part_' . $slug, $slug, $name );

		$load_template = apply_filters( 'epkb_allow_template_part_' . $slug . '_' . $name, true );
		if ( false === $load_template ) {
			return '';
		}

		// Setup possible parts
		$templates = array();
		if ( isset( $name ) )
			$templates[] = $slug . '-' . $name . '.php';
		$templates[] = $slug . '.php';

		// Allow template parts to be filtered
		$templates = apply_filters( 'epkb_get_template_part', $templates, $slug, $name );

		// Return the part that is found
		$template_path = self::locate_template( $templates );
		if ( ( true == $load ) && ! empty( $template_path ) ) {
			include( $template_path );
		}

		return $template_path;
	}

	/**
	 * Check if current post/page could be FAQ one
	 *
	 * @return bool
	 */
	public static function is_post_page() {
		global $wp_query;

		if ( ( isset( $wp_query->is_archive ) && $wp_query->is_archive ) ||
		     ( isset( $wp_query->is_embed ) && $wp_query->is_embed ) ||
		     ( isset( $wp_query->is_category ) && $wp_query->is_category ) ||
		     ( isset( $wp_query->is_tag ) && $wp_query->is_tag ) ||
		     ( isset( $wp_query->is_attachment ) && $wp_query->is_attachment ) ) {
			return false;
		}

		$post = isset($GLOBALS['post']) ? $GLOBALS['post'] : '';
		if ( empty($post) || ! $post instanceof WP_Post || empty($post->post_type) ) {
			return false;
		}

		return true;
	}

	/**
	 * Returns the path to the EP templates directory
	 * @return string
	 */
	private static function get_templates_dir() {
		return Echo_Knowledge_Base::$plugin_dir . 'templates';
	}

	/**
	 * Returns name of directory inside child or parent theme folder where FAQ templates are located
	 * Themes can filter this by using the epkb_templates_dir filter.
	 *
	 * @return string
	 */
	private static function get_theme_template_dir_name() {
		return trailingslashit( apply_filters( 'epkb_templates_dir', 'faq_templates' ) );
	}
}