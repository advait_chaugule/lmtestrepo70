<?php

/**  Register JS and CSS files  */

/**
 * FRONT-END pages using our plugin features
 */
function epkb_faq_load_public_resources() {

    global $eckb_faq_id;

    // if this is not FAQ Main Page then do not load public resources or is a Category Archive page
    if ( empty($eckb_faq_id) ) {
        return;
    }

	epkb_faq_load_public_resources_now();
}
add_action( 'wp_enqueue_scripts', 'epkb_faq_load_public_resources' );

function epkb_faq_load_public_resources_now() {

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style( 'epkb-public-faq-styles', Echo_Knowledge_Base::$plugin_url . 'css/public-faq-styles' . $suffix . '.css', array(), Echo_Knowledge_Base::$version );
	wp_enqueue_script( 'epkb-public-faq-scripts', Echo_Knowledge_Base::$plugin_url . 'js/public-faq-scripts' . $suffix . '.js', array('jquery'), Echo_Knowledge_Base::$version );
	wp_localize_script( 'epkb-public-faq-scripts', 'epkb_faq_vars', array(
		'msg_try_again'         => esc_html__( 'Please try again later.', 'echo-knowledge-base' ),
		'error_occurred'        => esc_html__( 'Error occurred (16)', 'echo-knowledge-base' ),
		'not_saved'             => esc_html__( 'Error occurred - configuration NOT saved.', 'echo-knowledge-base' ),
		'unknown_error'         => esc_html__( 'unknown error (17)', 'echo-knowledge-base' ),
		'reload_try_again'      => esc_html__( 'Please reload the page and try again.', 'echo-knowledge-base' ),
		'save_config'           => esc_html__( 'Saving configuration', 'echo-knowledge-base' ),
		'input_required'        => esc_html__( 'Input is required', 'echo-knowledge-base' ),
		'reduce_name_size'      => esc_html__( 'Warning: Please reduce your name size. Tab will only show first 25 characters', 'echo-knowledge-base' ),
	));
}

/**
 * Only used for FAQ Configuration page
 */
function epkb_faq_config_load_public_css() {

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style( 'epkb-public-faq-styles', Echo_Knowledge_Base::$plugin_url . 'css/public-faq-styles' . $suffix . '.css', array(), Echo_Knowledge_Base::$version );

	$faq_id = EPKB_FAQ_Handler::get_current_faq_id();
	$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
	if ( is_wp_error( $faq_config ) ) {
		return;
	}

//	echo epkb_frontend_faq_theme_styles_now( $faq_config );
}

/**
 * Only used for KB Configuration page
 */
function epkb_faq_for_kb_config_load_public_css() {

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style( 'epkb-public-styles', Echo_Knowledge_Base::$plugin_url . 'css/public-styles' . $suffix . '.css', array(), Echo_Knowledge_Base::$version );
}

/**
 * ADMIN-PLUGIN MENU PAGES (Plugin settings, reports, lists etc.)
 */
function epkb_faq_load_admin_plugin_pages_resources() {

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style( 'epkb-admin-faq-plugin-pages-styles', Echo_Knowledge_Base::$plugin_url . 'css/admin-faq-plugin-pages' . $suffix . '.css', array(), Echo_Knowledge_Base::$version );

	wp_enqueue_script( 'epkb-admin-faq-plugin-pages-scripts', Echo_Knowledge_Base::$plugin_url . 'js/admin-faq-plugin-pages' . $suffix . '.js',
					array('jquery', 'jquery-ui-core','jquery-ui-dialog','jquery-effects-core','jquery-effects-bounce', 'jquery-ui-sortable'), Echo_Knowledge_Base::$version );
	wp_localize_script( 'epkb-admin-faq-plugin-pages-scripts', 'epkb_faq_vars', array(
					'msg_try_again'         => esc_html__( 'Please try again later.', 'echo-knowledge-base' ),
					'error_occurred'        => esc_html__( 'Error occurred (11)', 'echo-knowledge-base' ),
					'not_saved'             => esc_html__( 'Error occurred - configuration NOT saved (12).', 'echo-knowledge-base' ),
					'unknown_error'         => esc_html__( 'unknown error (13)', 'echo-knowledge-base' ),
					'reload_try_again'      => esc_html__( 'Please reload the page and try again.', 'echo-knowledge-base' ),
					'save_config'           => esc_html__( 'Saving configuration', 'echo-knowledge-base' ),
					'input_required'        => esc_html__( 'Input is required', 'echo-knowledge-base' ),
					'sending_feedback'      => esc_html__('Sending feedback ...', 'echo-knowledge-base' ),
					'changing_debug'        => esc_html__('Changing debug ...', 'echo-knowledge-base' ),
					'help_text_coming'      => esc_html__('Help text is coming soon.', 'echo-knowledge-base' )
				));

	if ( EPKB_FAQ_Utilities::get('page') == 'epkb-faq-configuration' ) {
		wp_enqueue_script( 'epkb-admin-faq-config-script', Echo_Knowledge_Base::$plugin_url . 'js/admin-faq-config-script' . $suffix . '.js',
					array('jquery',	'jquery-ui-core', 'jquery-ui-dialog', 'jquery-effects-core', 'jquery-effects-bounce'), Echo_Knowledge_Base::$version );
		wp_localize_script( 'epkb-admin-faq-config-script', 'epkb_faq_vars', array(
			'msg_try_again'         => esc_html__( 'Please try again later.', 'echo-knowledge-base' ),
			'error_occurred'        => esc_html__( 'Error occurred (14)', 'echo-knowledge-base' ),
			'not_saved'             => esc_html__( 'Error occurred - configuration NOT saved.', 'echo-knowledge-base' ),
			'unknown_error'         => esc_html__( 'unknown error (15)', 'echo-knowledge-base' ),
			'reload_try_again'      => esc_html__( 'Please reload the page and try again.', 'echo-knowledge-base' ),
			'save_config'           => esc_html__( 'Saving configuration', 'echo-knowledge-base' ),
			'input_required'        => esc_html__( 'Input is required', 'echo-knowledge-base' ),
			'reduce_name_size'      => esc_html__( 'Warning: Please reduce your name size. Tab will only show first 25 characters', 'echo-knowledge-base' ),
			'archive_page'          => esc_html__( 'Archive Page configuration is available only for FAQ Template. Switch on FAQ Template to continue.', 'echo-knowledge-base' ),
			'updating_preview'      => esc_html__( 'Updating page preview ...', 'echo-knowledge-base' ),
			'changing_config'       => esc_html__('Changing to selected configuration...', 'echo-knowledge-base' ),
			'switching_article_seq' => esc_html__('Switching article sequence ...', 'echo-knowledge-base' ),
			'preview'               => esc_html__('Preview', 'echo-knowledge-base' )
		));
	}

	wp_enqueue_script( 'wp-color-picker' );
	wp_enqueue_style( 'wp-jquery-ui-dialog' );
}

/**
 * Add style for current FAQ theme
 */
function epkb_frontend_faq_theme_styles() {

	global $eckb_faq_id;

	$faq_id = empty($eckb_faq_id) ? EPKB_FAQ_Handler::get_faq_id_from_faq_main_shortcode() : $eckb_faq_id;
	if ( empty( $faq_id ) ) {
		return;
	}

	$faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
	if ( is_wp_error( $faq_config ) ) {
		$faq_config = EPKB_FAQ_Config_Specs::get_default_faq_config( $faq_id );
	}

	echo epkb_frontend_faq_theme_styles_now( $faq_config );
}
// TODO do not run if FAQ not enabled -> add_action( 'wp_head', 'epkb_frontend_faq_theme_styles' );

/**
 * Certain styles need to be inserted in the header.
 *
 * @param $faq_config
 * @return string
 */
function epkb_frontend_faq_theme_styles_now( $faq_config ) {

	global $eckb_is_faq_main_page;

	$is_faq_main_page = ! empty($eckb_is_faq_main_page);

	// get any style from add-ons
	$add_on_output = apply_filters( 'eckb_frontend_faq_theme_style', '', $faq_config['id'], $is_faq_main_page );
	if ( empty($add_on_output) || ! is_string($add_on_output) )  {
		$add_on_output = '';
	}

	$output = 'test';

	$output .= $add_on_output;

	$output .= '</style>';

	return $output;
}
