<?php  if ( ! defined( 'ABSPATH' ) ) exit;

spl_autoload_register(array('EPKB_FAQ_Autoloader', 'autoload'), false);

/**
 * A class which contains the autoload function, that the spl_autoload_register
 * will use to autoload PHP classes.
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */
class EPKB_FAQ_Autoloader {

	public static function autoload( $class ) {
		static $classes = null;

		if ( $classes === null ) {
			$classes = array(

				// CORE
				'epkb_faq_utilities'                =>  'includes-faq/class-epkb-faq-utilities.php',
				'epkb_faq_icons'                    =>  'includes-faq/class-epkb-faq-icons.php',

				// SYSTEM
				'epkb_faq_templates'                =>  'includes-faq/system/class-epkb-faq-templates.php',

				// ADMIN CORE
				'epkb_faq_admin_notices'            =>  'includes-faq/admin/class-epkb-faq-admin-notices.php',

				// ADMIN PLUGIN MENU PAGES
				'epkb_faq_page'                     =>  'includes-faq/admin/settings/class-epkb-faq-page.php',

				// FAQ Configuration
				'epkb_faq_config_controller'        =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-controller.php',
				'epkb_faq_config_specs'             =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-specs.php',
				'epkb_faq_config_db'                =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-db.php',
				'epkb_faq_config_layouts'           =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-layouts.php',
				'epkb_faq_config_layout_faq'        =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-layout-faq.php',
				'epkb_faq_config_page'              =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-page.php',
				'epkb_faq_config_overview'          =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-overview.php',
				'epkb_faq_config_sequence'          =>  'includes-faq/admin/faq-configuration/class-epkb-faq-config-sequence.php',
				'epkb_faq_demo_data'                =>  'includes-faq/admin/faq-configuration/class-epkb-faq-demo-data.php',
				'epkb_faq_menu_configuration'       =>  'includes-faq/admin/faq-configuration/class-epkb-faq-menu-configuration.php',

				// FEATURES - LAYOUT
				'epkb_faq_layout'                   =>  'includes-faq/features/layouts/class-epkb-faq-layout.php',
				'epkb_faq_layout_faq'               =>  'includes-faq/features/layouts/class-epkb-faq-layout-faq.php',
				'epkb_faq_layouts_setup'            =>  'includes-faq/features/layouts/class-epkb-faq-layouts-setup.php',

				// FEATURES - FAQ
				'epkb_faq_handler'                  =>  'includes-faq/features/faqs/class-epkb-faq-handler.php',
				'epkb_faq_search'                   =>  'includes-faq/features/faqs/class-epkb-faq-search.php',

				// FEATURES - CATEGORIES
				'epkb_faq_categories_db'            =>  'includes-faq/features/categories/class-epkb-faq-categories-db.php',
				'epkb_faq_categories_admin'         =>  'includes-faq/features/categories/class-epkb-faq-categories-admin.php',
				'epkb_faq_categories_array'         =>  'includes-faq/features/categories/class-epkb-faq-categories-array.php',

				// FEATURES - ARTICLES
				'epkb_faq_articles_cpt_setup'       =>  'includes-faq/features/articles/class-epkb-faq-articles-cpt-setup.php',
				'epkb_faq_articles_db'              =>  'includes-faq/features/articles/class-epkb-faq-articles-db.php',
				'epkb_faq_articles_admin'           =>  'includes-faq/features/articles/class-epkb-faq-articles-admin.php',
				'epkb_faq_articles_array'           =>  'includes-faq/features/articles/class-epkb-faq-articles-array.php',

				// TEMPLATES
				'epkb_faq_templates_various'        =>  'templates/helpers/class-epkb-faq-templates-various.php'
			);
		}

		$cn = strtolower( $class );
		if ( isset( $classes[ $cn ] ) ) {
			/** @noinspection PhpIncludeInspection */
			include_once( plugin_dir_path( Echo_Knowledge_Base::$plugin_file ) . $classes[ $cn ] );
		}
	}
}
