<?php

/**
 * Setup WordPress menu for this plugin
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */

/**
 *  Register plugin menus
 */
function epkb_faq_add_plugin_menus() {

	if ( ! EPKB_FAQ_Utilities::is_faq_enabled() ) {

		$post_type_name = EPKB_KB_Handler::get_current_kb_post_type();
		if ( empty($post_type_name) ) {
			$post_type_name = EPKB_KB_Handler::get_post_type( EPKB_KB_Config_DB::DEFAULT_KB_ID );
		}

		add_submenu_page( 'edit.php?post_type=' . $post_type_name, __( 'FAQ Configuration - Echo Knowledge Base', 'echo-knowledge-base' ), __( 'FAQ Configuration', 'echo-knowledge-base' ),
				'manage_options', 'epkb-faq-configuration', array(new EPKB_FAQ_Menu_Configuration, 'display_faq_config_page') );

		return;
	}

	// Add FAQ menu that belongs to the post type that is listed in the URL or use default one if none specified
	$post_type_name = EPKB_FAQ_Handler::get_current_faq_post_type();
	if ( empty($post_type_name) ) {
		$post_type_name = EPKB_FAQ_Handler::get_post_type( EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID );
	}

	add_submenu_page( 'edit.php?post_type=' . $post_type_name, __( 'Configuration - Echo Knowledge Base', 'echo-knowledge-base' ), __( 'Configuration', 'echo-knowledge-base' ),
		'manage_options', 'epkb-faq-configuration', array(new EPKB_FAQ_Menu_Configuration, 'display_faq_config_page') );

	do_action( 'eckb_add_faq_menu_item', $post_type_name );
}
add_action( 'admin_menu', 'epkb_faq_add_plugin_menus', 10 );

/**
 * Display tabs representing existing knowledge bases at the top of each FAQ admin page
 */
function epkb_faq_add_page_tabs() {

	global $current_screen;

	// first determine if this page belongs to Knowledge Base and return if it does not
	$current_faq_id = EPKB_FAQ_Handler::get_current_faq_id();
	if ( empty($current_faq_id) ) {
		return;
	}

	// determine tab label e.g. 'Templates For:'
	$screen_id = isset( $current_screen->id ) ? $current_screen->id : '';
	$screen_id = str_replace( EPKB_FAQ_Handler::get_post_type( $current_faq_id ), 'EFAQ_SCREEN', $screen_id );

	// if add-on is not using tabs then exit
	$no_faq_tabs = apply_filters( 'eckb_hide_faq_tabs', $screen_id );
	if ( isset($no_faq_tabs) && $no_faq_tabs == 'no_faq_tabs' ) {
		return;
	}

	$disable_faq_buttons = false;

	switch ( $screen_id ) {

		// All Articles page
		case 'edit-EFAQ_SCREEN':
			$tab_url_base = 'edit.php?post_type=*';
			break;

		// Add New Article page
		case 'EFAQ_SCREEN':
			$tab_url_base = 'post-new.php?post_type=*';
			break;

		// Categories page
		case 'edit-EFAQ_SCREEN_category':
			$tab_url_base = 'edit-tags.php?taxonomy=*_category&post_type=*';
			break;

		// Tags page
		case 'edit-EFAQ_SCREEN_tag':
			$tab_url_base = 'edit-tags.php?taxonomy=*_tag&post_type=*';
			break;

		// FAQ Configuration page
		case 'EFAQ_SCREEN_page_epkb-faq-configuration':
			return;

		// Settings page
		case 'EFAQ_SCREEN_page_epkb-plugin-settings':
			return;

		// Add-ons page
		case 'EFAQ_SCREEN_page_epkb-add-ons':
			return;

		// Analytics page
		case 'EFAQ_SCREEN_page_epkb-plugin-analytics':
			return;

		default:
			$tab_url_base = 'edit.php?post_type=*';
	}

	epkb_display_faq_navigation_tabs( $current_faq_id, $tab_url_base, $disable_faq_buttons, $screen_id );
}
add_action( 'all_admin_notices', 'epkb_faq_add_page_tabs', 99999 );

/**
 * Generate navigation bars that show available and selected knowledge bases
 *
 * @param $current_faq_id
 * @param $tab_url_base
 * @param $disable_faq_buttons
 * @param $screen_id
 *
 * @return int
 */
function epkb_display_faq_navigation_tabs( $current_faq_id, $tab_url_base, $disable_faq_buttons, $screen_id ) {	?>

	<div class="wrap">
		<h1></h1>
	</div>

	<div id="ekb_core_top_heading">
		<ul class="tab_navigation">  			<?php

			$ix = 1;
			$nof_tabs_visible = 3;
			$all_faq_configs = epkb_get_instance()->faq_config_obj->get_faq_configs();

			$nof_faqs = 0;
			$active_faq_configs = array();
			foreach ( $all_faq_configs as $one_faq_config ) {

				// skip archived FAQs
				if ($one_faq_config['id'] !== EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID && EPKB_FAQ_Utilities::is_faq_archived( $one_faq_config['status'] ) ) {
					continue;
				}

				if ( $current_faq_id == $one_faq_config['id'] ) {
					array_unshift($active_faq_configs , $one_faq_config);
				} else {
					$active_faq_configs[] = $one_faq_config;
				}

				$nof_faqs++;
			}

			// display FAQ tabs
			foreach ( $active_faq_configs as $one_faq_config ) {

			// if is more than $nof_tabs_visible FAQ then start putting them into a drop down list
			if ( $ix == ( $nof_tabs_visible + 1 ) &&  ( $nof_faqs > $nof_tabs_visible + 1 ) ) {	?>
			<li class="drop_down_tabs">
				<span class="more_tabs"><?php echo esc_html__( 'More Tabs', 'echo-knowledge-base' ) . ' (' . ( $nof_faqs - $nof_tabs_visible ) . ')';	?></span>
				<ul><?php
					}

					// output FAQ tab
					$faq_name = isset($one_faq_config['faq_name']) ? $one_faq_config['faq_name'] : __( 'Knowledge Base', 'echo-knowledge-base' );
					$tab_url = str_replace( '*', EPKB_FAQ_Handler::get_post_type( $one_faq_config['id'] ), $tab_url_base );
					$active  = ( $current_faq_id == $one_faq_config['id'] ? 'active' : '' );
					echo '<li>';
					echo    '<a ' . ( $disable_faq_buttons ? '' : 'href="' . esc_url( $tab_url ) . '"' ) . ' title="' . esc_attr( $faq_name ) . '" class="nav_tab' . ' ' . $active . '">';
					echo       '<span>' . esc_html( $faq_name ) . '</span>';
					echo    '</a>';
					echo '</li>';

					$ix++;

					} //foreach

					//If the last list item add the closing ul li tags
					if ( $nof_faqs > $nof_tabs_visible ) {	?>
				</ul>
			</li>	<?php
		}	?>

		</ul><!-- Tab Navigation -->

	</div><!-- ekb_core_top_heading -->     	<?php
}
