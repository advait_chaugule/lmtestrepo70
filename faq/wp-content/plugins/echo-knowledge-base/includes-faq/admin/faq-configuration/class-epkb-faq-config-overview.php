<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Display FAQ Overview information that is displayed with FAQ Configuration page
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */
class EPKB_FAQ_Config_Overview {

	/**
	 * Display Overview Page
	 *
	 * @param $faq_config
	 * @param $feature_specs
	 * @param EPKB_KB_Config_Elements $form
	 */
	public static function display_overview( $faq_config, $feature_specs, $form ) {

		$faq_id = $faq_config['id'];
		$faq_main_pages_url = '';
		$faq_main_pages_info = self::get_faq_main_pages( $faq_config );
		foreach( $faq_main_pages_info as $post_id => $post_info ) {
			$post_status = $post_info['post_status'] == EPKB_FAQ_Utilities::get_post_status_text( 'publish' ) ? '' : ' (' . $post_info['post_status'] . ')';
			$faq_main_pages_url .= '  <li>' .	$post_info['post_title'] . $post_status . ' &nbsp;&nbsp;';
			$main_page_view_url = get_permalink( $post_id );
			$faq_main_pages_url .= '<a href="' . ( empty($main_page_view_url) || is_wp_error( $main_page_view_url ) ? '' : $main_page_view_url ) . '" target="_blank">' . __( 'View', 'echo-knowledge-base' ) . '</a> ';
			$post_link = get_edit_post_link( $post_id );
			$faq_main_pages_url .= ' &nbsp;&nbsp;<a href="' . ( empty($post_link) ? '' : $post_link ) . '" target="_blank">' . __( 'Edit', 'echo-knowledge-base' ) . '</a></li>';
		}

		$faq_main_pages_url = empty($faq_main_pages_url) ? ' ' . __( 'None found', 'echo-knowledge-base' ) : $faq_main_pages_url;


		/***  Errors  ***/

		// LICENSE / ADD-ON issues
		$add_on_messages = apply_filters( 'epkb_add_on_license_message', array() );
		if ( ( ! empty($add_on_messages) && is_array($add_on_messages) ) || did_action('faq_overview_add_on_errors' ) ) {        ?>

		    <section class="overview-info-section">
	            <div class="overview-error">
	                <div class="overview-header">
	                    <div class="overview-title"><?php _e( 'Errors', 'echo-knowledge-base' ); ?></div>
	                    <div class="overview-brief"><span class="overview-icon ep_font_icon_error_circle"></span><?php _e( 'Critical Errors Detected', 'echo-knowledge-base' ); ?></div>
	                </div>
	                <div class="overview-content">	<?php
			                if ( ! empty($add_on_messages) && is_array($add_on_messages) ) {
				                foreach( $add_on_messages as $add_on_name => $add_on_message ) {
					                $add_on_name = str_replace( array('2', '3', '4'), '', $add_on_name );
					                echo
						                '<div class="callout callout_error">' .
						                '<h4>' . esc_html($add_on_name) . ': ' . __( 'License issue', 'echo-knowledge-base' ) . '</h4>' .
						                '<p>' . wp_kses_post( $add_on_message ) . '</p>' .
						                '</div>';
				                }
			                }
			                do_action('faq_overview_add_on_errors' );				?>
	                </div>
	            </div>
	        </section>      <?php
		}   ?>

        <!-- Configuration -->
        <section class="overview-info-section">
            <div class="overview-config">
                <div class="overview-header">
                    <div class="overview-title"><?php _e( 'Configuration', 'echo-knowledge-base' ); ?></div>
                    <div class="overview-brief"><span class="overview-icon ep_font_icon_gear"></span><?php _e( 'Additional Global Settings', 'echo-knowledge-base' ); ?></div>
                    <div class="overview-toggle"><?php _e( 'View Details', 'echo-knowledge-base' ); ?></div>
                </div>
                <div class="overview-content">

                    <form id="epkb-config-config3">

                        <!--  FAQ NAME and other global settings -->
                        <div class="callout callout_default">
                            <h4>FAQ Name</h4>
                            <div class="row">
                                <div class="config-col-4">		            <?php
                                    echo $form->text(  array(
                                            'value' => $faq_config[ 'faq_name' ],
                                            'input_group_class' => 'config-col-12',
                                            'label_class' => 'config-col-3',
                                            'input_class' => 'config-col-9'
                                        ) + $feature_specs['faq_name'] );		            ?>
                                </div>
                                <div class="config-col-6">

                                    <div class="config-col-3">			            <?php
                                        $form->submit_button( array(
                                            'label'             => 'Update',
                                            'id'                => 'epkb_save_dashboard',
                                            'main_class'        => 'epkb_save_dashboard',
                                            'action'            => 'epkb_save_dashboard',
                                            'input_class'       => 'epkb-info-settings-button'
                                        ) );			            ?>
                                    </div>
                                    <div class="config-col-3">			            <?php
                                        $form->submit_button( array(
                                            'label'             => 'Cancel',
                                            'id'                => 'epkb_cancel_dashboard',
                                            'main_class'        => 'epkb_cancel_dashboard',
                                            'action'            => 'epkb_cancel_dashboard',
                                            'input_class'       => 'epkb-info-settings-button',
                                        ) );			            ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>

	                <div class="callout callout_default">
		                <h4><?php _e( 'FAQ Main Page', 'echo-knowledge-base' ); ?></h4>
		                <p><?php _e( 'To display a <strong>Knowledge Base Main page</strong>, add the following FAQ shortcode to any page: &nbsp;&nbsp;<strong>', 'echo-knowledge-base' ); ?>
				                [<?php echo EPKB_FAQ_Handler::FAQ_MAIN_PAGE_SHORTCODE_NAME . ' id=' . $faq_id; ?>]</strong></p>
		                <p><strong><?php _e( 'Existing FAQ Main Page(s)', 'echo-knowledge-base' ); ?>:</strong></p>
		                <ul>			                <?php
			                echo wp_kses_post( $faq_main_pages_url );    ?>
		                </ul>
	                </div>

                </div>
            </div>
        </section>        <?php
	}

	/**
	 * Return all FAQ Main pages that we know about. Also remove old ones.
	 *
	 * @param $faq_config
	 * @return array a list of FAQ Main Pages titles and links
	 */
	private static function get_faq_main_pages( $faq_config) {

		$faq_main_pages = $faq_config['faq_main_pages'];
		$faq_main_pages_info = array();
		foreach ( $faq_main_pages as $post_id => $post_title ) {

			$post_status = get_post_status( $post_id );

			// remove previous page versions
			if ( empty( $post_status ) || $post_status == 'inherit' || $post_status == 'trash' ) {
				unset( $faq_main_pages[ $post_id ] );
				continue;
			}

			$post = get_post( $post_id );
			if ( empty( $post ) || is_array( $post ) || ! $post instanceof WP_Post ) {
				unset( $faq_main_pages[ $post_id ] );
				continue;
			}

			// remove page that does not contain FAQ shortcode any more
			$faq_id = EPKB_FAQ_Handler::get_faq_id_from_faq_main_shortcode( $post );
			if ( empty( $faq_id ) || $faq_id != $faq_config['id'] ) {
				unset( $faq_main_pages[ $post_id ] );
				continue;
			}

			$faq_post_slug = get_page_uri($post_id);  // includes PARENT directory slug
			if ( is_wp_error( $faq_post_slug ) || empty($faq_post_slug) || is_array($faq_post_slug) ) {
				$faq_post_slug = EPKB_FAQ_Handler::get_default_slug( $faq_id );
			}

			$faq_main_pages_info[$post_id] = array( 'post_title' => $post_title, 'post_status' => EPKB_FAQ_Utilities::get_post_status_text( $post_status ), 'post_slug' => urldecode($faq_post_slug) );
		}

		// we need to remove pages that are revisions
		if ( count( $faq_config['faq_main_pages'] ) != count($faq_main_pages) ) {
			$faq_config['faq_main_pages'] = $faq_main_pages;
			epkb_get_instance()->faq_config_obj->update_faq_configuration( $faq_config['id'], $faq_config );
		}

		return $faq_main_pages_info;
	}
}
