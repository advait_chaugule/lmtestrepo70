<?php  if ( ! defined( 'ABSPATH' ) ) exit;

class EPKB_FAQ_Config_Layouts {

	const FAQ_DEFAULT_LAYOUT_STYLE = 'Boxed';
	const FAQ_DEFAULT_COLORS_STYLE = 'black-white2';

	/**
	 * Get all known layouts including add-ons
	 * @return array all defined layout names
	 */
	public static function get_main_page_layout_name_value() {
		$core_layouts = array (
			EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME => __( 'FAQ', 'echo-knowledge-base' )
		);
		return apply_filters( 'epkb_layout_names', $core_layouts );
	}

	/**
	 * Get all known layouts including add-ons
	 * @return array all defined layout names
	 */
	public static function get_main_page_layout_names() {
		$layout_name_values = self::get_main_page_layout_name_value();
		return array_keys($layout_name_values);
	}

	/**
	 * Return current layout or default layout if not found.
	 *
	 * @param $faq_config
	 * @return string
	 */
	public static function get_faq_main_page_layout_name( $faq_config ) {
		$layout = empty($faq_config['faq_main_page_layout']) || ! in_array($faq_config['faq_main_page_layout'], self::get_main_page_layout_names() )
						? EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME : $faq_config['faq_main_page_layout'];
		return $layout;
	}

	/**
	 * Main Page: Get all known styles (based on layout) including add-ons
	 *
	 * @param $faq_config
	 * @return array all defined color names
	 */
	public static function get_main_page_style_names( $faq_config ) {
		$faq_main_page_layout = self::get_faq_main_page_layout_name( $faq_config );

		$add_on_style_names = apply_filters( 'epkb_style_names', array() );
		if ( isset($add_on_style_names[$faq_main_page_layout]) ) {
			return $add_on_style_names[$faq_main_page_layout];
		}

		switch( $faq_main_page_layout ) {
			case EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME:
			default:
				return array( EPKB_FAQ_Config_Layout_FAQ::LAYOUT_STYLE_1 => EPKB_FAQ_Config_Layout_FAQ::LAYOUT_STYLE_1,
				              EPKB_FAQ_Config_Layout_FAQ::LAYOUT_STYLE_2 => EPKB_FAQ_Config_Layout_FAQ::LAYOUT_STYLE_2 );
				break;
		}
	}

	/**
	 * Get all known search box styles (based on layout) including add-ons
	 *
	 * @param $faq_config
	 * @return array all defined color names
	 */
	public static function get_search_box_style_names( $faq_config ) {
		$faq_main_page_layout = self::get_faq_main_page_layout_name( $faq_config );

		switch( $faq_main_page_layout ) {
			case EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME:
			default:
				return array( EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_1 => EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_1,
				              EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_2 => EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_2,
				              EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_3 => EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_3,
				              EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_4 => EPKB_FAQ_Config_Layout_FAQ::SEARCH_BOX_LAYOUT_STYLE_4
				);
				break;
		}
	}

	/**
	 * Get all known colors including add-ons
	 * @return array all defined color names
	 */
	public static function get_colors_names() {
		$core_colors = array (
				'black-white1'  => 'black-white1',
				'black-white2'  => 'black-white2',
				'black-white3'  => 'black-white3',
				'black-white4'  => 'black-white4',
				'blue1'         => 'blue1',
				'blue2'         => 'blue2',
				'blue3'         => 'blue3',
				'blue4'         => 'blue4',
				'green1'        => 'green1',
				'green2'        => 'green2',
				'green3'        => 'green3',
				'green4'        => 'green4',
				'red1'          => 'red1',
				'red2'          => 'red2',
				'red3'          => 'red3',
				'red4'          => 'red4',

		);
		return apply_filters( 'epkb_colors_names', $core_colors );
	}

	/**
	 * Register filters for layouts that are part of the plugin or add-ons
	 */
	public static function register_faq_config_hooks() {
		
		// register layouts and colors and text
		add_filter( 'epkb_faq_main_page_style_settings', array( 'EPKB_FAQ_Config_Layout_FAQ', 'get_faq_config_style' ), 10, 2 );
		add_filter( 'epkb_faq_main_page_colors_settings', array( 'EPKB_FAQ_Config_Layout_FAQ', 'get_faq_config_colors' ), 10, 2 );
		add_filter( 'epkb_faq_main_page_text_settings', array( 'EPKB_FAQ_Config_Layout_FAQ', 'get_faq_config_text' ), 10, 2 );

		// register style, search box style and color sets
		add_filter( 'epkb_faq_main_page_style_set', array( 'EPKB_FAQ_Config_Layout_FAQ', 'get_style_set' ), 10, 3 );
		add_filter( 'epkb_faq_main_page_search_box_style_set', array( 'EPKB_FAQ_Config_Layout_FAQ', 'get_search_box_style_set' ), 10, 3 );
		add_filter( 'epkb_faq_main_page_colors_set', array( 'EPKB_FAQ_Config_Layout_FAQ', 'get_colors_set' ), 10, 3 );

		do_action( 'epkb_register_faq_config_hooks' );
	}

	public static function get_main_page_style_set( $layout_name, $set_name ) {
		return apply_filters( 'epkb_faq_main_page_style_set', array(), $layout_name, $set_name );
	}

	public static function get_main_page_colors_set( $layout_name, $set_name ) {
		return apply_filters( 'epkb_faq_main_page_colors_set', array(), $layout_name, $set_name );
	}
}
