<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Display feature settings
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */
class EPKB_FAQ_Config_Page {
	
	var $faq_config = array();
	/** @var  EPKB_KB_Config_Elements */
	var $form;
	var $feature_specs = array();
	var $faq_main_page_layout = EPKB_FAQ_Config_Layout_FAQ::LAYOUT_NAME;
	var $show_main_page = false;
	var $can_save_config = true;

	public function __construct( $faq_config=array(), $new_faq_config=array() ) {
		// retrieve current FAQ configuration
		$faq_config = empty($faq_config) ? epkb_get_instance()->faq_config_obj->get_current_faq_configuration() : $faq_config;
		if ( is_wp_error( $faq_config ) ) {
			$this->can_save_config = false;
			$faq_config = EPKB_FAQ_Config_Specs::get_default_faq_config( EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID );
		}

		$this->faq_config             = $faq_config;
		$this->feature_specs          = EPKB_FAQ_Config_Specs::get_fields_specification( $faq_config['id'] );
		$this->form                   = new EPKB_KB_Config_Elements();
		$this->faq_main_page_layout    = 'FAQ';
		$this->show_main_page         = isset($_REQUEST['epkb-demo']) || isset($_REQUEST['ekb-main-page']);
	}

	/**
	 * Displays the FAQ Config page with top panel + sidebar + preview panel
	 */
	public function display_faq_config_page() {

		$faq_enabled = EPKB_FAQ_Utilities::get_wp_option( 'epkb_faq_enabled', false );
		if ( empty($faq_enabled) ) {
			$this->show_faq_intro_page();
			return;
		}

		if ( ! $this->can_save_config ) {
			echo '<p>' . __( 'Could not retrieve FAQ configuration.', 'echo-knowledge-base' ) . '</p>';
			return;
		}

		// setup hooks for FAQ config fields for core layouts
		EPKB_FAQ_Config_Layouts::register_faq_config_hooks();

		// display all elements of the configuration page
		$this->display_page();
	}

	/**
	 * Display FAQ Config content areas
	 */
	private function display_page() {        ?>

		<div class="wrap">
			<h1></h1>
		</div>
		<div id="ekb-admin-page-wrap" class="ekb-admin-page-wrap epkb-config-container">
			<div class="epkb-config-wrapper">
				<div class="wrap" id="ekb_core_top_heading"></div>

				<div id="epkb-config-main-info">		<?php
					$this->display_top_panel();         ?>
                    <div class="epkb-open-mm">
                        <span class="ep_font_icon_arrow_carrot_down"></span>
                    </div>
				</div>                                  <?php

                    $this->top_panel_demo_info();       ?>

				<div id="epkb-admin-mega-menu" <?php echo $this->show_main_page ? 'class="epkb-active-page"' : ''; ?>>         <?php
                    $this->display_mega_menu();         ?>
				</div>                                  <?php

					$this->display_main_panel();

					$this->display_sidebar();			?>
			</div>

            <div class="eckb-bottom-notice-message"></div>
		</div>

		<div id="epkb-dialog-info-icon" title="" style="display: none;">
			<p id="epkb-dialog-info-icon-msg"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span></p>
		</div>      <?php

	}


	/**************************************************************************************
	 *
	 *                   MEGA MENU
	 * Prefix mm = mega menu
	 *************************************************************************************/

	public function display_mega_menu() {

		$setup_i18 = __( 'SETUP', 'echo-knowledge-base' );
		$organize_i18 = __( 'ORGANIZE', 'echo-knowledge-base' );
		$all_text_i18 = __( 'ALL TEXT', 'echo-knowledge-base' );
		$tuning_i18 = __( 'TUNING', 'echo-knowledge-base' );

        echo '<div class="epkb-mm-sidebar">';

        /********************************************************************
         * 1. display MAIN PAGE and CATEGORY/TAG PAGE menu (right side)
         ********************************************************************/

        $main_page_core_links = array($setup_i18, $organize_i18, $all_text_i18, $tuning_i18);
        $this->mega_menu_sidebar_links( array(
            'id'            => 'eckb-mm-mp-links',
            'core_links'    => $main_page_core_links,
            'add_on_links'  => ''
        ));

		$this->mega_menu_sidebar_links( array(
			'id'            => 'eckb-mm-arch-links',
			'core_links'    => array($setup_i18, $all_text_i18),
			'add_on_links'  => ''
		));

		echo '</div>';
	    echo '<div class="epkb-mm-content">';
        echo '<form id="epkb-config-config2">';

		// define Search Box or Advanced Search Box configuration menu
		$search_box_menu = array(
			'heading'   => 'Search Box',
			'links'     => array( 'Layout', 'Colors', 'Text', 'Advanced' )
		);
		$search_box_menu = apply_filters( 'eckb_search_box_configuration', $search_box_menu );


        /********************************************************************
         * 2. display MAIN PAGE menu content (Right side)
         ********************************************************************/

        // MAIN PAGE - SETUP menu item
		$this->mega_menu_item_custom_html_content( array(
			'id'        => 'eckb-mm-mp-links-setup',
			'sections'  => array(
				array(
					'heading' => '1. Layout',
					'form_elements' => array(
						array(
							'id'   => 'mega-menu-main-page-layout',
							'html' => $this->form->radio_buttons_vertical( array('label' => '') + $this->feature_specs['faq_main_page_layout'] + array(
											'current'           => $this->faq_config['faq_main_page_layout'],
											'input_group_class' => 'config-col-12',
											'main_label_class'  => 'config-col-4',
											'input_class'       => '',
											'radio_class'       => 'config-col-12' ) )
						)
					)
				),
				array(
					'heading' => '2. Style',
					'form_elements' => array(
                        array(
                            'id'   => 'mega-menu-main-page-style',
                            'html' => $this->form->radio_buttons_vertical( array(
	                            'id' => 'main_page_reset_style',
	                            'name' => 'main_page_reset_style',
	                            'label' => '',
	                            'options' => EPKB_FAQ_Config_Layouts::get_main_page_style_names( $this->faq_config ),
	                            'input_group_class' => '',
	                            'main_label_class'  => '',
	                            'input_class'       => 'radio_buttons_resets',
	                            'radio_class'       => ''
                            )),
                        )
                    )
				),
				array(
					'heading' => '3. Colors',
					'form_elements' => array(
						array(
							'id'   => 'mega-menu-main-page-colors',
							'html' => $this->mega_menu_colors()
						)
					)
				),
                array(
                    'heading' => '4. Template',
                    'form_elements' => array(
                        array(
                            'id'   => 'mega-menu-main-page-faq-template',
                            'html' => $this->mega_menu_faq_templates()
                        )
                    )
                )
			)
		));

		// MAIN PAGE - ORGANIZE menu item
		$this->mega_menu_item_custom_html_content( array(
			'id'        => 'eckb-mm-mp-links-organize',
			'sections'  => array(
                array(
                    'heading' => 'Organize',
                    'links' => array(),
                    'form_elements' => array(
                        array(
                            'id'   => 'mega-menu-main-page-organize',
                            'html' => $this->mega_menu_organize()
                        )
                    )
                )
			)
		));

		// MAIN PAGE - ALL TEXT menu item
		$this->mega_menu_item_content( array(
			'id'        => 'eckb-mm-mp-links-alltext',
			'sections'  => array(
				array(
					'heading' => 'Text',
					'links' => array( 'Search Box', 'Categories', 'Articles' )
				),
			)
		));

		// MAIN PAGE - TUNING menu item
        $this->mega_menu_item_content( array(
            'id'        => 'eckb-mm-mp-links-tuning',
            'sections'  => array(

            	// ------- Search Box --------
	            $search_box_menu,
	            // ------- Content -----------
                array(
	                'heading' => 'Content',
	                'links' => array( 'Style', 'Colors' ),
                ),
	            // ------- Categories --------
                array(
	                'heading' => 'Categories',
	                'links' => array( 'Style', 'Colors', 'Text', 'Advanced' )
                ),

            )
        ));
		
		echo '</div>';

        echo '</form>';
        echo '</div>';

		echo '<div class="epkb-close-mm">';
                echo '<span class="ep_font_icon_arrow_carrot_up"></span>';
        echo '</div>';
	}

	/**
	 * Display MAIN PAGE and CATEGORY and TAG PAGE Sidebar menu items on the right side of the Mega Menu
	 *
	 * @param array $args
	 */
	private function mega_menu_sidebar_links( $args = array() ) {

		echo '<ul class="' . ( empty($args['class']) ? '' : $args['class'] ) . '" id="' . $args['id'] . '">';

		$ix = 0;
		foreach( $args['core_links'] as $link ) {
			$class = $ix++ == 0 ? 'class="epkb-mm-active"' : '';
			$linkID = $args['id'] . '-' . str_replace(' ', '', strtolower( $link ) );
			echo '<li id="' . $linkID . '" ' . $class . '>' . $link . '</li>';
		}

		echo '</ul>';
	}

	/**
	 * Show content of a menu item (list of links on the right side)
	 *
	 * @param array $args
	 */
	private function mega_menu_item_content( $args = array() ) {

		echo '<div class="epkb-mm-links ' . ( empty($args['class']) ? '' : $args['class'] ) . '" id="' . $args['id'] . '-list' . '">';
		foreach( $args['sections'] as $section ) {

            if ( ! empty($section['exclude']) ) {
                continue;
            }

			echo '<section>' .
				'	<h3>' . ( empty($section['heading']) ? '' :  __( $section['heading'], 'echo-knowledge-base' ) ) . '</h3>' .
			    '   <p>' . ( empty($section['info']) ? '' : $section['info'] ) .'</p>' .
				'	<ul>';

			foreach ( $section[ 'links'] as $link ) {
				$linkID = $args['id'] . '-' . str_replace( array( ' ', ':' ), '', strtolower($section['heading'] . '-' . $link ) );
				echo '<li id="' . $linkID . '">' . __( $link, 'echo-knowledge-base' ) . '</li>';
			}

			echo '	</ul>' .
				'</section>';
		}
		echo '</div>';
	}

	private function mega_menu_item_custom_html_content( $args = array() ) {

		echo '<div class="epkb-mm-links ' . ( empty($args['class']) ? '' : $args['class'] ) . '" id="' . $args['id'] . '-list' . '">';
		foreach( $args['sections'] as $section ) {

			echo '<section>';
			echo '<h3>' . __( $section['heading'], 'echo-knowledge-base' ) . '</h3>';

			foreach ( $section['form_elements'] as $html ) {
				echo '<div id="' . $html['id'] . '">';
				echo $html['html'];
				echo '</div>';
			}

			echo '</section>';

		}
		echo '</div>';
	}

	private function mega_menu_colors( $is_main_page=true ) {
		ob_start();	    ?>

        <div class="reset_colors" id="main_page_reset_colors">
            <ul>
                <li class="config-col-12"><?php _e( 'Black / White', 'echo-knowledge-base' ); ?></li>
                <li class="config-col-4">
                    <div class="color_palette black-white">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
                <li class="config-col-8">
                    <ul class="epkb_rest_buttons">
                        <li><button type="button" value="black-white1">1</button></li>
                        <li><button type="button" value="black-white2">2</button></li>
                        <li><button type="button" value="black-white3">3</button></li>
                        <li><button type="button" value="black-white4">4</button></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li class="config-col-12"><?php _e( 'Red', 'echo-knowledge-base' ); ?></li>
                <li class="config-col-4">
                    <div class="color_palette red">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
                <li class="config-col-8">
                    <ul class="epkb_rest_buttons">
                        <li><button type="button" value="red1">1</button></li>
                        <li><button type="button" value="red2">2</button></li>
                        <li><button type="button" value="red3">3</button></li>
                        <li><button type="button" value="red4">4</button></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li class="config-col-12"><?php _e( 'Blue', 'echo-knowledge-base' ); ?></li>
                <li class="config-col-4">
                    <div class="color_palette blue">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
                <li class="config-col-8">
                    <ul class="epkb_rest_buttons">
                        <li><button type="button" value="blue1"> 1 </button></li>
                        <li><button type="button" value="blue2"> 2 </button></li>
                        <li><button type="button" value="blue3"> 3 </button></li>
                        <li><button type="button" value="blue4"> 4 </button></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li class="config-col-12"><?php _e( 'Green', 'echo-knowledge-base' ); ?></li>
                <li class="config-col-4">
                    <div class="color_palette green">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
                <li class="config-col-8">
                    <ul class="epkb_rest_buttons">
                        <li><button type="button" value="green1">1</button></li>
                        <li><button type="button" value="green2">2</button></li>
                        <li><button type="button" value="green3">3</button></li>
                        <li><button type="button" value="green4">4</button></li>
                    </ul>
                </li>
            </ul>
        </div>    <?php

		return ob_get_clean();
	}

	/**
	 * Display menu content for FAQ Template Choice
	 *
	 * @return string
	 */
	private function mega_menu_faq_templates() {

		ob_start();

		echo  $this->form->radio_buttons_vertical( $this->feature_specs['templates_for_faq'] + array(
				'current'           => $this->faq_config['templates_for_faq'],
				'input_group_class' => 'config-col-12',
				'main_label_class'  => 'config-col-12',
				'input_class'       => '',
				'radio_class'       => 'config-col-12' ) );

        echo '<p><a href="http://www.echoknowledgebase.com/documentation/faq-templates/" target="_blank" class="eckb-external-link">' . __( 'More about templates', 'echo-knowledge-base' ) . '</a>';

		return ob_get_clean();
     }

	private function mega_menu_organize() {
        ob_start();
        echo '<p><strong>' . __( 'To Organize Categories and Articles', 'echo-knowledge-base' ) . ':</strong></p>';
        echo '<p style="padding-left: 20px;">a) ' . __( 'In the preview below, drag and drop categories and articles in any order', 'echo-knowledge-base' ) . ' </p>';
        echo '<p>   OR</p>';
        echo '<p style="padding-left: 20px;">b) ' . __( 'In the configuration on the right, set chronological or alphabetical order', 'echo-knowledge-base' ) . ' </p>';
        return ob_get_clean();
	}


	/**************************************************************************************
	 *
	 *                   TOP PANEL
	 *
	 *************************************************************************************/

	/**
	 * Display top overview panel
	 */
	private function display_top_panel() {

        // display link to FAQ Main Page if any
		$link_output = self::get_first_faq_main_page_url( $this->faq_config );
		if ( ! empty($link_output) ) {
			$link_output = '<a href="' . $link_output . '" target="_blank"><div class="epkb-view ep_font_icon_external_link"></div></a>';
		}

        // for demo switch
		$checked = '';
		if ( isset($_REQUEST['epkb-demo']) || ( isset($_POST['epkb_demo_faq']) && $_POST['epkb_demo_faq'] == "true" ) ) {
			$checked = 'checked';
		}   ?>

		<div class="epkb-info-section epkb-faq-name-section">   <?php
			$this->display_list_of_faqs(); 			?>
		</div>
        <div class="epkb-info-section epkb-view">
            <?php echo $link_output; ?>
        </div>

		<div class="epkb-info-section epkb-info-main <?php echo $this->show_main_page ? '' : 'epkb-active-page'; ?>">
			<div class="overview-icon-container">
				<p><?php _e( 'Overview', 'echo-knowledge-base' ); ?></p>
				<div class="page-icon overview-icon ep_font_icon_data_report" id="epkb-config-overview"></div>
			</div>
		</div>

		<!--  MAIN PAGE BUTTONS -->
		<div class="epkb-info-section epkb-info-pages <?php echo $this->show_main_page ? 'epkb-active-page' : ''; ?>" id="epkb-main-page-button">
			<div class="page-icon-container">
				<p><?php _e( 'Main Page', 'echo-knowledge-base' ); ?></p>
				<div class="page-icon ep_font_icon_flow_chart" id="epkb-main-page"></div>
                <div id="epkb-user-flow-arrow" class="user_flow_arrow_icon  ep_font_icon_arrow_carrot_right"></div>
			</div>
		</div>

		<!--  CATEGORY/TAG PAGE BUTTON -->
		<div class="epkb-info-section epkb-info-pages" id="epkb-archive-page-button">
			<div class="page-icon-container">
				<p><?php _e( 'Archive Page', 'echo-knowledge-base' ); ?></p>
				<div class="page-icon fa fa-archive" id="epkb-archive-page"></div>
			</div>
		</div>

        <!--  DEMO SWITCH -->
        <div class="epkb-info-section epkb-demo-data-button">
            <div class="page-icon-container">
                <div class="epkb-data-switch">
                    <div class="epkb-switch-container">
                        <label class="epkb-switch">
                            <input id="epkb-layout-preview-data" type="chekbox" name="layout-preview-data" <?php echo $checked; ?>>
                            <div class="epkb-slider round"></div>
                            <div class="faq-name"><?php _e( 'Demo FAQ', 'echo-knowledge-base' ); ?></div>
                            <div class="faq-demo"><?php _e( 'Current FAQ', 'echo-knowledge-base' ); ?></div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="ep_font_icon_info"></div>
        </div>

        <!--  SETTINGS BUTTONS -->
        <!--<div class="epkb-info-section epkb-mega-menu-toggle" id="epkb-settings-mega-menu-button">
            <div class="page-icon-container">
                <div class="epkb-setting-icon ep_font_icon_gear" id="epkb-settings-mega-menu"></div>
            </div>
        </div>-->
		<div class="epkb-info-section epkb-info-save" style="display:none;">			<?php
			$this->form->submit_button( array(
				'label'             => __( 'Save', 'echo-knowledge-base' ),
				'id'                => 'epkb_save_faq_config',
				'main_class'        => 'epkb_save_faq_config',
				'action'            => 'epkb_save_faq_config',
				'input_class'       => 'epkb-info-settings-button success-btn',
			) );   ?>
		</div>      <?php
	}

	private function top_panel_demo_info(){ ?>
        <div class="epkb-demo-info-content hidden">
            <h5 class="option-info-title"><?php _e( 'Demo Data', 'echo-knowledge-base' ); ?></h5>
            <p><?php _e( 'The preview box below shows only a simplified version of the actual page. The preview can help you to visualize how changes to configuration will affect the page.', 'echo-knowledge-base' ); ?></p>
            <p><strong><?php _e( 'Current / Demo FAQ', 'echo-knowledge-base' ); ?></strong>: <?php _e( 'You can switch to the Demo FAQ to see how populated Knowledge Base looks with a specific configuration. The Demo data is never saved to your FAQ.', 'echo-knowledge-base' ); ?> </p>
        </div>    <?php
    }

	/**************************************************************************************
	 *
	 *                   MAIN PANEL
	 *
	 *************************************************************************************/

	/**
	 * Display individual preview panels
	 */
	private function display_main_panel() {       ?>

		<div class="epkb-config-content" id="epkb-config-overview-content" <?php echo $this->show_main_page ? 'style="display: none;"' : ''; ?>>   <?php
			EPKB_FAQ_Config_Overview::display_overview( $this->faq_config, $this->feature_specs, $this->form );  	?>
		</div>

		<div class="epkb-config-content" id="epkb-main-page-content" <?php echo $this->show_main_page ? '' : 'style="display: none;"'; ?>>    <?php
			$this->display_faq_main_page_layout_preview();     ?>
		</div>

		<div class="epkb-config-content" id="epkb-archive-page-content" style="display: none;">    <?php
			$this->display_archive_page_layout_preview();     ?>
		</div>

		<input type="hidden" id="epkb_faq_id" value="<?php echo $this->faq_config['id']; ?>"/>   <?php
	}

	/**
	 * Display the Main Page layout preview.
	 *
	 * @param bool $display
	 * @param array $articles_seq_data
	 * @param array $category_seq_data
	 * @return string
	 */
	public function display_faq_main_page_layout_preview( $display=true, $articles_seq_data=array(), $category_seq_data=array() ) {
		global $eckb_is_faq_main_page;

		// retrieve FAQ preview using Current FAQ or Demo FAQ
		if ( isset($_REQUEST['epkb-demo']) || ( isset($_POST['epkb_demo_faq']) && $_POST['epkb_demo_faq'] == "true" ) ) {
			$demo_data = EPKB_FAQ_Demo_Data::get_category_demo_data( $this->faq_config['faq_main_page_layout'], $this->faq_config );
			$category_seq_data = $demo_data['category_seq'];
			$articles_seq_data = $demo_data['article_seq'];
		}

		$eckb_is_faq_main_page = true;   // pretend this is Main Page
		$main_page_output = EPKB_FAQ_Layouts_Setup::output_main_page( $this->faq_config, true, $articles_seq_data, $category_seq_data );
		
		if ( $display ) {
			echo $main_page_output;
		}

		return $main_page_output;
	}

	public function display_archive_page_layout_preview (){	}


	/**************************************************************************************
	 *
	 *                   SIDEBARS: FAQ MAIN PAGE
	 *
	 *************************************************************************************/

    /**
     * Display SIDEBAR for given TOP icon - FAQ Main Page
     */
    private function display_sidebar() {	    ?>

        <form id="epkb-config-config">

            <div class="epkb-sidebar-container" id="epkb-main-page-settings">
                <?php $this->display_faq_main_page_sections(); ?>
            </div>

            <div id='epkb-ajax-in-progress' style="display:none;">
                <?php esc_html__( 'Saving configuration', 'echo-knowledge-base' ); ?> <img class="epkb-ajax waiting" style="height: 30px;" src="<?php echo Echo_Knowledge_Base::$plugin_url . 'img/loading_spinner.gif'; ?>">
            </div>  <?php

	        do_action( 'eckb_additional_output', $this->faq_config );        ?>

        </form>      <?php
    }

	/**
	 * Display all sidebar forms for MAIN PAGE
	 */
	private function display_faq_main_page_sections() {
        echo $this->get_main_page_templates_form();
        echo $this->get_main_page_order_form();
		echo $this->get_main_page_styles_form();
		echo $this->get_main_page_colors_form();
        echo $this->get_main_page_text_form();
		do_action( 'eckb_main_page_sidebar_additional_output', $this->faq_config );
	}

    /**
     * Generate form fields for the MAIN PAGE side bar
     */
    public function get_main_page_templates_form() {

        ob_start();     ?>

        <div class="epkb-config-sidebar" id="epkb-config-main-setup-sidebar">
            <div class="epkb-config-sidebar-options">                        <?php
                $feature_specs = EPKB_FAQ_Config_Specs::get_fields_specification( $this->faq_config['id'] );
                $form = new EPKB_KB_Config_Elements();

                $arg_bn_padding_top    = $feature_specs['templates_for_faq_padding_top'] + array( 'value' => $this->faq_config['templates_for_faq_padding_top'], 'current' => $this->faq_config['templates_for_faq_padding_top'], 'text_class' => 'config-col-6' );
                $arg_bn_padding_bottom = $feature_specs['templates_for_faq_padding_bottom'] + array( 'value' => $this->faq_config['templates_for_faq_padding_bottom'], 'current' => $this->faq_config['templates_for_faq_padding_bottom'], 'text_class' => 'config-col-6' );
                $arg_bn_padding_left   = $feature_specs['templates_for_faq_padding_left'] + array( 'value' => $this->faq_config['templates_for_faq_padding_left'], 'current' => $this->faq_config['templates_for_faq_padding_left'], 'text_class' => 'config-col-6' );
                $arg_bn_padding_right  = $feature_specs['templates_for_faq_padding_right'] + array( 'value' => $this->faq_config['templates_for_faq_padding_right'], 'current' => $this->faq_config['templates_for_faq_padding_right'], 'text_class' => 'config-col-6' );

                $arg_bn_margin_top    = $feature_specs['templates_for_faq_margin_top'] + array( 'value' => $this->faq_config['templates_for_faq_margin_top'], 'current' => $this->faq_config['templates_for_faq_margin_top'], 'text_class' => 'config-col-6' );
                $arg_bn_margin_bottom = $feature_specs['templates_for_faq_margin_bottom'] + array( 'value' => $this->faq_config['templates_for_faq_margin_bottom'], 'current' => $this->faq_config['templates_for_faq_margin_bottom'], 'text_class' => 'config-col-6' );
                $arg_bn_margin_left   = $feature_specs['templates_for_faq_margin_left'] + array( 'value' => $this->faq_config['templates_for_faq_margin_left'], 'current' => $this->faq_config['templates_for_faq_margin_left'], 'text_class' => 'config-col-6' );
                $arg_bn_margin_right  = $feature_specs['templates_for_faq_margin_right'] + array( 'value' => $this->faq_config['templates_for_faq_margin_right'], 'current' => $this->faq_config['templates_for_faq_margin_right'], 'text_class' => 'config-col-6' );

                $form->option_group( $feature_specs, array(
                    'info'              => array( 'templates_for_faq_padding_top', 'templates_for_faq_padding_bottom', 'templates_for_faq_padding_left', 'templates_for_faq_padding_right', 'templates_display_main_page_main_title'  ),
                    'option-heading'    => __( 'Templates', 'echo-knowledge-base' ),
                    'class'             => 'eckb-mm-mp-links-setup-main-template',
                    'inputs'            => array(
	                    '2' => $form->checkbox( $feature_specs['templates_display_main_page_main_title'] + array(
			                    'value'             => $this->faq_config['templates_display_main_page_main_title'],
			                    'id'                => 'templates_display_main_page_main_title',
			                    'input_group_class' => 'config-col-12',
			                    'label_class'       => 'config-col-5',
			                    'input_class'       => 'config-col-2'
		                    ) ),
	                    '4' => $form->multiple_number_inputs(
		                    array(
			                    'id'                => 'templates_for_faq_padding_group',
			                    'input_group_class' => '',
			                    'main_label_class'  => '',
			                    'input_class'       => '',
			                    'label'             => 'Padding( px )'
		                    ),
		                    array( $arg_bn_padding_top, $arg_bn_padding_bottom, $arg_bn_padding_left, $arg_bn_padding_right )
	                    ),
	                    '5' => $form->multiple_number_inputs(
		                    array(
			                    'id'                => 'templates_for_faq_margin_group',
			                    'input_group_class' => '',
			                    'main_label_class'  => '',
			                    'input_class'       => '',
			                    'label'             => 'Margin( px )'
		                    ),
		                    array( $arg_bn_margin_top, $arg_bn_margin_bottom , $arg_bn_margin_left, $arg_bn_margin_right )
	                    ),
                    )
                )); ?>
            </div>
        </div>      <?php

        return ob_get_clean();
    }

    /**
	 * Generate form fields for the side bar
	 */
	public function get_main_page_order_form() {
		ob_start();	    ?>

		<div class="epkb-config-sidebar" id="epkb-config-ordering-sidebar" hidden>
			<div class="epkb-config-sidebar-options">            <?php

            $sequence_widets = array(
                '0' => $this->form->radio_buttons_vertical(
                    $this->feature_specs['categories_display_sequence'] +
                    array(
                        'id'        => 'front-end-columns',
                        'value'     => $this->faq_config['categories_display_sequence'],
                        'current'   => $this->faq_config['categories_display_sequence'],
                        'input_group_class' => 'config-col-12',
                        'main_label_class'  => 'config-col-12',
                        'input_class'       => 'config-col-12',
                        'radio_class'       => 'config-col-12'
                    )
                )
            );

            $sequence_widets[1] = $this->form->radio_buttons_vertical(
                $this->feature_specs['articles_display_sequence'] +
                array(
                    'id'        => 'front-end-columns',
                    'value'     => $this->faq_config['articles_display_sequence'],
                    'current'   => $this->faq_config['articles_display_sequence'],
                    'input_group_class' => 'config-col-12',
                    'main_label_class'  => 'config-col-12',
                    'input_class'       => 'config-col-12',
                    'radio_class'       => 'config-col-12'
                )
            );

            $this->form->option_group( $this->feature_specs, array(
                'option-heading'    => 'Organize Categories and Articles',
                'class'             => 'eckb-mm-mp-links-organize--organize',
                'info' => array( 'categories_display_sequence', 'articles_display_sequence' ),
                'inputs' => $sequence_widets
            ));            ?>
            </div>
        </div>        <?php

		return ob_get_clean();
	}

	/**
	 * Generate form fields for the side bar
	 */
	public function get_main_page_styles_form() {
		ob_start();	    ?>

		<div class="epkb-config-sidebar" id="epkb-config-styles-sidebar">
			<div class="epkb-config-sidebar-options" id="epkb_style_sidebar_options">                <?php
				apply_filters( 'epkb_faq_main_page_style_settings', $this->faq_main_page_layout, $this->faq_config ); ?>
			</div>
		</div>      <?php

		return ob_get_clean();
	}

	/**
	 * Generate form fields for the side bar
	 */
	public function get_main_page_colors_form() {
		ob_start();	    ?>

		<div class="epkb-config-sidebar" id="epkb-config-colors-sidebar">
			<div class="epkb-config-sidebar-options">
				<?php apply_filters( 'epkb_faq_main_page_colors_settings', $this->faq_main_page_layout, $this->faq_config ); ?>
			</div>
		</div>			         <?php

		return ob_get_clean();
	}

	/**
	 * Generate form fields for the side bar
	 */
	public function get_main_page_text_form() {
		ob_start();	    ?>

		<div class="epkb-config-sidebar" id="epkb-config-text-sidebar">
			<div class="epkb-config-sidebar-options">
				<?php apply_filters( 'epkb_faq_main_page_text_settings', $this->faq_main_page_layout, $this->faq_config ); ?>
			</div>
		</div>			     <?php

		return ob_get_clean();
	}


	/**************************************************************************************
	 *
	 *                   OTHERS / SUPPORT FUNCTIONS
	 *
	 *************************************************************************************/

	private function display_list_of_faqs() {

		if ( ! defined('EM' . 'KB_PLUGIN_NAME') ) {
			$faq_name = $this->faq_config[ 'faq_name' ];
			echo '<h1 class="epkb-faq-name">' . esc_html( $faq_name ) . '</h1>';
			return;
		}

		// output the list
		$list_output = '<select class="epkb-faq-name" id="epkb-list-of-faqs">';
		$all_faq_configs = epkb_get_instance()->faq_config_obj->get_faq_configs();
		foreach ( $all_faq_configs as $one_faq_config ) {

			if ( $one_faq_config['id'] !== EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID && EPKB_FAQ_Utilities::is_faq_archived( $one_faq_config['status'] ) ) {
				continue;
			}

			$faq_name = $one_faq_config[ 'faq_name' ];
			$active = ( $this->faq_config['id'] == $one_faq_config['id'] ? 'selected' : '' );
			$tab_url = 'edit.php?post_type=' . EPKB_FAQ_Handler::FAQ_POST_TYPE_PREFIX . $one_faq_config['id'] . '&page=epkb-faq-configuration';

			$list_output .= '<option value="' . $one_faq_config['id'] . '" ' . $active . ' data-faq-admin-url=' . esc_url($tab_url) . '>' . esc_html( $faq_name ) . '</option>';
			$list_output .= '</a>';
		}

		$list_output .= '</select>';

		echo $list_output;
	}

	public static function show_faq_intro_page() {    ?>

		<form id="epkb-enable-faq-layout">

			<!--  FAQ NAME and other global settings -->
			<div class="callout callout_default">
				<h4>FAQ (Beta)</h4>
				<div>Backup your website before proceeding.</div>
				<div class="row">
					<div class="config-col-2">
						<section class="save-settings">    <?php
							$form = new EPKB_HTML_Elements();
							$form->submit_button( 'Enable', 'epkb_save_faq_settings' ); ?>
						</section>
					</div>
				</div>
			</div>

		</form>
		<div class="eckb-bottom-notice-message"></div>      <?php
	}

	/**
	 * Find FAQ Main Page that is not in trash and get its URL.
	 *
	 * @param $faq_config
	 * @return string|<empty>
	 */
	private static function get_first_faq_main_page_url( $faq_config ) {
		$first_page_id = '';
		$faq_main_pages = $faq_config['faq_main_pages'];
		foreach ( $faq_main_pages as $post_id => $post_title ) {
			$first_page_id = $post_id;
			break;
		}

		$first_page_url = empty($first_page_id) ? '' : get_permalink( $first_page_id );

		return is_wp_error( $first_page_url ) ? '' : $first_page_url;
	}
}
