<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Display FAQ Configuration
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */
class EPKB_FAQ_Menu_Configuration {
	
	/**
	 * Displays the FAQ Config page with top panel + sidebar + preview panel
	 */
	public function display_faq_config_page() {

		$faq_enabled = EPKB_FAQ_Utilities::get_wp_option( 'epkb_faq_enabled', false );
		if ( empty($faq_enabled) ) {
			EPKB_FAQ_Config_Page::show_faq_intro_page();
			return;
		}

		$faq_config_page = new EPKB_FAQ_Config_Page();
		$faq_config_page->display_faq_config_page();
	}
}
