<?php

/**
 * Handle saving specific FAQ configuration.
 */
class EPKB_FAQ_Config_Controller {

	public function __construct() {
		add_action( 'wp_ajax_epkb_faq_change_article_category_sequence', array( $this, 'change_article_category_sequence' ) );
		add_action( 'wp_ajax_nopriv_epkb_faq_change_article_category_sequence', array( $this, 'user_not_logged_in' ) );
		add_action( 'wp_ajax_epkb_faq_change_main_page_config_ajax', array( $this, 'change_main_page_config_ajax' ) );
		add_action( 'wp_ajax_nopriv_epkb_faq_change_main_page_config_ajax', array( $this, 'user_not_logged_in' ) );
		add_action( 'wp_ajax_epkb_faq_change_one_config_param_ajax', array( $this, 'change_one_configuration_param' ) );
		add_action( 'wp_ajax_nopriv_epkb_faq_change_one_config_param_ajax', array( $this, 'user_not_logged_in' ) );
		add_action( 'wp_ajax_epkb_faq_save_faq_config_changes', array( $this, 'save_faq_config_changes_in_db' ) );
		add_action( 'wp_ajax_nopriv_epkb_faq_save_faq_config_changes', array( $this, 'user_not_logged_in' ) );
		add_action( 'wp_ajax_epkb_save_faq_settings', array( $this, 'enable_faq_layout' ) );
	}

	/**
	 * Triggered when user changes article or category sequence drop down
	 */
	public function change_article_category_sequence() {

		// verify that request is authentic
		if ( empty( $_REQUEST['_wpnonce_epkb_save_faq_config'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce_epkb_save_faq_config'], '_wpnonce_epkb_save_faq_config' ) ) {
			$this->ajax_show_error_die( __( 'Sequence not changed. First refresh your page', 'echo-knowledge-base' ) );
		}

		// ensure user has correct permissions
		if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) {
			$this->ajax_show_error_die( __( 'You do not have permission to edit this knowledge base', 'echo-knowledge-base' ) );
		}

		// retrieve user input
		if ( empty( $_POST['categories_sequence_new_value'] ) || empty($_POST['articles_sequence_new_value']) ) {
			$this->ajax_show_error_die( __( 'Invalid parameters. Please refresh your page', 'echo-knowledge-base' ) );
		}

		// retrieve FAQ ID we are saving
		$faq_id = empty( $_POST['epkb_faq_id'] ) ? '' : EPKB_FAQ_Utilities::sanitize_get_id( $_POST['epkb_faq_id'] );
		if ( empty($faq_id) || is_wp_error( $faq_id ) ) {
			EPKB_Logging::add_log( "invalid FAQ id", $faq_id );
			$this->ajax_show_error_die( __( 'This page is outdated. Please refresh your browser', 'echo-knowledge-base' ) );
		}

		// retrieve current FAQ configuration
		$new_faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
		if ( is_wp_error( $new_faq_config ) ) {
			$this->ajax_show_error_die( __( 'Error occurred. Please refresh your browser and try again.', 'echo-knowledge-base' ) );
		}

		$articles_sequence_new_value = $new_faq_config['articles_display_sequence'];
		$categories_sequence_new_value = $new_faq_config['categories_display_sequence'];

		$config_seq = new EPKB_FAQ_Config_Sequence();
		$new_sequence = $config_seq->get_new_sequence();
		if ( $new_sequence === false ) {
			EPKB_Logging::add_log( "Could not retrieve new sequence" );
			$this->ajax_show_error_die( __( 'Error occurred. Could not retrieve new sequence.', 'echo-knowledge-base' ) );
		}

		// SEQUENCE ARTICLES and CATEGORIES
		$is_demo_data = isset($_POST['epkb_demo_faq']) && $_POST['epkb_demo_faq'] == "true";
		if ( $is_demo_data ) {
			$article_seq = array();
			$category_seq = array();
		} else {
			// get non-custom ordering regardless (default to by title if this IS custom order)
			$articles_order_method = $articles_sequence_new_value == 'user_sequenced' ? 'alphabetical-title' : $articles_sequence_new_value;
			$articles_admin = new EPKB_FAQ_Articles_Admin();
			$article_seq = $articles_admin->get_articles_sequence_non_custom( $faq_id, $articles_order_method );
			if ( $article_seq === false ) {
				$this->ajax_show_error_die( __( 'Error occurred. Please refresh your browser and try again. (1)', 'echo-knowledge-base' ) );
			}

			// ARTICLES: change to custom sequencde if necessary
			if ( $articles_sequence_new_value == 'user-sequenced' ) {
				$new_articles_ids_obj = $config_seq->update_articles_order( $faq_id, $new_sequence, new EPKB_FAQ_Articles_Array( $article_seq ) );
				if ( $new_articles_ids_obj === false ) {
					$this->ajax_show_error_die( __( 'This page is outdated. Please refresh your browser (2)', 'echo-knowledge-base' ) );
				}
				$article_seq = $new_articles_ids_obj->ids_array;
			}

			// get non-custom ordering regardless (default to by title if this IS custom order)
			$categories_order_method = $categories_sequence_new_value == 'user_sequenced' ? 'alphabetical-title' : $categories_sequence_new_value;
			$cat_admin = new EPKB_FAQ_Categories_Admin();
			$category_seq = $cat_admin->get_categories_sequence_non_custom( $faq_id, $categories_order_method );
			if ( $category_seq === false ) {
				$this->ajax_show_error_die( __( 'Error occurred. Please refresh your browser and try again. (3)', 'echo-knowledge-base' ) );
			}

			// CATEGORIES: change to custom sequence if necessary
			if ( $categories_sequence_new_value == 'user-sequenced' ) {
				$new_cat_ids_obj = $config_seq->update_categories_order( $faq_id, $new_sequence, new EPKB_FAQ_Categories_Array( $category_seq ) );
				if ( $new_cat_ids_obj === false ) {
					$this->ajax_show_error_die( __( 'This page is outdated. Please refresh your browser (BD06)', 'echo-knowledge-base' ) );
				}
				$category_seq = $new_cat_ids_obj->ids_array;
			}

			if ( ! $article_seq || ! $category_seq ) {
				$this->ajax_show_error_die( __( 'Error occurred. Please refresh your browser and try again. (4)', 'echo-knowledge-base' ) );
			}
		}

		$faq_config_page = new EPKB_FAQ_Config_Page( $new_faq_config );
		$output = $faq_config_page->display_faq_main_page_layout_preview( false, $article_seq, $category_seq );

		// add to output <script>
		//$output = epkb_frontend_faq_theme_styles_now( $new_faq_config ) . $output;
		
		$msg = __( ' Sequence udpated but not saved.', 'echo-knowledge-base' );
		wp_die( json_encode( array( 'faq_page_output' => $output, 'message' => $this->get_config_message_box( $msg, '', 'success') ) ) );
	}

	public function change_one_configuration_param() {

		// don't need nonce for preview

		// ensure user has correct permissions
		if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) {
			$this->ajax_show_error_die(__( 'You do not have permission to edit this knowledge base', 'echo-knowledge-base' ));
		}

		// retrieve FAQ ID we are saving
		$faq_id = empty($_POST['epkb_faq_id']) ? '' : EPKB_FAQ_Utilities::sanitize_get_id( $_POST['epkb_faq_id'] );
		if ( empty($faq_id) || is_wp_error( $faq_id ) ) {
			EPKB_Logging::add_log( "invalid FAQ id", $faq_id );
			$this->ajax_show_error_die(__( 'This page is outdated. Please refresh your browser', 'echo-knowledge-base' ));
		}

		// retrieve current FAQ configuration
		$orig_faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
		if ( is_wp_error( $orig_faq_config ) ) {
			$this->ajax_show_error_die(__( 'Error occurred. Please refresh your browser and try again.', 'echo-knowledge-base' ));
		}

		// retrieve user input
		$new_faq_config = $this->populate_faq_config_from_form( $faq_id, $orig_faq_config );

		$faq_config_page = new EPKB_FAQ_Config_Page( $new_faq_config );

		$output = $faq_config_page->display_faq_main_page_layout_preview( false );

		wp_die( json_encode( array( 'faq_info_panel_output' => $output, 'message' => '' ) ) );
	}

	/**
	 * Triggered when user changes style, search box style or colors on MAIN PAGE
	 */
	public function change_main_page_config_ajax() {

		// don't need nonce for preview

		// ensure user has correct permissions
		if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) {
			$this->ajax_show_error_die(__( 'You do not have permission to edit this knowledge base', 'echo-knowledge-base' ));
		}

		// validate user input
		if ( empty($_POST['target_type']) || empty($_POST['target_name']) || empty($_POST['epkb_chosen_main_page_layout']) ) {
			$this->ajax_show_error_die(__( 'Invalid parameters. Please refresh your page', 'echo-knowledge-base' ));
		}

		// retrieve FAQ ID we are saving
		$faq_id = empty($_POST['epkb_faq_id']) ? '' : EPKB_FAQ_Utilities::sanitize_get_id( $_POST['epkb_faq_id'] );
		if ( empty($faq_id) || is_wp_error( $faq_id ) ) {
			EPKB_Logging::add_log( "invalid FAQ id", $faq_id );
			$this->ajax_show_error_die(__( 'This page is outdated. Please refresh your browser', 'echo-knowledge-base' ));
		}

		// retrieve current FAQ configuration
		$current_faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
		if ( is_wp_error( $current_faq_config ) ) {
			$this->ajax_show_error_die(__( 'Error occurred. Please refresh your browser and try again.', 'echo-knowledge-base' ));
		}

		$target_type = sanitize_text_field( $_POST['target_type'] );
		if ( ! in_array( $target_type, array('layout', 'style', 'search box style', 'colors', 'demo' ) ) ) {
			$this->ajax_show_error_die(__( 'Invalid type. Please refresh your page', 'echo-knowledge-base' ));
		}

		// retrieve user changes
		$faq_config = $this->populate_faq_config_from_form( $faq_id, $current_faq_config );

		// temporary update Main Page Layout to currently chosen layout
		$chosen_main_page_layout = sanitize_text_field( $_POST['epkb_chosen_main_page_layout'] );
		if ( ! in_array($chosen_main_page_layout, EPKB_FAQ_Config_Layouts::get_main_page_layout_names()) ) {
			$this->ajax_show_error_die(__( 'Invalid parameters. Please refresh your page. (6)', 'echo-knowledge-base' ));
		}
		$faq_config['faq_main_page_layout'] = $chosen_main_page_layout;

		$target_name = sanitize_text_field( $_POST['target_name'] );
		if ( ( $target_type == 'style' && ! in_array($target_name, EPKB_FAQ_Config_Layouts::get_main_page_style_names( $faq_config )) ) ||
		     ( $target_type == 'search_box_style' && ! in_array($target_name, EPKB_FAQ_Config_Layouts::get_search_box_style_names( $faq_config )) ) ||
		     ( $target_type == 'colors' && ! in_array($target_name, EPKB_FAQ_Config_Layouts::get_colors_names()) ) ) {
			$this->ajax_show_error_die(__( 'Invalid parameters. Please refresh your page.', 'echo-knowledge-base' ));
		}

		// add filters for core layouts and colors
		EPKB_FAQ_Config_Layouts::register_faq_config_hooks();

		// get given layout or color settings input
		$style_tab_output = 'NONE';
		$colors_tab_output = 'NONE';
        $faq_mega_menu = 'NONE';
		$message = '';
		$overview_page_output = '';
		$ordering_output = '';
		$text_output = '';
		$faq_config_page = new EPKB_FAQ_Config_Page( $faq_config );

		if ( $target_type == 'layout' ) {

			// get Ordering tab
			$ordering_output = $faq_config_page->get_main_page_order_form();

			// get Overview tab
			ob_start();
			EPKB_FAQ_Config_Overview::display_overview( $faq_config_page->faq_config, $faq_config_page->feature_specs, $faq_config_page->form );
			$overview_page_output = ob_get_clean();

			// update Text tab based on current layout (Sidebar has its own set)
			$text_output = $faq_config_page->get_main_page_text_form();

            $faq_config_page = new EPKB_FAQ_Config_Page( $faq_config );

			// get Mega Menu
            ob_start();
            $faq_config_page->display_mega_menu();
            $faq_mega_menu = ob_get_clean();
		}

		// get new style
		if ( $target_type == 'layout' || $target_type == 'style' ) {
			$target_name = $target_type == 'layout' ? EPKB_FAQ_Config_Layouts::FAQ_DEFAULT_LAYOUT_STYLE : $target_name;
			$reset_style_config  = EPKB_FAQ_Config_Layouts::get_main_page_style_set( $chosen_main_page_layout, $target_name );
			$faq_config = array_merge($faq_config, $reset_style_config);
			$faq_config_page = new EPKB_FAQ_Config_Page( $faq_config );
			$style_tab_output = $faq_config_page->get_main_page_styles_form();
			$message .= __( 'Style was set to', 'echo-knowledge-base' ) . ' ' . ucfirst($target_name) . '. ' ;
		}

		// get new colors
		if ( $target_type == 'layout' ||  $target_type == 'colors' ) {
			$target_name = $target_type == 'layout' ? EPKB_FAQ_Config_Layouts::FAQ_DEFAULT_COLORS_STYLE : $target_name;
			$reset_color_config  = EPKB_FAQ_Config_Layouts::get_main_page_colors_set( $chosen_main_page_layout, $target_name );
			$faq_config = array_merge($faq_config, $reset_color_config);
			$faq_config_page = new EPKB_FAQ_Config_Page( $faq_config );
			$colors_tab_output = $faq_config_page->get_main_page_colors_form();
			$message = $this->get_color_change_msg( $target_name );
		}

		if ( empty($style_tab_output) || empty($colors_tab_output) ) {
			$this->ajax_show_error_die( $this->get_target_error_msg( $target_type ) );
		}

		// update Main Page layout
		$main_page_layout = $faq_config_page->display_faq_main_page_layout_preview( false );

		$message .= __( 'Configuration NOT saved. ', 'echo-knowledge-base' );

		// add to output <script>
		// $main_page_layout = epkb_frontend_faq_theme_styles_now( $faq_config ) . $main_page_layout;

		// we are done here
		wp_die( json_encode( array( 'overview_page_output' => $overview_page_output, 'faq_mega_menu' => $faq_mega_menu, 'faq_main_page_output' => $main_page_layout,
		                            'ordering_output' => $ordering_output, 'style_tab_output' => $style_tab_output, 'colors_tab_output' => $colors_tab_output,
		                            'main_page_text_output' => $text_output, 'message' => $this->get_config_message_box( $message, '', 'attention' ) ) ) );
	}

	private function get_color_change_msg( $target_name ) {
		$option_name = substr($target_name, -1);
		$option_name = empty($option_name) ? '' : ', ' . __( 'Option', 'echo-knowledge-base' ) . $option_name;
		$theme_name = substr($target_name, 0, strlen($target_name) - 1);
		$color_theme_name = ( empty($theme_name) ? $target_name : $theme_name ) . $option_name;
		return 'Colors were set to ' . ucfirst($color_theme_name) . '. ';
	}

	/**
	 * Triggered when user submits changes to FAQ configuration
	 */
	public function save_faq_config_changes_in_db() {

		// verify that the request is authentic
		if ( empty( $_REQUEST['_wpnonce_epkb_save_faq_config'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce_epkb_save_faq_config'], '_wpnonce_epkb_save_faq_config' ) ) {
			$this->ajax_show_error_die(__( 'Settings not saved. First refresh your page', 'echo-knowledge-base' ));
		}

		// ensure user has correct permissions
		if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) {
			$this->ajax_show_error_die(__( 'You do not have permission to edit this knowledge base', 'echo-knowledge-base' ));
		}

		// retrieve FAQ ID we are saving
		$faq_id = empty($_POST['epkb_faq_id']) ? '' : EPKB_FAQ_Utilities::sanitize_get_id( $_POST['epkb_faq_id'] );
		if ( empty($faq_id) || is_wp_error( $faq_id ) ) {
			EPKB_Logging::add_log( "invalid FAQ id", $faq_id );
			$this->ajax_show_error_die(__( 'This page is outdated. Please refresh your browser', 'echo-knowledge-base' ));
		}

		// core handles only default FAQ
		if ( $faq_id != EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID && ! defined( 'E' . 'MKB_PLUGIN_NAME' ) ) {
			EPKB_Logging::add_log("received invalid faq_id when saving config. (x5)", $faq_id );
			$this->ajax_show_error_die(__( 'Ensure that Multiple KB add-on is active and refresh this page.', 'echo-knowledge-base' ));
			return;
		}

		// retrieve current FAQ configuration
		$orig_faq_config = epkb_get_instance()->faq_config_obj->get_faq_config( $faq_id );
		if ( is_wp_error( $orig_faq_config ) ) {
			$this->ajax_show_error_die(__( 'Error occurred. Please refresh your browser and try again.', 'echo-knowledge-base' ));
		}

		// retrieve user input
		$form_fields = empty($_POST['form']) ? array() : EPKB_FAQ_Utilities::sanitize_form( $_POST['form'] );
		if ( empty($form_fields) ) {
			EPKB_Logging::add_log("form fields missing");
			$this->ajax_show_error_die(__( 'Form fields missing. Please refresh your browser', 'echo-knowledge-base' ));
		} else if ( count($form_fields) < 100 ) {
			EPKB_Logging::add_log("Found FAQ configuration is incomplete", count($form_fields));
			$this->ajax_show_error_die(__( 'Some form fields are missing. Please refresh your browser and try again or contact support', 'echo-knowledge-base' ));
		}

		$faq_field_specs = EPKB_FAQ_Config_Specs::get_fields_specification( $faq_id );
		$input_handler = new EPKB_Input_Filter();
		$new_faq_config = $input_handler->retrieve_form_fields( $form_fields, $faq_field_specs, $orig_faq_config );

		// let add-ons to process the input
		$result = apply_filters( 'epkb_faq_config_save_input', '', $faq_id, $form_fields, $new_faq_config['faq_main_page_layout'] );
		if ( is_wp_error( $result ) ) {
			/* @var $result WP_Error */
			$message = $result->get_error_data();
			if ( empty($message) ) {
				$this->ajax_show_error_die( $result->get_error_message(), __( 'Could not save the new configuration (2)', 'echo-knowledge-base' ) );
			} else {
				$this->ajax_show_error_die( $this->generate_error_summary( $message ), __( 'Configuration NOT saved due to following problems:', 'echo-knowledge-base' ) );
			}
		}

		// ensure faq id is preserved
		$new_faq_config['id'] = $faq_id;

		// sanitize and save configuration in the database.
		$result = epkb_get_instance()->faq_config_obj->update_faq_configuration( $faq_id, $new_faq_config );
		if ( is_wp_error( $result ) ) {
			/* @var $result WP_Error */
			$message = $result->get_error_data();
			if ( empty($message) ) {
				$this->ajax_show_error_die( $result->get_error_message(), __( 'Could not save the new configuration (3)', 'echo-knowledge-base' ) );
			} else {
				$this->ajax_show_error_die( $this->generate_error_summary( $message ), __( 'Configuration NOT saved due to following problems:', 'echo-knowledge-base' ) );
			}
		}

		// save category icons
		$result = $this->save_categories_icons( $faq_id, $form_fields );
		if ( is_wp_error( $result ) ) {
			$this->ajax_show_error_die( $result->get_error_message(), __( 'Could not save category icons', 'echo-knowledge-base' ) );
		}

		// update sequence of articles and categories
		$sync_sequence = new EPKB_FAQ_Config_Sequence();
		$sync_sequence->update_articles_sequence( $faq_id, $new_faq_config );
		$sync_sequence->update_categories_sequence( $faq_id, $new_faq_config );

		// some settings require page reload
		$reload = $this->is_page_reload( $orig_faq_config, $new_faq_config, $faq_field_specs);

		// we are done here
		$this->ajax_show_info_die( $reload ? __( 'Reload Settings saved. PAGE WILL RELOAD NOW.', 'echo-knowledge-base' ) : __( 'Settings saved', 'echo-knowledge-base' ) );
	}

	private function populate_faq_config_from_form( $faq_id, $orig_faq_config ) {

		// get user input
		$form_fields = empty($_POST['form']) ? array() : EPKB_FAQ_Utilities::sanitize_form( $_POST['form'] );

		$feature_specs = EPKB_FAQ_Config_Specs::get_fields_specification( $faq_id );
		$input_handler = new EPKB_Input_Filter();
		$new_faq_config = $input_handler->retrieve_form_fields( $form_fields, $feature_specs, $orig_faq_config );
		$new_faq_config = $this->retrieve_add_on_faq_config( $faq_id, $form_fields, $new_faq_config );

		$articles_display_sequence = empty($_POST['articles_sequence_new_value']) ? '' : EPKB_FAQ_Utilities::sanitize_english_text( $_POST['articles_sequence_new_value'] );
		$new_faq_config['articles_display_sequence'] = empty($articles_display_sequence) ? $new_faq_config['articles_display_sequence'] : $articles_display_sequence;
		$categories_display_sequence = empty($_POST['categories_sequence_new_value']) ? '' : EPKB_FAQ_Utilities::sanitize_english_text( $_POST['categories_sequence_new_value'] );
		$new_faq_config['categories_display_sequence'] = empty($categories_display_sequence) ? $new_faq_config['categories_display_sequence'] : $categories_display_sequence;

		return $new_faq_config;
	}

	/**
	 * Merge core FAQ config with add-ons FAQ config
	 *
	 * @param $faq_id
	 * @param $form_fields
	 * @param $faq_config
	 * @return array
	 */
	private function retrieve_add_on_faq_config( $faq_id, $form_fields, $faq_config ) {
		// get add-on configuration from user changes if applicable
		$add_on_config = apply_filters( 'epkb_faq_config_get_add_on_input', array(), $faq_id, $form_fields );
		if ( ! is_array($add_on_config) || is_wp_error( $add_on_config )) {
			$this->ajax_show_error_die(__( 'Could not change FAQ configuration. (8)', 'echo-knowledge-base' ));
		}

		// merge core and add-on configuration
		return array_merge( $add_on_config, $faq_config );
	}

	private function is_page_reload( $orig_settings, $new_settings, $spec ) {

		$diff = EPKB_FAQ_Utilities::diff_two_dimentional_arrays( $new_settings, $orig_settings );
		foreach( $diff as $key => $value ) {
			if ( ! empty($spec[$key]['reload']) ) {
				return true;
			}
		}

		return false;
	}

	private function generate_error_summary( $errors ) {

		$output = '';

		if ( empty( $errors ) || ! is_array( $errors )) {
			return $output . __( 'unknown error', 'echo-knowledge-base' ) . ' (344)';
		}

		$output .= '<ol>';
		foreach( $errors as $error ) {
			$output .= '<li>' . wp_kses( $error, array('strong' => array('style' => array()),'div' => array('style' => array()),'p' => array()) ) . '</li>';
		}
		$output .= '</ol>';

		return $output;
	}

	public function user_not_logged_in() {
		$this->ajax_show_error_die( '<p>' . __( 'You are not logged in. Refresh your page and log in', 'echo-knowledge-base' ) . '.</p>', __( 'Cannot save your changes', 'echo-knowledge-base' ) );
	}

	/**
	 * AJAX: Used on response back to JS. will call wp_die()
	 *
	 * @param string $message
	 * @param string $title
	 * @param string $type
	 */
	private function ajax_show_info_die( $message, $title='', $type='success' ) {
		wp_die( json_encode( array( 'message' => $this->get_config_message_box( $message, $title, $type) ) ) );
	}

	/**
	 * AJAX: Used on response back to JS. will call wp_die()
	 *
	 * @param $message
	 * @param string $title
	 */
	private function ajax_show_error_die( $message, $title='' ) {
		wp_die( json_encode( array( 'error' => true, 'message' => $this->get_config_message_box( $message, $title, 'error') ) ) );
	}

	/**
	 * Show info or error message to the user
	 *
	 * @param $message
	 * @param string $title
	 * @param string $type
	 *
	 * @return string
	 */
	private function get_config_message_box( $message, $title='', $type='success' ) {
		$title = empty($title) ? '' : '<h4>' . $title . '</h4>';
		$message = empty($message) ? '' : $message;
		return
			"<div class='eckb-bottom-notice-message'>
				<div class='contents'>
					<span class='$type'>
						$title
						<p> " . wp_kses_post($message) . "</p>
					</span>
				</div>
				<div class='epkb-close-notice icon_close'></div>
			</div>";
	}

	private function get_target_error_msg( $target_type ) {
		 if ( $target_type == 'style' ) {
			$i18_message = __( 'Could not switch Style.', 'echo-knowledge-base' );
		} else if ( $target_type == 'colors' ) {
			$i18_message = __( 'Could not switch Colors.', 'echo-knowledge-base' );
		} else {
			$i18_message = __( 'Could not switch Search Box Style.', 'echo-knowledge-base' );
		}

		return $i18_message;
	}

	/**
	 * Update categories icons when user is saving FAQ configuration
	 *
	 * @param $faq_id
	 * @param $form_fields
	 *
	 * @return true|WP_Error
	 */
	private function save_categories_icons( $faq_id, $form_fields ) {

		// if user did not modify icons then don't save them
		$save_icons = empty($form_fields['epkb_save_categories_icons']) ? '' : $form_fields['epkb_save_categories_icons'];
		$is_save_icons = sanitize_text_field( $save_icons );
		if ( empty($is_save_icons) ) {
			return true;
		}

		// validate input
		if ( ! isset($form_fields['epkb_categories_icons']) ) {
			return new WP_Error(11, 'Invalid input (312)');
		}

		// convert input to array of category ids and icon names; ignore empty value
		$user_input = sanitize_text_field( $form_fields['epkb_categories_icons'] );
		if ( empty($user_input) ) {
			return true;
		}

		// parse the input
		$user_input = explode('.', $user_input);
		if ( $user_input === false ) {
			return new WP_Error(11, 'Invalid input (122)');
		}

		$new_categories_icons = array();
		foreach( $user_input as $name_category ) {
			$name_category = explode('=', $name_category);
			if ( $name_category === false || sizeof($name_category) != 2 ) {
				return new WP_Error(11, 'Invalid input (362)');
			}
			$icon_name = $name_category[0];
			$category_id = $name_category[1];
			if ( ! EPKB_FAQ_Utilities::is_positive_or_zero_int( $category_id ) || strlen($icon_name) > 50 ||
			     ( EPKB_FAQ_Utilities::substr($icon_name, 0, strlen('ep_font_icon_')) !== 'ep_font_icon_' &&
			       EPKB_FAQ_Utilities::substr($icon_name, 0, strlen('fa-')) !== 'fa-' ) ) {
				return new WP_Error(11, 'Invalid input (323)');
			}
			$new_categories_icons[$category_id] = $icon_name;
		}

		// save new icons
		$result = EPKB_FAQ_Utilities::save_faq_option( $faq_id, EPKB_FAQ_Icons::CATEGORIES_ICONS, $new_categories_icons, true );
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		return true;
	}

	/**
	 * Enable FAQ layout.
	 */
	public function enable_faq_layout() {

		// verify that request is authentic
		if ( ! isset( $_REQUEST['_wpnonce_epkb_save_faq_settings'] ) || !wp_verify_nonce( $_REQUEST['_wpnonce_epkb_save_faq_settings'], '_wpnonce_epkb_save_faq_settings' ) ) {
			EPKB_FAQ_Utilities::ajax_show_error_die( __( 'Refresh your page', 'echo-knowledge-base' ) );
		}

		// ensure user has correct permissions
		if ( ! current_user_can( 'manage_options' ) ) {
			EPKB_FAQ_Utilities::ajax_show_error_die( __( 'You do not have permission.', 'echo-knowledge-base' ) );
		}

		$is_faq_enabled = EPKB_FAQ_Utilities::post('epkb_faq_enabled');
		if ( $is_faq_enabled !== 'true' ) {
			EPKB_FAQ_Utilities::ajax_show_error_die( __( 'Refresh your page.', 'echo-knowledge-base' ) );
		}

		EPKB_FAQ_Utilities::save_wp_option( 'epkb_faq_enabled', $is_faq_enabled, true );

		// create default FAQ
		EPKB_FAQ_Handler::add_new_faq_knowledge_base( EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID, 'FAQ' );  // ignore errors

		// we are done here
		EPKB_FAQ_Utilities::ajax_show_info_die( __( 'FAQ Layout is now ' . ( $is_faq_enabled ? 'on' : 'off' ), 'echo-knowledge-base' ) );
	}
}