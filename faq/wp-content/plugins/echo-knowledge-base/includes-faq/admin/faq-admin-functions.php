<?php

/*** GENERIC NON-FAQ functions  ***?

/**
 * When a post is added/updated, check if it contains FAQ main page shortcode. If it does, add the page to FAQ config.
 *
 * @param int $post_id The ID of the post being saved.
 * @param object $post The post object.
 */
function epkb_faq_save_any_post( $post_id, $post ) {

	// ignore autosave/revision which is not article submission; same with ajax and bulk edit
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_autosave( $post_id ) || ( defined( 'DOING_AJAX') && DOING_AJAX ) || isset( $_REQUEST['bulk_edit'] ) ) {
		return;
	}

	// return if this page does not have FAQ shortcode
	$faq_id = EPKB_FAQ_Handler::get_faq_id_from_faq_main_shortcode( $post );
	if ( empty( $faq_id ) ) {
		return;
	}

	// core handles only default FAQ
	if ( $faq_id != EPKB_FAQ_Config_DB::DEFAULT_FAQ_ID && ! defined( 'E' . 'MKB_PLUGIN_NAME' ) ) {
		return;
	}

	// update FAQ config if needed
	$faq_main_pages = epkb_get_instance()->faq_config_obj->get_value( $faq_id, 'faq_main_pages', null );
	if ( $faq_main_pages === null || ! is_array($faq_main_pages) ) {
		EPKB_Logging::add_log('Could not update FAQ Main Pages (2)', $faq_id);
		return;
	}

	// don't update if the page is not relevant and not stored
	if ( in_array($post->post_status, array('inherit', 'trash')) ) {
		if ( ! isset($faq_main_pages[$post_id]) ) {
			return;
		}

		// don't update if the page is stored already
	} else if ( in_array($post_id, array_keys($faq_main_pages)) && $faq_main_pages[$post_id] == $post->post_title ) {
		return;
	}

	// update list of FAQ Main Pages
	if ( in_array($post->post_status, array('inherit', 'trash')) ) {
		unset($faq_main_pages[$post_id]);
	} else {
		$faq_main_pages[$post_id] = $post->post_title;   // post_title not used
	}

	// sanitize and save configuration in the database.
	$result = epkb_get_instance()->faq_config_obj->set_value( $faq_id, 'faq_main_pages', $faq_main_pages );
	if ( is_wp_error( $result ) ) {
		EPKB_Logging::add_log('Could not update FAQ Main Pages', $faq_id);
		return;
	}
}
add_action( 'save_post', 'epkb_faq_save_any_post', 10, 2 );
