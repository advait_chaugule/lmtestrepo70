jQuery(document).ready(function($) {

	// Enable FAQ
	$( '#epkb_save_faq_settings' ).on( 'click', function (e) {
		e.preventDefault();  // do not submit the form
		var msg = '';

		var postData = {
			action: 'epkb_save_faq_settings',
			_wpnonce_epkb_save_faq_settings: $('#_wpnonce_epkb_save_faq_settings').val(),
			epkb_kb_id: $('#epkb_kb_id').val(),
			epkb_faq_enabled: true
		};

		$.ajax({
			type: 'POST',
			dataType: 'json',
			data: postData,
			url: ajaxurl,
			beforeSend: function (xhr)
			{
				//noinspection JSUnresolvedVariable
				$('#epkb-ajax-in-progress').text(epkb_vars.save_config).dialog('open');
			}

		}).done(function (response)
		{
			response = ( response ? response : '' );
			if ( ! response.error && typeof response.message !== 'undefined' )
			{
				msg = response.message;

			} else {
				//noinspection JSUnresolvedVariable
				msg = response.message ? response.message : epkb_admin_notification('', epkb_vars.reload_try_again, 'error');
			}

		}).fail(function (response, textStatus, error)
		{
			//noinspection JSUnresolvedVariable
			msg = ( error ? ' [' + error + ']' : epkb_vars.unknown_error );
			//noinspection JSUnresolvedVariable
			msg = epkb_admin_notification(epkb_vars.not_saved + ' ' + epkb_vars.msg_try_again, msg, 'error');

		}).always(function ()
		{
			$('#epkb-ajax-in-progress').dialog('close');

			if ( msg ) {
				$('.eckb-bottom-notice-message').replaceWith(msg);
				$("html, body").animate({scrollTop: 0}, "slow");
			}
		});
	});


	/********************************************************************************************
	 *
	 *                OTHER
	 *
	 ********************************************************************************************/

	// SHOW INFO MESSAGES
	function epkb_admin_notification( $title, $message , $type ) {
		return '<div class="eckb-bottom-notice-message">' +
			'<div class="contents">' +
			'<span class="' + $type + '">' +
			($title ? '<h4>' + $title + '</h4>' : '' ) +
			($message ? $message : '') +
			'</span>' +
			'</div>' +
			'<div class="epkb-close-notice fa fa-window-close"></div>'+
			'</div>';
	}

});