-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2018 at 06:42 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_wp_trash_meta_status', '1'),
(2, 1, '_wp_trash_meta_time', '1545115752');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-12-18 05:17:23', '2018-12-18 05:17:23', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'trash', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wordpress', 'yes'),
(2, 'home', 'http://localhost/wordpress', 'yes'),
(3, 'blogname', 'ACMT FAQ', 'yes'),
(4, 'blogdescription', 'Help doc for ACMT users', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'kamal.asawara@learningmate.copm', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:74:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:43:\"quick-and-easy-faqs/quick-and-easy-faqs.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'twentynineteen', 'yes'),
(41, 'stylesheet', 'twentynineteen', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '43764', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:5:{i:1545286644;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1545288111;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1545326244;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1545369469;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}}', 'yes'),
(120, '_site_transient_timeout_browser_0ac1f9240df96b3586c220faef490724', '1545715070', 'no'),
(121, '_site_transient_browser_0ac1f9240df96b3586c220faef490724', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"71.0.3578.98\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(125, 'can_compress_scripts', '1', 'no'),
(138, 'nav_menu_options', 'a:1:{s:8:\"auto_add\";a:1:{i:0;i:2;}}', 'yes'),
(144, 'recently_activated', 'a:1:{s:22:\"ultimate-faqs/Main.php\";i:1545117584;}', 'yes'),
(148, 'WPLANG', '', 'yes'),
(149, 'new_admin_email', 'kamal.asawara@learningmate.copm', 'yes'),
(153, 'category_children', 'a:0:{}', 'yes'),
(171, 'EWD_UFAQ_Toggle', 'Yes', 'yes'),
(172, 'EWD_UFAQ_Category_Toggle', 'No', 'yes'),
(173, 'EWD_UFAQ_Category_Accordion', 'No', 'yes'),
(174, 'EWD_UFAQ_Expand_Collapse_All', 'No', 'yes'),
(175, 'EWD_UFAQ_FAQ_Accordion', 'No', 'yes'),
(176, 'EWD_UFAQ_Hide_Categories', 'No', 'yes'),
(177, 'EWD_UFAQ_Hide_Tags', 'No', 'yes'),
(178, 'EWD_UFAQ_Scroll_To_Top', 'Yes', 'yes'),
(179, 'EWD_UFAQ_Display_All_Answers', 'No', 'yes'),
(180, 'EWD_UFAQ_Display_Author', 'Yes', 'yes'),
(181, 'EWD_UFAQ_Display_Date', 'Yes', 'yes'),
(182, 'EWD_UFAQ_Display_Back_To_Top', 'No', 'yes'),
(183, 'EWD_UFAQ_Include_Permalink', 'Yes', 'yes'),
(184, 'EWD_UFAQ_Permalink_Type', 'SamePage', 'yes'),
(185, 'EWD_UFAQ_Show_TinyMCE', 'Yes', 'yes'),
(186, 'EWD_UFAQ_Comments_On', 'Yes', 'yes'),
(187, 'EWD_UFAQ_Access_Role', 'edit_posts', 'yes'),
(188, 'EWD_UFAQ_Display_Style', 'One', 'yes'),
(189, 'EWD_UFAQ_Color_Block_Shape', 'Square', 'yes'),
(190, 'EWD_UFAQ_Page_Type', 'Load_More', 'yes'),
(191, 'EWD_UFAQ_FAQ_Ratings', 'No', 'yes'),
(192, 'EWD_UFAQ_WooCommerce_FAQs', 'No', 'yes'),
(193, 'EWD_UFAQ_Use_Product', 'Yes', 'yes'),
(194, 'EWD_UFAQ_Reveal_Effect', 'none', 'yes'),
(195, 'EWD_UFAQ_Pretty_Permalinks', 'No', 'yes'),
(196, 'EWD_UFAQ_Allow_Proposed_Answer', 'No', 'yes'),
(197, 'EWD_UFAQ_Submit_Custom_Fields', 'No', 'yes'),
(198, 'EWD_UFAQ_Submit_Question_Captcha', 'No', 'yes'),
(199, 'EWD_UFAQ_Admin_Question_Notification', 'No', 'yes'),
(200, 'EWD_UFAQ_Submit_FAQ_Email', '0', 'yes'),
(201, 'EWD_UFAQ_Auto_Complete_Titles', 'Yes', 'yes'),
(202, 'EWD_UFAQ_Highlight_Search_Term', 'No', 'yes'),
(203, 'EWD_UFAQ_Slug_Base', 'ufaqs', 'yes'),
(204, 'EWD_UFAQ_FAQ_Elements', 'a:10:{i:0;s:11:\"Author_Date\";i:1;s:4:\"Body\";i:2;s:13:\"Custom_Fields\";i:3;s:10:\"Categories\";i:4;s:4:\"Tags\";i:5;s:7:\"Ratings\";i:6;s:12:\"Social_Media\";i:7;s:9:\"Permalink\";i:8;s:8:\"Comments\";i:9;s:11:\"Back_To_Top\";}', 'yes'),
(205, 'EWD_UFAQ_Group_By_Category', 'No', 'yes'),
(206, 'EWD_UFAQ_Group_By_Category_Count', 'No', 'yes'),
(207, 'EWD_UFAQ_Group_By_Order_By', 'name', 'yes'),
(208, 'EWD_UFAQ_Group_By_Order', 'ASC', 'yes'),
(209, 'EWD_UFAQ_Order_By', 'date', 'yes'),
(210, 'EWD_UFAQ_Order', 'DESC', 'yes'),
(211, 'EWD_UFAQ_Hide_Blank_Fields', 'Yes', 'yes'),
(212, 'EWD_UFAQ_Styling_Category_Heading_Type', 'h4', 'yes'),
(213, 'EWD_UFAQ_Styling_FAQ_Heading_Type', 'h4', 'yes'),
(214, 'EWD_UFAQ_Toggle_Symbol', 'A', 'yes'),
(215, 'EWD_UFAQ_Full_Version', 'No', 'yes'),
(216, 'EWD_UFAQ_Update_Flag', 'Yes', 'yes'),
(217, 'EWD_UFAQ_Install_Flag', 'Yes', 'yes'),
(218, 'EWD_UFAQ_Install_Version', '1.6', 'yes'),
(219, 'EWD_UFAQ_Install_Time', '1545117455', 'yes'),
(220, 'FAQ_Auto_Complete_Titles', 'Yes', 'yes'),
(221, 'EWD_UFAQ_Version', '1.8.0c', 'yes'),
(222, 'UFAQ_Run_Tutorial', 'Yes', 'yes'),
(225, 'widget_ewd_ufaq_display_faq_post_list', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(226, 'widget_ewd_ufaq_display_recent_faqs', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(227, 'widget_ewd_ufaq_display_popular_faqs', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(228, 'widget_ewd_ufaq_display_random_faq', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(229, 'widget_ewd_ufaq_display_faq_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(231, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1545283771;s:7:\"checked\";a:3:{s:19:\"akismet/akismet.php\";s:3:\"4.1\";s:9:\"hello.php\";s:5:\"1.7.1\";s:43:\"quick-and-easy-faqs/quick-and-easy-faqs.php\";s:5:\"1.1.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:3:\"4.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/akismet.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"quick-and-easy-faqs/quick-and-easy-faqs.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/quick-and-easy-faqs\";s:4:\"slug\";s:19:\"quick-and-easy-faqs\";s:6:\"plugin\";s:43:\"quick-and-easy-faqs/quick-and-easy-faqs.php\";s:11:\"new_version\";s:5:\"1.1.3\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/quick-and-easy-faqs/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/quick-and-easy-faqs.1.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/quick-and-easy-faqs/assets/icon-256x256.png?rev=1137830\";s:2:\"1x\";s:72:\"https://ps.w.org/quick-and-easy-faqs/assets/icon-128x128.png?rev=1137830\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/quick-and-easy-faqs/assets/banner-1544x500.png?rev=1137830\";s:2:\"1x\";s:74:\"https://ps.w.org/quick-and-easy-faqs/assets/banner-772x250.png?rev=1137830\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(232, 'quick_and_easy_faqs_options', '', 'yes'),
(234, 'faq-group_children', 'a:0:{}', 'yes'),
(250, '_site_transient_timeout_theme_roots', '1545285559', 'no'),
(251, '_site_transient_theme_roots', 'a:3:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(253, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1545283770;s:15:\"version_checked\";s:5:\"5.0.2\";s:12:\"translations\";a:0:{}}', 'no'),
(254, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1545283771;s:7:\"checked\";a:3:{s:14:\"twentynineteen\";s:3:\"1.0\";s:15:\"twentyseventeen\";s:3:\"1.8\";s:13:\"twentysixteen\";s:3:\"1.6\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:4:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.1\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.1.zip\";}s:15:\"twentyseventeen\";a:4:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"1.9\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.1.9.zip\";}s:13:\"twentysixteen\";a:4:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.1.7.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(255, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:31:\"kamal.asawara@learningmate.copm\";s:7:\"version\";s:5:\"5.0.2\";s:9:\"timestamp\";i:1545283773;}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_trash_meta_status', 'publish'),
(4, 5, '_wp_trash_meta_time', '1545110963'),
(5, 6, '_menu_item_type', 'post_type'),
(6, 6, '_menu_item_menu_item_parent', '7'),
(7, 6, '_menu_item_object_id', '2'),
(8, 6, '_menu_item_object', 'page'),
(9, 6, '_menu_item_target', ''),
(10, 6, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(11, 6, '_menu_item_xfn', ''),
(12, 6, '_menu_item_url', ''),
(14, 2, '_edit_lock', '1545115021:1'),
(15, 7, '_menu_item_type', 'custom'),
(16, 7, '_menu_item_menu_item_parent', '0'),
(17, 7, '_menu_item_object_id', '7'),
(18, 7, '_menu_item_object', 'custom'),
(19, 7, '_menu_item_target', ''),
(20, 7, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(21, 7, '_menu_item_xfn', ''),
(22, 7, '_menu_item_url', 'http://localhost/wordpress/'),
(24, 8, '_edit_lock', '1545115193:1'),
(25, 9, '_menu_item_type', 'post_type'),
(26, 9, '_menu_item_menu_item_parent', '0'),
(27, 9, '_menu_item_object_id', '8'),
(28, 9, '_menu_item_object', 'page'),
(29, 9, '_menu_item_target', ''),
(30, 9, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(31, 9, '_menu_item_xfn', ''),
(32, 9, '_menu_item_url', ''),
(33, 11, '_edit_lock', '1545115211:1'),
(34, 12, '_menu_item_type', 'post_type'),
(35, 12, '_menu_item_menu_item_parent', '9'),
(36, 12, '_menu_item_object_id', '11'),
(37, 12, '_menu_item_object', 'page'),
(38, 12, '_menu_item_target', ''),
(39, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(40, 12, '_menu_item_xfn', ''),
(41, 12, '_menu_item_url', ''),
(42, 14, '_edit_lock', '1545115224:1'),
(43, 15, '_menu_item_type', 'post_type'),
(44, 15, '_menu_item_menu_item_parent', '0'),
(45, 15, '_menu_item_object_id', '14'),
(46, 15, '_menu_item_object', 'page'),
(47, 15, '_menu_item_target', ''),
(48, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(49, 15, '_menu_item_xfn', ''),
(50, 15, '_menu_item_url', ''),
(51, 17, '_edit_lock', '1545115236:1'),
(52, 18, '_menu_item_type', 'post_type'),
(53, 18, '_menu_item_menu_item_parent', '15'),
(54, 18, '_menu_item_object_id', '17'),
(55, 18, '_menu_item_object', 'page'),
(56, 18, '_menu_item_target', ''),
(57, 18, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(58, 18, '_menu_item_xfn', ''),
(59, 18, '_menu_item_url', ''),
(60, 20, '_edit_lock', '1545115255:1'),
(61, 21, '_menu_item_type', 'post_type'),
(62, 21, '_menu_item_menu_item_parent', '7'),
(63, 21, '_menu_item_object_id', '20'),
(64, 21, '_menu_item_object', 'page'),
(65, 21, '_menu_item_target', ''),
(66, 21, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(67, 21, '_menu_item_xfn', ''),
(68, 21, '_menu_item_url', ''),
(69, 23, '_menu_item_type', 'post_type'),
(70, 23, '_menu_item_menu_item_parent', '0'),
(71, 23, '_menu_item_object_id', '20'),
(72, 23, '_menu_item_object', 'page'),
(73, 23, '_menu_item_target', ''),
(74, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(75, 23, '_menu_item_xfn', ''),
(76, 23, '_menu_item_url', ''),
(77, 23, '_menu_item_orphaned', '1545115415'),
(78, 24, '_menu_item_type', 'post_type'),
(79, 24, '_menu_item_menu_item_parent', '0'),
(80, 24, '_menu_item_object_id', '17'),
(81, 24, '_menu_item_object', 'page'),
(82, 24, '_menu_item_target', ''),
(83, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(84, 24, '_menu_item_xfn', ''),
(85, 24, '_menu_item_url', ''),
(86, 24, '_menu_item_orphaned', '1545115416'),
(87, 25, '_menu_item_type', 'post_type'),
(88, 25, '_menu_item_menu_item_parent', '0'),
(89, 25, '_menu_item_object_id', '14'),
(90, 25, '_menu_item_object', 'page'),
(91, 25, '_menu_item_target', ''),
(92, 25, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(93, 25, '_menu_item_xfn', ''),
(94, 25, '_menu_item_url', ''),
(95, 25, '_menu_item_orphaned', '1545115416'),
(96, 26, '_menu_item_type', 'post_type'),
(97, 26, '_menu_item_menu_item_parent', '0'),
(98, 26, '_menu_item_object_id', '11'),
(99, 26, '_menu_item_object', 'page'),
(100, 26, '_menu_item_target', ''),
(101, 26, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(102, 26, '_menu_item_xfn', ''),
(103, 26, '_menu_item_url', ''),
(104, 26, '_menu_item_orphaned', '1545115416'),
(105, 27, '_menu_item_type', 'post_type'),
(106, 27, '_menu_item_menu_item_parent', '0'),
(107, 27, '_menu_item_object_id', '8'),
(108, 27, '_menu_item_object', 'page'),
(109, 27, '_menu_item_target', ''),
(110, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(111, 27, '_menu_item_xfn', ''),
(112, 27, '_menu_item_url', ''),
(113, 27, '_menu_item_orphaned', '1545115417'),
(114, 28, '_menu_item_type', 'post_type'),
(115, 28, '_menu_item_menu_item_parent', '15'),
(116, 28, '_menu_item_object_id', '2'),
(117, 28, '_menu_item_object', 'page'),
(118, 28, '_menu_item_target', ''),
(119, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(120, 28, '_menu_item_xfn', ''),
(121, 28, '_menu_item_url', ''),
(123, 1, '_edit_lock', '1545115805:1'),
(128, 31, '_edit_lock', '1545115931:1'),
(131, 31, '_oembed_7814e50a6a2e17e28367dc07bcca709c', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/yRLvJgz9Dxk?start=266&feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(132, 31, '_oembed_time_7814e50a6a2e17e28367dc07bcca709c', '1545116029'),
(133, 33, '_edit_lock', '1545116270:1'),
(134, 34, '_wp_attached_file', '2018/12/Capture.png'),
(135, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1249;s:6:\"height\";i:439;s:4:\"file\";s:19:\"2018/12/Capture.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Capture-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"Capture-300x105.png\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"Capture-768x270.png\";s:5:\"width\";i:768;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"Capture-1024x360.png\";s:5:\"width\";i:1024;s:6:\"height\";i:360;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(142, 37, '_edit_lock', '1545116861:1'),
(143, 38, '_wp_attached_file', '2018/12/SampleAudio_0.7mb.mp3'),
(144, 38, '_wp_attachment_metadata', 'a:14:{s:10:\"dataformat\";s:3:\"mp3\";s:8:\"channels\";i:2;s:11:\"sample_rate\";i:44100;s:7:\"bitrate\";i:128000;s:11:\"channelmode\";s:12:\"joint stereo\";s:12:\"bitrate_mode\";s:3:\"cbr\";s:8:\"lossless\";b:0;s:15:\"encoder_options\";s:6:\"CBR128\";s:17:\"compression_ratio\";d:0.09070294784580499;s:10:\"fileformat\";s:3:\"mp3\";s:8:\"filesize\";i:725240;s:9:\"mime_type\";s:10:\"audio/mpeg\";s:6:\"length\";i:45;s:16:\"length_formatted\";s:4:\"0:45\";}'),
(147, 37, 'enclosure', 'http://localhost/wordpress/wp-content/uploads/2018/12/SampleAudio_0.7mb.mp3\n725240\naudio/mpeg\n'),
(148, 40, '_edit_last', '1'),
(149, 40, '_edit_lock', '1545117629:1'),
(150, 41, '_edit_lock', '1545123230:1'),
(151, 42, '_menu_item_type', 'post_type'),
(152, 42, '_menu_item_menu_item_parent', '0'),
(153, 42, '_menu_item_object_id', '41'),
(154, 42, '_menu_item_object', 'page'),
(155, 42, '_menu_item_target', ''),
(156, 42, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(157, 42, '_menu_item_xfn', ''),
(158, 42, '_menu_item_url', ''),
(159, 44, '_edit_last', '1'),
(160, 44, '_edit_lock', '1545118717:1'),
(161, 45, '_edit_last', '1'),
(162, 45, '_edit_lock', '1545119189:1'),
(163, 46, '_edit_last', '1'),
(164, 46, '_edit_lock', '1545119188:1'),
(165, 47, '_edit_last', '1'),
(166, 47, '_edit_lock', '1545119188:1'),
(167, 48, '_edit_last', '1'),
(168, 48, '_edit_lock', '1545119187:1'),
(169, 40, '_wp_trash_meta_status', 'publish'),
(170, 40, '_wp_trash_meta_time', '1545118662'),
(171, 40, '_wp_desired_post_slug', 'faq1'),
(172, 54, '_edit_last', '1'),
(173, 54, '_edit_lock', '1545119379:1'),
(174, 55, '_edit_last', '1'),
(175, 55, '_edit_lock', '1545121714:1'),
(176, 55, '_oembed_a52cd07c6074c1ec846616934762be26', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/yRLvJgz9Dxk?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(177, 55, '_oembed_time_a52cd07c6074c1ec846616934762be26', '1545119623'),
(178, 59, '_wp_attached_file', '2018/12/acmt-CFDocuments.png'),
(179, 59, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:1021;s:6:\"height\";i:598;s:4:\"file\";s:28:\"2018/12/acmt-CFDocuments.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(180, 59, '_edit_lock', '1545220742:1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-12-18 05:17:23', '2018-12-18 05:17:23', '<!-- wp:paragraph -->\n<p>Answer 1</p>\n<!-- /wp:paragraph -->', 'Question 1', '', 'publish', 'closed', 'open', '', 'hello-world', '', '', '2018-12-18 06:51:06', '2018-12-18 06:51:06', '', 0, 'http://localhost/wordpress/?p=1', 0, 'post', '', 0),
(2, 1, '2018-12-18 05:17:23', '2018-12-18 05:17:23', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/wordpress/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-12-18 05:17:23', '2018-12-18 05:17:23', '', 0, 'http://localhost/wordpress/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-12-18 05:17:23', '2018-12-18 05:17:23', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/wordpress.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-12-18 05:17:23', '2018-12-18 05:17:23', '', 0, 'http://localhost/wordpress/?page_id=3', 0, 'page', '', 0),
(4, 1, '2018-12-18 05:17:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-12-18 05:17:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=4', 0, 'post', '', 0),
(5, 1, '2018-12-18 05:29:22', '2018-12-18 05:29:22', '{\n    \"twentynineteen::nav_menu_locations[menu-1]\": {\n        \"value\": -719708101,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-12-18 05:29:22\"\n    },\n    \"nav_menu[-719708101]\": {\n        \"value\": {\n            \"name\": \"faq\",\n            \"description\": \"\",\n            \"parent\": 0,\n            \"auto_add\": true\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-12-18 05:29:22\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'ee160f2b-2184-473d-b91b-c3af6505b34f', '', '', '2018-12-18 05:29:22', '2018-12-18 05:29:22', '', 0, 'http://localhost/wordpress/2018/12/18/ee160f2b-2184-473d-b91b-c3af6505b34f/', 0, 'customize_changeset', '', 0),
(6, 1, '2018-12-18 06:30:33', '2018-12-18 06:30:33', '', 'Context menu 1', '', 'publish', 'closed', 'closed', '', '6', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/?p=6', 2, 'nav_menu_item', '', 0),
(7, 1, '2018-12-18 06:39:50', '2018-12-18 06:39:50', '', 'Tab1', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/?p=7', 1, 'nav_menu_item', '', 0),
(8, 1, '2018-12-18 06:42:08', '2018-12-18 06:42:08', '', 'Tab 2', '', 'publish', 'closed', 'closed', '', 'tab-2', '', '', '2018-12-18 06:42:08', '2018-12-18 06:42:08', '', 0, 'http://localhost/wordpress/?page_id=8', 0, 'page', '', 0),
(9, 1, '2018-12-18 06:42:08', '2018-12-18 06:42:08', ' ', '', '', 'publish', 'closed', 'closed', '', '9', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/2018/12/18/9/', 4, 'nav_menu_item', '', 0),
(10, 1, '2018-12-18 06:42:08', '2018-12-18 06:42:08', '', 'Tab 2', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-12-18 06:42:08', '2018-12-18 06:42:08', '', 8, 'http://localhost/wordpress/2018/12/18/8-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-12-18 06:42:31', '2018-12-18 06:42:31', '', 'context 2', '', 'publish', 'closed', 'closed', '', 'context-2', '', '', '2018-12-18 06:42:31', '2018-12-18 06:42:31', '', 0, 'http://localhost/wordpress/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-12-18 06:42:32', '2018-12-18 06:42:32', ' ', '', '', 'publish', 'closed', 'closed', '', '12', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/2018/12/18/12/', 5, 'nav_menu_item', '', 0),
(13, 1, '2018-12-18 06:42:31', '2018-12-18 06:42:31', '', 'context 2', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-12-18 06:42:31', '2018-12-18 06:42:31', '', 11, 'http://localhost/wordpress/2018/12/18/11-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2018-12-18 06:42:44', '2018-12-18 06:42:44', '', 'Tab 3', '', 'publish', 'closed', 'closed', '', 'tab-3', '', '', '2018-12-18 06:42:44', '2018-12-18 06:42:44', '', 0, 'http://localhost/wordpress/?page_id=14', 0, 'page', '', 0),
(15, 1, '2018-12-18 06:42:44', '2018-12-18 06:42:44', ' ', '', '', 'publish', 'closed', 'closed', '', '15', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/2018/12/18/15/', 6, 'nav_menu_item', '', 0),
(16, 1, '2018-12-18 06:42:44', '2018-12-18 06:42:44', '', 'Tab 3', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2018-12-18 06:42:44', '2018-12-18 06:42:44', '', 14, 'http://localhost/wordpress/2018/12/18/14-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2018-12-18 06:42:57', '2018-12-18 06:42:57', '', 'Context 3', '', 'publish', 'closed', 'closed', '', 'context-3', '', '', '2018-12-18 06:42:57', '2018-12-18 06:42:57', '', 0, 'http://localhost/wordpress/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-12-18 06:42:57', '2018-12-18 06:42:57', ' ', '', '', 'publish', 'closed', 'closed', '', '18', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/2018/12/18/18/', 7, 'nav_menu_item', '', 0),
(19, 1, '2018-12-18 06:42:57', '2018-12-18 06:42:57', '', 'Context 3', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-12-18 06:42:57', '2018-12-18 06:42:57', '', 17, 'http://localhost/wordpress/2018/12/18/17-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2018-12-18 06:43:13', '2018-12-18 06:43:13', '', 'Context 1 A', '', 'publish', 'closed', 'closed', '', 'context-1-a', '', '', '2018-12-18 06:43:13', '2018-12-18 06:43:13', '', 0, 'http://localhost/wordpress/?page_id=20', 0, 'page', '', 0),
(21, 1, '2018-12-18 06:43:13', '2018-12-18 06:43:13', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2018-12-18 06:45:22', '2018-12-18 06:45:22', '', 0, 'http://localhost/wordpress/2018/12/18/21/', 3, 'nav_menu_item', '', 0),
(22, 1, '2018-12-18 06:43:13', '2018-12-18 06:43:13', '', 'Context 1 A', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-12-18 06:43:13', '2018-12-18 06:43:13', '', 20, 'http://localhost/wordpress/2018/12/18/20-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2018-12-18 06:43:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-12-18 06:43:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2018-12-18 06:43:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-12-18 06:43:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2018-12-18 06:43:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-12-18 06:43:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2018-12-18 06:43:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-12-18 06:43:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=26', 1, 'nav_menu_item', '', 0),
(27, 1, '2018-12-18 06:43:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-12-18 06:43:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=27', 1, 'nav_menu_item', '', 0),
(28, 1, '2018-12-18 06:44:41', '2018-12-18 06:44:41', '', 'Context 3 A', '', 'publish', 'closed', 'closed', '', '28', '', '', '2018-12-18 06:45:23', '2018-12-18 06:45:23', '', 0, 'http://localhost/wordpress/?p=28', 8, 'nav_menu_item', '', 0),
(29, 1, '2018-12-18 06:48:08', '2018-12-18 06:48:08', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-12-18 06:48:08', '2018-12-18 06:48:08', '', 1, 'http://localhost/wordpress/2018/12/18/1-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2018-12-18 06:51:06', '2018-12-18 06:51:06', '<!-- wp:paragraph -->\n<p>Answer 1</p>\n<!-- /wp:paragraph -->', 'Question 1', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-12-18 06:51:06', '2018-12-18 06:51:06', '', 1, 'http://localhost/wordpress/2018/12/18/1-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2018-12-18 06:53:48', '2018-12-18 06:53:48', '<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=yRLvJgz9Dxk\\u0026t=266s\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"className\":\"wp-embed-aspect-16-9 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=yRLvJgz9Dxk&amp;t=266s\n</div></figure>\n<!-- /wp:core-embed/youtube -->', 'Question with Video', '', 'publish', 'open', 'open', '', 'question-with-video', '', '', '2018-12-18 06:53:48', '2018-12-18 06:53:48', '', 0, 'http://localhost/wordpress/?p=31', 0, 'post', '', 0),
(32, 1, '2018-12-18 06:53:48', '2018-12-18 06:53:48', '<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=yRLvJgz9Dxk\\u0026t=266s\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"className\":\"wp-embed-aspect-16-9 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=yRLvJgz9Dxk&amp;t=266s\n</div></figure>\n<!-- /wp:core-embed/youtube -->', 'Question with Video', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2018-12-18 06:53:48', '2018-12-18 06:53:48', '', 31, 'http://localhost/wordpress/2018/12/18/31-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2018-12-18 06:55:10', '2018-12-18 06:55:10', '<!-- wp:image {\"id\":34} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/wordpress/wp-content/uploads/2018/12/Capture-1024x360.png\" alt=\"\" class=\"wp-image-34\"/></figure>\n<!-- /wp:image -->', 'Question with Image?', '', 'publish', 'closed', 'open', '', 'question-with-image', '', '', '2018-12-18 06:57:14', '2018-12-18 06:57:14', '', 0, 'http://localhost/wordpress/?p=33', 0, 'post', '', 0),
(34, 1, '2018-12-18 06:55:05', '2018-12-18 06:55:05', '', 'Capture', '', 'inherit', 'open', 'closed', '', 'capture', '', '', '2018-12-18 06:55:05', '2018-12-18 06:55:05', '', 33, 'http://localhost/wordpress/wp-content/uploads/2018/12/Capture.png', 0, 'attachment', 'image/png', 0),
(35, 1, '2018-12-18 06:55:10', '2018-12-18 06:55:10', '<!-- wp:image {\"id\":34} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/wordpress/wp-content/uploads/2018/12/Capture-1024x360.png\" alt=\"\" class=\"wp-image-34\"/></figure>\n<!-- /wp:image -->', 'Question with Image?', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2018-12-18 06:55:10', '2018-12-18 06:55:10', '', 33, 'http://localhost/wordpress/2018/12/18/33-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2018-12-18 06:57:36', '2018-12-18 06:57:36', '<!-- wp:image {\"id\":34} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost/wordpress/wp-content/uploads/2018/12/Capture-1024x360.png\" alt=\"\" class=\"wp-image-34\"/></figure>\n<!-- /wp:image -->', 'Question with Image?', '', 'inherit', 'closed', 'closed', '', '33-autosave-v1', '', '', '2018-12-18 06:57:36', '2018-12-18 06:57:36', '', 33, 'http://localhost/wordpress/2018/12/18/33-autosave-v1/', 0, 'revision', '', 0),
(37, 1, '2018-12-18 07:07:28', '2018-12-18 07:07:28', '<!-- wp:audio {\"id\":38} -->\n<figure class=\"wp-block-audio\"><audio controls src=\"http://localhost/wordpress/wp-content/uploads/2018/12/SampleAudio_0.7mb.mp3\"></audio></figure>\n<!-- /wp:audio -->', 'Question with Audio', '', 'publish', 'closed', 'open', '', 'question-with-audio', '', '', '2018-12-18 07:07:28', '2018-12-18 07:07:28', '', 0, 'http://localhost/wordpress/?p=37', 0, 'post', '', 0),
(38, 1, '2018-12-18 07:07:23', '2018-12-18 07:07:23', '', 'SampleAudio_0.7mb', '', 'inherit', 'closed', 'closed', '', 'sampleaudio_0-7mb', '', '', '2018-12-18 07:07:23', '2018-12-18 07:07:23', '', 37, 'http://localhost/wordpress/wp-content/uploads/2018/12/SampleAudio_0.7mb.mp3', 0, 'attachment', 'audio/mpeg', 0),
(39, 1, '2018-12-18 07:07:28', '2018-12-18 07:07:28', '<!-- wp:audio {\"id\":38} -->\n<figure class=\"wp-block-audio\"><audio controls src=\"http://localhost/wordpress/wp-content/uploads/2018/12/SampleAudio_0.7mb.mp3\"></audio></figure>\n<!-- /wp:audio -->', 'Question with Audio', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2018-12-18 07:07:28', '2018-12-18 07:07:28', '', 37, 'http://localhost/wordpress/2018/12/18/37-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-12-18 07:22:11', '2018-12-18 07:22:11', 'Answer 1', 'FAQ1', '', 'trash', 'closed', 'closed', '', 'faq1__trashed', '', '', '2018-12-18 07:37:42', '2018-12-18 07:37:42', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=40', 0, 'faq', '', 0),
(41, 1, '2018-12-18 07:23:39', '2018-12-18 07:23:39', '<!-- wp:paragraph -->\n<p><br>[faqs style=\"filterable-toggle\"]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2018-12-18 08:48:53', '2018-12-18 08:48:53', '', 0, 'http://localhost/wordpress/?page_id=41', 0, 'page', '', 0),
(42, 1, '2018-12-18 07:23:39', '2018-12-18 07:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '42', '', '', '2018-12-18 07:23:39', '2018-12-18 07:23:39', '', 0, 'http://localhost/wordpress/2018/12/18/42/', 9, 'nav_menu_item', '', 0),
(43, 1, '2018-12-18 07:23:39', '2018-12-18 07:23:39', '<!-- wp:paragraph -->\n<p>\n\n[faqs]\n\n</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 07:23:39', '2018-12-18 07:23:39', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2018-12-18 07:35:53', '2018-12-18 07:35:53', '<p class=\"para\">From the <a class=\"link\" href=\"http://php.net/manual/en/preface.php\">preface of the manual</a>:</p>\r\n<p class=\"para\">PHP is an HTML-embedded scripting language. Much of its syntax is borrowed from C, Java and Perl with a couple of unique PHP-specific features thrown in. The goal of the language is to allow web developers to write dynamically generated pages quickly.</p>', 'What is PHP?', '', 'publish', 'closed', 'closed', '', 'what-is-php', '', '', '2018-12-18 07:39:58', '2018-12-18 07:39:58', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=44', 0, 'faq', '', 0),
(45, 1, '2018-12-18 07:36:15', '2018-12-18 07:36:15', 'PHP stands for <em class=\"emphasis\">PHP: Hypertext Preprocessor</em>. This confuses many people because the first word of the acronym is the acronym. This type of acronym is called a recursive acronym. For more information, the curious can visit <a class=\"link external\" href=\"http://foldoc.org/\">» Free On-Line Dictionary of Computing</a> or the <a class=\"link external\" href=\"http://en.wikipedia.org/wiki/Recursive_acronym\">» Wikipedia entry on recursive acronyms</a>.', 'What does PHP stand for?', '', 'publish', 'closed', 'closed', '', 'what-does-php-stand-for', '', '', '2018-12-18 07:48:49', '2018-12-18 07:48:49', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=45', 0, 'faq', '', 0),
(46, 1, '2018-12-18 07:36:31', '2018-12-18 07:36:31', 'PHP/FI 2.0 is an early and no longer supported version of PHP. PHP 3 is the successor to PHP/FI 2.0 and is a lot nicer. PHP 7 is the current generation of PHP, which uses the Zend engine 3 which, among other things, offers many additional <a class=\"link\" href=\"http://php.net/manual/en/language.oop5.php\">OOP</a> features.', 'What is the relation between the versions?', '', 'publish', 'closed', 'closed', '', 'what-is-the-relation-between-the-versions', '', '', '2018-12-18 07:48:46', '2018-12-18 07:48:46', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=46', 0, 'faq', '', 0),
(47, 1, '2018-12-18 07:36:55', '2018-12-18 07:36:55', 'Yes. See the <var class=\"filename\">INSTALL</var> file that is included in the PHP source distribution.', 'Can I run several versions of PHP at the same time?', '', 'publish', 'closed', 'closed', '', 'can-i-run-several-versions-of-php-at-the-same-time', '', '', '2018-12-18 07:48:42', '2018-12-18 07:48:42', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=47', 0, 'faq', '', 0),
(48, 1, '2018-12-18 07:37:10', '2018-12-18 07:37:10', 'You should go to the PHP Bug Database and make sure the bug isn\'t a known bug. If you don\'t see it in the database, use the reporting form to report the bug. It is important to use the bug database instead of just sending an email to one of the mailing lists because the bug will have a tracking number assigned and it will then be possible for you to go back later and check on the status of the bug. The bug database can be found at <a class=\"link external\" href=\"http://bugs.php.net/\">» http://bugs.php.net/</a>.', 'I think I found a bug! Who should I tell?', '', 'publish', 'closed', 'closed', '', 'i-think-i-found-a-bug-who-should-i-tell', '', '', '2018-12-18 07:48:41', '2018-12-18 07:48:41', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=48', 0, 'faq', '', 0),
(49, 1, '2018-12-18 07:41:15', '2018-12-18 07:41:15', '<!-- wp:paragraph -->\n<p>[faqs grouped=”yes”]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 07:41:15', '2018-12-18 07:41:15', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-12-18 07:45:40', '2018-12-18 07:45:40', '<!-- wp:paragraph -->\n<p><br>[faqs style=”toggle”]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 07:45:40', '2018-12-18 07:45:40', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-12-18 07:46:28', '2018-12-18 07:46:28', '<!-- wp:paragraph -->\n<p><br>[faqs style=\"toggle\"]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 07:46:28', '2018-12-18 07:46:28', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2018-12-18 07:48:09', '2018-12-18 07:48:09', '<!-- wp:paragraph -->\n<p><br><br>[faqs style=\"toggle\" grouped=\"yes\"]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 07:48:09', '2018-12-18 07:48:09', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2018-12-18 07:49:40', '2018-12-18 07:49:40', '<!-- wp:paragraph -->\n<p><br>[faqs style=\"filterable-toggle\"]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 07:49:40', '2018-12-18 07:49:40', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-12-18 07:51:57', '2018-12-18 07:51:57', '<p class=\"para\">Of course! There are many mailing lists for several subjects. The most common community mailing lists can be found on our <a class=\"link external\" href=\"http://www.php.net/mailing-lists.php\">» mailing list</a> page.</p>\r\n<p class=\"para\">The most general mailing list is <em>php-general</em>. To subscribe, send a blank email message to <a class=\"link external\" href=\"mailto:php-general-subscribe@lists.php.net\">» php-general-subscribe@lists.php.net</a>. To unsubscribe, send a blank email to <a class=\"link external\" href=\"mailto:php-general-unsubscribe@lists.php.net\">» php-general-unsubscribe@lists.php.net</a>.</p>\r\n<p class=\"para\">You can also subscribe and unsubscribe using the web interface on our <a class=\"link external\" href=\"http://www.php.net/mailing-lists.php\">» mailing list</a> page, and unsubscribe instructions are included in the footer of every mailing list message.</p>', 'Are there any PHP mailing lists?', '', 'publish', 'closed', 'closed', '', 'are-there-any-php-mailing-lists', '', '', '2018-12-18 07:51:57', '2018-12-18 07:51:57', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=54', 0, 'faq', '', 0),
(55, 1, '2018-12-18 07:52:14', '2018-12-18 07:52:14', 'There are countless PHP-centric communities around the world, and we have links to some of these resources (and information on how to learn more) on our <a class=\"link external\" href=\"http://www.php.net/support.php\">» Support</a> page.\r\n\r\n&nbsp;\r\n\r\n[embed]https://www.youtube.com/watch?v=yRLvJgz9Dxk[/embed]', 'Are there any other communities?', '', 'publish', 'closed', 'closed', '', 'are-there-any-other-communities', '', '', '2018-12-18 07:53:46', '2018-12-18 07:53:46', '', 0, 'http://localhost/wordpress/?post_type=faq&#038;p=55', 0, 'faq', '', 0),
(56, 1, '2018-12-18 07:53:18', '2018-12-18 07:53:18', 'There are countless PHP-centric communities around the world, and we have links to some of these resources (and information on how to learn more) on our <a class=\"link external\" href=\"http://www.php.net/support.php\">» Support</a> page.\n\n&nbsp;\n\n&nbsp;', 'Are there any other communities?', '', 'inherit', 'closed', 'closed', '', '55-autosave-v1', '', '', '2018-12-18 07:53:18', '2018-12-18 07:53:18', '', 55, 'http://localhost/wordpress/2018/12/18/55-autosave-v1/', 0, 'revision', '', 0),
(57, 1, '2018-12-18 08:48:10', '2018-12-18 08:48:10', '<!-- wp:paragraph -->\n<p><br>[faqs style=\"toggle\" grouped=\"yes\"]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 08:48:10', '2018-12-18 08:48:10', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2018-12-18 08:48:53', '2018-12-18 08:48:53', '<!-- wp:paragraph -->\n<p><br>[faqs style=\"filterable-toggle\"]</p>\n<!-- /wp:paragraph -->', 'faq', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-12-18 08:48:53', '2018-12-18 08:48:53', '', 41, 'http://localhost/wordpress/2018/12/18/41-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2018-12-19 12:01:21', '2018-12-19 12:01:21', '', 'acmt-CFDocuments', '', 'inherit', 'closed', 'closed', '', 'acmt-cfdocuments', '', '', '2018-12-19 12:01:21', '2018-12-19 12:01:21', '', 0, 'http://localhost/wordpress/wp-content/uploads/2018/12/acmt-CFDocuments.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'faq', 'faq', 0),
(3, 'General Information', 'general-information', 0),
(4, 'Mailing lists', 'mailing-lists', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(6, 2, 0),
(7, 2, 0),
(9, 2, 0),
(12, 2, 0),
(15, 2, 0),
(18, 2, 0),
(21, 2, 0),
(28, 2, 0),
(31, 1, 0),
(37, 1, 0),
(42, 2, 0),
(44, 3, 0),
(45, 3, 0),
(46, 3, 0),
(47, 3, 0),
(48, 3, 0),
(54, 4, 0),
(55, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'nav_menu', '', 0, 9),
(3, 3, 'faq-group', '', 0, 5),
(4, 4, 'faq-group', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'kamal'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,ufaq_admin_pointers_tutorial-one,ufaq_admin_pointers_tutorial-two'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'nav_menu_recently_edited', '2'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(21, 1, 'wp_user-settings', 'mfold=o'),
(22, 1, 'wp_user-settings-time', '1545218089'),
(24, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'kamal', '$P$B8mUdrtJZ4edKvMyA/0Ajqu.wSf5QI1', 'kamal', 'kamal.asawara@learningmate.copm', '', '2018-12-18 05:17:22', '', 0, 'kamal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
