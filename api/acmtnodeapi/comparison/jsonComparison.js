var diff = require('deep-diff').diff;
const db = require('../db_adapter');
const fs = require('fs')
const config = require('../config/default_'+process.env.NODEJSENV+'.json');
const documentDetail = require('./document_detail.js');
const nodeDetail = require('./node_detail.js');
const AWS = require('aws-sdk');
const connection = db.getConnection();
const batchSize = 1000;
AWS.config.update(config.AWSGLOBAL);
const S3 = new AWS.S3();
/**
 * This js compare between two Case & Custom JSON 
 * First Download All the case and Custom JSON from S3
 * Then Compare both case json then compare custom json
 * After getting diffences from coparison method data get prepared to insert in nodetype table & metadata table
 * 
 */

/**
 *
 *
 * @param {*} json1
 * @param {*} json2
 * @param {*} custJson1
 * @param {*} custJson2
 * @param {*} comparison_id
 * @returns
 */
function compareJSON(json1, json2, custJson1, custJson2, comparison_id) {

    return new Promise((resolve, reject) => {
        try {
            let msg = '';
            let diffObjet = new Object();
            /**
             * First read CFDocument from both JSON 
             */
            if (json1['CFDocument']) {
                if (json2['CFDocument']) {
                    diffObjet['CFDocument'] = {
                        edited: [{
                            id: json1['CFDocument'].identifier,
                            diff: getDiffrences(json1['CFDocument'], json2['CFDocument'], ['lastChangeDateTime', 'uri', 'nodeTypeId']),
                            itemType: 'Document',
                            itemTypeId: json1['CFDocument'].identifier,
                            detail: json1['CFDocument']
                        }],
                        deleted: [],
                        added: []
                    };
                }
            }
            /**
             * CFItems compare each items
             */
            if (json1['CFItems']) {
                if (json2['CFItems']) {
                    let cfItems = json1['CFItems'];
                    let cfItems2 = json2['CFItems'];

                    compareArray(cfItems, cfItems2, 'CFItems', diffObjet, json1, ['lastChangeDateTime', 'uri'], false);
                }
            }
            /******************************************************************************/
            if (json1['CFAssociations']) {
                if (json2['CFAssociations']) {
                    let cfAssociatoins = json1['CFAssociations'];
                    let cfAssociatoins2 = json2['CFAssociations'];
                    compareArray(cfAssociatoins, cfAssociatoins2, 'CFAssociations', diffObjet, json1, ['identifier', 'lastChangeDateTime', 'internal_name', 'field_type', 'field_possible_values', 'last_field_possible_values', 'order', 'is_custom', 'is_document', 'is_mandatory', 'is_active', 'is_deleted', 'updated_by', 'created_at', 'updated_at', 'uri'], false);
                }
            }
            if (json1['CFDefinitions']) {
                if (json2['CFDefinitions']) {
                    for (const key in json1['CFDefinitions']) {
                        if (json1['CFDefinitions'].hasOwnProperty(key)) {
                            const array1 = json1['CFDefinitions'][key];
                            const array2 = json2['CFDefinitions'][key];
                            compareArray(array1, array2, key, diffObjet, json1, ['lastChangeDateTime', 'uri'], false);
                        }
                    }
                }
            }

            let insertData = [];
            let insertMetaData = [];
            for (const key in diffObjet) {

                if (diffObjet.hasOwnProperty(key)) {
                    const element = diffObjet[key];
                    prepareCaseNodeTypeEditedData(element.edited, comparison_id, key, insertData, insertMetaData, json1, custJson1);
                    prepareCaseNodeTypeData(element.deleted, comparison_id, key, insertData, insertMetaData, 'D', json1, custJson1);
                    prepareCaseNodeTypeData(element.added, comparison_id, key, insertData, insertMetaData, 'N', json2, custJson2);
                }
            }
            prepareNodeTypeQueryData(insertData).then((result) => {
                let msg = result;
                prepareMetadataQueryData(insertMetaData).then(result => {
                    msg = msg + ' ' + result;
                    resolve({
                        'error': null,
                        'msg': msg
                    });

                }).catch(ex => {
                    reject({
                        'function': 'compareJSON',
                        'error': error
                    });
                });

            }).catch(ex => {
                reject({
                    'function': 'compareJSON',
                    'error': error
                });
            });


        } catch (error) {
            reject({
                'function': 'compareJSON',
                'error': error
            });
        }
    });
}

/**
 * 
 * @param {Array} array1 array to compare with array2
 * @param {Array} array2 array to compare with array1
 * @param {String} propertyName CASE JSON properties name (CFItems,CFAssociations etc.)
 * @param {Object} diffObjet Object which hold the diffrences
 * @param {Boolean} ifLogs to show hide logs
 */

function compareArray(array1, array2, propertyName, diffObjet, caseJson, ingnorableProperties, ifLogs) {

    if (ifLogs) console.log(`*********************${propertyName}1 lenght `, array1.length);
    if (ifLogs) console.log(`*********************${propertyName}2 lenght `, array2.length);
    let diffrenceArray = [];
    let deletedItems = [];
    if (array1 && array1.length > 0) {
        array1.forEach(element => {
            if (element) {
                let identifier = element.identifier;
                if (propertyName === 'CFAssociations') {
                    identifier = element.associationPersistentId;
                }
                let found = false;
                for (let index = 0; index < array2.length; index++) {
                    const element2 = array2[index];
                    let identifier2 = element2.identifier;
                    if (propertyName === 'CFAssociations') {
                        identifier2 = element2.associationPersistentId;
                    }
                    if (identifier2 === identifier) {
                        let diffrence = getDiffrences(element, element2, ingnorableProperties);
                        if (diffrence) {
                            let obj = {
                                id: element.identifier,
                                diff: diffrence,
                                detail: element
                            }
                            if (propertyName === 'CFItems') {
                                obj['itemType'] = element.CFItemType;
                                obj['itemTypeId'] = element.CFItemTypeURI.identifier;
                            }
                            if (propertyName === 'CFAssociations') {
                                obj['itemType'] = element.associationType;
                                obj['itemTypeId'] = element.identifier;
                            }
                            if (propertyName === 'CFItemTypes') {
                                obj['itemType'] = element.title;
                                obj['itemTypeId'] = element.identifier;
                                // console.log('********* diffrence', diffrence);
                            }
                            if (propertyName === 'CFConcepts' ||
                                propertyName === 'CFLicenses' ||
                                propertyName === 'CFSubjects') {
                                let nodedata = getItemDetail(caseJson, element.identifier, propertyName);
                                if (nodedata) {
                                    obj['id'] = nodedata.identifier;
                                    obj['itemType'] = nodedata.CFItemType;
                                    obj['itemTypeId'] = nodedata.CFItemTypeURI.identifier;
                                } else {
                                    obj['itemType'] = element.title;
                                    obj['itemTypeId'] = element.identifier;
                                }
                                // console.log('********* diffrence', diffrence);
                            }
                            diffrenceArray.push(obj);
                        }
                        array2.splice(index, 1);
                        found = true;
                        break;
                    }
                    // console.log('index ',index);
                }
                if (found === false) {
                    if (ifLogs) console.log(`*********************${propertyName} note found  `, identifier);
                    deletedItems.push(element);
                }
            }

        });
    }
    if (ifLogs) console.log(`********************* new ${propertyName} `, array2.length);
    diffObjet[propertyName] = {
        edited: diffrenceArray,
        deleted: deletedItems,
        added: array2
    };
    if (ifLogs) console.log(`********************* deleted ${propertyName} `, deletedItems.length);
}

/**
 * 
 * @param {*} compareArray 
 * @param {*} comparison_id 
 * @param {*} key 
 * @param {*} insertData 
 * @param {*} insertMetaData 
 */
function prepareCaseNodeTypeEditedData(compareArray, comparison_id, key, insertData, insertMetaData, caseJson1, custJson1) {

    compareArray.forEach(element => {
        let row = [];
        // console.log(`-------------${key} |  ${ element.id}  | ${ element.itemType } | ${element.itemTypeId}  `);
        if (element && element.diff) {
            row.push(`'${comparison_id}'`); //comparision_id
            row.push(`'${element.id}'`); // node_id
            row.push(`'${key}'`); //
            row.push(element.itemType ? `'${element.itemType}'` : `''`);
            row.push(element.itemTypeId ? `'${element.itemTypeId}'` : `''`);
            row.push(`'E'`);
            row.push(element.diff.length);
            // row.push(`'${connection.escape(JSON.stringify(element.diff))}'`);
            row.push(`'${(JSON.stringify(element.detail)).replace(/'/g, "\\'")}'`);
            // data = connection.escape(data);
            if (key === 'CFItems' || key === 'CFConcepts' || key === 'CFLicenses') {
                row.push(`'${JSON.stringify(nodeDetail.getNodeDetail(element.id,caseJson1,custJson1)).replace(/'/g, "\\'")}'`)
            } else if (key === 'CFDocument') {
                row.push(`'${JSON.stringify(documentDetail.getDocumentDetail(element.id,caseJson1,custJson1)).replace(/'/g, "\\'")}'`)
            } else {
                row.push(`''`);
            }

            insertData.push(`(${row.join()})`);

            element.diff.forEach(ele => {
                if (ele) {
                    let metadataRow = [];
                    metadataRow.push(`'${comparison_id}'`);
                    metadataRow.push(`'${element.id}'`);
                    metadataRow.push(element.itemTypeId ? `'${element.itemTypeId}'` : `''`);
                    if (key === 'CFSubjects' || key === 'CFConcepts' || key === 'CFLicenses') {
                        metadataRow.push(`'${key +'-'+ ele.path.join()}'`); //Metadata 
                    } else {
                        metadataRow.push(`'${ele.path.join()}'`); //Metadata 
                    }
                    metadataRow.push(`'${ele.kind}'`);
                    metadataRow.push(`'${escape({old:ele.lhs?ele.lhs:'',new:ele.rhs?ele.rhs:''})}'`);
                    metadataRow.push(`'case'`);
                    insertMetaData.push(`(${metadataRow.join()})`);
                }
            });
        }
    });
}

/**
 * 
 * @param {*} compareArray 
 * @param {*} comparison_id 
 * @param {*} key 
 * @param {*} insertData 
 * @param {*} insertMetaData 
 * @param {*} action 
 */
function prepareCaseNodeTypeData(compareArray, comparison_id, key, insertData, insertMetaData, action, json1, custJson1) {
    compareArray.forEach(element => {

        let row = [];
        row.push(`'${comparison_id}'`);
        row.push(`'${element.identifier}'`);
        row.push(`'${key}'`);

        if (key === 'CFItems') {
            row.push(element.CFItemType ? `'${element.CFItemType}'` : `''`);
            row.push(element.CFItemTypeURI.identifier ? `'${element.CFItemTypeURI.identifier}'` : `''`);
        } else if (key === 'CFItemTypes') {
            row.push(element.typeCode ? `'${element.typeCode}'` : `''`);
            row.push(element.identifier ? `'${element.identifier}'` : `''`);
        } else if (key === 'CFAssociations') {
            row.push(element.associationType ? `'${element.associationType}'` : `''`);
            row.push(element.identifier ? `'${element.identifier}'` : `''`);
        } else {
            row.push(`'${element.title}'`);
            row.push(`'${element.identifier}'`);
        }
        row.push(`'${action}'`);
        row.push(1);
        row.push(`'${(JSON.stringify(element)).replace(/'/g, "\\'")}'`);
        if (key === 'CFItems') {
            row.push(`'${JSON.stringify(nodeDetail.getNodeDetail(element.identifier,json1,custJson1)).replace(/'/g, "\\'")}'`)
        } else {
            row.push(`''`);
        }

        insertData.push(`(${row.join()})`);
        prepareCaseCustomData(element, comparison_id, key, insertMetaData, action);
    });
}

/**
 * 
 * @param {*} element 
 * @param {*} key 
 * @param {*} insertMetaData 
 * @param {*} action 
 */
function prepareCaseCustomData(element, comparison_id, key, insertMetaData, action) {

    if (key === 'CFItems') {
        for (const key in element) {
            if (element.hasOwnProperty(key)) {
                if (key !== 'identifier' && key !== 'lastChangeDateTime') {
                    const ele = element[key];
                    if (ele) {
                        let metadataRow = [];
                        metadataRow.push(`'${comparison_id}'`);
                        metadataRow.push(`'${element.identifier}'`);
                        metadataRow.push(element.CFItemTypeURI.identifier ? `'${element.CFItemTypeURI.identifier}'` : `''`);
                        metadataRow.push(`'${key}'`);
                        metadataRow.push(`'${action}'`);
                        metadataRow.push(`'${escape({old:ele,new:''})}'`);
                        metadataRow.push(`'case'`);
                        insertMetaData.push(`(${metadataRow.join()})`);
                    }
                }
            }
        }
        // console.log('added insertMetaData ', insertMetaData);
    }
}

/**
 * 
 * @param {*} json1 
 * @param {*} json2 
 * @param {*} caseJson1 
 * @param {*} caseJson2 
 * @param {*} comparison_id 
 */
function compareCustomJSON(json1, json2, caseJson1, caseJson2, comparison_id) {

    return new Promise((resolve, reject) => {

        try {
            let msg = '';
            let diffObjet = new Object();
            if (json1['ACMTcustomfields']) {
                if (json2['ACMTcustomfields']) {
                    // for (const key in json1['ACMTcustomfields']) 
                    {
                        if (json1['ACMTcustomfields'].hasOwnProperty('CFItem')) {
                            const array1 = json1['ACMTcustomfields']['CFItem'];
                            const array2 = json2['ACMTcustomfields']['CFItem'];
                            compareCustomMetadata(array1, array2, 'CFItem', diffObjet, false);
                        }
                    }
                }
            }

            let insertData = [];
            let insertMetaData = [];
            diffObjet.CFItem.edited.forEach(item => {
                prepareCustomMetaData(item, comparison_id, caseJson1, json1, 'E', insertData, insertMetaData);
            });
            diffObjet.CFItem.added.forEach(item => {
                prepareCustomMetaData(item, comparison_id, caseJson2, json2, 'N', insertData, insertMetaData);
            });
            diffObjet.CFItem.deleted.forEach(item => {
                prepareCustomMetaData(item, comparison_id, caseJson1, json1, 'D', insertData, insertMetaData);
            });

            // console.log('diffObjet.CFItem **************  ', insertData);
            //TODO ADDED & Deleted Item of diffObject.CFItem
            prepareNodeTypeQueryData(insertData).then(result => {
                let msg = result;
                prepareMetadataQueryData(insertMetaData).then((result) => {
                    msg = msg + ' ' + result;
                    resolve({
                        'error': null,
                        'msg': msg
                    });
                }).catch((ex) => {
                    reject(ex);
                });
            }).catch(ex => {
                reject(ex);
            });

        } catch (error) {
            reject({
                'error': error,
                'msg': "Some thing went wrong"
            });
        }
    });
}

function prepareCustomMetaData(item, comparison_id, caseJson1, customJson1, action, insertData, insertMetaData) {
    if (item) {
        let row = [];
        if (item) {
            let data = getItemDetail(caseJson1, item.identifier, 'CFItems');
            let nodetypeId = '';
            row.push(`'${comparison_id}'`);
            row.push(`'${item.identifier}'`);
            row.push(`'CFItems'`);
            if (action === 'E') {
                row.push(item.detail.title ? `'${item.detail.title}'` : `''`);
            } else {
                row.push(item.title ? `'${item.title}'` : `''`);
            }
            if (data && data.CFItemTypeURI) {
                row.push(`'${data.CFItemTypeURI.identifier}'`);
                nodetypeId = data.CFItemTypeURI.identifier;
            } else {
                row.push(`'${item.node_type_id}'`);
                nodetypeId = item.node_type_id;
            }
            row.push(`'${action}'`);
            row.push(1);
            data = JSON.stringify(data);
            row.push(`'${data?data.replace(/'/g, "\\'"):''}'`);
            row.push(`'${JSON.stringify(nodeDetail.getNodeDetail(item.id,caseJson1,customJson1)).replace(/'/g, "\\'")}'`)
            let diffObjet2 = new Object();
            let ingnorableProperties = ['lastChangeDateTime', 'internal_name', 'field_type', 'field_possible_values', 'last_field_possible_values', 'order', 'is_custom', 'is_document', 'is_mandatory', 'is_active', 'is_deleted', 'updated_by', 'created_at', 'updated_at', 'item_linked_id', 'name', 'uploaded_date_time', 'organization_id', 'asset_content_type', 'asset_name', 'asset_size', 'uri', 'metadata_id'];
            let ingnorableProperties2 = ['lastChangeDateTime', 'association_type', 'association_group_id', 'origin_node_id', 'destination_node_id', 'destination_document_id', 'sequence_number', 'description', 'organization_id', 'has_asset', 'uri', 'is_reverse_association', 'assets', 'source_document_id', 'source_item_id', 'target_document_id', 'target_item_id'];
            let custom1 = [];
            let custom2 = [];
            let asset1 = [];
            let asset2 = [];
            let exemplar1 = [];
            let exemplar2 = [];
            let identifier = '';
            switch (action) {
                case 'E':
                    custom1 = item.element1.custom;
                    custom2 = item.element2.custom;
                    asset1 = item.element1.asset;
                    asset2 = item.element2.asset;
                    exemplar1 = item.element1.exemplar_and_association;
                    exemplar2 = item.element2.exemplar_and_association;
                    identifier = item.element1.identifier;
                    break;
                case 'N':
                    custom1 = [];
                    custom2 = item.custom;
                    asset1 = [];
                    asset2 = item.asset;
                    exemplar1 = [];
                    exemplar2 = item.exemplar_and_association;
                    identifier = item.identifier;
                    break;
                case 'D':
                    custom1 = item.custom;
                    custom2 = [];
                    asset1 = item.asset;
                    asset2 = [];
                    exemplar1 = item.exemplar_and_association;
                    exemplar2 = [];
                    identifier = item.identifier;
                    break;
                default:
                    break;
            }

            compareCustomMetadataArray(custom1, custom2, 'metadata_id', identifier, nodetypeId, 'custom', diffObjet2, false, ingnorableProperties);
            compareCustomMetadataArray(asset1, asset2, 'identifier', identifier, nodetypeId, 'asset', diffObjet2, false, ingnorableProperties);
            compareCustomMetadataArray(exemplar1, exemplar2, 'source_item_association_id', identifier, nodetypeId, 'exemplar_and_association', diffObjet2, false, ingnorableProperties2);
            //Check is there any diffrence or not if there are diffrences then add item 
            let hasDiffrences = false;
            // Here Key are 'custom', 'asset'
            //console.log('----------Action ', action, diffObjet2);
            for (const key in diffObjet2) {
                if (diffObjet2.hasOwnProperty(key)) {
                    const element = diffObjet2[key];
                    createInsertData(element.edited, insertMetaData, 'E', comparison_id);
                    createInsertData(element.added, insertMetaData, 'N', comparison_id);
                    createInsertData(element.deleted, insertMetaData, 'D', comparison_id);
                    if (element.edited && element.edited.length > 0 ||
                        element.added && element.edited.added > 0 ||
                        element.deleted && element.deleted.length > 0) {
                        hasDiffrences = true;
                    }
                }
            }
            // if there is differenct then add it nodetype
            if (hasDiffrences && action === 'E') {
                insertData.push(`(${row.join()})`);
            }
            if (action === 'N' || action === 'D') {
                insertData.push(`(${row.join()})`);
            }
        }
    }
}
/**
 * Get node detail by node id
 * @param {*} caseJson 
 * @param {*} identifier
 * @param {*} type
 * @Return Node Detail Object
 */
function getItemDetail(caseJson, identifier, type) {
    if (caseJson && caseJson.CFItems) {
        for (let index = 0; index < caseJson.CFItems.length; index++) {
            const element = caseJson.CFItems[index];
            // console.log('getItemDetail   ',element.identifier,identifier);
            switch (type) {
                case 'CFItems':
                    if (element.identifier === identifier) {
                        return element;
                    }
                    break;
                case 'CFConcepts':
                    if (element.conceptKeywordsURI) {
                        if (element.conceptKeywordsURI.identifier === identifier) {
                            return element;
                        }
                    }
                    break;
                case 'CFLicenses':
                    if (element.licenseURI) {
                        if (element.licenseURI.identifier === identifier) {
                            return element;
                        }
                    }
                    break;
                case 'CFSubjects':
                    if (element.subjectURI) {
                        if (element.subjectURI.identifier === identifier) {
                            return element;
                        }
                    }
                    break;
                default:
                    break;
            }

        }
    }
}

function compareCustomMetadata(array1, array2, propertyName, diffObjet, ifLogs) {

    if (ifLogs) console.log(`*********************${propertyName}1 lenght `, array1.length);
    if (ifLogs) console.log(`*********************${propertyName}2 lenght `, array2.length);
    let diffrenceArray = [];
    let deletedItems = [];
    if (array1 && array1.length > 0) {
        array1.forEach(element => {
            if (element) {
                let identifier = element.identifier;
                let found = false;
                for (let index = 0; index < array2.length; index++) {
                    const element2 = array2[index];
                    if (element2 && element2.identifier === identifier) {
                        let diffrence = getDiffrences(element, element2, ['lastChangeDateTime', 'internal_name', 'field_type', 'field_possible_values', 'last_field_possible_values', 'order', 'is_custom', 'is_document', 'is_mandatory', 'is_active', 'is_deleted', 'updated_by', 'created_at', 'updated_at', 'uri']);
                        if (diffrence) {
                            let obj = {
                                identifier: identifier,
                                diff: [],
                                detail: {
                                    "identifier": element.identifier,
                                    "node_type_id": element.node_type_id,
                                    "title": element.title
                                },
                                element1: element,
                                element2: element2
                            }
                            if (propertyName === 'CFItems') {
                                obj['itemType'] = element.title;
                                obj['itemTypeId'] = element.node_type_id;
                            }
                            diffrenceArray.push(obj);
                        }
                        array2.splice(index, 1);
                        found = true;
                        break;
                    }
                    // console.log('index ',index);
                }
                if (found === false) {
                    if (ifLogs) console.log(`*********************${propertyName} note found  `, identifier);
                    deletedItems.push(element);
                }
            }

        });
    }
    // if (ifLogs) console.log(`********************* new ${propertyName} `, array2.length);
    // console.log('diffrencte ', diffrenceArray);

    diffObjet[propertyName] = {
        edited: diffrenceArray,
        deleted: deletedItems,
        added: array2
    };
    // if (ifLogs) console.log(`********************* deleted ${propertyName} `, deletedItems.length);
}
/**
 * 
 * @param {*} array1 
 * @param {*} array2 
 * @param {*} id_propertie 
 * @param {*} node_id 
 * @param {*} node_type_id 
 * @param {*} propertyName 
 * @param {*} diffObjet 
 * @param {*} ifLogs 
 * @param {*} ingnorableProperties 
 */
function compareCustomMetadataArray(array1, array2, id_propertie, node_id, node_type_id, propertyName, diffObjet, ifLogs, ingnorableProperties) {
    let diffrenceArray = [];
    let deletedItems = [];
    //console.log('compareCustomMetadataArray ',array1,array2);
    if (array1 && array1.length > 0) {
        array1.forEach(element => {
            if (element) {
                let identifier = element[id_propertie];
                let found = false;
                for (let index = 0; index < array2.length; index++) {
                    const element2 = array2[index];
                    if (propertyName !== 'custom') {
                        if (element2 && element2[id_propertie] === identifier) {
                            let diffrence = getDiffrences(element, element2, ingnorableProperties);
                            if (diffrence) {
                                diffrenceArray.push(getDiffrencObj(node_id, diffrence, element, node_type_id, propertyName));
                            }
                            array2.splice(index, 1);
                            found = true;
                            break;
                        }
                    } else {
                        //console.log('Custom ******* out ', propertyName, '-', element2.name, '-', element.name, '-', element2.field_type, '-', element.field_type)
                        // For custom metada we are comparing based on name & field_type
                        if (element2 && element2.name === element.name && element2.field_type === element.field_type) {
                            // console.log('Custom $$$$$$$$$$$$$$$$$$$$$$ ', propertyName, '-', element2.name, '-', element.name, '-', element2.field_type, '-', element.field_type)
                            let diffrence = getDiffrences(element, element2, ingnorableProperties);
                            if (diffrence) {
                                diffrenceArray.push(getDiffrencObj(node_id, diffrence, element, node_type_id, propertyName));
                            }
                            array2.splice(index, 1);
                            found = true;
                            break;
                        }
                    }
                }
                if (found === false) {
                    deletedItems.push(getDiffrencObj(node_id, [element], element, node_type_id, propertyName));
                }
            }
        });


    }
    let addedArray = [];
    for (let index = 0; index < array2.length; index++) {
        const arrObj = array2[index];
        addedArray.push(getDiffrencObj(node_id, [arrObj], arrObj, node_type_id, propertyName));
    }
    // console.log('compareCustomMetadataArray ****** ', diffrenceArray, deletedItems, addedArray);
    diffObjet[propertyName] = {
        edited: diffrenceArray,
        deleted: deletedItems,
        added: addedArray
    };
}

function getDiffrencObj(node_id, diffArr, element, node_type_id, propertyName) {

    return {
        id: node_id,
        diff: diffArr,
        detail: element,
        itemTypeId: node_type_id,
        propertyName: propertyName
    }
}

function createInsertData(objArr, insertMetaData, action, comparison_id) {
    objArr.forEach(element => {
        element.diff.forEach(ele => {
            if (ele) {
                let metadataRow = [];
                metadataRow.push(`'${comparison_id}'`);
                metadataRow.push(`'${element.id}'`);
                metadataRow.push(element.itemTypeId ? `'${element.itemTypeId}'` : `''`);
                if (element.propertyName === 'custom') {
                    metadataRow.push(`'${element.detail.name}'`); // Name of custome metadata
                }
                if (element.propertyName === 'asset') {
                    metadataRow.push(`'${element.detail.title}'`); // Name of custome metadata
                }
                if (element.propertyName === 'exemplar_and_association') {
                    let title = element.detail.external_node_title;
                    metadataRow.push(`'${title}'`)
                }
                metadataRow.push(`'${action}'`);
                if (action === 'E') {

                    metadataRow.push(`'${escape({old:ele.lhs?ele.lhs:'',new:ele.rhs?ele.rhs:''})}'`);
                }
                if (action === 'N') {
                    metadataRow.push(`'${escape({old:'',new:ele})}'`);
                }
                if (action === 'D') {
                    metadataRow.push(`'${escape({old:ele,new:''})}'`);
                }
                metadataRow.push(`'${element.propertyName}'`)
                insertMetaData.push(`(${metadataRow.join()})`);
            }
        });
    });
}

function escape(data){
    return   JSON.stringify(data).replace(/'/g, "\\'");

}
function prepareNodeTypeQueryData(insertData) {

    return new Promise((resolve, reject) => {
        try {
            let msg = '';
            if (insertData && insertData.length > 0) {

                const resultArr = chunkArray(insertData, batchSize);
                for (let i = 0; i < resultArr.length; i++) {
                    insertDataInToTable(resultArr[i]).then((resule) => {
                        console.log('insertDataInToTable  ');
                    }).catch((ex) => {
                        console.log('insertDataInToTable error  ', ex);
                    });
                    interval = setInterval(() => {}, 100);
                }
                msg = 'Sucessfully generated comparison';
            } else {
                console.log('There are no changes in both version!');
                msg = 'There are no changes in both version!';
            }
            resolve(msg);
        } catch (error) {
            reject(error);
        }

    })

}

function prepareMetadataQueryData(insertMetaData) {

    return new Promise((resolve, reject) => {

        try {
            let msg = '';
            if (insertMetaData && insertMetaData.length > 0) {

                const resultArr = chunkArray(insertMetaData, batchSize);
                for (let i = 0; i < resultArr.length; i++) {
                    executeInsertMetaDataQuery(resultArr[i]).then((result) => {
                        console.log('executeInsertMetaDataQuery  ');

                    }).catch((ex) => {
                        console.log('executeInsertMetaDataQuery error  ', ex);
                        reject(ex);
                        return;
                    });
                    interval = setInterval(() => {}, 100);
                }
                msg = 'Sucessfully generated comparison';
            } else {
                console.log('There are no changes in METADATA OF both version!');
                msg = msg + ' There are no changes in METADATA OF both version!'
            }
            resolve(msg);
        } catch (ex) {
            reject(ex);
        }
    });

}



/**
 * 
 * @param {Array} resultArr array of values which need to be inserted
 */
function insertDataInToTable(resultArr) {

    return new Promise((resolve, reject) => {
        try {
            let insertQuery = `INSERT INTO acmt_version_node_type (\`comparison_id\`,\`node_id\`,\`type\`,\`node_type\`,\`node_type_id\`,\`action\`,\`count\`,\`value\`,\`node_detail\`) VALUES ${resultArr.join()}`;
            // console.log('insertQuery ', insertQuery);
            connection.query(insertQuery, function (error, results, fields) {
                if (error) {
                    reject({
                        'function': 'insertDataInToTable',
                        'error': error
                    });
                }
                resolve(results);
            });
        } catch (error) {
            reject({
                'function': 'insertDataInToTable',
                'error': error
            });
        }
    });
}
/**
 * 
 * @param {*} resultArr 
 */
function executeInsertMetaDataQuery(resultArr) {

    return new Promise((resolve, reject) => {
        try {
            let insertQuery = `INSERT INTO acmt_version_metadata (\`comparison_id\`,\`node_id\`,\`node_type_id\`,\`metadata\`,\`action\`,\`value\`,\`metadta_type\`) VALUES ${resultArr.join()}`;
            //  console.log('insertQuery ', insertQuery);
            connection.query(insertQuery, function (error, results, fields) {
                if (error) {
                    reject({
                        'function': 'insertDataInToTable',
                        'error': error
                    });
                }
                resolve(results);
            });
        } catch (error) {
            reject({
                'function': 'insertDataInToTable',
                'error': error
            });
        }
    });
}

/**
 * 
 * @param {JSON} json1 json object 
 * @param {JSON} json2 jsone object 
 * @param {Array} ingnorableProperties array of json properties which you don't want to compare
 */
function getDiffrences(json1, json2, ingnorableProperties) {

    return diff(json1, json2, function (path, key) {
        let found = false;
        for (let index = 0; index < ingnorableProperties.length; index++) {
            const element = ingnorableProperties[index];
            if (element === key) {
                found = true;
            }
        }
        return found;
    });
}
/**
 * 
 * @param {Array} array 
 * @param {Int} size 
 */
function chunkArray(array, size) {
    if (array.length <= size) {
        return [array]
    }
    return [array.slice(0, size), ...chunkArray(array.slice(size), size)]
}

/**
 * Get comarison details (json files url) from acmt_version_comparison table using comparision_id 
 * @param {string} comparison_id 
 */
function getComparisonDetail(comparison_id) {
    return new Promise((resovle, reject) => {

        let data = [];
        let query = `SELECT * from acmt_version_comparison where comparison_id = '${comparison_id}'`;
        connection.query(query, function (error, results, fields) {
            if (error) {
                reject({
                    'function': 'getComparisonDetail',
                    'error': error
                });
            }
            if (results && results.length > 0) {
                resovle(results[0]);
            } else {
                reject({
                    'function': 'getComparisonDetail',
                    'error': "Please provide valid comparison_id"
                });
            }
        });
    });
}

function readDataFromS3(path) {
    return new Promise((resolve, reject) => {
        let jsonObj = null;
        try {

            const params = {
                Bucket: config.AWSS3.AWS_S3_BUCKET,
                Key: path
            };
            S3.getObject(params, function (error, data) {

                if (error) {
                    reject({
                        message: error.message,
                        code: error.code,
                        path: path
                    });
                } else {
                    jsonObj = JSON.parse(data.Body.toString());
                    resolve({
                        json: jsonObj,
                        error: null
                    });
                }

            })

        } catch (error) {
            reject(error)
        }

    });
}


function compare(comparison_id) {

    return new Promise((resolve, reject) => {

        try {
            getComparisonDetail(comparison_id).then((result) => {
                let caseJsonData, caseJsonData2 = null;
                let cusJsonData, cusJsonData2 = null;
                readCaseJSON(result.first_version_case_url, result.second_version_case_url, comparison_id, true, null).then((res) => {
                    caseJsonData = res.json1;
                    caseJsonData2 = res.json2;
                    readCaseJSON(result.first_version_custom_url, result.second_version_custom_url, comparison_id, false, res).then((res) => {
                        cusJsonData = res.json1;
                        cusJsonData2 = res.json2;
                        compareJSON(caseJsonData, caseJsonData2, cusJsonData, cusJsonData2, comparison_id).then((result) => {
                            compareCustomJSON(cusJsonData, cusJsonData2, caseJsonData, caseJsonData2, comparison_id).then((result) => {
                                resolve(result);
                            }).catch((ex) => {
                                reject(ex)
                            });
                        }).catch((ex) => {
                            reject(ex)
                        });
                    }).catch((ex) => {
                        reject(ex);
                    })
                }).catch((ex) => {
                    reject(ex);
                })

            }).catch((ex) => {
                reject(ex);
            });


        } catch (ex) {
            reject(ex)
        }
    });
}

function readCaseJSON(firstUrl, secondUrl, comparison_id, isCase, jsonConatiner) {

    return new Promise((resolve, reject) => {
        let jsonData, jsonData2 = null;
        readDataFromS3(firstUrl).then((data) => {
            jsonData = data.json;
            readDataFromS3(secondUrl).then((data2) => {
                jsonData2 = data2.json;
                resolve({
                    json1: jsonData,
                    json2: jsonData2
                });
            }).catch((ex) => {
                reject(ex);

            });
        }).catch((ex) => {
            reject(ex);
        });
    });

}

exports.compare = compare;