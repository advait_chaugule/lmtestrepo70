function getDocumentDetail(documentId, caseJson, customJson) {
    //return new Promise((resolve, reject) => {
    try {
        let documentDetailJson = require('./documnet_detail.json');
        let document_detail = JSON.parse(JSON.stringify(documentDetailJson));
        documentDetailJson = null;
        if (caseJson && caseJson.CFDocument) {
            const element = caseJson.CFDocument;
            if (element.identifier === documentId) {
                document_detail.item_id = element.identifier;
                document_detail.source_document_id = element.identifier;
                document_detail.creator = element.creator;
                document_detail.title = element.title;
                document_detail.adoption_status = element.adoptionStatus;
                document_detail.node_type_id = element.nodeTypeId?element.nodeTypeId:'';
                document_detail.custom_metadata = getCustomMetadata(documentId, customJson);
                document_detail.assets = getAssets(documentId,customJson);
            }

        }
        return document_detail;
    } catch (error) {
        throw (error);
    }
}


function getCustomMetadata(documentId, json) {

    let customMetaDataArr = [];
    if (json.ACMTcustomfields && json.ACMTcustomfields.CFDocument) {
        for (let index = 0; index < json.ACMTcustomfields.CFDocument.length; index++) {
            const document = json.ACMTcustomfields.CFDocument[index];
            if (document.identifier === documentId) {
                for (let index = 0; index < document.custom.length; index++) {
                    const element = document.custom[index];
                    let metadata = {
                        "metadata_id": element.metadata_id,
                        "title": element.name,
                        "field_type":element.field_type,
                        "field_possible_values": element.field_possible_values,
                        "metadata_value": element.metadata_value,
                        "metadata_value_html":  element.metadata_value,
                        "is_additional_metadata": 1
                    }
                    customMetaDataArr.push(metadata);
                }
                break;
            }
        }
    }
    return customMetaDataArr;
}


function getAssets(document_id,json) {
    let assets = [];
    if (json.ACMTcustomfields && json.ACMTcustomfields.CFDocument) {
        for (let index = 0; index < json.ACMTcustomfields.CFDocument.length; index++) {
            const document = json.ACMTcustomfields.CFDocument[index];
            if (document.identifier === document_id) {
                for (let index = 0; index < document.document_asset.length; index++) {
                    const element = document.document_asset[index];
                    let asset = {
                        "asset_id": element.identifier,
                        "title": element.title,
                        "description": element.description,
                        "preview_url": element.uri,
                        "download_url": element.uri,
                        "asset_file_name": element.name,
                        "asset_content_type": element.asset_content_type,
                        "asset_size_in_bytes": element.asset_size,
                        "asset_target_file_name": element.asset_name,
                        "asset_uploaded_date_time": element.uploaded_date_time
                    }
                    assets.push(asset);
                }
                break;
            }
        }
    }
    return assets;
}


exports.getDocumentDetail = getDocumentDetail;