function getNodeDetail(item_id, caseJson, customJson) {
    //return new Promise((resolve, reject) => {
    try {
        let nodeDetailJson = require('./node_detail.json');
        let node_detail = JSON.parse(JSON.stringify(nodeDetailJson));
        nodeDetailJson = null;
        if (caseJson && caseJson.CFItems) {
            for (let index = 0; index < caseJson.CFItems.length; index++) {
                const element = caseJson.CFItems[index];
                if (element.identifier === item_id) {
                    //console.log('getItemDetail   ', element.identifier, item_id);
                    node_detail.item_id = element.identifier;
                    node_detail.source_item_id = element.identifier;
                    node_detail.full_statement = element.fullStatement;
                    node_detail.full_statement_html = element.fullStatement;
                    node_detail.human_coding_scheme = element.humanCodingScheme;
                    node_detail.human_coding_scheme_html = element.humanCodingScheme;
                    node_detail.list_enumeration = '';
                    node_detail.education_level = element.educationLevel ? element.educationLevel : '';
                    node_detail.notes = element.notes ? element.notes : '';
                    node_detail.notes_html = element.notes ? element.notes : '';
                    if (element.conceptKeywordsURI) {
                        node_detail.concept_id = element.conceptKeywordsURI ? element.conceptKeywordsURI.identifier : '';
                        node_detail.concept_title = element.conceptKeywordsURI ? element.conceptKeywordsURI.title : '';
                        node_detail.concept_title_html = element.conceptKeywordsURI ? element.conceptKeywordsURI.title : '';
                        let concept = getConceptDetail(node_detail.concept_id, caseJson.CFDefinitions.CFConcepts);
                        if (concept) {
                            node_detail.concept_description = concept.description;
                            node_detail.concept_description_html = concept.description;
                            node_detail.concept_keywords = concept.keywords;
                        }
                    }
                    if (element.licenseURI) {
                        node_detail.license_id = element.licenseURI.identifier;
                        node_detail.license_title = element.licenseURI.title;
                        node_detail.license_title_html = element.licenseURI.title;
                        let license = getLicenseDetail(node_detail.license_id, caseJson.CFDefinitions.CFLicenses);
                        if (license) {
                            node_detail.license_text = license.licenseText;
                            node_detail.license_text_html = license.licenseText;
                            node_detail.license_description = license.description;
                            node_detail.license_description_html = license.description;
                        }
                    }
                    node_detail.node_type = element.CFItemType;
                    node_detail.node_type_id = element.CFItemTypeURI ? element.CFItemTypeURI.identifier : '';
                    node_detail.document_id = element.CFDocumentURI ? element.CFDocumentURI.identifier : '';
                    node_detail.item_associations = getItemAssociations(element.identifier, caseJson.CFAssociations);
                    node_detail.linked_item = getLinkedItems(); //TODO
                    node_detail.status_start_date = element.statusStartDate;
                    node_detail.status_end_date = element.statusEndDate;
                    node_detail.custom_metadata = getCustomMetadata(item_id, customJson);
                    node_detail.exemplar_associations = getExemplar(item_id,customJson);
                    node_detail.assets = getAssets(item_id,customJson);
                    node_detail.language_name = element.language;
                    break;
                }
            }
        }
        return node_detail;
    } catch (error) {
        throw (error);
    }

    /* }).catch((ex) => {
         console.log("EXception ", ex);
     });*/
}

function getConceptDetail(concept_id, concepts) {
    for (let index = 0; index < concepts.length; index++) {
        const concept = concepts[index];
        if (concept_id === concept.identifier) {
            return concept;
        }

    }
}

function getLicenseDetail(license_id, licenses) {
    for (let index = 0; index < licenses.length; index++) {
        const license = licenses[index];
        if (license_id === license.identifier) {
            return license;
        }

    }
}

function getItemAssociations(item_id, associations) {

    let associationArr = [];
    for (let index = 0; index < associations.length; index++) {
        const asso = associations[index];
        if (asso) {
            let assoJson = JSON.parse(JSON.stringify(associationJson));
            if (assoJson) {
                if ((asso.originNodeURI && assoJson.origin_node.item_id === item_id) || (asso.destinationNodeURI && asso.destinationNodeURI.identifier === item_id)) {
                    assoJson.item_association_id = asso.identifier;
                    assoJson.association_type.type_name = asso.associationType;
                    assoJson.association_type.display_name = asso.associationType;
                    assoJson.document.document_id = asso.CFDocumentURI.identifier;
                    assoJson.document.title = asso.CFDocumentURI.title;
                    assoJson.document.uri = asso.CFDocumentURI.uri;
                    if (asso.originNodeURI) {
                        assoJson.origin_node.item_id = asso.originNodeURI.identifier;
                        assoJson.origin_node.uri = asso.originNodeURI.uri;
                        if (asso.originNodeURI.title) {
                            let arr = asso.originNodeURI.title.split(':');
                            if (arr && arr.length > 3) {
                                assoJson.origin_node.humanCodingScheme = arr[2];
                                assoJson.origin_node.fullStatement = arr[3];
                            }
                        }
                    }

                    if (asso.destinationNodeURI) {
                        assoJson.destination_node.item_id = asso.destinationNodeURI.identifier;
                        let arr = asso.destinationNodeURI.title.split(':');
                        if (arr && arr.length > 3) {
                            assoJson.destination_node.humanCodingScheme = arr[2];
                            assoJson.destination_node.fullStatement = arr[3];
                        }
                    }
                    assoJson.list_enumeration = asso.sequenceNumber;
                    associationArr.push(assoJson);
                }
            }
        }

    }
    return associationArr;

}

function getLinkedItems() {
    return [];
}

function getCustomMetadata(item_id, json) {
    let customMetaDataArr = [];
    if (json.ACMTcustomfields && json.ACMTcustomfields.CFItem) {
        for (let index = 0; index < json.ACMTcustomfields.CFItem.length; index++) {
            const item = json.ACMTcustomfields.CFItem[index];
            if (item.identifier === item_id) {
                for (let index = 0; index < item.custom.length; index++) {
                    const element = item.custom[index];
                    let metadata = {
                        "metadata_id": element.metadata_id,
                        "title": element.name,
                        "field_type": element.field_type,
                        "field_possible_values": element.field_possible_values,
                        "metadata_value": element.metadata_value,
                        "metadata_value_html": element.metadata_value,
                        "is_additional_metadata": 1
                    }
                    customMetaDataArr.push(metadata);
                }
                break;
            }
        }
    }
    return customMetaDataArr;
}

function getExemplar(item_id,json) {
    let exemplars = [];
    if (json.ACMTcustomfields && json.ACMTcustomfields.CFItem) {
        for (let index = 0; index < json.ACMTcustomfields.CFItem.length; index++) {
            const item = json.ACMTcustomfields.CFItem[index];
            if (item.identifier === item_id) {
                for (let index = 0; index < item.exemplar_and_association.length; index++) {
                    const element = item.exemplar_and_association[index];
                    let exeplar = {
                        "association_id": element.origin_node_id,
                        "title": element.external_node_title,
                        "description": element.description,
                        "has_asset": element.has_asset,
                        "external_url": element.external_node_url,
                        "external_download_url": element.external_node_url,
                        "asset_id": "",
                        "asset_file_name": "",
                        "asset_content_type": "",
                        "asset_size_in_bytes": 0,
                        "asset_target_file_name": "",
                        "asset_uploaded_date_time": ""
                      }
                    exemplars.push(exeplar);
                }
                break;
            }
        }
    }
    return exemplars;
}

function getAssets(item_id,json) {
    let assets = [];
    if (json.ACMTcustomfields && json.ACMTcustomfields.CFItem) {
        for (let index = 0; index < json.ACMTcustomfields.CFItem.length; index++) {
            const item = json.ACMTcustomfields.CFItem[index];
            if (item.identifier === item_id) {
                for (let index = 0; index < item.asset.length; index++) {
                    const element = item.asset[index];
                    let asset = {
                        "asset_id": element.identifier,
                        "title": element.title,
                        "description": element.description,
                        "preview_url": element.uri,
                        "download_url": element.uri,
                        "asset_file_name": element.name,
                        "asset_content_type": element.asset_content_type,
                        "asset_size_in_bytes": element.asset_size,
                        "asset_target_file_name": element.asset_name,
                        "asset_uploaded_date_time": element.uploaded_date_time
                    }
                    assets.push(asset);
                }
                break;
            }
        }
    }
    return assets;
}
let associationJson = {
    "item_association_id": "",
    "created_at": "",
    "association_type": {
        "type_id": 1,
        "type_name": "",
        "display_name": ""
    },
    "document": {
        "document_id": "",
        "title": "",
        "official_source_url": "",
        "language_id": "",
        "adoption_status": 1,
        "status": 1,
        "notes": "",
        "publisher": "",
        "description": "",
        "version": "",
        "status_start_date": "",
        "status_end_date": "",
        "license_id": "",
        "source_license_uri_object": null,
        "organization_id": "",
        "document_type": 1,
        "creator": "",
        "is_deleted": 0,
        "created_by": "",
        "updated_by": "",
        "source_document_id": "",
        "pulished_taxonomy_snapshot_s3_zip_file_id": null,
        "node_type_id": "",
        "project_id": "",
        "node_template_id": "",
        "created_at": "",
        "updated_at": "",
        "import_type": 2,
        "actual_import_type": 2,
        "uri": "",
        "title_html": null,
        "notes_html": null,
        "publisher_html": null,
        "description_html": null,
        "custom_view_visibility": 0,
        "display_options": null,
        "import_status": 0,
        "is_locked": 0,
        "source_type": null
    },
    "origin_node": {
        "item_id": "",
        "parent_id": "",
        "document_id": "",
        "full_statement": "",
        "alternative_label": null,
        "item_type_id": null,
        "human_coding_scheme": "",
        "list_enumeration": "",
        "abbreviated_statement": null,
        "concept_id": "",
        "notes": null,
        "language_id": null,
        "education_level": null,
        "license_id": "",
        "source_license_uri_object": "",
        "status_start_date": null,
        "status_end_date": null,
        "is_deleted": 0,
        "updated_by": "",
        "organization_id": "",
        "source_item_id": "",
        "node_type_id": "",
        "created_at": "",
        "updated_at": "",
        "uri": "",
        "source_node_type_uri_object": "",
        "source_concept_uri_object": "",
        "full_statement_html": "",
        "alternative_label_html": null,
        "human_coding_scheme_html": "",
        "abbreviated_statement_html": null,
        "notes_html": null,
        "custom_view_visibility": 0,
        "custom_view_data": "",
        "import_type": 2
    },
    "destination_node": {
        "item_id": "",
        "parent_id": "",
        "item_association_id": "",
        "destination_node_url": "",
        "human_coding_scheme": "",
        "full_statement": "",
        "node_type_title": "",
        "document": {
            "document_id": "",
            "document_title": ""
        },
        "project_type": 0,
        "project_id": "",
        "is_external": 0
    },
    "description": "",
    "metadata": [

    ],
    "sequence_number": 1
};

exports.getNodeDetail = getNodeDetail;
/*const json1 = require('./json/451c8d95-8c62-4973-9df0-bb759cd99514.json');
const jsoncust = require('./json/451c8d95-8c62-4973-9df0-bb759cd99514_custom.json');
getNodeDetail("15628cbb-2064-4ae4-8d70-83956335b371", json1, jsoncust).then((result) => {
    console.log('retuslt ', result);

}).catch((ex) => {

});*/