class Utils {


  static sortData(data, sortBy = 'human_coding_scheme') {
    if (data instanceof Array) {
      if (data.length > 0) {
        sortBy = this.checkSortType(data);
      }
    } else {
      if (data.children && data.children.length > 0) {
        sortBy = this.checkSortType(data.children);
      }
    }
    data.sort(function (a, b) {

      if (sortBy === 'sequence_number' &&
        parseInt(a[sortBy], 10) && parseInt(b[sortBy], 10)) {
        return (parseInt(a[sortBy], 10) < parseInt(b[sortBy], 10) ? -1 : 1) * (1);
      } else {
        const valA = a[sortBy].toUpperCase();
        const valB = b[sortBy].toUpperCase();

        let comparison = 0;
        if (valA > valB) {
          comparison = 1;
        } else if (valA < valB) {
          comparison = -1;
        }
        return comparison;
      }

    });

    data.forEach(element => {
      if (element.children && element.children.length > 0) {
        sortBy = this.checkSortType(element.children);
        this.sortData(element.children, sortBy);
      }
    });
  }

  static checkSortType(children) {
    if (children[0].sequence_number && children[0].sequence_number.trim() !== '' && children[0].sequence_number !== '0' &&
      children[0].sequence_number !== 'undefined') {
      return 'sequence_number';
    } else {
      if (children[0].human_coding_scheme && children[0].human_coding_scheme.trim() !== '') {
        return 'human_coding_scheme';
      } else {
        return 'full_statement';
      }
    }
  }



  static getMaxLE(nodes) {
    const listEnumerations = [];
    for (const node of nodes) {
      listEnumerations.push(node.sequence_number);
    }
    return (Math.max(...listEnumerations) + 1);
  }

  static checkNumber(value) {
    return parseInt(value, 10);
  }

  static sortDataArray(array, sortby, isDate, isAsce) {
    array.sort(function (a, b) {
      let val1 = '';
      let val2 = '';
      if (!isDate && typeof (a[sortby]) === 'number') {
        val1 = a[sortby];
        val2 = b[sortby];
      } else {
        val1 = a[sortby] ? a[sortby].toUpperCase() : '';
        val2 = b[sortby] ? b[sortby].toUpperCase() : '';
      }
      if (isDate) {
        val1 = new Date(a[sortby].replace(/-/g, '/'));
        val2 = new Date(b[sortby].replace(/-/g, '/'));
      }
      if (val1 === val2) {
        return 0;
      }
      if (val1 > val2) {
        return isAsce ? 1 : -1;
      }
      if (val1 < val2) {
        return isAsce ? -1 : 1;
      }
    });
  }

  static getApiUrl(env) {
    var url = '';
    switch (env) {
      case 'dev':
        url = 'https://api.acmt-dev.learningmate.co/server';
        break;
      case 'qa':
        url = 'https://api.acmt-qa.learningmate.com/server';
        break;
      case 'uat':
        url = 'https://api.acmt-staging.learningmate.com/server';
        break;
      case 'comet':
        url = 'https://api.comet-stg.learningmate.com/server';
        break;
      case 'prod':
        url = 'https://api.acmt-prod.learningmate.com/server';
        break;
      case 'preprod':
        url = 'https://api.acmt-preprod.learningmate.com/server';
        break;
      default:
        url = 'http://localhost/lmp70/api/v2/public';

    }

    return url;
  }

  static getCustomMetadataType(val) {
    const metadataType = +val;
    let type = '';
    switch (metadataType) {
      case 1:
        type = 'text';
        break;
      case 2:
        type = 'long text';
        break;
      case 3:
        type = 'list';
        break;
      case 4:
        type = 'date';
        break;
      case 5:
        type = 'time';
        break;
      case 6:
        type = 'number';
        break;
    }
    return type;

  }
}

module.exports = {
  Utils
};