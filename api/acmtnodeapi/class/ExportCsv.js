const {Utils} =  require('./Utils.js');
const config = require('../config/default_'+process.env.NODEJSENV+'.json');
const ExportLib = require('export-to-csv');
const AWS = require('aws-sdk')
const querystring = require('querystring');
const axios = require('axios');

//AWS.config.update({region: 'us-east-1'});

class  ExportCsv 
{ 
    

    constructor(data, email, orgid) {
    
    this.emailadd = email;
    this.orgid = orgid;
    this.customMetadata = [];
  //colHeaders = ['full_statement', 'human_coding_scheme']; // holds internal names for column headers
    this.metadataList = []; // holds metadata list used for table view headers
    this.caseMetadataList = [];
    this.associationList = []; // holds associations list for csv export
    this.exportLoading = false; // Loading state for taxonomy export
    this.textLength = 50;
    this.taxonomyTitle = '';
    
    this.nodes = [];
    this.relations = [];
    this.treeResponseData = {};
    
    this.parentObj = {};
    this.topParentIds = [];
    this.expandLevel = 1;
    this.rootNodeId = '';
    this.orphanNodeList = [];
    this.topMostOrphanNodes = []; // holds list of top most orphan nodes
    this.externalRelations = [];
    this.taxonomyTitle = '';
    
        this.treeResponseData = data;
        
        this.nodes = this.treeResponseData.nodes;
        this.setRelations(true);
        this.findTopmostOrphan();
       
        this.parseTreeData().then((response) => { 

       const res = response;
       this.exportData = this.nodes;
       this.associationList = this.externalRelations;
       this.exportData.forEach(csvdata => {
        const caseMetadata = csvdata.case_metadata[0];
        for (const metadata in caseMetadata) {
          if (this.caseMetadataList.findIndex(x => x.metadata_name === metadata.toLowerCase()) === -1) {
            this.caseMetadataList.push({
                     metadata_name: metadata
            });
          }
        }
        csvdata.metadata_detail.forEach(item => {
          if (this.customMetadata.findIndex(x => x.name === item.name.toLowerCase()) === -1) {
            this.customMetadata.push({
              name: item.name.toLowerCase(),
              field_type: item.field_type
            });
          }
        });
      });
      const data = [];
      // Utils.sortData(res.children);
      
      this.createCSVData(data, res);
      
      this.exportCSVData(data, this.taxonomyTitle);
    }).catch(ex => {
      console.log('Export CSV error ', ex);
      this.exportLoading = false;
    });
        
 
  }

   
    setRelations(isExport = false) {
        let apiResponse  = this.treeResponseData;
     
        let relationsArr = [];
        
        apiResponse.relations.forEach(relation => {
          if (relation) {
            let assoCondition;
            if (!isExport) {
              assoCondition = relation.external_link === true || relation.external_association;
            } else {
              assoCondition = true;
            }
            if (assoCondition) {
              // external_association flag for export CSV external association
              // if (relation.external_association) {
              this.externalRelations.push(relation);
              // }
              // For external taxonomy, if relation does not exists in array, push it otherwise don't push for dulpicating
              if (!this.isRelationDupli(relationsArr, relation)) {
                relationsArr.push(relation);
              }
            } else {
              // If not external taxonomy, push it directly
              relationsArr.push(relation);
            }
          }
        });
       
        this.relations = relationsArr;
 
      }  

      findTopmostOrphan() {
        const nodes = this.nodes;
        
        for (const node of nodes) {
          if (node.is_orphan === 1) { // top most orphan node
            this.topMostOrphanNodes.push(node);
          }
        }
      }


      parseTreeData() {
        return new Promise((resolve, reject) => {
          
        const parsedTrees = [];
        if (this.nodes.length < 1) {
          return parsedTrees;
        }
        this.rootNodeId = null;
        let hasDocumentNode = false;
        
        this.findTopMostParent();
        if (this.topParentIds.length === 1) {
          //console.log('Got the top parent ', this.topParentIds[0]);
        } else {
          console.log('2 NeedRATING_CONTROL_VALUE_ACCESSOR to find top parent ', this.topParentIds);
        }
        if (this.topParentIds && this.topParentIds.length < 1) {
          this.topParentIds.push(this.getNodes()[0].id);
        }
        if (this.topParentIds.length > 0) {
          for (let index = 0; index < this.topParentIds.length; index++) {
            // for (let index = 0; index < 1; index++) {
            const parentNodeId = this.topParentIds[index];
            let documentNode = {
              id: parentNodeId,
              children: [],
              full_statement: '',
              is_document: 0
            };
            // check if data has document id
            if (this.nodes && this.nodes.length > 0) {
              const node = this.nodes[0];
              //SOF For Export CSV 
              if (node.document_id) {
                node.is_document = 1;
                if (node.case_metadata && node.case_metadata[0]) {
                  node.title = node.case_metadata[0].title;
                }
              }
              //EOF Export CSV
              if ((node.is_document === 1) && (parentNodeId === node.id)) {
                documentNode = node;
                this.setDocumentNode(documentNode, node);
                hasDocumentNode = true;
              }
            }
            const parsedTree = documentNode;
    
            if (parsedTree.is_document === 1 || !this.isTopMostOrphan(documentNode.id)) {
              // If node is document or is not top most orphan, build tree structure
              parsedTree['children'] = this.createTreeStructure(documentNode.id, '', 1, documentNode.breadCrumb);
            } else {
              parsedTree.id = '';
            }
            parsedTree.isFirst = index === 0 ? true : false; // set is it first node or not
            if (parsedTree['children'] && parsedTree['children'].length > 0 && this.topParentIds.length > 1) {
              parsedTree['children'][0].isFirst = index === 0 ? true : false; // set is it first node or not
            }
            try {
    
              Utils.sortData(parsedTree['children']);
            } catch (error) {
              console.log('sorting error', error);
            }
            /* Orphan lable node appending under document node STARTS */
            // if (index === (this.topParentIds.length - 1)) {
            //   this.orphanNodeList = this.getOrphanNodes();
            //   if (this.orphanNodeList.length > 0) {
            //   this.buildOrphanLableNode(parsedTree);
            //   }
            // }
            /* Orphan lable node appending under document node ENDS */
    
            // if (this.topParentIds.length === 1 && hasDocumentNode) {
            if (hasDocumentNode) {
              parsedTree.is_document = 1;
              // parsedTree.is_editable = parsedTree.is_editable;
              const wrapperNode = {
                children: [parsedTree]
              };
              // if (!this.isTopMostOrphan(this.topParentIds[index])) {
              //   parsedTrees.push(wrapperNode);
              // }
              if (parsedTree.id === (this.topParentIds[index])) { // to prevent duplicate pushing
    
                /* Orphan lable node appending under document node STARTS */
                this.buildOrphanLableNode(wrapperNode.children[0]);
                /* Orphan lable node appending under document node ENDS */
                parsedTrees.push(wrapperNode);
              }
            } else {
              /*if (parsedTree.children && parsedTree.children.length === 0) {
                const dummyWapper = {
                  id: parentNodeId,
                  children: [],
                  full_statement: '',
                  is_document: 0
                };
                dummyWapper.children.push(parsedTree);
                parsedTrees.push(dummyWapper);
              } else {}*/
              // In case of no dument node i.e. partially selected
              if (index === (this.topParentIds.length - 1)) {
                // Appending orphan tree at last iteration
    
                /* Orphan lable node appending at last position of tree STARTS */
                this.buildOrphanLableNode(parsedTree);
                /* Orphan lable node appending at last position of tree ENDS */
              }
              if (parsedTree.children.length) {
                parsedTrees.push(parsedTree);
              }
              // if (parsedTrees.length < 1 && parsedTree && parsedTree.children.length) {
              //   parsedTrees.push(parsedTree);
              // }
            }
          }
        } else {
          // when there is no relation in relation array
          let documentNode;
          const treeNodes = this.getNodes();
          const node = treeNodes[0] ? treeNodes[0] : treeNodes;
          node.children = [];
          const wrapper = {
            children: []
          };
          if (node.is_document === 1) {
            documentNode = node;
            this.setDocumentNode(documentNode, node);
          }
          wrapper.children.push(documentNode);
          console.log(wrapper);
          return [wrapper];
    
        }
    
        // console.log('parsedTree*****************   ', parsedTrees);
        resolve(parsedTrees);
        }).catch(ex => {
            console.log('list of taxonomies ex ', ex);
            reject(ex);
    });
      }
      setDocumentNode(documentNode, node) {
        // tslint:disable-next-line:max-line-length
        documentNode.full_statement = node.title ? node.title : (node.full_statement ? node.full_statement : null);
        documentNode.full_statement_html = node.title_html ? node.title_html : node.title ? node.title : node.full_statement_html ? node.full_statement_html : node.full_statement ? node.full_statement : null;
        documentNode.level = 1;
        documentNode.expand = true;
        documentNode.cut = 0;
        documentNode.reorder = 0;
        documentNode.paste = 0;
        documentNode.breadCrumb = node.title;
        documentNode.breadCrumbArray = documentNode.breadCrumb.split('>');
        documentNode.list_enumeration = documentNode.sequence_number ? '' + documentNode.sequence_number : documentNode.list_enumeration;
        documentNode.sequence_number = documentNode.sequence_number ? '' + documentNode.sequence_number : documentNode.list_enumeration;
        documentNode.hasMoreChild = false;
        this.rootNodeId = node.id;
      }

      findTopMostParent() {

        this.parentObj = {};
        this.topParentIds = [];
        // console.log('Need to find this.parentObj ', this.relations);

        for (let index = 0; index < this.relations.length; index++) {
          const element = this.relations[index];
          this.findParent(element.parent_id, element.child_id);
        }
        // console.log('Need to find this.parentObj ', this.parentObj);
        const parentArray = [];
        for (const key in this.parentObj) {
          if (this.parentObj.hasOwnProperty(key)) {
            // const element = this.parentObj[key];
            parentArray.push(key);
          }
        }
        // If didn't find relation between relation array, it means all nodes are child of first node
        if (parentArray.length === 0) {
          if (this.relations.length > 0) {
            this.parentObj = this.relations[0];
            parentArray.push(this.relations[0].parent_id);
          }
        }
        if (parentArray.length === 1) {
          this.topParentIds.push(parentArray[0]);
          // console.log('Got the top parent ', parentArray[0]);
        } else {
          // console.log('1 Need to find top parent ', parentArray);
          parentArray.forEach(element => {
            let found = false;
            for (let index = 0; index < this.relations.length; index++) {
              const relation = this.relations[index];
              if (relation.child_id === element) {
                found = true;
              }
            }
            if (found === false) {
              this.topParentIds.push(element);
            }
          });
        }
      }
    
      findParent(parentId, child_id) {
        let foundTopParentId = false;
        let index = 0;
        for (let i = 0; i < this.relations.length; i++) {
          const n = this.relations[i];
          index++;
          if (n.child_id !== n.parent_id) {
            if (n.child_id === parentId) {
              this.parentObj[n.parent_id] = n.child_id;
              foundTopParentId = true;
              if (child_id !== n.parent_id) {
                this.findParent(n.parent_id, n.child_id);
              }
            } else {
              // If parentId is not a child of any node then it should be top node of that branch
              if (index === this.relations.length && foundTopParentId === false) {
                this.parentObj[parentId] = parentId;
              }
            }
          }
        }
      }

      isRelationDupli(relations, relation) {
        if (relations.length) {
          if (relations.find(rel => rel.child_id === relation.child_id)) {
            return true; // duplicate
          } else {
            return false; // not duplicate
          }
        } else {
          return false; // not duplicate
        }
      }

      createTreeStructure(nodeId, parent_id, level, previousBC) {
        const data = [];
        const childrenNodes = this.extractChildren(nodeId);
        if (childrenNodes.length > 0) {
          level = level + 1;
          for (const key in childrenNodes) {
            // console.log(key, '---------', childrenNodes[key]);
            if (childrenNodes[key]) {
              const childNode = childrenNodes[key];
              if (childNode) {
                nodeId = childNode.id;
                childNode.children = [];
                childNode.level = level;
                childNode.expand = level <= this.expandLevel ? true : false;
                childNode.full_statement_html = childNode.full_statement_html ? childNode.full_statement_html : childNode.full_statement;
                childNode.cut = 0;
                childNode.reorder = 0;
                childNode.paste = 0;
                childNode.hasMoreChild = true;
                // tslint:disable-next-line:max-line-length
                childNode.breadCrumb = previousBC + ' > ' + (childNode.human_coding_scheme && childNode.human_coding_scheme.length > 0 ? childNode.human_coding_scheme : childNode.full_statement);
                childNode.list_enumeration = childNode.sequence_number ? '' + childNode.sequence_number : childNode.list_enumeration;
                childNode.sequence_number = '' + childNode.relation_sequence_number;
                childNode.breadCrumbArray = childNode.breadCrumb.split('>');
                if (nodeId !== parent_id) {
                  const nodeChildren = this.createTreeStructure(nodeId, childNode.parent_id, level,
                    childNode.breadCrumb);
                  if (nodeChildren.length > 0 && (childNode.tree_association || childNode.tree_association === undefined)) {
                    childNode.children = nodeChildren;
                  }
                }
                data.push(childNode);
              }
            }
          }
        }
        return data;
      }
    
      extractChildren(nodeId) {
        const treeNodes = this.nodes;
        const nodeRelations = this.relations;
        const data = [];
        for (const key in nodeRelations) {
          if (nodeRelations[key]) {
            const relation = nodeRelations[key];
            if (relation.child_id !== relation.parent_id) {
              const parentId = relation.parent_id;
              //  console.log(' extractChildren  ', relation, '  ', parentId);
              if (nodeId === parentId) {
                const childId = relation.child_id;
                const childNode = this.extractNode(childId);
                childNode['relation_sequence_number'] = relation.sequence_number;
                if (childNode) {
                  childNode.parent_id = parentId;
                  // These properties used in Export CSV to show association and association type
                  if (nodeRelations[key].tree_association !== undefined) {
                    childNode.tree_association = nodeRelations[key].tree_association;
                    childNode.association_type = nodeRelations[key].association_type;
                  }
                  data.push(childNode);
                }
              }
            }
          }
        }
        return data;
      }
    
      extractNode(nodeIdToSearchWith) {
        const treeNodes = this.nodes;
        let nodeFound;
        for (const key in treeNodes) {
          if (treeNodes[key]) {
            const node = treeNodes[key];
            if (nodeIdToSearchWith === node.id) {
              nodeFound = Object.assign({}, node);
              break;
            }
          }
        }
        return nodeFound;
      }

      buildOrphanLableNode(parentNode) {
        let orphanFound = false;
        const orphanLableNode = {}; // 'orphanLableNode' is one dummy node
        const title = 'Nodes with no linked parent';
        orphanLableNode.full_statement = title;
        orphanLableNode.full_statement_html = title;
        orphanLableNode.human_coding_scheme = '';
        orphanLableNode.is_document = 0;
        // orphanLableNode.title = title;
        orphanLableNode.cut = 0;
        orphanLableNode.paste = 0;
        orphanLableNode.reorder = 0;
        orphanLableNode.children = [];
        orphanLableNode.sequence_number = '';
        orphanLableNode.isOrphan = true;
        orphanLableNode.isOrphanLabel = true;
        orphanLableNode.parent_id = parentNode ? parentNode.id : '';
        orphanLableNode.id = this.DUMMY_ID_FOR_ORPHANLABEL; // assigning dummy id
        orphanLableNode.list_enumeration = (parentNode && parentNode.children.length) ? ('' + (Utils.getMaxLE(parentNode.children))) : ('' + 1);
        orphanLableNode.sequence_number = (parentNode && parentNode.children.length) ? ('' + (Utils.getMaxLE(parentNode.children))) : ('' + 1);
        if (parentNode) {
          parentNode.level = (parentNode.level !== undefined) ? parentNode.level : 1;
        }
        orphanLableNode.level = parentNode ? (parentNode.level + 1) : 1;
        // orphanLableNode.expand = orphanLableNode.level <= this.expandLevel ? true : false;
        orphanLableNode.expand = true;
    
        // for (const orphanNode of this.orphanNodeList) {
        for (const node of this.topMostOrphanNodes) {
          // tslint:disable-next-line:max-line-length
          node.breadCrumb = 'Orphan node > ' + (node.human_coding_scheme && node.human_coding_scheme.length > 0 ? node.human_coding_scheme : node.full_statement);
          node.breadCrumbArray = node.breadCrumb.split('>');
          orphanFound = true;
          // const node = this.findNodeById(orphanNode.id);
          node.cut = 0;
          node.paste = 0;
          node.reorder = 0;
          node.parent_id = this.DUMMY_ID_FOR_ORPHANLABEL;
          node.level = orphanLableNode.level + 1;
          node.expand = node.level <= this.expandLevel ? true : false;
          node.children = this.createTreeStructure(node.id, node.parent_id, node.level, node.breadCrumb);
          orphanLableNode.children.push(node);
        }
       // console.log('Orphan node tree =========================>', orphanLableNode);
        if (parentNode && orphanFound) { // If any orphan tree nodes found, then append orphan tree in parent tree's children
          parentNode.children.push(orphanLableNode);
        }
        return orphanLableNode;
      }

      createCSVData(csvData, data) {

        data.forEach(element => {
          /* '' is used for treating string of numbers as string, otherwise string of numbers is saving
            as number in file*/
          const obj = {
            action: '',
            node_id: element.id ? ('' + element.id) : '',
            'parent/destination_node_id': element.parent_id ? ('' + element.parent_id) : '',
            case_association_type: ''
          };

          if(element.title != '' && element.title !== undefined)
          {
            this.taxonomyTitle = element.title;
            //console.log(this.taxonomyTitle)
          }

          this.caseMetadataList.forEach(item => { 
            if (element && element.case_metadata && element.case_metadata.length > 0) {
    
              const value = element.case_metadata[0][item.metadata_name];
              obj['case_' + item.metadata_name] = value ? ('' + value) : '';
              if (element.tree_association !== undefined) {
                if (element.tree_association === 0) {
                  obj['case_' + item.metadata_name] = '';
                  obj['case_association_type'] = element.association_type ? ('' + element.association_type) : '';
                }
              }
            }
          });
          this.customMetadata.forEach(metadata => {
            if (element && element.metadata_detail) {
              const metadataItem = element.metadata_detail.find(x => {
                if (metadata.name === x.name.toLowerCase()) {
                  return x;
                }
              });
              obj[metadata.name + '(' + Utils.getCustomMetadataType(metadata.field_type) + ')'] = metadataItem ? metadataItem.metadata_value : '';
            }
    
          });
          if ((obj['parent/destination_node_id']) || element.document_id) {
            csvData.push(obj);
          }
          // This code is not requried, now assocation data is coming in relation array
          // pushing associations for node if any
          if (element.tree_association === 1) {
            const associations = this.findAssoByNodeId(element);
            if (associations.length > 0) {
              for (const asso of associations) {
                const assoObj = {
                  action: '',
                  node_id: asso.child_id ? ('' + asso.child_id) : '',
                  'parent/destination_node_id': asso.parent_id ? ('' + asso.parent_id) : '',
                  case_association_type: asso.association_type ? ('' + asso.association_type) : ''
                };
                csvData.push(assoObj);
              }
            }
          }
    
          if (element.children && element.children.length > 0) {
            this.createCSVData(csvData, element.children);
          }
        });
    
      }

      findAssoByNodeId(node) {
        const result = [];
        for (const asso of this.associationList) {
          if (asso.child_id === node.id) {
            if (asso.association_type !== 'isChildOf' || asso.parent_id !== node.parent_id) {
              result.push(asso);
            }
          }
        }
        Utils.sortDataArray(result, 'sequence_number', false, true);
        return result;
      }
    
     exportCSVData(data, name) {
        let email = this.emailadd;
        let org_id = this.orgid;
        const options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          noDownload: false,
          headers: ['action', 'node_id', 'parent/destination_node_id', 'case_association_type']
        };
    
        if (name === 'sample_csv') {
          options.headers.push('case_title', 'case_full_statement', 'case_human_coding_scheme',
            'case_list_enumeration', 'case_node_type', 'case_education_level');
        }
        this.caseMetadataList.forEach(metadata => {
          options.headers.push('case_' + metadata.metadata_name);
        });
    
        this.customMetadata.forEach(item => {
          options.headers.push(item.name + '(' + Utils.getCustomMetadataType(item.field_type) + ')');
        });
    
        const csvExporter = new ExportLib.ExportToCsv(options);
        
       let keys = Object.keys(data[0]);
     //  console.log('KEYS =======>Trans ID = '+transid+" ======="+keys);
       let final_obj = [];
        for (var item of data){
          let val = Object.values(item);
          
          let i = 0;
          let obj = {};
          for (var head of keys) {
            obj[head] = (val[i] !== undefined && val[i] != '')  ? val[i] : '';
            i++;
          }
         
          final_obj.push(obj);
          
    
        }
      
        this.awsFileSave(name, csvExporter.generateCsv(final_obj, true), email, org_id)
    
      }
    


      sendEmail(status , email,  fileName, taxoName, org_id)
      {
       
        let taxonamearr = taxoName.split('.');
        let taxoNametxt = taxonamearr[0]
        let apiUrl = Utils.getApiUrl(process.env.NODEJSENV);
        if(status == true)
        {
          
         var objData =  querystring.stringify({
            'recepient': email,
            'subject': `The Export Process Of The Taxonomy ${taxoNametxt}  is completed` ,
            'body': "Dear User, <br/> <a href='" + apiUrl+ "/api/v1/downloadTaxonomy/" + fileName + "' >Click Here</a> to download " + fileName,
            'url' : apiUrl + "/api/v1/downloadTaxonomy/" + fileName,
            'taxonomy_name' : taxoNametxt,
            'org_id' : org_id
          });
    
    
        }
        else{
    
          var objData =  querystring.stringify({
            'recepient': email,
            'subject': `The Export Process Of The Taxonomy ${taxoNametxt}  is failed` ,
            'body': "Dear User, <br/> We couldnt process your request ",
            'url' : '',
            'taxonomy_name' : taxoNametxt,
            'org_id' : org_id
          });
    
    
         
        }
    
        var config = {
          headers: { "Accept": 'Content-Type : multipart/form-data;' },   
        };
        axios.post(apiUrl + "/api/v1/sendEmail", objData, config)
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    
      awsFileSave(fileName, fileData, email, org_id) {
      
        let fileFor = fileName.replace(/[^\w\s]/gi, '');
        fileFor += ".csv"; 
        const s3 = new AWS.S3(config.AWSGLOBAL);
        var params = { Bucket: config.AWSS3.AWS_S3_BUCKET , Key: fileFor, Body: fileData };
        let self = this;

        s3.upload(params, function (err, data) {
          if (err) {
            self.sendEmail(false , email, fileName, '', org_id);
            return err;
          } else {
            if(data.Key !== undefined && data.Key != '')
            {
              
              let awsfileName = encodeURI(data.Key);
              self.sendEmail(true , email, awsfileName, fileName, org_id);
           
            }
            else
            {
              self.sendEmail(false , email, fileName, '', org_id);
            }
          }
        });
    
        
      }
    
    
     
    
    
       isTopMostOrphan(nodeId, topMostOrphanNodes) {
        let flag = false;
        for (const node of topMostOrphanNodes) {
          if (node.id === nodeId) {
            flag = true;
            break;
          }
        }
        return flag;
      }
    
      getNodes(){
        return this.nodes;
      }
     
    

}

module.exports = ExportCsv;
