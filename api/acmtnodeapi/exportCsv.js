function getExportTaxonomyTreeData() {
  const url = GlobalSettings.CSV_EXPORT_TAXONOMY + '/' + this.selectedTaxonomyId;
  this.treeService1.getTreeData(url, Utils.EXPAND_LEVEL, true).then((response: any) => {
    const res = response;
    this.exportData = this.treeService1.getNodes();
    this.associationList = this.treeService1.externalRelations;
    this.exportData.forEach(csvdata => {
      const caseMetadata = csvdata.case_metadata[0];
      for (const metadata in caseMetadata) {
        if (this.caseMetadataList.findIndex(x => x.metadata_name === metadata.toLowerCase()) === -1) {
          this.caseMetadataList.push({
     metadata_name: metadata
          });
        }
      }
      csvdata.metadata_detail.forEach(item => {
        if (this.customMetadata.findIndex(x => x.name === item.name.toLowerCase()) === -1) {
          this.customMetadata.push({
            name: item.name.toLowerCase(),
            field_type: item.field_type
          });
        }
      });
    });
    const data = [];
    // Utils.sortData(res.children);
    this.createCSVData(data, res);
    this.exportCSV(data, this.taxonomyTitle);
  }).catch(ex => {
    console.log('Export CSV error ', ex);
    this.exportLoading = false;
  });
}


var exportCsv  = {
createCSVData : function(csvData, data) {

  data.forEach(element => {
   
    const obj = {
      action: '',
      node_id: element.id ? ('' + element.id) : '',
      'parent/destination_node_id': element.parent_id ? ('' + element.parent_id) : '',
      case_association_type: ''
    };
    caseMetadataList.forEach(item => {
      if (element && element.case_metadata && element.case_metadata.length > 0) {

        const value = element.case_metadata[0][item.metadata_name];
        obj['case_' + item.metadata_name] = value ? ('' + value) : '';
        if (element.tree_association !== undefined) {
          if (element.tree_association === 0) {
            obj['case_' + item.metadata_name] = '';
            obj['case_association_type'] = element.association_type ? ('' + element.association_type) : '';
          }
        }
      }
    });
    customMetadata.forEach(metadata => {
      if (element && element.metadata_detail) {
        const metadataItem = element.metadata_detail.find(x => {
          if (metadata.name === x.name.toLowerCase()) {
            return x;
          }
        });
        obj[metadata.name + '(' + Utils.getCustomMetadataType(metadata.field_type) + ')'] = metadataItem ? metadataItem.metadata_value : '';
      }

    });
    if ((obj['parent/destination_node_id']) || element.document_id) {
      csvData.push(obj);
    }
    // This code is not requried, now assocation data is coming in relation array
    // pushing associations for node if any
    if (element.tree_association === 1) {
      const associations = this.findAssoByNodeId(element);
      if (associations.length > 0) {
        for (const asso of associations) {
          const assoObj = {
            action: '',
            node_id: asso.child_id ? ('' + asso.child_id) : '',
            'parent/destination_node_id': asso.parent_id ? ('' + asso.parent_id) : '',
            case_association_type: asso.association_type ? ('' + asso.association_type) : ''
          };
          csvData.push(assoObj);
        }
      }
    }

    if (element.children && element.children.length > 0) {
      createCSVData(csvData, element.children);
    }
  });
} ,

exportCSVFile : function(data, name) {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: ['action', 'node_id', 'parent/destination_node_id', 'case_association_type']
    };

    if (name === 'sample_csv') {
      options.headers.push('case_title', 'case_full_statement', 'case_human_coding_scheme',
        'case_list_enumeration', 'case_node_type', 'case_education_level');
    }
    this.caseMetadataList.forEach(metadata => {
      options.headers.push('case_' + metadata.metadata_name);
    });

    this.customMetadata.forEach(item => {
      options.headers.push(item.name + '(' + Utils.getCustomMetadataType(item.field_type) + ')');
    });

    console.log('CSV headers', options.headers);
    new Angular5Csv(data, name, options);
    this.exportLoading = false;

  } ,

  setTreeResponse : function(data) {
    this.treeResponseData = data;
  }

};
module.exports.exportCsv = exportCsv;