const { Utils } = require('./class/Utils.js');
const config = require('config');
const ExportLib = require('export-to-csv');
const AWS = require('aws-sdk')
const querystring = require('querystring');
const axios = require('axios');


//AWS.config.update({region: 'us-east-1'});


  var DUMMY_ID_FOR_ORPHANLABEL = '';
  var customMetadata = [];
  //colHeaders = ['full_statement', 'human_coding_scheme']; // holds internal names for column headers
  var metadataList = []; // holds metadata list used for table view headers
  var caseMetadataList = [];
  var associationList = []; // holds associations list for csv export
  var exportLoading = false; // Loading state for taxonomy export
  var textLength = 50;
  var taxonomyTitle = '';

  var nodes = [];
  var relations = [];
  var treeResponseData = {};

  var parentObj = {};
  var topParentIds = [];
  var  expandLevel = 1;
  var rootNodeId = '';
  var orphanNodeList = [];
  var topMostOrphanNodes = []; // holds list of top most orphan nodes
  var externalRelations = [];
  var taxonomyTitle = '';
  var org_id = '';

 
  var recepient_email = '';

  function exportCsv(data, email, orgid)
  {
    treeResponseData = data;
    nodes = data.nodes;
    recepient_email = email;
    org_id = orgid;
    setRelations(true);
    findTopmostOrphan(nodes);

    parseTreeData().then((response) => {

      const res = response;
      exportData = nodes;
      associationList = externalRelations;
      
      exportData.forEach(csvdata => {
        const caseMetadata = csvdata.case_metadata[0];
        for (const metadata in caseMetadata) {
          if (caseMetadataList.findIndex(x => x.metadata_name === metadata.toLowerCase()) === -1) {
            caseMetadataList.push({
              metadata_name: metadata
            });
          }
        }
        csvdata.metadata_detail.forEach(item => {
          if (customMetadata.findIndex(x => x.name === item.name.toLowerCase()) === -1) {
            customMetadata.push({
              name: item.name.toLowerCase(),
              field_type: item.field_type
            });
          }
        });
      });
      const data = [];
      // Utils.sortData(res.children);
      createCSVData(data, res);
      exportCSVData(data, taxonomyTitle, recepient_email, org_id);
    }).catch(ex => {
      console.log('Export CSV error ', ex);
      exportLoading = false;
    });


  }

  

    

    


  function setRelations(isExport = false) {
    let apiResponse = treeResponseData;

    let relationsArr = [];

    apiResponse.relations.forEach(relation => {
      if (relation) {
        let assoCondition;
        if (!isExport) {
          assoCondition = relation.external_link === true || relation.external_association;
        } else {
          assoCondition = true;
        }
        if (assoCondition) {
          // external_association flag for export CSV external association
          // if (relation.external_association) {
          externalRelations.push(relation);
          // }
          // For external taxonomy, if relation does not exists in array, push it otherwise don't push for dulpicating
          if (!isRelationDupli(relationsArr, relation)) {
            relationsArr.push(relation);
          }
        } else {
          // If not external taxonomy, push it directly
          relationsArr.push(relation);
        }
      }
    });

    relations = relationsArr;

  }

  function findTopmostOrphan(nodeData) {
    const nodes = nodeData;

    for (const node of nodes) {
      if (node.is_orphan === 1) { // top most orphan node
        topMostOrphanNodes.push(node);
      }
    }
  }


 function  parseTreeData() {
    return new Promise((resolve, reject) => {
      const parsedTrees = [];
      if (nodes.length < 1) {
        return parsedTrees;
      }
      rootNodeId = null;
      let hasDocumentNode = false;

      findTopMostParent();
      if (topParentIds.length === 1) {
        //console.log('Got the top parent ', topParentIds[0]);
      } else {
        console.log('2 NeedRATING_CONTROL_VALUE_ACCESSOR to find top parent ', topParentIds);
      }
      if (topParentIds && topParentIds.length < 1) {
        topParentIds.push(getNodes()[0].id);
      }
      if (topParentIds.length > 0) {
        for (let index = 0; index < topParentIds.length; index++) {
          // for (let index = 0; index < 1; index++) {
          const parentNodeId = topParentIds[index];
          let documentNode = {
            id: parentNodeId,
            children: [],
            full_statement: '',
            is_document: 0
          };
          // check if data has document id
          if (nodes && nodes.length > 0) {
            const node = nodes[0];
            //SOF For Export CSV 
            if (node.document_id) {
              node.is_document = 1;
              if (node.case_metadata && node.case_metadata[0]) {
                node.title = node.case_metadata[0].title;
              }
            }
            //EOF Export CSV
            if ((node.is_document === 1) && (parentNodeId === node.id)) {
              documentNode = node;
              setDocumentNode(documentNode, node);
              hasDocumentNode = true;
            }
          }
          const parsedTree = documentNode;

          if (parsedTree.is_document === 1 || !isTopMostOrphan(documentNode.id)) {
            // If node is document or is not top most orphan, build tree structure
            parsedTree['children'] = createTreeStructure(documentNode.id, '', 1, documentNode.breadCrumb);
          } else {
            parsedTree.id = '';
          }
          parsedTree.isFirst = index === 0 ? true : false; // set is it first node or not
          if (parsedTree['children'] && parsedTree['children'].length > 0 && topParentIds.length > 1) {
            parsedTree['children'][0].isFirst = index === 0 ? true : false; // set is it first node or not
          }
          try {

            Utils.sortData(parsedTree['children']);
          } catch (error) {
            console.log('sorting error', error);
          }
          /* Orphan lable node appending under document node STARTS */
          // if (index === (topParentIds.length - 1)) {
          //   orphanNodeList = getOrphanNodes();
          //   if (orphanNodeList.length > 0) {
          //   buildOrphanLableNode(parsedTree);
          //   }
          // }
          /* Orphan lable node appending under document node ENDS */

          // if (topParentIds.length === 1 && hasDocumentNode) {
          if (hasDocumentNode) {
            parsedTree.is_document = 1;
            // parsedTree.is_editable = parsedTree.is_editable;
            const wrapperNode = {
              children: [parsedTree]
            };
            // if (!isTopMostOrphan(topParentIds[index])) {
            //   parsedTrees.push(wrapperNode);
            // }
            if (parsedTree.id === (topParentIds[index])) { // to prevent duplicate pushing

              /* Orphan lable node appending under document node STARTS */
              buildOrphanLableNode(wrapperNode.children[0]);
              /* Orphan lable node appending under document node ENDS */
              parsedTrees.push(wrapperNode);
            }
          } else {
            /*if (parsedTree.children && parsedTree.children.length === 0) {
              const dummyWapper = {
                id: parentNodeId,
                children: [],
                full_statement: '',
                is_document: 0
              };
              dummyWapper.children.push(parsedTree);
              parsedTrees.push(dummyWapper);
            } else {}*/
            // In case of no dument node i.e. partially selected
            if (index === (topParentIds.length - 1)) {
              // Appending orphan tree at last iteration

              /* Orphan lable node appending at last position of tree STARTS */
              buildOrphanLableNode(parsedTree);
              /* Orphan lable node appending at last position of tree ENDS */
            }
            if (parsedTree.children.length) {
              parsedTrees.push(parsedTree);
            }
            // if (parsedTrees.length < 1 && parsedTree && parsedTree.children.length) {
            //   parsedTrees.push(parsedTree);
            // }
          }
        }
      } else {
        // when there is no relation in relation array
        let documentNode;
        const treeNodes = getNodes();
        const node = treeNodes[0] ? treeNodes[0] : treeNodes;
        node.children = [];
        const wrapper = {
          children: []
        };
        if (node.is_document === 1) {
          documentNode = node;
          setDocumentNode(documentNode, node);
        }
        wrapper.children.push(documentNode);
        console.log(wrapper);
        return [wrapper];

      }

      // console.log('parsedTree*****************   ', parsedTrees);
      resolve(parsedTrees);
    }).catch(ex => {
      console.log('list of taxonomies ex ', ex);
      reject(ex);
    });
  }
  function setDocumentNode(documentNode, node) {
    // tslint:disable-next-line:max-line-length
    documentNode.full_statement = node.title ? node.title : (node.full_statement ? node.full_statement : null);
    documentNode.full_statement_html = node.title_html ? node.title_html : node.title ? node.title : node.full_statement_html ? node.full_statement_html : node.full_statement ? node.full_statement : null;
    documentNode.level = 1;
    documentNode.expand = true;
    documentNode.cut = 0;
    documentNode.reorder = 0;
    documentNode.paste = 0;
    documentNode.breadCrumb = node.title;
    documentNode.breadCrumbArray = documentNode.breadCrumb.split('>');
    documentNode.list_enumeration = documentNode.sequence_number ? '' + documentNode.sequence_number : documentNode.list_enumeration;
    documentNode.sequence_number = documentNode.sequence_number ? '' + documentNode.sequence_number : documentNode.list_enumeration;
    documentNode.hasMoreChild = false;
    rootNodeId = node.id;
  }

  function findTopMostParent() {

    parentObj = {};
    topParentIds = [];
    // console.log('Need to find parentObj ', relations);

    for (let index = 0; index < relations.length; index++) {
      const element = relations[index];
      findParent(element.parent_id, element.child_id);
    }
    // console.log('Need to find parentObj ', parentObj);
    const parentArray = [];
    for (const key in parentObj) {
      if (parentObj.hasOwnProperty(key)) {
        // const element = parentObj[key];
        parentArray.push(key);
      }
    }
    // If didn't find relation between relation array, it means all nodes are child of first node
    if (parentArray.length === 0) {
      if (relations.length > 0) {
        parentObj = relations[0];
        parentArray.push(relations[0].parent_id);
      }
    }
    if (parentArray.length === 1) {
      topParentIds.push(parentArray[0]);
      // console.log('Got the top parent ', parentArray[0]);
    } else {
      // console.log('1 Need to find top parent ', parentArray);
      parentArray.forEach(element => {
        let found = false;
        for (let index = 0; index < relations.length; index++) {
          const relation = relations[index];
          if (relation.child_id === element) {
            found = true;
          }
        }
        if (found === false) {
          topParentIds.push(element);
        }
      });
    }
  }

  function findParent(parentId, child_id) {
    let foundTopParentId = false;
    let index = 0;
    for (let i = 0; i < relations.length; i++) {
      const n = relations[i];
      index++;
      if (n.child_id !== n.parent_id) {
        if (n.child_id === parentId) {
          parentObj[n.parent_id] = n.child_id;
          foundTopParentId = true;
          if (child_id !== n.parent_id) {
            findParent(n.parent_id, n.child_id);
          }
        } else {
          // If parentId is not a child of any node then it should be top node of that branch
          if (index === relations.length && foundTopParentId === false) {
            parentObj[parentId] = parentId;
          }
        }
      }
    }
  }

  function isRelationDupli(relations, relation) {
    if (relations.length) {
      if (relations.find(rel => rel.child_id === relation.child_id)) {
        return true; // duplicate
      } else {
        return false; // not duplicate
      }
    } else {
      return false; // not duplicate
    }
  }

 function createTreeStructure(nodeId, parent_id, level, previousBC) {
    const data = [];
    const childrenNodes = extractChildren(nodeId);
    if (childrenNodes.length > 0) {
      level = level + 1;
      for (const key in childrenNodes) {
        // console.log(key, '---------', childrenNodes[key]);
        if (childrenNodes[key]) {
          const childNode = childrenNodes[key];
          if (childNode) {
            nodeId = childNode.id;
            childNode.children = [];
            childNode.level = level;
            childNode.expand = level <= expandLevel ? true : false;
            childNode.full_statement_html = childNode.full_statement_html ? childNode.full_statement_html : childNode.full_statement;
            childNode.cut = 0;
            childNode.reorder = 0;
            childNode.paste = 0;
            childNode.hasMoreChild = true;
            // tslint:disable-next-line:max-line-length
            childNode.breadCrumb = previousBC + ' > ' + (childNode.human_coding_scheme && childNode.human_coding_scheme.length > 0 ? childNode.human_coding_scheme : childNode.full_statement);
            childNode.list_enumeration = childNode.sequence_number ? '' + childNode.sequence_number : childNode.list_enumeration;
            childNode.sequence_number = '' + childNode.relation_sequence_number;
            childNode.breadCrumbArray = childNode.breadCrumb.split('>');
            if (nodeId !== parent_id) {
              const nodeChildren = createTreeStructure(nodeId, childNode.parent_id, level,
                childNode.breadCrumb);
              if (nodeChildren.length > 0 && (childNode.tree_association || childNode.tree_association === undefined)) {
                childNode.children = nodeChildren;
              }
            }
            data.push(childNode);
          }
        }
      }
    }
    return data;
  }

  function extractChildren(nodeId) {
    const treeNodes = nodes;
    const nodeRelations = relations;
    const data = [];
    for (const key in nodeRelations) {
      if (nodeRelations[key]) {
        const relation = nodeRelations[key];
        if (relation.child_id !== relation.parent_id) {
          const parentId = relation.parent_id;
          //  console.log(' extractChildren  ', relation, '  ', parentId);
          if (nodeId === parentId) {
            const childId = relation.child_id;
            const childNode = extractNode(childId);
            childNode['relation_sequence_number'] = relation.sequence_number;
            if (childNode) {
              childNode.parent_id = parentId;
              // These properties used in Export CSV to show association and association type
              if (nodeRelations[key].tree_association !== undefined) {
                childNode.tree_association = nodeRelations[key].tree_association;
                childNode.association_type = nodeRelations[key].association_type;
              }
              data.push(childNode);
            }
          }
        }
      }
    }
    return data;
  }

  function extractNode(nodeIdToSearchWith) {
    const treeNodes = nodes;
    let nodeFound;
    for (const key in treeNodes) {
      if (treeNodes[key]) {
        const node = treeNodes[key];
        if (nodeIdToSearchWith === node.id) {
          nodeFound = Object.assign({}, node);
          break;
        }
      }
    }
    return nodeFound;
  }

  function buildOrphanLableNode(parentNode) {
    let orphanFound = false;
    const orphanLableNode = {}; // 'orphanLableNode' is one dummy node
    const title = 'Nodes with no linked parent';
    orphanLableNode.full_statement = title;
    orphanLableNode.full_statement_html = title;
    orphanLableNode.human_coding_scheme = '';
    orphanLableNode.is_document = 0;
    // orphanLableNode.title = title;
    orphanLableNode.cut = 0;
    orphanLableNode.paste = 0;
    orphanLableNode.reorder = 0;
    orphanLableNode.children = [];
    orphanLableNode.sequence_number = '';
    orphanLableNode.isOrphan = true;
    orphanLableNode.isOrphanLabel = true;
    orphanLableNode.parent_id = parentNode ? parentNode.id : '';
    orphanLableNode.id = DUMMY_ID_FOR_ORPHANLABEL; // assigning dummy id
    orphanLableNode.list_enumeration = (parentNode && parentNode.children.length) ? ('' + (Utils.getMaxLE(parentNode.children))) : ('' + 1);
    orphanLableNode.sequence_number = (parentNode && parentNode.children.length) ? ('' + (Utils.getMaxLE(parentNode.children))) : ('' + 1);
    if (parentNode) {
      parentNode.level = (parentNode.level !== undefined) ? parentNode.level : 1;
    }
    orphanLableNode.level = parentNode ? (parentNode.level + 1) : 1;
    // orphanLableNode.expand = orphanLableNode.level <= expandLevel ? true : false;
    orphanLableNode.expand = true;

    // for (const orphanNode of orphanNodeList) {
    for (const node of topMostOrphanNodes) {
      // tslint:disable-next-line:max-line-length
      node.breadCrumb = 'Orphan node > ' + (node.human_coding_scheme && node.human_coding_scheme.length > 0 ? node.human_coding_scheme : node.full_statement);
      node.breadCrumbArray = node.breadCrumb.split('>');
      orphanFound = true;
      // const node = findNodeById(orphanNode.id);
      node.cut = 0;
      node.paste = 0;
      node.reorder = 0;
      node.parent_id = DUMMY_ID_FOR_ORPHANLABEL;
      node.level = orphanLableNode.level + 1;
      node.expand = node.level <= expandLevel ? true : false;
      node.children = createTreeStructure(node.id, node.parent_id, node.level, node.breadCrumb);
      orphanLableNode.children.push(node);
    }
    // console.log('Orphan node tree =========================>', orphanLableNode);
    if (parentNode && orphanFound) { // If any orphan tree nodes found, then append orphan tree in parent tree's children
      parentNode.children.push(orphanLableNode);
    }
    return orphanLableNode;
  }

  function createCSVData(csvData, data) {
    
    data.forEach(element => {
      /* '' is used for treating string of numbers as string, otherwise string of numbers is saving
        as number in file*/
      const obj = {
        action: '',
        node_id: element.id ? ('' + element.id) : '',
        'parent/destination_node_id': element.parent_id ? ('' + element.parent_id) : '',
        case_association_type: ''
      };

      if (element.title != '' && element.title !== undefined) {
        taxonomyTitle = element.title;
        //console.log(taxonomyTitle)
      }

      caseMetadataList.forEach(item => {
        if (element && element.case_metadata && element.case_metadata.length > 0) {

          const value = element.case_metadata[0][item.metadata_name];
          obj['case_' + item.metadata_name] = value ? ('' + value) : '';
          if (element.tree_association !== undefined) {
            if (element.tree_association === 0) {
              obj['case_' + item.metadata_name] = '';
              obj['case_association_type'] = element.association_type ? ('' + element.association_type) : '';
            }
          }
        }
        
        
      });
      
      customMetadata.forEach(metadata => { 
        if (element && element.metadata_detail) {
          const metadataItem = element.metadata_detail.find(x => {
            if (metadata.name === x.name.toLowerCase()) {
              return x;
            }
          });
          obj[metadata.name + '(' + Utils.getCustomMetadataType(metadata.field_type) + ')'] = metadataItem ? metadataItem.metadata_value : '';
        }

      });
      
      if ((obj['parent/destination_node_id']) || element.document_id) {
        csvData.push(obj);
      }
      // This code is not requried, now assocation data is coming in relation array
      // pushing associations for node if any
      if (element.tree_association === 1) {
        const associations = findAssoByNodeId(element);
        if (associations.length > 0) {
          for (const asso of associations) {
            const assoObj = {
              action: '',
              node_id: asso.child_id ? ('' + asso.child_id) : '',
              'parent/destination_node_id': asso.parent_id ? ('' + asso.parent_id) : '',
              case_association_type: asso.association_type ? ('' + asso.association_type) : ''
            };
            csvData.push(assoObj);
          }
        }
      }
      
      if (element.children && element.children.length > 0) {
        createCSVData(csvData, element.children);
      }
    });

  }

 function findAssoByNodeId(node) {
    const result = [];
    for (const asso of associationList) {
      if (asso.child_id === node.id) {
        if (asso.association_type !== 'isChildOf' || asso.parent_id !== node.parent_id) {
          result.push(asso);
        }
      }
    }
    Utils.sortDataArray(result, 'sequence_number', false, true);
    return result;
  }

  function exportCSVData(data, name, email, org_id) {

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: ['action', 'node_id', 'parent/destination_node_id', 'case_association_type']
    };

    if (name === 'sample_csv') {
      options.headers.push('case_title', 'case_full_statement', 'case_human_coding_scheme',
        'case_list_enumeration', 'case_node_type', 'case_education_level');
    }
    caseMetadataList.forEach(metadata => {
      options.headers.push('case_' + metadata.metadata_name);
    });

    customMetadata.forEach(item => {
      options.headers.push(item.name + '(' + Utils.getCustomMetadataType(item.field_type) + ')');
    });

    const csvExporter = new ExportLib.ExportToCsv(options);
   
   let keys = Object.keys(data[0]);
   let final_obj = [];
    for (var item of data){
      let val = Object.values(item);
      
      let i = 0;
      let obj = {};
      for (var head of keys) {
        obj[head] = (val[i] !== undefined && val[i] != '')  ? val[i] : '';
        i++;
      }
     
      final_obj.push(obj);
      

    }
  
    awsFileSave(name + '.csv', csvExporter.generateCsv(final_obj, true), email, org_id)

  }


  function awsFileSave(fileName, fileData, email, org_id) {
   
    const s3 = new AWS.S3(config.get('AWSGLOBAL'));
    var params = { Bucket: config.get('AWSS3.AWS_S3_BUCKET'), Key: fileName, Body: fileData };

    s3.upload(params, function (err, data) {
      if (err) {
        sendEmail(false , email, fileName, '', org_id);
        return err;
      } else {
        if(data.key !== undefined && data.key != '')
        {
          let taxoName = data.key.split(".");
          let fileName = encodeURI(data.key);
            sendEmail(true , email, fileName, taxoName, org_id);
       
        }
        else
        {
          sendEmail(false , email, fileName, '', org_id);
        }
      }
    });

    
  }


  function sendEmail(status , email,  fileName, taxoName, org_id)
  {
    let apiUrl = Utils.getApiUrl('qa');
    if(status == true)
    {
      
     var objData =  querystring.stringify({
        'recepient': email,
        'subject': `The Export Process Of The Taxonomy ${taxoName[0]}  is completed` ,
        'body': "Dear User, <br/> <a href='" + apiUrl+ "/api/v1/downloadTaxonomy/" + fileName + "' >Click Here</a> to download " + taxoName[0],
        'url' : apiUrl + "/api/v1/downloadTaxonomy/" + fileName,
        'taxonomy_name' : taxoName[0],
        'org_id' : org_id
      });


    }
    else{

      var objData =  querystring.stringify({
        'recepient': email,
        'subject': `The Export Process Of The Taxonomy ${taxoName[0]}  is failed` ,
        'body': "Dear User, <br/> We couldnt process your request ",
        'url' : '',
        'taxonomy_name' : taxoName[0],
        'org_id' : org_id
      });


     
    }
console.log(objData);
    var config = {
      headers: { "Accept": 'Content-Type : multipart/form-data;' },   
    };
    axios.post(apiUrl + "/api/v1/sendEmail", objData, config)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }




module.exports = exportCsv;