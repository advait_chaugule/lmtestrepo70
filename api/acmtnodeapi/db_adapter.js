let mysql = require('mysql');
const config = require('./config/default_' + process.env.NODEJSENV + '.json');
let connection = mysql.createConnection({
    host: config.DBCONFIG.host,
    user: config.DBCONFIG.user,
    password: config.DBCONFIG.password,
    database: config.DBCONFIG.database,
    charset: 'utf8mb4'
});

module.exports.createConnection = function () {
    try {
        connection.connect();
        return connection;
    } catch (error) {
        console.log('error ', error);
        return null;
    }
}

module.exports.closeConnection = function () {
    try {
        if (connection) {
            connection.end();
        }
    } catch (error) {

    }
}

module.exports.getConnection = function(){
    return connection;
}