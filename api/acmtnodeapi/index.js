const express = require('express');
const dotenv = require('dotenv');
dotenv.config();
var bodyParser = require('body-parser');
const db = require('./db_adapter');
const connection = db.createConnection();
const ExportCsv = require('./class/ExportCsv');
const importCSV = require('./import_csv');
const compare = require('./comparison/jsonComparison');
const app = express();
const port = 3000;
const fs = require('fs');
const config = require('./config/default_' + process.env.NODEJSENV + '.json');
app.use(bodyParser.json({
    inflate: true,
    limit: '500MB'
}));


app.post('/exportcsv', function (req, res) {


    let file_loc = req.body.file_loc;

    let fileCon = JSON.parse(fs.readFileSync(file_loc, 'utf8'));

    let data = fileCon.data;
    let email = fileCon.email.email;
    let orgid = fileCon.orgid;




    if (data) {
        new ExportCsv(data, email, orgid);
        res.json({
            status: '1',
            msg: 'Taxonomy Processed'
        });
    } else {


        res.json({
            status: '0',
            msg: 'Data Not Found'
        });
    }

});

/**
 * Request parameter format
 * {
  "orgid": "a83db6c0-1a5e-428e-8384-c8d58d2a83ff",
  "csv_path": "IMPORT/CSV/44340d4a-7d06-4ff1-9508-e5316ead8556.csv",
  "batch_id": "b6def81a-29ad-4144-a511-5ee50223e5hb",
  "import_type": "1",
  "event_data": "{\"test\":\"data\"}"
} 
 */
app.post('/importcsv', function (req, res) {

    try {
        const data = req.body;
        const tableName = 'acmt_import_' + data.batch_id.replace(/-/g, '_');
        const params = {
            Bucket: config.AWSS3.AWS_S3_BUCKET,
            Key: data.csv_path
        };
        if (data.event_data) {
            data.event_data = JSON.parse(data.event_data);
            data.event_data.start = ((new Date()).getTime());
        }
        importCSV.importCSV(params, data.orgid, tableName, data.event_data, data.import_type).then((result) => {
            if (result) {
                const finish_time = ((new Date()).getTime());
                response = {
                    'success': true,
                    'message': `Table created sucessfully! ${tableName}`,
                    'event_data': JSON.stringify(result.event_data),
                    'table_name': result.tableName,
                    'records': result.records,
                    'start_time': result.event_data.start,
                    'finish_time': finish_time,
                    'total_time': finish_time - result.event_data.start + ' mili'
                };
                console.log('response ', response);
            }
            res.json(response);

        }).catch((error) => {

            console.log(`readDataFromCSV Final  ----- `, error);
            if (error) {
                response = {
                    'success': false,
                    'message': 'Someting went wrong!',
                    'data': error,
                    'error_code': 4040

                };
            }
            res.json(response);
        });
    } catch (error) {
        response = {
            'success': false,
            'message': 'Someting went wrong!',
            'data': error,
            'error_code': 4040

        };
        res.json(response);
    }

});

/**
 * Request parameter format
 * {"comparison_id": "a83db6c0-1a5e-428e-8384-c8d58d2a83ff"} 
 */
app.post('/compare', function (req, res) {

    try {
        const data = req.body;
        console.log(data);
        compare.compare(data.comparison_id).then((result) => {
            if (result) {
                const finish_time = ((new Date()).getTime());
                response = {
                    'success': true,
                    'message': result.msg,
                };
                console.log('response ', response);
            }
            res.json(response);

        }).catch((error) => {

            console.log(`compare error  ----- `, error);
            if (error) {
                response = {
                    'success': false,
                    'message': 'Someting went wrong!',
                    'data': error,
                    'error_code': 4040

                };
            }
            res.json(response);
        });
    } catch (error) {
        console.log(`compare error  ----- `, error);
        response = {
            'success': false,
            'message': 'Someting went wrong!',
            'data': error,
            'error_code': 4040

        };
        res.json(response);
    }

});

app.listen(port, () => console.log(`${port}!`));