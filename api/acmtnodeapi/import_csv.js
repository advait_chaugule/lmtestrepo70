const csv = require('csv-parser')
const fs = require('fs')
//const diff = require("fast-array-diff");
let arrayCompare = require("array-compare")
const db = require('./db_adapter');
const config = require('./config/default_'+process.env.NODEJSENV+'.json');
const AWS = require('aws-sdk');
const {
  uuid
} = require('uuidv4');
//let dbItems = [];
const connection = db.getConnection();
const batchSize = 1000;
AWS.config.update(config.AWSGLOBAL);
const S3 = new AWS.S3();
//readFileFromS3(params);
//readDataFromCSV(`./csv/${path}`);


/**
 * Read CSV file from give path
 * @param {string} path 
 */
function readDataFromCSV(params, orgid, tableName, eventData, import_type) {
  return new Promise((resolve, reject) => {
    try {

      const csvItems = []; //Hold only item_id
      const csvData = []; // Hold whole csv data
      let mili = (new Date()).getTime();
      console.log(`Start Reading CSV from path ${params.Key} at time ${mili} for table ${tableName}`);
      //const path = './csv/English Language Arts - Georgia Standards of Excellence.csv'
      //fs.createReadStream(path)
      S3.getObject(params).createReadStream()
        .on('error', error => {
          console.log("Error ********** ", error);
          reject({
            'function': 'readDataFromCSV',
            'error': error
          });
        })
        .pipe(csv())
        .on('data', (data) => {
          if (data && data.node_id) {
            csvItems.push(data.node_id.trim());
            csvData.push(data);
          }
        })
        .on('end', () => {
          console.log(`End Event CSV reading time ${((new Date()).getTime()) - mili} & itme count ${csvItems.length}`);
          if (csvData && csvData.length > 0 && csvData[0].node_id) {
            console.log('csvData ', csvData[0].node_id);
            checkDocumentExitOrNot(csvData[0].node_id, orgid, tableName, eventData, import_type)
              .then(function (documentId) {
                return getNodesForDocuments(documentId, orgid, tableName, eventData, import_type)
              })
              .then(function (dbItems) {
                return compareCSVItemWithDBItems(dbItems, csvItems, tableName, eventData, import_type)
              })
              .then(function (newItems) {
                return createUniqueId(newItems, csvItems, csvData, tableName, eventData, import_type)
              })
              .then(result => {
                if (result) {
                  let interval = null;
                  const resultArr = chunkArray(result, batchSize);
                  createTable(tableName, csvData[0]).then(function (columnNames) {
                    let count = 0;
                    for (let i = 0; i < resultArr.length; i++) {
                      count = count + resultArr[i].length;
                      insertDataInTempTable(tableName, columnNames, resultArr[i], eventData).then((result) => {
                        resolve({
                          tableName: tableName,
                          event_data: eventData,
                          result: result,
                          records: count
                        });
                      }).catch((error) => {
                        reject({
                          'function': 'readDataFromCSV',
                          'error': error
                        });
                      });
                      interval = setInterval(() => {}, 1000);
                    }
                  }).catch((error) => {
                    reject({
                      'function': 'readDataFromCSV',
                      'error': error
                    });
                  });
                  clearInterval(interval);
                }
              }).catch(error => {
                reject({
                  'function': 'readDataFromCSV',
                  'error': error
                });
              });
          } else {
            reject({
              'function': 'readDataFromCSV',
              'error': 'readDataFromCSV There is no data in csv'
            });
          }
        })
        .on('error', error => {
          console.log("Error ********** ", error);
          reject({
            'function': 'readDataFromCSV',
            'error': error
          });
        });
    } catch (error) {
      reject({
        'function': 'readDataFromCSV',
        'error': error
      });
    }
  });
}

function checkDocumentExitOrNot(source_document_id, orgId, csvItems, tableName, eventData, import_type) {

  return new Promise((resovle, reject) => {
    let documentId = null;
    let query = `SELECT source_document_id from acmt_documents where source_document_id = '${source_document_id}' and organization_id = '${orgId}'`;
    connection.query(query, function (error, results, fields) {
      if (error) {
        reject({
          'function': 'checkDocumentExitOrNot',
          'error': error
        });
      }
      if (results && results.length > 0) {
        console.log('The solution is: ', results[0].source_document_id);
        if (results[0].source_document_id) {
          documentId = results[0].source_document_id;
          resovle(documentId);
        }
      } else {
        resovle(documentId);
      }
    });

  });

}

function getNodesForDocuments(documentId, orgId, tableName, eventData, import_type) {
  return new Promise((resovle, reject) => {
    //if documentId null then resolve with blank array
    if (documentId === null) {
      resovle([]);
      return;
    }
    let dbItems = [];
    let query = `SELECT source_item_id from acmt_items where document_id = '${documentId}' and organization_id = '${orgId}'`;
    console.log('query ', query);
    connection.query(query, function (error, results, fields) {
      if (error) {
        reject({
          'function': 'getNodesForDocuments',
          'error': error
        });
      }
      if (results && results.length > 0) {
        dbItems.push(documentId.trim());
        results.forEach(element => {
          dbItems.push(element.source_item_id);
        });
        resovle(dbItems);
      } else {
        resovle([]);
      }
    });
  });
}

function compareCSVItemWithDBItems(dbItems, csvItems, tableName, eventData, import_type) {

  if (csvItems && csvItems.length > 0 && dbItems && dbItems.length > 0) {
    console.log('compareCSVItemWithDBItems clled', csvItems[0], dbItems[0]);
  }
  return new Promise((resolve, reject) => {
    try {
      //if dbItems blank then return blank array
      if (dbItems && dbItems.length === 0) {
        resolve([]);
        return;
      }
      let uniqueCsvItems = getUniqueIdArray(csvItems);
      let uniqueDbItems = getUniqueIdArray(dbItems);
      let result = arrayCompare(uniqueDbItems, uniqueCsvItems);
      let added = [];
      result.added.forEach(element => {
        added.push(element.b);
      });
      resolve(added);
    } catch (error) {
      reject({
        'function': 'compareCSVItemWithDBItems',
        'error': error
      });
    }
  });
}

/**
 *  For New File First find unique node_id and store in array then create new ids for them
 *  For Edit File 
 * if isExist false means its new file so create unique id for each row
 * if isExist true means its edited file so create unique id only for newly added row
 */
function createUniqueId(newItems, csvItems, csvData, tableName, eventData, import_type) {

  return new Promise((resovle, reject) => {
    try {

      const values = [];
      let uniqueCsvItems = getUniqueIdArray(csvItems);

      // 1 fress data,2 copy data so both case generate new id 
      /*if (import_type == 1 || import_type == 2) {

      }*/
      // 4 update taxonomy
      if (import_type == 4) {
        uniqueCsvItems = [];
        uniqueCsvItems = getUniqueIdArray(newItems);
      }
      //console.log('importtype ',import_type, newItems.length,uniqueCsvItems.length );
      uniqueCsvItems.forEach(uniqueId => {
        const newUniqid = uuid();
        csvData.forEach(obj => {
          //If uniqued
          if (uniqueId === obj.node_id) {
            obj.node_id = newUniqid;
          }
          if (uniqueId === obj['parent/destination_node_id']) {
            obj['parent/destination_node_id'] = newUniqid; // ⇨ '2c5ea4c0-4067-11e9-8b2d-1b9d6bcdbbfd'
          }
        });
      });
      csvData.forEach(obj => {

        let arr = [];
        // key is column in csv
        for (let key in csvData[0]) {
          if (key) {
            let data = obj[key];
            if (data) {
              data = connection.escape(data);
            } else {
              data = `""`;
            }
            arr.push(data);
          }
        }
        values.push(`(${arr.join()})`);
        arr = null;

      });
      resovle(values);
    } catch (error) {
      reject({
        'function': 'createUniqueId',
        'error': error
      });
    }
  });


}

function getUniqueIdArray(iteamIdArray) {

  /* mili = (new Date()).getTime();
   console.log(`start unique `, mili);
   const unique = [...new Set(csvItems)];
   console.log(`TimeTaken unique`, ((new Date()).getTime()) - mili);

   console.log(`unique array `, unique.length);*/
  // This is faster way, as compare to above one
  mili = (new Date()).getTime();
  console.log(`Start getUniqueIdArray ${mili}`);
  iteamIdArray = Array.from(new Set(iteamIdArray));
  console.log(`END TimeTaken for find unique value ${((new Date()).getTime()) - mili} & count ${iteamIdArray.length}`);
  return iteamIdArray;
}

function createTable(tableName, columnNameData) {

  return new Promise((resolve, reject) => {
    try {
      let columnNames = [];
      for (let key in columnNameData) {
        if (key) {
          columnNames.push(`\`${key}\``);
        }
      }
      let column = columnNames.join(' text,');
      column = column + ' text';
      column = column.replace('\\', '');
      let createTable = 'CREATE TABLE IF NOT EXISTS ' + tableName + ' (' + column + ')';
      connection.query(createTable, function (error, results, fields) {
        if (error) {
          console.log('error ', error);
          reject({
            'function': 'createTable',
            'error': error
          });
        }
        // console.log('sucess ', results);
        resolve(columnNames);
      });

    } catch (error) {
      reject({
        'function': 'createTable',
        'error': error
      });
    }

  });
}

function insertDataInTempTable(tableName, columnNames, resultArr, eventData) {

  return new Promise((resolve, reject) => {
    try {
      let insertQuery = `INSERT INTO ${tableName} (${columnNames.join()}) VALUES ${resultArr.join()}`;
      // console.log('insertQuery ',insertQuery);
      connection.query(insertQuery, function (error, results, fields) {
        if (error) {
          reject({
            'function': 'insertDataInTempTable',
            'error': error
          });
        }
        resolve(results);
      });
    } catch (error) {
      reject({
        'function': 'insertDataInTempTable',
        'error': error
      });
    }
  });
}

function chunkArray(array, size) {
  if (array.length <= size) {
    return [array]
  }
  return [array.slice(0, size), ...chunkArray(array.slice(size), size)]
}

exports.importCSV = readDataFromCSV;