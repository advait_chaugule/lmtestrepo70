<?php
namespace App\Data\Repositories;

use App\Data\Repositories\EloquentAuditRepository;

use App\Data\Repositories\Contracts\DimProjectRepositoryInterface;

use App\Data\Models\Report\DimProject;

class DimProjectEloquentRepository extends EloquentAuditRepository implements DimProjectRepositoryInterface
{
    public function getModel()
    {
        return DimProject::class;
    }
}