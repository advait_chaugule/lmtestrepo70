<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\LicenseRepositoryInterface;

use App\Data\Models\License;

class LicenseEloquentRepository extends EloquentRepository implements LicenseRepositoryInterface
{

    public function getModel()
    {
        return License::class;
    }
}