<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\StageRepositoryInterface;

use App\Data\Models\Stage;

class StageEloquentRepository extends EloquentRepository implements StageRepositoryInterface
{

    public function getModel()
    {
        return Stage::class;
    }

   
}