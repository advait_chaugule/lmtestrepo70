<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Data\Models\Asset;

use Illuminate\Support\Facades\DB;

class AssetEloquentRepository extends EloquentRepository implements AssetRepositoryInterface
{
    public function getModel()
    {
        return Asset::class;
    }

    public function getAssetId(string $assetId){
        $assetDetail = DB::table('assets')
        ->select('item_linked_id')
        ->where('asset_id',$assetId)
        ->get();
        $itemLinkedId = json_decode($assetDetail, true);
        return $itemLinkedId[0]['item_linked_id'];
    }

}