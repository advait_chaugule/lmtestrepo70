<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\MetaDataKeyRepositoryInterface;

use App\Data\Models\MetaDataKey;

class MetaDataKeyEloquentRepository extends EloquentRepository implements MetaDataKeyRepositoryInterface
{

    public function getModel()
    {
        return MetaDataKey::class;
    }

}