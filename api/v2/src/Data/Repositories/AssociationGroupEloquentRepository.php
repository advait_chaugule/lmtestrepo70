<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;

use App\Data\Models\AssociationGroup;

class AssociationGroupEloquentRepository extends EloquentRepository implements AssociationGroupRepositoryInterface
{

    public function getModel()
    {
        return AssociationGroup::class;
    }
}