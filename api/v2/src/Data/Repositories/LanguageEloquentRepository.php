<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\Language;

class LanguageEloquentRepository extends EloquentRepository implements LanguageRepositoryInterface
{

    public function getModel()
    {
        return Language::class;
    }

}