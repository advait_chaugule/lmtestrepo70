<?php

namespace App\Data\Repositories\Contracts;

interface ProjectRepositoryInterface
{
     public function getAll($input);
     public function getProjectData($input);

    /**
      * This method will create a new mapping record with project and taxonomy
      */
    public function associateItemWithProject(string $taxonomyIdentifier, string $projectIdentifier);

    /**
      * This method will update the multiple item which are parent to non-editable for a project
      */
      public function updateAllParentItemToNonEditable(array $arrayOfparentId, string $projectIdentifier, string $documentId);
     
    /**
    * return active project record count
    */
    public function getActiveProjectsCount(): int;

   /**
   * Fetch project item mapping from pivot table for specified items
   */
    public function getProjectItemMappingOfMultipleItems(array $itemIds);

   /**
    * Fetch Project name for specified projects
    */
   public function getNameOfMultipleProjects(array $projectIds);

   public function deleteProjectItem($taxonomyIdentifier);

   /**
    * This method returns the list of users associated with project
    */
   public function getMappedUsers($projectIdentifier);

   public function findWokflowStageID($workflow_id);
   
   /**
    * This method checks if user assigned to a  project
    */
    public function checkUserToProjectExists($projectIdentifier, $userId, $workflowStageRoleId);

   /**
    * This method assigns the list of users to roles of a project
    */
   public function assignUserToProject($attributes);

   /**
    * This method unassigns one user from one role of a project
    */
    public function unAssignUserFromProject($projectIdentifier, $attributes,$organizationId);

    /**
     * This method will check for editability of the associated node
     */
    public function checkIsItemEditable(string $itemIdentifier, $projectIdentifier);

    /**
     * This method will count for projects for list of users
     */
    public function projectCount($model, $mapColumn, $identifierSet, $fieldsToCount, $keyValuePairs);

     /**
     * Method to return array of item ids assigned to a project id
     *
     * @param string $projectIdentifier
     * @return array
     */
    public function getItemIdsAssignedToProject(string $projectIdentifier): array;

    public function deleteProjectIdAndDocumentId($projectId);

    public function getProjectListIdByProjectType();

    /**
     * Method to fetch project in PR with status
     *
     * @param string $organizationIdentifier
     * @param array $statusArray
     * @return void
     */
    public function fetchPublicReviewHistoryProjectList(string $organizationIdentifier, array $statusArray);

    public function getItemComplianceReport($itemsAndDocMappedWithProject,$oragnization_id);

    public function getProjectItemMappings($itemId);

    public function getWorkflowDetails($workflowId);

    public function getStageWorkflow($workflowStageId,$roleId);
}