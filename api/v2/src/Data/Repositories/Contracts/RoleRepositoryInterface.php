<?php

namespace App\Data\Repositories\Contracts;
use Illuminate\Http\Request;
interface RoleRepositoryInterface
{
    /**
    * This method will create a new mapping record with role and permission
    */
    public function associatePermissionWithRole(string $roleIdentifier, array $permissionIdentifierSet);

    /**
    * This method will delete the permission preset for a role
    */
    public function deleteRolePermission(string $roleIdentifier);

    /* 
     * This method will list the roles for an organization id
    */
    public function getRoleList(Request $request);

    
}