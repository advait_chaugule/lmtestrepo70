<?php

namespace App\Data\Repositories\Contracts;

use App\Data\Repositories\EloquentRepository;

use App\Data\Models\Item;

interface ItemRepositoryInterface
{
    /**
     * This method returns all the associated models of an item,
     * the associated models will also contains other models associated with it
     * and the projects and case associations for an item
     */
    public function getItemDetailsWithProjectAndCaseAssociation(string $itemId);

    /**
     * 
     * @param type $itemId
     * @return type array of all item
     */
    public function getItemTree($itemId);

    /**
     * 
     * @param type $documentId
     * @return type array of Project Id
     */
    public function getProjectID($documentId);

    public function addProjectItem($itemId, $projectID);

    public function findItemId($identifier,$organizationId);

    public function updateItemParentId($identifier, $parentId);

    public function updateSourceId($userOrganizationId, $documentId);

    public function addOrUpdateCustomMetadataValue(string $itemIdentifier, string $metadataIdentifier, string $value, string $value_html, string $isAdditioanl, string $action);

    public function updateProjectAndDocument($item_id);
    
    public function updateIsDelete($item_id,array $metadata_id);

    public function getItemDetails(array $itemsMappedWithProject);

    public function getItemsMappedWithDocument($documentIdentifier);

    public function getItemsAndDocMappedWithProjectDetails($projectIdentifier);

    public function getItemsAndDocMappedWithTaxonomyDetails($documentIdentifier);

    public function getOrganizationDetails($orgCode);
}