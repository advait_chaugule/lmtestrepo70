<?php
namespace App\Data\Repositories\Contracts;

interface NotificationInterface
{
    public function createNotification(array $attributes);
}
?>