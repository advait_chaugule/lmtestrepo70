<?php

namespace App\Data\Repositories\Contracts;

interface WorkflowRepositoryInterface
{
     public function getWorkflowList($organization_id);    
     public function geWorkflowDetails($organization_id, $workflow_id);

     /**
     * List Distinct Roles for all stages of a workflow
     */
    public function fetchRoleList($workflowStageIDArray);
}