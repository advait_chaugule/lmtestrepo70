<?php

namespace App\Data\Repositories\Contracts;

interface UserRepositoryInterface
{
    public function getUsersList($input);   
    /**
     * Get Active user usage count for a role
     */
    public function usageCountOnRoles($model, $mapColumn, $identifierSet, $fieldsToCount, $keyValuePairs);

    /**
     * Get the user project role list for multiple id
     */
    public function getUserProjectRoleList(array $userIds, string $projectIdentifier);

     /**
     * Get the project list for a user
     */
    public function fetchProjectsList(array $fieldsToReturn, array $conditionalKeyValuePairs);


    /**
     * Get Individual User Details
     */
    public function getIndividualUserDetail(array $active_access_token);

    public function getUserDetailBasedOnEmailId(string $emailId);

    public function updateUserOrganization($userId,array $input);

    public function updateOrganizationUserDeleteStatus($userId,$organizationId,array $input);

    public function fetchProjectUser($fieldsToReturn,$conditionalKeyValuePairs);

}