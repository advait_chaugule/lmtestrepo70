<?php

namespace App\Data\Repositories\Contracts;

use App\Data\Repositories\EloquentRepository;

interface AssetRepositoryInterface
{
    public function getAssetId(string $assetId);
}