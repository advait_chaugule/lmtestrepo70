<?php

namespace App\Data\Repositories\Contracts;

interface NodeTypeRepositoryInterface
{
    /**
     * This method returns all node type metadata
     */
    public function getNodeTypeMetadata(string $nodeTypeId);

    /**
     * This method saves metadata for a node type
     */
    public function saveNodeTypeMetadata(string $nodeTypeId, $requestData);

    /**
     * Fetch all the NodeType list
     */
    public function getNodetypeList($request);

    public function getNodeTypeIsUsedData(array $nodeTypeIdentifier);

    public function getUnmappedTaxonomyList(array $documentIdentifier);
}