<?php

namespace App\Data\Repositories\Contracts;

interface MetadataRepositoryInterface
{
        /**
         *Fetch all metadata list filtered by optional parameters
         */
        public function getMetadataList($request);

        /**
         * Get a metadata detail
         */
        public function getMetadataDetail(string $metadataIdentifier, string $organizationId);

        /**
         * Fetch all NodeTpye list for a Metadata
         */
        public function fetchNodeTypeList(string $metadataIdentifier);
        
        /**
         * Fetch related table list of values
         */
        public function getFieldPossibleValues(string $internal_name,  string $organizationIdentifier);
        
        /**
         * Create new list of values
         */
        public function createFieldPossibleValues(array $createData);

        /**
         * Update the selected list type metadata
         */
        public function updateSelectedFieldPossibleValueOnChangingMetadata(string $identifier, string $oldValue, string $newValue);

        /**
         * Delete respective additional metadata
         */
        public function deleteAdditionalMetadata($identifier, $metadataIdentifier, $entityFromDelete);

        public function getCustomMetadataDetailInfo($nodeIdIdentifier);

        public function deleteCustomMetadataInfo($nodeIdIdentifier,$customMetadataIdIdentifier,$metaDataIdSaved);

        public function saveCustomMetadata($customMetadata,$importDocumentId,$organizationId,$itemData,$newNodeCreated,$copy);
}