<?php

namespace App\Data\Repositories\Contracts;

use App\Data\Repositories\EloquentRepository;

use App\Data\Models\Organization;

interface OrganizationRepositoryInterface
{
    
    public function getDefaultOrganizationDetail();

}