<?php

namespace App\Data\Repositories\Contracts;

interface CompetencyFrameworkRepositoryInterface
{
    /**
     * Get the taxonomical hierarchial data in a tree like view
     * @param string $taxonomyId
     * @return void
     */
    public function getTaxonomyHierarchyById(string $taxonomyId);

    /**
     * Get nth children of a taxonomy 
     * @param string $taxonomyId
     * @return void
     */
    public function getChildren(string $taxonomyId);

    /**
     * Get all related data of a taxonomy
     * @param string $taxonomyId
     * @param bool $getChildStatus - If true then it will fetch the immediate child of the specified taxonomy
     * @param string $cfEntityType - cf_doc or cf_item
     * @return void
     */
    public function getTaxonomyData(string $taxonomyId, bool $getChildStatus, string $cfEntityType);

    /**
     * Get all taxonomies with entity type cf_doc
     */
    public function getAllCFDocuments(array $searchFilters );

    /**
     * This method returns all the associated models of a taxonomy
     * Also the associated models will also contains other models associated with it
     */
    public function getTaxonomyDetailsWithProjectAndCaseAssociation(string $taxonomyId);
    
    /**
     * This method returns all taxonomies that belongs to a rootNodeId (CFDocument Identifier)
     * along with the CFDocument itself
     */
    public function getAllTaxonomyNodesByRootNodeId(string $rootNodeIdentifier);

}