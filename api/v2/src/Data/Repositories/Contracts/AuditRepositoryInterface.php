<?php

namespace App\Data\Repositories\Contracts;

interface AuditRepositoryInterface
{
    /**
     * Dekete all records of a table
     */
    public function deleteAllRecords();

    /**
     * This method will create multiple records in one shot
     * $data = [
     *      [record_set1], [record_set2]......., [record_set(n)]
     * ]
     */
    public function saveMultipleRecords(array $data);

    /**
     * This method will create single record and return eloquent model
     */
    public function saveSingleRecord(array $attributes);

    /**
     * Return single or multiple records with pagination or sorting (both are optional).
     * Can be used to return records with selected fields
     * Where clause can contain multiple conditions with operator options
     * You can also opt for with to fetch related models too. (optional)
     * 
     * @param returnCollection: bool = determine single or multiple records to return
     * @param fieldsToReturn: array = [*] to return all or ["field1", "field2",....... "fieldn"]
     * @param conditionalKeyValuePairs: array = blank array [] or ["field1" => "value1",....."fieldn" => "valuen" ]
     * @param returnPaginatedData: bool = determine if records returned should be paginated
     * @param paginationFilters: array = pagination filters viz. ['limit' => 10, 'offset' => 0]
     * @param returnSortedData: bool = determine records returned should be sorted
     * @param sortfilters: array = sort filters viz. ['orderBy' => 'filed', 'sorting' => 'desc']
     * @param operator: string = operation to be performed on conditionalKeyValuePairs
     * @param relations: array = array of related methods to return
     */
    public function find (
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $conditionalKeyValuePairs = [],
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND', 
        $relations = null
    );

}