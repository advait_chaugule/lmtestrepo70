<?php

namespace App\Data\Repositories\Contracts;

interface NoteRepositoryInterface
{
    /**
     * Fetch all the Note
     */
    //public function getNoteList($request);

    public function getNoteIsUsedData(array $noteIdentifier);

}