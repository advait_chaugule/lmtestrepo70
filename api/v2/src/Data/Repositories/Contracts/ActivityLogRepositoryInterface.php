<?php
namespace App\Data\Repositories\Contracts;
use Illuminate\Support\Collection;

interface ActivityLogRepositoryInterface
{

    /**
     * This method return activity logs based on various parameters from the OLAP database
     * @param fieldsToReturn: array = [*] to return all or ["field1", "field2",....... "fieldn"]
     * @param keyValuePairs: array = blank array [] or ["field1" => "value1",....."fieldn" => "valuen" ]
     * @param returnPaginatedData: bool = determine if records returned should be paginated
     * @param paginationFilters: array = pagination filters viz. ['limit' => 10, 'offset' => 0]
     * @param returnSortedData: bool = determine records returned should be sorted
     * @param sortfilters: array = sort filters viz. ['orderBy' => 'activity_log_timestamp', 'sorting' => 'desc']
     * @param rangeFilters: array = mainly for date range ["start" => "Y-m-d", "end" => "Y-m-d"]
     * @param rangeQueryClauseField: string = e.g... activity_log_timestamp
     */
    public function getActivityLogs(
        array $fieldsToReturn = ['*'],
        array $keyValuePairs = [],
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'activity_log_timestamp', 'sorting' => 'desc'],
        array $rangeFilters = [],
        string $rangeQueryClauseField = ""
    ): Collection;

    public function getNodeIdMappedWithNodeName($organizationIdentifier);

    public function getItemDetails(string $documentIdentifier);

    public function getItemsMappedWithProjectDetails(string $projectIdentifier);

    public function getNodeIdDetails(array $itemsMappedDetails);
    
    public function getNodeTypeMetadataDetails(array $nodeType);
    
    public function getMetadataDetails(array $metadata);

    public function getFieldsFilledDetails(array $fieldName , array $documentIdentifier , array $nodeName ,array $nodeDetails , array $itemsMappedDetails, string $organizationIdentifier);

    public function getNodeNameDetails(array $nodeType);

    
}