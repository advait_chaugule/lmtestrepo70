<?php

namespace App\Data\Repositories\Contracts;

use App\Data\Repositories\EloquentRepository;

use App\Data\Models\ItemAssociation;

interface ItemAssociationRepositoryInterface
{
    /**
     * This method returns all the associated models of an item association
     */
    public function getItemAssociationAndRelatedModel(string $itemAssociationIdentifier);

    public function getOriginNodeId(string $associationId);
}