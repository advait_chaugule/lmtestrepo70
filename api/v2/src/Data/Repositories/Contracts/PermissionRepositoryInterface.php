<?php

namespace App\Data\Repositories\Contracts;

interface PermissionRepositoryInterface
{
    /**
     * This method returns all the permission
     */
    public function getInternalPermissionNamesAssignedToRole(string $userRoleId);
}