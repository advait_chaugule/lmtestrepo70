<?php

namespace App\Data\Repositories\Contracts;
use App\Data\Repositories\EloquentRepository;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;
use App\Data\Models\WorkflowStage;

interface WorkflowStageRepositoryInterface
{
    public function deleteandadd($workflowStageId, $data);   
    public function getWorkflowStageList($workflowId);   
    public function getRoleDetails($data);
    public function fetchRolesForAStage($workflowStageId);
    public function removeWorkflowStageRole($workflow_stage_role_id);
}