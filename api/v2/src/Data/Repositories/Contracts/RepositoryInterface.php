<?php

namespace App\Data\Repositories\Contracts;

interface RepositoryInterface
{

    /**
     * Returns the first record in the database.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function first();

    /**
     * Returns all the records.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Returns the count of all the records.
     *
     * @return int
     */
    public function count();

    /**
     * Returns a range of records bounded by pagination parameters.
     *
     * @param int limit
     * @param int $offset
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function page(int $limit = 10, int $offset = 0, array $relations = [], string $orderBy = 'updated_at', string $sorting = 'desc');

    /**
     * Find a record by its identifier.
     *
     * @param string $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(string $id);

    /**
     * Find a record by an attribute.
     * Fails if no model is found.
     *
     * @param string $attribute
     * @param string $value
     * @param array  $relations
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findBy(string $attribute, string $value, $relations = null);

    /**
     * Find all records by an associative array of attributes.
     * Two operators values are handled: AND | OR.
     *
     * @param array  $attributes
     * @param string $operator
     * @param array  $relations
     *
     * @return \Illuminate\Support\Collection
     */
    public function findByAttributes(
        array $attributes,

        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],

        string $operator = 'AND',
        $relations = null);

    /**
     * Fills out an instance of the model
     * with $attributes.
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function fill(array $attributes);

    /**
     * Fills out an instance of the model
     * and saves it, pretty much like mass assignment.
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function fillAndSave(array $attributes);

    /**
     * Remove a selected record.
     *
     * @param string $key
     *
     * @return bool
     */
    public function remove(string $key);

    /**
     * Update a single record
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function edit($id, array $attributes);

    /**
     * Undocumented function
     * Method to update multiple records based on id
     * @param string $arrayOfIds
     * @param array $attributeValuePairs example = ["field" => value]
     * @return void
     */
    public function updateMultipleByIds(string $arrayOfIds, array $attributeValuePairs);

    /**
     * Method to update single row based on filtered column value pair
     * @param array $columnValueFilterPairs
     * @param array $attributeValuePairs example = ["field" => value]
     * @return void
     */
    public function editWithCustomizedFields(array $arrayOfIds, array $attributeValuePairs);

    
    /**
     * this method creates multiple records at once
     * @param string $attributes - [[key1=>value1],[key2=>value2]]
     * @return void
     */
    public function saveMultiple(array $attributes);

    /**
     * This method will create and return eloquent model
     * This was implemented to prevent fillandsave method creating unwanted results during consecutive calls inside a transaction block
     */
    public function saveData(array $attributes);

    /**
     * Method to return multiple records using in query
     * attributeIn -> in query field name
     * containedInValues -> array of values for in query
     * whereClauseAttributes -> array of key=>value pairs for extra conditions
     * operator -> AND or OR operation between whereClauseAttributes
     * relations -> for eagerloading related models
     */
    public function findByAttributeContainedIn(
        string $attributeIn, 
        array $containedInValues,
        array $whereClauseAttributes,
        string $operator = 'AND', 
        $relations = null
    );

    /**
     * Clone of findByAttributes with features like return single or multiple records, query for selected fields only
     * with pagination and sorting
     */
    public function findByAttributesWithSpecifiedFields(
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs,
        $relations = null,
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND'
    );

    /**
    * Clone of findByAttributeContainedIn with features like return single or multiple records, query for selected fields only
    * with pagination and sorting
    */
    public function findByAttributesWithSpecifiedFieldsWithInQuery(
        string $attributeIn, 
        array $containedInValues,
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs = [],
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND', 
        $relations = null
    );

    /**
     * Method to truncate (empty table)
     */
    public function deleteAll();

    /**
     * This method will create a model instance
     * ## Mainly created to solve issues with fill method inside a loop or transaction block
     * @param array $attributes
     * @return void
     */
    public function createModelInstanceInMemory(array $attributes);

}