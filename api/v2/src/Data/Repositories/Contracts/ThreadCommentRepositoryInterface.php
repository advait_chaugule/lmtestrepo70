<?php
namespace App\Data\Repositories\Contracts;

use App\Data\Repositories\EloquentRepository;

use App\Data\Models\ThreadComment;

interface ThreadCommentRepositoryInterface
{
    
    public function getCommentReport(array $userData,array $projectId);

    public function getItemsCommentMadeFor(array $itemsDetails);

    public function getCommentList(array $itemsDetails,array $itemsCommentMadeFor);

    public function getDocumentCommentList(array $itemsDetails,array $itemsCommentMadeFor);
    
}