<?php

namespace App\Data\Repositories\Contracts;

use Illuminate\Http\Request;

interface NodeTemplateRepositoryInterface
{
     /**
      * Associate NodeType with NodeTemplate
      */
    public function associateNodeTypeWithTemplate(array $attributes);

    /**
      * List all node templates
      */
    public function getNodeTemplateList(Request $request);

    /**
      * Count usage of each node template
      */
    public function findUsageCount(string $nodeTemplateIdentifier);
}