<?php 
namespace App\Data\Repositories\Contracts;

use App\Data\Models\Document;

interface DocumentRepositoryInterface
{
    public function listDocumentRoot($searchFilterAttribut);
    public function getDocumentDataAndAssociatedModels(string $documentId);    
    public function filterDocumentByAttributes(array $attributes, bool $returnPaginatedData, array $paginationFilters, bool $returnSortedData, array $sortfilters, bool $returnSearchData, string $searchColumn, array $searchFilters);
    public function mapDocumentWithSubject(Document $document, string $subjectIdentifier);
    public function addOrUpdateCustomMetadataValue(string $documentIdentifier, string $metadataIdentifier, string $value, string $value_html,string $isAdditional, string $action);
    public function saveDocumentMapping(array $attributesToSave);
    public function fetchPublicReviewHistory(string $documentIdentifier, string $organizationIdentifier);
    public function checkPublicReviewIsOpen(string $documentIdentifier, string $organizationIdentifier, array $reviewStatus);
    public function savePublicReviewHistory(array $dataToInsert);
    public function getPublicReviewHistory(array $documentIdentifier);
    public function stopPublicReview(array $input , array $userData);
    public function exportTaxonomy(string $documentIdentifier, array $getItemDetailToExport, array $getDocDetailToExport);
    public function getDocDetailsWithProjectAndCaseAssociation(string $itemId);
}