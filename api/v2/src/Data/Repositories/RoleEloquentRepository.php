<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\Role;

class RoleEloquentRepository extends EloquentRepository implements RoleRepositoryInterface
{

    public function getModel()
    {
        return Role::class;
    }

    public function getRelatedModels($roleIdentifier){
        $fetchAssociatedModels = [
            "permissions"
        ];
        return $this->findBy("role_id", $roleIdentifier, $fetchAssociatedModels);
    }


    /**
     * This method will create a new mapping record with role and permission
     */
    public function associatePermissionWithRole(string $roleIdentifier, array $permissionIdentifierSet) {

        $role = $this->find($roleIdentifier);

        foreach($permissionIdentifierSet as $permissionIdentifier){
            $role->permissions()->detach($permissionIdentifier);
            $role->permissions()->attach($permissionIdentifier);
        }

        return $this->getRelatedModels($roleIdentifier);
    }

    /**
     * This method will create a new mapping record with role and permission
     */
    public function deleteRolePermission(string $roleIdentifier) {

        $role = $this->find($roleIdentifier);
        
        foreach($role->permissions as $permissionIdentifier){
            $role->permissions()->detach($permissionIdentifier);
        }

        return $this->getRelatedModels($roleIdentifier);
    }

    /* List all ROLES for an organization 
     * with Search and Sort parameter
     * which are optional
     */
    public function getRoleList(Request $request)
    {
        $allowedOrderByFields = ['name',  'status',  'usage_count'];
        $allowedOrderByMethods = ['asc', 'desc'];
        
        $limit = 10;
        if ($request->has('limit') && is_int($request->limit)) {
            $limit = $request->limit;
        }

        $offset = 0;
        if ($request->has('offset') && is_int($request->offset)) {
            $offset = $request->offset;
        }

        $organizationId = $request->auth_user['organization_id'];

      //  return $this->model->take($limit)->skip($offset)
      return $this->model
        ->when($request->has('sort'), function($query) use ($request, $allowedOrderByFields, $allowedOrderByMethods) {
            list($orderBy, $sorting) = explode(':', $request->sort);
            if (in_array($orderBy, $allowedOrderByFields) && in_array($sorting, $allowedOrderByMethods)) {
                $query->orderBy($orderBy, $sorting);
            }            
        })
        ->where('organization_id', $organizationId)
        ->where('is_deleted', 0)
        ->when($request->has('search'), function($query) use ($request) {
            $query->orWhere('name', 'like', "%{$request->search}%");
            $query->orWhere('description', 'like', "%{$request->search}%");
        })
        ->select('role_id', 'name', 'description', 'is_active', 'role_code')
        ->get();
    }
}