<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ProjectAccessRequestRepositoryInterface;

use App\Data\Models\ProjectAccessRequest;

class ProjectAccessRequestEloquentRepository extends EloquentRepository implements ProjectAccessRequestRepositoryInterface
{

    public function getModel()
    {
        return ProjectAccessRequest::class;
    }

   
}