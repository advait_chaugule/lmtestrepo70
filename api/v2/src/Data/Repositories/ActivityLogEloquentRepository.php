<?php
namespace App\Data\Repositories;

use App\Data\Repositories\EloquentAuditRepository;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

use Illuminate\Support\Collection;

use App\Data\Models\Report\FactActivityLog;

use Illuminate\Support\Facades\DB;

class ActivityLogEloquentRepository extends EloquentAuditRepository implements ActivityLogRepositoryInterface
{
    public function getModel()
    {
        return FactActivityLog::class;
    }


    /**
     * This method return activity logs based on various parameters from the OLAP database
     * @param fieldsToReturn: array = [*] to return all or ["field1", "field2",....... "fieldn"]
     * @param keyValuePairs: array = blank array [] or ["field1" => "value1",....."fieldn" => "valuen" ]
     * @param returnPaginatedData: bool = determine if records returned should be paginated
     * @param paginationFilters: array = pagination filters viz. ['limit' => 10, 'offset' => 0]
     * @param returnSortedData: bool = determine records returned should be sorted
     * @param sortfilters: array = sort filters viz. ['orderBy' => 'activity_log_timestamp', 'sorting' => 'desc']
     * @param rangeFilters: array = mainly for date range ["start" => "Y-m-d", "end" => "Y-m-d"]
     * @param rangeQueryClauseField: string = e.g... activity_log_timestamp
     */
    public function getActivityLogs(
        array $fieldsToReturn = ['*'],
        array $keyValuePairs = [],
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'activity_log_timestamp', 'sorting' => 'desc'],
        array $rangeFilters = [],
        string $rangeQueryClauseField = ""
    ): Collection {

         // prepare the query initially
         $query = $this->model->select($fieldsToReturn);

         if(!empty($rangeFilters) && !empty($rangeQueryClauseField)){
             extract($rangeFilters);
            // $query = $query->whereBetween($rangeQueryClauseField, [$start, $end]);
            $query = $query
                        ->where($rangeQueryClauseField, '>=', $start)
                        ->where($rangeQueryClauseField, '<=', $end);
         }

         if(!empty($keyValuePairs)) {
             // Get the last value of the associative array
             $lastValue = end($keyValuePairs);
             // Get the last key of the associative array
             $lastKey = key($keyValuePairs);
             // update the query with a where clause initially
             $query = $query->where($lastKey, $lastValue);
             // Pop the last key value pair of the associative array now that it has been added to Builder already
             array_pop($keyValuePairs);
             foreach ($keyValuePairs as $key => $value) {
                 $query->where($key, $value);
             }
         }
 
         if($returnPaginatedData===true){
             extract($paginationFilters);
             $query->take($limit)->skip($offset);
         }
         //Sorting is commented for Performance issue
         /*
         if($returnSortedData===true){
             extract($sortfilters);
            // $query->orderBy($orderBy, $sorting);
         }
         */
         return $query->get();
    }

    public function getNodeIdMappedWithNodeName($organizationIdentifier){
        
        $nodeTypeArray = config("node")["NodeTypeArray"];
        $getNodeTypeDetails = DB::table('node_types')
                    ->select('*')
                    ->where('organization_id', $organizationIdentifier)
                    ->whereIn('title', $nodeTypeArray)
                    ->where('is_deleted','0')
                    ->where('used_for',0)
                    ->get()
                    ->toArray();
        
        $nodeIdMappedWithNodeName=array();
        foreach($nodeTypeArray as $nodeTypeArrayKey => $nodeTypeArrayValue ){
            foreach($getNodeTypeDetails as $getNodeTypeDetailsKey => $getNodeTypeDetailsValue ){
                if($nodeTypeArrayValue==$getNodeTypeDetailsValue->title){
                    $nodeIdMappedWithNodeName[$getNodeTypeDetailsValue->node_type_id]=$nodeTypeArrayValue;
                }
            }
        }
        
        return $nodeIdMappedWithNodeName;

    }

    public function getDocumentMetadataIdMappedWithMetadataName($organizationIdentifier){
        
        $documentOrderArr = config("node")["DocumentOrderArr"];
        $getMetadataDetails = DB::table('metadata')
                    ->select('*')
                    ->where('organization_id', $organizationIdentifier)
                    ->whereIn('name', $documentOrderArr)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $documentMetadataIdMappedWithMetadataName=array();
        foreach($documentOrderArr as $documentOrderArrKey => $documentOrderArrValue ){
            foreach($getMetadataDetails as $getMetadataDetailsKey => $getMetadataDetailsValue ){
                if($documentOrderArrValue==$getMetadataDetailsValue->name){
                    $documentMetadataIdMappedWithMetadataName[$getMetadataDetailsValue->metadata_id]=$documentOrderArrValue;
                }
            }
        }
        
        return $documentMetadataIdMappedWithMetadataName;

    }

    public function getNonDocumentMetadataIdMappedWithMetadataName($organizationIdentifier){
        
        $nonDocumentOrderArr = config("node")["NondocumentOrderArr"];
        $getMetadataDetails = DB::table('metadata')
                    ->select('*')
                    ->where('organization_id', $organizationIdentifier)
                    ->whereIn('name', $nonDocumentOrderArr)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $nonDocumentMetadataIdMappedWithMetadataName=array();
        foreach($nonDocumentOrderArr as $nonDocumentOrderArrKey => $nonDocumentOrderArrValue ){
            foreach($getMetadataDetails as $getMetadataDetailsKey => $getMetadataDetailsValue ){
                if($nonDocumentOrderArrValue==$getMetadataDetailsValue->name){
                    $nonDocumentMetadataIdMappedWithMetadataName[$getMetadataDetailsValue->metadata_id]=$nonDocumentOrderArrValue;
                }
            }
        }
        
        return $nonDocumentMetadataIdMappedWithMetadataName;

    }

    public function getDocumentDetails($documentIdentifier){

        $getDocumentDetails = DB::table('documents')
                    ->select('*')
                    ->where('document_id', $documentIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        $getTitleAndCreatorDetail=array();            
        foreach($getDocumentDetails as $getDocumentDetailsKey => $getDocumentDetailsValue){
                $getTitleAndCreatorDetail['title']=$getDocumentDetailsValue->title;
                $getTitleAndCreatorDetail['creator']=$getDocumentDetailsValue->creator;
        }
        
        return $getTitleAndCreatorDetail;
    }

    public function getProjectRelatedDocumentId($itemsMappedDetails){

        $count=count($itemsMappedDetails);
        for($i=0;$i<=$count-1;$i++){
            $itemsMapped[]=$itemsMappedDetails[$i]->item_id;
        }
        $getDocumentIds = DB::table('items')
                    ->select('document_id')
                    ->whereIn('item_id',$itemsMapped)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $storeDocumentId=array();            
        foreach($getDocumentIds as $getDocumentIdsKey=>$getDocumentIdsValue){
            $storeDocumentId[]=$getDocumentIdsValue->document_id;
        }            

        $uniqueDocumentId=array_unique($storeDocumentId);
        $documentId = implode(',', $uniqueDocumentId);
        return $documentId;
       
    }

    public function getItemDetails(string $documentIdentifier){

        $itemDetails = DB::table('items')
                    ->select('node_type_id')
                    ->where('document_id', $documentIdentifier)
                    ->where('is_deleted','0')
                    ->groupBy('node_type_id')
                    ->get()
                    ->toArray();
        return $itemDetails;

    }

   
    public function getItemsMappedWithProjectDetails(string $projectIdentifier){
        
        $itemsMappedDetails = DB::table('project_items')
                    ->select('item_id')
                    ->where('project_id', $projectIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        return $itemsMappedDetails;

    }

    public function getNodeIdDetails(array $itemsMappedDetails){
        $count=count($itemsMappedDetails);
        for($i=0;$i<=$count-1;$i++){
            $itemsMapped[]=$itemsMappedDetails[$i]->item_id;
        }
        $nodeIdDetailData = DB::table('items')
                    ->select('node_type_id')
                    ->whereIn('item_id', $itemsMapped)
                    ->where('is_deleted','0')
                    ->groupBy('node_type_id')
                    ->get()
                    ->toArray();
        return $nodeIdDetailData;            
    }

    public function getNodeTypeMetadataDetails(array $nodeType){
        $count=count($nodeType);
        for($i=0;$i<=$count-1;$i++){
            $nodesTypeData[]=$nodeType[$i]->node_type_id;
        }
        $metadata = DB::table('node_type_metadata')
                    ->select('node_type_id','metadata_id')
                    ->whereIn('node_type_id',$nodesTypeData)
                    // ->whereIn('is_mandatory',array(1))
                    ->get()
                    ->toArray();
        return $metadata;
        
    }

    public function getMetadataDetails(array $metadata){
        $count=count($metadata);
        for($i=0;$i<=$count-1;$i++){
            $metadataId[]=$metadata[$i]->metadata_id;
        }
        $fieldName = DB::table('metadata')
                    ->select('metadata_id','organization_id','name','internal_name','is_custom')
                    ->whereIn('metadata_id',$metadataId)
                    ->whereIn('is_custom',array(0,1))
                    ->whereIn('is_deleted',array(0))
                    ->get()
                    ->toArray();
        return $fieldName;
    }

    public function getNodeNameDetails(array $nodeType){
        $count=count($nodeType);
        for($i=0;$i<=$count-1;$i++){
            $nodesTypeData[]=$nodeType[$i]->node_type_id;
        }
        $nodeName = DB::table('node_types')
                    ->select('node_type_id','title','is_custom')
                    ->whereIn('node_type_id',$nodesTypeData)
                    ->whereIn('is_deleted',array(0))
                    ->where('used_for',0)
                    ->get()
                    ->toArray();
        return $nodeName;

    }

    public function getFieldsFilledDetails(array $fieldName , array $documentIdentifier , array $nodeName ,array $nodeDetails , array $itemsMappedDetails, string $organizationIdentifier ){
        // to get empty field name and their respective count start
        // Get Mapped Id with Node Name and Metadata Name start
        //$nodeIdMappedWithNodeName=$this->getNodeIdMappedWithNodeName($organizationIdentifier);
        //$documentMetadataIdMappedWithMetadataName=$this->getDocumentMetadataIdMappedWithMetadataName($organizationIdentifier);
        //$nonDocumentMetadataIdMappedWithMetadataName=$this->getNonDocumentMetadataIdMappedWithMetadataName($organizationIdentifier);
        
        //documentIdentifier
        if(!empty($documentIdentifier)){
            $getTitleAndCreatorDetail=$this->getDocumentDetails($documentIdentifier);
        }
        //documentIdentifier

        //projectIdentifier
        if(!empty($itemsMappedDetails)){
            $getProjectRelatedDocumentId=$this->getProjectRelatedDocumentId($itemsMappedDetails);
        }
        if(!empty($getProjectRelatedDocumentId)){
            $getTitleAndCreatorDetail=$this->getDocumentDetails($getProjectRelatedDocumentId);
        }
        //projectIdentifier
        
        $complianceConsideredFields=array();
        if(!empty($getTitleAndCreatorDetail)){
            foreach($getTitleAndCreatorDetail as $getTitleAndCreatorDetailKey => $getTitleAndCreatorDetailValue){
                if($getTitleAndCreatorDetailValue==""){
                    $complianceConsideredFields[$getTitleAndCreatorDetailKey]=1;
                }
            }
        }
        
        // Get Mapped Id with Node Name and Metadata Name end

        
        // TOP SECTION
        $count=count($fieldName);
        $casefieldTitle=array();
        $customfieldTitle=array();
        $fieldTitle=array();
        // to filter case fields
        for($i=0;$i<=$count-1;$i++){
            if($fieldName[$i]->is_custom==0){
                $casefieldTitle[]=$fieldName[$i]->internal_name;
            }
        }
        // to filter custom fields
        for($i=0;$i<=$count-1;$i++){
            if($fieldName[$i]->is_custom==1){
                $customfieldTitle[$fieldName[$i]->metadata_id]=$fieldName[$i]->name;
            }
        }
        for($i=0;$i<=$count-1;$i++){
            // if($fieldName[$i]->internal_name!=null){
                $fieldTitles[]=$fieldName[$i]->internal_name;
            // }        
        }
        foreach($fieldTitles as $fieldTitlesKey=>$fieldTitlesValue){
            if($fieldTitlesValue!=null){
                $fieldTitle[]=$fieldTitlesValue;
            }
        }
        
        $keyValueFlipped= array_flip($fieldTitle);
        $columnName = array_keys($keyValueFlipped);
        $license_data=array('license_title' , 'license_text','license_description');
        $concept_data=array('concept_title','concept_keywords','concept_description','concept_hierarchy_code');
        $language_data=array('language');
        $subject_data=array('subject_title','subject_hierarchy_code','subject_description');
        foreach($columnName as $key=>$columnNameIdentifier){
            if(in_array($columnNameIdentifier,$license_data)){
                unset($columnName[$key]);
                $columnName[]='license_id';
            }
            if(in_array($columnNameIdentifier,$concept_data)){
                unset($columnName[$key]);
                $columnName[]='concept_id';
            }
            if(in_array($columnNameIdentifier,$language_data)){
                unset($columnName[$key]);
                $columnName[]='language_id';
            }
        }
        $columnName=array_unique($columnName);

        if(in_array('pacing_guide_item_type',$columnName))
        {
            $columnName=array_flip($columnName);
            unset($columnName['pacing_guide_item_type']);
            $columnName=array_flip($columnName);
        }
        
        $excludeColumn=array('alternative_label');
        $columnName=array_diff($columnName,$excludeColumn);
        
        if(!in_array('full_statement',$columnName))
        {
            array_push($columnName,'full_statement');
            
        }
        
        
        // to fetch empty value
        if(!empty($documentIdentifier['document_id'])){
            $fieldValue = DB::table('items')
                        ->select($columnName)
                        ->where('document_id',$documentIdentifier)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
        }
        else{
            $count=count($itemsMappedDetails);
            for($i=0;$i<=$count-1;$i++){
                $itemsMapped[]=$itemsMappedDetails[$i]->item_id;
            }
            $fieldValue = DB::table('items')
                        ->select($columnName)
                        ->whereIn('item_id',$itemsMapped)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
        }
        
        $getCount = count($fieldValue);
        $emptyFieldCount=array();
        $emptyField=array();
        foreach($columnName as $singleColumnName){
            for($i=0;$i<=$getCount-1;$i++){
                if($fieldValue[$i]->$singleColumnName==""){
                    $emptyField[$singleColumnName]=$fieldValue[$i]->$singleColumnName;
                    $emptyFieldCount[]=array_push($emptyFieldCount,$singleColumnName);
                }
            }
        }
        // dd($emptyField,$emptyFieldCount);
        $caseFieldSortedData=array();
        foreach($emptyField as $emptyFieldKey => $emptyFieldValue){
            $countValue=1;
            foreach($emptyFieldCount as $emptyFieldCountKey => $emptyFieldCountValue){
                if($emptyFieldKey==$emptyFieldCountValue){
                    $caseFieldSortedData[$emptyFieldKey]=$countValue++;
                }
            }
        }
        // dd($caseFieldSortedData);
        arsort($caseFieldSortedData);
        $caseFieldSortedDataTop=$caseFieldSortedData;
        $caseFieldSortedKeyValue=array();
        if(!empty($complianceConsideredFields)){
            $caseFieldSortedDataTop=$complianceConsideredFields+$caseFieldSortedDataTop;
        }
        
        /** check document case fields based on mapped with project start */
        if(!empty($documentIdentifier['document_id']))
        {
            $fieldsToCheck=array('creator','title','full_statement');
            $titleCreatorFlag = 1;
        }
        else
        {
            $countDocumentNode=count($itemsMappedDetails);
            for($i=0;$i<=$countDocumentNode-1;$i++){
                $itemIDMapped[]=$itemsMappedDetails[$i]->item_id;
            }

            $projectIDArray = DB::table('project_items')
                        ->select('project_id')
                        ->whereIn('item_id',$itemIDMapped)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
            $projectID=$projectIDArray[0]->project_id;
            
            $getProjectIdAndDocumentId = DB::table('projects')
                        ->select('project_id','document_id')
                        ->where('project_id',$projectID)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
            $projectRelatedDocumentId=$getProjectIdAndDocumentId[0]->document_id;

            $getProjectIdAndDocumentIdFromDocumentTable = DB::table('documents')
                        ->select('project_id','document_id')
                        ->where('document_id',$projectRelatedDocumentId)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
                        
            $documentTableProjectId  = $getProjectIdAndDocumentIdFromDocumentTable[0]->project_id;
            $documentTableDocumentId = $getProjectIdAndDocumentIdFromDocumentTable[0]->document_id;                       
            
            if($projectID==$documentTableProjectId && $projectRelatedDocumentId==$documentTableDocumentId)
            {
                $fieldsToCheck=array('creator','title','full_statement');
                $titleCreatorFlag = 1;
            }
            else
            {
                $fieldsToCheck=array('full_statement');
                $titleCreatorFlag = 0;
            }
            
        }
        /** check document case fields based on mapped with project end */

        // $fieldsToCheck=array('creator','title','full_statement');
        $caseFieldSortedDataTopShow=array();
        $customFieldSortedDataTopShow=array(); 
        foreach($caseFieldSortedDataTop as $caseFieldSortedDataTopKeyData => $caseFieldSortedDataTopValueData){
            if(in_array($caseFieldSortedDataTopKeyData,$fieldsToCheck)){
                $caseFieldSortedDataTopShow[$caseFieldSortedDataTopKeyData]=$caseFieldSortedDataTopValueData;
            }
            //     else{
            //     $customFieldSortedDataTopShow[$caseFieldSortedDataTopKeyData]=$caseFieldSortedDataTopValueData;
            // }
            // dd($caseFieldSortedData);
            //DEVMODIFIED
            ///if(!in_array($caseFieldSortedDataTopKeyData,$fieldsToCheck)){
                $customFieldSortedDataTopShow[$caseFieldSortedDataTopKeyData]=$caseFieldSortedDataTopValueData;
            ///}
        }
        //added
        arsort($caseFieldSortedDataTopShow);
        $caseFieldSortedDataTopShows=$caseFieldSortedDataTopShow;
        //added
        if(!empty($caseFieldSortedDataTopShows)){
            $i=0;
            foreach($caseFieldSortedDataTopShows as $caseFieldSortedDataTopKey=>$caseFieldSortedDataTopValue){
                $caseFieldSortedKeyValue[$i]['key']=$caseFieldSortedDataTopKey;
                $caseFieldSortedKeyValue[$i]['value']=$caseFieldSortedDataTopValue;
                $i++;
            }
        }
        else{
            $caseFieldSortedKeyValue=array();
        }
        $caseFieldSortedKeyValues=array_slice($caseFieldSortedKeyValue,0,10);
        ////custom field
        
        if(!empty($customFieldSortedDataTopShow)){
            $i=0;
            foreach($customFieldSortedDataTopShow as $customFieldSortedDataTopKey=>$customFieldSortedDataTopValue){
                $customFieldSortedKeyValue[$i]['key']=$customFieldSortedDataTopKey;
                $customFieldSortedKeyValue[$i]['value']=$customFieldSortedDataTopValue;
                $i++;
            }
        }
        else{
            $customFieldSortedKeyValue=array();
        }
        //uncomment
        $customFieldSortedKeyValues=array_slice($customFieldSortedKeyValue,0,10);

        //coverage mandatory field only check start
        if(!empty($documentIdentifier['document_id'])){
            $dataField = DB::table('items')
                        ->where('document_id',$documentIdentifier)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
        }
        else{
        $dataField = DB::table('items')
                        ->whereIn('item_id',$itemsMapped)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
        }
        $nodeIds=array();                
        foreach($dataField as $dataFieldKey=>$dataFieldValue){
            $nodeIds[]=$dataFieldValue->node_type_id;
        }
        
        $nodeIdMappedWithMedataId = DB::table('node_type_metadata')
                        ->rightjoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
                        ->select('node_type_metadata.node_type_id','node_type_metadata.metadata_id','metadata.is_custom','metadata.name','metadata.internal_name')
                        ->whereIn('node_type_metadata.node_type_id',$nodeIds)
                        ->where('node_type_metadata.is_mandatory','1')
                        ->get()
                        ->toArray();
        $nodeIdUnique=array_unique($nodeIds);
        $nodeIdWithMetadataId=array();
           
        
         $nodeIDwithMetadataID = array();
         $nodeIDwithMetadataName = array();
         $nodeIDwithConceptName = array();
         $nodeIDwithLicenseName = array();
         $nodeIDwithLanguageName = array();
            foreach($nodeIdUnique as $nodeIdUniqueIdK=>$nodeIdUniqueV)
            {
                $metanameArray=array();
                $metaIdArray=array();
                $conceptArray=array();
                $licenseArray=array();
                $languageArray=array();
                $customArray=array();
                $customNameArray=array();
                foreach($nodeIdMappedWithMedataId as $nodeIdMappedWithMedataIdK=>$nodeIdMappedWithMedataIdV){
                    
                    if($nodeIdMappedWithMedataIdV->node_type_id == $nodeIdUniqueV && $nodeIdMappedWithMedataIdV->is_custom == 0 && $nodeIdMappedWithMedataIdV->internal_name != "full_statement")
                    {
                        $internal_name = $nodeIdMappedWithMedataIdV->internal_name;
                        if (in_array($internal_name, $license_data))
                        {
                            $name = str_replace("license_","",$internal_name);
                         array_push($licenseArray,''.$name);
                        }
                        else if (in_array($internal_name, $concept_data))
                        {
                            $name = str_replace("concept_","",$internal_name);
                            array_push($conceptArray,$name);
                        }
                        else if (in_array($internal_name, $language_data))
                        {
                            $name = "name";
                            array_push($languageArray,$name);
                        }
                        else
                        {
                            array_push($metanameArray,$internal_name);
                        }
                       
                        array_push($metaIdArray,$nodeIdMappedWithMedataIdV->metadata_id);
                    }
                    else if($nodeIdMappedWithMedataIdV->node_type_id == $nodeIdUniqueV && $nodeIdMappedWithMedataIdV->is_custom == 1 && $nodeIdMappedWithMedataIdV->internal_name != "full_statement")
                    {   
                        $arrIdwithName = array();
                        $arrIdwithName[$nodeIdMappedWithMedataIdV->metadata_id] = $nodeIdMappedWithMedataIdV->name;
                        array_push($customNameArray,$nodeIdMappedWithMedataIdV->name);
                        array_push($customArray,$nodeIdMappedWithMedataIdV->metadata_id);
                       $customIdNameArray[count($customArray) - 1][$nodeIdMappedWithMedataIdV->metadata_id] = $nodeIdMappedWithMedataIdV->name;
                        //array_push($customIdNameArray,$arrIdwithName);
                      //  unset($arrIdwithName);
                    }

                }
                
                $nodeIdWithMetadataId[$nodeIdUniqueV] = $metaIdArray;
                $nodeIDwithMetadataName[$nodeIdUniqueV] = $metanameArray;
                $nodeIDwithConceptName[$nodeIdUniqueV] = $conceptArray;
                $nodeIDwithLicenseName[$nodeIdUniqueV] = $licenseArray;
                $nodeIDwithLanguageName[$nodeIdUniqueV] = $languageArray;
                unset($metaIdArray);
                unset($metanameArray);
                unset($conceptArray);
                unset($licenseArray);
                unset($languageArray);
                if(isset($customNameArray))
                {
                    $nodeIDwithCustomName[$nodeIdUniqueV] = $customNameArray;
                    unset($customNameArray);
                }
                if(isset($customArray))
                {
                    $nodeIDwithCustomMetadataId[$nodeIdUniqueV] = $customArray;
                    unset($customArray);
                }
                if(isset($customIdNameArray))
                {
                    $nodeIDwithCustomNameMetadataId[$nodeIdUniqueV] = $customIdNameArray;
                    unset($customIdNameArray);
                }
                
                
                
            }
            
              $conceptIdArr = array();
              $licensesIdArr = array();
              $languageIdArr = array();
            foreach($dataField as $dataFieldKey=>$dataFieldValue){
                if($dataFieldValue->concept_id != "")
                {
                    array_push($conceptIdArr,$dataFieldValue->concept_id);
                }
                if($dataFieldValue->license_id != "")
                {
                    array_push($licensesIdArr,$dataFieldValue->license_id);
                }
                if($dataFieldValue->language_id != "")
                {
                    array_push($languageIdArr,$dataFieldValue->language_id);
                }

                
            }
            $conceptFlag = 0;
            $licenseFlag = 0;
            $languageFlag = 0;
            
            $conceptdataField=array();
            if(!empty($conceptIdArr))
            {
                $conceptFlag = 1;
                $conceptdataField = DB::table('concepts')
                ->whereIn('concept_id',$conceptIdArr)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            }

            $conceptIdDetail=array();
            foreach($conceptdataField as $conceptdataFieldKey=>$conceptdataFieldValue){
                $conceptIdDetail[$conceptdataFieldValue->concept_id]=$conceptdataFieldValue;
            }
            
            $licensedataField=array();
            if(!empty($licensesIdArr))
             {
                $licenseFlag = 1;
                $licensedataField = DB::table('licenses')
                ->whereIn('license_id',$licensesIdArr)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            }

            $licenseIdDetail=array();
            foreach($licensedataField as $licensedataFieldKey=>$licensedataFieldValue){
                $licenseIdDetail[$licensedataFieldValue->license_id]=$licensedataFieldValue;
            }
            
            $languageArrdataField=array();
            if(!empty($languageIdArr))
            {
                $languageFlag = 1;
                $languageArrdataField = DB::table('languages')
                ->whereIn('language_id',$languageIdArr)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            }

            $languageIdDetail=array();
            foreach($languageArrdataField as $languageArrdataFieldKey=>$languageArrdataFieldValue){
                $languageIdDetail[$languageArrdataFieldValue->language_id]=$languageArrdataFieldValue;
            }

            $metaDataIdMapped=array();
            foreach($nodeIDwithCustomMetadataId as $nodeIDwithCustomMetadataIdKey => $nodeIDwithCustomMetadataIdValue){
                foreach($nodeIDwithCustomMetadataIdValue as $nodeIDwithCustomMetadataIdValueData){
                    $metaDataIdMapped[]=$nodeIDwithCustomMetadataIdValueData;
                }
            }

            $itemIdFlag=0;
            $itemIdArrdataField=array();
            if(!empty($metaDataIdMapped))
            {
                $itemIdFlag = 1;
                $itemIdArrdataField = DB::table('item_metadata')
                ->whereIn('metadata_id',$metaDataIdMapped)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            }
            
            $customIdDetail=array();
            $customItemIdData=array();
            foreach($itemIdArrdataField as $itemIdArrdataFieldKey=>$itemIdArrdataFieldValue){
             
               $customIdDetail[$itemIdArrdataFieldValue->item_id][$itemIdArrdataFieldValue->metadata_id]=$itemIdArrdataFieldValue;
               $customItemIdData[]=$itemIdArrdataFieldValue->item_id;
            }
           
        $nodeTYpeCount = array();
        
        foreach($dataField as $dataFieldKey=>$dataFieldValue){
            $nodeTypeFlag = 0;

            foreach($nodeIdWithMetadataId as $nodeIdWithMetadataIdKey=> $nodeIdWithMetadataIdV)
            {
                if($nodeIdWithMetadataIdKey == $dataFieldValue->node_type_id)
                {
                    $dataFieldValuearray = json_decode(json_encode($dataFieldValue), True);
                    
                    /** Mandatory fields calculation start */
                    if(!empty($nodeIDwithMetadataName))
                    {
                        foreach($nodeIDwithMetadataName as $nodeIDwithMetadataNameK=>$nodeIDwithMetadataNameV)
                        {   
                            foreach($nodeIDwithMetadataNameV as $nodeIDwithMetadataNameKey=>$nodeIDwithMetadataNameValue)
                            {
                                
                                if(!empty($nodeIDwithMetadataNameValue))
                                {
                                    if($dataFieldValuearray[$nodeIDwithMetadataNameValue] == "" && $dataFieldValuearray["node_type_id"] == $nodeIDwithMetadataNameK)
                                    {
                                       $item_id = $dataFieldValuearray['item_id'];
                                       $fieldMetadataArr[$item_id][$nodeIDwithMetadataNameK][$nodeIDwithMetadataNameValue] = 1;
                                    }
                                    else
                                    {
                                        $item_id = $dataFieldValuearray['item_id'];

                                    }
                                }
                            }
                           
                        }
                    }
                    /** Mandatory fields calculation end */

                    /** Concept data calculation start */
                    if(count($nodeIDwithConceptName[$dataFieldValuearray["node_type_id"]])>0 && $dataFieldValuearray["concept_id"] != "" )
                    {
                        $conceptNameArr = $nodeIDwithConceptName[$dataFieldValuearray["node_type_id"]];
                        foreach($conceptIdDetail[$dataFieldValuearray["concept_id"]] as $conceptIdDetailK=>$conceptIdDetailV)
                        {
                            $ArrconceptArrDetail[$dataFieldValuearray["concept_id"]][$conceptIdDetailK] = $conceptIdDetailV;
                        }
                       
                        foreach($conceptNameArr as $conceptNameArrK=>$conceptNameArrV)
                        {
                           if($ArrconceptArrDetail[$dataFieldValuearray["concept_id"]][$conceptNameArrV] == "")
                            {
                                $item_id = $dataFieldValuearray['item_id'];
                                $concept_name = "concept_".$conceptNameArrV;
                                $fieldMetadataArr[$item_id][$dataFieldValuearray["node_type_id"]][$concept_name] = 1;
                            }
                            
                        }
                    }
                    /** Concept data calculation end */

                    /** License data calculation start */
                    if(count($nodeIDwithLicenseName[$dataFieldValuearray["node_type_id"]])>0 && $dataFieldValuearray["license_id"] != "" )
                    {
                        $licenseNameArr = $nodeIDwithLicenseName[$dataFieldValuearray["node_type_id"]];
                        foreach($licenseIdDetail[$dataFieldValuearray["license_id"]] as $licenseIdDetailK=>$licenseIdDetailV)
                        {
                            $ArrlicenseArrDetail[$dataFieldValuearray["license_id"]][$licenseIdDetailK] = $licenseIdDetailV;
                        }
                            
                        foreach($licenseNameArr as $licenseNameArrK=>$licenseNameArrV)
                        {   
                            if($licenseNameArrV=="text")
                            {
                                $licenseNameArrV="license_text";
                            }
                            if($ArrlicenseArrDetail[$dataFieldValuearray["license_id"]][$licenseNameArrV] == "")
                            {
                                $item_id = $dataFieldValuearray['item_id'];
                                $license_name = "license_".$licenseNameArrV;
                                $fieldMetadataArr[$item_id][$dataFieldValuearray["node_type_id"]][$license_name] = 1;
                            }
                        }

                    }
                    /** License data calculation end */

                    /** Language data calculation start */
                    if(count($nodeIDwithLanguageName[$dataFieldValuearray["node_type_id"]])>0 && $dataFieldValuearray["language_id"] != "" )
                    {
                        $languageNameArr = $nodeIDwithLanguageName[$dataFieldValuearray["node_type_id"]];
                        foreach($languageIdDetail[$dataFieldValuearray["language_id"]] as $languageIdDetailK=>$languageIdDetailV)
                        {
                            $ArrlanguageArrDetail[$dataFieldValuearray["language_id"]][$languageIdDetailK] = $languageIdDetailV;
                        }
                            
                        foreach($languageNameArr as $languageNameArrK=>$languageNameArrV)
                        {
                            if($ArrlanguageArrDetail[$dataFieldValuearray["language_id"]][$languageNameArrV] == "")
                            {
                                $item_id = $dataFieldValuearray['item_id'];
                                $language_name = "language_".$languageNameArrV;
                                $fieldMetadataArr[$item_id][$dataFieldValuearray["node_type_id"]][$language_name] = 1;
                            }
                        }
                    }
                    /** Language data calculation end */

                    /** Custom Metadata calculation start*/
                   
                    if(isset($nodeIDwithCustomNameMetadataId[$dataFieldValuearray["node_type_id"]]))
                    {   
                      
                        $customNameArr = $nodeIDwithCustomNameMetadataId[$dataFieldValuearray["node_type_id"]];
                        
                        if(isset($customIdDetail[$dataFieldValuearray["item_id"]]))
                        {
                            foreach($customIdDetail[$dataFieldValuearray["item_id"]] as $customIdDetailK=>$customIdDetailV)
                            {
                                $ArrcustomArrDetail[$dataFieldValuearray["item_id"]][$customIdDetailK] = $customIdDetailV;
                            }
                        }
                       
                        foreach($customNameArr as $customNameArrK=>$customNameArrV)
                        {   

                            $custMetaId = (array_keys($customNameArrV));
                            $custMetaValues = (array_values($customNameArrV));
                            
                            if(isset($ArrcustomArrDetail[$dataFieldValuearray["item_id"]]))
                            {
                                $ItemcustomValue = json_decode(json_encode($ArrcustomArrDetail[$dataFieldValuearray["item_id"]]), True);
                            }
                           
                          if(isset($ItemcustomValue[$custMetaId[0]]["metadata_value"]))
                          {
                            if($ItemcustomValue[$custMetaId[0]]["metadata_value"] == "")
                            {
    
                                $item_id = $dataFieldValuearray['item_id'];
                                $fieldMetadataArr[$item_id][$dataFieldValuearray["node_type_id"]][$custMetaValues[0]] = 1;
                              
                            }
                          }
                         else
                        {
                                $item_id = $dataFieldValuearray['item_id'];
                                $fieldMetadataArr[$item_id][$dataFieldValuearray["node_type_id"]][$custMetaValues[0]] = 1;
                        }
                          
                            unset($ItemcustomValue);
                            unset($custMetaValues);
                            unset($custMetaId);
                        }
                    }
                    /** Custom Metadata calculation end*/
                }
                
            }
           
        }

       
        
        // calculation for document node coverage start
        //if document identifier else project identifier
        if($titleCreatorFlag==1)
        {
            if(!empty($documentIdentifier["document_id"]))
            {
                $documentDataField = DB::table('documents')
                        ->where('document_id',$documentIdentifier["document_id"])
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
            }
            else
            {
                $documentDataField = DB::table('documents')
                        ->where('document_id',$getProjectRelatedDocumentId)
                        ->where('is_deleted','0')
                        ->get()
                        ->toArray();
            }
            $documentNodeId=array();
            foreach($documentDataField as $documentDataFieldKey=>$documentDataFieldValue)
            {
                $documentNodeId[]=$documentDataFieldValue->node_type_id;
            }
            
            $documentNodeIdMappedWithMedataId = DB::table('node_type_metadata')
                        ->rightjoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
                        ->select('node_type_metadata.node_type_id','node_type_metadata.metadata_id','metadata.is_custom','metadata.name','metadata.internal_name')
                        ->whereIn('node_type_metadata.node_type_id',$documentNodeId)
                        ->where('node_type_metadata.is_mandatory','1')
                        ->get()
                        ->toArray();
            
            $nodeIdwithMetadataIdDocument = array();
            $nodeIDwithMetadataNameDocument = array();
            $nodeIDwithSubjectNameDocument = array();
            $nodeIDwithLicenseNameDocument = array();
            $nodeIDwithLanguageNameDocument = array();

            foreach($documentNodeId as $documentNodeIdKey=>$documentNodeIdValue)
            {
                $metanameArrayDocument=array();
                $metaIdArrayDocument=array();
                $subjectArrayDocument=array();
                $licenseArrayDocument=array();
                $languageArrayDocument=array();
                $customArrayDocument=array();
                $customNameArrayDocument=array();

                foreach($documentNodeIdMappedWithMedataId as $documentNodeIdMappedWithMedataIdKey=>$documentNodeIdMappedWithMedataIdValue)
                {
                    if($documentNodeIdMappedWithMedataIdValue->node_type_id == $documentNodeIdValue && $documentNodeIdMappedWithMedataIdValue->is_custom == 0 && $documentNodeIdMappedWithMedataIdValue->internal_name != "creator" && $documentNodeIdMappedWithMedataIdValue->internal_name != "title")
                    {
                        $internal_name = $documentNodeIdMappedWithMedataIdValue->internal_name;
                        if (in_array($internal_name, $license_data))
                        {
                            $name = str_replace("license_","",$internal_name);
                         array_push($licenseArrayDocument,''.$name);
                        }
                        else if (in_array($internal_name, $subject_data))
                        {
                            $name = str_replace("subject_","",$internal_name);
                            array_push($subjectArrayDocument,$name);
                        }
                        else if (in_array($internal_name, $language_data))
                        {
                            $name = "name";
                            array_push($languageArrayDocument,$name);
                        }
                        else
                        {
                            array_push($metanameArrayDocument,$internal_name);
                        }
                       
                        array_push($metaIdArrayDocument,$documentNodeIdMappedWithMedataIdValue->metadata_id);
                    }
                    else if($documentNodeIdMappedWithMedataIdValue->node_type_id == $documentNodeIdValue && $documentNodeIdMappedWithMedataIdValue->is_custom == 1 && $documentNodeIdMappedWithMedataIdValue->internal_name != "creator" && $documentNodeIdMappedWithMedataIdValue->internal_name != "title")
                    {   
                        array_push($customNameArrayDocument,$documentNodeIdMappedWithMedataIdValue->name);
                        array_push($customArrayDocument,$documentNodeIdMappedWithMedataIdValue->metadata_id);
                        $customIdNameArrayDocument[$documentNodeIdMappedWithMedataIdValue->metadata_id] = $documentNodeIdMappedWithMedataIdValue->name;
                    }
                }

                $nodeIdwithMetadataIdDocument[$documentNodeIdValue] = $metaIdArrayDocument;
                $nodeIDwithMetadataNameDocument[$documentNodeIdValue] = $metanameArrayDocument;
                $nodeIDwithSubjectNameDocument[$documentNodeIdValue] = $subjectArrayDocument;
                $nodeIDwithLicenseNameDocument[$documentNodeIdValue] = $licenseArrayDocument;
                $nodeIDwithLanguageNameDocument[$documentNodeIdValue] = $languageArrayDocument;
                $nodeIDwithCustomNameDocument[$documentNodeIdValue] = $customNameArrayDocument;
                $nodeIDwithCustomMetadataIdDocument[$documentNodeIdValue] = $customArrayDocument;
                if(isset($customIdNameArrayDocument))
                {
                    $nodeIDwithCustomNameMetadataIdDocument[$documentNodeIdValue] = $customIdNameArrayDocument;
                }
                
                
                /*
                unset($metaIdArray);
                unset($metanameArray);
                unset($conceptArray);
                unset($licenseArray);
                unset($languageArray);
                unset($customNameArray);
                unset($customArray);
                */
            }

              $subjectIdArrDocument = array();
              $licensesIdArrDocument = array();
              $languageIdArrDocument = array();
            foreach($documentDataField as $documentDataFieldKey=>$documentDataFieldValue)
            {
                if($documentDataFieldValue->license_id != "")
                {
                    array_push($licensesIdArrDocument,$documentDataFieldValue->license_id);
                }
                if($documentDataFieldValue->language_id != "")
                {
                    array_push($languageIdArrDocument,$documentDataFieldValue->language_id);
                }
            }

            $subjectFlagDocument = 0;
            $licenseFlagDocument = 0;
            $languageFlagDocument = 0;

            $licensedataFieldDocument=array();
            if(!empty($licensesIdArrDocument))
             {
                $licenseFlagDocument = 1;
                $licensedataFieldDocument = DB::table('licenses')
                ->whereIn('license_id',$licensesIdArrDocument)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            }
            
            $licenseIdDetailDocument=array();
            foreach($licensedataFieldDocument as $licensedataFieldDocumentKey=>$licensedataFieldDocumentValue){
                $licenseIdDetailDocument[$licensedataFieldDocumentValue->license_id]=$licensedataFieldDocumentValue;
            }
            
            $languageArrdataFieldDocument=array();
            if(!empty($languageIdArrDocument))
            {
                $languageFlagDocument = 1;
                $languageArrdataFieldDocument = DB::table('languages')
                ->whereIn('language_id',$languageIdArrDocument)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            }

            $languageIdDetailDocument=array();
            foreach($languageArrdataFieldDocument as $languageArrdataFieldDocumentKey=>$languageArrdataFieldDocumentValue){
                $languageIdDetailDocument[$languageArrdataFieldDocumentValue->language_id]=$languageArrdataFieldDocumentValue;
            }

            if(!empty($documentIdentifier["document_id"]))
            {
                $getSubjectId = DB::table('document_subject')
                        ->select('subject_id')
                        ->where('document_id',$documentIdentifier["document_id"])
                        ->get()
                        ->toArray();
            }
            else
            {
                $getSubjectId = DB::table('document_subject')
                        ->select('subject_id')
                        ->where('document_id',$getProjectRelatedDocumentId)
                        ->get()
                        ->toArray();
            }

            $subjectId=array();
            foreach($getSubjectId as $getSubjectIdKey=>$getSubjectIdValue)
            {
                $subjectId['subject_id']=$getSubjectIdValue->subject_id;
            }

            $subjectArrdataFieldDocument=array();
            if(!empty($subjectId['subject_id']))
            {
                $subjectArrdataFieldDocument = DB::table('subjects')
                        ->where('subject_id',$subjectId)
                        ->get()
                        ->toArray();
            }
            
            $subjectIdDetailDocument=array();
            foreach($subjectArrdataFieldDocument as $subjectArrdataFieldDocumentKey=>$subjectArrdataFieldDocumentValue){
                $subjectIdDetailDocument[$subjectArrdataFieldDocumentValue->subject_id]=$subjectArrdataFieldDocumentValue;
            }
            
            $metaDataIdMappedDocument=array();
            foreach($nodeIDwithCustomMetadataIdDocument as $nodeIDwithCustomMetadataIdDocumentKey => $nodeIDwithCustomMetadataIdDocumentValue){
                foreach($nodeIDwithCustomMetadataIdDocumentValue as $nodeIDwithCustomMetadataIdDocumentValueData){
                    $metaDataIdMappedDocument[]=$nodeIDwithCustomMetadataIdDocumentValueData;
                }
            }

            $itemIdFlagDocument=0;
            $itemIdArrdataFieldDocument=array();
            if(!empty($metaDataIdMappedDocument))
            {
                $itemIdFlagDocument = 1;
                if(!empty($documentIdentifier["document_id"]))
                {
                    $itemIdArrdataFieldDocument = DB::table('document_metadata')
                    ->whereIn('metadata_id',$metaDataIdMappedDocument)
                    ->where('document_id',$documentIdentifier["document_id"])
                    // ->where('is_deleted','0')
                    ->get()
                    ->toArray();
                }
                else
                {
                    $itemIdArrdataFieldDocument = DB::table('document_metadata')
                    ->whereIn('metadata_id',$metaDataIdMappedDocument)
                    ->where('document_id',$getProjectRelatedDocumentId)
                    // ->where('is_deleted','0')
                    ->get()
                    ->toArray();
                }
            }
            
            $customIdDetailDocument=array();
            $customItemIdDataDocument=array();
            
            foreach($itemIdArrdataFieldDocument as $itemIdArrdataFieldDocumentKey=>$itemIdArrdataFieldDocumentValue){
               $customIdDetailDocument[]=$itemIdArrdataFieldDocumentValue;
               $customItemIdDataDocument[$itemIdArrdataFieldDocumentValue->document_id]=$itemIdArrdataFieldDocumentValue;
               $i++;
            }
            
            foreach($documentDataField as $documentDataFieldKey=>$documentDataFieldValue)
            {
                foreach($nodeIdwithMetadataIdDocument as $nodeIdwithMetadataIdDocumentKey=>$nodeIdwithMetadataIdDocumentValue)
                {
                    if($nodeIdwithMetadataIdDocumentKey == $documentDataFieldValue->node_type_id)
                        {
                            $dataFieldValuearrayDocument = json_decode(json_encode($documentDataFieldValue), True);

                            /** Mandatory fields calculation start */
                                if(!empty($nodeIDwithMetadataNameDocument))
                                {
                                    foreach($nodeIDwithMetadataNameDocument as $nodeIDwithMetadataNameDocumentK=>$nodeIDwithMetadataNameDocumentV)
                                    {   
                                        foreach($nodeIDwithMetadataNameDocumentV as $nodeIDwithMetadataNameDocumentKey=>$nodeIDwithMetadataNameDocumentValue)
                                        {
                                            if(!empty($nodeIDwithMetadataNameDocumentValue))
                                            {    
                                                if($nodeIDwithMetadataNameDocumentValue == "status_start_date" || $nodeIDwithMetadataNameDocumentValue == "status_end_date")
                                                {
                                                    if($dataFieldValuearrayDocument[$nodeIDwithMetadataNameDocumentValue] == "0000-00-00 00:00:00" && $dataFieldValuearrayDocument["node_type_id"] == $nodeIDwithMetadataNameDocumentK)
                                                    {   
                                                        $document_id = $dataFieldValuearrayDocument['document_id'];
                                                        $fieldMetadataArrDocument[$document_id][$nodeIDwithMetadataNameDocumentK][$nodeIDwithMetadataNameDocumentValue] = 1;
                                                    }
                                                    else
                                                    {
                                                        $document_id = $dataFieldValuearrayDocument['document_id'];
                                                    }
                                                }
                                                else
                                                {
                                                    if($dataFieldValuearrayDocument[$nodeIDwithMetadataNameDocumentValue] == "" && $dataFieldValuearrayDocument["node_type_id"] == $nodeIDwithMetadataNameDocumentK)
                                                    {   
                                                        $document_id = $dataFieldValuearrayDocument['document_id'];
                                                        $fieldMetadataArrDocument[$document_id][$nodeIDwithMetadataNameDocumentK][$nodeIDwithMetadataNameDocumentValue] = 1;
                                                    }
                                                    else
                                                    {
                                                        $document_id = $dataFieldValuearrayDocument['document_id'];
                                                    }
                                                }
                                                
                                            }
                                        }
                                    
                                    }
                                }
                            /** Mandatory fields calculation end */

                            /** License data calculation start */
                                if(count($nodeIDwithLicenseNameDocument[$dataFieldValuearrayDocument["node_type_id"]])>0 && $dataFieldValuearrayDocument["license_id"] != "" )
                                {
                                    $licenseNameArrDocument = $nodeIDwithLicenseNameDocument[$dataFieldValuearrayDocument["node_type_id"]];
                                    foreach($licenseIdDetailDocument[$dataFieldValuearrayDocument["license_id"]] as $licenseIdDetailDocumentK=>$licenseIdDetailDocumentV)
                                    {
                                        $ArrlicenseArrDetailDocument[$dataFieldValuearrayDocument["license_id"]][$licenseIdDetailDocumentK] = $licenseIdDetailDocumentV;
                                    }
                                        
                                    foreach($licenseNameArrDocument as $licenseNameArrDocumentK=>$licenseNameArrDocumentV)
                                    {   
                                        if($licenseNameArrDocumentV=="text")
                                        {
                                            $licenseNameArrDocumentV="license_text";
                                        }
                                        if($ArrlicenseArrDetailDocument[$dataFieldValuearrayDocument["license_id"]][$licenseNameArrDocumentV] == "")
                                        {
                                            $document_id = $dataFieldValuearrayDocument['document_id'];
                                            $license_nameDocument = "license_".$licenseNameArrDocumentV;
                                            $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$license_nameDocument] = 1;
                                        }
                                    }

                                }
                                else
                                {  
                                    $licenseNameArrDocument = $nodeIDwithLicenseNameDocument[$dataFieldValuearrayDocument["node_type_id"]];
                                    foreach($licenseNameArrDocument as $licenseNameArrDocumentK=>$licenseNameArrDocumentV)
                                    {   
                                        if($licenseNameArrDocumentV=="text")
                                        {
                                            $licenseNameArrDocumentV="license_text";
                                        }
                                        
                                            $document_id = $dataFieldValuearrayDocument['document_id'];
                                            $license_nameDocument = "license_".$licenseNameArrDocumentV;
                                            $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$license_nameDocument] = 1;
                                     
                                    }
                                }
                            /** License data calculation end */

                            /** Language data calculation start */
                                if(count($nodeIDwithLanguageNameDocument[$dataFieldValuearrayDocument["node_type_id"]])>0 && $dataFieldValuearrayDocument["language_id"] != "" )
                                {
                                    $languageNameArrDocument = $nodeIDwithLanguageNameDocument[$dataFieldValuearrayDocument["node_type_id"]];
                                    foreach($languageIdDetailDocument[$dataFieldValuearrayDocument["language_id"]] as $languageIdDetailDocumentK=>$languageIdDetailDocumentV)
                                    {
                                        $ArrlanguageArrDetailDocument[$dataFieldValuearrayDocument["language_id"]][$languageIdDetailDocumentK] = $languageIdDetailDocumentV;
                                    }
                                        
                                    foreach($languageNameArrDocument as $languageNameArrDocumentK=>$languageNameArrDocumentV)
                                    {
                                        if($ArrlanguageArrDetailDocument[$dataFieldValuearrayDocument["language_id"]][$languageNameArrDocumentV] == "")
                                        {
                                            $document_id = $dataFieldValuearrayDocument['document_id'];
                                            $language_nameDocument = "language_".$languageNameArrDocumentV;
                                            $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$language_nameDocument] = 1;
                                        }
                                    }
                                }
                            /** Language data calculation end */

                            /** Subject Calculation start */
                                if(count($subjectArrayDocument)>0)
                                {
                                    if(count($subjectIdDetailDocument) > 0)
                                    {
                                        foreach($subjectArrayDocument as $subjectArrayDocumentKey=>$subjectArrayDocumentValue)
                                        {
                                            foreach($subjectIdDetailDocument as $subjectIdDetailDocumentKey=>$subjectIdDetailDocumentValue)
                                            {
                                                foreach($subjectIdDetailDocumentValue as $subjectIdDetailDocumentValueK=>$subjectIdDetailDocumentValueData)
                                                {   
                                                    if($subjectArrayDocumentValue==$subjectIdDetailDocumentValueK && $subjectIdDetailDocumentValueData=="")
                                                    {   
                                                        $document_id = $dataFieldValuearrayDocument['document_id'];
                                                        $subject_nameDocument = "subject_".$subjectArrayDocumentValue;
                                                        $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$subject_nameDocument] = 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach($subjectArrayDocument as $subjectArrayDocumentKey=>$subjectArrayDocumentValue)
                                        {
                                            $document_id = $dataFieldValuearrayDocument['document_id'];
                                            $subject_nameDocument = "subject_".$subjectArrayDocumentValue;
                                            $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$subject_nameDocument] = 1;

                                        }
                                    }
                                   
                                }
                            /** Subject Calculation end */
                              //  print_r($nodeIDwithCustomNameMetadataIdDocument);
                            /** Custom Metadata calculation start*/
                                $ArrcustomArrDetailDocument=array();
                            if(count($nodeIDwithCustomNameDocument[$dataFieldValuearrayDocument["node_type_id"]])>0)
                                {   
                                    //$customNameArrDocument = $nodeIDwithCustomNameDocument[$dataFieldValuearrayDocument["node_type_id"]];
                                    $customNameArrDocument = $nodeIDwithCustomNameMetadataIdDocument[$dataFieldValuearrayDocument["node_type_id"]];
                                
                                    foreach($customIdDetailDocument as $customIdDetailDocumentK=>$customIdDetailDocumentV)
                                    {   
                                        $ArrcustomArrDetailDocument[$dataFieldValuearrayDocument["document_id"]][$customIdDetailDocumentK] = $customIdDetailDocumentV;
                                    }

                                    $inc = 0;
                                    if(!empty($customNameArrDocument))
                                    {
                                        foreach($customNameArrDocument as $customNameArrDocumentK=>$customNameArrDocumentV)
                                        {  
                                           
                                            $documentcustomValueDetail=array();
                                            if(isset($ArrcustomArrDetailDocument[$dataFieldValuearrayDocument["document_id"]]))
                                            {
                                                $documentcustomValue = json_decode(json_encode($ArrcustomArrDetailDocument[$dataFieldValuearrayDocument["document_id"]]), True);
                                                foreach($documentcustomValue as $documentcustomValueK=>$documentcustomValueV){
                                                    $documentcustomValueDetail[$documentcustomValueV['metadata_id']]=$documentcustomValueV;
                                                }
                                            }
                                             
                                            if(isset($documentcustomValueDetail[$customNameArrDocumentK]['metadata_value']))
                                            {
                                                
                                                if($documentcustomValueDetail[$customNameArrDocumentK]['metadata_value'] == "")
                                                {   
                                                    $document_id = $dataFieldValuearrayDocument['document_id'];
                                                    $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$customNameArrDocumentV] = 1;
                                                }
                                            }
                                            else
                                            {
                                               
                                                $document_id = $dataFieldValuearrayDocument['document_id'];
                                                $fieldMetadataArrDocument[$document_id][$dataFieldValuearrayDocument["node_type_id"]][$customNameArrDocumentV] = 1;
                                         
                                            }
                                            
                                            $inc = $inc + 1;
                                            unset($documentcustomValueDetail);
                                        }
                                    }
                                }
                            
                            /** Custom Metadata calculation end*/

                        }
                }
            }
            
            $documentEmptyFields=array();
            if(!empty($fieldMetadataArrDocument))
            {
                foreach($fieldMetadataArrDocument as $fieldMetadataArrDocumentKey=>$fieldMetadataArrDocumentValue)
                {
                    foreach($fieldMetadataArrDocumentValue as $documentDataKey=>$documentDataValue)
                    {
                        $documentEmptyFields=$documentDataValue;
                    }
                }
            }

        }
        // calculation for document node coverage end

        

        //main calculation starts
        if(!empty($documentDataField))
        {   
            if($titleCreatorFlag==1)
            {
                $totalCoverageNodes=count($dataField)+1;        // +1 Added for Coverage Document Node
            }
            else
            {
                $totalCoverageNodes=count($dataField)+0;
            }    
        }
        else
        {
            $totalCoverageNodes=count($dataField);
        }
        
        $coverageEmptyNodes=0;
        $coverageEmptyFieldsCount=array();
        $coverageEmptyNodeCount=array();
        
        if(!empty($fieldMetadataArr)){
            $coverageEmptyNodes=count($fieldMetadataArr);
            $coverageEmptyFields=array();
            $coverageEmptyNodeId=array();
            foreach($fieldMetadataArr as $fieldMetadataArrKey=>$fieldMetadataArrValue){
                foreach ($fieldMetadataArrValue as $dataKey=>$dataValue){
                        $coverageEmptyNodeId[]=$dataKey;
                    foreach($dataValue as $emptyFieldsKey=>$emptyFieldsValue){
                        $coverageEmptyFields[]=$emptyFieldsKey;
                    }
                }
            }
            
            $coverageEmptyFieldsCount=array_count_values($coverageEmptyFields);
            $coverageEmptyNodeIdCount=array_count_values($coverageEmptyNodeId);
            
            foreach($coverageEmptyNodeIdCount as $coverageEmptyNodeIdCountK=>$coverageEmptyNodeIdCountV){
                foreach($nodeName as $nodeNameK=>$nodeNameV){
                    if($coverageEmptyNodeIdCountK==$nodeNameV->node_type_id){
                        $coverageEmptyNodeCount[$nodeNameV->title]=$coverageEmptyNodeIdCountV;
                    }
                }
            }
        }

            if(!empty($coverageEmptyFieldsCount))
            {
                $coverage_cnt = count($coverageEmptyFieldsCount);
            }
            
            $document_cnt=0;
            if(!empty($documentEmptyFields))
            {
                $document_cnt = count($documentEmptyFields);
            }
            
            if(($document_cnt > 0))
            {
                $finalArr = array();
                foreach($coverageEmptyFieldsCount as $coverageEmptyFieldsCountK=>$coverageEmptyFieldsCountV)
                {
                    foreach($documentEmptyFields as $documentEmptyFieldsK=>$documentEmptyFieldsV)
                    {
                        if($coverageEmptyFieldsCountK == $documentEmptyFieldsK)
                        {   
                            
                            if(empty($finalArr[$coverageEmptyFieldsCountK]) || !isset($finalArr[$coverageEmptyFieldsCountK]))
                            {
                                $finalArr[$coverageEmptyFieldsCountK] = $coverageEmptyFieldsCountV + $documentEmptyFieldsV;
                            }
                            else
                            {
                                $sum = $coverageEmptyFieldsCountV + $documentEmptyFieldsV;
                                if($finalArr[$coverageEmptyFieldsCountK] < $sum)
                                {
                                    $finalArr[$coverageEmptyFieldsCountK] = $sum;
                                }
                            }
                           
                        }
                        else
                        {
                            if(empty($finalArr[$coverageEmptyFieldsCountK]) || !isset($finalArr[$coverageEmptyFieldsCountK]))
                            {
                                $finalArr[$coverageEmptyFieldsCountK] = $coverageEmptyFieldsCountV;
                            }
                            else
                            {
                                if($finalArr[$coverageEmptyFieldsCountK] < $coverageEmptyFieldsCountV)
                                {
                                    $finalArr[$coverageEmptyFieldsCountK] = $coverageEmptyFieldsCountV;
                                }
                            }
                           
                        } 
                    }
                }
                
                $arrayDiff=array_diff_key($documentEmptyFields,$finalArr);
                $finalArr = $finalArr + $arrayDiff;
        
            }
            else
            {
                $finalArr = $coverageEmptyFieldsCount;
            }
        
        $coverageEmptyFieldsDisplay=array();
        $i=0;
        arsort($finalArr);
        $coverageEmptyFieldsDataTop=array_slice($finalArr,0,10);
        if(!empty($coverageEmptyFieldsDataTop))
        {
            foreach($coverageEmptyFieldsDataTop as $coverageEmptyFieldsCountDataK=>$coverageEmptyFieldsCountDataV){
                    $coverageEmptyFieldsDisplay[$i]['key']=$coverageEmptyFieldsCountDataK;
                    $coverageEmptyFieldsDisplay[$i]['value']=$coverageEmptyFieldsCountDataV;
                    $i++;
            }
        }   
        
        $coverageEmptyNodesDisplay=array();
        $l=0;
        arsort($coverageEmptyNodeCount);
        $coverageEmptyNodeDataTop=array_slice($coverageEmptyNodeCount,0,10);
        if(!empty($coverageEmptyNodeDataTop))
        {
            foreach($coverageEmptyNodeDataTop as $coverageEmptyNodeCountDataK=>$coverageEmptyNodeCountDataV){
                $coverageEmptyNodesDisplay[$l]['key']=$coverageEmptyNodeCountDataK;
                $coverageEmptyNodesDisplay[$l]['value']=$coverageEmptyNodeCountDataV;
                $l++;
            }
        }

        if($document_cnt > 0)
            {
                if($titleCreatorFlag==1)
                {
                    $coverageEmptyNodesDisplay[$l]['key']='Document';
                    $coverageEmptyNodesDisplay[$l]['value']='1';
                }
                     
            }

        if($document_cnt > 0)
        {   
            if($titleCreatorFlag==1)
                {
                    $coverageEmptyNodes=$coverageEmptyNodes+1;
                }
                else
                {
                    $coverageEmptyNodes=$coverageEmptyNodes+0;
                }
        }  
            
        $totalCoverageNodesFilled=$totalCoverageNodes-$coverageEmptyNodes;
        $coveragePercentageDisplay=array();
        if(!empty($totalCoverageNodesFilled)){
            $coveragePercentage = ($totalCoverageNodesFilled / $totalCoverageNodes) * 100;
            $coveragePercentageDisplay=round($coveragePercentage,2);
        }
        else{
            $coveragePercentageDisplay='00';
        }


        // TOP SECTION
        // to get empty field name and their respective count end 

        // to get node name and their respective count start
        // BOTTOM SECTION
        
        $nodeCount=count($nodeName);
        $caseNodeTitle=array();
        $customNodeTitle=array();
        // to filter case nodes
        for($i=0;$i<=$nodeCount-1;$i++){
            // if($nodeName[$i]->is_custom==0){
                $caseNodeTitle[$nodeName[$i]->node_type_id]=$nodeName[$i]->title;
            // }
        }
        // to filter custom nodes
        for($i=0;$i<=$nodeCount-1;$i++){
            if($nodeName[$i]->is_custom==null){
                $customNodeTitle[$nodeName[$i]->node_type_id]=$nodeName[$i]->title;
            }
        }
        $itemIDColumn='item_id';
        array_push($columnName,$itemIDColumn);
        $nodeTypeIDColumn='node_type_id';
        array_push($columnName,$nodeTypeIDColumn);
        $additionalColumn=$columnName;

        if(in_array('pacing_guide_item_type',$additionalColumn))
        {
            $additionalColumn=array_flip($additionalColumn);
            unset($additionalColumn['pacing_guide_item_type']);
            $additionalColumn=array_flip($additionalColumn);
        }
        
        if(!array_key_exists('full_statement',$additionalColumn))
        {
            $insertFullStatement=array('full_statement');
            $additionalColumn=array_merge($additionalColumn,$insertFullStatement);
        }
        

        if(!empty($documentIdentifier['document_id'])){
            $nodeData = DB::table('items')
                        ->select($additionalColumn)
                        ->whereIn('document_id',$documentIdentifier)
                        ->whereIn('is_deleted',array(0))
                        ->orderBy('node_type_id')
                        ->get()
                        ->toArray();
        }
        else{
            $nodeData = DB::table('items')
                        ->select($additionalColumn)
                        ->whereIn('item_id',$itemsMapped)
                        ->where('is_deleted','0')
                        ->orderBy('node_type_id')
                        ->get()
                        ->toArray();
        }            
        $getNodeCount = count($nodeData);
        // to get Case Node Name and Count start
        $caseNodeAndCount=array();
        
        $j=0;
        $totalCaseNodeFields=0;
        $totalCaseNodeFieldsEmpty=0;
        $totalCaseNodeFieldsCount=array();
        $checkFullStatement=array('full_statement');
        foreach($caseNodeTitle as $caseNodeKey=>$searchSingleNodeName){
            foreach($checkFullStatement as $singleColumnName){
                for($i=0;$i<=$getNodeCount-1;$i++){
                    if($caseNodeKey==$nodeData[$i]->node_type_id){
                        if($nodeData[$i]->$singleColumnName==""){
                            $caseNodeAndCount[$searchSingleNodeName][$j++]=$nodeData[$i]->item_id;
                        }
                        if(($nodeData[$i]->$singleColumnName=="") || ($nodeData[$i]->$singleColumnName!="")){
                            $totalCaseNodeFieldsCount[$searchSingleNodeName][$j++]=$nodeData[$i]->item_id;
                        }
                            
                    }  
                }
            }
        }
        
        $getUniqueItemForEmptyNode=array();
        foreach($caseNodeAndCount as $caseNodeAndCountKey => $caseNodeAndCountValue){
            $getUniqueItemForEmptyNode[$caseNodeAndCountKey]=array_unique($caseNodeAndCountValue);
        }
        $caseNodeSortedData=array();
        foreach($getUniqueItemForEmptyNode as $caseNodeAndCountKey=>$caseNodeAndCountValue){
            $caseNodeSortedData[$caseNodeAndCountKey]=count($caseNodeAndCountValue);
        }

        $getUniqueItemForTotalNode=array();
        foreach($totalCaseNodeFieldsCount as $totalCaseNodeFieldsCountKey => $totalCaseNodeFieldsCountValue){
            $getUniqueItemForTotalNode[$totalCaseNodeFieldsCountKey]=array_unique($totalCaseNodeFieldsCountValue);
        }
        $getTotalNodeCountData=array();
        foreach($getUniqueItemForTotalNode as $getUniqueItemForTotalNodeKey=>$getUniqueItemForTotalNodeValue){
            $getTotalNodeCountData[$getUniqueItemForTotalNodeKey]=count($getUniqueItemForTotalNodeValue);
        }
        
        if(!empty($complianceConsideredFields)){
            if($titleCreatorFlag==1)
            {
                $totalCaseNodeFieldsEmpty=array_sum($caseNodeSortedData)+1;
            }
            else
            {
                $totalCaseNodeFieldsEmpty=array_sum($caseNodeSortedData);
            }
            
        }
        else{
            $totalCaseNodeFieldsEmpty=array_sum($caseNodeSortedData);
        }
        
        if($titleCreatorFlag==1)
        {
            $totalCaseNodeFields=array_sum($getTotalNodeCountData)+1;          // +1 given for DOCUMENT NODE
        }
        else
        {
            $totalCaseNodeFields=array_sum($getTotalNodeCountData)+0;
        }
        
        arsort($caseNodeSortedData);
        $caseNodeSortedDataTop=array_slice($caseNodeSortedData,0,10);
        $caseNodeSortedKeyValue=array();
        
        if(!empty($caseNodeSortedDataTop)){
            $i=0;
            foreach($caseNodeSortedDataTop as $caseNodeSortedDataTopKey=>$caseNodeSortedDataTopValue){
                $caseNodeSortedKeyValue[$i]['key']=$caseNodeSortedDataTopKey;
                $caseNodeSortedKeyValue[$i]['value']=$caseNodeSortedDataTopValue;
                $i++;
            }
        }
        else{
            $caseNodeSortedKeyValue=array();
        }
        $d=0;
      
        if(!empty($complianceConsideredFields)){
                
                if(empty($caseNodeSortedKeyValue))
                {
                    if($titleCreatorFlag==1)
                    {
                        $caseNodeSortedKeyValue[$d]['key']='Document';
                        $caseNodeSortedKeyValue[$d]['value']='1';
                    }
                }
                else
                {   
                    if($titleCreatorFlag==1)
                    {
                        $caseNodeSortedKeyValue[$i]['key']='Document';
                        $caseNodeSortedKeyValue[$i]['value']='1';
                    }    
                }
                
        }
        // to get Case Node Name and Count end

        // to get Custom Node Name and Count start
        $customFieldToCheck=array('creator','title','full_statement','item_id','node_type_id');
        $customField=array_diff($columnName,$customFieldToCheck);
        $customNodeAndCount=array();
        $totalCustomNodeFieldsCount=array();
        $m=0;
        foreach($caseNodeTitle as $caseNodeKey=>$searchSingleNodeName){
            foreach($customField as $singleColumnName){
                for($i=0;$i<=$getNodeCount-1;$i++){
                    if($caseNodeKey==$nodeData[$i]->node_type_id){
                        if($nodeData[$i]->$singleColumnName==""){
                            $customNodeAndCount[$searchSingleNodeName][$m++]=$nodeData[$i]->item_id;
                        }
                        if(($nodeData[$i]->$singleColumnName=="") || ($nodeData[$i]->$singleColumnName!="")){
                            $totalCustomNodeFieldsCount[$searchSingleNodeName][$m++]=$nodeData[$i]->item_id;
                        }
                            
                    }  
                }
            }
        }
    
        $getUniqueItemForEmptyNodeCustom=array();
        foreach($customNodeAndCount as $customNodeAndCountKey => $customNodeAndCountValue){
            $getUniqueItemForEmptyNodeCustom[$customNodeAndCountKey]=array_unique($customNodeAndCountValue);
        }
        $customNodeSortedData=array();
        foreach($getUniqueItemForEmptyNodeCustom as $customNodeAndCountKey=>$customNodeAndCountValue){
            $customNodeSortedData[$customNodeAndCountKey]=count($customNodeAndCountValue);
        }

        $getUniqueItemForTotalNode=array();
        foreach($totalCustomNodeFieldsCount as $totalCustomNodeFieldsCountKey => $totalCustomNodeFieldsCountValue){
            $getUniqueItemForTotalNode[$totalCustomNodeFieldsCountKey]=array_unique($totalCustomNodeFieldsCountValue);
        }
        $getTotalNodeCountDataCustom=array();
        foreach($getUniqueItemForTotalNode as $getUniqueItemForTotalNodeKey=>$getUniqueItemForTotalNodeValue){
            $getTotalNodeCountDataCustom[$getUniqueItemForTotalNodeKey]=count($getUniqueItemForTotalNodeValue);
        }
        
        $totalCustomNodeFieldsEmpty=array_sum($customNodeSortedData);
        $totalCustomNodeFields=array_sum($getTotalNodeCountDataCustom);
        
        if(!empty($customNodeSortedData)){
            arsort($customNodeSortedData);
            $customNodeSortedDataTop=array_slice($customNodeSortedData,0,10);
            $customNodeSortedKeyValue=array();
            // if(!empty($caseNodeSortedDataTop)){
                $i=0;
                foreach($customNodeSortedDataTop as $customNodeSortedDataTopKey=>$customNodeSortedDataTopValue){
                    $customNodeSortedKeyValue[$i]['key']=$customNodeSortedDataTopKey;
                    $customNodeSortedKeyValue[$i]['value']=$customNodeSortedDataTopValue;
                    $i++;
                }
            // }
            // else{
                // $customNodeSortedKeyValue=array();
            // }
        }
        else{
            $customNodeSortedKeyValue=array();
        }
        // to get Custom Node Name and Count end

        

        // BOTTOM SECTION
        // to get node name and their respective count end

        // to get case percentage bar start
        $totalCaseNodeFieldsFilled=$totalCaseNodeFields-$totalCaseNodeFieldsEmpty;
        $casePercentageDisplay=array();
        if(!empty($totalCaseNodeFieldsFilled)){
            $casePercentage = ($totalCaseNodeFieldsFilled / $totalCaseNodeFields) * 100;
            $casePercentageDisplay=round($casePercentage,2);
        }
        else{
            $casePercentageDisplay='00';
        }
        // to get case percentage bar end

        
        // final data returned start
          $caseComplianceReportData['overall-percentage']=$casePercentageDisplay;
          $caseComplianceReportData['case-filled-nodes']=$totalCaseNodeFieldsFilled;
          $caseComplianceReportData['case-total-nodes']=$totalCaseNodeFields;
          $caseComplianceReportData['fields']=$caseFieldSortedKeyValues;
          $caseComplianceReportData['nodes']=$caseNodeSortedKeyValue;
          
          $customComplianceReportData['overall-percentage']=$coveragePercentageDisplay;
          $customComplianceReportData['custom-filled-nodes'] = $totalCoverageNodesFilled;
          $customComplianceReportData['custom-total-nodes'] =$totalCoverageNodes;
          $customComplianceReportData['fields']=$coverageEmptyFieldsDisplay;
          $customComplianceReportData['nodes']=$coverageEmptyNodesDisplay;
          
          $complianceReportDataDisplay['case-compliance']=$caseComplianceReportData;
          $complianceReportDataDisplay['coverage-compliance']=$customComplianceReportData;  
          return $complianceReport=$complianceReportDataDisplay;
        // final data returned end           
    }
}


