<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

use App\Data\Models\ItemAssociation;

use Illuminate\Support\Facades\DB;

class ItemAssociationEloquentRepository extends EloquentRepository implements ItemAssociationRepositoryInterface
{

    public function getModel()
    {
        return ItemAssociation::class;
    }

    /**
     * This method returns all the associated models of an item association
     */
    public function getItemAssociationAndRelatedModel(string $itemAssociationId){
        $fetchAssociatedModels = [
            "document:document_id,source_document_id,title,node_type_id",
            "sourceItemId:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id",
            "targetItemId:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id"
        ];
        return $this->findBy("item_association_id", $itemAssociationId, $fetchAssociatedModels);
    }

    /**
     * This method returns all the associated models of an item association
     */
    public function getItemAssociationAndRelatedModelBySourceId(string $sourceItemAssociationId){
        $fetchAssociatedModels = [
            "document:document_id,source_document_id,title,node_type_id",
            "sourceItemId:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id",
            "targetItemId:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id",
            "associationGroup:association_group_id,title,source_association_group_id"
        ];
        return $this->findBy("source_item_association_id", $sourceItemAssociationId, $fetchAssociatedModels);
    }

    public function getOriginNodeId(string $associationId){
        $itemAssociationData = DB::table('item_associations')
                ->select('source_item_id')
                ->where('item_association_id',$associationId)
                ->get();
        $originNodeId = json_decode($itemAssociationData, true);    
        return $originNodeId[0]["source_item_id"];        

    }
}