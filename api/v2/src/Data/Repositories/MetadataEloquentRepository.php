<?php

namespace App\Data\Repositories;

use App\Data\Models\Asset;
use App\Data\Models\ItemAssociation;
use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\Metadata;
use App\Data\Models\NodeTypeMetaData;
use App\Data\Models\Item;
use App\Data\Models\Document;
use App\Data\Models\ItemMetadata;
use App\Data\Models\DocumentMetadata;
use App\Data\Models\NodeType;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Support\Facades\Storage;

class MetadataEloquentRepository extends EloquentRepository implements MetadataRepositoryInterface
{
    use UuidHelperTrait;

    public function getModel()
    {
        return Metadata::class;
    }

    private $itemArr;

    public function getMetadataList($request){
        $input = $request->all();
        $isCustom = isset($input['is_custom'])?$input['is_custom']:'';
        $fieldTypeValues = config("default_enum_setting.metadata_field_type");
        $allowedOrderByFields = ['name'];
        $allowedOrderByMethods = ['asc'];

        $limit = 100;
        if ($request->has('limit') && is_int($request->limit)) {
            $limit = $request->limit;
        }

        $offset = 0;
        if ($request->has('offset') && is_int($request->offset)) {
            $offset = $request->offset;
        }
        // Check Metadata custom values
        if($isCustom=='') {
            $isCustomVal = [1,0];
        }else{
            $isCustomVal = array($isCustom);
        }
       // return $this->model->take($limit)->skip($offset)
       return $this->model
        ->when($request->has('sort'), function($query) use ($request, $allowedOrderByFields, $allowedOrderByMethods) {
            list($orderBy, $sorting) = explode(':', $request->sort);
            if (in_array($orderBy, $allowedOrderByFields) && in_array($sorting, $allowedOrderByMethods)) {
                $query->orderBy($orderBy, $sorting);
            }            
        })
        
        ->when($request->has('search'), function($query) use ($request, $fieldTypeValues) {
            $query->where(function ($query) use ($request, $fieldTypeValues) {
                $query->where('name', 'like', "%{$request->search}%");
                $query->orWhere('field_type', array_search(strtoupper($request->search), $fieldTypeValues));
            }); 
        })

        ->where('organization_id', $input['auth_user']['organization_id'])
        ->where('is_deleted',0)
        ->whereIn('is_custom',$isCustomVal)
        ->select('metadata_id','parent_metadata_id','name','internal_name','field_type','field_possible_values', 'order', 'is_custom', 'is_document', 'is_mandatory', 'is_active', 'updated_by')
        ->with([
            'users' => function($query) {
                $query->select('user_id');
                $query->selectRaw('concat_ws(" ", first_name, last_name) as name');
            }
        ])
        ->orderBy('order', 'asc')
        ->get();
    }

    public function getMetadataDetail(string $metadataIdentifier, string $organizationId){

        return $this->model->select('metadata_id', 'name', 'field_type', 'field_possible_values', 'internal_name', 'is_custom', 'is_document', 'is_active', 'updated_by', 'updated_at')
        ->where('organization_id', $organizationId)
        ->where('metadata_id', $metadataIdentifier)
        ->with([
            'users' => function($query) {
                $query->select('user_id');
                $query->selectRaw('concat_ws(" ", first_name, last_name) as name');
            }
        ])

        ->get()->first();
    }

    public function fetchNodeTypeList(string $metadataIdentifier){
        $arrayNodeId = [];
        $tableName = "node_type_metadata";

        $nodeTypeList = DB::table($tableName)->select("node_type_id")->where(array("metadata_id" => $metadataIdentifier))->distinct()->get()->toArray();

        foreach($nodeTypeList as $nodeType){
            array_push($arrayNodeId, $nodeType->node_type_id);
        }
        
        return $arrayNodeId;
        
    }

    public function getFieldPossibleValues(string $internal_name,  string $organizationIdentifier){
        if($internal_name == 'language'){
            $query = DB::table('languages')
            ->select('language_id', 'name','short_code')
            ->where('organization_id', '=', $organizationIdentifier)
            ->where('is_deleted', '=', 0);
            return $query->get();
        }else if($internal_name == 'license'){
            $query = DB::table('licenses')
            ->select('license_id', 'title')
            ->where('organization_id', '=', $organizationIdentifier)
            ->where('is_deleted', '=', 0);
            return $query->get();
        }else if($internal_name == 'concept'){
            $query = DB::table('concepts')
            ->select('concept_id', 'title')
            ->where('organization_id', '=', $organizationIdentifier)
            ->where('is_deleted', '=', 0);
            return $query->get();
        }else if($internal_name == 'subject'){
            $query = DB::table('subjects')
            ->select('subject_id', 'title')
            ->where('organization_id', '=', $organizationIdentifier)
            ->where('is_deleted', '=', 0);
            return $query->get();
        }
       
       // return $collection;
    }

    public function createFieldPossibleValues(array $createData){
        $query = DB::table('languages')->insert($createData);
        return $query;
    }


    public function updateAllFieldPossibleValues(string $organizationIdentifier, array $attributes){
        $query = DB::table('languages')->where('organization_id', $organizationIdentifier)->update($attributes);
        return $query;
    }

    public function updateFieldPossibleValuesById(string $identifier, array $attributes){
        $query = DB::table('languages')->where('language_id', $identifier)->update($attributes);
        return $query;
    }

    public function updateSelectedFieldPossibleValueOnChangingMetadata(string $identifier, string $oldValue, string $newValue){
        $queryToUpdateItemMetadata = DB::table('item_metadata')->where(['metadata_id' => $identifier, 'metadata_value' => $oldValue])->update(['metadata_value'    =>  $newValue, 'metadata_value_html' => $newValue]);// Metadata Value HTML changed on list update as per UF-3503

        $queryToUpdateDocumentMetadata = DB::table('document_metadata')->where(['metadata_id' => $identifier, 'metadata_value' => $oldValue])->update(['metadata_value'    =>  $newValue, 'metadata_value_html' => $newValue]);// Metadata Value HTML changed on list update as per UF-3503
        // code to added to update metadata value for list in case of updation
		$queryToUpdateItemAssociationMetadata = DB::table('item_association_metadata')->where(['metadata_id' => $identifier, 'metadata_value' => $oldValue])->update(['metadata_value'    =>  $newValue]);
        
        return true;
    }

    public function deleteAdditionalMetadata($identifier, $metadataIdentifier, $entityFromDelete) {
        if($entityFromDelete == 'item') {
            $columnValueFilterPairs =   ['item_id' => $identifier, 'metadata_id' => $metadataIdentifier, 'is_additional'=>1];
            return DB::table('item_metadata')->where($columnValueFilterPairs)->delete();
        }
        else {
            $columnValueFilterPairs =   ['document_id' => $identifier, 'metadata_id' => $metadataIdentifier];
            return DB::table('document_metadata')->where($columnValueFilterPairs)->delete();
        }
    }

    public function getCustomMetadataDetailInfo($nodeIdIdentifier)
    {
        $getCustomMetadataId = NodeTypeMetaData::select('metadata_id')
                                ->where(['node_type_id'=>$nodeIdIdentifier])
                                ->get()
                                ->toArray();
        
        return $getCustomMetadataId;                   
    }

    public function deleteCustomMetadataInfo($nodeIdIdentifier,$customMetadataIdIdentifier,$metaDataIdSaved)
    {
        
        $baseMetadata = array();
        foreach($customMetadataIdIdentifier as $customMetadataIdIdentifierK=>$customMetadataIdIdentifierV)
        {
            $baseMetadata[]=$customMetadataIdIdentifierV['metadata_id'];
        }

        $childMetadata = array();
        foreach($metaDataIdSaved as $metaDataIdSavedK=>$metaDataIdSavedV)
        {
            $childMetadata[]=$metaDataIdSavedV['id'];
        }
        
        $diffMetaData = array();
        $diffMetaData = array_diff($baseMetadata,$childMetadata);
        
        $itemMetaDataToBeDeleted=array();
        $itemMetaDataToBeDeleted = DB::table('item_metadata')
            ->select('item_metadata.item_id','item_metadata.metadata_id')
            ->join('items', 'item_metadata.item_id', '=', 'items.item_id')
            ->where('items.node_type_id',$nodeIdIdentifier)
            ->whereIn('item_metadata.metadata_id',$diffMetaData)
            ->where('item_metadata.is_deleted',0)
            ->where('item_metadata.is_additional',0)
            ->get()
            ->toArray();

        $docMetaDataToBeDeleted=array();
        $docMetaDataToBeDeleted = DB::table('document_metadata')
            ->select('document_metadata.document_id','document_metadata.metadata_id')
            ->join('documents', 'document_metadata.document_id', '=', 'documents.document_id')
            ->where('documents.node_type_id',$nodeIdIdentifier)
            ->whereIn('document_metadata.metadata_id',$diffMetaData)
            ->where('document_metadata.is_additional',0)
            ->get()
            ->toArray();    

        if(!empty($itemMetaDataToBeDeleted))
        {
            foreach($itemMetaDataToBeDeleted as $itemMetaDataToBeDeletedK=>$itemMetaDataToBeDeletedV)
            {
                DB::table('item_metadata')->where(['item_id'=>$itemMetaDataToBeDeletedV->item_id,'metadata_id'=>$itemMetaDataToBeDeletedV->metadata_id])->update(['is_deleted' => 1]);
            }
        }

        if(!empty($docMetaDataToBeDeleted))
        {
            foreach($docMetaDataToBeDeleted as $docMetaDataToBeDeletedK=>$docMetaDataToBeDeletedV)
            {
                DB::table('document_metadata')->where(['document_id'=>$docMetaDataToBeDeletedV->document_id,'metadata_id'=>$docMetaDataToBeDeletedV->metadata_id])->delete();
            }
        }


    }

    public function saveCustomMetadata($customMetadata,$importDocumentId,$organizationId,$itemArr=[],$newNodeCreated,$copy)
    {
        /********* ITEM LEVEL DATA HANDLED START **********/
        $this->itemArr=$itemArr;
        $itemCustomMetadata = $customMetadata['ACMTcustomfields']['CFItem'];
        $getItemCustomMetadata = array();
        $getSourceId = array();
        foreach($itemCustomMetadata as $itemCustomMetadataK=>$itemCustomMetadataV)
        {
            if(!empty($itemCustomMetadataV['custom']))
            {

            foreach($itemCustomMetadataV['custom'] as $itemCustomKey=>$itemCustomValue)
            {
                if(!empty($itemCustomValue)) {
                    $itemCustomValue['identifier'] = $itemCustomMetadataV['identifier'];
                    $itemCustomValue['title']=$itemCustomMetadataV['title'];
                    $itemCustomValue['metadata_id'] = "";
                    $itemCustomValue['is_additional'] = "";
                    $getItemCustomMetadata[] = $itemCustomValue;

                    $getSourceId[] = $itemCustomMetadataV['identifier'];
                }
            }
            }
        }

        $getCustomName = array();
        $itemCustomDetail = array();
        $j=0;
        foreach($getItemCustomMetadata as $getItemCustomMetadataK=>$getItemCustomMetadataV)
        {
                $getCustomName[$j]=$getItemCustomMetadataV['name'];
                $itemCustomDetail[]=$getItemCustomMetadataV;
                $j=$j+1;
        }


        $getUniqueCustomName = array();
        $getUniqueCustomName=array_unique($getCustomName);

        $getMetadataData = Metadata::select('metadata_id','name','field_type','field_possible_values','last_field_possible_values')
                                              ->whereIn('name',$getUniqueCustomName)
                                              ->where(['organization_id'=>$organizationId,'is_deleted'=>'0','is_custom'=>'1'])
                                            //   ->groupBy('name')
                                              ->get()
                                              ->toArray();


        // Looping to get already exist metadata start
        $existingMetadata = array();
        foreach($itemCustomDetail as $itemCustomDetailK=>$itemCustomDetailV)
        {
            foreach($getMetadataData as $getMetadataDataK=>$getMetadataDataV)
            {
                if(strtolower($itemCustomDetailV['name'])==strtolower($getMetadataDataV['name']) && $itemCustomDetailV['field_type']==$getMetadataDataV['field_type'])
                {
                    $itemCustomDetailV['created_at'] = date("Y-m-d H:i:s",time());
                    $itemCustomDetailV['updated_at'] = date("Y-m-d H:i:s",time());
                    $itemCustomDetailV['metadata_id'] = $getMetadataDataV['metadata_id'];
                    $itemCustomDetailV['is_additional'] = 0;
                    $existingMetadata[]=$itemCustomDetailV;
                }
            }
        }
        // Looping to get already exist metadata end
        
        $getExistingCustomName = array();
        foreach($existingMetadata as $existingMetadataKeyData=>$existingMetadataKeyValue)
        {
            $getExistingCustomName[]=$existingMetadataKeyValue['name'];
        }
        $getExistingCustomNameUnique = array_unique($getExistingCustomName);
        
        $getFieldTypeMappedWithUniqueCustomName = array();
        $jk=0;
        foreach($getExistingCustomNameUnique as $getExistingCustomNameUniqueK=>$getExistingCustomNameUniqueV)
        {
            foreach($existingMetadata as $existingMetadataInfo=>$existingMetadataData)
            {
                if(strtolower($getExistingCustomNameUniqueV)==strtolower($existingMetadataData['name']))
                {
                    $getFieldTypeMappedWithUniqueCustomName[$existingMetadataData['name']][$jk]=$existingMetadataData['field_type'];
                    $jk = $jk+1;
                }
            }
        }
        
        
        $getUniqueFieldTypeMappedWithUniqueCustomName = array();
        foreach($getFieldTypeMappedWithUniqueCustomName as $getFieldTypeMappedWithUniqueCustomNameK=>$getFieldTypeMappedWithUniqueCustomNameV)
        {
            $getUniqueFieldTypeMappedWithUniqueCustomName[$getFieldTypeMappedWithUniqueCustomNameK] = array_unique($getFieldTypeMappedWithUniqueCustomNameV);
        }

        
        $getAdditionalData = array();
        $getCustomNameAndFieldType = array();
        $rk=0;
        foreach($getItemCustomMetadata as $getItemCustomMetadataK=>$getItemCustomMetadataV)
        {
            if(in_array(strtolower($getItemCustomMetadataV['name']),array_map('strtolower', $getExistingCustomNameUnique)))
            {
                if(in_array($getItemCustomMetadataV['field_type'],$getUniqueFieldTypeMappedWithUniqueCustomName[$getItemCustomMetadataV['name']]))
                {
                    
                }
                else
                {
                    $getAdditionalData[]=$getItemCustomMetadataV;
                    $getCustomNameAndFieldType[$rk]['name']=$getItemCustomMetadataV['name'];
                    $getCustomNameAndFieldType[$rk]['field_type']=$getItemCustomMetadataV['field_type'];
                    $rk=$rk+1;
                }
            }
            else
            {
                $getAdditionalData[]=$getItemCustomMetadataV;
                $getCustomNameAndFieldType[$rk]['name']=$getItemCustomMetadataV['name'];
                $getCustomNameAndFieldType[$rk]['field_type']=$getItemCustomMetadataV['field_type'];
                $rk=$rk+1;
            }
        }
        
        
        // get unique data and remove duplicate data for custom data and custom name and field type start
        $additionalUniqueMetadataArray = array_map("unserialize", array_unique(array_map("serialize", $getAdditionalData)));
        $uniqueCustomNameAndFieldType = array_map("unserialize", array_unique(array_map("serialize", $getCustomNameAndFieldType)));
        // get unique data and remove duplicate data for custom data and custom name and field type end

        // get Additional Metadata Data start
        //create unique identifier for new custom data start
        $createUniqueIdentifierForCustomData = array();
        foreach($uniqueCustomNameAndFieldType as $uniqueCustomNameAndFieldTypeK=>$uniqueCustomNameAndFieldTypeV)
        {
            $uniqueCustomNameAndFieldTypeV['metadata_id'] = $this->createUniversalUniqueIdentifier();
            $createUniqueIdentifierForCustomData[]=$uniqueCustomNameAndFieldTypeV;
        }
        //create unique identifier for new custom data end

        // map newly created unique metadata id identifier with custom data start
        $newAdditionalCustomData = array();
        foreach($additionalUniqueMetadataArray as $additionalUniqueMetadataArrayK=>$additionalUniqueMetadataArrayV)
        {
            foreach($createUniqueIdentifierForCustomData as $createUniqueIdentifierForCustomDataK=>$createUniqueIdentifierForCustomDataV)
            {
                if(strtolower($additionalUniqueMetadataArrayV['name'])==strtolower($createUniqueIdentifierForCustomDataV['name']) && $additionalUniqueMetadataArrayV['field_type']==$createUniqueIdentifierForCustomDataV['field_type'])
                {
                    $additionalUniqueMetadataArrayV['metadata_id'] = $createUniqueIdentifierForCustomDataV['metadata_id'];
                    $additionalUniqueMetadataArrayV['is_additional'] = 1;
                    $additionalUniqueMetadataArrayV['created_at'] = date("Y-m-d H:i:s",time());
                    $additionalUniqueMetadataArrayV['updated_at'] = date("Y-m-d H:i:s",time());
                    $newAdditionalCustomData[] = $additionalUniqueMetadataArrayV;
                }
            }
        }
        // map newly created unique metadata id identifier with custom data end
        
        // get Additional Metadata Data end

        // Adding Existing metadata and new metadata start
        $totalMetadataFormated = array();
        $totalMetadataFormated = array_merge($existingMetadata,$newAdditionalCustomData);
        // Adding Existing metadata and new metadata end

        
        // Formatting data to insert in item_metadata table start
        $itemMetaData = array();
        $dataForFormattingSelectionData = array();
        $k=0;

        foreach($getItemCustomMetadata as $getItemCustomMetadataKey=>$getItemCustomMetadataValue)
        {
                foreach($totalMetadataFormated as $totalMetadataFormatedK=>$totalMetadataFormatedV)
                {
                    if(strtolower($getItemCustomMetadataValue['name'])==strtolower($totalMetadataFormatedV['name']) && $getItemCustomMetadataValue['field_type']==$totalMetadataFormatedV['field_type'] && $getItemCustomMetadataValue['identifier']==$totalMetadataFormatedV['identifier'])
                    {
                        $itemMetaData[$k]['item_id']=$getItemCustomMetadataValue['identifier'];
                        $itemMetaData[$k]['metadata_id']=$totalMetadataFormatedV['metadata_id'];
                        $itemMetaData[$k]['metadata_value']=$totalMetadataFormatedV['metadata_value'];
                        $itemMetaData[$k]['is_additional']=$totalMetadataFormatedV['is_additional'];
                        $k=$k+1;
                    }
                }
        }

        $getUniqueSourceId = array();
        $getUniqueSourceId=array_unique($getSourceId);

        $getItemSourceId = Item::select('source_item_id')
                                              ->where('document_id',$importDocumentId)
                                              ->where(['organization_id'=>$organizationId,'is_deleted'=>'0'])
                                              ->get()
                                              ->toArray();

        $itemSourceIdDate = array();
        foreach($getItemSourceId as $getItemSourceIdK=>$getItemSourceIdV)
        {
            $itemSourceIdDate[]=$getItemSourceIdV['source_item_id'];
        }
        $getUniqueItemSourceId=array_unique($itemSourceIdDate);
        
        if($copy==0)
        {
            $getItemId = Item::select('item_id','source_item_id','node_type_id')
                            ->whereIn('source_item_id',$getUniqueItemSourceId)
                            ->where(['organization_id'=>$organizationId,'is_deleted'=>'0'])
                            ->get()
                            ->toArray();

            $itemMetadataToInsert = array();
            foreach($itemMetaData as $itemMetaDataK=>$itemMetaDataV)
            {
                foreach($getItemId as $getItemIdK=>$getItemIdV)
                {
                    if($itemMetaDataV['item_id']==$getItemIdV['source_item_id'])
                    {
                        $itemMetaDataV['item_id']=$getItemIdV['item_id'];
                        $itemMetadataToInsert[]=$itemMetaDataV;
                    }
                }
            }
                            
        }
        else
        {
            $getItemId = Item::select('item_id','uri','node_type_id','source_item_id')
                            ->where(['document_id'=>$importDocumentId,'organization_id'=>$organizationId,'is_deleted'=>'0'])
                            ->get()
                            ->toArray();

            $itemMetadataToInsert = array();
            foreach($itemMetaData as $itemMetaDataK=>$itemMetaDataV)
            {
                foreach($getItemId as $getItemIdK=>$getItemIdV)
                {
                    $originalSourceId = substr($getItemIdV['uri'], -36);
                    if($itemMetaDataV['item_id']==$originalSourceId)
                    {
                        $itemMetaDataV['item_id']=$getItemIdV['item_id'];
                        $itemMetadataToInsert[]=$itemMetaDataV;
                    }
                }
            }                

        }    

        
        // Formatting data to insert in item_metadata table end

        // Formatting Additional Metadata to insert in metadata table start
        $getSelectionDataToUpdate = array();
        foreach($existingMetadata as $existingMetadataK=>$existingMetadataV)
        {
            foreach($getMetadataData as $getMetadataDataKey=>$getMetadataDataValue)
            {
                if($existingMetadataV['metadata_id']==$getMetadataDataValue['metadata_id'] && $existingMetadataV['field_type']==3)
                {
                    $finalFieldValueArray = array();
                    $existingField = explode(',', $existingMetadataV['field_possible_values']);
                    $metadataField = explode(',', $getMetadataDataValue['field_possible_values']);
                    $finalFieldValueArray=array_unique(array_merge($existingField,$metadataField));
                    $finalFieldValueString=implode(',', $finalFieldValueArray);

                    $existingMetadataV['field_possible_values']=$finalFieldValueString;
                    $existingMetadataV['last_field_possible_values']=$finalFieldValueString;
                    $existingMetadataV['organization_id']=$organizationId;
                    $existingMetadataV['name']=$getMetadataDataValue['name'];
                    $getSelectionDataToUpdate[]=$existingMetadataV;

                }
            }
        }
        // Formatting Additional Metadata to insert in metadata table end

        // Queries to insert data start

        // Insert In item_metadata table start
        
        if(!empty($itemMetadataToInsert))
        {
            ItemMetadata::insert($itemMetadataToInsert);
        }
        // Insert In item_metadata table end

        // Insert in metadata table start
        $dataToInsertInMetadataTable = array();
        foreach($createUniqueIdentifierForCustomData as $createUniqueIdentifierForCustomDataKey=>$createUniqueIdentifierForCustomDataValue)
        {
            foreach($additionalUniqueMetadataArray as $additionalUniqueMetadataArrayKey=>$additionalUniqueMetadataArrayValue)
            {
                if(strtolower($createUniqueIdentifierForCustomDataValue['name'])==strtolower($additionalUniqueMetadataArrayValue['name']) && $createUniqueIdentifierForCustomDataValue['field_type']==$additionalUniqueMetadataArrayValue['field_type'])
                {
                    $createUniqueIdentifierForCustomDataValue['organization_id']=$organizationId;
                    $createUniqueIdentifierForCustomDataValue['field_possible_values']=$additionalUniqueMetadataArrayValue['field_possible_values'];
                    $createUniqueIdentifierForCustomDataValue['last_field_possible_values']=$additionalUniqueMetadataArrayValue['last_field_possible_values'];
                    $createUniqueIdentifierForCustomDataValue['is_custom']=1;
                    $createUniqueIdentifierForCustomDataValue['is_active']=1;
                    $createUniqueIdentifierForCustomDataValue['is_deleted']=0;
                    $createUniqueIdentifierForCustomDataValue['created_at']=date("Y-m-d H:i:s",time());
                    $createUniqueIdentifierForCustomDataValue['updated_at']=date("Y-m-d H:i:s",time());
                    $dataToInsertInMetadataTable[]=$createUniqueIdentifierForCustomDataValue;
                    break;
                }
            }
        }
        
        if(!empty($dataToInsertInMetadataTable))
        {
            Metadata::insert($dataToInsertInMetadataTable);
        }
        // Insert in metadata table end

        // Update metadata table start
        if(!empty($getSelectionDataToUpdate))
        {
            foreach($getSelectionDataToUpdate as $getSelectionDataToUpdateK=>$getSelectionDataToUpdateV)
            {
                Metadata::where(['metadata_id'=>$getSelectionDataToUpdateV['metadata_id'], 'organization_id'=>$getSelectionDataToUpdateV['organization_id']])->update(['field_possible_values' => $getSelectionDataToUpdateV['field_possible_values'], 'last_field_possible_values'=>$getSelectionDataToUpdateV['last_field_possible_values']]);
            }
        }
    
        // Update metadata table end

        // Formatting data to insert in node_type_metadata table start
        if(!empty($newNodeCreated))
        {
            $getNewNodeTypeCreatedName = array();
            $nodeCreatedArray = array();
            $ak=0;
            foreach($newNodeCreated as $newNodeCreatedK=>$newNodeCreated)
            {
                $getNewNodeTypeCreatedName[]=$newNodeCreated['title'];
                $nodeCreatedArray[$ak]['title']=$newNodeCreated['title'];
                $nodeCreatedArray[$ak]['node_type_id']=$newNodeCreated['node_type_id'];
                $ak = $ak+1;
            }
        

        $getUniqueNewNodeTypeCreatedName = array();
        $getUniqueNewNodeTypeCreatedName = array_unique($getNewNodeTypeCreatedName);
            
        $createUniqueIdentifierForEachNode = array();
        $jk=0;
        
        foreach($getUniqueNewNodeTypeCreatedName as $getUniqueNewNodeTypeCreatedNameK=>$getUniqueNewNodeTypeCreatedNameV)
        {
            foreach($nodeCreatedArray as $nodeCreatedArrayK=>$nodeCreatedArrayV)
            {
                if($getUniqueNewNodeTypeCreatedNameV==$nodeCreatedArrayV['title'])
                {
                    $createUniqueIdentifierForEachNode[$jk]['node_type_id'] = $nodeCreatedArrayV['node_type_id'];
                    $createUniqueIdentifierForEachNode[$jk]['title'] = $getUniqueNewNodeTypeCreatedNameV;
                    $createUniqueIdentifierForEachNode[$jk]['type_code'] = $getUniqueNewNodeTypeCreatedNameV;
                    $createUniqueIdentifierForEachNode[$jk]['organization_id'] = $organizationId;
                    $createUniqueIdentifierForEachNode[$jk]['is_deleted'] = '0';
                    $createUniqueIdentifierForEachNode[$jk]['source_node_type_id'] = $nodeCreatedArrayV['node_type_id'];
                    $createUniqueIdentifierForEachNode[$jk]['created_at'] = date("Y-m-d H:i:s",time());
                    $createUniqueIdentifierForEachNode[$jk]['updated_at'] = date("Y-m-d H:i:s",time());
                    $jk = $jk+1;
                    break;
                }
            }
        }

            
            $getCustomNameOfNode = array();
            $getNewNodeCreatedCustomData = array();
            $da=0;
            foreach($createUniqueIdentifierForEachNode as $createUniqueIdentifierForEachNodeK=>$createUniqueIdentifierForEachNodeV)
            {
                foreach($getItemCustomMetadata as $getItemCustomMetadataKeyData=>$getItemCustomMetadataValueData)
                {
                    if($createUniqueIdentifierForEachNodeV['title']==$getItemCustomMetadataValueData['title'])
                    {
                        $getCustomNameOfNode[]=$getItemCustomMetadataValueData['name'];
                        $getNewNodeCreatedCustomData[]=$getItemCustomMetadataValueData;
                    }
                }
            }

            

            $getUniqueCustomNameOfNode = array();
            $getUniqueCustomNameOfNode = array_unique($getCustomNameOfNode);

            $getNewNodeMetadataData = Metadata::select('metadata_id','name','field_type','field_possible_values','last_field_possible_values')
                                              ->whereIn('name',$getUniqueCustomNameOfNode)
                                              ->where(['organization_id'=>$organizationId,'is_deleted'=>'0','is_custom'=>'1'])
                                            //   ->groupBy('name')
                                              ->get()
                                              ->toArray();

            
            $existingNewNodeMetadata = array();
            foreach($getNewNodeCreatedCustomData as $getNewNodeCreatedCustomDataK=>$getNewNodeCreatedCustomDataV)
            {
                foreach($getNewNodeMetadataData as $getNewNodeMetadataDataK=>$getNewNodeMetadataDataV)
                {
                    if(strtolower($getNewNodeCreatedCustomDataV['name'])==strtolower($getNewNodeMetadataDataV['name']) && $getNewNodeCreatedCustomDataV['field_type']==$getNewNodeMetadataDataV['field_type'])
                    {
                        $getNewNodeCreatedCustomDataV['created_at'] = date("Y-m-d H:i:s",time());
                        $getNewNodeCreatedCustomDataV['updated_at'] = date("Y-m-d H:i:s",time());
                        $getNewNodeCreatedCustomDataV['metadata_id'] = $getNewNodeMetadataDataV['metadata_id'];
                        $getNewNodeCreatedCustomDataV['is_additional'] = 0;
                        $existingNewNodeMetadata[]=$getNewNodeCreatedCustomDataV;
                    }
                }
            }   
            
            $getNewNodeExistingCustomName = array();
            foreach($existingNewNodeMetadata as $existingNewNodeMetadataData=>$existingNewNodeMetadataValue)
            {
                $getNewNodeExistingCustomName[]=$existingNewNodeMetadataValue['name'];
            }
            $getNewNodeExistingCustomNameUnique = array_unique($getNewNodeExistingCustomName);
            
            $getNewNodeFieldTypeMappedWithUniqueCustomName = array();
            $ak=0;
            foreach($getNewNodeExistingCustomNameUnique as $getNewNodeExistingCustomNameUniqueK=>$getNewNodeExistingCustomNameUniqueV)
            {
                foreach($existingNewNodeMetadata as $existingNewNodeMetadataInfo=>$existingNewNodeMetadataData)
                {
                    if(strtolower($getNewNodeExistingCustomNameUniqueV)==strtolower($existingNewNodeMetadataData['name']))
                    {
                        $getNewNodeFieldTypeMappedWithUniqueCustomName[$existingNewNodeMetadataData['name']][$ak]=$existingNewNodeMetadataData['field_type'];
                        $ak = $ak+1;
                    }
                }
            }

            $getNewNodeUniqueFieldTypeMappedWithUniqueCustomName = array();
            foreach($getNewNodeFieldTypeMappedWithUniqueCustomName as $getNewNodeFieldTypeMappedWithUniqueCustomNameK=>$getNewNodeFieldTypeMappedWithUniqueCustomNameV)
            {
                $getNewNodeUniqueFieldTypeMappedWithUniqueCustomName[$getNewNodeFieldTypeMappedWithUniqueCustomNameK] = array_unique($getNewNodeFieldTypeMappedWithUniqueCustomNameV);
            }

            $getNewNodeAdditionalData = array();
            $getNewNodeCustomNameAndFieldType = array();
            $ark=0;
            foreach($getNewNodeCreatedCustomData as $getNewNodeCreatedCustomDataK=>$getNewNodeCreatedCustomDataV)
            {
                if(in_array(strtolower($getNewNodeCreatedCustomDataV['name']),array_map('strtolower', $getNewNodeExistingCustomNameUnique)))
                {
                    if(in_array($getNewNodeCreatedCustomDataV['field_type'],$getNewNodeUniqueFieldTypeMappedWithUniqueCustomName[$getNewNodeCreatedCustomDataV['name']]))
                    {
                        
                    }
                    else
                    {
                        $getNewNodeAdditionalData[]=$getNewNodeCreatedCustomDataV;
                        $getNewNodeCustomNameAndFieldType[$ark]['name']=$getNewNodeCreatedCustomDataV['name'];
                        $getNewNodeCustomNameAndFieldType[$ark]['field_type']=$getNewNodeCreatedCustomDataV['field_type'];
                        $ark=$ark+1;
                    }
                }
                else
                {
                    $getNewNodeAdditionalData[]=$getNewNodeCreatedCustomDataV;
                    $getNewNodeCustomNameAndFieldType[$ark]['name']=$getNewNodeCreatedCustomDataV['name'];
                    $getNewNodeCustomNameAndFieldType[$ark]['field_type']=$getNewNodeCreatedCustomDataV['field_type'];
                    $ark=$ark+1;
                }
            }

            
            $additionalNewNodeUniqueMetadataArray = array_map("unserialize", array_unique(array_map("serialize", $getNewNodeAdditionalData)));
            $uniqueNewNodeCustomNameAndFieldType = array_map("unserialize", array_unique(array_map("serialize", $getNewNodeCustomNameAndFieldType)));
            
            $createNewNodeUniqueIdentifierForCustomData = array();
            foreach($uniqueNewNodeCustomNameAndFieldType as $uniqueNewNodeCustomNameAndFieldTypeK=>$uniqueNewNodeCustomNameAndFieldTypeV)
            {
                $uniqueNewNodeCustomNameAndFieldTypeV['metadata_id'] = $this->createUniversalUniqueIdentifier();
                $createNewNodeUniqueIdentifierForCustomData[]=$uniqueNewNodeCustomNameAndFieldTypeV;
            }

            $newNewNodeAdditionalCustomData = array();
            foreach($additionalNewNodeUniqueMetadataArray as $additionalNewNodeUniqueMetadataArrayK=>$additionalNewNodeUniqueMetadataArrayV)
            {
                foreach($createNewNodeUniqueIdentifierForCustomData as $createNewNodeUniqueIdentifierForCustomDataK=>$createNewNodeUniqueIdentifierForCustomDataV)
                {
                    if(strtolower($additionalNewNodeUniqueMetadataArrayV['name'])==strtolower($createNewNodeUniqueIdentifierForCustomDataV['name']) && $additionalNewNodeUniqueMetadataArrayV['field_type']==$createNewNodeUniqueIdentifierForCustomDataV['field_type'])
                    {
                        $additionalNewNodeUniqueMetadataArrayV['metadata_id'] = $createNewNodeUniqueIdentifierForCustomDataV['metadata_id'];
                        $additionalNewNodeUniqueMetadataArrayV['is_additional'] = 1;
                        $additionalNewNodeUniqueMetadataArrayV['created_at'] = date("Y-m-d H:i:s",time());
                        $additionalNewNodeUniqueMetadataArrayV['updated_at'] = date("Y-m-d H:i:s",time());
                        $newNewNodeAdditionalCustomData[] = $additionalNewNodeUniqueMetadataArrayV;
                    }
                }
            }
            
            $totalNewNodeMetadataFormated = array();
            $totalNewNodeMetadataFormated = array_merge($existingNewNodeMetadata,$newNewNodeAdditionalCustomData);
            
            $itemNewNodeMetaData = array();
            $dataNewNodeForFormattingSelectionData = array();
            $kj=0;
            
            foreach($getNewNodeCreatedCustomData as $getNewNodeCreatedCustomDataKey=>$getNewNodeCreatedCustomDataValue)
            {
                    foreach($totalNewNodeMetadataFormated as $totalNewNodeMetadataFormatedK=>$totalNewNodeMetadataFormatedV)
                    {
                        if(strtolower($getNewNodeCreatedCustomDataValue['name'])==strtolower($totalNewNodeMetadataFormatedV['name']) && $getNewNodeCreatedCustomDataValue['field_type']==$totalNewNodeMetadataFormatedV['field_type'] && $getNewNodeCreatedCustomDataValue['identifier']==$totalNewNodeMetadataFormatedV['identifier'])
                        {
                            $itemNewNodeMetaData[$kj]['item_id']=$getNewNodeCreatedCustomDataValue['identifier'];
                            $itemNewNodeMetaData[$kj]['metadata_id']=$totalNewNodeMetadataFormatedV['metadata_id'];
                            $itemNewNodeMetaData[$kj]['metadata_value']=$totalNewNodeMetadataFormatedV['metadata_value'];
                            $itemNewNodeMetaData[$kj]['is_additional']=$totalNewNodeMetadataFormatedV['is_additional'];
                            $kj=$kj+1;
                        }
                    }
            }
            
            $itemNewNodeMetadataToInsert = array();
            foreach($itemNewNodeMetaData as $itemNewNodeMetaDataK=>$itemNewNodeMetaDataV)
            {
                foreach($getItemId as $getItemIdK=>$getItemIdV)
                {
                    if($itemNewNodeMetaDataV['item_id']==$getItemIdV['source_item_id'])
                    {
                        $itemNewNodeMetaDataV['item_id']=$getItemIdV['item_id'];
                        $itemNewNodeMetadataToInsert[]=$itemNewNodeMetaDataV;
                    }
                }
            }

            
            $dataNewNodeToInsertInMetadataTable = array();
            foreach($createNewNodeUniqueIdentifierForCustomData as $createNewNodeUniqueIdentifierForCustomDataKey=>$createNewNodeUniqueIdentifierForCustomDataValue)
            {
                foreach($additionalNewNodeUniqueMetadataArray as $additionalNewNodeUniqueMetadataArrayKeyInfo=>$additionalNewNodeUniqueMetadataArrayDataValue)
                {
                    if(strtolower($createNewNodeUniqueIdentifierForCustomDataValue['name'])==strtolower($additionalNewNodeUniqueMetadataArrayDataValue['name']) && $createNewNodeUniqueIdentifierForCustomDataValue['field_type']==$additionalNewNodeUniqueMetadataArrayDataValue['field_type'])
                    {
                        $createNewNodeUniqueIdentifierForCustomDataValue['organization_id']=$organizationId;
                        $createNewNodeUniqueIdentifierForCustomDataValue['field_possible_values']=$additionalNewNodeUniqueMetadataArrayDataValue['field_possible_values'];
                        $createNewNodeUniqueIdentifierForCustomDataValue['last_field_possible_values']=$additionalNewNodeUniqueMetadataArrayDataValue['last_field_possible_values'];
                        $createNewNodeUniqueIdentifierForCustomDataValue['is_custom']=1;
                        $createNewNodeUniqueIdentifierForCustomDataValue['is_active']=1;
                        $createNewNodeUniqueIdentifierForCustomDataValue['is_deleted']=0;
                        $createNewNodeUniqueIdentifierForCustomDataValue['created_at']=date("Y-m-d H:i:s",time());
                        $createNewNodeUniqueIdentifierForCustomDataValue['updated_at']=date("Y-m-d H:i:s",time());
                        $dataNewNodeToInsertInMetadataTable[]=$createNewNodeUniqueIdentifierForCustomDataValue;
                        break;
                    }
                }
            }
            
            
            $getNodeIdMappedWithEachMetadataId = array();
            $getNodeId = array();
            $ni=0;
            
            foreach($createUniqueIdentifierForEachNode as $createUniqueIdentifierForEachNodeKey=>$createUniqueIdentifierForEachNodeValue)
            {
                foreach($totalNewNodeMetadataFormated as $totalNewNodeMetadataFormatedKey=>$totalNewNodeMetadataFormatedValue)
                {
                    if($createUniqueIdentifierForEachNodeValue['title']==$totalNewNodeMetadataFormatedValue['title'])
                    {
                        $getNodeId[]=$createUniqueIdentifierForEachNodeValue['node_type_id'];
                        $getNodeIdMappedWithEachMetadataId[$createUniqueIdentifierForEachNodeValue['node_type_id']][$ni] = $totalNewNodeMetadataFormatedValue['metadata_id'];
                        $ni = $ni+1;
                    }
                }
            }

            
            $getUniqueNodeId = array();
            $getUniqueNodeId = array_unique($getNodeId);

            $getMultipleNodeIdMappedWithMetadataId = array();
            foreach($getUniqueNodeId as $getUniqueNodeIdK=>$getUniqueNodeIdV)
            {
                foreach($getNodeIdMappedWithEachMetadataId as $getNodeIdMappedWithEachMetadataIdK=>$getNodeIdMappedWithEachMetadataIdV)
                {
                    if($getUniqueNodeIdV==$getNodeIdMappedWithEachMetadataIdK)
                    {
                        foreach($getNodeIdMappedWithEachMetadataIdV as $getNodeIdMappedWithEachMetadataIdVK=>$getNodeIdMappedWithEachMetadataIdVV)
                        {
                            $getMultipleNodeIdMappedWithMetadataId[][$getUniqueNodeIdV][]=$getNodeIdMappedWithEachMetadataIdVV;
                        }
                    }
                }
            }
            
            $getFinalNodeIdMappedWithMetadataId = array();
            $nti=0;
            foreach($getMultipleNodeIdMappedWithMetadataId as $getMultipleNodeIdMappedWithMetadataIdK=>$getMultipleNodeIdMappedWithMetadataIdV)
            {
                foreach($getMultipleNodeIdMappedWithMetadataIdV as $getMultipleNodeIdMappedWithMetadataIdVK=>$getMultipleNodeIdMappedWithMetadataIdVV)
                {
                    $getFinalNodeIdMappedWithMetadataId[$nti]['node_type_id'] = $getMultipleNodeIdMappedWithMetadataIdVK;
                    $getFinalNodeIdMappedWithMetadataId[$nti]['metadata_id'] = $getMultipleNodeIdMappedWithMetadataIdVV[0];
                    $nti = $nti+1;
                }
            }

            // Insert in node_type_metadata table start
            if(!empty($getFinalNodeIdMappedWithMetadataId))
            {
                // Below script added to avoid dupliate metadata entries in single metadata set
                // Don't remove the below script
                $getUniqueFinalNodeIdMappedWithMetadataId = array_unique($getFinalNodeIdMappedWithMetadataId, SORT_REGULAR);
                NodeTypeMetaData::insert($getUniqueFinalNodeIdMappedWithMetadataId);
            }
            // Insert in node_type_metadata table end    
 
        }
        
        // Formatting data to insert in node_type_metadata table end

        // Queries to insert data end

        /********* ITEM LEVEL DATA HANDLED END **********/

        /********* DOC LEVEL DATA HANDLED START **********/

        $docCustomMetadata = $customMetadata['ACMTcustomfields']['CFDocument'];
        $getDocCustomMetadata = array();
        $getDocSourceId = array();
        foreach($docCustomMetadata as $docCustomMetadataK=>$docCustomMetadataV)
        {
            if(!empty($docCustomMetadataV['custom']))
            {
                foreach($docCustomMetadataV['custom'] as $docCustomMetadataVKey=>$docCustomMetadataVValue)
                {   
                    if(!empty($docCustomMetadataVValue))
                    {
                        $docCustomMetadataVValue['identifier']=$docCustomMetadataV['identifier'];
                        $docCustomMetadataVValue['title']=$docCustomMetadataV['title'];
                        $docCustomMetadataVValue['metadata_id'] = "";
                        $docCustomMetadataVValue['is_additional'] = "";
                        $getDocCustomMetadata[]=$docCustomMetadataVValue;

                        $getDocSourceId[]=$docCustomMetadataV['identifier'];
                    }
                }
            }
            }
        

        $getDocCustomName = array();
        $docCustomDetail = array();
        $a=0;
        foreach($getDocCustomMetadata as $getDocCustomMetadataK=>$getDocCustomMetadataV)
        {
                $getDocCustomName[$a]=$getDocCustomMetadataV['name'];
                $docCustomDetail[]=$getDocCustomMetadataV;
                $a=$a+1;
        }

        $getDocUniqueCustomName = array();
        $getDocUniqueCustomName=array_unique($getDocCustomName);
        
        $getDocMetadataData = Metadata::select('metadata_id','name','field_type','field_possible_values','last_field_possible_values')
                                              ->whereIn('name',$getDocUniqueCustomName)
                                              ->where(['organization_id'=>$organizationId,'is_deleted'=>'0','is_custom'=>'1'])
                                            //   ->groupBy('name')
                                              ->get()
                                              ->toArray();

        // Looping to get already exist metadata start
        $existingDocMetadata = array();
        
        foreach($docCustomDetail as $docCustomDetailK=>$docCustomDetailV)
        {
            foreach($getDocMetadataData as $getDocMetadataDataK=>$getDocMetadataDataV)
            {
                if(strtolower($docCustomDetailV['name'])==strtolower($getDocMetadataDataV['name']) && $docCustomDetailV['field_type']==$getDocMetadataDataV['field_type'])
                {
                    $docCustomDetailV['created_at'] = date("Y-m-d H:i:s",time());
                    $docCustomDetailV['updated_at'] = date("Y-m-d H:i:s",time());
                    $docCustomDetailV['metadata_id'] = $getDocMetadataDataV['metadata_id'];
                    $docCustomDetailV['is_additional'] = 0;
                    $existingDocMetadata[]=$docCustomDetailV;
                }
            }


        }

        // Looping to get already exist metadata end

        // get Additional Metadata Data start
        $getDocExistingCustomName = array();
        foreach($existingDocMetadata as $existingDocMetadataK=>$existingDocMetadataV)
        {
            $getDocExistingCustomName[]=$existingDocMetadataV['name'];
        }

        $getDocAdditionalMetadataName = array();
        $getDocAdditionalMetadataName=array_diff($getDocUniqueCustomName,$getDocExistingCustomName);

        $additionalDocMetadata = array();
        foreach($getDocCustomMetadata as $getDocCustomMetadataInfo=>$getDocCustomMetadataData)
        {
            foreach ($getDocAdditionalMetadataName as $getDocAdditionalMetadataNameK=>$getDocAdditionalMetadataNameV)
            {
                if(strtolower($getDocCustomMetadataData['name'])==strtolower($getDocAdditionalMetadataNameV))
                {
                    $getDocCustomMetadataData['created_at'] = date("Y-m-d H:i:s",time());
                    $getDocCustomMetadataData['updated_at'] = date("Y-m-d H:i:s",time());
                    $getDocCustomMetadataData['metadata_id'] = $this->createUniversalUniqueIdentifier();
                    $getDocCustomMetadataData['is_additional'] = 1;
                    $additionalDocMetadata[]=$getDocCustomMetadataData;
                }
            }
        }
        // get Additional Metadata Data end

        // Adding Existing metadata and new metadata start
        $totalDocMetadataFormated = array();
        $totalDocMetadataFormated = array_merge($existingDocMetadata,$additionalDocMetadata);
        // Adding Existing metadata and new metadata end

        // Formatting data to insert in document_metadata table start
        $docMetaData = array();
        $dataForFormattingSelectionDataDoc = array();
        $b=0;
        
        foreach($getDocCustomMetadata as $getDocCustomMetadataKey=>$getDocCustomMetadataValue)
        {
                foreach($totalDocMetadataFormated as $totalDocMetadataFormatedK=>$totalDocMetadataFormatedV)
                {
                    if(strtolower($getDocCustomMetadataValue['name'])==strtolower($totalDocMetadataFormatedV['name']) && $getDocCustomMetadataValue['field_type']==$totalDocMetadataFormatedV['field_type'] && $getDocCustomMetadataValue['identifier']==$totalDocMetadataFormatedV['identifier'])
                    {
                        $docMetaData[$b]['document_id']=$getDocCustomMetadataValue['identifier'];
                        $docMetaData[$b]['metadata_id']=$totalDocMetadataFormatedV['metadata_id'];
                        $docMetaData[$b]['metadata_value']=$totalDocMetadataFormatedV['metadata_value'];
                        $docMetaData[$b]['is_additional']=$totalDocMetadataFormatedV['is_additional'];
                        $b=$b+1;
                    }
                }
        }

        $getDocUniqueSourceId = array();
        $getDocUniqueSourceId=array_unique($getDocSourceId);

        $getDocSourceId = Document::select('source_document_id')
                                              ->where('document_id',$importDocumentId)
                                              ->where(['organization_id'=>$organizationId,'is_deleted'=>'0'])
                                              ->get()
                                              ->toArray();

        $docSourceIdDate = array();
        foreach($getDocSourceId as $getDocSourceIdK=>$getDocSourceIdV)
        {
            $docSourceIdDate[]=$getDocSourceIdV['source_document_id'];
        }
        $getUniqueDocSourceId=array_unique($docSourceIdDate);
        
        if($copy==0)
        {
            $getDocId = Document::select('document_id','source_document_id','node_type_id')
                        ->whereIn('source_document_id',$getUniqueDocSourceId)
                        ->where(['organization_id'=>$organizationId,'is_deleted'=>'0'])
                        ->get()
                        ->toArray();

            $docMetadataToInsert = array();
            
            foreach($docMetaData as $docMetaDataK=>$docMetaDataV)
            {
                foreach($getDocId as $getDocIdK=>$getDocIdV)
                {
                    if($docMetaDataV['document_id']==$getDocIdV['source_document_id'])
                    {
                        $docMetaDataV['document_id']=$getDocIdV['document_id'];
                        $docMetadataToInsert[]=$docMetaDataV;
                    }
                }
            }
        }
        else
        {

            $getDocId = Document::select('document_id','uri','node_type_id')
                        ->where(['document_id'=>$importDocumentId,'organization_id'=>$organizationId,'is_deleted'=>'0'])
                        ->get()
                        ->toArray();

            $docMetadataToInsert = array();
            foreach($docMetaData as $docMetaDataK=>$docMetaDataV)
            {
                foreach($getDocId as $getDocIdK=>$getDocIdV)
                {
                    $originalDocSourceId = substr($getDocIdV['uri'], -36);
                    if($docMetaDataV['document_id']==$originalDocSourceId)
                    {
                        $docMetaDataV['document_id']=$getDocIdV['document_id'];
                        $docMetadataToInsert[]=$docMetaDataV;
                    }
                }
            }
        }

        // Formatting data to insert in document_metadata table end

        // Formatting Additional Metadata to insert in metadata table start
        $getDocSelectionDataToUpdate = array();
        foreach($existingDocMetadata as $existingDocMetadataK=>$existingDocMetadataV)
        {
            foreach($getDocMetadataData as $getDocMetadataDataKey=>$getDocMetadataDataValue)
            {
                if($existingDocMetadataV['metadata_id']==$getDocMetadataDataValue['metadata_id'] && $existingDocMetadataV['field_type']==3)
                {
                    $finalDocFieldValueArray = array();
                    $existingDocField = explode(',', $existingDocMetadataV['field_possible_values']);
                    $metadataDocField = explode(',', $getDocMetadataDataValue['field_possible_values']);
                    $finalDocFieldValueArray=array_unique(array_merge($existingDocField,$metadataDocField));
                    $finalDocFieldValueString=implode(',', $finalDocFieldValueArray);

                    $existingDocMetadataV['field_possible_values']=$finalDocFieldValueString;
                    $existingDocMetadataV['last_field_possible_values']=$finalDocFieldValueString;
                    $existingDocMetadataV['organization_id']=$organizationId;
                    $existingDocMetadataV['name']=$getDocMetadataDataValue['name'];
                    $getDocSelectionDataToUpdate[]=$existingDocMetadataV;

                }
            }
        }

        // Formatting Additional Metadata to insert in metadata table end

        // Formatting data to insert in node_type_metadata table start
        /*$nodeIdMappedWithCustomMetadataIdDoc = array();
        $c=0;
        foreach($getDocId as $getDocIdKey=>$getDocIdvalue)
        {
            foreach($totalDocMetadataFormated as $totalDocMetadataFormatedKey=>$totalDocMetadataFormatedValue)
            {
                if($getDocIdvalue['source_document_id']==$totalDocMetadataFormatedValue['identifier'] && $totalDocMetadataFormatedValue['is_additional']==0)
                {
                    $nodeIdMappedWithCustomMetadataIdDoc[$c][$getDocIdvalue['node_type_id']]=$totalDocMetadataFormatedValue['metadata_id'];
                    $c=$c+1;
                }
            }
        }


        $singleDocNodeIdMappedWithMultipleMetadataId = array();
        $w=0;
        foreach($getDocId as $getDocIdInfo=>$getDocIdData)
        {
            foreach($nodeIdMappedWithCustomMetadataIdDoc as $nodeIdMappedWithCustomMetadataIdDocK=>$nodeIdMappedWithCustomMetadataIdDocV)
            {
                foreach($nodeIdMappedWithCustomMetadataIdDocV as $nodeIdMappedWithCustomMetadataIdDocVKey=>$nodeIdMappedWithCustomMetadataIdDocVValue)
                {
                    if($getDocIdData['node_type_id']==$nodeIdMappedWithCustomMetadataIdDocVKey)
                    {
                        $singleDocNodeIdMappedWithMultipleMetadataId[$getDocIdData['node_type_id']][$w]=$nodeIdMappedWithCustomMetadataIdDocVValue;
                        $w=$w+1;
                    }
                }
            }

        }

        $getDocNodeId = array();
        foreach($getDocId as $getDocIdInfo=>$getDocIdData)
        {
            $getDocNodeId[] = $getDocIdData['node_type_id'];
        }
        $getUniqueDocNodeId = array();
        $getUniqueDocNodeId = array_unique($getDocNodeId);

        // Get Node Id and Metadata Id from node_type_metadata start
        $getDocNodeIdMetadataId = NodeTypeMetaData::select('node_type_id','metadata_id')
                                ->whereIn('node_type_id',$getUniqueDocNodeId)
                                ->get()
                                ->toArray();

        // Get Node Id and Metadata Id from node_type_metadata end

        $mapExisitingDocMetadataToNodeId = array();
        $t=0;
        foreach($getUniqueDocNodeId as $getUniqueDocNodeIdK=>$getUniqueDocNodeIdV)
        {
            foreach($getDocNodeIdMetadataId as $getDocNodeIdMetadataIdK=>$getDocNodeIdMetadataIdV)
            {
                if($getUniqueDocNodeIdV==$getDocNodeIdMetadataIdV['node_type_id'])
                {
                    $mapExisitingDocMetadataToNodeId[$getUniqueDocNodeIdV][$t]=$getDocNodeIdMetadataIdV['metadata_id'];
                    $t=$t+1;
                }
            }
        }

        $getDocNodeIdAndMetadataIdToBeInserted = array();
        $getDocMetadataIdDiff = array();
        foreach($mapExisitingDocMetadataToNodeId as $mapExisitingDocMetadataToNodeIdK=>$mapExisitingDocMetadataToNodeIdV)
        {
            foreach($singleDocNodeIdMappedWithMultipleMetadataId as $singleDocNodeIdMappedWithMultipleMetadataIdK=>$singleDocNodeIdMappedWithMultipleMetadataIdV)
            {
                if($mapExisitingDocMetadataToNodeIdK==$singleDocNodeIdMappedWithMultipleMetadataIdK)
                {
                    $getDocMetadataIdDiff = array_diff($singleDocNodeIdMappedWithMultipleMetadataIdV,$mapExisitingDocMetadataToNodeIdV);
                    $getDocNodeIdAndMetadataIdToBeInserted[$mapExisitingDocMetadataToNodeIdK]=$getDocMetadataIdDiff;
                }
            }
        }

        $finakDocNodeIdMappedWithMetadataIdToInsert = array();
        foreach($getUniqueDocNodeId as $getUniqueDocNodeIdKey=>$getUniqueDocNodeIdValue)
        {
            foreach($getDocNodeIdAndMetadataIdToBeInserted as $getDocNodeIdAndMetadataIdToBeInsertedK=>$getDocNodeIdAndMetadataIdToBeInsertedV)
            {
                if($getUniqueDocNodeIdValue==$getDocNodeIdAndMetadataIdToBeInsertedK)
                {
                   foreach($getDocNodeIdAndMetadataIdToBeInsertedV as $getDocNodeIdAndMetadataIdToBeInsertedVInfo=>$getDocNodeIdAndMetadataIdToBeInsertedVData)
                   {
                       $finakDocNodeIdMappedWithMetadataIdToInsert[][$getUniqueDocNodeIdValue]=$getDocNodeIdAndMetadataIdToBeInsertedVData;
                   }
                }
            }
        }
        */
        // Queries to insert data start

        // Insert In document_metadata table start
        if(!empty($docMetadataToInsert))
        {
            DocumentMetadata::insert($docMetadataToInsert);
        }
        // Insert In document_metadata table end

        // Insert in metadata table start
        if(!empty($additionalDocMetadata))
        {
            $finalDocadditionalMetadataToInsert = array();
            foreach($additionalDocMetadata as $additionalDocMetadataInfo=>$additionalDocMetadataData)
            {
                unset($additionalDocMetadataData['metadata_value']);
                unset($additionalDocMetadataData['identifier']);
                unset($additionalDocMetadataData['is_additional']);
                unset($additionalDocMetadataData['title']);
                $additionalDocMetadataData['organization_id']=$organizationId;

                $finalDocadditionalMetadataToInsert[]=$additionalDocMetadataData;

            }
            
            Metadata::insert($finalDocadditionalMetadataToInsert);
        }
        // Insert in metadata table end

        // Update metadata table start
        if(!empty($getDocSelectionDataToUpdate))
        {
            foreach($getDocSelectionDataToUpdate as $getDocSelectionDataToUpdateK=>$getDocSelectionDataToUpdateV)
            {
                Metadata::where(['metadata_id'=>$getDocSelectionDataToUpdateV['metadata_id'], 'organization_id'=>$getDocSelectionDataToUpdateV['organization_id']])->update(['field_possible_values' => $getDocSelectionDataToUpdateV['field_possible_values'], 'last_field_possible_values'=>$getDocSelectionDataToUpdateV['last_field_possible_values']]);
            }
        }
    
        // Update metadata table end

        // Insert in node_type_metadata table start
        /*if(!empty($finakDocNodeIdMappedWithMetadataIdToInsert))
        {
            $finalDocAdditionalDataToInsert = array();
            $ry=0;
            foreach($finakDocNodeIdMappedWithMetadataIdToInsert as $finakDocNodeIdMappedWithMetadataIdToInsertK=>$finakDocNodeIdMappedWithMetadataIdToInsertV)
            {
                foreach($finakDocNodeIdMappedWithMetadataIdToInsertV as $finakDocNodeIdMappedWithMetadataIdToInsertVInfo=>$finakDocNodeIdMappedWithMetadataIdToInsertVData)
                {
                    $finalDocAdditionalDataToInsert[$ry]['node_type_id']=$finakDocNodeIdMappedWithMetadataIdToInsertVInfo;
                    $finalDocAdditionalDataToInsert[$ry]['metadata_id']=$finakDocNodeIdMappedWithMetadataIdToInsertVData;
                    $ry=$ry+1;
                }
            }

            NodeTypeMetaData::insert($finalDocAdditionalDataToInsert);
        }*/
        // Insert in node_type_metadata table end

        // Queries to insert data end

        // Formatting data to insert in node_type_metadata table end
        
        /********* DOC LEVEL DATA HANDLED END **********/

        /******** Insert Data in asset Table Document item starts*******/
        foreach ($itemCustomMetadata as $assetK=>$assetV) {
            $itemId =$this->getItemIdFromSourceId($assetV['identifier'],$importDocumentId,$organizationId,$copy);
            foreach ($assetV['asset'] as $assetDataK => $assetDataV) {
                $asset_id    = $this->createUniversalUniqueIdentifier();
                $asset_name             = isset($assetDataV['asset_name']) ? $assetDataV['asset_name'] : '';
                $name             = isset($assetDataV['name']) ? $assetDataV['name'] : '';
                $ext=$this->get_file_extension($name);
                $targetFile = $asset_id.'.'.$ext;
                $bucketName = config("filesystems")["disks"]["s3"]["bucket"];
                $path = "ASSET/ITEM/";
                $exists = Storage::disk('s3')->exists($path.$name);
                if($exists)
                {
                    if($name!=="")
                    {
                        Storage::disk('s3')->copy($path.$name,$path.$targetFile);
                    }
                $asset_target_file_name = isset($assetDataV['asset_target_file_name']) ? $assetDataV['asset_target_file_name'] : '';
                $asset_content_type     = isset($assetDataV['asset_content_type']) ? $assetDataV['asset_content_type'] : '';
                $asset_size             = isset($assetDataV['asset_size']) ? $assetDataV['asset_size'] : '';
                $title                  = isset($assetDataV['title']) ? $assetDataV['title'] : '';
                $description            = isset($assetDataV['description']) ? $assetDataV['description'] : '';
                $asset_linked_type      = isset($assetDataV['asset_linked_type']) ? $assetDataV['asset_linked_type'] : '';
                $uploaded_at            = isset($assetDataV['uploaded_date_time']) ? $assetDataV['uploaded_date_time'] : '';
                $asset_order            = isset($assetDataV['asset_order']) ? $assetDataV['asset_order'] : '';
                $status                 = isset($assetDataV['status']) ? $assetDataV['status'] : '';

                $assetArr = [
                    'asset_id'              =>$asset_id,
                    'organization_id'       =>isset($organizationId)?$organizationId:'',
                    'document_id'           =>isset($importDocumentId)?$importDocumentId:'',
                    'asset_name'            =>isset($asset_name)?$asset_name:'',
                    'asset_target_file_name'=>isset($targetFile)?$targetFile:'',
                    'asset_content_type'    =>isset($asset_content_type)?$asset_content_type:'',
                    'asset_size'            =>isset($asset_size)?$asset_size:'',
                    'title'                 =>isset($title)?$title:'',
                    'description'           =>isset($description)?$description:'',
                    'asset_linked_type'     =>isset($asset_linked_type)?$asset_linked_type:'',
                    'item_linked_id'        =>isset($itemId)?$itemId:'',
                    'asset_order'           =>isset($asset_order)?$asset_order:'',
                    'status'                =>isset($status)?$status:'',
                ];
                unset($targetFile);
                Asset::insert($assetArr);
                }
            }
        }

         /*********************************Item Insert in asset table ends here***********************************/

        /*****************************Insert data for exemplar and association **********************************/
        foreach ($itemCustomMetadata as $exemplarK =>$exemplarV) {
            foreach ($exemplarV['exemplar_and_association'] as $exemplarDataK => $exemplarDataArrV) {
                    $itemId =$this->getItemIdFromSourceId($exemplarV['identifier'],$importDocumentId,$organizationId,$copy);
                    $association_type           = 8;
                    $document_id                = isset($importDocumentId)?$importDocumentId:'' ;
                    $association_group_id       = isset($exemplarDataArrV['association_group_id']) ? $exemplarDataArrV['association_group_id'] : '';
                    $destination_node_id        = '';
                    $destination_document_id    = '';
                    $sequence_number            = isset($exemplarDataArrV['sequence_number']) ? $exemplarDataArrV['sequence_number'] : '';
                    $external_node_title        = isset($exemplarDataArrV['external_node_title']) ? $exemplarDataArrV['external_node_title'] : '';
                    $description                = isset($exemplarDataArrV['description']) ? $exemplarDataArrV['description'] : '';
                    $external_node_url          = isset($exemplarDataArrV['external_node_url']) ? $exemplarDataArrV['external_node_url'] : '';
                    $organization_id            = isset($organizationId) ?$organizationId : '';
                    $has_asset                  = isset($exemplarDataArrV['has_asset']) ? $exemplarDataArrV['has_asset'] : '';
                    $uri                        = isset($exemplarDataArrV['uri']) ? $exemplarDataArrV['uri'] : '';
                    $is_reverse_association     = isset($exemplarDataArrV['is_reverse_association']) ? $exemplarDataArrV['is_reverse_association'] : '';
                    $exemplarname     = isset($exemplarDataArrV['assets']['asset_target_file_name']) ? $exemplarDataArrV['assets']['asset_target_file_name'] : '';
                    $exemplarasset_id    = $this->createUniversalUniqueIdentifier();
                    $exemplarext=$this->get_file_extension($exemplarname);
                    $exemplartargetFile = $exemplarasset_id.'.'.$exemplarext;
                    $exemplarbucketName = config("filesystems")["disks"]["s3"]["bucket"];
                    $exemplarpath = "ASSET/EXEMPLAR/";
                    if($exemplarname!=="")
                    {
                        Storage::disk('s3')->copy($exemplarpath.$exemplarname,$exemplarpath.$exemplartargetFile);
                    }    

                    $associationId = $this->createUniversalUniqueIdentifier();
                    $exemplarArr = [
                        'item_association_id'        => $associationId,
                        'association_type'           => $association_type,
                        'document_id'                => $document_id,
                        'association_group_id'       => $association_group_id,
                        'origin_node_id'             => isset($itemId)?$itemId:'',
                        'destination_node_id'        => $destination_document_id,
                        'destination_document_id'    => $destination_node_id,
                        'sequence_number'            => $sequence_number,
                        'is_deleted'                 => 0,
                        'source_item_association_id' => $associationId,
                        'external_node_title'        => $external_node_title,
                        'description'                => $description,
                        'external_node_url'          => $external_node_url,
                        'organization_id'            => $organization_id,
                        'has_asset'                  => $has_asset,
                        'uri'                        => $uri,
                        'is_reverse_association'     => $is_reverse_association,
                        'source_document_id'         => $document_id,
                        'source_item_id'             => isset($itemId)?$itemId:'',
                        'target_document_id'         => '',
                        'target_item_id'             => '',
                    ];
                    ItemAssociation::insert($exemplarArr);

                $assetArr = [
                    'asset_id'              =>$exemplarasset_id,
                    'organization_id'       =>isset($organizationId)?$organizationId:'',
                    'document_id'           =>isset($importDocumentId)?$importDocumentId:'',
                    'asset_name'            =>isset($exemplarDataArrV['assets']['name'])?$exemplarDataArrV['assets']['name']:'',
                    'asset_target_file_name'=>isset($exemplartargetFile)?$exemplartargetFile:'',
                    'asset_content_type'    =>isset($exemplarDataArrV['assets']['asset_content_type'])?$exemplarDataArrV['assets']['asset_content_type']:'',
                    'asset_size'            =>isset($exemplarDataArrV['assets']['asset_size_in_bytes'])?$exemplarDataArrV['assets']['asset_size_in_bytes']:'',
                    'title'                 =>isset($exemplarDataArrV['assets']['title'])?$exemplarDataArrV['assets']['title']:'',
                    'description'           =>isset($exemplarDataArrV['assets']['description'])?$exemplarDataArrV['assets']['description']:'',
                    'asset_linked_type'     =>3,
                    'item_linked_id'        =>isset($associationId)?$associationId:'',
                    'uploaded_at'           =>isset($exemplarDataArrV['assets']->uploaded_date_time)?$exemplarDataArrV['assets']['uploaded_date_time']:'',
                    'asset_order'           =>0,
                    'status'                =>0
                ];
                unset($exemplartargetFile);
                Asset::insert($assetArr);
            }
        }



        /***********************Exemplar Ends here*************************************/

        /*****************Insert data in asset table for document ************************/
         foreach ($docCustomMetadata as $documentAssetDataK=>$documentAssetDataV)
         {
             foreach ($documentAssetDataV['document_asset'] as $assetKey =>$assetValue)
             {
                $docasset_id    = $this->createUniversalUniqueIdentifier();
                $nameDoc             = isset($assetValue['name']) ? $assetValue['name'] : '';
                $ext=$this->get_file_extension($nameDoc);
                $targetFileDoc = $docasset_id.'.'.$ext;
                $bucketName = config("filesystems")["disks"]["s3"]["bucket"];
                $docpath = "ASSET/DOCUMENT/";
                if($nameDoc!=="")
                { 
                    Storage::disk('s3')->copy($docpath.$nameDoc,$docpath.$targetFileDoc);
                }
                 $assetDocArr = [
                     'asset_id'              =>$docasset_id,
                     'organization_id'       =>isset($organizationId)?$organizationId:'',
                     'document_id'           =>isset($importDocumentId)?$importDocumentId:'',
                     'asset_name'            =>isset($assetValue['asset_name'])?$assetValue['asset_name']:'',
                     'asset_target_file_name'=>isset($targetFileDoc)?$targetFileDoc:'',
                     'asset_content_type'    =>isset($assetValue['asset_content_type'])?$assetValue['asset_content_type']:'',
                     'asset_size'            =>isset($assetValue['asset_size'])?$assetValue['asset_size']:'',
                     'title'                 =>isset($assetValue['title'])?$assetValue['title']:'',
                     'description'           =>isset($assetValue['description'])?$assetValue['description']:'',
                     'asset_linked_type'     =>isset($assetValue['asset_linked_type'])?$assetValue['asset_linked_type']:'',
                     'item_linked_id'        =>isset($importDocumentId)?$importDocumentId:'',
                     'uploaded_at'           =>isset($assetValue['uploaded_date_time'])?$assetValue['uploaded_date_time']:'',
                     'asset_order'           =>isset($assetValue['asset_order'])?$assetValue['asset_order']:'',
                     'status'                =>isset($assetValue['status'])?$assetValue['status']:'',
                 ];
                 unset($targetFile);
                 Asset::insert($assetDocArr);
             }
         }
     /*********************Document Insert Data in asset table ends here*****/

    }

    /**
     * @param $source_item_id
     * @return mixed
     * @purpose This function is used to return item based on source item id
     */
    function getItemIdFromSourceId($source_item_id,$importDocumentId,$organizationId,$copy){
        
        if($copy==0)
        {
            foreach($this->itemArr as $savedItem) {
                if($source_item_id==$savedItem['source_item_id']){
                    $itemId = $savedItem['item_id'];
                    return $itemId;
                }
            }
        }
        else
        {
            $getItemId = Item::select('item_id','uri')
                            ->where(['document_id'=>$importDocumentId,'organization_id'=>$organizationId,'is_deleted'=>'0'])
                            ->get()
                            ->toArray();
            $getFinalItemId = '';
            foreach($getItemId as $getItemIdKeyData=>$getItemIdInfoValue)
            {
                $originalSourceId = substr($getItemIdInfoValue['uri'], -36);
                if($source_item_id==$originalSourceId)
                {
                    $getFinalItemId=$getItemIdInfoValue['item_id'];
                }
            }    
            
            return $getFinalItemId;
                            
        }

    }

    function super_unique($array,$key,$fieldtype,$title)
    {
       $temp_array = [];
       foreach ($array as &$v) {
           if (!isset($temp_array[$v[$key]]))
           $temp_array[$v[$key]] =& $v;
       }
       $array = array_values($temp_array);
       return $array;

    }

    function get_file_extension($file_name) {
        return substr(strrchr($file_name,'.'),1);
    }

}