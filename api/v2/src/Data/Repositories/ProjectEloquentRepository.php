<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\Project;
use App\Data\Models\PublicReview;
class ProjectEloquentRepository extends EloquentRepository implements ProjectRepositoryInterface
{

    public function getModel()
    {
        return Project::class;
    }

    /**
     * This method returns all project record count
     * reusable method to be used inside this class only
     */
    private function getProjectCount($returnActive = true, $returnDeleted = false): int {
        $filterValuePair = [
            'is_active' => $returnActive ? 1 : 0, 
            'is_deleted' => $returnDeleted ? 1 : 0
        ];
        return $this->findByAttributes($filterValuePair)->count();
    }

    /**
     * This method returns all the associated models of a project
     * Also the associated models will also contains other models associated with it
     */
    public function getProjectDetailsWithRelations(string $projectId){
        $fetchAssociatedModels = [
            "prefDocument", 
            "items"
        ];
        return $this->findBy("project_id", $projectId, $fetchAssociatedModels);
    }
    
    public function getProjectList(Request $request)
    {
        $input = $request->all();
        
        $allowedOrderByFields = ['project_name', 'description', 'created_at', 'updated_at'];
        $allowedOrderByMethods = ['asc', 'desc'];

        $limit = 10;
        if ($request->has('limit') && is_int($request->limit)) {
            $limit = $request->limit;
        }

        $offset = 0;
        if ($request->has('offset') && is_int($request->offset)) {
            $offset = $request->offset;
        }


         //return $this->model->take($limit)->skip($offset)
        /* 
        Update For ACMT-1072
       Reason :Previous code show only 10 record for the project Please if ticket mention only 10 project then please revert my code which is updated
       Developer : Amey Joshi
       Updated By 09-11-2018        
        */
        $projectType    =   !empty($input['project_type']) ? $input['project_type'] : "1";

        return $this->model
            ->when($request->has('sort'), function($query) use ($request, $allowedOrderByFields, $allowedOrderByMethods) {
                list($orderBy, $sorting) = explode(':', $request->sort);
                if (in_array($orderBy, $allowedOrderByFields) && in_array($sorting, $allowedOrderByMethods)) {
                    $query->orderBy($orderBy, $sorting);
                }            
            })
            ->where('is_deleted', 0)
            ->where('project_type', $projectType)
            ->where('organization_id', $input['auth_user']['organization_id'])
            ->when($request->has('search'), function($query) use ($request) {
                $query->where('project_name', 'like', "%{$request->search}%");
            })
            ->select('project_id', 'project_name', 'description', 'workflow_id','project_type','created_at', 'updated_at', 'document_id', 'current_workflow_stage_id', 'updated_by','created_by')
            ->with([
                'workflow' => function($query) {
                    $query->select('workflow_id', 'name');
                }
            ])
            ->with([
                'project_status' => function($query) {
                    $query->select('workflow_stage_id', 'stage_name');
                }
            ])
            ->with([
                'user' => function($query) {
                    $query->select('user_id');
                    $query->selectRaw('concat_ws(" ", first_name, last_name) as name');
                }
            ])
        ->get();  
        
    }
    
    public function getAll($input){
        $searchCondition = '';
        $searchQuery = $input['search_key'];
        $arrayOfSpecialCharacter = ['&','_'];

        /*********************************************************************/
        $query = DB::table('projects')
        ->join('workflows', 'projects.workflow_id', '=', 'workflows.workflow_id')
        ->leftjoin('users', 'projects.created_by', '=', 'users.user_id')
        ->select('projects.project_id','projects.project_name','projects.description','projects.workflow_id',
                'projects.is_active', 'projects.created_at','projects.updated_at',
                'workflows.title as workflow_title', 'users.first_name as created_by_fname','users.last_name as created_by_lname')
        ->orderBy('created_at', 'desc');
            

        if(!empty($input['search_key'])){
            // if(in_array($input['search_key'], $arrayOfSpecialCharacter) ){
            //     $searchQuery = str_replace($input['search_key'],'\\'.$input['search_key'], $input['search_key']);
            // }
            if (strpos($input['search_key'], '_') !== false) {
                $searchQuery = str_replace('_','\_', $input['search_key']);    
            }

            if (strpos($input['search_key'], '%') !== false) {
                $searchQuery = str_replace('%','\%', $input['search_key']);    
            }
            
            $searchCondition = "%". $searchQuery . '%';
            $query = $query->where('projects.project_name','LIKE', $searchCondition );
        }

        $items = $query->where('projects.is_active', '=', 1)
        ->where('projects.is_deleted', '=', 0)
        ->get();
        
        /*********************************************************************/
        
        $count =DB::table('projects')
                ->join('workflows', 'projects.workflow_id', '=', 'workflows.workflow_id')
                ->select('projects.project_id')
                ->where('projects.project_name','LIKE', $searchCondition )
                ->where('projects.is_active', '=', 1)
                ->where('projects.is_deleted', '=', 0)
                ->count();
        $ret = ['projects'=>$items,'count'=>count($items), 'totalProjects'=>$count];
        return  $ret;
    }

    /**
     * Get project details using id
     */
    public function getProjectData($input){        
        $projectDetail  =   $this->getProjectDetailsWithRelations($input['project_id']);
        if($projectDetail) {
            $res = $projectDetail->toArray();
            unset($res['pref_document']);
            unset($res['items']);
            if ($res['project_type'] == 1) {
                $sub_query = DB::table('workflow_stage')
                    ->select('stage_name as current_workflow_stage_name');
                $sub_query->where('workflow_stage.workflow_stage_id', '=', $res['current_workflow_stage_id']);
                $res['current_workflow_stage'] = $sub_query->get()->first();
                if ($res) {
                    return $res;
                }
                return [];
            } else if($res['project_type'] == 3){
                  $userId         = $input['auth_user']['user_id'];
                  $arrData= $this->getStatus($input['project_id'],$userId);
                  $res['review_status'] = 1;
                  foreach ($arrData as $key => $data) {                      
                      if($input['project_id'] ==$data['project_id']) {
                          $res['review_status'] = $data['review_status'];
                      }
                  }
                  if($res) {
                      return $res;
                  }
            }else {
                $sub_query = DB::table('workflow_stage')
                    ->select('stage_name as current_workflow_stage_name');
                $sub_query->where('workflow_stage.workflow_stage_id', '=', $res['current_workflow_stage_id']);
                $res['current_workflow_stage'] = $sub_query->get()->first();

                $documentsAssociated = $projectDetail->prefDocument->toArray();

                foreach ($documentsAssociated as $documents) {
                    $res['project_documents_details'][] = ['document_id' => $documents['document_id'], 'title' => $documents['title']];
                }

                if ($res) {
                    return $res;
                }
                return [];
            }
        }
        
    }

    public function getStatus($projectId,$userId)
    {
        $query = PublicReview::where('project_id',$projectId)
                              ->where('user_id', $userId)->get();
        if (!$query) {
            $res = [];
            return $res;
        }
        $data = $query->toArray();
        return $data;
    }

    /**
     * Get project details using taxonomy_id
     */
    public function getProjectDataByTaxonomy($input){
        $res = $this->findByAttributes(['taxonomy_id'=>$input['taxonomy_id']])->first();
        if (count($res)){
            return $res;
        }
        return [];
    }

    /**
     * This method will create a new mapping record with project and taxonomy
     */
    public function associateItemWithProject(string $taxonomyIdentifier, string $projectIdentifier) {
        $project = $this->find($projectIdentifier);
        $project->items()->attach($taxonomyIdentifier);
    }

    /**
     * This method will create a new mapping record with project and taxonomy
     */
    public function updateAllParentItemToNonEditable(array $arrayOfParentId, string $projectIdentifier,string $documentId) {
        $tableName = "project_items";
        $projectArrParentItem = []; 
        foreach($arrayOfParentId as $arrayOfParentIdV)
        {
            if($arrayOfParentIdV != $documentId)
            {
                $projectArrParentItem[] = [
                    "project_id" => $projectIdentifier,
                    "item_id" => $arrayOfParentIdV,
                    "is_editable" => 0,
                    "is_deleted" => 0,
                    "created_at" => now()->toDateTimeString(),
                    "updated_at" => now()->toDateTimeString(),
                ];
            }
           
        }
        if(count($projectArrParentItem) > 0)
        {
                $projectItemParentInsertArr = array_chunk($projectArrParentItem,1000);
                foreach($projectItemParentInsertArr as $projectItemParentInsertArrK => $projectItemParentInsertArrV)
                {
                    DB::table('project_items')->insert($projectItemParentInsertArrV);
                }
        }
        return 1;
    }

    /**
     * This method will check for editability of the associated node
     */
    public function checkIsItemEditable(string $itemIdentifier, $projectIdentifier) {
        $tableName = "project_items";
        $queryToExecute = DB::table($tableName)->select("is_editable")->where(array("item_id" => $itemIdentifier));

        if($projectIdentifier != ''){
            $queryToExecute->whereIn("project_id", $projectIdentifier);
        }

        $editableFlag = $queryToExecute->get();
        if(isset($editableFlag[0]->is_editable) && $editableFlag[0]->is_editable === 1){
            return 1;
        }
        elseif(isset($editableFlag[0]->is_editable) && $editableFlag[0]->is_editable === 0){
            return 0;
        }
        else{
            return 2;
        }
    }

    /**
     * This method returns the list of nodes associated with 
     * project
     * @return array $nodesArray
     */
    public function getMappedNodes($projectId) {
        $nodesArray = [];
        //Get Project Detail
        $project = $this->find($projectId);
        //Select mapped nodes for the project
        $mappedNodes = $project->items;
        //Create array of nodes id
        foreach($mappedNodes as $key => $value){
            $nodesArray[$key]['item_id'] = $value->item_id ;
        }

        return $nodesArray;
    }

    /**
     * This method returns the list of documents associated with 
     * project
     * @return array $nodesArray
     */
    public function getMappedDocument($projectId) {
        $nodesArray = [];
        //Get Project Detail
        $project = $this->find($projectId);
        //Select mapped nodes for the project
        $mappedDocuments = $project->prefDocument;
        //Create array of nodes id
        foreach($mappedDocuments as $key => $value){
            $nodesArray[$key]['document_id'] = $value->document_id ;
        }

        return $nodesArray;
    }

    /**
     * return active project record count
     */
    public function getActiveProjectsCount(): int {
        $returnActive = true;
        return $this->getProjectCount($returnActive);
    }

    /**
     * Fetch project item mapping from pivot table for specified items
     */
    public function getProjectItemMappingOfMultipleItems(array $itemIds) {
        $fieldsToReturn = [ "project_items.project_id", "project_items.item_id", "project_items.is_editable" ];
        $tableName = "project_items";
        $query = DB::table($tableName)->select($fieldsToReturn)->whereIn("item_id", $itemIds)
        ->join('projects', 'projects.project_id', '=', 'project_items.project_id')
        ->where('projects.project_type','1');         
        $data = $query->get();
        return $data;
    }

    public function getProjectItemMappings($itemId)
    {
        $tableName = "project_items";
        $query = DB::table($tableName)
                 ->select('project_id')
                 ->where("item_id",$itemId)
                 ->where('is_deleted',0)
                 ->first();
        $projectId = isset($query->project_id)?$query->project_id:'';

        $projectType = Project::select('project_type','project_id')
                      ->where('project_id',$projectId)
                      ->where('is_deleted',0)
                      ->get()
                      ->toArray();
        return $projectType;
    }

    /**
     * Fetch Project name for specified projects
     */
    public function getNameOfMultipleProjects(array $projectIds) {
        $attributeIn = "project_id";
        $containedInValues = $projectIds;
        $returnCollection = true;
        $fieldsToReturn = ["project_id", "project_name"];
        return $this->findByAttributesWithSpecifiedFieldsWithInQuery($attributeIn, $projectIds, $returnCollection, $fieldsToReturn);
    }

    /**
     * This method will return document_id for a project
     */
    public function getDocumentId(string $projectIdentifier) {
        $returnCollection = false;
        $fieldsToReturn = ["document_id"];
        $conditionalKeyValuePair = ["project_id" =>  $projectIdentifier ];
        return $this->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair)->document_id;
    }

    /**
     * This method will return project items for a project
     */
    public function getProjectItems(string $projectIdentifier) {
        $fieldsToReturn = ["item_id"];
        $tableName = "project_items";
        $query = DB::table($tableName)->select($fieldsToReturn)->where("project_id", $projectIdentifier)->where("is_deleted", 0);
        $data = $query->get();
        return $data;
    }


    public function deleteProjectItem($taxonomyIdentifier){
        $tableName = "project_items";
        $query = DB::table($tableName)->where(['item_id' => $taxonomyIdentifier])->delete();
        return $query;
    }
    
    /**
     * This method returns the list of users associated with 
     * project
     * @return array $userIds
     */
    public function getMappedUsers($projectId) {
        $tableName = "project_users";
        
        $result = DB::table($tableName)
        ->where(array('project_id' => $projectId, 'is_deleted' => '0'))
        ->get();

        $userId = [];

        $userId = [
            'user_id' => collect($result)->map(function ($item, $key) {
                return $item->user_id;
            })->toArray(),
        ];
        
        return $userId;
    }

    public function findWokflowStageID($workflow_id){
        $result = DB::table('workflow_stage')
        ->select('workflow_stage_id')
        ->where('workflow_id', $workflow_id)
        ->where('order', 1)
        ->get();
        return $result;
    }
    
    /**
     * Assign Multiple User to Role in a project
     */
    public function checkUserToProjectExists($projectIdentifier, $userId, $workflowStageRoleId){
        $userExists = [];
        $tableName = "project_users";
        
        $result = DB::table($tableName)
        ->where(array('project_id' => $projectIdentifier, 'user_id' => $userId, 'workflow_stage_role_id' => $workflowStageRoleId, 'is_deleted' => '0'))
        ->get();

        if($result->count() > 0)
        {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Assign Multiple User to Role in a project
     */
    public function assignUserToProject($attributes) {
        return DB::table('project_users')->insert(array($attributes));
    }

    /**
     * UnAssign One User From One Role in a project
     */
    public function unAssignUserFromProject($projectIdentifier, $attributes,$organizationId) {
        $tableName = "project_users";

        $dataFound = DB::table($tableName)
        ->where(array('project_id' => $projectIdentifier, 'user_id' => $attributes['user_id'], 'workflow_stage_role_id' => $attributes['workflow_stage_role_id'],'is_deleted'=>0))
        ->get()->toArray();

        if(!empty($dataFound)) {
            return DB::table($tableName)
                ->where(array('project_id' => $projectIdentifier, 'user_id' => $attributes['user_id'], 'workflow_stage_role_id' => $attributes['workflow_stage_role_id'],'organization_id'=>$dataFound[0]->organization_id))
                ->update(['is_deleted' => '1','deleted_by'=>$attributes['deleted_by']]);
        }

        return false;
    }

    //Count project for users
    public function projectCount($model, $mapColumn, $identifierSet, $fieldsToCount, $keyValuePairs){
        $usageCount = [];

        foreach($identifierSet as $identifier){
            $query = DB::table($model)
                ->select($fieldsToCount);

            foreach ($keyValuePairs as $key => $value) {
                $query->where($key, $value);
            }            
            
            $query->where(array($model.'.'.$mapColumn => $identifier))->groupBy($model.'.'.$fieldsToCount);
            $result = $query->get()->toArray();

            $usageCount[$identifier] = sizeOf($result);
        }
        
        return $usageCount;
    }

    /**
     * Method to return array of item ids assigned to a project id
     *
     * @param string $projectIdentifier
     * @return array
     */
    public function getItemIdsAssignedToProject(string $projectIdentifier): array {
        $itemIdsToReturn = [];
        $tempProjectData = [ "project_id" => $projectIdentifier ];
        $temProjectModel = $this->createModelInstanceInMemory($tempProjectData);
        $projectItems = $temProjectModel->items()->where("project_id", $projectIdentifier)->select("items.item_id")->get();
        if($projectItems->isNotempty()) {
            $itemIdsToReturn = $projectItems->pluck("item_id")->toArray();
        }
        return $itemIdsToReturn;
    }

    public function deleteProjectIdAndDocumentId($projectId){
        $removeProjectMappingwithDocument = DB::table('project_pref_documents')
                ->where('project_id',$projectId)
                ->update(['is_deleted'=>'1']);
    }

    public function getProjectListIdByProjectType(){

        $projectIdList=DB::table('projects')
                ->select('project_id')
                ->where('project_type','=','3')
                ->where('is_deleted','=','0')
                ->get()
                ->toArray();
         
        $projectId=array();
        foreach($projectIdList as $projectIdListK=>$projectIdListV){
            foreach($projectIdListV as $projectIdListVKey=>$projectIdListVData){
                $projectId[]=$projectIdListVData;
            }
        }
               
        return $projectId;        
    }

    public function getPubicReviewTaxonomy($organization_id){

        $projectIdList=DB::table('projects')
                ->select('document_id','project_name')
                ->where('project_type','=','3')
                ->where('is_deleted','=','0')
                ->where('organization_id','=',$organization_id)
                ->get()
                ->toArray();
         
                       
        return $projectIdList;        
    }

    public function fetchPublicReviewHistoryProjectList(string $organizationIdentifier, array $statusArray) {
        $projectIdList=DB::table('public_review_histories')
                ->select('document_id','project_id','status')
                ->where('status', $statusArray)
                ->where('is_deleted','=','0')
                ->where('organization_id','=',$organizationIdentifier)
                ->get();
         
                       
        return $projectIdList; 
    }

    public function fetchFilteredProjectPRList($projectIdArray ) {
        $projectIdList=DB::table('public_review_histories')
                ->select('project_id','status')
                ->whereIn('project_id', $projectIdArray)
                ->where('is_deleted','=','0')
                ->get();
         
                       
        return $projectIdList;
    }

    public function getItemComplianceReport($itemsAndDocMappedWithProject,$organization_id)
    {   
        $itemArray = array();
        $docArray = array();
        foreach($itemsAndDocMappedWithProject as $itemsAndDocMappedWithProjectK=>$itemsAndDocMappedWithProjectV)
        {
            if($itemsAndDocMappedWithProjectV['is_doc']=="0")
            {
                $itemArray[] = $itemsAndDocMappedWithProjectV["item_doc_id"];
            }
            else if($itemsAndDocMappedWithProjectV['is_doc']=="1")
            {
                $docArray[] = $itemsAndDocMappedWithProjectV["item_doc_id"];
            }
        }
        
        $getItemDetail =$this->getItemDetail($itemArray);
        $getDocDetail = $this->getDocDetail($docArray);
        
        $DocMetadatadataIdForCase = $this->getDocMetadatadataIdForCase($organization_id);
        $NonDocMetadatadataIdForCase = $this->getNonDocMetadatadataIdForCase($organization_id);

        $getItemIdMappedWithNodeId = $this->getItemIdMappedWithNodeId($getItemDetail);
        $getDocIdMappedWithNodeId = $this->getDocIdMappedWithNodeId($getDocDetail);

        $getItemNodeIdMetadataId = $this->getItemNodeIdMetadataId($getItemIdMappedWithNodeId,$NonDocMetadatadataIdForCase);
        $getDocNodeIdMetadataId = $this->getDocNodeIdMetadataId($getDocIdMappedWithNodeId,$DocMetadatadataIdForCase);

        $getItemNodeIdMappedWithMetadataId = $this->getItemNodeIdMappedWithMetadataId($getItemNodeIdMetadataId);
        $getDocNodeIdMappedWithMetadataId = $this->getDocNodeIdMappedWithMetadataId($getDocNodeIdMetadataId);
        
        $getItemMetadataDetail = $this->getItemMetadataDetail($getItemNodeIdMappedWithMetadataId);
        $getDocMetadataDetail = $this->getDocMetadataDetail($getDocNodeIdMappedWithMetadataId);

        $getKeyValuePairForItemDetail = $this->getKeyValuePairForItemDetail($getItemDetail);
        $getKeyValuePairForDocDetail = $this->getKeyValuePairForDocDetail($getDocDetail);

        $getKeyValuePairForItemMetadataDetail = $this->getKeyValuePairForItemMetadataDetail($getItemMetadataDetail);
        $getKeyValuePairForDocMetadataDetail = $this->getKeyValuePairForDocMetadataDetail($getDocMetadataDetail);

        /** Get Mandatory Status for Item and Doc Start */
        $getItemMandatoryStatus = $this->getItemMandatoryStatus($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail);
        $getDocMandatoryStatus = $this->getDocMandatoryStatus($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail);
        /** Get Mandatory Status for Item and Doc End */

        /** Get Mandatory Status for Concept License Language For Item Start */
        $getItemNodeIdMappedWithConceptId = $this->getItemNodeIdMappedWithConceptId($getItemDetail);
        $getConceptIdForItem = $this->getConceptIdForItem($getItemNodeIdMappedWithConceptId);
        $getItemConceptDetail = $this->getItemConceptDetail($getConceptIdForItem);
        $getKeyValuePairForConceptItemDetail = $this->getKeyValuePairForConceptItemDetail($getItemConceptDetail);
        $getItemMandatoryStatusForConcept = $this->getItemMandatoryStatusForConcept($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForConceptItemDetail);
        
        $getItemNodeIdMappedWithLicenseId = $this->getItemNodeIdMappedWithLicenseId($getItemDetail);
        $getLicenseIdForItem = $this->getLicenseIdForItem($getItemNodeIdMappedWithLicenseId);
        $getItemLicenseDetail = $this->getItemLicenseDetail($getLicenseIdForItem);
        $getKeyValuePairForLicenseItemDetail = $this->getKeyValuePairForLicenseItemDetail($getItemLicenseDetail);
        $getItemMandatoryStatusForLicense = $this->getItemMandatoryStatusForLicense($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForLicenseItemDetail);

        $getItemNodeIdMappedWithLanguageId = $this->getItemNodeIdMappedWithLanguageId($getItemDetail);
        $getLanguageIdForItem = $this->getLanguageIdForItem($getItemNodeIdMappedWithLanguageId);
        $getItemLanguageDetail = $this->getItemLanguageDetail($getLanguageIdForItem);
        $getKeyValuePairForLanguageItemDetail = $this->getKeyValuePairForLanguageItemDetail($getItemLanguageDetail);
        $getItemMandatoryStatusForLanguage = $this->getItemMandatoryStatusForLanguage($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForLanguageItemDetail);
        /**Get Mandatory Status for Concept License Language For Item End */

        /**Get Mandatory Status for Subject License Language For Doc Start */
        $checkDocSubjectIdExist = $this->checkDocSubjectIdExist($getDocDetail);
        $getDocMandatoryStatusForSubject = array();
        if(count($checkDocSubjectIdExist)>0)
        {
            $getDocSubjectDetail = $this->getDocSubjectDetail($checkDocSubjectIdExist);
            $getKeyValuePairForSubjectItemDetail = $this->getKeyValuePairForSubjectItemDetail($getDocSubjectDetail);
            $getDocMandatoryStatusForSubject = $this->getDocMandatoryStatusForSubjectIfNotEmpty($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForSubjectItemDetail);
        }
        else
        {
            $getDocMandatoryStatusForSubject = $this->getDocMandatoryStatusForSubject($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail);
        }

        
        $checkDocLicenseIdExist = $this->checkDocLicenseIdExist($getDocDetail);
        $getDocMandatoryStatusForLicense = array();
        if(!empty($checkDocLicenseIdExist[0]))
        {
            $getDocLicenseDetail = $this->getDocLicenseDetail($getDocDetail);
            $getKeyValuePairForLicenseDocDetail = $this->getKeyValuePairForLicenseDocDetail($getDocLicenseDetail);
            $getDocMandatoryStatusForLicense = $this->getDocMandatoryStatusForLicenseIfNotEmpty($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForLicenseDocDetail);
        }
        else
        {
            $getDocMandatoryStatusForLicense = $this->getDocMandatoryStatusForLicense($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail);
        }

        
        $getDocNodeIdMappedWithLanguageId = $this->getDocNodeIdMappedWithLanguageId($getDocDetail);
        $getLanguageIdForDoc = $this->getLanguageIdForDoc($getDocNodeIdMappedWithLanguageId);
        $getDocLanguageDetail = $this->getDocLanguageDetail($getLanguageIdForDoc);
        $getKeyValuePairForLanguageDocDetail = $this->getKeyValuePairForLanguageDocDetail($getDocLanguageDetail);
        $getDocMandatoryStatusForLanguage = $this->getDocMandatoryStatusForLanguage($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForLanguageDocDetail);
        /**Get Mandatory Status for Subject License Language For Doc End */


        /** Get Mandatory Status for Item Custom Start */
        $getItemIdToFetchCustomMetadataDetail = $this->getItemIdToFetchCustomMetadataDetail($getItemDetail);
        $getItemCustomMetadataDetail = $this->getItemCustomMetadataDetail($getItemIdToFetchCustomMetadataDetail);
        $getKeyValuePairForItemCustomMetadataDetail = $this->getKeyValuePairForItemCustomMetadataDetail($getItemCustomMetadataDetail);
        $getItemMandatoryStatusForCustom = $this->getItemMandatoryStatusForCustom($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForItemCustomMetadataDetail);
        /** Get Mandatory Status for Item Custom End */
        
        /** Get Mandatory Status for Doc Custom Start */
        $getDocIdToFetchCustomMetadataDetail = $this->getDocIdToFetchCustomMetadataDetail($getDocDetail);
        $getDocCustomMetadataDetail = $this->getDocCustomMetadataDetail($getDocIdToFetchCustomMetadataDetail);
        $getKeyValuePairForDocCustomMetadataDetail = $this->getKeyValuePairForDocCustomMetadataDetail($getDocCustomMetadataDetail);
        $getDocMandatoryStatusForCustom = $this->getDocMandatoryStatusForCustom($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForDocCustomMetadataDetail);
        /** Get Mandatory Status for Doc Custom End */

        /** Get Compliance Report For Item Level Start */
        $getComplianceReportForItemLevelData = $this->getComplianceReportForItemData($getKeyValuePairForItemDetail,$getItemMandatoryStatus,$getItemMandatoryStatusForConcept,$getItemMandatoryStatusForLicense,$getItemMandatoryStatusForLanguage,$getItemMandatoryStatusForCustom);
        /** Get Compliance Report For Item Level End */

        /** Get Compliance Report For Doc Level Start */
        $getComplianceReportForDocLevelData = $this->getComplianceReportForDocLevelData($getKeyValuePairForDocDetail,$getDocMandatoryStatus,$getDocMandatoryStatusForSubject,$getDocMandatoryStatusForLicense,$getDocMandatoryStatusForLanguage,$getDocMandatoryStatusForCustom);
        /** Get Compliance Report For Doc Level Start */
        
        return array_merge($getComplianceReportForItemLevelData,$getComplianceReportForDocLevelData);

    }

    /** Get Item Detail And Doc Detail Start */
    public function getItemDetail($itemArray)
    {   
        $getItemDetail = array();
        if(count($itemArray)>0)
        {
            $getItemDetail=DB::table('items')
                    ->whereIn('item_id',$itemArray)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        }
        
        return $getItemDetail;
    }
    
    public function getDocDetail($docArray)
    {
        $getDocDetail = array();
        if(count($docArray)>0)
        {
            $getDocDetail=DB::table('documents')
                ->whereIn('document_id',$docArray)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
        }

        return $getDocDetail;
    }
    /** Get Item Detail And Doc Detail End */

    
    /** Get Item Mapped With Node And Doc Mapped With Node Start */
    public function getItemIdMappedWithNodeId($getItemDetail)
    {   
        $getItemIdMappedWithNodeId = array();
        if(count($getItemDetail)>0)
        {
            foreach($getItemDetail as $getItemDetailK=>$getItemDetailV)
            {
                $getItemIdMappedWithNodeId[$getItemDetailV->item_id]=$getItemDetailV->node_type_id;
            }
        }

        return $getItemIdMappedWithNodeId;
    }

    public function getDocIdMappedWithNodeId($getDocDetail)
    {
        $getDocIdMappedWithNodeId = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailK=>$getDocDetailV)
            {
                $getDocIdMappedWithNodeId[$getDocDetailV->document_id]=$getDocDetailV->node_type_id;
            }
        }

        return $getDocIdMappedWithNodeId;
    }
    /** Get Item Mapped With Node And Doc Mapped With Node End */

    /** Get Node Id and Metadata Id Detail for Item and Doc Start */
    public function getItemNodeIdMetadataId($getItemIdMappedWithNodeId,$NonDocMetadatadataIdForCase)
    {
        $getItemNodeIdMetadataId = array();
        if(count($getItemIdMappedWithNodeId)>0)
        {
            $getItemNodeIdMetadataId=DB::table('node_type_metadata')
                ->whereIn('node_type_id',$getItemIdMappedWithNodeId)
                ->where('is_mandatory','1')
                ->orWhereIn('metadata_id',$NonDocMetadatadataIdForCase)
                ->get()
                ->toArray();
        }

        return $getItemNodeIdMetadataId;
    }

    public function getDocNodeIdMetadataId($getDocIdMappedWithNodeId,$DocMetadatadataIdForCase)
    {

        $getDocNodeIdMetadataId = array();
        if(count($getDocIdMappedWithNodeId)>0)
        {
            $getDocNodeIdMetadataId=DB::table('node_type_metadata')

                ->whereIn('node_type_id',$getDocIdMappedWithNodeId)
                ->where('is_mandatory','1')
                ->orWhereIn('metadata_id',$DocMetadatadataIdForCase)
                ->get()
                ->toArray();
        }
        return $getDocNodeIdMetadataId;
    }
    /** Get Node Id and Metadata Id Detail for Item and Doc End */


    /** Get Node Id Mapped With Metadata Id for Item and Doc Start */
    public function getItemNodeIdMappedWithMetadataId($getItemNodeIdMetadataId)
    {
        $getItemNodeIdMappedWithMetadataId = array();
        if(count($getItemNodeIdMetadataId)>0)
        {   $a=0;
            foreach($getItemNodeIdMetadataId as $getItemNodeIdMetadataIdK=>$getItemNodeIdMetadataIdV)
            {
               $getItemNodeIdMappedWithMetadataId[$a][$getItemNodeIdMetadataIdV->node_type_id]=$getItemNodeIdMetadataIdV->metadata_id;
               $a=$a+1; 
            }
        }

        return $getItemNodeIdMappedWithMetadataId;
    }

    public function getDocNodeIdMappedWithMetadataId($getDocNodeIdMetadataId)
    {
        $getDocNodeIdMappedWithMetadataId = array();
        if(count($getDocNodeIdMetadataId)>0)
        {
            $b=0;
            foreach($getDocNodeIdMetadataId as $getDocNodeIdMetadataIdK=>$getDocNodeIdMetadataIdV)
            {
               $getDocNodeIdMappedWithMetadataId[$b][$getDocNodeIdMetadataIdV->node_type_id]=$getDocNodeIdMetadataIdV->metadata_id;
               $b=$b+1; 
            }
        }

        return $getDocNodeIdMappedWithMetadataId;
    }
    /** Get Node Id Mapped With Metadata Id for Item and Doc End */

    /** Get Item and Doc Metadata Detail Start */
    public function getItemMetadataDetail($getItemNodeIdMappedWithMetadataId)
    {
        $getItemMetadataDetail = array();
        if(count($getItemNodeIdMappedWithMetadataId)>0)
        {
            $getItemMetadataDetail=DB::table('metadata')
                ->whereIn('metadata_id',$getItemNodeIdMappedWithMetadataId)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
        }

        return $getItemMetadataDetail;
    }

    public function getDocMetadataDetail($getDocNodeIdMappedWithMetadataId)
    {
        $getDocMetadataDetail = array();
        if(count($getDocNodeIdMappedWithMetadataId)>0)
        {
            $getDocMetadataDetail=DB::table('metadata')
                ->whereIn('metadata_id',$getDocNodeIdMappedWithMetadataId)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
        }
        return $getDocMetadataDetail;
    }
    /** Get Item and Doc Metadata Detail End */
        /** get IMS Case Doc Level Metadata Start */
        public function getDocMetadatadataIdForCase($organization_id)
        {
            $internal_name = ['creator','title']; 
            $getDocMetadatadataIdForCase=DB::table('metadata')
                    ->select('metadata_id')
                    ->where('is_deleted','0')
                    ->where('organization_id',$organization_id)
                    ->whereIn('internal_name',$internal_name)
                    ->get()
                    ->toArray();              
                foreach($getDocMetadatadataIdForCase as $getDocMetadatadataIdForCaseV)
                {
                    $metdataForCaseDoc[] = $getDocMetadatadataIdForCaseV->metadata_id;
                }
                    return $metdataForCaseDoc;
        }
        /** get IMS Case Doc Level Metadata End */
     
            /** get IMS Case Not Doc Level Metadata Start */
            public function getNonDocMetadatadataIdForCase($organization_id)
            {
                $internal_name = ['full_statement']; 
                $getNonDocMetadatadataIdForCase=DB::table('metadata')
                        ->select('metadata_id')
                        ->where('is_deleted','0')
                        ->where('organization_id',$organization_id)
                        ->whereIn('internal_name',$internal_name)
                        ->get()
                        ->toArray();
                        foreach($getNonDocMetadatadataIdForCase as $getNonDocMetadatadataIdForCaseV)
                        {
                            $metdataForCaseNonDoc[] = $getNonDocMetadatadataIdForCaseV->metadata_id;
                        }
                            return $metdataForCaseNonDoc;
            }
            /** get IMS Case Not Doc Level Metadata End */ 
    /** Get Key Value Pair for ItemDetail and DocDetail Start */
    public function getKeyValuePairForItemDetail($getItemDetail)
    {
        $getKeyValuePairForItemDetail = array();
        if(count($getItemDetail)>0)
        {   
            foreach($getItemDetail as $getItemDetailKey=>$getItemDetailValue)
            {
                $getKeyValuePairForItemDetail[] = [
                                                    "item_id"=>$getItemDetailValue->item_id,
                                                    "parent_id"=>$getItemDetailValue->parent_id,
                                                    "document_id"=>$getItemDetailValue->document_id,
                                                    "full_statement"=>$getItemDetailValue->full_statement,
                                                    "alternative_label"=>$getItemDetailValue->alternative_label,
                                                    "item_type_id"=>$getItemDetailValue->item_type_id,
                                                    "human_coding_scheme"=>$getItemDetailValue->human_coding_scheme,
                                                    "list_enumeration"=>$getItemDetailValue->list_enumeration,
                                                    "abbreviated_statement"=>$getItemDetailValue->abbreviated_statement,
                                                    "concept_id"=>$getItemDetailValue->concept_id,
                                                    "notes"=>$getItemDetailValue->notes,
                                                    "language_id"=>$getItemDetailValue->language_id,
                                                    "education_level"=>$getItemDetailValue->education_level,
                                                    "license_id"=>$getItemDetailValue->license_id,
                                                    "status_start_date"=>$getItemDetailValue->status_start_date,
                                                    "status_end_date"=>$getItemDetailValue->status_end_date,
                                                    "is_deleted"=>$getItemDetailValue->is_deleted,
                                                    "updated_by"=>$getItemDetailValue->updated_by,
                                                    "organization_id"=>$getItemDetailValue->organization_id,
                                                    "source_item_id"=>$getItemDetailValue->source_item_id,
                                                    "node_type_id"=>$getItemDetailValue->node_type_id,
                                                    "created_at"=>$getItemDetailValue->created_at,
                                                    "updated_at"=>$getItemDetailValue->updated_at
                                                ];
            }                                  
        }

        return $getKeyValuePairForItemDetail;
    }

    public function getKeyValuePairForDocDetail($getDocDetail)
    {
        $getKeyValuePairForDocDetail = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailKey=>$getDocDetailValue)
            {
                $getKeyValuePairForDocDetail[] = [
                                                    "document_id"=>$getDocDetailValue->document_id,
                                                    "title"=>$getDocDetailValue->title,
                                                    "official_source_url"=>$getDocDetailValue->official_source_url,
                                                    "language_id"=>$getDocDetailValue->language_id,
                                                    "adoption_status"=>$getDocDetailValue->adoption_status,
                                                    "status"=>$getDocDetailValue->status,
                                                    "notes"=>$getDocDetailValue->notes,
                                                    "publisher"=>$getDocDetailValue->publisher,
                                                    "description"=>$getDocDetailValue->description,
                                                    "version"=>$getDocDetailValue->version,
                                                    "status_start_date"=>$getDocDetailValue->status_start_date,
                                                    "status_end_date"=>$getDocDetailValue->status_end_date,
                                                    "license_id"=>$getDocDetailValue->license_id,
                                                    "organization_id"=>$getDocDetailValue->organization_id,
                                                    "document_type"=>$getDocDetailValue->document_type,
                                                    "creator"=>$getDocDetailValue->creator,
                                                    "is_deleted"=>$getDocDetailValue->is_deleted,
                                                    "created_by"=>$getDocDetailValue->created_by,
                                                    "updated_by"=>$getDocDetailValue->updated_by,
                                                    "source_document_id"=>$getDocDetailValue->source_document_id,
                                                    "pulished_taxonomy_snapshot_s3_zip_file_id"=>$getDocDetailValue->pulished_taxonomy_snapshot_s3_zip_file_id,
                                                    "node_type_id"=>$getDocDetailValue->node_type_id,
                                                    "project_id"=>$getDocDetailValue->project_id,
                                                    "node_template_id"=>$getDocDetailValue->node_template_id,
                                                    "created_at"=>$getDocDetailValue->created_at,
                                                    "updated_at"=>$getDocDetailValue->updated_at
                                                 ];
            }
        }

        return $getKeyValuePairForDocDetail;

    }
    /** Get Key Value Pair for ItemDetail and DocDetail End */

    /** Get Key Value Pair for Metadata Detail for Item and Doc Start */
    public function getKeyValuePairForItemMetadataDetail($getItemMetadataDetail)
    {
        $getKeyValuePairForItemMetadataDetail = array();
        if(count($getItemMetadataDetail)>0)
        {
            foreach($getItemMetadataDetail as $getItemMetadataDetailK=>$getItemMetadataDetailV)
            {
                $getKeyValuePairForItemMetadataDetail[] = [
                                                        "metadata_id"=>$getItemMetadataDetailV->metadata_id,
                                                        "organization_id"=>$getItemMetadataDetailV->organization_id,
                                                        "parent_metadata_id"=>$getItemMetadataDetailV->parent_metadata_id,
                                                        "name"=>$getItemMetadataDetailV->name,
                                                        "internal_name"=>$getItemMetadataDetailV->internal_name,
                                                        "field_type"=>$getItemMetadataDetailV->field_type,
                                                        "field_possible_values"=>$getItemMetadataDetailV->field_possible_values,
                                                        "last_field_possible_values"=>$getItemMetadataDetailV->last_field_possible_values,
                                                        "order"=>$getItemMetadataDetailV->order,
                                                        "is_custom"=>$getItemMetadataDetailV->is_custom,
                                                        "is_document"=>$getItemMetadataDetailV->is_document,
                                                        "is_mandatory"=>$getItemMetadataDetailV->is_mandatory,
                                                        "is_active"=>$getItemMetadataDetailV->is_active,
                                                        "is_deleted"=>$getItemMetadataDetailV->is_deleted,
                                                        "updated_by"=>$getItemMetadataDetailV->updated_by, 
                                                        "created_at"=>$getItemMetadataDetailV->created_at,
                                                        "updated_at"=>$getItemMetadataDetailV->updated_at
                                                        ];
                }                                          
        }

        return $getKeyValuePairForItemMetadataDetail;
    }

    public function getKeyValuePairForDocMetadataDetail($getDocMetadataDetail)
    {
        $getKeyValuePairForDocMetadataDetail = array();
        if(count($getDocMetadataDetail)>0)
        {
            foreach($getDocMetadataDetail as $getDocMetadataDetailK=>$getDocMetadataDetailV)
            {
                $getKeyValuePairForDocMetadataDetail[] = [
                                                            "metadata_id"=>$getDocMetadataDetailV->metadata_id,
                                                            "organization_id"=>$getDocMetadataDetailV->organization_id,
                                                            "parent_metadata_id"=>$getDocMetadataDetailV->parent_metadata_id,
                                                            "name"=>$getDocMetadataDetailV->name,
                                                            "internal_name"=>$getDocMetadataDetailV->internal_name,
                                                            "field_type"=>$getDocMetadataDetailV->field_type,
                                                            "field_possible_values"=>$getDocMetadataDetailV->field_possible_values,
                                                            "last_field_possible_values"=>$getDocMetadataDetailV->last_field_possible_values,
                                                            "order"=>$getDocMetadataDetailV->order,
                                                            "is_custom"=>$getDocMetadataDetailV->is_custom,
                                                            "is_document"=>$getDocMetadataDetailV->is_document,
                                                            "is_mandatory"=>$getDocMetadataDetailV->is_mandatory,
                                                            "is_active"=>$getDocMetadataDetailV->is_active,
                                                            "is_deleted"=>$getDocMetadataDetailV->is_deleted,
                                                            "updated_by"=>$getDocMetadataDetailV->updated_by, 
                                                            "created_at"=>$getDocMetadataDetailV->created_at,
                                                            "updated_at"=>$getDocMetadataDetailV->updated_at
                                                        ];
            }
        }

        return $getKeyValuePairForDocMetadataDetail;
    }
    /** Get Key Value Pair for Metadata Detail for Item and Doc End */

    /** Get Mandatory Status for Item and Doc Start */
    public function getItemMandatoryStatus($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail)
    {
        // 0 = completely filled , 1 = partially filled
        $itemFieldsToExclude = array('concept_title','concept_keywords','concept_hierarchy_code','concept_description','license_title','license_description','license_text','language');
        $getItemMandatoryStatus = array();

        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailK=>$getKeyValuePairForItemDetailV)
        {
            foreach($getItemNodeIdMappedWithMetadataId as $getItemNodeIdMappedWithMetadataIdK=>$getItemNodeIdMappedWithMetadataIdV)
            {   
                foreach($getItemNodeIdMappedWithMetadataIdV as $getItemNodeIdMappedWithMetadataIdVData=>$getItemNodeIdMappedWithMetadataIdVInfo)
                {
                    if($getKeyValuePairForItemDetailV['node_type_id']==$getItemNodeIdMappedWithMetadataIdVData)
                    {
                        foreach($getKeyValuePairForItemMetadataDetail as $getKeyValuePairForItemMetadataDetailK=>$getKeyValuePairForItemMetadataDetailV)
                        {
                            if($getItemNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForItemMetadataDetailV['metadata_id'] && $getKeyValuePairForItemMetadataDetailV['is_custom']==0)
                            {
                                if(!in_array($getKeyValuePairForItemMetadataDetailV['internal_name'],$itemFieldsToExclude))
                                {
                                    if($getKeyValuePairForItemDetailV[$getKeyValuePairForItemMetadataDetailV['internal_name']]=="")
                                    {
                                        $getItemMandatoryStatus[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['internal_name']]=1;
                                    }
                                    else
                                    {
                                        $getItemMandatoryStatus[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['internal_name']]=0;
                                    }
                                }
                            }
                        }
                    }
                }
             
            }
        }

        return $getItemMandatoryStatus;
        
    }

    public function getDocMandatoryStatus($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail)
    {

        // 0 = completely filled , 1 = partially filled
        $docFieldsToExclude = array('subject_title','subject_hierarchy_code','subject_description','license_title','license_description','license_text','language');
        $getDocMandatoryStatus = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailK=>$getKeyValuePairForDocDetailV)
        {
            foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdK=>$getDocNodeIdMappedWithMetadataIdV)
            {
                foreach($getDocNodeIdMappedWithMetadataIdV as $getDocNodeIdMappedWithMetadataIdVData=>$getDocNodeIdMappedWithMetadataIdVInfo)
                {
                    if($getKeyValuePairForDocDetailV['node_type_id']==$getDocNodeIdMappedWithMetadataIdVData)
                    {
                        foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailK=>$getKeyValuePairForDocMetadataDetailV)
                        {
                            if($getDocNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForDocMetadataDetailV['metadata_id'] && $getKeyValuePairForDocMetadataDetailV['is_custom']==0)
                            {
                                if(!in_array($getKeyValuePairForDocMetadataDetailV['internal_name'],$docFieldsToExclude))
                                {
                                    if($getKeyValuePairForDocDetailV[$getKeyValuePairForDocMetadataDetailV['internal_name']]=="" || $getKeyValuePairForDocDetailV[$getKeyValuePairForDocMetadataDetailV['internal_name']]=="0000-00-00 00:00:00")
                                    {
                                        $getDocMandatoryStatus[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['internal_name']]=1;
                                    }
                                    else
                                    {
                                        $getDocMandatoryStatus[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['internal_name']]=0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return $getDocMandatoryStatus;

    }
    /** Get Mandatory Status for Item and Doc End */

    /** Get NodeId Mapped With Concept Id(Item Level) Start */
    public function getItemNodeIdMappedWithConceptId($getItemDetail)
    {
        $getItemNodeIdMappedWithConceptId = array();
        if(count($getItemDetail)>0)
        {
            foreach($getItemDetail as $getItemDetailCK=>$getItemDetailCV)
            {
                $getItemNodeIdMappedWithConceptId[$getItemDetailCV->concept_id]=$getItemDetailCV->node_type_id;
            }
        }

        return $getItemNodeIdMappedWithConceptId;
    }

    /** Get NodeId Mapped With Concept Id(Item Level) End */

    /** Get Concept Id for Item Start*/
    public function getConceptIdForItem($getItemNodeIdMappedWithConceptId)
    {
        $getConceptIdForItem = array();
        if(count($getItemNodeIdMappedWithConceptId)>0)
        {
            foreach($getItemNodeIdMappedWithConceptId as $getItemNodeIdMappedWithConceptIdK=>$getItemNodeIdMappedWithConceptIdV)
            {
                $getConceptIdForItem[]=$getItemNodeIdMappedWithConceptIdK;
            }
        }

        return $getConceptIdForItem;
    }
    /** Get Concept Id for Item End*/

    /** Get Concept Detail for Item Start */
    public function getItemConceptDetail($getConceptIdForItem)
    {
        $getItemConceptDetail = array();
        if(count($getConceptIdForItem)>0)
        {
            $getItemConceptDetail = DB::table('concepts')
                                    ->whereIn('concept_id',$getConceptIdForItem)
                                    ->where('is_deleted','0')
                                    ->get()
                                    ->toArray();
        }

        return $getItemConceptDetail;
    }
    /** Get Concept Detail for Item Start */

    /** Get Key Value Pair of Concept Detail For Item Start */
    public function getKeyValuePairForConceptItemDetail($getItemConceptDetail)
    {
        $getKeyValuePairForConceptItemDetail = array();
        if(count($getItemConceptDetail)>0)
        {
            foreach($getItemConceptDetail as $getItemConceptDetailK=>$getItemConceptDetailV)
            {
                $getKeyValuePairForConceptItemDetail[]=[
                                                        "concept_id"=>$getItemConceptDetailV->concept_id,
                                                        "title"=>$getItemConceptDetailV->title,
                                                        "keywords"=>$getItemConceptDetailV->keywords,
                                                        "hierarchy_code"=>$getItemConceptDetailV->hierarchy_code,
                                                        "description"=>$getItemConceptDetailV->description,
                                                        "is_deleted"=>$getItemConceptDetailV->is_deleted,
                                                        "source_concept_id"=>$getItemConceptDetailV->source_concept_id,
                                                        "organization_id"=>$getItemConceptDetailV->organization_id,
                                                        "created_at"=>$getItemConceptDetailV->created_at,
                                                        "updated_at"=>$getItemConceptDetailV->updated_at
                                                        ];
            }
        }

        return $getKeyValuePairForConceptItemDetail;
    }
    /** Get Key Value Pair of Concept Detail For Item End */

    /** Get Mandatory Status of Item For Concept Start */
    public function getItemMandatoryStatusForConcept($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForConceptItemDetail)
    {
        $itemconceptFieldsToConsider = array('concept_title','concept_keywords','concept_hierarchy_code','concept_description');
        $getItemMandatoryStatusForConcept = array();

        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailKey=>$getKeyValuePairForItemDetailValue)
        {
            foreach($getKeyValuePairForConceptItemDetail as $getKeyValuePairForConceptItemDetailKey=>$getKeyValuePairForConceptItemDetailValue)
            {   
                if($getKeyValuePairForItemDetailValue['concept_id']==$getKeyValuePairForConceptItemDetailValue['concept_id'])
                {
                    foreach($getItemNodeIdMappedWithMetadataId as $getItemNodeIdMappedWithMetadataIdKey=>$getItemNodeIdMappedWithMetadataIdValue)
                    {
                        foreach($getItemNodeIdMappedWithMetadataIdValue as $getItemNodeIdMappedWithMetadataIdValueData=>$getItemNodeIdMappedWithMetadataIdValueInfo)
                        {
                            if($getKeyValuePairForItemDetailValue['node_type_id']==$getItemNodeIdMappedWithMetadataIdValueData)
                            {
                                foreach($getKeyValuePairForItemMetadataDetail as $getKeyValuePairForItemMetadataDetailKey=>$getKeyValuePairForItemMetadataDetailValue)
                                {
                                    if($getItemNodeIdMappedWithMetadataIdValueInfo==$getKeyValuePairForItemMetadataDetailValue['metadata_id'] && $getKeyValuePairForItemMetadataDetailValue['is_custom']==0)
                                    {
                                        if(in_array($getKeyValuePairForItemMetadataDetailValue['internal_name'],$itemconceptFieldsToConsider))
                                        {   
                                            $conceptField=str_replace('concept_','', $getKeyValuePairForItemMetadataDetailValue['internal_name']);
                                            if($getKeyValuePairForConceptItemDetailValue[$conceptField]=="")
                                            {
                                                $getItemMandatoryStatusForConcept[$getKeyValuePairForItemDetailValue['item_id']][$getKeyValuePairForItemMetadataDetailValue['internal_name']]=1;
                                            }
                                            else
                                            {
                                                $getItemMandatoryStatusForConcept[$getKeyValuePairForItemDetailValue['item_id']][$getKeyValuePairForItemMetadataDetailValue['internal_name']]=0;    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $getItemMandatoryStatusForConcept;

    }
    /** Get Mandatory Status of Item For Concept End */

    /** Get NodeId Mapped With License Id(Item Level) Start */
    public function getItemNodeIdMappedWithLicenseId($getItemDetail)
    {
        $getItemNodeIdMappedWithLicenseId = array();
        if(count($getItemDetail)>0)
        {
            foreach($getItemDetail as $getItemDetailCK=>$getItemDetailCV)
            {
                $getItemNodeIdMappedWithLicenseId[$getItemDetailCV->license_id]=$getItemDetailCV->node_type_id;
            }
        }
        
        return $getItemNodeIdMappedWithLicenseId;
    }
    /** Get NodeId Mapped With License Id(Item Level) End */

    /** Get License Id for Item Start*/
    public function getLicenseIdForItem($getItemNodeIdMappedWithLicenseId)
    {
        $getLicenseIdForItem = array();
        if(count($getItemNodeIdMappedWithLicenseId)>0)
        {
            foreach($getItemNodeIdMappedWithLicenseId as $getItemNodeIdMappedWithLicenseIdK=>$getItemNodeIdMappedWithLicenseIdV)
            {
                $getLicenseIdForItem[]=$getItemNodeIdMappedWithLicenseIdK;
            }
        }
        
        return $getLicenseIdForItem;
    }
    /** Get License Id for Item End*/

    /** Get License Detail for Item Start */
    public function getItemLicenseDetail($getLicenseIdForItem)
    {
        $getItemLicenseDetail = array();
        if(count($getLicenseIdForItem)>0)
        {
            $getItemLicenseDetail = DB::table('licenses')
                                    ->whereIn('license_id',$getLicenseIdForItem)
                                    ->where('is_deleted','0')
                                    ->get()
                                    ->toArray();
        }
        
        return $getItemLicenseDetail;
    }
    /** Get License Detail for Item End */

    /** Get Key Value Pair of License Detail For Item Start */
    public function getKeyValuePairForLicenseItemDetail($getItemLicenseDetail)
    {   
        $getKeyValuePairForLicenseItemDetail = array();
        if(count($getItemLicenseDetail)>0)
        {
            foreach($getItemLicenseDetail as $getItemLicenseDetailK=>$getItemLicenseDetailV)
            {
                $getKeyValuePairForLicenseItemDetail[]=[
                                                        "license_id"=>$getItemLicenseDetailV->license_id,
                                                        "title"=>$getItemLicenseDetailV->title,
                                                        "description"=>$getItemLicenseDetailV->description,
                                                        "license_text"=>$getItemLicenseDetailV->license_text,
                                                        "is_deleted"=>$getItemLicenseDetailV->is_deleted,
                                                        "source_license_id"=>$getItemLicenseDetailV->source_license_id,
                                                        "organization_id"=>$getItemLicenseDetailV->organization_id,
                                                        "created_at"=>$getItemLicenseDetailV->created_at,
                                                        "updated_at"=>$getItemLicenseDetailV->updated_at
                                                        ];
                                                        
            }
        }
        
        return $getKeyValuePairForLicenseItemDetail;
    }
    /** Get Key Value Pair of License Detail For Item End */

    /** Get Mandatory Status of Item For License Start */
    public function getItemMandatoryStatusForLicense($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForLicenseItemDetail)
    {
        $itemlicenseFieldsToConsider = array('license_title','license_description','license_text');
        $getItemMandatoryStatusForLicense = array();

        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailKey=>$getKeyValuePairForItemDetailValue)
        {
            foreach($getKeyValuePairForLicenseItemDetail as $getKeyValuePairForLicenseItemDetailKey=>$getKeyValuePairForLicenseItemDetailValue)
            {   
                if($getKeyValuePairForItemDetailValue['license_id']==$getKeyValuePairForLicenseItemDetailValue['license_id'])
                {
                    foreach($getItemNodeIdMappedWithMetadataId as $getItemNodeIdMappedWithMetadataIdKey=>$getItemNodeIdMappedWithMetadataIdValue)
                    {
                        foreach($getItemNodeIdMappedWithMetadataIdValue as $getItemNodeIdMappedWithMetadataIdValueData=>$getItemNodeIdMappedWithMetadataIdValueInfo)
                        {
                            if($getKeyValuePairForItemDetailValue['node_type_id']==$getItemNodeIdMappedWithMetadataIdValueData)
                            {
                                foreach($getKeyValuePairForItemMetadataDetail as $getKeyValuePairForItemMetadataDetailKey=>$getKeyValuePairForItemMetadataDetailValue)
                                {
                                    if($getItemNodeIdMappedWithMetadataIdValueInfo==$getKeyValuePairForItemMetadataDetailValue['metadata_id'] && $getKeyValuePairForItemMetadataDetailValue['is_custom']==0)
                                    {
                                        if(in_array($getKeyValuePairForItemMetadataDetailValue['internal_name'],$itemlicenseFieldsToConsider))
                                        {   
                                            if($getKeyValuePairForItemMetadataDetailValue['internal_name']!='license_text')
                                            {
                                                $conceptField=str_replace('license_','', $getKeyValuePairForItemMetadataDetailValue['internal_name']);
                                            }
                                            else
                                            {
                                                $conceptField=$getKeyValuePairForItemMetadataDetailValue['internal_name'];
                                            }
                                            
                                            
                                            if($getKeyValuePairForLicenseItemDetailValue[$conceptField]=="")
                                            {
                                                $getItemMandatoryStatusForLicense[$getKeyValuePairForItemDetailValue['item_id']][$getKeyValuePairForItemMetadataDetailValue['internal_name']]=1;
                                            }
                                            else
                                            {
                                                $getItemMandatoryStatusForLicense[$getKeyValuePairForItemDetailValue['item_id']][$getKeyValuePairForItemMetadataDetailValue['internal_name']]=0;    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return $getItemMandatoryStatusForLicense;

    }
    /** Get Mandatory Status of Item For License End */

    /** LANGUAGE START */
    /** Get NodeId Mapped With Language Id(Item Level) Start */
    public function getItemNodeIdMappedWithLanguageId($getItemDetail)
    {
        $getItemNodeIdMappedWithLanguageId = array();
        if(count($getItemDetail)>0)
        {
            foreach($getItemDetail as $getItemDetailCK=>$getItemDetailCV)
            {
                $getItemNodeIdMappedWithLanguageId[$getItemDetailCV->language_id]=$getItemDetailCV->node_type_id;
            }
        }
        
        return $getItemNodeIdMappedWithLanguageId;
    }
    /** Get NodeId Mapped With Language Id(Item Level) End */

    /** Get Language Id for Item Start*/
    public function getLanguageIdForItem($getItemNodeIdMappedWithLanguageId)
    {
        $getLanguageIdForItem = array();
        if(count($getItemNodeIdMappedWithLanguageId)>0)
        {
            foreach($getItemNodeIdMappedWithLanguageId as $getItemNodeIdMappedWithLanguageIdK=>$getItemNodeIdMappedWithLanguageIdV)
            {
                $getLanguageIdForItem[]=$getItemNodeIdMappedWithLanguageIdK;
            }
        }
        
        return $getLanguageIdForItem;
    }
    /** Get Language Id for Item End*/

    /** Get Language Detail for Item Start */
    public function getItemLanguageDetail($getLanguageIdForItem)
    {
        $getItemLanguageDetail = array();
        if(count($getLanguageIdForItem)>0)
        {
            $getItemLanguageDetail = DB::table('languages')
                                    ->whereIn('language_id',$getLanguageIdForItem)
                                    ->where('is_deleted','0')
                                    ->get()
                                    ->toArray();
        }
        
        return $getItemLanguageDetail;
    }
    /** Get Language Detail for Item End */

    /** Get Key Value Pair of Language Detail For Item Start */
    public function getKeyValuePairForLanguageItemDetail($getItemLanguageDetail)
    {   
        $getKeyValuePairForLanguageItemDetail = array();
        if(count($getItemLanguageDetail)>0)
        {
            foreach($getItemLanguageDetail as $getItemLanguageDetailK=>$getItemLanguageDetailV)
            {
                $getKeyValuePairForLanguageItemDetail[]=[
                                                        "language_id"=>$getItemLanguageDetailV->language_id,
                                                        "name"=>$getItemLanguageDetailV->name,
                                                        "short_code"=>$getItemLanguageDetailV->short_code,
                                                        "is_deleted"=>$getItemLanguageDetailV->is_deleted,
                                                        "organization_id"=>$getItemLanguageDetailV->organization_id,
                                                        "created_at"=>$getItemLanguageDetailV->created_at,
                                                        "updated_at"=>$getItemLanguageDetailV->updated_at
                                                        ];
                                                        
            }
        }
        
        return $getKeyValuePairForLanguageItemDetail;
    }
    /** Get Key Value Pair of Language Detail For Item End */

    /** Get Mandatory Status of Item For Language Start */
    public function getItemMandatoryStatusForLanguage($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail)
    {
        // 0 = completely filled , 1 = partially filled
        $itemlanguageFieldsToConsider = array('language');
        $getItemMandatoryStatusForLanguage = array();

        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailK=>$getKeyValuePairForItemDetailV)
        {
            foreach($getItemNodeIdMappedWithMetadataId as $getItemNodeIdMappedWithMetadataIdK=>$getItemNodeIdMappedWithMetadataIdV)
            {   
                foreach($getItemNodeIdMappedWithMetadataIdV as $getItemNodeIdMappedWithMetadataIdVData=>$getItemNodeIdMappedWithMetadataIdVInfo)
                {
                    if($getKeyValuePairForItemDetailV['node_type_id']==$getItemNodeIdMappedWithMetadataIdVData)
                    {
                        foreach($getKeyValuePairForItemMetadataDetail as $getKeyValuePairForItemMetadataDetailK=>$getKeyValuePairForItemMetadataDetailV)
                        {
                            if($getItemNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForItemMetadataDetailV['metadata_id'] && $getKeyValuePairForItemMetadataDetailV['is_custom']==0)
                            {
                                if(in_array($getKeyValuePairForItemMetadataDetailV['internal_name'],$itemlanguageFieldsToConsider))
                                {   
                                    if($getKeyValuePairForItemDetailV['language_id']=="")
                                    {
                                        $getItemMandatoryStatusForLanguage[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['internal_name']]=1;
                                    }
                                    else
                                    {
                                        $getItemMandatoryStatusForLanguage[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['internal_name']]=0;
                                    }
                                }
                            }
                        }
                    }
                }
             
            }
        }

        return $getItemMandatoryStatusForLanguage;
    }
    /** Get Mandatory Status of Item For Language End */
    /** LANGUAGE END */

    /** DOC SUBJECT STARTS */
    public function checkDocSubjectIdExist($getDocDetail)
    {
        $getDocumentId = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailData=>$getDocDetailInfo)
            {
                $getDocumentId[]=$getDocDetailInfo->document_id;
            }
        }
        
        $checkDocSubjectIdExist = array();
        if(count($getDocumentId)>0)
        {
            $checkDocSubjectIdExist = DB::table('document_subject')
                                    ->whereIn('document_id',$getDocumentId)
                                    ->get()
                                    ->toArray();
        }

        return $checkDocSubjectIdExist;

    }

    public function getDocSubjectDetail($checkDocSubjectIdExist)
    {
        $getSubjectId = array();
        if(count($checkDocSubjectIdExist)>0)
        {
            foreach($checkDocSubjectIdExist as $checkDocSubjectIdExistK=>$checkDocSubjectIdExistV)
            {
                $getSubjectId[]=$checkDocSubjectIdExistV->subject_id;
            }
        }

        $getDocSubjectDetail = array();
        if(count($getSubjectId)>0)
        {
            $getDocSubjectDetail = DB::table('subjects')
                                    ->whereIn('subject_id',$getSubjectId)
                                    ->get()
                                    ->toArray();
        }

        return $getDocSubjectDetail;
    }

    public function getKeyValuePairForSubjectItemDetail($getDocSubjectDetail)
    {
        $getKeyValuePairForSubjectItemDetail = array();
        if(count($getDocSubjectDetail)>0)
        {
            foreach($getDocSubjectDetail as $getDocSubjectDetailK=>$getDocSubjectDetailV)
            {
                $getKeyValuePairForSubjectItemDetail[] = [
                                                            "subject_id"=>$getDocSubjectDetailV->subject_id,
                                                            "title"=>$getDocSubjectDetailV->title,
                                                            "hierarchy_code"=>$getDocSubjectDetailV->hierarchy_code,
                                                            "description"=>$getDocSubjectDetailV->description,
                                                            "is_deleted"=>$getDocSubjectDetailV->is_deleted,
                                                            "source_subject_id"=>$getDocSubjectDetailV->source_subject_id,
                                                            "organization_id"=>$getDocSubjectDetailV->organization_id,
                                                            "created_at"=>$getDocSubjectDetailV->created_at,
                                                            "updated_at"=>$getDocSubjectDetailV->updated_at
                                                         ];
            }
        }

        return $getKeyValuePairForSubjectItemDetail;
    }

    public function getDocMandatoryStatusForSubject($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail)
    {
        $docsubjectFieldsToConsider = array('subject_title','subject_hierarchy_code','subject_description');
        $getDocMandatoryStatusForSubject = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailK=>$getKeyValuePairForDocDetailV)
        {
            foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdK=>$getDocNodeIdMappedWithMetadataIdV)
            {
                foreach($getDocNodeIdMappedWithMetadataIdV as $getDocNodeIdMappedWithMetadataIdVData=>$getDocNodeIdMappedWithMetadataIdVInfo)
                {
                    if($getKeyValuePairForDocDetailV['node_type_id']==$getDocNodeIdMappedWithMetadataIdVData)
                    {
                        foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailK=>$getKeyValuePairForDocMetadataDetailV)
                        {
                            if($getDocNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForDocMetadataDetailV['metadata_id'] && $getKeyValuePairForDocMetadataDetailV['is_custom']==0)
                            {
                                if(in_array($getKeyValuePairForDocMetadataDetailV['internal_name'],$docsubjectFieldsToConsider))
                                {
                                    
                                    $getDocMandatoryStatusForSubject[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['internal_name']]=1;
                                    
                                }
                            }
                        }
                    }
                }
            }
        }

        return $getDocMandatoryStatusForSubject;

    }

    public function getDocMandatoryStatusForSubjectIfNotEmpty($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForSubjectItemDetail)
    {   
        
        $docsubjectFieldsToConsider = array('subject_title','subject_hierarchy_code','subject_description');
        $getDocMandatoryStatusForSubjectIfNotEmpty = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailKey=>$getKeyValuePairForDocDetailValue)
        {
            foreach($getKeyValuePairForSubjectItemDetail as $getKeyValuePairForSubjectItemDetailKey=>$getKeyValuePairForSubjectItemDetailValue)
            {   
                foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdKey=>$getDocNodeIdMappedWithMetadataIdValue)
                    {
                        foreach($getDocNodeIdMappedWithMetadataIdValue as $getDocNodeIdMappedWithMetadataIdValueData=>$getDocNodeIdMappedWithMetadataIdValueInfo)
                        {
                            if($getKeyValuePairForDocDetailValue['node_type_id']==$getDocNodeIdMappedWithMetadataIdValueData)
                            {
                                foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailKey=>$getKeyValuePairForDocMetadataDetailValue)
                                {
                                    if($getDocNodeIdMappedWithMetadataIdValueInfo==$getKeyValuePairForDocMetadataDetailValue['metadata_id'] && $getKeyValuePairForDocMetadataDetailValue['is_custom']==0)
                                    {
                                        if(in_array($getKeyValuePairForDocMetadataDetailValue['internal_name'],$docsubjectFieldsToConsider))
                                        {   
                                            $subjectField=str_replace('subject_','', $getKeyValuePairForDocMetadataDetailValue['internal_name']);
                                            if($getKeyValuePairForSubjectItemDetailValue[$subjectField]=="")
                                            {
                                                $getDocMandatoryStatusForSubjectIfNotEmpty[$getKeyValuePairForDocDetailValue['document_id']][$getKeyValuePairForDocMetadataDetailValue['internal_name']]=1;
                                            }
                                            else
                                            {
                                                $getDocMandatoryStatusForSubjectIfNotEmpty[$getKeyValuePairForDocDetailValue['document_id']][$getKeyValuePairForDocMetadataDetailValue['internal_name']]=0;    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }

        return $getDocMandatoryStatusForSubjectIfNotEmpty;

    }
    /** DOC SUBJECT ENDS */

    /** DOC LICENSE STARTS */
    public function checkDocLicenseIdExist($getDocDetail)
    {
        $checkDocLicenseIdExist = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailData=>$getDocDetailInfo)
            {
                $checkDocLicenseIdExist[]=$getDocDetailInfo->license_id;
            }
        }

        return $checkDocLicenseIdExist;
    }

    public function getDocLicenseDetail($getDocDetail)
    {
        $getLicenseId = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailK=>$getDocDetailV)
            {
                $getLicenseId[]=$getDocDetailV->license_id;
            }
        }

        $getDocLicenseDetail = array();
        if(count($getLicenseId)>0)
        {
            $getDocLicenseDetail = DB::table('licenses')
                                    ->whereIn('license_id',$getLicenseId)
                                    ->get()
                                    ->toArray();
        }

        return $getDocLicenseDetail;   
    }

    public function getKeyValuePairForLicenseDocDetail($getDocLicenseDetail)
    {
        $getKeyValuePairForLicenseDocDetail = array();
        if(count($getDocLicenseDetail)>0)
        {
            foreach($getDocLicenseDetail as $getDocLicenseDetailK=>$getDocLicenseDetailV)
            {
                $getKeyValuePairForLicenseDocDetail[] = [
                                                            "license_id"=>$getDocLicenseDetailV->license_id,
                                                            "title"=>$getDocLicenseDetailV->title,
                                                            "description"=>$getDocLicenseDetailV->description,
                                                            "license_text"=>$getDocLicenseDetailV->license_text,
                                                            "is_deleted"=>$getDocLicenseDetailV->is_deleted,
                                                            "source_license_id"=>$getDocLicenseDetailV->source_license_id,
                                                            "organization_id"=>$getDocLicenseDetailV->organization_id,
                                                            "created_at"=>$getDocLicenseDetailV->created_at,
                                                            "updated_at"=>$getDocLicenseDetailV->updated_at
                                                         ];
            }
        }

        return $getKeyValuePairForLicenseDocDetail;
    }

    public function getDocMandatoryStatusForLicense($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail)
    {
        $doclicenseFieldsToConsider = array('license_title','license_description','license_text');
        $getDocMandatoryStatusForLicense = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailK=>$getKeyValuePairForDocDetailV)
        {
            foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdK=>$getDocNodeIdMappedWithMetadataIdV)
            {
                foreach($getDocNodeIdMappedWithMetadataIdV as $getDocNodeIdMappedWithMetadataIdVData=>$getDocNodeIdMappedWithMetadataIdVInfo)
                {
                    if($getKeyValuePairForDocDetailV['node_type_id']==$getDocNodeIdMappedWithMetadataIdVData)
                    {
                        foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailK=>$getKeyValuePairForDocMetadataDetailV)
                        {
                            if($getDocNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForDocMetadataDetailV['metadata_id'] && $getKeyValuePairForDocMetadataDetailV['is_custom']==0)
                            {
                                if(in_array($getKeyValuePairForDocMetadataDetailV['internal_name'],$doclicenseFieldsToConsider))
                                {
                                    $getDocMandatoryStatusForLicense[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['internal_name']]=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $getDocMandatoryStatusForLicense;
    }

    public function getDocMandatoryStatusForLicenseIfNotEmpty($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForLicenseDocDetail)
    {
        $doclicenseFieldsToConsider = array('license_title','license_description','license_text');
        $getDocMandatoryStatusForLicenseIfNotEmpty = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailKey=>$getKeyValuePairForDocDetailValue)
        {
            foreach($getKeyValuePairForLicenseDocDetail as $getKeyValuePairForLicenseDocDetailKey=>$getKeyValuePairForLicenseDocDetailValue)
            {   
                foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdKey=>$getDocNodeIdMappedWithMetadataIdValue)
                    {
                        foreach($getDocNodeIdMappedWithMetadataIdValue as $getDocNodeIdMappedWithMetadataIdValueData=>$getDocNodeIdMappedWithMetadataIdValueInfo)
                        {
                            if($getKeyValuePairForDocDetailValue['node_type_id']==$getDocNodeIdMappedWithMetadataIdValueData)
                            {
                                foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailKey=>$getKeyValuePairForDocMetadataDetailValue)
                                {
                                    if($getDocNodeIdMappedWithMetadataIdValueInfo==$getKeyValuePairForDocMetadataDetailValue['metadata_id'] && $getKeyValuePairForDocMetadataDetailValue['is_custom']==0)
                                    {
                                        if(in_array($getKeyValuePairForDocMetadataDetailValue['internal_name'],$doclicenseFieldsToConsider))
                                        {   
                                            if($getKeyValuePairForDocMetadataDetailValue['internal_name']!='license_text')
                                            {
                                                $licenseField=str_replace('license_','', $getKeyValuePairForDocMetadataDetailValue['internal_name']);
                                            }
                                            else
                                            {
                                                $licenseField=$getKeyValuePairForDocMetadataDetailValue['internal_name'];
                                            }

                                            
                                            if($getKeyValuePairForLicenseDocDetailValue[$licenseField]=="")
                                            {
                                                $getDocMandatoryStatusForLicenseIfNotEmpty[$getKeyValuePairForDocDetailValue['document_id']][$getKeyValuePairForDocMetadataDetailValue['internal_name']]=1;
                                            }
                                            else
                                            {
                                                $getDocMandatoryStatusForLicenseIfNotEmpty[$getKeyValuePairForDocDetailValue['document_id']][$getKeyValuePairForDocMetadataDetailValue['internal_name']]=0;    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }

        return $getDocMandatoryStatusForLicenseIfNotEmpty;

    }

    /** DOC LICENSE ENDS */

    /** DOC LANGUAGE STARTS */
    public function getDocNodeIdMappedWithLanguageId($getDocDetail)
    {
        $getDocNodeIdMappedWithLanguageId = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailCK=>$getDocDetailCV)
            {
                $getDocNodeIdMappedWithLanguageId[$getDocDetailCV->language_id]=$getDocDetailCV->node_type_id;
            }
        }
        
        return $getDocNodeIdMappedWithLanguageId;
    }

    public function getLanguageIdForDoc($getDocNodeIdMappedWithLanguageId)
    {
        $getLanguageIdForDoc = array();
        if(count($getDocNodeIdMappedWithLanguageId)>0)
        {
            foreach($getDocNodeIdMappedWithLanguageId as $getDocNodeIdMappedWithLanguageIdK=>$getDocNodeIdMappedWithLanguageIdV)
            {
                $getLanguageIdForDoc[]=$getDocNodeIdMappedWithLanguageIdK;
            }
        }
        
        return $getLanguageIdForDoc;
    }

    public function getDocLanguageDetail($getLanguageIdForDoc)
    {
        $getDocLanguageDetail = array();
        if(count($getLanguageIdForDoc)>0)
        {
            $getDocLanguageDetail = DB::table('languages')
                                    ->whereIn('language_id',$getLanguageIdForDoc)
                                    ->where('is_deleted','0')
                                    ->get()
                                    ->toArray();
        }
        
        return $getDocLanguageDetail;
    }

    public function getKeyValuePairForLanguageDocDetail($getDocLanguageDetail)
    {
        $getKeyValuePairForLanguageDocDetail = array();
        if(count($getDocLanguageDetail)>0)
        {
            foreach($getDocLanguageDetail as $getDocLanguageDetailK=>$getDocLanguageDetailV)
            {
                $getKeyValuePairForLanguageDocDetail[]=[
                                                        "language_id"=>$getDocLanguageDetailV->language_id,
                                                        "name"=>$getDocLanguageDetailV->name,
                                                        "short_code"=>$getDocLanguageDetailV->short_code,
                                                        "is_deleted"=>$getDocLanguageDetailV->is_deleted,
                                                        "organization_id"=>$getDocLanguageDetailV->organization_id,
                                                        "created_at"=>$getDocLanguageDetailV->created_at,
                                                        "updated_at"=>$getDocLanguageDetailV->updated_at
                                                        ];
                                                        
            }
        }
        
        return $getKeyValuePairForLanguageDocDetail; 
    }

    public function getDocMandatoryStatusForLanguage($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForLanguageDocDetail)
    {
        $doclanguageFieldsToConsider = array('language');
        $getDocMandatoryStatusForLanguage = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailK=>$getKeyValuePairForDocDetailV)
        {
            foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdK=>$getDocNodeIdMappedWithMetadataIdV)
            {   
                foreach($getDocNodeIdMappedWithMetadataIdV as $getDocNodeIdMappedWithMetadataIdIdVData=>$getDocNodeIdMappedWithMetadataIdIdVInfo)
                {
                    if($getKeyValuePairForDocDetailV['node_type_id']==$getDocNodeIdMappedWithMetadataIdIdVData)
                    {
                        foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailK=>$getKeyValuePairForDocMetadataDetailV)
                        {
                            if($getDocNodeIdMappedWithMetadataIdIdVInfo==$getKeyValuePairForDocMetadataDetailV['metadata_id'] && $getKeyValuePairForDocMetadataDetailV['is_custom']==0)
                            {
                                if(in_array($getKeyValuePairForDocMetadataDetailV['internal_name'],$doclanguageFieldsToConsider))
                                {   
                                    if($getKeyValuePairForDocDetailV['language_id']=="")
                                    {
                                        $getDocMandatoryStatusForLanguage[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['internal_name']]=1;
                                    }
                                    else
                                    {
                                        $getDocMandatoryStatusForLanguage[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['internal_name']]=0;
                                    }
                                }
                            }
                        }
                    }
                }
             
            }
        }
        
        return $getDocMandatoryStatusForLanguage;
    }
    /** DOC LANGUAGE ENDS */

    /** Get Mandatory Status for Custom Metadata for Item Start */
    public function getItemIdToFetchCustomMetadataDetail($getItemDetail)
    {
        $getItemIdToFetchCustomMetadataDetail = array();
        if(count($getItemDetail)>0)
        {
            foreach($getItemDetail as $getItemDetailKD=>$getItemDetailKV)
            {
                $getItemIdToFetchCustomMetadataDetail[] = $getItemDetailKV->item_id;
            }
        }

        return $getItemIdToFetchCustomMetadataDetail;
    }

    public function getItemCustomMetadataDetail($getItemIdToFetchCustomMetadataDetail)
    {
        $getItemCustomMetadataDetail = array();
        if(count($getItemIdToFetchCustomMetadataDetail)>0)
        {
            $getItemCustomMetadataDetail = DB::table('item_metadata')
                                    ->whereIn('item_id',$getItemIdToFetchCustomMetadataDetail)
                                    ->where('is_deleted','0')
                                    ->get()
                                    ->toArray();
        }

        return $getItemCustomMetadataDetail;
    }

    public function getKeyValuePairForItemCustomMetadataDetail($getItemCustomMetadataDetail)
    {
        $getKeyValuePairForItemCustomMetadataDetail = array();
        if(count($getItemCustomMetadataDetail)>0)
        {
            foreach($getItemCustomMetadataDetail as $getItemCustomMetadataDetailK=>$getItemCustomMetadataDetailV)
            {
                $getKeyValuePairForItemCustomMetadataDetail[]=[
                                                                "item_id"=>$getItemCustomMetadataDetailV->item_id,
                                                                "metadata_id"=>$getItemCustomMetadataDetailV->metadata_id,
                                                                "metadata_value"=>$getItemCustomMetadataDetailV->metadata_value,
                                                                "is_deleted"=>$getItemCustomMetadataDetailV->is_deleted
                                                                ];

            }
        }

        return $getKeyValuePairForItemCustomMetadataDetail;
    }

    public function getItemMandatoryStatusForCustom($getKeyValuePairForItemDetail,$getItemNodeIdMappedWithMetadataId,$getKeyValuePairForItemMetadataDetail,$getKeyValuePairForItemCustomMetadataDetail)
    {
        
        $getItemMandatoryStatusForCustom = array();
        $getItemMandatoryStatusForCustomAddedLaterOn = array();

        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailK=>$getKeyValuePairForItemDetailV)
        {
            foreach($getItemNodeIdMappedWithMetadataId as $getItemNodeIdMappedWithMetadataIdK=>$getItemNodeIdMappedWithMetadataIdV)
                    {   
                        foreach($getItemNodeIdMappedWithMetadataIdV as $getItemNodeIdMappedWithMetadataIdVData=>$getItemNodeIdMappedWithMetadataIdVInfo)
                        {
                            if($getKeyValuePairForItemDetailV['node_type_id']==$getItemNodeIdMappedWithMetadataIdVData)
                            {
                                foreach($getKeyValuePairForItemMetadataDetail as $getKeyValuePairForItemMetadataDetailK=>$getKeyValuePairForItemMetadataDetailV)
                                {
                                    if($getItemNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForItemMetadataDetailV['metadata_id'] && $getKeyValuePairForItemMetadataDetailV['is_custom']==1)
                                    {   
                                        $getItemMandatoryStatusForCustomAddedLaterOn[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['name']]=1;

                                        foreach($getKeyValuePairForItemCustomMetadataDetail as $getKeyValuePairForItemCustomMetadataDetailK=>$getKeyValuePairForItemCustomMetadataDetailV)
                                        {
                                            if($getKeyValuePairForItemCustomMetadataDetailV['item_id']==$getKeyValuePairForItemDetailV['item_id'] && $getKeyValuePairForItemCustomMetadataDetailV['metadata_id']==$getKeyValuePairForItemMetadataDetailV['metadata_id'])
                                            {
                                                //$getItemMandatoryStatusForCustom[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemDetailV['full_statement']][$getKeyValuePairForItemMetadataDetailV['name']]=1;
                                                if($getKeyValuePairForItemCustomMetadataDetailV['metadata_value']=="")
                                                {
                                                    $getItemMandatoryStatusForCustom[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['name']]=1;
                                                }
                                                else
                                                {
                                                    $getItemMandatoryStatusForCustom[$getKeyValuePairForItemDetailV['item_id']][$getKeyValuePairForItemMetadataDetailV['name']]=0;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
             
        }
        
        // dump($getItemMandatoryStatusForCustomAddedLaterOn);
        // dump($getItemMandatoryStatusForCustom);
        // $totalCustomData=array();
        // $addedCustomDataInDB=array();
        // $customDataToBeAddedInDB=array();
        // $customDataAddedLaterOn = array();
        $finalItemCustomData=array();
        // if(count($getItemMandatoryStatusForCustomAddedLaterOn)>count($getItemMandatoryStatusForCustom))
        // {

            foreach($getItemMandatoryStatusForCustomAddedLaterOn as $getItemMandatoryStatusForCustomAddedLaterOnK=>$getItemMandatoryStatusForCustomAddedLaterOnV)
            {
                foreach($getItemMandatoryStatusForCustom as $getItemMandatoryStatusForCustomK=>$getItemMandatoryStatusForCustomV)
                {
                    if($getItemMandatoryStatusForCustomAddedLaterOnK==$getItemMandatoryStatusForCustomK)
                    {
                        $customDataAddedLaterOnItem=array_diff_key($getItemMandatoryStatusForCustomAddedLaterOnV,$getItemMandatoryStatusForCustomV);
                        $finalItemCustomData[$getItemMandatoryStatusForCustomK]=array_merge($getItemMandatoryStatusForCustomV,$customDataAddedLaterOnItem);
                    }

                }
            }

            /*foreach($getItemMandatoryStatusForCustomAddedLaterOn as $getItemMandatoryStatusForCustomAddedLaterOnK=>$getItemMandatoryStatusForCustomAddedLaterOnV)
            {
                $totalCustomData[]=$getItemMandatoryStatusForCustomAddedLaterOnK;
            }

            foreach($getItemMandatoryStatusForCustom as $getItemMandatoryStatusForCustomK=>$getItemMandatoryStatusForCustomV)
            {
                $addedCustomDataInDB[]=$getItemMandatoryStatusForCustomK;
            }

            $customDataToBeAddedInDB=array_diff($totalCustomData,$addedCustomDataInDB);

            foreach($customDataToBeAddedInDB as $customDataToBeAddedInDBK=>$customDataToBeAddedInDBV)
            {
                foreach($getItemMandatoryStatusForCustomAddedLaterOn as $getItemMandatoryStatusForCustomAddedLaterOnK=>$getItemMandatoryStatusForCustomAddedLaterOnV)
                {
                    if($customDataToBeAddedInDBV==$getItemMandatoryStatusForCustomAddedLaterOnK)
                    {
                        $customDataAddedLaterOn[$customDataToBeAddedInDBV]['custom_data_added_on_later']=1;
                    }
                }
            }*/

        // }
        
        //$getItemMandatoryStatusForCustomFinal=array_merge($getItemMandatoryStatusForCustom,$customDataAddedLaterOn);
        
        return $finalItemCustomData;

    }
    /** Get Mandatory Status for Custom Metadata for Item End */

    /** Get Mandatory Status for Custom Metadata for Doc Start */
    public function getDocIdToFetchCustomMetadataDetail($getDocDetail)
    {
        $getDocIdToFetchCustomMetadataDetail = array();
        if(count($getDocDetail)>0)
        {
            foreach($getDocDetail as $getDocDetailKD=>$getDocDetailKV)
            {
                $getDocIdToFetchCustomMetadataDetail[] = $getDocDetailKV->document_id;
            }
        }
        
        return $getDocIdToFetchCustomMetadataDetail;
    }

    public function getDocCustomMetadataDetail($getDocIdToFetchCustomMetadataDetail)
    {
        $getDocCustomMetadataDetail = array();
        if(count($getDocIdToFetchCustomMetadataDetail)>0)
        {
            $getDocCustomMetadataDetail = DB::table('document_metadata')
                                    ->whereIn('document_id',$getDocIdToFetchCustomMetadataDetail)
                                    ->get()
                                    ->toArray();
        }
        
        return $getDocCustomMetadataDetail;
    }

    public function getKeyValuePairForDocCustomMetadataDetail($getDocCustomMetadataDetail)
    {
        $getKeyValuePairForDocCustomMetadataDetail = array();
        if(count($getDocCustomMetadataDetail)>0)
        {
            foreach($getDocCustomMetadataDetail as $getDocCustomMetadataDetailK=>$getDocCustomMetadataDetailV)
            {
                $getKeyValuePairForDocCustomMetadataDetail[]=[
                                                                "document_id"=>$getDocCustomMetadataDetailV->document_id,
                                                                "metadata_id"=>$getDocCustomMetadataDetailV->metadata_id,
                                                                "metadata_value"=>$getDocCustomMetadataDetailV->metadata_value,
                                                                ];

            }
        }
        
        return $getKeyValuePairForDocCustomMetadataDetail;
    }

    public function getDocMandatoryStatusForCustom($getKeyValuePairForDocDetail,$getDocNodeIdMappedWithMetadataId,$getKeyValuePairForDocMetadataDetail,$getKeyValuePairForDocCustomMetadataDetail)
    {
        $getDocMandatoryStatusForCustom=array();
        $getDocMandatoryStatusForCustomAddedLaterOn = array();

        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailK=>$getKeyValuePairForDocDetailV)
        {
            foreach($getDocNodeIdMappedWithMetadataId as $getDocNodeIdMappedWithMetadataIdK=>$getDocNodeIdMappedWithMetadataIdV)
            {
                foreach($getDocNodeIdMappedWithMetadataIdV as $getDocNodeIdMappedWithMetadataIdVData=>$getDocNodeIdMappedWithMetadataIdVInfo)
                {
                    if($getKeyValuePairForDocDetailV['node_type_id']==$getDocNodeIdMappedWithMetadataIdVData)
                    {
                        foreach($getKeyValuePairForDocMetadataDetail as $getKeyValuePairForDocMetadataDetailK=>$getKeyValuePairForDocMetadataDetailV)
                        {
                            if($getDocNodeIdMappedWithMetadataIdVInfo==$getKeyValuePairForDocMetadataDetailV['metadata_id'] && $getKeyValuePairForDocMetadataDetailV['is_custom']==1)
                            {
                                $getDocMandatoryStatusForCustomAddedLaterOn[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['name']]=1;

                                foreach($getKeyValuePairForDocCustomMetadataDetail as $getKeyValuePairForDocCustomMetadataDetailK=>$getKeyValuePairForDocCustomMetadataDetailV)
                                {
                                    if($getKeyValuePairForDocCustomMetadataDetailV['document_id']==$getKeyValuePairForDocDetailV['document_id'] && $getKeyValuePairForDocCustomMetadataDetailV['metadata_id']==$getKeyValuePairForDocMetadataDetailV['metadata_id'])
                                    {
                                        if($getKeyValuePairForDocCustomMetadataDetailV['metadata_value']=="")
                                        {
                                            $getDocMandatoryStatusForCustom[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['name']]=1;
                                        }
                                        else
                                        {
                                            $getDocMandatoryStatusForCustom[$getKeyValuePairForDocDetailV['document_id']][$getKeyValuePairForDocMetadataDetailV['name']]=0;
                                        }
                                    }
                                    
                                }

                            }
                        }
                    }
                }
            }
        }

        
        $customDataAddedLaterOnDoc=array();
        $finalDocCustomData=array();
        if(count($getDocMandatoryStatusForCustomAddedLaterOn)>count($finalDocCustomData))
        {
            foreach($getDocMandatoryStatusForCustomAddedLaterOn as $getDocMandatoryStatusForCustomAddedLaterOnK=>$getDocMandatoryStatusForCustomAddedLaterOnV)
            {
                foreach($getDocMandatoryStatusForCustom as $getDocMandatoryStatusForCustomK=>$getDocMandatoryStatusForCustomV)
                {
                    if($getDocMandatoryStatusForCustomAddedLaterOnK==$getDocMandatoryStatusForCustomK)
                    {
                        $customDataAddedLaterOnDoc=array_diff_key($getDocMandatoryStatusForCustomAddedLaterOnV,$getDocMandatoryStatusForCustomV);
                        $finalDocCustomData[$getDocMandatoryStatusForCustomK]=array_merge($getDocMandatoryStatusForCustomV,$customDataAddedLaterOnDoc);
                    }

                }
            }
        }
        
        return $finalDocCustomData;

    }

    /** Get Item Level Compliance Report Start */
    public function getComplianceReportForItemData($getKeyValuePairForItemDetail,$getItemMandatoryStatus,$getItemMandatoryStatusForConcept,$getItemMandatoryStatusForLicense,$getItemMandatoryStatusForLanguage,$getItemMandatoryStatusForCustom)
    {
        
        $getComplianceReportForItemData = array();
        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailInfo=>$getKeyValuePairForItemDetailData)
        {
            foreach($getItemMandatoryStatus as $getItemMandatoryStatusK=>$getItemMandatoryStatusV)
            {
                if($getKeyValuePairForItemDetailData['item_id']==$getItemMandatoryStatusK)
                {   
                    $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=$getItemMandatoryStatusV;
                }
            }

            
            foreach($getItemMandatoryStatusForConcept as $getItemMandatoryStatusForConceptK=>$getItemMandatoryStatusForConceptV)
            {
                if($getKeyValuePairForItemDetailData['item_id']==$getItemMandatoryStatusForConceptK)
                {   
                    if(isset($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']])){
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=array_merge($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']],$getItemMandatoryStatusForConceptV);
                    }else{
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=$getItemMandatoryStatusForConceptV;
                    }
                }
                
            }


            foreach($getItemMandatoryStatusForLicense as $getItemMandatoryStatusForLicenseK=>$getItemMandatoryStatusForLicenseV)
            {
                if($getKeyValuePairForItemDetailData['item_id']==$getItemMandatoryStatusForLicenseK)
                {
                    if(isset($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']])){
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=array_merge($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']],$getItemMandatoryStatusForLicenseV);
                    }else{
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=$getItemMandatoryStatusForLicenseV;
                    }
                }
            }


            foreach($getItemMandatoryStatusForLanguage as $getItemMandatoryStatusForLanguageK=>$getItemMandatoryStatusForLanguageV)
            {
                if($getKeyValuePairForItemDetailData['item_id']==$getItemMandatoryStatusForLanguageK)
                {
                    if(isset($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']])){
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=array_merge($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']],$getItemMandatoryStatusForLanguageV);
                    }else{
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=$getItemMandatoryStatusForLanguageV;
                    }
                }
            }

            foreach($getItemMandatoryStatusForCustom as $getItemMandatoryStatusForCustomK=>$getItemMandatoryStatusForCustomV)
            {
                if($getKeyValuePairForItemDetailData['item_id']==$getItemMandatoryStatusForCustomK)
                {
                    if(isset($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']])){
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=array_merge($getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']],$getItemMandatoryStatusForCustomV);
                    }else{
                        $getComplianceReportForItemData[$getKeyValuePairForItemDetailData['item_id']]=$getItemMandatoryStatusForCustomV;
                    }
                }
            }

        }
        
        /** Add full_statement to item array if not in node/metadata set start */
        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailDataK=>$getKeyValuePairForItemDetailDataV)
        {
            foreach($getComplianceReportForItemData as $getComplianceReportForItemDataFSK=>$getComplianceReportForItemDataFSV)
            {
                if($getKeyValuePairForItemDetailDataV['item_id']==$getComplianceReportForItemDataFSK)
                {
                    $insertFullStatement=array();
                    if(!array_key_exists('full_statement',$getComplianceReportForItemDataFSV))
                    {
                        if($getKeyValuePairForItemDetailDataV['full_statement']=="")
                        {
                            $insertFullStatement=array('full_statement'=>1);
                        }
                        else
                        {
                            $insertFullStatement=array('full_statement'=>0);
                        }
                        
                    }
                    $getComplianceReportForItemDataFSV=array_merge($getComplianceReportForItemDataFSV,$insertFullStatement);
                    $getComplianceReportForItemData[$getKeyValuePairForItemDetailDataV['item_id']]=$getComplianceReportForItemDataFSV;
                }
            }
        }
        
        /** Add full_statement to item array if not in node/metadata set end */
        
        /** Add status as fully non compliance(2) if full_statment is not added in the node/metadata set start*/
        $getItemLevelDataToArray=array();
        foreach($getKeyValuePairForItemDetail as $getKeyValuePairForItemDetailToArrayKey=>$getKeyValuePairForItemDetailToArrayValue)
        {
            $getItemLevelDataToArray[]=$getKeyValuePairForItemDetailToArrayValue['item_id'];
        }
        
        $getComplianceDataToArray=array();
        foreach($getComplianceReportForItemData as $getComplianceReportForItemDataToArrayKey=>$getComplianceReportForItemDataToArrayValue)
        {
            $getComplianceDataToArray[]=$getComplianceReportForItemDataToArrayKey;
        }
        
        $complianceArrayDataDiff=array();
        $complianceArrayDataDiff=array_diff($getItemLevelDataToArray,$getComplianceDataToArray);
        
        $itemFullStatmentStatus=$getKeyValuePairForItemDetail;
        $fullStatementStatus = $this->checkFullStatementStatus($itemFullStatmentStatus,$complianceArrayDataDiff);
        /** Add status as fully non compliance(2) if full_statment is not added in the node/metadata set end*/

        $getComplianceReportItemLevel = array();
        $ic=0;
        foreach($getComplianceReportForItemData as $getComplianceReportForItemDataK=>$getComplianceReportForItemDataV)
        {
            $getItemCount = count($getComplianceReportForItemDataV);
            $getItemSum = array_sum($getComplianceReportForItemDataV);
            
            if($getItemCount==$getItemSum)
            {   
                // completely unfilled item
                $getComplianceReportItemLevel[$ic]['item_id']=$getComplianceReportForItemDataK;
                $getComplianceReportItemLevel[$ic]['status']=2;
            }
            else if($getItemCount>$getItemSum && $getItemSum>0)
            {
                // partially filled item
                $getComplianceReportItemLevel[$ic]['item_id']=$getComplianceReportForItemDataK;
                $getComplianceReportItemLevel[$ic]['status']=1;
            }
            else if($getItemCount>$getItemSum && $getItemSum==0)
            {
                // completely filled item
                $getComplianceReportItemLevel[$ic]['item_id']=$getComplianceReportForItemDataK;
                $getComplianceReportItemLevel[$ic]['status']=0;
            }

            $ic=$ic+1;

        }

        return array_merge($getComplianceReportItemLevel,$fullStatementStatus);    

    }
    /** Get Item Level Compliance Report End */

    /** Check Full Statment Status Start */
    public function checkFullStatementStatus($itemFullStatmentStatus,$complianceArrayDataDiff)
    {   
        $fss=0;
        $checkFullStatementStatus=array();
        foreach($itemFullStatmentStatus as $itemFullStatmentStatusK=>$itemFullStatmentStatusV)
        {
            foreach($complianceArrayDataDiff as $complianceArrayDataDiffK=>$complianceArrayDataDiffV)
            {
                if($itemFullStatmentStatusV['item_id']==$complianceArrayDataDiffV)
                {
                    if($itemFullStatmentStatusV['full_statement']=="")
                    {
                        $checkFullStatementStatus[$fss]['item_id']=$itemFullStatmentStatusV['item_id'];
                        $checkFullStatementStatus[$fss]['status']=2;
                    }
                    else
                    {
                        $checkFullStatementStatus[$fss]['item_id']=$itemFullStatmentStatusV['item_id'];
                        $checkFullStatementStatus[$fss]['status']=0;
                    }
                }
            }
            $fss=$fss+1;
        }

        return $checkFullStatementStatus;
    }
    /** Check Full Statment Status End */

    /** Get Doc Level Compliance Report Start */
    public function getComplianceReportForDocLevelData($getKeyValuePairForDocDetail,$getDocMandatoryStatus,$getDocMandatoryStatusForSubject,$getDocMandatoryStatusForLicense,$getDocMandatoryStatusForLanguage,$getDocMandatoryStatusForCustom)
    {
        
        $getComplianceReportForDocLevelData = array();
        $getComplianceReportDocLevel = array();
        foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailInfo=>$getKeyValuePairForDocDetailData)
        {
            foreach($getDocMandatoryStatus as $getDocMandatoryStatusK=>$getDocMandatoryStatusV)
            {
                if($getKeyValuePairForDocDetailData['document_id']==$getDocMandatoryStatusK)
                {   
                    $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=$getDocMandatoryStatusV;
                }
            }

            
            foreach($getDocMandatoryStatusForSubject as $getDocMandatoryStatusForSubjectK=>$getDocMandatoryStatusForSubjectV)
            {
                if($getKeyValuePairForDocDetailData['document_id']==$getDocMandatoryStatusForSubjectK)
                {   
                    $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=array_merge($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']],$getDocMandatoryStatusForSubjectV);
                }
                
            }


            foreach($getDocMandatoryStatusForLicense as $getDocMandatoryStatusForLicenseK=>$getDocMandatoryStatusForLicenseV)
            {
                if($getKeyValuePairForDocDetailData['document_id']==$getDocMandatoryStatusForLicenseK)
                {
                    if(isset($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']])){
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=array_merge($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']],$getDocMandatoryStatusForLicenseV);
                    }else{
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=$getDocMandatoryStatusForLicenseV;
                    }
                }
            }


            foreach($getDocMandatoryStatusForLanguage as $getDocMandatoryStatusForLanguageK=>$getDocMandatoryStatusForLanguageV)
            {
                if($getKeyValuePairForDocDetailData['document_id']==$getDocMandatoryStatusForLanguageK)
                {
                    if(isset($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']])){
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=array_merge($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']],$getDocMandatoryStatusForLanguageV);
                    }else{
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=$getDocMandatoryStatusForLanguageV;
                    }
                }
            }

            foreach($getDocMandatoryStatusForCustom as $getDocMandatoryStatusForCustomK=>$getDocMandatoryStatusForCustomV)
            {
                if($getKeyValuePairForDocDetailData['document_id']==$getDocMandatoryStatusForCustomK)
                {
                    if(isset($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']])){
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=array_merge($getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']],$getDocMandatoryStatusForCustomV);
                    }else{
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailData['document_id']]=$getDocMandatoryStatusForCustomV;
                    }
                }
            }

        }

            /** Add title or/and creator to doc array if not in node/metadata set start */
            foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailDataK=>$getKeyValuePairForDocDetailDataV)
            {
                foreach($getComplianceReportForDocLevelData as $getComplianceReportForDocLevelDataKK=>$getComplianceReportForDocLevelDataVV)
                {
                    if($getKeyValuePairForDocDetailDataV['document_id']==$getComplianceReportForDocLevelDataKK)
                    {
                        $insertTitle=array();
                        $insertCreator=array();
                        if(!array_key_exists('title',$getComplianceReportForDocLevelDataVV))
                        {
                            if($getKeyValuePairForDocDetailDataV['title']=="")
                            {
                                $insertTitle=array('title'=>1);
                            }
                            else
                            {
                                $insertTitle=array('title'=>0);
                            }
                        }
                        $getComplianceReportForDocLevelDataVV=array_merge($getComplianceReportForDocLevelDataVV,$insertTitle);
                        if(!array_key_exists('creator',$getComplianceReportForDocLevelDataVV))
                        {
                            if($getKeyValuePairForDocDetailDataV['creator']=="")
                            {
                                $insertCreator=array('creator'=>1);
                            }
                            else
                            {
                                $insertCreator=array('creator'=>0);
                            }
                        }
                        $getComplianceReportForDocLevelDataVV=array_merge($getComplianceReportForDocLevelDataVV,$insertCreator);
                        $getComplianceReportForDocLevelData[$getKeyValuePairForDocDetailDataV['document_id']]=$getComplianceReportForDocLevelDataVV;
                    }
                }
            }
            
            /** Add title or/and creator to doc array if not in node/metadata set end */

            /** Add status as fully non compliance(2) if title or/and creator is not added in the node/metadata set start */
            $getDocLevelDataToArray=array();
            foreach($getKeyValuePairForDocDetail as $getKeyValuePairForDocDetailToArrayKey=>$getKeyValuePairForDocDetailToArrayValue)
            {
                $getDocLevelDataToArray[]=$getKeyValuePairForDocDetailToArrayValue['document_id'];
            }
            
            $getDocComplianceDataToArray=array();
            foreach($getComplianceReportForDocLevelData as $getComplianceReportForDocLevelDataToArrayKey=>$getComplianceReportForDocLevelDataToArrayValue)
            {
                $getDocComplianceDataToArray[]=$getComplianceReportForDocLevelDataToArrayKey;
            }
            
            $complianceDocArrayDataDiff=array();
            $complianceDocArrayDataDiff=array_diff($getDocLevelDataToArray,$getDocComplianceDataToArray);
            
            $docTitleCreatorStatus=$getKeyValuePairForDocDetail;
            $titleCreatorStatus = $this->checkTitleCreatorStatus($docTitleCreatorStatus,$complianceDocArrayDataDiff);
            /** Add status as fully non compliance(2) if title or/and creator is not added in the node/metadata set end */
            
            $dc=0;
            foreach($getComplianceReportForDocLevelData as $getComplianceReportForDocLevelDataK=>$getComplianceReportForDocLevelDataV)
            {
                $getDocCount = count($getComplianceReportForDocLevelDataV);
                $getDocSum = array_sum($getComplianceReportForDocLevelDataV);
                
                if($getDocCount==$getDocSum)
                {   
                    // completely unfilled doc
                    $getComplianceReportDocLevel[$dc]['item_id']=$getComplianceReportForDocLevelDataK;
                    $getComplianceReportDocLevel[$dc]['status']=2;
                }
                else if($getDocCount>$getDocSum && $getDocSum>0)
                {
                    // partially filled doc
                    $getComplianceReportDocLevel[$dc]['item_id']=$getComplianceReportForDocLevelDataK;
                    $getComplianceReportDocLevel[$dc]['status']=1;
                }
                else if($getDocCount>$getDocSum && $getDocSum==0)
                {
                    // completely filled doc
                    $getComplianceReportDocLevel[$dc]['item_id']=$getComplianceReportForDocLevelDataK;
                    $getComplianceReportDocLevel[$dc]['status']=0;
                }

                $dc=$dc+1;
            }

        return array_merge($getComplianceReportDocLevel,$titleCreatorStatus);

    }


    public function checkTitleCreatorStatus($docTitleCreatorStatus,$complianceDocArrayDataDiff)
    {
        $tcs=0;
        $checkTitleCreatorStatus=array();
        foreach($docTitleCreatorStatus as $docTitleCreatorStatusK=>$docTitleCreatorStatusV)
        {
            foreach($complianceDocArrayDataDiff as $complianceDocArrayDataDiffK=>$complianceDocArrayDataDiffV)
            {
                if($docTitleCreatorStatusV['document_id']==$complianceDocArrayDataDiffV)
                {
                    if($docTitleCreatorStatusV['title']=="" && $docTitleCreatorStatusV['creator']=="")
                    {
                        $checkTitleCreatorStatus[$tcs]['item_id']=$docTitleCreatorStatusV['document_id'];
                        $checkTitleCreatorStatus[$tcs]['status']=2;
                        
                    }
                    else if($docTitleCreatorStatusV['title']=="" || $docTitleCreatorStatusV['creator']=="")
                    {
                        $checkTitleCreatorStatus[$tcs]['item_id']=$docTitleCreatorStatusV['document_id'];
                        $checkTitleCreatorStatus[$tcs]['status']=1;
                        
                    }
                    else
                    {
                        $checkTitleCreatorStatus[$tcs]['item_id']=$docTitleCreatorStatusV['document_id'];
                        $checkTitleCreatorStatus[$tcs]['status']=0;
                        
                    }
                }
            }
            $tcs=$tcs+1;
        }

        return $checkTitleCreatorStatus;
    }
    
    public function updateAssignUserToProject($attributes) {
        return  DB::table('project_users')
            ->where(array('project_id' => $attributes['project_id'], 'user_id' => $attributes['user_id']))
            ->update(['updated_by' => $attributes['updated_by'],'workflow_stage_role_id' => $attributes['workflow_stage_role_id'] ]);
    }


    /** Get Doc Level Compliance Report End */

    /**
     * @param $workflowId
     * @return mixed
     * @FunctionName getWorkflowDetails
     * @purpose This function is used to return workflow stage id based on workflow id
     */
    public function getWorkflowDetails($workflowId){
        $workflow = DB::table('workflow_stage')
            ->select('workflow_stage_id')
            ->where('workflow_id',$workflowId)
            ->get()
            ->toArray();
        return $workflow;
    }

    /**
     * @param $workflowStageId
     * @param $roleId
     * @return mixed
     * @FunctionName getStageWorkflow
     * @purpose This function is used to return workflow stage role id based on workflow stage id
     */
    public function getStageWorkflow($workflowStageId,$roleId){
        $workflowStageRoleData = DB::table('workflow_stage_role')
            ->select('workflow_stage_role_id')
            ->whereIn('workflow_stage_id',$workflowStageId)
            ->where('role_id',$roleId)
            ->get()
            ->toArray();
        return $workflowStageRoleData;
    }

    /**
     * This method will create a new mapping record with project and taxonomy
     */
    public function updateAllParentItemToNonEditablePacingGuide(array $arrayOfParentId, string $projectIdentifier) {
        $tableName = "project_items";
        $queryToExecute = DB::table($tableName)->whereIn("item_id", $arrayOfParentId)->update(['is_editable' => '0']);
        return $queryToExecute;
    }
}
