<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Data\Models\WorkflowStage;

class WorkflowStageEloquentRepository extends EloquentRepository implements WorkflowStageRepositoryInterface
{
    public function getModel()
    {
        return WorkflowStage::class;
    }
    
    public function deleteandadd($workflowStageId, $data){
        $roleId = $data['role_id'];
        //DB::table('workflow_stage_role')->where('workflow_stage_role_id', '=', $workflowStageId)->delete();
        $roleIds = explode(',', $roleId);
        foreach( $roleIds as $roleID )
        {
            $workflow_stage_role_id = $workflowStageId."||".trim($roleID);
            DB::table('workflow_stage_role')->where('workflow_stage_role_id', '=', $workflow_stage_role_id)->delete();
            DB::table('workflow_stage_role')->insert(
                ['workflow_stage_role_id' => $workflow_stage_role_id, 'workflow_stage_id' => $workflowStageId, 'role_id' => $roleID]
            );
        } 
        return $workflow_stage_role_id;
    }

    public function getWorkflowStageList($workflowId){
        $query = $this->model->select('workflow_stage_id', 'stage_name', 'stage_description');
        $query->where('workflow_id', $workflowId);
        $query->orderBy('order');
        return $query->get()->toArray();
    }

    public function getRoleDetails($data){        
        $sub_query_role = DB::table('roles')
        ->select('role_id','name');
        $sub_query_role->where('role_id', '=', $data['role_id']);
        $role = $sub_query_role->get()->toArray();
        return $role;
    }

    public function fetchRolesForAStage($workflowStageId) {
        $sub_query_role_id = DB::table('workflow_stage_role')
                ->select('role_id');
        $sub_query_role_id->where('workflow_stage_role.workflow_stage_id', '=', $workflowStageId);
        $role_id = $sub_query_role_id->get()->toArray();
        $role = [];
        foreach($role_id as $key=>$rd){
            
            if(count($role_id)>0){
                $sub_query_role = DB::table('roles')
                ->select('role_id','name');
                $sub_query_role->where('roles.role_id', '=', $rd->role_id);
                $role[$key] = $sub_query_role->get()->first();
                
            }
                    
        }      
        return $role;
    }

    

    public function removeWorkflowStageRole($workflow_stage_role_id){
        DB::table('workflow_stage_role')->where('workflow_stage_role_id', '=', $workflow_stage_role_id)->delete();
    }

    /**
     * Clone of findByAttributes with features like return single or multiple records, query for selected fields only
     * with pagination and sorting
     */
    public function findByWorkflowStageAndRoleId(
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs,
        $relations = null
    ){
        // prepare the query initially
        $query = DB::table('workflow_stage_role')->select($fieldsToReturn);

        // Get the last value of the associative array
        $lastValue = end($keyValuePairs);

        // Get the last key of the associative array
        $lastKey = key($keyValuePairs);

        // update the query with a where clause initially
        $query = $query->where($lastKey, $lastValue);

        // Pop the last key value pair of the associative array now that it has been added to Builder already
        array_pop($keyValuePairs);

        $method = 'where';

        foreach ($keyValuePairs as $key => $value) {
            $query->$method($key, $value);
        }

        return $returnCollection ? $query->get() : $query->get()->first();

    }
}