<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\CompetencyFrameworkRepositoryInterface;

use App\Data\Models\Taxonomy;

class CompetencyFrameworkEloquentRepository extends EloquentRepository implements CompetencyFrameworkRepositoryInterface
{

    public function getModel()
    {
        return Taxonomy::class;
    }

    /**
     * This method is currently in use for the following methods only
     * [getTaxonomyData, getAllCFDocuments]
     */
    private function getQueryWithAllDependentJoins() {
        $fieldsToReturn = [
            'taxonomies.id',
            'taxonomies.cf_entity_type',
            'taxonomies.uri',
            'taxonomies.extra',
            'taxonomies.title',
            'taxonomies.creator',
            'taxonomies.official_source_url',
            'taxonomies.url_name',
            'taxonomies.version',
            'taxonomies.description',
            'taxonomies.adoption_status',
            'taxonomies.status_start_date',
            'taxonomies.status_end_date',
            'taxonomies.status_start_date',
            'taxonomies.note',
            'taxonomies.created_at',
            'taxonomies.updated_at',
            'taxonomies.abbreviated_statement',
            'taxonomies.human_coding_scheme',
            'taxonomies.list_enumeration',
            'taxonomies.full_statement',
            'taxonomies.education_level',
            'taxonomies.alternative_label',
            'taxonomies.parent_id',
            'taxonomies.root_node_id',
            
            'publishers.name as publisher_name',

            'subjects.id as subject_id',
            'subjects.uri as subject_uri',
            'subjects.title as subject_title',
            'subjects.hierarchy_code as subject_hierarchy_code',
            
            'languages.id as language_id',
            'languages.name as language_name',
            'languages.short_code as language_short_code',
            
            'concepts.id as concept_id',
            'concepts.keyword as concept_keyword',
            'concepts.uri as concept_uri',
            
            'licenses.id as license_id',
            'licenses.title as license_title',
            'licenses.uri as license_uri',
            
            'item_types.id as item_type_id',
            'item_types.title as item_type_title',
            'item_types.uri as item_type_uri',
            'item_types.type_code as item_type_type_code',
            'item_types.hierarchy_code as item_type_hierarchy_code',
            'item_types.description as item_type_description',
            'item_types.updated_at as item_type_updated_at',
        ];

        $query = DB::table('taxonomies')
                    ->select($fieldsToReturn)
                    ->leftJoin('publishers', function($join){
                        $join->on('taxonomies.publisher_id', '=', 'publishers.id');
                        $join->where('publishers.is_active', '=', 1);
                        $join->where('publishers.is_deleted', '=', 0);
                    })
                    ->leftJoin('subjects', function($join){
                        $join->on('taxonomies.subject_id', '=', 'subjects.id');
                        $join->where('subjects.is_active', '=', 1);
                        $join->where('subjects.is_deleted', '=', 0);
                    })
                    ->leftJoin('languages', function($join){
                        $join->on('taxonomies.language_id', '=', 'languages.id');
                        $join->where('languages.is_active', '=', 1);
                        $join->where('languages.is_deleted', '=', 0);
                    })
                    ->leftJoin('concepts', function($join){
                        $join->on('taxonomies.concept_id', '=', 'concepts.id');
                        $join->where('concepts.is_active', '=', 1);
                        $join->where('concepts.is_deleted', '=', 0);
                    })
                    ->leftJoin('licenses', function($join){
                        $join->on('taxonomies.license_id', '=', 'licenses.id');
                        $join->where('licenses.is_active', '=', 1);
                        $join->where('licenses.is_deleted', '=', 0);
                    })
                    ->leftJoin('item_types', function($join){
                        $join->on('taxonomies.item_type_id', '=', 'item_types.id');
                        $join->where('item_types.is_active', '=', 1);
                        $join->where('item_types.is_deleted', '=', 0);
                    })
                    ->where('taxonomies.is_active', '=', 1)
                    ->where('taxonomies.is_deleted', '=', 0);
        return $query;
    }

    /**
     * This method is currently in use for the following methods only
     * [getDocumentData()]
     */
    private function getQueryForDocument() {
        $fieldsToReturn = [
            'documents.document_id',
            'documents.title',
            'documents.creator',
            'documents.official_source_url',
            'documents.version',
            'documents.description',
            'documents.adoption_status',
            'documents.status_start_date',
            'documents.status_end_date',
            'documents.notes',
            'documents.created_at',
            'documents.updated_at',
        ];

        $query = DB::table('documents')
                    ->select($fieldsToReturn)
                    ->where('documents.is_active', '=', 1)
                    ->where('documents.is_deleted', '=', 0);
        return $query;
    }


    /**
     * Get the taxonomical hierarchial data in a tree like view
     * @param string $taxonomyId
     * @return void
     */
    public function getTaxonomyHierarchyById(string $taxonomyId){
        $parent = $this->getTaxonomyData($taxonomyId);
        $parent->children = $this->getChildren($parent->id);
        return $parent;
    }

    /**
     * Get nth children of a taxonomy 
     * @param string $taxonomyId
     * @return void
     */
    public function getChildren(string $taxonomyId){
        $data = [];
        $taxonomies = $this->getTaxonomyData($taxonomyId, true);
        if( !empty($taxonomies) && $taxonomies->isNotEmpty() ){
            foreach($taxonomies as $key=>$taxonomy){
                $taxonomy->children = [];
                $taxonomyChildren = $this->getChildren($taxonomy->id);
                if(!empty($taxonomyChildren)){
                    $taxonomy->children = $taxonomyChildren;
                }
                $data[] = $taxonomy;
             }
        }
        return $data;
    }

    /**
     * Get all related data of a taxonomy
     * @param string $taxonomyId
     * @param bool $getChildStatus - If true then it will fetch the immediate child of the specified taxonomy
     * @return void
     */
    public function getTaxonomyData(string $taxonomyId, bool $getChildStatus = false, string $cfEntityType = ""){
        $query = $this->getQueryWithAllDependentJoins();

        $query = $getChildStatus ? 
                    $query->where('taxonomies.parent_id', '=', $taxonomyId) :
                    $query->where('taxonomies.id', '=', $taxonomyId);

        if(!empty($cfEntityType) && in_array($cfEntityType, ["cf_doc","cf_item"])){
            $query->where('taxonomies.cf_entity_type', '=', $cfEntityType);
        }
                    
        $data = [];
        $collection = $query->get();
        if($collection->isNotEmpty()){
            $data = $getChildStatus ? $collection : $collection->first();
        }
        return $data;
    }

    /**
     * Get all related data of a document
     * @param string $documentId
     * @return void
     */
    public function getDocumentData(string $documentId, string $cfEntityType = ""){
        $query = $this->getQueryForDocument();

        $query = $query->where('documents.document_id', '=', $documentId);

        $data = [];
        $collection = $query->get();
        if($collection->isNotEmpty()){
            $data = $collection->first();
        }
        return $data;
    }


    /**
     * Get all taxonomies with entity type cf_doc
     */
    public function getAllCFDocuments( array $searchFilters = [ "limit" => 100, "offset" => 0, "sorting" => "desc", "orderBy" => "taxonomies.updated_at"]) {
        $cfEntityType = "cf_doc";

        // get query with all relevant table joins
        $query = $this->getQueryWithAllDependentJoins();

        // extract filters
        extract($searchFilters);
        
        // set default values for filters if not set
        if(!isset($orderBy) && !isset($sorting)){
            $orderBy = "taxonomies.updated_at";
            $sorting = "desc";
        }

        $limit = !isset($limit) ? 100 : $limit;
        $offset = !isset($offset) ? 0 : $offset;
        

        // set entity type filter to cf_doc
        $query->where('taxonomies.cf_entity_type', '=', $cfEntityType);

         // set sort order
        $query->orderBy($orderBy, $sorting);

        // set filters to paginate data
        $query->offset($offset)->limit($limit);

        // execute query
        $collection = $query->get();

        $data = $collection->isNotEmpty() ? $collection : [];
        return $data;
    }

    /**
     * This method returns all the associated models of a taxonomy
     * Also the associated models will also contains other models associated with it
     */
    public function getTaxonomyDetailsWithProjectAndCaseAssociation(string $taxonomyId){
        $fetchAssociatedModels = [
            "license", 
            "concept_uri", 
            "language", 
            "Publisher", 
            "projects", 
            "subject", 
            "itemType",
            "caseAssociations.caseAssociationType:type_id,type_name,display_name",
            "caseAssociations.cfDocument:id,cf_entity_type,title,human_coding_scheme,full_statement,root_node_id,parent_id",
            "caseAssociations.originNode:id,cf_entity_type,title,human_coding_scheme,full_statement,root_node_id,parent_id",
            "caseAssociations.destinationNode:id,cf_entity_type,title,human_coding_scheme,full_statement,root_node_id,parent_id"
        ];
        return $this->findBy("id", $taxonomyId, $fetchAssociatedModels);
    }

    /**
     * This method returns all taxonomies that belongs to a rootNodeId (CFDocument Identifier)
     * along with the CFDocument itself
     */
    public function getAllTaxonomyNodesByRootNodeId(string $rootNodeIdentifier){
        // setting up attribute-value pairs for the query
        $attributes = ["root_node_id" => $rootNodeIdentifier, "id" => $rootNodeIdentifier];
        // disable pagination and it's filter
        $returnPaginatedData = false;
        $paginationFilters = [];
        // enable sorting
        $returnSortedData = true;
        $sortfilters = ['orderBy' => 'created_at', 'sorting' => 'asc'];
        // set attribute-value pairs condition
        $operator = 'OR';
        // set the dependent data models to return
        $fetchAssociatedModels = [
            "license:id,title,uri,description,license_text,created_at,updated_at", 
            "concept_uri:id,keyword,uri,title,hierarchy_code,description,created_at,updated_at", 
            "language:id,short_code", 
            "Publisher:id,name",
            "subject:id,title,uri,hierarchy_code,description,created_at,updated_at", 
            "itemType:id,title,type_code,hierarchy_code,description,uri,created_at,updated_at",
            "caseAssociations.caseAssociationType:type_id,type_name",
            "caseAssociations.destinationNode:id,uri,cf_entity_type,title,human_coding_scheme,full_statement,root_node_id,parent_id",
            "caseAssociations.caseAssociationGroup:group_id,uri,title,description,created_at,updated_at"
        ];
        // execute query and set db data
        $data = $this->findByAttributes(
            $attributes,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator,
            $fetchAssociatedModels
        );
        return $data;
    }


}