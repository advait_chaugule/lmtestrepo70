<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

use App\Data\Models\User;
//use DB;
class UserEloquentRepository extends EloquentRepository implements UserRepositoryInterface
{

    public function getModel()
    {
        return User::class;
    }

    public function getUsersList($organization_id)
    {
        $userList = DB::table('users')
                    ->select('users.user_id','users.email','users.first_name','users.last_name','roles.role_id','roles.name','users_organization.is_active','users.is_register')
                    ->join('users_organization','users_organization.user_id','=','users.user_id')
                    ->join('roles','roles.role_id','=','users_organization.role_id')
                    ->where('users_organization.organization_id','=',$organization_id)
                    ->where('users.is_deleted','=',0)
                    ->where('users_organization.is_deleted','=',0)
                    ->get();
        $userData =[];
        foreach ($userList as $key => $user)
        {
            $userData[] = ['user_id'=>$user->user_id,
                "email"       => $user->email,
                "first_name"  => $user->first_name,
                "last_name"   => $user->last_name,
                "is_active"   => $user->is_active,
                "role_id"     => $user->role_id,
                "is_register" => $user->is_register,
                "system_role" =>['role_id'=>$user->role_id,'name'=>$user->name]
            ];
        }
      $users = json_decode (json_encode ($userData), FALSE);
        return $users;


    }

    public function usageCountOnRoles($model, $mapColumn, $identifierSet, $fieldsToCount, $keyValuePairs){
        $usageCount = [];

        foreach($identifierSet as $identifier){
            $query = DB::table($model)
                ->select(DB::raw('COUNT('.$fieldsToCount.') as usage_count'));

            foreach ($keyValuePairs as $key => $value) {
                $query->where($key, $value);
            }
            
            $query->where(array($model.'.'.$mapColumn => $identifier))->groupBy($model.'.'.$mapColumn);
            
            $usageCount[$identifier] = $query->get()->toArray();
        }

        return $usageCount;
    }
    
    public function getUserProjectRoleList(array $userIds, string $projectId) {
        $tableName = "project_users";
        
        $result = DB::table($tableName)
        ->where(array('project_id' => $projectId, 'is_deleted' => '0'))
        ->whereIn('user_id', $userIds)
        ->get();

        return $result;
    }

    public function fetchProjectsList(array $fieldsToReturn, array $conditionalKeyValuePairs) {
        $tableName = "project_users";
        
        $query = DB::table($tableName)->select($fieldsToReturn);

        foreach ($conditionalKeyValuePairs as $key => $value) {
            $query->where($key, $value);
        }

        $result = $query->get();
        return $result;
    }

    public function fetchProjectUser($fieldsToReturn,$conditionalKeyValuePairs)
    {
        $tableName = "project_users";
        $query     = DB::table($tableName)->select($fieldsToReturn)
                   ->where('user_id',$conditionalKeyValuePairs['user_id'])
                   ->where('organization_id',$conditionalKeyValuePairs['organization_id'])
                   ->where('is_deleted',0)
                   ->get()
                   ->toArray();
       return $query;
    }

    public function getUsersListForPermission($organization_id,$permissionId){
      
           

         $query = DB::table('users')
        ->leftjoin('role_permission', 'role_permission.role_id', '=', 'users.role_id')
        ->select('user_id','first_name','last_name','email')
        ->where('users.is_active', '=', 1)
        ->where('users.is_deleted', '=', 0)
        ->where('users.organization_id', '=', $organization_id)
        ->where('role_permission.permission_id', '=', $permissionId);
          
       $result = $query->get()->toArray();

        return $result;
     }

    public function getTenentUserForPermission($organization_id,$permissionId){

        $query = DB::table('users')
            ->join('users_organization','users_organization.user_id','=','users.user_id')
            ->leftjoin('role_permission', 'role_permission.role_id', '=', 'users_organization.role_id')
            ->select('users.user_id','first_name','last_name','email')
            ->where('users_organization.is_active', '=', 1)
            ->where('users_organization.is_deleted', '=', 0)
            ->where('users_organization.organization_id', '=', $organization_id)
            ->where('role_permission.permission_id', '=', $permissionId);

        $result = $query->get()->toArray();

        return $result;
    }

     public function getUserListForRename($email)
     {
        $query = DB::table('users')
        ->select('user_id','username')
        ->where('username','LIKE','%'.$email);

        $result = $query->get()->toArray();
       
        return $result;
     }

     public function getIndividualUserDetail(array $active_access_token){
        
        $query = DB::table('users')
        ->select('user_id')
        ->where('active_access_token','=',$active_access_token['active_access_token'])
        ->where('is_deleted','0');

        $result = $query->get()->toArray();
        $data=array();
        foreach($result as $resultDataK=>$resultData){
            foreach($resultData as $resultDataKey=>$resultDataValue){
                $data[]=$resultDataValue;
            }
            
        }
        
        return $data;

     }

     public function getUserDetailBasedOnEmailId(string $emailId)
     {
        $userDetail = DB::table('users')
                ->where('email',$emailId)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
        // Send Org id only if user with that email exists
        return !empty($userDetail) ? $userDetail[0]->current_organization_id : "";   
                
     }

    /**
     * @param $user_id
     * @param array $input
     * @return mixed
     * $purpose This function is used to update is active field of users organization table
     */
     public function updateUserOrganization($userId,array $input)
     {
         // * issue - Inactive tenants to activate delete users issue resolved
        $users = DB::table('users')
        ->where('user_id','=', $userId)
        ->where('is_deleted','=',0)
        ->first();
        if(!$users)
            return null;
            
         if(isset($input['is_active']))
         {
            DB::table('users_organization')
                            ->where('user_id','=',$userId)
                            ->where('organization_id','=',$input['organization_id'])
                            ->update(['is_active' => $input['is_active']]);
                if($input['is_active'] == 0)
                {
					// Added is_deleted condition so that deleted org are updated in curret org
                    $query = DB::table('users_organization')
                            ->where('user_id','=', $userId)
                            ->where('organization_id','!=',$input['organization_id'])
                            ->where('is_active','=',1)
                            ->where('is_deleted','=',0)
                            ->first();
                
                        if(!$query) {
                            $queryData = DB::table('users')
                            ->where('user_id','=',$userId)
                            ->update(['is_active'=>0]);
                        }
                        else
                        {
                            DB::table('users')
                            ->where('user_id','=',$userId)
                            ->update(['current_organization_id'=>$query->organization_id]);
                        }

                }
                else
                {

                    $queryData = DB::table('users')
                            ->where('user_id','=',$userId)
                            ->update(['is_active'=>1]);
					// Added is_deleted condition so that deleted org are updated in curret org
                    $query = DB::table('users_organization')
                            ->where('user_id','=', $userId)
                            ->where('organization_id','!=',$input['organization_id'])
                            ->where('is_active','=',1)
                            ->where('is_deleted','=',0)
                            ->first();
                
                        if(!$query) {
                            DB::table('users')
                            ->where('user_id','=',$userId)
                            ->update(['current_organization_id'=>$input['organization_id']]);
                        }
                    
                }
                            

                      
         }
         if(isset($input['role_id']))
         {
            DB::table('users_organization')
            ->where('user_id','=',$userId)
            ->where('organization_id','=',$input['organization_id'])
            ->update(['role_id' => $input['role_id']]);

          
         }

         if(isset($input['enable_email_notification']))
         {
            DB::table('users_organization')
            ->where('user_id','=',$userId)
            ->where('organization_id','=',$input['organization_id'])
            ->update(['enable_email_notification' => $input['enable_email_notification']]);

            /* DB::table('users')->where('user_id','=',$userId)->
                where('organization_id','=',$input['organization_id'])
                ->update(['role_id' => $input['role_id']]); */
         }
          

         $data         = DB::table('users')->where('user_id','=',$userId)->first();

         return $data;
     }

    /**
     * @param $userId
     * @param $organizationId
     * @param array $input
     * @return array|mixed|object
     * @purpose This is used update is_deleted column in users organization table
     * and return with users details which will help to send email via email id
     */
     public function updateOrganizationUserDeleteStatus($userId,$organizationId,array $input)
     {
         DB::table('users_organization')
             ->where('user_id','=',$userId)
             ->where('organization_id','=',$organizationId)
             ->update(['is_active'=> $input['is_active'],'is_deleted'=> $input['is_deleted']]);

         $data = DB::table('users')->where('user_id','=',$userId)->get();
         $arr = json_decode(json_encode($data), true);
         return $arr;
     }



}