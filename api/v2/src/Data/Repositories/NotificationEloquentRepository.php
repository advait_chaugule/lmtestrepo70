<?php
namespace App\Data\Repositories;
use App\Data\Models\AppNotification;
use App\Data\Repositories\Contracts\NotificationInterface;
use App\Data\Repositories\EloquentRepository;

class NotificationEloquentRepository extends EloquentRepository implements NotificationInterface
{
    public function getModel()
    {
        return AppNotification::class;
    }

    /**
     * @param array $attribute
     * @return mixed
     * @purpose This function is used to insert data in notification table
     */
    public function createNotification(array $attribute)
    {
        $notificationId        = $attribute['notification_id'];
        $description           = $attribute['description'];
        $organizationId        = $attribute['organization_id'];
        $loginUser             = $attribute['created_by'];
        $targetId              = $attribute['target_id'];
        $targetType            = $attribute['target_type'];
        $userId                = $attribute['user_id'];
        $targetContext         = $attribute['target_context'];
        $notificationCategory  = $attribute['notification_category'];

        $data           = ['notification_id'=>$notificationId,
                          'description'=> $description,
                          'target_type'=>$targetType,
                          'target_id'=>$targetId,
                          'user_id'  =>$userId,
                          'organization_id'=>$organizationId,
                          'notification_category'=>$notificationCategory,
                          'created_by'=>$loginUser,
                          'updated_by'=>$loginUser,
                          'target_context'=>$targetContext,
                          'read_status'=>0
                           ];

        $query = AppNotification::create($data);

        return $query;
    }
}
