<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\NodeType;

class NodeTypeEloquentRepository extends EloquentRepository implements NodeTypeRepositoryInterface
{
    public function getModel()
    {
        return NodeType::class;
    }

    /**
     * This method returns all node type metadata
     */
    public function getNodeTypeMetadata(string $nodeTypeId)
    {
        return $this->model
            ->where('node_types.node_type_id', $nodeTypeId)
            ->where('node_types.used_for',0)
            ->select('node_types.node_type_id')
            ->with([
                'metadata' => function($query) {
                    $query->select('metadata.metadata_id', 'metadata.parent_metadata_id', 'metadata.name', 'metadata.field_type', 'metadata.field_possible_values', 'metadata.is_custom','metadata.is_mandatory', 'metadata.internal_name');
                    $query->where('metadata.is_deleted', 0);
                    $query->where('metadata.is_active', 1);
                }
            ])
            ->first();
    }

    /**
     * This method saves metadata for a node type
     */
    public function saveNodeTypeMetadata(string $nodeTypeId, $requestData)
    {
        DB::table('node_type_metadata')->where('node_type_id', $nodeTypeId)->delete();
        $data = [];
        $return = [];
        if (count($requestData['metadata']) > 0) {
            foreach ($requestData['metadata'] as $metadata) {
                $data[] = [
                    'node_type_id'  =>  $nodeTypeId,
                    'metadata_id'   =>  $metadata['id'],
                    'sort_order'    =>  $metadata['order'],
                    'is_mandatory'  =>  $metadata['is_mandatory'],
                ];
                $return[] = [
                    'metadata_id' => $metadata
                ];
            }
        }

        DB::table('node_type_metadata')->insert($data);
        return $return;
    }
    
    public function getNodetypeList($request){
        $input = $request->all();
        $allowedOrderByFields = ['title', 'updated_at'];
        $allowedOrderByMethods = ['asc', 'desc'];

        $limit = 100;
        if ($request->has('limit') && is_int($request->limit)) {
            $limit = $request->limit;
        }

        $offset = 0;
        if ($request->has('offset') && is_int($request->offset)) {
            $offset = $request->offset;
        }

        //return $this->model->take($limit)->skip($offset)
        return $this->model
        ->when($request->has('sort'), function($query) use ($request, $allowedOrderByFields, $allowedOrderByMethods) {
            list($orderBy, $sorting) = explode(':', $request->sort);
            if (in_array($orderBy, $allowedOrderByFields) && in_array($sorting, $allowedOrderByMethods)) {
                $query->orderBy($orderBy, $sorting);
            }            
        })
        ->where('is_deleted', 0)
        ->where('used_for', 0)
        ->where('organization_id', $input['auth_user']['organization_id'])
        ->when($request->has('search'), function($query) use ($request) {
            $query->where('title', 'like', "%{$request->search}%");
        })
        ->select('node_type_id', 'title', 'is_document', 'is_deleted', 'is_default', 'updated_at', 'created_by', 'updated_by')
        ->with([
            'createdBy' => function($query) {
                $query->select('user_id');
                $query->selectRaw('concat_ws(" ", first_name, last_name) as name');
            }
        ])

        ->get();
    }

    public function getNodeType($identifier, $organizationId){
        return $this->model->select('node_type_id', 'title', 'updated_at', 'updated_by')
        ->where('node_type_id', $identifier)
        ->where('organization_id', $organizationId)
        ->with([
            'updatedBy' => function($query) {
                $query->select('user_id');
                $query->selectRaw('concat_ws(" ", first_name, last_name) as name');
            }
        ])

        ->get()->first();
    }

    public function getNodeTypeIsUsedData(array $nodeTypeIdentifier)
    {
        
        $nodeTemplateIdInTemplate = DB::table('node_template_node_types')
        ->select('node_template_id')
        ->where($nodeTypeIdentifier)
        ->get()
        ->toArray();

        $nodeTemplateList=array();
        foreach($nodeTemplateIdInTemplate as $nodeTemplateIdInTemplateKey=>$nodeTemplateIdInTemplateValue)
        {
            $nodeTemplateList[]=$nodeTemplateIdInTemplateValue->node_template_id;
        }
        
        $nodeTemplateIdListData = DB::table('node_templates')
        ->select('node_template_id')
        ->whereIn('node_template_id',$nodeTemplateList)
        ->where('is_deleted','0')
        ->get()
        ->toArray();

        $nodeCountInTemplate=count($nodeTemplateIdListData);

        // $nodeInMetadata = DB::table('node_type_metadata')
        // ->select('node_type_id')
        // ->where($nodeTypeIdentifier)
        // ->get()
        // ->toArray();

        // $nodeCountInMetadata=count($nodeInMetadata);

        $nodeInItems = DB::table('items')
        ->select('node_type_id')
        ->where($nodeTypeIdentifier)
        ->where('is_deleted','0')
        ->get()
        ->toArray();

        $nodeCountInItems=count($nodeInItems);

        $totalNodeCount=$nodeCountInTemplate+$nodeCountInItems;
        
        if($totalNodeCount>0)
        {
            return 1;
        }
        else{
            return 0;
        }

    }

    public function getUnmappedTaxonomyList(array $documentIdentifier)
    {   
        $documentData = DB::table('documents')
        ->select('document_id','title')
        ->where($documentIdentifier)
        ->where('is_deleted','0')
        ->get()
        ->toArray();
        
        $documentNodeData=array();
        $documentNodeData[$documentData[0]->document_id]=$documentData[0]->title;
        
        $totalTaxonomyNodes = DB::table('items')
        ->select('item_id','human_coding_scheme','full_statement')
        ->groupBy('item_id')
        ->where($documentIdentifier)
        ->where('is_deleted','0')
        ->get()
        ->toArray();

        $arrayListTotalTaxonomyNodes=array();
        foreach($totalTaxonomyNodes as $totalTaxonomyNodesKey=>$totalTaxonomyNodesValue)
        {
            $arrayListTotalTaxonomyNodes[]=$totalTaxonomyNodesValue->item_id;
        }
        
        $totalTaxonomyMappedNodes = DB::table('project_items')
        ->select('item_id')
        ->groupBy('item_id')
        ->whereIn('item_id',$arrayListTotalTaxonomyNodes)
        ->where('is_deleted','0')
        ->get()
        ->toArray();

        $arrayListMappedTaxonomyNodes=array();
        foreach($totalTaxonomyMappedNodes as $totalTaxonomyMappedNodesKey=>$totalTaxonomyMappedNodesValue)
        {
            $arrayListMappedTaxonomyNodes[]=$totalTaxonomyMappedNodesValue->item_id;
        }
        
        $arrayListUnmappedNodes=array();
        $arrayListUnmappedNodes=array_diff($arrayListTotalTaxonomyNodes,$arrayListMappedTaxonomyNodes);

        $listOfUnmappedNodes=array();
        foreach($arrayListUnmappedNodes as $arrayListUnmappedNodesKey=>$arrayListUnmappedNodesValue)
        {
            foreach($totalTaxonomyNodes as $totalTaxonomyNodesKey=>$totalTaxonomyNodesValue)
            {   
                if($arrayListUnmappedNodesValue==$totalTaxonomyNodesValue->item_id)
                {
                    $listOfUnmappedNodes[$arrayListUnmappedNodesValue]=$totalTaxonomyNodesValue->human_coding_scheme.' '.$totalTaxonomyNodesValue->full_statement;
                }
            }
        }

        if(count($listOfUnmappedNodes)>0)
        {
            $unmappedNodes=$documentNodeData+$listOfUnmappedNodes;
        }
        else
        {
            $unmappedNodes=$listOfUnmappedNodes;
        }
        
        $displaylistOfUnmappedNodes=array();
        $i=0;
        foreach($unmappedNodes as $unmappedNodesKey=>$unmappedNodesValue)
        {   
            $displaylistOfUnmappedNodes[$i]['key']      = $unmappedNodesKey;
            $displaylistOfUnmappedNodes[$i]['value']    = $unmappedNodesValue;
            $i++;
        }

        return $displaylistOfUnmappedNodes;

    }

    public function getAllNodeTypesInCsv($attributes, $nodeTypes = [])
    {
        $results = DB::table('node_types')->where($attributes);

        if(!empty($nodeTypes)) {
            $results = $results->whereIn('title', $nodeTypes);
        }
        $results = $results->get();

        return $results;
    }
}