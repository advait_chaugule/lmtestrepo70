<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ItemTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\ItemType;

class ItemTypeEloquentRepository extends EloquentRepository implements ItemTypeRepositoryInterface
{
    public function getModel()
    {
        return ItemType::class;
    }
}