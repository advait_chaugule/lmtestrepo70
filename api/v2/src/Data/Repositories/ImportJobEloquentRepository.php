<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;
use App\Data\Repositories\Contracts\ImportJobRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Data\Models\ImportJob;

class ImportJobEloquentRepository extends EloquentRepository implements ImportJobRepositoryInterface {

    public function getModel() {
        return ImportJob::class;
    }
}
