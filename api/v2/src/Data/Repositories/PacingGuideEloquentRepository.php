<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\PacingGuideRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\PacingGuide;

class PacingGuideEloquentRepository extends EloquentRepository implements PacingGuideRepositoryInterface
{

    public function getModel()
    {
        return PacingGuide::class;
    }

}