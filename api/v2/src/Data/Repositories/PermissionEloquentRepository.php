<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\PermissionRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\Permission;

class PermissionEloquentRepository extends EloquentRepository implements PermissionRepositoryInterface
{

    public function getModel()
    {
        return Permission::class;
    }

    /**
     * This method returns all the permission
     */
    public function getInternalPermissionNamesAssignedToRole(string $userRoleId) {
        $permissions = DB::table('permissions')->select('internal_name')->get();
        $extractInternalPermissionName = $permissions->pluck("internal_name")->filter(function ($item, $key) {
            if(!empty($item)){
                return $item;
            }
        })->toArray();
        return $extractInternalPermissionName;
    }
}