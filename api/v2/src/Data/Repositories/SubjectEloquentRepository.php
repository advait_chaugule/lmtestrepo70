<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\SubjectRepositoryInterface;

use App\Data\Models\Subject;

class SubjectEloquentRepository extends EloquentRepository implements SubjectRepositoryInterface
{
    public function getModel()
    {
        return Subject::class;
    }
}