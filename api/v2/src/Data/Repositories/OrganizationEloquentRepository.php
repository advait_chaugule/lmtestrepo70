<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

use App\Data\Models\Organization;

class OrganizationEloquentRepository extends EloquentRepository implements OrganizationRepositoryInterface
{

    public function getModel()
    {
        return Organization::class;
    }

   public function getDefaultOrganizationDetail(){
       
    $organizationDetail = DB::table('organizations')
    ->select('organization_id')
    ->where('default','=','1')
    ->where('is_deleted','=','0')
    ->where('is_active','=','1')
    ->limit(1)
    ->get()
    ->toArray();

    $organizationId=array();
    foreach($organizationDetail as $organizationDetail=>$organizationDetailValue){
        foreach($organizationDetailValue as $organizationDetailValueData){
            $organizationId[]=$organizationDetailValueData;
        }
    }

    return $organizationId;

   }
}