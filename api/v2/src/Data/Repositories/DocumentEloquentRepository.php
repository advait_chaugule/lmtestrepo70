<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

use App\Data\Models\Document;
use App\Data\Models\Item;
use App\Data\Models\Metadata;
use App\Data\Models\NodeType;
use App\Data\Models\ItemMetadata;
use App\Data\Models\DocumentMetadata;

class DocumentEloquentRepository extends EloquentRepository implements DocumentRepositoryInterface
{

    public function getModel()
    {
        return Document::class;
    }   

    /**
     * This method returns all the associated models of a document
     * Also the associated models will also contains other models associated with it
     */
    public function getDocumentDetails(string $documentId){
        $fetchAssociatedModels = [
            "license", 
            "language", 
            "subjects",
            "items", 
            "nodeType",
            "nodeType.metadata",
            "customMetadata",
        ];
        return $this->findBy("document_id", $documentId, $fetchAssociatedModels);
    }

    public function listDocumentRoot($searchFilterAttribut)
    {
        $limit              =   $searchFilterAttribut['limit'];
        $offset             =   $searchFilterAttribut['offset'];
        $organization_id    =   $searchFilterAttribut['organization_id'];
        $documentType       =   $searchFilterAttribut['document_type'];
        $adoptionStatus     =   $searchFilterAttribut['adoption_status'];
        $importStatus       =   $searchFilterAttribut['import_status'];

        $query = $this->model->select('document_id', 'title', 'document_type', 'created_at','updated_at','adoption_status','status','created_by','updated_by','import_type','actual_import_type','creator','uri','source_document_id','is_locked','source_type');
        $query->where('is_deleted',0);
        if(!empty($documentType)){
           $query->whereIn('document_type', $documentType);
        }
        $query->whereNOTIn('adoption_status', $adoptionStatus);
        $query->where('organization_id',$organization_id);
        $query->where('import_status',$importStatus);
        $query->orderBy('created_at', 'desc');
        $parent = $query->get();
        return $parent;
    }

    public function mapDocumentWithSubject(Document $document, string $subjectIdentifier) {
        $document->subjects()->attach($subjectIdentifier);
    }

    /**
     * Get all related data of a document
     * @param string $documentId
     * @return void
     */
    public function getDocumentDataAndAssociatedModels(string $documentId){
        $fetchAssociatedModels = [
            "license", 
            "language", 
            "subjects",
            "customMetadata",
            "nodeType"
        ];

        return $this->findBy("document_id", $documentId, $fetchAssociatedModels);
    }

    /**
     * @param array $searchFilters
     * @param string $organizationId
     * @return array
     */
    public function getAllCFDocuments( array $searchFilters = [ "limit" => 100, "offset" => 0, "sorting" => "desc", "orderBy" => "documents.updated_at"],$organizationId='',$ischeck = 0) {


        $query = DB::table('documents')
        ->where('documents.is_deleted', '=', 0)
        ->where('documents.document_type', 1) // only taxonomy data, filter out pacing guide data
        ->where('documents.status',3);// Showing publish taxonomy case
        if($ischeck == 1)
        {
            $query = $query->where('documents.organization_id',$organizationId);
        }
        // extract filters
        extract($searchFilters);
        
        // set default values for filters if not set
        if(!isset($orderBy) && !isset($sorting)){
            $orderBy = "documents.updated_at";
            $sorting = "desc";
        }

        $limit = !isset($limit) ? 100 : $limit;
        $offset = !isset($offset) ? 0 : $offset;

        // set sort order
        $query->orderBy($orderBy, $sorting);

        // set filters to paginate data
        $query->offset($offset)->limit($limit);

        // execute query
        $collection = $query->get();

        $data = $collection->isNotEmpty() ? $collection : [];
        return $data;
    }

    public function filterDocumentByAttributes(
        array $attributes,
    
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        bool $returnSearchData = false,
        string $searchColumn,
        array $searchfilters
    ){
        $query = $this->model->select('document_id', 'title', 'created_at');

        // Get the last value of the associative array
        $lastValue = end($attributes);

        // Get the last key of the associative array
        $lastKey = key($attributes);

        // Builder
        $query->where($lastKey, $lastValue);

        // Pop the last key value pair of the associative array now that it has been added to Builder already
        array_pop($attributes);


        foreach ($attributes as $key => $value) {
            $query->where($key, $value);
        }

        $searchQuery = $searchfilters['search_key'];

        if(!empty($searchQuery)){
            if (strpos($searchQuery, '_') !== false) {
                $searchQuery = str_replace('_','\_', $searchQuery);    
            }

            if (strpos($searchQuery, '%') !== false) {
                $searchQuery = str_replace('%','\%', $searchQuery);    
            }
            
            $searchCondition = "%". $searchQuery . '%';
            $query->where($searchColumn,'LIKE', $searchCondition );
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }
        
        $parent = $query->get();

        $count = $query->count();
        $response = ['taxonomies'=>$parent,'count'=>count($parent), 'totalTaxonomies'=>$count];
        return $response; 
    }

    public function addOrUpdateCustomMetadataValue(string $documentIdentifier, string $metadataIdentifier, string $value, string $value_html, string $isAdditional, string $action) {
        if($action === 'insert') {
            return DB::table('document_metadata')->insert(
                ['document_id' => $documentIdentifier, 'metadata_id' => $metadataIdentifier, 'metadata_value'    =>  $value ,'metadata_value_html'    =>  $value_html ,'is_additional' => $isAdditional]
            );
        }else {
            return DB::table('document_metadata')->where(['metadata_id' => $metadataIdentifier, 'document_id' => $documentIdentifier])->update(['metadata_value' => $value,'metadata_value_html'=>  $value_html, 'is_additional' => $isAdditional]);
        }
        
    }

    public function countMetadataValueUsage(string $metadataIdentifier, string $fieldPossibleValue) {
        $metadataValueUsage =   DB::table('document_metadata')->where(['metadata_id' => $metadataIdentifier, 'metadata_value' => $fieldPossibleValue])->get();

        return $metadataValueUsage->count();
    }

    public function saveDocumentMapping(array $attributesToSave) {
        return DB::table('document_public_reviews')->insert(
            ['original_document_id' => $attributesToSave['original_document_id'], 'copied_document_id' => $attributesToSave['copied_document_id'], 'created_by' => $attributesToSave['created_by'], 'created_at'    => $attributesToSave['timestamp'], 'updated_at'   => $attributesToSave['timestamp']]);
    }

    public function findDocumentIdsForWhichStatusToBeChanged() {
        return DB::table('document_public_reviews')->select('original_document_id')->get();
    }

    public function fetchPublicReviewHistory(string $originalDocumentIdentifier, string $organizationIdentifier) {

        return DB::table('public_review_histories')->where(['document_id'   =>  $originalDocumentIdentifier, 'organization_id'  => $organizationIdentifier])->get();
    }

    public function checkPublicReviewIsOpen(string $originalDocumentIdentifier, string $organizationIdentifier, array $reviewStatus) {

        return DB::table('public_review_histories')->where(['document_id'   =>  $originalDocumentIdentifier, 'organization_id'  => $organizationIdentifier])->whereIn('status', $reviewStatus)->get();
    }

    public function savePublicReviewHistory(array $dataToInsert) {
        return DB::table('public_review_histories')->insert($dataToInsert);
    }

    public function getPublicReviewHistory(array $documentIdentifier)
    {
        $getPublicReviewHistoryData = DB::table('public_review_histories')
                                      ->whereIn('document_id',$documentIdentifier)
                                      ->whereIn('status',array(1,2,3))
                                      ->orderBy('review_number')
                                      ->get()
                                      ->toArray();
        
        
        $createdBy=array();
        $updatedBy=array();
        $publishedBy=array();
        $getProjectId=array();
        foreach($getPublicReviewHistoryData as $getPublicReviewHistoryDataK=>$getPublicReviewHistoryDataV)
        {
            $createdBy[]=$getPublicReviewHistoryDataV->created_by;
            $updatedBy[]=$getPublicReviewHistoryDataV->updated_by;
            $publishedBy[]=$getPublicReviewHistoryDataV->document_publisehed_by;
            $getProjectId[]=$getPublicReviewHistoryDataV->project_id;
        }
        
        $uniquecreatedBy=array_unique($createdBy);
        $uniqueupdatedBy=array_unique($updatedBy);
        $uniquepublishedBy=array_unique($publishedBy);
        
        $getUserCreatedBy = DB::table('users')
                            ->select('user_id','first_name','last_name')
                            ->whereIn('user_id',$uniquecreatedBy)
                            ->where('is_deleted','0')
                            ->get()
                            ->toArray();

        $getUserUpdatedBy = DB::table('users')
                            ->select('user_id','first_name','last_name')
                            ->whereIn('user_id',$uniqueupdatedBy)
                            ->where('is_deleted','0')
                            ->get()
                            ->toArray();

        $getUserPublishedBy = DB::table('users')
                            ->select('user_id','first_name','last_name')
                            ->whereIn('user_id',$uniquepublishedBy)
                            ->where('is_deleted','0')
                            ->get()
                            ->toArray();

        $getProjectName = DB::table('projects')
                            ->select('project_id','project_name')
                            ->whereIn('project_id',$getProjectId)
                            ->where('is_deleted','0')
                            ->get()
                            ->toArray();                    
                            
        $initiatedBy=array();                            
        foreach($getUserCreatedBy as $getUserCreatedByK=>$getUserCreatedByV)
        {
            $initiatedBy[$getUserCreatedByV->user_id]=$getUserCreatedByV->first_name.' '.$getUserCreatedByV->last_name;
        }

        $completedBy=array();                            
        foreach($getUserUpdatedBy as $getUserUpdatedByK=>$getUserUpdatedByV)
        {
            $completedBy[$getUserUpdatedByV->user_id]=$getUserUpdatedByV->first_name.' '.$getUserUpdatedByV->last_name;
        }

        $docPublishedBy=array();                            
        foreach($getUserPublishedBy as $getUserPublishedByK=>$getUserPublishedByV)
        {
            $docPublishedBy[$getUserPublishedByV->user_id]=$getUserPublishedByV->first_name.' '.$getUserPublishedByV->last_name;
        }

        $projectIdMappedWithProjectName=array();
        foreach($getProjectName as $getProjectNameK=>$getProjectNameV)
        {
            $projectIdMappedWithProjectName[$getProjectNameV->project_id]=$getProjectNameV->project_name;     
        }
        
        $publicReviewHistory=array();
        if(!empty($docPublishedBy))
        {
        // if the public review project is published start
        foreach($getPublicReviewHistoryData as $getPublicReviewHistoryDataKey=>$getPublicReviewHistoryDataValue)
        {
            foreach($projectIdMappedWithProjectName as $projectIdMappedWithProjectNameK=>$projectIdMappedWithProjectNameV)
            {
                if($getPublicReviewHistoryDataValue->project_id==$projectIdMappedWithProjectNameK)
                {   
                    foreach($initiatedBy as $initiatedByK=>$initiatedByV)
                    {   
                        if($getPublicReviewHistoryDataValue->created_by==$initiatedByK)
                        {   
                            foreach($completedBy as $completedByK=>$completedByV)
                            {
                                if($getPublicReviewHistoryDataValue->updated_by==$completedByK)
                                {   
                                    foreach($docPublishedBy as $docPublishedByK=>$docPublishedByV)
                                    {
                                        if($getPublicReviewHistoryDataValue->document_publisehed_by==$docPublishedByK)
                                        {
                                            $publicReviewHistory[]=[
                                                                    "review_number"=>$getPublicReviewHistoryDataValue->review_number,
                                                                    "document_id"=>$getPublicReviewHistoryDataValue->document_id,
                                                                    "project_id"=>$getPublicReviewHistoryDataValue->project_id,
                                                                    "public_review_name"=>$projectIdMappedWithProjectNameV,
                                                                    "initiated_by"=>$initiatedByV,
                                                                    "completed_by"=>$completedByV,
                                                                    "start_date"=>$getPublicReviewHistoryDataValue->start_date,
                                                                    "end_date"=>$getPublicReviewHistoryDataValue->end_date,
                                                                    "status"=>$getPublicReviewHistoryDataValue->status,
                                                                    "doc_publish_date"=>$getPublicReviewHistoryDataValue->document_published_date,
                                                                    "doc_publish_by"=>$docPublishedByV,
                                                                    "doc_adoption_status"=>$getPublicReviewHistoryDataValue->document_status   
                                                                    ];
                                        }
                                        else if($getPublicReviewHistoryDataValue->document_publisehed_by=="")
                                        {
                                            $publicReviewHistory[]=[
                                                                    "review_number"=>$getPublicReviewHistoryDataValue->review_number,
                                                                    "document_id"=>$getPublicReviewHistoryDataValue->document_id,
                                                                    "project_id"=>$getPublicReviewHistoryDataValue->project_id,
                                                                    "public_review_name"=>$projectIdMappedWithProjectNameV,
                                                                    "initiated_by"=>$initiatedByV,
                                                                    "completed_by"=>$completedByV,
                                                                    "start_date"=>$getPublicReviewHistoryDataValue->start_date,
                                                                    "end_date"=>$getPublicReviewHistoryDataValue->end_date,
                                                                    "status"=>$getPublicReviewHistoryDataValue->status,
                                                                    "doc_publish_date"=>!empty($getPublicReviewHistoryDataValue->document_published_date)?$getPublicReviewHistoryDataValue->document_published_date:"",
                                                                    "doc_publish_by"=>!empty($getPublicReviewHistoryDataValue->document_publisehed_by)?$getPublicReviewHistoryDataValue->document_publisehed_by:"",
                                                                    "doc_adoption_status"=>$getPublicReviewHistoryDataValue->document_status   
                                                                    ];
                                        }
                                    }                          
                                }
                            }
                        }
                    }
                }
            }    
        }
        // if the public review project is published end
        }
        else{
        // if the public review project is not published start
        foreach($getPublicReviewHistoryData as $getPublicReviewHistoryDataKey=>$getPublicReviewHistoryDataValue)
        {
            foreach($projectIdMappedWithProjectName as $projectIdMappedWithProjectNameK=>$projectIdMappedWithProjectNameV)
            {
                if($getPublicReviewHistoryDataValue->project_id==$projectIdMappedWithProjectNameK)
                {
                    foreach($initiatedBy as $initiatedByK=>$initiatedByV)
                    {   
                        if($getPublicReviewHistoryDataValue->created_by==$initiatedByK)
                        {   
                            foreach($completedBy as $completedByK=>$completedByV)
                            {
                                if($getPublicReviewHistoryDataValue->updated_by==$completedByK)
                                {   
                                            $publicReviewHistory[]=[
                                                                    "review_number"=>$getPublicReviewHistoryDataValue->review_number,
                                                                    "document_id"=>$getPublicReviewHistoryDataValue->document_id,
                                                                    "project_id"=>$getPublicReviewHistoryDataValue->project_id,
                                                                    "public_review_name"=>$projectIdMappedWithProjectNameV,
                                                                    "initiated_by"=>$initiatedByV,
                                                                    "completed_by"=>$completedByV,
                                                                    "start_date"=>$getPublicReviewHistoryDataValue->start_date,
                                                                    "end_date"=>$getPublicReviewHistoryDataValue->end_date,
                                                                    "status"=>$getPublicReviewHistoryDataValue->status,
                                                                    "doc_publish_date"=>!empty($getPublicReviewHistoryDataValue->document_published_date)?$getPublicReviewHistoryDataValue->document_published_date:"",
                                                                    "doc_publish_by"=>!empty($getPublicReviewHistoryDataValue->document_publisehed_by)?$getPublicReviewHistoryDataValue->document_publisehed_by:"",
                                                                    "doc_adoption_status"=>$getPublicReviewHistoryDataValue->document_status   
                                                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }
        // if the public review project is not published start
        }

        return $publicReviewHistory;

    }

    public function listPublishedDocumentRoot($searchFilterAttribut)
    {
        $organization_id    =   $searchFilterAttribut['organization_id'];
        $status     =   $searchFilterAttribut['status'];

        $query = $this->model->select('document_id', 'title', 'description', 'updated_at','updated_by');
        $query->where('is_deleted',0);
        $query->where('status', $status);
        $query->where('organization_id',$organization_id);
        $query->orderBy('updated_at', 'desc');        
        $parent = $query->get();
        
        return $parent;
    }
    
    public function stopPublicReview(array $input , array $userData)
    {
        $stopPublicReviewStatus = DB::table('public_review_histories')
                                ->where($input)
                                ->update(['status'=>3,'updated_by'=>$userData['updated_by'],'updated_at'=>now()]);

        if($stopPublicReviewStatus===1)
        {
            $unlockTaxonomy = DB::table('documents')
                            ->where('document_id',$input['document_id'])
                            ->update(['adoption_status'=>2,'updated_by'=>$userData['updated_by'],'updated_at'=>now()]); // Adoption Status changed to 2 for Draft as per UF-3498
        }
        
        return true;
    }

   

    public function exportTaxonomy(string $documentIdentifier, array $getItemDetailToExport, array $getDocDetailToExport)
    {
        /***** Item Level Data Sorting Start ******/

        /** Get Item Detail Start */
        $getItemDetail = $this->getItemDetail($documentIdentifier);
        
        //$getItemDetailToExport = $this->getItemDetailToExport($getItemDetail);
        /** Get Item Detail End */

        /** Get Node Name For Item Detail Start */
        $getNodeName = $this->getNodeName($getItemDetail);
        /** Get Node Name For Item Detail End */

        /** Map Node Name to Item Detail Start */
        $itemDetailMappedWithNodeName = $this->itemDetailMappedWithNodeName($getItemDetailToExport,$getNodeName);

        /** Map Node Name to Item Detail End */

        /** Get Item Id from Item Detail Start */
        $getItemId = $this->getItemId($getItemDetail);
        /** Get Item Id from Item Detail End */

        /** Get Metadata Value Detail Start */
        $getMetadataValueDetail = $this->getMetadataValueDetail($getItemId);
        /** Get Metadata Value Detail End */

        /** Get Metadata Id from Metadata Value Detail Start */
        $getMetadataId = $this->getMetadataId($getMetadataValueDetail);
        /** Get Metadata Id from Metadata Value Detail Start */

        /** Get Metadata Data Detail Start */
        $getMetadataDetail = $this->getMetadataDetail($getMetadataId);
        /** Get Metadata Data Detail End */

        /** Get Metadata Array Start */
        $getMetadataInfo = $this->getMetadataInfo($getMetadataValueDetail,$getMetadataDetail);


        /** Get Metadata Array End */
        /** Item Level Information Start */
        if(!empty($getMetadataInfo))
        {
            $getItemLevelInformation = $this->getItemLevelInformation($itemDetailMappedWithNodeName,$getMetadataInfo);
        }
        else
        {
            $getItemLevelInformation = $itemDetailMappedWithNodeName;    
        }

        /** Item Level Information End */

        /***** Item Level Data Sorting End ******/

        /***** Doc Level Data Sorting Start ******/

        /** Get Doc Detail Start */
        $getDocDetail = $this->getDocDetail($documentIdentifier);
        //$getDocDetailToExport = $this->getDocDetailToExport($getDocDetail);
        /** Get Doc Detail End */

        /** Get Doc Metadata Value Detail Start */
        $getDocMetadataValueDetail = $this->getDocMetadataValueDetail($documentIdentifier);
        /** Get Doc Metadata Value Detail End */

        /** Get Doc Metadata Id from Metadata Value Detail Start */
        $getDocMetadataId = $this->getDocMetadataId($getDocMetadataValueDetail);
        /** Get Doc Metadata Id from Metadata Value Detail Start */

        /** Get Doc Metadata Data Detail Start */
        $getDocMetadataDetail = $this->getDocMetadataDetail($getDocMetadataId);
        /** Get Doc Metadata Data Detail End */

        /** Get Metadata Array Start */
        $getDocMetadataInfo = $this->getDocMetadataInfo($getDocMetadataValueDetail,$getDocMetadataDetail);
        /** Get Metadata Array End */

        /** Doc Level Information Start */
        if(!empty($getDocMetadataDetail))
        {
            $getDocLevelInformation = $this->getDocLevelInformation($getDocDetailToExport,$getDocMetadataInfo);
        }
        else
        {
            $getDocLevelInformation = $getDocDetailToExport;
        }
        /** Doc Level Information End */

        /***** Doc Level Data Sorting End ******/

        /***** Merging Doc Information and Item Information to get final result start *****/
        $exportTaxonomyData = array_merge($getDocLevelInformation,$getItemLevelInformation);
        /***** Merging Doc Information and Item Information to get final result end *****/

        return $exportTaxonomyData;
    }



    /** Get Item Detail Start */
    public function getItemDetail($documentIdentifier)
    {
        $getItemDetail = array();
        $getItemDetail = Item::select('*')
                           ->where('document_id',$documentIdentifier)
                           ->where('is_deleted','0')
                           ->get()
                           ->toArray();

        return $getItemDetail;                           
    }

    /* public function getItemDetailToExport($getItemDetail)
    {
        $getItemDetailToExport = array();

        $i=0;
        foreach($getItemDetail as $getItemDetailK=>$getItemDetailV)
        {
            $getItemDetailToExport[$i]['id'] = $getItemDetailV['item_id'];
            $getItemDetailToExport[$i]['node_id'] = $getItemDetailV['source_item_id'];
            $getItemDetailToExport[$i]['parent_id'] = $getItemDetailV['parent_id'];
            $getItemDetailToExport[$i]['title'] = "";
            $getItemDetailToExport[$i]['full_statement'] = $getItemDetailV['full_statement'];
            $getItemDetailToExport[$i]['human_coding_scheme'] = $getItemDetailV['human_coding_scheme'];
            $getItemDetailToExport[$i]['list_enumeration'] = $getItemDetailV['list_enumeration'];
            $getItemDetailToExport[$i]['node_type'] = "";
            $getItemDetailToExport[$i]['education_level'] = !empty($getItemDetailV['education_level']) ? $getItemDetailV['education_level']:"";

            $getItemDetailToExport[$i]['alternative_label'] = !empty($getItemDetailV['alternative_label']) ? $getItemDetailV['alternative_label']:"";
            
            $getItemDetailToExport[$i]['abbreviated_statement'] = !empty($getItemDetailV['abbreviated_statement']) ? $getItemDetailV['abbreviated_statement']:"";
            
            $getItemDetailToExport[$i]['notes'] = !empty($getItemDetailV['notes']) ? $getItemDetailV['notes']:"";
           
            $getItemDetailToExport[$i]['status_start_date'] = !empty($getItemDetailV['status_start_date']) ? $getItemDetailV['status_start_date']:"";
            
            $getItemDetailToExport[$i]['status_end_date'] = !empty($getItemDetailV['status_end_date']) ? $getItemDetailV['status_end_date']:"";

            $getItemDetailToExport[$i]['metadata_detail'] = array();
            $i = $i+1;
        }

        return $getItemDetailToExport;
    } */
    
    /** Get Node Name For Item Detail Start */
    public function getNodeName($getItemDetail)
    {
        $getNodeTypeId = array();
        $i=0;
        foreach($getItemDetail as $getItemDetailK=>$getItemDetailV)
        {
            $getNodeTypeId[$i] = $getItemDetailV['node_type_id'];
            $i = $i+1;
        }

        $nodeData = NodeType::select('node_type_id','title')
                            ->whereIn('node_type_id',$getNodeTypeId)
                            ->get()
                            ->toArray();
        
        $itemIdMappedNodeName = array();
        $j=0;
        foreach($getItemDetail as $getItemDetailKey=>$getItemDetailValue)
        {
            foreach($nodeData as $nodeDataK=>$nodeDataV)
            {
                if($getItemDetailValue['node_type_id']==$nodeDataV['node_type_id'])
                {
                    $itemIdMappedNodeName[$j]['item_id']=$getItemDetailValue['item_id'];
                    $itemIdMappedNodeName[$j]['node_type']=$nodeDataV['title'];
                    $j=$j+1;
                }
            }
        }    
        return $itemIdMappedNodeName;

    }
    /** Get Node Name For Item Detail End */
    

    public function itemDetailMappedWithNodeName($getItemDetailToExport,$getNodeName)
    {   
        $itemDetailMappedWithNodeName = array();
        foreach($getItemDetailToExport as $getItemDetailToExportK=>$getItemDetailToExportV)
        {
            foreach($getNodeName as $getNodeNameK=>$getNodeNameV)
            {
                if($getItemDetailToExportV['item_id']==$getNodeNameV['item_id'])
                {
                    $getItemDetailToExportV['node_type']=$getNodeNameV['node_type'];
                    $itemDetailMappedWithNodeName[]=$getItemDetailToExportV;
                }
            }
        }
        return $itemDetailMappedWithNodeName;
        
    }

    public function getItemId($getItemDetail)
    {
        $getItemId = array();

        foreach($getItemDetail as $getItemDetailK=>$getItemDetailV)
        {
            $getItemId[]=$getItemDetailV['item_id'];
        }

        return $getItemId;
    }

    public function getMetadataValueDetail($getItemId)
    {
        $getMetadataValueDetail = array();

        $getMetadataValueDetail = ItemMetadata::select('item_id','metadata_id','metadata_value')
                                              ->whereIn('item_id',$getItemId)
                                              ->where('is_deleted','0') // Is deleted 0 added since deleted metadata were shown in export
                                              ->get()
                                              ->toArray();
        # Code to solve UF-762 start
        $metaDataIdAlreadyInArr = array_column($getMetadataValueDetail, 'metadata_id');
        $getNodeTypeMetadata = DB::table('node_type_metadata')->select('items.item_id', 'node_type_metadata.metadata_id','metadata.name')
                                        ->leftJoin('items','items.node_type_id','=','node_type_metadata.node_type_id')
                                        ->leftJoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
                                        ->where('metadata.is_custom', 1)
                                        ->whereIn('items.item_id',$getItemId)
                                        ->whereNotIn('node_type_metadata.metadata_id',$metaDataIdAlreadyInArr)
                                        ->get();

        foreach($getNodeTypeMetadata as $data) {
            $getMetadataValueDetail[] = [
                'item_id' => $data->item_id ,
                'metadata_id' => $data->metadata_id,
                'metadata_value' => ''
            ];
        }
        # Code to solve UF-762 end
                                              
        return $getMetadataValueDetail;
                                              
    }

    public function getMetadataId($getMetadataValueDetail)
    {
        $getMetadataId = array();

        foreach($getMetadataValueDetail as $getMetadataValueDetailK=>$getMetadataValueDetailV)
        {
            $getMetadataId[]=$getMetadataValueDetailV['metadata_id'];
        }

        $getMetadataIdUnique=array_unique($getMetadataId);

        return $getMetadataIdUnique;

    }

    public function getMetadataDetail($getMetadataId)
    {
        $getMetadataDetail = array();

        $getMetadataDetail = Metadata::select('metadata_id','name','field_type')
                                       ->whereIn('metadata_id',$getMetadataId)
                                       ->where('is_deleted','0')
                                       ->get()
                                       ->toArray();
                                       
        return $getMetadataDetail;                                       
    }

    public function getMetadataInfo($getMetadataValueDetail,$getMetadataDetail)
    {
        $getMetadataInfo = array();

        foreach($getMetadataValueDetail as $getMetadataValueDetailK=>$getMetadataValueDetailV)
        {
            foreach($getMetadataDetail as $getMetadataDetailK=>$getMetadataDetailV)
            {
                if($getMetadataValueDetailV['metadata_id']==$getMetadataDetailV['metadata_id'])
                {
                    $getMetadataInfo[] = array_merge($getMetadataValueDetailV,$getMetadataDetailV);
                }
            }
        }

        return $getMetadataInfo;
    }

    public function getItemLevelInformation($itemDetailMappedWithNodeName,$getMetadataInfo)
    {
        $itemLevelData = array();

        foreach($itemDetailMappedWithNodeName as $itemDetailMappedWithNodeNameK=>$itemDetailMappedWithNodeNameV)
        {
            foreach($getMetadataInfo as $getMetadataInfoK=>$getMetadataInfoV)
            {
                if($itemDetailMappedWithNodeNameV['item_id']==$getMetadataInfoV['item_id'])
                {
                    unset($getMetadataInfoV['item_id']);
                    $itemDetailMappedWithNodeNameV['metadata_detail'][]=$getMetadataInfoV;
                    $itemLevelData[$itemDetailMappedWithNodeNameV['item_id']]=$itemDetailMappedWithNodeNameV;
                }
                else
                {
                    $itemLevelData[$itemDetailMappedWithNodeNameV['item_id']]=$itemDetailMappedWithNodeNameV;
                }
            }
        }

        $getItemLevelInformation = array();

        foreach($itemLevelData as $itemLevelDataK=>$itemLevelDataV)
        {
            $getItemLevelInformation[] = $itemLevelDataV;
        }
        
        return $getItemLevelInformation;
    }

    /** Get Item Detail End */

    /** Get Doc Detail Start */

    public function getDocDetail($documentIdentifier)
    {
        $getDocDetail = array();
        $getDocDetail = Document::select('document_id','title','source_document_id')
                        ->where('document_id',$documentIdentifier)
                        ->get()
                        ->toArray();

        return $getDocDetail;                
    }

    /* public function getDocDetailToExport($getDocDetail)
    {
        $getDocDetailToExport = array();

        $i=0;
        foreach($getDocDetail as $getDocDetailK=>$getDocDetailV)
        {
            $getDocDetailToExport[$i]['id'] = $getDocDetailV['document_id'];
            $getDocDetailToExport[$i]['node_id'] = $getDocDetailV['source_document_id'];
            $getDocDetailToExport[$i]['parent_id'] = "";
            $getDocDetailToExport[$i]['title'] = $getDocDetailV['title'];
            $getDocDetailToExport[$i]['full_statement'] = "";
            $getDocDetailToExport[$i]['human_coding_scheme'] = "";
            $getDocDetailToExport[$i]['list_enumeration'] = "";
            $getDocDetailToExport[$i]['node_type'] = "Document";
            $getDocDetailToExport[$i]['education_level'] = "";
            $getDocDetailToExport[$i]['metadata_detail'] = array();
            $i = $i+1;
        }
        
        return $getDocDetailToExport;
    } */

    public function getDocMetadataValueDetail($documentIdentifier)
    {
        $getDocMetadataValueDetail = array();

        $getDocMetadataValueDetail = DocumentMetadata::select('document_id','metadata_id','metadata_value')
                                                ->where(['document_id'=>$documentIdentifier])
                                                ->get()
                                                ->toArray();
        
        return $getDocMetadataValueDetail;
    }

    public function getDocMetadataId($getDocMetadataValueDetail)
    {
        $getDocMetadataId = array();

        foreach($getDocMetadataValueDetail as $getDocMetadataValueDetailK=>$getDocMetadataValueDetailV)
        {
            $getDocMetadataId[]=$getDocMetadataValueDetailV['metadata_id'];
        }
        
        $getDocMetadataIdUnique=array_unique($getDocMetadataId);
        
        return $getDocMetadataIdUnique;
    }

    public function getDocMetadataDetail($getDocMetadataId)
    {
        $getDocMetadataDetail = array();
        
        $getDocMetadataDetail = Metadata::select('metadata_id','name','field_type')
                                       ->whereIn('metadata_id',$getDocMetadataId)
                                       ->where('is_deleted','0')
                                       ->get()
                                       ->toArray();
                                       
        return $getDocMetadataDetail;
    }

    public function getDocMetadataInfo($getDocMetadataValueDetail,$getDocMetadataDetail)
    {
        $getDocMetadataInfo = array();

        foreach($getDocMetadataValueDetail as $getDocMetadataValueDetailK=>$getDocMetadataValueDetailV)
        {
            foreach($getDocMetadataDetail as $getDocMetadataDetailK=>$getDocMetadataDetailV)
            {
                if($getDocMetadataValueDetailV['metadata_id']==$getDocMetadataDetailV['metadata_id'])
                {
                    $getDocMetadataInfo[] = array_merge($getDocMetadataValueDetailV,$getDocMetadataDetailV);
                }
            }
        }
        
        return $getDocMetadataInfo;
    }

    public function getDocLevelInformation($getDocDetailToExport,$getDocMetadataInfo)
    {
        $docLevelData = array();

        foreach($getDocDetailToExport as $getDocDetailToExportK=>$getDocDetailToExportV)
        {
            foreach($getDocMetadataInfo as $getDocMetadataInfoK=>$getDocMetadataInfoV)
            {
                if($getDocDetailToExportV['document_id']==$getDocMetadataInfoV['document_id'])
                {
                    unset($getDocMetadataInfoV['document_id']);
                    $getDocDetailToExportV['metadata_detail'][]=$getDocMetadataInfoV;
                    $docLevelData[$getDocDetailToExportV['document_id']]=$getDocDetailToExportV;
                }
                else
                {
                    $docLevelData[$getDocDetailToExportV['document_id']]=$getDocDetailToExportV;
                }
            }
        }

        $getDocLevelInformation = array();

        foreach($docLevelData as $docLevelDataK=>$docLevelDataV)
        {
            $getDocLevelInformation[] = $docLevelDataV;
        }
        
        return $getDocLevelInformation;

    }

    /** Get Doc Detail End */

    public function getDocDetailsWithProjectAndCaseAssociation(string $doc){
        $fetchAssociatedModels = [
            "license", 
            "language", 
            "customMetadata", 
            "nodeType",
            "nodeType.metadata",
            "itemAssociations",
            "itemAssociations.document:document_id,source_document_id,title,node_type_id",
            "itemAssociations.destinationNode:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id",
            "itemAssociations.destinationDocumentNode:document_id,source_document_id,title,node_type_id",
            
        ];
        return $this->findBy("document_id", $doc, $fetchAssociatedModels);
    }

}
