<?php
namespace App\Data\Repositories;

use App\Data\Repositories\EloquentAuditRepository;

use App\Data\Repositories\Contracts\DimUserRepositoryInterface;

use App\Data\Models\Report\DimUser;

class DimUserEloquentRepository extends EloquentAuditRepository implements DimUserRepositoryInterface
{
    public function getModel()
    {
        return DimUser::class;
    }
}