<?php

namespace App\Data\Repositories;

use Illuminate\Support\Facades\DB;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\LinkedServerRepositoryInterface;

use App\Data\Models\LinkedServer;

class LinkedServerEloquentRepository extends EloquentRepository implements LinkedServerRepositoryInterface
{

    public function getModel()
    {
        return LinkedServer::class;
    }

   
}