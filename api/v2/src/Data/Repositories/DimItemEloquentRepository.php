<?php
namespace App\Data\Repositories;

use App\Data\Repositories\EloquentAuditRepository;

use App\Data\Repositories\Contracts\DimItemRepositoryInterface;

use App\Data\Models\Report\DimItem;

class DimItemEloquentRepository extends EloquentAuditRepository implements DimItemRepositoryInterface
{
    public function getModel()
    {
        return DimItem::class;
    }
}