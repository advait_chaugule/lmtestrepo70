<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\Thread;

class ThreadEloquentRepository extends EloquentRepository implements ThreadRepositoryInterface
{

    public function getModel()
    {
        return Thread::class;
    }

    /* List all ROLES for an organization 
     * with Search and Sort parameter
     * which are optional
     */
    public function getItemCommentList(Request $request)
    {
        $allowedOrderByFields = ['name',  'status'];
        $allowedOrderByMethods = ['asc', 'desc'];

        $limit = 10;
        if ($request->has('limit') && is_int($request->limit)) {
            $limit = $request->limit;
        }

        $offset = 0;
        if ($request->has('offset') && is_int($request->offset)) {
            $offset = $request->offset;
        }

        $organizationId = $request->auth_user['organization_id'];

       // return $this->model->take($limit)->skip($offset)
       return $this->model
        ->when($request->has('sort'), function($query) use ($request, $allowedOrderByFields, $allowedOrderByMethods) {
            list($orderBy, $sorting) = explode(':', $request->sort);
            if (in_array($orderBy, $allowedOrderByFields) && in_array($sorting, $allowedOrderByMethods)) {
                $query->orderBy($orderBy, $sorting);
            }            
        })
        ->where('organization_id', $organizationId)
        ->where('is_deleted', 0)
        ->when($request->has('search'), function($query) use ($request) {
            $query->orWhere('name', 'like', "%{$request->search}%");
            $query->orWhere('description', 'like', "%{$request->search}%");
        })
        ->select('role_id', 'name', 'description', 'is_active')
        ->get();
    }
}