<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\ThreadComment;

class ThreadCommentEloquentRepository extends EloquentRepository implements ThreadCommentRepositoryInterface
{

    public function getModel()
    {
        return ThreadComment::class;
    }

    public function getCommentReport(array $userData, array $projectId)
    {   
        $itemLinkedWithProject= $this->getItemsAssociatedWithProject($projectId);
        $itemAvailableForComment = $this->getItemData($itemLinkedWithProject);
        $itemsCommented = $this->getItemsCommented($itemLinkedWithProject,$userData);
        
        // get list of item id for which comment is made and it's count start
        $itemsCommentMadeListArr=array();
        foreach($itemAvailableForComment as $itemAvailableForCommentKey=>$itemAvailableForCommentValue)
        {
            foreach($itemsCommented as $itemsCommentedKey=>$itemsCommentedValue)
            {
                if($itemAvailableForCommentValue->item_id==$itemsCommentedValue->thread_source_id && $itemsCommentedValue->comment!="")
                {   
                    array_push($itemsCommentMadeListArr,$itemAvailableForCommentValue->item_id);
                }
            }
        }

        $totalNodesCommentedCount=count($itemsCommentMadeListArr);
        // get list of item id for which comment is made and it's count end

        // get list of items available for which comment is to be made and it's count start
        $itemAvailableForCommentListArr=array();
        foreach($itemAvailableForComment as $itemAvailableForCommentKey=>$itemAvailableForCommentValue){
            array_push($itemAvailableForCommentListArr,$itemAvailableForCommentValue->item_id);
        }

        $totalNodesAvaiableForCommentCount=count($itemAvailableForCommentListArr);
        // get list of items available for which comment is to be made and it's count start

        // get list of items for which comments is pending start
        $nodesPendingForComments = array_diff($itemAvailableForCommentListArr,$itemsCommentMadeListArr);
        // get list of items for which comments is pending end

        // get list of item id mapped with full statement and human coding scheme for which comment is pending start
        $nodesPendingForCommentsList=array();
        foreach($nodesPendingForComments as $nodesPendingForCommentsValue){
            foreach($itemAvailableForComment as $itemAvailableForCommentK=>$itemAvailableForCommentV){
                if($nodesPendingForCommentsValue==$itemAvailableForCommentV->item_id)
                {
                    $nodesPendingForCommentsList[$itemAvailableForCommentV->item_id]=$itemAvailableForCommentV->full_statement.' '.$itemAvailableForCommentV->human_coding_scheme;
                }
            }
        }
        // get list of item id mapped with full statement and human coding scheme for which comment is pending end

        $reviewCompletionPercentage=round(($totalNodesCommentedCount/$totalNodesAvaiableForCommentCount)*100);

        $uncommentedNodeKeyValuePair=array();
        $i=0;
        foreach($nodesPendingForCommentsList as $nodesPendingForCommentsListKey=>$nodesPendingForCommentsListValue)
        {   
            $uncommentedNodeKeyValuePair[$i]['item_id']=$nodesPendingForCommentsListKey;
            $uncommentedNodeKeyValuePair[$i]['value']=$nodesPendingForCommentsListValue;
            $i++;
        }

        
        $commentReport=array();
        $commentReport['comment_done_nodes']=$totalNodesCommentedCount;
        $commentReport['comment_total_nodes']=$totalNodesAvaiableForCommentCount;
        $commentReport['review_completion_percentage']=$reviewCompletionPercentage;
        $commentReport['list_pending_nodes']=$uncommentedNodeKeyValuePair;


        return $commentReport;
    }

    public function getItemsAssociatedWithProject($projectId)
    {
        $itemsAssociatedWithProject = DB::table('project_items')
        ->select('item_id')
        ->where($projectId)
        ->where('is_deleted','0')
        ->get()
        ->toArray();

        $itemLinkedWithProject=array();
        foreach($itemsAssociatedWithProject as $itemsAssociatedWithProjectKey=>$itemsAssociatedWithProjectValue){
            foreach($itemsAssociatedWithProjectValue as $itemId){
                array_push($itemLinkedWithProject,$itemId);
            }
        }

        return $itemLinkedWithProject;
    }

    public function getItemData($itemLinkedWithProject)
    {
        $itemAvailableForComment = DB::table('items')
        ->select('item_id','document_id','full_statement','human_coding_scheme')
        ->whereIn('item_id',$itemLinkedWithProject)
        ->where('is_deleted','0')
        ->get()
        ->toArray();

        return $itemAvailableForComment;
    }

    public function getItemsCommented($itemLinkedWithProject,$userData)
    {
        $itemsCommented = DB::table('thread_comments')
        ->select('thread_source_id','comment')
        ->whereIn('thread_source_id',$itemLinkedWithProject)
        ->where('created_by',$userData['auth_user']['user_id'])
        ->where('is_deleted','0')
        ->groupBy('thread_source_id')
        ->get()
        ->toArray();

        return $itemsCommented;
    }

    public function getItemsCommentMadeFor(array $itemsDetails)
    {
        $count=count($itemsDetails);
        for($i=0;$i<=$count-1;$i++){
            $itemsId[]=$itemsDetails[$i]->item_id;
        }
        $itemsCommentMadeFor = DB::table('threads')
                    ->whereIn('thread_source_id', $itemsId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        return $itemsCommentMadeFor;            
    }

    public function getCommentList(array $itemsDetails,array $itemsCommentMadeFor)
    {   
        // Project : Item Level Start
        $threadId=array();
        $assignToId=array();
        $getAssignedStatusAndThreadSourceType=array();
        $getUnassignedStatusAndThreadSourceType=array();
        $count=count($itemsCommentMadeFor);
        for($i=0;$i<=$count-1;$i++){
            $threadId[]=$itemsCommentMadeFor[$i]->thread_id;
            $assignToId[$itemsCommentMadeFor[$i]->thread_id]=$itemsCommentMadeFor[$i]->assign_to;
            $getAssignedStatusAndThreadSourceType[]=$itemsCommentMadeFor[$i]->thread_id;
            $getUnassignedStatusAndThreadSourceType[]=$itemsCommentMadeFor[$i]->thread_id;
        }
        
        $threadComment = DB::table('thread_comments')
                    ->whereIn('thread_id', $threadId)
                    ->where('parent_id','')
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        
        $getUserIdForParent=array();
        $getThreadIdForUnassignedComment=array();
        foreach($threadComment as $threadCommentK=>$threadCommentV)
        {
            $getUserIdForParent[$threadCommentV->thread_comment_id]=$threadCommentV->created_by;
            $getThreadIdForUnassignedComment[$threadCommentV->thread_comment_id]=$threadCommentV->thread_id;
        }
        $userIdForParent=array_unique($getUserIdForParent);
        $threadCommentId=array_keys($getUserIdForParent);
        

        $userDetail = DB::table('users')
                    ->whereIn('user_id', $userIdForParent)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        
        $userData = array();
        foreach($userDetail as $userDetailK=>$userDetailV)
        {
            $userData[$userDetailV->user_id] = $userDetailV->first_name.' '.$userDetailV->last_name;
        }

        $assignToIdDetail = DB::table('users')
                    ->whereIn('user_id', $assignToId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        $assginedUserData = array();
        foreach($assignToIdDetail as $assignToIdDetailK=>$assignToIdDetailV)
        {
            $assginedUserData[$assignToIdDetailV->user_id] = $assignToIdDetailV->first_name.' '.$assignToIdDetailV->last_name;
        }

        $getAssignedStatusandThreadSourceTypeDetail = DB::table('threads')
                    ->whereIn('thread_id', $getAssignedStatusAndThreadSourceType)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $parentAssignedThreadCommentData=array();
        foreach($threadComment as $threadCommentKey=>$threadCommentValue)
        {
            foreach($userData as $userDataK=>$userDataV)
            {
                if($threadCommentValue->created_by==$userDataK)
                {   
                    foreach($assignToId as $assignToIdK=>$assignToIdV)
                    {   
                        if($threadCommentValue->thread_id==$assignToIdK)
                        {
                            foreach($assginedUserData as $assginedUserDataK=>$assginedUserDataV)
                            {
                                if($assignToIdV==$assginedUserDataK)
                                {   
                                    foreach($getAssignedStatusandThreadSourceTypeDetail as $getAssignedStatusandThreadSourceTypeDetailK=>$getAssignedStatusandThreadSourceTypeDetailV)
                                    {
                                        if($threadCommentValue->thread_id==$getAssignedStatusandThreadSourceTypeDetailV->thread_id)
                                        {
                                            $parentAssignedThreadCommentData[]=[
                                                                "thread_comment_id"=>$threadCommentValue->thread_comment_id,
                                                                "thread_source_id"=>$threadCommentValue->thread_source_id,
                                                                "thread_id"=>$threadCommentValue->thread_id,
                                                                "comment"=>$threadCommentValue->comment,
                                                                "assign_to_id"=>$assginedUserDataK,
                                                                "assigned_to"=>$assginedUserDataV,
                                                                "created_by"=>$threadCommentValue->created_by,
                                                                "created_by_name"=>$userDataV,
                                                                "updated_at"=>$getAssignedStatusandThreadSourceTypeDetailV->updated_at,
                                                                "thread_source_type"=>$getAssignedStatusandThreadSourceTypeDetailV->thread_source_type,
                                                                "status"=>$getAssignedStatusandThreadSourceTypeDetailV->status,
                                                                "created_at"=>$getAssignedStatusandThreadSourceTypeDetailV->created_at,
                                                                "comments"=>[]
                                                                ];
                                        }
                                    }
                                }
                            }
                                
                        }
                         
                    }   
                                       
                }
            }
        }
        
        // unassigned comment start

        $getUniqueThreadIdForUnassignedComment=array_unique($getThreadIdForUnassignedComment);
        $getThreadDetails = DB::table('threads')
                    ->whereIn('thread_id', $getUniqueThreadIdForUnassignedComment)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        $getUnassignedStatusandThreadSourceTypeDetail = DB::table('threads')
                                                ->whereIn('thread_id', $getUnassignedStatusAndThreadSourceType)
                                                ->where('is_deleted','0')
                                                ->get()
                                                ->toArray();
                                                
                  
        $parentUnassignedThreadCommentData = array();
        foreach($threadComment as $threadCommentKData=>$threadCommentVData)
        {   
            foreach($userData as $userDataKey=>$userDataValue)
            {
                if($threadCommentVData->created_by==$userDataKey)
                {   
                    foreach($getThreadDetails as $getThreadDetailsK=>$getThreadDetailsV)
                        {
                            if($threadCommentVData->thread_id==$getThreadDetailsV->thread_id && $getThreadDetailsV->assign_to=="")
                                {   
                                    foreach($getUnassignedStatusandThreadSourceTypeDetail as $getUnassignedStatusandThreadSourceTypeDetailK=>$getUnassignedStatusandThreadSourceTypeDetailV)
                                    {
                                        if($threadCommentVData->thread_id==$getUnassignedStatusandThreadSourceTypeDetailV->thread_id)
                                        {

                                            $parentUnassignedThreadCommentData[]=[
                                                                "thread_comment_id"=>$threadCommentVData->thread_comment_id,
                                                                "thread_source_id"=>$threadCommentVData->thread_source_id,
                                                                "thread_id"=>$threadCommentVData->thread_id,
                                                                "comment"=>$threadCommentVData->comment,
                                                                "assign_to_id"=>$getThreadDetailsV->assign_to,
                                                                "assigned_to"=>$getThreadDetailsV->assign_to,
                                                                "created_by"=>$threadCommentVData->created_by,
                                                                "created_by_name"=>$userDataValue,
                                                                "updated_at"=>$getUnassignedStatusandThreadSourceTypeDetailV->updated_at,
                                                                "thread_source_type"=>$getUnassignedStatusandThreadSourceTypeDetailV->thread_source_type,
                                                                "status"=>$getUnassignedStatusandThreadSourceTypeDetailV->status,
                                                                "created_at"=>$getUnassignedStatusandThreadSourceTypeDetailV->created_at,
                                                                "comments"=>[]
                                                            ];
                                        }                    
                                    }                        
                                }
                        }
                }
            }
        }
        // unassgined comment end

        // merging assinged and unassigned parent comment start
        $parentThreadCommentData=array();
        $parentThreadCommentData=array_merge($parentAssignedThreadCommentData,$parentUnassignedThreadCommentData);
        // merging assinged and unassigned parent comment start

        // calling sorting function starts
        usort($parentThreadCommentData, function($a, $b) {
            return $a['updated_at'] < $b['updated_at'];
        });
        // calling sorting function ends

        $childCommentData = DB::table('thread_comments')
                    ->whereIn('parent_id', $threadCommentId)
                    ->where('is_deleted','0')
                    ->orderBy('updated_at','DESC')
                    ->get()
                    ->toArray();

        
        $getUserIdForChild = array();
        foreach($childCommentData as $childCommentDataK=>$childCommentDataV)
        {
            $getUserIdForChild[$childCommentDataV->thread_comment_id]=$childCommentDataV->created_by;
        }
        $userIdForChild=array_unique($getUserIdForChild);
        
        $userDetailForChild = DB::table('users')
                    ->whereIn('user_id', $userIdForChild)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $userDataForChild = array();
        foreach($userDetailForChild as $userDetailForChildK=>$userDetailForChildV)
        {
            $userDataForChild[$userDetailForChildV->user_id] = $userDetailForChildV->first_name.' '.$userDetailForChildV->last_name;
        }
        
        $childCommentDataStored=array();
        foreach($childCommentData as $childCommentDataK=>$childCommentDataV)
        {   
            foreach($userDataForChild as $userDataForChildK=>$userDataForChildV)
            {
                if($childCommentDataV->created_by==$userDataForChildK)
                {
                    $childCommentDataStored[] =[
                                                    "thread_comment_id"=>$childCommentDataV->thread_comment_id,
                                                    "thread_source_id"=>$childCommentDataV->thread_source_id,
                                                    "thread_id"=>$childCommentDataV->thread_id,
                                                    "parent_id"=>$childCommentDataV->parent_id,
                                                    "comment"=>$childCommentDataV->comment,
                                                    "created_by"=>$childCommentDataV->created_by,
                                                    "created_by_name"=>$userDataForChildV,
                                                    "updated_at"=>$childCommentDataV->updated_at
                                                ]; 
                }
            }
        }
        
        foreach($parentThreadCommentData as $parentThreadCommentDataK=>$parentThreadCommentDataV)
        {
            $incr = 0;
            $childCommentArr = array();
            foreach($childCommentDataStored as $childCommentDataStoredK=>$childCommentDataStoredV)
                {   
                    if($parentThreadCommentDataV["thread_id"]==$childCommentDataStoredV["thread_id"])
                    {
                        $childCommentArr[] = $childCommentDataStoredV;
                        $parentThreadCommentDataV["comments"] = $childCommentArr;
                    }
                    $incr = $incr + 1;
                }
                unset($childCommentArr);
                $parentThreadCommentData[$parentThreadCommentDataK] = $parentThreadCommentDataV;
        }
        

        $displayCommentList=array();
        $commentDisplayList=array();
        foreach($itemsDetails as $itemsDetailsK=>$itemsDetailsV)
        {   
            $l=0;
            $k=0;
            foreach($parentThreadCommentData as $parentThreadCommentDataKey=>$parentThreadCommentDataValue)
            {
                if($itemsDetailsV->item_id==$parentThreadCommentDataValue["thread_source_id"])
                {   
                    $commentDisplayList[$itemsDetailsV->item_id][]=$parentThreadCommentDataValue;
                    
                }
                $k=$k+1;
                $l=$l+1;
                
            }

                $data=array();
                $data['node_id']=$itemsDetailsV->item_id;
                $data['node_name']=$itemsDetailsV->human_coding_scheme.' '.$itemsDetailsV->full_statement;
                if(isset($commentDisplayList[$itemsDetailsV->item_id]))
                {
                    $data['node_updated_at'] = $commentDisplayList[$itemsDetailsV->item_id][0]['updated_at'];
                }
                else
                {
                    $data['node_updated_at'] = "";
                }
               
                $data['threads']= isset($commentDisplayList[$itemsDetailsV->item_id])?$commentDisplayList[$itemsDetailsV->item_id]:array();
                $displayCommentList[]=$data;

            unset($commentDisplayList);
            
        }
        

        // Project : Item Level End

        // Project : Document Level Start
            $addOn=count($itemsDetails);
            for($a=0;$a<=$addOn-1;$a++){
                $documentId=$itemsDetails[$a]->document_id;
                
            }
            
            $documentCommentMadeFor = DB::table('threads')
                    ->where('thread_source_id', $documentId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
                    
            $documentThreadId=array();
            $documentAssignToId=array();
            foreach($documentCommentMadeFor as $documentCommentMadeForK=>$documentCommentMadeForV)
            {
                $documentThreadId[$documentCommentMadeForV->thread_id]=$documentCommentMadeForV->thread_id;
                $documentAssignToId[$documentCommentMadeForV->thread_id]=$documentCommentMadeForV->assign_to;
            }
            
            $documentComment = DB::table('thread_comments')
                    ->whereIn('thread_id', $documentThreadId)
                    ->where('parent_id','')
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
            
            $getUserIdForDocumentParent=array();
            $getThreadIdForUnassignedDocumentComment=array();
            foreach($documentComment as $documentCommentK=>$documentCommentV)
            {
                $getUserIdForDocumentParent[$documentCommentV->thread_comment_id]=$documentCommentV->created_by;
                $getThreadIdForUnassignedDocumentComment[$documentCommentV->thread_comment_id]=$documentCommentV->thread_id;
            }
            
            $userIdForDocumentParent=array_unique($getUserIdForDocumentParent);
            $threadDocumentCommentId=array_keys($getUserIdForDocumentParent);
            
            $userDetailDocument = DB::table('users')
                    ->whereIn('user_id', $userIdForDocumentParent)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
            

            $userDataDocument = array();
            foreach($userDetailDocument as $userDetailDocumentK=>$userDetailDocumentV)
            {
                $userDataDocument[$userDetailDocumentV->user_id] = $userDetailDocumentV->first_name.' '.$userDetailDocumentV->last_name;
            }
            
            $assignToIdDetailDocument = DB::table('users')
                    ->whereIn('user_id', $documentAssignToId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

            $assginedDocumentUserData = array();
            foreach($assignToIdDetailDocument as $assignToIdDetailDocumentK=>$assignToIdDetailDocumentV)
            {
                $assginedDocumentUserData[$assignToIdDetailDocumentV->user_id] = $assignToIdDetailDocumentV->first_name.' '.$assignToIdDetailDocumentV->last_name;
            }
            
            $parentAssignedThreadCommentDataDocument=array();
            foreach($documentComment as $documentCommentKey=>$documentCommentValue)
            {
                foreach($userDataDocument as $userDataDocumentK=>$userDataDocumentV)
                {
                    if($documentCommentValue->created_by==$userDataDocumentK)
                    {   
                        foreach($documentAssignToId as $documentAssignToIdK=>$documentAssignToIdV)
                        {   
                            if($documentCommentValue->thread_id==$documentAssignToIdK)
                            {
                                foreach($assginedDocumentUserData as $assginedDocumentUserDataK=>$assginedDocumentUserDataV)
                                {
                                    if($documentAssignToIdV==$assginedDocumentUserDataK)
                                    {
                                        foreach($documentCommentMadeFor as $documentCommentMadeForDataK=>$documentCommentMadeForDataV)
                                        {
                                            if($documentCommentValue->thread_id==$documentCommentMadeForDataV->thread_id)
                                            {   
                                                $parentAssignedThreadCommentDataDocument[]=[
                                                                    "thread_comment_id"=>$documentCommentValue->thread_comment_id,
                                                                    "thread_source_id"=>$documentCommentValue->thread_source_id,
                                                                    "thread_id"=>$documentCommentValue->thread_id,
                                                                    "comment"=>$documentCommentValue->comment,
                                                                    "assign_to_id"=>$assginedDocumentUserDataK,
                                                                    "assigned_to"=>$assginedDocumentUserDataV,
                                                                    "created_by"=>$documentCommentValue->created_by,
                                                                    "created_by_name"=>$userDataDocumentV,
                                                                    "updated_at"=>$documentCommentMadeForDataV->updated_at,
                                                                    "thread_source_type"=>$documentCommentMadeForDataV->thread_source_type,
                                                                    "status"=>$documentCommentMadeForDataV->status,
                                                                    "created_at"=>$documentCommentMadeForDataV->created_at,
                                                                    "comments"=>[]
                                                                    ];
                                            }
                                        }                            
                                        
                                    }
                                }
                                    
                            }
                            
                        }   
                                        
                    }
                }
            }

            // unassinged comment start
            
            $getUniqueThreadIdForUnassignedDocumentComment=array_unique($getThreadIdForUnassignedDocumentComment);
            $getThreadDocumentDetails = DB::table('threads')
                    ->whereIn('thread_id', $getUniqueThreadIdForUnassignedDocumentComment)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
            
            $parentUnassignedThreadCommentDataDocument=array();
            foreach($documentComment as $documentCommentDataKey=>$documentCommentDataValue)
            {
                foreach($userDataDocument as $userDataDocumentDataK=>$userDataDocumentDataV)
                {
                    if($documentCommentDataValue->created_by==$userDataDocumentDataK)
                    {   
                        foreach($getThreadDocumentDetails as $getThreadDocumentDetailsK=>$getThreadDocumentDetailsV)
                        {   
                            if($documentCommentDataValue->thread_id==$getThreadDocumentDetailsV->thread_id && $getThreadDocumentDetailsV->assign_to=="")
                            {   
                                foreach($documentCommentMadeFor as $documentCommentMadeForInfoK=>$documentCommentMadeForInfoV)
                                {
                                    if($documentCommentDataValue->thread_id==$documentCommentMadeForInfoV->thread_id)
                                    {
                                        $parentUnassignedThreadCommentDataDocument[]=[
                                                                            "thread_comment_id"=>$documentCommentDataValue->thread_comment_id,
                                                                            "thread_source_id"=>$documentCommentDataValue->thread_source_id,
                                                                            "thread_id"=>$documentCommentDataValue->thread_id,
                                                                            "comment"=>$documentCommentDataValue->comment,
                                                                            "assign_to_id"=>$getThreadDocumentDetailsV->assign_to,
                                                                            "assigned_to"=>$getThreadDocumentDetailsV->assign_to,
                                                                            "created_by"=>$documentCommentDataValue->created_by,
                                                                            "created_by_name"=>$userDataDocumentDataV,
                                                                            "updated_at"=>$documentCommentMadeForInfoV->updated_at,
                                                                            "thread_source_type"=>$documentCommentMadeForInfoV->thread_source_type,
                                                                            "status"=>$documentCommentMadeForInfoV->status,
                                                                            "created_at"=>$documentCommentMadeForInfoV->created_at,
                                                                            "comments"=>[]
                                                                            ];

                                    }
                                                            
                                }
                                        
                            }
                            
                        }   
                                        
                    }
                }
            }
            
            // unassinged comment end

            // merging parent assigned and unassigned comment start
            $parentThreadCommentDataDocument=array();
            $parentThreadCommentDataDocument=array_merge($parentAssignedThreadCommentDataDocument,$parentUnassignedThreadCommentDataDocument);
            // merging parent assigned and unassigned comment end

            // calling sorting function starts
            usort($parentThreadCommentDataDocument, function($c, $d) {
                return $c['updated_at'] < $d['updated_at'];
            });
            // calling sorting function ends
            

            $childCommentDataDocument = DB::table('thread_comments')
                    ->whereIn('parent_id', $threadDocumentCommentId)
                    ->where('is_deleted','0')
                    ->orderBy('updated_at','DESC')
                    ->get()
                    ->toArray();

        
            $getUserIdForChildDocument = array();
            foreach($childCommentDataDocument as $childCommentDataDocumentK=>$childCommentDataDocumentV)
            {
                $getUserIdForChildDocument[$childCommentDataDocumentV->thread_comment_id]=$childCommentDataDocumentV->created_by;
            }
            $userIdForChildDocument=array_unique($getUserIdForChildDocument);

            $userDetailForChildDocument = DB::table('users')
                    ->whereIn('user_id', $userIdForChildDocument)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
            $userDataForChildDocument = array();
            foreach($userDetailForChildDocument as $userDetailForChildDocumentK=>$userDetailForChildDocumentV)
            {
                $userDataForChildDocument[$userDetailForChildDocumentV->user_id] = $userDetailForChildDocumentV->first_name.' '.$userDetailForChildDocumentV->last_name;
            }

            $childCommentDataStoredDocument=array();
            foreach($childCommentDataDocument as $childCommentDataDocumentK=>$childCommentDataDocumentV)
            {   
                foreach($userDataForChildDocument as $userDataForChildDocumentK=>$userDataForChildDocumentV)
                {
                    if($childCommentDataDocumentV->created_by==$userDataForChildDocumentK)
                    {
                        $childCommentDataStoredDocument[] =[
                                                        "thread_comment_id"=>$childCommentDataDocumentV->thread_comment_id,
                                                        "thread_source_id"=>$childCommentDataDocumentV->thread_source_id,
                                                        "thread_id"=>$childCommentDataDocumentV->thread_id,
                                                        "parent_id"=>$childCommentDataDocumentV->parent_id,
                                                        "comment"=>$childCommentDataDocumentV->comment,
                                                        "created_by"=>$childCommentDataDocumentV->created_by,
                                                        "created_by_name"=>$userDataForChildDocumentV,
                                                        "updated_at"=>$childCommentDataDocumentV->updated_at
                                                    ]; 
                    }
                }
            }
            
            foreach($parentThreadCommentDataDocument as $parentThreadCommentDataDocumentK=>$parentThreadCommentDataDocumentV)
            {
                $incrm = 0;
                $childCommentArrDocument = array();
                foreach($childCommentDataStoredDocument as $childCommentDataStoredDocumentK=>$childCommentDataStoredDocumentV)
                    {   
                        if($parentThreadCommentDataDocumentV["thread_id"]==$childCommentDataStoredDocumentV["thread_id"])
                        {
                            $childCommentArrDocument[] = $childCommentDataStoredDocumentV;
                            $parentThreadCommentDataDocumentV["comments"] = $childCommentArrDocument;
                        }
                        $incrm = $incrm + 1;
                    }
                    unset($childCommentArrDocument);
                    $parentThreadCommentDataDocument[$parentThreadCommentDataDocumentK] = $parentThreadCommentDataDocumentV;
            }
            
            $getDocumentDataDetail = DB::table('documents')
                    ->where('document_id', $documentId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

            
            $displayCommentListDocument=array();
            $commentDisplayListDocument=array();
        
            foreach($getDocumentDataDetail as $getDocumentDataDetailK=>$getDocumentDataDetailV)
            {   
                $g=0;
                $p=0;
                foreach($parentThreadCommentDataDocument as $parentThreadCommentDataDocumentKey=>$parentThreadCommentDataDocumentValue)
                {
                    if($getDocumentDataDetailV->document_id==$parentThreadCommentDataDocumentValue["thread_source_id"])
                    {   
                        $commentDisplayListDocument[$getDocumentDataDetailV->document_id][]=$parentThreadCommentDataDocumentValue;
                    }
                    $g=$g+1;
                    $p=$p+1;
                    
                }

                $dataDocument=array();
                $dataDocument['node_id']=$getDocumentDataDetailV->document_id;
                $dataDocument['node_name']=$getDocumentDataDetailV->title;
                if(isset($commentDisplayListDocument[$getDocumentDataDetailV->document_id]))
                {
                    $dataDocument['node_updated_at'] = $commentDisplayListDocument[$getDocumentDataDetailV->document_id][0]['updated_at'];
                }
                else
                {
                    $dataDocument['node_updated_at'] = "";
                }
                $dataDocument['threads']= isset($commentDisplayListDocument[$getDocumentDataDetailV->document_id])?$commentDisplayListDocument[$getDocumentDataDetailV->document_id]:array();
                $displayCommentListDocument[]=$dataDocument;

                unset($commentDisplayListDocument);
                
            }
            
        // Project : Document Level End
        $projectCommentList=array_merge($displayCommentList,$displayCommentListDocument);
        return $projectCommentList;
            
    }

    public function getDocumentCommentList(array $itemsDetails,array $itemsCommentMadeFor)
    {
        // Taxonomy : Item Level Start
        $threadId=array();
        $assignToId=array();
        $getAssignedStatusAndThreadSourceType=array();
        $getUnassignedStatusAndThreadSourceType=array();
        $count=count($itemsCommentMadeFor);
        for($i=0;$i<=$count-1;$i++){
            $threadId[]=$itemsCommentMadeFor[$i]->thread_id;
            $assignToId[$itemsCommentMadeFor[$i]->thread_id]=$itemsCommentMadeFor[$i]->assign_to;
            $getAssignedStatusAndThreadSourceType[]=$itemsCommentMadeFor[$i]->thread_id;
            $getUnassignedStatusAndThreadSourceType[]=$itemsCommentMadeFor[$i]->thread_id;
        }

        $threadComment = DB::table('thread_comments')
                    ->whereIn('thread_id', $threadId)
                    ->where('parent_id','')
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        
        $getUserIdForParent=array();
        $getThreadIdForUnassignedComment=array();//
        foreach($threadComment as $threadCommentK=>$threadCommentV)
        {
            $getUserIdForParent[$threadCommentV->thread_comment_id]=$threadCommentV->created_by;
            $getThreadIdForUnassignedComment[$threadCommentV->thread_comment_id]=$threadCommentV->thread_id;//
        }
        $userIdForParent=array_unique($getUserIdForParent);
        $threadCommentId=array_keys($getUserIdForParent);
        

        $userDetail = DB::table('users')
                    ->whereIn('user_id', $userIdForParent)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        
        $userData = array();
        foreach($userDetail as $userDetailK=>$userDetailV)
        {
            $userData[$userDetailV->user_id] = $userDetailV->first_name.' '.$userDetailV->last_name;
        }

        $assignToIdDetail = DB::table('users')
                    ->whereIn('user_id', $assignToId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        $assginedUserData = array();
        foreach($assignToIdDetail as $assignToIdDetailK=>$assignToIdDetailV)
        {
            $assginedUserData[$assignToIdDetailV->user_id] = $assignToIdDetailV->first_name.' '.$assignToIdDetailV->last_name;
        }

        $getAssignedStatusandThreadSourceTypeDetail = DB::table('threads')
                    ->whereIn('thread_id', $getAssignedStatusAndThreadSourceType)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $parentAssignedThreadCommentData=array();//
        foreach($threadComment as $threadCommentKey=>$threadCommentValue)
        {
            foreach($userData as $userDataK=>$userDataV)
            {
                if($threadCommentValue->created_by==$userDataK)
                {   
                    foreach($assignToId as $assignToIdK=>$assignToIdV)
                    {   
                        if($threadCommentValue->thread_id==$assignToIdK)
                        {
                            foreach($assginedUserData as $assginedUserDataK=>$assginedUserDataV)
                            {
                                if($assignToIdV==$assginedUserDataK)
                                {
                                    foreach($getAssignedStatusandThreadSourceTypeDetail as $getAssignedStatusandThreadSourceTypeDetailK=>$getAssignedStatusandThreadSourceTypeDetailV)
                                    {
                                        if($threadCommentValue->thread_id==$getAssignedStatusandThreadSourceTypeDetailV->thread_id)
                                        {

                                            $parentAssignedThreadCommentData[]=[
                                                                "thread_comment_id"=>$threadCommentValue->thread_comment_id,
                                                                "thread_source_id"=>$threadCommentValue->thread_source_id,
                                                                "thread_id"=>$threadCommentValue->thread_id,
                                                                "comment"=>$threadCommentValue->comment,
                                                                "assign_to_id"=>$assginedUserDataK,
                                                                "assigned_to"=>$assginedUserDataV,
                                                                "created_by"=>$threadCommentValue->created_by,
                                                                "created_by_name"=>$userDataV,
                                                                "updated_at"=>$getAssignedStatusandThreadSourceTypeDetailV->updated_at,
                                                                "thread_source_type"=>$getAssignedStatusandThreadSourceTypeDetailV->thread_source_type,
                                                                "status"=>$getAssignedStatusandThreadSourceTypeDetailV->status,
                                                                "created_at"=>$getAssignedStatusandThreadSourceTypeDetailV->created_at,
                                                                "comments"=>[]
                                                                ];
                                        }                        
                                    }
                                }
                            }
                                
                        }
                         
                    }   
                                       
                }
            }
        }
        
        // unassigned comment start

        $getUniqueThreadIdForUnassignedComment=array_unique($getThreadIdForUnassignedComment);
        $getThreadDetails = DB::table('threads')
                    ->whereIn('thread_id', $getUniqueThreadIdForUnassignedComment)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

        $getUnassignedStatusandThreadSourceTypeDetail = DB::table('threads')
                    ->whereIn('thread_id', $getUnassignedStatusAndThreadSourceType)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();            
                  
        $parentUnassignedThreadCommentData = array();
        foreach($threadComment as $threadCommentKData=>$threadCommentVData)
        {   
            foreach($userData as $userDataKey=>$userDataValue)
            {
                if($threadCommentVData->created_by==$userDataKey)
                {   
                    foreach($getThreadDetails as $getThreadDetailsK=>$getThreadDetailsV)
                        {
                            if($threadCommentVData->thread_id==$getThreadDetailsV->thread_id && $getThreadDetailsV->assign_to=="")
                                {
                                    foreach($getUnassignedStatusandThreadSourceTypeDetail as $getUnassignedStatusandThreadSourceTypeDetailK=>$getUnassignedStatusandThreadSourceTypeDetailV)
                                    {
                                         if($threadCommentVData->thread_id==$getUnassignedStatusandThreadSourceTypeDetailV->thread_id)
                                            {     
                                                $parentUnassignedThreadCommentData[]=[
                                                                        "thread_comment_id"=>$threadCommentVData->thread_comment_id,
                                                                        "thread_source_id"=>$threadCommentVData->thread_source_id,
                                                                        "thread_id"=>$threadCommentVData->thread_id,
                                                                        "comment"=>$threadCommentVData->comment,
                                                                        "assign_to_id"=>$getThreadDetailsV->assign_to,
                                                                        "assigned_to"=>$getThreadDetailsV->assign_to,
                                                                        "created_by"=>$threadCommentVData->created_by,
                                                                        "created_by_name"=>$userDataValue,
                                                                        "updated_at"=>$getUnassignedStatusandThreadSourceTypeDetailV->updated_at,
                                                                        "thread_source_type"=>$getUnassignedStatusandThreadSourceTypeDetailV->thread_source_type,
                                                                        "status"=>$getUnassignedStatusandThreadSourceTypeDetailV->status,
                                                                        "created_at"=>$getUnassignedStatusandThreadSourceTypeDetailV->created_at,
                                                                        "comments"=>[]
                                                                    ];
                                            }
                                    }
                                }
                        }
                }
            }
        }
        // unassgined comment end

        // merging assinged and unassigned parent comment start
        $parentThreadCommentData=array();
        $parentThreadCommentData=array_merge($parentAssignedThreadCommentData,$parentUnassignedThreadCommentData);
        // merging assinged and unassigned parent comment start

        // calling sorting function starts
        usort($parentThreadCommentData, function($a, $b) {
            return $a['updated_at'] < $b['updated_at'];
        });
        // calling sorting function ends

        $childCommentData = DB::table('thread_comments')
                    ->whereIn('parent_id', $threadCommentId)
                    ->where('is_deleted','0')
                    ->orderBy('updated_at','DESC')
                    ->get()
                    ->toArray();

        
        $getUserIdForChild = array();
        foreach($childCommentData as $childCommentDataK=>$childCommentDataV)
        {
            $getUserIdForChild[$childCommentDataV->thread_comment_id]=$childCommentDataV->created_by;
        }
        $userIdForChild=array_unique($getUserIdForChild);
        
        $userDetailForChild = DB::table('users')
                    ->whereIn('user_id', $userIdForChild)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $userDataForChild = array();
        foreach($userDetailForChild as $userDetailForChildK=>$userDetailForChildV)
        {
            $userDataForChild[$userDetailForChildV->user_id] = $userDetailForChildV->first_name.' '.$userDetailForChildV->last_name;
        }
        
        $childCommentDataStored=array();
        foreach($childCommentData as $childCommentDataK=>$childCommentDataV)
        {   
            foreach($userDataForChild as $userDataForChildK=>$userDataForChildV)
            {
                if($childCommentDataV->created_by==$userDataForChildK)
                {
                    $childCommentDataStored[] =[
                                                    "thread_comment_id"=>$childCommentDataV->thread_comment_id,
                                                    "thread_source_id"=>$childCommentDataV->thread_source_id,
                                                    "thread_id"=>$childCommentDataV->thread_id,
                                                    "parent_id"=>$childCommentDataV->parent_id,
                                                    "comment"=>$childCommentDataV->comment,
                                                    "created_by"=>$childCommentDataV->created_by,
                                                    "created_by_name"=>$userDataForChildV,
                                                    "updated_at"=>$childCommentDataV->updated_at
                                                ]; 
                }
            }
        }
        
        foreach($parentThreadCommentData as $parentThreadCommentDataK=>$parentThreadCommentDataV)
        {
            $incr = 0;
            $childCommentArr = array();
            foreach($childCommentDataStored as $childCommentDataStoredK=>$childCommentDataStoredV)
                {   
                    if($parentThreadCommentDataV["thread_id"]==$childCommentDataStoredV["thread_id"])
                    {
                        $childCommentArr[] = $childCommentDataStoredV;
                        $parentThreadCommentDataV["comments"] = $childCommentArr;
                    }
                    $incr = $incr + 1;
                }
                unset($childCommentArr);
                $parentThreadCommentData[$parentThreadCommentDataK] = $parentThreadCommentDataV;
        }
        

        $displayCommentList=array();
        $commentDisplayList=array();
        foreach($itemsDetails as $itemsDetailsK=>$itemsDetailsV)
        {   
            $l=0;
            $k=0;
            foreach($parentThreadCommentData as $parentThreadCommentDataKey=>$parentThreadCommentDataValue)
            {
                if($itemsDetailsV->item_id==$parentThreadCommentDataValue["thread_source_id"])
                {   
                    $commentDisplayList[$itemsDetailsV->item_id][]=$parentThreadCommentDataValue;
                    
                }
                $k=$k+1;
                $l=$l+1;
                
            }

                $data=array();
                $data['node_id']=$itemsDetailsV->item_id;
                $data['node_name']=$itemsDetailsV->human_coding_scheme.' '.$itemsDetailsV->full_statement;
                if(isset($commentDisplayList[$itemsDetailsV->item_id]))
                {
                    $data['node_updated_at'] = $commentDisplayList[$itemsDetailsV->item_id][0]['updated_at'];
                }
                else
                {
                    $data['node_updated_at'] = "";
                }
                $data['threads']= isset($commentDisplayList[$itemsDetailsV->item_id])?$commentDisplayList[$itemsDetailsV->item_id]:array();
                $displayCommentList[]=$data;

            unset($commentDisplayList);
            
        }

        // Taxonomy : Item Level End

        // Taxonomy : Document Level Start

        $addOn=count($itemsDetails);
            for($a=0;$a<=$addOn-1;$a++){
                $documentId=$itemsDetails[$a]->document_id;
                
            }
            
            $documentCommentMadeFor = DB::table('threads')
                    ->where('thread_source_id', $documentId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
            
            $documentThreadId=array();
            $documentAssignToId=array();        
            foreach($documentCommentMadeFor as $documentCommentMadeForK=>$documentCommentMadeForV)
            {
                $documentThreadId[$documentCommentMadeForV->thread_id]=$documentCommentMadeForV->thread_id;
                $documentAssignToId[$documentCommentMadeForV->thread_id]=$documentCommentMadeForV->assign_to;
            }
            
            $documentComment = DB::table('thread_comments')
                    ->whereIn('thread_id', $documentThreadId)
                    ->where('parent_id','')
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
            
            $getUserIdForDocumentParent=array();
            $getThreadIdForUnassignedDocumentComment=array();//
            foreach($documentComment as $documentCommentK=>$documentCommentV)
            {
                $getUserIdForDocumentParent[$documentCommentV->thread_comment_id]=$documentCommentV->created_by;
                $getThreadIdForUnassignedDocumentComment[$documentCommentV->thread_comment_id]=$documentCommentV->thread_id;//
            }
            $userIdForDocumentParent=array_unique($getUserIdForDocumentParent);
            $threadDocumentCommentId=array_keys($getUserIdForDocumentParent);
            
            $userDetailDocument = DB::table('users')
                    ->whereIn('user_id', $userIdForDocumentParent)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
            

            $userDataDocument = array();
            foreach($userDetailDocument as $userDetailDocumentK=>$userDetailDocumentV)
            {
                $userDataDocument[$userDetailDocumentV->user_id] = $userDetailDocumentV->first_name.' '.$userDetailDocumentV->last_name;
            }
            
            $assignToIdDetailDocument = DB::table('users')
                    ->whereIn('user_id', $documentAssignToId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

            $assginedDocumentUserData = array();
            foreach($assignToIdDetailDocument as $assignToIdDetailDocumentK=>$assignToIdDetailDocumentV)
            {
                $assginedDocumentUserData[$assignToIdDetailDocumentV->user_id] = $assignToIdDetailDocumentV->first_name.' '.$assignToIdDetailDocumentV->last_name;
            }
            
            $parentAssignedThreadCommentDataDocument=array();//
            foreach($documentComment as $documentCommentKey=>$documentCommentValue)
            {
                foreach($userDataDocument as $userDataDocumentK=>$userDataDocumentV)
                {
                    if($documentCommentValue->created_by==$userDataDocumentK)
                    {   
                        foreach($documentAssignToId as $documentAssignToIdK=>$documentAssignToIdV)
                        {   
                            if($documentCommentValue->thread_id==$documentAssignToIdK)
                            {
                                foreach($assginedDocumentUserData as $assginedDocumentUserDataK=>$assginedDocumentUserDataV)
                                {
                                    if($documentAssignToIdV==$assginedDocumentUserDataK)
                                    {
                                        foreach($documentCommentMadeFor as $documentCommentMadeForDataK=>$documentCommentMadeForDataV)
                                        {
                                            if($documentCommentValue->thread_id==$documentCommentMadeForDataV->thread_id)
                                            {
                                                $parentAssignedThreadCommentDataDocument[]=[
                                                                            "thread_comment_id"=>$documentCommentValue->thread_comment_id,
                                                                            "thread_source_id"=>$documentCommentValue->thread_source_id,
                                                                            "thread_id"=>$documentCommentValue->thread_id,
                                                                            "comment"=>$documentCommentValue->comment,
                                                                            "assign_to_id"=>$assginedDocumentUserDataK,
                                                                            "assigned_to"=>$assginedDocumentUserDataV,
                                                                            "created_by"=>$documentCommentValue->created_by,
                                                                            "created_by_name"=>$userDataDocumentV,
                                                                            "updated_at"=>$documentCommentMadeForDataV->updated_at,
                                                                            "thread_source_type"=>$documentCommentMadeForDataV->thread_source_type,
                                                                            "status"=>$documentCommentMadeForDataV->status,
                                                                            "created_at"=>$documentCommentMadeForDataV->created_at,
                                                                            "comments"=>[]
                                                                            ];
                                            }
                                        }                                    
                                        
                                    }
                                }
                                    
                            }
                            
                        }   
                                        
                    }
                }
            }

            // unassinged comment start
            
            $getUniqueThreadIdForUnassignedDocumentComment=array_unique($getThreadIdForUnassignedDocumentComment);
            $getThreadDocumentDetails = DB::table('threads')
                    ->whereIn('thread_id', $getUniqueThreadIdForUnassignedDocumentComment)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

            $parentUnassignedThreadCommentDataDocument=array();
            foreach($documentComment as $documentCommentDataKey=>$documentCommentDataValue)
            {
                foreach($userDataDocument as $userDataDocumentDataK=>$userDataDocumentDataV)
                {
                    if($documentCommentDataValue->created_by==$userDataDocumentDataK)
                    {   
                        foreach($getThreadDocumentDetails as $getThreadDocumentDetailsK=>$getThreadDocumentDetailsV)
                        {   
                            if($documentCommentDataValue->thread_id==$getThreadDocumentDetailsV->thread_id && $getThreadDocumentDetailsV->assign_to=="")
                            {
                                foreach($documentCommentMadeFor as $documentCommentMadeForInfoK=>$documentCommentMadeForInfoV)
                                {
                                    if($documentCommentDataValue->thread_id==$documentCommentMadeForInfoV->thread_id)
                                    {
                                        $parentUnassignedThreadCommentDataDocument[]=[
                                                                    "thread_comment_id"=>$documentCommentDataValue->thread_comment_id,
                                                                    "thread_source_id"=>$documentCommentDataValue->thread_source_id,
                                                                    "thread_id"=>$documentCommentDataValue->thread_id,
                                                                    "comment"=>$documentCommentDataValue->comment,
                                                                    "assign_to_id"=>$getThreadDocumentDetailsV->assign_to,
                                                                    "assigned_to"=>$getThreadDocumentDetailsV->assign_to,
                                                                    "created_by"=>$documentCommentDataValue->created_by,
                                                                    "created_by_name"=>$userDataDocumentDataV,
                                                                    "updated_at"=>$documentCommentMadeForInfoV->updated_at,
                                                                    "thread_source_type"=>$documentCommentMadeForInfoV->thread_source_type,
                                                                    "status"=>$documentCommentMadeForInfoV->status,
                                                                    "created_at"=>$documentCommentMadeForInfoV->created_at,
                                                                    "comments"=>[]
                                                                    ];
                                    }                                
                                }        
                            }
                            
                        }   
                                        
                    }
                }
            }
            
            // unassinged comment end

            // merging parent assigned and unassigned comment start
            $parentThreadCommentDataDocument=array();
            $parentThreadCommentDataDocument=array_merge($parentAssignedThreadCommentDataDocument,$parentUnassignedThreadCommentDataDocument);
            // merging parent assigned and unassigned comment end

            // calling sorting function starts
            usort($parentThreadCommentDataDocument, function($c, $d) {
                return $c['updated_at'] < $d['updated_at'];
            });
        // calling sorting function ends


            $childCommentDataDocument = DB::table('thread_comments')
                    ->whereIn('parent_id', $threadDocumentCommentId)
                    ->where('is_deleted','0')
                    ->orderBy('updated_at','DESC')
                    ->get()
                    ->toArray();

        
            $getUserIdForChildDocument = array();
            foreach($childCommentDataDocument as $childCommentDataDocumentK=>$childCommentDataDocumentV)
            {
                $getUserIdForChildDocument[$childCommentDataDocumentV->thread_comment_id]=$childCommentDataDocumentV->created_by;
            }
            $userIdForChildDocument=array_unique($getUserIdForChildDocument);

            $userDetailForChildDocument = DB::table('users')
                    ->whereIn('user_id', $userIdForChildDocument)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
            $userDataForChildDocument = array();
            foreach($userDetailForChildDocument as $userDetailForChildDocumentK=>$userDetailForChildDocumentV)
            {
                $userDataForChildDocument[$userDetailForChildDocumentV->user_id] = $userDetailForChildDocumentV->first_name.' '.$userDetailForChildDocumentV->last_name;
            }

            $childCommentDataStoredDocument=array();
            foreach($childCommentDataDocument as $childCommentDataDocumentK=>$childCommentDataDocumentV)
            {   
                foreach($userDataForChildDocument as $userDataForChildDocumentK=>$userDataForChildDocumentV)
                {
                    if($childCommentDataDocumentV->created_by==$userDataForChildDocumentK)
                    {
                        $childCommentDataStoredDocument[] =[
                                                        "thread_comment_id"=>$childCommentDataDocumentV->thread_comment_id,
                                                        "thread_source_id"=>$childCommentDataDocumentV->thread_source_id,
                                                        "thread_id"=>$childCommentDataDocumentV->thread_id,
                                                        "parent_id"=>$childCommentDataDocumentV->parent_id,
                                                        "comment"=>$childCommentDataDocumentV->comment,
                                                        "created_by"=>$childCommentDataDocumentV->created_by,
                                                        "created_by_name"=>$userDataForChildDocumentV,
                                                        "updated_at"=>$childCommentDataDocumentV->updated_at
                                                    ]; 
                    }
                }
            }
            
            foreach($parentThreadCommentDataDocument as $parentThreadCommentDataDocumentK=>$parentThreadCommentDataDocumentV)
            {
                $incrm = 0;
                $childCommentArrDocument = array();
                foreach($childCommentDataStoredDocument as $childCommentDataStoredDocumentK=>$childCommentDataStoredDocumentV)
                    {   
                        if($parentThreadCommentDataDocumentV["thread_id"]==$childCommentDataStoredDocumentV["thread_id"])
                        {
                            $childCommentArrDocument[] = $childCommentDataStoredDocumentV;
                            $parentThreadCommentDataDocumentV["comments"] = $childCommentArrDocument;
                        }
                        $incrm = $incrm + 1;
                    }
                    unset($childCommentArrDocument);
                    $parentThreadCommentDataDocument[$parentThreadCommentDataDocumentK] = $parentThreadCommentDataDocumentV;
            }
            
            $getDocumentDataDetail = DB::table('documents')
                    ->where('document_id', $documentId)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();

            
            $displayCommentListDocument=array();
            $commentDisplayListDocument=array();
        
            foreach($getDocumentDataDetail as $getDocumentDataDetailK=>$getDocumentDataDetailV)
            {   
                $g=0;
                $p=0;
                foreach($parentThreadCommentDataDocument as $parentThreadCommentDataDocumentKey=>$parentThreadCommentDataDocumentValue)
                {
                    if($getDocumentDataDetailV->document_id==$parentThreadCommentDataDocumentValue["thread_source_id"])
                    {   
                        $commentDisplayListDocument[$getDocumentDataDetailV->document_id][]=$parentThreadCommentDataDocumentValue;
                    }
                    $g=$g+1;
                    $p=$p+1;
                    
                }

                $dataDocument=array();
                $dataDocument['node_id']=$getDocumentDataDetailV->document_id;
                $dataDocument['node_name']=$getDocumentDataDetailV->title;
                if(isset($commentDisplayListDocument[$getDocumentDataDetailV->document_id]))
                {
                    $dataDocument['node_updated_at'] = $commentDisplayListDocument[$getDocumentDataDetailV->document_id][0]['updated_at'];
                }
                else
                {
                    $dataDocument['node_updated_at'] = "";
                }
                $dataDocument['threads']= isset($commentDisplayListDocument[$getDocumentDataDetailV->document_id])?$commentDisplayListDocument[$getDocumentDataDetailV->document_id]:array();
                $displayCommentListDocument[]=$dataDocument;

                unset($commentDisplayListDocument);
                
            }

            // Taxonomy : Document Level End

            $taxonomyCommentList=array_merge($displayCommentList,$displayCommentListDocument);
            return $taxonomyCommentList;
        

    }


}