<?php

namespace App\Data\Repositories;

use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;
use Illuminate\Support\Facades\DB;

use App\Data\Models\NodeTemplate;

class NodeTemplateEloquentRepository extends EloquentRepository implements NodeTemplateRepositoryInterface
{

    public function getModel()
    {
        return NodeTemplate::class;
    }

    public function associateNodeTypeWithTemplate(array $attributes) {
        $tableName = "node_template_node_types";
        $queryToExecute = DB::table($tableName)->insert($attributes);
        return $queryToExecute;
    }

    public function getNodeTemplateList(Request $request)
    {
        $input = $request->all();
        $allowedOrderByFields = ['name', 'created_at', 'updated_at'];
        $allowedOrderByMethods = ['asc', 'desc'];

        $limit = 100;
        if ($request->has('limit') && is_int($request->limit)) {
            $limit = $request->limit;
        }

        $offset = 0;
        if ($request->has('offset') && is_int($request->offset)) {
            $offset = $request->offset;
        }

       // return $this->model->take($limit)->skip($offset)
       return $this->model
        ->when($request->has('sort'), function($query) use ($request, $allowedOrderByFields, $allowedOrderByMethods) {
            list($orderBy, $sorting) = explode(':', $request->sort);
            if (in_array($orderBy, $allowedOrderByFields) && in_array($sorting, $allowedOrderByMethods)) {
                $query->orderBy($orderBy, $sorting);
            }            
        })
        ->where(array('is_active'   =>   1, 'is_deleted'    =>  0))
        ->where('organization_id', $input['auth_user']['organization_id'])
        ->when($request->has('search'), function($query) use ($request) {
            $query->where('name', 'like', "%{$request->search}%");
        })
        ->select('node_template_id', 'name')
        ->get();
    }

    public function findUsageCount(string $nodeTemplateIdentifier) {
        $tableName = "documents";
        $queryToExecute = DB::table($tableName)->select('document_id')->where('node_template_id', $nodeTemplateIdentifier);
        return $queryToExecute->count();
    }

}