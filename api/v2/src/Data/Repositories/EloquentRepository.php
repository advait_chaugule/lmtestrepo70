<?php

namespace App\Data\Repositories;

use App\Data\Repositories\Contracts\RepositoryInterface;

abstract class EloquentRepository implements RepositoryInterface
{

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * Get model class name from the respective repository
     * @return string
     */
    abstract public function getModel();

    /**
     * Create and set the model object
     */
    public function setModel()
    {
        $this->model = app()->make( $this->getModel() );
    }

    /**
     * Returns the first record in the database.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function first()
    {
        return $this->model->first();
    }

    /**
     * Returns all the records.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Returns the count of all the records.
     *
     * @return int
     */
    public function count()
    {
        return $this->model->count();
    }

    /**
     * Returns a range of records bounded by pagination parameters.
     *
     * @param int limit
     * @param int $offset
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function page(int $limit = 10, int $offset = 0, array $relations = [], string $orderBy = 'updated_at', string $sorting = 'desc')
    {
        return $this->model->with($relations)->take($limit)->skip($offset)->orderBy($orderBy, $sorting)->get();
    }

    /**
     * Find a record by its identifier.
     *
     * @param string $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(string $id)
    {
        return $this->model->find($id);
    }

    /**
     * Find a record by an attribute.
     * Fails if no model is found.
     *
     * @param string $attribute
     * @param string $value
     * @param array  $relations
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findBy(string $attribute, string $value, $relations = null)
    {
        $query = $this->model->where($attribute, $value);

        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        return $query->first();
    }

    /**
     * Find all records by an associative array of attributes.
     * Two operators values are handled: AND | OR.
     *
     * @param array  $attributes
     * @param string $operator
     * @param array  $relations
     *
     * @return \Illuminate\Support\Collection
     */
    public function findByAttributes(
        array $attributes,

        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],

        string $operator = 'AND', 
        $relations = null
    )
    {
        // In the following it doesn't matter wivh element to start with, in all cases all attributes will be appended to the
        // builder.

        // Get the last value of the associative array
        $lastValue = end($attributes);

        // Get the last key of the associative array
        $lastKey = key($attributes);

        // Builder
        $query = $this->model->where($lastKey, $lastValue);

        // Pop the last key value pair of the associative array now that it has been added to Builder already
        array_pop($attributes);

        $method = 'where';

        if (strtoupper($operator) === 'OR') {
            $method = 'orWhere';
        }

        foreach ($attributes as $key => $value) {
            $query->$method($key, $value);
        }

        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }
        
        return $query->get();
    }

    

    /**
     * Fills out an instance of the model
     * with $attributes.
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function fill(array $attributes)
    {
        return $this->model->fill($attributes);
    }

    /**
     * Fills out an instance of the model
     * and saves it, pretty much like mass assignment.
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function fillAndSave(array $attributes)
    {   
        $this->model->fill($attributes);
        $this->model->save();

        return $this->model;
    }

    /**
     * Update a single record
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function edit($id, array $attributes)
    {
        $record = $this->find($id);
        
        if(!empty($record)) {
            $record->update($attributes);
            return $record;
        }
        return false;
    }

    /** Update a single record by specified column(s) value(s)
     *  @param array $columnValueFilterPairs
     * @param array $attributes
     * @return bool|mixed
     */
    public function editWithCustomizedFields(array $columnValueFilterPairs, array $attributes)
    {   
        $record = $this->model->where($columnValueFilterPairs);
        $record->update($attributes);
        return $record->first();
    }

    /**
     * Undocumented function
     * Method to update multiple records based on id
     * @param array $arrayOfIds [1,2,3]
     * @param array $attributeValuePairs example = ["field" => value]
     * @return void
     */
    public function updateMultipleByIds(string $arrayOfIds, array $attributeValuePairs){
        return $this->model->where('id', $arrayOfIds)->update($attributeValuePairs);
    }

    /**
     * Remove a selected record.
     *
     * @param string $key
     *
     * @return bool
     */
    public function remove(string $key)
    {
        return $this->model->where($this->model->getKeyName(), $key)->delete();
    }

    /**
     * Remove a selected record.
     *
     * @param array $columnValueFilterPairs
     *
     * @return bool
     */
    public function delete(array $columnValueFilterPairs)
    {
        return $this->model->where($columnValueFilterPairs)->delete();
    }
    
    /**
     * Implement a convenience call to findBy
     * which allows finding by an attribute name
     * as follows: findByName or findByAlias.
     *
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call(string $method, array $arguments)
    {
        /*
         * findBy convenience calling to be available
         * through findByName and findByTitle etc.
         */

        if (preg_match('/^findBy/', $method)) {
            $attribute = strtolower(substr($method, 6));
            array_unshift($arguments, $attribute);

            return call_user_func_array(array($this, 'findBy'), $arguments);
        }
    }

    /**
     * this method creates multiple records at once
     * @param string $attributes - [[key1=>value1],[key2=>value2]]
     * @return void
     */
    public function saveMultiple(array $attributes){
        $this->model->insert($attributes);
    }

    /**
     * This method will create and return eloquent model
     * This was implemented to prevent fillandsave method creating unwanted results during consecutive calls inside a transaction block
     */
    public function saveData(array $attributes) {
        return $this->model->create($attributes);
    }

    /**
     * Method to return multiple records using in query
     * attributeIn -> in query field name
     * containedInValues -> array of values for in query
     * whereClauseAttributes -> array of key=>value pairs for extra conditions
     * operator -> AND or OR operation between whereClauseAttributes
     * relations -> for eagerloading related models
     */
    public function findByAttributeContainedIn(
        string $attributeIn, 
        array $containedInValues,
        array $whereClauseAttributes,
        string $operator = 'AND', 
        $relations = null
    ) {
        $containedInValuesArry = array_chunk($containedInValues,1000);
        $method = 'where';
        if (strtoupper($operator) === 'OR') {
            $method = 'orWhere';
        }
        $result = collect();
        foreach($containedInValuesArry as $containedInValuesArryK => $containedInValues)
        {

            $query = $this->model->whereIn($attributeIn, $containedInValues);

            foreach ($whereClauseAttributes as $key => $value) {
                $query->$method($key, $value);
            }

            if ($relations && is_array($relations)) {
                foreach ($relations as $relation) {
                    $query->with($relation);
                }
            }
                            
            $result = $result->merge($query->get());
        }
        return $result;
    }

    /**
     * Clone of findByAttributes with features like return single or multiple records, query for selected fields only
     * with pagination and sorting
     */
    public function findByAttributesWithSpecifiedFields(
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs,
        $relations = null,
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = true,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND',
        $additionalMethod = null,
        $additionalKey = null,
        $additionalValues = null
    ){

        // prepare the query initially
        $query = $this->model->select($fieldsToReturn);

        // Get the last value of the associative array
        $lastValue = end($keyValuePairs);

        // Get the last key of the associative array
        $lastKey = key($keyValuePairs);

        // update the query with a where clause initially
        $query = $query->where($lastKey, $lastValue);

        // Pop the last key value pair of the associative array now that it has been added to Builder already
        array_pop($keyValuePairs);

        $method = 'where';

        if (strtoupper($operator) === 'OR') {
            $method = 'orWhere';
        }

        foreach ($keyValuePairs as $key => $value) {
            $query->$method($key, $value);
        }

        if($additionalMethod !== null)
        {
            $query->$additionalMethod($additionalKey, $additionalValues);
        }
        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }
        
        return $returnCollection ? $query->get() : $query->get()->first();

    }

     /**
     * Clone of findByAttributeContainedIn with features like return single or multiple records, query for selected fields only
     * with pagination and sorting
     */
    public function findByAttributesWithSpecifiedFieldsWithInQuery(
        string $attributeIn, 
        array $containedInValues,
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs = [],
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND', 
        $relations = null
    ){

        // prepare the query initially
        $query = $this->model->select($fieldsToReturn)->whereIn($attributeIn, $containedInValues);

        $method = 'where';

        if (strtoupper($operator) === 'OR') {
            $method = 'orWhere';
        }

        foreach ($keyValuePairs as $key => $value) {
            $query->$method($key, $value);
        }

        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }

        return $returnCollection ? $query->get() : $query->get()->first();

    }

     /**
     * Clone of findByAttributeContainedIn with features like return single or multiple records, query for selected fields only
     * with pagination and sorting
     */
    public function findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
        string $attributeIn, 
        array $containedInValues,
        bool $returnCollection      =   true,
        array $fieldsToReturn       =   ['*'],
        array $keyValuePairs        =   [],
        array $notInKeyValuePairs   =   [],
        bool $returnPaginatedData   =   false,
        array $paginationFilters    =   ['limit' => 10, 'offset' => 0],
        bool $returnSortedData      =   false,
        array $sortfilters          =   ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator            =   'AND', 
        $relations                  =   null
    ){

        // prepare the query initially
        $query = $this->model->select($fieldsToReturn)->whereIn($attributeIn, $containedInValues);

        $method = 'where';

        if (strtoupper($operator) === 'OR') {
            $method = 'orWhere';
        }

        foreach ($keyValuePairs as $key => $value) {
            $query->$method($key, $value);
        }

        if( count($notInKeyValuePairs) > 0) {
            foreach ($notInKeyValuePairs as $key => $value) {
                $query->$method($key, '!=',  $value );
            }
        }

        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }

        return $returnCollection ? $query->get() : $query->get()->first();

    }

    /**
     * Method to truncate (empty table)
     */
    public function deleteAll() {
        $this->model->truncate();
    }

    /**
     * This method will create a model instance
     * ## Mainly created to solve issues with fill method inside a loop or transaction block
     * @param array $attributes
     * @return void
     */
    public function createModelInstanceInMemory(array $attributes) {
        return $this->model->newInstance($attributes);
    }

    /**
     * Method to find count with whereIn clause
     */
    public function whereCount($fieldsToReturn,$attributeIn, $containedInValues) {
        
        $query = $this->model->whereIn($attributeIn, $containedInValues);            
        
        return $query->count($fieldsToReturn);
    }

    public function findByAttributesWithSpecifiedFieldsUpdated(
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs,
        $relations = null,
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = true,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND',
        $additionalMethod = null,
        $additionalKey = null,
        $additionalValues = null
    ){
        // prepare the query initially
        $query = $this->model->select($fieldsToReturn);

        $method = 'where';

        if (strtoupper($operator) === 'OR') {
            $method = 'orWhere';
        }

        foreach ($keyValuePairs as $key => $value) {
            $query->$method($key, $value);
        }

        if($additionalMethod !== null)
        {
            $query->$additionalMethod($additionalKey, $additionalValues);
        }
        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }
        return $returnCollection ? $query->get() : $query->get()->first();

    }

    
}