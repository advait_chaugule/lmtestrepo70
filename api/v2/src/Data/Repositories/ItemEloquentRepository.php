<?php

namespace App\Data\Repositories;

use App\Data\Models\Organization;
use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

use App\Data\Models\Item;
use Illuminate\Support\Facades\DB;
use App\Data\Models\ItemMetadata;
use App\Data\Models\ItemAssociation;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;

class ItemEloquentRepository extends EloquentRepository implements ItemRepositoryInterface
{

    public function getModel()
    {
        return Item::class;
    }
    
    
    /**
     * This method returns all the associated models of an item
     * Also the associated models will also contains other models associated with it
     */
    public function getItemDetailsWithProjectAndCaseAssociation(string $itemId){
        $fetchAssociatedModels = [
            "license", 
            "concept", 
            "language", 
            "project",
            "customMetadata", 
            "itemType",
            "nodeType",
            "nodeType.metadata",
            "itemAssociations",
            "itemAssociations.document:document_id,source_document_id,title,node_type_id",
            "itemAssociations.originNode:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id",
            "itemAssociations.destinationNode:item_id,source_item_id,human_coding_scheme,full_statement,document_id,parent_id,node_type_id",
            "itemAssociations.destinationDocumentNode:document_id,source_document_id,title,node_type_id",
            
        ];
        return $this->findBy("item_id", $itemId, $fetchAssociatedModels);
    }

    /**
     * 
     * @param type $itemId
     * @return type array of all item
     */
    public function getItemTree($itemId) {
        $cfTree = [];
        //get parent node
        $query = $this->model->where('is_deleted', 0);
        $query->where('item_id',$itemId);
     
        $parent = $query->get()->first();

        if(!empty($parent->item_id)){
            //get child iteratively
            $children = $this->getCFChildItems($parent->item_id);
            $parent['children'] = $children;
            // prepare ui friendly the tree data
            $treeData = [ "children" => [$parent] ];
        }
        else{
            $treeData = [];
        }   
        return $treeData;               
    }

    /**
     * 
     * @param type $parentId
     * @return type array of all item
     */
    public function getItemTreeofDocument($parentId) {
        $treeData = $this->getCFChildItems($parentId);
        return $treeData;              
    }

    /**
     * @param $item_id
     * @param array $metadata_id
     * @return mixed
     */
    public function updateIsDelete($item_id,array $metadata_id)
    {
        $query = DB::table("item_metadata")->where('item_id',$item_id)
            ->whereIn('metadata_id',$metadata_id)
            ->update(['is_deleted'=>0]);
        return $query;
    }
    
    /**
     * 
     * @param type $parentId
     * @return array of all child elements
     */
    protected function getCFChildItems($parentId){
        $query = $this->model->where('is_deleted', 0);
        $query->where('parent_id',$parentId);
        $items = $query->get();

        $child_arr = [];
        foreach($items as $item){
            $item['children']=[];
            $child = $this->getCFChildItems($item->item_id);
            
            if(count($child)){
                $item['children']=$child;
            }
            // set title to humancodingscheme for cfitem
            // if not set then fallback to list_enumeration
            $item->title = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : $item->list_enumeration;
            //pushing child item
            array_push($child_arr, $item);
        }        

        return $child_arr;
               
    }

    public function getProjectID($documentId) {
        $query = DB::table('projects')
        ->select('project_id')
        ->where('document_id', $documentId)
        ->get();

        return $query;
    }

    public function addProjectItem($itemId, $projectID){
        $query = DB::table('project_items')->insert(
            ['project_id' => $projectID, 'item_id' => $itemId, 'created_at'=> date('Y-m-d H:i:s'), 'updated_at'=> date('Y-m-d H:i:s') ]
        );
    }

    public function findItemId($identifier,$organizationId){
        $query =  $this->model->select('item_id');
        $query->where('organization_id',$organizationId);
        $query->where('source_item_id','LIKE', "%". $identifier . '%' );
        $item = $query->get()->toArray();    
        return $item;
    }

    public function updateItemParentId($identifier, $parentId){
        DB::table('items')->where('source_item_id',$identifier)
        ->update(['parent_id' => $parentId]);
    }

    public function updateSourceId($userOrganizationId, $documentId){
        DB::table('items')->where('organization_id',$userOrganizationId)
        ->where('document_id',$documentId)
        ->where('source_item_id','LIKE', "%||%" )
        ->update(['source_item_id' => 'item_id']);
    }

    public function addOrUpdateCustomMetadataValue(string $itemIdentifier, string $metadataIdentifier, string $value, string $value_html, string $isAdditioanl, string $action) {
        if($action === 'insert') {
            $count = ItemMetadata::where(['item_id'=>$itemIdentifier,'metadata_id'=>$metadataIdentifier,'is_deleted'=>'0'])->count();
            if($count==0){
                return DB::table('item_metadata')->insert(['item_id' => $itemIdentifier, 'metadata_id' => $metadataIdentifier, 'metadata_value' =>  $value ,'metadata_value_html' =>  $value_html ,'is_additional'=>$isAdditioanl]);
            }
        }
        else {
            $count = ItemMetadata::where(['item_id'=>$itemIdentifier,'metadata_id'=>$metadataIdentifier,'is_deleted'=>'0'])->count();
            if($count==1){
                return DB::table('item_metadata')->where(['metadata_id' => $metadataIdentifier, 'item_id'   =>  $itemIdentifier])->update(['metadata_value' => $value,'metadata_value_html' =>  $value_html,'is_deleted' => "0", 'is_additional'=>$isAdditioanl]);
            }
        }
    }

    public function countMetadataValueUsage(string $metadataIdentifier, string $fieldPossibleValue) {
        $metadataValueUsage =   DB::table('item_metadata')->where(['metadata_id' => $metadataIdentifier, 'metadata_value' => $fieldPossibleValue])->get();

        return $metadataValueUsage->count();
    }

    public function updateProjectAndDocument($item_id){
        $getProjectItemDetails = DB::table('project_items')
                            ->select('project_id')
                            ->where('item_id',$item_id)
                            ->get();
        $projectItemDetails = json_decode($getProjectItemDetails, true);
        if(!empty($projectItemDetails[0]['project_id'])){
            $updateProjects     = DB::table('projects')
                                ->where('project_id', $projectItemDetails[0]['project_id'])
                                ->update(['updated_at' => date('Y-m-d H:i:s')]);
            $getDocumentId      = DB::table('projects')
                                ->select('document_id')
                                ->where('project_id',$projectItemDetails[0]['project_id'])
                                ->get();
            $documentId = json_decode($getDocumentId, true);                     
        }
        else
        {

            $getDocumentId = DB::table('items')
            ->select('document_id')
            ->where('item_id',$item_id)
            ->get();
            $documentId = json_decode($getDocumentId, true);

        }
                      
        if(!empty($documentId[0]['document_id'])){
            $updateProjects     = DB::table('documents')
                                ->where('document_id', $documentId[0]['document_id'])
                                ->update(['updated_at' => date('Y-m-d H:i:s')]);

            // raise delta document update to cloud search each time document date is updated
            $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
            $eventType = $sqsUploadEventConfigurations["update_document"];
            $eventData = [
                "type_id" => $documentId[0]['document_id'],
                "event_type" => $eventType
            ];
            event(new UploadSearchDataToSqsEvent($eventData));

        }                    
        return true;                    

    }

    /**
     * @param $parentId
     * @param $return as reference
     * @purpose : This recursive function is used to provide all child elements
     */
    public function getAllChildItems($parentId, & $return ){
        $query = $this->model->where('is_deleted', 0);
        $query->where('parent_id',$parentId);
        $items = $query->get();
        foreach($items as $item){
            $return[]=$item->item_id;
            $this->getAllChildItems($item->item_id,$return);
        }
    }

    public function getItemDetails(array $itemsMappedWithProject)
    {
        $count=count($itemsMappedWithProject);
        for($i=0;$i<=$count-1;$i++){
            $itemsMapped[]=$itemsMappedWithProject[$i]->item_id;
        }
        $itemsDetails = DB::table('items')
                    ->whereIn('item_id', $itemsMapped)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        return $itemsDetails;      
    }

    public function getItemsMappedWithDocument($documentIdentifier)
    {
        $itemsDetails = DB::table('items')
                    ->where('document_id', $documentIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        return $itemsDetails;
    }

    public function getItemsMappedWithProject($projectIdentifier)
    {
        $getItemsMappedWithProjectResult = DB::table('project_items')
                    ->where('project_id', $projectIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $getItemsMappedWithProject=array();            
        $inc=0;
        foreach($getItemsMappedWithProjectResult as $getItemsMappedWithProjectResultK=>$getItemsMappedWithProjectResultV)
        {
            $getItemsMappedWithProject[$inc]['item_doc_id']=$getItemsMappedWithProjectResultV->item_id;
            $getItemsMappedWithProject[$inc]['is_doc']='0';
            $inc=$inc+1;
        }
        return $getItemsMappedWithProject;
        
    }

    public function getDocMappedWithProject($projectIdentifier)
    {
        $getDocMappedWithProjectResult = DB::table('documents')
                    ->where('project_id', $projectIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $getDocMappedWithProject=array();            
        $incr=0;
        foreach($getDocMappedWithProjectResult as $getDocMappedWithProjectResultK=>$getDocMappedWithProjectResultV)
        {
            $getDocMappedWithProject[$incr]['item_doc_id']=$getDocMappedWithProjectResultV->document_id;
            $getDocMappedWithProject[$incr]['is_doc']='1';
            $incr=$incr+1;
        }
        return $getDocMappedWithProject;
    }

    public function getItemsAndDocMappedWithProjectDetails($projectIdentifier)
    {   
        
        $getItemsMappedWithProject=$this->getItemsMappedWithProject($projectIdentifier);
        $getDocMappedWithProject=$this->getDocMappedWithProject($projectIdentifier);
        $getItemsAndDocMappedWithProject=array_merge($getItemsMappedWithProject,$getDocMappedWithProject);
        return $getItemsAndDocMappedWithProject;
    }

    public function getItemsMappedWithTaxonomy($documentIdentifier)
    {
        $getItemsMappedWithTaxonomyResult = DB::table('items')
                    ->where('document_id', $documentIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $getItemsMappedWithTaxonomy=array();            
        $inc=0;
        foreach($getItemsMappedWithTaxonomyResult as $getItemsMappedWithTaxonomyResultK=>$getItemsMappedWithTaxonomyResultV)
        {
            $getItemsMappedWithTaxonomy[$inc]['item_doc_id']=$getItemsMappedWithTaxonomyResultV->item_id;
            $getItemsMappedWithTaxonomy[$inc]['is_doc']='0';
            $inc=$inc+1;
        }
        return $getItemsMappedWithTaxonomy;

    }

    public function getDocMappedWithTaxonomy($documentIdentifier)
    {
        $getDocMappedWithTaxonomyResult = DB::table('documents')
                    ->where('document_id', $documentIdentifier)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
        
        $getDocMappedWithTaxonomy=array();            
        $incr=0;
        foreach($getDocMappedWithTaxonomyResult as $getDocMappedWithTaxonomyResultK=>$getDocMappedWithTaxonomyResultV)
        {
            $getDocMappedWithTaxonomy[$incr]['item_doc_id']=$getDocMappedWithTaxonomyResultV->document_id;
            $getDocMappedWithTaxonomy[$incr]['is_doc']='1';
            $incr=$incr+1;
        }
        return $getDocMappedWithTaxonomy;

    }

    public function getItemsAndDocMappedWithTaxonomyDetails($documentIdentifier)
    {   
        
        $getItemsMappedWithTaxonomy=$this->getItemsMappedWithTaxonomy($documentIdentifier);
        $getDocMappedWithTaxonomy=$this->getDocMappedWithTaxonomy($documentIdentifier);
        $getItemsAndDocMappedWithTaxonomy=array_merge($getItemsMappedWithTaxonomy,$getDocMappedWithTaxonomy);
        return $getItemsAndDocMappedWithTaxonomy;
    }

    /**
     * Clone of findByAttributeContainedIn with features like return single or multiple records, query for selected fields only
     * with pagination and sorting
     */
    public function findByCustomMetadataWithItemIdInQuery(
        string $attributeIn, 
        array $containedInValues,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs = []
    ){

        // prepare the query initially
        $query = DB::table('item_metadata')->select($fieldsToReturn)->whereIn($attributeIn, $containedInValues);

        foreach ($keyValuePairs as $key => $value) {
            $query->where($key, $value);
        }

        return $query->get();

    }

    public function updateDocument($documentId){

        if(!empty($documentId)){
            $updateProjects     = DB::table('documents')
                                ->where('document_id', $documentId)
                                ->update(['updated_at' => date('Y-m-d H:i:s')]);

            // raise delta document update to cloud search each time document date is updated
            $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
            $eventType = $sqsUploadEventConfigurations["update_document"];
            $eventData = [
                "type_id" => $documentId,
                "event_type" => $eventType
            ];
            event(new UploadSearchDataToSqsEvent($eventData));

        }                    
        return true;                

    }

    /**
     * @param $orgCode
     * @return mixed
     * @Funcrion getOrganizationDetails
     * @Purpose Based on org code get org id for tenant specific
     */
    public function getOrganizationDetails($orgCode)
    {
        $orgId = Organization:: select('organization_id')
            ->where('org_code',$orgCode)
            ->first();
        if($orgId) {
            $orgId = $orgId->toArray();
        }
        return $orgId ;
    }

    public function getHierarchyAllChildItems($document_id,$parentId, & $return ){
        $query = ItemAssociation:: select('*')->where('association_type', 1)->where('is_deleted', 0);
        $query->where('destination_node_id',$parentId);
        $items = $query->get();
        foreach($items as $item){
            $return[]=[
                'item_id'   => $item->origin_node_id,
                'parent_id' => $item->destination_node_id,
                'sequence_number' => $item->sequence_number
            ];
            $this->getHierarchyAllChildItems($document_id,$item->origin_node_id,$return);
        }
    }

    public function getAllChildItemsV2($parentId, & $return ){
        $items = DB::select( DB::raw("SELECT origin_node_id FROM acmt_item_associations WHERE is_deleted = 0 AND association_type = 1 AND destination_node_id = '$parentId'") );
        foreach($items as $item){
            $return[]=$item->origin_node_id;
            $this->getAllChildItemsV2($item->origin_node_id,$return);
        }
    }

    public function getTargetDocumentIdBasedOnSourceItemId($sourceItemId,$organizationId)
    {
        $documentId = DB::table('items')
        ->select('document_id')
        ->where('source_item_id',$sourceItemId)
        ->where('organization_id',$organizationId)
        ->where('is_deleted',0)
        ->get()
        ->toArray();

        return $documentId;
    }

    public function getTargetDocumentIdBasedOnSourceItemIdArr($sourceItemIdArr,$organizationId)
    {
        $sourceItemIdArrChunk = array_chunk($sourceItemIdArr, 1000);
        $documentIdArr = collect();
        foreach($sourceItemIdArrChunk as $chunk) {
            $documentIdArrChunk = DB::table('items')
                                    ->select('document_id', 'source_item_id')
                                    ->whereIn('source_item_id',$chunk)
                                    ->where('organization_id',$organizationId)
                                    ->where('is_deleted',0)
                                    ->get();
            $documentIdArr = $documentIdArr->merge($documentIdArrChunk);
        }

        return $documentIdArr;
    }

}