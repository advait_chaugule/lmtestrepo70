<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\ConceptRepositoryInterface;

use App\Data\Models\Concept;

class ConceptEloquentRepository extends EloquentRepository implements ConceptRepositoryInterface
{
    public function getModel()
    {
        return Concept::class;
    }
}