<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;
use App\Data\Repositories\Contracts\JReportRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Data\Models\User;

class JReportEloquentRepository extends EloquentRepository implements JReportRepositoryInterface {

    public function getModel() {
        return User::class;
    }   
}
