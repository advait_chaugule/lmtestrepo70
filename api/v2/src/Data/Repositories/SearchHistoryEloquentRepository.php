<?php
namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;

use App\Data\Repositories\Contracts\SearchHistoryEloquentRepositoryInterface;

use App\Data\Models\SearchHistory;

class SearchHistoryEloquentRepository extends EloquentRepository implements SearchHistoryEloquentRepositoryInterface
{
    public function getModel()
    {
        return SearchHistory::class;
    }
}