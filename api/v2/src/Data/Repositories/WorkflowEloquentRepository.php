<?php

namespace App\Data\Repositories;

use App\Data\Repositories\EloquentRepository;
use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Data\Models\Workflow;

class WorkflowEloquentRepository extends EloquentRepository implements WorkflowRepositoryInterface {

    public function getModel() {
        return Workflow::class;
    }

    public function getWorkflowList($organizationId) {
        $query = $this->model->where('is_deleted', 0);
        $query->where('organization_id', $organizationId);
        // $query->withCount([
        //     'project' => function($query) {
        //         $query->selectRaw('count(\'project_id\') as aggregate');
        //     }
        // ]);
        $query->with([
            'user' => function($query) {
                $query->select("user_id");
                $query->selectRaw("concat_ws(' ', first_name, last_name) as name");
            }
        ]);
        
        $workflows = $query->get();
        return $workflows;
    }
    
    /**
     * List Distinct Roles for all stages of a workflow
     */
    public function fetchRoleList($workflowStageIDArray){
        $tableName = "workflow_stage_role";
        
        $result = DB::table($tableName)
        ->whereIn('workflow_stage_id', $workflowStageIDArray)
        ->groupBy('role_id')
        ->get();

        return $result;
    }

    public function geWorkflowDetails($organizationId, $workflowId) {
       
        $query = $this->model->where('is_deleted', 0);
        $query->where('workflow_id', $workflowId);
        $query->where('organization_id', $organizationId);
        $workflow = $query->get()->toArray();
       
        $data['workflow_id'] = $workflow[0]['workflow_id'];
        $data['name'] = $workflow[0]['name'];
        $data['is_default'] = $workflow[0]['is_default'];
        $sub_query = DB::table('workflow_stage')
        ->where('workflow_stage.workflow_id', '=', $workflowId);
        $sub_query->orderBy('order');
        $workflow_stage = $sub_query->get()->toArray();
        $data['stages'] =$workflow_stage;
        foreach($workflow_stage as $key=>$ws){
            $sub_query_role_id = DB::table('workflow_stage_role')
                ->select('role_id');
            $sub_query_role_id->where('workflow_stage_role.workflow_stage_id', '=', $ws->workflow_stage_id);
            $role_id = $sub_query_role_id->get()->toArray();
            $role = [];
            foreach($role_id as $key=>$rd){
                
                if(count($role_id)>0){
                    $sub_query_role = DB::table('roles')
                    ->select('role_id','name');
                    $sub_query_role->where('roles.role_id', '=', $rd->role_id);
                    $role[$key] = $sub_query_role->get()->first();
                    
                }
                     
            }      
            $ws->roles = $role; 
        }   
        
        return $data;
    }
    
    public function duplicate($workflowId, $organizationId, $input){
        $getStages = DB::table('workflow_stage as ws')
            ->join('stage as s', 's.stage_id', '=', 'ws.stage_id')
            ->select('s.name','ws.workflow_stage_id','ws.stage_id','ws.order','ws.stage_description')
            ->orderBy('s.order')
            ->where('s.is_deleted',0)
            ->where('ws.workflow_id',$workflowId)
            ->get();
            return  $getStages;
    }
}
