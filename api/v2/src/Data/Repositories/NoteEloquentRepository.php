<?php

namespace App\Data\Repositories;
use Illuminate\Http\Request;
use App\Data\Repositories\EloquentRepository;
use App\Data\Repositories\Contracts\NoteRepositoryInterface;

use App\Data\Models\Note;
use DB;

class NoteEloquentRepository extends EloquentRepository implements NoteRepositoryInterface
{
    public function getModel()
    {
        return Note::class;
    }

    public function getNoteIsUsedData(array $noteIdentifier)
    {
        $noteData = DB::table('notes')
            ->select('note_id')
            ->where($noteIdentifier)
            ->get()
            ->toArray();
        //dd($noteData);
    }

    function  getNote($noteIdentifier){

        $noteData = DB::table('notes')
            ->select('title','description')
            ->where('note_id',$noteIdentifier)
            ->get()
            ->toArray();
        return $noteData;
    }

    public function geNoteDetails($organizationId, $noteId) {

        $query = $this->model->where('is_deleted', 0);
        $query->where('note_id', $noteId);
        $query->where('organization_id', $organizationId);
        $note = $query->get()->toArray();

        $data['note_id'] = $note[0]['note_id'];
        $data['title'] = $note[0]['title'];
        $data['is_default'] = $note[0]['is_default'];
        $sub_query = DB::table('notes')
            ->where('note_id', '=', $noteId);
        $sub_query->orderBy('note_order');

        return $data;
    }
}