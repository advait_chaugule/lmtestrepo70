<?php

namespace App\Data\Repositories;

use App\Data\Repositories\Contracts\AuditRepositoryInterface;

abstract class EloquentAuditRepository implements AuditRepositoryInterface
{

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    protected $dbConnectionName;

    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setDBConnectionName();
        $this->setModel();
    }

    /**
     * Get model class name from the respective repository
     * @return string
     */
    abstract public function getModel();

    public function setDBConnectionName() {
        $this->dbConnectionName = "mysql_audit";
    }

    /**
     * Create and set the model object
     */
    public function setModel()
    {
        $this->model = app()->make( $this->getModel() )->setConnection($this->dbConnectionName);
    }

    /**
     * Dekete all records of a table
     */
    public function deleteAllRecords() {
        $this->model->truncate();
    }

    /**
     * This method will create multiple records in one shot
     * $data = [
     *      [record_set1], [record_set2]......., [record_set(n)]
     * ]
     */
    public function saveMultipleRecords(array $data) {
        $this->model->insert($data);
    }

    /**
     * This method will create single record and return eloquent model
     */
    public function saveSingleRecord(array $attributes) {
        return $this->model->create($attributes);
    }

    /**
     * Return single or multiple records with pagination or sorting (both are optional).
     * Can be used to return records with selected fields
     * Where clause can contain multiple conditions with operator options
     * You can also opt for with to fetch related models too. (optional)
     * 
     * @param returnCollection: bool = determine single or multiple records to return
     * @param fieldsToReturn: array = [*] to return all or ["field1", "field2",....... "fieldn"]
     * @param conditionalKeyValuePairs: array = blank array [] or ["field1" => "value1",....."fieldn" => "valuen" ]
     * @param returnPaginatedData: bool = determine if records returned should be paginated
     * @param paginationFilters: array = pagination filters viz. ['limit' => 10, 'offset' => 0]
     * @param returnSortedData: bool = determine records returned should be sorted
     * @param sortfilters: array = sort filters viz. ['orderBy' => 'filed', 'sorting' => 'desc']
     * @param operator: string = operation to be performed on conditionalKeyValuePairs
     * @param relations: array = array of related methods to return
     */
    public function find(
        bool $returnCollection = true,
        array $fieldsToReturn = ['*'],
        array $keyValuePairs = [],
        bool $returnPaginatedData = false,
        array $paginationFilters = ['limit' => 10, 'offset' => 0],
        bool $returnSortedData = false,
        array $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'],
        string $operator = 'AND', 
        $relations = null
    ){
        // prepare the query initially
        $query = $this->model->select($fieldsToReturn);

        if(!empty($keyValuePairs)) {
            // Get the last value of the associative array
            $lastValue = end($keyValuePairs);
            // Get the last key of the associative array
            $lastKey = key($keyValuePairs);
            // update the query with a where clause initially
            $query = $query->where($lastKey, $lastValue);
            // Pop the last key value pair of the associative array now that it has been added to Builder already
            array_pop($keyValuePairs);
            $method = 'where';
            if (strtoupper($operator) === 'OR') {
                $method = 'orWhere';
            }
            foreach ($keyValuePairs as $key => $value) {
                $query->$method($key, $value);
            }
        }

        if ($relations && is_array($relations)) {
            foreach ($relations as $relation) {
                $query->with($relation);
            }
        }

        if($returnPaginatedData===true){
            extract($paginationFilters);
            $query->take($limit)->skip($offset);
        }
        
        if($returnSortedData===true){
            extract($sortfilters);
            $query->orderBy($orderBy, $sorting);
        }

        return $returnCollection ? $query->get() : $query->get()->first();

    }

}