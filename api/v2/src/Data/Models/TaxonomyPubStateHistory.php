<?php
namespace App\Data\Models;
use Lucid\Foundation\Model;

class TaxonomyPubStateHistory extends Model
{
    protected $table = 'taxonomy_pubstate_history';
    public    $timestamps = true;
    protected $fillable = [
                            'document_id',
                            'publish_number',
                            'publish_date',
                            'unpublish_date',
                            'published_by',
                            'unpublished_by',
                            'organization_id',
                            'is_deleted',
                            'created_by',
                            'updated_by',
                            'created_at',
                            'updated_at',
                            'taxonomy_json_file_s3',
                            'custom_json_file_s3',
                            'tag'                                 // tag to store version
                          ];
}