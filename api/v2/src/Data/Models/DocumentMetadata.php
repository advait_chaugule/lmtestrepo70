<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentMetadata extends Model
{
    protected $table = 'document_metadata';
    protected $fillable=[
        'document_id',
        'metadata_id',
        'metadata_value',
        'is_additional',
        'metadata_value_html'
    ];
}
