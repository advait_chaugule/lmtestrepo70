<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAccessRequest extends Model
{
    public $incrementing = false;
    protected $primaryKey = "project_access_request_id";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        'project_access_request_id', 
        'organization_id',
        'project_id',
        'user_id', 
        'workflow_stage_role_ids', 
        'comment',
        'status',
        'created_at',
        'updated_at',
        'status_changed_at',
        'updated_by'
    ];


    public function users() {
        return $this->belongsToMany('App\Data\Models\User', 'project_id', 'user_id');
    }

    public function user() {
        return $this->hasOne('App\Data\Models\User', 'user_id', 'updated_by');
    }

    public function project() {
        return $this->hasOne('App\Data\Models\Project', 'project_id', 'project_id');
    }

    public function organization()
    {
        return $this->belongsTo('App\Data\Models\Organization', 'organization_id', 'organization_id');
    }

    public function requestAccessUser() {
        return $this->hasOne('App\Data\Models\User', 'user_id', 'user_id');
    }  

    public function requestAccessUpdatedBy() {
        return $this->hasOne('App\Data\Models\User', 'user_id', 'updated_by');
    } 
}
