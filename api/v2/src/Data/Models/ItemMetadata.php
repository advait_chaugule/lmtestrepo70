<?php


namespace App\Data\Models;


use Illuminate\Database\Eloquent\Model;

class ItemMetadata extends Model
{
    protected $table = 'item_metadata';
    protected $fillable=[
        'item_id',
        'metadata_id',
        'metadata_value',
        'is_deleted',
        'is_additional',
        'metadata_value_html'
    ];
}