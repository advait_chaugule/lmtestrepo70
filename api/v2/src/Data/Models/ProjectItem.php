<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectItem extends Model
{
    protected $table ='project_items';
    protected $fillable = [
    'project_id',
    'item_id',
    'is_editable',
    'is_deleted',
    'created_at',
    'updated_at',
    ];
}
