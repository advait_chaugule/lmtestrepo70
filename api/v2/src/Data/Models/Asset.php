<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $primaryKey = "asset_id";
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'asset_id', 
        'organization_id',
        'document_id',
        'asset_name', 
        'asset_target_file_name',
        'asset_content_type', 
        'asset_size',
        'title',
        'description',
        'asset_linked_type',
        'item_linked_id',
        'uploaded_at',
        'asset_order',
        'status',
    ];

    public function itemAssociation() {
        return $this->belongsTo('App\Data\Models\ItemAssociation', "item_linked_id", "item_association_id");
    }

}
