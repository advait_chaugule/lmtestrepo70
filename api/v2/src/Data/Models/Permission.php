<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $incrementing = false;
    
    protected $primaryKey = "permission_id";  

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'permission_id', 'display_name', 'permission_group',  'parent_permission_id', 'order', 'created_at','updated_at'
    ];

    public function roles() {
        return $this->belongsToMany('App\Data\Models\Role', 'role_permission',  'permission_id', 'role_id');
    }
}
