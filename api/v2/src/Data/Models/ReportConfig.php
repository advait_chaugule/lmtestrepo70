<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ReportConfig extends Model
{
    protected $table = 'report_configs';
    protected $fillable=[
        'report_config_id',
        'internal_name',
        'title',
        'parameter_1_display_name',
        'parameter_1_subtype',
        'parameter_1_prebuild_type',
        'parameter_2_display_name',
        'parameter_2_subtype',
        'parameter_2_prebuild_type',
        'parameter_3_display_name',
        'parameter_3_subtype',
        'parameter_3_prebuild_type',
        'parameter_4_display_name',
        'parameter_4_subtype',
        'parameter_4_prebuild_type',
        'parameter_5_display_name',
        'parameter_5_subtype',
        'parameter_5_prebuild_type',
        'endpoint_url',
        'is_deleted'
    ];
}
