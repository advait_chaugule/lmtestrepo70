<?php
namespace App\Data\Models;
use Lucid\Foundation\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VersionSubscription extends Model
{
    use SoftDeletes;
    protected $table = 'version_subscription';
    public $incrementing = false;
    protected $primaryKey = "subscription_id";
    protected $dates = ['deleted_at'];
    public $timestamps = false;
    protected $fillable = [
                            'subscription_id',
                            'sub_document_id',
                            'subscriber_detail_id',
                            'subscriber_org_id',
                            'pub_source_document_id',
                            'pub_document_id',
                            'publisher_org_id',
                            'subscription_datetime',
                            'is_active',
                            'version'
                          ];
}