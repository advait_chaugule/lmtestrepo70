<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    public $incrementing = false;
    protected $primaryKey = "thread_id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_id', 'thread_source_id', 'thread_source_type', 'organization_id', 'assign_to', 'status', 'is_deleted', 'updated_at', 'created_at'
    ];

    
    public function assignTo() {
        return $this->belongsTo('App\Data\Models\User', 'assign_to', 'user_id');
    }

    public function threadCommentDetail() {
        return $this->hasMany('App\Data\Models\ThreadComment', 'thread_id', 'thread_id');
    }
}
