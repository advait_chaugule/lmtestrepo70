<?php
namespace App\Data\Models;
use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    protected $table ='user_settings';
    protected $fillable = [
        'user_id',
        'organization_id',
        'id',
        'id_type',
        'updated_by',
        'is_deleted',
        'json_config_value',
        'created_at',
        'updated_at',
    ];
}