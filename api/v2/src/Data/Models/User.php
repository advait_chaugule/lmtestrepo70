<?php

namespace App\Data\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public $incrementing = false;
    
    use Notifiable, HasApiTokens;
    
    protected $primaryKey = "user_id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'username',
        'active_access_token',
        'access_token_updated_at',
        'role_id',
        'organization_id',
        'is_active',
        'is_deleted',
        'password',
		'self_reg_status',
		'reg_user_type',
		'reg_admin_languages',
		'reg_admin_other_languages',
		'reg_teacher_grades',
		'reg_teacher_subjects',
		'reg_is_work_higher_education',
        'self_reg_expiry',
        'current_organization_id',
        "is_super_admin",
        'is_register',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * Get the system role of a user
     */
    public function systemRole()
    {
        return $this->belongsTo('App\Data\Models\Role', 'role_id', 'role_id');
    }

    /**
     * Get the user's organization.
     */
    public function organization()
    {
        return $this->belongsTo('App\Data\Models\Organization', 'organization_id', 'organization_id');
    }

    public function projects() {
        return $this->belongsToMany('App\Data\Models\Project', 'project_users', 'user_id', 'project_id');
    }    
}
