<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    public $incrementing = false;
    protected $primaryKey = "license_id";

    protected $fillable = [
        "license_id",
        "title",
        "description",
        "license_text",
        "is_deleted",
        "created_at",
        "updated_at",
        "source_license_id",
        "organization_id",
        "uri",
        "title_html",
        "description_html",
        "license_text_html"
    ];

}
