<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Concept extends Model
{

    public $incrementing = false;
    protected $primaryKey = "concept_id";

    protected $fillable = [
        "concept_id",
        "keywords",
        "title",
        "hierarchy_code",
        "description",
        "created_at",
        "updated_at",
        "source_concept_id",
        "organization_id",
        "uri",
        "title_html",
        "description_html"
    ];

}
