<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ItemAssociation extends Model
{
    public $incrementing = false;

    protected $primaryKey = "item_association_id";
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_association_id', 
        'association_type', 
        'document_id', 
        'origin_node_id', 
        'destination_node_id',
        'destination_document_id',
        'association_group_id',
        'sequence_number',
        'is_reverse_association',
        'is_deleted',
        'created_at',
        'updated_at',
        "source_item_association_id",
        "external_node_title",
        "external_node_url",
        "organization_id",
        "has_asset",
        "description",
        "uri",
        "is_reverse_association",
        "source_document_id",
        "source_item_id",
        "target_document_id",
        "target_item_id"
    ];

    public function document() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "document_id");
    }

    public function sourceDocumentId() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "document_id");
    }

    public function targetDocumentId() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "document_id");
    }

    public function originNode() {
        return $this->hasOne('App\Data\Models\Item', "item_id", "origin_node_id");
    }

    public function sourceItemId() {
        return $this->hasOne('App\Data\Models\Item', "item_id", "source_item_id");
    }

    public function originDocumentNode() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "origin_node_id");
    }

    public function sourceDocumentNode() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "source_item_id");
    }

    public function destinationNode() {
        return $this->hasOne('App\Data\Models\Item', "item_id", "destination_node_id");
    }

    public function targetItemId() {
        return $this->hasOne('App\Data\Models\Item', "item_id", "target_item_id");
    }

    public function destinationDocumentNode() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "destination_node_id");
    }

    public function targetItemDocumentNode() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "target_item_id");
    }

    public function targetDocumentNode() {
        return $this->hasOne('App\Data\Models\Document', "document_id", "target_item_id");
    }

    public function associationGroup() {
        return $this->hasOne('App\Data\Models\AssociationGroup', "association_group_id", "association_group_id");
    }

    public function asset() {
        return $this->hasOne('App\Data\Models\Asset', "item_association_id", "item_association_id");
    }

}
