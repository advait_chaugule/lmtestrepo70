<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class NodeTemplate extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $primaryKey = "node_template_id";
    public $incrementing = false;
    
    protected $fillable =  [
        'node_template_id',
        'organization_id',
        'name',
        'is_active', 
        'is_deleted',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public function nodeTypes() {
        return $this->belongsToMany("App\Data\Models\NodeType", "node_template_node_types", "node_template_id", "node_type_id")->withPivot('parent_node_type_id', 'sort_order');
    }

    public function updatedBy() {
        return $this->belongsTo('App\Data\Models\User', 'updated_by', 'user_id');
    }

}
