<?php
namespace App\Data\Models;
use Illuminate\Database\Eloquent\Model;

class UserOrganization extends Model
{
    protected $table ='users_organization';
    protected $fillable = [
    'user_id',
    'organization_id',
    'role_id',
    'is_active',
    'created_by',
    'updated_by',
    'is_deleted',
    'created_at',
    'updated_at',
    'enable_email_notification',
    ];
}