<?php
namespace App\Data\Models\Report;

use Illuminate\Database\Eloquent\Model;

class DimUser extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        "user_id",
        "first_name",
        "last_name"
    ];

}
