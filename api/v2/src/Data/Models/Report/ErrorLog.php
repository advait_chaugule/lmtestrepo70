<?php
namespace App\Data\Models\Report;
use Illuminate\Database\Eloquent\Model;

class ErrorLog extends Model
{
    protected $connection = 'mysql_audit';
    protected $table      = 'error_logs';
    protected $fillable   = ['id','service_logical_id','error_summary','trace_log'];

}