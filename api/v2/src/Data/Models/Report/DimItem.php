<?php
namespace App\Data\Models\Report;

use Illuminate\Database\Eloquent\Model;

class DimItem extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        "item_id",
        "item_display_name"
    ];

}
