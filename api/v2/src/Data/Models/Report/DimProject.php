<?php
namespace App\Data\Models\Report;

use Illuminate\Database\Eloquent\Model;

class DimProject extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        "project_id",
        "project_display_name"
    ];

}
