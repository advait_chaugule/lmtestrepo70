<?php
namespace App\Data\Models\Report;

use Illuminate\Database\Eloquent\Model;

class FactActivityLog extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $connection = 'mysql_audit';
    protected $table      = 'fact_activity_logs';
    public $incrementing = false;

    protected $fillable = [
        "activity_log_id",
        "user_id",
        "organization_id",
        "activity_event_type",
        "activity_id",
        "activity_event_sub_type",
        "activity_event_sub_type_activity_id",
        "activity_package_s3_file_id",
        "cache_activity_name",
        "cache_activity_subtype_activity_name",
        "cache_user_display_name",
        "activity_log_timestamp",
        "ip_address"
    ];

}
