<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public $incrementing = false;
    protected $primaryKey = "language_id";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        'language_id', 
        'name',
        'short_code',
        'is_deleted', 
        'created_at', 
        'updated_at',
        'organization_id'
    ];
}
