<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $primaryKey = "project_id";
    public $incrementing = false;
    
    protected $fillable =  [
        'project_id',
        'organization_id',
        'project_type',
        'project_name',
        'description',
        'workflow_id',
        'project_type',
        'is_deleted', 
        'created_at',
        'updated_at',
        'document_id',
        'current_workflow_stage_id',
        'updated_by',
        'created_by',
    ];

    public function workflow() {
        return $this->hasOne('App\Data\Models\Workflow', 'workflow_id', 'workflow_id');
    }

    public function items() {
        return $this->belongsToMany('App\Data\Models\Item', 'project_items', 'project_id', 'item_id');
    }

    public function prefDocument() {
        return $this->belongsToMany('App\Data\Models\Document', 'project_pref_documents', 'project_id', 'document_id');
    }

    public function document() {
        return $this->hasOne('App\Data\Models\Document', 'document_id', 'document_id');
    }

    public function users() {
        return $this->belongsToMany('App\Data\Models\User', 'project_users', 'project_id', 'user_id');
    }

    public function project_status() {
        return $this->hasOne('App\Data\Models\WorkflowStage', 'workflow_stage_id', 'current_workflow_stage_id');
    }

    public function user() {
        return $this->hasOne('App\Data\Models\User', 'user_id', 'updated_by');
    }
}
