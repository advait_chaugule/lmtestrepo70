<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class PacingGuide extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $primaryKey = "pacing_guide_id";
    public $incrementing = false;

    protected $fillable =  [
        'pacing_guide_id',
        'organization_id',
        'workflow_id',
        'name',
        'description',
        'is_deleted', 
        'created_at',
        'updated_at',
        'updated_by',
    ];
}
