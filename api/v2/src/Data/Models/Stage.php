<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public $incrementing = false;

    protected $table = 'stage';
    protected $primaryKey = "stage_id";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stage_id',
        'organization_id',
        'name',
        'order',
        'is_deleted'
    ];
}
