<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class SearchHistory extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $primaryKey = "search_id";
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'search_id', 
        'title',
        'description',
        'search_filter',
        'is_explicit_saved',
        'context_page',
        'organization_id',
        'created_by',
        'updated_at'
    ];

}
