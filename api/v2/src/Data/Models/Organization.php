<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    public $incrementing = false;
    protected $primaryKey = "organization_id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id', 'name', 'default', 'org_code', 'is_deleted', 'created_at','updated_at', 'created_by','is_active','updated_by'
    ];

    public function users() {
        return $this->hasMany('App\Data\Models\User', 'organization_id', 'organization_id');
    }

}
