<?php
namespace App\Data\Models;
use Illuminate\Database\Eloquent\Model;

class AppNotification extends Model
{
    protected $table      = 'notifications';
    protected $primaryKey = 'user_id';
    public    $timestamps = true;
    //public    $updated_at = false;

    protected $fillable = [
        'notification_id',
        'description',
        'target_type',
        'target_id',
        'user_id',
        'organization_id',
        'notification_category',
        'is_deleted',
        'notification_status', //1=read,0=not read
        'created_by',
        'updated_by',
        'target_context',
        'read_status',
        'created_at',
        'updated_at'
    ];

}