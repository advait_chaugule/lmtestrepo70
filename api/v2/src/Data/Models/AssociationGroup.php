<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class AssociationGroup extends Model
{
    public $incrementing = false;

    protected $primaryKey = "association_group_id";
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'association_group_id',
        'title', 
        'description',
        'created_at',
        'updated_at',
        "source_association_group_id",
        "organization_id",
        "uri"
    ];

    public function itemAssociations() {
        return $this->hasMany('App\Data\Models\ItemAssociation', "association_group_id", "association_group_id");
    }

}
