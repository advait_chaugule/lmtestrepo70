<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public $incrementing = false;
    protected $primaryKey = "document_id";

    protected $fillable = [
        "document_id",
        "title",
        "official_source_url",
        "language_id",
        "adoption_status",
        "status",
        "notes",
        "publisher",
        "description",
        "version",
        "status_start_date",
        "status_end_date",
        "license_id",
        "source_license_uri_object",
        "creator",
        "is_deleted",
        "created_at",
        "updated_at",
        "organization_id",
        'document_type',
        "source_document_id",
        "created_by",
        "updated_by",
        "project_id",
        "node_template_id",
        "node_type_id",
        "import_type",
        "actual_import_type",       //for import type column view
        "uri",
        "title_html",
        "notes_html",
        "publisher_html",
        "description_html",
        "display_options",
        "import_status",
        "is_locked",
        "source_type",
    ];

    public function license()
    {
        return $this->belongsTo('App\Data\Models\License', 'license_id', 'license_id');
    }

    public function language()
    {
        return $this->belongsTo('App\Data\Models\Language', 'language_id', 'language_id');
    }

    public function subjects() {
        return $this->belongsToMany('App\Data\Models\Subject', 'document_subject', 'document_id', 'subject_id')->withPivot('source_subject_uri_object');
    }

    public function projects()
    {
        return $this->belongsTo('App\Data\Models\Projects', 'document_id', 'document_id');
    }

    public function items() {
        return $this->hasMany('App\Data\Models\Item', 'document_id', 'document_id');
    }

    public function itemAssociations() {
        return $this->hasMany('App\Data\Models\ItemAssociation', 'origin_node_id', 'document_id');
    }

    public function nodeType()
    {        
        return $this->belongsTo('App\Data\Models\NodeType', 'node_type_id', 'node_type_id');
    }

    public function customMetadata()
    {        
        return $this->belongsToMany('App\Data\Models\Metadata', 'document_metadata', 'document_id', 'metadata_id')->withPivot('metadata_value','metadata_value_html','is_additional');
    }

    public function asset()
    {        
        return $this->hasMany('App\Data\Models\Asset', 'item_linked_id', 'document_id');
    }

    public function publicReviewDocuments()
    {        
        return $this->belongsToMany('App\Data\Models\Document', 'document_public_reviews', 'document_id', 'original_document_id')->withPivot('created_by', 'created_at');
    }
}
