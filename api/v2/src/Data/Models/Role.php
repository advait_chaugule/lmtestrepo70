<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $incrementing = false;
    protected $primaryKey = "role_id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'organization_id', 'name', 'description','role_code', 'is_active', 'is_deleted', 'usage_count', 'created_at','updated_at'
    ];

    public function permissions() {
        return $this->belongsToMany('App\Data\Models\Permission', 'role_permission', 'role_id', 'permission_id');
    }

    public function users() {
        return $this->hasMany('App\Data\Models\User', 'role_id', 'role_id');
    }

}
