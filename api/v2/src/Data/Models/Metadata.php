<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Metadata extends Model
{
    public $incrementing = false;
    protected $primaryKey = "metadata_id";

    protected $fillable = [
        "metadata_id",
        "organization_id",
        "parent_metadata_id",
        "name",
        "internal_name",
        "field_type",
        "field_possible_values",
        "last_field_possible_values",
        "order",
        "is_custom",
        "is_document",
        "is_mandatory",
        "is_active",
        "is_deleted",
        "updated_by",
        "updated_at"        
    ];

    public function users() {
        return $this->belongsTo('App\Data\Models\User', 'updated_by', 'user_id');
    }

    public function nodeType() {
        return $this->belongsToMany('App\Data\Models\NodeType', 'node_type_metadata', 'metadata_id', 'node_type_id');
    }

    public function documents() {
        return $this->belongsToMany('App\Data\Models\Document', 'document_metadata', 'metadata_id', 'document_id');
    }
}
