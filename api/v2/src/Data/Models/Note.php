<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //public $incrementing = false;   
    protected $primaryKey = "note_id";
    protected $table = 'notes';
    protected $updated_at = 'false';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        "note_id",
        "title",
        "description",
        "note_status",
        "note_order",
        "created_at",
        "updated_at",
        "is_deleted",
        "created_by",
        "updated_by",
        "organization_id"
       
    ];

}