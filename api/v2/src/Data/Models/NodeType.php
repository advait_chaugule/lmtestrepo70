<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class NodeType extends Model
{
    public $incrementing = false;   
    protected $primaryKey = "node_type_id";
    protected $table = 'node_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        "node_type_id",
        "organization_id",
        "title",
        "type_code",
        "hierarchy_code",
        "description",
        "source_node_type_id",
        "is_custom",
        "is_deleted",
        "created_by",
        "updated_by",
        "updated_at",
        "is_default",
        "is_document",
        "uri",
        "used_for"
    ];

    public function updatedBy() {
        return $this->belongsTo('App\Data\Models\User', 'updated_by', 'user_id');
    }

    public function createdBy() {
        return $this->belongsTo('App\Data\Models\User', 'created_by', 'user_id');
    }

    public function metadata() {
        return $this->belongsToMany('App\Data\Models\Metadata', 'node_type_metadata', 'node_type_id', 'metadata_id')->withPivot('sort_order','is_mandatory');
    }
}
