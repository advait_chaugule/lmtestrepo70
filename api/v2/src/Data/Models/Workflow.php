<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    public $incrementing = false;

    protected $primaryKey = "workflow_id";

    protected $fillable = [
        "workflow_id",
        "organization_id",
        "name",
        "workflow_code",
        "is_active",
        "is_deleted",
        "updated_by",
        "created_at",
        "updated_at",
        "is_default"
    ];

    /**
     * Get the project count of a workflow
     */
    public function project()
    {
        return $this->belongsTo('App\Data\Models\Project', 'workflow_id', 'workflow_id');
    }

    public function user() {
        return $this->hasOne('App\Data\Models\User', 'user_id', 'updated_by');
    }
}
