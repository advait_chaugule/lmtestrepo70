<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class LinkedServer extends Model
{
    //
    public $incrementing = false;
    protected $primaryKey = "linked_server_id";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        'linked_server_id',
        'linked_type',
        'linked_name',
        'description',
        'server_url',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
        'connection_status',
        'organization_id',
        'is_deleted'
    ];
}
