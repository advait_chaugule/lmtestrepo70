<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class NodeTypeMetaData extends Model
{
    protected $table ='node_type_metadata';
    
    protected $fillable = [
        'node_type_id',
        'metadata_id',
        'is_mandatory',
        'sort_order',
    ];
}
