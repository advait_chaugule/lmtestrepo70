<?php


namespace App\Data\Models;


use Illuminate\Database\Eloquent\Model;

class PublicReview extends Model
{
    protected $table ='project_user_status';
    public    $incrementing = false;
    protected $primaryKey = 'project_id';
    public    $timestamps = true;
    public    $updated_at = false;

    protected $fillable = [
        'organization_id',
        'project_id',
        'user_id',
        'review_status',
        'is_deleted',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}