<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ImportJob extends Model
{
    public $incrementing = false;
    protected $table = "import_job";

    protected $primaryKey = "import_job_id";

    protected $fillable = [
        "import_job_id",
        "results",
        "filefullpath",
        "errors",
        "user_id",
        "organization_id",
        "source_document_id",
        "process_type",
        "status",
        "created_at",
        "updated_at",
        "batch_id",
        "is_deleted",
        "processed_csv_s3_file_path",
        "import_taxonomy_settings",
        "success_message_data"
    ];
}
