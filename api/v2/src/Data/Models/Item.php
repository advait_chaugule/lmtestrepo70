<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $incrementing = false;
    protected $primaryKey = "item_id";

    protected $fillable = [
        "item_id",
        "parent_id",
        "document_id",
        "full_statement",
        "human_coding_scheme",
        "list_enumeration",
        "abbreviated_statement",
        "alternative_label",
        "education_level",
        "notes",
        "concept_id",
        "source_concept_uri_object",
        "language_id",
        "node_type_id",
        "source_node_type_uri_object",
        "license_id",
        "source_license_uri_object",
        "status_start_date",
        "status_end_date",
        "is_deleted",
        "created_at",
        "updated_at",
        "organization_id",
        "source_item_id",
        "updated_by",
        "uri",
        "full_statement_html",
        "alternative_label_html",
        "human_coding_scheme_html",
        "abbreviated_statement_html",
        "notes_html",
        "import_type"
    ];

    public function license()
    {
        return $this->belongsTo('App\Data\Models\License', 'license_id', 'license_id');
    }

    public function concept()
    {
        return $this->belongsTo('App\Data\Models\Concept', 'concept_id', 'concept_id');
    }

    public function conceptMapping()
    {
        return $this->belongsToMany('App\Data\Models\Concept', 'item_concepts', 'item_id', 'concept_id')->withPivot(['source_concept_id','organization_id']);
    }

    public function language()
    {
        return $this->belongsTo('App\Data\Models\Language', 'language_id', 'language_id');
    }

    public function project() {
        return $this->belongsToMany('App\Data\Models\Project', 'project_items', 'item_id', 'project_id');
    }

    public function itemType()
    {        
        return $this->belongsTo('App\Data\Models\ItemType', 'item_type_id', 'item_type_id');
    }

    public function document()
    {        
        return $this->belongsTo('App\Data\Models\Document', 'document_id', 'document_id');
    }

    public function itemAssociations() {
        return $this->hasMany("App\Data\Models\ItemAssociation", "origin_node_id", "item_id");
    }

    public function itemAssociationsV2() {
        return $this->hasMany("App\Data\Models\ItemAssociation", "source_item_id", "item_id");
    }

    public function itemDestinationAssociations() {
        return $this->hasMany("App\Data\Models\ItemAssociation", "destination_node_id", "item_id");
    }

    public function itemDestinationAssociationsV2() {
        return $this->hasMany("App\Data\Models\ItemAssociation", "target_item_id", "item_id");
    }

    public function nodeType()
    {        
        return $this->belongsTo('App\Data\Models\NodeType', 'node_type_id', 'node_type_id');
    }

    public function customMetadata() {
        return $this->belongsToMany('App\Data\Models\Metadata', 'item_metadata', 'item_id', 'metadata_id')->withPivot(['metadata_value','metadata_value_html','is_additional']);
    }

    public function asset()
    {        
        return $this->hasMany('App\Data\Models\Asset', 'item_linked_id', 'item_id');
    }
}
