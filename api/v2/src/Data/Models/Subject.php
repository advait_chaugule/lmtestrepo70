<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $incrementing = false;
    protected $primaryKey = "subject_id";

    protected $fillable = [
        "subject_id",
        "title",
        "hierarchy_code",
        "description",
        "is_deleted",
        "created_at",
        "updated_at",
        "source_subject_id",
        "source_subject_uri_object",
        "organization_id",
        "uri",
        "title_html",
        "description_html"
    ];

    public function documents() {
        return $this->belongsToMany('App\Data\Models\Document', 'document_subject', 'subject_id', 'document_id');
    }

}
