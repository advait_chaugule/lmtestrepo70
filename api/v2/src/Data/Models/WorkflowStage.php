<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class WorkflowStage extends Model
{
    public $incrementing = false;

    protected $table = 'workflow_stage';
    protected $primaryKey = "workflow_stage_id";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workflow_stage_id',
        'workflow_id',
        'stage_id',
        'stage_name',
        'stage_description',
        'order'
    ];
}
