<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ThreadComment extends Model
{
    public $incrementing = false;
    protected $primaryKey = "thread_comment_id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_comment_id', 'thread_id', 'thread_source_id', 'organization_id', 'parent_id', 'comment',  'is_deleted', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];

    public function createdBy() {
        return $this->belongsTo('App\Data\Models\User', 'created_by', 'user_id');
    }

    public function threadDetail() {
        return $this->belongsTo('App\Data\Models\Thread', 'thread_id', 'thread_id');
    }

}
