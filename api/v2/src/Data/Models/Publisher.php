<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        'id', 
        'name', 
        'is_active', 
        'is_deleted', 
        'created_at', 
        'updated_at'
    ];
}
