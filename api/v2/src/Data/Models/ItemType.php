<?php
namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    public $incrementing = false;
    protected $primaryKey = "item_type_id";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  [
        'item_type_id', 
        'title',
        'type_code',
        'hierarchy_code',
        'description',
        'is_deleted', 
        'created_at', 
        'updated_at',
        "source_item_type_id",
        "uri"
    ]; 
}
