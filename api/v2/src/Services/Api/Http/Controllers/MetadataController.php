<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListMetadataFeature;
use App\Services\Api\Features\CreateMetadataFeature;
use App\Services\Api\Features\GetMetadataFeature;
use App\Services\Api\Features\UpdateMetadataFeature;
use App\Services\Api\Features\DeleteMetadataFeature;
use App\Services\Api\Features\CheckMetadataValueUsageFeature;


class MetadataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /**
     * The REST read request message for the index() API call.
     * @SWG\Get(
     *     path="metadata",
     *     tags={"METADATA"},
     *     summary="The REST read request message for the index() API call.",
     *     description="This is a request to the service provider to  get metadata",
     *     operationId="index",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/indexMetadataResponse",
     *     )
     * )
     *
     */
    
    /**
     * @SWG\Response(
     *      response="indexMetadataResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="array",
     *                     description="",
     *                                @SWG\Items(   
     *                                        @SWG\Property(
     *                                        property="metadata_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="parent_metadata_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="name",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="internal_name",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="type",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_type_id",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_possible_values",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="order",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_custom",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_document",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_mandatory",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_active",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="updated_by",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="updated_at",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="usage_count",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                 )                    
     *                      ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="integer",
     *                     description=""
     *                     ),
     *             ),
     * )
     */ 
    public function index()
    {
        //List the metadata with search
        //increase memory limit to 256MB
        ini_set('memory_limit','256M');
        return $this->serve(ListMetadataFeature::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     /**
     * The REST read request message for the create() API call.
     * @SWG\Post(
     *     path="metadata",
     *     tags={"METADATA"},
     *     summary="The REST read request message for the create() API call.",
     *     description="This is a request to the service provider to create metadata",
     *     operationId="create",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="Body",
     *     in="body",
     *     required=true,
     *     type="object",
     *     ref="$/responses/createMetadata",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/createMetadataResponse",
     *     )
     * )
     *
     */
 
    /**
     * @SWG\Response(
     *      response="createMetadata",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="name",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="field_type",
     *                     type="integer",
     *                     description=""
     *                     ),
     *             ),
     * )
     */

    /**
     * @SWG\Response(
     *      response="createMetadataResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="array",
     *                     description="",
     *                                @SWG\Items(   
     *                                        @SWG\Property(
     *                                        property="name",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_type",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="organization_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_possible_values",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="last_field_possible_values",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="metadata_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="order",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_custom",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="updated_at",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="created_at",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                 )                    
     *                      ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="integer",
     *                     description=""
     *                     ),
     *             ),
     * )
     */  
    public function create()
    {
        //Create the custom metadata
        return $this->serve(CreateMetadataFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     
     /**
     * The REST read request message for the show() API call.
     * @SWG\Get(
     *     path="metadata/{metadata_id}",
     *     tags={"METADATA"},
     *     summary="The REST read request message for the show() API call.",
     *     description="This is a request to the service provider to show metadata",
     *     operationId="show",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="metadata_id",
     *     in="path",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/getMetadataResponse",
     *     )
     * )
     *
     */

      
    /**
     * @SWG\Response(
     *      response="getMetadataResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="integer",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="boolean",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */
    public function show($id)
    {
        //Get the single Metadata
        return $this->serve(GetMetadataFeature::class);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /**
     * The REST read request message for the edit() API call.
     * @SWG\Put(
     *     path="metadata/{metadata_id}",
     *     tags={"METADATA"},
     *     summary="The REST read request message for the edit() API call.",
     *     description="This is a request to the service provider to edit metadata",
     *     operationId="edit",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="metadata_id",
     *     in="path",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="Body",
     *     in="body",
     *     required=true,
     *     type="object",
     *     ref="$/responses/editMetadata",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/editMetadataResponse",
     *     )
     * )
     *
     */
 
    /**
     * @SWG\Response(
     *      response="editMetadata",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="name",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="field_type",
     *                     type="integer",
     *                     description=""
     *                     ),
     *             ),
     * )
     */
    
    /**
     * @SWG\Response(
     *      response="editMetadataResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="array",
     *                     description="",
     *                                @SWG\Items(                                          
     *                                        @SWG\Property(
     *                                        property="metadata_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="name",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="internal_name",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_type",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_possible_values",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_custom",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_active",
     *                                        type="integer",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="updated_at",
     *                                        type="integer",
     *                                        description=""
     *                                        )
     *                                 )                    
     *                      ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="integer",
     *                     description=""
     *                     ),
     *             ),
     * )
     */ 
    public function edit($id)
    {
        //update any attribute of the metadata
        return $this->serve(UpdateMetadataFeature::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     
     /**
     * The REST read request message for the destroy() API call.
     * @SWG\Delete(
     *     path="metadata/{metadata_id}",
     *     tags={"METADATA"},
     *     summary="The REST read request message for the destroy() API call.",
     *     description="This is a request to the service provider to delete metadata",
     *     operationId="destroy",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="metadata_id",
     *     in="path",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *          ref="$/responses/deleteMetadataResponse",
     *     )
     * )
     *
     */
 
     
    /**
     * @SWG\Response(
     *      response="deleteMetadataResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="array",
     *                     description="",
     *                                @SWG\Items(                                          
     *                                        @SWG\Property(
     *                                        property="metadata_id",
     *                                        type="string",
     *                                        description="",
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="name",
     *                                        type="string",
     *                                        description="",
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_type",
     *                                        type="string",
     *                                        description="",
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="field_possible_values",
     *                                        type="string",
     *                                        description="",
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_custom",
     *                                        type="integer",
     *                                        description="",
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="is_active",
     *                                        type="integer",
     *                                        description="",
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="updated_at",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                  @SWG\Property(
     *                                                  property="date",
     *                                                  type="string",
     *                                                  description="",
     *                                                  ),                                        
     *                                                  @SWG\Property(
     *                                                  property="timezone_type",
     *                                                  type="string",
     *                                                  description="",
     *                                                  ),                                        
     *                                                  @SWG\Property(
     *                                                  property="timezone",
     *                                                  type="string",
     *                                                  description="",
     *                                                  ),
     *                                               )
     *                                        ),
     *                                 )                    
     *                      ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="integer",
     *                     description=""
     *                     ),
     *             ),
     * )
     */ 
    public function destroy($id)
    {
        //Delete the custome metadata
        return $this->serve(DeleteMetadataFeature::class);
    }

    public function checkMetadataValueUsage($id)
    {
        //Delete the custome metadata
        return $this->serve(CheckMetadataValueUsageFeature::class);
    }
}
