<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\MultipleItemDetailsFeature;

class MultipleItemDetailsController extends Controller
{
    public function list()
    {
        return $this->serve(MultipleItemDetailsFeature::class);
    }
}
