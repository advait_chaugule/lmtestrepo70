<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\GetProtipJsonFeature;
use App\Services\Api\Features\GetWalkthroughJsonFeature;
use App\Services\Api\Features\GetInputControlsJsonFeature;
use App\Services\Api\Features\GetAssetFileFromS3Feature;

class ProxyController extends Controller
{
    public function getProtipJson() {
        return $this->serve(GetProtipJsonFeature::class);
    }

    public function getWalkthroughJson() {
        return $this->serve(GetWalkthroughJsonFeature::class);
    }

    public function getInputControlsJson() {
        return $this->serve(GetInputControlsJsonFeature::class);
    }

    public function fetchAssetFileFromS3() {
        return $this->serve(GetAssetFileFromS3Feature::class);
    }
}
