<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListRoleFeature;
use App\Services\Api\Features\CreateRoleFeature;
use App\Services\Api\Features\SetRolePermissionFeature;
use App\Services\Api\Features\GetRoleFeature;
use App\Services\Api\Features\DeleteRoleFeature;

use App\Services\Api\Features\UpdateRoleFeature;

class RoleController extends Controller
{
    /**
     * Display a listing of the role.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        //list the roles
        return $this->serve(ListRoleFeature::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //create a new role
        return $this->serve(CreateRoleFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $roleId
     * @return \Illuminate\Http\Response
     */
    public function show($roleId)
    {
        //get role detail
        return $this->serve(GetRoleFeature::class);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setPermission()
    {
        //set permission for role
        return $this->serve(SetRolePermissionFeature::class);
    }

    /**
     * Update the status of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //Update the role with attributes
        return $this->serve(UpdateRoleFeature::class);
    }

    /**
     * Update the status of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {
        //Update the role with attributes
        return $this->serve(UpdateRoleFeature::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($roleId)
    {
        //Delete a Role 
        return $this->serve(DeleteRoleFeature::class);
    }
}
