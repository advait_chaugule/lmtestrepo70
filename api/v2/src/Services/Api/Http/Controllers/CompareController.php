<?php
namespace App\Services\Api\Http\Controllers;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\ExportVersionJsonFeature;
use App\Services\Api\Features\ComparisonSummaryReportFeature;
use App\Services\Api\Features\NodeDetailsFromPublishedJsonFeature;

class CompareController extends Controller
{
    /**
     * Create a Subscription
     */
    public function compareVersion()
    {
        return $this->serve(ExportVersionJsonFeature::class);
    }

    public function getComparisonSummaryReport()
    {
        return $this->serve(ComparisonSummaryReportFeature::class);
    }    

    public function getNodeDetailsFromPublishedJson()
    {
        return $this->serve(NodeDetailsFromPublishedJsonFeature::class);
    }
}