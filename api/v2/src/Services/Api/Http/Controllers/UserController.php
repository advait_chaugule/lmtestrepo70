<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ValidateLoginFeature;
use App\Services\Api\Features\LogoutFeature;
use App\Services\Api\Features\ListingUserFeature;
use App\Services\Api\Features\CreateUserFeature;
use App\Services\Api\Features\GetUserDetailsByTokenFeature;
use App\Services\Api\Features\UserRegisterCompleteFeature;
use App\Services\Api\Features\UserDetailsFeature;
use App\Services\Api\Features\UseEditFeature;
use App\Services\Api\Features\UseDeleteFeature;
use App\Services\Api\Features\UpdateUserFeature;
use App\Services\Api\Features\ForgotPasswordFeature;
use App\Services\Api\Features\ResetRequestFeature;
use App\Services\Api\Features\ResetPasswordFeature;
use App\Services\Api\Features\CreateOrgnizationFeature;
use App\Services\Api\Features\ForgotPasswordAgainFeature;

use App\Services\Api\Features\SetUserInfoByOrganizationFeature;
use App\Services\Api\Features\GetUserInfoByOrganizationFeature;
use App\Services\Api\Features\UpdateProfileFeature;
use App\Services\Api\Features\ChangePasswordFeature;
use App\Services\Api\Features\GetEditedProfileFeature;
use App\Services\Api\Features\createUserViaCSVFeature;
use App\Services\Api\Features\resendEmailFeature;
use App\Services\Api\Features\UpdateRefreshFeature;
use App\Services\Api\Features\CISLoginFeature;
use App\Services\Api\Features\CISCreateUserFeature;

class UserController extends Controller
{
    /**
     * This method will be used to authenticate a user manually
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Response(
     *      response="validateLoginResponse",
     *      description="the basic response",
     *      @SWG\Schema(
     *                           @SWG\Property(
     *                              property="user_id",
     *                              type="string",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="active_access_token",
     *                              type="string",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="email",
     *                              type="string",
     *                              description=""
     *                           ), 
     *                           @SWG\Property(
     *                              property="first_name",
     *                              type="string",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="last_name",
     *                              type="string",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="updated_at",
     *                              type="string",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="current_organization_id",
     *                              type="string",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="is_super_admin",
     *                              type="integer",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="system_role",
     *                              type="array",
     *                              description="",
     *                                @SWG\Items(                                          
     *                                        @SWG\Property(
     *                                        property="role_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="name",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="role_code",
     *                                        type="string",
     *                                        description=""
     *                                        )
     *                                     )
     *                           ),
     *                           @SWG\Property(
     *                              property="project_role_permissions",
     *                              type="[]",
     *                              description=""
     *                           ),
     *                           @SWG\Property(
     *                              property="system_role_permisssion",
     *                              type="array",
     *                              description="",
     *                                @SWG\Items(                                          
     *                                        @SWG\Property(
     *                                        property="project_id",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="project_permission",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="taxonomy_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="comment_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="metadata_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="node_type_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="node_template_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="workflow_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="role_user_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="notification_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="public_review_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="note_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                        @SWG\Property(
     *                                        property="pacing_guide_permissions",
     *                                        type="array",
     *                                        description="",
     *                                        @SWG\Items(                                          
     *                                                   @SWG\Property(
     *                                                   property="permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="parent_permission_id",
     *                                                   type="string",
     *                                                   description=""
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_name",
     *                                                   type="string",
     *                                                   description="View Project List"
     *                                                   ),
     *                                                   @SWG\Property(
     *                                                   property="display_order",
     *                                                   type="integer",
     *                                                   description=""
     *                                                   )
     *                                              ),
     *                                       ),
     *                                 )
     *                           ),
     *                           @SWG\Property(
     *                           property="organization",
     *                           type="array",
     *                           description="",
     *                           @SWG\Items(
     *                                      @SWG\Property(
     *                                      property="organization_id",
     *                                      type="string",
     *                                      description="",
     *                                      ),
     *                                      @SWG\Property(
     *                                      property="name",
     *                                      type="string",
     *                                      description="",
     *                                      ),
     *                                      @SWG\Property(
     *                                      property="org_code",
     *                                      type="string",
     *                                      description="",
     *                                      ),
     *                                      @SWG\Property(
     *                                      property="domian_name",
     *                                      type="string",
     *                                      description="",
     *                                      ),
     *                                   )
     *                           ),
     *                           @SWG\Property(
     *                           property="message",
     *                           type="string",
     *                           description="",
     *                           ),
     *                  ),
     * )
     *
     */


     /**
     * The REST read request message for the validateLogin() API call.
     * @SWG\Post(
     *     path="validateLogin",
     *     tags={"IAM"},
     *     summary="The REST read request message for the validateLogin() API call.",
     *     description="This is a request to the service provider to Validate Login",
     *     operationId="validateLogin",
     *     produces={"application/json"},  
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         type="object",
     *         ref="$/responses/validateLogin",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/validateLoginResponse",
     *     )
     * )
     *
     */

     /**
     * @SWG\Response(
     *      response="validateLogin",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="username",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="password",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */

    public function validateLogin(){
       return $this->serve(ValidateLoginFeature::class);
    }

    /**
     * The REST read request message for the logout() API call.
     * @SWG\Get(
     *     path="logout",
     *     tags={"IAM"},
     *     summary="The REST read request message for the logout() API call.",
     *     description="This is a request to the service provider to logout User",
     *     operationId="logout",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/LogoutResponse",
     *     )
     * )
     *
     */
       
    /**
     * @SWG\Response(
     *      response="LogoutResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="integer",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="array",
     *                     description="",
     *                                @SWG\Items(                                          
     *                                        @SWG\Property(
     *                                        property="message",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                 )                    
     *                      ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */ 
    public function logout(){
       return $this->serve(LogoutFeature::class);
    }

    /**
     * The REST read request message for the listing() API call.
     * @SWG\Get(
     *     path="users",
     *     tags={"User Api"},
     *     summary="The REST read request message for the listing() API call.",
     *     description="This is a request to the service provider to get list of users",
     *     operationId="listing",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/listingResponse",
     *     )
     * )
     *
     */
    
    /**
     * @SWG\Response(
     *      response="listingResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="status",
     *                     type="integer",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="data",
     *                     type="array",
     *                     description="",
     *                     @SWG\Items(                                          
     *                               @SWG\Property(
     *                               property="UserList",
     *                               type="string",
     *                                description=""
     *                                        ),                               
     *                                @SWG\Property(
     *                                property="users",
     *                                type="array",
     *                                description="",
     *                                @SWG\Items(                                          
     *                                          @SWG\Property(
     *                                          property="user_id",
     *                                          type="string",
     *                                          description=""
     *                                                      ),                               
     *                                           @SWG\Property(
     *                                           property="email",
     *                                           type="string",
     *                                           description=""
     *                                                      ),                            
     *                                           @SWG\Property(
     *                                           property="first_name",
     *                                           type="string",
     *                                           description=""
     *                                                      ),                          
     *                                           @SWG\Property(
     *                                           property="last_name",
     *                                           type="string",
     *                                           description=""
     *                                                      ),                         
     *                                           @SWG\Property(
     *                                           property="is_active",
     *                                           type="string",
     *                                           description=""
     *                                                      ),                        
     *                                           @SWG\Property(
     *                                           property="role_id",
     *                                           type="string",
     *                                           description=""
     *                                                      ),                         
     *                                           @SWG\Property(
     *                                           property="system_role",
     *                                           type="array",
     *                                           description="",
     *                                           @SWG\Items(                                          
     *                                                      @SWG\Property(
     *                                                      property="role_id",
     *                                                      type="string",
     *                                                      description=""
     *                                                                  ),                             
     *                                                      @SWG\Property(
     *                                                      property="name",
     *                                                      type="string",
     *                                                      description=""
     *                                                                  ),
     *                                                      )
     *                                                      ),                      
     *                                           @SWG\Property(
     *                                           property="count",
     *                                           type="integer",
     *                                           description=""
     *                                                      ),                        
     *                                           @SWG\Property(
     *                                           property="totalUsers",
     *                                           type="integer",
     *                                           description=""
     *                                                      ),     
     *                                          )  
     *                                ),       
     *                             )                    
     *                ),
     *                 @SWG\Property(
     *                     property="message",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="success",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */ 
    public function listing(){
        return $this->serve(ListingUserFeature::class);
    }
    /**
     * The REST read request message for the create() API call.
     * @SWG\Post(
     *     path="users",
     *     tags={"User Api"},
     *     summary="The REST read request message for the create() API call.",
     *     description="This is a request to the service provider to create user",
     *     operationId="create",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="Body",
     *     in="body",
     *     required=true,
     *     type="object",
     *     ref="$/responses/create",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     )
     * )
     *
     */

    /**
     * @SWG\Response(
     *      response="create",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="email_id",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="role_id",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */
    public function create(){
        return $this->serve(CreateUserFeature::class);
    }

    public function getbytoken(){
        return $this->serve(GetUserDetailsByTokenFeature::class);
    }
    public function registercomplete(){
        
        return $this->serve(UserRegisterCompleteFeature::class);
    }
    public function getUserDetails(){
        
        return $this->serve(UserDetailsFeature::class);
    }
    public function userEdit(){
        
        return $this->serve(UseEditFeature::class);
    }

    /**
     * The REST read request message for the userDelete() API call.
     * @SWG\Delete(
     *     path="users/{user_id}",
     *     tags={"User Api"},
     *     summary="The REST read request message for the userDelete() API call.",
     *     description="This is a request to the service provider to Delete user",
     *     operationId="userDelete",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="user_id",
     *     in="path",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     )
     * )
     *
     */
    public function userDelete(){
        
        return $this->serve(UseDeleteFeature::class);
    }

    /**
     * Update the status of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {
        //Update the role with attributes
        return $this->serve(UpdateUserFeature::class);
    }

    public function forgotPassword()
    {
        return $this->serve(ForgotPasswordFeature::class);
    }

    public function isResetPasswordTokenValid()
    {
        return $this->serve(ResetRequestFeature::class);
    }

    public function resetPassword()
    {
        return $this->serve(ResetPasswordFeature::class);
    }

    public function createOrgnization()
    {
        return $this->serve(CreateOrgnizationFeature::class);
    }

    public function reforgotPassword()
    {
        return $this->serve(ForgotPasswordAgainFeature::class);
    }

    /**
     * Method to set the user info for a organization
     *
     * @return void
     */
    public function setUserInfo() {
        return $this->serve(SetUserInfoByOrganizationFeature::class);
    }

    public function getUserInfo()
    {
        return $this->serve(GetUserInfoByOrganizationFeature::class);
    }

    /**
     * The REST read request message for the updateProfile() API call.
     * @SWG\Put(
     *     path="user",
     *     tags={"User Api"},
     *     summary="The REST read request message for the updateProfile() API call.",
     *     description="This is a request to the service provider to update user profile",
     *     operationId="updateProfile",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="Body",
     *     in="body",
     *     type="object",
     *     ref="$/responses/updateProfile",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     )
     * )
     *
     */

    /**
     * @SWG\Response(
     *      response="updateProfile",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="first_name",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="last_name",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="reg_user_type",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="reg_teacher_grades",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="reg_teacher_subjects",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="reg_admin_languages",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="reg_admin_other_languages",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */
    public function updateProfile()
    {
        return $this->serve(UpdateProfileFeature::class);
    }

    /**
     * The REST read request message for the changePassword() API call.
     * @SWG\Put(
     *     path="user/password",
     *     tags={"User Api"},
     *     summary="The REST read request message for the changePassword() API call.",
     *     description="This is a request to the service provider to change password",
     *     operationId="changePassword",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="Body",
     *     in="body",
     *     type="object",
     *     ref="$/responses/changePassword",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     )
     * )
     *
     */
     
    /**
     * @SWG\Response(
     *      response="changePassword",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="currentPassword",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="newPassword",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="confirmNewPassword",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */
    public function changePassword()
    {
        return $this->serve(ChangePasswordFeature::class);
    }

    /**
     * The REST read request message for the getEditedProfile() API call.
     * @SWG\Get(
     *     path="user",
     *     tags={"User Api"},
     *     summary="The REST read request message for the getEditedProfile() API call.",
     *     description="This is a request to the service provider to edit user profile",
     *     operationId="getEditedProfile",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     )
     * )
     *
     */
    public function getEditedProfile()
    {
        return $this->serve(getEditedProfileFeature::class);
    }

    public function createUserViaCSV()
    {
        return $this->serve(createUserViaCSVFeature::class);
    }

    public function resendMail()
    {    
        return $this->serve(resendEmailFeature::class);
    }

    public function updateToken()
    {
        return $this->serve(UpdateRefreshFeature::class);
    }

    public function cisLogin(){
        return $this->serve(CISLoginFeature::class);
     }

     public function cisSetUser(){
        return $this->serve(CISCreateUserFeature::class);
     }

}
