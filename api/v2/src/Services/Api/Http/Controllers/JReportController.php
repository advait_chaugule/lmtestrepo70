<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\JReportFeature;
use App\Services\Api\Features\JReportShowFilterFeature;
use App\Services\Api\Features\ExportTaxonomyPdfFeature;
use App\Services\Api\Features\JasperDrillDownReportFeature;
use App\Services\Api\Features\ShowJasperDrillDownReportFeature;
use App\Services\Api\Features\ShowJasperDrillDownSubReportFeature;
use App\Services\Api\Features\StatisticsFeature;
use App\Services\Api\Features\CustomViewFeature;
use App\Services\Api\Features\ExportCustomViewFeature;
use App\Services\Api\Features\ListJasperReportFeature;
use App\Services\Api\Features\ViewJasperReportFeature;
use App\Services\Api\Features\CoverageReportFeature;

class JReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        
        return $this->serve(JReportShowFilterFeature::class);

    }    
    public function show()
    {
       return $this->serve(JReportFeature::class);      

    } 

    public function exportTaxonomyPdf()
    {
       return $this->serve(ExportTaxonomyPdfFeature::class);  
    }

    public function JasperDrillDownReport()
    {
       return $this->serve(JasperDrillDownReportFeature::class);  
    }

    public function DrillDownShow()
    {      
      return $this->serve(ShowJasperDrillDownReportFeature::class);  
    }
    public function DrillDownSubShow()
    {      
      return $this->serve(ShowJasperDrillDownSubReportFeature::class);  
    }
    public function statistics()
    {      
      return $this->serve(StatisticsFeature::class);  
    }
    
    public function custom_view()
    {      
      return $this->serve(CustomViewFeature::class);  
    }

    public function exportCustomView()
    {      
      return $this->serve(ExportCustomViewFeature::class);  
    }

    public function listJasperReport()
    {      
      return $this->serve(ListJasperReportFeature::class);  
    }

    public function viewJasperReport()
    {
      return $this->serve(ViewJasperReportFeature::class);
    }

    public function coverageReport()
    {
      return $this->serve(CoverageReportFeature::class);
    }

}