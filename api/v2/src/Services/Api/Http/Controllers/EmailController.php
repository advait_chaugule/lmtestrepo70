<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\SendEmailFeature;

class EmailController extends Controller
{
    public function sendEmail() {
        
        return $this->serve(SendEmailFeature::class);
    }
}
