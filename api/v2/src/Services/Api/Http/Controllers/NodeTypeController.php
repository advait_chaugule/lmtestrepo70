<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

//Features for NodeType
use App\Services\Api\Features\ListNodeTypeFeature;
use App\Services\Api\Features\CreateNodeTypeFeature;
use App\Services\Api\Features\UpdateNodeTypeFeature;
use App\Services\Api\Features\DeleteNodeTypeFeature;

//Features for NodeTypeMetadata
use App\Services\Api\Features\ListNodeTypeMetadataFeature;
use App\Services\Api\Features\SaveNodeTypeMetadataFeature;
use App\Services\Api\Features\GetNodeTypeFeature;
use App\Services\Api\Features\DuplicateNodeTypeFeature;
use App\Services\Api\Features\GetNodeTypesFromCacheFeature;

class NodeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //List of NodeType
        return $this->serve(ListNodeTypeFeature::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Create a nodetype 
        return $this->serve(CreateNodeTypeFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Show One Nodetype
        return $this->serve(GetNodeTypeFeature::class);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Update the NodeType
        return $this->serve(UpdateNodeTypeFeature::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete the NodeType
        return $this->serve(DeleteNodeTypeFeature::class);
    }

    /**
     * Display a listing of the resource of node type metadata.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNodeTypeMetadata()
    {
        return $this->serve(ListNodeTypeMetadataFeature::class);
    }

    /**
     * Saves metadata for a node type
     *
     * @return \Illuminate\Http\Response
     */
    public function saveNodeTypeMetadata()
    {
        return $this->serve(SaveNodeTypeMetadataFeature::class);
    }

    /**
     * Duplicate a node type with _dup as suffix in name
     *
     * @return \Illuminate\Http\Response
     */
    public function duplicateNodetype()
    {
        return $this->serve(DuplicateNodeTypeFeature::class);
    }

    /**
     * Get a note from cache
     */
    public function getNodeTypesFromCache($id){
        return $this->serve(GetNodeTypesFromCacheFeature::class);
    }
}
