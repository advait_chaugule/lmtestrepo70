<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\GetListOfLanguagesFeature;

class LanguageController extends Controller
{
    public function getList()
    {
        return $this->serve(GetListOfLanguagesFeature::class);
    }
}
