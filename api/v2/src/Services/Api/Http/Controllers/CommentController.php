<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListCommentFeature;
use App\Services\Api\Features\ListArchiveCommentFeature;
use App\Services\Api\Features\CreateCommentFeature;
use App\Services\Api\Features\UpdateCommentFeature;
use App\Services\Api\Features\DeleteCommentFeature;
use App\Services\Api\Features\GetDocumentUserCommentsFeature;
use App\Services\Api\Features\GetProjectCommentsFeature;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //List the Item Comments
        return $this->serve(ListCommentFeature::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listArchive()
    {
        //List the Item Archived Comments
        return $this->serve(ListArchiveCommentFeature::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Create a new item comment
        return $this->serve(CreateCommentFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Update an item thread or item thread comment
        return $this->serve(UpdateCommentFeature::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete a item thread or item thread comment
        return $this->serve(DeleteCommentFeature::class);
    }

    /**
     * Method to get all user comments for self registered users
     * for a public review taxonomy
     *
     * @return void
     */
    public function getDocumentUserComments() {
        return $this->serve(GetDocumentUserCommentsFeature::class);
    }

    /**
     * Method to get all user comments for a taxonomy
     *
     * @return void
     */
    public function getProjectUserComments() {
        return $this->serve(GetProjectCommentsFeature::class);
    }


}
