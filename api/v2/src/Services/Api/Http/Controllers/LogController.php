<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\LogDataDisplayFeature;
use App\Services\Api\Features\FetchLogDataFeature;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Read the logs from s3 and display
        return $this->serve(LogDataDisplayFeature::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchLogData()
    {
        //
        return $this->serve(FetchLogDataFeature::class);
    }

    
}
