<?php
namespace App\Services\Api\Http\Controllers;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\GetSystemSettingFeature;
use App\Services\Api\Features\SetSystemSettingFeature;
use App\Services\Api\Features\SetDefaultSettingOrgFeature;
class SystemSettingController extends Controller
{
    public function getSystemSetting()
    {
        return $this->serve(GetSystemSettingFeature::class);
    }

    public function setSystemSetting()
    {
        return $this->serve(SetSystemSettingFeature::class);
    }

    public function setDefaultSettingOrg()
    {
        return $this->serve(SetDefaultSettingOrgFeature::class);
    }
}
