<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\CreateTenantFeature;
use App\Services\Api\Features\GetOrganizationDetailsFeature;
use App\Services\Api\Features\ListOrganizationFeature;
use App\Services\Api\Features\GetOrganizationByIdFeature;
use App\Services\Api\Features\UpdateOrganizationByIdFeature;

class TenentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->serve(CreateTenantFeature::class);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getOrgDetails()
    {
        return $this->serve(GetOrganizationDetailsFeature::class);
    }

    public function orgList()
    {
        return $this->serve(ListOrganizationFeature::class);
    }

    public function editOrg()
    {
        return $this->serve(GetOrganizationByIdFeature::class);
    }

    public function updateOrg()
    {
        return $this->serve(UpdateOrganizationByIdFeature::class);
    }

    public function serverDetails()
    {
        echo "<pre>";
        print_r($_SERVER);
        print_r($_SESSION);
        echo "</pre>";
    }
}
