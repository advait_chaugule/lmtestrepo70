<?php
namespace App\Services\Api\Http\Controllers;

use App\Services\Api\Features\GetCountUnreadNotifications;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\CreateNotificationFeature;
use App\Services\Api\Features\GetNotificationFeature;
class NotificationController extends Controller
{
    public function index()
    {

    }
    //Create Notification
    public function create()
    {
     return $this->serve(CreateNotificationFeature::class);
    }

    public function getCount()
    {
        return $this->serve(GetCountUnreadNotifications::class);
    }

    public function show()
    {
        return $this->serve(GetNotificationFeature::class);
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }
}
