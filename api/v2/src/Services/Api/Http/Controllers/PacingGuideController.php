<?php

namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\PublishPacingGuideFeature;
use App\Services\Api\Features\PreviewPacingGuideFeature;

class PacingGuideController extends Controller
{
    public function publish() {
        return $this->serve(PublishPacingGuideFeature::class);
    }

    public function preview() {
        return $this->serve(PreviewPacingGuideFeature::class);
    }
}
