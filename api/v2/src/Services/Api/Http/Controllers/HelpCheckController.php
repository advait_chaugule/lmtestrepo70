<?php
namespace App\Services\Api\Http\Controllers;

use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\HelpCheckFeature;
class HelpCheckController extends Controller
{
    public function helpCheckInfo(){
        return $this->serve(HelpCheckFeature::class);
    }
}