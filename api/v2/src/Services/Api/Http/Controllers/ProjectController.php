<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\GetProjectFeature;
use App\Services\Api\Features\ProjectCreateFeature;
use App\Services\Api\Features\GetProjectByIdFeature;
use App\Services\Api\Features\ProjectItemMappingFeature;
use App\Services\Api\Features\ProjectEditFeature;
use App\Services\Api\Features\ProjectDeleteFeature;
use App\Services\Api\Features\ListProjectFeature;
use App\Services\Api\Features\SearchProjectFeature;
use App\Services\Api\Features\MappedNodesToProjectNewFeature;
use App\Services\Api\Features\AssociatedNodesFeature;
use App\Services\Api\Features\DeleteAssociatedNodesFeature;
use App\Services\Api\Features\ShowUsersAssociatedFeature;
use App\Services\Api\Features\UpdateProjectWorflowStageFeature;
use App\Services\Api\Features\AssignProjectUserFeature;
use App\Services\Api\Features\UnAssignProjectUserFeature;
use App\Services\Api\Features\LogProjectSessionActivityFeature;
use App\Services\Api\Features\CreateProjectAccessRequestFeature;
use App\Services\Api\Features\GetAllProjectAccessRequestFeature;
use App\Services\Api\Features\UpdateProjectAccessRequestFeature;
use App\Services\Api\Features\ListProjectCommentFeature;
use App\Services\Api\Features\AuthoredNodesForPacingGuideFeature;
//Features to be used for Cache 
use App\Services\Api\Features\GetProjectFromCacheFeature;


use App\Services\Api\Features\AuthoredNodesFeature;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //List all projects
        return $this->serve(GetProjectFeature::class);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->serve(ProjectCreateFeature::class);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //fetch project details        
        return $this->serve(GetProjectByIdFeature::class);
        
    }
    
    public function mapTaxonomies(){
        //update project taxanomy mapping
        return $this->serve(ProjectItemMappingFeature::class);
    }

    public function update() {
        return $this->serve(ProjectEditFeature::class);
    }

    public function delete() {
        return $this->serve(ProjectDeleteFeature::class);
    }

    public function showAll() {
        return $this->serve(ListProjectFeature::class);
    }

    /**
     * Method for Whole Taxonomy and Associated nodes.
     *
     * @return \Illuminate\Http\Response
     */
    public function mappedNodesNew()
    {
        //List whole taxonomy with associated nodes
        return $this->serve(MappedNodesToProjectNewFeature::class);
        
    }

    /**
     * Method for only associated nodes for a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function associatedNodes()
    {
        //List all associated nodes for a project
        return $this->serve(AssociatedNodesFeature::class);
        
    }

    /**
     * Method for only authored nodes for a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function authoredNodes()
    {
        //List all authored nodes for a project
        return $this->serve(AuthoredNodesFeature::class);
        
        
    }

    /**
     * Method for delete associated nodes for a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteAssociatedNodes()
    {
        //Delete associated nodes for a project
        return $this->serve(DeleteAssociatedNodesFeature::class);
        
    }

    /* List of users associated to the project.
     *
     * @return \Illuminate\Http\Response
     */
    public function showUsers()
    {
        //List all projects
        return $this->serve(ShowUsersAssociatedFeature::class);
        
    }

    public function UpdateProjectWorflowStage()
    {
        //List all projects
        return $this->serve(UpdateProjectWorflowStageFeature::class);
        
    }
    
    /**
     * Assign Multi Users to a project stage role.
     *
     * @return \Illuminate\Http\Response
     */
    public function AssignProjectUser(){
    return $this->serve(AssignProjectUserFeature::class);
    }

    /**
     * UnAssign Users from a project stage role.
     *
     * @return \Illuminate\Http\Response
     */
    public function UnAssignProjectUser(){
        return $this->serve(UnAssignProjectUserFeature::class);
    }

    public function logSessionActivity() {
        return $this->serve(LogProjectSessionActivityFeature::class);
    }

    public function createProjectAccessRequest() {
        return $this->serve(CreateProjectAccessRequestFeature::class);
    }

    public function getAllProjectAccessRequests() {
        return $this->serve(GetAllProjectAccessRequestFeature::class);
    }

    public function updateProjectAccessRequest()
    {
        return $this->serve(UpdateProjectAccessRequestFeature::class);
    }

    public function listComments()
    {
        return $this->serve(ListProjectCommentFeature::class);
    }

    /**
     * Method for only pacing guide nodes.
     *
     * @return \Illuminate\Http\Response
     */
    public function pacingNodes()
    {
        //List all authored nodes for a project
        return $this->serve(AuthoredNodesForPacingGuideFeature::class);
        
    }

    /**
     * Method to get the Project hierarchy
     * from Cache
     *
     * @return void
     */
    public function getProjectHierarchy() {
        return $this->serve(GetProjectFromCacheFeature::class);
    }

    /**
     * Method to get the Project details
     * from Cache
     *
     * @return void
     */
    public function getProjectDetails() {
        return $this->serve(GetProjectFromCacheFeature::class);
    }
    
}
