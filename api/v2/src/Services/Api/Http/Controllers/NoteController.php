<?php
namespace App\Services\Api\Http\Controllers;

use App\Services\Api\Features\GetAllNoteFromCacheFeature;
use App\Services\Api\Features\GetCreatedNoteFromCacheFeature;
use App\Services\Api\Features\GetDeletedNoteFromCacheFeature;
use App\Services\Api\Features\GetNoteFromCacheFeature;
use App\Services\Api\Features\GetUpdatedNoteFromCacheFeature;
use App\Services\API\Features\ReorderNotesFeature;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

//Features for Note
use App\Services\Api\Features\GetAllNoteFeature;
use App\Services\Api\Features\CreateNoteFeature;
use App\Services\Api\Features\GetNoteFeature;
use App\Services\Api\Features\UpdateNoteFeature;
use App\Services\Api\Features\DeleteNoteFeature;

class NoteController extends Controller
{
    /**
     * Get all note
     */
    public function index()
    {
        return $this->serve(GetAllNoteFeature::class);
    }

    /**
     * Create a note
     */
    public function create()
    {
        return $this->serve(CreateNoteFeature::class);
    }

    /**
     * Display a note
     */
    public function show($id)
    {
        return $this->serve(GetNoteFeature::class);
    }
    
    /**
     * Edit a note
     */
    public function edit($id)
    {
        return $this->serve(UpdateNoteFeature::class);
    }

    /**
     * Update a note
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Destroy a note
     */
    public function destroy($id)
    {
        return $this->serve(DeleteNoteFeature::class);
    }

    /**
     * Reorder notes
     */
    public function reorder()
    {
        return $this->serve(ReorderNotesFeature::class);
    }

    /**
     * Get created Note from cache
     */
    public function getCreatedNoteFromCache(){
        return $this->serve(GetCreatedNoteFromCacheFeature::class);
    }
    /**
     * Get updated Note from cache
     */
    public function getUpdatedNoteFromCache(){
        return $this->serve(GetUpdatedNoteFromCacheFeature::class);
    }

    /**
     * Remove deleted Note from cache
     */
    public function getDeletedNoteFromCache(){
        return $this->serve(GetDeletedNoteFromCacheFeature::class);
     }

    /**
     * Get all Note from cache
     */
    public  function getAllNoteFromCache(){
        return $this->serve(GetAllNoteFromCacheFeature::class);
    }

    /**
     * Get a note from cache
     */
    public function getNoteFromCache($id){
        return $this->serve(GetNoteFromCacheFeature::class);
    }

}