<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListCaseAssociationsFeature;
use App\Services\Api\Features\CreateCaseAssociationFeature;
use App\Services\Api\Features\DeleteCaseAssociationFeature;
use App\Services\Api\Features\EditCaseAssociationFeature;
use App\Services\Api\Features\CreateExemplarAssociationFeature;
use App\Services\Api\Features\DeleteExemplarAssociationFeature;

class CaseAssociationController extends Controller
{

    public function listCaseAssociationTypes() {
        return $this->serve(ListCaseAssociationsFeature::class);
    }

    public function createAssociation() {
        return $this->serve(CreateCaseAssociationFeature::class);
    }

    public function deleteAssociation() {
        return $this->serve(DeleteCaseAssociationFeature::class);
    }

    public function updateAssociation() {
        return $this->serve(EditCaseAssociationFeature::class);
    }

    public function createExemplarAssociation() {
        return $this->serve(CreateExemplarAssociationFeature::class);
    }

    public function deleteExemplarAssociation() {
        return $this->serve(DeleteExemplarAssociationFeature::class);
    }

}
