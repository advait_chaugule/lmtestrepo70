<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

class DataMockupController extends Controller
{
    public function metadataShowAll(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Country',
                'field_type' => 3,
                'field_possible_values' => 'India,Australia,Russia',
                'is_custom' => 0,
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
                'updated_by' => 'John Smith',
                'usage_count' => 100,
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function metadataShow(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Country',
                'field_type' => 3,
                'field_possible_values' => 'India,Australia,Russia',
                'is_custom' => 0,
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
                'updated_by' => 'John Smith',
            ],
            'message' => 'Data found.',
        ]);
    }

    public function metadataCreate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Country',
                'field_type' => 3,
                'field_possible_values' => 'India,Australia,Russia',
                'is_custom' => 0,
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data created successfully.',
        ]);
    }

    public function metadataUpdate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Country',
                'field_type' => 3,
                'field_possible_values' => 'India,Australia,Russia',
                'is_custom' => 0,
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data updated successfully.',
        ]);
    }

    public function metadataDelete(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Country',
                'field_type' => 3,
                'field_possible_values' => 'India,Australia,Russia',
                'is_custom' => 0,
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data deleted successfully.',
        ]);
    }

    public function nodeTypeShowAll(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'abc',
                'updated_at' => '2018-06-19T05:56:00+00:00',
                'updated_by' => 'John Smith',
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function nodeTypeShow(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'abc',
                'updated_at' => '2018-06-19T05:56:00+00:00',
                'updated_by' => 'John Smith',
            ],
            'message' => 'Data found.',
        ]);
    }

    public function nodeTypeCreate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'abc',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data created successfully.',
        ]);
    }

    public function nodeTypeUpdate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'abc',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data updated successfully.',
        ]);
    }

    public function nodeTypeDelete(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'abc',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data deleted successfully.',
        ]);
    }

    public function nodeTypeMetadata(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Full Statement',
                'field_type' => 1,
                'field_possible_values' => '',
                'is_custom' => 0,
                'internal_name' => 'full_statement',
            ], [
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7482',
                'name' => 'Country',
                'field_type' => 3,
                'field_possible_values' => 'India,Australia,Russia',
                'is_custom' => 1,
                'internal_name' => '',
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function nodeTypeMetadataSave(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ], [
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7482',
            ]],
            'message' => 'Data saved successfully.',
        ]);
    }

    public function itemMetadata(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'metadata_value' => 'abc',
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function itemMetadataSave(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'metadata_value' => 'abc',
            ]],
            'message' => 'Data saved successfully.',
        ]);
    }

    public function documentMetadata(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'metadata_value' => 'abc',
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function documentMetadataSave(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'metadata_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'metadata_value' => 'abc',
            ]],
            'message' => 'Data saved successfully.',
        ]);
    }

    public function itemComments(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'item_thread_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'assign_to' => 'Andrew Simpson',
                'status' => 1,
                'comments' => [[
                    'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7482',
                    'parent_id' => '',
                    'comment' => 'Thread 1 Comment 1',
                    'created_by' => 'John Smith',
                    'updated_at' => '2018-06-19T05:56:00+00:00',
                ], [
                    'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7483',
                    'parent_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7482',
                    'comment' => 'Comment 1 reply',
                    'created_by' => 'Andrew Simpson',
                    'updated_at' => '2018-06-19T05:56:00+00:00',
                ], [
                    'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7484',
                    'parent_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7483',
                    'comment' => 'Reply of reply',
                    'created_by' => 'John Smith',
                    'updated_at' => '2018-06-19T05:56:00+00:00',
                ]],
            ], [
                'item_thread_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7471',
                'assign_to' => 'Andrew Simpson',
                'status' => 1,
                'comments' => [[
                    'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7472',
                    'parent_id' => '',
                    'comment' => 'Thread 2 Comment 1',
                    'created_by' => 'John Smith',
                    'updated_at' => '2018-06-19T05:56:00+00:00',
                ], [
                    'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7473',
                    'parent_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7472',
                    'comment' => 'Comment 1 reply',
                    'created_by' => 'Andrew Simpson',
                    'updated_at' => '2018-06-19T05:56:00+00:00',
                ]],
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function itemCommentCreate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7675',
                'parent_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7480',
                'comment' => 'This is a comment text',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data created successfully.',
        ]);
    }

    public function itemCommentUpdate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7675',
                'parent_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7480',
                'comment' => 'This is an updated comment text',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data updated successfully.',
        ]);
    }

    public function itemCommentDelete(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'item_thread_comment_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7675',
                'parent_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7480',
                'comment' => 'This is an updated comment text',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Data deleted successfully.',
        ]);
    }

    public function getAllNodeTemplates(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [[
                'node_template_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Node Template 1',
                'usage_count' => 15,
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
                'updated_by' => 'John Smith',
            ]],
            'message' => 'Data found.',
        ]);
    }

    public function getNodeTemplate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_template_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'Node Template 1',
                'is_active' => 1,
                'updated_at' => '2018-06-19T05:56:00+00:00',
                'updated_by' => 'John Smith',
                'node_types' => [[
                    'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7786',
                    'title' => 'Document',
                    'parent_node_type_id' => '',
                    'sort_order' => 1,
                ], [
                    'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7787',
                    'title' => 'Subject',
                    'parent_node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7786',
                    'sort_order' => 2,
                ], [
                    'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7788',
                    'title' => 'Grade',
                    'parent_node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7787',
                    'sort_order' => 3,
                ], [
                    'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7789',
                    'title' => 'Strand',
                    'parent_node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7788',
                    'sort_order' => 4,
                ]],
            ],
            'message' => 'Data found.',
        ]);
    }

    public function createNodeTemplate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_template_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data created successfully.',
        ]);
    }

    public function updateNodeTemplate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_template_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data updated successfully.',
        ]);
    }

    public function deleteNodeTemplate(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_template_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data deleted successfully.',
        ]);
    }

    public function generateTaxonomy(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'document_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data created successfully.',
        ]);
    }

    public function getAllProjectAccessRequests(Request $request)
    {
        if ($request->status == 1) {
            return json_encode([
                'status' => 200,
                'data' => [[
                    'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                    'project_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7482',
                    'project_name' => 'Project 1',
                    'user_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7483',
                    'user_name' => 'User 1',
                    'user_email' => 'test@test.com',
                    'workflow_stage_role_ids' => 'd5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2,d5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2',
                    'workflow_stage_roles' => 'Author, Reviewer',
                    'status' => 1,
                    'status_text' => 'pending',
                    'comment' => '',
                    'date_of_application' => '2018-06-19T05:56:00+00:00',
                    'date_of_last_status_change' => '',
                    'updated_by' => '',
                    'updated_by_name' => '',
                ]],
                'message' => 'Data found.',
            ]);
        } elseif ($request->status == 2) {
            return json_encode([
                'status' => 200,
                'data' => [[
                    'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7485',
                    'project_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7486',
                    'project_name' => 'Project 2',
                    'user_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7487',
                    'user_name' => 'User 2',
                    'user_email' => 'test2@test.com',
                    'workflow_stage_role_ids' => 'd5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2,d5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2',
                    'workflow_stage_roles' => 'Author, Reviewer',
                    'status' => 2,
                    'status_text' => 'accepted',
                    'comment' => '',
                    'date_of_application' => '2018-06-19T05:56:00+00:00',
                    'date_of_last_status_change' => '2018-06-19T05:56:00+00:00',
                    'updated_by' => 'e685d64b-21d3-41da-8dcc-8e4128fe7488',
                    'updated_by_name' => 'John Smith',
                ]],
                'message' => 'Data found.',
            ]);
        } elseif ($request->status == 3) {
            return json_encode([
                'status' => 200,
                'data' => [[
                    'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7475',
                    'project_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7476',
                    'project_name' => 'Project 3',
                    'user_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7477',
                    'user_name' => 'User 3',
                    'user_email' => 'test3@test.com',
                    'workflow_stage_role_ids' => 'd5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2,d5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2',
                    'workflow_stage_roles' => 'Author, Reviewer',
                    'status' => 3,
                    'status_text' => 'rejected',
                    'comment' => 'This is a reason for rejection',
                    'date_of_application' => '2018-06-19T05:56:00+00:00',
                    'date_of_last_status_change' => '2018-06-19T05:56:00+00:00',
                    'updated_by' => 'e685d64b-21d3-41da-8dcc-8e4128fe7478',
                    'updated_by_name' => 'John Smith',
                ]],
                'message' => 'Data found.',
            ]);
        }
    }

    public function getProjectAccessRequest(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'project_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7482',
                'user_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7483',
                'workflow_stage_roles' => [[
                    'id' => 'd5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2',
                    'name' => 'Author',
                ], [
                    'id' => 'd5484194-ecad-44fe-b271-e385b89bc5ef||12ac29e8-fd2a-4158-9db4-4865f36ee1fa||8d69a1a1-a82d-4550-8a32-4c67d08349d2',
                    'name' => 'Reviewer',
                ]],
                'status' => 1,
                'status_text' => 'pending',
                'comment' => '',
                'date_of_application' => '2018-06-19T05:56:00+00:00',
                'date_of_last_status_change' => '',
                'updated_by' => '',
            ],
            'message' => 'Data found.',
        ]);
    }

    public function createProjectAccessRequest(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data created successfully.',
        ]);
    }

    public function updateProjectAccessRequest(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data updated successfully.',
        ]);
    }

    public function deleteProjectAccessRequest(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'project_access_request_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
            ],
            'message' => 'Data deleted successfully.',
        ]);
    }

    public function duplicateNodeType(Request $request)
    {
        return json_encode([
            'status' => 200,
            'data' => [
                'node_type_id' => 'e685d64b-21d3-41da-8dcc-8e4128fe7481',
                'name' => 'abc',
                'updated_at' => '2018-06-19T05:56:00+00:00',
            ],
            'message' => 'Metadata set is duplicated successfully.',
        ]);
    }
}
