<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\DeleteDuplicateAssociationFeature;

class MigrationApiController extends Controller
{
    #to run both sequence no migration as API
    public function DeleteDuplicateAssociationMigration(Request $request)
    {
        return $this->serve(DeleteDuplicateAssociationFeature::class);
    }
}