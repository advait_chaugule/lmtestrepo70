<?php
namespace App\Services\Api\Http\Controllers;

use App\Services\Api\Features\GetAuthPublishDocumentFromCacheFeature;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListTaxonomiesFeature;
use App\Services\Api\Features\RearrangeTaxonomyFeature;
use App\Services\Api\Features\UpdateAssignProjectUserFeature;
use App\Services\Api\Features\CreateAndUploadTaxonomySearchDataFeature;
use App\Services\Api\Features\SearchTaxonomyFeature;
use App\Services\Api\Features\ListUnmappedTaxonomyFeature;
use App\Services\Api\Features\ExportTaxonomyFeature;
use App\Services\Api\Features\TaxonomyNodeTypeFeature;
use App\Services\Api\Features\GetGroupForTaxonomyFeature;
use App\Services\Api\Features\UpdateGroupForTaxonomyFeature;
use App\Services\Api\Features\CreateGroupForTaxonomyFeature;
use App\Services\Api\Features\DeleteGroupForTaxonomyFeature;
use App\Services\Api\Features\AddTaxonomyToGroupFeature;
use App\Services\Api\Features\DeleteTaxonomyFromGroupFeature;
use App\Services\Api\Features\GetTaxonomyFromGroupFeature;
use App\Services\Api\Features\DownloadTaxonomyFeature;

class TaxonomyController extends Controller
{
    public function list() {
        return $this->serve(ListTaxonomiesFeature::class);
    }

    public function rearrange() {
        return $this->serve(RearrangeTaxonomyFeature::class);
    }

    public function createAndUploadTaxonomySearchData()
    {
        return $this->serve(CreateAndUploadTaxonomySearchDataFeature::class);
    }

    public function search()
    {
        return $this->serve(SearchTaxonomyFeature::class);
    }

    public function getUnmappedTaxonomy()
    {
        return $this->serve(ListUnmappedTaxonomyFeature::class);
    }
    public function getAuthPublishDocumentFromCache(){
        return $this->serve(GetAuthPublishDocumentFromCacheFeature::class);
    }
    public function editAssignProjectUser()
    {
       return $this->serve(UpdateAssignProjectUserFeature::class);
    }
    public function exportTaxonomy()
    {
        return $this->serve(ExportTaxonomyFeature::class);
    }

    public function getNodeDetails()
    {
        return $this->serve(TaxonomyNodeTypeFeature::class);
    }

    public function getGroupForTaxonomyList()
    {
        return $this->serve(GetGroupForTaxonomyFeature::class);
    }
    
    public function getGroupForTaxonomy()
    {
        return $this->serve(GetGroupForTaxonomyFeature::class);
    }
    
    public function updateGroupForTaxonomy()
    {
        return $this->serve(UpdateGroupForTaxonomyFeature::class);
    }
    
    public function createGroupForTaxonomy()
    {
        return $this->serve(CreateGroupForTaxonomyFeature::class);
    }
    
    public function deleteGroupForTaxonomy()
    {
        return $this->serve(DeleteGroupForTaxonomyFeature::class);
    }
    
    public function addTaxonomyToGroup()
    {
        return $this->serve(AddTaxonomyToGroupFeature::class);
    }
    
    public function deleteTaxonomyFromGroup()
    {
        return $this->serve(DeleteTaxonomyFromGroupFeature::class);
    }
    
    public function getTaxonomyFromGroup()
    {
        return $this->serve(GetTaxonomyFromGroupFeature::class);
    }

    public function downloadTaxonomy()
    {
        return $this->serve(DownloadTaxonomyFeature::class);
    }
}
