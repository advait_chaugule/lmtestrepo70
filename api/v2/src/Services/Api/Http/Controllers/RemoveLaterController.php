<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\RemoveLaterFeature;
use App\Services\Api\Features\CreateAndUploadProjectSearchDataFeature;

class RemoveLaterController extends Controller
{
    public function removeLater() {
        return $this->serve(RemoveLaterFeature::class);
    }
    
    public function testDeltaUpdateCloudSearchFeature() {
        return $this->serve(CreateAndUploadProjectSearchDataFeature::class);
    }
}
