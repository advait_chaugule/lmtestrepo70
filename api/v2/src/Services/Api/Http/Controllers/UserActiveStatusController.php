<?php
namespace App\Services\Api\Http\Controllers;


use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\CheckUserActiveStatusFeature;
class UserActiveStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkActiveStatus()
    {
        return $this->serve(CheckUserActiveStatusFeature::class);
    }
}