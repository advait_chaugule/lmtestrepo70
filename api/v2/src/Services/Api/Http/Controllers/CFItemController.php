<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\CreateCFItemFeature;
use App\Services\Api\Features\FetchCFItemFeature;
use App\Services\Api\Features\EditCFItemFeature;
use App\Services\Api\Features\DeleteItemFeature;


use App\Services\Api\Features\CreateItemAdditionalMetadataFeature;
use App\Services\Api\Features\UpdateItemAdditionalMetadataFeature;
use App\Services\Api\Features\DestroyItemAdditionalMetadataFeature;
use App\Services\Api\Features\CreateItemAssociationFeature;
class CFItemController extends Controller
{

    public function create()
    {
        return $this->serve(CreateCFItemFeature::class);
    }

    public function fetch() {
        return $this->serve(FetchCFItemFeature::class);
    }
    /**
     * The REST read request message for the update() API call.
     * @SWG\Post(
     *     path="cfitem/edit/{item_id}",
     *     tags={"Cfitem-edit"},
     *     summary="The REST read request message for the update() API call.",
     *     description="This is a request to the service provider to edit/update CFITEM",
     *     operationId="update",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="item_id",
     *     in="path",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *     )
     * )
     *
     */

    public function update() {
        return $this->serve(EditCFItemFeature::class);
    }

    public function delete() {
        return $this->serve(DeleteItemFeature::class);
    }

    public function createAdditionalMetadata() {
        return $this->serve(CreateItemAdditionalMetadataFeature::class);
    }

    public function updateAdditionalMetadata() {
        return $this->serve(UpdateItemAdditionalMetadataFeature::class);
    }

    public function destroyAdditionalMetadata() {
        return $this->serve(DestroyItemAdditionalMetadataFeature::class);
    }

    public function createMultipleAssociation() {
        return $this->serve(CreateItemAssociationFeature::class);
    }
   
}
