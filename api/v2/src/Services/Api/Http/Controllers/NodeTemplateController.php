<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListNodeTemplateFeature;
use App\Services\Api\Features\CreateNodeTemplateFeature;
use App\Services\Api\Features\GetNodeTemplateFeature;
use App\Services\Api\Features\EditNodeTemplateFeature;
use App\Services\Api\Features\DeleteNodeTemplateFeature;

class NodeTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //List all NodeTemplates
        return $this->serve(ListNodeTemplateFeature::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Create new template
        return $this->serve(CreateNodeTemplateFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Show one node template and its related tables
        return $this->serve(GetNodeTemplateFeature::class);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit a nodeTemplate with change in associated nodetype
        return $this->serve(EditNodeTemplateFeature::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete a node template
        return $this->serve(DeleteNodeTemplateFeature::class);
    }
}
