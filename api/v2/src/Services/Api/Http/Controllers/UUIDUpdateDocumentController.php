<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\UUIDUpdateDocumentFeature;
use App\Services\Api\Features\cacheToS3MigrationFeature;

class UUIDUpdateDocumentController extends Controller
{
    #to run both sequence no migration as API
    public function UUIDMigration(Request $request)
    {
        return $this->serve(UUIDUpdateDocumentFeature::class);
    }

    public function cacheToS3Migration(Request $request)
    {
        return $this->serve(cacheToS3MigrationFeature::class);
    }
}