<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListDocumentsFeature;
use App\Services\Api\Features\CreateDocumentFeature;
use App\Services\Api\Features\GetDocumentByIdFeature;
use App\Services\Api\Features\UpdateDocumentByIdFeature;
use App\Services\Api\Features\SearchDocumentFeature;
use App\Services\Api\Features\DeleteDocumentFeature;
use App\Services\Api\Features\ListAssociatedProjectFeature;

use App\Services\Api\Features\CreateDocumentAdditionalMetadataFeature;
use App\Services\Api\Features\UpdateDocumentAdditionalMetadataFeature;
use App\Services\Api\Features\DestroyDocumentAdditionalMetadataFeature;

use App\Services\Api\Features\PublishDocumentFeature;

use App\Services\Api\Features\GetTaxonomyChildFeature;
use App\Services\Api\Features\PublishHistoryDocumentFeature;
class DocumentController extends Controller
{

    public function list() {
        return $this->serve(ListDocumentsFeature::class);
    }

    public function create() {
        return $this->serve(CreateDocumentFeature::class);
    }

    public function getbyid(){
        return $this->serve(GetDocumentByIdFeature::class);
    }

    public function update(){
        return $this->serve(UpdateDocumentByIdFeature::class);
    }

    public function search(){
        return $this->serve(SearchDocumentFeature::class);
    }

    public function checkProject(){
        return $this->serve(ListAssociatedProjectFeature::class);
    }

    public function destroy() {
        return $this->serve(DeleteDocumentFeature::class);
    }

    public function createAdditionalMetadata() {
        return $this->serve(CreateDocumentAdditionalMetadataFeature::class);
    }

    public function updateAdditionalMetadata() {
        return $this->serve(UpdateDocumentAdditionalMetadataFeature::class);
    }

    public function destroyAdditionalMetadata() {
        return $this->serve(DestroyDocumentAdditionalMetadataFeature::class);
    }

    public function publish() {
        return $this->serve(PublishDocumentFeature::class);
    }

    public function PublishHistory() {
        return $this->serve(PublishHistoryDocumentFeature::class);
    }

    public function geHierarchy() {
        return $this->serve(GetTaxonomyChildFeature::class);
    }
}
