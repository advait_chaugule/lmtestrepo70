<?php
namespace App\Services\Api\Http\Controllers;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\CreateSubscriptionFeature;
use App\Services\Api\Features\UpdateSubscriptionFeature;
use App\Services\Api\Features\DeleteSubscriptionFeature;
use App\Services\Api\Features\GetSubscribedTaxonomyVersionFeature;

class SubscribeController extends Controller
{
    /**
     * Create a Subscription
     */
    public function createSubscription()
    {
        return $this->serve(CreateSubscriptionFeature::class);
    }
    public function updateSubscription()
    {
        return $this->serve(UpdateSubscriptionFeature::class);
    }
    public function deleteSubscription()
    {
        return $this->serve(DeleteSubscriptionFeature::class);
    }
    public function getSubscribedTaxonomyVersion()
    {
        return $this->serve(GetSubscribedTaxonomyVersionFeature::class);
    }
}