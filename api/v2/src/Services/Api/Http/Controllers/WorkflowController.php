<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\GetAllWorkflowFeature;
use App\Services\Api\Features\GetWorkflowDetailsFeature;
use App\Services\Api\Features\UpdateWorkflowFeature;
use App\Services\Api\Features\GetAllWorkflowRolesFeature;
use App\Services\Api\Features\UpdateWorkflowStageFeature;
use App\Services\Api\Features\GetAllWorflowStagesFeature;
use App\Services\Api\Features\StageRoleDeleteFeature;
use App\Services\Api\Features\CreateWorkflowFeature;
use App\Services\Api\Features\AddStageFeature;
use App\Services\Api\Features\DeleteStageFeature;
use App\Services\Api\Features\EditWorkflowFeature;
use App\Services\Api\Features\DeleteWorkflowFeature;
use App\Services\Api\Features\DuplicateWorkflowFeature;

class WorkflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //list all workflow
        return $this->serve(GetAllWorkflowFeature::class);
        
    }

    /**
     * Display a details of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetWorkflow()
    {
        //list all workflow
        return $this->serve(GetWorkflowDetailsFeature::class);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->serve(CreateWorkflowFeature::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //update the workflow
        return $this->serve(UpdateWorkflowFeature::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * List the Cummulative role set for an workflow
     *
     * @return \Illuminate\Http\Response
     */
    public function GetAllWorkflowRoles()
    {
        //list all roles for a workflow
        return $this->serve(GetAllWorkflowRolesFeature::class);
    }
    
    public function UpdateWorkflowStage()
    {
        return $this->serve(UpdateWorkflowStageFeature::class);
    }

    public function GetAllWorflowStages()
    {
        return $this->serve(GetAllWorflowStagesFeature::class);
    }

    public function stageRoleDelete()
    {
        return $this->serve(StageRoleDeleteFeature::class);
    }

    public function stageAdd()
    {
        return $this->serve(AddStageFeature::class);
    }

    public function stageDelete()
    {
        return $this->serve(DeleteStageFeature::class);
    }

    public function editWorkflow()
    {
        return $this->serve(EditWorkflowFeature::class);
    }

    public function workflowDelete()
    {
        return $this->serve(DeleteWorkflowFeature::class);
    }

    public function duplicateWorkflow()
    {
        return $this->serve(DuplicateWorkflowFeature::class);
    }
    
}
