<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ExportCFPackadgeJsonFeature;
use App\Services\Api\Features\ImportCFPackadgeJsonFeature;
use App\Services\Api\Features\GetTaxonomyHierarchyFeature;
use App\Services\Api\Features\SetTaxonomyHierarchyFeature;
use App\Services\Api\Features\CreateTaxonomyFeature;
use App\Services\Api\Features\ImportCFPackadgeApiFeature;
use App\Services\Api\Features\SetForPublicReviewFeature;
use App\Services\Api\Features\GetProjectStatusToPublishFeature;
use App\Services\Api\Features\SetForPublishFeature;
use App\Services\Api\Features\SetPublishDocumentToCacheFeature;
use App\Services\Api\Features\GetPublishDocumentFromCacheFeature;
use App\Services\Api\Features\PublicReviewHistoryFeature;
use App\Services\Api\Features\SetDocumentListToCacheFeature;
use App\Services\Api\Features\GetDocumentListFromCacheFeature;
use App\Services\Api\Features\StopPublicReviewFeature;
use App\Services\Api\Features\UpdatedImportCFPackadgeJsonFeature;
use App\Services\Api\Features\ImportTaxonomyFromCSVFeature;
use App\Services\Api\Features\GetimportStatusFromImportJobFeature;
use App\Services\Api\Features\GetDocumentDownloadFileFeature;
use App\Services\Api\Features\CopyNodesFeature;
use App\Services\Api\Features\ImportFromMultipleCsvFeature;
use App\Services\Api\Features\CancelCsvBatchFeature;

//version 2 api
use App\Services\Api\Features\GetTaxonomyHierarchyV2Feature;
use App\Services\Api\Features\GetTaxonomyDetailsV2Feature;
use App\Services\Api\Features\GetProjectHierarchyV2Feature;
use App\Services\Api\Features\GetProjectDetailsV2Feature;
use App\Services\Api\Features\SetTaxonomyHierarchyV2Feature;
use App\Services\Api\Features\MappedNodesNewV2Feature;

//version 3 api
use App\Services\Api\Features\GetTaxonomyHierarchyV3Feature;
use App\Services\Api\Features\GetTaxonomyHierarchyV4Feature;
use App\Services\Api\Features\GetTaxonomyHierarchyV5Feature;
use Illuminate\Support\Facades\DB;
use App\Services\Api\Features\CallBackFromNodejsForCsvFeature;

class CFPackadgeController extends Controller
{
    /**
     * Export to Competency Framework Packadge Json.
     * @return \Illuminate\Http\Response
     */
    public function exportToJson()
    {
        return $this->serve(ExportCFPackadgeJsonFeature::class);
    }

    /**
     * Import Competency Framework Packadge Json.
     * @return \Illuminate\Http\Response
     */
    public function importFromJson()
    {
        return $this->serve(ImportCFPackadgeJsonFeature::class);
    }

    /**
     * Import Competency Framework Packadge Json.
     * @return \Illuminate\Http\Response
     */
    public function importTaxonomyFromJson()
    {
        return $this->serve(UpdatedImportCFPackadgeJsonFeature::class);
    }

    /**
     * Import Competency Framework Packadge from CSV.
     * @return \Illuminate\Http\Response
     */
    public function importFromCsv() {
        return $this->serve(ImportTaxonomyFromCSVFeature::class);
    }

    /**
     * Get Case Package tree data to be 
     */
    public function getTreeHierarchy()
    {
        return $this->serve(GetTaxonomyHierarchyFeature::class);
    }

    /**
     * Set nodes ordering in a tree data to be ordered for projects
     */
    public function setTreeHierarchy()
    {
        return $this->serve(SetTaxonomyHierarchyFeature::class);
    }

    /**
     * Set nodes ordering in a tree data to be orderedin a pacing guide
     */
    public function setPacingTreeHierarchy()
    {
        return $this->serve(SetTaxonomyHierarchyFeature::class);
    }

    public function createTaxonomy(){
        return $this->serve(CreateTaxonomyFeature::class);
    }

    public function importFromApi()
    {
        return $this->serve(ImportCFPackadgeApiFeature::class);
    }

    /**
     * Set taxonomy status to Public Review
     */
    public function setForPublicReview()
    {
        return $this->serve(SetForPublicReviewFeature::class);
    }

    /**
     * Get the Associated Project Status With Comments
     *
     * @return void
     */
    public function getProjectStatusToPublish() {
        return $this->serve(GetProjectStatusToPublishFeature::class);
    }

    /**
     * Set the taxonomy for publish
     *
     * @return void
     */
    public function setForPublish() {
        return $this->serve(SetForPublishFeature::class);
    }

    /**
     * Set the taxonomy data to cache
     *
     * @return void
     */
    public function setPublishDocumentToCache() {
        return $this->serve(SetPublishDocumentToCacheFeature::class);
    }

    /**
     * Get the taxonomy data from cache
     *
     * @return void
     */
    public function getPublishDocument() {
        return $this->serve(GetPublishDocumentFromCacheFeature::class);
    }

    public function publicReviewHistory()
    {
        return $this->serve(PublicReviewHistoryFeature::class);
    }
    
    public function getTaxonomyHierarchy() {
        return $this->serve(GetPublishDocumentFromCacheFeature::class);
    }

    public function getTaxonomyDetails() {
        return $this->serve(GetPublishDocumentFromCacheFeature::class);
    }

    public function setPublishDocumentListToCache() {
        return $this->serve(SetDocumentListToCacheFeature::class);
    }

    public function getTaxonomyList()
    {
        return $this->serve(GetDocumentListFromCacheFeature::class);
    }
    public function stopPublicReview()
    {
        return $this->serve(StopPublicReviewFeature::class);
    }
    public function getTreeHierarchyV2()
    {
        return $this->serve(GetTaxonomyHierarchyV2Feature::class);
    }
    public function getTreeDetailsV2()
    {
        return $this->serve(GetTaxonomyDetailsV2Feature::class);
    }
    public function getProjectTreeHierarchyV2()
    {
        return $this->serve(GetProjectHierarchyV2Feature::class);
    }
    public function getProjectTreeDetailsV2()
    {
        return $this->serve(GetProjectDetailsV2Feature::class);
    }
    public function setTreeHierarchyV2()
    {
        return $this->serve(SetTaxonomyHierarchyV2Feature::class);
    }
    public function mappedNodesNewV2()
    {
        return $this->serve(MappedNodesNewV2Feature::class);
    }

    public function getimportStatusFromImportJob()
    {
        return $this->serve(GetimportStatusFromImportJobFeature::class);
    }

    public function getTaxonomyDownload()
    {
        return $this->serve(GetDocumentDownloadFileFeature::class);
    }
    public function getTreeHierarchyV3()
    {
        return $this->serve(GetTaxonomyHierarchyV3Feature::class);
    }

    #taxonomy heirarchy with tree structure
    public function getTreeHierarchyV4()
    {
        return $this->serve(GetTaxonomyHierarchyV4Feature::class);
    }

    public function copyNodes()
    {
        return $this->serve(CopyNodesFeature::class);
    }

    public function importFromMultipleCsv()
    {
        return $this->serve(ImportFromMultipleCsvFeature::class);
    }

    public function CallBackFromNodejsForCsv()
    {
        return $this->serve(CallBackFromNodejsForCsvFeature::class);
    }

    public function cancelCsvBatch()
    {
        return $this->serve(CancelCsvBatchFeature::class);
    }
    
    #taxonomy heirarchy with tree structure
    public function getTreeHierarchyV5()
    {
        return $this->serve(GetTaxonomyHierarchyV5Feature::class);
    }
}
