<?php
namespace App\Services\Api\Http\Controllers;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\OrgTestFeature;
class OrgTestController extends Controller
{
	 public function getData()
    {
        return $this->serve(OrgTestFeature::class);
    }
}
?>
