<?php
namespace App\Services\Api\Http\Controllers;

use App\Services\Api\Features\GetAllFileFromCacheFeature;
use App\Services\Api\Features\ReorderAdminDocumentFeature;
use App\Services\Api\Features\UpdateAssetFeature;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\CreateAssetFeature;
use App\Services\Api\Features\DeleteAssetFeature;
use App\Services\Api\Features\ListAssetFeature;

class AssetController extends Controller
{
    public function create()
    {
        return $this->serve(CreateAssetFeature::class);
    }

    public function delete() 
    {
        return $this->serve(DeleteAssetFeature::class);
    }

    public function reorderFiles(){
        return $this->serve(ReorderAdminDocumentFeature::class);
    }
    public function getAllFileFromCache(){
        return $this->serve(GetAllFileFromCacheFeature::class);
    }
    public function updateAsset(){
        return $this->serve(UpdateAssetFeature::class);
    }

    public function list() 
    {
        return $this->serve(ListAssetFeature::class);
    }
}
