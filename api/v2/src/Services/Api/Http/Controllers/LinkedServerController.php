<?php

namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\CreateLinkedCaseServerFeature;
use App\Services\Api\Features\UpdateLinkedServerFeature;
use App\Services\Api\Features\GetLinkedServerFeature;
use App\Services\Api\Features\DeleteLinkedServerFeature;
use App\Services\Api\Features\GetTaxonomyListFeature;

class LinkedServerController extends Controller
{
    //
    public function create()
    {
        //
        return $this->serve(CreateLinkedCaseServerFeature::class);
    }

    public function update()
    {
        //
        return $this->serve(UpdateLinkedServerFeature::class);
    }

    public function getList()
    {
        //
        return $this->serve(GetLinkedServerFeature::class);
    }

    public function deleteLinkServer()
    {
        //
        return $this->serve(DeleteLinkedServerFeature::class);
    }

    public function getTaxonomyList()
    {
        return $this->serve(GetTaxonomyListFeature::class);
    }
}
