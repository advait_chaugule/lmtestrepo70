<?php

namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\GetUserOrganizationListFeature;
use App\Services\Api\Features\SetUserOrganizationFeature;
class UserOrganizationController extends Controller
{
    public function getUserOrgList() {
        return $this->serve(GetUserOrganizationListFeature::class);
    }

    public function setUserOrgList()
    {
        return $this->serve(SetUserOrganizationFeature::class);
    }
}
