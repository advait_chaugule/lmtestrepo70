<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\SequenceNoFeature;
use App\Services\Api\Features\SequenceNoLEFeature;

class SequenceNoController extends Controller
{
    #to run both sequence no migration as API
    public function SequenceNoMigration(Request $request)
    {
        return $this->serve(SequenceNoFeature::class);
    }

    #to replace sequence no with list enumeration for specific taxonomy or organization
    public function SequenceNoMigrationLE(Request $request)
    {
        return $this->serve(SequenceNoLEFeature::class);
    }
}