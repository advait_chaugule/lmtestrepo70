<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\SearchFeature;
use App\Services\Api\Features\SaveSearchFeature;
use App\Services\Api\Features\GetSearchHistoryFeature;
use App\Services\Api\Features\SearchForCacheFeature;

class SearchController extends Controller
{
    /**
     * The REST read request message for the search() API call.
     * @SWG\Post(
     *     path="search",
     *     tags={"Search"},
     *     summary="The REST read request message for the search() API call.",
     *     description="This is a request to the service provider to search",
     *     operationId="search",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="Body",
     *     in="body",
     *     required=true,
     *     type="object",
     *     ref="$/responses/search",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/searchResponse",
     *     )
     * )
     *
     */

    /**
     * @SWG\Response(
     *      response="search",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="search_term",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */
    
    /**
     * @SWG\Response(
     *      response="searchResponse",
     *      description="the basic request",
     *      @SWG\Schema(
     *                 @SWG\Property(
     *                     property="search_term",
     *                     type="string",
     *                     description=""
     *                     ),
     *                 @SWG\Property(
     *                     property="search_optional_filters",
     *                     type="array",
     *                     description="",
     *                                @SWG\Items(                                          
     *                                        @SWG\Property(
     *                                        property="filter_type",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="filter_value",
     *                                        type="string",
     *                                        description=""
     *                                        ),
     *                                        @SWG\Property(
     *                                        property="filter_key",
     *                                        type="string",
     *                                        description=""
     *                                        )
     *                                 )                    
     *                      ),
     *                 @SWG\Property(
     *                     property="context_page",
     *                     type="string",
     *                     description=""
     *                     ),
     *             ),
     * )
     */ 

    public function search()
    {
        return $this->serve(SearchFeature::class);
    }

    public function save()
    {
        return $this->serve(SaveSearchFeature::class);
    }

    public function getHistoryList() {
        return $this->serve(GetSearchHistoryFeature::class);
    }

    public function searchForCache()
    {
        return $this->serve(SearchForCacheFeature::class); 
    }
}
