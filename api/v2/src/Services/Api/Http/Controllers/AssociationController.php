<?php
namespace App\Services\Api\Http\Controllers;
use Lucid\Foundation\Http\Controller;
use App\Services\Api\Features\CreateAssociationPresetFeature;
use App\Services\Api\Features\UpdateAssociationPresetFeature;
use App\Services\Api\Features\DeleteAssociationPresetFeature;
use App\Services\Api\Features\GetAssociationPresetFeature;
use App\Services\Api\Features\GetAssociationPresetListFeature;
use App\Services\Api\Features\CreateItemAssociationMetadataFeature;
use App\Services\Api\Features\UpdateItemAssociationMetadataFeature;
use App\Services\Api\Features\AssociationMetadataFeature;
use App\Services\Api\Features\DeleteAssociationMetadataFeature;

class AssociationController extends Controller
{
    public function createAssociationPreset()
    {
        return $this->serve(CreateAssociationPresetFeature::class);
    }

    public function deleteAssociationPreset()
    {
        return $this->serve(DeleteAssociationPresetFeature::class);
    }

    public function updateAssociationPreset()
    {
        return $this->serve(UpdateAssociationPresetFeature::class);
    }

    public function getAssociationPresetList()
    {
        return $this->serve(GetAssociationPresetListFeature::class);
    }

    public function getAssociationPreset()
    {
       return $this->serve(GetAssociationPresetFeature::class);
    }

    public function createItemAssociationMetadata(){

        return $this->serve(CreateItemAssociationMetadataFeature::class);
    }

    public function updateItemAssociationMetadata(){
        
        return $this->serve(UpdateItemAssociationMetadataFeature::class);
    }

    public function getAssociationMetadata(){
        return $this->serve(AssociationMetadataFeature::class);
    }

    public function deleteAssociationMetadata(){
        return $this->serve(DeleteAssociationMetadataFeature::class);
    }

}