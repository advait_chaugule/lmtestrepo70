<?php
namespace App\Services\Api\Http\Controllers;

use App\Services\Api\Features\GetCaseAssociationFeature;        // DONE
use App\Services\Api\Features\GetCaseConceptFeature;            // DONE
use App\Services\Api\Features\GetCaseDocumentFeature;           // DONE
use App\Services\Api\Features\GetCaseDocumentsFeature;          // DONE
use App\Services\Api\Features\GetCaseItemAssociationsFeature;   // DONE
use App\Services\Api\Features\GetCaseItemFeature;               // DONE
use App\Services\Api\Features\GetCaseItemTypeFeature;           // DONE
use App\Services\Api\Features\GetCaseLicenseFeature;            // DONE
use App\Services\Api\Features\GetCaseSubjectFeature;            // DONE
use App\Services\Api\Features\GetCFAssociationGroupingFeature;  // DONE
use App\Services\Api\Features\GetCFPackageFeature;
use App\Services\Api\Features\GetCustomMetadataFeature;
use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Competencies and Academic Standards Exchange (CASE) Service OpenAPI (JSON) Definition",
 *         description="The Competencies and Academic Standards Exchange (CASE) Service enables the exchange of data between a Competency Records Service Provider and the consumers of the associated data.",
 *         @SWG\Contact(
 *             name="Contact the developer",
 *             email="kamal.asawara@learningmate.com?subject=Competencies and Academic Standards Exchange (CASE) Service"
 *         ),
 *         @SWG\License(
 *             name="IMS Global",
 *             url="https://www.imsglobal.org/license.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="See more at http://imsglobal.org",
 *         url="https://www.imsglobal.org"
 *     )
 * )
 */

/**
 * @SWG\Response(
 *      response="CFDocuments",
 *      description="the basic response",
 *      @SWG\Schema(
 *          @SWG\Property(
 *              property="CFDocuments",
 *              type="array",
 *              @SWG\Items(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="creator",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                  @SWG\Property(
 *                      property="officialSourceURL",
 *                      type="string",
 *                      description="The data-type for establishing a Uniform Resource Locator (URL) as defined by W3C."
 *                  ),
 *                  @SWG\Property(
 *                      property="publisher",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="subject",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="Subject Name",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="subjectURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="language",
 *                      type="string",
 *                      description="Model Primitive Datatype = Language",
 *                  ),
 *                  @SWG\Property(
 *                      property="version",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                  ),
 *                  @SWG\Property(
 *                      property="adoptionStatus",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusStartDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusEndDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseURI",
 *                      type="object",
 *                      description="",
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                  ),
 *                  @SWG\Property(
 *                      property="notes",
 *                      type="string",
 *                      description="Model Primitive Datatype = String",
 *                  ),
 *                  @SWG\Property(
 *                      property="CFPackageURI",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI",
 *                  ),
 *              ),
 *          )
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFDocument",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="creator",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                  @SWG\Property(
 *                      property="officialSourceURL",
 *                      type="string",
 *                      description="The data-type for establishing a Uniform Resource Locator (URL) as defined by W3C."
 *                  ),
 *                  @SWG\Property(
 *                      property="publisher",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="subject",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="Subject Name",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="subjectURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="language",
 *                      type="string",
 *                      description="Model Primitive Datatype = Language",
 *                  ),
 *                  @SWG\Property(
 *                      property="version",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                  ),
 *                  @SWG\Property(
 *                      property="adoptionStatus",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusStartDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusEndDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseURI",
 *                      type="object",
 *                      description="",
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                  ),
 *                  @SWG\Property(
 *                      property="notes",
 *                      type="string",
 *                      description="Model Primitive Datatype = String",
 *                  ),
 *                  @SWG\Property(
 *                      property="CFPackageURI",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI",
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFItem",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="CFDocumentURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="fullStatement",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="alternativeLabel",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="CFItemType",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="humanCodingScheme",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="listEnumeration",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="abbreviatedStatement",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="conceptKeywords",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="keywords",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="conceptKeywordsURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="notes",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="language",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="educationLevel",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="Education Level",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFItemTypeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="statusStartDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusEndDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFConcept",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="keywords",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="hierarchyCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFItemType",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="hierarchyCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="typeCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFLicense",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseText",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFSubject",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="hierarchyCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFAssociation",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="associationType",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="sequenceNumber",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI"
 *                  ),
 *                  @SWG\Property(
 *                      property="originNodeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="destinationNodeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFAssociationGroupingURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFDocumentURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFAssociationGrouping",
 *      description="the basic response",
 *      @SWG\Schema(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *      )
 * )
 *
 */

/**
 * @SWG\Response(
 *      response="CFPackage",
 *      description="the basic response",
 *      @SWG\Schema(
 *              @SWG\Property(
 *                   property = "CFDocument",
 *                   @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="creator",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                  @SWG\Property(
 *                      property="officialSourceURL",
 *                      type="string",
 *                      description="The data-type for establishing a Uniform Resource Locator (URL) as defined by W3C."
 *                  ),
 *                  @SWG\Property(
 *                      property="publisher",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="subject",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="Subject Name",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="subjectURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="language",
 *                      type="string",
 *                      description="Model Primitive Datatype = Language",
 *                  ),
 *                  @SWG\Property(
 *                      property="version",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                  ),
 *                  @SWG\Property(
 *                      property="adoptionStatus",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusStartDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusEndDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseURI",
 *                      type="object",
 *                      description="",
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                  ),
 *                  @SWG\Property(
 *                      property="notes",
 *                      type="string",
 *                      description="Model Primitive Datatype = String",
 *                  ),
 *                  @SWG\Property(
 *                      property="CFPackageURI",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI",
 *                  ),
 *              ),
 *           @SWG\Property(
 *              property="CFItems",
 *              type="array",
 *              @SWG\Items(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="CFDocumentURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="fullStatement",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="alternativeLabel",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="CFItemType",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="humanCodingScheme",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="listEnumeration",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="abbreviatedStatement",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="conceptKeywords",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="keywords",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="conceptKeywordsURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="notes",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="language",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="educationLevel",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="Education Level",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFItemTypeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="statusStartDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusEndDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *              ),
 *          ),
 *          @SWG\Property(
 *              property="CFAssociations",
 *              type="array",
 *              @SWG\Items(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="associationType",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="CFDocumentURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="sequenceNumber",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI"
 *                  ),
 *                  @SWG\Property(
 *                      property="originNodeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="destinationNodeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFAssociationGroupingURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *              )
 *          ),
 *          @SWG\Property(
 *              property="CFDefinitions",
 *                  @SWG\Property(
 *                      property = "CFConcepts",
 *                      type = "array",
 *                      @SWG\Items(
 *                          @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="keywords",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="hierarchyCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property = "CFSubjects",
 *                      type = "array",
 *                      @SWG\Items(
 *                          @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="hierarchyCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property = "CFLicences",
 *                      type = "array",
 *                      @SWG\Items(
 *                          @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseText",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property = "CFItemTypes",
 *                      type = "array",
 *                      @SWG\Items(
 *                          @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="hierarchyCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="typeCode",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property = "CFAssociationGroupings",
 *                      type = "array",
 *                      @SWG\Items(
 *                          @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="title",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="description",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *                      )
 *                  )
 *          )
 *      )
 * )
 *
 */

 /**
 * @SWG\Response(
 *      response="CFItemAssociation",
 *      description="the basic response",
 *      @SWG\Schema(
 *              @SWG\Property(
 *                   property = "CFItem",
 *                   @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="CFDocumentURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="fullStatement",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="alternativeLabel",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="CFItemType",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI "
 *                  ),
 *                  @SWG\Property(
 *                      property="humanCodingScheme",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="listEnumeration",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="abbreviatedStatement",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="conceptKeywords",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="keywords",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="conceptKeywordsURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="notes",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="language",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="educationLevel",
 *                      type="array",
 *                      description="Model Primitive Datatype = NormalizedString",
 *                      @SWG\Items(
 *                           title="Education Level",
 *                           type="string"
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFItemTypeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="licenseURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="statusStartDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="statusEndDate",
 *                      type="string",
 *                      description="Model Primitive Datatype = Date",
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *              ),
 *           @SWG\Property(
 *              property="CFAssociations",
 *              type="array",
 *              @SWG\Items(
 *                  @SWG\Property(
 *                      property="identifier",
 *                      type="string",
 *                      description="The data-type for establishing a Globally Unique Identifier (GUID). The form of the GUID is a Universally Unique Identifier (UUID) of 16 hexadecimal characters (lower case) in the format 8-4-4-4-12."
 *                  ),
 *                  @SWG\Property(
 *                      property="associationType",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString "
 *                  ),
 *                  @SWG\Property(
 *                      property="sequenceNumber",
 *                      type="string",
 *                      description="Model Primitive Datatype = NormalizedString"
 *                  ),
 *                  @SWG\Property(
 *                      property="uri",
 *                      type="string",
 *                      description="Model Primitive Datatype = AnyURI"
 *                  ),
 *                  @SWG\Property(
 *                      property="originNodeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="destinationNodeURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFAssociationGroupingURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="CFDocumentURI",
 *                      type="array",
 *                      description="",
 *                      @SWG\Items(
 *                           @SWG\Property(
 *                              property="title",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="identifier",
 *                              type="string",
 *                              description=""
 *                           ),
 *                           @SWG\Property(
 *                              property="uri",
 *                              type="string",
 *                              description=""
 *                           )
 *                      )
 *                  ),
 *                  @SWG\Property(
 *                      property="lastChangeDateTime",
 *                      type="string",
 *                      description="Model Primitive Datatype = DateTime"
 *                  ),
 *              )
 *                  
 *          )
 *      )
 * )
 *
 */

class CaseController extends Controller
{

    /**
     * The REST read request message for the getCFItem() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFItems/{sourceId}",
     *     tags={"ItemsManager"},
     *     summary="The REST read request message for the getCFItem() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Item. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFItem",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFItem",
     *     )
     * )
     *
     */
    public function getCFItem()
    {
        return $this->serve(GetCaseItemFeature::class);
    }

    /**
     * The REST read request message for the getAllCFDocument() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFDocuments/{sourceId}",
     *     tags={"DocumentsManager"},
     *     summary="The REST read request message for the getCFDocument() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Document. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFDocument",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFDocument",
     *     )
     * )
     *
     */
    public function getCFDocument()
    {
        return $this->serve(GetCaseDocumentFeature::class);
    }

    /**
     * The REST read request message for the getAllCFDocuments() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFDocuments",
     *     tags={"DocumentsManager"},
     *     summary="The REST read request message for the getAllCFDocuments() API call.",
     *     description="This is a request to the Service Provider to provide all of the Competency Framework Documents.",
     *     operationId="getAllCFDocuments",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFDocuments",
     *     )
     * )
     *
     */
    public function getAllCFDocuments()
    {
        return $this->serve(GetCaseDocumentsFeature::class);
    }

    /**
     * The REST read request message for the getCFAssociation() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFAssociations/{sourceId}",
     *     tags={"AssociationsManager"},
     *     summary="The REST read request message for the getCFAssociation() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Association. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFAssociation",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFAssociation",
     *     )
     * )
     *
     */

    public function getCFAssociation()
    {
        return $this->serve(GetCaseAssociationFeature::class);
    }

    /**
     * The REST read request message for the getCFItemAssociations() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFItemAssociations/{sourceId}",
     *     tags={"AssociationsManager"},
     *     summary="The REST read request message for the getCFItemAssociations() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Item Association. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFItemAssociations",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFItemAssociation",
     *     )
     * )
     *
     */

    public function getCFItemAssociations()
    {
        return $this->serve(GetCaseItemAssociationsFeature::class);
    }

    /**
     * The REST read request message for the getCFConcept() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFConcepts/{sourceId}",
     *     tags={"ConceptsManager"},
     *     summary="The REST read request message for the getCFConcept() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Concept. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFConcept",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFConcept",
     *     )
     * )
     *
     */
    public function getCFConcept()
    {
        return $this->serve(GetCaseConceptFeature::class);
    }

    /**
     * The REST read request message for the getCFItemType() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFItemTypes/{sourceId}",
     *     tags={"ItemTypesManager"},
     *     summary="The REST read request message for the getCFItemType() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework ItemType. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFItemType",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFItemType",
     *     )
     * )
     *
     */
    public function getCFItemType()
    {
        return $this->serve(GetCaseItemTypeFeature::class);
    }

    /**
     * The REST read request message for the getCFLicense() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFLicenses/{sourceId}",
     *     tags={"LicensesManager"},
     *     summary="The REST read request message for the getCFLicense() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework License. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFLicense",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFLicense",
     *     )
     * )
     *
     */
    public function getCFLicense()
    {
        return $this->serve(GetCaseLicenseFeature::class);
    }

    /**
     * The REST read request message for the getCFSubject() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFSubjects/{sourceId}",
     *     tags={"SubjectsManager"},
     *     summary="The REST read request message for the getCFSubject() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Subject. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFSubject",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFSubject",
     *     )
     * )
     *
     */
    public function getCFSubject()
    {
        return $this->serve(GetCaseSubjectFeature::class);
    }

    /**
     * The REST read request message for the getCFPackage() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFPackages/{sourceId}",
     *     tags={"PackagesManager"},
     *     summary="The REST read request message for the getCFPackage() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Package. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFPackage",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFPackage",
     *     )
     * )
     *
     */
    public function getCFPackage()
    {
        return $this->serve(GetCFPackageFeature::class);
    }

    /**
     * The REST read request message for the getCFAssociationGrouping() API call.
     * @SWG\Get(
     *     path="org01/ims/case/v1p0/CFAssociationGroupings/{sourceId}",
     *     tags={"AssociationsManager"},
     *     summary="The REST read request message for the getCFAssociationGrouping() API call.",
     *     description="This is a request to the service provider to provide the information for the specific Competency Framework Association. If the identified record cannot be found then the 'unknownobject' status code must be reported.",
     *     operationId="getCFAssociationGrouping",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sourceId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         ref="$/responses/CFAssociationGrouping",
     *     )
     * )
     *
     */

    public function getCFAssociationGrouping()
    {
        return $this->serve(GetCFAssociationGroupingFeature::class);
    }

    public function getCustomMetadata()
    {
        return $this->serve(GetCustomMetadataFeature::class);
    }

}
