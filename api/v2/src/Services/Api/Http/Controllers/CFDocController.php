<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\ListCFDocumentsFeature;
use App\Services\Api\Features\GetCFDocByIdFeature;
use App\Services\Api\Features\GetCFMasterDataFeature;

class CFDocController extends Controller
{

    public function listing() {
        return $this->serve(ListCFDocumentsFeature::class);
    }
    
    public function getbyid(){
        return $this->serve(GetCFDocByIdFeature::class);
        
    }
    
    public function getCFMasterData(){
        
        return $this->serve(GetCFMasterDataFeature::class);
        
    }

   
}
