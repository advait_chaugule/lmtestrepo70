<?php
namespace App\Services\Api\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Api\Features\GetReportDashboardDataFeature;
use App\Services\Api\Features\GetComplianceReportDataFeature;
use App\Services\Api\Features\GetCommentReportDataFeature;
use App\Services\Api\Features\GetItemComplianceReportFeature;

class ReportController extends Controller
{

    public function getDashboardData()
    {
        return $this->serve(GetReportDashboardDataFeature::class);
    }
    
    public function getComplianceReport()
    {
        return $this->serve(GetComplianceReportDataFeature::class);
    }

    public function getCommentReport()
    {
        return $this->serve(GetCommentReportDataFeature::class);
    }

    public function getItemComplianceReport()
    {
        return $this->serve(GetItemComplianceReportFeature::class);
    }
}
