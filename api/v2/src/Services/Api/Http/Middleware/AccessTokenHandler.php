<?php
namespace App\Services\Api\Http\Middleware;
use Illuminate\Http\Request;

use Closure;
use Carbon\Carbon;
use App\Foundation\ResponseHandler;
use App\Services\Api\Traits\StringHelper;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Domains\User\Events\UserLogoutEvent;
use App\Domains\User\Events\SessionRefreshedEvent;

class AccessTokenHandler
{

    use StringHelper;

    private $userRepository;
    private $responseHandler;

    private $authenticatedUser;
    private $userRole;
    private $requestAccessToken;
    private $isAccessTokenValid;
    private $isUserAuthenticated;
    private $isUserBlocked;
    private $isUserDeleted;
    private $isUserRoleActive;
    private $isUserRoleDeleted;

    public function __construct(UserRepositoryInterface $userRepository, ResponseHandler $responseHandler)
    {
      $this->userRepository = $userRepository;
      $this->responseHandler = $responseHandler;
    }

    private function setUser($user) {
        $this->authenticatedUser = $user;
    }

    public function getUser() {
      return $this->authenticatedUser;
    }

    public function setUserRole($data) {
      $this->userRole = $data;
    }

    public function getUserRole() {
      return $this->userRole;
    }

    public function setRequestAccessToken(string $token) {
      $this->requestAccessToken = $token;
    }

    public function getRequestAccessToken(): string {
      return $this->requestAccessToken;
    }

    public function setAccessTokenValidityStatus(bool $status) {
      $this->isAccessTokenValid = $status;
    }

    public function getAccessTokenValidityStatus() {
      return $this->isAccessTokenValid;
    }

    public function setUserAuthenticationStatus(bool $status) {
      $this->isUserAuthenticated = $status;
    }

    public function getUserAuthenticationStatus() {
      return $this->isUserAuthenticated;
    }

    public function setUserBlockedStatus(bool $status) {
      $this->isUserBlocked = $status;
    }

    public function getUserBlockedStatus() {
      return $this->isUserBlocked;
    }

    public function setUserDeletedStatus(bool $status) {
      $this->isUserDeleted = $status;
    }

    public function getUserDeletedStatus() {
      return $this->isUserDeleted;
    }

    public function setUserRoleActiveStatus(bool $status) {
      $this->isUserRoleActive = $status;
    }

    public function getUserRoleActiveStatus() {
      return $this->isUserRoleActive;
    }

    public function setUserRoleDeletedStatus(bool $status) {
      $this->isUserRoleDeleted = $status;
    }

    public function getUserRoleDeletedStatus() {
      return $this->isUserRoleDeleted;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      // extract access token from request header
      $accessToken = $this->extractBearerAccessTokenFromHeader($request);
      // check and set access token validity status
      if(!empty($accessToken)){
        $this->setAccessTokenValidityStatus(true);
        $this->setRequestAccessToken($accessToken);
        $this->processUserValidationPossibilitiesAndSetThem();
      }
      else {
        $this->setAccessTokenValidityStatus(false);
      }
      
      // get all kinds of statuses for the user asking access
      $accessTokenValidationStatus = $this->getAccessTokenValidityStatus();
      $userAuthenticationStatus = $this->getUserAuthenticationStatus();
      $userBlockedStatus = $this->getUserBlockedStatus();
      $userDeletedStatus = $this->getUserDeletedStatus();
      $userRoleActiveStatus = $this->getUserRoleActiveStatus();
      $userRoleDeletedStatus = $this->getUserRoleDeletedStatus();
      $accessTokenExpiredStatus = $this->checkIfAccessTokenHasExpired();

      if($accessTokenExpiredStatus===true) {
        $this->raiseLogoutEvent();
      }

      //settings for front facing
      $front_facing = (null!==$request->input('front_facing'))?$request->input('front_facing'):false;
      if(null!==$request->input('is_internal')){
        $front_facing = $request->input('is_internal')==='false' ? true : false;
      }
      
      if($front_facing==true){
        $accessTokenValidationStatus = true;
        $accessTokenExpiredStatus = false;
        $userAuthenticationStatus = true;
        $userBlockedStatus = false; 
        $userRoleActiveStatus = true;
      }

      // set error response messages based on statuses
      $responseMessageList = [];
      if($accessTokenValidationStatus !==null && !$accessTokenValidationStatus)
          $responseMessageList[] = "Please provide a valid access token.";
      if($userAuthenticationStatus !==null && !$userAuthenticationStatus)
          $responseMessageList[] = "Please provide a valid access token.";
      if($accessTokenExpiredStatus !==null && $accessTokenExpiredStatus === true)
        $responseMessageList[] = "User session has expired. Please login again.";
      if($userBlockedStatus !==null && $userBlockedStatus===true)
          $responseMessageList[] = "User Blocked.";
      if($userDeletedStatus !==null && $userDeletedStatus===true)
          $responseMessageList[] = "User Deleted.";
      if($userRoleActiveStatus !==null && !$userRoleActiveStatus)
        $responseMessageList[] = "User Role Inactive.";
      if($userRoleDeletedStatus !==null && $userRoleDeletedStatus === true)
        $responseMessageList[] = "User Role Deleted.";

      // assign status for the user asking to Access ACMT service(s)
      $overallAccessStatus =  $accessTokenValidationStatus && $userAuthenticationStatus && !$accessTokenExpiredStatus && 
                              !$userBlockedStatus && !$userDeletedStatus && $userRoleActiveStatus && !$userRoleDeletedStatus;

      if($overallAccessStatus===true && $front_facing==false) {
        // if the user is allowed to Access ACMT service(s) update the incoming request body with request user details
        $request = $this->updateRequestBodyWithUserDetails($request);
        // refresh user session
        $this->refreshUserSession();
      }

      return $overallAccessStatus ? 
             $next($request) : 
             $this->responseHandler->respondAuthorizationError($this->arrayContentToCommaeSeparatedString($responseMessageList));
    }

    /**
     * method to extract authorizaton token
     */
    private function extractBearerAccessTokenFromHeader(Request $request): string {
        $authorizationToken = $request->header('Authorization');
        return !empty($authorizationToken) ? trim($authorizationToken) : "";
    }

    /**
     * This method will process and set all validation possibilities of request user viz.
     * user blocked/deleted, user-role blocked/deleted, user-organization delted
     */
    private function processUserValidationPossibilitiesAndSetThem() {
        $accessToken = $this->getRequestAccessToken();
        // get user authentication status and set it
        $userAuthenticationStatus = $this->authenticateUserUsingAccessToken($accessToken);
        $this->setUserAuthenticationStatus($userAuthenticationStatus);

        // if the user passes token authentication then process and set other access statuses
        if($userAuthenticationStatus===true){
          // fetch and set user role
          $userRole = $this->fetchUserRole();
          $this->setUserRole($userRole);
  
          // set user blocked status
          $userBlockedStatus = $this->isUserBlocked();
          $this->setUserBlockedStatus($userBlockedStatus);
  
          // set user deleted status
          $userDeletedStatus = $this->isUserDeleted();
          $this->setUserDeletedStatus($userDeletedStatus);
  
          // set  user role active status
          $userRoleActiveStatus = $this->isUserRoleActive();
          $this->setUserRoleActiveStatus($userRoleActiveStatus);
  
          // set  user role deleted status
          $userRoleDeletedStatus = $this->isUserRoleDeleted();
          $this->setUserRoleDeletedStatus($userRoleDeletedStatus);
        }
    }

    /**
     * This returns user authentication status based on token provided
     * Also set the authenticated user
     */
    private function authenticateUserUsingAccessToken(string $token): bool {
      $conditionAttributeData = [ "active_access_token" => $token ];
      $returnCollection = false;
      $fieldsToReturn = [
        "user_id", "email", "first_name", "last_name", 
        "role_id", "current_organization_id as organization_id", "is_active", "is_deleted", 
        "access_token_updated_at","is_super_admin"
      ];
      $user = $this->userRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionAttributeData);
      $this->setUser($user);
      return !empty($user->user_id);
    }

    /**
     * Method to fetch user role
     */
    private function fetchUserRole() {
      $user = $this->getUser();
      $userRole = $user->systemRole()->select("is_active", "is_deleted")->get()->first();
      return $userRole;
    }

    /**
     * Method to return user blocked status
     */
    private function isUserBlocked(): bool {
      $user = $this->getUser();
      return $user->is_active==0 ? true : false;
    }

    /**
     * Method return user deleted status
     */
    private function isUserDeleted(): bool {
      $user = $this->getUser();
      return $user->is_deleted==1 ? true : false;
    }

    /**
     * Method to return user role active status
     */
    private function isUserRoleActive(): bool {
      $userRole = $this->getUserRole();
      return $userRole->is_active==1 ? true : false;
    }

    /***
     * Method to return user role deleted status
     */
    private function isUserRoleDeleted(): bool {
      $userRole = $this->getUserRole();
      return $userRole->is_deleted==1 ? true : false;
    }

    /**
     * Method to merge request body with details of user asking to Access ACMT
     */
    private function updateRequestBodyWithUserDetails(Request $request) {
      $user = $this->getUser()->toArray();

      $loggedInUserData = [ "auth_user" => $user ];
      return $request->merge($loggedInUserData);
    }

    private function checkIfAccessTokenHasExpired() {
      $user = $this->getUser();
      $status = null;
      if(!empty($user->access_token_updated_at)) {
        $accessTokenLastUpdatedAt = Carbon::parse($user->access_token_updated_at);
        $currentTimestamp = now();
        $sessionTimeOutInMinutes = config("session.access_token_timeout_in_minutes");
        $timeSpentByUserInMinutes = $currentTimestamp->diffInMinutes($accessTokenLastUpdatedAt);
        if( $timeSpentByUserInMinutes <= $sessionTimeOutInMinutes ) {
          $status = false;
        }
        else {
          $status = true;
        }
      }
      return $status;
    }

    private function raiseLogoutEvent() {
      if(!empty($this->getUser())) {
        $user = $this->getUser()->toArray();
        // raise event to track activities for reporting
        $eventData = [
            "beforeEventRawData" => $user,
            "afterEventRawData" => $user,
            "requestUserDetails" => $user
        ];
        event(new UserLogoutEvent($eventData));
      }
    }

    private function refreshUserSession() {
      $user = $this->getUser();
      if(!empty($user)) {
        $conditionalAttributeValuePairs = [ "user_id" => $user->user_id ];
        $attributeValuePairsToUpdate = [ "access_token_updated_at" =>  now()->toDateTimeString()];
        $this->userRepository->editWithCustomizedFields($conditionalAttributeValuePairs, $attributeValuePairsToUpdate);

        // raise event to track user session refreshed activity
        $data = $user->toArray();
        $eventData = [
            "beforeEventRawData" => $data,
            "afterEventRawData" => $data,
            "requestUserDetails" => $data
        ];
        event(new SessionRefreshedEvent($eventData));
      }
    }

}
