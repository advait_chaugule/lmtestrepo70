<?php

namespace App\Services\Api\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use App\Foundation\ResponseHandler;
use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

class OrgActiveHandler
{
    public function __construct(OrganizationRepositoryInterface $orgRepository, ResponseHandler $responseHandler)
    {
      $this->orgRepository = $orgRepository;
      $this->responseHandler = $responseHandler;
    }
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route()->hasParameter('org_code')) {
            $conditionAttributeData = [ "org_code" => $request->route()->parameter('org_code'),'is_active' => '1' ];
            $returnCollection = false;
            $fieldsToReturn = ["organization_id"];
            $organization = $this->orgRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionAttributeData);
            if ($organization === null) {
                return $this->responseHandler->respondAuthorizationError("Organization Code does not exist.");
            }
        }

        return $next($request);
    }

}