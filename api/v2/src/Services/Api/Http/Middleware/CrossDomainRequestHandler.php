<?php
namespace App\Services\Api\Http\Middleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


use Closure;  

class CrossDomainRequestHandler
{

    // will try to populate this array from db
    private $allowedOriginsWhitelist = [
      'http://www.xyz.com',
      'http://host.something.com',
      'http://localhost:8090',
      'http://localhost:8091',
      'http://localhost:8005',
      'http://172.16.12.164:8091',
    ];
 
    // All the headers must be a string
    
    // default origin ( Please set * for local or dev environment )
    // pseudocode -> env==prod then {url} , otherwise {*}
    private $allowedOrigin = '*'; //'https://www.google.co.in';
 
    private $allowedMethods = 'OPTIONS, GET, POST, PUT, PATCH, DELETE';
 
    private $allowCredentials = 'true';
 
    private $accessControlAllowHeader = '';

    private $SymfonyResponse = 'Symfony\Component\HttpFoundation\BinaryFileResponse';
 
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      if ($this->isCorsRequest($request))
      {
        // resolve and set origin, methods, headers to be allowed
        $this->setAllowedOrigin($request);
        $this->setAllowedHeaders($request);
        $this->allowedOrigin=config('app')['allowedOrigin'];
        $headers = [
          'Access-Control-Allow-Origin'       => $this->allowedOrigin,
          'Access-Control-Allow-Methods'      =>  $this->allowedMethods,
          'Access-Control-Allow-Headers'      =>  $this->accessControlAllowHeader,
          'Access-Control-Allow-Credentials'  =>  $this->allowCredentials,
        ];
        
        // For preflighted requests, return blank response with all the allowed headers
        if ($request->getMethod() === 'OPTIONS')
        {
          return response('', 200)->withHeaders($headers);
        }

        /* if($next($request) instanceof $this->SymfonyResponse) {
          return $next($request);//->withHeaders($headers);
        } else {
          // continue the request flow to the router
          return $next($request)->withHeaders($headers);
        } */

        // continue the request flow to the router
        return $next($request)->withHeaders($headers);
        
      }
      else{
        // allow request to pass for non-cors requests
        return $next($request);
      }
    }
 
    /**
     * Incoming request is a CORS request if the Origin
     * header is set and Origin !== Host
     *
     * @param  \Illuminate\Http\Request  $request
     */
    private function isCorsRequest(Request $request)
    {
        $status = false;
        $searchHeaderKey = "Origin";
        $requestHasOrigin = $request->headers->has($searchHeaderKey);
        if ($requestHasOrigin)
        {
            $origin = $request->headers->get($searchHeaderKey);
            $host = $request->getSchemeAndHttpHost();
            $status = $origin !== $host;
        }
        return $status;
    }
 
    /**
     * Dynamic resolution of allowed origin.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    private function setAllowedOrigin(Request $request)
    {
      $origin = $request->headers->get('Origin');
      // if requested origin is present inside allowed white list then set it
      if (in_array($origin, $this->allowedOriginsWhitelist))
      {
        $this->allowedOrigin = $origin;
      }
    }
 
    /**
     * Take & set the incoming client request headers. 
     * Will be used to pass in Access-Control-Allow-Headers
     * @param  \Illuminate\Http\Request  $request
     */
    private function setAllowedHeaders(Request $request)
    {
      $this->accessControlAllowHeader = $request->headers->get('Access-Control-Request-Headers');
    }

}
