<?php

namespace App\Services\Api\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use App\Foundation\ResponseHandler;

use App\Services\Api\Traits\StringHelper;

use App\Data\Repositories\Contracts\PermissionRepositoryInterface;

class PermissionHandler
{
    use StringHelper;

    private $responseHandler;
    private $permissionRepository;

    private $requestUserDetails;
    private $permissionUserWantsToAccess;
    private $userRoleId;
    private $internalPermissionsAssignedToRole;

    public function __construct(ResponseHandler $responseHandler, PermissionRepositoryInterface $permissionRepository)
    {
        $this->responseHandler = $responseHandler;
        $this->permissionRepository = $permissionRepository;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setPermissionUserWantsToAccess(string $data) {
        $this->permissionUserWantsToAccess = $data;
    }

    public function getPermissionUserWantsToAccess(): string {
        return $this->permissionUserWantsToAccess;
    }

    public function setUserRoleId(string $data) {
        $this->userRoleId = $data;
    }

    public function getUserRoleId(): string {
        return $this->userRoleId;
    }

    public function setInternalPermissionsAssignedToRole($data) {
        $this->internalPermissionsAssignedToRole = $data;
    }

    public function getInternalPermissionsAssignedToRole() {
        return $this->internalPermissionsAssignedToRole;
    }

    public function setPermissionGrantStatus(bool $data) {
        $this->permissionGrantStatus = $data;
    }

    public function getPermissionGrantStatus(): bool {
        return $this->permissionGrantStatus;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $requestData = $request->all();
            //settings for front facing
        $front_facing = !empty($request->input('front_facing')) ? $request->input('front_facing') : false;
        $is_enternal =  !empty($request->input('is_internal')) ? $request->input('is_internal') : false;
        if($is_enternal == false){
            $front_facing =  true;
        }
        else
        {
            $front_facing =  false;
        }
        if($front_facing == true)
        {
            return $next($request);
        }
        $this->setRequestUserDetails($requestData["auth_user"]);

        // extract internal name for the permission that user wants access to and set it
        $this->extractPermissionThatUserWantsToAccessAndSetIt($request);
        // get role id from request user and set it
        $this->extractRoleIdFromRequestUser();
        // fetch all internal permissions assigned to user role and set them
        $this->fetchPermissionsAssignedToUserRole();
        // check and set status about whether user-role has access to the requested permission
        $this->checkWhetherUserRoleHasAccessToTheRequestedPermission();

        $userCanAccessStatus = $this->getPermissionGrantStatus();

        return $userCanAccessStatus ? 
               $next($request) : 
               $this->responseHandler->respondAuthorizationError("Permission not granted");
    }

    private function extractPermissionThatUserWantsToAccessAndSetIt(Request $request) {
        $routeAccessedObject = $request->route();
        $routeDetails = $this->delimittedStringToArray($routeAccessedObject->getActionName(), "\\");
        $permissionUserWantsToAccess = last($routeDetails);
        $this->setPermissionUserWantsToAccess($permissionUserWantsToAccess);
    }

    private function extractRoleIdFromRequestUser() {
        $requestUser = $this->getRequestUserDetails();
        $userRoleId = $requestUser["role_id"];
        $this->setUserRoleId($userRoleId);
    }

    private function fetchPermissionsAssignedToUserRole() {
        $userRoleId = $this->getUserRoleId();
        $internalPermissionNamesAssignedToRole = $this->permissionRepository->getInternalPermissionNamesAssignedToRole($userRoleId);
        $this->setInternalPermissionsAssignedToRole($internalPermissionNamesAssignedToRole);
    }

    private function checkWhetherUserRoleHasAccessToTheRequestedPermission() {
        $getPermissionUserWantsToAccess = $this->getPermissionUserWantsToAccess();
        $internalPermissionsAssignedToUserRole = $this->getInternalPermissionsAssignedToRole();
        $status = in_array($getPermissionUserWantsToAccess, $internalPermissionsAssignedToUserRole);
        $this->setPermissionGrantStatus($status);
    }


}
