
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
    </style>
    <title>AWS LOG</title>
  </head>
  <body>
    
  <div class="container">
    <h1>Set Date to fetch log data</h1>
    <div   class="row" >
    <form action="" target="_blank" id="log_search_form" name="log_search_form">
    <div class="row">
    <div class="col-sm">
      Select a Date : 
    </div>
    <div class="col-sm">
      <input type="text" name="from_date" id="from_date">
    </div>
   
    
  </div>
  <br/>
  <div class="row">
    <div class="col-sm">

    </div>
    <div class="col-sm">
   
    </div>
    <div class="col-sm">
    
    </div>
    <div class="col-sm">
    <button type="submit" class="btn btn-primary btn-block" >Submit</button>
    </div>
    
  </div>
  </form>
  </div>
  <div   class="row" >
  <div class="col-sm my-3" >
  <div class="mx-auto" style="width: 200px;display:none" id='loader'>
  <em class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></em>
  </div>
  <div class="col-sm" id="result_loading">
    
  </div>
  <div class="col-sm" id="result_div">
    <h2>Data Log Table</h2> 
    <table>
      <tr>
        <th>URL</th>
        <th>PROTOCOL</th>
        <th>Request Date Time </th>
        <th>REQUESTER</th>
        <th>REMOTE IP</th>
        <th>REQUEST ID</th>
        <th>Total Time</th>
        <th>TurnAround Time</th>
        <th></th>
        <th>HTTP status</th>
        <th>Error Code</th>
        <th>Bytes Sent</th>
        <th>Object Size</th>
        <th>Request-URI</th>
        <th>User-Agent</th>
      </tr>
      
    </table>
  </div>
  
  </div>
  </div>
</div>

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
type="text/javascript"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
rel="Stylesheet"type="text/css"/>

    <script>
    $(function(){
      if(localStorage.getItem("access_token") == null)
      {
        alert("Please login to access this page");
        window.location = '/';
      }

      $("#from_date").datepicker({
          dateFormat: 'yy/mm/dd',
          numberOfMonths: 2,
          onSelect: function (selected) {
              var dt = new Date(selected);
              dt.setDate(dt.getDate() + 1);
              $("#to_date").datepicker("option", "minDate", dt);
          }
      });

      $('#log_search_form').submit(function(e){
         
        e.preventDefault();  //prevent form from submitting
        
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        $('#result_loading').html('');
        
        if($("#from_date").val() == "") {
            alert("Please select date to search the log");
            return false;
        } else {
          var data = {
                  'from_date': $("#from_date").val(),
          };

          $.ajax({
              type: "POST",
              url: "log/fetchLogData",
              headers: {
                  'Authorization': localStorage.getItem("access_token"),//'8f3c9e9a-2dfe-4a6c-9c12-00c24da050c2'
                  'Accept':'application/json'
              },
              data: JSON.stringify(data),
              contentType: "application/json; charset=utf-8",
              crossDomain: true,
              //dataType: "json",
              beforeSend: function() {
                  // setting a timeout
                  $('#result_loading').html('<p>Loading data...</p>');
              },
              complete: function() {

                //$('#result_loading').html('');
              },
              success: function(data) {
                var markup  = '';
                console.log(data.html.length);
                if(data.html.length > 0) {
                  //var nestedValue = '';
                  $.each(data.html, function( index, value ) {
                    markup += "<tr>";
                    console.log(value);
                    $.each(value, function( key, nestedValue){
                      markup += "<td>"+nestedValue+"</td>";
                    })
                    markup += "</tr>";
                  });
                  $('#result_div table tbody').append(markup);
                } else {
                  $('#result_loading').html('<p>No data found for the selected date</p>');
                }
                
              },
              error: function(xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                alert(xhr.responseText);
              }
          });
        }
      });
    });
      

      
    </script>

  </body>
</html>