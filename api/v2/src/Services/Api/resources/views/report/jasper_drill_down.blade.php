<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    #back_btn{
      display:none;margin-top:15px;
    }
    td{ 
      line-height:0;
    }
    
    </style>
    <title>JReport</title>
  </head>
  <body>
    

    <div class="container">
    
    <div   class="row float-right" >
    <form action="jreport/show" target="_blank" id="report_form" >
  <div class="row">
    
    <div class="col-sm">
    <input type='hidden' name="organization_id" id="organization_id" value="{{ $organization_id }}">
    <button type="button" class="btn btn-primary btn-block" id="back_btn" >Back</button>

    <!-- <button type="submit" class="btn btn-primary btn-block" >Submit</button> -->
    </div>
    
  </div>
  </form>
  </div>
  <div   class="row" >
  <div class="col-sm my-3" >
  <div class="mx-auto" style="width: 200px;display:none" id='loader'>
  <em class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></em>
  </div>
  
  <div class="col-sm" id="result_div">
  
  </div>
  
  </div>
  </div>
</div>


    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script>

    $(document).ready(function(){
            var token= localStorage.getItem("access_token");
            
            if(token == null)
            {
              alert("Please login to access this page");
              window.location = '/';
            }else{
              queryString= getUrlVars();
              if(queryString.token==undefined ){
                var pageURL = $(location). attr("href");
                window.location = pageURL+'?token='+token+'&show_report=1';
              }    

              if(queryString.show_report ){
                show_main_report();                
              }
            }        

          $("#back_btn").click(function(event) {         
                         
              show_main_report();

          });   
      });

      $('#report_form').on('submit',function(e){
        e.preventDefault();  //prevent form from submitting
          show_main_report();

        }) 

        function show_sub_report(href){
          $('#loader').show();

          $.ajax({url: href, type: "GET", success: function(result){            
                      
            $('#result_div').html(result);
            $('#loader').hide();       
            $("#back_btn").show();
          },
          error(){
            $('#loader').hide();
          }      

          });

        }

        function show_main_report(){
          var requestData = $("#report_form :input").serializeArray();
       
          $('#loader').show();

          $.ajax({url: "drill_down_show", type: "GET", data:requestData,  success: function(result){          

            $('#result_div').html(result);
            $('#loader').hide();
            $("#back_btn").hide();
            $("a").click(function(event) {
              event.preventDefault();                      
              show_sub_report($(this).attr('href'));

            });
          },
          error(){
            $('#loader').hide();
          }      
          
          });

        }

    // Read a page's GET URL variables and return them as an associative array.
      function getUrlVars()
      {
          var vars = [], hash;
          var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for(var i = 0; i < hashes.length; i++)
          {
              hash = hashes[i].split('=');
              vars.push(hash[0]);
              vars[hash[0]] = hash[1];
          }
          return vars;
      }
    </script>

  </body>
</html>