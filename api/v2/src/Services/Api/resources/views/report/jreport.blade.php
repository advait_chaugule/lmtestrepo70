
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
    <title>JReport</title>
  </head>
  <body>
    

    <div class="container">
   <!--  <h1>JReport</h1>
    <div   class="row" >
    <form action="jreport/show" target="_blank" id="report_form" >
  <div class="row">
    <div class="col-sm">
    <select required="required" class="form-control" name="file_type" id="file_type">
    
    @foreach ($list as $key => $val)       
        <option value="{{ $key }}">{{ $val }}</option>       
    @endforeach
    </select>
    </div>
    <div class="col-sm">
    <select required="required" class="form-control" name="document_id" id="document_id">
    
    @foreach ($document_list as $key => $val)       
        <option value="{{ $val['document_id'] }}">{{ $val['title'] }}</option>       
    @endforeach
    </select>
    </div>
    <div class="col-sm">
    <select required="required" class="form-control" name="report_type" id="report_type">
    
    @foreach ($report_list as $key => $val)       
        <option value="{{ $val }}">{{ $val }}</option>       
    @endforeach
    </select>
    </div>
    <div class="col-sm">
    <input type='hidden' name="organization_id" id="organization_id" value="{{ $organization_id }}">
    <input type='hidden' name="access_token" id="access_token" value="">
    <button type="submit" class="btn btn-primary btn-block" >Submit</button>
    </div>
    
  </div>
  </form>
  </div> -->
  <h1>Public Review Report</h1>
    <div   class="row" >
    <form action="jreport/show" target="_blank" id="user_comment_report_form" >
  <div class="row">
    <div class="col-sm">
    <label class="control-label">Format</label>
    <select required="required" class="form-control" name="file_type" id="file_type">
    
    @foreach ($list as $key => $val)      
        @if ($key === 'xlsx') 
        <option value="{{ $key }}" selected='selected' >{{ $val }}</option>    
        @else
        <option value="{{ $key }}">{{ $val }}</option> 
        @endif
    @endforeach
    </select>
    </div>
    <div class="col-sm">
    <label class="control-label">Public Review Taxonomy</label>
    <select required="required" class="form-control" name="document_id" id="document_id">
    
    @foreach ($projectlist as $key => $val)       
        <option value="{{ $val->document_id }}">{{ $val->project_name }}</option>       
    @endforeach
    </select>
    </div>
    <div class="col-sm">
    <label class="control-label">Start Date</label>
    <input type='text' name='from_date' id='from_date' class="form-control">
    </div>
    <div class="col-sm">
    <label class="control-label">End Date</label>
    <input type='text' name='to_date' id='to_date' class="form-control">
    </div>
    
    <div class="col-sm">
    <input type='hidden' name="organization_id" id="organization_id" value="{{ $organization_id }}">
    <input type='hidden' name="access_token" id="access_token" value="">
    <input type='hidden' name="report_type" id="report_type" value="user_comment">
    
    <button type="submit" class="btn btn-primary btn-block" style="margin-top: 32px;" >Submit</button>
    </div>
    
  </div>
  </form>
  </div>

  <h1>User Demographic Detail</h1>
    <div   class="row" >
    <form action="jreport/show" target="_blank" id="user_demographic_detail_form" >
  <div class="row">
    <div class="col-sm">
    <label class="control-label">Format</label>
    <select required="required" class="form-control" name="file_type" id="file_type">
    
    @foreach ($list as $key => $val)      
        @if ($key === 'xlsx') 
        <option value="{{ $key }}" selected='selected' >{{ $val }}</option>    
        @else
        <option value="{{ $key }}">{{ $val }}</option> 
        @endif
    @endforeach
    </select>
    </div>
    
    <div class="col-sm">
    <label class="control-label">Start Date</label>
    <input type='text' name='from_date' id='demographic_from_date' class="form-control">
    </div>
    <div class="col-sm">
    <label class="control-label">End Date</label>
    <input type='text' name='to_date' id='demographic_to_date' class="form-control">
    </div>
    
    <div class="col-sm">
    <input type='hidden' name="organization_id" id="organization_id" value="{{ $organization_id }}">
    <input type='hidden' name="access_token" id="access_token" value="">
    <input type='hidden' name="report_type" id="report_type" value="user_demographic">
    
    <button type="submit" class="btn btn-primary btn-block" style="margin-top: 32px;" >Submit</button>
    </div>
    
  </div>
  </form>
  </div>

  <h1>Association Coverage</h1>
    <div   class="row" >
    <form action="jreport/show" target="_blank" id="association_coverage_form" >
  <div class="row">
    <div class="col-sm">
    <label class="control-label">Format</label>
    <select required="required" class="form-control" name="file_type" id="file_type">
    
    @foreach ($list as $key => $val)      
        @if ($key === 'xlsx') 
        <option value="{{ $key }}" selected='selected' >{{ $val }}</option>    
        @else
        <option value="{{ $key }}">{{ $val }}</option> 
        @endif
    @endforeach
    </select>
    </div>
    
    <div class="col-sm">
    <label class="control-label">Target Taxonomy</label>
    <select required="required" class="form-control" name="target_taxonomy" id="target_taxonomy">
    
    @foreach ($taxonomy as $key => $val)       
        <option value="{{ $val['document_id'] }}">{{ $val['title'] }}</option>       
    @endforeach
    </select>
    </div>
    <div class="col-sm">
    <label class="control-label">Source Taxonomy</label>
    <select required="required" class="form-control" name="source_taxonomy" id="source_taxonomy" multiple>
    
    @foreach ($taxonomy as $key => $val)       
        <option value="{{ $val['document_id'] }}">{{ $val['title'] }}</option>       
    @endforeach
    </select>
    </div>
    
    <div class="col-sm">
    <input type='hidden' name="organization_id" id="organization_id" value="{{ $organization_id }}">
    <input type='hidden' name="access_token" id="access_token" value="">
    <input type='hidden' name="report_type" id="report_type" value="association_coverage">
    
    <button type="submit" class="btn btn-primary btn-block" style="margin-top: 32px;" >Submit</button>
    </div>
    
  </div>
  </form>
  </div>


  <div   class="row" >
  <div class="col-sm my-3" >
  <div class="mx-auto" style="width: 200px;display:none" id='loader'>
  <em class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></em>
  </div>
  
  <div class="col-sm" id="result_div">
  
  </div>
  
  </div>
  </div>
</div>


    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
  
  $( function() {
    $( "#from_date" ).datepicker({ dateFormat: "yy-mm-dd"});
    $( "#to_date" ).datepicker({ dateFormat: "yy-mm-dd"});

    $( "#demographic_from_date" ).datepicker({ dateFormat: "yy-mm-dd"});
    $( "#demographic_to_date" ).datepicker({ dateFormat: "yy-mm-dd"});

    
  } );

    $(document).ready(function(){
            
            var token= localStorage.getItem("access_token");

            if(window.location.hostname=='localhost')
              token= '2cb7b507-3653-469b-aa41-2d08f63951ca';
            if(token == null)
            {
              alert("Please login to access this page");
              window.location = '/';
            }else{
              queryString= getUrlVars();
              if(queryString.token==undefined ){
                var pageURL = $(location). attr("href");
                window.location = pageURL+'?token='+token;
              }             
            }
            
      });

      $('#report_form').on('submit',function(e){
        e.preventDefault();  //prevent form from submitting
        var requestData = $("#report_form :input").serializeArray();
       
        $('#loader').show();

        $.ajax({url: "jreport/show", type: "GET", data:requestData,  success: function(result){
          var fileType=  $('#report_form #file_type').val();
          var document_id=  $('#report_form #document_id').val();
          var report_type=  $('#report_form #report_type').val();
          var organization_id=  $('#report_form #organization_id').val();
         

          if( fileType=='html'){
            $('#result_div').html(result);
          }else{
            
            window.location="jreport/show?file_type="+fileType+"&document_id="+document_id+"&report_type="+report_type+"&organization_id="+organization_id
          }            
           $('#loader').hide();
        },
        error(errorData){
          $('#loader').hide();
        }      
        
        });

        }) 
    // Read a page's GET URL variables and return them as an associative array.
      function getUrlVars()
      {
          var vars = [], hash;
          var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for(var i = 0; i < hashes.length; i++)
          {
              hash = hashes[i].split('=');
              vars.push(hash[0]);
              vars[hash[0]] = hash[1];
          }
          return vars;
      }

      $('#user_comment_report_form').on('submit',function(e){
        e.preventDefault();  //prevent form from submitting
        var requestData = $("#user_comment_report_form :input").serializeArray();
       
        $('#loader').show();

        $.ajax({url: "jreport/show", type: "GET", data:requestData,  success: function(result){
          var fileType=  $('#user_comment_report_form #file_type').val();
          var document_id=  $('#user_comment_report_form #document_id').val();
          var report_type=  $('#user_comment_report_form #report_type').val();
          var organization_id=  $('#user_comment_report_form #organization_id').val();
          var from_date=  $('#user_comment_report_form #from_date').val();
          var to_date=  $('#user_comment_report_form #to_date').val();

          if( fileType=='html'){
            $('#result_div').html(result);
          }else{
            
            window.location="jreport/show?file_type="+fileType+"&document_id="+document_id+"&report_type="+report_type+"&organization_id="+organization_id+"&from_date="+from_date+"&to_date="+to_date
          }            
           $('#loader').hide();
        },
        error(){
          $('#loader').hide();
        }      
        
        });

        }) 

        $('#user_demographic_detail_form').on('submit',function(e){
        e.preventDefault();  //prevent form from submitting
        var requestData = $("#user_demographic_detail_form :input").serializeArray();
       
        $('#loader').show();

        $.ajax({url: "jreport/show", type: "GET", data:requestData,  success: function(result){
          var fileType=  $('#user_demographic_detail_form #file_type').val();
         
          var report_type=  $('#user_demographic_detail_form #report_type').val();
          var organization_id=  $('#user_demographic_detail_form #organization_id').val();
          var from_date=  $('#demographic_from_date').val();
          var to_date=  $('#demographic_to_date').val();

          if( fileType=='html'){
            $('#result_div').html(result);
          }else{
            
            window.location="jreport/show?file_type="+fileType+"&report_type="+report_type+"&organization_id="+organization_id+"&from_date="+from_date+"&to_date="+to_date
          }            
           $('#loader').hide();
        },
        error(){
          $('#loader').hide();
        }      
        
        });

        })

        $('#association_coverage_form').on('submit',function(e){
        e.preventDefault();  //prevent form from submitting
        var requestData = $("#association_coverage_form :input").serializeArray();
        var fileType=  $('#association_coverage_form #file_type').val();
        var report_type=  $('#association_coverage_form #report_type').val();
          var organization_id=  $('#association_coverage_form #organization_id').val();
          var target_taxonomy=  $('#association_coverage_form #target_taxonomy').val();
          var source_taxonomy=  $('#association_coverage_form #source_taxonomy').val();
        if( fileType!='html'){                        
            window.location="jreport/show?file_type="+fileType+"&report_type="+report_type+"&organization_id="+organization_id+"&target_taxonomy="+target_taxonomy+"&source_taxonomy="+source_taxonomy
          }else{

        $('#loader').show();       

        $.ajax({url: "jreport/show", type: "GET", data:requestData,  success: function(result){                 
          if( fileType=='html'){
            $('#result_div').html(result);
          }           
           $('#loader').hide();
        },
        error(){
          $('#loader').hide();
        }      
        
        });

        }

        })
    </script>

  </body>
</html>