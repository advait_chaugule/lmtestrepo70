<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif;  font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
 
    <head> <title>Reset Password</title>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600" rel="stylesheet" type='text/css'>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Nunito:300,400,600');
            @media screen{
                body{
                    margin: 0;
                    padding: 0;
                    min-width: 100%;
                    width: 100% !important;
                }
                img{
                    border: 0;
                    height: auto;
                    line-height: 100%;
                    outline: none;
                    text-decoration: none;
                }
                table{
                    border-collapse: collapse !important;
                }
                .min-width{
                    min-width: 100% !important;
                }
            }
            @media screen and (min-width: 600px){
                .email_txt{
                    padding-right: 15%;
                    width: 100%;
                }
            }
        </style>
    </head>
    <body style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0; background: none repeat scroll 0 0 #ffffff; color:#000000;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed;" class="min-width" role="presentation">
        <tr>
            <td>
                <div style="width:auto; max-width:600px; margin-left:auto; margin-right:auto; margin-top:0; margin-bottom:0; padding-left: 10%; padding-right: 10%;">
                    <table width="100%" cellpadding="0" cellpadding="0" align="center" border="0" role="presentation">
                        <tr>
                            <td style="height: 80px;">&#160;</td>
                        </tr>
                        @if(strpos($domainName,'localhost')!==false)
                        <tr>
                            <td><img src="{{ env('BASE_URL') }}server/logo/logo.png" width="91" height="20" alt="ACMT" role="img" aria-label="ACMT" /></td>
                        </tr>
                        @else
                            <tr>
                                <td><img src="{{$domainName}}server/logo/logo.png" width="91" height="20" alt="ACMT" role="img" aria-label="ACMT" /></td>
                            </tr>
                        @endif
                        <tr>
                            <td style="height: 50px;">&#160;</td>
                        </tr>                    
    
                        <tr>
                            <td>
                                <h1 style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; font-size: 24px; color: #000000;font-weight: 600;" role="heading" aria-label="Confirm your email address on ACMT">Reset your password</h1>
                            </td>
                        </tr>
    
                        <tr>
                            <td style="height: 10px;">&#160;</td>
                        </tr>  
                        
                        <tr>
                            <td class="email_txt" style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; font-weight: 400;font-size:15px;" aria-label="Hello! We just need to verify that gaurav.sharma@learningmate.com is your email address, and then we’ll help you find your space on COMET.">
                                Hello! <span style="color: #3d5afe;text-decoration: underline;">{{ $user_name }}</span>,<br/>
                                You have received a request to reset the password on ACMT. Please click on the button below and follow the prompts. Once complete, you will be able to log in to ACMT again.
                            </td>
                        </tr>

                        <tr>
                            <td style="height: 50px;">&#160;</td>
                        </tr>
    
                        <tr>
                            <td>
                                <a href="{{ $body }}" style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; background:#3d5afe;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;color: #fff;-webkit-box-shadow: -1px 4px 20px 0 rgba(0, 110, 255, 0.34);-moz-box-shadow: -1px 4px 20px 0 rgba(0, 110, 255, 0.34);-o-box-shadow: -1px 4px 20px 0 rgba(0, 110, 255, 0.34);-ms-box-shadow: -1px 4px 20px 0 rgba(0, 110, 255, 0.34);box-shadow: -1px 4px 20px 0 rgba(0, 110, 255, 0.34);text-decoration: none; padding: 10px 35px;" title="Reset Password" role="link" aria-label="Reset Password">Reset Password</a>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="height: 50px;">&#160;</td>
                        </tr>                    
    
                        <tr>
                            <td style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; font-weight: 600;font-size:12px;" aria-label="Didn’t expect this email?">
                                Didn’t expect this email?
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px;"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif;font-size:12px;" aria-label="No worries! Someone might have added your email address by mistake. You can ignore this email for now.">
                                No worries! Someone might have added your email address by mistake. You can ignore this email for now.
                            </td>
                        </tr>
    
                        <tr><td style="height: 100px;">&#160;</td></tr>                         
    
                        <tr>
                            <td style="margin-bottom: 3px; font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif;font-size:12px;" aria-label="Team ACMT">Team ACMT</td>
                        </tr>
                        <tr><td style="height: 3px;"></td></tr> 
                        <tr>
                            <td style="margin-top: 0; font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif;font-size:12px;" aria-label="A LearningMate product.">A LearningMate product.</td>
                        </tr>
    
                        <tr><td style="height: 100px;">&#160;</td></tr>                      
                    </table>
                </div>
            </td>
        </tr>
    </table>


    </body>
</html>