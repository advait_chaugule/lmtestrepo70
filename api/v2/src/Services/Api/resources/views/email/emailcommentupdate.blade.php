<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif;  font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">

    <head> <title>ACMT</title>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600" rel="stylesheet" type='text/css'>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Nunito:300,400,600');
            @media screen{
                body{
                    margin: 0;
                    padding: 0;
                    min-width: 100%;
                    width: 100% !important;
                }
                img{
                    border: 0;
                    height: auto;
                    line-height: 100%;
                    outline: none;
                    text-decoration: none;
                }
                table{
                    border-collapse: collapse !important;
                }
                .min-width{
                    min-width: 100% !important;
                }
            }
            @media screen and (min-width: 600px){
                .email_txt{
                    padding-right: 15%;
                    width: 100%;
                }
            }
        </style>
    </head>
    <body style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0; background: none repeat scroll 0 0 #ffffff; color:#000000;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed;" class="min-width" role="presentation" >
        <tr>
            <td>
                <div style="width:auto; max-width:600px; margin-left:auto; margin-right:auto; margin-top:0; margin-bottom:0; padding-left: 10%; padding-right: 10%; font-size: 14px;">
                    <table width="100%" cellpadding="0" cellpadding="0" align="center" border="0" role="presentation">
                        <tr>
                            <td style="height: 40px;">&#160;</td>
                        </tr>
    
                        <tr>
                            <td><img src="{{ env('BASE_URL') }}server/logo/logo.png" width="91" height="20" alt="ACMT" role="img" aria-label="ACMT" /></td>
                        </tr>
    
                        <tr>
                            <td style="height: 40px;">&#160;</td>
                        </tr>                    
    
                        <tr>
                            <td style="border-bottom:1px solid #dddddd; padding-bottom: 15px;">
                                <p style="font-family: 'Nunito', sans-serif, Arial, Helvetica, sans-serif; font-size:14px;" role="heading" aria-label="Avalon Saldanha assigned you an action item in taxonomy builder">{!! $body !!}<span style="color:#3d5afe;">{!! $project !!}</span></p>
                            </td>
                        </tr>
    
                        <tr>
                            <td style="height: 10px;">&#160;</td>
                        </tr>  
                        
                        <tr>
                            <td aria-label="Avalon Saldanha assigned gaurav.sharma@learningmate.com" style="padding:0 15px;">
                                <!-- <div style="float: left; width: 50px; height: 50px; float: left; border:1px solid #dddddd;">&#160;</div> -->
                                <!-- <div style="font-size:14px;">
                                        <strong style="color: #000000; font-size:15px;">{{ $parentComment[0] }}</strong><br/>
                                        <span style="color:#72809A; display:inline-block; margin: 5px 0;">{{ $parentComment[1] }}</span><br/>
                                        +<span style="color: #3d5afe;text-decoration: underline;">{{ $parentComment[2] }}</span><br/>
                                        <em style="color: #72809A;">Assigned to you</em>
                                </div> -->

                                {!! $parentComment !!}
                                
                            </td>
                        </tr>
						<tr>
                            <td style="height: 20px;">&#160;</td>
                        </tr>
						<!-- <tr>
							<td aria-label="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." style="border-top:1px solid #dddddd; padding:15px; background:#F7F9FA;">
                                <div>
                                        <strong style="color: #000000;">Avalon Saldanha</strong><br/>
										<p style="margin-top:0; font-size: 14px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </td>
                        </tr> -->
                        {!! $commentList !!}
                        <tr>
                            <td style="height: 50px;">&#160;</td>
                        </tr>
    
                        <tr>
                            <td>
                                    <a href="{!! $projectLink !!}" style="background:#3d5afe;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;color: #fff;box-shadow: -1px 4px 20px 0 rgba(0, 110, 255, 0.34);text-decoration: none; padding: 10px 35px;" title="View Comment" role="link" aria-label="View Comment">View Comment</a>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="height: 50px;">&#160;</td>
                        </tr>                    
    
                        <tr>
                            
                        </tr>
    
                        <tr><td style="height: 100px;">&#160;</td></tr>                         
    
                        <tr>
                            <td style="margin-bottom: 3px;" aria-label="Team COMET">Team COMET</td>
                        </tr>
                        <tr><td style="height: 3px;"></td></tr> 
                        <tr>
                            <td style="margin-top: 0;" aria-label="A LearningMate product.">A LearningMate product.</td>
                        </tr>
    
                        <tr><td style="height: 100px;">&#160;</td></tr>                      
                    </table>
                </div>
            </td>
        </tr>
    </table>


    </body>
</html>