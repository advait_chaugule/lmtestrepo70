
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Tenant</title>
  </head>
  <body>
    

    <div class="container">
    <h1>Create Tenant</h1>
    <div   class="row" >
    <form action="" target="_blank" id="report_form" name="report_form">
    <div class="row">
    <div class="col-sm">
  Organization Name : 
    </div>
    <div class="col-sm">
   <input type="text" name="organization_name" id="organization_name">
    </div>
   
    
  </div>
  <br/>
  <div class="row">
    <div class="col-sm">
  Email : 
    </div>
    <div class="col-sm">
   <input type="text" name="admin_account" id="admin_account">
    </div>
   
    
  </div>
  <br/>
  <div class="row">
    <div class="col-sm">
  Admin Name : 
    </div>
    <div class="col-sm">
   <input type="text" name="admin_name" id="admin_name">
    </div>
   
    
  </div>
  <br/>
  <div class="row">
    <div class="col-sm">
  Short Code : 
    </div>
    <div class="col-sm">
   <input type="text" name="shortcode" id="shortcode" maxlength="5" required pattern="[a-zA-Z0-9]+">
    </div>
   
    
  </div>
  <br/>
  <div class="row">
    <div class="col-sm">
  Password : 
    </div>
    <div class="col-sm">
   <input type="password" name="password" id="password">
    </div>
   
    
  </div>
  <br/>
  <div class="row">
    <div class="col-sm">

    </div>
    <div class="col-sm">
   
    </div>
    <div class="col-sm">
    
    </div>
    <div class="col-sm">
    <button type="submit" class="btn btn-primary btn-block" >Submit</button>
    </div>
    
  </div>
  </form>
  </div>
  <div   class="row" >
  <div class="col-sm my-3" >
  <div class="mx-auto" style="width: 200px;display:none" id='loader'>
  <em class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></em>
  </div>
  
  <div class="col-sm" id="result_div">
  
  </div>
  
  </div>
  </div>
</div>


    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script>
    $(document).ready(function(){

            if(localStorage.getItem("access_token") == null)
            {
              alert("Please login to access this page");
              window.location = '/';
            }

      });
      
      $('#shortcode').keypress(function(event){
        
        //get envent value       
        var inputValue = event.which;
        // check whitespaces only.
        if(inputValue == 32 || inputValue == 95 || inputValue == 94){
          event.preventDefault();
        }
         // check number only.
        if(inputValue == 48 || inputValue == 49 || inputValue == 50 || inputValue == 51 || inputValue == 52 || inputValue == 53 ||  inputValue ==  54 ||  inputValue == 55 || inputValue == 56 || inputValue == 57){
            return true;
        }
        // check special char.
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
        }
    })

    $('#report_form').on('submit',function(e){
         
         e.preventDefault();  //prevent form from submitting
 
         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
 
         if($("#organization_name").val() == "" || $("#admin_account").val() == "" || $("#admin_name").val() == "" || $("#password").val() == "" || $("#shortcode").val() == "")
         {
           alert("Please Enter Data in All Fields");
           return false;
         }
         else
         {
           var str = $("#shortcode").val();
           var n = str.length;
          if(n < 5)
          {
            alert("Tenant shortcode Should be 5 characters");
            return false;
          }
          else if (reg.test($("#admin_account").val()) == false) 
         {
             alert('Invalid Email Address');
             return false;
         }
         else
         {
           var data1 = {
                   'organization_name': $("#organization_name").val(),
                   'admin_account' : $("#admin_account").val(),
                   'admin_name' : $("#admin_name").val(),
                   'password' : $("#password").val(),
                   'shortcode' : $("#shortcode").val(),
         };
      
         
         $.ajax({
              type: "POST",
              url: "createOrgnization",
              headers: {
         'Authorization': localStorage.getItem("access_token"),
         'Accept':'application/json'
     },
              data: JSON.stringify(data1),
              contentType: "application/json; charset=utf-8",
              crossDomain: true,
              //dataType: "json",
              success: function(data) {
               if(data.data['result'] == 'exist') 
               {
                  alert(data.message);
               }
               else
               {
                 alert("Tenant Created Successfully");
                 
                 document.getElementById("organization_name").value = "";
                 document.getElementById("admin_account").value = "";
                 document.getElementById("admin_name").value = "";
                 document.getElementById("password").value = "";
                 document.getElementById("shortcode").value = "";
               }
                    console.log(data);
         },
              error: function () {
                  // error handler
                  alert('Error occured');
              }
           });
         }
      
         }
         }) 
    
    </script>

  </body>
</html>