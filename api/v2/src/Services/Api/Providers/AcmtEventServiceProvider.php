<?php

namespace App\Services\Api\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class AcmtEventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Project Event-Listener(s)
        'App\Domains\Project\Events\CreateProjectEvent' => [ 'App\Domains\Project\Listeners\CreateProjectEventListener' ],
        'App\Domains\Project\Events\DeleteProjectEvent' => [ 'App\Domains\Project\Listeners\DeleteProjectEventListener' ],
        'App\Domains\Project\Events\ViewProjectItemsEvent' => [ 'App\Domains\Project\Listeners\ViewProjectItemsEventListener' ],
        'App\Domains\Project\Events\SessionStartedEvent' => [ 'App\Domains\Project\Listeners\SessionStartedEventListener' ],

        // Item Event-Listener(s)
        'App\Domains\Item\Events\CreateItemInsideProjectEvent' => [ 'App\Domains\Item\Listeners\CreateItemInsideProjectEventListener' ],
        'App\Domains\Item\Events\UpdateItemInsideProjectEvent' => [ 'App\Domains\Item\Listeners\UpdateItemInsideProjectEventListener' ],
        'App\Domains\Item\Events\DeleteItemInsideProjectEvent' => [ 'App\Domains\Item\Listeners\DeleteItemInsideProjectEventListener' ],
        'App\Domains\Item\Events\ViewItemInsideProjectEvent' => [ 'App\Domains\Item\Listeners\ViewItemInsideProjectEventListener' ],

        // User Event-Listener(s)
        'App\Domains\User\Events\UserLoginEvent' => [ 'App\Domains\User\Listeners\UserLoginEventListener' ],
        'App\Domains\User\Events\UserLogoutEvent' => [ 'App\Domains\User\Listeners\UserLogoutEventListener' ],
        'App\Domains\User\Events\SessionRefreshedEvent' => [ 'App\Domains\User\Listeners\SessionRefreshedEventListener' ],

        // search related Event-Listener(s)
        'App\Domains\Search\Events\UploadSearchDataToSqsEvent' => [ 'App\Domains\Search\Listeners\UploadSearchDataToSqsEventListener' ],
        'App\Domains\Search\Events\SaveSearchHistoryEvent' => [ 'App\Domains\Search\Listeners\SaveSearchHistoryEventListener' ],

        // App Notification Event listener
        'App\Domains\Notifications\Events\NotificationsEvent' => [ 'App\Domains\Notifications\Listeners\NotificationsEventListeners' ],

         // App Cache Event listener
         'App\Domains\Caching\Events\CachingEvent' => [ 'App\Domains\Caching\Listeners\CachingEventListeners' ],
        // CFPackage Case API
        'App\Domains\CaseCaching\Events\CaseCachingEvent' => [ 'App\Domains\CaseCaching\Listeners\CaseCachingEventListeners' ],
        // CFDocumentsAll
        'App\Domains\CaseCaching\Events\CFDocumentsAllEvent' => [ 'App\Domains\CaseCaching\Listeners\CFDocumentsAllEventListeners' ],
        //CFDocument{identifier}
        'App\Domains\CaseCaching\Events\CFDocumentsEvent' => [ 'App\Domains\CaseCaching\Listeners\CFDocumentsEventListeners' ],
        // CFItem
        'App\Domains\CaseCaching\Events\CFItemEvent' => [ 'App\Domains\CaseCaching\Listeners\CFItemEventListeners' ],

        //Import Event Listener
        'App\Domains\Import\Events\ImportEvent' => [ 'App\Domains\Import\Listeners\ImportEventListeners' ],
        //Import Event Listener for multiple csv
        'App\Domains\Import\Events\ImportMultipleCsvEvent' => [ 'App\Domains\Import\Listeners\ImportMultipleCsvEventListeners' ],
        //Summary Event Listener for multiple csv
        'App\Domains\Import\Events\SummaryMultipleCsvEvent' => [ 'App\Domains\Import\Listeners\SummaryMultipleCsvEventListeners' ],
        // Custom Metadata
        'App\Domains\CaseCaching\Events\CustomMetadataEvent' => [ 'App\Domains\CaseCaching\Listeners\CustomMetadataEventListeners' ],
        // CFConcept
        'App\Domains\CaseCaching\Events\ConceptEvent' => [ 'App\Domains\CaseCaching\Listeners\ConceptEventListeners' ],
        // CFItemType
        'App\Domains\CaseCaching\Events\ItemTypeEvent' => [ 'App\Domains\CaseCaching\Listeners\ItemTypeEventListeners' ],
        // CFLicence
        'App\Domains\CaseCaching\Events\CFLicenceEvent' => [ 'App\Domains\CaseCaching\Listeners\CFLicenceEventListeners' ],
        // CFSubject
        'App\Domains\CaseCaching\Events\CFSubjectEvent' => [ 'App\Domains\CaseCaching\Listeners\CFSubjectEventListeners' ],
        // CFAssociation
        'App\Domains\CaseCaching\Events\CFAssociationsEvent' => [ 'App\Domains\CaseCaching\Listeners\CFAssociationsEventListeners' ],
        // CFItemAssociations
        'App\Domains\CaseCaching\Events\CFItemAssociationsEvent' => [ 'App\Domains\CaseCaching\Listeners\CFItemAssociationsEventListeners' ],
        // UnPublish Taxonomy
        'App\Domains\CaseCaching\Events\UnPublishTaxonomyEvent' => [ 'App\Domains\CaseCaching\Listeners\UnPublishTaxonomyEventListeners' ],
        // Update Project and Taxonomy updated_at column for any activity
        'App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent' => [ 'App\Domains\Taxonomy\UpdateTaxonomyTime\Listeners\UpdateTaxonomyTimeListeners' ],
        // Copy Taxonomy
        'App\Domains\Taxonomy\Events\CopyTaxonomyEvent' => [ 'App\Domains\Taxonomy\Listeners\CopyTaxonomyEventListeners' ],
        // Case Api Background Process
        'App\Domains\Taxonomy\Events\CaseApiBackgroundEvent' => [ 'App\Domains\Taxonomy\Listeners\CaseApiBackgroundEventListeners' ],
        // Case Api Background Process
        'App\Domains\Taxonomy\Events\ExportCSVBackgroundEvent' => [ 'App\Domains\Taxonomy\Listeners\ExportCSVBackgroundEventListeners' ],
        // Subscribed Taxonomy Update Background Process
        'App\Domains\Notifications\Events\SubscribedTaxonomyUpdateEvent' => [ 'App\Domains\Notifications\Listeners\SubscribedTaxonomyUpdateEventListeners' ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
