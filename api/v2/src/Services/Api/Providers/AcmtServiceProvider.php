<?php

namespace App\Services\Api\Providers;

use Illuminate\Support\ServiceProvider;

class AcmtServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( 'App\Data\Repositories\Contracts\CompetencyFrameworkRepositoryInterface', 'App\Data\Repositories\CompetencyFrameworkEloquentRepository' );
        $this->app->bind( 'App\Data\Repositories\Contracts\ProjectRepositoryInterface', 'App\Data\Repositories\ProjectEloquentRepository' );
        $this->app->bind( 'App\Data\Repositories\Contracts\WorkflowRepositoryInterface', 'App\Data\Repositories\WorkflowEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\RoleRepositoryInterface', 'App\Data\Repositories\RoleEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\PermissionRepositoryInterface', 'App\Data\Repositories\PermissionEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\NodeTypeRepositoryInterface', 'App\Data\Repositories\NodeTypeEloquentRepository');

        // For Refactor
        $this->app->bind( 'App\Data\Repositories\Contracts\UserRepositoryInterface', 'App\Data\Repositories\UserEloquentRepository' );
        $this->app->bind( 'App\Data\Repositories\Contracts\DocumentRepositoryInterface', 'App\Data\Repositories\DocumentEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\ItemRepositoryInterface', 'App\Data\Repositories\ItemEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface', 'App\Data\Repositories\ItemAssociationEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface', 'App\Data\Repositories\AssociationGroupEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\SubjectRepositoryInterface', 'App\Data\Repositories\SubjectEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\ConceptRepositoryInterface', 'App\Data\Repositories\ConceptEloquentRepository' );
        $this->app->bind( 'App\Data\Repositories\Contracts\LicenseRepositoryInterface', 'App\Data\Repositories\LicenseEloquentRepository' );
        $this->app->bind( 'App\Data\Repositories\Contracts\LanguageRepositoryInterface', 'App\Data\Repositories\LanguageEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\ItemTypeRepositoryInterface', 'App\Data\Repositories\ItemTypeEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface', 'App\Data\Repositories\WorkflowStageEloquentRepository');

        // Report related repositories (these repositories will be based on models that is part of separate Reporting database)
        $this->app->bind( 'App\Data\Repositories\Contracts\ActivityLogRepositoryInterface', 'App\Data\Repositories\ActivityLogEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\DimUserRepositoryInterface', 'App\Data\Repositories\DimUserEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\DimItemRepositoryInterface', 'App\Data\Repositories\DimItemEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\DimProjectRepositoryInterface', 'App\Data\Repositories\DimProjectEloquentRepository');
        
        // $this->app->singleton(
        //     'App\Repositories\User\UserRepositoryInterface',
        //     'App\Repositories\User\UserEloquentRepository'
        // );

        //Comment Related Repositories
        $this->app->bind( 'App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface', 'App\Data\Repositories\ThreadCommentEloquentRepository');
        $this->app->bind( 'App\Data\Repositories\Contracts\ThreadRepositoryInterface', 'App\Data\Repositories\ThreadEloquentRepository');
        
        //Metadata related repositories
        $this->app->bind( 'App\Data\Repositories\Contracts\MetadataRepositoryInterface', 'App\Data\Repositories\MetadataEloquentRepository');

        //NodeType related repositories
        $this->app->bind( 'App\Data\Repositories\Contracts\NodeTypeRepositoryInterface', 'App\Data\Repositories\NodeTypeEloquentRepository');

        //NodeTemplate related repositories
        $this->app->bind( 'App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface', 'App\Data\Repositories\NodeTemplateEloquentRepository');

        $this->app->bind( 
            'App\Data\Repositories\Contracts\ProjectAccessRequestRepositoryInterface', 
            'App\Data\Repositories\ProjectAccessRequestEloquentRepository'
        );

        $this->app->bind( 
            'App\Data\Repositories\Contracts\AssetRepositoryInterface', 
            'App\Data\Repositories\AssetEloquentRepository'
        );

        $this->app->bind( 
            'App\Data\Repositories\Contracts\OrganizationRepositoryInterface', 
            'App\Data\Repositories\OrganizationEloquentRepository'
        );

        $this->app->bind( 
            'App\Data\Repositories\Contracts\StageRepositoryInterface', 
            'App\Data\Repositories\StageEloquentRepository'
        );

        $this->app->bind( 
            'App\Data\Repositories\Contracts\JReportRepositoryInterface', 
            'App\Data\Repositories\JReportEloquentRepository'
        );

        $this->app->bind( 
            'App\Data\Repositories\Contracts\LinkedServerRepositoryInterface', 
            'App\Data\Repositories\LinkedServerEloquentRepository'
        );

        $this->app->bind( 
            'App\Data\Repositories\Contracts\SearchHistoryEloquentRepositoryInterface', 
            'App\Data\Repositories\SearchHistoryEloquentRepository'
        );

        $this->app->bind(
            'App\Data\Repositories\Contracts\NotificationInterface',
            'App\Data\Repositories\NotificationEloquentRepository'
        );

        $this->app->bind(
            'App\Data\Repositories\Contracts\NoteRepositoryInterface',
            'App\Data\Repositories\NoteEloquentRepository'
        );

        $this->app->bind(
            'App\Data\Repositories\Contracts\ImportJobRepositoryInterface',
            'App\Data\Repositories\ImportJobEloquentRepository'
        );
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'App\Data\Repositories\Contracts\CompetencyFrameworkRepositoryInterface',
            'App\Data\Repositories\Contracts\ProjectRepositoryInterface',
            'App\Data\Repositories\Contracts\WorkflowRepositoryInterface',
            'App\Data\Repositories\Contracts\RoleRepositoryInterface',
            'App\Data\Repositories\Contracts\PermissionRepositoryInterface',
            'App\Data\Repositories\Contracts\NodeTypeRepositoryInterface',
            
            //For Document Refactor
            'App\Data\Repositories\Contracts\UserRepositoryInterface',
            'App\Data\Repositories\Contracts\DocumentRepositoryInterface',
            'App\Data\Repositories\Contracts\ItemRepositoryInterface',
            'App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface',
            'App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface',
            'App\Data\Repositories\Contracts\SubjectRepositoryInterface',
            'App\Data\Repositories\Contracts\ConceptRepositoryInterface',
            'App\Data\Repositories\Contracts\LicenseRepositoryInterface',
            'App\Data\Repositories\Contracts\LanguageRepositoryInterface',
            'App\Data\Repositories\Contracts\ItemTypeRepositoryInterface',
            'App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface',
            'App\Data\Repositories\Contracts\ActivityLogRepositoryInterface',
            'App\Data\Repositories\Contracts\DimUserRepositoryInterface',
            'App\Data\Repositories\Contracts\DimItemRepositoryInterface',
            'App\Data\Repositories\Contracts\DimProjectRepositoryInterface',

            //Comment Repositories
            'App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface',
            'App\Data\Repositories\Contracts\ThreadRepositoryInterface',
            
            //Metadata related
            'App\Data\Repositories\Contracts\MetadataRepositoryInterface',

            //NodeType related
            'App\Data\Repositories\Contracts\NodeTypeRepositoryInterface',

            //NodeTemplate related
            'App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface',

            'App\Data\Repositories\Contracts\ProjectAccessRequestRepositoryInterface',
            'App\Data\Repositories\Contracts\AssetRepositoryInterface',
            'App\Data\Repositories\Contracts\OrganizationRepositoryInterface',
            'App\Data\Repositories\Contracts\StageRepositoryInterface',

            //Linked Server
            'App\Data\Repositories\Contracts\LinkedServerRepositoryInterface',

            'App\Data\Repositories\Contracts\AssetRepositoryInterface',
            
            'App\Data\Repositories\Contracts\SearchHistoryEloquentRepositoryInterface',

            'App\Data\Repositories\Contracts\NoteRepositoryInterface',

            'App\Data\Repositories\Contracts\ImportJobRepositoryInterface',

        ];
    }
}
