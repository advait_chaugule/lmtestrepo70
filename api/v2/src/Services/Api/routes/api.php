<?php

/*
  |--------------------------------------------------------------------------
  | Service - API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for this service.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// Prefix: /api/api
// 'auth:api' add this to include middleware to handle oauth authetntication

Route::group(['prefix' => 'v1'], function() {

    
    // (currently publicly available)
    Route::get('', 'DataMockupController@getListPeople');
    Route::post('sendEmail','EmailController@sendEmail');
    Route::post('validateLogin', 'UserController@validateLogin');
    Route::get('register/{active_access_token}', 'UserController@getbytoken');
    Route::post('registerComplete', 'UserController@registercomplete');
    Route::post('forgotPassword', 'UserController@forgotPassword');  // 
    Route::post('reforgotPassword', 'UserController@reforgotPassword');  // 
    Route::get('checkResetPasswordToken/{reset_request_token}', 'UserController@isResetPasswordTokenValid');  // 
    Route::post('resetPassword', 'UserController@resetPassword');  // 
    Route::put('user/check_user_status/{token_id}/{organization_id}','UserActiveStatusController@checkActiveStatus');
    Route::post('set-user', 'UserController@cisSetUser');
    Route::post('loginUser', 'UserController@cisLogin');
    // All private api(s) can be accessed using a token
    Route::get('downloadTaxonomy/{key}','TaxonomyController@downloadTaxonomy');
     
    Route::middleware(['authenticate-access-token'])->group(function() {


        Route::get('logout', 'UserController@logout');
        

       

        Route::middleware(['permission-handler'])->group(function() {
            Route::group(["prefix" => "users"], function() {
                Route::get('', 'UserController@listing');   // DONE
                Route::post('', 'UserController@create');   // DONE
                Route::put('{user_id}', 'UserController@userEdit');  // DONE
                Route::delete('{user_id}', 'UserController@userDelete'); // DONE
                Route::post('status/{user_id}', 'UserController@updateStatus'); // DONE
            });
            
    
            Route::group(["prefix" => "cfdoc"], function() {
                //Route::post('create', 'DocumentController@create');   // DONE
                Route::delete('delete/{document_id}', 'DocumentController@destroy');   // DONE
            });
    
            Route::group(["prefix" => "cfitem"], function() {
                Route::post('create', 'CFItemController@create');         // DONE
                Route::put('edit/{item_id}', 'CFItemController@update');  // DONE
            });
    
            Route::group(["prefix" => "taxonomy"], function() {
                Route::get('list', 'DocumentController@list');                                // DONE
                Route::delete('delete/{item_id}', 'CFItemController@delete');                 // DONE
                Route::get('getTreeHierarchy/{document_id}', 'CFPackadgeController@getTreeHierarchy'); // DONE
                Route::post('', 'CFPackadgeController@createTaxonomy'); // DONE
            });

            Route::post('importCFPackageJson', 'CFPackadgeController@importFromJson'); // DONE
    
            Route::group(["prefix" => "projects"], function() {
                Route::get('', 'ProjectController@showAll');               // DONE
                Route::post('', 'ProjectController@create');               // DONE
                Route::put('{project_id}', 'ProjectController@update');    // DONE
                Route::delete('{project_id}', 'ProjectController@delete'); // DONE
    
                Route::get('users/{project_id}', 'ProjectController@showUsers');                        // DONE
                Route::post('users/assign/{project_id}', 'ProjectController@AssignProjectUser');        // DONE
                Route::post('users/unassign/{project_id}', 'ProjectController@UnAssignProjectUser');    // DONE
    
                Route::post('{project_id}/workflowStage', 'ProjectController@UpdateProjectWorflowStage');  // DONE
                Route::get('nodes/{project_id}', 'ProjectController@associatedNodes');          // DONE       
            });
            
            
            // various dropdown options for the ui
            Route::get('getCFMasterData', 'CFDocController@getCFMasterData'); // DONE
    
            /**
            * Role Management routes
            */
            Route::group(["prefix" => "roles"], function() {
                Route::get('', 'RoleController@index'); // DONE
                Route::post('', 'RoleController@create'); // DONE
                Route::put('{role_id}', 'RoleController@update'); // DONE
                Route::post('status/{role_id}', 'RoleController@updateStatus'); // DONE
                Route::delete('{role_id}', 'RoleController@destroy'); // DONE
                Route::post('permissions/{role_id}', 'RoleController@setPermission'); //DONE
            });
    
            /**
            * Association Management routes
            */
            Route::group(["prefix" => "caseAssociation"], function() {
                // create associations between cfitem or cfdocument
                Route::post('create', 'CaseAssociationController@createAssociation');                      // DONE
                // delete the case association
                Route::delete('delete/{association_id}', 'CaseAssociationController@deleteAssociation');   // DONE
                // edit the case association
                Route::put('edit/{association_id}', 'CaseAssociationController@updateAssociation');        // DONE
            });
    
            // export has nothing to do with CASE
            Route::get('exportCFPackageJson/{documentId}', 'CFPackadgeController@exportToJson');
    
            Route::get('exportTaxonomyPdf/{documentId}', 'JReportController@exportTaxonomyPdf');


            //Metadata related routes
            Route::group(["prefix" => "metadata"], function() {
                Route::get('', 'MetadataController@index');
                Route::post('', 'MetadataController@create');
                Route::put('{metadata_id}', 'MetadataController@edit');     
                Route::delete('{metadata_id}', 'MetadataController@destroy');       
            });

            Route::group(["prefix" => "nodeTypes"], function() {
                Route::get('', 'NodeTypeController@index');
                Route::post('', 'NodeTypeController@create');
                Route::put('{node_type_id}', 'NodeTypeController@edit');
                Route::delete('{node_type_id}', 'NodeTypeController@destroy');
                Route::post('duplicate/{node_type_id}', 'NodeTypeController@duplicateNodetype');
            });

            Route::group(["prefix" => "itemComment"], function() {
                Route::get('{thread_source_id}', 'CommentController@index');
                Route::post('{thread_source_id}', 'CommentController@create');
                Route::put('{thread_comment_id}', 'CommentController@edit');
                Route::delete('{thread_comment_id}', 'CommentController@destroy');
            });

            Route::group(["prefix" => "nodeTemplate"], function() {
                Route::get('', 'NodeTemplateController@index');
                Route::post('', 'NodeTemplateController@create');
                Route::put('{node_template_id}', 'NodeTemplateController@edit');
                Route::delete('{node_template_id}', 'NodeTemplateController@destroy');
            });
        });


        Route::group(["prefix" => "users"], function() {
            Route::post('user_info/{user_id}', 'UserController@setUserInfo');          
            Route::get('user_info', 'UserController@getUserInfo');
            Route::post('resendMail', 'UserController@resendMail');// resend mail for users that are inactive
        });
        
        Route::group(["prefix" => "projects"], function() {
            Route::get('nodesWithTaxonomyTree/{project_id}', 'ProjectController@mappedNodesNew'); 
            Route::delete('delete/nodes/{project_id}', 'ProjectController@deleteAssociatedNodes');   
            Route::post('logSessionActivity/{project_id}', 'ProjectController@logSessionActivity');
            Route::get('pacingNodes/{project_id}', 'ProjectController@pacingNodes');

            Route::get('getProjectHierarchy/{project_id}', 'ProjectController@getProjectHierarchy');
            Route::get('getProjectDetails/{project_id}', 'ProjectController@getProjectDetails');
        });

        Route::post('importTaxonomy', 'CFPackadgeController@importTaxonomyFromJson');
        Route::get('getStatusForBack/{import_job_id}', 'CFPackadgeController@getimportStatusFromImportJob');
        //Notifications
        Route::get('notifications','NotificationController@show');
        Route::Post('set_notifications_read','NotificationController@create');
        
        Route::get('notification_count','NotificationController@getCount');
        Route::get('org','OrgTestController@getData');
        Route::put('user','UserController@updateProfile');
        Route::put('user/password','UserController@changePassword');
        Route::get('user','UserController@getEditedProfile');
        Route::post('user/upload','UserController@createUserViaCSV');

        //User organization
        Route::get('user/orgs','UserOrganizationController@getUserOrgList');
        Route::post('user/set_org','UserOrganizationController@setUserOrgList');

        Route::group(["prefix" => "users"], function() {
            Route::get('{user_id}', 'UserController@getUserDetails');
        });
    
        Route::group(["prefix" => "cfdoc"], function() {
            Route::get('{id}', 'DocumentController@getbyid'); 
            Route::put('edit/{document_id}', 'DocumentController@update'); 
            Route::get('checkProject/{document_id}', 'DocumentController@checkProject');
        });
    
        Route::group(["prefix" => "cfitem"], function() {
            Route::get('fetch/{item_id}', 'CFItemController@fetch');
        });
    
        Route::group(["prefix" => "taxonomy"], function() {
            Route::get('filter/{order_by}', 'DocumentController@search', '?search_key');
            
            Route::post('setTreeHierarchy/{document_id}', 'CFPackadgeController@setTreeHierarchy');
            Route::post('setPacingTreeHierarchy/{document_id}', 'CFPackadgeController@setPacingTreeHierarchy');
            Route::post('setForPublicReview/{document_id}', 'CFPackadgeController@setForPublicReview');
            Route::get('getProjectStatusToPublish/{document_id}', 'CFPackadgeController@getProjectStatusToPublish');
            Route::post('setForPublish/{document_id}', 'CFPackadgeController@setForPublish');
            Route::post('publish', 'DocumentController@publish');
            Route::get('PublishHistory/{document_id}', 'DocumentController@PublishHistory');
            Route::post('setPublishDocumentToCache/{document_id}', 'CFPackadgeController@setPublishDocumentToCache');
            Route::get('getPublishDocument/{document_id}', 'CFPackadgeController@getPublishDocument');
            Route::get('publicReviewHistory/{document_id}', 'CFPackadgeController@publicReviewHistory');
            Route::post('setPublishDocumentListToCache', 'CFPackadgeController@setPublishDocumentListToCache');
            Route::post('stopPublicReview', 'CFPackadgeController@stopPublicReview');
            Route::get('downloadTaxonomy/{uuid}', 'CFPackadgeController@getTaxonomyDownload');
            Route::post('CopyNodes', 'CFPackadgeController@copyNodes');
             
            Route::get('group', 'TaxonomyController@getGroupForTaxonomyList');
            Route::get('group/{group_id}', 'TaxonomyController@getGroupForTaxonomy');
            Route::put('group', 'TaxonomyController@updateGroupForTaxonomy');
            Route::post('group', 'TaxonomyController@createGroupForTaxonomy');
            Route::delete('group', 'TaxonomyController@deleteGroupForTaxonomy');
            Route::post('add_group', 'TaxonomyController@addTaxonomyToGroup');
            Route::delete('delete_group', 'TaxonomyController@deleteTaxonomyFromGroup');
            Route::get('get_group', 'TaxonomyController@getTaxonomyFromGroup');
           
        });
    
        Route::post('seq-no-mig/{org_code}', 'SequenceNoController@SequenceNoMigration');
        Route::post('seq-no-le-mig/{type}/{code}', 'SequenceNoController@SequenceNoMigrationLE');
        Route::post('delete-dup-asso/{document_id}', 'MigrationApiController@DeleteDuplicateAssociationMigration');
        
        Route::post('uuid-migration/{org_code}', 'UUIDUpdateDocumentController@UUIDMigration');
        Route::post('cacheToS3/{org_code}', 'UUIDUpdateDocumentController@cacheToS3Migration');

        Route::group(["prefix" => "projects"], function() {
            Route::get('{project_id}', 'ProjectController@show'); 
            Route::post('mapTaxonomies', 'ProjectController@mapTaxonomies');    
            Route::get('authoredNodes/{project_id}', 'ProjectController@authoredNodes');
            Route::get('comments/{project_id}', 'CommentController@getProjectUserComments');
        });

        Route::group(["prefix" => "pacing_guide"], function() {
            Route::get('preview/{project_id}', 'PacingGuideController@preview'); 
            Route::post('publish', 'PacingGuideController@publish');    
        });

        Route::get('taxonomyProjectComments/{identifier}/{identifier_type}', 'ProjectController@listComments');
    
        Route::group(["prefix" => "workflows"], function() {
            Route::get('roles/{workflow_id}', 'WorkflowController@GetAllWorkflowRoles');     
        });
    
        Route::group(["prefix" => "roles"], function() {
            Route::get('{role_id}', 'RoleController@show');
        });
    
        Route::group(["prefix" => "caseAssociation"], function() {
            Route::get('listTypes', 'CaseAssociationController@listCaseAssociationTypes');
            // create association of type exemplar
            Route::post('exemplar', 'CaseAssociationController@createExemplarAssociation');  
            // delete association of type exemplar
            Route::delete('exemplar/{association_id}', 'CaseAssociationController@deleteExemplarAssociation');  
        });
    
        Route::group(["prefix" => "permissions"], function() {
            Route::get('{role_id}', 'PermissionController@index'); 
        });
    
        Route::group(["prefix" => "report"], function() {
            // fetch report dashboard data
            Route::get('getDashboardData', 'ReportController@getDashboardData');
            // fetch compliance report for taxonomy
            Route::get('compliance/{identifier}/{identifier_type}', 'ReportController@getComplianceReport');
            // fetch comment report
            Route::get('get_comment_report/{project_id}', 'ReportController@getCommentReport');
            // fetch itme complicance for project and taxonomy
            Route::get('itemCompliance/{identifier}/{identifier_type}', 'ReportController@getItemComplianceReport');
            // fetch list of Jasper Reports
            Route::get('list', 'JReportController@listJasperReport');
            // view Jasper Report Dynamically
            Route::get('view', 'JReportController@viewJasperReport');
            // export coverage report
            Route::post('coverage', 'JReportController@coverageReport');
        });
    
        Route::get('process-activity-log', 'RemoveLaterController@removeLater');  
    
        Route::group(["prefix" => "nodeTypes"], function() {
            Route::get('{node_type_id}', 'NodeTypeController@show');
            //Route::post('duplicate/{node_type_id}', 'DataMockupController@duplicateNodeType');
        });

        Route::group(["prefix" => "nodeTypeMetadata"], function() {
            Route::get('{node_type_id}', 'NodeTypeController@getNodeTypeMetadata');
            Route::post('{node_type_id}', 'NodeTypeController@saveNodeTypeMetadata');
        });
    
        Route::group(["prefix" => "metadata"], function() {
            Route::get('{metadata_id}', 'MetadataController@show');
            Route::get('{metadata_id}/{field_possible_value}', 'MetadataController@checkMetadataValueUsage');
        });


        Route::group(["prefix" => "projectAccessRequests"], function() {
           // Route::get('', 'DataMockupController@getAllProjectAccessRequests');
           // Route::get('{project_access_request_id}', 'DataMockupController@getProjectAccessRequest');
            Route::post('', 'ProjectController@createProjectAccessRequest');
            Route::get('', 'ProjectController@getAllProjectAccessRequests');
            Route::get('{project_access_request_id}', 'ProjectController@getAllProjectAccessRequests');
            Route::put('{project_access_request_id}', 'ProjectController@updateProjectAccessRequest');
            //Route::put('{project_access_request_id}', 'DataMockupController@updateProjectAccessRequest');
            Route::delete('{project_access_request_id}', 'DataMockupController@deleteProjectAccessRequest');

        });

        Route::group(["prefix" => "proxy"], function() {
            Route::get('protip/{s3_bucket_name}', 'ProxyController@getProtipJson');
            Route::get('walkthrough/{s3_bucket_name}', 'ProxyController@getWalkthroughJson');
            Route::get('inputControls/{s3_bucket_name}', 'ProxyController@getInputControlsJson');
        });

        Route::group(["prefix" => "itemComment"], function() {
            Route::get('archive/{thread_source_id}', 'CommentController@listArchive');
        });
    
        Route::group(["prefix" => "nodeTemplate"], function() {
            Route::get('{node_template_id}', 'NodeTemplateController@show');
        });
    
        Route::group(["prefix" => "language"], function() {
            Route::get('list', 'LanguageController@getList');
        });

        Route::group(['prefix' => 'asset'], function() {
            Route::post('', 'AssetController@create');
            Route::put('/reorder','AssetController@reorderFiles');
            Route::put('{asset_id}','AssetController@updateAsset');
            Route::delete('{asset_id}', 'AssetController@delete'); 
            Route::get('{item_linked_id}', 'AssetController@list');
        });

        Route::group(["prefix" => "cfitem"], function() {
            Route::post('additional/metadata/{item_id}', 'CFItemController@createAdditionalMetadata');
            Route::put('additional/metadata/{item_id}', 'CFItemController@updateAdditionalMetadata');
            Route::delete('additional/metadata/{item_id}/{metadata_id}', 'CFItemController@destroyAdditionalMetadata');
            Route::post('createAssociations','CFItemController@createMultipleAssociation');
        });

        Route::group(["prefix" => "cfdoc"], function() {
            Route::post('additional/metadata/{document_id}', 'DocumentController@createAdditionalMetadata');
            Route::put('additional/metadata/{document_id}', 'DocumentController@updateAdditionalMetadata');
            Route::delete('additional/metadata/{document_id}/{metadata_id}', 'DocumentController@destroyAdditionalMetadata');
        });

        Route::group(["prefix" => "system"] , function (){
            Route::get('config/{feature_id}/{setting_id}',"SystemSettingController@getSystemSetting");
            Route::put("config","SystemSettingController@setSystemSetting");
        });

        Route::group(["prefix" => "association"] , function (){
            Route::post("preset","AssociationController@createAssociationPreset");
            Route::delete("preset","AssociationController@deleteAssociationPreset");
            Route::put("preset","AssociationController@updateAssociationPreset");
            Route::get('preset', "AssociationController@getAssociationPresetList");
            Route::get('preset/{source_taxonomy_type_id}/{target_taxonomy_type_id}/{association_type}', "AssociationController@getAssociationPreset");

            #get individual association metadata value
            Route::get('metadata/{item_association_id}/{association_type_id}/{item_id}', "AssociationController@getAssociationMetadata");
            Route::delete('metadata/{item_association_id}/{metadata_id}/{is_additioanl}', "AssociationController@deleteAssociationMetadata");
        });

        Route::group(["prefix" => "item"] , function (){
            Route::post("association/metadata","AssociationController@createItemAssociationMetadata");
            Route::put("association/metadata","AssociationController@updateItemAssociationMetadata");
        });

        Route::group(["prefix" => "taxonomy"], function() {
            Route::post('create-taxonomy-search-data', 'TaxonomyController@createAndUploadTaxonomySearchData');
            Route::get('search', 'TaxonomyController@search');
            Route::get('unmappedTaxonomy/{document_id}', 'TaxonomyController@getUnmappedTaxonomy');
            Route::get('getTaxonomyHierarchy/{document_id}','TaxonomyController@getAuthPublishDocumentFromCache');
            Route::put('users/assign/{project_id}', 'TaxonomyController@editAssignProjectUser');
            Route::get('export/{documentId}','TaxonomyController@exportTaxonomy');
            Route::get('node_type/{document_id}/{type}/{view_type}','TaxonomyController@getNodeDetails');
        });
        
        Route::post('importTaxonomyFromCsv', 'CFPackadgeController@importFromCsv'); 
        Route::post('importTaxonomiesFromCsv', 'CFPackadgeController@importFromMultipleCsv');
        Route::delete('cancel-csv-import-batch/{batch_id}', 'CFPackadgeController@cancelCsvBatch');
        
        Route::get('exportTaxonomyPdf/{documentId}', 'JReportController@exportTaxonomyPdf');
        Route::get('statistics/{documentId}', 'JReportController@statistics');

        Route::group(["prefix" => "workflows"], function() {
            Route::post('stage/{workflow_id}', 'WorkflowController@stageAdd');
            Route::post('edit/{workflow_id}', 'WorkflowController@editWorkflow'); 
            Route::delete('stageDelete/{workflow_stage_id}', 'WorkflowController@stageDelete');
            Route::get('', 'WorkflowController@index');                                                             // DONE
            Route::get('{workflow_id}', 'WorkflowController@GetWorkflow'); 
            Route::post('create', 'WorkflowController@create'); 
            Route::post('{workflow_id}', 'WorkflowController@edit');  
            Route::post('copy/{workflow_id}','WorkflowController@duplicateWorkflow');                                        // DONE
            Route::post('stages/{workflow_stage_id}', 'WorkflowController@UpdateWorkflowStage');                    // DONE    
            Route::get('stages/{workflow_stage_id}', 'WorkflowController@GetAllWorflowStages');                     // DONE
            Route::delete('stageRoleDelete/{workflow_stage_id}/{role_id}', 'WorkflowController@stageRoleDelete');   // DONE         
            Route::delete('delete/{workflow_id}', 'WorkflowController@workflowDelete');           
        }); 

        Route::group(["prefix" => "link"], function() {
            
            Route::post('create', 'LinkedServerController@create');
            Route::post('update/{linked_server_id}', 'LinkedServerController@update');
            Route::get('listAll', 'LinkedServerController@getList');
            Route::get('list/{linked_server_id}', 'LinkedServerController@getList');
            Route::delete('delete/{linked_server_id}', 'LinkedServerController@deleteLinkServer');
            Route::get('listTaxonomy/{linked_server_id}', 'LinkedServerController@getTaxonomyList');
        });

        Route::group(["prefix" => "search"], function() {
            Route::post('', 'SearchController@search');
            Route::post('save', 'SearchController@save');
            Route::get('search-history', 'SearchController@getHistoryList');
        });
        Route::post('importCFPackageApi', 'CFPackadgeController@importFromApi');    

        Route::group(["prefix" => "document"], function() {
            Route::get('user-comments/{document_id}', 'CommentController@getDocumentUserComments');
        });

        Route::post('submit_review','PublicReviewController@submitReview');

        Route::group(["prefix" => "notes"], function() {
            //creating a note
            Route::post('','NoteController@create');

            // reorder notes
            Route::put('/reorder','NoteController@reorder');

            //display a note
            Route::get('{note_id}','NoteController@show');
            // edit a note
            Route::put('/{note_id}','NoteController@edit');
            // destroy a note
           Route::delete('/{note_id}','NoteController@destroy');

            // get all notes
            Route::get('','NoteController@index');
        });

        Route::group(["prefix" => "taxonomy"], function() {
            //get a child list as per required
            Route::get('child-hierachy/{node_id}','DocumentController@geHierarchy');
        });

        Route::get('taxonomy_custom_view/{document_id}/{item_id}', 'JReportController@custom_view');
        
        Route::get('exportCustomView/{document_id}/{file_type}', 'JReportController@exportCustomView');
        
        Route::group(['prefix' => 'user'], function() {
             Route::post('createTenant', 'UserController@createOrgnization');  // 
        });

        Route::group(['prefix' => 'log'], function() {
            Route::post('fetchLogData', 'LogController@fetchLogData');  // 
       });

        Route::group(['prefix' => 'org'], function() {
            Route::get('OrgList', 'TenentController@orgList');
            Route::get('GetOrgDetails', 'TenentController@editOrg');
            Route::put('UpdateOrg', 'TenentController@updateOrg');
        });

        #get metadata fields from a set of uuids and metadata field names
        Route::post('item/metadata', 'MultipleItemDetailsController@list');

        
        
         #to update refresh token
         Route::put('refreshToken', 'UserController@updateToken');
         
        Route::post('default-setting-org', 'SystemSettingController@setDefaultSettingOrg');
        
        Route::group(["prefix" => "{org_code}"], function() {
            Route::post('subscription','SubscribeController@createSubscription');
            Route::post('compareVersion', 'CompareController@compareVersion');   
            Route::get('getVersion','SubscribeController@getSubscribedTaxonomyVersion');                    
        });
        Route::put('changeStatus','SubscribeController@updateSubscription');
        Route::delete('deleteSubscription','SubscribeController@deleteSubscription');
        Route::get('comparison-summary-report', 'CompareController@getComparisonSummaryReport');
        Route::post('versionNodeDetails', 'CompareController@getNodeDetailsFromPublishedJson'); 
    });

    
    Route::post('callBackFromNodejsForCsv', 'CFPackadgeController@CallBackFromNodejsForCsv');


    Route::group(['prefix' => 'asset'], function() {
        // public api to preview exemplar asset files
        Route::get('preview/exemplar/{asset_target_file_name}', 'ProxyController@fetchAssetFileFromS3')->name('exemplar');
        // public api to download exemplar asset files
        Route::get('download/exemplar/{asset_target_file_name}', 'ProxyController@fetchAssetFileFromS3')->name('exemplar');
        // public api to preview asset files on document/item level
        Route::get('preview/{asset_target_file_name}', 'ProxyController@fetchAssetFileFromS3')->name('doc_item');
        // public api to download asset files
        Route::get('download/{asset_target_file_name}', 'ProxyController@fetchAssetFileFromS3')->name('doc_item');
    });
    

    /**
     * CASE API routes (currently publicly available)
     */
//    Route::group(["prefix" => "ims/case/v1p0"], function() {
//        Route::get('CFItems/{identifier}', 'CaseController@getCFItem');  // DONE
//        Route::get('CFDocuments', 'CaseController@getAllCFDocuments');      // DONE
//        Route::get('CFDocuments/{identifier}', 'CaseController@getCFDocument'); // DONE
//        Route::get('CFPackages/{identifier}', 'CaseController@getCFPackage');
//        Route::get('CFConcepts/{identifier}', 'CaseController@getCFConcept');   // DONE
//        Route::get('CFItemTypes/{identifier}', 'CaseController@getCFItemType'); // DONE
//        Route::get('CFLicenses/{identifier}', 'CaseController@getCFLicense');   // DONE
//        Route::get('CFSubjects/{identifier}', 'CaseController@getCFSubject');   // DONE
//        Route::get('CFAssociations/{identifier}', 'CaseController@getCFAssociation');   // DONE
//        Route::get('CFItemAssociations/{identifier}', 'CaseController@getCFItemAssociations'); // DONE
//        Route::get('CFAssociationGroupings/{identifier}', 'CaseController@getCFAssociationGrouping'); // DONE
//        Route::get('CustomMetadata/{identifier}','CaseController@getCustomMetadata');
//    });
    // case api with tenant specific
    // Middleware to allow api only if org is active
    Route::middleware(['orgactive-handler'])->group(function() {
        Route::group(["prefix" => "{org_code}/ims/case/v1p0"], function() {
            Route::get('CFDocuments', 'CaseController@getAllCFDocuments');      // DONE
            Route::get('CFDocuments/{identifier}', 'CaseController@getCFDocument'); // DONE
            Route::get('CFPackages/{identifier}', 'CaseController@getCFPackage'); // Done
            Route::get('CFItems/{identifier}', 'CaseController@getCFItem');  // Done
            Route::get('CFConcepts/{identifier}', 'CaseController@getCFConcept');//Done
            Route::get('CFItemTypes/{identifier}', 'CaseController@getCFItemType');// Done
            Route::get('CFLicenses/{identifier}', 'CaseController@getCFLicense');  // Done
            Route::get('CFSubjects/{identifier}', 'CaseController@getCFSubject');// Done
            Route::get('CFAssociations/{identifier}', 'CaseController@getCFAssociation');//Done
            Route::get('CFItemAssociations/{identifier}', 'CaseController@getCFItemAssociations');
            Route::get('CFAssociationGroupings/{identifier}', 'CaseController@getCFAssociationGrouping');
            Route::get('CustomMetadata/{identifier}','CaseController@getCustomMetadata','?document_id');
        });
    });

    Route::get('healthCheck', 'HelpCheckController@helpCheckInfo');
    
    Route::group(["prefix" => "tenant"], function() {
        Route::get('', 'TenentController@index'); 
    });

    Route::group(["prefix" => "log"], function() {
        Route::get('', 'LogController@index');          
    });
    
    Route::post('_test', 'RemoveLaterController@testDeltaUpdateCloudSearchFeature');  
     
    // Get Default Organization
    Route::post('self-register', 'PublicReviewController@selfRegistration');
    // Self Registration
    Route::get('getDefaultOrganization', 'PublicReviewController@getDefaultOrganization');
    // Self Registration Email Verification
    Route::get('confirm/{active_access_token}', 'PublicReviewController@selfRegistrationEmailVerification');
    // Note

    Route::group(['prefix' => 'cache'], function() {
        Route::get('getTaxonomyHierarchy/{document_id}', 'CFPackadgeController@getTaxonomyHierarchy');
        Route::get('getTaxonomyDetails/{document_id}', 'CFPackadgeController@getTaxonomyDetails');
        Route::get('getTaxonomyList/{organization_id}', 'CFPackadgeController@getTaxonomyList');

        Route::get('notes/{organization_id}', 'NoteController@getAllNoteFromCache');
        Route::get('note/{note_id}', 'NoteController@getNoteFromCache');
        Route::get('file/{asset_id]','AssetController@getAllFileFromCache');

        Route::get('node_types/{organization_id}', 'NodeTypeController@getNodeTypesFromCache');
        Route::get('file/{organization_id}','AssetController@getAllFileFromCache');

        Route::post('search', 'SearchController@search');

        
    });
    Route::get('checkip', 'TenentController@serverDetails');

    Route::get('orgDetails/{org_code}', 'TenentController@getOrgDetails');    
});

// Performance Related Api in Version 2
/*
  |--------------------------------------------------------------------------
  | Service - API Routes Version 2.0 
  |--------------------------------------------------------------------------
  |
  | start date : 26-04-2019 
  | 
  |
  |
 */
 
 Route::group(['prefix' => 'v2'], function() {
	     Route::middleware(['authenticate-access-token'])->group(function() {

        Route::middleware(['permission-handler'])->group(function() {
			
		 });
		 Route::group(["prefix" => "taxonomy"], function() {
			 
                  Route::get('getTreeHierarchy/{document_id}', 'CFPackadgeController@getTreeHierarchyV3'); 
                  Route::get('getTreeDetails/{document_id}', 'CFPackadgeController@getTreeDetailsV2');                 
                  Route::post('setTreeHierarchy/{document_id}', 'CFPackadgeController@setTreeHierarchyV2');
                  Route::get('getTreeHierarchyV3/{document_id}', 'CFPackadgeController@getTreeHierarchyV2');

                  #taxonomy heirarchy with tree structure
                  Route::get('getTreeHierarchyV4/{document_id}', 'CFPackadgeController@getTreeHierarchyV4'); 
                  Route::get('getTreeHierarchyV5/{document_id}', 'CFPackadgeController@getTreeHierarchyV5'); 

                  
            });
            Route::group(["prefix" => "project"], function() {
                Route::get('getTreeHierarchy/{project_id}', 'CFPackadgeController@getProjectTreeHierarchyV2'); 
                Route::get('getTreeDetails/{project_id}', 'CFPackadgeController@getProjectTreeDetailsV2');
                Route::get('nodesWithTaxonomyTree/{project_id}', 'CFPackadgeController@mappedNodesNewV2');                  
            });
            
        });		 
	 });
 
     Route::get('/', function(){

        echo 'Route not found';
     });
