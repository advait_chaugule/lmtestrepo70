<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('user_id', 36)->primary();
            $table->string('email', 200);
            $table->string('username', 50)->unique();
            $table->string('password')->nullable(true);
            $table->string('active_access_token');
            $table->timestamp('access_token_updated_at')->nullable(true);
            $table->string('first_name', 50)->nullable(true);
            $table->string('last_name', 50)->nullable(true);
            $table->string('organization_id', 36);
            $table->string('role_id', 36);
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('forgot_pasword_request_token',100)->nullable(true);
            $table->timestamp('forgot_pasword_request_token_expiry');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
