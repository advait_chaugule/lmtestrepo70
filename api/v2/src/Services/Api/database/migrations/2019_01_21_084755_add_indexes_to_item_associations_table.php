<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->index('origin_node_id');
	        $table->index('destination_node_id');
        });

        Schema::table('item_metadata', function(Blueprint $table)
        {
            $table->index('metadata_id');
	        $table->index('item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->dropIndex(['origin_node_id']);
            $table->dropIndex(['destination_node_id']);
        });

        Schema::table('item_metadata', function(Blueprint $table)
        {
            $table->dropIndex(['metadata_id']);
            $table->dropIndex(['item_id']);
        });
    }
}