<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_activity_logs', function (Blueprint $table) {
            $table->string('activity_log_id', 36)->primary();
            $table->string('user_id', 36);
            $table->string('organization_id', 36);

            $table->tinyInteger('activity_event_type')->comment("1, 2, 3 etc. Check event_activity config file");
            $table->string('activity_id', 36)->comment("primary key of project, item etc");

            $table->tinyInteger('activity_event_sub_type')->nullable(true)->comment("1, 2, 3 etc. Check event_activity config file");
            $table->string('activity_event_sub_type_activity_id', 36)->nullable(true)->comment("primary key of project, item etc");

            $table->string('activity_package_s3_file_id', 1024)->comment("
                                                                            BeforeActionDataJSON, AfterActionDataJSON
                                                                            this zip file will have both the JSON files stored in s3
                                                                        ");

            $table->string('cache_activity_name', 1024);
            $table->string('cache_activity_subtype_activity_name', 1024)->nullable(true);
            $table->string('cache_user_display_name', 1024);

            $table->timestamp('activity_log_timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_activity_logs');
    }
}
