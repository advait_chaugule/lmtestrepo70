<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnItemsAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->index('document_id');	 
            $table->index('organization_id');	         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->dropIndex(['document_id']); 
            $table->dropIndex(['organization_id']);          
        });
    }
}
