<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceAssociationGroupingUriObjectToItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            //
            $table->string('source_association_grouping_uri_object', 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            //
            $table->dropColumn('source_association_grouping_uri_object');
        });
    }
}
