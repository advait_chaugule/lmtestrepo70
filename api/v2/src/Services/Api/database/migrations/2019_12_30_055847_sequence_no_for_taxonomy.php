<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SequenceNoForTaxonomy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $this->migrationSequenceNo();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function migrationSequenceNo()
    {
        ini_set('memory_limit','80980M');
        ini_set('max_execution_time', 0);
        $time_start = microtime(true);

        # exclude taxonomies with import type 1
        $document_id_arr = DB::select("select `document_id` from `acmt_documents` where `is_deleted` = 0 and `import_type` != 1");
        # preparing array
        $tax_arr = [];
        foreach($document_id_arr as $doc_id_value) {
            $tax_arr[] = $doc_id_value->document_id;
        }
        $document_id_arr_chunk = array_chunk($tax_arr, 10);

        # execute script for 1000 documents/trees at a time
        foreach ($document_id_arr_chunk as $chunk) {
            $item_child_asso = DB::select( "select `item_id`, `full_statement`, `human_coding_scheme`, `item_association_id`, `sequence_number`, `acmt_item_associations`.`source_item_id`, `acmt_item_associations`.`target_item_id`
                                            from `acmt_item_associations`
                                            left join `acmt_items` on `acmt_item_associations`.`source_item_id` = `acmt_items`.`item_id`
                                            where `association_type` = 1
                                            and `source_document_id` in ('".implode("','",$chunk)."')
                                            and `acmt_items`.`is_deleted` = 0
                                            and `acmt_item_associations`.`is_deleted` = 0
                                            and `acmt_item_associations`.`sequence_number` = 0" );
            // test for multiple doc_id's in $chunk in implode

            $grouped_level_arr = [];
            # group nodes with the same parent
            foreach ($item_child_asso as $asso_key => $asso_value) {
                $grouped_level_arr[$asso_value->target_item_id][] = (array)$asso_value;
            }

            $sorted_arr = [];
            # call sort function on each group
            foreach($grouped_level_arr as $key => $value) {
                $sortBy = $this->checkSortType($value);
                # no need to update if sequence number found
                if($sortBy != 'sequence_number') {
                    $sorted_arr[] = array_values($this->arraySort($value, $sortBy));
                }
            }
            $sequence_arr = [];
            # assign sequence no to each child association
            foreach ($sorted_arr as $group_key => $group_value) {
                $counter = 1;
                foreach ($group_value as $asso_key => $asso_value) {
                    $sequence_arr[$asso_value['item_association_id']] = $counter;
                    $counter++;
                }
            }
            # prepare array such that sequence no is key and all the corresponding asso with that seq no as array
            $prep_sequence_arr = [];
            foreach($sequence_arr as $key => $value) {
                $prep_sequence_arr[$value][] = $key;
            }

            foreach($prep_sequence_arr as $seq_no => $asso_arr) {
                $statement = '';
                $arr_chunk = array_chunk($asso_arr, 1000); // change the chunk number
                foreach($arr_chunk as $chunk) {
                    $statement .= " UPDATE acmt_item_associations SET sequence_number =".$seq_no." where item_association_id in ('".implode("','",$chunk)."'); ";
                }
                DB::unprepared($statement);
                unset($statement);
            }
        }
        // echo 'item seq no Total execution time in seconds: ' . (microtime(true) - $time_start);
        // echo('-----code ends------');
    }

    public function checkSortType($data)
    {
        $data = (array) $data[0];
        if (isset($data['sequence_number']) && !empty(trim($data['sequence_number']))) {
            return 'sequence_number';
        } else {
            if (isset($data['human_coding_scheme']) && trim($data['human_coding_scheme']) !== '') {
                return 'human_coding_scheme';
            } else {
                return 'full_statement';
            }
        }
    }

    public function arraySort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array((array)$v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }
            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }
        return $new_array;
    }
}
