<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnableEmailNotificationToUsersOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_organization', function (Blueprint $table) {
            //
            $table->tinyInteger('enable_email_notification')
            ->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_organization', function (Blueprint $table) {
            //
            $table->dropColumn('enable_email_notification');
        });
    }
}
