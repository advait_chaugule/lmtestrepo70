<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowStageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_stage', function (Blueprint $table) {
            $table->string('workflow_stage_id', 80)->primary();
            $table->string('workflow_id', 36);
            $table->string('stage_id',36);
            $table->tinyInteger('order')->default(0);
            $table->string('stage_name',250);
            $table->string('stage_description', 1000)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_stage');
    }
}
