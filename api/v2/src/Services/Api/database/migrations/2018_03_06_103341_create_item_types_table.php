<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_types', function (Blueprint $table) {
            $table->string('item_type_id', 36)->primary();
            $table->string('title', 100)->nullable(true);
            $table->string('type_code', 100)->nullable(true);
            $table->string('hierarchy_code', 25)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('source_item_type_id', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_types');
    }
}
