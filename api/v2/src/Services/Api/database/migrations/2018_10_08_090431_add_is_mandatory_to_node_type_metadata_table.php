<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsMandatoryToNodeTypeMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('node_type_metadata', function (Blueprint $table) {
            $table->tinyInteger('is_mandatory')
            ->default(0)
            ->after('metadata_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('node_type_metadata', function (Blueprint $table) {
            $table->dropColumn('is_mandatory');
        });
    }
}
