<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->string('role_id', 36)->primary();
            $table->string('organization_id', 36);
            $table->string('name', 50);
            $table->string('description', 1000)->nullable(true);
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_deleted')->default(0);
            $table->integer('usage_count')->default(0);
            $table->nullableTimestamps();
            // set indexes all together (index name will be created automatically)
            // $table->index(['organization_id', 'is_active', 'is_deleted']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
