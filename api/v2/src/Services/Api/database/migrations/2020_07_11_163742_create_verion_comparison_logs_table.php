<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerionComparisonLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_compairison_logs', function (Blueprint $table) {
            $table->increments('logs_id');
            $table->string('comparison_id', 36)->nullable(true);
            $table->string('type', 45)->nullable(true);
            $table->string('node_id', 45)->nullable(true);
            $table->string('metadata', 45)->nullable(true);
            $table->longText('summary_value')->nullable(true);
            $table->longText('error_description')->nullable(true);
            $table->string('error_type', 45)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_compairison_logs');
    }
}
