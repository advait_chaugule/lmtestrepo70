<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCurrentUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = DB::table('users')->select('user_id','organization_id','role_id')
                          ->where('is_deleted','=',0)->get()->toArray();
        $userArr = json_decode(json_encode($users),true);
        foreach ($userArr as $user)
        {
            $userOrg  = ['user_id'         => $user['user_id'],
                         'organization_id' => $user['organization_id'],
                         'role_id'         => $user['role_id'],
                         'is_active'         => 1,
                        ];
             DB::table('users_organization')->insert($userOrg);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users_organization')->delete();
    }

}
