<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForReorderInPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $date = date('Y-m-d H:i:s');

        $permissionOrder1 = DB::table('permissions')
        ->select('order')
        ->where('permission_group','1')
        ->orderBy('order','desc')
        ->get()
        ->toArray();
        $order1 = isset($permissionOrder1[0]->order) ? $permissionOrder1[0]->order : 0;
      
        $permissionOrder2 = DB::table('permissions')
        ->select('order')
        ->where('permission_group','2')
        ->orderBy('order','desc')
        ->get()
        ->toArray();
        $order2 = isset($permissionOrder2[0]->order) ? $permissionOrder2[0]->order : 0;

        $adminResendEmailPermisssion = [[
            "permission_id"=>"9c3daeff-0f01-4ece-b621-537dab6abdb4",
            "internal_name"=>"reorder",
            "display_name"=>"Reorder",
            "permission_group"=>1,
            "parent_permission_id"=>"",
            "order"=>($order1+1),
            "created_at"=>$date,
            "updated_at"=>$date
        ],[
            "permission_id"=>"1c38172b-e80c-489c-915f-dbe8e17afe45",
            "internal_name"=>"reorder",
            "display_name"=>"Reorder",
            "permission_group"=>2,
            "parent_permission_id"=>"",
            "order"=>($order2+1),
            "created_at"=>$date,
            "updated_at"=>$date
        ]];
       
        DB::table('permissions')->insert($adminResendEmailPermisssion);     
        $permissionArr = ['9c3daeff-0f01-4ece-b621-537dab6abdb4','1c38172b-e80c-489c-915f-dbe8e17afe45'];
        $getAdmin = DB::table('roles')
                    ->select('role_id')
                    ->where('name','Admin')
                    ->where('role_code','DAR00')
                    ->where('is_active',1)
                    ->where('is_deleted',0)
                    ->get()
                    ->toArray();
      
        $allAdmins=[];
        foreach($getAdmin as $admin){
            foreach($permissionArr as $permission) {
                $adminData =    [
                    "role_id" => $admin->role_id,
                    "permission_id" => $permission
                ];

                $allAdmins[] = $adminData;
            } 
        }
        DB::table('role_permission')->insert($allAdmins);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
