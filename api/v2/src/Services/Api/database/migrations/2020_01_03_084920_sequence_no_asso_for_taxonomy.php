<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SequenceNoAssoForTaxonomy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $this->migrationAssoSequenceNo();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function migrationAssoSequenceNo()
    {
        ini_set('memory_limit','80980M');
        ini_set('max_execution_time', 0);
        $time_start = microtime(true);

        $document_id_arr = DB::select("select `document_id` from `acmt_documents` where `is_deleted` = 0 and `import_type` != 1 ");
        # preparing array
        $tax_arr = [];
        foreach($document_id_arr as $doc_id_value) {
            $tax_arr[] = $doc_id_value->document_id;
        }
        $document_id_arr_chunk = array_chunk($tax_arr, 10);
        # execute script for 1000 documents/trees at a time
        # doing this so that when we make chunk of associations related assocaitions of one node dont go in another chunk
        foreach ($document_id_arr_chunk as $chunk) {
            $item_child_asso = DB::select("select `item_association_id`, `sequence_number`, `association_type`, `source_item_id`, `created_at`
                                            from `acmt_item_associations`
                                            where `acmt_item_associations`.`is_deleted` = 0
                                            and `source_document_id` in ('".implode("','",$chunk)."')
                                            order by `acmt_item_associations`.`created_at` asc");

            $grouped_asso_arr = [];
            foreach ($item_child_asso as $value) {
                $grouped_asso_arr[$value->source_item_id][] = [
                    'item_association_id' => $value->item_association_id,
                    'sequence_number' => (int)$value->sequence_number,
                    'association_type' => (int)$value->association_type,
                ];
            }
            $insert_arr = [];
            foreach ($grouped_asso_arr as $source_item_id => $group) {
                $counter_value = max(array_column($group, 'sequence_number')) + 1;
                foreach ($group as $value) {
                    if($value['association_type'] != 1) {
                        $insert_arr[$value['item_association_id']] = $counter_value;
                        $counter_value++;
                    }
                }
            }
            # prepare array such that sequence no is key and all the corresponding asso with that seq no as array
            $prep_sequence_arr = [];
            foreach($insert_arr as $key => $value) {
                $prep_sequence_arr[$value][] = $key;
            }

            #insert into DB
            foreach($prep_sequence_arr as $seq_no => $asso_arr) {
                $statement = '';
                $arr_chunk = array_chunk($asso_arr, 1000); // change the chunk number
                foreach($arr_chunk as $chunk) {
                    $statement .= " UPDATE acmt_item_associations SET sequence_number =".$seq_no." where item_association_id in ('".implode("','",$chunk)."'); ";
                }
                DB::unprepared($statement);
                unset($statement);
            }
        }
        // echo 'asso seq no Total execution time in seconds: ' . (microtime(true) - $time_start);
        // echo('-----code ends-----');
    }
}
