<?php
ini_set('memory_limit','2048M');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIsReverseAssociationToItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itemsFromAssociationDocument   =   [];
        $reverseAssociationTypeArray    =   ['3', '5', '7'];
        $itemAssociations = DB::table('item_associations')
            ->select('*')
            ->get();
        foreach($itemAssociations as $association) {
            $arrayOfItemId              =   [];
            if(!in_array($association->association_type, $reverseAssociationTypeArray)) {
                DB::table('item_associations')
                    ->where('origin_node_id', $association->origin_node_id)
                    ->where('document_id', $association->document_id)
                    ->update(['is_reverse_association' => 0]);   
            } else {
                $itemsFromAssociationDocument   =   DB::table('items')
                    ->select('item_id')
                    ->where(['document_id' => $association->document_id])
                    ->get();
                
                foreach($itemsFromAssociationDocument as $items) {
                    $arrayOfItemId[]    =   $items->item_id;
                }

                if(in_array($association->origin_node_id, $arrayOfItemId)) {
                    DB::table('item_associations')
                    ->where('origin_node_id', $association->origin_node_id)
                    ->where('document_id', $association->document_id)
                    ->update(['is_reverse_association' => 0]); 
                } else {
                    DB::table('item_associations')
                    ->where('destination_node_id', $association->origin_node_id)
                    ->update(['is_reverse_association' => 1]);
                }
            }          
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            //
        });
    }
}

