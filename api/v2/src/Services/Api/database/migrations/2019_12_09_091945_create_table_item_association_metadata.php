<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItemAssociationMetadata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_association_metadata', function (Blueprint $table) {
            $table->string('item_association_id',36);
            $table->string('metadata_id',36);
            $table->string('metadata_value',36);
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamp('created_at')->nullable(true);
            $table->timestamp('updated_at')->nullable(true);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_association_metadata');
    }
}
