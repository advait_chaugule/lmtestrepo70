<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateSearchHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_histories', function (Blueprint $table) {
            $table->string('search_id', 36)->primary();
            $table->string('title', 100)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->string('search_filter', 5000);
            $table->tinyInteger('is_explicit_saved')->default(0)->index('search_history_is_explicit_saved')->comment("1->explicit, 0->implicit");
            $table->string('organization_id', 36)->nullable(true)->index('search_history_organization_id');
            $table->string('created_by', 36)->nullable(true)->index('search_history_created_by');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'))->index('search_history_updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_histories');
    }
}
