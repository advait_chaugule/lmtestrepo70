<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVersionSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('version_subscription', function (Blueprint $table) {
            $table->integer('version')->unsigned()->comment('Publish number pubstate_history')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('version_subscription', function (Blueprint $table) {
            $table->dropColumn('version');
        });
    }
}
