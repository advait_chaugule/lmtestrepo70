<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class UpdateIsRegisterCloumnFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $updateColumn=['is_register'=>1];

        $resendEmail=DB::table('users')
                        ->where('is_active',1)
                        ->where('is_deleted',0)
                        ->update($updateColumn);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
