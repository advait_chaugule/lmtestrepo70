<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTreeDetailsJsonFileS3ColumnInTaxonomyPubstateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->string('treedetails_json_file_s3',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->dropColumn('treedetails_json_file_s3');
        });
    }
}
