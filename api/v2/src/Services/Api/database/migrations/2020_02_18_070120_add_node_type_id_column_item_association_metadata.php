<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNodeTypeIdColumnItemAssociationMetadata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_association_metadata', function (Blueprint $table) {
            $table->string('node_type_id',36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_association_metadata', function (Blueprint $table) {
            $table->dropColumn('node_type_id');
        });
    }
}
