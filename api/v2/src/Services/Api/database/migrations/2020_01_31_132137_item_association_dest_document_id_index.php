<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemAssociationDestDocumentIdIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
         
            $table->index('destination_document_id');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->dropIndex(['destination_document_id']);  
        });
    }
}
