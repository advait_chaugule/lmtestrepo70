<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVersionComparison extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_comparison', function (Blueprint $table) {
            $table->string('comparison_id', 36)->primary();
            $table->string('document_id', 36)->nullable(true)->index('version_comparison_document_id');
            $table->string('subscription_id', 36)->nullable(true);
            $table->string('source_document_id', 36)->nullable(true)->index('version_comparison_source_document_id');
            $table->string('organization_id', 36)->nullable(true)->index('version_comparison_organization_id');
            $table->string('first_version', 100)->nullable(true);
            $table->string('second_version', 100)->nullable(true);
            $table->string('first_version_case_url', 500)->nullable(true);
            $table->string('first_version_custom_url', 500)->nullable(true);
            $table->string('second_version_case_url', 500)->nullable(true);
            $table->string('second_version_custom_url', 500)->nullable(true);
            $table->timestamp('comparison_datetime')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('version_comparison');
    }
}
