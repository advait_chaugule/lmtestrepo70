<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertOrganizationIdProjectUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $project = DB::table('project_users')
            ->join('projects', 'project_users.project_id', '=', 'projects.project_id')
            ->select('projects.organization_id', 'project_users.user_id', 'project_users.project_id')
            ->get();
        $projectArr = json_decode(json_encode($project),true);
        foreach ($projectArr as $project)
        {
             $organizationId =  $project['organization_id'];
             $projectId      =  $project['project_id'];
             $userId         =  $project['user_id'];

            DB::table('project_users')
                ->where('user_id', $userId)
                ->where('project_id', $projectId)
                ->update(['organization_id' => $organizationId]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
