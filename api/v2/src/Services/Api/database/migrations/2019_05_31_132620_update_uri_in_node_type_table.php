<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Data\Models\NodeType;
use App\Data\Models\Organization;

class UpdateUriInNodeTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //get the node_type_id and organization_id from NodeType table
        $getOrganizationIds = Organization::select('org_code','organization_id')->where('is_deleted',0)->get();
        $orgCodeArr = [];
        foreach($getOrganizationIds as $getOrganizationId)
            {
                $orgCodeArr[$getOrganizationId['organization_id']] = $getOrganizationId['org_code'];    
            }

       //get the node_type_id and organization_id from NodeType table
        $getNodeTypeIds = NodeType::select('source_node_type_id','organization_id','uri')->where('is_deleted',0)->get();
        $hostname = env('APP_URL');
        $acmtApiVersionPrefix = "api/v1";
        $imsUrl ="ims/case/v1p0/CFItemTypes";
        foreach($getNodeTypeIds as $getNodeTypeId)
            {
                $getNodeTId = $getNodeTypeId['source_node_type_id'];
                $getNodeOrg = $getNodeTypeId['organization_id'];    
                $getNodeUri = $getNodeTypeId['uri'];
                
                $orgCode = !empty($orgCodeArr[$getNodeOrg]) ? $orgCodeArr[$getNodeOrg] : "" ;
                $url = $hostname."/".$acmtApiVersionPrefix."/".$imsUrl."/".$getNodeTId;
                if($orgCode != "")
                {
                    $url = $hostname."/".$acmtApiVersionPrefix."/".$orgCode."/".$imsUrl."/".$getNodeTId; 
                 }
                else
                {
                    $url = $hostname."/".$acmtApiVersionPrefix."/".$imsUrl."/".$getNodeTId;
                }
                if(empty($getNodeUri)){
                    NodeType::where('source_node_type_id', $getNodeTId)
                    ->update(['uri' => $url]);
                }
            }  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
