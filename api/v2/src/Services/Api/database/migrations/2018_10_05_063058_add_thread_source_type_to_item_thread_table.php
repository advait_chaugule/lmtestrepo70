<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreadSourceTypeToItemThreadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_threads', function (Blueprint $table) {
            //
            $table->tinyInteger('thread_source_type')
                  ->default(1)
                  ->after('thread_source_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_threads', function (Blueprint $table) {
            //
            $table->dropColumn('thread_source_type');
        });
    }
}
