<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_configs', function (Blueprint $table) {
            $table->string('report_config_id', 36);
            $table->string('internal_name', 256);
            $table->string('title', 256);
            $table->string('parameter_1_display_name', 256);
            $table->string('parameter_1_subtype', 256);
            $table->tinyInteger('parameter_1_prebuild_type')->comment("xls,csv,html");
            $table->string('parameter_2_display_name', 256);
            $table->string('parameter_2_subtype', 256);
            $table->tinyInteger('parameter_2_prebuild_type')->comment("list of taxonomy");
            $table->string('parameter_3_display_name', 256);
            $table->string('parameter_3_subtype', 256);
            $table->tinyInteger('parameter_3_prebuild_type');
            $table->string('parameter_4_display_name', 256);
            $table->string('parameter_4_subtype', 256);
            $table->tinyInteger('parameter_4_prebuild_type');
            $table->string('parameter_5_display_name', 256);
            $table->string('parameter_5_subtype', 256);
            $table->tinyInteger('parameter_5_prebuild_type');
            $table->string('endpoint_url', 1024);
            $table->tinyInteger('is_deleted')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_configs');
    }
}
