<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInImportJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('import_job', function (Blueprint $table) {
            $table->longtext('import_taxonomy_settings');
            $table->longtext('success_message_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('import_job', function (Blueprint $table) {
            $table->dropColumn('import_taxonomy_settings');
            $table->dropColumn('success_message_data');
        });
    }
}
