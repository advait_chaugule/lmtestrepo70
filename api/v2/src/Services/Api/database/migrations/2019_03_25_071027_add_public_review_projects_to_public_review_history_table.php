<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Data\Models\Project;

class AddPublicReviewProjectsToPublicReviewHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $queryDataForProject = DB::table('projects')->select('projects.project_id','projects.document_id','projects.organization_id')
            ->where('project_type','=',3)
            ->where('project_id','=','dedd0edf-1fb7-4d4c-812b-5a3866caff00')
            ->where('projects.is_deleted','=',0)
            ->get()->first();
        
        if($queryDataForProject != null && $queryDataForProject->count() > 0) {
            //dd($queryDataForProject->document_id);

            $queryForDocumentToBeCopied =   DB::table('document_public_reviews')->select('original_document_id')->where('copied_document_id','=',$queryDataForProject->document_id)->get()->first();

            //dd($queryForDocumentToBeCopied);

            $checkIfPRHistoriesExist    =   DB::table('public_review_histories')->where(['document_id' => $queryForDocumentToBeCopied->original_document_id,'project_id' => 'dedd0edf-1fb7-4d4c-812b-5a3866caff00'])->get();

            if($checkIfPRHistoriesExist->count() == 0) {
                $dataToInsert   =   [
                    'project_id'        =>  'dedd0edf-1fb7-4d4c-812b-5a3866caff00',
                    'document_id'       =>  $queryForDocumentToBeCopied->original_document_id,
                    'organization_id'   =>  $queryDataForProject->organization_id,
                    'review_number'     =>  '1',
                    'timezone'          =>  'Etc/GMT-5',
                    'start_date'        =>  '2019-01-25 00:00:00',
                    'end_date'          =>  '2019-03-01 00:00:00',
                    'status'            =>  '3',
                    'created_by'        =>  '749f10ce-7162-432a-a82a-2e9ccf2e27bf',
                    'updated_by'        =>  '749f10ce-7162-432a-a82a-2e9ccf2e27bf',
                    'created_at'        =>  '2019-01-25 00:00:00',
                    'updated_at'        =>  '2019-01-25 00:00:00',
                ];

                $insertPublicReviewHistory  =   DB::table('public_review_histories')->insert($dataToInsert);
            }
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
