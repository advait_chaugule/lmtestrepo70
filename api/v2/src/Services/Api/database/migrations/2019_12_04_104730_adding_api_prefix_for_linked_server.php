<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingApiPrefixForLinkedServer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $getUrl = DB::table('linked_servers')
        ->select('linked_server_id','server_url')
        ->where('is_deleted' , 0)
        ->get()
        ->toArray();
        
        $getUrlArray = array();
        $i=0;
        foreach($getUrl as $getUrlK=>$getUrlV)
        {
            $getUrlArray[$i]['linked_server_id'] = $getUrlV->linked_server_id    ;
            $getUrlArray[$i]['server_url'] =  $getUrlV->server_url ;
            $i = $i+1;
        }
        
        $urlsToCheck = array(
                                'https://comet-stg.learningmate.com/'=>'https://api.comet-stg.learningmate.com/',
                                'http://comet-stg.learningmate.com/'=>'http://api.comet-stg.learningmate.com/',
                                'https://acmt-qa.learningmate.com/'=>'https://api.acmt-qa.learningmate.com/',
                                'http://acmt-qa.learningmate.com/'=>'http://api.acmt-qa.learningmate.com/',
                                'https://acmt-dev.learningmate.com/'=>'https://api.acmt-dev.learningmate.com/',
                                'http://acmt-dev.learningmate.com/'=>'http://api.acmt-dev.learningmate.com/'
                                );
        
        $getDataMapped = array();
        $j =0;
        foreach($getUrlArray as $getUrlArrayK=>$getUrlArrayV)
        {
            
            if (strpos($getUrlArrayV['server_url'], 'server') !== false)
            {
                $chunk = explode('server',$getUrlArrayV['server_url']);
                
                if (array_key_exists($chunk[0],$urlsToCheck))
                    {
                        $appendedAPIURL = $urlsToCheck[$chunk[0]];
                        $getDataMapped[$j]['linked_server_id'] = $getUrlArrayV['linked_server_id'];
                        $getDataMapped[$j]['server_url'] = $appendedAPIURL.'server'.$chunk[1];
                        $j=$j+1;
                    }    
            }
            
        }
        
        foreach($getDataMapped as $getDataMappedK=>$getDataMappedV)
        {
            DB::table('linked_servers')
                ->where('linked_server_id',$getDataMappedV['linked_server_id'])
                ->update(['server_url'=>$getDataMappedV['server_url']]);
        }
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
