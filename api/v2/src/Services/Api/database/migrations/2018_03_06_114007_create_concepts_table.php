<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concepts', function (Blueprint $table) {
            $table->string('concept_id', 36)->primary();
            $table->string('title', 100)->nullable(true);
            $table->string('keywords', 1000)->nullable(true);
            $table->string('hierarchy_code', 10)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('source_concept_id', 36)->nullable();
            $table->string('organization_id', 36);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concepts');
    }
}
