<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    private $table = "documents";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->string('document_id', 36)->primary();
            $table->string('title', 100);
            $table->string('official_source_url', 500)->nullable(true);
            $table->string('language_id', 36)->nullable(true);
            $table->tinyInteger('adoption_status')->default(1)->comment(" For CASE status
                                                                        draft -> 1
                                                                        private_draft -> 2
                                                                        adopted -> 3, 
                                                                        deprecated -> 4");
            $table->tinyInteger('status')->default(1)->comment("For ACMT specific business status
                                                                draft -> 1
                                                                public_review -> 2
                                                                published -> 3");
            $table->string('notes', 6000)->nullable(true);
            $table->string('publisher', 1000)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->string('version', 10)->nullable(true);
            $table->timestamp('status_start_date')->nullable(true);
            $table->timestamp('status_end_date')->nullable(true);
            $table->string('license_id', 36)->nullable(true);
            $table->string('organization_id', 36);
            $table->string('creator', 4000)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('updated_by', 36)->nullable(true);
            $table->string('source_document_id', 36)->nullable(true);
            $table->string('pulished_taxonomy_snapshot_s3_zip_file_id', 255)->nullable(true);
            $table->string('node_type_id', 36)->nullable(true);
            $table->string('project_id', 36)->nullable(true);
            $table->string('node_template_id', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
