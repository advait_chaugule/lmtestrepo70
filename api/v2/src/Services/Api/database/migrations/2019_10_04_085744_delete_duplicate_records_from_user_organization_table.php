<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteDuplicateRecordsFromUserOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = DB::table('users_organization')->select('user_id','organization_id')
                          ->groupBy('user_id','organization_id')
                          ->havingRaw('COUNT(user_id) > ?', [1])
                          ->get()->toArray();
        $userList = json_decode(json_encode($users),true);
        foreach ($userList as $user)
        {         
         
            $checkUserOrgExists = DB::table('users_organization')->select('user_id','organization_id','role_id','created_at')
							  ->where('user_id','=',$user['user_id'])
                              ->where('organization_id','=',$user['organization_id'])
                              ->orderBy('created_at','desc')
                              ->limit(1)
                              ->get()->toArray(); 
           if(!empty($checkUserOrgExists))
           {  
                DB::table('users_organization')
                ->where('user_id', $checkUserOrgExists[0]->user_id)
                ->where('organization_id', $checkUserOrgExists[0]->organization_id)
                ->where('role_id', $checkUserOrgExists[0]->role_id)
                ->where('created_at', $checkUserOrgExists[0]->created_at)
                ->update(['is_active' => 0,'is_deleted' => 1]);
            }

        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
