<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigSettingsPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = date('Y-m-d H:i:s');
        $userPermission = [
                             "permission_id"=>"3b7eb0a1-869c-401c-aeb2-617aac654f80",
                             "internal_name"=>"update_taxonomy_list_view",
                             "display_name"=>"Update taxonomy list view",
                             "permission_group"=>2,
                             "parent_permission_id"=>"",
                             "order"=>"7",
                             "created_at"=>$date,
                             "updated_at"=>$date
                          ];
        DB::table('permissions')->insert($userPermission);

        $adminPermission = [
            "permission_id"=>"8cb84bf9-a8d4-40ca-aefd-2c1fc997044d",
            "internal_name"=>"admin_setup_for_taxonomy_list_view",
            "display_name"=>"Admin setup for taxonomy list view",
            "permission_group"=>2,
            "parent_permission_id"=>"",
            "order"=>"8",
            "created_at"=>$date,
            "updated_at"=>$date
        ];
        DB::table('permissions')->insert($adminPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
