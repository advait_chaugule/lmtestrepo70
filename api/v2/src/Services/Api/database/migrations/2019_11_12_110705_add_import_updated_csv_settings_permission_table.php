<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportUpdatedCsvSettingsPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = date('Y-m-d H:i:s');
        $permissionOrder = DB::table('permissions')
                    ->select('order')
                    ->where('permission_group','2')
                    ->orderBy('order','desc')
                    ->get()
                    ->toArray();
        $order = isset($permissionOrder[0]->order) ? $permissionOrder[0]->order : 0;
        $userPermission = [
                             "permission_id"=>"f3098223-b01e-4eb0-9087-d469bebde8db",
                             "internal_name"=>"import_updated_csv",
                             "display_name"=>"Import Updated CSV",
                             "permission_group"=>2,
                             "parent_permission_id"=>"",
                             "order"=>($order+1),
                             "created_at"=>$date,
                             "updated_at"=>$date
                          ];
        DB::table('permissions')->insert($userPermission);

        $adminPermission = [
            "permission_id"=>"8caee8d9-ba2b-4a24-bf72-b169bb4498de",
            "internal_name"=>"admin_setup_for_import_updated_csv",
            "display_name"=>"Admin setup for Import Updated CSV",
            "permission_group"=>2,
            "parent_permission_id"=>"",
            "order"=>($order+2),
            "created_at"=>$date,
            "updated_at"=>$date
        ];
        DB::table('permissions')->insert($adminPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
