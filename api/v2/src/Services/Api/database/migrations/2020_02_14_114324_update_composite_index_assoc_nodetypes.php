<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompositeIndexAssocNodetypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_association_nodetype', function (Blueprint $table) {
            $table->dropUnique('composite_index');
            $table->unique(['source_document_type','destination_document_type','item_association_type','node_type_id'],'composite_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_association_nodetype', function (Blueprint $table) {
            $table->dropUnique('composite_index');
            $table->unique(['source_document_type','destination_document_type','item_association_type'],'composite_index');
        });
    }
}
