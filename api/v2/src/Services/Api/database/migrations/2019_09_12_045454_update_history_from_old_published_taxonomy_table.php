<?php

use App\Data\Models\TaxonomyPubStateHistory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHistoryFromOldPublishedTaxonomyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $publishTaxonomy =  DB::table('documents')
                            ->where('status',3)
                            ->where('is_deleted',0)
                            ->get()
                            ->toArray();

        $publishDate = date("Y-m-d H:i:s");
        foreach($publishTaxonomy as $publishTaxonomyK=>$publishTaxonomyV){
                $documentId     = $publishTaxonomyV->document_id;
                $userId         = $publishTaxonomyV->updated_by;
                $organizationId = $publishTaxonomyV->organization_id;

            TaxonomyPubStateHistory::firstOrCreate(['document_id'=>$documentId],
            [       'document_id'     => $documentId,
                    'publish_number'  => 1,
                    'publish_date'    => $publishDate,
                    'unpublish_date'  => '',
                    'published_by'    => $userId,
                    'unpublished_by'  => '',
                    'organization_id' => $organizationId,
                    'is_deleted'      => 0,
                    'created_by'      => $userId,
                    'updated_by'      => '',
                    'taxonomy_json_file_s3' => ''
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
