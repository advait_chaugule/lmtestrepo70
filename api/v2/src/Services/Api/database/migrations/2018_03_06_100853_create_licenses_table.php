<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->string('license_id', 36)->primary();
            $table->string('title', 100)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->text('license_text')->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('source_license_id', 36)->nullable(true);
            $table->string('organization_id', 36);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licenses');
    }
}
