<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsCustomAdditionalToItemMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_metadata', function (Blueprint $table) {
            $table->integer('is_additional')->comment('0 for Custom Metadata, 1 for Additional Metadata')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_metadata', function (Blueprint $table) {
            $table->dropColumn('is_additional');
        });
    }
}
