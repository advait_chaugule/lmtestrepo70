<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsOfItemThreadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_threads', function (Blueprint $table) {
            //
            $table->renameColumn('item_thread_id', 'thread_id');
            $table->renameColumn('item_id', 'thread_source_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_threads', function (Blueprint $table) {
            //
            $table->renameColumn('thread_id', 'item_thread_id');
            $table->renameColumn('thread_source_id', 'item_id');

        });
    }
}