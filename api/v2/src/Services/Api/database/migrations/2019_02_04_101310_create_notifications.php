<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('notifications',function(Blueprint $table){
          $table->string('notification_id',36)->primary();
          $table->string('description',1000);
          $table->integer('target_type');
          $table->string('target_id',36);
          $table->string('user_id',36); //receiver user id
          $table->string('organization_id',36);
          $table->integer('notification_category');
          $table->integer('is_deleted')->default(0);
          $table->string('created_by',36);
          $table->string('updated_by',36);
          $table->string('target_context',1000);
          $table->integer('read_status');
          $table->timestamps();
       });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
