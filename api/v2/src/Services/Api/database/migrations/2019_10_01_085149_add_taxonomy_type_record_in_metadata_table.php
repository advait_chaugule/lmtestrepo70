<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
//use App\Services\Api\Traits\UuidHelperTrait;
use \Ramsey\Uuid\Uuid;

class AddTaxonomyTypeRecordInMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   // use UuidHelperTrait;

    public function up()
    {
        $queryData = DB::table('organizations')->select('organization_id')
                 ->where('is_deleted','=',0)
                 ->where('is_active','=',1)
                 ->get()
                 ->toArray();
		$organizationList = json_decode(json_encode($queryData),true);
		
		$field_values = array("Generic", "Pacing Guide");
        $fieldvalues = array();
        $num = 1; 
        foreach($field_values as $key=>$value){
            
            $Valuesarray['key'] = $num;
            $Valuesarray['value'] = $value;
            $fieldvalues[] = $Valuesarray;
            $num++;
        }  
        $listValues = json_encode($fieldvalues);

        foreach($organizationList as $row){
            
            $Found_Metadata = DB::table('metadata')->where('organization_id','=',$row['organization_id'])->where('name','like','Taxonomy Type')->get();
            $found_row = json_decode(json_encode($Found_Metadata),true);
			if (empty($found_row)) {
                //$metadata_id =  $this->createUniversalUniqueIdentifier();
				$metadata_id =  Uuid::uuid4();
                $insert_metadata = ["metadata_id"=>$metadata_id ,"organization_id"=>$row['organization_id'],"parent_metadata_id"=>"","name"=>"Taxonomy Type","internal_name"=>"Taxonomy Type","field_type"=>"7","field_possible_values"=>$listValues,"order"=>'28',"is_custom"=>"1","is_document"=>"3","is_mandatory"=>'0',"is_active"=>"1","is_deleted"=>"0","updated_by"=>"","updated_at"=>"2019-10-01 05:56:00"];
                DB::table('metadata')->insert($insert_metadata);
            }
              
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
