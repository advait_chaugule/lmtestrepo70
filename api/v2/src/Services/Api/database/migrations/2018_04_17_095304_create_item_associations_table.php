<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_associations', function (Blueprint $table) {
            $table->string('item_association_id', 36)->primary();
            $table->tinyInteger('association_type')->comment("  isChildOf -> 1
                                                                isPeerOf -> 2
                                                                isPartOf -> 3
                                                                exactMatchOf -> 4
                                                                precedes -> 5
                                                                isRelatedTo -> 6
                                                                replacedBy -> 7
                                                                exemplar -> 8
                                                                hasSkillLevel -> 9");
            $table->string('document_id', 36);
            $table->string('association_group_id', 36)->nullable(true);
            $table->string('origin_node_id', 36);
            $table->string('destination_node_id', 36);
            $table->integer('sequence_number')->default(0)->comment('will be used to order the list');
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('source_item_association_id', 36)->nullable(true);
            $table->string('external_node_title', 4000);
            $table->string('external_node_url', 1000);
            $table->string('organization_id', 36);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_associations');
    }
}
