<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditAssociationConfigurationPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = date('Y-m-d H:i:s');
        $permissionOrder = DB::table('permissions')
                    ->select('order')
                    ->where('permission_group','5')
                    ->orderBy('order','desc')
                    ->get()
                    ->toArray();
        $order = isset($permissionOrder[0]->order) ? $permissionOrder[0]->order : 0;
        $userPermission = [
                             "permission_id"=>"9638a3aa-8dad-4e12-8be6-4e5295af9ade",
                             "internal_name"=>"edit_association_configuration",
                             "display_name"=>"Edit association configuration",
                             "permission_group"=>5,
                             "parent_permission_id"=>"",
                             "order"=>($order+1),
                             "created_at"=>$date,
                             "updated_at"=>$date
                          ];
        DB::table('permissions')->insert($userPermission);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
