<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicReviewHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_review_histories', function (Blueprint $table) {
            $table->string('document_id', 36);
            $table->string('project_id', 36);
            $table->string('organization_id', 36);
            $table->tinyInteger('review_number');

            $table->string('timezone', 36);
            $table->timestamp('start_date')->nullable(true);
            $table->timestamp('end_date')->nullable(true);
            $table->tinyInteger('status')->comment("scheduled->1, open/in progress -> 2, closed -> 3");

            $table->tinyInteger('is_deleted')->default(0);

            $table->timestamp('document_published_date')->nullable(true);
            $table->tinyInteger('document_status')->default(0);
            $table->string('document_publisehed_by', 36)->nullable(true);

            $table->string('created_by', 36)->nullable(true);
            $table->string('updated_by', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_review_histories');
    }
}
