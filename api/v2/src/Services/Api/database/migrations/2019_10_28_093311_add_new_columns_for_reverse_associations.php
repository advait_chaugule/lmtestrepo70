<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsForReverseAssociations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            $table->string('source_document_id', 36)->nullable();
            $table->string('source_item_id', 36)->nullable();
            $table->string('target_document_id', 36)->nullable();
            $table->string('target_item_id', 36)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            $table->dropColumn('source_document_id');
            $table->dropColumn('source_item_id');
            $table->dropColumn('target_document_id');
            $table->dropColumn('target_item_id');
        });
    }
}
