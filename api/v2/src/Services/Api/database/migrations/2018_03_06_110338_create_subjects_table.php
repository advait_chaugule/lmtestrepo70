<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->string('subject_id', 36)->primary();
            $table->string('title', 100)->nullable(true);
            $table->string('hierarchy_code', 25)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('source_subject_id', 36)->nullable(true);
            $table->string('organization_id', 36);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
