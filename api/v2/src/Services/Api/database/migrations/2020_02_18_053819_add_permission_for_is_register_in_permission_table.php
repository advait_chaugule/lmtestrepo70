<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForIsRegisterInPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        $date = date('Y-m-d H:i:s');

        $permissionOrder = DB::table('permissions')
        ->select('order')
        ->where('permission_group','4')
        ->orderBy('order','desc')
        ->get()
        ->toArray();
        $order = isset($permissionOrder[0]->order) ? $permissionOrder[0]->order : 0;
      
        $adminResendEmailPermisssion = [
            "permission_id"=>"6c1ea114-36e1-4570-9598-835523416a5a",
            "internal_name"=>"admin_setup_for_resend_email",
            "display_name"=>"Admin setup for resend email",
            "permission_group"=>4,
            "parent_permission_id"=>"",
            "order"=>($order+1),
            "created_at"=>$date,
            "updated_at"=>$date
        ];
       
        DB::table('permissions')->insert($adminResendEmailPermisssion);     

        $getAdmin = DB::table('roles')
                    ->select('role_id')
                    ->where('name','Admin')
                    ->where('role_code','DAR00')
                    ->where('is_active',1)
                    ->where('is_deleted',0)
                    ->get()
                    ->toArray();
      
        $allAdmins=[];
        foreach($getAdmin as $admin){

            $adminData =    [
                                "role_id"=>$admin->role_id,
                                "permission_id"=>"6c1ea114-36e1-4570-9598-835523416a5a"
                            ];

            $allAdmins[]=$adminData;
            }
          
        DB::table('role_permission')->insert($allAdmins);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
