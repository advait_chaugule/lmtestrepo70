<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVersionSubscriberDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('version_subscriber_detail', function (Blueprint $table) {
            $table->string('subscriber_detail_id', 36)->primary();
            $table->string('subscriber_id', 36)->nullable(true)->index('version_subscriber_det_subscriber_id')->comment('Subscriber User id');
            $table->string('subscriber_org_id', 36)->nullable(true);
            $table->string('publisher_org_id', 36)->nullable(true)->index('version_subscriber_det_publisher_org_id');
            $table->tinyInteger('is_internal')->default(1);
            $table->string('subscriber_end_point', 500)->nullable(true);            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('version_subscriber_detail');
    }
}
