<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('association_groups', function (Blueprint $table) {
            $table->string('association_group_id', 36)->primary();
            $table->string('title', 100);
            $table->string('description', 1000);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('source_association_group_id', 36)->nullable(true);
            $table->string('organization_id', 36);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('association_groups');
    }
}
