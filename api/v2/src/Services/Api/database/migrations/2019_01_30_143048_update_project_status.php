<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProjectStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = DB::table('projects')->select('projects.project_id','thread_comments.created_by')
            ->join('project_items','project_items.project_id','=','projects.project_id')
            ->join('thread_comments','thread_comments.thread_source_id','=','project_items.item_id')
            ->where('project_type','=',3)
            ->where('project_items.is_deleted','=',0)
            ->where('thread_comments.is_deleted','=',0)
            ->where('projects.is_deleted','=',0)
            ->groupBy('thread_comments.created_by','projects.project_id')
            ->get();
        return $query;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
