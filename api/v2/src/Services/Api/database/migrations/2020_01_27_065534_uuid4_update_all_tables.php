<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Services\Api\Traits\UuidHelperTrait;

class Uuid4UpdateAllTables extends Migration
{
    use UuidHelperTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* DB::transaction(function(){
            $documentListAll = DB::table('documents')
                ->select('document_id')
                ->where(function ($query) {
                        $query->where(DB::raw('SUBSTR(document_id, 15, 1)'),'!=','4')
                            ->orWhereNotIn(DB::raw('SUBSTR(document_id, 20, 1)'),['8','9','a','b']);
                })
                ->where('organization_id','f068a10c-96a7-48dd-9366-b71a199da48e')
                ->where('is_deleted','0')
                ->get()
                ->toArray();
            $documentListArray = array_chunk($documentListAll,50);
            foreach($documentListArray as $allkey => $documentList){
                $statement = "";            
                foreach($documentList as $key => $documentIdVal){                    
                    $documentId = $documentIdVal->document_id;
                    $updatedDocumentId = $this->createUniversalUniqueIdentifier();

                    $statement.= "UPDATE `acmt_assets` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_documents` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_documents` SET `source_document_id` = '$updatedDocumentId' WHERE `source_document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_documents` SET `uri` = ".(DB::raw("REPLACE(uri,  '$documentId', '$updatedDocumentId')"))." WHERE `uri` like '%$documentId'; ";
                    $statement.= "UPDATE `acmt_document_metadata` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_document_public_reviews` SET `original_document_id` = '$updatedDocumentId' WHERE `original_document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_document_public_reviews` SET `copied_document_id` = '$updatedDocumentId' WHERE `copied_document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_document_subject` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_group_document` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_import_job` SET `source_document_id` = '$updatedDocumentId' WHERE `source_document_id` = '$documentId';";
                    $statement.= "UPDATE `acmt_items` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_items` SET `parent_id` = '$updatedDocumentId' WHERE `parent_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `destination_document_id` = '$updatedDocumentId' WHERE `destination_document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `source_document_id` = '$updatedDocumentId' WHERE `source_document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `target_document_id` = '$updatedDocumentId' WHERE `target_document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `origin_node_id` = '$updatedDocumentId' WHERE `origin_node_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `destination_node_id` = '$updatedDocumentId' WHERE `destination_node_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `source_item_id` = '$updatedDocumentId' WHERE `source_item_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_item_associations` SET `target_item_id` = '$updatedDocumentId' WHERE `target_item_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_projects` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_project_pref_documents` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_public_review_histories` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_taxonomy_pubstate_history` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_user_settings` SET `id` = '$updatedDocumentId' WHERE `id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_notifications` SET `target_id` = '$updatedDocumentId' WHERE `target_id` = '$documentId'; ";
                    $statement.= "UPDATE `acmt_import_job` SET `results` = ".(DB::raw("REPLACE(results,  '$documentId', '$updatedDocumentId')"))." WHERE `results` like '%$documentId'; ";
                    $statement.= "UPDATE `acmt_notifications` SET `target_context` = ".(DB::raw("REPLACE(target_context,  '$documentId', '$updatedDocumentId')"))." WHERE `target_context` like '%$documentId'; ";
                    $statement.= "UPDATE `acmt_search_histories` SET `search_filter` = ".(DB::raw("REPLACE(search_filter,  '$documentId', '$updatedDocumentId')"))." WHERE `search_filter` like '%$documentId'; ";
                }
                DB::unprepared($statement);
                unset($statement);
            }
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
