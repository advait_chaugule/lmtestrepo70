<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHtmlColumnsInItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->longText('full_statement_html')->nullable();
            $table->longText('alternative_label_html')->nullable();
            $table->longText('human_coding_scheme_html')->nullable();
            $table->longText('abbreviated_statement_html')->nullable();
            $table->longText('notes_html')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('full_statement_html');
            $table->dropColumn('alternative_label_html');
            $table->dropColumn('human_coding_scheme_html');
            $table->dropColumn('abbreviated_statement_html');
            $table->dropColumn('notes_html');
        });
    }
}
