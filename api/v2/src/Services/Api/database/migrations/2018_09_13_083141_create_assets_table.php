<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->string('asset_id', 36)->primary();
            $table->string('organization_id', 36);
            $table->string('asset_name', 1000);
            $table->string('asset_temp_name', 1000);
            $table->string('asset_content_type', 100);
            $table->string('asset_size', 100)->comment("size in bytes");
            $table->string('asset_package_s3_file', 1024);
            $table->string('title', 100)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->string('item_association_id', 36)->nullable(true);
            $table->timestamp('uploaded_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
