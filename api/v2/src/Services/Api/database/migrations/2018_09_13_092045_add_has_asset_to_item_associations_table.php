<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasAssetToItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            $table->tinyInteger('has_asset')
                  ->default(0)
                  ->after('organization_id')
                  ->comment("1-> association has an asset associated to it.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            $table->dropColumn('has_asset');
        });
    }
}
