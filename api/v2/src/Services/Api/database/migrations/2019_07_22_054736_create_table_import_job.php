<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImportJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_job', function (Blueprint $table) {
            $table->string('import_job_id')->primary();
            $table->longText('results');
            $table->longText('filefullpath');
            $table->longText('errors');
            $table->string('user_id', 36);
            $table->string('organization_id', 36);
            $table->string('source_document_id', 36);
            $table->integer('process_type')->comment('1 for forground, 2 for background')->default(1);
            $table->integer('status')->comment('1 for Not Started, 2 for Work In Progress, 3 for Complete, 4 for Failed')->default(1);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('import_job');
    }
}
