<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpUserDemographic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `sp_user_demographic`(
            IN `organization_id` VARCHAR(36) ,
            IN `from_date` DATE,
            IN `to_date` DATE
            ) 
            
            SELECT  CONCAT( IFNULL(u.first_name, ''),' ',IFNULL(u.last_name, '')) AS username,
            IFNULL(GROUP_CONCAT(DISTINCT lang.lang),'') AS lang,
            IFNULL(GROUP_CONCAT(DISTINCT sub.subject),'') AS `subject`,
            CASE 
                WHEN u.reg_user_type = 1 THEN 'administrator' 
                WHEN u.reg_user_type = 2 THEN 'teacher' 
                WHEN u.reg_user_type = 3 
                THEN 'work in higher education' ELSE '' 
            END AS user_type,
            IFNULL(GROUP_CONCAT(DISTINCT gl.grade_level),'') AS grade_level,
            IFNULL(u.reg_admin_other_languages,'') AS other_languages ,u.created_at AS registration_date,u.email
                FROM acmt_users u 
            LEFT JOIN (
            SELECT  'K-5' AS grade_level ,'9f30e198-961d-4a56-91a3-58c87c8c741f' AS id 
            UNION 
            SELECT  '6-8' AS grade_level,'d629761f-ca97-4ea8-bcf4-44df06b37325' AS id 
            UNION 
            SELECT  '9-12' AS NAME,'57954137-5b29-49e8-8ca9-06c4daafb512' AS id 
            ) AS gl ON   FIND_IN_SET(gl.id,u.reg_teacher_grades) > 0 
            LEFT JOIN (
            SELECT  'Modern' AS SUBJECT ,'4ece9b76-aba0-4c41-bcc8-9bc8cae42902' AS id 
            UNION 
            SELECT  'Classics' AS SUBJECT,'f9527237-c3b3-448f-9dee-f59636ca8a6f' AS id 
            ) AS sub  ON   FIND_IN_SET(sub.id,u.reg_teacher_subjects) > 0 
            LEFT JOIN (
            SELECT  'Arabic' AS lang ,'28cb9cda-5958-4f28-b477-6554d9798c98' AS id 
            UNION 
            SELECT  'Chinese (Mandarin)' AS lang,'ba421ce5-166b-416c-8b33-1b64ea3b0afe' AS id 
            UNION 
            SELECT  'French' AS lang,'37837614-4b4f-4879-a8a0-161d6ea465e2' AS id 
            UNION 
            SELECT  'German' AS lang,'c4a46b8f-7996-4a5f-b50a-bb7f4cd0bdd3' AS id 
            UNION 
            SELECT  'Hebrew' AS lang,'1363d5df-5841-49f4-9766-82820685569f' AS id 
            UNION 
            SELECT  'Japanese' AS lang,'7d0f4368-23f8-4dbf-a7d4-59367583c755' AS id 
            UNION 
            SELECT  'Latin' AS lang,'2116a8bc-542f-416a-8432-78a506b65f02' AS id 
            UNION 
            SELECT  'Portuguese' AS lang,'ab9a14ea-e02b-48dd-bf75-69cf995a5a4f' AS id 
            UNION 
            SELECT  'Russian' AS lang,'b3867218-1879-4915-8336-72708b0d3ec6' AS id 
            UNION 
            SELECT  'Spanish' AS lang,'cb1ed39f-8981-4924-94e1-b2bbd92f18be' AS id 
            ) AS lang  ON   FIND_IN_SET(lang.id,u.reg_admin_languages) > 0 
            WHERE u.organization_id = organization_id AND u.is_deleted=0
            AND u.created_at >= from_date  
            AND u.created_at <= to_date 
            GROUP BY u.user_id
            ORDER BY u.created_at DESC
            ";

            DB::unprepared("DROP procedure IF EXISTS sp_user_demographic");
            DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS sp_user_demographic");
    }
}
