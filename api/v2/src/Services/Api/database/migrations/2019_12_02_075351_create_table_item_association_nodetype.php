<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItemAssociationNodetype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_association_nodetype', function (Blueprint $table) {
            $table->integer('source_document_type');
            $table->integer('destination_document_type');
            $table->integer('item_association_type');
            $table->string('node_type_id',36)->primary();
            $table->string('organization_id',36)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('created_by',36)->nullable(true);
            $table->string('updated_by',36)->nullable(true);
            $table->timestamp('created_at')->nullable(true);
            $table->timestamp('updated_at')->nullable(true);
            $table->unique(['source_document_type','destination_document_type','item_association_type'],'composite_index');
        });
    }                                                    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_association_nodetype');
    }
}
