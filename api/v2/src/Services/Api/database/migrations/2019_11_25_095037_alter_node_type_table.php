<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNodeTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('node_types', function (Blueprint $table) {
            $table->tinyInteger('used_for')->default(0)->comment('0 -> default, 1 -> item_asssociation_metadata');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('node_types', function (Blueprint $table) {
            $table->dropColumn('used_for');
        });
    }
}
