<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCurrOrgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		$users = DB::table('users')->select('user_id','current_organization_id','role_id')
                          ->where('is_deleted','=',0)->get()->toArray();
        $userList = json_decode(json_encode($users),true);
        foreach ($userList as $user)
        {
			if(!empty($user['current_organization_id']))
			{
				$checkUserOrgExists = DB::table('users_organization')->select('user_id','organization_id','role_id')
							  ->where('user_id','=',$user['user_id'])
							  ->where('organization_id','=',$user['current_organization_id'])
							  ->where('role_id','=',$user['role_id'])
							  ->get()->toArray();
				if(empty($checkUserOrgExists)){
					$userOrg  = ['user_id'         => $user['user_id'],
							 'organization_id' => $user['current_organization_id'],
							 'role_id'         => $user['role_id'],
							 'is_active'         => 1,
							];
					DB::table('users_organization')->insert($userOrg);
				}
			}            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users_organization')->delete();
    }
}
