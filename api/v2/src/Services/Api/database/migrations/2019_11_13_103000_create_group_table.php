<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('group');
        Schema::create('group', function (Blueprint $table) {
            $table->string('group_id', 36)->primary();
            $table->string('name', 100);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('organization_id', 36);
            $table->string('user_id', 36);           
            $table->string('created_by', 36)->nullable(true);
            $table->string('updated_by', 36)->nullable(true);
            $table->integer('is_user_default')->default(0)->comment = 'Default for a user';
            $table->integer('is_default')->default(0)->comment = 'Default for a organization';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group');
    }
}
