<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaxonomyJsonFileS3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->string('taxonomy_json_file_s3', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->dropColumn('taxonomy_json_file_s3');
        });
    }
}
