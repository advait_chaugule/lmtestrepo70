<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_types', function (Blueprint $table) {
            $table->string('node_type_id', 36)->primary();
            $table->string('organization_id', 36);
            $table->string('title', 100)->nullable(true);
            $table->string('type_code', 100)->nullable(true);
            $table->string('hierarchy_code', 25)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->string('source_node_type_id', 36)->nullable(true);
            $table->tinyInteger('is_document')->default(0);
            $table->tinyInteger('is_custom')->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->tinyInteger('is_default')->default(0);
            $table->string('created_by', 36)->nullable(true);
            $table->string('updated_by', 36)->nullable(true);
            
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_types');
    }
}
