<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddMigrationForDeletedParentNode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        foreach (range(1, 10) as $i) {
            
            DB::raw("UPDATE  acmt_items SET is_deleted =1  WHERE is_deleted=0 AND parent_id IN (
                SELECT item_id FROM 
                (SELECT item_id FROM acmt_items WHERE is_deleted=1) AS temp
                )");
        
        }
        DB::raw("UPDATE item_association SET is_deleted=1 WHERE  origin_node_id IN (SELECT item_id FROM acmt_items WHERE is_deleted=1)");
        DB::raw("UPDATE item_association SET is_deleted=1 WHERE  destination_node_id IN (SELECT item_id FROM acmt_items WHERE is_deleted=1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
