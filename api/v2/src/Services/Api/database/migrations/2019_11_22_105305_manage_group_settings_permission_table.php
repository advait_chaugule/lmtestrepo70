<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ManageGroupSettingsPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = date('Y-m-d H:i:s');
        $permissionOrder = DB::table('permissions')
                    ->select('order')
                    ->where('permission_group','2')
                    ->orderBy('order','desc')
                    ->get()
                    ->toArray();
        $order = isset($permissionOrder[0]->order) ? $permissionOrder[0]->order : 0;
        $userPermission = [
                             "permission_id"=>"50e9de3e-cfcb-41a2-af4d-8f33d7a0c1c5 ",
                             "internal_name"=>"manage_group",
                             "display_name"=>"Manage group",
                             "permission_group"=>2,
                             "parent_permission_id"=>"",
                             "order"=>($order+1),
                             "created_at"=>$date,
                             "updated_at"=>$date
                          ];
        DB::table('permissions')->insert($userPermission);

        $adminPermission = [
            "permission_id"=>"381b057e-3ef1-47c5-b1cc-fa770e55ed8e",
            "internal_name"=>"admin_setup_for_manage_group",
            "display_name"=>"Admin setup for Manage group",
            "permission_group"=>2,
            "parent_permission_id"=>"",
            "order"=>($order+2),
            "created_at"=>$date,
            "updated_at"=>$date
        ];
        DB::table('permissions')->insert($adminPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
