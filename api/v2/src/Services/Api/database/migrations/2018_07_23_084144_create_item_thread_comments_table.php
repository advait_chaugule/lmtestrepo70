<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemThreadCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_thread_comments', function (Blueprint $table) {
            $table->string('item_thread_comment_id', 36)->primary();
            $table->string('item_id', 36)->nullable();
            $table->string('item_thread_id', 36)->nullable();
            $table->string('parent_id', 36)->nullable();
            $table->string('comment', 1000)->nullable(true);
            $table->string('created_by', 36)->nullable(true);
            $table->string('updated_by', 36)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('organization_id', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_thread_comments');
    }
}
