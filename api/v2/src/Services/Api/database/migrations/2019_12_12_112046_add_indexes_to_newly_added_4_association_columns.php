<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToNewlyAdded4AssociationColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->index('source_document_id');
            $table->index('source_item_id');
            $table->index('target_document_id');
            $table->index('target_item_id');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function(Blueprint $table)
        {
            $table->dropIndex(['source_document_id']);
            $table->dropIndex(['source_item_id']);
            $table->dropIndex(['target_document_id']);
            $table->dropIndex(['target_item_id']);        
        });
    }
}
