<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTagCustomPubstateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->string('custom_json_file_s3', 100)->nullable(true);
            $table->string('tag', 100)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->dropColumn('custom_json_file_s3');
            $table->dropColumn('tag');
        });
    }
}
