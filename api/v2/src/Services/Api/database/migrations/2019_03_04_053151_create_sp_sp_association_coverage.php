<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSpAssociationCoverage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "        
        CREATE PROCEDURE sp_association_coverage(
        IN target_document_id VARCHAR(36) ,
        IN source_document_id VARCHAR(36)
        ) 

        SELECT 
        target.human_coding_scheme AS target_coding_scheme 
        ,target.full_statement AS target_full_statement
        ,target_doc.title AS target_taxonomy_name
        ,1 AS no_of_associations
        ,target_node_type.title AS target_node_type
        ,source.human_coding_scheme AS source_coding_scheme 
        ,source.full_statement AS source_full_statement
        ,source_doc.title AS source_taxonomy_name
        ,source_node_type.title AS source_node_type
        ,
        CASE 
        WHEN association_type=1 THEN 'isChildOf'
        WHEN association_type=2 THEN 'isPeerOf'
        WHEN association_type=3 THEN 'isPartOf'
        WHEN association_type=4 THEN 'exactMatchOf'
        WHEN association_type=5 THEN 'precedes'
        WHEN association_type=6 THEN 'isRelatedTo'
        WHEN association_type=7 THEN 'replacedBy'
        WHEN association_type=8 THEN 'exemplar'
        WHEN association_type=9 THEN 'hasSkillLevel'
        ELSE ''
        END AS association_type
        ,ia.created_at,target.item_id AS target_item_id
        FROM acmt_item_associations ia 

        INNER JOIN acmt_items target ON target.item_id=ia.destination_node_id AND   target.document_id=target_document_id
        INNER JOIN acmt_items source ON source.item_id=ia.origin_node_id AND source.document_id =source_document_id

        INNER JOIN acmt_documents AS target_doc ON target_doc.document_id=target.document_id
        INNER JOIN acmt_documents AS source_doc ON source_doc.document_id=source.document_id

        INNER JOIN acmt_node_types AS target_node_type ON target_node_type.node_type_id=target.node_type_id
        INNER JOIN acmt_node_types AS source_node_type ON source_node_type.node_type_id=source.node_type_id

        ORDER BY source_doc.title,target_doc.title
        ";

        DB::unprepared("DROP procedure IF EXISTS sp_association_coverage");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS sp_association_coverage");
    }
}
