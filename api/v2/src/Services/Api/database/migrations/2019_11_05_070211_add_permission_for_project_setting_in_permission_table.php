<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForProjectSettingInPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = date('Y-m-d H:i:s');
        $userProjectPermission = [
            "permission_id"=>"6bac2a57-ff7b-4a72-a29c-fd4bccbb83d9",
            "internal_name"=>"update_project_table_view",
            "display_name"=>"Update project table view",
            "permission_group"=>1,
            "parent_permission_id"=>"",
            "order"=>"10",
            "created_at"=>$date,
            "updated_at"=>$date
        ];
        DB::table('permissions')->insert($userProjectPermission);

        $adminProjectPermission = [
            "permission_id"=>"d0506caa-ab39-46d4-8a37-48a8489895eb",
            "internal_name"=>"admin_setup_for_project_table_view",
            "display_name"=>"Admin setup for project table view",
            "permission_group"=>1,
            "parent_permission_id"=>"",
            "order"=>"11",
            "created_at"=>$date,
            "updated_at"=>$date
        ];
        DB::table('permissions')->insert($adminProjectPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
