<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use \Ramsey\Uuid\Uuid;


class CreatePublicReviewWorkflowToDefaultTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        //  
        
        $listOrganization = DB::table('organizations')
        ->select('organization_id')
        ->where('is_deleted','0')
        ->get()
        ->toArray();
        $list = array();
        foreach($listOrganization as $listOrganizationK=>$listOrganizationV)
        {
            array_push($list,$listOrganizationV->organization_id);
        }

        $roleCode = config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"];
        $workflow_code = config("selfRegistration")["WORKFLOW"]["DEFAULT_WORKFLOW_PROJECT_PUBLIC_REVIEW"];
        $listRoles = DB::table('roles')
        ->select('role_id','organization_id')
        ->where('is_deleted','0')
        ->where('role_code',$roleCode)
        ->get()
        ->toArray();
        foreach($listRoles as $listRolesK=>$listRolesV)
        {
            $organizationID = $listRolesV->organization_id;
            $roleID = $listRolesV->role_id;

                $workflow_id = Uuid::uuid4();
                $workflowname = 'Public Review';
        
                $acmt_workflows = array(
                    array('workflow_id' => $workflow_id,
                    'organization_id' => $organizationID ,
                    'name' => $workflowname,
                    'workflow_code' => $workflow_code,
                    'is_active' => '1',
                    'is_deleted' => '0',
                    'is_default' => '0',
                    'updated_by' => NULL,
                    'created_at' => '2019-01-10 10:06:16',
                    'updated_at' => '2019-01-10 10:06:16')
                  );
                  DB::table('workflows')->insert($acmt_workflows);

                  $stage_id = Uuid::uuid4();
                $stageName = 'Stage 1';
                
                  $acmt_stage = array(
                    array('stage_id' => $stage_id,
                    'organization_id' => $organizationID,
                    'name' => $stageName,
                    'order' => '1',
                    'is_deleted' => '0')
                  );

                  DB::table('stage')->insert($acmt_stage);
                  
              
                  $workflow_stage_id = $workflow_id."||".$stage_id;

                  $acmt_workflow_stage = array(
                    array('workflow_stage_id' => $workflow_stage_id,
                    'workflow_id' => $workflow_id,
                    'stage_id' => $stage_id,
                    'order' => '1',
                    'stage_name' => $stageName,
                    'stage_description' => $stageName)
                  );
                  
                  DB::table('workflow_stage')->insert($acmt_workflow_stage);

                  $workflow_stage_role_id = $workflow_id."||".$stage_id."||".$roleID;
                  $acmt_workflow_stage_role = array(
                    array('workflow_stage_role_id' => $workflow_stage_role_id,
                    'workflow_stage_id' => $workflow_stage_id,
                    'role_id' => $roleID)
                  );
                  DB::table('workflow_stage_role')->insert($acmt_workflow_stage_role);

               unset($acmt_workflow_stage_role);
               unset($acmt_workflow_stage);
               unset($acmt_stage);
               unset($acmt_workflows); 
        }
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
