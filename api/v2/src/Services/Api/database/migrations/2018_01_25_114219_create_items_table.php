<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    private $table = "items";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->string('item_id', 36)->primary();
            $table->string('parent_id', 36)->nullable(true)->comment('##refers to id field of parent item');
            $table->string('document_id', 36)->comment('##refers to document table');
            $table->longText('full_statement');
            $table->string('alternative_label', 100)->nullable(true);
            $table->string('item_type_id', 36)->nullable(true);
            $table->string('human_coding_scheme', 4000)->nullable(true);
            $table->string('list_enumeration', 4000)->nullable(true);
            $table->string('abbreviated_statement', 50)->nullable(true);
            $table->string('concept_id', 36)->nullable(true);
            $table->string('notes', 6000)->nullable(true); 
            $table->string('language_id', 36)->nullable(true);
            $table->string('education_level', 50)->nullable(true);
            $table->string('license_id', 36)->nullable(true);
            $table->timestamp('status_start_date')->nullable(true);
            $table->timestamp('status_end_date')->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('updated_by', 36)->nullable(true);
            $table->string('organization_id', 36)->nullable(true);
            $table->string('source_item_id', 80)->nullable(true);
            $table->string('node_type_id', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
