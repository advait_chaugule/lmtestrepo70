<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateItemAssociationsTableWithNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('item_associations')->update(['item_associations.source_document_id' => DB::raw('acmt_item_associations.document_id')]);
        DB::table('item_associations')->update(['item_associations.source_item_id' => DB::raw('acmt_item_associations.origin_node_id')]);
        DB::table('item_associations')->update(['item_associations.target_item_id' => DB::raw('acmt_item_associations.destination_node_id')]);

        $destination_node_id = DB::table('item_associations')
                                    ->select('destination_node_id')
                                    ->where('target_document_id','')
                                    ->where('is_deleted',0)
                                    ->get()
                                    ->chunk(1000, function($destination_node_idDetail)
                                    {
                                        foreach ($destination_node_idDetail as $destination_node_idK)
                                        {
                                            $destination_node_id = $destination_node_idK;
                                        }
                                    });

        
        $getDestinationNodeIdToArray = array();
        
        
        foreach($destination_node_id as $destination_node_idK=>$destination_node_idV)
        {
            foreach($destination_node_idV as $destination_node_idVKey=>$destination_node_idVValue)
            {
                $getDestinationNodeIdToArray[] =$destination_node_idVValue->destination_node_id;
            }
            
        }

        
        $getDestinationNodeIdToArray = array_unique($getDestinationNodeIdToArray);
        $getDestinationNodeIdToArray1000Chunk = array_chunk($getDestinationNodeIdToArray,1000);
        
        $target_document_id = array();
        foreach($getDestinationNodeIdToArray1000Chunk as $getDestinationNodeIdToArray1000ChunkKey=>$getDestinationNodeIdToArray1000ChunkValue)
        {
            $target_document_id[] = DB::table('items')
                                    ->select('item_id','document_id')
                                    ->whereIn('item_id',$getDestinationNodeIdToArray1000ChunkValue)
                                    ->get()
                                    ->toArray();
        }                        
        
        $getItemIdMappedWithDocumentIdObtainedFromQuery = array();
        $i=0;
        
        foreach($target_document_id as $target_document_idK=>$target_document_idV)
        {
            foreach($target_document_idV as $target_document_idVKey=>$target_document_idVValue)
            {
                $getItemIdMappedWithDocumentIdObtainedFromQuery[$i][$target_document_idVValue->item_id] = $target_document_idVValue->document_id;
                $i = $i+1;
            }
        }
        
        
        $mapItemIdToDocumentIdRaw1 = array();
        $getItemIdArray = array();
        $a = 0;
        foreach($getDestinationNodeIdToArray as $getDestinationNodeIdToArrayK=>$getDestinationNodeIdToArrayV)
        {
            foreach($getItemIdMappedWithDocumentIdObtainedFromQuery as $getItemIdMappedWithDocumentIdObtainedFromQueryK=>$getItemIdMappedWithDocumentIdObtainedFromQueryV)
            {
                if(array_key_exists($getDestinationNodeIdToArrayV,$getItemIdMappedWithDocumentIdObtainedFromQueryV))
                {
                    foreach($getItemIdMappedWithDocumentIdObtainedFromQueryV as $getItemIdMappedWithDocumentIdObtainedFromQueryVKey=>$getItemIdMappedWithDocumentIdObtainedFromQueryVValue)
                    {
                        $mapItemIdToDocumentIdRaw1[$a]['item_id']       = $getItemIdMappedWithDocumentIdObtainedFromQueryVKey;
                        $mapItemIdToDocumentIdRaw1[$a]['document_id']   = $getItemIdMappedWithDocumentIdObtainedFromQueryVValue;
                        $a = $a + 1;

                        $getItemIdArray[] = $getItemIdMappedWithDocumentIdObtainedFromQueryVKey;
                    }
                }
            }
        }
        
        
        $getDiffId = array();
        $getDiffId = array_unique(array_diff($getDestinationNodeIdToArray,$getItemIdArray));
       
        $getDiffId1000Chunk = array_chunk($getDiffId,1000);
        $target_document_id2 = array();
        foreach($getDiffId1000Chunk as $getDiffId1000ChunkKey=>$getDiffId1000ChunkValue)
        {
            $target_document_id2[] = DB::table('documents')
                                    ->select('document_id')
                                    ->whereIn('document_id',$getDiffId1000ChunkValue)
                                    ->get()
                                    ->toArray();
        }                        
        
        
        $getDocumentIdArray = array();
        
        foreach($target_document_id2 as $target_document_id2K=>$target_document_id2V)
        {
            foreach($target_document_id2V as $target_document_id2VKey=>$target_document_id2VValue)
            {
                $getDocumentIdArray[] = $target_document_id2VValue->document_id;
            }
            
        }
    
        $mapItemIdToDocumentIdRaw2 = array();
        $getItemIdArray2 = array();
        $d = 0;
        foreach($getDiffId as $getDiffIdK=>$getDiffIdV)
        {
            foreach($getDocumentIdArray as $getDocumentIdArrayK=>$getDocumentIdArrayV)
            {
                if($getDiffIdV==$getDocumentIdArrayV)
                {
                    $mapItemIdToDocumentIdRaw2[$d]['item_id']       = $getDiffIdV;
                    $mapItemIdToDocumentIdRaw2[$d]['document_id']   = $getDocumentIdArrayV;
                    $d = $d + 1;

                    $getItemIdArray2[] = $getDiffIdV;

                }
            }
        }
        

        $getIdsOutSideTenant = array();
        $getIdsOutSideTenant = array_unique(array_diff($getDiffId,$getItemIdArray2));
        
        $mapItemIdToDocumentIdMain = array();
        $mapItemIdToDocumentIdMain = array_merge($mapItemIdToDocumentIdRaw1,$mapItemIdToDocumentIdRaw2);
        
        $mapItemIdToDocumentIdMain1000 = array_chunk($mapItemIdToDocumentIdMain,1000);
        
        
        foreach($mapItemIdToDocumentIdMain1000 as $mapItemIdToDocumentIdMain1000K=>$mapItemIdToDocumentIdMain1000V)
        {
            $statement = '';
            foreach($mapItemIdToDocumentIdMain1000V as $mapItemIdToDocumentIdMain1000VKey=>$mapItemIdToDocumentIdMain1000VValue)
            {
                $statement .= "UPDATE acmt_item_associations SET target_document_id ='".$mapItemIdToDocumentIdMain1000VValue['document_id']."' where destination_node_id ='".$mapItemIdToDocumentIdMain1000VValue['item_id']."'; ";
                
            }
            
            DB::unprepared($statement);
            unset($statement);    
            
            
        }
        
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
