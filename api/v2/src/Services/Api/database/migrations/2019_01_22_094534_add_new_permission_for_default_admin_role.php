<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionForDefaultAdminRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $listOrganization = DB::table('organizations')
        ->select('organization_id')
        ->where('is_deleted','0')
        ->get()
        ->toArray();
        $list = array();
        $permissionArr = array();
        foreach($listOrganization as $listOrganizationK=>$listOrganizationV)
        {
            array_push($list,$listOrganizationV->organization_id);
        }
        $roleCode = config("selfRegistration")["ROLE"]["SYSTEM_DEFAULT_ADMIN_ROLE"];
        $listRoles = DB::table('roles')
        ->select('role_id','organization_id')
        ->where('is_deleted','0')
        ->where('role_code',$roleCode)
        ->get()
        ->toArray();

            $listPermission = DB::table('permissions')
            ->select('permission_id')
            ->where('permission_group','10')
            ->get()
            ->toArray(); 
            foreach($listPermission as $listPermissionK=>$listPermissionV)
            {
                array_push($permissionArr,$listPermissionV->permission_id);
            }   

        foreach($listRoles as $listRolesK=>$listRolesV)
        {
            $organizationID = $listRolesV->organization_id;
            $roleID = $listRolesV->role_id;
            foreach($permissionArr as $permissionArrK=>$permissionArrV)
            {
                $insertPermission = array(
                    array('role_id' => $roleID,
                    'permission_id' => $permissionArrV)
                );
                DB::table('role_permission')->insert($insertPermission);
                unset($insertPermission);
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
