<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class AddDefaultValueFromPreviousColumnForTheHtml extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $prefix = DB::getTablePrefix();    
        DB::statement("UPDATE  ".$prefix."documents SET title_html= title, notes_html=notes,publisher_html=publisher,description_html=description");
        DB::statement("UPDATE  ".$prefix."items SET full_statement_html= full_statement, alternative_label_html=alternative_label,human_coding_scheme_html=human_coding_scheme,abbreviated_statement_html=abbreviated_statement,notes_html=notes");
        DB::statement("UPDATE  ".$prefix."concepts SET title_html= title,description_html=description");
        DB::statement("UPDATE  ".$prefix."subjects SET title_html= title,description_html=description");
        DB::statement("UPDATE  ".$prefix."licenses SET title_html= title,description_html=description,license_text_html=license_text");
        DB::statement("UPDATE  ".$prefix."document_metadata SET metadata_value_html= metadata_value");
        DB::statement("UPDATE  ".$prefix."item_metadata SET metadata_value_html= metadata_value");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
