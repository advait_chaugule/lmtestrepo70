<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExternalNodeDescriptionToItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            $table->string('description', 1000)
                  ->nullable(true)
                  ->after('external_node_title')
                  ->comment("Will be used to store exemplar description if any");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
