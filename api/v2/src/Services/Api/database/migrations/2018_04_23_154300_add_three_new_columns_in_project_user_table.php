<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreeNewColumnsInProjectUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_users', function (Blueprint $table) {
            $table->string('created_by', 36)->nullable(true);
            $table->string('updated_by', 36)->nullable(true);
            $table->string('deleted_by', 36)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('project_users', function (Blueprint $table) {
        //     $table->dropColumn('created_by', 36)->nullable(true);
        //     $table->dropColumn('updated_by', 36)->nullable(true);
        //     $table->dropColumn('deleted_by', 36)->nullable(true);
        // });
    }
}
