<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHtmlColumnsInDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->longText('title_html')->nullable();
            $table->longText('notes_html')->nullable();
            $table->longText('publisher_html')->nullable();
            $table->longText('description_html')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('title_html');
            $table->dropColumn('notes_html');
            $table->dropColumn('publisher_html');
            $table->dropColumn('description_html');
        });
    }
}
