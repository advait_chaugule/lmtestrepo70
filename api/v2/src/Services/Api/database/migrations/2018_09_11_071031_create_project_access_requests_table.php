<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectAccessRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_access_requests', function (Blueprint $table) {
            $table->string('project_access_request_id', 36)->primary();
            $table->string('organization_id', 36);
            $table->string('project_id', 36);
            $table->string('user_id', 36);
            $table->string('workflow_stage_role_ids', 5000);
            $table->tinyInteger('status')->default(1);
            $table->string('comment', 5000)->nullable(true);
            $table->timestamp('created_at')->nullable(true);
            $table->timestamp('updated_at')->nullable(true);
            $table->timestamp('status_changed_at')->nullable(true);
            $table->string('updated_by', 36)->nullable(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_access_requests');
    }
}
