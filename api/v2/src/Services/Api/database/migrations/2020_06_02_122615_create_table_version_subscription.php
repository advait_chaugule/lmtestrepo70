<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVersionSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_subscription', function (Blueprint $table) {
            $table->string('subscription_id', 36)->primary();
            $table->string('sub_document_id', 36)->nullable(true)->index('version_subscription_sub_document_id')->comment('Subscriber doc id');
            $table->string('subscriber_detail_id', 36)->nullable(true)->index('version_subscription_subscriber_detail_id')->comment('Subscriber Detail id');
            $table->string('pub_source_document_id', 36)->nullable(true)->comment('Publisher source document id');
            $table->string('pub_document_id', 36)->nullable(true)->index('version_subscription_pub_document_id')->comment('Publisher doc id');
            $table->string('publisher_org_id', 36)->nullable(true)->index('version_subscription_publisher_org_id');
            $table->timestamp('subscription_datetime')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->tinyInteger('is_active')->default(1);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('version_subscription');
    }
}
