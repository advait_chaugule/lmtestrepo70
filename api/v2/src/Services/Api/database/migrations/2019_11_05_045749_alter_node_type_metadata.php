<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNodeTypeMetadata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $exists = $this->checkIfIndexExists('node_type_metadata_node_type_id_index');
        if(!$exists) {
            Schema::table('node_type_metadata', function (Blueprint $table) {
                $table->index('node_type_id', 'node_type_metadata_node_type_id_index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $exists = $this->checkIfIndexExists('node_type_metadata_node_type_id_index');
        if($exists) {
            Schema::table('node_type_metadata', function (Blueprint $table) {
                $table->dropIndex('node_type_metadata_node_type_id_index');
            });
        }
    }

    public function checkIfIndexExists($index_name) {
        $prefix = DB::getTablePrefix();
        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        $indexesFound = $sm->listTableIndexes($prefix.'node_type_metadata');

        if(array_key_exists("node_type_metadata_node_type_id_index", $indexesFound)) {
            return true;
        } else {
            return false;
        }
    }
}
