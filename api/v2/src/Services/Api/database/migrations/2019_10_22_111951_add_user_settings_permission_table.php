<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserSettingsPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = date('Y-m-d H:i:s');
        $userPermission = [
                             "permission_id"=>"04f2f055-5705-4078-90d6-c001661e84e8",
                             "internal_name"=>"Update_taxonomy_table_view",
                             "display_name"=>"Update taxonomy table view",
                             "permission_group"=>2,
                             "parent_permission_id"=>"",
                             "order"=>"15",
                             "created_at"=>$date,
                             "updated_at"=>$date
                          ];
        DB::table('permissions')->insert($userPermission);

        $adminPermission = [
            "permission_id"=>"0c2aaf9b-bd95-4c5e-8d7d-a67819affb54",
            "internal_name"=>"admin_setup_for_table_view",
            "display_name"=>"Admin setup for table view",
            "permission_group"=>2,
            "parent_permission_id"=>"",
            "order"=>"16",
            "created_at"=>$date,
            "updated_at"=>$date
        ];
        DB::table('permissions')->insert($adminPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
