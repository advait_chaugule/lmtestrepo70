<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDocumentToMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metadata', function (Blueprint $table) {
            $table->tinyInteger('is_document')
                  ->default(0)
                  ->after('is_custom')
                  ->comment("   0-> metadata associated to item only
                                1-> metadata associated to document only
                                2-> metadata associated to both document and item");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metadata', function (Blueprint $table) {
            $table->dropColumn('is_document');
        });
    }
}
