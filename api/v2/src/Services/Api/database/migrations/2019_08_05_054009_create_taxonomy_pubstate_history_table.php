<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomyPubstateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomy_pubstate_history', function (Blueprint $table) {
            $table->string('document_id',36);
            $table->integer('publish_number');
            $table->dateTime('publish_date');
            $table->dateTime('unpublish_date');
            $table->string('published_by',36)->nullable(true);
            $table->string('unpublished_by',36)->nullable(true);
            $table->string('organization_id',36)->nullable(true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('created_by',36)->nullable(true);;
            $table->string('updated_by',36)->nullable(true);;
            $table->timestamp('created_at')->nullable(true);
            $table->timestamp('updated_at')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('published_taxonomy_history');
    }
}
