<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Data\Models\PublicReview;
class UpdateProjectUserStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $queryData = DB::table('projects')->select('projects.project_id','thread_comments.created_by','projects.organization_id')
            ->join('project_items','project_items.project_id','=','projects.project_id')
            ->join('thread_comments','thread_comments.thread_source_id','=','project_items.item_id')
            ->where('project_type','=',3)
            ->where('project_items.is_deleted','=',0)
            ->where('thread_comments.is_deleted','=',0)
            ->where('projects.is_deleted','=',0)
            ->groupBy('thread_comments.created_by','projects.project_id')
            ->get()
            ->toArray();

        foreach ($queryData as $data) {
            $projectId = $data->project_id;
            $userId = $data->created_by;
            $organizationId = $data->organization_id;

               PublicReview::firstOrCreate(['project_id' => $projectId, 'user_id' => $userId],
                ['organization_id' => $organizationId,
                    'project_id' => $projectId,
                    'user_id' => $userId,
                    'review_status' => 2,
                    'is_deleted' => 0,
                    'created_by' => $userId,
                    'updated_by' => '']
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
