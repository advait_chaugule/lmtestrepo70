<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->string('permission_id', 36)->primary();
            $table->string('display_name', 36);
            $table->string('internal_name', 50);
            $table->tinyInteger('permission_group')->comment("1 = ProjectMgmt, 2 = TaxonomyMgmt, 3 = Role&UserMgmt");
            $table->string('parent_permission_id', 36);
            $table->integer('order');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
