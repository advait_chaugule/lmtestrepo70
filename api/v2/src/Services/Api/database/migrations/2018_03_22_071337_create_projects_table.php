<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->string('project_id', 36)->primary();
            $table->string('workflow_id', 36);
            $table->string('organization_id', 36);
            $table->string('project_name', 100)->nullable(true);
            $table->string('description', 1000)->nullable(true);
            $table->string('document_id', 36)->nullable(true);
            $table->string('current_workflow_stage_id', 80)->nullable(true);
            $table->smallInteger('is_deleted')->default(0);
            $table->string('updated_by', 36)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
