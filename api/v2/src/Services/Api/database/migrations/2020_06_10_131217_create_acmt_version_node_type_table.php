<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcmtVersionNodeTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_node_type', function (Blueprint $table) {
            $table->string('comparison_id', 36)->nullable(true);
            $table->string('node_id', 45)->nullable(true);
            $table->string('type', 45)->nullable(true);
            $table->string('node_type', 45)->nullable(true);
            $table->string('node_type_id', 45)->nullable(true);
            $table->string('action', 1)->nullable(true);
            $table->integer('count', false)->nullable(true);
            $table->text('value')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_node_type');
    }
}
