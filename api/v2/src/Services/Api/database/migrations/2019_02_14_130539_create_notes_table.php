<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
           $table->string('note_id')->primary();
            $table->string('title', 1000);
            $table->longText('description');
            $table->integer('note_status')->comment('O for Inactive, 1 for Active')->default(0);
            $table->integer('note_order')->default(0);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('organization_id', 36);
            $table->string('created_by', 36);
            $table->string('updated_by', 36)->nullable(true);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
