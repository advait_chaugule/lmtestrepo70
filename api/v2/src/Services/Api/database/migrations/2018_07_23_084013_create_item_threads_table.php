<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_threads', function (Blueprint $table) {
            $table->string('item_thread_id', 36)->primary();
            $table->string('item_id', 36)->nullable();
            $table->string('assign_to', 36)->nullable(true);
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('organization_id', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_threads');
    }
}
