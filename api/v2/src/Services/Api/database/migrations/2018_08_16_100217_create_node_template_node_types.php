<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodeTemplateNodeTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('node_template_node_types', function (Blueprint $table) {
            $table->string('node_template_id', 36);
            $table->string('node_type_id', 36);
            $table->string('parent_node_type_id', 36);
            $table->tinyInteger('sort_order');
            $table->tinyInteger('is_deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('node_template_node_types');
    }
}
