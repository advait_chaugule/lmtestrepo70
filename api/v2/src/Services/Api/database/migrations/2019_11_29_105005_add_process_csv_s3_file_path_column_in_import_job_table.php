<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessCsvS3FilePathColumnInImportJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('import_job', function (Blueprint $table) {
            $table->string('processed_csv_s3_file_path', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('import_job', function (Blueprint $table) {
            $table->dropColumn('processed_csv_s3_file_path');
        });
    }
}
