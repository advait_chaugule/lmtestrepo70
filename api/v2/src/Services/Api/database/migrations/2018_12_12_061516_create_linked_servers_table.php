<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkedServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('linked_servers', function (Blueprint $table) {
            $table->string('linked_server_id', 36)->primary();
            $table->string("organization_id", 36);
            $table->string('linked_name',100)->nullable(true);
            $table->string('server_url',1000)->nullable();
            $table->string('description',1000)->nullable();
            $table->smallInteger('linked_type')->default(1);
            $table->smallInteger('connection_status')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linked_servers');
    }
}
