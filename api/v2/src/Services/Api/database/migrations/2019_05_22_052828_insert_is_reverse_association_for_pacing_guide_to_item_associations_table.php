<?php
ini_set('memory_limit','2048M');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIsReverseAssociationForPacingGuideToItemAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itemsFromAssociationDocument   =   [];
        $reverseAssociationTypeArray    =   ['3', '5', '7'];
        $pacingGuideProjects = DB::table('projects')
            ->select('document_id')
            ->where('project_type', '2')
            ->get()->toArray();
        //dd($pacingGuideProjects);
        $documentIdArray = array_column($pacingGuideProjects, 'document_id');
        //dd($documentIdArray);
        foreach($documentIdArray as $documentId) {
            $originNodeDocumentIdArray      =   [];
            $originIdArray                  =   [];

            if(!empty($documentId)) {
                $associationOfStandards =   DB::table('item_associations')
                ->select('*')
                ->where(['document_id' => $documentId])
                ->get();

                foreach($associationOfStandards as $association) {
                    if($association->association_type != 1) {
                        $originIdArray[]   =   $association->origin_node_id;
                    }
                }

                if(sizeOf($originIdArray) > 0) {
                    $originNodeDetail  =   DB::table('items')
                    ->select('item_id','document_id')
                    ->whereIn('item_id', $originIdArray)
                    ->get();

                    if($originNodeDetail->count() > 0) {
                        
                        foreach($originNodeDetail as $originNode) {
                            $originNodeDocumentIdArray[$originNode->item_id] = !empty($originNode->item_id) ? $originNode->document_id : '';                          
                        }
    
                        foreach($originIdArray as $originNodeId) {
                            if(!empty($originNodeDocumentIdArray[$originNodeId])) {
                                $caseAssociationsToUpdate = [
                                    "is_reverse_association" => "1"
                                ];
        
                                $columnValueFilterPairs =   ['document_id' => $documentId, 'origin_node_id' => $originNodeId];
                                
                                DB::table('item_associations')->where($columnValueFilterPairs)->update($caseAssociationsToUpdate);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_associations', function (Blueprint $table) {
            //
        });
    }
}

