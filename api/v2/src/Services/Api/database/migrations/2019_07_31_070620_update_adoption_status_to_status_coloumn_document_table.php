<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Data\Models\Document;
class UpdateAdoptionStatusToStatusColoumnDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Document::where('adoption_status',7)
                  ->update(['status'=>3]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Document::where('adoption_status',7)
            ->update(['status'=>'']);
    }
}
