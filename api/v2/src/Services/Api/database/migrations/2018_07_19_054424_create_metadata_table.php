<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata', function (Blueprint $table) {
            $table->string('metadata_id', 36)->primary();
            $table->string('organization_id', 36);
            $table->string('parent_metadata_id', 36);
            $table->string('name', 50);
            $table->string('internal_name', 50)->nullable(true);
            $table->tinyInteger('field_type');
            $table->string('field_possible_values', 6000)->nullable(true);
            $table->tinyInteger('order');
            $table->tinyInteger('is_custom')->default(0);
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('updated_by', 36)->nullable(true);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metadata');
    }
}
