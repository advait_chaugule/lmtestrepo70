<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class DeletePermissionRolePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $permission = DB::table('role_permission')->whereIn('permission_id',
         array("4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34", 
        "acee9a40-f0c0-4576-9f5c-0becfc177907",
        "c15c1498-e94d-426c-84d9-bdc67e97c4bb",
        "918769fd-1e54-43a1-a09f-97034b8e4f22",
        "7e7e2562-5f65-42b3-b520-51802ab9ccbd",
        "ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4"))->delete();

        $permission = DB::table('permissions')->whereIn('permission_id',
         array("4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34", 
        "acee9a40-f0c0-4576-9f5c-0becfc177907",
        "c15c1498-e94d-426c-84d9-bdc67e97c4bb",
        "918769fd-1e54-43a1-a09f-97034b8e4f22",
        "7e7e2562-5f65-42b3-b520-51802ab9ccbd",
        "ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4"))->delete();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        $acmt_permissions = array(
            array('permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34','display_name' => 'List Workflow','internal_name' => 'WorkflowController@index','permission_group' => '3','parent_permission_id' => '','order' => '1','created_at' => '2018-04-10 05:56:00','updated_at' => '2018-04-10 05:56:00'),
            array('permission_id' => '7e7e2562-5f65-42b3-b520-51802ab9ccbd','display_name' => 'Rearrange Workflow Stage','internal_name' => 'WorkflowController@edit','permission_group' => '3','parent_permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34','order' => '5','created_at' => '2018-04-10 05:56:00','updated_at' => '2018-04-10 05:56:00'),
            array('permission_id' => '918769fd-1e54-43a1-a09f-97034b8e4f22','display_name' => 'Edit Workflow Stage','internal_name' => 'WorkflowController@UpdateWorkflowStage','permission_group' => '3','parent_permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb','order' => '4','created_at' => '2018-04-10 05:56:00','updated_at' => '2018-04-10 05:56:00'),
            array('permission_id' => 'acee9a40-f0c0-4576-9f5c-0becfc177907','display_name' => 'Edit Workflow','internal_name' => 'WorkflowController@GetWorkflow','permission_group' => '3','parent_permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34','order' => '2','created_at' => '2018-04-10 05:56:00','updated_at' => '2018-04-10 05:56:00'),
            array('permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb','display_name' => 'List Workflow Stages','internal_name' => 'WorkflowController@GetAllWorflowStages','permission_group' => '3','parent_permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34','order' => '3','created_at' => '2018-04-10 05:56:00','updated_at' => '2018-04-10 05:56:00'),
            array('permission_id' => 'ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4','display_name' => 'Delete Role from Workflow Stage','internal_name' => 'WorkflowController@stageRoleDelete','permission_group' => '3','parent_permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb','order' => '6','created_at' => '2018-04-10 05:56:00','updated_at' => '2018-04-10 05:56:00')
          );

        DB::table('permissions')->insert($acmt_permissions);


        $acmt_role_permission = array(
            array('role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa','permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34'),
            array('role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa','permission_id' => 'acee9a40-f0c0-4576-9f5c-0becfc177907'),
            array('role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa','permission_id' => '918769fd-1e54-43a1-a09f-97034b8e4f22'),
            array('role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa','permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb'),
            array('role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa','permission_id' => '7e7e2562-5f65-42b3-b520-51802ab9ccbd'),
            array('role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa','permission_id' => 'ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4'),
            array('role_id' => 'd1b83505-70ee-46ea-991a-df82d4b390eb','permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34'),
            array('role_id' => 'd1b83505-70ee-46ea-991a-df82d4b390eb','permission_id' => 'acee9a40-f0c0-4576-9f5c-0becfc177907'),
            array('role_id' => 'd1b83505-70ee-46ea-991a-df82d4b390eb','permission_id' => '918769fd-1e54-43a1-a09f-97034b8e4f22'),
            array('role_id' => 'd1b83505-70ee-46ea-991a-df82d4b390eb','permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb'),
            array('role_id' => 'd1b83505-70ee-46ea-991a-df82d4b390eb','permission_id' => '7e7e2562-5f65-42b3-b520-51802ab9ccbd'),
            array('role_id' => 'd1b83505-70ee-46ea-991a-df82d4b390eb','permission_id' => 'ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4'),
            array('role_id' => '8d69a1a1-a82d-4550-8a32-4c67d08349d2','permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34'),
            array('role_id' => '8d69a1a1-a82d-4550-8a32-4c67d08349d2','permission_id' => 'acee9a40-f0c0-4576-9f5c-0becfc177907'),
            array('role_id' => '8d69a1a1-a82d-4550-8a32-4c67d08349d2','permission_id' => '918769fd-1e54-43a1-a09f-97034b8e4f22'),
            array('role_id' => '8d69a1a1-a82d-4550-8a32-4c67d08349d2','permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb'),
            array('role_id' => '8d69a1a1-a82d-4550-8a32-4c67d08349d2','permission_id' => '7e7e2562-5f65-42b3-b520-51802ab9ccbd'),
            array('role_id' => '8d69a1a1-a82d-4550-8a32-4c67d08349d2','permission_id' => 'ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4'),
            array('role_id' => 'cbd20e40-0524-45de-b669-495a7decfc0d','permission_id' => '4c00d8c5-10a2-4eaa-ae83-9c997d6e4a34'),
            array('role_id' => 'cbd20e40-0524-45de-b669-495a7decfc0d','permission_id' => '7e7e2562-5f65-42b3-b520-51802ab9ccbd'),
            array('role_id' => 'cbd20e40-0524-45de-b669-495a7decfc0d','permission_id' => '918769fd-1e54-43a1-a09f-97034b8e4f22'),
            array('role_id' => 'cbd20e40-0524-45de-b669-495a7decfc0d','permission_id' => 'acee9a40-f0c0-4576-9f5c-0becfc177907'),
            array('role_id' => 'cbd20e40-0524-45de-b669-495a7decfc0d','permission_id' => 'c15c1498-e94d-426c-84d9-bdc67e97c4bb'),
            array('role_id' => 'cbd20e40-0524-45de-b669-495a7decfc0d','permission_id' => 'ccbc7dbf-2cb6-4e0d-8df5-1c63fd0ceed4')
          );

        DB::table('role_permission')->insert($acmt_role_permission);

       
    }
}
