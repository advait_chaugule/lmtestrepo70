<?php
use Faker\Generator as Faker;
use App\Data\Models\Document;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(Document::class, function (Faker $faker) {
    return [
        'document_id' => $faker->uuid,
        'title'       => $faker->text(15),
        'official_source_url' => "http://www.nextgenscience.org/sites/ngss/files/NGSS%20DCI%20Combined%2011.6.13.pdf",
        'language_id' => "42ef69f9-8b02-43ee-85e2-83a5de6998b9",
        'adoption_status' => 7,
        'status'          => 1,
        'notes'           => $faker->text(15),
        'publisher'       => "consulting group",
        'description'     => $faker->text(150),
        'version'         => 2019,
        'status_start_date' => date('Y-m-d H:i:s'),
        'status_end_date'   => date('Y-m-d H:i:s'),
        'license_id'        => 123,
        'organization_id'   => "a83db6c0-1a5e-428e-8384-c8d58d2a83ff",
        'document_type'    => 1,
        'creator'    => "Organization 1",
        'is_deleted'    => 0,
        'created_by'    => '4fa46dbd-e314-40cb-9439-929b94779fd6',
        'updated_by'    => '4fa46dbd-e314-40cb-9439-929b94779fd6',
        'source_document_id' => '02d870db-83e1-d4ef-3586-54d1aabf46d6',
        'pulished_taxonomy_snapshot_s3_zip_file_id' => '',
        'node_type_id' => 'fa65eb18-d6d4-4480-a127-e4583814bc2c',
        'project_id' => '',
        'node_template_id' => '33f5593b-018d-4ce1-ab48-15594343b400',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});
?>