<?php
use Faker\Generator as Faker;

use App\Data\Models\Project;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(Project::class, function (Faker $faker) {
    return [
        'project_id' => $faker->uuid,
        'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
        'project_name' => $faker->text(15),
        'description' => $faker->text(150),
        'workflow_id' => "a428e24e-bdbd-46a2-af60-f37b261235c2"
    ];
});
