<?php
use Faker\Generator as Faker;

use App\Data\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(User::class, function (Faker $faker) {
    /**
     * @var string
     */
    static $password;

    $id = $faker->uuid;

    return [
        'user_id' => $id,
        'username' => $faker->userName . "--$id",
        'email' => $faker->safeEmail . "--$id",
        'password' => bcrypt('P@ssw0rd'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
        'role_id' => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa',
        'active_access_token' => $faker->uuid
    ];
});
