<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class LicenseSeeder extends Seeder
{
    private $table = "licenses";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
         $license = $this->getLicense();
        foreach( $license as  $licenseToInsert) {
            $searchLicense =   DB::table($this->table)->where('license_id',  $licenseToInsert['license_id']);
            if($searchLicense->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('license_id',  $licenseToInsert['license_id'])->update( $licenseToInsert);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert( $licenseToInsert);
            }
        }
    }

    private function getLicense()
    {
        return 
        [
            ["license_id"=>"1afa0b98-e321-4a71-8a34-4efb54724f56","source_license_id"=>"1afa0b98-e321-4a71-8a34-4efb54724f56","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"License1",
            "created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["license_id"=>"705123bc-403e-40a0-94b3-3acf3c60e87b","source_license_id"=>"705123bc-403e-40a0-94b3-3acf3c60e87b","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"License2","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["license_id"=>"a96d616e-7d90-409b-8ad5-b63556904ad7","source_license_id"=>"a96d616e-7d90-409b-8ad5-b63556904ad7",
            "organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"License1","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["license_id"=>"5c93d55a-c30e-47e6-9d9e-94c1982faa5d","source_license_id"=>"5c93d55a-c30e-47e6-9d9e-94c1982faa5d","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"License2","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            //onlyforstaging
            ["license_id"=>"c8413318-ad7d-4ab4-9b96-bdfe7e1aba6c","source_license_id"=>"c8413318-ad7d-4ab4-9b96-bdfe7e1aba6c","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"License1","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["license_id"=>"166c49a7-b685-43d9-8b46-7ece61887e36",
            "source_license_id"=>"166c49a7-b685-43d9-8b46-7ece61887e36","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"License2","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"]
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
