<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    private $table = "languages";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $language = $this->getData();
        // insert the fresh datasets
        foreach($language as $languageToInsert) {
            $searchLanguage =   DB::table($this->table)->where('language_id', $languageToInsert['language_id']);
            if($searchLanguage->count() > 0 ) {
                // update Language
                DB::table($this->table)->where('language_id', $languageToInsert['language_id'])->update($languageToInsert);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($languageToInsert);
            }
        }
    }

    private function getData()
    {
        return 
        [
            ["language_id"=>"42ef69f9-8b02-43ee-85e2-83a5de6998b9","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"English","short_code"=>"en","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"03b508a0-06ec-4e9a-b41c-35da9d94d20b","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Afrikaans","short_code"=>"af","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"994bcb3a-dff4-4cab-85e6-53809530895f","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"German","short_code"=>"de","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"90e60f5e-fab1-411b-b989-5921a36a1944","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"English","short_code"=>"en","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"1bbf7afe-54a0-4c2c-b53a-4b1e071efd07","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Afrikaans","short_code"=>"af","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"11810e1f-22bc-4beb-8817-9b5ac131d347","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"German","short_code"=>"de","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            //onlyforstaging
            ["language_id"=>"6c002328-d6a9-4b3b-b2bb-a5dc40707d44","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"English","short_code"=>"en","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"a43bcb30-a659-4ac2-a9ec-fe4161630401","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Afrikaans","short_code"=>"af","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["language_id"=>"992316b3-f954-4677-bcc0-34515eca2915","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"German","short_code"=>"de","created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"]
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
