<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class WorkflowStageSeeder extends Seeder
{
    private $table = "workflow_stage";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $workflowStage = $this->getWorkflowStageData();
        
        foreach($workflowStage as $workflowStageToInsertOrUpdate) {
            $searchWorkflowStage =   DB::table($this->table)->where('workflow_stage_id', $workflowStageToInsertOrUpdate['workflow_stage_id']);
            if($searchWorkflowStage->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('workflow_stage_id', $workflowStageToInsertOrUpdate['workflow_stage_id'])->update($workflowStageToInsertOrUpdate);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($workflowStageToInsertOrUpdate);
            }
        }
    }

    private function getWorkflowStageData()
    {
        return 
        [ ["workflow_stage_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2||97abdc88-0b08-4d76-88ca-6fb09fd08544","workflow_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2","stage_id"=>"97abdc88-0b08-4d76-88ca-6fb09fd08544","stage_name"=>"Authoring","stage_description"=>"Authoring","order"=>"1"],
            ["workflow_stage_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2||e57deb64-cf2b-4711-87f5-4c091b07db8d","workflow_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2","stage_id"=>"e57deb64-cf2b-4711-87f5-4c091b07db8d","stage_name"=>"Internal Review","stage_description"=>"Internal Review","order"=>"2"],
            ["workflow_stage_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2||ad083d46-4a63-4830-a7a4-fe21f23c9c4e","workflow_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2","stage_id"=>"ad083d46-4a63-4830-a7a4-fe21f23c9c4e","stage_name"=>"External review","stage_description"=>"External Review","order"=>"3"],
            ["workflow_stage_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2||e22bc74f-226e-4766-8853-ee15f27621e7","workflow_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2","stage_id"=>"e22bc74f-226e-4766-8853-ee15f27621e7","stage_name"=>"Published","stage_description"=>"Published","order"=>"4"],
            //otherorganization
            ["workflow_stage_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42||c70604c6-cc88-4d6a-949d-69bab299dda7","workflow_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42","stage_id"=>"c70604c6-cc88-4d6a-949d-69bab299dda7","stage_name"=>"Authoring","stage_description"=>"Authoring","order"=>"1"],
            ["workflow_stage_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42||0a7e7a4c-06bd-496c-82fe-b2dcc69c0af0","workflow_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42","stage_id"=>"0a7e7a4c-06bd-496c-82fe-b2dcc69c0af0","stage_name"=>"Internal Review","stage_description"=>"Internal Review","order"=>"2"],
            ["workflow_stage_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42||37f42483-50ef-46e1-8c49-59645811cbb8","workflow_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42","stage_id"=>"37f42483-50ef-46e1-8c49-59645811cbb8","stage_name"=>"External review","stage_description"=>"External Review","order"=>"3"],
            ["workflow_stage_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42||82c675f6-fbe1-4e33-9e86-2e0ff9c111ce","workflow_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42","stage_id"=>"82c675f6-fbe1-4e33-9e86-2e0ff9c111ce","stage_name"=>"Published","stage_description"=>"Published","order"=>"4"],
            //onlyforstaging
            ["workflow_stage_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef||b6b56543-335d-4a51-ad47-56e1ceb7363b","workflow_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef","stage_id"=>"b6b56543-335d-4a51-ad47-56e1ceb7363b","stage_name"=>"Authoring","stage_description"=>"Authoring","order"=>"1"],
            ["workflow_stage_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef||c066da9f-9f95-4e77-9a20-eee757c26707","workflow_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef","stage_id"=>"c066da9f-9f95-4e77-9a20-eee757c26707","stage_name"=>"Internal Review","stage_description"=>"Internal Review","order"=>"2"],
            ["workflow_stage_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef||2fe0737c-d3bc-47c5-8cfa-dc874ec8eb59","workflow_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef","stage_id"=>"2fe0737c-d3bc-47c5-8cfa-dc874ec8eb59","stage_name"=>"External review","stage_description"=>"External Review","order"=>"3"],
            ["workflow_stage_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef||d3274391-41bc-4526-826b-d51d5e8885e0","workflow_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef","stage_id"=>"d3274391-41bc-4526-826b-d51d5e8885e0","stage_name"=>"Published","stage_description"=>"Published","order"=>"4"]];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
