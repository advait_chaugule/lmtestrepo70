<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class ItemTypeSeeder extends Seeder
{
    private $table = "item_types";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $itemType = $this->getItemType();
        // insert the fresh datasets
        foreach($itemType as $itemTypeToInsert) {
            $searchItemType =   DB::table($this->table)->where('item_type_id', $itemTypeToInsert['item_type_id']);
            if($searchItemType->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('item_type_id', $itemTypeToInsert['item_type_id'])->update($itemTypeToInsert);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($itemTypeToInsert);
            }
        }
    }

    private function getItemType()
    {
        return 
        [
            ["item_type_id"=>"a3a738cc-db86-4603-b729-6df44a229a8b","source_item_type_id"=>"a3a738cc-db86-4603-b729-6df44a229a8b","title"=>"Item Type 1","created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s")],
            ["item_type_id"=>"4ccd5a28-bc73-4e3c-92bf-123b0bccde0d","source_item_type_id"=>"4ccd5a28-bc73-4e3c-92bf-123b0bccde0d","title"=>"Item Type 2","created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s")],
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
