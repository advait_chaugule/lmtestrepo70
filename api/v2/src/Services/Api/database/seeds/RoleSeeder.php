<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    private $table = "roles";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $role = $this->getRoleData();

        foreach($role as $roleToInsertOrUpdate) {
            $searchRole =   DB::table($this->table)->where('role_id', $roleToInsertOrUpdate['role_id']);
            if($searchRole->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('role_id', $roleToInsertOrUpdate['role_id'])->update($roleToInsertOrUpdate);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($roleToInsertOrUpdate);
            }
        }      

    }

    private function getRoleData()
    {
        return 
        [
            
            ["role_id"=>"a987a611-15d7-425f-81a1-8b0f72a0f6fa","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Admin","role_code"=>config("selfRegistration")["ROLE"]["SYSTEM_DEFAULT_ADMIN_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"b928bfc5-3a6f-4fd1-be40-51dce7d05455","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Author","is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"cbbd08c6-7f3a-4b27-a08a-d2b2e52402e0","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Platform review- Global role","role_code"=>config("selfRegistration")["ROLE"]["PROJECT_PUBLIC_REVIEW_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"035583f8-fea7-40af-aaa6-43190cffb62e","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Platform review- Taxonomy role","role_code"=>config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"d1b83505-70ee-46ea-991a-df82d4b390eb","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Admin","role_code"=>config("selfRegistration")["ROLE"]["SYSTEM_DEFAULT_ADMIN_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"3c1e874c-a4c1-4c69-9112-2b835b5d64e0","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Author","is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"6480a9f5-312d-48b3-9cef-5fc4a31cdaa6","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Platform review- Global role","role_code"=>config("selfRegistration")["ROLE"]["PROJECT_PUBLIC_REVIEW_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"e201707f-406e-459a-b5dd-3465537e4347","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Platform review- Taxonomy role","role_code"=>config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            //onlyforstaging
            ["role_id"=>"8d69a1a1-a82d-4550-8a32-4c67d08349d2","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Admin","role_code"=>config("selfRegistration")["ROLE"]["SYSTEM_DEFAULT_ADMIN_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"f990595c-d55e-48b8-bf1d-46d00145f812","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Platform review- Global role","role_code"=>config("selfRegistration")["ROLE"]["PROJECT_PUBLIC_REVIEW_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
            ["role_id"=>"227e43a0-4bf9-403f-8e2b-e638333bcadf","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Platform review- Taxonomy role","role_code"=>config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"],"is_active"=>1,"is_deleted"=>0,"usage_count"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],

        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
