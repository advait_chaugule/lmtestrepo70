<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    private $table = "subjects";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $subject = $this->getSerachData();
        
        foreach($subject as $subjectToInsertOrUpdate) {
            $searchSubject =   DB::table($this->table)->where('subject_id', $subjectToInsertOrUpdate['subject_id']);
            if($searchSubject->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('subject_id', $subjectToInsertOrUpdate['subject_id'])->update($subjectToInsertOrUpdate);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($subjectToInsertOrUpdate);
            }
        }
    }

    private function getSerachData()
    {
        return 
        [
            ["subject_id"=>"14906e33-629d-4ade-8f43-eef71ff6983b","title"=>"Mathematics",
            "created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"14906e33-629d-4ade-8f43-eef71ff6983b","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","updated_at"=>"2018-02-09 05:56:00"],
            ["subject_id"=>"fc6934b1-fcdf-4793-9350-2c7fe912dfe0","title"=>"Biology","created_at"=>"2018-02-09 05:56:00",
            "source_subject_id"=>"fc6934b1-fcdf-4793-9350-2c7fe912dfe0","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","updated_at"=>"2018-02-09 05:56:00"],
            ["subject_id"=>"20ada6d3-9941-4818-9a1b-be0e20945edc","title"=>"Physics","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"20ada6d3-9941-4818-9a1b-be0e20945edc",
            "organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","updated_at"=>"2018-02-09 05:56:00"],
            ["subject_id"=>"0e41b747-cf51-41c5-b3c2-c4b67672b435",
            "title"=>"Mathematics","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"0e41b747-cf51-41c5-b3c2-c4b67672b435","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","updated_at"=>"2018-02-09 05:56:00"],
            [
                "subject_id"=>"0264605a-114c-4c09-a53b-e1f8ae28cdfa","title"=>"Biology","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"0264605a-114c-4c09-a53b-e1f8ae28cdfa","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","updated_at"=>"2018-02-09 05:56:00"
            ],
            ["subject_id"=>"98457db5-e1bf-4ca5-bcdf-2cbe217a76e8","title"=>"Physics","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"98457db5-e1bf-4ca5-bcdf-2cbe217a76e8","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","updated_at"=>"2018-02-09 05:56:00"],
            //onlyforstaging
            ["subject_id"=>"63f2e7f9-639f-4c2e-b8c7-5733d91e5ed2","title"=>"Mathematics","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"63f2e7f9-639f-4c2e-b8c7-5733d91e5ed2","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1",
            "updated_at"=>"2018-02-09 05:56:00"],
            ["subject_id"=>"7c35bf30-09f3-4d8c-b1e4-6eff057f34db","title"=>"Biology","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"7c35bf30-09f3-4d8c-b1e4-6eff057f34db","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","updated_at"=>"2018-02-09 05:56:00"],
            ["subject_id"=>"d8f312aa-b6cc-430f-baf6-d4c5b423b275","title"=>"Physics","created_at"=>"2018-02-09 05:56:00","source_subject_id"=>"d8f312aa-b6cc-430f-baf6-d4c5b423b275","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","updated_at"=>"2018-02-09 05:56:00"]

        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
