<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use App\Data\Models\Project;

class DummyProjectSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dummyProject = config('database.dummy_data_count');
        factory(Project::class, $dummyProject)->create()->each(function ($project) {
            $project->save();
        });

    }
}
