<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class WorkflowSeeder extends Seeder
{
    private $table = "workflows";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $workflow = $this->getWorkflow();
        foreach($workflow as $workflowInsert) {
            $searchWorkflow =   DB::table($this->table)->where('workflow_id', $workflowInsert['workflow_id']);
            if($searchWorkflow->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('workflow_id', $workflowInsert['workflow_id'])->update($workflowInsert);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($workflowInsert);
            }
        }
    }

    private function getWorkflow()
    {
        return 
        [
            ["workflow_id"=>"a428e24e-bdbd-46a2-af60-f37b261235c2","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Default workflow","updated_by"=>"","created_at"=>"2018-06-19 05:56:00","updated_at"=>"2018-06-19 05:56:00","is_default"=>1],
            ["workflow_id"=>"ee2c5254-6ec3-48bb-9d9a-eac6ce72ee42","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Default workflow","updated_by"=>"","created_at"=>"2018-06-19 05:56:00","updated_at"=>"2018-06-19 05:56:00","is_default"=>1],
            //onlyforstaging
            ["workflow_id"=>"d5484194-ecad-44fe-b271-e385b89bc5ef","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Default workflow","updated_by"=>"","created_at"=>"2018-06-19 05:56:00","updated_at"=>"2018-06-19 05:56:00","is_default"=>1],
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
