<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    private $table = "users";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $user = $this->getUserData();
        // insert the fresh datasets
        foreach($user as $userToInsertOrUpdate) {
            $searchUser =   DB::table($this->table)->where('user_id', $userToInsertOrUpdate['user_id']);
            if($searchUser->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('user_id', $userToInsertOrUpdate['user_id'])->update($userToInsertOrUpdate);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($userToInsertOrUpdate);
            }
        }
    }

    private function getUserData()
    {
        return 
        [
            ["user_id"=>"aa06d122-7147-43c4-9a12-5c22c591bdd8","first_name"=>"John","last_name"=>"Admin","username"=>"testuser1@mail.com","email"=>"testuser1@mail.com","password"=>'$2y$10$8fUcVMGetC5oxle/5SRw8u/QdCCb4wi2.ys9kVytqARc9DNHMWcAm',"active_access_token"=>"cc89916fd6596e371c772f66912d88c772b3762894d9b05e906350978bb6bb357b81481ee28fade1","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","current_organization_id" => "a83db6c0-1a5e-428e-8384-c8d58d2a83ff","role_id"=>"a987a611-15d7-425f-81a1-8b0f72a0f6fa","is_active"=>1,"created_at"=>"2018-02-09 05:56:00","updated_at"=>"2018-02-09 05:56:00"],
            ["user_id"=>"a428e24e-bdbd-46a2-af60-f37b264465c2","first_name"=>"Ben","last_name"=>"Author","username"=>"testuser2@mail.com","email"=>"testuser2@mail.com","password"=>'$2y$10$8fUcVMGetC5oxle/5SRw8u/QdCCb4wi2.ys9kVytqARc9DNHMWcAm',"active_access_token"=>"cc89916fd6596e371c772f66912d88c772b3762894d9b05e906350978bb6bb357b81481ee28fade1","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","current_organization_id" => "a83db6c0-1a5e-428e-8384-c8d58d2a83ff","role_id"=>"a987a611-15d7-425f-81a1-8b0f72a0f6fa","is_active"=>1,"created_at"=>"2018-02-09 05:58:00","updated_at"=>"2018-02-09 05:58:00"],
            ["user_id"=>"4fa46dbd-e314-40cb-9439-929b94779fd6","first_name"=>"Admin","last_name"=>"1","username"=>"admin@org1.com","email"=>"admin@org1.com","password"=>'$2y$10$8fUcVMGetC5oxle/5SRw8u/QdCCb4wi2.ys9kVytqARc9DNHMWcAm',"active_access_token"=>"cc89916fd6596e371c772f66912d88c772b3762894d9b05e906350978bb6bb357b81481ee28fade1","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","current_organization_id" => "a83db6c0-1a5e-428e-8384-c8d58d2a83ff","role_id"=>"a987a611-15d7-425f-81a1-8b0f72a0f6fa","is_active"=>1,"created_at"=>"2018-02-09 05:58:00","updated_at"=>"2018-02-09 05:58:00"],
            ["user_id"=>"749f10ce-7162-432a-a82a-2e9ccf2e27bf","first_name"=>"Admin","last_name"=>"2","username"=>"admin@org2.com","email"=>"admin@org2.com","password"=>'$2y$10$8fUcVMGetC5oxle/5SRw8u/QdCCb4wi2.ys9kVytqARc9DNHMWcAm',"active_access_token"=>"d56ecdb7cb78981042ffb3138d3962a06ff560e3a78b95c5702fc6c7b2c3d14b26b0354c66004811","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","current_organization_id" => "f068a10c-96a7-48dd-9366-b71a199da48e","role_id"=>"d1b83505-70ee-46ea-991a-df82d4b390eb","is_active"=>1,"created_at"=>"2018-02-09 05:58:00","updated_at"=>"2018-02-09 05:58:00"],
            ["user_id"=>"36370260-0d00-4109-8e60-8cd6e795f0d5","first_name"=>"Mark","last_name"=>"M","username"=>"testuser5@mail.com","email"=>"testuser5@mail.com","password"=>'$2y$10$8fUcVMGetC5oxle/5SRw8u/QdCCb4wi2.ys9kVytqARc9DNHMWcAm',"active_access_token"=>"d56ecdb7cb78981042ffb3138d3962a06ff560e3a78b95c5702fc6c7b2c3d14b26b0354c66004812","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","current_organization_id" => "f068a10c-96a7-48dd-9366-b71a199da48e","role_id"=>"d1b83505-70ee-46ea-991a-df82d4b390eb","is_active"=>1,"created_at"=>"2018-02-09 05:58:00","updated_at"=>"2018-02-09 05:58:00"],
            //onlyforstaging
            ["user_id"=>"03cb69b7-43d2-4bca-a8f3-f44072f22991","first_name"=>"Admin","last_name"=>"ORG3","username"=>"admin@org3.com","email"=>"admin@org3.com","password"=>'$2y$10$8fUcVMGetC5oxle/5SRw8u/QdCCb4wi2.ys9kVytqARc9DNHMWcAm',"active_access_token"=>"8f3c9e9a-2dfe-4a6c-9c12-00c24da050c2","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","current_organization_id" => "9518d0fe-e174-4b93-aad8-6295d34287a1","role_id"=>"8d69a1a1-a82d-4550-8a32-4c67d08349d2","is_active"=>1,"created_at"=>"2018-02-09 05:58:00","updated_at"=>"2018-02-09 05:58:00"]
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
