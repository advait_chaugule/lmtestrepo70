<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class NodeTypeSeeder extends Seeder
{
    private $table = "node_types";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // fetch the default  dataset
            $nodetype = $this->getNodeType();
            foreach ($nodetype as $nodetypeToInsert) {
                $searchNodeType = DB::table($this->table)->where('node_type_id', $nodetypeToInsert['node_type_id']);
                if ($searchNodeType->count() > 0) {
                    // update Role
                    DB::table($this->table)->where('node_type_id', $nodetypeToInsert['node_type_id'])->update($nodetypeToInsert);
                } else {
                    // insert the fresh datasets
                    DB::table($this->table)->insert($nodetypeToInsert);
                }
            }
    }

    private function getNodeType()
    {
        return 
        [
            // NODE TYPE for ORG1
            ["node_type_id"=>"8e3112cb-d79e-4c28-b05f-981964a1cc90","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Default","type_code"=>"Default","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"8e3112cb-d79e-4c28-b05f-981964a1cc90","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>1],
            ["node_type_id"=>"613e8742-feb0-4fb8-a224-81bc31acf008","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Document","type_code"=>"Document","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"613e8742-feb0-4fb8-a224-81bc31acf008","is_document"=>"1","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"769dfb8e-4800-4283-942c-6d51910cfd3d","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Component","type_code"=>"Component","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"769dfb8e-4800-4283-942c-6d51910cfd3d","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"5a861448-ad7d-4488-b8f1-3d84f6be496d","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Standard","type_code"=>"Standard","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"5a861448-ad7d-4488-b8f1-3d84f6be496d","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"1a264e9c-da2d-49e1-9948-513ce77e0aeb","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Grade Level","type_code"=>"Grade Level","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"1a264e9c-da2d-49e1-9948-513ce77e0aeb","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"f49433ae-b273-46c6-8f97-da7c05a8b9ea","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Domain","type_code"=>"Domain","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"f49433ae-b273-46c6-8f97-da7c05a8b9ea","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"0955a66c-5982-4433-bd10-cf620af0ecc2","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Strand","type_code"=>"Strand","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"0955a66c-5982-4433-bd10-cf620af0ecc2","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"9264fea2-228a-4384-aa57-9612ac916b66","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Conceptual Category","type_code"=>"Conceptual Category","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"9264fea2-228a-4384-aa57-9612ac916b66","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"49104495-666e-4a6c-8f92-043ad107cea2","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","title"=>"Cluster","type_code"=>"Cluster","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"49104495-666e-4a6c-8f92-043ad107cea2","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            //NODETYPEforORG2
            ["node_type_id"=>"07893e8f-2a07-47ad-a213-419922a15b6c","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Default","type_code"=>"Default","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"07893e8f-2a07-47ad-a213-419922a15b6c","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>1],
            ["node_type_id"=>"ed9807d1-63ed-4703-ba67-e4103addc29e","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Document","type_code"=>"Document","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"ed9807d1-63ed-4703-ba67-e4103addc29e","is_document"=>"1","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"9d2601a7-7f61-4ebd-b2f5-b6c3f7bab973","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Component","type_code"=>"Component","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"9d2601a7-7f61-4ebd-b2f5-b6c3f7bab973","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"fa65eb18-d6d4-4480-a127-e4583814bc2c","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Standard","type_code"=>"Standard","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"fa65eb18-d6d4-4480-a127-e4583814bc2c","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"a76e00f1-1e5d-4b75-9a77-ddd942a1d97f","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Grade Level","type_code"=>"Grade Level","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"a76e00f1-1e5d-4b75-9a77-ddd942a1d97f","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"fe1e95c5-2189-4103-bf5c-44674603aecb","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Domain","type_code"=>"Domain","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"fe1e95c5-2189-4103-bf5c-44674603aecb","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"4ce62a1d-dd37-4fbf-b61e-95cce7493452","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Strand","type_code"=>"Strand","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"4ce62a1d-dd37-4fbf-b61e-95cce7493452","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"779269c6-1435-4bc9-9701-9f0708909c38","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Conceptual Category","type_code"=>"Conceptual Category","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"779269c6-1435-4bc9-9701-9f0708909c38","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"c963b77d-5411-4e52-951b-71b8bbb12c27","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","title"=>"Cluster","type_code"=>"Cluster","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"c963b77d-5411-4e52-951b-71b8bbb12c27","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            //NODETYPEforORG3(onlyforstaging)
            ["node_type_id"=>"d6321a1e-748f-4d6c-a255-3d4a255c1936","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Default","type_code"=>"Default","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"d6321a1e-748f-4d6c-a255-3d4a255c1936","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>1],
            ["node_type_id"=>"53611c78-b464-4337-94b3-804a510439ea","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Document","type_code"=>"Document","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"53611c78-b464-4337-94b3-804a510439ea","is_document"=>"1","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"4041b6c7-efea-42c1-b911-36a3a87ea6af","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Component","type_code"=>"Component","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"4041b6c7-efea-42c1-b911-36a3a87ea6af","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"fa457823-4415-4b88-b4c9-a3f4950d2be7","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Standard","type_code"=>"Standard","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"fa457823-4415-4b88-b4c9-a3f4950d2be7","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"bcdb6283-7ec5-4e20-b730-7dd3541c71df","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Grade Level","type_code"=>"Grade Level","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"bcdb6283-7ec5-4e20-b730-7dd3541c71df","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"564ca962-fa79-4fbc-ac2a-a33d7101f75f","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Domain","type_code"=>"Domain","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"564ca962-fa79-4fbc-ac2a-a33d7101f75f","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"f3167496-dea5-4cee-8997-194a5ec3192e","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Strand","type_code"=>"Strand","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"f3167496-dea5-4cee-8997-194a5ec3192e","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"12bd9d36-cff1-4d37-992b-487f0993db2e","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Conceptual Category","type_code"=>"Conceptual Category","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"12bd9d36-cff1-4d37-992b-487f0993db2e","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
            ["node_type_id"=>"9cc4ea62-ea32-4588-ab4a-c2150ace150a","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","title"=>"Cluster","type_code"=>"Cluster","hierarchy_code"=>"","description"=>"","source_node_type_id"=>"9cc4ea62-ea32-4588-ab4a-c2150ace150a","is_document"=>"0","is_custom"=>"0","is_deleted"=>"0","created_by"=>"","updated_by"=>"","updated_at"=>"2018-06-19 05:56:00","is_default"=>0],
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
