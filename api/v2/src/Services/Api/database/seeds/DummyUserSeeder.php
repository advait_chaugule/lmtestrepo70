<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use App\Data\Models\User;

class DummyUserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dummyUsers = config('database.dummy_data_count');
        factory(User::class, $dummyUsers)->create()->each(function ($user) {
            $user->save();
        });

    }
}
