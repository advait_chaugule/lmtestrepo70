<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 15-04-2019
 * Time: 15:15
 */

namespace App\Services\Api\database\seeds;


use App\Data\Models\Document;
use Illuminate\Database\Seeder;

class DummyDocumentSeeder extends Seeder
{
    public function run()
    {
        $dummyDocuments = config('database.dummy_data_count');
        factory(Document::class, $dummyDocuments)->create()->each(function ($document) {
            $document->save();
        });
    }
}