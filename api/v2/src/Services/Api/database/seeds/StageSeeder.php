<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class StageSeeder extends Seeder
{
    private $table = "stage";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // fetch the default  dataset
        $stage = $this->getStageData();
        foreach($stage as $stageToInsertOrUpdate) {
            $searchStage =   DB::table($this->table)->where('stage_id', $stageToInsertOrUpdate['stage_id']);
            if($searchStage->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('stage_id', $stageToInsertOrUpdate['stage_id'])->update($stageToInsertOrUpdate);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($stageToInsertOrUpdate);
            }
        }
    }

    private function getStageData()
    {
        return 
        [
            ["stage_id"=>"97abdc88-0b08-4d76-88ca-6fb09fd08544","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Authoring","order"=>"0","is_deleted"=>"0"],
            ["stage_id"=>"e57deb64-cf2b-4711-87f5-4c091b07db8d","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Internal Review","order"=>"1","is_deleted"=>"0"],
            ["stage_id"=>"ad083d46-4a63-4830-a7a4-fe21f23c9c4e","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Stakeholder review","order"=>"2","is_deleted"=>"0"],
            ["stage_id"=>"be9e0228-8bf3-48ff-aecc-902aed73a1fe","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Ready for stakeholder review","order"=>"3","is_deleted"=>"0"],
            ["stage_id"=>"7b527f77-2890-4b46-9d22-b74786c71026","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Public review & feedback","order"=>"4","is_deleted"=>"0"],
            ["stage_id"=>"645a1c77-89e4-40ad-a43d-b31b66667760","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Adjudication","order"=>"5","is_deleted"=>"0"],
            ["stage_id"=>"7d5428e3-7029-4055-9555-ded78b5ceb71","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Final stakeholder approval","order"=>"6","is_deleted"=>"0"],
            ["stage_id"=>"e22bc74f-226e-4766-8853-ee15f27621e7","organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Published","order"=>"7","is_deleted"=>"0"],
            ["stage_id"=>"f3ce331c-d84a-4cd3-b76e-a4a820e4d556","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Authoring","order"=>"0","is_deleted"=>"0"],
            ["stage_id"=>"e2735e99-7945-4cf9-ae41-b4eaf0270ebf","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Internal Review","order"=>"1","is_deleted"=>"0"],
            ["stage_id"=>"6caa13e8-c8fc-4bfb-aaa8-e5f84e9c7e21","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Stakeholder review","order"=>"2","is_deleted"=>"0"],
            ["stage_id"=>"2eda31e5-1f58-466d-a82c-2556c9c0ab1e","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Ready for stakeholder review","order"=>"3","is_deleted"=>"0"],
            ["stage_id"=>"9c66d712-11b8-47e9-9b59-61fb051c985e","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Public review & feedback","order"=>"4","is_deleted"=>"0"],
            ["stage_id"=>"d13d401e-0ceb-4980-9f1d-ca37a4579c30","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Adjudication","order"=>"5","is_deleted"=>"0"],
            ["stage_id"=>"55fbb74c-5233-45a7-aa9e-c1a995696c2f","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Final stakeholder approval","order"=>"6","is_deleted"=>"0"],
            ["stage_id"=>"cbbd81ed-9bc7-4041-9f85-806e66999c14","organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"Published","order"=>"7","is_deleted"=>"0"],
            //onlyforstaging
            ["stage_id"=>"b6b56543-335d-4a51-ad47-56e1ceb7363b","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Authoring","order"=>"0","is_deleted"=>"0"],
            ["stage_id"=>"c066da9f-9f95-4e77-9a20-eee757c26707","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Internal Review","order"=>"1","is_deleted"=>"0"],
            ["stage_id"=>"2fe0737c-d3bc-47c5-8cfa-dc874ec8eb59","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Stakeholder review","order"=>"2","is_deleted"=>"0"],
            ["stage_id"=>"3150a618-3d3b-40a7-89cb-0178d9b3063c","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Ready for stakeholder review","order"=>"3","is_deleted"=>"0"],
            ["stage_id"=>"12ac29e8-fd2a-4158-9db4-4865f36ee1fa","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Public review & feedback","order"=>"4","is_deleted"=>"0"],
            ["stage_id"=>"c1346b69-5d22-44ee-80fe-53954398a684","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Adjudication","order"=>"5","is_deleted"=>"0"],
            ["stage_id"=>"3a0eded9-0e36-40cc-9c1d-7c56d4b4c819","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Final stakeholder approval","order"=>"6","is_deleted"=>"0"],
            ["stage_id"=>"d3274391-41bc-4526-826b-d51d5e8885e0","organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Published","order"=>"7","is_deleted"=>"0"]
        ];
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
