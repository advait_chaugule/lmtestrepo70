<?php
namespace App\Services\Api\database\seeds;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class OrganizationSeeder extends Seeder
{
    private $table = "organizations";
    
    private $truncateFirstAndContinue = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $getEnvName = config('app')['env'];
        // fetch the default  dataset
        $organization = $this->getOrganization($getEnvName);
        foreach($organization as $organizationToInsertOrUpdate) {
            $searchOrganization =   DB::table($this->table)->where('organization_id', $organizationToInsertOrUpdate['organization_id']);
            if($searchOrganization->count() > 0 ) {
                // update Role
                DB::table($this->table)->where('organization_id', $organizationToInsertOrUpdate['organization_id'])->update($organizationToInsertOrUpdate);
            }
            else {
                // insert the fresh datasets
                DB::table($this->table)->insert($organizationToInsertOrUpdate);
            }
        }
        
    }

    private function getOrganization($getEnvName)
    {
         $organization = 
            [
                
                ["organization_id"=>"a83db6c0-1a5e-428e-8384-c8d58d2a83ff","name"=>"Organization 1","default"=>1,"org_code"=>"ORG01","is_deleted"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
                ["organization_id"=>"f068a10c-96a7-48dd-9366-b71a199da48e","name"=>"South Carolina department of Education","default"=>0,"org_code"=>"ORG02","is_deleted"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"],
                //onlyforstaging
                ["organization_id"=>"9518d0fe-e174-4b93-aad8-6295d34287a1","name"=>"Organization 3","default"=>0,"org_code"=>"ORG03","is_deleted"=>0,"created_at"=>"2018-04-06 05:56:00","updated_at"=>"2018-04-06 05:56:00"]
            ];
        
        if($getEnvName=='prod' || $getEnvName=='scde-prod')
        {
            foreach($organization as $key=>$value)
            {
                if($value['organization_id']=='a83db6c0-1a5e-428e-8384-c8d58d2a83ff')
                {
                    $organization[$key]['default']=0;
                }
                else if($value['organization_id']=='f068a10c-96a7-48dd-9366-b71a199da48e')
                {
                    $organization[$key]['default']=1;
                }
            }

            return $organization;
        }
        else
        {   
            return $organization;
        }    
    
    }

    private function truncateTable()
    {
        if($this->truncateFirstAndContinue){
            DB::table($this->table)->truncate();
        }
    }
}
