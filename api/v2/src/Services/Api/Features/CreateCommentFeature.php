<?php
namespace App\Services\Api\Features;

use App\Domains\User\Jobs\GetProjectDetailsByIdJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

//use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Thread\Jobs\ValidateThreadSourceByIdJob;
//use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Thread\Jobs\CheckThreadSourceIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;

use App\Domains\User\Jobs\GetUserByIdJob;

use App\Domains\Comment\Jobs\ValidateCommentInputJob;
//use App\Domains\Comment\Jobs\CreateItemThreadJob;
use App\Domains\Thread\Jobs\CreateThreadJob;
//use App\Domains\Comment\Jobs\CreateItemThreadCommentJob;
use App\Domains\Thread\Jobs\CreateThreadCommentJob;
//use App\Domains\Comment\Jobs\GetItemThreadJob;
use App\Domains\Thread\Jobs\GetThreadJob;
//use App\Domains\Comment\Jobs\UpdateItemThreadJob;
use App\Domains\Thread\Jobs\UpdateThreadJob;
//use App\Domains\Comment\Jobs\ListCommentbyItemThreadIdJob;
use App\Domains\Thread\Jobs\ListCommentbyThreadIdJob;

//Email notification jobs
use App\Domains\Comment\Jobs\SendCommentAssignmentNotificationJob;
use App\Domains\Comment\Jobs\SendCommentOwnerNotificationJob;

//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Project\Jobs\CheckPublicReviewCommentJob;
use App\Domains\Project\Jobs\CheckInsertedDataJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use Log;

class CreateCommentFeature extends Feature
{
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $threadIdentifier   =   '';

            $requestData                =   $request->all();
            $userId                     =   $requestData['auth_user']['user_id'];
            $organizationId             =   $requestData['auth_user']['organization_id'];
            $isDeleted                  =   $requestData['auth_user']['is_deleted'];
            $threadSourceIdentifier     =   $request->route('thread_source_id');
            $threadSourceType           =   $requestData['thread_source_type'];
            $inputData                  =   ['comment' => $requestData['comment'], 'organization_id' => $requestData['auth_user']['organization_id']];
            $requestUrl                 = url('/');
            $serverName                 = str_replace('server', '', $requestUrl);

            $validateCommentInputData = $this->run(new ValidateCommentInputJob($inputData));
            if($validateCommentInputData===true) {
                $validateThreadSource = $this->run(new ValidateThreadSourceByIdJob($threadSourceIdentifier, $threadSourceType));

                if($validateThreadSource === true){
                    $checkIsThreadSourceDeleted = $this->run(new CheckThreadSourceIsDeletedJob($threadSourceIdentifier, $threadSourceType));
                    if(!$checkIsThreadSourceDeleted === true){
                        $threadSourceDetails    =   '';
                        if($threadSourceType    ==  '1') {
                            //Fetch Thread Source Item Details
                            $threadSourceDetails = $this->run(new GetCFItemDetailsJob($threadSourceIdentifier, $organizationId));
                        }
                        else{
                            //Fetch Thread Source Document Details
                            $threadSourceDetails = $this->run(new GetDocumentByIdJob(['id' => $threadSourceIdentifier]));
                        }

                        
                        $inputData['thread_source_id']  =   $threadSourceIdentifier;
                        $inputData['status']            =   !empty($requestData['status']) ? $requestData['status'] : "1";

                        if(isset($requestData['thread_id']) && !empty($requestData['thread_id'])){
                            //Get Parent Comment
                            $parentOldComment  =   $this->run(new GetThreadJob($requestData['thread_id']));
                            //Set the Thread Identifier for reply comment
                            $threadIdentifier          =   $requestData['thread_id'];
                            $inputData['thread_id']    =   $threadIdentifier;
                            $owner_id = $parentOldComment['thread_comment']['created_by'];
                            // Check if Owner of the comment is similar to Replier of the comment
                            
                                $inputData['replied_by']    =   $requestData['auth_user']['user_id'];

                                unset($inputData['status']);

                                $threadCommentIdentifier            = $this->createUniversalUniqueIdentifier();
                                $inputData['thread_comment_id']     = $threadCommentIdentifier;
                                $inputData['parent_id']             = !empty($requestData['parent_id']) ? $requestData['parent_id'] : "";
                                $inputData['created_by']            = $requestData['auth_user']['user_id'];
                                $inputData['updated_at']            = $requestData['updated_at'];
                                $saveThreadComment = $this->run(new CreateThreadCommentJob($threadSourceIdentifier, $inputData));

                                //Update the main comment with updated at
                                $updateThreadData = ['thread_id' => $inputData['thread_id']];
                                $updateThreadData['updated_at'] =   $inputData['updated_at'];
                                $this->run(new UpdateThreadJob($updateThreadData));
                               

                                //Update the assignee if changed to a new one
                                if(!empty($requestData['assign_to']) && ($parentOldComment['assign_to'] !== $requestData['assign_to'])){
                                    $inputData['assign_to'] = $requestData['assign_to'];
                                    //Update the assignee to main comment
                                    $this->run(new UpdateThreadJob($inputData));
                                    $assign_to = $requestData['assign_to'];
                                }
                                else
                                {
                                    $assign_to = $parentOldComment['assign_to'];
                                }

                                 // In App Notifications // Jira point => When replies are added
                                    
                                 $eventType          = config("event_activity")["Notifications"]["COMMENT_REPLY_TO_NOTIFICATION"];
                                 $eventData = [
                                     'user_id'            => $userId,
                                     'owner_id'           => $owner_id,
                                     'assign_to'          => $assign_to,
                                     'organization_id'    => $organizationId,
                                     'is_deleted'         => $isDeleted,
                                     'created_by'         =>  $inputData['created_by']  ,
                                     'updated_by'         => $userId,
                                     'thread_comment_id'  => $inputData['thread_comment_id'],
                                     'event_type'         => $eventType,
                                     'requestUserDetails' => $request->input("auth_user"),
                                     "beforeEventRawData" => [],
                                     'afterEventRawData'  => ''
                                 ];
                                 event(new NotificationsEvent($eventData)); // notification ends here
                                                       

                            //Get Parent Comment
                            $parentComment  =   $this->run(new GetThreadJob($threadIdentifier));
                            //Fetch comment list
                            $commentList    =   $this->run(new ListCommentbyThreadIdJob($threadIdentifier, $inputData['organization_id']));

                            if($parentOldComment['assign_to'] !== $parentComment['assign_to']) {
                                /**
                                 * Email Notification
                                 */
                                // assigned to message
                                $eventType = config("event_activity")["Notifications"]["COMMENT_NOTIFICATION_ASSIGNEE_CHANGE"];
                                $eventData = [
                                    'user_id' => $userId,
                                    'owner_id' => $parentComment['thread_comment']['created_by'],
                                    'assign_to' => $parentComment['assign_to'],
                                    'old_assign_to' => $parentOldComment['assign_to'],
                                    'organization_id' => $organizationId,
                                    'is_deleted' => $isDeleted,
                                    'created_by' => $inputData['created_by'],
                                    'updated_by' => $userId,
                                    'thread_comment_id' => $inputData['thread_comment_id'],
                                    'event_type' => $eventType,
                                    'requestUserDetails' => $request->input("auth_user"),
                                    "beforeEventRawData" => [],
                                    'afterEventRawData' => ''
                                ];
                                event(new NotificationsEvent($eventData)); // notification ends here
                                //Check user email notification setting configuration ACMT-1869 Sub ticket 1954
                                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($organizationId));
                                $organizationName = $organizationData['organization_name'];
                                $organizationCode = $organizationData['organization_code'];

                                $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $parentComment['assign_to'], 'organization_id' => $organizationId]));
                                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                                if ($emailSettingStatus==1) {
                                    //Send mail to assignee
                                    $this->sendMailToAssignee($parentComment, $inputData, $threadSourceDetails, $commentList, $parentOldComment['assign_to'],$organizationName,$organizationCode,$serverName);
                                }
                            }
                            $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($organizationId));
                            $organizationName = $organizationData['organization_name'];
                            $organizationCode = $organizationData['organization_code'];
                            //Check user email notification setting configuration ACMT-1869 Sub ticket 1954
                            $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $owner_id, 'organization_id' => $organizationId]));
                            $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                            if ($emailSettingStatus==1) {
                                /**
                                 * Email Notification
                                 */
                                //Send mail to comment owner and assignee
                                $this->sendMailToOwner($parentComment, $inputData, $threadSourceDetails, $commentList,$organizationName,$organizationCode,$serverName);
                            }
                        }
                        else{
                            //Generate the Thread Identifier 
                            $threadIdentifier       = $this->createUniversalUniqueIdentifier();
                            $inputData['thread_id'] = $threadIdentifier;

                            if(isset($requestData['assign_to']) && !empty($requestData['assign_to'])){
                                $inputData['assign_to'] =   $requestData['assign_to'];
                            }
                            else{
                                $inputData['assign_to'] =   '';
                            }

                            $inputData['updated_at']    =   $requestData['updated_at'];

                            //Create Thread for the main comment
                            $inputData['thread_source_type']=$threadSourceType;
                            $inputData['created_at']    =   $requestData['updated_at'];
                            $this->run(new CreateThreadJob($threadSourceIdentifier, $inputData));
                                                        
                            unset($inputData['assign_to']);
                            unset($inputData['status']);

                            $threadCommentIdentifier           =   $this->createUniversalUniqueIdentifier();
                            $inputData['thread_comment_id']    =    $threadCommentIdentifier;
                            $inputData['parent_id']            =    !empty($requestData['parent_id']) ? $requestData['parent_id'] : "";
                            $inputData['created_by']           =    $requestData['auth_user']['user_id'];
                            $inputData['updated_at']           =    $requestData['updated_at'];
                            $saveThreadComment                 =    $this->run(new CreateThreadCommentJob($threadSourceIdentifier, $inputData));

                            $arrData = ['userId'=>$userId,'itemId'=>$threadSourceIdentifier];
                            $checkPublicReviewStatus = $this->run(new CheckPublicReviewCommentJob($arrData));
                            if(count($checkPublicReviewStatus)>0){

                                foreach ($checkPublicReviewStatus as $data) {
                                    $projectId    = $data->project_id;
                                    $comment      = $data->comment;
                                    if($comment=='') {
                                        $count =0;
                                    }else{
                                        $count =1;
                                    }
                                }
                                $identifier = ['project_id' => $projectId,'user_id' =>$userId,'organization_id'=>$organizationId,'is_deleted'=>$isDeleted];
                                //if($count==0) 
                                {
                                    $this->run(new CheckInsertedDataJob($identifier));
                                }
                            }
                            if(isset($requestData['assign_to']) && !empty($requestData['assign_to'])) {
                                //Get Parent Comment
                                $parentComment = $this->run(new GetThreadJob($threadIdentifier));
                                $threadCommentId = $parentComment['thread_comment']['thread_comment_id'];
                                $createdBy = $parentComment['thread_comment']['created_by'];
                                //Fetch comment list
                                $commentList = $this->run(new ListCommentbyThreadIdJob($threadIdentifier, $inputData['organization_id']));
                                $threadSourceId = ['thread_comment_id' => $commentList[0]['comments'][0]['thread_comment_id']];

                                //$getThreadSourceId = $this->run(new GetThreadSourceId($threadSourceId));
                                // $threadSourceId    = $getThreadSourceId[0]->thread_source_id;
                                //$getItemId        = $this->run(new GetItemId(['thread_source_id'=>$threadSourceId]));
                                //print_r($getItemId);
                                //$getDocumentDetails = $this->run(new GetTaxonomyNameById($documentId));

                                //In App Notification events
                                //$messageNotify      = config('event_activity')['notification_message']["2"];
                                //$notification       = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);

                                /**
                                 * Email Notification
                                 */
                                //Send mail to assignee
                                // assigned to message 
                                $eventType = config("event_activity")["Notifications"]["COMMENT_ASSIGN_TO_NOTIFICATION"];
                                $eventData = [
                                    'user_id' => $userId,
                                    'owner_id' => $parentComment['thread_comment']['created_by'],
                                    'assign_to' => $parentComment['assign_to'],
                                    'organization_id' => $organizationId,
                                    'is_deleted' => $isDeleted,
                                    'created_by' => $inputData['created_by'],
                                    'updated_by' => $userId,
                                    'thread_comment_id' => $inputData['thread_comment_id'],
                                    'event_type' => $eventType,
                                    'requestUserDetails' => $request->input("auth_user"),
                                    "beforeEventRawData" => [],
                                    'afterEventRawData' => ''
                                ];
                                event(new NotificationsEvent($eventData)); // notification ends here
                                // Check Email enable and disable Ticket ACMT-1869->1962
                                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($organizationId));
                                $organizationName = $organizationData['organization_name'];
                                $organizationCode = $organizationData['organization_code'];
                                $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $parentComment['assign_to'], 'organization_id' => $organizationId]));
                                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                                if ($emailSettingStatus==1) {
                                    $this->sendMailToAssignee($parentComment, $inputData, $threadSourceDetails, $commentList, $parentOldComment = '',$organizationName,$organizationCode,$serverName);
                                }
                            }
                        }
                        $resultData = $saveThreadComment; //$permissionSet;

                        //event for project and document update(updated_at) start
                        event(new UpdateTaxonomyTimeEvent($threadSourceIdentifier));
                        //event for project and document update(updated_at) end

                        $successType = 'created';
                        $message = 'Comment created successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status));
                    }
                    else{
                        $errorType = 'validation_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'The selected source is already deleted.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else{
                    $errorType = 'validation_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Please provide valid identifier.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please provide valid inputs.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function sendMailToAssignee($parentComment, $inputData, $threadSourceDetails, $commentList,$parentOldComment,$organizationName,$organizationCode,$serverName){
        // echo $parentOldComment;
        if(empty($parentOldComment))
        {
            //Get the Assignee User Detail
            $assignToIdentifier         =   $parentComment['assign_to'];
            $assigneeDetail             =   $this->run(new GetUserByIdJob(['user_id' => $assignToIdentifier]));


            //Get the Comment Owner Detail
            $ownerIdentifier            =   $parentComment['thread_comment']['created_by'];
            $ownerDetail                =   $this->run(new GetUserByIdJob(['user_id' => $ownerIdentifier]));

            $emailInput['receiver_email']   =   [$assigneeDetail->email];
            $emailInput['project_id']       =   $threadSourceDetails['projects_associated'][0]['project_id'];
            $emailInput['project_name']     =   $threadSourceDetails['projects_associated'][0]['project_name'];
            $emailInput['assigned_by']      =   !empty($ownerDetail->first_name)? $ownerDetail->first_name : "User";
            $emailInput['organization_name']=   $organizationName;
            $emailInput['organization_code']=   $organizationCode;
            $emailInput['domain_name']      =   $serverName;


            $emailInput['parent_comment']   =   $ownerDetail->first_name.' '.$ownerDetail->last_name."||".$parentComment['thread_comment']['comment']."||".$assigneeDetail->email;

            if(sizeOf($commentList)>0){
                $emailInput['commentList']      =   $commentList[0]['comments'];
            }
            else{
                $emailInput['commentList']      =   [];
            }
            $emailInput['identifying_data'] = $parentOldComment;
            $emailInput['reassigneUserName'] = '';
            $emailInput['assigneRechangeComment'] = '';
            $emailInput['subject']  =   'You have a comment';
            $emailInput['organization_name']=   $organizationName;
            $emailInput['organization_code']=   $organizationCode;
            $emailInput['domain_name']=   $serverName;
            //dd($emailInput);

            //Send Mail to Assignee
            $this->run(new SendCommentAssignmentNotificationJob($emailInput));
        }
        else
        {
            //Get the Assignee User Detail
            $assignToIdentifier         =   $parentComment['assign_to'];
            $assigneeDetail             =   $this->run(new GetUserByIdJob(['user_id' => $assignToIdentifier]));

            //Get Name of Reassigned User Start
            $reassigneeDetail             =   $this->run(new GetUserByIdJob(['user_id' => $parentOldComment]));
            //$emailInput['reassigneUserName'] =  !empty($reassigneeDetail->first_name)? $reassigneeDetail->first_name : "User";   
            //Get Name of Reassigned User End

            //Get the Comment Owner Detail
            $ownerIdentifier            =   $parentComment['thread_comment']['created_by'];
            $ownerDetail                =   $this->run(new GetUserByIdJob(['user_id' => $ownerIdentifier]));

            $emailInput['receiver_email']   =   [$assigneeDetail->email];
            $emailInput['project_id']       =   $threadSourceDetails['projects_associated'][0]['project_id'];
            $emailInput['project_name']     =   $threadSourceDetails['projects_associated'][0]['project_name'];
            $emailInput['assigned_by']      =   !empty($ownerDetail->first_name)? $ownerDetail->first_name : "User";
            $emailInput['organization_name']=   $organizationName;
            $emailInput['organization_code']=   $organizationCode;
            $emailInput['domain_name']=   $serverName;

            $emailInput['parent_comment']   =   $ownerDetail->first_name.' '.$ownerDetail->last_name."||".$parentComment['thread_comment']['comment']."||".$assigneeDetail->email;

            if(sizeOf($commentList)>0){
                $emailInput['commentList']      =   $commentList[0]['comments'];
            }
            else{
                $emailInput['commentList']      =   [];
            }

            $emailInput['identifying_data'] = $parentOldComment;
            $emailInput['reassigneUserName'] = !empty($assigneeDetail->first_name)? $assigneeDetail->first_name : "User";
            $emailInput['assigneRechangeComment'] = $parentComment['thread_comment']['comment'];

            $emailInput['subject']  =   'Assignee changed for the comment '.$parentComment['thread_comment']['comment'];
            //dd($emailInput);
            //Check user email notification setting configuration ACMT-1869 Sub ticket 1954
//            $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $ownerIdentifier, 'organization_id' => $organizationId]));
//            $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
//            if ($emailSettingStatus == 1) {
                //Send Mail to Assignee
                $this->run(new SendCommentAssignmentNotificationJob($emailInput));
            //}

            //Send Mail to Comment Creater
            $emailData['receiver_email']   =   [$ownerDetail->email];
            $emailData['project_id']       =   $threadSourceDetails['projects_associated'][0]['project_id'];
            $emailData['project_name']     =   $threadSourceDetails['projects_associated'][0]['project_name'];
            $emailData['assigned_by']      =   !empty($ownerDetail->first_name)? $ownerDetail->first_name : "User";
            $emailInput['organization_name']=   $organizationName;
            $emailInput['organization_code']=   $organizationCode;

            $emailData['parent_comment']   =   $ownerDetail->first_name.' '.$ownerDetail->last_name."||".$parentComment['thread_comment']['comment']."||".$assigneeDetail->email;

            if(sizeOf($commentList)>0){
                $emailData['commentList']      =   $commentList[0]['comments'];
            }
            else{
                $emailData['commentList']      =   [];
            }

            $emailData['identifying_data'] = $parentOldComment;
            $emailData['reassigneUserName'] = !empty($assigneeDetail->first_name)? $assigneeDetail->first_name : "User";
            $emailData['assigneRechangeComment'] = $parentComment['thread_comment']['comment'];

            $emailData['subject']  =   'Assignee changed for the comment '.$parentComment['thread_comment']['comment'];

            $this->run(new SendCommentAssignmentNotificationJob($emailData));

                  //Send Mail to Comment For Reassigner
                  $emailData['receiver_email']   =   [$reassigneeDetail->email];
                  $emailData['project_id']       =   $threadSourceDetails['projects_associated'][0]['project_id'];
                  $emailData['project_name']     =   $threadSourceDetails['projects_associated'][0]['project_name'];
                  $emailData['assigned_by']      =   !empty($ownerDetail->first_name)? $ownerDetail->first_name : "User";
                  $emailInput['organization_name']=   $organizationName;
                  $emailInput['organization_code']=   $organizationCode;
                  $emailData['parent_comment']   =   $ownerDetail->first_name.' '.$ownerDetail->last_name."||".$parentComment['thread_comment']['comment']."||".$assigneeDetail->email;
      
                  if(sizeOf($commentList)>0){
                      $emailData['commentList']      =   $commentList[0]['comments'];
                  }
                  else{
                      $emailData['commentList']      =   [];
                  }
      
                  $emailData['identifying_data'] = $parentOldComment;
                  $emailData['reassigneUserName'] = !empty($assigneeDetail->first_name)? $assigneeDetail->first_name : "User";
                  $emailData['assigneRechangeComment'] = $parentComment['thread_comment']['comment'];
      
                  $emailData['subject']  =   'Assignee changed for the comment '.$parentComment['thread_comment']['comment'];
      
                  $this->run(new SendCommentAssignmentNotificationJob($emailData));
        }
        
    }

    private function sendMailToOwner($parentComment, $inputData, $threadSourceDetails, $commentList,$organizationName,$organizationCode,$serverName){
        //Get the Replying User Detail
        $repliedByIdentifier        =   $inputData['replied_by'];
        $replierDetail              =   $this->run(new GetUserByIdJob(['user_id' => $repliedByIdentifier]));

        //Get the Comment Owner Detail
        $ownerIdentifier            =   $parentComment['thread_comment']['created_by'];
        $ownerDetail                =   $this->run(new GetUserByIdJob(['user_id' => $ownerIdentifier]));

        //Get the Assignee User Detail
        if($parentComment['assign_to'] !== ''){

            $assigneeIdentifier         =   $parentComment['assign_to'];
            $assigneeDetail             =   $this->run(new GetUserByIdJob(['user_id' => $assigneeIdentifier]));

            $emailInput['receiver_email']       =   [$ownerDetail['email'],$assigneeDetail['email']];
            $emailInput['parent_comment']       =   $ownerDetail['first_name'].' '.$ownerDetail['last_name']."||".$parentComment['thread_comment']['comment']."||".$assigneeDetail['email'];
        }
        else
        {
            $emailInput['receiver_email']   =   [$ownerDetail['email']];
            $emailInput['parent_comment']   =   $ownerDetail['first_name'].' '.$ownerDetail['last_name']."||".$parentComment['thread_comment']['comment'];
        }

        $emailInput['project_id']   =   $threadSourceDetails['projects_associated'][0]['project_id'];
        $emailInput['project_name'] =   $threadSourceDetails['projects_associated'][0]['project_name'];
        $emailInput['replied_by']   =   !empty($replierDetail->first_name)? $replierDetail->first_name : "User";
        $emailInput['organization_name']=   $organizationName;
        $emailInput['organization_code']=   $organizationCode;
        //$emailInput['domain_name']  =   $serverName;

        if(sizeOf($commentList)>0){
            $emailInput['commentList']      =   $commentList[0]['comments'];
        }
        else{
            $emailInput['commentList']      =   [];
        }
        $emailInput['domain_name']  =   $serverName;
        $emailInput['subject']  =   'You have a reply for the '.$parentComment['thread_comment']['comment'];

        //Send Mail to Owner and Assignee
        $this->run(new SendCommentOwnerNotificationJob($emailInput));
    }
}
