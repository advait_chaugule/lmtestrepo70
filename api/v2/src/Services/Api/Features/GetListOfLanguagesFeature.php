<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Language\Jobs\GetListOfLanguagesJob;
use Log;

class GetListOfLanguagesFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $organizationIdentifier = $request->input("auth_user")["organization_id"];
            $languageList = $this->run(new GetListOfLanguagesJob($organizationIdentifier));

            if(!empty($languageList)) {
                $successType = 'found';
                $message = "Languages Found.";
                $_status = "found";
            }
            else {
                $successType = 'not_found';
                $message = "No Languages found.";
                $_status = "not_found";
            }

            return $this->run(new RespondWithJsonJob($successType,  $languageList, $message, $_status));
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
