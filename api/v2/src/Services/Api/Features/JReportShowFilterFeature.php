<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Document\Jobs\ListDocumentJob;
use App\Domains\JReport\Jobs\JReportJob;
use App\Data\Repositories\ProjectEloquentRepository;
use App\Domains\Document\Jobs\ListDocumentByTypeJob;

class JReportShowFilterFeature extends Feature
{
    public function handle(Request $request)
    {
        $data['list']= config('jasper.jasper_file_type');     
        $data['document_list'] = array();        
        $data['organization_id'] = ''; 
        $data['projectlist'] =array();
        $data['taxonomy'] =array();
        if($request->input('token')){
            $organization_id = $this->run(new JReportJob($request->input('token')));

            $data['document_list'] = $this->run(new ListDocumentJob($organization_id));     
            //$request->merge(['auth_user' =>['organization_id'=>$organization_id]]);      
            $data['taxonomy'] = $this->run(new ListDocumentByTypeJob($organization_id,1));

            //print_r($data['taxonomy']); die;
            $data['organization_id'] = $organization_id;     
            
            //get list of published taxonomy
            $ProjectObj= new ProjectEloquentRepository();
            $data['projectlist'] = $ProjectObj->getPubicReviewTaxonomy($organization_id);
        }  
        $data['report_list'] = config('jasper.jasper_report_list');          
             // print_r($data); die;
        return view('api::report.jreport',$data);
    }    
}
