<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Organization\Jobs\ValidateOrgCodeJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as IlluminateResponse;

use App\Domains\CaseStandard\Jobs\ValidateCFAssociationGroupBySourceIdJob;
use App\Domains\CaseStandard\Jobs\GetCaseFrameworkAssociationGroupJob;

use App\Services\Api\Traits\UuidHelperTrait;
use Log;
use App\Services\Api\Traits\SavingIMSResponseTrait;

class GetCFAssociationGroupingFeature extends Feature
{
    use UuidHelperTrait, SavingIMSResponseTrait;

    public function handle(Request $request)
    {
        try{
            $orgCode = isset($request['org_code']) ? $request['org_code'] : '';
            $requestUrl = url('/');
            if ($orgCode) {
                $orgCodeArr['org_code'] = $orgCode;
                $validateOrgCode = $this->run(new ValidateOrgCodeJob($orgCodeArr));
                if ($validateOrgCode['org_code'][0] != false) {
                    return $this->respondWithCASEJsonError(404, false);
                }
            }
            $identifier = $request->route('identifier');
            $validateUUID = $this->isUuidValid($identifier);
            if($validateUUID===true){
                $validationStatus = $this->run(new ValidateCFAssociationGroupBySourceIdJob($identifier));
                if($validationStatus===true) {
                    $cfAssociationGrpKey  = ['identifier' =>$identifier,'prefix' => 'cfAssociationGrp'];
                    $caseFrameworkAssociationGrouping   = $this->run(new GetFromCacheJob($cfAssociationGrpKey));
                    if(isset($caseFrameworkAssociationGrouping['status']) && $caseFrameworkAssociationGrouping['status'] == "error") {
                        // get the CFAssociationGrouping in CASE standard structure
                        $caseFrameworkAssociationGrouping = $this->run(new GetCaseFrameworkAssociationGroupJob($identifier,$orgCode,$requestUrl));
                        $this->run(new DeleteFromCacheJob($cfAssociationGrpKey));
                        $this->run(new SetToCacheJob($caseFrameworkAssociationGrouping,$cfAssociationGrpKey));
                        $caseFrameworkAssociationGrouping = $this->run(new GetFromCacheJob($cfAssociationGrpKey));
                    }
                    if($caseFrameworkAssociationGrouping!==false) {
                        return $this->respondWithCASEJsonSuccess($caseFrameworkAssociationGrouping);
                    }
                    else {
                        return $this->respondWithCASEJsonError(404);
                    }
                }
                else {
                    return $this->respondWithCASEJsonError(404);
                }
            }
            else {
                return $this->respondWithCASEJsonError(404, false);
            }
        }
        catch(\Exception $ex){
            Log::error($ex);
            return $this->respondWithCASEJsonError(500);
        }
    }

    private function respondWithCASEJsonSuccess($data) {
        $responseReturn = response()->json($data, 200, [], JSON_UNESCAPED_SLASHES);
        $this->IMSResponse($responseReturn);
        return $responseReturn;
    }

    private function respondWithCASEJsonError($httpStatus, $validUUID = true) {
        $errorResponseBody = [
            "imsx_codeMajor" => "failure",
            "imsx_severity" => "error",
            "imsx_description" => ""
        ];
        switch ($httpStatus) {
            case '400':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "invalid_selection_field"
                                                                                ];
                break;

            case '401':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "unauthorisedrequest"
                                                                                ];
                break;
                
            case '403':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "forbidden"
                                                                                ];
                break;

            case '404':
                $imsx_codeMinorFieldValue = $validUUID ? "unknownobject" : "invaliduuid";
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => $imsx_codeMinorFieldValue
                                                                                ];
                break;

            case '429':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "server_busy"
                                                                                ];
                break;

            default:
                case '500':
                    $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                        "imsx_codeMinorFieldName" => "",
                                                                                        "imsx_codeMinorFieldValue" => "internal_server_error"
                                                                                    ];
                break;
        }

        if (!$validUUID) {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES)
                        ->setStatusCode(404, 'Invalid UUID');
        } else {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES);
        }
    }
}
