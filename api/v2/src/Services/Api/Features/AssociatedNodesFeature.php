<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;

use App\Domains\Project\Jobs\GetMappedNodesForProjectJob;

use App\Domains\Project\Jobs\GetNodesAssignedToProjectInTreeCompatibleFormJob;

use App\Domains\Project\Events\ViewProjectItemsEvent;
use Log;

class AssociatedNodesFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $treeData = [];
            //Get project identifier from route
            $projectId = $request->route('project_id');

            $inputData  =   $request->all();
            $documentIdentifier =   !empty($inputData['document_id']) ? $inputData['document_id'] : '';
            $input = [ 'project_id' => $projectId];
            //Validate the project identifier
            $validateProjectExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateProjectExistence===true) {
                $requestUserDetails = $request->input("auth_user");
                $taxonomyHierarchy = $this->run(new GetNodesAssignedToProjectInTreeCompatibleFormJob($projectId, $requestUserDetails, $documentIdentifier));
                
                if(count($taxonomyHierarchy['nodes']) > 0 || count($taxonomyHierarchy['relations']) > 0)
                {
                        // raise event to track activities for reporting
                        $beforeEventRawData = [];
                        $afterEventRawData = $input;
                        $eventData = [
                            "beforeEventRawData" => (object) $beforeEventRawData,
                            "afterEventRawData" => (object) $afterEventRawData,
                            "requestUserDetails" => $request->input("auth_user")
                        ];
                        event(new ViewProjectItemsEvent($eventData));
                        
                        /*** Create the success response ***/
                        $successType = 'found';
                        $message = 'Mapped Node listed successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
                }
                else
                {
                    $errorType = 'validation_error';
                    $message = "data not found";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
                
            }
            else {
                $errors = $validateProjectExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
