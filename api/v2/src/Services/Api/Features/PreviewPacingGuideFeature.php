<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\ValidateProjectForATenantJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\PreviewPacingGuideProjectJob;
use Log;

class PreviewPacingGuideFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData            =   $request->all();
            $pacingGuideIdentifier  =   $request->route('project_id');
            $requestingUserDetail   =   $requestData['auth_user'];
            $userId                 =   $requestingUserDetail['user_id'];
            $organizationId         =   $requestData['auth_user']['organization_id'];

            $validateProjectId = $this->run(new ValidateProjectIdJob(['project_id'  =>  $pacingGuideIdentifier]));

            if($validateProjectId===true)
            {
                $validateProjectForTheTenant   =   $this->run(new ValidateProjectForATenantJob($pacingGuideIdentifier, $organizationId));
                
                if($validateProjectForTheTenant == true)
                {
                    $validatePacingGuideIsDeleted  =   $this->run(new CheckIsProjectDeletedJob(['project_id'  =>  $pacingGuideIdentifier]));
                    
                    if(!$validatePacingGuideIsDeleted === true)
                    {
                        $previewData    =   $this->run(new PreviewPacingGuideProjectJob($pacingGuideIdentifier, $organizationId));

                        $response       = $previewData;
                        $successType    = 'found';
                        $message        = 'Preview of Pacing Guide';
                        $_status        = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else
                    {
                        $errorType = 'validation_error';
                        $message = "This pacing guide project is deleted.";
                        $_status = '';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else
                {
                    $errorType = 'validation_error';
                    $message = "This pacing guide project does not belong to this organization.";
                    $_status = '';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else
            {
                $errorType = 'internal_error';
                $validatemessage="Invalid Project ID";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
            }     

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
