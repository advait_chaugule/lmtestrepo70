<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeTemplate\Jobs\ValidateNodeTemplateByIdJob;
use App\Domains\NodeTemplate\Jobs\CheckNodeTemplateIsDeleteJob;
use App\Domains\NodeTemplate\Jobs\GetNodeTemplateDetailJob;
use Log;

class GetNodeTemplateFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $nodeTemplateId         = $request->route('node_template_id');
            $identifier             = ['node_template_id' => $nodeTemplateId];
            
            $validateTemplateStatus = $this->run(new ValidateNodeTemplateByIdJob($identifier));

            if($validateTemplateStatus === true) {
                $checkTemplateDeletedStatus = $this->run(new CheckNodeTemplateIsDeleteJob($nodeTemplateId));
                if(!$checkTemplateDeletedStatus === true){
                    $templateDetail = $this->run(new GetNodeTemplateDetailJob($nodeTemplateId));
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $templateDetail, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Template is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid template.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
