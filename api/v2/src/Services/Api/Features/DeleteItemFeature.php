<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemHasProjectJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\DeleteItemJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\Item\Jobs\DeleteItemAssociationJob;


use App\Domains\Project\Jobs\DeleteProjectItemJob;

use App\Domains\Item\Events\DeleteItemInsideProjectEvent;
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class DeleteItemFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $itemIdentifier             =   $request->route('item_id');
            $requestData                =   $request->all();
            $organizationId             =   $requestData['auth_user']['organization_id'];
            // create the input data to validate
            $input = [ 'item_id' => $itemIdentifier ];
            $validationStatus = $this->run(new ValidateItemByIdJob($input));
            if($validationStatus===true) {
                if(!empty($request->input('hasProject'))) {
                    $itemHasProject = $this->run(new CheckItemHasProjectJob($input));
                    if(count($itemHasProject) > 0)
                    {
                        $data = ['result' => 'assigned', 'project_id' => $itemHasProject[0]['project_id']];
                        $response = $data; 
                        $successType = 'found';
                        $message = 'Deleting this will also remove the node for its assigned project. Are you sure you want to delete?';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
              
                    }
                    else
                    {
                        $data = ['result' => 'not_assigned'];
                        $response = $data; 
                        $successType = 'found';
                        $message = 'Are you sure you want to delete the selected node?';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
              
                    }
                }
                
                //event for project and document update(updated_at) start
                event(new UpdateTaxonomyTimeEvent($itemIdentifier));
                //event for project and document update(updated_at) end

                //check if item already deleted
                $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemIdentifier));
                
                if(!$itemIsDeleted) {
                    // soft delete item on successful validation
                    $this->run(new DeleteItemJob($itemIdentifier));
                    // fetch the deleted item to remove project-item mapping
                    $deletedItem = $this->run(new GetCFItemDetailsJob($itemIdentifier, $organizationId));
                    // delete project item mapping
                    $this->deleteAssociationAndMapping($deletedItem);

                    $this->raiseEventToUploadTaxonomySearchDataToSqs($itemIdentifier);
                    
                    // raise event to track activities for reporting
                    if(!empty($request->input('project_id'))) {
                        $beforeEventRawData = $deletedItem;
                        $afterEventRawData = $deletedItem;
                        $afterEventRawData['project_id'] = $request->input('project_id');
                        $beforeEventRawData['is_deleted'] = 0;
                        $eventData = [
                            "beforeEventRawData" => (object) $beforeEventRawData,
                            "afterEventRawData" => (object) $afterEventRawData,
                            "requestUserDetails" => $request->input("auth_user")
                        ];
                        event(new DeleteItemInsideProjectEvent($eventData));
                    }

                    $successType = 'updated';
                    $message = 'Data deleted successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $deletedItem, $message, $_status));
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Item does not exist';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }

    /**
     * Remove item-to-item Association and project-item mapping
     */
    private function deleteAssociationAndMapping($item) {
        $itemAssociations = $item['item_associations'];

        foreach($itemAssociations as $association) {
            $associationIdentifier = $association['item_association_id'];
            $this->run(new DeleteItemAssociationJob($associationIdentifier));
        }

        // figureout the associated project internaly
        $associatedProject = $item['projects_associated'];
        
        if(!empty($associatedProject)){
            $this->run(new DeleteProjectItemJob($item['item_id'], $associatedProject[0]['project_id']));
        } 
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $itemId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_item"];
        $eventData = [
            "type_id" => $itemId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
