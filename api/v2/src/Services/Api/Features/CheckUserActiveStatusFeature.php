<?php
namespace App\Services\Api\Features;
use App\Domains\User\Jobs\UpdateUserActiveStatusJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\User\Jobs\ValidateUserActiveStatusJob;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\User\Jobs\GetUserDerailsJob;
use App\Domains\Organization\Jobs\GetOrganizationDetailByIdJob;
use App\Domains\User\Jobs\GetUserActiveStatusJob;
use App\Domains\Organization\Jobs\GetActiveOrganizationsByUserIdJob;
use Log;

class CheckUserActiveStatusFeature extends Feature
{
    use ArrayHelper, StringHelper,ErrorMessageHelper;
    public function handle(Request $request)
    {
        try{
            $requestData    =   $request->all();
            $tokenId        = $request->route('token_id');
            $organizationId = $request->route('organization_id');
            $status         = $requestData['is_check'];
            $checkInput     = isset($status)?$status:0;
            $userStatus     = ['token_id'=>$tokenId,'organization_id'=>$organizationId];
            $getUsers       = $this->run(new GetUserDerailsJob($userStatus));
            if($getUsers){
                $userId       = $getUsers->user_id;
                //$orgId        = $getUsers->organization_id;
                $identifiers  = ['user_id' => $userId, 'organization_id' => $organizationId];
                $getActiveStatus = $this->run(new GetUserActiveStatusJob($identifiers));
                $activeStatus = !empty($getActiveStatus) ? $getActiveStatus->is_active : 0;
                $orgDetails= $this->run(new GetOrganizationDetailByIdJob($organizationId));
                // Check if current organization if inactive
                if($orgDetails->is_active=='0' && !empty($userId)){
                    // Get all associated active organizations of this user through email
                    $currOrganization = $this->run(new GetActiveOrganizationsByUserIdJob($userId));
                    // If no organizations found, show error. Else continue resetting password
                    if(empty($currOrganization)){
                        $errorType = 'custom_found';
                        $message = ['This Tenant is deactivated and please contact admin'];
                        return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
                    }                
                }
                $orgName['organization_name']   = $orgDetails['name'];
                $orgName['organization_code']   = $orgDetails['org_code'];
                if ($checkInput == 1 && $activeStatus == 1) {
                    $response = ['user_status' => 'active','user_org'=>$orgName];
                    $successType = 'found';
                    $message = 'User already active.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else if ($checkInput == 1 && $activeStatus == 0) {
                    $response = ['user_status' => 'inactive','user_org'=>$orgName];
                    $successType = 'found';
                    $message = 'User is inactive.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else {
//                    $orgDetails= $this->run(new GetOrganizationDetailByIdJob($currentOrg));
//                    $orgName['organization_name']   = $orgDetails['name'];
//                    $orgName['organization_code']   = $orgDetails['org_code'];
                    $update = $this->run(new UpdateUserActiveStatusJob($identifiers));
                    if ($update) {
                        $response = $orgName;
                        $successType = 'found';
                        $message = 'User added in organization.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    } else {
                        $response = '';
                        $successType = 'found';
                        $message = 'User already added';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                }
            }else{
                $response = '';
                $successType = 'found';
                $message = 'User details not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($successType, $response, $message, $_status));
            }
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}