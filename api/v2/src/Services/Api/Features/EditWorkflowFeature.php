<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowInputJob;
use App\Domains\Workflow\Jobs\EditWorkflowJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use Log;

class EditWorkflowFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            
            $workflowIdentifier =   $request->route('workflow_id');
            $requestData        =   $request->all();

            $identifier     =   ['workflow_id'  =>  $workflowIdentifier];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            
            $validateWorkflow    =   $this->run(new ValidateWorkflowIdJob($identifier));
            
            if($validateWorkflow === true) {
                $workflowInput = ["name" => $requestData['name'],'organization_id' => $organizationIdentifier];
                $validateWorkflowInput = $this->run(new ValidateWorkflowInputJob($workflowInput));
                if($validateWorkflowInput === true) {
                    $updatedWorkflow    =   $this->run(new EditWorkflowJob($workflowIdentifier, $organizationIdentifier, $requestData));
                
                    $response = $updatedWorkflow;
                    $successType = 'found';
                    $message = 'Data updated.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
              
                }
                else
                {
                    $errorType = 'validation_error';
                    $message = $validateWorkflowInput;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
                } else {
                $errors = $validateWorkflow;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }          
           
        }
        catch(\Exception $ex){
            dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
