<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Workflow\Jobs\GetWorkflowJob; 
use App\Domains\Workflow\Jobs\UsageCountWorkflowJob;
use Log;

class GetAllWorkflowFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $organizationId = $requestData['auth_user']['organization_id'];
            $workflowIdArray = [];

            $workflowlist = $this->run(new GetWorkflowJob( $request->all()));
            
            foreach($workflowlist['workflows'] as $workflows){
                $workflowIdArray[] = $workflows['workflow_id'];
            }
            
            $usageCount = $this->run(new UsageCountWorkflowJob($workflowIdArray, $organizationId));
            $response = ['WorkflowList' => $workflowlist, 'WorkflowUsageCount' => $usageCount];

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
