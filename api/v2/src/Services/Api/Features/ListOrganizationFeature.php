<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Organization\Jobs\ListOrganizationJob;
use Illuminate\Support\Facades\Log;

class ListOrganizationFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestUserDetails = $request->input("auth_user");
            if($requestUserDetails['is_super_admin'] == 1)
            {
                $organization = $this->run(ListOrganizationJob::class);
                if(!empty($organization)){
                    $list = ['organization'=>$organization];
                    $successType = 'found';
                    $rmessage = 'Data found.';
                    return $this->run(new RespondWithJsonJob($successType, $list, $rmessage));
                }else{
                    $errorType = 'not_found';
                    $rmessage = 'Data not found.';
                }
            }else
            {
                $errorType = 'validation_error';
                $rmessage = 'You do not have priviledge to view tenant list.';                
            }
            // Modified header to send header with status 200 for all errors & remove unused variable $_status
            // To identify validation error, we have sent ['result' => 'exist']
            return $this->run(new RespondWithJsonJob($errorType, ['result' => 'exist'], $rmessage));
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
