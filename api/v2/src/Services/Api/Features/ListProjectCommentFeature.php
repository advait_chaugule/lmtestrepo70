<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Report\Jobs\GetItemsMappedWithProjectJob;
use App\Domains\Item\Jobs\GetAllCommentDetailsJob;
use App\Domains\Comment\Jobs\GetItemsCommentMadeForJob;
use App\Domains\Comment\Jobs\GetCommentListJob;
use App\Domains\Report\Jobs\ValidateDocumentExistJob;
use App\Domains\Item\Jobs\GetItemsMappedToDocumentJob;
use App\Domains\Taxonomy\Jobs\GetDocumentCommentListJob;
use Log;

class ListProjectCommentFeature extends Feature
{
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestVar = $request->all();
            $identifier=$request->route('identifier');
            $identifierType=$request->route('identifier_type');
            $identifierTypeValidator=array('1','2');
            
            if(in_array($identifierType,$identifierTypeValidator))
            {
                if($identifierType=="1")
                {
                    $input['document_id'] = $request->route('identifier');
                    $validateDocumentId = $this->run(new ValidateDocumentExistJob($input));
                    if($validateDocumentId===true)
                    {
                        $itemsDetails = $this->run(new GetItemsMappedToDocumentJob($input['document_id']));

                        $itemsCommentMadeFor = $this->run(new GetItemsCommentMadeForJob($itemsDetails));

                        $documentCommentList = $this->run(new GetDocumentCommentListJob($itemsDetails,$itemsCommentMadeFor));

                        if(!empty($documentCommentList))
                        {
                            $successType = 'found';
                            $message = $documentCommentList;
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message ,$_status));
                        }
                        
                    }
                    else
                    {
                        $errorType = 'internal_error';
                        $validatemessage="Invalid Document ID";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
                    }
                }
                else if($identifierType=="2")
                {
                    $input['project_id'] = $request->route('identifier');
                    $validateProjectId = $this->run(new ValidateProjectIdJob($input));
                    
                    if($validateProjectId===true)
                    {
                        $itemsMappedDetails = $this->run(new GetItemsMappedWithProjectJob($input['project_id']));

                        $itemsDetails = $this->run(new GetAllCommentDetailsJob($itemsMappedDetails));

                        $itemsCommentMadeFor = $this->run(new GetItemsCommentMadeForJob($itemsDetails));

                        $commentList = $this->run(new GetCommentListJob($itemsDetails,$itemsCommentMadeFor));

                        if(!empty($commentList))
                        {
                            $successType = 'found';
                            $message = $commentList;
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message ,$_status));
                        }
                        
                    }
                    else
                    {
                        $errorType = 'internal_error';
                        $validatemessage="Invalid Project ID";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
                    }
                }
            }
            else
            {
                $errorType = 'validation_error';
                $message = 'enter a valid identifier type';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
