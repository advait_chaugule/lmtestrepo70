<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

class LogDataDisplayFeature extends Feature
{
    public function handle(Request $request)
    {
        return view('api::awslog.awslogdisplay');
    }
}
