<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\RejectProjectAccessSJob;
use App\Domains\Project\Jobs\AcceptProjectAccessSJob;
use App\Domains\Project\Jobs\ProjectAccessRequestDetailsJob;
use App\Domains\Project\Jobs\SendRejectProjectAccessEmailNotificationJob;
use App\Domains\Project\Jobs\SendAcceptProjectAccessEmailNotificationJob;
use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\CheckProjectUserMappingJob;
use App\Domains\Project\Jobs\AssignProjectUserJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Caching\Events\CachingEvent;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use Log;

class UpdateProjectAccessRequestFeature extends Feature
{
    public function handle(Request $request)
    {

        $projectAccessRequestId = $request->route('project_access_request_id');
        $projectId = $request->input('project_id');
        $workflowStageRoleIds = implode(',', $request->input('workflow_stage_role_ids'));
        $accessStatus = $request->input('status');
        $accessReason = $request->input('comment');
        $updatedAt = date("Y-d-m H:i:s");
        $loginUserId = $request['auth_user']['user_id'];
        $updatedBy = $request['auth_user']['user_id'];
        $statusChangedAt = date("Y-d-m H:i:s");
        $organizationId = $request['auth_user']['organization_id'];
        $requestUrl    = url('/');
        $serverName    = str_replace('server', '', $requestUrl);
        $input = ['project_access_request_id' => $projectAccessRequestId,
           'organization_id' => $organizationId];
        $requestData = ['project_access_request_id' => $projectAccessRequestId,
                        'project_id' => $projectId, 
                        'status' => $accessStatus,
                        'comment' => $accessReason,
                        'workflow_stage_role_ids' => $workflowStageRoleIds,
                        'updated_at' => $updatedAt,
                        'updated_by' => $updatedBy,
                        'status_changed_at' => $updatedAt];
        try{
                if($accessStatus == 3)
                {
                    $updateProjectAccessStatus = $this->run(new RejectProjectAccessSJob($requestData));
                     $ProjectAccessRequestList = $this->run(new ProjectAccessRequestDetailsJob($input));
                     $requestUserId = $ProjectAccessRequestList[0]['user_id'];
                     $projectName = $ProjectAccessRequestList[0]['project_name'];
                     $userName = $ProjectAccessRequestList[0]['user_name'];
                     $userEmail = $ProjectAccessRequestList[0]['user_email'];
                     $comment = $ProjectAccessRequestList[0]['user_email'];
                     $updatedBy = $ProjectAccessRequestList[0]['updated_by_name'];
                    $organizationData = $this->run(new GetOrgNameByOrgIdJob($organizationId));
                    $organizationName = $organizationData['organization_name'];
                    $organizationCode = $organizationData['organization_code'];
                     $input['projectName'] = $projectName;
                     $input['userName'] = $userName;
                     $input['email'] = $userEmail;
                     $input['comment'] = $comment;
                     $input['updatedBy'] = $updatedBy;
                     $input['organization_name'] = $organizationName;
                     $input['organization_code'] = $organizationCode;
                     $input['domain_name'] = $serverName;
                     $data = ['project_access_request_id' => $projectAccessRequestId];
                     // Check Email enable or disable ACMT-1869-1962

                    $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$requestUserId,'organization_id'=>$organizationId]));
                    $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                    if ($emailSettingStatus==1) {
                        $this->run(new SendRejectProjectAccessEmailNotificationJob($input));
                        unset($input['organization_name']);
                        unset($input['organization_code']);
                    }

                    $successType = 'rejected';
                    $message = 'Project access request has been rejected successfully.';
                    $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
                else  if($accessStatus == 2)
                {
                    $updateProjectAccessStatus = $this->run(new AcceptProjectAccessSJob($requestData));
                    $ProjectAccessRequestList = $this->run(new ProjectAccessRequestDetailsJob($input));
                    $userId      = $ProjectAccessRequestList[0]['user_id'];
                    $projectName = $ProjectAccessRequestList[0]['project_name'];
                    $workflowStageRoleIds = explode(",",$workflowStageRoleIds);
                    $identifier = ['project_id' => $projectId, 'organization_id' => $organizationId];
                   
                            $validateStatus = $this->run(new ValidateProjectIdJob($identifier));
                            if($validateStatus === true){
                                $checkIsProjectDeleted = $this->run(new CheckIsProjectDeletedJob($identifier));
                                if(!$checkIsProjectDeleted === true) {
                                    foreach($workflowStageRoleIds as $workflowStageRoleId)
                                    {
                                            $requestWorkflowStageRoleData = ['user_id' => $userId, 'workflow_stage_role_id' => $workflowStageRoleId ];
                
                                    $checkProjectUserRoleMappingIsExists = $this->run(new CheckProjectUserMappingJob($identifier, $requestWorkflowStageRoleData));
                                    //$response = $checkProjectUserRoleMappingIsExists;

                                    //dd($checkProjectUserRoleMappingIsExists);
                                    
                                    foreach($checkProjectUserRoleMappingIsExists as $data){
                                            if($data['user_exists'] === false){
                                                $mapUserToStageRole = $this->run(new AssignProjectUserJob($identifier, $data,  $loginUserId ));
                                            }
                                        }
                                    }
                                }
                                else{
                                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                                    $errorType = 'not_found_error';
                                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                                    $message = 'Project is already deleted';
                                    $_status = 'custom_status_here';
                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                }
                            }
                            else{
                                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                                $errorType = 'not_found_error';
                                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                                $message = 'Project ID is invalid';
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                            }
                  //In App Notification when user accept project access request
                    $eventType            = config("event_activity")["Notifications"]["PROJECT_ACCESS_RECEIVED"];
                    $notificationCategory = config("event_activity")["Notification_category"]["PROJECT_ACCESS_RECEIVED"];
                    $arrayData = ['project_id' =>$projectId,
                        'user_id'        => $userId,
                        'project_name'   =>$projectName,
                        'organization_id'=>$organizationId,
                        'event_type'     =>$eventType,
                        'access_status'  =>$accessStatus,
                        'notification_category'=>$notificationCategory,
                        'requestUserDetails' => $request->input("auth_user"),
                        'beforeEventRawData'=>'',
                        'afterEventRawData' => ''
                    ];
                    event(new NotificationsEvent($arrayData));  //In App Notification ends
                    $organizationData = $this->run(new GetOrgNameByOrgIdJob($organizationId));
                    $organizationName = $organizationData['organization_name'];
                    $organizationCode = $organizationData['organization_code'];

                    $projectName = $ProjectAccessRequestList[0]['project_name'];
                    $userName = $ProjectAccessRequestList[0]['user_name'];
                    $userEmail = $ProjectAccessRequestList[0]['user_email'];
                    $comment = $ProjectAccessRequestList[0]['user_email'];
                    $updatedBy = $ProjectAccessRequestList[0]['updated_by_name'];
                    $workflowStageRoles = $ProjectAccessRequestList[0]['workflow_stage_roles'];
                    $input['projectName'] = $projectName;
                    $input['userName'] = $userName;
                    $input['email'] = $userEmail;
                    $input['comment'] = $comment;
                    $input['updatedBy'] = $updatedBy;
                    $input['organization_name'] = $organizationName;
                    $input['organization_code'] = $organizationCode;
                    $input['workflowStageRoles'] = $workflowStageRoles;
                    $input['domain_name'] = $serverName;
                    $data = ['project_access_request_id' => $projectAccessRequestId];
                    // Check Email enable or disable ACMT-1869-1962
                    $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$userId,'organization_id'=>$organizationId]));
                    $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                    if ($emailSettingStatus==1) {
                        $this->run(new SendAcceptProjectAccessEmailNotificationJob($input));
                        unset($input['organization_name']);
                        unset($input['organization_code']);
                    }
                    
                   $successType = 'accepted';
                   $message = 'Project access request has been accepted successfully.';
                   $_status = 'custom_status_here';
                  return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
           
                }
                else
                {
                    $data = ['result' => 'invalid'];
                    $successType = 'invalid';
                    $message = 'This action is invalid.';
                    $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }

                //dd($projectAccessRequestId);
           }
           catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
