<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Document\Jobs\GetDocumentSearchJob;
use Log;

class SearchDocumentFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $searchParam = $request->input('search_key');
            $orderByParam = $request->route('order_by');
            $input = [ 'search_key' => $searchParam, "sorting" => "desc", "orderBy" => $orderByParam ];

            $documentList = $this->run(new GetDocumentSearchJob($input));
            $response = $documentList;
            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            
            
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
