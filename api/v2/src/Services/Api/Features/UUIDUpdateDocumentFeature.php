<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;
use App\Services\Api\Traits\UuidHelperTrait;
use Storage;

class UUIDUpdateDocumentFeature extends Feature
{
    use UuidHelperTrait;
    public function handle(Request $request)
    {
        ini_set('memory_limit','80980M');
        ini_set('max_execution_time', 0);

        try {
            $output_response = [];
            $org_code = trim($request->org_code);
            Storage::append('uuid.log','$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
            Storage::append('uuid.log','Step1 : Get organization_id query started');
            $org = DB::table('organizations')->select('organization_id')->where('org_code', $org_code)->where('is_deleted', 0)->first();
            Storage::append('uuid.log','Step2 : organization_id query executed successfully - '.$org_code);
            if(!$org) {
                $successType = 'custom_not_found';
                $message = 'Organization not found';
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
            }

            $org_id = $org->organization_id;
            Storage::append('uuid.log','Step3 : Get documents as per organization_id - '.$org_id);            
            $transaction = DB::transaction(function() use ($org_id) {
                $documentListAll = DB::table('documents')
                    ->select('document_id')
                    ->where(function ($query) {
                            $query->where(DB::raw('SUBSTR(document_id, 15, 1)'),'!=','4')
                                ->orWhereNotIn(DB::raw('SUBSTR(document_id, 20, 1)'),['8','9','a','b']);
                    })
                    ->where('organization_id',$org_id)
                    ->where('is_deleted','0')
                    ->get()
                    ->toArray();
                Storage::append('uuid.log','Step4 : Get documents query executed successfully'); 
                $documentListArray = array_chunk($documentListAll,50);
                Storage::append('uuid.log','Step5 : Divided document into chunks');
                foreach($documentListArray as $allkey => $documentList){
                    $statement = "";            
                    foreach($documentList as $key => $documentIdVal){  
                        $documentId = $documentIdVal->document_id;
                        $updatedDocumentId = $this->createUniversalUniqueIdentifier();
                        Storage::append('uuid.log','Step6 : Old Id - '.$documentId.' New Id - '.$updatedDocumentId);
    
                        $statement.= "UPDATE `acmt_assets` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_documents` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_documents` SET `source_document_id` = '$updatedDocumentId' WHERE `source_document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_documents` SET `uri` = ".(DB::raw("REPLACE(uri,  '$documentId', '$updatedDocumentId')"))." WHERE `uri` like '%$documentId'; ";
                        $statement.= "UPDATE `acmt_document_metadata` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_document_public_reviews` SET `original_document_id` = '$updatedDocumentId' WHERE `original_document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_document_public_reviews` SET `copied_document_id` = '$updatedDocumentId' WHERE `copied_document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_document_subject` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_group_document` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_import_job` SET `source_document_id` = '$updatedDocumentId' WHERE `source_document_id` = '$documentId';";
                        $statement.= "UPDATE `acmt_items` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_items` SET `parent_id` = '$updatedDocumentId' WHERE `parent_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `destination_document_id` = '$updatedDocumentId' WHERE `destination_document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `source_document_id` = '$updatedDocumentId' WHERE `source_document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `target_document_id` = '$updatedDocumentId' WHERE `target_document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `origin_node_id` = '$updatedDocumentId' WHERE `origin_node_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `destination_node_id` = '$updatedDocumentId' WHERE `destination_node_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `source_item_id` = '$updatedDocumentId' WHERE `source_item_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_item_associations` SET `target_item_id` = '$updatedDocumentId' WHERE `target_item_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_projects` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_project_pref_documents` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_public_review_histories` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_taxonomy_pubstate_history` SET `document_id` = '$updatedDocumentId' WHERE `document_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_user_settings` SET `id` = '$updatedDocumentId' WHERE `id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_notifications` SET `target_id` = '$updatedDocumentId' WHERE `target_id` = '$documentId'; ";
                        $statement.= "UPDATE `acmt_import_job` SET `results` = ".(DB::raw("REPLACE(results,  '$documentId', '$updatedDocumentId')"))." WHERE `results` like '%$documentId'; ";
                        $statement.= "UPDATE `acmt_notifications` SET `target_context` = ".(DB::raw("REPLACE(target_context,  '$documentId', '$updatedDocumentId')"))." WHERE `target_context` like '%$documentId'; ";
                        $statement.= "UPDATE `acmt_search_histories` SET `search_filter` = ".(DB::raw("REPLACE(search_filter,  '$documentId', '$updatedDocumentId')"))." WHERE `search_filter` like '%$documentId'; ";
                    }
                    DB::unprepared($statement);
                    unset($statement);
                    Storage::append('uuid.log','Step7 : Transaction completed');
                }
                $successType = 'custom_found';
                $message = 'Execution completed successfully';
                $_status = 'custom_status_here';
                Storage::append('uuid.log','Step8 : Migration completed');
                Storage::append('uuid.log','###########################');

                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));
           });
           return $transaction;
            
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
            $_status = 'custom_status_here';
            Log::error($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
