<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentV2Job;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV2Job;
use Log;

class GetTaxonomyHierarchyV2Feature extends Feature
{
    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $documentIdentifier = $request->route('document_id');
            $requestingUserDetails = $request->input("auth_user");
            if($request->input('is_customview')){
                $isCustomview = $request->input('is_customview');
            }else{
                $isCustomview = 0;
            }
            $requestingUserDetails['is_customview'] = $isCustomview;
           
            $validationStatus = $this->run(new ValidateDocumentV2Job($documentIdentifier, $requestingUserDetails));
            if($validationStatus===true) {
            $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeV2Job($documentIdentifier, $requestingUserDetails));
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validationStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
