<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Project\Jobs\GetProjectByIdJob;
use App\Domains\Project\Jobs\DeleteAssociatedNodesJob;

use App\Domains\Project\Jobs\GetFlattenedTaxonomyForProjectCompleteTreeJob;
use Log;

class DeleteAssociatedNodesFeature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        try {
            $requestData        =   $request->all();
            $requestUser        =   $request->input("auth_user");
            $projectIdentifier  =   $request->route('project_id');

            $projectDetails =   $this->run(new GetProjectByIdJob(['project_id' => $projectIdentifier]));
            if($projectDetails['project_type']==1) {

            } else {
                $documentIdentifier =   $requestData['document_id'];

                $deleteAssociatedNodes  =   $this->run(new DeleteAssociatedNodesJob($projectIdentifier, $documentIdentifier, $requestData));

                $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyForProjectCompleteTreeJob($documentIdentifier, $projectIdentifier, $requestUser, $request));

                $successType = 'found';
                $message = 'Pacing Guide taxonomy mapping deleted successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
            }
        } catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
