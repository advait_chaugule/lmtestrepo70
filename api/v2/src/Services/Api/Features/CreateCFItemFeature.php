<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\CaseFrameworkTrait;


use App\Domains\CFEntityType\Jobs\UpdateInputWithCFEntityTypeJob;

use App\Domains\Item\Jobs\ValidateCFItemInputJob;
use App\Domains\Item\Jobs\CreateCFItemJob;

use App\Domains\Concept\Jobs\ValidateConceptInputJob;
use App\Domains\Concept\Jobs\CreateConceptJob;

use App\Domains\License\Jobs\ValidateLicenseInputJob;
use App\Domains\License\Jobs\CreateLicenseJob;
use App\Data\Models\Organization;
use App\Domains\CaseAssociation\Jobs\CreateItemAssociationsJob;
use App\Domains\CaseAssociation\Jobs\OriginDestinationNodeAssociationTypeExistStatusJob;
use App\Domains\Item\Events\CreateItemInsideProjectEvent;
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Item\Jobs\AddCustomMetadataForPacingGuideItemJob;
use Log;
use App\Domains\Item\Jobs\GetItemJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use Illuminate\Support\Facades\DB;

class CreateCFItemFeature extends Feature
{
    use ArrayHelper, StringHelper, UuidHelperTrait, DateHelpersTrait, ErrorMessageHelper, CaseFrameworkTrait;

    public function handle(Request $request)
    {
        try{
            $input = $request->input();
            $requestUrl = url('/');
            $documentId = $request->input("document_id");
            $organizationIdentifier = $request->all()['auth_user']['organization_id'];
            $orgCode = Organization::select('org_code')->where('organization_id', $organizationIdentifier)->first()->org_code;
            $userID = $request->all()['auth_user']['organization_id'];
            $organizationId = $input['auth_user']['organization_id'];
            $validationStatus = $this->run(new ValidateCFItemInputJob($input));
            $organizationCode =   Organization::select('org_code')->where('organization_id', $organizationId)->first()->org_code;
            if($validationStatus===true) {
                // set entity type
                $input = $this->run(new UpdateInputWithCFEntityTypeJob($input));
                // create the uuid
                $identifier = $this->createUniversalUniqueIdentifier();
                // add uuid to the input array
                $input['item_id'] = $identifier;
                $input['source_item_id'] = $identifier;
                $input['uri'] = $this->getCaseApiUri("CFItemTypes", $identifier, true, $organizationCode,$requestUrl);
                // validate concept master data
                $validateConceptInput = $this->run(new ValidateConceptInputJob($input));

                if($validateConceptInput===true){
                    // create the uuid
                    $conceptIdentifier = $this->createUniversalUniqueIdentifier();
                    $conceptKeyword = !empty($input['concept_keywords']) ? $input['concept_keywords'] : "";

                    $conceptURI =   $this->getCaseApiUri("CFConcepts", $conceptIdentifier, true, $orgCode,$requestUrl);

                    // create concept master record
                    $conceptData = ['keyword' => $conceptKeyword, 'concept_id' => $conceptIdentifier, 'source_concept_id' => $conceptIdentifier, 'organization_id'  =>  $input['auth_user']['organization_id'] , 'uri' => $this->getCaseApiUri("CFConcepts", $conceptIdentifier, true, $organizationCode,$requestUrl)  ];
                    // create concept
                    $concept = $this->run(new CreateConceptJob($conceptData));
                    // remove concept related fields from request input
                    $input = $this->removeSpecifiedElementsFromArray($input, ['concept_keywords']);
                    // add concept_id to input
                    $input['concept_id'] = $concept->id;
                }
                else{
                    // remove concept related fields from request input
                    $input = $this->removeSpecifiedElementsFromArray($input, ['concept_keywords']);
                }

                // validate license master data
                $validateLicenseInput = $this->run(new ValidateLicenseInputJob($input));
                if($validateLicenseInput===true){
                    // create the uuid
                    $licenseIdentifier = $this->createUniversalUniqueIdentifier();
                    $licenseURI        = $this->getCaseApiUri("CFLicenses", $licenseIdentifier, true, $orgCode,$requestUrl);
                    // create the license data
                    $licenseData = ['license_id' => $licenseIdentifier, 'source_license_id' => $licenseIdentifier, 'organization_id'    =>  $input['auth_user']['organization_id'], 'uri' => $licenseURI];
                    // create license master record
                    $license = $this->run(new CreateLicenseJob($licenseData));
                    // remove license related fields from request input
                    $input = $this->removeSpecifiedElementsFromArray($input);
                    // add concept_id to input
                    $input['license_id'] = $license->license_id;
                }
                else{
                    // remove license related fields from request input
                    $input = $this->removeSpecifiedElementsFromArray($input);
                }
                
                $updatedBy = $request->input("auth_user")["user_id"];
                $input['updated_by'] = $updatedBy;
                // create cfItem
                $cfItem = $this->run(new CreateCFItemJob($input,$requestUrl));

                if(isset($request['project_type']) && $request['project_type'] == '2') {
                    $this->run(new AddCustomMetadataForPacingGuideItemJob($cfItem->item_id, $input['auth_user']['organization_id'], '3', 'Container'));
                }

                $originNodeIdentifier = $identifier;
                $associationTypeIdentifier = 1; // statically set to isChildOf association
                $destinationNodeIdentifier = !empty($input['parent_id']) ? $input['parent_id'] : $cfItem->document_id;
                $createDataForAssociationExistsChecking = [
                    'source_item_id' => $originNodeIdentifier,
                    'association_type' => $associationTypeIdentifier,
                    'target_item_id' => $destinationNodeIdentifier,
                    // 'is_reverse_association' => 0
                ];
                $checkAssociationExists = $this->run(new OriginDestinationNodeAssociationTypeExistStatusJob($createDataForAssociationExistsChecking));
                if($checkAssociationExists===false){
                    $cfDocumentIdentifier = $cfItem->document_id;
                    // create unique identifier
                    $associationIdentifier = $this->createUniversalUniqueIdentifier();

                    $destination = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));
                    if(empty($destination['document_id']))
                    {
                        $destination = $this->run(new GetDocumentByIdJob(['id' => $destinationNodeIdentifier]));
                    }
                    # add max sequence no to newly created item node
                    $max_seq_query = DB::table('item_associations')->where('association_type', 1)->groupBy('target_item_id')->where('target_item_id','=', $destinationNodeIdentifier)->get(['target_item_id', DB::raw('MAX(sequence_number) as max_seq')])->first();

                    if($max_seq_query) {
                        $sequence_number = $max_seq_query->max_seq + 1;
                    } else {
                        $sequence_number = !empty($request->list_enumeration) ? $request->list_enumeration : 1;
                    }
                    
                    # add max sequence no to newly created item node
                    $max_seq_query = DB::table('item_associations')->where('association_type', 1)->groupBy('target_item_id')->where('target_item_id','=', $destinationNodeIdentifier)->get(['target_item_id', DB::raw('MAX(sequence_number) as max_seq')])->first();

                    if($max_seq_query) {
                        $sequence_number = $max_seq_query->max_seq + 1;
                    } else {
                        $sequence_number = !empty($request->list_enumeration) ? $request->list_enumeration : 1;
                    }
                    //prepare the data
                    $associationDataToSave[] = [
                        "item_association_id" => $associationIdentifier,
                        "association_type" => $associationTypeIdentifier,
                        "document_id" => $cfDocumentIdentifier,
                        "origin_node_id" => $originNodeIdentifier,
                        "destination_node_id" => $destinationNodeIdentifier,
                        "destination_document_id" => $cfDocumentIdentifier,
                        "created_at"    => $this->createDateTime(),
                        "updated_at"    => $this->createDateTime(),
                        "source_item_association_id" => $associationIdentifier,
                        "organization_id" => $input['auth_user']['organization_id'],
                        "source_document_id"            =>  $cfDocumentIdentifier,
                        "source_item_id"                =>  $originNodeIdentifier,
                        "target_document_id"            =>  $cfDocumentIdentifier,
                        "target_item_id"                =>  $destinationNodeIdentifier,
                        "sequence_number" => $sequence_number //adding sequence number to item node while adding cf item node
                    ];
                    // create childOf association
                    $this->run(new CreateItemAssociationsJob($associationDataToSave,$request['project_type'],''));
                }

                // raise event to track activities for reporting
                if(!empty($request->input('project_id'))) {
                    $afterEventRawData = $cfItem->toArray();
                    $afterEventRawData['project_id'] = $request->input('project_id');
                    $afterEventRawData['is_deleted'] = 0;
                    $eventData = [
                        "beforeEventRawData" => [],
                        "afterEventRawData" => (object) $afterEventRawData,
                        "requestUserDetails" => $request->input("auth_user")
                    ];
                    event(new CreateItemInsideProjectEvent($eventData));
                }

                //event for project and document update(updated_at) start
                event(new UpdateTaxonomyTimeEvent($identifier));
                //event for project and document update(updated_at) end

                // get is childof item association id
                $cfItemData = $cfItem->toArray();
                $childofassociationIdentifier = $this->getIsChildofItemAssociationId($cfItemData );
                $cfItemData['item_association_id'] = $childofassociationIdentifier;

                $successType = 'created';
                $message = 'CFItem created successfully.';
                $_status = 'created';
                return $this->run(new RespondWithJsonJob($successType,  $cfItemData, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function getIsChildofItemAssociationId($cfItemData){
        $childofAssociation = DB::table('item_associations')
        ->select('item_association_id')
        ->where('association_type',1)
        ->where('source_item_id',$cfItemData['item_id'])
        ->where('target_item_id',$cfItemData['parent_id'])
        ->where('destination_document_id',$cfItemData['document_id'])
        ->get()->toArray();
        
        return !empty($childofAssociation) ? $childofAssociation[0]->item_association_id : '';

    }
}
