<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeType\Jobs\ValidateNodeTypeIdJob;
use App\Domains\NodeType\Jobs\CheckNodeTypeIsDeleteJob;
use App\Domains\NodeType\Jobs\UpdateNodeTypeJob;

use App\Domains\Caching\Events\CachingEvent;
use App\Domains\NodeType\Jobs\ValidateNodeTypeIdUniqueTitleJob;
use App\Services\Api\Traits\StringHelper;
use Log;

class UpdateNodeTypeFeature extends Feature
{
    use StringHelper;

    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $nodeTypeId  = $request->route('node_type_id');

            $nodeTypeIdentifier =  [
                'node_type_id' => $nodeTypeId,
                'title' => $requestData['name'],
                'organization_id' => $requestData['auth_user']['organization_id']
            ];

            $validateNodeTypeStatus = $this->run(new ValidateNodeTypeIdUniqueTitleJob($nodeTypeIdentifier));

            if($validateNodeTypeStatus === true){
                $checkNodeTypeIsDeleted = $this->run(new CheckNodeTypeIsDeleteJob($nodeTypeId));
                if(!$checkNodeTypeIsDeleted === true){
                    $updateNodeTypeEntity = $this->run(new UpdateNodeTypeJob($nodeTypeId, $requestData));
                    
                    $response = $updateNodeTypeEntity;

                    // Caching Event
                    $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                    $eventData = [
                        "event_type"         => $eventType,
                        "organizationId"     => $requestData['auth_user']['organization_id'],
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];

                    event(new CachingEvent($eventData)); // caching ends here

                    $successType = 'custom_found';
                    $message = ['Nodetype updated succesfully.'];
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status, [], true));
                }
                else{
                    $errorType = 'custom_not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = ['NodeType is already deleted'];
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status, [], false));
                }
            }
            else{
                $errorType = 'custom_validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = $validateNodeTypeStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
