<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\User\Events\SessionRefreshedEvent;

class UpdateRefreshFeature extends Feature
{
    public function handle(Request $request)
    {
        $authorizationToken = $request->header('Authorization');
        $accessToken = !empty($authorizationToken) ? trim($authorizationToken) : "";
        if(!empty($accessToken)){
            $userData = DB::table('users')->select('users.*')->where('active_access_token',$accessToken)->where('is_deleted',0)->first();
            if($userData)
            {
                $flag = 1;
            }
            else {
                $flag = 0;
            }
        }
        else {

            $flag = 0;
           
        }
        if( $flag == 0)
        {
            $errorType = 'validation_error';
            $message = "Please provide a valid access token.";
           $_status = ' Invalid Token.';
           return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
        else {
            $update = [ "access_token_updated_at" =>  now()->toDateTimeString()];
            $userId = $userData->user_id;
            DB::table('users')->where('user_id', $userId)
            ->update($update);

            $users = json_decode(json_encode($userData), true);
            $eventData = [
                "beforeEventRawData" => $users,
                "afterEventRawData" => $users,
                "requestUserDetails" => $users
            ];
            event(new SessionRefreshedEvent($eventData)); 
            $successType = 'found';
            $message = 'Token is Refreshed';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
        }
    }


}
