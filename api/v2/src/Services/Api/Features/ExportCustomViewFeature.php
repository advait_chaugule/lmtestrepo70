<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportConnectionJob;
use App\Domains\JReport\Jobs\ExportCustomViewJob;
use App\Domains\JReport\Jobs\SetContentTypeJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

class ExportCustomViewFeature extends Feature
{
    public function handle(Request $request)
    {
        $connection= $this->run(new JReportConnectionJob());
        $input = $request->input();
        
        $organization_id = $input['auth_user']['organization_id'];
        $document_id = $request->route('document_id');
        $file_type = $request->route('file_type');  
        $file_type_allowed = array('pdf','docx');

        if(in_array($file_type,$file_type_allowed))
        {
            $documentDetail = $this->run(new GetDocumentByIdJob([ 'id' => $document_id]));        
            
            if($documentDetail === NULL) {
                $errorType = 'validation_error';
                $message = "Invalid document_id provided.";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));            
            }
            else
            {
                $file_name= $documentDetail['title'];
                $content=$this->run(new ExportCustomViewJob($connection,$organization_id,$document_id,$file_type));
                $this->run(new SetContentTypeJob($file_type,$content,$file_name));
            }
        }
        else
        {
            $errorType = 'validation_error';
            $message = "Invalid file type provided.";
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
