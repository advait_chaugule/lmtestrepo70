<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\WorkflowStage\Jobs\ValidateWorkflowStageIdJob;
use App\Domains\WorkflowStage\Jobs\GetWorkflowStageJob;
use Log;

class GetAllWorflowStagesFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $identifier = $request->route('workflow_stage_id');
            $input = [ 'workflow_id' => $identifier ];
            
            $validateWorkflowStageId = $this->run(new ValidateWorkflowStageIdJob($input));
            if($validateWorkflowStageId===true){
                $input[] = $request->all();
                $workflowStagelist = $this->run(new GetWorkflowStageJob($input));
                $response = $workflowStagelist;
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }else{
                $errorType = 'not_found';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Workflow Stage Id is not valid';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
