<?php
namespace App\Services\Api\Features;
ini_set('max_execution_time', 0);
use App\Domains\Asset\Jobs\GetAssetListJob;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;

class GetAuthPublishDocumentFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try {

            $requestData            =   $request->all();
            $documentIdentifier     =   $request->route('document_id');
            $taxonomySetType        =   $requestData['type'];
            $organizationIdentifier =   $requestData['organization_id'];
            $requestUrl = url('/');
                $keyInfo = array('identifier' => $documentIdentifier, 'prefix' => 'adoc_' . $taxonomySetType);
                $status_data = $this->run(new GetFromCacheJob($keyInfo));
             
                if (!empty($status_data['status']) && $status_data['status'] == "error") {
                    $requestData1["auth_user"]["organization_id"] = $organizationIdentifier;
                    $requestData2["organization_id"] = $organizationIdentifier;
                    $request->merge($requestData1);
                    $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeJob($documentIdentifier, $requestData2));

                    $dataToSend1 = $taxonomyHierarchy;
                    if($taxonomySetType == "hierarchy")
                    {
                        $taxonomySetType1 = "hierarchy";
                        $keyInfo1 = array('identifier' => $documentIdentifier, 'prefix' => 'adoc_' . $taxonomySetType1);
                        $status1 = $this->run(new SetToCacheJob($dataToSend1, $keyInfo1));
                    }
                    else
                    {
                         // Set Details of Taxonomy In cache
                            foreach ($taxonomyHierarchy['nodes'] as $node) {

                                $itemId = $node['id'];
                                $isDocument = $node['is_document'];
                                if ($isDocument == 1) {
                                    $validateDocument = $this->run(new ValidateDocumentByIdJob(['document_id' => $itemId]));
                                    if ($validateDocument === true) {
                                        $validateDocumentIsDeleted = $this->run(new CheckDocumentIsDeleteJob($itemId));
                                        if (!$validateDocumentIsDeleted) {
                                            $documentDetails = $this->run(new GetDocumentAndRespectiveMetadataValuesJob($itemId));

                                            $documentAssets = $this->run(new GetAssetListJob($itemId, $organizationIdentifier,$requestUrl));
                                            $documentDetails["assets"] = $documentAssets;
                                            $taxonomyHierarchy['details'][] = $documentDetails;
                                            unset($documentDetails);
                                        }
                                    }
                                } else {
                                    $validationStatus = $this->run(new ValidateItemByIdJob(['item_id' => $itemId]));

                                    if ($validationStatus === true) {

                                        $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemId));

                                        if (!$itemIsDeleted) {

                                            $response = $this->run(new GetCFItemDetailsJob($itemId, $organizationIdentifier));
                                            $exemplarAssociationList = $this->run(new GetExemplarAssociationsJob($itemId, ['organization_id' => $organizationIdentifier],$requestUrl));
                                            // embed the exemplar association collection inside the current response body
                                            $response["exemplar_associations"] = $exemplarAssociationList;

                                            // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                                            $itemAssets = $this->run(new GetAssetListJob($itemId, $organizationIdentifier,$requestUrl));
                                            $response["assets"] = $itemAssets;
                                            $taxonomyHierarchy['details'][] = $response;
                                            unset($response);
                                        }
                                    }
                                }
                            }
                            $dataToSend2['details'] = $taxonomyHierarchy['details'];
                            $taxonomySetType2 = "details";
                            $keyInfo2 = array('identifier' => $documentIdentifier, 'prefix' => 'adoc_' . $taxonomySetType2);
                            $status2 = $this->run(new SetToCacheJob($dataToSend2, $keyInfo2));   
                    }
                    

                    
                    $status3 = $this->run(new GetFromCacheJob($keyInfo));
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status3, $message, $_status));


                } else {
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status_data, $message, $_status));

                }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
