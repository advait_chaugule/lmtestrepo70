<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\LinkedServer\Jobs\ValidateLinkedServerIdJob;
use App\Domains\LinkedServer\Jobs\ValidateCreateLinkedServerInputJob;
use App\Domains\LinkedServer\Jobs\CheckedConnectionForLinkedServerJob;
use App\Domains\LinkedServer\Jobs\UpdateLinkedServerInputJob;
use Log;

class UpdateLinkedServerFeature extends Feature
{
    use StringHelper, ErrorMessageHelper;
    
    public function handle(Request $request)
    {
        try{
           
            $linkedServerIdentifier =   $request->route('linked_server_id');
            $requestData        =   $request->all();
            
            $linkeServerArr["linked_server_id"] = $linkedServerIdentifier;
            $linkeServerArr["linked_type"] = isset($requestData["linked_type"]) ? $requestData["linked_type"] : "1";  
            $linkeServerArr["linked_name"] = isset($requestData["linked_name"]) ? $requestData["linked_name"] : ""; 
            $linkeServerArr["description"] = isset($requestData["description"]) ? $requestData["description"] : ""; 
            $linkeServerArr["server_url"] = isset($requestData["server_url"]) ? $requestData["server_url"] : ""; 
            $linkeServerArr["organization_id"] = $requestData["auth_user"]["organization_id"];
            $linkeServerArr["updated_at"] = now()->toDateTimeString();
            $linkeServerArr["updated_by"] = $requestData["auth_user"]["user_id"];
            $linkeServerArr["is_save"] = isset($requestData["is_save"]) ? $requestData["is_save"] : "0"; 
            $linkeServerArr["is_create"] = 0;

            $identifier     =   ['linked_server_id'  =>  $linkedServerIdentifier];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            
            $validateLinkedServer    =   $this->run(new ValidateLinkedServerIdJob($identifier));
            if($validateLinkedServer === true) {
              
                    if($linkeServerArr["is_save"] == 0)
                    {
                        $checkedConnectionForLinkedServerStatus = $this->run(new CheckedConnectionForLinkedServerJob($linkeServerArr));
                        if($checkedConnectionForLinkedServerStatus === true)
                        {
                            $data = ['result' => 'success'];
                            $response = $data; 
                            $successType = 'found';
                            $message = 'connection successful';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                        }
                        else
                        {
                            $data = ['result' => 'failed'];
                            $response = $data; 
                            $successType = 'found';
                            $message = 'Connection failed';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                        }

                    }
                 else if($linkeServerArr["is_save"] == 1)
                {
                    $validateCreateLinkedServerInput = $this->run(new ValidateCreateLinkedServerInputJob($linkeServerArr));
                
                    if($validateCreateLinkedServerInput === true)
                    {
                        $linkeServerArr["connection_status"] = 1;
                        $updateLinkedServerInput = $this->run(new UpdateLinkedServerInputJob($linkeServerArr));
                        $successType = 'created';
                        $message = 'Linked Server updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $updateLinkedServerInput, $message, $_status));
                    } 
                    else
                    {
                    
                    $errorType = 'validation_error';
                    $message = $validateCreateLinkedServerInput;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));         
                    }   
                }
                  
               
            }
            else {
                $errors = $validateLinkedServer;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
