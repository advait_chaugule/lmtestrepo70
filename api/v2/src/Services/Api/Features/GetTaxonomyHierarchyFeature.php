<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentJob;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeJob;
use Log;

class GetTaxonomyHierarchyFeature extends Feature
{
    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $documentIdentifier = $request->route('document_id');
            $validationStatus = $this->run(new ValidateDocumentJob($documentIdentifier));
            if($validationStatus===true) {
            $requestingUserDetails = $request->input("auth_user");
            $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeJob($documentIdentifier, $requestingUserDetails));
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
