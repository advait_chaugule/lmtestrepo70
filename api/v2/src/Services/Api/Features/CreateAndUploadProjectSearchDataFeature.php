<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Project\Jobs\ValidateInputForCreateAndUploadProjectSearchDataJob;
use App\Domains\Project\Jobs\ValidateProjectIdJob;

use App\Domains\Project\Jobs\CreateProjectSearchDataJob;
use App\Domains\Document\Jobs\CreateDeltaDocumentUpdatesSearchDataJob;
use App\Domains\Item\Jobs\CreateDeltaItemUpdatesSearchDataJob;
use App\Domains\Search\Jobs\UploadSearchDataJob;
use App\Domains\Taxonomy\Jobs\GetNodeIdsAssociatedWithProjectJob;
use Log;

class CreateAndUploadProjectSearchDataFeature extends Feature
{

    use ErrorMessageHelper, ArrayHelper, StringHelper;

    public function handle(Request $request)
    {
        // set higher memory limit and execution time since this feature is resource intensive
        ini_set('memory_limit','8000M');
        ini_set('max_execution_time', 0);

        try{
            $requestInput = $request->all();
            $validateRequest = $this->run(new ValidateInputForCreateAndUploadProjectSearchDataJob($requestInput));
            if($validateRequest===true) {
                $projectIds = $this->createArrayFromCommaeSeparatedString($request->input('project_ids'));
                $httpResponseMessage = $this->executeCreateAndUploadSearchDataProcess($projectIds);
                $successType = 'created';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validateRequest;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function executeCreateAndUploadSearchDataProcess(array $projectIds): array {
        $httpResponseMessage = [];
        foreach($projectIds as $projectId) {
            $projectIdentifier = trim($projectId);
            $validateProjectId = $this->run(new ValidateProjectIdJob([ "project_id" => $projectIdentifier ]));
            if($validateProjectId===true) {
                $projectSearchDataToUpload = $this->run(new CreateProjectSearchDataJob($projectIdentifier));
                if(!empty($projectSearchDataToUpload)) {
                    // prepare the cloud search batch to include all nodes assigned to a project
                    $nodeIds = $this->run(new GetNodeIdsAssociatedWithProjectJob($projectIdentifier));
                    $projectAssociatedDocumentId = $nodeIds['document_id'];
                    $projectAssociatedItemIds = $nodeIds['item_ids'];
                    if(!empty($projectAssociatedDocumentId)) {
                        $deltaDocumentSearchDataToUpload = $this->run(new CreateDeltaDocumentUpdatesSearchDataJob($projectAssociatedDocumentId));
                        // push document cloud serch document to project search data collection
                        $projectSearchDataToUpload->push($deltaDocumentSearchDataToUpload->first());
                    }
                    if(!empty($projectAssociatedItemIds)) {
                        $deltaItemSearchDataToUpload = $this->run(new CreateDeltaItemUpdatesSearchDataJob($projectAssociatedItemIds));
                        $itemSearchDataArray = $deltaItemSearchDataToUpload->toArray();
                        // merge item cloud search document to project search data collection
                        $projectSearchDataToUpload = $projectSearchDataToUpload->merge($itemSearchDataArray);
                    }
                    
                    // finally upload the full batch together
                    $this->run(new UploadSearchDataJob($projectSearchDataToUpload));

                    $message = "Search data for Project:$projectIdentifier has been uploaded successfully.";
                }
                else {
                    $message = "Search data for Project:$projectIdentifier was not uploaded successfully.";
                }
            }
            else {
                $message = "Invalid Project:$projectIdentifier.";
            }
            $httpResponseMessage[] = $message;
        }
        return $httpResponseMessage;
        // return $this->arrayContentToCommaeSeparatedString($httpResponseMessage);
    }
}
