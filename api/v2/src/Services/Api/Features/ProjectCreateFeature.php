<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ValidateCreateProjectInputJob;
use App\Domains\Project\Jobs\SaveProjectJob;
use App\Domains\Project\Jobs\EditProjectJob;

use App\Domains\NodeType\Jobs\ListNodeTypeJob;
use App\Domains\Taxonomy\Jobs\CreateTaxonomyJob;

use App\Domains\Metadata\Jobs\ListMetadataJob;
use App\Domains\Metadata\Jobs\CreateMetadataJob;

use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Project\Events\CreateProjectEvent;
//use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class ProjectCreateFeature extends Feature
{
    use UuidHelperTrait;

    public function handle(Request $request)
    {
        try{
            $input = $request->input();
            $requestUrl = url('/');
            $data = [];
            $data['project_id'] = $this->createUniversalUniqueIdentifier();
            $data['project_name'] = $input['project_name'];
            $data['description'] = $input['description'];
            $data['workflow_id'] = $input['workflow_id'];
            $data['project_type'] = isset($input['project_type']) ? $input['project_type'] : "1" ;
            $data['is_deleted'] = 0;
            $data['created_by'] = $input['auth_user']['user_id'];
            $data['updated_by'] = $input['auth_user']['user_id'];

            // add organization id of the user performing project create action
            $userPerformingAction = $request->input("auth_user");
            $data['organization_id'] = $userPerformingAction["organization_id"];

            $validationStatus = $this->run(new ValidateCreateProjectInputJob($data));
            if($validationStatus===true) {
                $project = $this->run(new SaveProjectJob($data));
                $projectArray   =   $project->toArray();
                if($data['project_type'] != '1') {
                    $identifier = $this->createUniversalUniqueIdentifier();
                    $request->request->add(['document_title' => $data['project_name']]);
                    $request->request->add(['document_id' => $identifier]);
                    $request->request->add(['document_type' => '2' ]);
                    $request->request->add(['language_id' => '' ]);
                    $nodeTypeList = $this->run(new ListNodeTypeJob($request));

                    foreach($nodeTypeList['nodetype'] as $nodeType) {
                        if($nodeType['is_document'] ==  '1') {
                            $request->request->add(['document_node_type_id' => $nodeType['node_type_id'] ]);
                        }
                    }

                    $input['project_id']    =  $data['project_id']; 
                    
                    $input['document_id'] = isset($request['document_id'])?$request['document_id']:$identifier;  
                    $defaultDocumentIdForPacingGuide = $this->run(new CreateTaxonomyJob($input,$requestUrl));

                    $projectArray['document_id']    =   $defaultDocumentIdForPacingGuide;

                    $attributes =   ['project_id'   => $project->project_id, 'document_id'  =>  $request['document_id'] ];

                    $this->run(new EditProjectJob($attributes));

                }

                // raise event to track activities for reporting
                $afterEventRawData = $project->toArray();
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData" => (object) $afterEventRawData,
                    "requestUserDetails" => $request->input("auth_user")
                ];
                event(new CreateProjectEvent($eventData));
                //In App Notification events
                //event(new NotificationsEvent($eventData));

                $successType = 'created';
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, (object) $projectArray, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please provide valid inputs.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
