<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Organization\Jobs\GetDefaultOrganizationDetailJob;
use Log;

class GetDefaultOrganizationFeature extends Feature
{   
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {

        try{
             $organizationDetail=$this->run(new GetDefaultOrganizationDetailJob);
             if(!empty($organizationDetail)){
                $successType = 'found';
                $organizationDetails=$organizationDetail;
                $message = 'Data Found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $organizationDetails, $message, $_status));
             }
             else{
                $successType = 'not found';
                $organizationDetails="Default Organization id not found";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($successType, $organizationDetails, $_status));
             }
                
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
