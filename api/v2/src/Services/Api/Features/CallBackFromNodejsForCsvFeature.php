<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\Log;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Import\Events\SummaryMultipleCsvEvent;

class CallBackFromNodejsForCsvFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            Log::error('In CallBackFromNodejsForCsvFeature');
            Log::error('event_data:::'.$request->data['event_data']);
            Log::error('table_name:::'.$request->data['table_name']);
            $eventData = json_decode($request->data['event_data'], true);
            $eventData['table_name'] = $request->data['table_name'];
            event(new SummaryMultipleCsvEvent($eventData));

            return;
        } catch (\Exception $e) {
            // dd($this->createErrorMessageFromException($e));
            Log::error('In CallBackFromNodejsForCsvFeature');
            Log::error($this->createErrorMessageFromException($e));
        }

    }
}
