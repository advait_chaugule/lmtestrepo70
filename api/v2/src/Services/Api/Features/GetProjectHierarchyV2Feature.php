<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Taxonomy\Jobs\GetProjectTreeV2Job;

use App\Domains\Project\Events\ViewProjectItemsEvent;
use Log;
use Illuminate\Support\Facades\DB;

class GetProjectHierarchyV2Feature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $projectId = $request->route('project_id');

            $inputData  =   $request->all();
            $input = [ 'project_id' => $projectId];
            //Validate the project identifier
            $validateProjectExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateProjectExistence===true) {
                $requestUserDetails = $request->input("auth_user");

                /*** Create the success response ***/
                $successType = 'custom_found';
                $message = 'Mapped Node listed successfully.';
                $_status = 'custom_status_here';

                if($request->isMethod('head'))
                {
                    $taxonomyHierarchy = [];
                    $document = DB::table('documents')
                                    ->select('documents.updated_at')
                                    ->leftjoin('projects','projects.document_id', '=', 'documents.document_id')
                                    ->where('documents.organization_id', $inputData['auth_user']['organization_id'])
                                    ->where('projects.project_id', $projectId)
                                    ->first();

                    $headers = [
                        'LastModifiedDate' => $document->updated_at
                    ];
                    return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status, $headers, true));
                }

                $taxonomyHierarchy = $this->run(new GetProjectTreeV2Job($requestUserDetails,$projectId));
                if(count($taxonomyHierarchy['nodes']) > 0 || count($taxonomyHierarchy['relations']) > 0)
                {
                    $headers = [
                        'LastModifiedDate' => $taxonomyHierarchy['modified_at']->format('Y-m-d H:i:s')
                    ];
                    unset($taxonomyHierarchy['modified_at']);
                    // raise event to track activities for reporting
                    $beforeEventRawData = [];
                    $afterEventRawData = $input;
                    $eventData = [
                        "beforeEventRawData" => (object) $beforeEventRawData,
                        "afterEventRawData" => (object) $afterEventRawData,
                        "requestUserDetails" => $request->input("auth_user")
                    ];
                    event(new ViewProjectItemsEvent($eventData));

                    return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status, $headers, true));
                }
                else
                {
                    $errorType = 'validation_error';
                    $message = "data not found";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }

            }
            else {
                $errors = $validateProjectExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
