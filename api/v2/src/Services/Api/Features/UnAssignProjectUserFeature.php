<?php
namespace App\Services\Api\Features;

use App\Domains\Notifications\Jobs\GetProjectDetailByProjectId;
use App\Domains\Notifications\Jobs\GetTaxonomyNameById;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\UnAssignProjectUserJob;
use App\Domains\Project\Jobs\GetUserRoleInfoJob;
use App\Domains\Project\Jobs\SendUserRemoveProjectAccessEmailNotificationJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Notifications\Jobs\NotificationJobForEmail;
use App\Domains\User\Jobs\GetUserByIdJob;
use App\Domains\Caching\Events\CachingEvent;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use Log;

class UnAssignProjectUserFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData = $request->input();
            $input = $request->all();
            $projectId = $request->route('project_id');
            $userId = $request->input('user_id');
            $organizationId = $input['auth_user']['organization_id'];
            $loginUser = $input['auth_user']['user_id'];
            $projectIdentifier = ['project_id' => $projectId];
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);
            $validateStatus = $this->run(new ValidateProjectIdJob($projectIdentifier));
            if($validateStatus === true){
                $checkIsProjectDeleted = $this->run(new CheckIsProjectDeletedJob($projectIdentifier));
                if(!$checkIsProjectDeleted === true) {
                    $getUserRoleInfo =   $this->run(new GetUserRoleInfoJob($projectId, $requestData));
                    $deleteUserToRoleInProject = $this->run(new UnAssignProjectUserJob($projectId, $requestData, $organizationId, $loginUser));
                    if($deleteUserToRoleInProject) {
                        foreach($getUserRoleInfo as $userRoleInfo){
                            $inputDetail['user_name'] = $userRoleInfo['user_name'];
                            $inputDetail['email'] = $userRoleInfo['user_email'];
                            $inputDetail['user_id'] = $userRoleInfo['user_id'];
                            $inputDetail['current_organization_id'] = $userRoleInfo['current_organization_id'];
                            $inputDetail['role_name'] = $userRoleInfo['role_name'];
                            $inputDetail['project_name'] = $userRoleInfo['project_name'];
                            $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($loginUser,$organizationId));
                            $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                            $inputDetail['organization_name'] = $organizationData['organization_name'];
                            $inputDetail['organization_code'] = $organizationData['organization_code'];
                            $inputDetail['domain_name'] = $serverName;
                            $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$inputDetail['user_id'], 'organization_id' => $inputDetail['current_organization_id']]));
                            $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                           if ($emailSettingStatus==1) {
                                unset($inputDetail['user_id']);
                                unset($inputDetail['current_organization_id'] );
                                $this->run(new SendUserRemoveProjectAccessEmailNotificationJob($inputDetail));
                            }
                        }
                        // In App Notifications
                        $eventType = config("event_activity")["Notifications"]["REVOKE_PROJECT_ROLE"];
                        $eventData = ['project_id'  => $projectId,
                                       'user_id'    => $userId,
                                      'event_type'  => $eventType,
                                      'domain_name' => $serverName,
                                      "beforeEventRawData" => [],
                                      "afterEventRawData"  => '',
                                      "requestUserDetails" => $request->input("auth_user")
                                      ];
                        event(new NotificationsEvent($eventData));  //In App Notification ends
                        // Sent Mail for assigned user to project
                        
                        $projectName          = isset($inputDetail['project_name'])?$inputDetail['project_name']:'';
                        //$messageNotify        = config('event_activity')['notification_message']["5"];
                        $notification         = "Your access for the role {$inputDetail['role_name']} has been removed for the project {$projectName}.";
                
                        $email          = $inputDetail['email'];
                      
                       $userName       = $inputDetail['user_name'];  
                       $emailData = [
                                          'subject'=>$notification,
                                           'email'    => $email,
                                          'body'     => ['email_body'=>$notification,'user_name'=>$userName],
                                       ];             
                                       // Email notifications
                        //$this->run(new NotificationJobForEmail($emailData)); 
                        
                        $successType = 'deleted';
                        $message     = 'User deleted successfully.';
                        $_status     = 'deleted';
                        return $this->run(new RespondWithJsonJob($successType,  [], $message, $_status));
                    }
                    else{
                        // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                        $errorType = 'not_found_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'User is already deleted';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Project is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'not_found_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Project ID is invalid';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
