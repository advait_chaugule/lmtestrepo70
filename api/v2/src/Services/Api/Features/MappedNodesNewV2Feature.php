<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Taxonomy\Jobs\GetCompleteTreeWithProjectV2Job;
use Log;

class MappedNodesNewV2Feature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $projectId = $request->route('project_id');

            $inputData  =   $request->all();
            $input = [ 'project_id' => $projectId];
            //Validate the project identifier
            $validateProjectExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateProjectExistence===true) {
                $requestUserDetails = $request->input("auth_user");
                $taxonomyHierarchy = $this->run(new GetCompleteTreeWithProjectV2Job($requestUserDetails,$projectId));
                
               /*** Create the success response ***/
               $successType = 'found';
               $message = 'Mapped Node listed successfully.';
               $_status = 'custom_status_here';
               return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));

            }
            else {
                $errors = $validateProjectExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
