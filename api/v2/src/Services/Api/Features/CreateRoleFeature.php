<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ValidateCreateRoleInputJob;
use App\Domains\Role\Jobs\CreateRoleJob;
//use App\Domains\Role\Jobs\RolePermissionSetJob;
use Log;

class CreateRoleFeature extends Feature
{
    use ArrayHelper, StringHelper, UuidHelperTrait, DateHelpersTrait;

    public function handle(Request $request)
    {
        try{
            $permissionSet = '';
            $requestData = $request->all();
            $inputData = ['name' => $requestData['name'],'description' => $requestData['description'], 'organization_id' => $requestData['auth_user']['organization_id']]; //,'permission_set' => $requestData['permission_set']

            $validateRoleData = $this->run(new ValidateCreateRoleInputJob($inputData));
            if($validateRoleData===true) {
                $roleId = $this->createUniversalUniqueIdentifier();
                $inputData['role_id'] = $roleId;

                $roleCreated = $this->run(new CreateRoleJob($inputData));
                //Do not omit this code, it might be required based on UI
                /*if($roleCreated->role_id !== false){
                    $identifier = ['role_id' => $roleCreated->role_id];
                    $permissionArray = $this->delimittedStringToArray($inputData['permission_set']);
                    $permissionSet = $this->run(new RolePermissionSetJob($identifier, $permissionArray));
                }*/

                $resultData = $roleCreated; //$permissionSet;

                $successType = 'custom_created';
                $message = ['Role Created Successfully.'];
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status, [], true));
            }
            else{
                $errorType = 'custom_validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = [$validateRoleData];
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
