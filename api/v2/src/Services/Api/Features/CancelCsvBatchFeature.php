<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Import\Jobs\ValidateBatchIdJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

class CancelCsvBatchFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try {
            $batchId = $request->batch_id;
            $validateBatchId = $this->run(new ValidateBatchIdJob($batchId));
            if ($validateBatchId === true) {
                if(DB::table('import_job')->where('batch_id', $batchId)->update(['is_deleted' => 1])) {
                    $successType = 'custom_found';
                    $message = 'Batch import cancelled successfully';
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));
                } else {
                    $successType = 'custom_not_found';
                    $message = 'Batch import could not be cancelled, please try again';
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
            } else {
                $errorType = 'custom_validation_error';
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($errorType, [], $validateBatchId, $_status, [], false));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
