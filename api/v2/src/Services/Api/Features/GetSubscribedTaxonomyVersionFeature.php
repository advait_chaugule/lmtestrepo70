<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\GetSubscribedTaxonomyVersionJob;
use App\Domains\Document\Jobs\ValidateDocumentIdsAllExistsJob;
use App\Domains\Document\Jobs\ValidateSourceDocumentIdsAllExistsJob;
use App\Domains\Organization\Jobs\GetOrganizationIdByOrgCodeJob;

class GetSubscribedTaxonomyVersionFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestDetails = $request->all();
            $orgCode        = $request->route('org_code');
            $publisherOrg   = $this->run(new GetOrganizationIdByOrgCodeJob($orgCode)); // Get organization_id based on org_code
            $documentIdentifier    = (null!==$request->input('document_id')) ? $request->input('document_id') : '';
            
            if(isset($publisherOrg->organization_id)) {
                $publisherOrgId = $publisherOrg->organization_id; // Get organization_id based on org_code
                if(!empty($documentIdentifier)){
                    $validateDocuments = $this->run(new ValidateSourceDocumentIdsAllExistsJob([$documentIdentifier],$publisherOrgId));
                    if($validateDocuments['status']=='false'){
                        // If status fasle, Check if publisher's document id is active and belong to the same tenant as publisher's org_id
                        $validateDocuments = $this->run(new ValidateDocumentIdsAllExistsJob([$documentIdentifier],$publisherOrgId));
                    }
                    // If status = true from either source_doument_id or document_id, get details from documents table                               
                    if($validateDocuments['status']=='true'){
                        $publisherDocument = $validateDocuments['data'][0];
                        $publisherDocumentId = $publisherDocument->document_id;
                        $publishedVersion = $this->run(new GetSubscribedTaxonomyVersionJob($publisherDocumentId));
                        if(!empty($publishedVersion)){
                            $successType = 'custom_found';
                            $message = 'Data Found.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $publishedVersion, $message, $_status, [], true));
                        }                        
                    }
                    else{ // If no record inserted, it means already exists in table
                        $successType = 'custom_not_found';
                        $message = 'Document not found.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                    }
                }
                else{
                    $successType = 'custom_not_found';
                    $message = 'Document Id is mandatory.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid tenant_id.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
