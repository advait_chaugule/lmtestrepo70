<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Workflow\Jobs\ExistWorkflowInProjectJob;
use App\Domains\Workflow\Jobs\DeleteWorkflowJob;
use Log;

class DeleteWorkflowFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $workflowIdentifier =   $request->route('workflow_id');
            $requestData        =   $request->all();

            $identifier     =   ['workflow_id'  =>  $workflowIdentifier];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            
            $validateWorkflow    =   $this->run(new ValidateWorkflowIdJob($identifier));
            if($validateWorkflow === true) {
                    if (isset($requestData['del']) && $requestData['del'] == 'yes') {

                        $deleteworkflow = $this->run(new DeleteWorkflowJob($identifier));
                        $response = ''; 
                        $successType = 'found';
                        $message = 'Workflow deleted successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

                    }
                    else
                    {
                       $projectList = $this->run(new ExistWorkflowInProjectJob($identifier));
                       if($projectList == "")
                       {
                            $deleteworkflow = $this->run(new DeleteWorkflowJob($identifier));
                            $response = ''; 
                            $successType = 'found';
                            $message = 'Workflow deleted successfully.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

                       }
                       else
                       {
                        $response = $projectList; 
                        $successType = 'found';
                        $message = 'List of Project';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

                       }
                    }


            } else {
                $errors = $validateWorkflow;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
