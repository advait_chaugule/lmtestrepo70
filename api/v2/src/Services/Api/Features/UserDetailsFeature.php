<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\User\Jobs\GetUserByIdJob;
use Log;

class UserDetailsFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $user_id = $request->route('user_id');
            $input = [ 'user_id' => $user_id ];
            $document = $this->run(new GetUserByIdJob($input));
            $successType = 'Document found';
            $message = 'Document found';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $document, $message, $_status));
           
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
