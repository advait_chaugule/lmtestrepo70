<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithDownloadFileJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentJob;

use App\Domains\CaseStandard\Jobs\CreateCaseStandardDataJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageJob;
use App\Domains\Organization\Jobs\GetOrganizationIdByOrgCodeJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageWithActiveMetadataJob;
use App\Domains\Taxonomy\Jobs\GetPublishedTaxonomyJsonJob;
use App\Domains\Document\Jobs\ValidateDocumentIdsAllExistsJob;
use App\Domains\Document\Jobs\ValidateSourceDocumentIdsAllExistsJob;
use App\Domains\Taxonomy\Jobs\GetVersionSubscriptionDetailsJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Domains\CaseStandard\Jobs\CustomMetadataJob;

use Log;
use Storage;
use DB;
use App\Services\Api\Traits\UuidHelperTrait;

class ExportVersionJsonFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;

    public function handle(Request $request)
    {
        try{
            $orgCode            = $request->route('org_code');
            $requestUrl         = (null!==$request->input("request_url")) ? $request->input("request_url") : url('/');
            $publisherOrg       = $this->run(new GetOrganizationIdByOrgCodeJob($orgCode)); // Get organization_id based on org_code
            $documentIdentifier = $request->input('document_id');
            $is_html            = '';
            $requestUserDetails = $request->input("auth_user");
            $organizationId     = (null!==$request->input("subscriber_org_id")) ? $request->input("subscriber_org_id") : (isset($requestUserDetails["organization_id"]) ? $requestUserDetails["organization_id"] : '');
            $userId             = (null!==$request->input("subscriber_user_id")) ? $request->input("subscriber_user_id") : (isset($requestUserDetails["user_id"]) ? $requestUserDetails["user_id"] : '');
            $versioning         = true;  //Added to check request is for versioning - UF-1871
            $subscriberDocId     = (null!==$request->input("subscriberDocId")) ? $request->input("subscriberDocId") : '';
            $checkSubscriptionId = (null!==$request->input("subscription_id")) ? $request->input("subscription_id") : '';
            if(isset($publisherOrg->organization_id)) {
                $publisherOrgId = $publisherOrg->organization_id; // Get organization_id based on org_code                
                
                $inValidDocument = true; // Flag for invalid document
                // Check if publisher's source_document id is active and belong to the same tenant as publisher's org_id 
                $validateDocuments = $this->run(new ValidateSourceDocumentIdsAllExistsJob([$documentIdentifier],$publisherOrgId));
                if($validateDocuments['status']=='false'){
                    // If status fasle, Check if publisher's document id is active and belong to the same tenant as publisher's org_id
                    $validateDocuments = $this->run(new ValidateDocumentIdsAllExistsJob([$documentIdentifier],$publisherOrgId));
                }
                // If status = true from either source_doument_id or document_id, get details from documents table                               
                if($validateDocuments['status']=='true'){
                    $publisherDocument = $validateDocuments['data'][0];
                    $publisherDocumentId = $publisherDocument->document_id;
                    $sourceDocumentId   = $publisherDocument->source_document_id;
                    $lastChange         = $publisherDocument->updated_at;
                    $firstVersionTag = $secondVersionTag = ""; // Added to show in comparison report screen
                    // Check if logged user is publisher or has access to publisher's tenant by comparing user's org id to tenant code
                    $isPublisher = ($organizationId == $publisherOrgId) ? true : false;
                    //Check subscription in db.. if not then show error message
                    
                    $subscriptionId = $this->run(new GetVersionSubscriptionDetailsJob($publisherDocumentId,$publisherOrgId,$userId,$organizationId,$subscriberDocId));                                     
                    // Check subscription in db.. if not then show error message                    
                    if($isPublisher===false && ($subscriptionId=='' || (!empty($checkSubscriptionId) && $checkSubscriptionId!=$subscriptionId))){
                        $message = 'No subscription found.';
                    }

                    $s3Bucket        =  config("event_activity")["Taxonomy_Folder"];
                    $S3URL           = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/";
                    $firstVersion = '0'; // Default first version
                    if((isset($request['first_version']) && !empty($request['first_version'])) && !isset($message)){
                        // Get Details of first version based on param first_version
                        $firstVersion  = $request['first_version'];
                        $publishedFirstVersion = $this->run(new GetPublishedTaxonomyJsonJob($publisherDocumentId,$publisherOrgId,$firstVersion));
                        if($publishedFirstVersion===false){
                            $message = "Invalid first version";
                        }
                        else{
                            $firstVersionCaseUrl = $S3URL.$publishedFirstVersion->taxonomy_json_file_s3;
                            $firstVersionCustomUrl = $S3URL.$publishedFirstVersion->custom_json_file_s3;
                            $firstVersionTag = $publishedFirstVersion->publish_number.' - '.(!empty($publishedFirstVersion->tag) ? $publishedFirstVersion->tag.' - ': '').$publishedFirstVersion->publish_date; // Added to show as per UF-3344 in comparison report screen
                        }
                    }

                    if(isset($request['second_version']) && !empty($request['second_version'])){
                        // Get Details of second version based on param second_version
                        $secondVersion  = $request['second_version'];
                        $publishedSecondVersion = $this->run(new GetPublishedTaxonomyJsonJob($publisherDocumentId,$publisherOrgId,$secondVersion));
                    }
                    else{
                        // By default second version is latest version                    
                        $publishedSecondVersion = $this->run(new GetPublishedTaxonomyJsonJob($publisherDocumentId,$publisherOrgId,''));
                    }
                    
                    // Assign values of variables for second version
                    if(isset($publishedSecondVersion->publish_number)){
                        $secondVersion  = $publishedSecondVersion->publish_number;
                        $secondVersionCaseUrl = $S3URL.$publishedSecondVersion->taxonomy_json_file_s3;
                        $secondVersionCustomUrl = $S3URL.$publishedSecondVersion->custom_json_file_s3;
                        $secondVersionTag = $publishedSecondVersion->publish_number.' - '.(!empty($publishedSecondVersion->tag) ? $publishedSecondVersion->tag.' - ': '').$publishedSecondVersion->publish_date; // Added to show as per UF-3344 in comparison report screen
                    }
                    else{
                        $message = "Invalid second version";
                    }
                    $documentIdForCustom = $publisherDocumentId;
                    // Check if subscriber doc id passed is valid and get last change details
                    if($firstVersion==='0' && $isPublisher === false){
                        $validateSubscribeDocs = $this->run(new ValidateSourceDocumentIdsAllExistsJob([$sourceDocumentId],$organizationId));
                        if($validateSubscribeDocs['status']=='true'){
                            $subscriberDocument = $validateSubscribeDocs['data'][0];
                            $lastChange         = $subscriberDocument->updated_at;
                            $sourceDocumentId   = $subscriberDocument->source_document_id;
                            $documentIdForCustom= $subscriberDocument->document_id;
                            $getOrgCode         = $this->run(new GetOrgCodeByOrgId($organizationId)); // Get org_code based on organization_id
                            $orgCode            = $getOrgCode->org_code;
                        }
                        else{
                            $message = "Invalid subscriber document id";
                        }
                    }                    

                    if(isset($message)){
                        $successType = 'custom_not_found';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                    }

                    // Check comparison report already exists for combo of first & second version
                    // for draft version of publisher check if report is generated after the last changes made to the document
                    $reportQuery = DB::table('version_comparison')
                                    ->where(['document_id'=>$publisherDocumentId,
                                    'organization_id'=>$publisherOrgId,
                                    'first_version'=>$firstVersion,
                                    'second_version'=>$secondVersion]);
                    if($firstVersion==='0'){
                        $reportQuery->where('comparison_datetime','>=',$lastChange);
                        $reportQuery->where('subscription_id',$subscriptionId);
                    }                                    
                    $comparisonReport = $reportQuery->first();
                    if(empty($comparisonReport)){
                        if($firstVersion==='0'){
                            $fileUUID           = $this->createUniversalUniqueIdentifier();
                            $s3Bucket           = config("event_activity")["Version_Folder"];                
                            $s3FileName         = $fileUUID.'_case.json';
                            $packageData        = $this->run(new GetCasePackageWithActiveMetadataJob($sourceDocumentId,[],$orgCode,0,'',$requestUrl,['versioning'=>true]));
                            $destinationS3PackageData   = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/$s3FileName";
                            Storage::disk('s3')->put($destinationS3PackageData,json_encode($packageData));
                            
                            $s3FileName         = $fileUUID.'_custom.json';
                            $customMetadata = $this->run(new CustomMetadataJob($sourceDocumentId, $orgCode, $documentIdForCustom,$requestUrl));
                            $destinationS3CustomMetadata  = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/$s3FileName";
                            Storage::disk('s3')->put($destinationS3CustomMetadata,json_encode($customMetadata));

                            $firstVersionCaseUrl = $destinationS3PackageData;
                            $firstVersionCustomUrl = $destinationS3CustomMetadata;
                        }
                        $comparisonId = $this->createUniversalUniqueIdentifier();
                        $comparisonReport = [
                            'comparison_id' => $comparisonId,
                            'subscription_id' => $subscriptionId,
                            'document_id' => $publisherDocumentId,
                            'source_document_id' => $sourceDocumentId,
                            'organization_id' => $publisherOrgId,
                            'comparison_datetime' => date('Y-m-d H:i:s'),
                            'first_version' => $firstVersion,
                            'second_version' => $secondVersion,
                            'first_version_case_url' => $firstVersionCaseUrl,
                            'first_version_custom_url' => $firstVersionCustomUrl,
                            'second_version_case_url' => $secondVersionCaseUrl,
                            'second_version_custom_url' => $secondVersionCustomUrl
                        ];
                        DB::table('version_comparison')->insert($comparisonReport);
                        $report_generated = "0";                   
                    }
                    else{
                        $comparisonId = $comparisonReport->comparison_id;
                        $report_generated = $comparisonReport->report_generated;
                    }

                    $comparisonArray = ['comparison_id' => $comparisonId];
                    if($report_generated=="0"){
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "localhost:3000/compare",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($comparisonArray),
                            CURLOPT_HTTPHEADER => array(
                                "Content-Type: application/json"
                            ),
                        ));

                        $jsonresponse = curl_exec($curl);
                        curl_close($curl);
                        $response = json_decode($jsonresponse, true);
                        if(!isset($response['success']) || (isset($response['success']) && $response['success'] !== true)) {
                            $successType = 'custom_not_found';
                            $message = 'Something went wrong, Please try again later';
                            $_status = 'custom_status_here';
                            $currentTime = now()->toDateTimeString();
                            $comparisonError = ['comparison_id' => $comparisonId, 'error_type' => 'NODEJS_ERROR','error_description'=>$jsonresponse,'created_at' => $currentTime,'updated_at' => $currentTime];
                            DB::table('version_compairison_logs')->insert($comparisonError);
                            return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                        }
                    }
                    DB::table('version_comparison')->where($comparisonArray)->update(['report_generated'=>'1']);

                    $successType = 'custom_found';
                    $message = 'Comparison report generated';
                    $_status = 'custom_status_here';
                    $comparisonArray['versionTagInfo'] = ['first_version' => $firstVersionTag,'second_version' => $secondVersionTag];// Added to show in comparison report screen
                    return $this->run(new RespondWithJsonJob($successType, $comparisonArray, $message,$_status,[],true));
                }
                else{ 
                    $successType = 'custom_not_found';
                    $message = 'Document is not present';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid tenant_id.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
