<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ComparisonSummaryHelperTrait;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

class NodeDetailsFromPublishedJsonFeature extends Feature
{
    use ErrorMessageHelper,ComparisonSummaryHelperTrait;
    public function handle(Request $request)
    {
        try
        {
            $requestingUserDetails = $request->input("auth_user");
            $comparisonId = (null!==$request->input("comparison_id")) ? $request->input('comparison_id') : '';
            $nodeId = (null!==$request->input("node_id")) ? $request->input('node_id') : '';
            $error = $this->checkComparisonIdExists($comparisonId);

            if($error == true){
                $message = 'Comparison Id not found.';
                $successType = 'custom_not_found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
            }            
            
            $nodeDetails = $this->getNodeDetailsFromNodeId($comparisonId,$nodeId);
            if(!empty($nodeDetails)) 
            {
                $successType = 'custom_found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $nodeDetails, $message, $_status, [], true));
            }
            else
            {
                $successType = 'custom_not_found';
                $message = 'Node Id Not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
            }
        }
        catch(\Exception $ex)
        {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
