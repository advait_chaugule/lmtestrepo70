<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Log;
use App\Domains\User\Jobs\GetUserSettingDetailsJob;
use App\Domains\User\Jobs\GetTaxonomyConfigSettingsJob;
use App\Domains\User\Jobs\GetProjectSettingDetailsJob;
use Lucid\Foundation\Feature;

class GetSystemSettingFeature extends Feature
{
    public function handle(Request $request)
    {
       try{
            $requestData     = $request->all();
            $userId          = isset($requestData['auth_user']['user_id'])?($requestData['auth_user']['user_id']):'';    
            $organizationId  = isset($requestData['auth_user']['organization_id'])?($requestData['auth_user']['organization_id']):$request->get('organization_id');
            if(empty($organizationId))   {                                          //for front-facing
                    $errorType = 'custom_not_found';
                    $message   = 'Organization_id not found';
                    $_status   = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status,[],false));
                }
            $featureId       = $request->route('feature_id');
            $settingId       = $request->route('setting_id');
            $documentId      = $request->get('document_id');
            $projectId       = $request->get('project_id');
            $response = [];
            switch ($featureId)
            {
                case 1: // List view of taxonomies
                    $response = $this->run(new GetTaxonomyConfigSettingsJob($userId,$organizationId,$settingId,$featureId));
                    break;
                case 2: // List view of projects
                    break;
                case 3: // list view of a individual taxonomy
                    $response = $this->run(new GetUserSettingDetailsJob($userId,$organizationId,$documentId,$settingId,$featureId));
                    break;
                case 4: // list view of individual a project
                    $response = $this->run(new GetProjectSettingDetailsJob($userId,$organizationId,$projectId,$featureId,$settingId));
                    break;
                default:
                    break;
            }
            $resultData = $response;
            $successType = 'custom_found';
            $_status = 'custom_status_here';
            $message = !empty($resultData) ? 'Data Found.' : 'Data Not Found.';
            $is_success = !empty($resultData) ?'true' : 'false';
            return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status,[], $is_success));
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}