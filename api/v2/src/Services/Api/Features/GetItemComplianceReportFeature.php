<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Report\Jobs\GetItemsAndDocMappedWithProjectJob;
use App\Domains\Report\Jobs\GetItemComplianceReportJob;

use App\Domains\Report\Jobs\ValidateDocumentExistJob;
use App\Domains\Report\Jobs\GetItemsAndDocMappedWithTaxonomyJob;
use Log;

class GetItemComplianceReportFeature extends Feature
{
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestVar = $request->all();
            $oragnization_id =  $requestVar['auth_user']['organization_id'];
            $identifier=$request->route('identifier');
            $identifierType=$request->route('identifier_type');
            $identifierTypeValidator=array('1','2');

            if(in_array($identifierType,$identifierTypeValidator))
            {
                if($identifierType=="1")
                {
                    $input['document_id'] = $request->route('identifier');
                    $validateDocumentId = $this->run(new ValidateDocumentExistJob($input));

                    if($validateDocumentId===true)
                    {
                        $getItemsAndDocMappedWithProject = $this->run(new GetItemsAndDocMappedWithTaxonomyJob($input['document_id']));
                        
                        $getItemComplianceReport = $this->run(new GetItemComplianceReportJob($getItemsAndDocMappedWithProject,$oragnization_id));

                        if(!empty($getItemComplianceReport))
                        {
                            $successType = 'found';
                            $message = $getItemComplianceReport;
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message ,$_status));
                        }

                    }
                    else
                    {
                        $errorType = 'internal_error';
                        $validatemessage="Invalid Document ID";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
                    }
                }
                else if($identifierType=="2")
                {
                    $input['project_id'] = $request->route('identifier');
                    $validateProjectId = $this->run(new ValidateProjectIdJob($input));

                    if($validateProjectId===true)
                    {
                        $getItemsAndDocMappedWithProject = $this->run(new GetItemsAndDocMappedWithProjectJob($input['project_id']));
                        
                        $getItemComplianceReport = $this->run(new GetItemComplianceReportJob($getItemsAndDocMappedWithProject,$oragnization_id));

                        if(!empty($getItemComplianceReport))
                        {
                            $successType = 'found';
                            $message = $getItemComplianceReport;
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message ,$_status));
                        }
                    }
                    else
                    {
                        $errorType = 'internal_error';
                        $validatemessage="Invalid Project ID";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
                    }

                }
            }
            else
            {
                $errorType = 'validation_error';
                $message = 'enter a valid identifier type';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
