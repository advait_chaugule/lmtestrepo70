<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Note\Jobs\GetAllNoteJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;


class GetAllNoteFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
          //  !empty($getPublicReviewHistoryDataValue->document_published_date)?$getPublicReviewHistoryDataValue->document_published_date:"",
            $requestData = $request->all();
            $noteId = $request->route('notes');
            $noteId = !empty($noteId) ? $noteId : "";
            $organizationId = $requestData['auth_user']['organization_id'];
            $input = ['note_id'         => $noteId,
                      'organization_id' => $organizationId];
            //dd($input);
            $noteList = $this->run(new GetAllNoteJob($input));

            if(count($noteList) == 0)
            {
                $successType = 'No Data ';
                $message = 'Data not found';
                $_status = 'custom_status_here';
                $data = [];
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }else
            {
                $successType = 'found';
                $message = 'Data found';
                $_status = 'custom_status_here';
                // $data = ['result' => 'empty'];
                return $this->run(new RespondWithJsonJob($successType, $noteList, $message, $_status));


            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
