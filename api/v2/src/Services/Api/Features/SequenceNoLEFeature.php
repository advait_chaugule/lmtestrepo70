<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;

class SequenceNoLEFeature extends Feature
{
    public function handle(Request $request)
    {
        ini_set('memory_limit','80980M');
        ini_set('max_execution_time', 0);

        try {
            $output_response = [];
            $type = trim($request->type);
            $code = trim($request->code);
            if($type == '' || $code == '') {
                $successType = 'custom_not_found';
                $message = 'Invalid parameters';
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
            }
            if($type == 'taxonomy') {
                # exclude taxonomies with import type 1
                $document_id_arr = DB::select("select `document_id` from `acmt_documents` where `is_deleted` = 0 and `import_type` != 1 and document_id='".$code."'");
                if(!$document_id_arr) {
                    $successType = 'custom_not_found';
                    $message = 'Taxonomy not found or not of import type 1';
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
                }
                $this->setSequenceNo($document_id_arr);

            } else if($type == 'org') {
                $org = DB::table('organizations')->select('organization_id')->where('org_code', $code)->where('is_deleted', 0)->first();
                if(!$org) {
                    $successType = 'custom_not_found';
                    $message = 'Organization not found';
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
                }

                $org_id = $org->organization_id;
                # exclude taxonomies with import type 1
                $document_id_arr = DB::select("select `document_id` from `acmt_documents` where `is_deleted` = 0 and `import_type` != 1 and organization_id='".$org_id."'");
                $this->setSequenceNo($document_id_arr);
            } else {
                $successType = 'custom_not_found';
                $message = 'Invalid type';
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
            }


            $successType = 'custom_found';
            $message = 'Execution completed successfully';
            $_status = 'custom_status_here';

            return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], true));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
            $_status = 'custom_status_here';
            Log::error($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    # set list enumeration as sequence_number if integer
    public function setSequenceNo($tax_arr)
    {
        $tax_arr_new = [];
        foreach($tax_arr as $doc_id_value) {
            $tax_arr_new[] = $doc_id_value->document_id;
        }
        $tax_arr_chunk = array_chunk($tax_arr_new,10);
        foreach($tax_arr_chunk as $chunk) {
            $item_child_asso = DB::select( "select `list_enumeration`, `item_association_id`
                                            from `acmt_item_associations`
                                            left join `acmt_items` on `acmt_item_associations`.`source_item_id` = `acmt_items`.`item_id`
                                            where `association_type` = 1
                                            and `source_document_id` in ('".implode("','",$chunk)."')
                                            and `source_document_id` = `target_document_id`
                                            and `acmt_items`.`is_deleted` = 0
                                            and `acmt_item_associations`.`is_deleted` = 0" );
            // test for multiple doc_id's in $chunk in implode ----- and `source_document_id` in ('".implode("','",$chunk)."')
            $insert_arr = [];
            foreach($item_child_asso as $asso) {
                #check if list enumeration is integer
                if($this->validatesAsInt($asso->list_enumeration)) {
                    $insert_arr[(int)$asso->list_enumeration][] = $asso->item_association_id;
                }
            }

            foreach($insert_arr as $seq_no => $asso_arr) {
                $statement = '';
                $arr_chunk = array_chunk($asso_arr, 1000);
                foreach($arr_chunk as $chunk) {
                    $statement .= " UPDATE acmt_item_associations SET sequence_number =".$seq_no." where item_association_id in ('".implode("','",$chunk)."'); ";
                }
                DB::unprepared($statement);
                unset($statement);
            }
        }
    }

    function validatesAsInt($number)
    {
        $number = filter_var($number, FILTER_VALIDATE_INT);
        return ($number !== FALSE);
    }
}
