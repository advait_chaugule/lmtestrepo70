<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ValidateDocumentV3Job;
use App\Domains\Taxonomy\Jobs\GetTaxonomyTreeStructureV2Job;
use App\Domains\Taxonomy\Jobs\GetSubscribedTaxonomyVersionJob;
use App\Domains\User\Jobs\GetUserByPermissionJob;

class GetTaxonomyHierarchyV5Feature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        try
        {
            $documentIdentifier = $request->route('document_id');
            $requestingUserDetails = $request->input("auth_user");
            if($request->input('is_customview')){
                $isCustomview = $request->input('is_customview');
            }else{
                $isCustomview = 0;
            }

            $requestingUserDetails['is_customview'] = $isCustomview;
            $organizationId = $requestingUserDetails["organization_id"];
            $userId = $requestingUserDetails["user_id"];

            $validationStatus = $this->run(new ValidateDocumentV3Job($documentIdentifier, $requestingUserDetails));

            if($validationStatus===true) 
            {
                $publishedVersion = $this->run(new GetSubscribedTaxonomyVersionJob($documentIdentifier)); // To check if taxonomy was published
                $checkPublishedPermission = false;
                if(!empty($publishedVersion)){
                    $checkPublishedPermission = $this->run(new GetUserByPermissionJob(['organization_id' => $organizationId, 'permission_name' => "View Published Taxonomies",'user_id' => $userId])); //To check has permission to view
                }
                $taxonomyStructure = $this->run(new GetTaxonomyTreeStructureV2Job($documentIdentifier, $organizationId, $userId,$checkPublishedPermission));
                $successType = 'custom_found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $taxonomyStructure, $message, $_status, [], true));
            }
            else
            {
                $errorType = 'validation_error';
                $message = $validationStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }


        }
        catch(\Exception $ex)
        {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
