<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

//use App\Domains\Comment\Jobs\ValidateItemThreadCommentJob;
use App\Domains\Thread\Jobs\ValidateThreadCommentJob;
use App\Domains\Thread\Jobs\DeleteCommentJob;
//use App\Domains\Comment\Jobs\UpdateItemThreadJob;
use App\Domains\Thread\Jobs\UpdateThreadJob;
//use App\Domains\Comment\Jobs\GetItemThreadCommentJob;
use App\Domains\Thread\Jobs\GetThreadCommentJob;
//use App\Domains\Comment\Jobs\GetItemThreadJob;
use App\Domains\Thread\Jobs\GetThreadJob;
//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;
use Log;


class DeleteCommentFeature extends Feature
{
    use DateHelpersTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {   
        try{
            $requestData    =   $request->all();
            $roleId         =   $requestData['auth_user']['role_id'];

            $threadCommentIdentifier        =   $request->route('thread_comment_id');
            $threadCommentDetail            =   $this->run(new GetThreadCommentJob($threadCommentIdentifier));
            $parentComment                  =   $this->run(new GetThreadJob($threadCommentDetail['thread_id'])); 
            
            // create the input data to validate
            $input = [ 'thread_comment_id' => $threadCommentIdentifier ];
            $validationStatus = $this->run(new ValidateThreadCommentJob($input));
        
            if($validationStatus===true) {  
                // soft delete comment
                $deletedComment = $this->run(new DeleteCommentJob($threadCommentIdentifier));
                
                //Update the main comment with updated at
                // $updateThreadData                   =   ['thread_id' => $deletedComment['thread_id'], 'is_deleted' => '1', 'updated_at'=> $this->createDateTime() ];
                // $this->run(new UpdateThreadJob($updateThreadData));

                //event for project and document update(updated_at) start
                event(new UpdateTaxonomyTimeEvent($threadCommentDetail['thread_source_id']));
                //event for project and document update(updated_at) end

                if($deletedComment['is_deleted'] === 1){
                    $response = $deletedComment;
                    $successType = 'found';
                    $message = 'Data deleted successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
            }
            else {
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
