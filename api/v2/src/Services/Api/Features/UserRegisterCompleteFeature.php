<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\User\Jobs\ValidateuserRegistrationInputJob;
use App\Domains\User\Jobs\UpdateUserRegisterJob;
use Log;

class UserRegisterCompleteFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $active_access_token = $request->route('active_access_token');
            $input = [ 'active_access_token' => $active_access_token ];            
            
            $requestData = $request->all();
            
            $validateInputs = $this->run(new ValidateuserRegistrationInputJob($requestData));
            
            if($validateInputs===true){
                $requestInputs = $requestData + $input;
                
                $user = $this->run(new UpdateUserRegisterJob($requestInputs));
                if($user!==false){
                    $response = $user;
                    $successType = 'updated';
                    $message = 'Data updated successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    $errorType = 'internal_error';
                    return $this->run(new RespondWithJsonErrorJob($errorType));
                }
            }
            else {
                $errors = $validateInputs;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
           
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
