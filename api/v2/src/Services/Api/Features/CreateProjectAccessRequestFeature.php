<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\CreateProjectRequestJob;
use App\Domains\Project\Jobs\ProjectAccessRequestViewersListJob;
use App\Domains\Project\Jobs\ProjectAccessRequestDetailsJob;
use App\Domains\Project\Jobs\SendRequestProjectAccessEmailNotificationJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Notifications\Jobs\GetProjectDetailByProjectId;
use App\Domains\Caching\Events\CachingEvent;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use Log;

class CreateProjectAccessRequestFeature extends Feature
{
    use UuidHelperTrait;

    public function handle(Request $request)
    {
       
        try{

            $permisionId = "fd3c70eb-b6a2-453d-9812-70331eeebe28";
            $projectId = $request->input('project_id');
            $workflowStageRoleIds = $request->input('workflow_stage_role_ids');
            $organizationId = $request['auth_user']['organization_id'];
            $status  = 1;
            $projectAccessRequestId = $this->createUniversalUniqueIdentifier();
            $userId = $request['auth_user']['user_id'];
            $updatedBy = $request['auth_user']['user_id'];
            $createdAt = now()->toDateTimeString();
            $identifier = ['project_id' => $projectId];
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);
            $requestData = [
                'project_access_request_id' => $projectAccessRequestId, 
                'organization_id' => $organizationId,
                'project_id' => $projectId,
                 'user_id' => $userId, 
                 'workflow_stage_role_ids' => $workflowStageRoleIds,
                 'status' => $status, 
                 'created_at' => $createdAt,
                 'updated_by' => $updatedBy
                ];
           
            $validateStatus = $this->run(new ValidateProjectIdJob($identifier));
            if($validateStatus === true){
                $checkIsProjectDeleted = $this->run(new CheckIsProjectDeletedJob($identifier));
                if(!$checkIsProjectDeleted === true) {
                    $projectRequestId = $this->run(new CreateProjectRequestJob($requestData));
                        //dd($projectRequestId);

                        $inputList = ['permission_id' => $permisionId, 'organization_id' => $organizationId];
    
                        $ProjectAccessRequestViewersList = $this->run(new ProjectAccessRequestViewersListJob($inputList));
                        $projectAccessRequestId = $projectRequestId['project_access_request_id'];
                        $inputProjectAccessList = ['project_access_request_id' => $projectAccessRequestId,
                        'organization_id' => $organizationId];  
                        $ProjectAccessRequestList = $this->run(new ProjectAccessRequestDetailsJob($inputProjectAccessList));
                     $projectName = $ProjectAccessRequestList[0]['project_name'];
                     $userName = $ProjectAccessRequestList[0]['user_name'];
                     $userEmail = $ProjectAccessRequestList[0]['user_email'];
                     $comment = $ProjectAccessRequestList[0]['user_email'];
                     $updatedBy = $ProjectAccessRequestList[0]['updated_by_name'];
                     $roleArr = [];
                      foreach ($ProjectAccessRequestList as $list) {
                          $roleArr = $list['workflow_stage_roles'];
                      }
                      $role = implode(',', $roleArr);
                    // $input['email'] = $userEmail;
                    // $input['comment'] = $comment;
                  //   $input['updatedBy'] = $updatedBy;
                    foreach($ProjectAccessRequestViewersList as $ViewersListdata){
                        $organizationData = $this->run(new GetOrgNameByOrgIdJob($organizationId));
                        $organizationName = $organizationData['organization_name'];
                        $organizationCode = $organizationData['organization_code'];
                        $senderEmail = $ViewersListdata->email;
                        $projectRequestUserId = $ViewersListdata->user_id;
                        $reviewerName = $ViewersListdata->first_name. " ".$ViewersListdata->last_name;
                        $input['projectName'] = $projectName;
                        $input['userName'] = $userName;
                        $input['email'] = $senderEmail;
                        $input['reviewerName'] = $reviewerName;    
                        $input['organization_name'] = $organizationName;
                        $input['organization_code'] = $organizationCode;
                        $input['domain_name'] = $serverName;
                        $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$projectRequestUserId,'organization_id'=>$organizationId]));
                        $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                        if ($emailSettingStatus==1) {
                            $this->run(new SendRequestProjectAccessEmailNotificationJob($input));
                            unset($input['organization_name']);
                            unset($input['organization_code']);
                        }
                    }
                    //In App Notification
                    $eventType            = config("event_activity")["Notifications"]["PROJECT_NEW_REQUEST_RECEIVED"];
                    $eventData = [
                        'project_id'         =>$projectId,
                        'event_type'         =>$eventType,
                        'user_name'          =>$userName,
                        'role'               =>$role,
                        'permission_id'      =>$permisionId,
                        "beforeEventRawData" => [],
                        "afterEventRawData" => '',
                        "requestUserDetails" => $request->input("auth_user")
                    ];
                    event(new NotificationsEvent($eventData));  //In App Notification ends
                           
                    $data = ['project_access_request_id' => $projectRequestId['project_access_request_id']];
                    $successType = 'success';
                    $message = 'Data created successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Project is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'not_found_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Project ID is invalid';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
          
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
