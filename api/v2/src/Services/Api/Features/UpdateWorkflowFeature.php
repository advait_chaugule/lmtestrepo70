<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Workflow\Jobs\UpdateWorkflowJob;
use Log;

class UpdateWorkflowFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            
            $workflowIdentifier =   $request->route('workflow_id');
            $requestData        =   $request->all();

            $identifier     =   ['workflow_id'  =>  $workflowIdentifier];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            
            $validateWorkflow    =   $this->run(new ValidateWorkflowIdJob($identifier));
            
            if($validateWorkflow === true) {
                $updatedWorkflow    =   $this->run(new UpdateWorkflowJob($workflowIdentifier, $organizationIdentifier, $requestData['workflow_stages']));
                
                $response = $updatedWorkflow;
                $successType = 'found';
                $message = 'Data updated.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            } else {
                $errorType = 'validation_error';
                $message = "Invalid workflow Id";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }          
           
        }
        catch(\Exception $ex){
            dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
