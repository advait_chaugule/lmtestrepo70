<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Log;
use App\Domains\Item\Jobs\CheckItemAssociationJob;
use App\Domains\Item\Jobs\CreateItemAssociationMetadataJob;
use App\Domains\Association\Jobs\GetAssociationMetadataNodetypeIdJob;
use Lucid\Foundation\Feature;

class CreateItemAssociationMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];
            $itemAssociationId          = $requestData['item_association_id'];
            $metadata                   = $requestData['metadata'];
            $associationTypeId          = $requestData['association_type_id'];
            $itemId                     = $requestData['item_id'];
            $response = [];

            $validateItemAssociation = $this->run(new CheckItemAssociationJob($itemAssociationId,$organizationId)); //Check if Item Association exists

            if($validateItemAssociation == false){
                $errorType = 'custom_validation_error';
                $_status = 'custom_status_here';
                $message = ['Invalid Item Association.'];
                return $this->run(new RespondWithJsonJob($errorType, null, $message , $_status,[], false));
            }else{
                $nodeTypeId = $this->run(new GetAssociationMetadataNodetypeIdJob($userId, $organizationId, $itemAssociationId, $associationTypeId,$itemId));
                $response = $this->run(new CreateItemAssociationMetadataJob($userId,$organizationId,$itemAssociationId,$metadata,$nodeTypeId));
                $successType = 'custom_created';
                $_status = 'custom_status_here';
                $message = ($response === true) ? ['Created successfully.'] : ['Not Created.'];
                $is_success = ($response === true) ? true : false;
                return $this->run(new RespondWithJsonJob($successType,[], $message, $_status,[], $is_success));
            }
            

        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}