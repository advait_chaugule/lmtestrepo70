<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\Document\Jobs\ValidateDocumentForATenantJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use App\Domains\Taxonomy\Jobs\GetProjectListAssociatedWithTaxonomyJob;
use Log;

class GetProjectStatusToPublishFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData        =   $request->all();
            $documentIdentifier =   $request->route('document_id');
            
            $validateDocument   =   $this->run(new ValidateDocumentForATenantJob($documentIdentifier, $requestData['auth_user']['organization_id']));

            if($validateDocument    === true) {
                $taxonomyDetail    =   $this->run(new GetDocumentByIdJob(['id' => $documentIdentifier]));
                
                // if($taxonomyDetail->adoption_status == 1) {
                    $projectList = $this->run(new GetProjectListAssociatedWithTaxonomyJob($documentIdentifier, $requestData['auth_user']['organization_id']));
                    if(count($projectList) > 0) {
                        $successType = 'found';
                        $message = 'Project List found';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $projectList, $message, $_status));
                    } else {
                        $successType = 'found';
                        $message = 'No Project List found';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $projectList, $message, $_status));
                    }
                // } else {
                //     $errorType = 'validation_error';
                //     $message = "This is not a draft taxonomy";
                //     $_status = '';
                //     return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                // }
            } else {
                $errorType = 'validation_error';
                $message = "This taxonomy does not belong to this organization";
                $_status = '';
                return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
            }
        } catch (\Exception $ex) {
            //dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
