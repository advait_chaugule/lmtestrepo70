<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Metadata\Jobs\ValidateCreateMetadataInputJob;
use App\Domains\Metadata\Jobs\ListMetadataJob;
use App\Domains\Metadata\Jobs\CreateMetadataJob;
use App\Domains\Metadata\Jobs\ValidateMetadataNameAndFieldJob;
use Log;

class CreateMetadataFeature extends Feature
{
    use UuidHelperTrait;
    public function handle(Request $request)
    {
        try{
            $requestData    =   $request->all();
            $listValues     =   !empty($requestData['field_possible_values']) ? implode(',', $requestData['field_possible_values']) : "";
            $inputData = ['name' => $requestData['name'],   'field_type' => $requestData['field_type'],    'organization_id' => $requestData['auth_user']['organization_id'],    'field_possible_values'   => $listValues, 'last_field_possible_values'   => $listValues];
            
            $validateMetadataFieldAndName = $this->run(new ValidateMetadataNameAndFieldJob($inputData,'create'));
            
            if($validateMetadataFieldAndName==0) 
            {
                    $validateMetadataData = $this->run(new ValidateCreateMetadataInputJob($inputData));
                
                    if($validateMetadataData===true) {
                        $metadataList = $this->run(new ListMetadataJob($request));
                        $sortedMetadata = array();
                        foreach ($metadataList['metadata'] as $key => $row)
                        {
                            $sortedMetadata[$key] = $row['order'];
                        }
                        
                        array_multisort($sortedMetadata, SORT_DESC, $metadataList['metadata']);

                        $metadataId = $this->createUniversalUniqueIdentifier();
                        $inputData['metadata_id']   =   $metadataId;
                        $inputData['order']         =   ++$metadataList['metadata'][0]['order'];
                        $inputData['is_custom']     =   '1';
                        $metadataCreated = $this->run(new CreateMetadataJob($inputData));
                        
                        $resultData = $metadataCreated; //$permissionSet;
                        
                        // to giving response for new metadata field type (dictionary)
                        if($requestData['field_type'] == 7){       
                            $reultarr = array();
                            $reultarr['name'] = $resultData->name;
                            $reultarr['organization_id'] = $resultData->organization_id;
                            $reultarr['field_possible_values'] = json_decode($resultData->field_possible_values);
                            $reultarr['last_field_possible_values'] = json_decode($resultData->last_field_possible_values);
                            $reultarr['metadata_id'] = $resultData->metadata_id;
                            $reultarr['order'] = $resultData->order;
                            $reultarr['is_custom'] = $resultData->is_custom;
                            $reultarr['updated_at'] = $resultData->updated_at;
                            $reultarr['created_at'] = $resultData->created_at;
                            $resultData = $reultarr;
                        }
                        
                        $successType = 'created';
                        $message = 'Metadata created successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status));
                    }
                    else{
                        $errorType = 'validation_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'Please provide valid inputs.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else
                {
                        $errorType = 'validation_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'Metadata with same name and field type already exists';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
