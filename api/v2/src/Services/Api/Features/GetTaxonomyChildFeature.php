<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Taxonomy\Jobs\GetChildTreeJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class GetTaxonomyChildFeature extends Feature
{
    public function handle(Request $request)
    {
        try {

            $requestData            =   $request->all();
            $nodeId                 =   $request->route('node_id');
            $currentPosition        =   $requestData['offset'];
            $limit                  =   $requestData['limit'];
            $type                   =   $requestData['is_document'];
            
           $requestData1["auth_user"]["organization_id"] = $requestData['auth_user']['organization_id']; 
            $requestData2["organization_id"] = $requestData['auth_user']['organization_id']; 
            $requestData2["currentPosition"] = $currentPosition; 
            $requestData2["limit"] = $limit; 
            $requestData2["type"] = $type; 
            $request->merge($requestData1);
            $taxonomyHierarchy          = $this->run(new GetChildTreeJob($nodeId, $requestData2));
            $successType = 'created';
            $message = 'Data Found';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
            

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
