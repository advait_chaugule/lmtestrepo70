<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\UpdateGroupForTaxonomyJob;
use App\Domains\Taxonomy\Jobs\ValidateGroupByIdAndNameJob;
use App\Domains\Taxonomy\Jobs\CheckGroupNameExistsJob;
use Illuminate\Support\Facades\Log;

class UpdateGroupForTaxonomyFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $groupName = (null!==$request->input('group_name')) ? trim($request->input('group_name')) : '';
            $groupId = (null!==$request->input('group_id')) ? trim($request->input('group_id')) : '';
            $isUserDefault = (null!==$request->input('is_user_default')) ? trim($request->input('is_user_default')) : '';
            $isDefault = (null!==$request->input('is_default')) ? trim($request->input('is_default')) : '';
            $requestData = $request->all();
            $userId = $requestData['auth_user']['user_id'];
            $organizationId = $requestData['auth_user']['organization_id'];
            $grpDetails = ['name' => $groupName,'group_id' => $groupId,'user_id' => $userId,'organization_id' => $organizationId,'is_default' => $isDefault,'is_user_default' => $isUserDefault];
            $validateGrptatus = $this->run(new ValidateGroupByIdAndNameJob($grpDetails)); //Check if group name exists
            //Save group name if it does not exists                
            if($validateGrptatus === true){
                if(!empty($isDefault) && $isDefault!="0" && $isDefault!="1"){
                    $errorType = 'custom_validation_error';
                    $rmessage = ['Invalid is_default value.'];
                }
                if(!empty($groupName)){
                    $validateGrpIfExists = $this->run(new CheckGroupNameExistsJob($groupName,$groupId,$organizationId)); //Check if group name exists
                    
                    if($validateGrpIfExists==true){
                        $errorType = 'custom_validation_error';
                        $rmessage = ['Group name already exists.'];
                    }
                }
                if(isset($errorType))              
                    return $this->run(new RespondWithJsonJob($errorType, null, $rmessage, '', [], false));
                
                $response = $this->run(new UpdateGroupForTaxonomyJob($grpDetails));
                $successType = 'custom_found';
                $rmessage = ['Group updated successfully.'];
                return $this->run(new RespondWithJsonJob($successType, $response, $rmessage, '', [], true));
            }else{
                $errorType = 'custom_validation_error';
                return $this->run(new RespondWithJsonJob($errorType, null, $validateGrptatus, '', [], false));
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
