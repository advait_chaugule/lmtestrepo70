<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\User\Jobs\ValidateResetTokenJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\User\Jobs\GetEmailByTokenIdJob;
use Log;

class ResetRequestFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $resultResetToken = $this->run(new ValidateResetTokenJob(['reset_request_token' => $request->route('reset_request_token')]));
            if ($resultResetToken == 0) {
                // invalid token
                $successType = 'found';
                $message = 'Please contact administrator.';
                $_status = 'custom_status_here';
                $data = ['result' => 'invalid'];
            } else if ($resultResetToken == 1) {
                // valid token
                $getEmail = $this->run(new GetEmailByTokenIdJob(['reset_request_token' => $request->route('reset_request_token')]));
                $successType = 'found';
                $message = 'Token is Valid';
                $_status = 'custom_status_here';
                $data = ['result' => 'valid','email'=>$getEmail->email];
            } else if ($resultResetToken == 2) {
                // token time expired
                $successType = 'found';
                $message = 'Token has expired';
                $_status = 'custom_status_here';
                $data = ['result' => 'expired'];
            }
            return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
