<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use Artisan;

class RemoveLaterFeature extends Feature
{
    public function handle(Request $request)
    {
        Artisan::call('event-tracking:process-queue');
        echo "Processing of Queue completed.";
    }
}
