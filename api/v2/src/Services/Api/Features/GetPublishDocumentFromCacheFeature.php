<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ListPublishedDocumentRootJob;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeJob;
use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;
use App\Domains\Asset\Jobs\GetAssetListJob;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV3Job;
class GetPublishDocumentFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try {

            $requestData            =   $request->all();
            $documentIdentifier     =   $request->route('document_id');
            $taxonomySetType        =   $requestData['type'];
            $organizationIdentifier =   $requestData['organization_id'];
            $requestUrl = url('/');
            $keyInfo = array('identifier'=>$documentIdentifier, 'prefix'=>'doc_'.$taxonomySetType);

            $treeFile = '';                                                             //initializing variable
            if($taxonomySetType == "hierarchy"){
                $treeFile ='treehierarchy_json_file_s3';                                //set $treefile = $taxonomySetType

            }elseif($taxonomySetType == "details"){
                $treeFile ='treedetails_json_file_s3';                                  //set $treefile = $taxonomySetType
            }

            $publshStatus = DB::table('documents')                                      //query to check if document is published
                            ->select('status')
                            ->where('document_id',$documentIdentifier)
                            ->where('organization_id',$organizationIdentifier)
                            ->first();

            if($publshStatus->status == 3){
                $status_data = DB::table('taxonomy_pubstate_history')                       //query to get file name from DB       
                            ->select($treeFile)
                            ->where('document_id',$documentIdentifier)
                            ->where('organization_id',$organizationIdentifier)
                            ->orderby('publish_number','desc')
                            ->first();
            }else{
                $message = 'Data not found.';
                $successType = 'custom_validation_error';
                return $this->run(new RespondWithJsonJob($successType,null,$message,'',[], false));
            }
            
            if(isset($status_data->$treeFile) && !empty($status_data->$treeFile)){
                    
                $s3FileName     = $status_data->$treeFile;                                   // set file name            
                $s3Bucket       =  config("event_activity")["HIERARCHY_FOLDER"];             // event activity
                $destinationS3  = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/$s3FileName";          // s3 destination              
               
                return Storage::disk('s3')->get($destinationS3);                //return taxonomy data from S3
           
            }else{
                $status_data = $this->run(new GetFromCacheJob($keyInfo));
        
                if(!empty($status_data['status']) && $status_data['status']=="error"){
                    // $this->run(new DeleteFromCacheJob($keyInfo));
                    $requestData1["auth_user"]["organization_id"] = $organizationIdentifier; 
                    $requestData2["organization_id"] = $organizationIdentifier; 
                    $request->merge($requestData1);
                    $taxonomyHierarchy          = $this->run(new GetFlattenedTaxonomyTreeV3Job($documentIdentifier, $requestData2));
                    
                    if($taxonomySetType == "hierarchy")
                    {
                        $dataToSend1 = $taxonomyHierarchy;
                        $taxonomySetType1 = "hierarchy";
                        $keyInfo1 = array('identifier'=>$documentIdentifier, 'prefix'=>'doc_'.$taxonomySetType1);
                        // $status1 = $this->run(new SetToCacheJob($dataToSend1, $keyInfo1));
                        $message = 'Data found.';
                        $successType = 'found';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $dataToSend1, $message, $_status));
                    }
                    else
                    {
                        // Set Details of Taxonomy In cache
                        foreach($taxonomyHierarchy['nodes'] as $node) {

                            $itemId = $node['id'];
                            $isDocument = $node['is_document'];
                            if($isDocument == 1)
                            {
                                $validateDocument   =   $this->run(new ValidateDocumentByIdJob(['document_id'   =>  $itemId]));  
                                if($validateDocument===true){
                                    $validateDocumentIsDeleted  =   $this->run(new CheckDocumentIsDeleteJob($itemId));    
                                    if(!$validateDocumentIsDeleted) {
                                        $documentDetails = $this->run(new GetDocumentAndRespectiveMetadataValuesJob($itemId));

                                        $documentAssets = $this->run(new GetAssetListJob($itemId, $organizationIdentifier,$requestUrl));
                                        $documentDetails["assets"] = $documentAssets;
                                        if(isset($documentDetails['display_text'])){

                                            $languageWithCode = $documentDetails['display_text'];       // get language with its language code
                                            $language = explode(' (',$languageWithCode);                // explode and get only the language
                                            $documentDetails['language_name'] = $language[0];           // set language in 'langauge_name' for Table view(UF-1820)
                                        }
                                        $taxonomyHierarchy['details'][] = $documentDetails;
                                        unset($documentDetails);
                                    }
                                }
                            }
                            else
                            {
                                $validationStatus = $this->run(new ValidateItemByIdJob([ 'item_id' => $itemId ]));

                                if($validationStatus===true){
                
                                    $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemId));
                                    
                                    if(!$itemIsDeleted) {
                                        
                                        $response = $this->run(new GetCFItemDetailsJob($itemId, $organizationIdentifier));
                                        $exemplarAssociationList = $this->run(new GetExemplarAssociationsJob($itemId, ['organization_id' =>$organizationIdentifier],$requestUrl));
                                        // embed the exemplar association collection inside the current response body
                                        $response["exemplar_associations"] = $exemplarAssociationList;

                                        // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                                        $itemAssets = $this->run(new GetAssetListJob($itemId, $organizationIdentifier,$requestUrl));
                                        $response["assets"] = $itemAssets;
                                        if(isset($response['display_text'])){

                                            $languageWithCode = $response['display_text'];              // get language with its language code
                                            $language = explode(' (',$languageWithCode);                // explode and get only the language
                                            $response['language_name'] = $language[0];                  // set language in 'langauge_name' for Table view(UF-1820)
                                        }
                                        $taxonomyHierarchy['details'][] = $response;
                                        unset($response);
                                    }
                                }
                            }
                        }

                        $dataToSend2['details'] = $taxonomyHierarchy['details']; 
                        $taxonomySetType2 = "details";
                        $keyInfo2 = array('identifier'=>$documentIdentifier, 'prefix'=>'doc_'.$taxonomySetType2);
                        //$status2 = $this->run(new SetToCacheJob($dataToSend2, $keyInfo2));

                        $message = 'Data found.';
                        $successType = 'found';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $dataToSend2, $message, $_status));
                    }
                
                        //$status3 = $this->run(new GetFromCacheJob($keyInfo));
                    
                }
                else
                {
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status_data, $message, $_status));
        
                } 
            }    
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
