<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Note\Jobs\ValidateNoteIdJob;
use App\Domains\Note\Jobs\CheckNoteIsDeleteJob;
use App\Domains\Note\Jobs\UpdateNoteJob;
use App\Domains\Note\Jobs\ValidateNoteInputJob;
use Log;

class UpdateNoteFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();

            $inputData = ['title' => !empty($requestData['title']) ? $requestData['title'] : "" ,
                'description' =>!empty($requestData['description']) ? $requestData['description'] : "",
                'organization_id' => $requestData['auth_user']['organization_id']];


            $validateNoteData = $this->run(new ValidateNoteInputJob($inputData));
            if ($validateNoteData === true) {
                $noteId = $request->route('note_id');
                $organizationId = $inputData['organization_id'];
                $noteIdentifier = ['note_id' => $noteId];
                $validateNoteStatus = $this->run(new ValidateNoteIdJob($noteIdentifier));

                if ($validateNoteStatus === true) {

                    $checkNoteIsDeleted = $this->run(new CheckNoteIsDeleteJob($noteId));
                    if (!$checkNoteIsDeleted === true) {

                        $updateNoteEntity = $this->run(new UpdateNoteJob($noteId, $requestData));
                        // caching
                        $eventType1              = config("event_activity")["Cache_Activity"]["NOTES_GETALL"];
                        $eventDetail = [
                            'organization_id'    => $organizationId,
                            'event_type'         => $eventType1,
                            'requestUserDetails' => $request->input("auth_user"),
                            "beforeEventRawData" => [],
                            'afterEventRawData'  => ''
                        ];
                        event(new CachingEvent($eventDetail)); // notification ends here
                        $response = $updateNoteEntity;
                        $successType = 'found';
                        $message = 'Note updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    } else {
                        $errorType = 'not_found';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'Note is already deleted';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                } else {

                    $errorType = 'validation_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Please select valid note.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
