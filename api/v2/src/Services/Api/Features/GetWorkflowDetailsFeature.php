<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Workflow\Jobs\GetWorkflowDetailsJob;
use Log;

class GetWorkflowDetailsFeature extends Feature
{
    public function handle(Request $request)
    {
        try {

            $workflowId = $request->route('workflow_id');
            $identifier = ['workflow_id' => $workflowId];
            $validateWorkflowStatus = $this->run(new ValidateWorkflowIdJob($identifier));
            $input = $request->all();
            $input['workflowId'] = $workflowId;
            
            if($validateWorkflowStatus === true) {
                $workflowlist = $this->run(new GetWorkflowDetailsJob($input));
                $response = $workflowlist;
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }else{
                $errorType = 'not_found';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Role is already deleted';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));                
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
