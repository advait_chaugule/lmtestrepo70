<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Association\Jobs\ValidateAssociationByIdJob;
use App\Domains\CaseAssociation\Jobs\DeleteExemplarAssociationWithExternalResourceJob;

use App\Domains\CaseAssociation\Jobs\GetOriginNodeIdJob;
//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;
use Log;



class DeleteExemplarAssociationFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {   
        try{
            $associationId = $request->route('association_id');
            $dataToValidate = [ "item_association_id" => $associationId ];
            $validationStatus = $this->run(new ValidateAssociationByIdJob($dataToValidate));
            if($validationStatus===true) {
                $requestUserDetails = $request->input("auth_user");

                //get origin node id from acmt_item_associations
                $originNodeId=$this->run(new GetOriginNodeIdJob($associationId));
                //job to update project and document table after comment is added
                if(!empty($originNodeId)){
                    //event for project and document update(updated_at) start
                    event(new UpdateTaxonomyTimeEvent($originNodeId));
                    //event for project and document update(updated_at) end
                }    
                // job to delete the exemplar association associated with either wih an exeternal url or external asset file
                $this->run(new DeleteExemplarAssociationWithExternalResourceJob($associationId, $requestUserDetails));

                $successType = 'deleted';
                $responseData = [];
                $message = 'Exemplar deleted successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $responseData, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
