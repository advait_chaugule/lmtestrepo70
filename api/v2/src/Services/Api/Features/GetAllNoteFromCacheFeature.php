<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Note\Jobs\GetAllNoteJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;

class GetAllNoteFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {
       // try {

            $requestData = $request->all();
            $noteId = $request->route('notes');
            $noteId = !empty($noteId) ? $noteId : "";
            $organizationId = $request['organization_id'];
            $input = ['note_id'         => $noteId,
                      'organization_id' => $organizationId];
            $keyInfo = array('identifier' => $organizationId, 'prefix' => 'notes_');
            $status = $this->run(new GetFromCacheJob($keyInfo));

            if (isset($status['status']) && $status['status'] == "error") {

                $request->merge($requestData);
                $noteList =$this->run(new GetAllNoteJob($input));

                //dd($noteList);
                if (!empty($noteList) && count($noteList) > 0) {
                    $status1 = $this->run(new DeleteFromCacheJob($keyInfo));
                    $status2 = $this->run(new SetToCacheJob($noteList, $keyInfo));
                    $status3 = $this->run(new GetFromCacheJob($keyInfo));
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status3, $message, $_status));
                }
                else
                {
                    $message = 'Data not found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
                }
            } else {
                $message = 'Data found.';
                $successType = 'found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $status, $message, $_status));
            }

        /*} catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }*/
    }
}




