<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ValidateDocumentV3Job;
use App\Domains\Taxonomy\Jobs\GetTaxonomyTreeStructureJob;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV3Job;

class GetTaxonomyHierarchyV4Feature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        try
        {
            $documentIdentifier = $request->route('document_id');
            $requestingUserDetails = $request->input("auth_user");
            if($request->input('is_customview')){
                $isCustomview = $request->input('is_customview');
            }else{
                $isCustomview = 0;
            }

            if($request->orphan == 0){
                $orphan = 0;
            }else{
                $orphan = 1;
            }
            $requestingUserDetails['is_customview'] = $isCustomview;

            $validationStatus = $this->run(new ValidateDocumentV3Job($documentIdentifier, $requestingUserDetails));

            if($validationStatus===true) 
            {
                    $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeV3Job($documentIdentifier, $requestingUserDetails));
                    $taxonomyStructure = $this->run(new GetTaxonomyTreeStructureJob($taxonomyHierarchy, $orphan));

                    $headers = [
                        'LastModifiedDate' => $taxonomyHierarchy['modified_at']
                    ];
                    unset($taxonomyHierarchy['modified_at']);
                    $successType = 'custom_found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $taxonomyStructure, $message, $_status, $headers, true));
            }
            else
            {
                    $errorType = 'validation_error';
                    $message = $validationStatus;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }


        }
        catch(\Exception $ex)
        {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
