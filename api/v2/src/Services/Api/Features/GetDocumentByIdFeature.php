<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Document\Jobs\ValidateDocumentJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;

use App\Domains\Asset\Jobs\GetAssetListJob;
use Log;

class GetDocumentByIdFeature extends Feature
{

    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $documentId = $request->route('id');
            $requestUrl = url('/');
            $validateDocument = $this->run(new ValidateDocumentJob($documentId));
            if($validateDocument===true){
                $documentDetails = $this->run(new GetDocumentAndRespectiveMetadataValuesJob($documentId));

                // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                $documentAssets = $this->run(new GetAssetListJob($documentId, $request->input("auth_user")["organization_id"],$requestUrl));
                $documentDetails["assets"] = $documentAssets;
                $documentDetails['language_name'] = $documentDetails['language'];
                unset($documentDetails['language']);
                // ACMT-795 - Create separate job to fetch asset list
                $successType = 'Document found';
                $message = 'Document found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $documentDetails, $message, $_status));
            }
            else {
                $errors = $validateDocument;
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($errors);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
