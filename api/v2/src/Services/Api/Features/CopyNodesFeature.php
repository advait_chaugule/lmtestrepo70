<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\Project\Jobs\CopyNodeJob;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Project\Jobs\GetChildNodeJob;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Project\Jobs\NodeExistProjectJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Events\CopyTaxonomyEvent;
use App\Domains\Project\Jobs\ValidateInputForCopyNodesJob;
use App\Domains\Taxonomy\Jobs\ValidateInputForCopyTaxonomyJob;

class CopyNodesFeature extends Feature
{
    use ArrayHelper, StringHelper, UuidHelperTrait, DateHelpersTrait, ErrorMessageHelper, CaseFrameworkTrait;

    public function handle(Request $request)
    {
        ini_set('memory_limit','8098M');
        ini_set('max_execution_time', 0);
        try{
            $input = $request->input();
            $requestUrl = url('/');
            #isCopy(Exact match)
            if(array_key_exists('isCopy',$input))
            {
                $iscopy = $request->input('isCopy');
            }
            else
            {
                $iscopy = 0;
            }
            #isCopyAsso(copy associations)
            if(array_key_exists('isCopyAsso',$input))
            {
                $isCopyAsso = $request->input('isCopyAsso');
            }
            else
            {
                $isCopyAsso = 0;
            }

            if(isset($input['project_type']) && $input['project_type'] == 1)
            {
                $validateRequest = $this->run(new ValidateInputForCopyNodesJob($input));
                if($validateRequest === true)
                {
                    $projectNodeArr                  =   [ 'project_id' => $input['project_id'],'item_id' => $input['origin_node_id']];
                    $nodeExist = $this->run(new NodeExistProjectJob($projectNodeArr)); 
                    if($nodeExist == 1)
                    {
                            $uniqueNodeIds = array_unique($input['node_ids_in_csv']);
                            $ChildNodeIds =  !empty($input['node_hierarchy'])? $input['node_hierarchy']:[]; 
                            $results =  $this->run(new CopyNodeJob($uniqueNodeIds,$ChildNodeIds,$input['origin_node_id'],$input['project_id'],$input['required_full_hierarchy_copy'],$input['document_id'],$iscopy,$isCopyAsso,$request->input('auth_user'),$requestUrl));
                            $successType = 'created';
                            $message = 'Nodes copied successfully.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $results, $message, $_status));
                    }
                    else
                    {
                        $errorType = 'validation_error';
                            $message = 'Origin Node Id is not Present in the Project';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else {
                    $errorType = 'validation_error';
                    $message = $this->messageArrayToString($validateRequest);
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else
            {
                #Copy Taxonomy code starts here
                $validateRequest = $this->run(new ValidateInputForCopyTaxonomyJob($input));
                if($validateRequest === true) {
                    $domain_name    = str_replace('server', '', url('/'));
                    $event_data = [
                        'document_id' => $input['documentId'],
                        'taxonomy_name' => $input['taxonomyName'],
                        'is_copy' => $iscopy, // for exact match
                        'is_copy_asso' => $isCopyAsso, // for other associations than isChild
                        'user' => $request->auth_user,
                        'domain_name' => $domain_name,
                        'requestUrl' => $requestUrl
                    ];
                    # copy Taxonomy event
                    event(new CopyTaxonomyEvent($event_data));

                    $successType = 'custom_created';
                    $message = ['Email notification will be sent when taxonomy is finished copying'];
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, null, $message, $_status, [], true));
                } else {
                    $errorType = 'custom_validation_error';
                    $message = $validateRequest;
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($errorType, null, $message, $_status, [], false));
                }
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
