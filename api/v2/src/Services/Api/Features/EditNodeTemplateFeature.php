<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeTemplate\Jobs\ValidateNodeTemplateByIdJob;
use App\Domains\NodeTemplate\Jobs\CheckIsNodeTemplateDeletedJob;
use App\Domains\NodeTemplate\Jobs\ValidateNodeTemplateInputJob;
use App\Domains\NodeTemplate\Jobs\EditNodeTemplateJob;
use App\Domains\NodeTemplate\Jobs\GetNodeTemplateObjectJob;
use App\Domains\NodeTemplate\Jobs\AssociateNodeTypeWithNodeTemplateJob;
use Log;

class EditNodeTemplateFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData                =   $request->all();
            $nodeTemplateIdentifier     =   $request->route('node_template_id');
            $nodeTemplateName           =   $requestData['name'];

            $validateNodeTemplate       =   $this->run(new ValidateNodeTemplateByIdJob(['node_template_id'    =>  $nodeTemplateIdentifier]));
            if($validateNodeTemplate === true) {
                $checkIsNodeTemplateDeleted   =   $this->run(new CheckIsNodeTemplateDeletedJob($nodeTemplateIdentifier));
                if(!$checkIsNodeTemplateDeleted === true) {
                    $inputData                  =   ['name' =>  $nodeTemplateName];

                    $validateTemplateName   =   $this->run(new ValidateNodeTemplateInputJob($inputData));
                    if($validateTemplateName    === true) {
                        $inputData['organization_id']       =   $requestData['auth_user']['organization_id'];
                        $inputData['updated_by']            =   $requestData['auth_user']['user_id'];

                        $updatedNodeTemplate                =   $this->run(new EditNodeTemplateJob($nodeTemplateIdentifier, $inputData));
                        //Detach the associated node types
                        $nodeTemplateObject                 =   $this->run(new GetNodeTemplateObjectJob($updatedNodeTemplate['node_template_id']));
                        $mappedNodeTypes    =   $nodeTemplateObject->nodeTypes;
                        
                        foreach($mappedNodeTypes as $nodeType) {
                            $nodeTypeIdentifier = $nodeType['node_type_id'];
                            $nodeTemplateObject->nodeTypes()->detach($nodeTypeIdentifier);                           
                        }

                        foreach($requestData['node_types'] as $nodeTypes) {
                            $attributes =   ['node_template_id' =>  $updatedNodeTemplate['node_template_id'], 'node_type_id'   =>  $nodeTypes['node_type_id'], 'parent_node_type_id' =>  !empty($nodeTypes['parent_node_type_id']) ? $nodeTypes['parent_node_type_id'] : '', 'sort_order'   =>  $nodeTypes['sort_order']];

                            $this->run(new AssociateNodeTypeWithNodeTemplateJob($attributes));
                        }

                        $response       = $updatedNodeTemplate;
                        $successType    = 'created';
                        $message        = 'Node template is updated successfully.';
                        $_status        = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else {
                        $errorType = 'validation_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'Please provide template name';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                } else {
                    $errorType = 'validation_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'This Node Template is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select correct Node Template';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

            
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
