<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;

//use App\Domains\CaseAssociation\Jobs\ListCaseAssociationTypesJob;
use App\Services\Api\Traits\CaseFrameworkTrait;
use Log;

class ListCaseAssociationsFeature extends Feature
{
    use ErrorMessageHelper;
    use CaseFrameworkTrait;
    public function handle(Request $request)
    {
        try{
            $list = $this->getAllSystemSpecifiedAssociationType();//$this->run(ListCaseAssociationTypesJob::class);
            if(!empty($list)) {
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $list, $message, $_status));
            }
            else {
                $errorType = 'not_found';
                $message = 'Data not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
