<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Domains\Caching\Events\CachingEvent;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\CaseCaching\Events\CFItemEvent;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\CaseCaching\Events\ConceptEvent;
use App\Domains\CaseCaching\Events\ItemTypeEvent;
use App\Domains\Document\Jobs\PublishDocumentJob;
use App\Domains\User\Jobs\GetUserByPermissionJob;
use App\Domains\CaseCaching\Events\CFSubjectEvent;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\CaseCaching\Events\CaseCachingEvent;
use App\Domains\CaseCaching\Events\CFDocumentsEvent;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\CaseCaching\Events\CFAssociationsEvent;
use App\Domains\CaseCaching\Events\CFDocumentsAllEvent;
use App\Domains\CaseCaching\Events\CustomMetadataEvent;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Notifications\Jobs\GetTaxonomyNameById;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Document\Jobs\StorePublishDataHistoryJob;
use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\CaseCaching\Events\UnPublishTaxonomyEvent;
use App\Domains\CaseCaching\Events\CFItemAssociationsEvent;
use App\Domains\Document\Jobs\ValidateDocumentByDetailsJob;
use App\Domains\Notifications\Jobs\NotificationJobForEmail;
use App\Domains\Document\Jobs\ValidateDocumentForATenantJob;
use App\Domains\Document\Jobs\ValidateDocumentForPublishJob;
use App\Domains\Document\Jobs\ValidateDocumentForUnpublishJob;
use App\Domains\Document\Jobs\CheckTagJob;

class PublishDocumentFeature extends Feature
{
    use UuidHelperTrait;
    public function handle(Request $request)
    {
        try {
            $uuid                   = $this->createUniversalUniqueIdentifier();
            $requestData            =   $request->all();
            $documentIdentifier     =   $requestData['document_id'];
            $adoptionStatus         =   $requestData['adoption_status'];
            $publishStatus          =   isset($requestData['publish_status'])?$requestData['publish_status']:1; // 1= publish 2 = unpublish
            $requestingUserDetail   =   $requestData['auth_user'];
            $userId                 =   $requestingUserDetail['user_id'];
            $organizationId         =   $requestData['auth_user']['organization_id'];
            $requestUrl             = url('/');
            $serverName             = str_replace('server', '', $requestUrl);
            $versionTag             =   isset($requestData['version_tag'])?$requestData['version_tag']:'';        //tag to store version, this field is mandatory and cannot be duplicate
            /**
             * Flag is used to identify the document
             * 1 = not exist / blank
             * 2 = not in current tenent
             * 3 = deleted
             * 4 = already published
             * 5 = already unpublished
             * 6 = ok for action
             */
            $DocumentValidity = $this->run(new ValidateDocumentByDetailsJob(['document_id' => $documentIdentifier,
                                                                             'organization_id' => $requestingUserDetail['organization_id'],
                                                                              'user_id' => $userId, 
                                                                              'adoption_status' => $adoptionStatus, 
                                                                              'publish_status' => $publishStatus]));
            if($DocumentValidity == 1)
            {
                $errorType = 'validation_error';
                $message = "Please select a valid taxonomy.";
                $_status = 'Invalid Taxonomy.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }
            else if($DocumentValidity == 2)
            {
                $errorType = 'validation_error';
                $message = "This taxonomy does not belong to this organization.";
                $_status = '';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            else if($DocumentValidity == 3)
            {
                $errorType = 'validation_error';
                $message = "This taxonomy is deleted.";
                $_status = '';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            else if($DocumentValidity == 4)
            {
                $errorType = 'validation_error';
                $message = "This taxonomy is already published, you can not publish this taxonomy again.";
                $_status = '';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            else if($DocumentValidity == 5)
            {
                $errorType = 'validation_error';
                $message = "This taxonomy is already unpublished, you can not unpublish this taxonomy again.";
                $_status = '';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            else 
            {
                if($publishStatus == 2)
                {
                    // below code deletes the data from cache
                    $unPublishEventType               = config("event_activity")["Cache_Activity"]["UNPUBLISHTAXO_CASE"];
                    $unPublishEventData = [
                        'event_type'         => $unPublishEventType,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        'domain_name'        => $requestUrl,
                        'document_id'        => $documentIdentifier,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new UnPublishTaxonomyEvent($unPublishEventData));

                    //In App Notification when user unpublish taxonomy
                    $eventType               = config("event_activity")["Notifications"]["TAXONOMY_UNPUBLISHED"]; // 16
                    $eventData = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $eventType,
                        'requestUserDetails' => $requestingUserDetail,
                        'domain_name'        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new NotificationsEvent($eventData)); // notification ends here
                    $this->run(new StorePublishDataHistoryJob($documentIdentifier,$userId,$organizationId,$publishStatus,$uuid,$versionTag));
                    $this->run(new PublishDocumentJob($documentIdentifier,$userId,$adoptionStatus,$publishStatus));                    
                    $response       = [];
                    $successType    = 'found';
                    $message        = 'Taxonomy is unpublished successfully.';
                    $_status        = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else 
                {
                    /* if(empty($versionTag)){             //validation to check if tag is empty                    
                        $errorType    = 'custom_not_found';
                        $message        = 'Version cannot be empty.';
                        return $this->run(new RespondWithJsonErrorJob($errorType,$message,'',[],false));
                    }*/
                    if(!empty($versionTag)){
                        $checkTag = $this->run(new CheckTagJob($documentIdentifier,$organizationId,$versionTag));  // job to get tag if already exists
                        if(isset($checkTag) && $checkTag->tag==$versionTag){        //validation to check if tag is duplicate
                            $errorType    = 'custom_not_found';
                            $message        = 'Version cannot be duplicate.';
                            return $this->run(new RespondWithJsonErrorJob($errorType,$message,'',[],false));
                        }
                    }
                    $this->run(new StorePublishDataHistoryJob($documentIdentifier,$userId,$organizationId,$publishStatus,$uuid,$versionTag));
                    // Case API for CFPackage
                    $caseEventType               = config("event_activity")["Cache_Activity"]["CFPACKAGE_CASE"];
                    $caseEventData = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $caseEventType,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        'uuid'               => $uuid,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => '',
                        'versionTag'        => $versionTag
                    ];
                    event(new CaseCachingEvent($caseEventData)); // CFPackage Case API Ends here

                    // Case API for CFDocumentsAll
                    $caseEventCFDocAll   = config("event_activity")["Cache_Activity"]["CFDocumentsAll_CASE"];
                    $caseEventDataCFDocAll = [
                        'event_type'         => $caseEventCFDocAll,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CFDocumentsAllEvent($caseEventDataCFDocAll)); // Case API for CFDocumentAll Ends here

                    // Case API for CFDocument
                    $caseEventCFDocument   = config("event_activity")["Cache_Activity"]["CFDocument_CASE"];
                    $caseEventDataCFDocument = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $caseEventCFDocument,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CFDocumentsEvent($caseEventDataCFDocument));// Case API for CFDocument

                    // CFItem Event
                    /**
                     * below code is commented as per the new cache approach suggested by Anil start
                     */
                    /*
                    $cfItemEvent   = config("event_activity")["Cache_Activity"]["CFItem_CASE"];
                    $cfItemData = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $cfItemEvent,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CFItemEvent($cfItemData));
                    */
                    // Case API for CFItem
                    /**
                     * below code is commented as per the new cache approach suggested by Anil end
                     */

                    // Custom metadata event
                    $customEvent   = config("event_activity")["Cache_Activity"]["CUSTOMDATA_CASE"];
                    $customData = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $customEvent,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => '',
                        'uuid'               => $uuid
                    ];
                    event(new CustomMetadataEvent($customData));// Custom metadata

                    // Caching Event
                    $eventType1               = config("event_activity")["Cache_Activity"]["PUBLISH_TAXONOMY"]; // 11
                    $eventData1 = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $eventType1,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CachingEvent($eventData1)); // caching events ends here

                    //CFConcept Event
                    $eventConcept =  config('event_activity')['Cache_Activity']['CFCONCEPT_CASE'];
                    $conceptEvent = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $eventConcept,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new ConceptEvent($conceptEvent));

                    //Item Type Event
                    $eventItemType =  config('event_activity')['Cache_Activity']['CFITEMTYPE_CASE'];
                    $conceptEvent = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $eventItemType,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new ItemTypeEvent($conceptEvent));

                    // Subject Event
                    $eventSubjectType =  config('event_activity')['Cache_Activity']['CFSUBJECT_CASE'];
                    $subjectEvent = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $eventSubjectType,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CFSubjectEvent($subjectEvent));

                    // Association Event
                    /**
                     * below code is commented as per the new cache approach suggested by Anil start
                     */
                    /*
                    $eventAssociation = config('event_activity')['Cache_Activity']['CFASSOCIATION_CASE'];
                    $associationEvent = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $eventAssociation,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CFAssociationsEvent($associationEvent));
                    */
                    /**
                     * below code is commented as per the new cache approach suggested by Anil end
                     */

                    // Item Association Event
                    $itemAssociationEvent = config('event_activity')['Cache_Activity']['CFITEMASSOCIATION_CASE'];
                    $itemAssociation = [
                        'document_id'        => $documentIdentifier,
                        'event_type'         => $itemAssociationEvent,
                        'requestUserDetails' => $requestingUserDetail,
                        "organizationId"     => $organizationId,
                        "domain_name"        => $requestUrl,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];
                    event(new CFItemAssociationsEvent($itemAssociation));                    

                    // Node Type Caching Event added for New Nodes Type imported by JSON/CSV to be updated in Cache - UF-1630
                    $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                    $eventData = [
                        "event_type"         => $eventType,
                        "organizationId"     => $organizationId,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];

                    event(new CachingEvent($eventData)); 
                    // Node Type Caching ends here

                    // Node Type Caching Event added for New Nodes Type imported by JSON/CSV to be updated in Cache - UF-1630
                    $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                    $eventData = [
                        "event_type"         => $eventType,
                        "organizationId"     => $organizationId,
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];

                    event(new CachingEvent($eventData)); 
                    // Node Type Caching ends here

                    $this->run(new PublishDocumentJob($documentIdentifier,$userId,$adoptionStatus,$publishStatus));
                    // sent out the response
                    $response       = [];
                    $successType    = 'found';
                    $message        = 'The publish process is currently in progress . We will send you an email alert once the process completes';
                    $_status        = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $documentId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_document"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
