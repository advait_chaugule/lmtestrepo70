<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Helper\Jobs\RemoveSpecifiedElementsFromArrayJob;

use App\Domains\Item\Jobs\ValidateAndReturnCfItemJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\UpdateCFItemJob;

use App\Domains\CFEntityType\Jobs\UpdateInputWithCFEntityTypeJob;

use App\Domains\Concept\Jobs\ValidateConceptInputJob;
use App\Domains\Concept\Jobs\CreateConceptJob;
use App\Domains\Concept\Jobs\UpdateConceptJob;

use App\Domains\License\Jobs\ValidateLicenseInputJob;
use App\Domains\License\Jobs\CreateLicenseJob;
use App\Domains\License\Jobs\UpdateLicenseJob;

use App\Services\Api\Traits\ErrorMessageHelper;

use App\Data\Models\Organization;

use App\Domains\Item\Events\UpdateItemInsideProjectEvent;
use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;

use App\Domains\Item\Jobs\CheckItemHasProjectJob;
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use Log;

class EditCFItemFeature extends Feature
{

    use ErrorMessageHelper, UuidHelperTrait, CaseFrameworkTrait;

    public function handle(Request $request)
    {
        try{
            $itemId = $request->route('item_id');
            $requestUrl = url('/');
            $input = [ 'item_id' => $itemId ];
            $item = $this->run(new ValidateAndReturnCfItemJob($input));
            $userID = $request->input('auth_user');
            $orgCode = Organization::select('org_code')->where('organization_id', $userID['organization_id'])->first()->org_code;

            if($item!==false) {
                //check if item to be updated is already deleted
                $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemId));
                
                if(!$itemIsDeleted) {
                    $requestData = $request->all();
                    if(array_key_exists('language_name',$requestData))
                    {
                        $requestData['language'] = $requestData['language_name'];
                    }
                    
                    // process concept data
                    $createOrUpdateConceptData = $this->createOrUpdateConceptData($requestData, $item->concept_id, $orgCode,$requestUrl);
                    if($createOrUpdateConceptData != false){
                        $requestData =   $createOrUpdateConceptData;
                    }
                    // }
                    // else{
                    //     $errors = [['concept_title' => 'Title cannot be blank']];
                    //     $errorType = 'validation_error';
                    //     $message = $this->run(new MessageArrayToStringHelperJob($errors));
                    //     $_status = 'custom_status_here';
                    //     return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                    // }

                    // process license data
                    $createOrUpdateLicenseData = $this->createOrUpdateLicenseData($requestData, $item->license_id, $orgCode,$requestUrl);
                    if($createOrUpdateLicenseData != false){
                        $requestData =   $createOrUpdateLicenseData;
                    }
                    
                    $updatedBy = $request->input("auth_user")["user_id"];
                    $input['updated_by'] = $updatedBy;
                    // create cfitem data to update
                    $requestInputs = $requestData + $input;
                    
                    $updatedItem = $this->run(new UpdateCFItemJob($requestInputs,$requestUrl));
                    if($updatedItem!==false){
                        // raise event to track activities for reporting
                        if(!empty($request->input('project_id'))) {
                            $beforeEventRawData = $item->toArray();
                            $afterEventRawData = $updatedItem->toArray();
                            $afterEventRawData['project_id'] = $request->input('project_id');
                            $eventData = [
                                "beforeEventRawData" => (object) $beforeEventRawData,
                                "afterEventRawData" => (object) $afterEventRawData,
                                "requestUserDetails" => $request->input("auth_user")
                            ];
                            event(new UpdateItemInsideProjectEvent($eventData));
                        }
                        else
                        {
                            $itemHasProject = $this->run(new CheckItemHasProjectJob($input));
                            if(count($itemHasProject) > 0)
                            {
                                
                                $beforeEventRawData = $item->toArray();
                                $afterEventRawData = $updatedItem->toArray();
                                $afterEventRawData['project_id'] = $itemHasProject[0]['project_id'];
                                 $eventData = [
                                "beforeEventRawData" => (object) $beforeEventRawData,
                                "afterEventRawData" => (object) $afterEventRawData,
                                "requestUserDetails" => $request->input("auth_user")
                                 ];
                                event(new UpdateItemInsideProjectEvent($eventData));
                            }
                        }

                        //event for project and document update(updated_at) start
                        event(new UpdateTaxonomyTimeEvent($itemId));
                        //event for project and document update(updated_at) end

                        $response = $updatedItem;
                        $successType = 'updated';
                        $message = 'Data updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else{
                        $errorType = 'internal_error';
                        return $this->run(new RespondWithJsonErrorJob($errorType));
                    }
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Item is deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errors = [['item_id' => 'Invalid id provided.']];
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function createOrUpdateConceptData(array $input, $conceptId = null, string $orgCode,$requestUrl) {
        // validate concept master data
        //$validateConceptInput = $this->run(new ValidateConceptInputJob($input));

        $conceptTitle           = !empty($input['concept_title']) ? $input['concept_title'] : "";
        $conceptKeyword         = !empty($input['concept_keywords']) ? $input['concept_keywords'] : "";
        $conceptHierarchyCode   = !empty($input['concept_hierarchy_code']) ? $input['concept_hierarchy_code'] : "";
        $conceptDescription     = !empty($input['concept_description']) ? $input['concept_description'] : "";
        $conceptTitleHtml       = !empty($input['concept_title_html']) ? $input['concept_title_html'] : "";
        $conceptDescriptionHtml = !empty($input['concept_description_html']) ? $input['concept_description_html'] : "";
        $conceptOrganization    = $input['auth_user']['organization_id'];

        if(!empty($conceptId)){
            $conceptURI   = $this->getCaseApiUri("CFConcepts", $conceptId, true, $orgCode,$requestUrl);
            // create concept master record
            $conceptData = ['concept_id'=> $conceptId, 'keywords' => $conceptKeyword, 'hierarchy_code'  =>  $conceptHierarchyCode, 'title'  =>  $conceptTitle, 'description'    =>  $conceptDescription, 'organization_id'  =>  $conceptOrganization, 'uri' => $conceptURI,'title_html' => $conceptTitleHtml,'description_html' =>$conceptDescriptionHtml];
            $tempConcept =  $conceptData;
            // update job for concept
            $concept = $this->run(new UpdateConceptJob($conceptData));
        }
        else {
            // create the uuid
            $conceptIdentifier = $this->createUniversalUniqueIdentifier();
            $conceptURI   = $this->getCaseApiUri("CFConcepts", $conceptIdentifier,true, $orgCode,$requestUrl);
            // create concept master record
            $conceptData = ['concept_id'=> $conceptIdentifier, 'keywords' => $conceptKeyword, 'hierarchy_code'  =>  $conceptHierarchyCode, 'title'  =>  $conceptTitle, 'description'    =>  $conceptDescription, 'source_concept_id' => $conceptIdentifier, 'organization_id'  =>  $conceptOrganization, 'uri' => $conceptURI,'title_html' => $conceptTitleHtml,'description_html' =>$conceptDescriptionHtml];
            // create concept
            $concept = $this->run(new CreateConceptJob($conceptData));
        }

        // add concept_id to input
        $input['concept_id'] = $concept->concept_id;
        $input['title_html'] = $concept->title_html;
        $input['description_html']  = $concept->description_html;
        $input['source_concept_uri_object'] = json_encode(
            [
                "identifier" => $concept->concept_id,
                "uri"        => $conceptURI,
                "title"      => !empty($conceptTitleHtml)? $conceptTitleHtml :$concept->title
            ]
            );
        return $input;

        /* if($validateConceptInput===true){
            
        }
        else{
            return false;
        } */
    }

    public function createOrUpdateLicenseData(array $input, $licenseId = null, string $orgCode,$requestUrl) {
        // validate license master data
        //$validateLicenseInput = $this->run(new ValidateLicenseInputJob($input));
        $licenseTitle           = !empty($input['license_title']) ? $input['license_title'] : "";
        $licenseDescription     = !empty($input['license_description']) ? $input['license_description'] : "";
        $licenseText            = !empty($input['license_text']) ? $input['license_text'] : "";
        $licenseTextHtml        = !empty($input['license_text_html']) ? $input['license_text_html'] : "";
        $licenseTitleHtml       = !empty($input['license_title_html']) ? $input['license_title_html'] : "";
        $licenseDescriptionHtml = !empty($input['license_description_html']) ? $input['license_description_html'] : "";
        $licenseOrganization    = $input['auth_user']['organization_id'];
        
        if(!empty($licenseId)){
            // update license master record
            $licenseURI             = $this->getCaseApiUri("CFLicenses", $licenseId, true,$orgCode,$requestUrl);

            $licenseData = ['license_id'=> $licenseId, 'title' => $licenseTitle, 'description'  =>  $licenseDescription, 'license_text'  =>  $licenseText,'license_text_html'  =>  $licenseTextHtml, 'organization_id'    =>  $licenseOrganization, 'uri' => $licenseURI,'title_html' => $licenseTitleHtml, 'description_html' => $licenseDescriptionHtml];
            // update job for license
            $license = $this->run(new UpdateLicenseJob($licenseData));
        }
        else {
            // create the uuid
            $licenseIdentifier      = $this->createUniversalUniqueIdentifier();
            $licenseURI             = $this->getCaseApiUri("CFLicenses", $licenseIdentifier, true, $orgCode,$requestUrl);
            // create license master record
            $licenseData = ['license_id'=> $licenseIdentifier, 'title' => $licenseTitle, 'description'  =>  $licenseDescription, 'license_text'  =>  $licenseText, 'license_text_html'  =>  $licenseTextHtml,'source_license_id' => $licenseIdentifier, 'organization_id'    =>  $licenseOrganization, 'uri' => $licenseURI,'title_html' => $licenseTitleHtml, 'description_html' => $licenseDescriptionHtml];
            // create license
            $license = $this->run(new CreateLicenseJob($licenseData));
        }
        // add license_id to input
        $input['license_id'] = $license->license_id;
        $input['title_html'] = $license->license_title_html;
        $input['description_html'] = $license->license_description_html;
        $input['license_text_html'] = $license->license_text_html;
        $input['source_license_uri_object'] = json_encode(
            [
                "identifier" => $license->license_id,
                "uri"        => $licenseURI,
                "title"      => !empty($licenseTitleHtml)? $licenseTitleHtml : $license->title
            ]
            );
        return $input;

        /* if($validateLicenseInput===true){
            
        }
        else{
            return false;
        } */
    }
}
