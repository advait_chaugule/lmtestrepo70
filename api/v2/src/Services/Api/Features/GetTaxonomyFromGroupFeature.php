<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\GetTaxonomyFromGroupJob;
use App\Domains\Taxonomy\Jobs\ValidateGroupByIdJob;
use Illuminate\Support\Facades\Log;

class GetTaxonomyFromGroupFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $groupId = (null!==$request->input('group_id')) ? $request->input('group_id') : '';
            $requestData = $request->all();
            $userId = $requestData['auth_user']['user_id'];
            $organizationId = $requestData['auth_user']['organization_id'];           
            $grpDetails = ['group_id' => $groupId];
            $validateGrptatus = $this->run(new ValidateGroupByIdJob($grpDetails)); //Check if group id exists 
            //Check if group id exists or if no group id send group id as blank and get all groups                
            if(!isset($validateGrptatus) || $validateGrptatus === true){
                $grpDetail = $this->run(new GetTaxonomyFromGroupJob($groupId,$organizationId,$userId));
                $successType = 'custom_found';
                $rmessage = ['Data found.'];
                return $this->run(new RespondWithJsonJob($successType, $grpDetail, $rmessage, '', [], true));
            }else{
                $errorType = 'custom_validation_error';
                return $this->run(new RespondWithJsonJob($errorType, null, $validateGrptatus, '', [], false));
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
