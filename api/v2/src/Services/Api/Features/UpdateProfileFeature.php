<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use Log;

use App\Domains\User\Jobs\UpdateProfileJob;

class UpdateProfileFeature extends Feature
{   

    public function handle(Request $request)
    {
        try{
            $requestUserData                       = $request->all();
            $userId                                = $requestUserData['auth_user']['user_id'];
            $firstName                             = $requestUserData['first_name'];
            $lastName                              = $requestUserData['last_name'];
            $reg_user_type                         = $requestUserData['reg_user_type'];
            $teacher_grade_level_taught            = $requestUserData['reg_teacher_grades']; 
            $teacher_subject_area_taught           = $requestUserData['reg_teacher_subjects'];
            $admin_district_language_taught        = $requestUserData['reg_admin_languages'];
            $admin_district_other_language_taught  = $requestUserData['reg_admin_other_languages'];

            $response    = $this->run(new UpdateProfileJob($userId,$firstName,$lastName,$reg_user_type,$teacher_grade_level_taught,$teacher_subject_area_taught,$admin_district_language_taught,$admin_district_other_language_taught));
            $successType = 'custom_found';
            $message     = ['Profile updated successfully'];
            $_status     = 'custom_status_here';
        
            return $this->run(new RespondWithJsonJob($successType, [], $message, '', [], true));
        }
        catch (\Exception $ex) {
            
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
       
}