<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\ValidateProjectForATenantJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\ValidateProjectForPublishJob;
use App\Domains\Project\Jobs\PreviewPacingGuideProjectJob;
use App\Domains\Workflow\Jobs\GetWorkflowDetailsJob;
use App\Domains\Project\Jobs\PublishPacingGuideProjectJob;
use Log;

class PublishPacingGuideFeature extends Feature
{
    public function handle(Request $request)
    {
        try
        {
            $requestData            =   $request->all();
            $publishConfirm         =   $requestData['publish_confirm'];
            $projectIdentifier['project_id']   =   $requestData['project_id'];
            $requestingUserDetail   =   $requestData['auth_user'];
            $userId                 =   $requestingUserDetail['user_id'];
            $organizationId         =   $requestData['auth_user']['organization_id'];
            
            $validateProjectId = $this->run(new ValidateProjectIdJob($projectIdentifier));

            if($validateProjectId===true)
            {
                $validateProjectForTheTenant   =   $this->run(new ValidateProjectForATenantJob($projectIdentifier['project_id'], $requestingUserDetail['organization_id']));
                
                if($validateProjectForTheTenant == true)
                {
                    $validatePacingGuideIsDeleted  =   $this->run(new CheckIsProjectDeletedJob($projectIdentifier));
                    
                    if(!$validatePacingGuideIsDeleted === true)
                    {
                        $validateProjectForPublish  =   $this->run(new ValidateProjectForPublishJob($projectIdentifier['project_id'], $requestingUserDetail['organization_id']));

                        if(!$validateProjectForPublish === true)
                        {
                            if(isset($publishConfirm) && $publishConfirm == 0) {
                                $previewData    =   $this->run(new PreviewPacingGuideProjectJob($projectIdentifier['project_id'], $requestingUserDetail['organization_id']));

                                $requestData['workflowId']  = $previewData['workflow_id'];

                                $workflowlist = $this->run(new GetWorkflowDetailsJob($requestData));
                                //dd($workflowlist['workflow']['stages']);
                                $workflowStages =   $workflowlist['workflow']['stages'];
                                
                                $lastStageOfWorkflow    =   $workflowStages[(sizeOf($workflowStages)-1)]->stage_name;
                                
                                if($lastStageOfWorkflow != $previewData['workflow_status']) {
                                    if(sizeOf($previewData['items']) > 0) {
                                        $response       = [];
                                        $successType    = 'found';
                                        $message        = 'This pacing guide is associated to nodes from taxonomies that are still in authoring state. Any updates to these nodes will reflect in the published pacing guide and this pacing guide has not reached the final stage of the workflow. Are you sure you want to continue?';
                                        $_status        = 'custom_status_here';
                                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status)); 
                                    } else {
                                        $response       = [];
                                        $successType    = 'found';
                                        $message        = 'This Pacing guide has not reached the final stage of the workflow.Are you sure you want to publish?';
                                        $_status        = 'custom_status_here';
                                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                                    }
                                    
                                } else {
                                    $response       = [];
                                    $successType    = 'found';
                                    $message        = 'Are you sure you want to publish?';
                                    $_status        = 'custom_status_here';
                                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                                }
                            } elseif(isset($publishConfirm) && $publishConfirm == 1) {
                                $publishPacingGuideProject                 =   $this->run(new PublishPacingGuideProjectJob($projectIdentifier['project_id'],$userId));

                                $response       = [];
                                $successType    = 'found';
                                $message        = 'Pacing Guide is published successfully.';
                                $_status        = 'custom_status_here';
                                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                            }
                        }
                        else
                        {
                            $errorType = 'validation_error';
                            $message = "This pacing guide project is already published, you can not publish this pacing guide project again.";
                            $_status = '';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                        }

                    }
                    else
                    {
                        $errorType = 'validation_error';
                        $message = "This pacing guide project is deleted.";
                        $_status = '';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }

                }
                else
                {
                    $errorType = 'validation_error';
                    $message = "This pacing guide project does not belong to this organization.";
                    $_status = '';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else
            {
                $errorType = 'internal_error';
                $validatemessage="Invalid Project ID";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
            }    
            
        }
        catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
