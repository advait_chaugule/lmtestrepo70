<?php


namespace App\Services\Api\Features;


use App\Domains\CaseAssociation\Jobs\Downloads3FileJob;
use App\Domains\Document\Jobs\ValidateDocumentJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Services\Api\Traits\UuidHelperTrait;
use Log;

class GetDocumentDownloadFileFeature extends Feature
{
    use ErrorMessageHelper, StringHelper,UuidHelperTrait;
    public function handle(Request $request)
    {
        try {
            $uuid = $request->route('uuid');
            $validateUUID = $this->isUuidValid($uuid);
            if($validateUUID===true){
                $fileJsonData= $this->run(new Downloads3FileJob($uuid));
                $jsonFileContent  = $fileJsonData["json_file_content"];
                $responseFileName = $fileJsonData["json_name"];
                $contentType      = $fileJsonData["json_content_type"];
                $assetSizeInBytes = $fileJsonData["json_size"];

                if(!empty($jsonFileContent)) {
                    $responseHeaders = [
                        'Content-type'   => $contentType,
                        'Content-Length' => $assetSizeInBytes
                    ];
                    $responseHeaders['Content-Disposition'] = 'attachment; filename="'.$responseFileName.'"';
                    $response       = $jsonFileContent;
                    $successType    = 'found';
                    $message        = 'Taxonomy is downloaded successfully.';
                    $_status        = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, json_decode($response), $message, $_status));
                    //return \Response::make($jsonFileContent, 200, $responseHeaders);

                }else{
                    $response       = $jsonFileContent;
                    $successType    = 'found';
                    $message        = 'Sorry we could not find the JSON as the taxonomy was published in an older version of the application.';
                    $_status        = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, json_decode($response), $message, $_status));
                }
            }else{
                $errors = $validateUUID;
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($errors);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }


        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}