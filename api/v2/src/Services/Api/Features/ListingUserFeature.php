<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ListUserJob;
use App\Domains\User\Jobs\ProjectCountforUsersJob;
use Illuminate\Support\Facades\DB;
use Log;

class ListingUserFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $organizationId = $requestData['auth_user']['organization_id'];
            $userIdArray = [];
            $userList = $this->run(ListUserJob::class, $request);
            foreach($userList['users'] as $users){
                $userIdArray[] = $users->user_id;
            }
            /** Added to handle null data start */
            $data['users']=$userList['users'];
            $userData=array();
            foreach($data['users'] as $key=>$userInfo)
            {   
               foreach ($userInfo as $key => $value) {
                   if (is_null($value)) {
                         $userInfo->$key = "";
                   }
                }
                $userData[]=$userInfo;    
            }
            $userShow['users']=$userData;
            $userShow['count']=$userList['count'];
            $userShow['totalUsers']=$userList['totalUsers'];
            /** Added to handle null data end */
            $usageCount = $this->run(new ProjectCountforUsersJob($userIdArray, $organizationId));

            $response = ['UserList' => $userShow, 'UserProjectCount' => $usageCount];

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
