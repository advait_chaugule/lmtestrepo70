<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Taxonomy\Jobs\ValidateInputDataJob;
use App\Domains\Taxonomy\Jobs\StopPublicReviewJob;
use Log;

class StopPublicReviewFeature extends Feature
{   
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestVar = $request->all();
            $input['document_id']    =  !empty($requestVar['document_id'])?$requestVar['document_id']:"";
            $input['project_id']     =  !empty($requestVar['project_id'])?$requestVar['project_id']:"";
            $input['review_number']  =  !empty($requestVar['review_number'])?$requestVar['review_number']:"";
            $userId['updated_by']     =  $requestVar['auth_user']['user_id'];
            
            $validateInputData = $this->run(new ValidateInputDataJob($input));
            if($validateInputData===true)
            {
                $stopPublicReviewStatus = $this->run(new StopPublicReviewJob($input,$userId));
                
                $successType = 'found';
                $data = array();
                $message = 'Public Review stopped successfully';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }
            else
            {
                $successType = 'found';
                $data = $validateInputData;
                $message = 'Validation Failed';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }
            

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
