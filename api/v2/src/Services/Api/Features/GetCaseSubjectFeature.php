<?php
namespace App\Services\Api\Features;
ini_set('max_execution_time', 0);

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Organization\Jobs\ValidateOrgCodeJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as IlluminateResponse;

use App\Domains\CaseStandard\Jobs\ValidateSubjectBySourceIdJob;
use App\Domains\CaseStandard\Jobs\GetCaseFrameworkSubjectJob;

use App\Services\Api\Traits\UuidHelperTrait;
use Log;
use App\Services\Api\Traits\SavingIMSResponseTrait;

class GetCaseSubjectFeature extends Feature
{
    use UuidHelperTrait,SavingIMSResponseTrait;
    
    public function handle(Request $request)
    {
        try{
            $orgCode = isset($request['org_code'])?$request['org_code']:'';
            $requestUrl = url('/');
            if($orgCode) {
                $orgCodeArr['org_code'] = $orgCode;
                $validateOrgCode = $this->run(new ValidateOrgCodeJob($orgCodeArr));
                if($validateOrgCode['org_code'][0]!=false) {
                    return $this->respondWithCASEJsonError(404, false);
                }
            }
            $identifier = $request->route('identifier');
            $validateUUID = $this->isUuidValid($identifier);
            if($validateUUID===true){
                $validationStatus = $this->run(new ValidateSubjectBySourceIdJob($identifier));
                if($validationStatus===true) {
                    $cfSubjectKey  = ['identifier' =>$identifier,'prefix' => 'cfSubject'];
                    $caseFrameworkSubject   = $this->run(new GetFromCacheJob($cfSubjectKey));
                    if(isset($caseFrameworkSubject['status']) && $caseFrameworkSubject['status'] == "error") {
                        // get the CFSubject in CASE standard structure
                        $caseFrameworkSubject = $this->run(new GetCaseFrameworkSubjectJob($identifier,$orgCode,$requestUrl));
                        $this->run(new DeleteFromCacheJob($cfSubjectKey));
                        $this->run(new SetToCacheJob($caseFrameworkSubject,$cfSubjectKey));
                        $caseFrameworkSubject = $this->run(new GetFromCacheJob($cfSubjectKey));
                    }

                    if($caseFrameworkSubject!==false) {
                        return $this->respondWithCASEJsonSuccess(["CFSubjects"=>[$caseFrameworkSubject]]);
                    }
                    else {
                        return $this->respondWithCASEJsonError(404);
                    }
                }
                else {
                    return $this->respondWithCASEJsonError(404);
                }
            }
            else {
                return $this->respondWithCASEJsonError(404, false);
            }
        }
        catch(\Exception $ex){
            Log::error($ex);
            return $this->respondWithCASEJsonError(500);
        }

    }

    private function respondWithCASEJsonSuccess($data) {
        $responseReturn = response()->json($data, 200, [], JSON_UNESCAPED_SLASHES);
        $this->IMSResponse($responseReturn);
        return $responseReturn;
    }

    private function respondWithCASEJsonError($httpStatus, $validUUID = true) {
        $errorResponseBody = [
            "imsx_codeMajor" => "failure",
            "imsx_severity" => "error",
            "imsx_description" => ""
        ];
        switch ($httpStatus) {
            case '404':
                $imsx_codeMinorFieldValue = $validUUID ? "unknownobject" : "invaliduuid";
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => $imsx_codeMinorFieldValue
                                                                                ];
                break;
            default:
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "",
                                                                                    "imsx_codeMinorFieldValue" => "internal_server_error"
                                                                                ];
                break;
        }

        if (!$validUUID) {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES)
                        ->setStatusCode(404, 'Invalid UUID');
        } else {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES);
        }
    }
}
