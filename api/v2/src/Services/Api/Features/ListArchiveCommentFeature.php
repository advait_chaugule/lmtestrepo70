<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Comment\Jobs\ListCommentJob;
use Log;

class ListArchiveCommentFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData            = $request->all();
            $organizationId         = $requestData['auth_user']['organization_id'];
            $threadSourceIdentifier = $request->route('thread_source_id');

            $commentList = $this->run(new ListCommentJob($threadSourceIdentifier, $organizationId, '4'));

            $response   =   $commentList;
            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
