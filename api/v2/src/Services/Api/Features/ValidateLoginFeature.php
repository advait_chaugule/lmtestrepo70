<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ValidateLoginInputJob;
use App\Domains\User\Jobs\LoginJob;
use App\Domains\User\Jobs\UpdateAccessTokenJob;

use App\Domains\User\Jobs\IsCheckUserCurrentStatusJob;
use App\Domains\User\Jobs\ValidateLoginInputForTenantJob;

use App\Domains\User\Events\UserLoginEvent;
use App\Domains\User\Events\SessionRefreshedEvent;
use App\Domains\Organization\Jobs\ValidateUsersOrganizationJob;
use Illuminate\Routing\UrlGenerator;
use App\Domains\User\Jobs\GetOrgCodeFromEmailIdJob;
use Log;
use App\Services\Api\Traits\CloudWatchTrait;

class ValidateLoginFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, CloudWatchTrait;

    public function handle(Request $request)
    {
        try {
            $input = $request->all();
            $data['username'] = $input['username'];
            $dataEmail = ['email' => $data['username']];
            //print_r($dataEmail);die;
           // $dataTenent = ['email' => $data['username'],'organization_id' => $input['organization_id']];
            $validationStatus = $this->run(new ValidateLoginInputJob($input));
           
            if($validationStatus===true) {
                $domainUrl  = url('/'); // Get domain name
                $domainPath = parse_url($domainUrl);
                $domainName = '';
                if($domainPath){
                    $domainName = $domainPath['host'];
                }
                if(strpos($domainName,'localhost')!==false) {
                    $baseUrl = env("APP_URL");
                }else{
                    if(strpos($domainName,'api.')!==false)
                    {
                        $baseUrl = str_replace('api.', '',$domainName);
                    }else{
                        $baseUrl = $domainName;
                    }
                }
                  $checkUsersOrganization = $this->run(new ValidateUsersOrganizationJob($dataEmail,$baseUrl)); // Validate domain name
                 if($checkUsersOrganization!=1) {
                     $successType = 'validation_error';
                     $message = 'Invalid username or password .';
                     $_status = 'custom_status_here';
                     return $this->run(new RespondWithJsonErrorJob($successType, $message, $_status));
                 }

                $validationStatus = $this->run(new IsCheckUserCurrentStatusJob($dataEmail));
                if($validationStatus != 1){
                    $loginResponse = $this->run(new LoginJob($input));
                    $loginStatus = $loginResponse["loginStatus"];
                    
                    if($loginStatus!==false){
                        if(!isset($responseBody["organization"]["organization_id"]))
                        // raise event to track activities for reporting
                        $responseBody = $loginResponse["responseBody"];
                        $userId = $responseBody["user_id"];

                        // $updatedAccessToken = $this->run(new UpdateAccessTokenJob($userId));
                        // create new user access token, update the same and override the response with the updated token
                        // $responseBody["active_access_token"] = $updatedAccessToken;

                        $this->run(new UpdateAccessTokenJob($userId));

                        $requestUserDetails = [
                            "user_id" => $responseBody["user_id"],
                            "first_name" => $responseBody["first_name"],
                            "last_name" => $responseBody["last_name"],
                            "organization" => $responseBody["organization"]
                        ];

                        $eventData = [
                            "beforeEventRawData" => [],
                            "afterEventRawData" => $requestUserDetails,
                            "requestUserDetails" => $requestUserDetails
                        ];
                        event(new UserLoginEvent($eventData));

                        // raise user session refreshed event ( to indicate session start time )
                        $dataForUserSessionRefreshedEvent = [
                            "user_id" => $userId,
                            "organization_id" => $responseBody["organization"]["organization_id"],
                            "first_name" => $responseBody["first_name"],
                            "last_name" => $responseBody["last_name"]
                        ];
                        $userSessionStartedEventData = [
                            "beforeEventRawData" => $dataForUserSessionRefreshedEvent,
                            "afterEventRawData" => $dataForUserSessionRefreshedEvent,
                            "requestUserDetails" => $dataForUserSessionRefreshedEvent
                        ];
                        event(new SessionRefreshedEvent($userSessionStartedEventData));

                        $successType = 'found';
                        $message = 'Login successfull.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $responseBody, $message, $_status));
                    }
                    else {
                        $errorType = 'authorization_error';
                        $message = $loginResponse["responseMessageList"];//$this->arrayContentToCommaeSeparatedString();
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
                    }
                }
                else
                {
                    $errorType = 'authorization_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'This account is not verified yet. Kindly verify your email';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
                }
              /*  }
                else
                {
                    $errorType = 'authorization_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Invalid username or password.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
                }*/
            }
            else {

                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);

            $this->CloudWatchLogger($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
