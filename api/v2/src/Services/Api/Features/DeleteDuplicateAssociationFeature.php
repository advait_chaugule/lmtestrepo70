<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;

class DeleteDuplicateAssociationFeature extends Feature
{
    public function handle(Request $request)
    {
        ini_set('memory_limit','80980M');
        ini_set('max_execution_time', 0);

        try {
            $output_response = [];
            $document_id = trim($request->document_id);

            $document_id_arr = DB::select("select document_id, source_document_id from `acmt_documents` where `is_deleted` = 0 and document_id='".$document_id."'");
            if(!$document_id_arr) {
                $successType = 'custom_not_found';
                $message = 'Taxonomy not found';
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
            }
            $this->deleteAsso($document_id_arr);

            $successType = 'custom_found';
            $message = 'Execution completed successfully';
            $_status = 'custom_status_here';

            return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], true));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
            $_status = 'custom_status_here';
            Log::error($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    # delete duplicate association and update association to keep with updated data
    public function deleteAsso($tax_arr)
    {
        $document_id = $tax_arr[0]->document_id;
        $source_document_id = $tax_arr[0]->source_document_id;

        #is child and first level associations
        $firstLevelAsso = DB::table('item_associations')
                            ->where('association_type', 1)
                            ->where('is_deleted', 0)
                            ->where('source_document_id', $document_id)
                            ->whereColumn('source_document_id', 'destination_document_id')
                            ->whereIn('target_item_id', [$document_id, $source_document_id])
                            ->orderBy('created_at', 'asc')
                            ->get();

        $firstLevelAssoArr = [];
        foreach($firstLevelAsso as $asso) {
            $firstLevelAssoArr[$asso->source_item_id][] = $asso;
        }

        $updateAssociation = [];
        $deleteAssociation = [];
        foreach($firstLevelAssoArr as $row) {
            $assoCount = count($row);
            if($assoCount >= 2) {
                #only sequence no needs to be updated
                $updateAssociation[$row[0]->item_association_id] = $row[$assoCount-1]->sequence_number;
                $temp = 1;
                foreach($row as $asso) {
                    if($temp == 1) {
                        $temp++;
                        continue;
                    }
                    $deleteAssociation[] = $asso->item_association_id;
                }
            }
        }

        if(count($updateAssociation)) {
            foreach($updateAssociation as $asso_id => $seq_no) {
                DB::table('item_associations')
                    ->where('item_association_id', $asso_id)
                    ->update(['sequence_number' => $seq_no]);
            }
        }

        if(count($deleteAssociation)) {
            DB::table('item_associations')
                ->whereIn('item_association_id', $deleteAssociation)
                ->update(['is_deleted' => 1]);
        }
    }
}
