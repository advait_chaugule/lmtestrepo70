<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeTemplate\Jobs\ListNodeTemplateJob;
use Log;

class ListNodeTemplateFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $organizationId = $requestData['auth_user']['organization_id'];
            $nodeTemplateIdArray = [];
            $nodeTemplateList = $this->run(ListNodeTemplateJob::class, $request);
            
            
            $response = $nodeTemplateList; //'RoleUsageCount' => $usageCount

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
