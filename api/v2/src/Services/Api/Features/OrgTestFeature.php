<?php 
namespace App\Services\Api\Features;
use App\Domains\Organization\Jobs\GetOrgMappingDetails;
use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

class OrgTestFeature extends Feature
{
	public function handle()
    {

        $getData = $this->run(new GetOrgMappingDetails());
        if($getData) {

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $getData, $message, $_status));
        }else{
            $errorType = 'not_found';
            // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
            $message = 'No records found';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}

?>