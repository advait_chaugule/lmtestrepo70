<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\User\Jobs\IsCheckUserInputJob;
use App\Domains\User\Jobs\IsCheckUserCurrentStatusJob;
use App\Domains\User\Jobs\CreateUserJob;
use App\Domains\User\Jobs\SendSelfRegistrationEmailJob;

use App\Domains\Organization\Jobs\GetOrganizationDetailByIdJob;

use Illuminate\Support\Facades\Hash;

use App\Domains\User\Jobs\GetDefaultUserRoleIdJob;
use Log;

class PublicReviewFeature extends Feature
{   
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestVar = $request->all();
            $input['first_name'] = !empty($requestVar['first_name']) ? $requestVar['first_name'] : '';
            $input['last_name'] = !empty($requestVar['last_name']) ? $requestVar['last_name'] : '';
            $input['email'] = !empty($requestVar['email_id']) ? $requestVar['email_id'] : '';
            $input['password'] = !empty($requestVar['password']) ? $requestVar['password'] : '';
            $input['organization_id'] = !empty($requestVar['organization_id']) ? $requestVar['organization_id'] : '';
            $input['reg_user_type'] = !empty($requestVar['reg_user_type']) ? $requestVar['reg_user_type'] : null;
            $input['teacher_grade_optional_data'] = !empty($requestVar['teacher_grade_level_taught']) ? $requestVar['teacher_grade_level_taught'] : '';
            $input['teacher_subject_optional_data'] = !empty($requestVar['teacher_subject_area_taught']) ? $requestVar['teacher_subject_area_taught'] : '';
            $input['admin_languages_optional_data'] = !empty($requestVar['admin_district_language_taught']) ? $requestVar['admin_district_language_taught'] : '';
            $input['reg_admin_other_languages'] = !empty($requestVar['admin_district_other_language_taught']) ? $requestVar['admin_district_other_language_taught'] : null ;
            $input['self_reg_expiry'] = date('Y-m-d H:i:s', strtotime('+24 hours'));
            $requestUrl                 = url('/');
            $serverName                 = str_replace('server', '', $requestUrl);

            // For cached inactive org, org id is sent as blank, condition handled
            if(!empty($input['organization_id'])){               
                $organizationDetail =   $this->run(new GetOrganizationDetailByIdJob($input['organization_id']));         
                
                // Check if current organization if inactive
                if($organizationDetail->is_active=='0'){                
                    $errorType = 'custom_found';
                    $message = ['This Tenant is deactivated and please contact admin'];
                    return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
                }
            }

            $validationStatus = $this->run(new IsCheckUserInputJob($input));
            if($validationStatus===true){
                
                $inputEmail = ['email' => $requestVar['email_id']];
                $validationStatus = $this->run(new IsCheckUserCurrentStatusJob($inputEmail));
                
                if($validationStatus == 0) {
                    $input['user_id']=$this->createUniversalUniqueIdentifier();
                    $input['username'] = $requestVar['email_id'];
                    $input['active_access_token'] = $this->createUniversalUniqueIdentifier();

                    //get demographic optional data in string form start
                    if(!empty($input['teacher_grade_optional_data'])){
                        $input['reg_teacher_grades']='';
                        foreach($input['teacher_grade_optional_data'] as $teacherGradeK=>$teacherGradeV){
                            foreach($teacherGradeV as $teacherGradeDataK=>$teacherGradeDataV){
                                if(!empty($teacherGradeDataV)){
                                    $input['reg_teacher_grades'].=$teacherGradeDataV.",";
                                }    
                            }
                        }
                        $input['reg_teacher_grades']=rtrim($input['reg_teacher_grades'],',');
                        $input['reg_teacher_grades']=!empty($input['reg_teacher_grades']) ? $input['reg_teacher_grades'] : null;
                    }

                    if(!empty($input['teacher_subject_optional_data'])){
                        $input['reg_teacher_subjects']='';
                        foreach($input['teacher_subject_optional_data'] as $teacherSubjectK=>$teacherSubjectV){
                            foreach($teacherSubjectV as $teacherSubjectDataK=>$teacherSubjectDataV){
                                if(!empty($teacherSubjectDataV)){
                                    $input['reg_teacher_subjects'].=$teacherSubjectDataV.",";
                                }
                            }
                        }
                        $input['reg_teacher_subjects']=rtrim($input['reg_teacher_subjects'],',');
                        $input['reg_teacher_subjects']=!empty($input['reg_teacher_subjects']) ? $input['reg_teacher_subjects'] : null;
                    }

                    if(!empty($input['admin_languages_optional_data'])){
                        $input['reg_admin_languages']='';
                        foreach($input['admin_languages_optional_data'] as $adminLanguageK=>$adminLanguageV){
                            foreach($adminLanguageV as $adminLanguageDataK=>$adminLanguageDataV){
                                if(!empty($adminLanguageDataV)){
                                    $input['reg_admin_languages'].=$adminLanguageDataV.",";
                                }

                            }
                        }
                        $input['reg_admin_languages']=rtrim($input['reg_admin_languages'],',');
                        $input['reg_admin_languages']=!empty($input['reg_admin_languages']) ? $input['reg_admin_languages'] : null;
                    }
                    
                    //get demographic optional data in string form end
                    $input['password'] = bcrypt($requestVar['password']);
                    // Get Default user role id
                    $input['role_code']=config('selfRegistration')['ROLE']['PROJECT_PUBLIC_REVIEW_ROLE'];
                    $input['role_id'] = $this->run(new GetDefaultUserRoleIdJob($input));
                    
                    if($input['role_id']!==null){

                        $user = $this->run(new CreateUserJob($input));
                        $email['email']                 =   $input['email'];
                        $email['active_access_token']   =   $input['active_access_token'];
                        $email['org_code']              =   $organizationDetail->org_code;
                        $email['domain_name']           =   $serverName;
                        $this->run(new SendSelfRegistrationEmailJob($email));
                        $user['email'] = "Sent verification mail.";
                        $successType = 'created';
                        $message = 'Data created successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $user, $message, $_status));
                    }
                    else{
                        $errorType = 'internal_error';
                        $message = "Default Role id not found";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                    
                }
                else if($validationStatus == 1) {
                    $successType = 'found';
                    $data = ['result' => 'deactive'];
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Email already exists in system. But it is not active.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
                else {
                    $successType = 'found';
                    $data = ['result' => 'exist'];
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Email already exists in system. Please try with different email address.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
            }
            else{
                $errorType = 'internal_error';
                $message = $validationStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
