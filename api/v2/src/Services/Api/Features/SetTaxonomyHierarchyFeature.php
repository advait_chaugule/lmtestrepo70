<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use App\Domains\Taxonomy\Jobs\SetOrderedTaxonomyTreeForPacingGuideJob;
use App\Domains\Taxonomy\Jobs\SetOrderedTaxonomyTreeJob;
use App\Domains\PacingGuide\Jobs\GetFlattenedTaxonomyTreeForPacingGuideJob;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeJob;
use App\Domains\Item\Jobs\DocumentMultipleTableJob;
use App\Domains\Caching\Events\CachingEvent;
use Log;


class SetTaxonomyHierarchyFeature extends Feature
{
    public function handle(Request $request)
    {   
        ini_set('max_execution_time', 0);
        try {
            $requestData            =   $request->input();
            $documentIdentifier     =   $request->route('document_id');
            $requestingUserDetails  =   $request->input("auth_user");
            $taxonomyType           =   isset($requestData['taxonomy_type'])?$requestData['taxonomy_type']:'';
            $orderedTaxonomy    =   '';
           //if taxonomy is not coming from frontend then find from db
            if($taxonomyType==NULL) {
                $documentDetails = $this->run(new GetDocumentAndRespectiveMetadataValuesJob($documentIdentifier));
            }else{
                //get taxonomy type from front end
                $documentDetails['document_type'] = $taxonomyType;
            }
            if($documentDetails['document_type'] == '2') {
                $this->run(new SetOrderedTaxonomyTreeForPacingGuideJob($documentIdentifier,$requestData));

                $orderedTaxonomy        =   $this->run(new GetFlattenedTaxonomyTreeForPacingGuideJob($documentIdentifier, $requestingUserDetails));
            } else {
                $this->run(new SetOrderedTaxonomyTreeJob($documentIdentifier,$requestData));

                $orderedTaxonomy        =   $this->run(new GetFlattenedTaxonomyTreeJob($documentIdentifier, $requestingUserDetails));

            }
            
            //project and document table update
            $this->run(new DocumentMultipleTableJob($documentIdentifier));
            //$orderedTaxonomy        =   $this->run(new GetFlattenedTaxonomyTreeForPacingGuideJob($documentIdentifier, $requestingUserDetails));
            //dd($orderedTaxonomy);
            $successType = 'created';
            $message = 'Data created successfully.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $orderedTaxonomy, $message, $_status));

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
