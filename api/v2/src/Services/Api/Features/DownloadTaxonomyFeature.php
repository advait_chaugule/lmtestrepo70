<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Aws\S3\S3Client;
use Aws\Iam\IamClient;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;  
use Log;

//use App\Domains\JReport\Jobs\DownloadTaxonomyFromS3Job;


class DownloadTaxonomyFeature extends Feature
{
    public function handle(Request $request)
    {
        
        try
        {
            
            $baseFolderPathForStorage = storage_path('app').'\downloaded_taxonomy';
            
            if (!file_exists($baseFolderPathForStorage)) {
               
                mkdir($baseFolderPathForStorage, 0777);
            }
          
            $bucket = env('AWS_S3_BUCKET');
            $requestData = $request->all();
            $key = $request->key;
            $s3Client = S3Client::factory([
                'credentials' => [
                    'key' => env('AWS_KEY'),
                    'secret' => env('AWS_SECRET')
                ],
                'version' => '2006-03-01',
                'region' => env('AWS_REGION')
            ]);

            $result = $s3Client->getObject([
                'Bucket' => $bucket,
                'Key' => $key,
                'SaveAs' => $baseFolderPathForStorage.'/'.$key
            ]);
              
            $file =  $baseFolderPathForStorage.'/'.$key;
            header('Content-Encoding: UTF-8');
            header('Content-type: text/csv; charset=UTF-8');

            header ("Content-Length: ".filesize($file));

            header ("Content-Disposition: attachment; filename=".$key);

            readfile($file);
            

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}

