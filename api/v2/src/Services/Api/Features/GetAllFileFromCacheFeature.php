<?php
namespace App\Services\Api\Features;

use App\Domains\Asset\Jobs\GetListOfAssetJob;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;

class GetAllFileFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
        $requestData = $request->all();
        $itemLinkedId = $request->route('organization_id');
        $requestUserDetails['organization_id'] =  $itemLinkedId;
        $keyInfo = array('identifier' => $itemLinkedId, 'prefix' => 'FILES_');
        $status = $this->run(new GetFromCacheJob($keyInfo));
        if (isset($status['status']) && $status['status'] == "error") {
            $request->merge($requestData);
            $assetList =$this->run(new GetListOfAssetJob($itemLinkedId,$requestUserDetails));
            if (!empty($assetList) && count($assetList) > 0) {
                $status1 = $this->run(new DeleteFromCacheJob($keyInfo));
                $status2 = $this->run(new SetToCacheJob($assetList, $keyInfo));
                $status3 = $this->run(new GetFromCacheJob($keyInfo));
                $message = 'Asset Data found.';
                $successType = 'found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $status3, $message, $_status));
            }
            else{
                $message = 'Asset Data Not found.';
                $successType = 'found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
            }
        } else {
            $message = 'Asset Data found.';
            $successType = 'found';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $status, $message, $_status));
        }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
