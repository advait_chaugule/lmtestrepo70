<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Organization\Jobs\ValidateOrgCodeJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;

use App\Domains\CaseStandard\Jobs\ValidateNodeTypeBySourceIdJob;
use App\Domains\CaseStandard\Jobs\GetCaseFrameworkItemTypeJob;

use App\Services\Api\Traits\UuidHelperTrait;
use Log;
use App\Services\Api\Traits\SavingIMSResponseTrait;
use App\Domains\CaseStandard\Jobs\ValidateSourceNodeIdFromItemTableJob;
class GetCaseItemTypeFeature extends Feature
{
    use UuidHelperTrait,SavingIMSResponseTrait;

    public function handle(Request $request)
    {
       try{
            $ordCode = isset($request['org_code'])?$request['org_code']:'';
            $requestUrl = url('/');
            $checkServer = str_replace('server', '',$requestUrl);
            $url  = $checkServer.$_SERVER['REQUEST_URI'];
            if($ordCode)
            {
                $orgCodeArr['org_code'] = $ordCode;
                $validateOrgCode = $this->run(new ValidateOrgCodeJob($orgCodeArr));
                if($validateOrgCode['org_code'][0]!=false) {

                    return $this->respondWithCASEJsonError(404, false);
                }
            }
            $identifier = $request->route('identifier');
            $validateUUID = $this->isUuidValid($identifier);
            if($validateUUID===true){
                $validationStatus = $this->run(new ValidateNodeTypeBySourceIdJob($identifier));
                if($validationStatus===false) {
                   $validationStatus =  $this->run(new ValidateSourceNodeIdFromItemTableJob($identifier));
                }
                if($validationStatus===true) {
                    $cfItemTypeKey  = ['identifier' =>$identifier,'prefix' => 'cfItemType'];
                    $caseFrameworkItemType   = $this->run(new GetFromCacheJob($cfItemTypeKey));
                    if(isset($caseFrameworkItemType['status']) && $caseFrameworkItemType['status'] == "error") {
                        // get the CFItemType in CASE standard structure
                        $caseFrameworkItemType = $this->run(new GetCaseFrameworkItemTypeJob($identifier,$ordCode,$requestUrl,$url));
                        $this->run(new DeleteFromCacheJob($cfItemTypeKey));
                        $this->run(new SetToCacheJob($caseFrameworkItemType,$cfItemTypeKey));
                        $caseFrameworkItemType = $this->run(new GetFromCacheJob($cfItemTypeKey));
                    }

                    if($caseFrameworkItemType!=false) {
                        return $this->respondWithCASEJsonSuccess(["CFItemTypes"=>[$caseFrameworkItemType]]);
                    }else{
                        return $this->respondWithCASEJsonError(400);
                    }

                }
                else {
                    return $this->respondWithCASEJsonError(404);
                }
            }
            else {
                return $this->respondWithCASEJsonError(404, false);
            }
        }
        catch(\Exception $ex){
            Log::error($ex);
            return $this->respondWithCASEJsonError(500);
        }

    }

    private function respondWithCASEJsonSuccess($data) {
        $responseReturn = response()->json($data, 200, [], JSON_UNESCAPED_SLASHES);
        $this->IMSResponse($responseReturn);
        return $responseReturn;
    }

    private function respondWithCASEJsonError($httpStatus, $validUUID = true) {
        $errorResponseBody = [
            "imsx_codeMajor" => "failure",
            "imsx_severity" => "error",
            "imsx_description" => ""
        ];
        switch ($httpStatus) {
            case '400' :
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "invalid_selection_field"
                                                                                ];
                break;
                
            case '401' :
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "unauthorisedrequest"
                                                                                ];
                break;

            case '403' :
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "forbidden"
                                                                                ];
                break;

            case '404':
                $imsx_codeMinorFieldValue = $validUUID ? "unknownobject" : "invaliduuid";
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => $imsx_codeMinorFieldValue
                                                                                ];
                break;
            
            case '429':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "server_busy"
                                                                                ];
                break;

            default:
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "",
                                                                                    "imsx_codeMinorFieldValue" => "internal_server_error"
                                                                                ];
                break;
        }

        if (!$validUUID) {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES)
                        ->setStatusCode(404, 'Invalid UUID');
        } else {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES);
        }
    }
}
