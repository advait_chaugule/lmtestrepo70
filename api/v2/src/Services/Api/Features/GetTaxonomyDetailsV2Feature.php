<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentV2Job;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV2Job;
use App\Domains\Taxonomy\Jobs\GetTaxonomyNodesV2Job;
use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;
use App\Domains\CaseAssociation\Jobs\GetItemExemplarAssociationsV2Job;
use App\Domains\CaseAssociation\Jobs\GetItemAssociationsV2Job;
use App\Domains\Asset\Jobs\GetAssetListJob;
use App\Domains\Asset\Jobs\GetAssetListV2Job;
use App\Domains\Asset\Jobs\GetItemAssetListV2Job;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use Log;

class GetTaxonomyDetailsV2Feature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        
        try{
            $documentIdentifier = $request->route('document_id');
            $requestingUserDetails = $request->input("auth_user");
            $organizationIdentifier =   $requestingUserDetails['organization_id'];
            $requestUrl = url('/');
            $validationStatus = $this->run(new ValidateDocumentV2Job($documentIdentifier, $requestingUserDetails));
            if($validationStatus===true) {
                $documentAssets = $this->run(new GetAssetListV2Job($documentIdentifier, $organizationIdentifier,$requestUrl));
                $documentDetails["item_id"] = $documentIdentifier;
                $documentDetails["is_document"] = 1;
                $documentDetails["assets"] = $documentAssets;
                
                $taxonomyHierarchyV2[] = $documentDetails;

           $taxonomyHierarchy = $this->run(new GetTaxonomyNodesV2Job($documentIdentifier, $requestingUserDetails));
         
                       // Set Details of Taxonomy In cache
                       //print_r($taxonomyHierarchy['nodes']);
             $itemAssets = $this->run(new GetItemAssetListV2Job($documentIdentifier, $organizationIdentifier,$requestUrl));
            $exemplarAssociationList = $this->run(new GetItemExemplarAssociationsV2Job($documentIdentifier, ['organization_id' =>$organizationIdentifier]));   
           // $ItemAssociationList = $this->run(new GetItemAssociationsV2Job($documentIdentifier, ['organization_id' =>$organizationIdentifier]));   
                       foreach($taxonomyHierarchy as $node) {
                        $itemId = $node;
                                
                                  //  $response = $this->run(new GetCFItemDetailsJob($itemId, $organizationIdentifier));
                                 // $exemplarAssociationList = $this->run(new GetExemplarAssociationsJob($itemId, ['organization_id' =>$organizationIdentifier]));
                                    // embed the exemplar association collection inside the current response body
                                    $response["item_id"] = $itemId;
                                    $response["is_document"] = 0;
                                   //$response["exemplar_associations"] = $exemplarAssociationList;

                                    // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                                    if(!empty($exemplarAssociationList[$itemId])) 
                                    {
                                       $response["exemplar_associations"] = $exemplarAssociationList[$itemId]['exemplar_associations'];
                                    } 
                                    else
                                    {
                                       $response["exemplar_associations"] = [];
                                     }
                                 if(!empty($itemAssets[$itemId])) 
                                 {
                                    $response["assets"] = $itemAssets[$itemId]['assets'];
                                 } 
                                 else
                                 {
                                    $response["assets"] = [];
                                  }
                                    $taxonomyHierarchyV2[] = $response;
                                    unset($response);
                            
                            
                       
                    }
                    
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchyV2, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validationStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
