<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Import\Jobs\UploadCSVUserFileJob;
use App\Domains\Csv\Jobs\ValidateCsvFileJob;
use App\Domains\User\Jobs\ValidateBulkUserViaCSVJob;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\User\Jobs\SendInvitationEmailNotificationJob;
use App\Domains\User\Jobs\SendRegistrationEmailNotificationJob;

use Storage;
use Log;
use DB;

class createUserViaCSVFeature extends Feature
{
    use UuidHelperTrait;
    public function handle(Request $request)
    {
        try{
            $requestVar                 = $request->all();
            $orgID                      = $requestVar['current_organization_id'];
            $requestUrl                 = url('/');
            $serverName                 = str_replace('server', '', $requestUrl);
            $importRequestIdentifier    = $this->createUniversalUniqueIdentifier();;
            $validateCsvFile         = $this->run(new ValidateCsvFileJob($request));
            if($validateCsvFile['status']===true){
                 // get uploaded csv file
                 $csvFile           =   $validateCsvFile['file'];
                 // create a file identifier
                 $fileIdentifier    =   $importRequestIdentifier;
                 // name of file to create
                 $csvFileName       =   $fileIdentifier.".csv";
                 // store the content inside the file created above
                 $uploadCSVUsers    =   $this->run(new UploadCSVUserFileJob($csvFile,$csvFileName));
            
                 $fileContent = Storage::disk('s3')->get($uploadCSVUsers);
                 $userTitle   = strtolower($fileContent);
                 $lines       = explode("\n", $userTitle );
                $head         = str_getcsv(array_shift($lines));
                $array        = [];
                    foreach ($lines as $line) {
                        $row = array_pad(str_getcsv($line), count($head), '');
                        $array[] = array_combine($head, $row);
                    }
                $i=0;
                $result =[];
                $errarr=[];
                $emailRequired=[];
                $roleRequired=[];
                $emailExists=[];
                $emailIncorrect=[];
                $roleNotExists=[];
                    foreach($array as $arrayK=>$arrayV)
                    {
                        $result[$i]['firstName'] = $arrayV['first name'];
                        $result[$i]['lastName'] = $arrayV['last name'];
                        $result[$i]['email_id'] = $arrayV['email id'];
                        $result[$i]['role']    = $arrayV['role'];
                        $result[$i]['row']     =$i+2;
                        $result[$i]['orgID']   =$orgID; 
                        
                        if(!($arrayV['first name'] == '' && $arrayV['last name'] == '' && $arrayV['email id'] =='' && $arrayV['role'] ==''))
                       {
                           
                            $validateCSVContent  = $this->run(new ValidateBulkUserViaCSVJob($result[$i]));
                       
                       
                        if($validateCSVContent!==true){
                           
                            foreach($validateCSVContent as $key=>$value){
                                
                                switch($key){
                                    case '1':
                                            $emailRequired[]=$value;
                                            break;
                                    case '2':
                                            $roleRequired[]=$value;
                                            break;                                
                                    case '3':
                                            $emailExists[]=$value;
                                            break;
                                    case '4':
                                            $emailIncorrect[]=$value;
                                            break;
                                    case '5':
                                            $roleNotExists[]=$value;                                                 
                                    default:        
                                            break;    
                                }
                               
                            } 
                        }
                    }
                        $i++; // print_r($validateCSVContent);                         
                    }

                  if(!empty($emailRequired || $roleRequired ||  $emailExists || $emailIncorrect || $roleNotExists))
                  { 
                    $ErrorEmailRequired=implode(',',$emailRequired); 
                    $ErrorRoleRequired=implode(',',$roleRequired); 
                    $ErrorEmailExists=implode(',',$emailExists); 
                    $ErrorEmailIncorrect=implode(',',$emailIncorrect); 
                    $ErrorRoleNotExists=implode(',',$roleNotExists);  
                    

                    $errorReport=[
                                    'EmailRequired'=>$ErrorEmailRequired,
                                    'RoleRequired'=>$ErrorRoleRequired,
                                    'EmailExists'=>$ErrorEmailExists,
                                    'EmailIncorrect'=>$ErrorEmailIncorrect,
                                    'RoleNotExists'=>$ErrorRoleNotExists
                                    ];

                                   // print_r($errorReport);
                    $Errors=[];
                
                    foreach($errorReport as $keyForError => $value)
                    {   
                      
                        if($keyForError =='EmailRequired' && (!empty($value))){
                           $Errors[] =['Error' =>'Email Field cannot be empty','row' =>$value] ;
                        }
                        
                        if($keyForError =='RoleRequired' && (!empty($value))){
                            $Errors[] =['Error' =>'Role Field cannot be empty','row' =>$ErrorRoleRequired] ;
                        }

                        if($keyForError =='EmailExists' && (!empty($value))){
                            $Errors[] =['Error' =>'Email already exists','row' =>$ErrorEmailExists] ;
                        }

                        if($keyForError =='EmailIncorrect' && (!empty($value))){
                            $Errors[] =['Error' =>'Incorrect email format','row' =>$ErrorEmailIncorrect] ;
                        }

                        if($keyForError =='RoleNotExists' && (!empty($value))){
                            $Errors[] =['Error' =>'Role does not exists','row' =>$ErrorRoleNotExists] ;
                        }

                     
                    }
                
                    $successType = 'created';
                    $data = $Errors;
                    $message = 'Data created successfully.';
                    $_status = 'custom_status_here';
                    $fileSuccess = $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                    return $fileSuccess;  
                } 
              
                   if(empty($Errors))
                   {
                         foreach($result as $arrrayK=>$arrayV)
                        {                   
                             
                        
                                $userDetails['firstName'] = $arrayV['firstName'];
                                $userDetails['lastName'] = $arrayV['lastName'];
                                $userDetails['email_id'] = $arrayV['email_id'];
                                $userDetails['role']    = $arrayV['role'];
                               
                                $userId=$this->createUniversalUniqueIdentifier();
                                $accessToken = $this->createUniversalUniqueIdentifier();
                              if(!empty($userDetails['email_id'] && $userDetails['role'] )) 
                              { 
                                  
                                    $InsertUser= [
                                                'user_id'=>$userId,
                                                'email'=> $userDetails['email_id'],
                                                'username'=>$userDetails['email_id'],
                                                'first_name'=>$userDetails['firstName'],
                                                'last_name'=> $userDetails['lastName'], 
                                                'organization_id'=>$orgID
                                                ];
                    
                                    $InsertUserQuery = DB::table('users')
                                                        ->insert($InsertUser);

                                  
                                        $getRoleID = DB::table('roles')
                                                        ->where('organization_id',$orgID)
                                                        ->where('name',$userDetails['role'])
                                                        ->get()
                                                        ->toarray();
                                            if(!empty($getRoleID))
                                            {
                                                $roleID = $getRoleID[0]->role_id;
                                            }
                                          
                                        $InsertRole=[
                                                        'user_id'=>$userId,
                                                        'organization_id'=>$orgID,
                                                        'role_id'=>$roleID
                                                    ];
                                   
                                        $InsertRoleQuery=DB::table('users_organization')
                                                        ->insert($InsertRole);



                                        $mail= DB::table('users')
                                                ->where('email',$userDetails['email_id'])
                                                ->where('current_organization_id',$orgID)
                                                ->get()
                                                ->toarray();
                                             if(!empty($mail))
                                                {
                                                    $orgDetails= DB::table('organizations')
                                                    ->where('organization_id',$orgID)
                                                    ->get()
                                                    ->toarray();
                                                        if(!empty($orgDetails))
                                                        {
                                                            $orgName = $orgDetails[0]->name;
                                                            $orgCode = $orgDetails[0]->org_code;
                                                        }

                                                    $tenantInvite['email'] = $userDetails['email_id'];
                                                    $tenantInvite['active_access_token'] = $accessToken;
                                                    $tenantInvite['organization_id']  =   $orgID;
                                                    $tenantInvite['org_code']  =  $orgCode;
                                                    $tenantInvite['org_name']  = $orgName;
                                                    $tenantInvite['domain_name']  =  $serverName;
                                                    $tenantInvite['name']  =   $userDetails['firstName']." ".$userDetails['lastName'];
                                                    $this->run(new SendInvitationEmailNotificationJob($tenantInvite));
                                                   
                                              }else
                                                {
                                                    
                                                    $mail['email']                  =  $userDetails['email_id'];
                                                    $mail['active_access_token']   =   $accessToken;
                                                    $mail['org_code']              =   $orgID;
                                                    $mail['domain_name']           =   $serverName;

                                                    $this->run(new SendRegistrationEmailNotificationJob($mail));
                                                }

                                  } 
                                }                        
                    }
                    
                $successType = 'created';
                $data = "mail sent";
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                $fileSuccess = $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                return $fileSuccess;
               
        }else{
                $errorType = 'validation_error';
                $message = $validateCsvFile;
                $_status = 'Invalid file.';
                $fileError = $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                return $fileError;   
        }
         
    }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    } 
}

