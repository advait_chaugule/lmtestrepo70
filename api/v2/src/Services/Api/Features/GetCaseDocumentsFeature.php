<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Organization\Jobs\ValidateOrgCodeJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\CaseStandard\Jobs\GetAllCaseFrameworkDocumentsJob;
use App\Domains\Organization\Jobs\GetOrgDetailsJob;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use Log;
use App\Services\Api\Traits\SavingIMSResponseTrait;

class GetCaseDocumentsFeature extends Feature
{
    use SavingIMSResponseTrait;
    public function handle(Request $request)
    {
        try{
            $orgCode = isset($request['org_code'])?$request['org_code']:'';
            $requestUrl    = url('/');
            if($orgCode) {
                $orgCodeArr['org_code'] = $orgCode;
                $validateOrgCode = $this->run(new ValidateOrgCodeJob($orgCodeArr));
                if($validateOrgCode['org_code'][0]!=false) {
                    return $this->respondWithCASEJsonError(404);
                }
            }
            $getOrganizationId = $this->run(new GetOrgDetailsJob($request['org_code']));
            $organizationId    = $getOrganizationId['organization_id'];
            $keyInfos = ['identifier' => $organizationId, 'prefix' =>'cfdocs_'];
            $data = $this->run(new GetFromCacheJob($keyInfos));
            if(isset($data['status']) && $data['status'] == "error" || empty($data['CFDocuments'])) {
                $data = $this->run(new GetAllCaseFrameworkDocumentsJob($orgCode,$requestUrl));
                if(!empty($data) && count($data) > 0) {
                    $this->run(new DeleteFromCacheJob($keyInfos));
                    $this->run(new SetToCacheJob($data,$keyInfos));
                    $data = $this->run(new GetFromCacheJob($keyInfos));
                }
            }
            return $this->respondWithCASEJsonSuccess($data);
        }
        catch(\Exception $ex){
            Log::error($ex);
            return $this->respondWithCASEJsonError(500);
        }
    }

    private function respondWithCASEJsonSuccess($data) {
        $responseReturn = response()->json($data, 200, [], JSON_UNESCAPED_SLASHES);
        $this->IMSResponse($responseReturn);
        return $responseReturn;
    }

    private function respondWithCASEJsonError($httpStatus) {
        $errorResponseBody = [
            "imsx_codeMajor" => "failure",
            "imsx_severity" => "error",
            "imsx_description" => ""
        ];
        switch ($httpStatus) {
            case '400':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "invalid_selection_field"
                                                                                ];
                break;

            case '401':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "unauthorisedrequest"
                                                                                ];
                break;
                
            case '403':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "forbidden"
                                                                                ];
                break;

            case '404':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "Invalid UUID"
                                                                                ];
                break;

            case '429':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => "server_busy"
                                                                                ];
                break;

            default:
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "",
                                                                                    "imsx_codeMinorFieldValue" => "internal_server_error"
                                                                                ];
                break;
        }

        return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES);
    }

}
