<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithDownloadFileJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentJob;

use App\Domains\CaseStandard\Jobs\CreateCaseStandardDataJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageJob;
// use App\Domains\CaseStandard\Jobs\StoreCaseStandardJsonJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageWithActiveMetadataJob;
use Log;

class ExportCFPackadgeJsonFeature extends Feature
{
    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $documentIdentifier =   $request->route('documentId');
            $requestUrl         = url('/');
            //$exportKey          =   0;
            $validationStatus = $this->run(new ValidateDocumentJob($documentIdentifier));
            $is_html = $request->input('is_html', '');
            $additionalParams = (null!==$request->input('versioning') && $request->input('versioning')==='true') ? ['versioning'=>$request->input('versioning')] : [];  //Added to check request is for versioning - UF-1871
            
            if($validationStatus===true) {
                // $packageData = $this->run(new GetCasePackageJob($documentIdentifier));
                $packageData = $this->run(new GetCasePackageWithActiveMetadataJob($documentIdentifier, $request->input('auth_user'),'',$is_html,'',$requestUrl,$additionalParams));

                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $packageData, $message, $_status));
                
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
