<?php
namespace App\Services\Api\Features;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\Item\Jobs\GetItemJob;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\CaseAssociation\Jobs\CreateCFAssociationUriJob;
use App\Domains\CaseAssociation\Jobs\CreateItemAssociationsJob;
use App\Domains\CaseAssociation\Jobs\GetItemCreatedAssociationJob;
use App\Domains\CaseAssociation\Jobs\CreateItemStandardValidationJob;
use App\Domains\CaseAssociation\Jobs\CreateItemAssociationValidationJob;
use App\Domains\CaseAssociation\Jobs\OriginDestinationNodeAssociationTypeExistStatusJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use App\Domains\Association\Jobs\DocumentSelfIsChildOfValidationJob;
use Log;
use DB;

class CreateItemAssociationFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait;
    public function handle(Request $request)
    {
        try{
            $requestData = $request->input();
            if($requestData['project_type'] == 1) {
                $validation = $this->run(new CreateItemAssociationValidationJob($requestData));
                $validation = true;
            } else {
                $validation = $this->run(new CreateItemStandardValidationJob($requestData));
            }

            $selfAssociation = true;
            if(in_array($requestData['origin_node_id'],$requestData['destination_node_ids']))
            {
                $selfAssociation = false;
            }

            if($selfAssociation==true)
            {
                $docValidationResponse = $this->run(new DocumentSelfIsChildOfValidationJob($requestData));
                
                if($docValidationResponse==true)
                {

                    if($validation===true) {
                        $associationToSave = $this->prepareAssociationDataToSave($requestData);
                        if(!empty($associationToSave)){

                            // save the association
                            $this->run(new CreateItemAssociationsJob($associationToSave, $requestData['project_type'], $requestData['project_id']));
                            // Add project type in all array of data
                            for($i=0;$i<count($associationToSave);$i++){
                                $associationToSave[$i]['project_type'] = $requestData['project_type'];
                            }
                            $responseData   =   $this->run(new GetItemCreatedAssociationJob($associationToSave));
                            
                            //event for project and document update(updated_at) start
                            event(new UpdateTaxonomyTimeEvent($requestData['origin_node_id']));
                            //event for project and document update(updated_at) end

                            $this->raiseEventToUploadTaxonomySearchDataToSqs($requestData['origin_node_id']);
                            // set response body
                            $successType = 'created';
                            $message = 'Association(s) created.';
                            $_status = 'created';
                            return $this->run(new RespondWithJsonJob($successType,  $responseData, $message, $_status));
                        }
                        else{
                            $errorType = 'found';
                            $message = "No new associations to create.";
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($errorType, $message, $_status));
                        }
                    }
                    else {

                        $errorType = 'validation_error';
                        $message = $this->messageArrayToString($validation);
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else
                {
                    $errorType = 'found';
                    $message = "Document can not be childof of it's node.";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
        }
        else
        {
            $errorType = 'validation_error';
            $message = "Can not create association with self";
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function prepareAssociationDataToSave(array $requestData): array {
        $reverseAssociationType =   0;
        $associationToSave =    [];
        $cfDocumentIdentifier  =    '';
        // extract the validated inputs
        $originNodeIdentifier       = $requestData['origin_node_id']; // standard_id
        $associationTypeIdentifier  = $requestData['association_type'];
        $destinationIds             = $requestData['destination_node_ids'];
        $description                = isset($requestData['description'])?$requestData['description']:'';
        if($requestData['project_type'] == 2) {
            $associationTypeIdentifier  =   3;
        }

        if($associationTypeIdentifier == '10') {
            $reverseAssociationType =   1;
            $associationTypeIdentifier  =   3;
        } else if($associationTypeIdentifier == '11') {
            $reverseAssociationType =   1;
            $associationTypeIdentifier  =   5;
        } else if($associationTypeIdentifier == '12') {
            $reverseAssociationType =   1;
            $associationTypeIdentifier  =   7;
        }

        //get origin node details
        $originTaxonomy = $this->run(new GetItemJob(['id' => $originNodeIdentifier]));

        // keep cfdocument id same as origin node if origin is of cfdoc node type
        $cfDocumentIdentifier = isset($originTaxonomy->document_id) ? $originTaxonomy->document_id : $originNodeIdentifier;
        $maxSeqQuery = DB::table('item_associations')->groupBy('source_item_id')->where('source_item_id','=', $originNodeIdentifier)->get(['source_item_id', DB::raw('MAX(sequence_number) as max_seq')])->first();
        if($maxSeqQuery)
        {
            $maxSeq =  $maxSeqQuery->max_seq;
        }
        else {
            $maxSeq = 1;
        }
        $organizationId = $requestData["auth_user"]["organization_id"];
        $currentDateTime = now()->toDateTimeString();

        // loop throught them and prepare association data
        foreach($destinationIds as $destinationNodeIdentifier) {

            $createDataForAssociationExistsChecking = [
                'source_item_id'        => $originNodeIdentifier,
                'association_type'      => $associationTypeIdentifier,
                'target_item_id'   => $destinationNodeIdentifier,
                //'is_reverse_association'=> 0
            ];

            // get target document id based on destination node id start
            $documentIdentifier = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));
            $targetDocumentIdentifier = isset($documentIdentifier->document_id) ? $documentIdentifier->document_id : $destinationNodeIdentifier;

            // get target document id based on destination node id end

            if($reverseAssociationType == 1) {
                $createDataForAssociationExistsChecking = [
                    'source_item_id'        => $destinationNodeIdentifier,
                    'association_type'      => $associationTypeIdentifier,
                    'target_item_id'   => $originNodeIdentifier,
                    //'is_reverse_association'=> 1
                ];

                //get origin node details
                $reverseOriginTaxonomy = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));

                // keep cfdocument id same as origin node if origin is of cfdoc node type
                $reverseCfDocumentIdentifier = isset($reverseOriginTaxonomy->document_id) ? $reverseOriginTaxonomy->document_id : $destinationNodeIdentifier;
            }

            $checkAssociationExist = $this->run(new OriginDestinationNodeAssociationTypeExistStatusJob($createDataForAssociationExistsChecking));
            
            if($checkAssociationExist===false){ //
                $maxSeq = $maxSeq + 1;
                // create unique identifier
                $associationIdentifier = $this->createUniversalUniqueIdentifier();
                // create association uri
                $associationUri = $this->run(new CreateCFAssociationUriJob($associationIdentifier));
                if($associationTypeIdentifier == 4){
                    $destination = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));
                    if(empty($destination['full_statement']))
                    {
                        $destination = $this->run(new GetDocumentByIdJob(['id' => $destinationNodeIdentifier]));
                        $full_statement             =  !empty($destination['title']) ? $destination['title'] : "";
                        $external_node_url             =  "";
                    }
                    else
                    {
                        $full_statement             =  !empty($destination['full_statement']) ? $destination['full_statement'] : "";
                        $external_node_url          =  !empty($destination['uri']) ? $destination['uri'] : "";
                    }
                    $associationToSave[] = [
                        "item_association_id"           =>  $associationIdentifier,
                        "association_type"              =>  $associationTypeIdentifier,
                        "document_id"                   =>  $cfDocumentIdentifier,
                        "origin_node_id"                =>  $originNodeIdentifier,
                        "destination_node_id"           =>  $destinationNodeIdentifier,
                        "destination_document_id"       =>  $cfDocumentIdentifier,
                        "description"                   =>  $description,
                        "organization_id"               =>  $organizationId,
                        "source_item_association_id"    =>  $associationIdentifier,
                        "external_node_title"           =>  $full_statement,
                        "external_node_url"             =>  $external_node_url,
                        "created_at"                    =>  $currentDateTime,
                        "updated_at"                    =>  $currentDateTime,
                        "source_document_id"            =>  $cfDocumentIdentifier,
                        "source_item_id"                =>  $originNodeIdentifier,
                        "target_document_id"            =>  $targetDocumentIdentifier,
                        "target_item_id"                =>  $destinationNodeIdentifier,
                        "sequence_number"               =>  $maxSeq
                    ];
                }else if($reverseAssociationType == 1) {
                    $destination = $this->run(new GetItemJob(['id' => $originNodeIdentifier]));
                    //prepare the data
                    $associationToSave[] = [
                        "item_association_id"           =>  $associationIdentifier,
                        "association_type"              =>  $associationTypeIdentifier,
                        "document_id"                   =>  $reverseCfDocumentIdentifier,
                        "origin_node_id"                =>  $destinationNodeIdentifier,
                        "destination_node_id"           =>  $originNodeIdentifier,
                        "destination_document_id"       =>  $cfDocumentIdentifier,
                        "external_node_title"           =>  !empty($destination['full_statement']) ? $destination['full_statement'] : "",
                        "external_node_url"             =>  !empty($destination['uri']) ? $destination['uri'] : "",
                        "description"                   =>  $description,
                        "organization_id"               =>  $organizationId,
                        "source_item_association_id"    =>  $associationIdentifier,
                        "is_reverse_association"        =>  1,
                        "created_at"                    =>  $currentDateTime,
                        "updated_at"                    =>  $currentDateTime,
                        "source_document_id"            =>  $reverseCfDocumentIdentifier,
                        "source_item_id"                =>  $destinationNodeIdentifier,
                        "target_document_id"            =>  $cfDocumentIdentifier,
                        "target_item_id"                =>  $originNodeIdentifier,
                        "sequence_number"               =>  $maxSeq
                    ];
                } else {
                    $destination = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));
                    if(empty($destination['full_statement']))
                    {
                        $destination = $this->run(new GetDocumentByIdJob(['id' => $destinationNodeIdentifier]));
                        $full_statement             =  !empty($destination['title']) ? $destination['title'] : "";
                        $external_node_url             =  "";
                    }
                    else
                    {
                        $full_statement             =  !empty($destination['full_statement']) ? $destination['full_statement'] : "";
                        $external_node_url          =  !empty($destination['uri']) ? $destination['uri'] : "";
                    }
                    //prepare the data
                    $associationToSave[] = [
                        "item_association_id"           =>  $associationIdentifier,
                        "association_type"              =>  $associationTypeIdentifier,
                        "document_id"                   =>  $cfDocumentIdentifier,
                        "origin_node_id"                =>  $originNodeIdentifier,
                        "destination_node_id"           =>  $destinationNodeIdentifier,
                        "destination_document_id"       =>  $cfDocumentIdentifier,
                        "external_node_title"           =>  $full_statement,
                        "external_node_url"             =>  $external_node_url,
                        "description"                   =>  $description,
                        "organization_id"               =>  $organizationId,
                        "source_item_association_id"    =>  $associationIdentifier,
                        "created_at"                    =>  $currentDateTime,
                        "updated_at"                    =>  $currentDateTime,
                        "source_document_id"            =>  $cfDocumentIdentifier,
                        "source_item_id"                =>  $originNodeIdentifier,
                        "target_document_id"            =>  $targetDocumentIdentifier,
                        "target_item_id"                =>  $destinationNodeIdentifier,
                        "sequence_number"               =>  $maxSeq
                    ];
                }
            }
        }
        return $associationToSave;
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $itemIdentifier) {
        $sqsUploadEventConfig = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfig["update_item"];
        $eventsData = [
            "type_id" => $itemIdentifier,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventsData));
    }
}