<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Log;
use DB;
use App\Services\Api\Traits\AddDefaultSettings;
class SetDefaultSettingOrgFeature extends Feature
{
    use AddDefaultSettings;
    public function handle()
    {

        try{
            $queryData = DB::table('organizations')->select('organization_id')
            ->where('is_deleted','=',0)
            ->where('is_active','=',1)
            ->get()
            ->toArray();
            $organizationList = json_decode(json_encode($queryData),true);
            foreach($organizationList as $row){
                $this->updatedefaultconfigsettings($row['organization_id'],0,0,"Set");
            }
            
            $successType = 'custom_created';
            $message = 'Saved successfully.';
            $_status = 'custom_status_here';
            $is_success = 'true';
            
            return $this->run(new RespondWithJsonJob($successType, [], $message, $_status,[],$is_success));
            
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}