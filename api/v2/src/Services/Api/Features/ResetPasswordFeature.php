<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\User\Jobs\UpdatePasswordJob;
use App\Domains\User\Jobs\ValidateResetTokenJob;
use App\Domains\User\Jobs\PasswordExistJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Log;

class ResetPasswordFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $resultResetToken = $this->run(new ValidateResetTokenJob($requestData));
            if ($resultResetToken == 0) {
                // invalid token
                $successType = 'found';
                $message = 'Please contact administrator.';
                $_status = 'custom_status_here';
                $data = ['result' => 'invalid'];
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            } else if ($resultResetToken == 2) {
                // token time expired
                $successType = 'found';
                $message = 'Token has expired';
                $_status = 'custom_status_here';
                $data = ['result' => 'expired'];
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            } else if ($resultResetToken == 1) {
                $resultPasword = $this->run(new PasswordExistJob($requestData));
                if($resultPasword == 1)
                {
                      // valid token                
                        $this->run(new UpdatePasswordJob($requestData));
                        $response = [];
                        $successType = 'updated';
                        $message = 'Password updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else
                {
                    $successType = 'found';
                    $message = 'This password has been used previously, Please enter a new password';
                    $_status = 'custom_status_here';
                    $data = ['result' => 'exist'];
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
              
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
