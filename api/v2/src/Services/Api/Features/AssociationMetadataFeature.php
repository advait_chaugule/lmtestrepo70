<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Association\Jobs\GetAssociationMetadataJob;
use App\Domains\Association\Jobs\GetAssociationMetadataNodetypeIdJob;

class AssociationMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];
            $itemAssociationId          = $request->route('item_association_id');
            $associationTypeId          = $request->route('association_type_id');
            $itemId                     = $request->route('item_id');

            $itemAssociationIdExists = DB::table('item_associations')->where('item_association_id', $itemAssociationId)->where('is_deleted', 0)->get()->count();
            if(!$itemAssociationIdExists) {
                $successType = 'custom_found';
                $_status = 'custom_status_here';
                $message = ['Item association id not found'];
                $is_success = false;

                return $this->run(new RespondWithJsonJob($successType, [], $message,$_status,[],$is_success));
            }

            $nodetypeid = $this->run(new GetAssociationMetadataNodetypeIdJob($userId, $organizationId, $itemAssociationId, $associationTypeId, $itemId));
            $response = $this->run(new GetAssociationMetadataJob($organizationId, $itemAssociationId, $nodetypeid));

            $successType = 'custom_found';
            $_status = 'custom_status_here';
            $message = (!empty($response)) ? ['Data found.'] : ['Data Not found.'];
            $is_success = (!empty($response)) ? true : false;

            return $this->run(new RespondWithJsonJob($successType, $response, $message,$_status,[],$is_success));

          }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}