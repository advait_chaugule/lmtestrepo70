<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\WorkflowStage\Jobs\ValidateWorkflowStageRoleJob;
use App\Domains\WorkflowStage\Jobs\ExistingStageRoleJob;
use App\Domains\WorkflowStage\Jobs\DeleteStageRoleJob;
use Log;

class stageRoleDeleteFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $workflow_stage_id = $request->route('workflow_stage_id');
            $role_id = $request->route('role_id');
            $input["workflow_stage_role_id"] = trim($workflow_stage_id)."||".trim($role_id);
            $identifier = ['workflow_stage_role_id' => $input["workflow_stage_role_id"]];
            $validationStatus = $this->run(new ValidateWorkflowStageRoleJob($identifier));
           
            if($validationStatus === true) {
                $workflowStageArr = explode('||', $workflow_stage_id);
            $workflowId = $workflowStageArr[0];
            $workkflowInput["workflow_id"] = $workflowId;
        
                $existingStageRoleStatus = $this->run(new ExistingStageRoleJob($workkflowInput));
               if($existingStageRoleStatus === true)
               {
                    $deleteRole = $this->run(new DeleteStageRoleJob($input));
                    $response = $deleteRole;
                    $successType = 'found';
                    $message = 'Data removed successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));    
               }
               else
               {
                $errorType = 'validation_error';
                $message = $existingStageRoleStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
               }
               
            }else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid workflow and role id.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
