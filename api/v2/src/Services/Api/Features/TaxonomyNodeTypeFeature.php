<?php
namespace App\Services\Api\Features;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Report\Jobs\ValidateDocumentExistJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\Taxonomy\Jobs\GetTaxonomyNodeDetailsJob;
use App\Domains\Project\Jobs\GetProjectNodeDetailsJob;
use Log;
class TaxonomyNodeTypeFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData    = $request->all();
            $organizationId  = isset($requestData['auth_user']['organization_id'])?($requestData['auth_user']['organization_id']):$request->get('organization_id');  
            if(empty($organizationId))   {                                      //for front-facing
                        $errorType = 'custom_not_found';
                        $message   = 'Organization_id not found';
                        $_status   = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status,[],false));
                }  
            $documentId     = $request->route('document_id');
            $type           = $request->route('type');
            $selection      = strtolower($request->route('view_type'));
            $returnOrder   = $request->get('return_order');   
           
            if($type==1) { // 1 for taxonomy
                $validateDocumentId = $this->run(new ValidateDocumentExistJob(['document_id' => $documentId]));
                if ($validateDocumentId===true){
                    $nodeDetails = $this->run(new GetTaxonomyNodeDetailsJob($documentId, $organizationId,$selection,$returnOrder));
                    $resultData  = $nodeDetails;
                    $successType = 'custom_found';
                    $_status     = 'custom_status_here';
                    $message     = !empty($resultData) ? 'Data Found.' : 'Data Not Found.';
                    $is_success  = !empty($resultData) ? 'true' : 'false';
                    return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status, [], $is_success));

                }else{
                    $errorType = 'validation_error';
                    $message = 'Invalid document Id';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }else if($type=="2") // 2 for project
            {
                $projectId = $request->route('document_id');// From Angular side projectId Will come
                $validateProjectId = $this->run(new ValidateProjectIdJob(['project_id'=>$projectId]));
                if($validateProjectId===true) {
                    $projectDetails = $this->run(new GetProjectNodeDetailsJob($projectId,$organizationId,$selection,$returnOrder));
                    $resultData  = $projectDetails;
                    $successType = 'custom_found';
                    $_status     = 'custom_status_here';
                    $message     = !empty($resultData) ? 'Data Found.' : 'Data Not Found.';
                    $is_success  = !empty($resultData) ? 'true' : 'false';
                    return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status, [], $is_success));
                }else{
                    $errorType = 'validation_error';
                    $message   = 'Invalid project Id';
                    $_status   = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}