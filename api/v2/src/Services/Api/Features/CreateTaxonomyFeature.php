<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Taxonomy\Jobs\CreateTaxonomyJob;
use App\Domains\Taxonomy\Jobs\ValidateCreateDocumentJob;
use Log;

class CreateTaxonomyFeature extends Feature
{
    use UuidHelperTrait;

    public function handle(Request $request)
    {
        try{
            $input = $request->input();
            $input['documentTitle'] = $input['document_title'];
            $identifier = $this->createUniversalUniqueIdentifier();
            $input['document_id'] = isset($request['document_id'])?$request['document_id']:$identifier;           
           
            $input['documentId'] = $input['document_id'];
            $documentId = $request->input('document_id');
            $userID = $request->input('auth_user');
            $requestUrl = url('/');
            $validationStatus = $this->run(new ValidateCreateDocumentJob($input));
            
            if($validationStatus!==true) {
                // add organization id of the user performing the action
                $userPerformingAction = $request->input("auth_user");
                $document = $this->run(new CreateTaxonomyJob($input,$requestUrl));
               
                $successType = 'created';
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $document, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Document id is already exists';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            } 
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
