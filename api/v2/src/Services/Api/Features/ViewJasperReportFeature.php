<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportConnectionJob;
use App\Domains\JReport\Jobs\ViewJasperReportJob;
use App\Domains\JReport\Jobs\SetContentTypeJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

class ViewJasperReportFeature extends Feature
{
    public function handle(Request $request)
    {
        
        $requestData = $request->all();
        $connection= $this->run(new JReportConnectionJob());
        $report_type=$requestData['report_type'];
        $file_type=$requestData['format'];

        if($report_type!='TaxonomyDetail')
        {
            $organization_id['organization_id'] = $requestData['auth_user']['organization_id'];
        }
        else
        {
            $organization_id['orgid'] = $requestData['auth_user']['organization_id'];
        }
        
        $user_id['user_id'] = $requestData['auth_user']['user_id'];
        unset($requestData['report_type']);
        unset($requestData['auth_user']);
        unset($requestData['format']);
        
        $controls=array_merge($requestData,$organization_id);
        $controls=array_merge($controls,$user_id);
        
        if($report_type!='TaxonomyDetail')
        {
            $content=$this->run(new ViewJasperReportJob($connection,$organization_id['organization_id'],$file_type,$report_type,$controls));
        }
        else
        {
            $content=$this->run(new ViewJasperReportJob($connection,$organization_id['orgid'],$file_type,$report_type,$controls)); 
        }    

        $this->run(new SetContentTypeJob($file_type,$content,$report_type));

       

    }
}
