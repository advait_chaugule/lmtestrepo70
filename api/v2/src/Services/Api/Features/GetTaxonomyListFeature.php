<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\LinkedServer\Jobs\ValidateLinkedServerIdJob;
use App\Domains\LinkedServer\Jobs\GetTaxonomyListJob;
use Log;

class GetTaxonomyListFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
           
            $linkedServerIdentifier =   $request->route('linked_server_id');
            $requestData        =   $request->all();
            $identifier     =   ['linked_server_id'  =>  $linkedServerIdentifier];
            $validateLinkedServer    =   $this->run(new ValidateLinkedServerIdJob($identifier));
            if($validateLinkedServer === true) {
                $taxonomyList    =   $this->run(new GetTaxonomyListJob($identifier));
                if($taxonomyList  !== false)
                {
                    $successType = 'found';
                    $message = 'Data Found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $taxonomyList, $message, $_status));
             
                }
                else
                {
                    $response = ''; 
                    $successType = 'found';
                    $message = 'Data Not Found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

                }
            }
            else {
                $errors = $validateLinkedServer;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
