<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ValidateRoleByIdJob;
use App\Domains\Role\Jobs\CheckRoleIsDeleteJob;
use App\Domains\Role\Jobs\DeleteRoleJob;
use App\Domains\Role\Jobs\ExistingRoleInWorkflowJob;
use Log;

class DeleteRoleFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $roleIdentifier = $request->route('role_id');
            $identifier = ['role_id' => $roleIdentifier];

            $validationStatus = $this->run(new ValidateRoleByIdJob($identifier));
            if($validationStatus === true) {
                $checkRoleIsDeleted = $this->run(new CheckRoleIsDeleteJob($roleIdentifier));
                if(!$checkRoleIsDeleted === true) {
                    $existingRoleInWorkflow = $this->run(new ExistingRoleInWorkflowJob($identifier));
                    if($existingRoleInWorkflow === true) {
                        
                            $deleteRole = $this->run(new DeleteRoleJob($roleIdentifier));
                            
                            $response = $deleteRole;
                            $successType = 'found';
                            $message = 'Data updated successfully.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                            }
                            else
                            {
                                $errorType = 'validation_error';
                                $message = $existingRoleInWorkflow;
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                            }
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Role is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid role.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
