<?php
namespace App\Services\Api\Features;

use App\Domains\Notifications\Jobs\ValidateNotificationId;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Notifications\Jobs\GetNotificationJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Log;
class GetNotificationFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            //print_r($requestData);  die;
            $userId      = $requestData['auth_user']['user_id'];
            $organizationId   = $requestData['auth_user']['organization_id'];
            $identifiers      = ['user_id'=>$userId,'organization_id' =>$organizationId];
            $validationStatus = $this->run(new ValidateNotificationId($identifiers));
            if($validationStatus===true) {
            $getNotifications = $this->run(new GetNotificationJob($identifiers));
            if($getNotifications) {
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $getNotifications, $message, $_status));
            }else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'No records found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid user Id.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}