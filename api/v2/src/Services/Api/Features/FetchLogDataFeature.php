<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\AwsLog\Jobs\FetchLogDataForGivenTimeJob; 
use Log;

class FetchLogDataFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData    =   $request->all();
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            $fromDate       =   $requestData['from_date'];

            $readLogDataForDate    =   $this->run(new FetchLogDataForGivenTimeJob($fromDate, $organizationIdentifier));

            //return view('awslogdisplay', ['dataFromLogFile' => $readLogDataForDate]);
            ///return view('api::awslog.awslogdisplay', ['dataFromLogFile' => $readLogDataForDate]);
            //$view = view("api::awslog.awslogdisplay",['dataFromLogFile' => $readLogDataForDate])->render();
            return response()->json(['html'=>$readLogDataForDate]);
        } catch(\Exception $ex){
            Log::error($ex);
        }
    }
}
