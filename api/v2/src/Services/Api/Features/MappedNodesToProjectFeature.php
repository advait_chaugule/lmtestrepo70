<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\GetMappedNodesForProjectJob;
use App\Domains\Item\Jobs\GetChildrenNodesJob;
// use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeJob;
// use App\Domains\Project\Jobs\GetDocumentIdJob;
// use App\Domains\Project\Jobs\GetProjectItemsJob;
use Log;

class MappedNodesToProjectFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $treeData = [];
            //Get project identifier from route
            $projectId = $request->route('project_id');
            $input = [ 'project_id' => $projectId];
            //Validate the project identifier
            $validateProjectExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateProjectExistence===true) {
                //Fetch list of mapped nodes for the selected project identifier
                $mappedNodes = $this->run(new GetMappedNodesForProjectJob($input));

                // //Get the tree view for each mapped node 
                foreach($mappedNodes as $value){
                    $treeData[] = $this->run(new GetChildrenNodesJob($value));
                }
                // $documentIdentifier = $this->run(new GetDocumentIdJob($projectId));
                // $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeJob($documentIdentifier));
                // $taxonomyHierarchy['selected_nodes'] = $this->run(new GetProjectItemsJob($projectId));
                
                /*** Create the success response ***/
                $successType = 'found';
                $message = 'Mapped Node listed successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $treeData, $message, $_status));
            }
            else {
                $errors = $validateProjectExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            
             $errorType = 'internal_error';
             $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
             $_status = 'custom_status_here';
             Log::error($ex);
             return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
