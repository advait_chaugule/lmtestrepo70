<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\LinkedServer\Jobs\ValidateLinkedServerIdJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseServerTaxonomyApiJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonSpecificationJob;
use App\Domains\CaseStandard\Jobs\ExtractCaseFrameworkDocumentFromUploadedJsonContentJob;
use App\Domains\Document\Jobs\CaseServerDocumentJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Import\Jobs\CreateImportProcessJob;

use Log;
use App\Domains\Taxonomy\Events\CaseApiBackgroundEvent;
use Illuminate\Support\Facades\DB;

class ImportCFPackadgeApiFeature extends Feature
{
    use ErrorMessageHelper, UuidHelperTrait, ArrayHelper;

    public function handle(Request $request)
    {
        ini_set('memory_limit','1024M');
        try{
            $requestData                = $request->all();
            $requestUrl                 = url('/');
            $linkedServerId             = isset($requestData['linked_server_id']) ? $requestData['linked_server_id'] : "";
            $document_id                = isset($requestData['document_id']) ? $requestData['document_id'] : "";
            $inputIdentifier            = ["linked_server_id" => $linkedServerId, "document_id" => $document_id];
            $isReadyToCommit            = $request->input('is_ready_to_commit_changes');
            $sourceIdentifier           = !empty($request->input('document_id')) ? $request->input('document_id') : "";
            $importRequestIdentifier    = !empty($request->input('import_identifier')) ? $request->input('import_identifier') : "";
            $caseServer                 = 0;
            $importType                 = $request->input('import_type');
            $copy                       = 0;            
            $serverName                 = str_replace('server', '', $requestUrl);
            $requestingUserDetails      = $request->input("auth_user");
            $organizationId             = $requestingUserDetails['organization_id'];
            $importStatus = ['2'];
            
             $importData = DB::table('import_job')->select('import_job_id')->where('source_document_id',$sourceIdentifier)->where('organization_id',$organizationId)->whereIn('status',$importStatus)->first();
            if($importData)
            {
                $errorType = 'validation_error';
                $message = "This taxonomy is already in process. Please select different taxonomy to import.";
                $_status = 'Taxonomy Process already Started.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }
            $validateLinkedServer       = $this->run(new ValidateLinkedServerIdJob($inputIdentifier)); // Check linked server id passed is valid
            if($validateLinkedServer === true) {

                $validateCaseServerTaxonomyApi = $this->run(new ValidateCaseServerTaxonomyApiJob($inputIdentifier));// Check combination of server id & document idpassed is valid
                if($validateCaseServerTaxonomyApi !== false)
                {
                    if($validateCaseServerTaxonomyApi['error_no'] == 0)
                    {
                        $caseStandardTaxonomyData   = $validateCaseServerTaxonomyApi['result'];
                        $caseStandardTaxonomyData   = $this->convertJsonStringToArray($caseStandardTaxonomyData);
                        $caseServer                 = $this->run(new CaseServerDocumentJob($document_id,$organizationId));
                        if($caseServer == true || $importType == 2) {
                            $copy = 1;
                        }
                        if(isset($isReadyToCommit) && $isReadyToCommit == 1) {
                            // verify if content is compliant with CASE specifications
                            $validateCASESpecification = $this->run(new ValidateCaseStandardJsonSpecificationJob($caseStandardTaxonomyData));
                            if(!empty($validateCASESpecification) && $validateCASESpecification["status"]===true){

                                switch ($importType) {
                                    case '1':
                                    case '2':
                                    case '3':
                                        $eventData  =  [
                                                    'identifier'=>$inputIdentifier,
                                                    'user_data'=>$requestingUserDetails,
                                                    'import_identifier' => $importRequestIdentifier,
                                                    'import_type'=>$importType,
                                                    'case_server'=>$caseServer,
                                                    'request_url'=>$requestUrl,
                                                    'organization_id'=>$organizationId,
                                                    'copy'=>$copy,
                                                    'domain_name' => $serverName,
                                                    ];
                                        event (new CaseApiBackgroundEvent($eventData)) ;

                                        break;
                                    default:{
                                        $errorType = 'validation_error';
                                        $message = $validateCASESpecification['message'];
                                        $_status = 'Invalid file.';                                        
                                        break;
                                    }                                    
                                }
                                if(isset($_status) && $_status = 'Invalid file.'){
                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                }
                                $successType = 'custom_found';
                                $message = 'Import process has been sent to background.';
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status,[],true)); 
                            }
                            else {
                                $errorType = 'validation_error';
                                $message = $validateCASESpecification['message'];
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                            }            
                        }
                        else
                        {
                            $fileIdentifier = $this->createUniversalUniqueIdentifier();
                            $inputProcessInput['import_job_id']= $fileIdentifier;
                            $inputProcessInput['results']= '';
                            $inputProcessInput['errors']= '';
                            $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                            $inputProcessInput['organization_id']= $organizationId;
                            $inputProcessInput['source_document_id']= $sourceIdentifier;
                            $inputProcessInput['process_type']= 2;
                            $inputProcessInput['status']= 1;
                            $inputProcessInput['created_at']= now()->toDateTimeString();
                            $this->run(new CreateImportProcessJob($inputProcessInput));

                            if($caseServer == true) {
                                $successType = 'custom_found';
                                // please note client is using the message string to put conditions for clone popup ##so never change this
                                $message = "This taxonomy already exists. Please choose following option for upload";
                                $_status = 'custom_status_here';
                                $data = ['import_type' => [2],'package_exist' => 1,'import_identifier' => $fileIdentifier];
                                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                            } else {
                                $successType = 'custom_found';
                                // please note client is using the message string to put conditions for clone popup ##so never change this
                                $message = "Please choose following option for upload";
                                $_status = 'custom_status_here';
                                $data = ['import_type' => [1,2,3],'package_exist' =>$caseServer,'import_identifier' => $fileIdentifier];
                                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                            }
                        }
                    }
                    else
                    {
                        $errorType = 'validation_error';
                        $message = "Destination server is not responding";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));

                    }
                }
                else
                {
                    $errorType = 'validation_error';
                    // please note client is using the message string to put conditions for clone popup ##so never change this
                    $message = "Please select valid taxonomy for import..";
                    $_status = '';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));

                }
            }
            else
            {
                $errors = $validateLinkedServer;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}
