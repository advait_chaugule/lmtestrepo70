<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ListRoleJob;
use App\Domains\Role\Jobs\UsageCountRoleJob;
use Log;

class ListRoleFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $organizationId = $requestData['auth_user']['organization_id'];
            $roleIdArray = [];
            $roleList = $this->run(ListRoleJob::class, $request);
            
            foreach($roleList['roles'] as $roles){
                $roleIdArray[] = $roles->role_id;
            }
            
            $usageCount = $this->run(new UsageCountRoleJob($roleIdArray, $organizationId));

            $response = ['RoleList' => $roleList, 'RoleUsageCount' => $usageCount];

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
