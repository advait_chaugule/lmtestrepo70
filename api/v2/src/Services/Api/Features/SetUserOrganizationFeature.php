<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\User\Jobs\ValidateOrganizationInputJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\User\Jobs\SetUserOrganizationListJob;
use App\Domains\User\Jobs\SwitchUserOrganizationJob;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\User\Jobs\CheckUserOrganizationJob;
use Log;

class SetUserOrganizationFeature extends Feature
{
    use StringHelper, ErrorMessageHelper;
    public function handle(Request $request)
    {
        try {
            $input = $request->all();
            $userId = $input['auth_user']['user_id'];
            $organizationId = $input['organization_id'];
            $identifier = ['user_id' => $userId, 'organization_id' => $organizationId];
            $validationStatus = $this->run(new ValidateOrganizationInputJob($input));
            if ($validationStatus===true) {
                $this->run(new SetUserOrganizationListJob($identifier));
                $data = $this->run(new CheckUserOrganizationJob($identifier));
                if(!$data){
                    $errorType = 'authorization_error';
                    $message   =  'Invalid organization Id';
                    $_status   = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
                $switchLogin   = $this->run(new SwitchUserOrganizationJob($identifier));
                $response      = $switchLogin['responseBody'];
                if($response) {
                    $successType = 'found';
                    $message = 'Login successful.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }else{
                    $errorType = 'authorization_error';
                    $message = $switchLogin["responseMessageList"];//$this->arrayContentToCommaeSeparatedString();
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }else{
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}