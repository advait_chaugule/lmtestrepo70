<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\DeleteSubscribedTaxonomyJob;

class DeleteSubscriptionFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestDetails = $request->all();
            $inputData['subscriptionId']    = (null!==$request->input('subscription_id')) ? $request->input('subscription_id') : '';
            
            if(!empty($inputData['subscriptionId'])){
                $deleteSubscription = $this->run(new DeleteSubscribedTaxonomyJob($inputData['subscriptionId']));
                if($deleteSubscription){
                    $successType = 'custom_found';
                    $message = 'Taxonomy subscription deleted successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));
                }
                else{ // If no record inserted, it means already exists in table
                    $successType = 'custom_not_found';
                    $message = 'Taxonomy subscription not found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
            }
            else{
                $successType = 'custom_not_found';
                $message = 'Subscription Id, Status is mandatory.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
