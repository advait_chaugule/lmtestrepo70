<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ListPublishedDocumentRootJob;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;

class GetDocumentListFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try {

            $requestData            =   $request->all();
            $organizationId     =   $request->route('organization_id');
            

            $keyInfo = array('identifier'=>$organizationId, 'prefix'=>'docs');
            $status = $this->run(new GetFromCacheJob($keyInfo));
            //$status = $this->run(new DeleteFromCacheJob($keyInfo));
            if(isset($status['status']) && $status['status']=="error"){
                $requestData["organization_id"] = $organizationId; 
                $documents = $this->run(new ListPublishedDocumentRootJob($requestData));
                
                if(!empty($documents) && count($documents) > 0){
                    $status1 = $this->run(new DeleteFromCacheJob($keyInfo));
                    $status2 = $this->run(new SetToCacheJob($documents, $keyInfo));
                    $status3 = $this->run(new GetFromCacheJob($keyInfo));
                    $message = 'Data found.';

                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status3, $message, $_status));
                }
                else
                {
                    $message = 'Data not found.';

                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
                }
            }else
            {
                $message = 'Data found.';
               $successType = 'found';
              $_status = 'custom_status_here';
                 return $this->run(new RespondWithJsonJob($successType, $status, $message, $_status));
            }
                

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
