<?php
namespace App\Services\Api\Features;

use App\Domains\Asset\Jobs\UpdateAssetJob;
use App\Domains\Asset\Jobs\ValidateAssetByIdJob;
use App\Domains\Asset\Jobs\ValidateAssetCreateInputJob;
use App\Domains\Caching\Events\CachingEvent;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;

class UpdateAssetFeature extends Feature
{
    public function handle(Request $request)
    {
       try {
            $requestData = $request->all();

            $inputData = ['title' => !empty($requestData['title']) ? $requestData['title']: "",
                'description' => !empty($requestData['description']) ? $requestData['description']: "",
                'organization_id' => !empty($requestData["auth_user"]["organization_id"]) ?$requestData["auth_user"]["organization_id"]: "" ];
                $assetId = $request->route('asset_id');
                $organizationId = $inputData['organization_id'];
                $assetIdentifier = ['asset_id' => $assetId];
                $validateassetStatus = $this->run(new ValidateAssetByIdJob($assetIdentifier));


               if ($validateassetStatus === true) {
                        $updateAssetEntity = $this->run(new UpdateAssetJob($assetId, $requestData));
                        // caching
                       $eventType1 = config("event_activity")["Cache_Activity"]["FILES_GETALL"];
                       $eventDetail = [
                           'event_type' => $eventType1,
                           'requestUserDetails' => $request->input("auth_user"),
                           "beforeEventRawData" => [],
                           'afterEventRawData' => ''
                       ];
                        event(new CachingEvent($eventDetail)); // notification ends here
                        $response = $updateAssetEntity;
                        $successType = 'found';
                        $message = 'Asset updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                   }
                else {

                    $errorType = 'validation_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Please select valid asset.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
