<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Report\Jobs\ValidateProjectExistJob;
use App\Domains\Project\Jobs\GetDocumentIdJob;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Project\Jobs\GetProjectRelatedTaxonomyCommentsJob;
use Log;

class GetProjectCommentsFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try {
            $projectIdentifier      =   $request->route('project_id');
            $requestingUserDetail   =   $request->input('auth_user');

            $validateProject    =   $this->run(new ValidateProjectExistJob(['project_id' => $projectIdentifier]));

            if($validateProject ===  true) {
                $documentIdentifier     =   $this->run(new GetDocumentIdJob($projectIdentifier));
                $organizationIdentifier =   $requestingUserDetail['organization_id'];

                $validateDocument   =   $this->run(new ValidateDocumentByIdJob(['document_id'   => $documentIdentifier]));

                if($validateDocument == true) {
                    $checkIfTaxonomyDeleted =   $this->run(new CheckDocumentIsDeleteJob($documentIdentifier));

                    if($checkIfTaxonomyDeleted == false) {
                    
                        $commentList = $this->run(new GetProjectRelatedTaxonomyCommentsJob($documentIdentifier, $organizationIdentifier));

                        if(count($commentList) > 0) {
                            $response   =   $commentList;
                            $successType = 'found';
                            $message = 'Data found.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                        } else {
                            $response   =   $commentList;
                            $successType = 'not_found';
                            $message = 'Data not found.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                        }
                    } else {
                        $errorType = 'validation_error';
                        $message = "This taxonomy is deleted";
                        $_status = '';
                        return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
                    }
                } else {
                    $errorType = 'validation_error';
                    $message = "Please select a prject with valid taxonomy";
                    $_status = '';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
                }
            } else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validateProject);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

            
        } catch (\Exception $ex) {
            //dd($ex);
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
