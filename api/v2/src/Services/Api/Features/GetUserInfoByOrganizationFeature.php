<?php
namespace App\Services\Api\Features;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use Log;

class GetUserInfoByOrganizationFeature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        try {
            $requestData    =   $request->all();
            $organizationId =   $requestData['auth_user']['organization_id'];
            $userId         =   $requestData['auth_user']['user_id'];
            $identifiers    =   ['user_id'=>$userId,'organization_id'=>$organizationId];
            $getUserEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob($identifiers));
            if($getUserEmailStatus) {
                $message = 'Email configuration.';
                $successType = 'found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $getUserEmailStatus, $message, $_status));
            }else{
                $errorType = 'No result found';
                $message = 'User Details not found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}