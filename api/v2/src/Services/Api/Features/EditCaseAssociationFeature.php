<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Domains\CaseAssociation\Jobs\EditCaseAssociatonValidationJob;
use App\Domains\CaseAssociation\Jobs\OriginDestinationNodeAssociationTypeExistStatusJob;
use App\Domains\CaseAssociation\Jobs\EditCaseAssociatonJob;

use App\Domains\CaseAssociation\Jobs\GetOriginNodeIdJob;
//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;
use Log;

class EditCaseAssociationFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait, CaseFrameworkTrait;

    public function handle(Request $request)
    {
        try{
            $dataToValidate = $request->input();
           $dataToValidate['association_id'] = $request->route('association_id');
           // $dataToValidate['association_type'] = $request->input('association_type_id');
            $validationProcessOutput = $this->run(new EditCaseAssociatonValidationJob($dataToValidate));
           
            if($validationProcessOutput===true) {
               
                // check assocaition type to update exists between origin and destination
              //  $associationTypeBetweenOriginAndDestinationNodeExists = $this->run(new OriginDestinationNodeAssociationTypeExistStatusJob($dataToValidate));
                //dd($associationTypeBetweenOriginAndDestinationNodeExists);
              //  if($associationTypeBetweenOriginAndDestinationNodeExists===false) {
                    $caseAssociationUpdated = $this->run(new EditCaseAssociatonJob($dataToValidate));

                    //event for project and document update(updated_at) start
                    event(new UpdateTaxonomyTimeEvent($dataToValidate['origin_node_id']));
                    //event for project and document update(updated_at) end
                    
                    if($caseAssociationUpdated!==false){
                        $successType = 'updated';
                        $message = 'Data updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $caseAssociationUpdated, $message, $_status));
                    }
                    else{
                        $errorType = 'internal_error';
                        $message = "Some internal error occurred. Please try again.";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
               /* }
                else {
                   $errorType = 'validation_error';
                    $message = "Association type between origin and destination node already exists.";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }*/
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationProcessOutput);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
