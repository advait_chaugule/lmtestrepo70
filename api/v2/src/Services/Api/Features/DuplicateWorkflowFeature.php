<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Workflow\Jobs\ValidateWorkflowInputJob;
use App\Domains\Workflow\Jobs\DuplicateWorkflowJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use DB;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Workflow\Jobs\CreateWorkflowJob;
use App\Domains\Stage\Jobs\CreateStageJob;
use App\Domains\WorkflowStage\Jobs\CreateWorkflowStageJob;
use App\Domains\WorkflowStage\Jobs\CreateWorkflowStageRoleJob;
use Log;

class DuplicateWorkflowFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;
    public function handle(Request $request)
    {
        try{
            
            $workflowIdentifier =   $request->route('workflow_id');
            $requestData        =   $request->all();
            $identifier         =   ['workflow_id'  =>  $workflowIdentifier];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            $validateWorkflow    =   $this->run(new ValidateWorkflowIdJob($identifier));
            
            if($validateWorkflow === true) {
                $workflowInput = ["name" => $requestData['name'],'organization_id' => $organizationIdentifier];
                $validateWorkflowInput = $this->run(new ValidateWorkflowInputJob($workflowInput));
                // validate the the workflow id with the tenant 
                
                if($validateWorkflowInput === true) {
                    $updatedWorkflow    =   $this->run(new DuplicateWorkflowJob($workflowIdentifier, $organizationIdentifier, $requestData));
                    /** the updateWorkflow variable contain name of the stage, workflow_stage_id, order*/
                    $getStages = $updatedWorkflow;
                    $stageNameArr = [];
                    $wfstageIdArr = [];
                    foreach($getStages as $getStagesK => $getStagesV)
                    {
                        $stageNameArr[$getStagesV->workflow_stage_id]['name']= $getStagesV->name;
                        $stageNameArr[$getStagesV->workflow_stage_id]['order']= $getStagesV->order;
                       $stageNameArr[$getStagesV->workflow_stage_id]['stage_description']= $getStagesV->stage_description;
                    //  array_push($stageNameArr,$getStagesV->name);
                      array_push($wfstageIdArr,$getStagesV->workflow_stage_id);
                    }

                   $getAllRoles = DB::table('workflow_stage_role as wsr')
                    ->select('wsr.role_id','wsr.workflow_stage_id')
                    ->whereIn('wsr.workflow_stage_id',$wfstageIdArr)
                    ->get();
                    
                    $stageRoleArr = [];
                    foreach($getAllRoles as $getAllRolesK =>$getAllRolesV)
                    {
                        //workflow
                        $stageRoleArr[$getAllRolesV->workflow_stage_id][] = $getAllRolesV->role_id;
                    }
                 
                    // insert the data into the database as per the record fetched and generated
                    $workflowInput['workflow_id']= $this->createUniversalUniqueIdentifier();
                    $workflowInput['organization_id']= $organizationIdentifier;
                    $workflowInput['name']= $requestData['name'];
                    $workflowInput['updated_at']= now()->toDateTimeString();
                    $workflowInput['created_at']= now()->toDateTimeString();
                    $workflowInput['updated_by']= $requestData['auth_user']['user_id'];

                    //creating a new workflow
                    $getDuplicateDataWorkflow =$this->run(new CreateWorkflowJob($workflowInput));
                   
                    // create new stage 
                    foreach($stageNameArr as $stageNameArrK => $stageNameArrV)
                    {
                        $StageData["stage_id"] = $this->createUniversalUniqueIdentifier();
                        $StageData["organization_id"] = $organizationIdentifier;
                        $StageData["name"] = $stageNameArrV['name'];
                        $StageData["order"] = $stageNameArrV['order'];
                        $StageData["stage_description"] = $stageNameArrV['stage_description'];
                        $StageData["is_deleted"] = 0;
                       $createStageJob = $this->run(new CreateStageJob($StageData));

                        $workflowstageInput['workflow_stage_id'] = $workflowInput['workflow_id'].'||'.$StageData["stage_id"];
                        $workflowstageInput['workflow_id'] = $workflowInput['workflow_id'];
                        $workflowstageInput['stage_id'] = $StageData["stage_id"];
                        $workflowstageInput['order'] = $StageData["order"];
                        $workflowstageInput['stage_name'] =  $StageData["name"];
                        $workflowstageInput['stage_description'] =  $StageData["stage_description"];
                        //dump($workflowstageInput);
                        $worflowStageCreated = $this->run(new CreateWorkflowStageJob($workflowstageInput));

                       $worflowStageArr[$stageNameArrK] = $workflowstageInput['workflow_stage_id'];
                       unset($StageData);
                       unset($workflowstageInput);

                    }
                    foreach($worflowStageArr as $worflowStageArrK => $worflowStageArrV)
                    {
                        $roleArr = $stageRoleArr[$worflowStageArrK];
                      
                        foreach($roleArr as $roleArrK => $roleArrV)
                        {
                            $workflowStageRoleData['workflow_stage_role_id'] = $worflowStageArrV."||".$roleArrV;
                            $workflowStageRoleData['workflow_stage_id'] = $worflowStageArrV;
                            $workflowStageRoleData['role_id'] = $roleArrV;
                            $worflowStageRoleCreated = $this->run(new CreateWorkflowStageRoleJob($workflowStageRoleData));
                            unset($workflowStageRoleData);
                        }
                    }
                    $response = $getDuplicateDataWorkflow;
                    $successType = 'found';
                    $message = 'Duplicated Data Created.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else
                {
                    $errorType = 'validation_error';
                    $message = $validateWorkflowInput;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
                } else {
                $errors = $validateWorkflow;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }                     
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}