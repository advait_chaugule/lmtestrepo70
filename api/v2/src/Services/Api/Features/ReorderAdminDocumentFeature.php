<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use App\Domains\Document\Jobs\ReorderAdminDocumentJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Log;

class ReorderAdminDocumentFeature extends Feature
{
    public function handle(Request $request)
    {

        try {
            $requestData = $request->all();
            $organizationIdentifier = $requestData['auth_user']['organization_id'];
            $updatedFile = $this->run(new ReorderAdminDocumentJob($organizationIdentifier, $requestData));
            // caching
            $eventType1 = config("event_activity")["Cache_Activity"]["FILES_GETALL"];
            $eventDetail = [
                'event_type' => $eventType1,
                'requestUserDetails' => $request->input("auth_user"),
                "beforeEventRawData" => [],
                'afterEventRawData' => ''
            ];
            event(new CachingEvent($eventDetail)); // notification ends here
            $response = $updatedFile;
            $successType = 'found';
            $message = 'Asset ordered.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
