<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;

use App\Domains\Metadata\Jobs\InsertAdditionalMetadataJob;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class CreateDocumentAdditionalMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData        =   $request->all();
            $documentIdentifier =   $request->route('document_id');
            $documentId         =   ['document_id' => $documentIdentifier];
            

            $validateDocument   =   $this->run(new ValidateDocumentByIdJob($documentId));

            if($validateDocument === true) {
                $checkIsDocumentDeleted =   $this->run(new CheckDocumentIsDeleteJob($documentIdentifier));

                if($checkIsDocumentDeleted !== true) {
                    $addAdditionalMetadata = $this->run(new InsertAdditionalMetadataJob($documentIdentifier, $requestData, 'document'));

                    $this->raiseEventToUploadTaxonomySearchDataToSqs($documentIdentifier);
                    
                    if($addAdditionalMetadata !== false) {
                        $successType = 'created';
                        $message = 'Additional Metadata created successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $addAdditionalMetadata, $message, $_status));
                    }
                    else {
                        $errorType = 'internal_error';
                        return $this->run(new RespondWithJsonErrorJob($errorType));
                    }                    
                }
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $documentId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_document"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
