<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\LinkedServer\Jobs\ValidateLinkedServerIdJob;
use App\Domains\LinkedServer\Jobs\GetLinkedServerDetailJob;
use Log;

class GetLinkedServerFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $linkedServerIdentifier =   $request->route('linked_server_id');

            $requestData        =   $request->all();

            $identifier     =   ['linked_server_id'  =>  $linkedServerIdentifier];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            $getLinkServerInput = ['linked_server_id'  =>  $linkedServerIdentifier, 'organization_id' => $organizationIdentifier];
          
           if($linkedServerIdentifier != "")
           {

             $validateLinkedServer    =   $this->run(new ValidateLinkedServerIdJob($identifier));
            if($validateLinkedServer === true) {
                   
                        $getLinkedServer = $this->run(new GetLinkedServerDetailJob($getLinkServerInput));
                        $response = ''; 
                        $successType = 'found';
                        $message = 'Data Found';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $getLinkedServer, $message, $_status));

                   
            } else {
                $errors = $validateLinkedServer;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

           }
           else
           {
                $getLinkedServer = $this->run(new GetLinkedServerDetailJob($getLinkServerInput));
                if(count($getLinkedServer) > 0)
                {
                    $successType = 'found';
                    $message = 'Data Found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $getLinkedServer, $message, $_status));
             
                }
                else
                {
                    $response = ''; 
                    $successType = 'found';
                    $message = 'Data Not Found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

                }
           }
          

            
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
