<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ListProjectsJob;
use Illuminate\Support\Facades\DB;
use Log;

class ListProjectFeature extends Feature
{
    public function handle(Request $request)
    {
        
        try {
            $requestData     = $request->all();
            if(!isset($requestData['auth_user']['user_id']) && empty($requestData['auth_user']['user_id'])) 
            {
                $requestData['auth_user']['user_id'] = "";
            }
            if(!isset($requestData['auth_user']['organization_id']) && empty($requestData['auth_user']['organization_id']))  
            {
                $requestData['auth_user']['organization_id'] = $request->get('organization_id');
            }
            $request->merge($requestData);
            $projectlist = $this->run(ListProjectsJob::class, $request);
            if($projectlist!=null)
            {
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $projectlist, $message, $_status));
            }
            else
            {   

                $projectlist['projects']=array();
                $projectlist['count']=0;
                $projectlist['totalProjects']=0;
                $projectlist['totalPacingGuide']=0;

                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $projectlist, $message, $_status));
            }
            

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
