<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportConnectionJob;
use App\Domains\JReport\Jobs\CoverageReportJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\JReport\Jobs\SetContentTypeJob;
use App\Services\Api\Traits\CoverageReportHelperTrait;
use Log;
use DB;

class CoverageReportFeature extends Feature
{
    use CoverageReportHelperTrait;
    
    public function handle(Request $request)
    {
        try
        {
            $requestData = $request->all();
            $connection= $this->run(new JReportConnectionJob());
            $organizationId = $requestData['auth_user']['organization_id'];
            //Did the changes for UF-359 to pass response type for coverage report
            $file_type = isset($requestData['file_type']) ? $requestData['file_type'] : 'xls'; 
            $file_name = 'Association Coverage';
            $sourceData = $requestData['source_taxonomy_id'];
            $targetData = $requestData['target_taxonomy_id'];
            $sourceItems = '';
            $targetItems = '';
            $sourceNodeTypes = '';
            $targetNodeTypes = '';
           
            $sourceItemsArray = array();
            $sourceNodeTypesArray = array();
            $sourceTaxonomyId=$sourceData[0]['taxonomy_id'];
            $SourceitemSortArr = array();

            foreach($sourceData as $sourceDataK=>$sourceDataV)
            {
                 
                
                if(!empty($sourceDataV['item_id']))
                {
                    $sourceItemsArray= $sourceDataV['item_id'];
                }
                else
                {
                  
                    $sourceItemsdata = DB::table('items')->select('item_id')
                     ->where('document_id','=',$sourceTaxonomyId)
                     ->whereIn('node_type_id', $sourceDataV['node_type_id'])
                    ->where('is_deleted','!=', '1')
                    ->get()
                    ->toarray();
                   

                    $sourceItemsArray =  array_column($sourceItemsdata, "item_id");
                   
                    $sourceNodeTypesArray= $sourceDataV['node_type_id'];
                   
                }

                if (($key = array_search($sourceTaxonomyId, $sourceItemsArray)) !== false) {
                    unset($sourceItemsArray[$key]);
                }
            
               $SourceSortArray =  $this->getItemSort($sourceTaxonomyId, $sourceItemsArray);
         
               foreach($SourceSortArray as $val)
               {
                  
                   $SourceitemSortArr[$val->item_id] = $val->sortnum;
               }
            }


            $sourceItemsArray = array_values($sourceItemsArray);

            if(!empty($sourceItemsArray))
            {
                $sourceItems = implode(',', $sourceItemsArray);
            }
            else
            {
                $sourceNodeTypes = implode(',', $sourceNodeTypesArray);
            }    

            $targetTaxonomyArray = array();
            $targetItemsArray = array();
            $targetNodeTypesArray = array();
            $TargetitemSortArr = array();
            
         
            foreach($targetData as $targetDataK=>$targetDataV)
            {
                $targetTaxonomyArray[]=$targetDataV['taxonomy_id'];
                if(!empty($targetDataV['item_id']))
                {
                    $targetItemsArray=$targetItemsData=array_merge($targetItemsArray,$targetDataV['item_id']);
                }
                else
                {
                    
                    $targetItemsdata = DB::table('items')->select('item_id')
                    ->where('document_id', '=', $targetDataV['taxonomy_id'])
                    ->where('is_deleted', '!=', '1')
                    ->whereIn('node_type_id', $targetDataV['node_type_id'])
                    ->get()
                    ->toarray();

                    $targetItemsData =  array_column($targetItemsdata, "item_id");
                    $targetItemsArray =  array_merge($targetItemsArray, $targetItemsData);
                    $targetNodeTypesArray=array_merge($targetNodeTypesArray,$targetDataV['node_type_id']);
                }

                if (($key = array_search($targetDataV['taxonomy_id'], $targetItemsArray)) !== false) {
                    unset($targetItemsArray[$key]);
                }

                if (($key = array_search($targetDataV['taxonomy_id'], $targetItemsData)) !== false) {
                    unset($targetItemsData[$key]);
                }
         
      
                $TargetSortArray =  $this->getItemSort($targetDataV['taxonomy_id'], $targetItemsData);
        
                foreach($TargetSortArray as $val)
                {
                   
                    $TargetitemSortArr[$val->item_id] = $val->sortnum;
                   
                }
                
            }
            
           

            $targetItemsArray = array_values($targetItemsArray);

            $targetTaxonomyIds = implode(',', $targetTaxonomyArray);

            if(!empty($targetItemsArray))
            {
                $targetItems = implode(',', $targetItemsArray);
            }
            else
            {
                $targetNodeTypes = implode(',', $targetNodeTypesArray);
            }
            
            $trans_id = rand(10,100).substr(time(),6,10);
           
            $controls =
            [
                'source_document_id'=>$sourceTaxonomyId,
               // 'source_items'=>$sourceItems,
                'target_document_id'=>$targetTaxonomyIds,
              //  'target_items'=>$targetItems,
             //  'source_nodetypes'=>$sourceNodeTypes,
              //  'target_nodetypes'=>$targetNodeTypes,i
                'organization_id'=>$organizationId,
                'transaction_id' => $trans_id
            ];

        

          

            $cnt_ins = max(array(count($sourceItemsArray) , count($targetItemsArray) , count( $sourceNodeTypesArray), count($targetNodeTypesArray)));

            for($p = 0; $p < $cnt_ins; $p++)
            { 
               
               $sitem =  (isset($sourceItemsArray[$p]) && !empty($sourceItemsArray[$p])) ? $sourceItemsArray[$p] : '';
               $titem = (isset($targetItemsArray[$p]) && !empty($targetItemsArray[$p])) ? $targetItemsArray[$p] : '';
                
               
               $inputArr[$p] =
                [
                    'source_items'=> $sitem,
                    'target_items' => $titem,
                    'transaction_id' => $trans_id,
                    'source_nodetypes' =>  (isset($sourceNodeTypesArray[$p]) && !empty($sourceNodeTypesArray[$p])) ? $sourceNodeTypesArray[$p] : '',
                    'target_nodetypes' =>   (isset($targetNodeTypesArray[$p]) && !empty($targetNodeTypesArray[$p])) ? $targetNodeTypesArray[$p] : '',
                    'created_at' => date('Y-m-d h:i:s'),
                    'source_sorts' => (isset($SourceitemSortArr[$sitem]) && !empty($SourceitemSortArr[$sitem]))  ? $SourceitemSortArr[$sitem] : '',
                    'target_sorts' => (isset($TargetitemSortArr[$titem]) && !empty($TargetitemSortArr[$titem]))  ? $TargetitemSortArr[$titem] : ''
                ];
                


            }

         

            $array_ch = array_chunk($inputArr, 1000);
            
            foreach($array_ch as $val)
            {
                
                    DB::table('jasper_report_input')->insert($val);
            }


            $content=$this->run(new CoverageReportJob($connection,$file_type,$controls));
            $this->run(new SetContentTypeJob($file_type,$content,$file_name));

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}