<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Organization\Jobs\GetOrgDetailsJob;
use App\Domains\Organization\Jobs\ValidateOrganizationDomainJob;
use Log;
use App\Domains\AwsLog\Jobs\CloudFrontSignedCookiesJob;
class GetOrganizationDetailsFeature extends Feature
{
    public function handle(Request $request)
    {
        try {

            $requestData     =   $request->all();
            $serverUrl       =   url('/');
            $orgCode         =   $request->route('org_code');
//            $validateOrg     = $this->run(new ValidateOrganizationDomainJob($orgCode,$serverUrl));
//
//            if($validateOrg!=1) {
//                $successType = 'not_found_error';
//                $message = 'Domain is not belongs to this organizations';
//                $_status = 'custom_status_here';
//                return $this->run(new RespondWithJsonErrorJob($successType, $message, $_status));
//            }
            $CLoudFrontKeyPath = env('CLOUDFRONT_KEY_PATH');
            if(file_exists($CLoudFrontKeyPath)){
                $cookieName= 'CloudFront-Key-Pair-Id';
                if(!isset($_COOKIE[$cookieName])){
                    $this->run(new CloudFrontSignedCookiesJob());                   
                }
            }else{
                $orgDetail = $this->run(new GetOrgDetailsJob($orgCode));
            
                $response = $orgDetail;
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }            
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
