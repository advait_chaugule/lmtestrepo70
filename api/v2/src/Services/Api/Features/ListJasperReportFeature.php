<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\JReport\Jobs\ListJasperReportJob;
use Log;

class ListJasperReportFeature extends Feature
{
    public function handle(Request $request)
    {
        try 
        {
            $requestData                =   $request->all();
            $jasperReportList = $this->run(new ListJasperReportJob($requestData));
            if(!empty($jasperReportList))
            {
                $response = $jasperReportList;
                $successType = 'found';
                $message = 'Jasper List Found Successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else
            {
                $errorType = 'validation_error';
                $message = 'Jasper List Not Found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }    
        }
        catch(Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
