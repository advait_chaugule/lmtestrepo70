<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentV3Job;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV3Job;
use Log;
use Illuminate\Support\Facades\DB;

class GetTaxonomyHierarchyV3Feature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
       try
       {
            $documentIdentifier = $request->route('document_id');
            $requestingUserDetails = $request->input("auth_user");
            if($request->input('is_customview')){
                $isCustomview = $request->input('is_customview');
            }else{
                $isCustomview = 0;
            }
            $requestingUserDetails['is_customview'] = $isCustomview;

            $validationStatus = $this->run(new ValidateDocumentV3Job($documentIdentifier, $requestingUserDetails));

            if($validationStatus===true) 
            {
                    $successType = 'custom_found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    $taxonomyHierarchy = [];

                    if($request->isMethod('head'))
                    {
                        $document = DB::table('documents')
                                        ->select('documents.updated_at')
                                        ->where('documents.document_id', $documentIdentifier)
                                        ->where('documents.organization_id', $requestingUserDetails['organization_id'])
                                        ->first();

                        $headers = [
                            'LastModifiedDate' => $document->updated_at
                        ];
                        return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status, $headers, true));
                    }
                    $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeV3Job($documentIdentifier, $requestingUserDetails));
                    $headers = [
                        'LastModifiedDate' => $taxonomyHierarchy['modified_at']
                    ];
                    unset($taxonomyHierarchy['modified_at']);
                    return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status, $headers, true));
            }
            else
            {
                    $errorType = 'validation_error';
                    $message = $validationStatus;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }


       }
       catch(\Exception $ex)
        {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
