<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;

use App\Domains\Project\Jobs\GetNodesAssignedToProjectInTreeCompatibleFormJob;
use App\Domains\Project\Jobs\GetProjectByIdJob;

use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;

use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;
use App\Domains\Asset\Jobs\GetAssetListJob;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use Log;

class GetProjectFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {   
        ini_set('max_execution_time', 0);
        try {
            $requestData            =   $request->all();
            $projectIdentifier      =   $request->route('project_id');
            $projectSetType         =   $requestData['type'];
            $requestUserDetails     =   $requestData['auth_user'];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            $projectHierarchy       =   [];
            $requestUrl = url('/');
            $projectDet = $this->run(new GetProjectByIdJob(['project_id'    => $projectIdentifier]));
                
            if($projectDet['is_deleted'] == 0) {
                $keyInfo = array('identifier'=>$projectIdentifier, 'prefix'=>'pro_'.$projectSetType);
                //$this->run(new DeleteFromCacheJob($keyInfo));
                $status_data = $this->run(new GetFromCacheJob($keyInfo));
                
                //var_dump($status_data);
                if(!empty($status_data['status']) && $status_data['status']=="error"){
                    $this->run(new DeleteFromCacheJob($keyInfo));

                    
                    $projectHierarchyDetail = $this->run(new GetNodesAssignedToProjectInTreeCompatibleFormJob($projectIdentifier, $requestUserDetails, ''));

                    $projectHierarchy   =   array_merge($projectDet, $projectHierarchyDetail);

                    $dataToSendForProjectHierarchy  =   $projectHierarchy;
                    if($projectSetType == 'hierarchy')
                    {
                        $projectSetTypeToSend           =   'hierarchy';
                        $keyInfoToSetInCache            =   array('identifier'=>$projectIdentifier, 'prefix'=>'pro_'.$projectSetTypeToSend);
    
                        $statusOfSetDataCache           =   $this->run(new SetToCacheJob($dataToSendForProjectHierarchy, $keyInfoToSetInCache));
    
                    }
                    else
                    {
                             // Set Details of Taxonomy In cache
                            foreach($projectHierarchyDetail['nodes'] as $node) {

                                $itemId     = $node['id'];
                                $isDocument = $node['is_document'];
                                if($isDocument == 1)
                                {
                                    $validateDocument   =   $this->dispatch(new ValidateDocumentByIdJob(['document_id'   =>  $itemId]));  
                                    if($validateDocument===true){
                                        $validateDocumentIsDeleted  =   $this->dispatch(new CheckDocumentIsDeleteJob($itemId));    
                                        if(!$validateDocumentIsDeleted) {
                                            $documentDetails            =   $this->dispatch(new GetDocumentAndRespectiveMetadataValuesJob($itemId));
                                            $documentAssets             =   $this->dispatch(new GetAssetListJob($itemId, $requestUserDetails["organization_id"],$requestUrl));

                                            $documentDetails["assets"]      =   $documentAssets;
                                            $projectHierarchy['details'][]  =   $documentDetails;
                                            unset($documentDetails);
                                        }
                                    }
                                }
                                else
                                {
                                    $validationStatus = $this->dispatch(new ValidateItemByIdJob([ 'item_id' => $itemId ]));
                
                                    if($validationStatus===true){
                    
                                        $itemIsDeleted = $this->dispatch(new CheckItemIsDeletedJob($itemId));
                                        
                                        if(!$itemIsDeleted) {
                                            
                                            $response = $this->dispatch(new GetCFItemDetailsJob($itemId, $organizationIdentifier));
                                            $exemplarAssociationList = $this->dispatch(new GetExemplarAssociationsJob($itemId, $requestUserDetails,$requestUrl));
                                            // embed the exemplar association collection inside the current response body
                                            $response["exemplar_associations"] = $exemplarAssociationList;
                
                                            // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                                            $itemAssets = $this->dispatch(new GetAssetListJob($itemId, $requestUserDetails["organization_id"],$requestUrl));
                                            $response["assets"] = $itemAssets;
                                            $projectHierarchy['details'][] = $response;
                                            unset($response);
                                        }
                                    }
                                }
                            }
                            //$dataToSendForProjectDetails['project_detail']  =   $projectDet;
                            $dataToSendForProjectDetails['details']         =   $projectHierarchy['details']; 
                            $projectSetType2                                =   "details";
                            $keyInfoForProjectDetails                       =   array('identifier'=>$projectIdentifier, 'prefix'=>'pro_'.$projectSetType2);
                            
                            $status2 = $this->dispatch(new SetToCacheJob($dataToSendForProjectDetails, $keyInfoForProjectDetails));
                            if(is_array($status2) && $status2['status']=="error"){
                                // handle failure, data already exists
                            }
                    }
                   
                   
                    
                    $status3 = $this->run(new GetFromCacheJob($keyInfo));
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status3, $message, $_status));                     
                }
                else
                {
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status_data, $message, $_status));
                }
            } else {
                $message = 'Data not found.';
                $successType = 'not_found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, '', $message, $_status));
            }
            
            
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
