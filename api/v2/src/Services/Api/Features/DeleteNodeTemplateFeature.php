<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeTemplate\Jobs\ValidateNodeTemplateByIdJob;
use App\Domains\NodeTemplate\Jobs\CheckNodeTemplateIsDeleteJob;
use App\Domains\NodeTemplate\Jobs\DeleteNodeTemplateJob;
use Log;

class DeleteNodeTemplateFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $templateIdentifier = $request->route('node_template_id');
            $identifier = ['node_template_id' => $templateIdentifier];

            $validationStatus = $this->run(new ValidateNodeTemplateByIdJob($identifier));
            if($validationStatus === true) {
                $checkTemplateIsDeleted = $this->run(new CheckNodeTemplateIsDeleteJob($templateIdentifier));
                if(!$checkTemplateIsDeleted === true) {
                    $deleteTemplate = $this->run(new DeleteNodeTemplateJob($templateIdentifier));
                    
                    $response = $deleteTemplate;
                    $successType = 'found';
                    $message = 'Node Template deleted successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Node Template is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid template.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

        } catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
