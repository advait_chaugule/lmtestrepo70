<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\User\Jobs\ConfirmUserSelfRegisterationJob;
use App\Domains\User\Jobs\RenameUsernameForHistoryBackupJob;
use App\Domains\User\Jobs\GetUserEmailJob;
use App\Domains\User\Jobs\ValidateAccessTokenAndEmailJob;

use App\Domains\User\Jobs\GetIndividualUserDetailJob;
use App\Domains\Project\Jobs\ListProjectsIdJob;
use App\Domains\Project\Jobs\AssignedUserToPublicReviewProjectJob;
use DB;
use Log;

class SelfRegistrationEmailVerificationFeature extends Feature
{   
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {   
        try{ 
             $requestVar = $request->all();
             $input['active_access_token'] = $request->route('active_access_token');
            
                $accessTokenEmailValdidate = $this->run(new ValidateAccessTokenAndEmailJob($input));
                
                if($accessTokenEmailValdidate==1){

                        $accountStatus = $this->run(new ConfirmUserSelfRegisterationJob($input));
                        if ($accountStatus == 1) {
                            //Self Register Successful
                            // get user id and project id's detail
                            $userIdDetails = $this->run(new GetIndividualUserDetailJob($input));
                            $projectlist = $this->run(new ListProjectsIdJob);
                            $organizationIdValue = DB::table('users')
                            ->select('organization_id')
                            ->where('active_access_token','=',$request->route('active_access_token'))
                            ->where('is_deleted','0')->first()->organization_id;

                            if(!empty($projectlist) && !empty($userIdDetails) && !empty($organizationIdValue)){
                                $organizationDetail=$this->run(new AssignedUserToPublicReviewProjectJob($projectlist,$userIdDetails,$organizationIdValue));
                            }
                            $successType = 'found';
                            $message = 'You have successfully Self Registered. You can login now.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message ,$_status));
                        }
                        else if($accountStatus == 2){
                            //Account Already Activated!!Login
                            $successType = 'found';
                            $message = 'Account is aready active.You can login.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message, $_status));
                        }
                        else{
                            //Link Expired.Self Register Again
                            //Get User Email Id
                            $userEmailDetails = $this->run(new GetUserEmailJob($input));
                            $inputEmail = ['email' => $userEmailDetails];
                            $this->run(new RenameUsernameForHistoryBackupJob($inputEmail));
                            $successType = 'found';
                            $message = 'The link has expired.You have to register again';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $message, $_status));
                        }
                }
                else{
                    $errorType = 'internal_error';
                    $validatemessage="Active Access Token Invalid";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
                } 
            }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
