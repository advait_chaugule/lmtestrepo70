<?php
namespace App\Services\Api\Features;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\HelpCheck\Jobs\HelpCheckJob;
use Log;

class HelpCheckFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
             $this->run(new HelpCheckJob());
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
