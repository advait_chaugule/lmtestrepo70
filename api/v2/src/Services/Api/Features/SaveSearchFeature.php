<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Search\Jobs\ValidateSaveSearchInputParametersJob;
use App\Domains\Search\Jobs\PrepareRequestDataForSearchingJob;
use App\Domains\Search\Jobs\PrepareSearchHistoryDataJob;

use App\Domains\Search\Events\SaveSearchHistoryEvent;

use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class SaveSearchFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $validationStatus = $this->run(ValidateSaveSearchInputParametersJob::class);
            if($validationStatus===true) {
                $requesUserDetails = $request->input('auth_user');
                $searchDetails = $this->run(PrepareRequestDataForSearchingJob::class);
                $title = $request->input('title') ?: "";
                $description = $request->input('description') ?: "";
                $requestFilters = !empty($request->input('search_optional_filters')) ? $request->input('search_optional_filters') : [];

                $searchDetails['title'] = $title;
                $searchDetails['description'] = $description;
                $searchDetails['request_user'] = $requesUserDetails;
                // set 1 for explicit save
                $searchDetails['is_explicit_saved'] = 1;
                $searchDetails['request_filters'] = $requestFilters;
                
                $searchDataToSave = $this->run(new PrepareSearchHistoryDataJob($searchDetails));
                // raise event to store search history
                $eventData = ["search_history" => $searchDataToSave ];
                event(new SaveSearchHistoryEvent($eventData));

                $httpResponseMessage = 'Search saved successfully.';
                $httpResponse = [];
                $successType = 'found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $httpResponse, $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validationStatus ; //'Invalid inputs.'; //"Provide proper search query inputs."
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
