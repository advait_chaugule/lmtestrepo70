<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\NodeType\Jobs\ListNodeTypeMetadataJob;
use App\Domains\NodeType\Jobs\ValidateNodeTypeIdJob;
use Log;

class ListNodeTypeMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $nodeTypeId = $request->route('node_type_id');
            $input = ['node_type_id' => $nodeTypeId];
            $validateExistence = $this->run(new ValidateNodeTypeIdJob($input));

            if($validateExistence===true) {
                $nodeTypeMetadata = $this->run(new ListNodeTypeMetadataJob($nodeTypeId));
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $nodeTypeMetadata, $message, $_status));
            }
            else {
                $errors = $validateExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
