<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\NodeType\Jobs\SaveNodeTypeMetadataJob;
use App\Domains\NodeType\Jobs\ListNodeTypeMetadataJob;
use App\Domains\NodeType\Jobs\ValidateNodeTypeIdJob;
use App\Domains\Metadata\Jobs\GetCustomMetadataDetailJob;
use App\Domains\Metadata\Jobs\DeleteCustomMetadataJob;

use App\Domains\Caching\Events\CachingEvent;
use Log;

class SaveNodeTypeMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $nodeTypeId = $request->route('node_type_id');
            $input = ['node_type_id' => $nodeTypeId];
            $validateExistence = $this->run(new ValidateNodeTypeIdJob($input));
            
            if($validateExistence === true){
                
                /** Delete Custom Metadata from item_metadata and document_metadata table event start */
                $customMetadataId = $this->run(new GetCustomMetadataDetailJob($nodeTypeId));
                $this->run(new DeleteCustomMetadataJob($nodeTypeId, $customMetadataId,$requestData['metadata']));
                /** Delete Custom Metadata from item_metadata and document_metadata table event start */
                $this->run(new SaveNodeTypeMetadataJob($nodeTypeId, $requestData));
                

                // Caching Event
                $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                $eventData = [
                    "event_type"         => $eventType,
                    "organizationId"     => $requestData['auth_user']['organization_id'],
                    "beforeEventRawData" => [],
                    'afterEventRawData'  => ''
                ];

                event(new CachingEvent($eventData)); // caching ends here

                $response   =   $this->run(new ListNodeTypeMetadataJob($nodeTypeId));
                $successType = 'found';
                $message = 'Data saved successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else{
                $errors = $validateExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
