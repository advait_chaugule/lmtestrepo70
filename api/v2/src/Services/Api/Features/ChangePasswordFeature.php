<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ChangePasswordJob;
use App\Domains\User\Jobs\ValidatePasswordJob;
use Log;

class ChangePasswordFeature extends Feature
{   
    public function handle(Request $request)
    {
        try{
            $requestData         = $request->all();
            $userId              = $requestData['auth_user']['user_id'];
            $currentPassword     = $requestData['currentPassword'];
            $newPassword         = $requestData['newPassword'];
            $confirmNewPassword  = $requestData['confirmNewPassword'];

            $validations = ['currentPassword' =>$currentPassword,'newPassword'=>$newPassword,'confirmNewPassword'=>$confirmNewPassword];
            $validatePassword  =   $this->run(new ValidatePasswordJob($validations));
            if($validatePassword=='true'){
                    $response = $this->run(new ChangePasswordJob($userId,$currentPassword,$newPassword,$confirmNewPassword));
                if($response==1){
                    $successType = 'custom_validation_error';
                    $message     = ['current password does not match'];
                    $_status     = 'custom_status_here';
                
                    return $this->run(new RespondWithJsonJob($successType, null, $message, '', [], false));         
                }elseif($response==2){
                    $successType = 'custom_validation_error';
                    $message     = ['new password and confirm password does not match'];
                    $_status     = 'custom_status_here';
                    
                    return $this->run(new RespondWithJsonJob($successType, null, $message, '', [], false)); ;           
                }elseif($response==3){
                    $successType =  'custom_validation_error';
                    $message     =  ['current password and new password cannot be same'];
                    $_status     = 'custom_status_here';
                
                    return $this->run(new RespondWithJsonJob($successType, null, $message, '', [], false)); ;           
                }
                else{
                    $successType = 'custom_found';
                    $message     = ['password updated successfully'];
                    $_status     = 'custom_status_here';
                
                    return $this->run(new RespondWithJsonJob($successType, [], $message,'',[], true));
                    }
            }else{
                $successType = 'custom_validation_error';
                $message     = $this->run(new ValidatePasswordJob($validations));
                $_status     = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, null, $message, '', [], false));            
            }
        }
        catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }       
}
