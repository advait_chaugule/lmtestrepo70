<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;

use App\Domains\Metadata\Jobs\DestroyAdditionalMetadataJob;

//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class DestroyItemAdditionalMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData        =   $request->all();
            $itemIdentifier     =   $request->route('item_id');
            $metadataIdentifier =   $request->route('metadata_id');
            $itemId             =   ['item_id' => $itemIdentifier];

            $validateItem   =   $this->run(new ValidateItemByIdJob($itemId));

            if($validateItem === true) {
                $checkIsItemDeleted =   $this->run(new CheckItemIsDeletedJob($itemIdentifier));

                if($checkIsItemDeleted !== true) {
                    $destroyAdditionalMetadata = $this->run(new DestroyAdditionalMetadataJob($itemIdentifier, $metadataIdentifier, 'item'));
                    //event for project and document update(updated_at) start
                    event(new UpdateTaxonomyTimeEvent($itemIdentifier));
                    //event for project and document update(updated_at) end
                    if($destroyAdditionalMetadata !== false) {

                        $this->raiseEventToUploadTaxonomySearchDataToSqs($itemIdentifier);

                        $successType = 'updated';
                        $message = 'Additional Metadata deleted successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $destroyAdditionalMetadata, $message, $_status));
                    }
                    else {
                        $errorType = 'internal_error';
                        return $this->run(new RespondWithJsonErrorJob($errorType));
                    }                    
                }
            } else {
                $errors = [['item_id' => 'Invalid id provided.']];
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $itemIdentifier) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_item"];
        $eventData = [
            "type_id" => $itemIdentifier,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
