<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Log;
use App\Domains\Association\Jobs\GetAssociationPreset;
use Lucid\Foundation\Feature;

class GetAssociationPresetFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];
            $sourceTaxonomyTypeId       = $request->route('source_taxonomy_type_id');
            $targetTaxonomyTypeId       = $request->route('target_taxonomy_type_id');
            $associationType            = $request->route('association_type');

            $response = $this->run(new GetAssociationPreset($userId,$organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType));
            $successType = 'custom_found';
            $_status = 'custom_status_here';
            $message = (!empty($response)) ? ['Data found.'] : ['Data Not found.'];
            $is_success = (!empty($response)) ? true : false;
            return $this->run(new RespondWithJsonJob($successType, $response, $message,$_status,[],$is_success));

          }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}