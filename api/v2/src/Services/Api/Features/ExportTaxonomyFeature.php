<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Document\Jobs\ValidateDocumentJob;
use App\Domains\Taxonomy\Events\ExportCSVBackgroundEvent;

use Log;


class ExportTaxonomyFeature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        try
        {
           
            $documentIdentifier =   $request->route('documentId');
            $requestUrl         = url('/');
            $exportKey          =   1;
            $validationStatus = $this->run(new ValidateDocumentJob($documentIdentifier));
            $is_html = $request->input('is_html', '');
            $userDetails = $request->input('auth_user');
            $trans_id = rand(10,100).substr(time(),6,10);
            if($validationStatus===true) {
                $eventData  =  [
                    'documentIdentifier'=>$documentIdentifier,
                    'requestingUserDetails'=>$request->input('auth_user'),
                    'orgCode'=>'',
                    'is_html'=>$is_html,
                    'exportKey'=>$exportKey,
                    'request_url'=>$requestUrl,
                    'headers' => getallheaders(),
                    'organization_id' => $userDetails['organization_id'],
                    'trans_id' => $trans_id // Transaction id added to identify different export request
                    ];
                event (new ExportCSVBackgroundEvent($eventData)) ;
                $successType = 'found';
                $message = 'Import process has been sent to background.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));                
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

        }
        catch(\Exception $ex)
        {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
