<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Asset\Jobs\ValidateAssetByIdJob;
use App\Domains\Asset\Jobs\DeleteAssetJob;

use App\Domains\Asset\Jobs\GetAssetDetailJob;
//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;
use Log;

class DeleteAssetFeature extends Feature
{

    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $assetId = $request->route('asset_id');
            $dataToValidate = [ "asset_id" => $assetId ];
            $validationStatus = $this->run(new ValidateAssetByIdJob($dataToValidate));
            if($validationStatus===true) {

                // get details from asset table
                $itemLinkedId = $this->run(new GetAssetDetailJob($assetId));
                // job to update project and document table below deletion process
                if(!empty($itemLinkedId)){
                    //event for project and document update(updated_at) start
                    event(new UpdateTaxonomyTimeEvent($itemLinkedId));
                    //event for project and document update(updated_at) end
                }    
                $requestUserDetails = $request->input("auth_user");
                $this->run(new DeleteAssetJob($assetId, $requestUserDetails));
                $eventType1 = config("event_activity")["Cache_Activity"]["FILES_GETALL"];
                $eventDetail = [
                    'event_type' => $eventType1,
                    'requestUserDetails' => $request->input("auth_user"),
                    "beforeEventRawData" => [],
                    'afterEventRawData' => ''
                ];
                event(new CachingEvent($eventDetail));
                $successType = 'deleted';
                $responseData = [];
                $message = 'Asset deleted successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $responseData, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
