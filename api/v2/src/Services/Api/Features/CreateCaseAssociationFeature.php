<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\CaseAssociation\Jobs\CreateCaseAssociationValidationJob;
use App\Domains\CaseAssociation\Jobs\CreateStandardValidationJob;
use App\Domains\CaseAssociation\Jobs\CreateItemAssociationsJob;
use App\Domains\CaseAssociation\Jobs\OriginDestinationNodeAssociationTypeExistStatusJob;
use App\Domains\CaseAssociation\Jobs\CreateCFAssociationUriJob;
use App\Domains\CaseAssociation\Jobs\GetDestinationDetailsJob;
use App\Domains\CaseAssociation\Jobs\GetCreatedAssociationJob;
use App\Domains\CaseAssociation\Jobs\EditCaseAssociatonJob;
use App\Domains\Association\Jobs\DocumentSelfIsChildOfValidationJob;

use App\Domains\Project\Jobs\GetDocumentIdJob;

use App\Domains\Item\Jobs\GetItemJob;
use App\Domains\Item\Jobs\CreateCFItemJob;
use App\Domains\Item\Jobs\AddCustomMetadataForPacingGuideItemJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
//use App\Domains\Document\Jobs\CheckProjectAssociatedToDocumentJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;

//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use Log;

class CreateCaseAssociationFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait;
    
    public function handle(Request $request)
    {
        try{
            $requestData = $request->input();
            $requestUrl = url('/');
            
            $selfAssociation = true;
            if(($requestData['origin_node_id'] == $requestData['destination_node_ids']))
            {
                $selfAssociation = false;
            }
            if($selfAssociation==true)
            {

                // $docValidationResponse = $this->run(new DocumentSelfIsChildOfValidationJob($requestData));
                $docValidationResponse = true;
                if($docValidationResponse==true)
                {

                    if($requestData['project_type'] == 1) {
                        $validation = $this->run(new CreateCaseAssociationValidationJob($requestData));
                        $validation=true;
                    } else {
                        $validation = $this->run(new CreateStandardValidationJob($requestData));
                    }
                
                    if($validation===true) {
                        $associationDataToSave = $this->prepareAssociationDataToSave($requestData,$requestUrl);
                        if(!empty($associationDataToSave)){
                            
                            // save the association
                            $this->run(new CreateItemAssociationsJob($associationDataToSave, $requestData['project_type'], $requestData['project_id']));
                                
                            $associationDataToSave[0]['project_type']  =   $requestData['project_type'];
                            $responseData   =   $this->run(new GetCreatedAssociationJob($associationDataToSave));
                            
                            //event for project and document update(updated_at) start
                            event(new UpdateTaxonomyTimeEvent($requestData['origin_node_id']));
                            //event for project and document update(updated_at) end

                            $this->raiseEventToUploadTaxonomySearchDataToSqs($requestData['origin_node_id']);

                            // set response body
                            $successType = 'created';
                            $message = 'Association(s) created.';
                            $_status = 'created';
                            return $this->run(new RespondWithJsonJob($successType,  $responseData, $message, $_status));
                        }
                        else{
                            $errorType = 'found';
                            $message = "No new associations to create.";
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($errorType, $message, $_status));
                        }
                    }
                    else {
                        
                        $errorType = 'validation_error';
                        $message = $this->messageArrayToString($validation);
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else
                {
                    $errorType = 'found';
                    $message = "Document can not be childof of it's node.";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else
            {
                $errorType = 'validation_error';
                $message = "Can not create association with self";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));     
            }    
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function prepareAssociationDataToSave(array $requestData,$requestUrl): array {
        $reverseAssociationType =   0;
        $associationDataToSave =    [];
        $cfDocumentIdentifier  =    '';
        // extract the validated inputs
        $originNodeIdentifier       = $requestData['origin_node_id']; // standard_id 
        $associationTypeIdentifier  = $requestData['association_type'];
        //type casted to array in case string is sent in destination_node_ids
		$destinationIds             = (array) $requestData['destination_node_ids'];
        $description                = !empty($requestData['description']) ? $requestData['description'] : "";
        
        if($requestData['project_type'] == 2) {
            $associationTypeIdentifier  =   3;
        }

        if($associationTypeIdentifier == '10') {
            $reverseAssociationType =   1;
            $associationTypeIdentifier  =   3;
        } else if($associationTypeIdentifier == '11') {
            $reverseAssociationType =   1;
            $associationTypeIdentifier  =   5;
        } else if($associationTypeIdentifier == '12') {
            $reverseAssociationType =   1;
            $associationTypeIdentifier  =   7;
        }

        //get origin node details
        $originTaxonomy = $this->run(new GetItemJob(['id' => $originNodeIdentifier]));

        // keep cfdocument id same as origin node if origin is of cfdoc node type
        $cfDocumentIdentifier = isset($originTaxonomy->document_id) ? $originTaxonomy->document_id : $originNodeIdentifier;
        
        $organizationId = $requestData["auth_user"]["organization_id"];
        $currentDateTime = now()->toDateTimeString();

        // loop throught them and prepare association data
		// Destination id emoty condition handled
        if(!empty($destinationIds)){
            foreach($destinationIds as $destinationNodeIdentifier) {
                $createDataForAssociationExistsChecking = [
                    'source_item_id'        => $originNodeIdentifier,
                    'association_type'      => $associationTypeIdentifier,
                    'target_item_id'   => $destinationNodeIdentifier,
                    //'is_reverse_association'            => 0 
                ]; 
                
                if($reverseAssociationType == 1) {
                    $createDataForAssociationExistsChecking = [
                        'source_item_id'        => $destinationNodeIdentifier,
                        'association_type'      => $associationTypeIdentifier,
                        'target_item_id'   => $originNodeIdentifier,
                        //'is_reverse_association'=> 1
                    ];

                    //get origin node details
                    $reverseOriginTaxonomy = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));

                    // keep cfdocument id same as origin node if origin is of cfdoc node type
                    $reverseCfDocumentIdentifier = isset($reverseOriginTaxonomy->document_id) ? $reverseOriginTaxonomy->document_id : $destinationNodeIdentifier;
                }

                $checkAssociationExists = $this->run(new OriginDestinationNodeAssociationTypeExistStatusJob($createDataForAssociationExistsChecking));
                if($checkAssociationExists===false){ // 
                    // create unique identifier
                    $associationIdentifier = $this->createUniversalUniqueIdentifier();
                    // create association uri
                    $associationUri = $this->run(new CreateCFAssociationUriJob($associationIdentifier,$requestUrl));
                    if($associationTypeIdentifier == 4){
                        $destination = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));
                        $associationDataToSave[] = [
                            "item_association_id"           =>  $associationIdentifier,
                            "association_type"              =>  $associationTypeIdentifier,
                            "document_id"                   =>  $cfDocumentIdentifier,
                            "origin_node_id"                =>  $originNodeIdentifier,
                            "destination_node_id"           =>  $destinationNodeIdentifier,
                            "destination_document_id"       =>  $cfDocumentIdentifier,
                            "external_node_title"           =>  !empty($destination['full_statement']) ? $destination['full_statement'] : "",
                            "external_node_url"             =>  !empty($destination['uri']) ? $destination['uri'] : "",
                            "description"                   =>  $description,
                            "organization_id"               =>  $organizationId,
                            "source_item_association_id"    =>  $associationIdentifier,
                            "created_at"                    =>  $currentDateTime,
                            "updated_at"                    =>  $currentDateTime,
                            "source_document_id"            =>  $cfDocumentIdentifier,
                            "source_item_id"                =>  $originNodeIdentifier,
                            "target_document_id"            =>  $destination['document_id'],
                            "target_item_id"                =>  $destinationNodeIdentifier,
                        ];
                    }else if($reverseAssociationType == 1) {
                        $destination = $this->run(new GetItemJob(['id' => $originNodeIdentifier]));
                        //prepare the data
                        $associationDataToSave[] = [
                            "item_association_id"           =>  $associationIdentifier,
                            "association_type"              =>  $associationTypeIdentifier,
                            "document_id"                   =>  $reverseCfDocumentIdentifier,
                            "origin_node_id"                =>  $destinationNodeIdentifier,
                            "destination_node_id"           =>  $originNodeIdentifier,
                            "destination_document_id"       =>  $cfDocumentIdentifier,
                            "external_node_title"           =>  !empty($destination['full_statement']) ? $destination['full_statement'] : "",
                            "external_node_url"             =>  !empty($destination['uri']) ? $destination['uri'] : "",
                            "description"                   =>  $description,
                            "organization_id"               =>  $organizationId,
                            "source_item_association_id"    =>  $associationIdentifier,
                            "is_reverse_association"        =>  1,
                            "created_at"                    =>  $currentDateTime,
                            "updated_at"                    =>  $currentDateTime,
                            "source_document_id"            =>  $reverseCfDocumentIdentifier,
                            "source_item_id"                =>  $destinationNodeIdentifier,
                            "target_document_id"            =>  isset($destination['document_id']) ? $destination['document_id'] : $originNodeIdentifier,
                            "target_item_id"                =>  $originNodeIdentifier,
                        ];
                    } else {
                        $destination = $this->run(new GetItemJob(['id' => $destinationNodeIdentifier]));
                        if(empty($destination['full_statement']))
                        {
                            $destination = $this->run(new GetDocumentByIdJob(['id' => $destinationNodeIdentifier]));
                        }
                        //prepare the data
                        $associationDataToSave[] = [
                            "item_association_id"           =>  $associationIdentifier,
                            "association_type"              =>  $associationTypeIdentifier,
                            "document_id"                   =>  $cfDocumentIdentifier,
                            "origin_node_id"                =>  $originNodeIdentifier,
                            "destination_node_id"           =>  $destinationNodeIdentifier,
                            "destination_document_id"       =>  $cfDocumentIdentifier,
                            "external_node_title"           =>  !empty($destination['full_statement']) ? $destination['full_statement'] : "",
                            "external_node_url"             =>  !empty($destination['uri']) ? $destination['uri'] : "",
                            "description"                   =>  $description,
                            "organization_id"               =>  $organizationId,
                            "source_item_association_id"    =>  $associationIdentifier,
                            "created_at"                    =>  $currentDateTime,
                            "updated_at"                    =>  $currentDateTime,
                            "source_document_id"            =>  $cfDocumentIdentifier,
                            "source_item_id"                =>  $originNodeIdentifier,
                            "target_document_id"            =>  $destination['document_id'],
                            "target_item_id"                =>  $destinationNodeIdentifier,
                        ];
                    }
                }
            }
        }

        return $associationDataToSave;
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $itemIdentifier) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_item"];
        $eventData = [
            "type_id" => $itemIdentifier,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
