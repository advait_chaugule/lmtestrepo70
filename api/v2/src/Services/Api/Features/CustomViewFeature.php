<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportConnectionJob;
use App\Domains\JReport\Jobs\CustomViewJob;
use App\Domains\JReport\Jobs\SetContentTypeJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\ItemType\Jobs\GetItemTypeByItemIdJob;
use DB;
use App\Services\Api\Traits\CurlHelper;
use Config;

class CustomViewFeature extends Feature
{
    use CurlHelper;
    /*
    public function handle(Request $request)
    {      
        $input = $request->input();

        $organization_id = $input['auth_user']['organization_id'];
        $document_id = $request->route('document_id'); 
        $item_id = $request->route('item_id'); 
        $file_type='html';
        
        $documentDetail = $this->run(new GetDocumentByIdJob([ 'id' => $document_id]));        
            $itemTypeDetail = $this->GetItemTypeByItemId($item_id);
            $report_type=$itemTypeDetail[0]->title;
        if($documentDetail === NULL) {
            $errorType = 'validation_error';
            $message = "Invalid document_id provided.";
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));            
        }
        $file_name= $documentDetail['title'];
        

        $endPoint=config('jasper.jasper_servlet').'?report='.$report_type.'&orgid='.$organization_id.'&docid='.$document_id.'&itemid='.$item_id;
        
        $serverReponse = $this->checkUrl($endPoint,"GET");
        $content= $serverReponse['result'];
                
        echo '<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600" rel="stylesheet">';

        echo '<style type="text/css">.jrPage span{
                font-size: 14px !important;
                font-family: Nunito !important;
                }
            </style>';
               
        $this->run(new SetContentTypeJob($file_type,$content,$file_name));
    } */
    
    public function handle(Request $request)
    {
        $connection= $this->run(new JReportConnectionJob());
        $input = $request->input();

        $organization_id = $input['auth_user']['organization_id'];
        $document_id = $request->route('document_id'); 
        $item_id = $request->route('item_id'); 
        $file_type='html';
        
        $documentDetail = $this->run(new GetDocumentByIdJob([ 'id' => $document_id]));        
            $itemTypeDetail = $this->GetItemTypeByItemId($item_id);
            $report_type=$itemTypeDetail[0]->title;
            $customViewDataArray = json_decode($itemTypeDetail[0]->custom_view_data);
            $reportPath = $customViewDataArray->report_path;
            if(!empty($reportPath))
            {
                $report_type = $reportPath;
            }
            
        if($documentDetail === NULL) {
            $errorType = 'validation_error';
            $message = "Invalid document_id provided.";
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));            
        }
        $file_name= $documentDetail['title'];
        
        $content=$this->run(new CustomViewJob($connection,$organization_id,$document_id, $item_id,$file_type,$report_type));
        echo '<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600" rel="stylesheet">';

        echo '<style type="text/css">.jrPage span{
                font-size: 14px !important;
                font-family: Nunito !important;
                }
            </style>';
       
        $this->run(new SetContentTypeJob($file_type,$content,$file_name));
    }
    private function GetItemTypeByItemId($item_id){

        $nodeDetails =DB::table('items')
        ->join('node_types','node_types.node_type_id','=','items.node_type_id')
        ->where('items.item_id', '=',$item_id)   
        ->where('node_types.used_for',0)
        ->select('node_types.title','items.custom_view_data')     
        ->get();
        if(count($nodeDetails) > 0)
        {
            $nodetype = $nodeDetails->toArray();
            return $nodetype;
        }
        else
        {
            return 0;
        }
        

    }
}
