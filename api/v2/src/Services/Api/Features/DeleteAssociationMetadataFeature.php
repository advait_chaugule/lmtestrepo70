<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Item\Jobs\DeleteItemAssociationMetadataJob;
use Log;

class DeleteAssociationMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];
            $itemAssociationId          = $request->route('item_association_id');
            $metadataId                 = $request->route('metadata_id');
            $isAdditional                 = $request->route('is_additioanl');

            if($isAdditional==0)
            {
                $response = $this->run(new DeleteItemAssociationMetadataJob($itemAssociationId,$metadataId));
                
                if($response == true)
                {
                    $successType = 'custom_created';
                    $_status = 'custom_status_here';
                    $message = 'Metadata deleted successfully';
                    $is_success = true;
                    return $this->run(new RespondWithJsonJob($successType,[], $message, $_status,[], $is_success));
                }
                else
                {
                    $errorType = 'custom_validation_error';
                    $status = 'custom_status_here';
                    $message = 'Could not find metadata to delete';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message , $status));
                }

            }
            else
            {
                $errorType = 'custom_validation_error';
                $status = 'custom_status_here';
                $message = 'Metadata other than preset can not be deleted';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message , $status));
            }
            
            

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $status));
        }
    }
}
