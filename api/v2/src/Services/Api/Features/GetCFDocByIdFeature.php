<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Cfdoc\Jobs\GetCFDocByIdJob;
use Log;

class GetCFDocByIdFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $cfDocumentId = $request->route('id');
            $input = [ 'id' => $cfDocumentId ];
            //find cfdoc
            $cfdoc = $this->run(new GetCFDocByIdJob($input));
            //return 
            $response = $cfdoc;
            $successType = 'CFDocument found';
            $message = 'Document found';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            
        } catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
