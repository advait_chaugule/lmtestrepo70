<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use App\Domains\Cftree\Jobs\GetCFTreeJob;
use Log;

class GetCFTreeFeature extends Feature {

    public function handle(Request $request) {
        try {
            $input = $request->input();
            $documentId = $request->route('document_id');
            $requestData = $request->all();
            $input = [ 'document_id' => $documentId];
            $validateExistence = $this->run(new ValidateDocumentByIdJob($input));

            if($validateExistence===true) {
                $requestInputs = $requestData + $input;
                $documentDetail = $this->run(new GetDocumentByIdJob([ 'id' => $documentId]));
                $documentDetail['children'] = $this->run(new GetCFTreeJob($input)); 
                $treeData = ['children' => [$documentDetail]];
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $treeData, $message, $_status));
            }
            else {
                $errors = $validateExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}
