<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\ValidateUserByEmailJob;
use App\Domains\User\Jobs\CreateResetRequestJob;
use App\Domains\User\Jobs\SendResetPasswordEmailNotificationJob;
use App\Domains\User\Jobs\ResetUserStatusJob;
use Log;

class ForgotPasswordAgainFeature extends Feature
{
    use UuidHelperTrait;

    public function handle(Request $request)
    {
        $requestData = $request->all();        
        $input = ['email' => $request['email']];
        try{

           $this->run(new ResetUserStatusJob($input));
                         
                $input["forgot_pasword_request_token"] = $this->createUniversalUniqueIdentifier();
                $input["forgot_pasword_request_token_expiry"] = date("Y-d-m H:i:s", strtotime("+30 minutes"));
                
                $resetPasswordDetails = $this->run(new CreateResetRequestJob($input));
                
                $email['email'] = $input['email'];
                $email['forgot_reset_request_token'] = $resetPasswordDetails['reset_token'];
                $email['user_name'] = $resetPasswordDetails['user_name'];
             
               $this->run(new SendResetPasswordEmailNotificationJob($email));
                $successType = 'sent';
                $message = 'Reset password email sent successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));         
           
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}

