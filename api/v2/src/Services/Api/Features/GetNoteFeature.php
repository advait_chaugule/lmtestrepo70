<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Note\Jobs\ValidateNoteIdJob;
use App\Domains\Note\Jobs\CheckNoteIsDeleteJob;
use App\Domains\Note\Jobs\GetNoteDetailsJob;
use Log;

class GetNoteFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData    =   $request->all();
           // dd($requestData);
            $noteId         =   $request->route('note_id');
            //dd($noteId);
            $identifier     =   ['note_id' => $noteId];
            $organizationId =   $requestData['auth_user']['organization_id'];
            //dd($organizationId);

            $validateNoteStatus = $this->run(new ValidateNoteIdJob($identifier));
            //dd($validateNoteStatus);
            if($validateNoteStatus === true) {


                    $noteDetail = $this->run(new GetNoteDetailsJob($noteId, $organizationId));
                    //dd($noteDetail);
                    $response = $noteDetail;
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));



            }else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid note.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
