<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\CheckUserExistByEmailAndUsernameJob;
use App\Domains\User\Jobs\ValidateMandatoryFieldsJob;
use App\Domains\User\Jobs\CreateUserJob;
use App\Domains\User\Jobs\EditUserJob;
use App\Domains\User\Jobs\UpdateUserStatusJob;
use Log;
use DB;
use Response;
/**
 * 
 */

class CISCreateUserFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function handle(Request $request)
    {
        try
        {  
            // input parameters
            $input = $request->all();
            $data['first_name']  = isset($input['firstname']) ? $input['firstname'] : '';
            $data['last_name'] = isset($input['lastname']) ? $input['lastname'] : '';
            $data['username'] = isset($input['username']) ? $input['username'] : '';
            $data['email'] = isset($input['email']) ? $input['email'] : '';
            $data['role_id'] = isset($input['role_id']) ? $input['role_id'] : '';
            $data['organization_id'] = isset($input['organization_id']) ? $input['organization_id'] : '';
            $data['user_id'] = isset($input['account_id']) ? $input['account_id'] : '';
            // below data is used only to make user active and inactive
            $data['is_active'] = isset($input['isActive']) ? $input['isActive'] : '0';          // is_active is et to set just to enter the edit user block
            
            // validate mandatory fields
            $validation_result = $this->run(new ValidateMandatoryFieldsJob($data));
            
            if(count($validation_result) > 0 )
            {
                $message = implode(',',$validation_result). ' required';
                
                /**
                * below code is used to send response as per ACMT standard which is current commented start
                */
                /*$errorType = 'validation_error';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));*/
                /**
                * below code is used to send response as per ACMT standard which is current commented end
                */

                return Response::json(['status'=>400,'message' => $message]);
                
            }

            // first make a check wheather user exist
            $check_user_exist = $this->run(new CheckUserExistByEmailAndUsernameJob($data));
            
            // if user doesn't exist add user, if user exists update only firstname and lastname
            // check_user_exist  = 0 means user doesn't exists
            // check_user_exist  = 1 means user exists
            if( $check_user_exist == 0 )
            {
                $data['user_id'] = $this->createUniversalUniqueIdentifier();
                $data['active_access_token'] = $this->createUniversalUniqueIdentifier();
                $data["access_token_updated_at"] = now()->toDateTimeString();

                // is_active 0 changed to 1 and vice versa to make ACMT Application compatible with CIS

                if($data['is_active'] == 0 )
                {
                    $data['is_active'] = 1;
                }
                else
                {
                    $data['is_active'] = 0;
                }
                
                $user = $this->run(new CreateUserJob($data));

                /**
                * below code is used to send response as per ACMT standard which is current commented start
                */
                /*$user['status'] = 2001;
                $successType = 'cis_created';
                $message = 'user created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $user, $message, $_status));*/
                /**
                * below code is used to send response as per ACMT standard which is current commented end
                */

                return Response::json(['status'=>200,'message' => 'user created successfully.']);

            }
            else
            {
                // if user exists update only firstname and lastname
                $this->run(new EditUserJob($data));

                // is_active 0 changed to 1 and vice versa to make ACMT Application compatible with CIS

                $this->run(new UpdateUserStatusJob($data));

                /**
                 * below code is used to send response as per ACMT standard which is current commented start
                 */
                /*$data['status'] = 2001;
                $successType = 'updated';
                $message = 'user updated successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));*/
                /**
                * below code is used to send response as per ACMT standard which is current commented end
                */

                return Response::json(['status'=>200,'message' => 'user updated successfully.']);

            }

        }
        catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
