<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\ValidateDocumentForATenantJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\ValidateDocumentForPublishJob;

use App\Domains\Taxonomy\Jobs\CheckPublicReviewInProgressJob;
use App\Domains\Taxonomy\Jobs\CopyTaxonomyAndRelatedDataJob;

use App\Domains\Project\Jobs\AddNodesUnderProjectJob;
use App\Domains\Project\Jobs\UpdateProjectParentItemToNonEditableJob;
use App\Domains\Project\Jobs\AssignedUserToPublicReviewProjectJob;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Notifications\Events\NotificationsEvent;
use Log;

class SetForPublicReviewFeature extends Feature
{
    use ArrayHelper, DateHelpersTrait;
    public function handle(Request $request)
    {
        ini_set('memory_limit','8098M');
        ini_set('max_execution_time', 0);
        try {
            $requestData            =   $request->all();
            $documentIdentifier     =   $request->route('document_id');
            $requestingUserDetail   =   $requestData['auth_user'];
            $requestUrl             = url('/');
            $serverName             = str_replace('server', '', $requestUrl);
            $validateDocument   =   $this->run(new ValidateDocumentByIdJob(['document_id'   =>  $documentIdentifier]));

            if($validateDocument == true) {
                $validateDocumentForTheTenant   =   $this->run(new ValidateDocumentForATenantJob($documentIdentifier, $requestingUserDetail['organization_id']));

                if($validateDocumentForTheTenant == true) {
                    $validateDocumentIsDeleted  =   $this->run(new CheckDocumentIsDeleteJob($documentIdentifier));

                    if(!$validateDocumentIsDeleted == true) {

                        $validateDocumentForPublish  =   $this->run(new ValidateDocumentForPublishJob($documentIdentifier,$requestingUserDetail['organization_id']));
                   
                        if($validateDocumentForPublish != true)
                        {
                            $checkForPublicReviewInProgress = $this->run(new CheckPublicReviewInProgressJob($documentIdentifier,$requestingUserDetail['organization_id']));

                            if($checkForPublicReviewInProgress == true) {
                                $errorType = 'validation_error';
                                $message = "Open public review still exists. Please close to continue.";
                                $_status = '';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                            } else {
                                $timezone   =   'Etc/GMT-5';//date_default_timezone_get();
                                $formattedStartDateTime  =   !empty($requestData['start_date']) ? $requestData['start_date'] : ""; // 
                                //$formattedStartDateTime  =   $this->formatDateTimeToGivenTimezone($startDate, $timezone);

                                $formattedEndDateTime    =   !empty($requestData['end_date']) ? $requestData['end_date'] : "";
                                //$formattedEndDateTime   =   $this->formatDateTimeToGivenTimezone($endDate, $timezone);

                                $requestData['formatted_start_date']    =   $formattedStartDateTime;
                                $requestData['formatted_end_date']      =   $formattedEndDateTime;
                                $requestData['timezone']                =   $timezone;

                                $copyResponse   =   $this->run(new CopyTaxonomyAndRelatedDataJob($documentIdentifier, $requestingUserDetail, $requestData));

                                if($copyResponse != false) {
                                    $projectIdentifier          = $copyResponse["project_id"];
                                    $copiedDocumentIdentifier   = $copyResponse['document_id'];
                                    $itemIds                    = $copyResponse["item_id"];
                                    $parentIds                  = $copyResponse['document_id'];
                                    $rootSelectedStatus         = true;

                                    // Optimised Part
                                    $nodeIdsToAssign = array_unique($this->createArrayFromCommaeSeparatedString($itemIds . "," . $parentIds));
                                    $this->run(new AddNodesUnderProjectJob($nodeIdsToAssign, $projectIdentifier, $rootSelectedStatus, $copiedDocumentIdentifier));

                                    /**
                                     * Please do not remove this code 
                                     */
                                    $arrayParentId  =   $this->exportMultipleItemId($parentIds);
                                    //All the semi selected parent nodes are set to non editable for a project
                                    $this->run(new UpdateProjectParentItemToNonEditableJob($arrayParentId, $projectIdentifier,$copiedDocumentIdentifier));

                                    //Assign Self Registered user to public review project
                                    $this->run(new AssignedUserToPublicReviewProjectJob([$projectIdentifier],$copyResponse['user_id'], $requestingUserDetail['organization_id']));

                                    // raise event to upload delta updates to cloud search
                                    $this->raiseEventToUploadTaxonomySearchDataToSqs($documentIdentifier);
                                    // In App Notifications
                                    $eventType               = config("event_activity")["Notifications"]["PUBLIC_REVIEW_PUBLISH"];
                                    $eventData = [
                                        'document_id'        => $documentIdentifier,
                                        'project_id'         => $projectIdentifier,
                                        'event_type'         => $eventType,
                                        'domain_name'        => $serverName,
                                        'requestUserDetails' => $request->input("auth_user"),
                                        "beforeEventRawData" => [],
                                        'afterEventRawData'  => ''
                                    ];
                                    event(new NotificationsEvent($eventData)); // notification ends here

                                    if(!empty($copiedDocumentIdentifier)) {
                                        $successType = 'found';
                                        $message = 'Taxonomy is set for Public Review.';
                                        $_status = 'custom_status_here';
                                        return $this->run(new RespondWithJsonJob($successType, $copyResponse, $message, $_status));
                                    }
                                } else {
                                    $errorType = 'validation_error';
                                    $message = "The workflow to create public review project is not available";
                                    $_status = '';
                                    return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                                }
                            }
                        }
                        else
                        {
                            $errorType = 'validation_error';
                            $message = "This taxonomy is already published, you can not assigned this to public review.";
                            $_status = '';
                            return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                        } 
                    } else {
                        $errorType = 'validation_error';
                        $message = "This taxonomy is deleted";
                        $_status = '';
                        return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                    }
                } else {
                    $errorType = 'validation_error';
                    $message = "This taxonomy does not belong to this organization";
                    $_status = '';
                    return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                }
            } else {
                $errorType = 'validation_error';
                $message = "Please select a valid taxonomy";
                $_status = 'Invalid Taxonomy.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function exportMultipleItemId($data) {
        $arrayItemId = explode(',', $data);
        return $arrayItemId;
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $documentId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_document"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
