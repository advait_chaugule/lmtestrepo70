<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Report\Jobs\GetCommentReportJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use Log;

class GetCommentReportDataFeature extends Feature
{   
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $projectId = $request->route('project_id');
            $input = [ 'project_id' => $projectId ];
            
            $validateExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateExistence===true) {
                
                $commentReport = $this->run(new GetCommentReportJob($requestData,$input));

                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $commentReport, $message, $_status));
            }
            else {
                $errors = $validateExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }    
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        } 

    }
}
