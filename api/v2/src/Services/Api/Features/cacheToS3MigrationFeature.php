<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV3Job;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use App\Domains\Asset\Jobs\GetAssetListJob;
use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use Storage;

class cacheToS3MigrationFeature extends Feature
{
    public function handle(Request $request)
    {
        ini_set('max_execution_time', '0');                                                                 //to set execution time to infinite
        ini_set('memory_limit', '-1');                                                                      //to increase the memory limit
      
        try {

            $org_code = trim($request->org_code);                                                           //get org_code from url
            $org = DB::table('organizations')                                                               //get organization_id from from org_code 
                    ->select('organization_id')
                    ->where('org_code', $org_code)
                    ->where('is_deleted', 0)
                    ->first();

            $taxonomy = DB ::table('taxonomy_pubstate_history')                                                 //query to get document_id of publihsed taxonomy in cache 
                    ->join('documents','taxonomy_pubstate_history.document_id','=','documents.document_id')
                    ->select('documents.document_id')
                    ->where('taxonomy_pubstate_history.treehierarchy_json_file_s3','')
                    ->where('taxonomy_pubstate_history.treedetails_json_file_s3','')
                    ->where('documents.organization_id', $org->organization_id)
                    ->where('documents.status',3)
                    ->where('documents.is_deleted',0)
                    ->groupby('document_id')
                    ->get()
                    ->toArray();
    
            foreach($taxonomy as $taxonomyK=>$taxonomyV){
                
                foreach($taxonomyV as $key=>$documentId){
                
                        $organizationId["organization_id"] = $org->organization_id;                             //organization_id from DB query         
                        $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeV3Job($documentId, $organizationId));

                        $dataToSend1['status']  = 200;                                 // to set JSON format while saving file in S3
                        $dataToSend1['data']    = $taxonomyHierarchy;                  // to set JSON format while saving file in S3
                        $dataToSend1['message'] = 'Data Found';                        // to set JSON format while saving file in S3
                        $dataToSend1['success'] = "";                                  // to set JSON format while saving file in S3
                        
                        $s3FileNameHierarchy      =  $documentId.'-hierarchy'.'.json';                                 // set filename e.g. ba6f3ef5-0ca8-4e74-877d-0348358adc78-Hierarchy.json
                        $s3BucketHierarchy        =  config("event_activity")["HIERARCHY_FOLDER"];                     // event activity
                        $destinationS3Hierarchy   = "{$s3BucketHierarchy["MAIN_ARCHIVE_FOLDER"]}/$s3FileNameHierarchy";// s3 destination
                        Storage::disk('s3')->put($destinationS3Hierarchy,json_encode($dataToSend1),'public');          // Saving file in s3                        

                        foreach($taxonomyHierarchy['nodes'] as $node) {
                            
                            $organizationIdentifier = $org->organization_id;                                         //organization_id from DB query 
                            $requestUrl = url('/');
                            $itemId = $node['id'];
                            $isDocument = $node['is_document'];
                            if($isDocument == 1)
                            { 
                                $validateDocument   =   $this->run(new ValidateDocumentByIdJob(['document_id'   =>  $itemId]));  
                                if($validateDocument===true){
                                    $validateDocumentIsDeleted  =   $this->run(new CheckDocumentIsDeleteJob($itemId));    
                                    if(!$validateDocumentIsDeleted){
                                        $documentDetails = $this->run(new GetDocumentAndRespectiveMetadataValuesJob($itemId));
                                        
                                        $documentAssets = $this->run(new GetAssetListJob($itemId, $organizationIdentifier,$requestUrl));
                                        $documentDetails["assets"] = $documentAssets; 
                                        if(isset($documentDetails['display_text'])){

                                            $languageWithCode = $documentDetails['display_text'];       // get language with its language code
                                            $language = explode(' (',$languageWithCode);                // explode and get only the language
                                            $documentDetails['language_name'] = $language[0];           // set language in 'langauge_name' for Table view(UF-1820)
                                        }
                                        $taxonomyHierarchy['details'][] = $documentDetails;
                                        unset($documentDetails);
                                    }
                                }
                            }
                            else
                            { 
                                $validationStatus = $this->run(new ValidateItemByIdJob([ 'item_id' => $itemId ]));

                                if($validationStatus===true){
                
                                    $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemId));
                                    
                                    if(!$itemIsDeleted) {
                                        
                                        $response = $this->run(new GetCFItemDetailsJob($itemId, $organizationIdentifier));
                                        $exemplarAssociationList = $this->run(new GetExemplarAssociationsJob($itemId, ['organization_id' =>$organizationIdentifier],$requestUrl));
                                        // embed the exemplar association collection inside the current response body
                                        $response["exemplar_associations"] = $exemplarAssociationList;

                                        // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                                        $itemAssets = $this->run(new GetAssetListJob($itemId, $organizationIdentifier,$requestUrl));
                                        $response["assets"] = $itemAssets;
                                        if(isset($response['display_text'])){

                                            $languageWithCode = $response['display_text'];              // get language with its language code
                                            $language = explode(' (',$languageWithCode);                // explode and get only the language
                                            $response['language_name'] = $language[0];                  // set language in 'langauge_name' for Table view(UF-1820)
                                        }
                                        $taxonomyHierarchy['details'][] = $response;
                                        unset($response);
                                    }
                                }
                            }
                        }

                            $dataToSend2['status']  = 200;                                                      // to set JSON format while saving file in S3
                            $dataToSend2['data']    = ['details' => $taxonomyHierarchy['details']];             // to set JSON format while saving file in S3
                            $dataToSend2['message'] = "Data Found";                                             // to set JSON format while saving file in S3
                            $dataToSend2['success'] = "";                                                       // to set JSON format while saving file in S3
                        
                            $s3FileNameDetails      =  $documentId.'-details'.'.json';                       // set filename e.g. ba6f3ef5-0ca8-4e74-877d-0348358adc78-details.json
                            $s3BucketDetails        =  config("event_activity")["HIERARCHY_FOLDER"];                 // event activity
                            $destinationS3Details   = "{$s3BucketDetails["MAIN_ARCHIVE_FOLDER"]}/$s3FileNameDetails";// s3 destination
                            Storage::disk('s3')->put($destinationS3Details,json_encode($dataToSend2),'public');      // Saving file in s3

                $existsHeirarchy = Storage::disk('s3')->exists($destinationS3Hierarchy);                 // check if the taxonomyHeirarchy file exists in s3
                $existsDetails = Storage::disk('s3')->exists($destinationS3Details);                     // check if the taxonomyDetail file exists in s3
                
                if($existsHeirarchy && $existsDetails){
                    
                    $update=[
                                'treehierarchy_json_file_s3' => $s3FileNameHierarchy ,
                                'treedetails_json_file_s3'    => $s3FileNameDetails,
                            ];

                    $query = DB::table('taxonomy_pubstate_history')                                         //query to nupdate the columns in taxonomy_pubstate_history table
                            ->where('document_id',$documentId)
                            ->where('organization_id',$organizationIdentifier)
                            ->update($update);
                    }

                unset($taxonomyHierarchy);
                unset($documentId);
                }
            }
            $successType = 'custom_found';
            $message = 'Migration completed successfully';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));    
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
