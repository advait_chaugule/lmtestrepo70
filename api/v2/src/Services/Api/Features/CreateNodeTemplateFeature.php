<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\NodeTemplate\Jobs\ValidateNodeTemplateInputJob;
use App\Domains\NodeTemplate\Jobs\CreateNodeTemplateJob;
use App\Domains\NodeTemplate\Jobs\AssociateNodeTypeWithNodeTemplateJob;
use Log;

class CreateNodeTemplateFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $nodeTemplateName   =   $requestData['name'];
            $inputData          =   ['name' =>  $nodeTemplateName];

            $validateTemplateName   =   $this->run(new ValidateNodeTemplateInputJob($inputData));
           
            if($validateTemplateName) {
                $inputData['organization_id']       =   $requestData['auth_user']['organization_id'];
                $inputData['created_by']            =   $requestData['auth_user']['user_id'];

                $createNodeTemplateObject           =   $this->run(new CreateNodeTemplateJob($inputData));
                
                foreach($requestData['node_types'] as $nodeTypes) {
                    $attributes =   ['node_template_id' =>  $createNodeTemplateObject['node_template_id'], 'node_type_id'   =>  $nodeTypes['node_type_id'], 'parent_node_type_id' =>  !empty($nodeTypes['parent_node_type_id']) ? $nodeTypes['parent_node_type_id'] : '', 'sort_order'   =>  $nodeTypes['sort_order']];

                    $this->run(new AssociateNodeTypeWithNodeTemplateJob($attributes));
                }

                $response       = $createNodeTemplateObject;
                $successType    = 'created';
                $message        = 'Node template is created successfully.';
                $_status        = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please provide template name';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
