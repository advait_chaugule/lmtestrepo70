<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;


use App\Domains\Language\Jobs\GetListOfLanguagesJob;
use App\Domains\NodeType\Jobs\GetNodeTypeAndRelatedMetadataSetJob;
//use App\Domains\Note\Jobs\GetAllNoteJob;
use Log;

class GetNodeTypesFromCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData    = $request->all();
            $organizationId = $request['organization_id'];
            
            $input = ['organization_id' => $organizationId];
            $keyInfo = array('identifier' => $organizationId, 'prefix' => 'node_types_');
            $status = $this->run(new GetFromCacheJob($keyInfo));

            if (isset($status['status']) && $status['status'] == "error") {

                $request->merge($requestData);
                $nodesList      =   $this->run(new GetNodeTypeAndRelatedMetadataSetJob($organizationId));
                $languageList   =   $this->run(new GetListOfLanguagesJob($organizationId));

                $dataToCache    =  ['nodetype'  => $nodesList, 'language' => $languageList]; 
                //dd($noteList);
                if (!empty($dataToCache) && count($dataToCache) > 0) {
                    $status1 = $this->run(new DeleteFromCacheJob($keyInfo));
                    $status2 = $this->run(new SetToCacheJob($dataToCache, $keyInfo));
                    $status3 = $this->run(new GetFromCacheJob($keyInfo));
                    $message = 'Data found.';
                    $successType = 'found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $status3, $message, $_status));
                }
            } else {
                $message = 'Data found.';
                $successType = 'found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $status, $message, $_status));
            }
        } catch (\Exception $ex) {
            Log::error($ex);
        }
    }
}
