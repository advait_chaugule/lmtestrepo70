<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Taxonomy\Jobs\SetOrderedTaxonomyTreeV2Job;

use App\Domains\Item\Jobs\DocumentMultipleTableJob;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class SetTaxonomyHierarchyV2Feature extends Feature
{
    public function handle(Request $request)
    {
        ini_set('max_execution_time', 0);
        try {
            $requestData            =   $request->input();
            $documentIdentifier     =   $request->route('document_id');
            $requestingUserDetails  =   $request->input("auth_user");

            $this->run(new SetOrderedTaxonomyTreeV2Job($documentIdentifier,$requestData));

                 //project and document table update
            $this->run(new DocumentMultipleTableJob($documentIdentifier));
            
            $successType = 'created';
            $message = 'Data reordered successfully.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, "", $message, $_status));
        }
        catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
