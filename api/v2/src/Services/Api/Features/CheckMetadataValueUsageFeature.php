<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Metadata\Jobs\ValidateMetadataValueJob;
use App\Domains\Metadata\Jobs\CountMetadataValueUsageJob;
use Log;

class CheckMetadataValueUsageFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $metadataIdentifier =   $request->route('metadata_id');
            $fieldPossibleValue =   $request->route('field_possible_value');

            $organizationId     =   $request->all()['auth_user']['organization_id'];

            $validateMetadataValue  =   $this->run(new ValidateMetadataValueJob($metadataIdentifier, $fieldPossibleValue, $organizationId));

            if($validateMetadataValue == true) {
                $countUsage =  $this->run(new CountMetadataValueUsageJob($metadataIdentifier, $fieldPossibleValue, $organizationId));

                $successType = 'found';
                $message = 'Usage Count for metadata';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $countUsage, $message, $_status));
            } else {
                $successType = 'found';
                $message = 'Value do not match with metadata';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $validateMetadataValue, $message, $_status));
            }
        } catch(\Exception $ex){
            Log::error($ex);
        }
    }
}
