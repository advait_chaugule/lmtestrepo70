<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Walkthrough\Jobs\GetWalkthroughDetailsFromS3Job;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class GetWalkthroughJsonFeature extends Feature
{
    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $s3BucketName = env('AWS_S3_BUCKET');
            if(!empty($s3BucketName)) {
                $responseData = $this->run(new GetWalkthroughDetailsFromS3Job($s3BucketName));
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $responseData, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = 'S3 bucket not available';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
