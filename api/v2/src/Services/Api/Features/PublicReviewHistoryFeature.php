<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Report\Jobs\ValidateDocumentExistJob;
use App\Domains\Taxonomy\Jobs\GetPublicReviewHistoryJob;
use Log;

class PublicReviewHistoryFeature extends Feature
{   
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestVar = $request->all();
            $identifier['document_id']=$request->route('document_id');
            $validateDocumentId = $this->run(new ValidateDocumentExistJob($identifier));
            
            if($validateDocumentId===true)
            {
                $publicReviewHistory = $this->run(new GetPublicReviewHistoryJob($identifier));
                
                $successType = 'found';
                $data = $publicReviewHistory;
                $message = 'Public Review History Fetched';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));

            }
            else
            {
                $errorType = 'internal_error';
                $validatemessage="Invalid Document ID";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $validatemessage ,$_status));
            }    

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
