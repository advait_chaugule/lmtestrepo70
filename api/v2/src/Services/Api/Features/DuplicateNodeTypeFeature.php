<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeType\Jobs\ValidateNodeTypeIdJob;
use App\Domains\NodeType\Jobs\GetNodeTypeDetailsJob;
use App\Domains\NodeType\Jobs\DuplicateNodeTypeJob;

use App\Domains\Caching\Events\CachingEvent;
use Log;

class DuplicateNodeTypeFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestData        =   $request->all();
            $nodeTypeIdentifier =   $request->route('node_type_id');
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];

            $validateNodeTypeData = $this->run(new ValidateNodeTypeIdJob(['node_type_id' => $nodeTypeIdentifier, ]));
            if($validateNodeTypeData===true) { 
            
                $nodeTypeDuplicated = $this->run(new DuplicateNodeTypeJob($nodeTypeIdentifier, $requestData));
                
                // Caching Event
                $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                $eventData = [
                    "event_type"         => $eventType,
                    "organizationId"     => $requestData['auth_user']['organization_id'],
                    "beforeEventRawData" => [],
                    'afterEventRawData'  => ''
                ];

                event(new CachingEvent($eventData)); // caching ends here

                $resultData = $nodeTypeDuplicated;

                $successType = 'created';
                $message = 'Metadata Set duplicated successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status));
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please provide valid inputs.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
