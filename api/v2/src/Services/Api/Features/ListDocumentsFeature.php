<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ListDocumentRootJob;
use Log;

class ListDocumentsFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $documents = $this->run(ListDocumentRootJob::class);
            if(!empty($documents) && count($documents) > 0){
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $documents, $message, $_status));
            }
            else {
                $errorType = 'not_found';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Data not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
