<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Search\Jobs\ValidateSearchInputParametersJob;
use App\Domains\Search\Jobs\PrepareRequestDataForSearchingJob;
use App\Domains\Search\Jobs\SearchJob;

use App\Domains\Search\Jobs\PrepareSearchHistoryDataJob;
use App\Domains\Search\Events\SaveSearchHistoryEvent;

use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

/**
 * SearchFeature class
 * This feature will search for taxonomy and project from cloud search
 */
class SearchFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        ini_set('memory_limit','700M');
        
        try{
            $validationStatus = $this->run(ValidateSearchInputParametersJob::class);
            if($validationStatus===true) {
                $preparedRequestDataForSearching = $this->run(PrepareRequestDataForSearchingJob::class);
                $searchResult = $this->run(new SearchJob($preparedRequestDataForSearching));

                // save to search history if search term provided
                if(!empty($request->input('search_term'))) {
                    // prepare data to save search history
                    $requestFilters = !empty($request->input('search_optional_filters')) ? $request->input('search_optional_filters') : [];
                    $preparedRequestDataForSearching['title'] = '';
                    $preparedRequestDataForSearching['description'] = '';
                    $preparedRequestDataForSearching['request_user'] = $request->input('auth_user');
                    // set 0 for implicit save
                    $preparedRequestDataForSearching['is_explicit_saved'] = 0;
                    $preparedRequestDataForSearching['request_filters'] = $requestFilters;
                    $searchDataToSave = $this->run(new PrepareSearchHistoryDataJob($preparedRequestDataForSearching));
                    // raise event to store search history
                    $eventData = [ "search_history" => $searchDataToSave ];
                    event(new SaveSearchHistoryEvent($eventData));
                }

                $successType = 'found';
                $_status = 'custom_status_here';
                $httpResponseMessage = !empty($searchResult) ? 'Data Found.' : 'Data Not Found.';
                return $this->run(new RespondWithJsonJob($successType, $searchResult, $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validationStatus; //"Provide proper search query inputs."
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
