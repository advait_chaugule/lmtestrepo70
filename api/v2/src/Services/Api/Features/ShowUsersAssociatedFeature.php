<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\MappedUsersListJob;

use App\Domains\User\Jobs\UserRoleListJob;
use Log;

class ShowUsersAssociatedFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $projectId = $request->route('project_id');
            $identifier = ['project_id' => $projectId];
            $validateStatus = $this->run(new ValidateProjectIdJob($identifier));
            if($validateStatus === true){
                $checkIsProjectDeleted = $this->run(new CheckIsProjectDeletedJob($identifier));
                if(!$checkIsProjectDeleted === true) {
                    $mappedUsersList = $this->run(new MappedUsersListJob($identifier));
                   // dd($mappedUsersList);
                    $userNameAndRoleList = $this->run(new UserRoleListJob($mappedUsersList['user_id'], $projectId));
                    $response = $userNameAndRoleList;
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Project is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'not_found_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Project ID is invalid';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }            
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
