<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Log;
use App\Domains\Association\Jobs\UpdateAssociationPreset;
use App\Domains\Association\Jobs\ValidateNodeTypeIdJob;
use App\Domains\Association\Jobs\CheckPresetNameExistsJob;
use Lucid\Foundation\Feature;

class UpdateAssociationPresetFeature extends Feature
{
    public function handle(Request $request)
    {

        try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];
            $name                       = $requestData['name'];
            $sourceTaxonomyTypeId       = $requestData['source_taxonomy_type_id'];
            $targetTaxonomyTypeId       = $requestData['target_taxonomy_type_id'];
            $associationType            = $requestData['association_type'];
            $metadata                   = $requestData['metadata'];
            $nodeTypeId                 = $requestData['node_type_id'];
            $response = [];
          
            $validateNodeId = $this->run(new ValidateNodeTypeIdJob($nodeTypeId,$organizationId)); //Validate Node Type Id
            if($validateNodeId == true){
                $validatePresetIfExists = $this->run(new CheckPresetNameExistsJob($name, $organizationId, $nodeTypeId)); //Check if Association Preset name exists
                if($validatePresetIfExists == true){
                    $errorType = 'custom_validation_error';
                    $_status = 'custom_status_here';
                    $message = ['Preset name already exists.'];
                    return $this->run(new RespondWithJsonJob($errorType, null, $message , $_status,[], false));
                }else{
                    $response = $this->run(new UpdateAssociationPreset($userId,$organizationId,$name,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType,$metadata,$nodeTypeId));
                    $successType = 'custom_found';
                    $_status = 'custom_status_here';
                    $message = ($response === true) ? ['Preset updated successfully.'] : ['Preset Configuration Already Present.'];
                    $is_success = ($response === true) ? true : false;
                    return $this->run(new RespondWithJsonJob($successType,[], $message, $_status,[], $is_success));
                }
            }else{
                $errorType = 'custom_validation_error';
                $_status = 'custom_status_here';
                $message = ['Node Type Id does not exists.'];
                return $this->run(new RespondWithJsonJob($errorType, null, $message , $_status,[], false));
            }

        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}
