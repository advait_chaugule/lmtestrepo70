<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeType\Jobs\ValidateNodeTypeIdJob;
use App\Domains\NodeType\Jobs\CheckNodeTypeIsDeleteJob;
use App\Domains\NodeType\Jobs\GetNodeTypeDetailsJob;
use Log;

class GetNodeTypeFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData    =   $request->all();
            $nodetypeId     =   $request->route('node_type_id');
            $identifier     =   ['node_type_id' => $nodetypeId];
            $organizationId =   $requestData['auth_user']['organization_id'];

            $validateNodetypeStatus = $this->run(new ValidateNodeTypeIdJob($identifier));
            
            if($validateNodetypeStatus === true) {
                $checkIsNodeTypeDeleted =   $this->run(new CheckNodeTypeIsDeleteJob($nodetypeId));
                if(!$checkIsNodeTypeDeleted === true){
                    $nodeTypeDetail = $this->run(new GetNodeTypeDetailsJob($nodetypeId, $organizationId));
                    
                    $response = $nodeTypeDetail;
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }   
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'NodeType is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
                
            }else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid nodetype.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));                
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
