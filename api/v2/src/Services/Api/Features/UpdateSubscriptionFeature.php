<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\UpdateSubscribedTaxonomyJob;

class UpdateSubscriptionFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestDetails = $request->all();
            $inputData['subscriptionId']    = (null!==$request->input('subscription_id')) ? $request->input('subscription_id') : '';
            $inputData['isActive']          = (null!==$request->input('status')) ? $request->input('status') : '';
            
            if(!empty($inputData['subscriptionId']) && !empty($inputData['isActive'])){
                if($inputData['isActive']!='true' && $inputData['isActive']!='false'){
                    $successType = 'custom_validation_error';
                    $message = 'Invalid Status';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
                $updateSubscription = $this->run(new UpdateSubscribedTaxonomyJob($inputData['subscriptionId'], $inputData['isActive']));
                if($updateSubscription){
                    $successType = 'custom_found';
                    $message = 'Taxonomy subscription updated successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));
                }
                else{ // If no record inserted, it means already exists in table
                    $successType = 'custom_not_found';
                    $message = 'Taxonomy subscription not found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
            }
            else{
                $successType = 'custom_not_found';
                $message = 'Subscription Id, Status is mandatory.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
