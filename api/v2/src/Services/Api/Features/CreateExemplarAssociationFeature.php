<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\CaseAssociation\Jobs\ValidateCreateExemplarAssociationInputsJob;
use App\Domains\CaseAssociation\Jobs\CreateExemplarAssociationWithExternalUrlJob;
use App\Domains\CaseAssociation\Jobs\CreateExemplarAssociationWithExternalAssetJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class CreateExemplarAssociationFeature extends Feature
{

    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $requestUrl = url('/');
            $firstLevelValidationStatus = $this->run(new ValidateCreateExemplarAssociationInputsJob($requestData));
            if($firstLevelValidationStatus===true) {
                if($requestData["has_asset"]==0) {
                    // create exemplar association with external_link url
                    $savedExemplar = $this->run(new CreateExemplarAssociationWithExternalUrlJob($requestData));
                }
                else{
                    // create exemplar association with external asset file
                    $savedExemplar = $this->run(new CreateExemplarAssociationWithExternalAssetJob($requestData,$requestUrl));
                }

                //event for project and document update(updated_at) start
                event(new UpdateTaxonomyTimeEvent($requestData['origin_node_id']));
                //event for project and document update(updated_at) end
              
                $responseData = $savedExemplar;
                $successType = 'created';
                $message = 'Exemplar association created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $responseData, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($firstLevelValidationStatus);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
