<?php

namespace App\Services\Api\Features;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Project\Jobs\ValidateSubmitReviewJob;
use App\Domains\Project\Jobs\InsertPublicReview;
use Log;

class submitReviewFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData      = $request->all();
            $projectId        = !empty($requestData['project_id']) ? $requestData['project_id'] : '';
            $userId           = $requestData['auth_user']['user_id'];
            $organizationId   = $requestData['auth_user']['organization_id'];
            $isDeleted        = $requestData['auth_user']['is_deleted'];
            $identifier       = ['project_id' => $projectId,'user_id' =>$userId,'organization_id'=>$organizationId,'is_deleted'=>$isDeleted ];
            $validationStatus = $this->run(new ValidateSubmitReviewJob($identifier));
            if($validationStatus === true) {
                $insertPublicReview = $this->run(new InsertPublicReview($identifier));
                if($insertPublicReview) {
                    $response       = $insertPublicReview;
                    $successType    = 'created';
                    $message        = 'success';
                    $_status        = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
            }else{
                $errorType = 'validation_error';
                $message = 'Please provide valid inputs';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
