<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Taxonomy\Jobs\ValidateInputsForTaxonomyDeltaUpdatesSearchDataUploadJob;
use App\Domains\Document\Jobs\CreateDeltaDocumentUpdatesSearchDataJob;
use App\Domains\Item\Jobs\CreateDeltaItemUpdatesSearchDataJob;
use App\Domains\Search\Jobs\UploadSearchDataJob;
use Log;

class CreateAndUploadTaxonomyDeltaUpdatesSearchDataFeature extends Feature
{

    use ErrorMessageHelper, ArrayHelper, StringHelper;

    public function handle(Request $request)
    {
        // set higher memory limit and execution time since this feature is resource intensive
        ini_set('memory_limit','8000M');
        ini_set('max_execution_time', 0);

        try{
            $requestInput = $request->all();
            $validationResponse = $this->run(new ValidateInputsForTaxonomyDeltaUpdatesSearchDataUploadJob($requestInput));
            if($validationResponse===true) {
                $deltaDocumentIdsInputStatus = ($request->exists('document_ids') && !empty($request->input('document_ids')));
                $deltaItemIdsInputStatus = ($request->exists('item_ids') && !empty($request->input('item_ids')));
                if($deltaDocumentIdsInputStatus===true) {
                    $deltaUpdateDocumentIds = $this->createArrayFromCommaeSeparatedString($requestInput['document_ids']);
                    $deltaDocumentSearchDataToUpload = $this->run(new CreateDeltaDocumentUpdatesSearchDataJob($deltaUpdateDocumentIds));
                    $this->run(new UploadSearchDataJob($deltaDocumentSearchDataToUpload));
                }
                if($deltaItemIdsInputStatus===true) {
                    $deltaUpdateItemIds = $this->createArrayFromCommaeSeparatedString($requestInput['item_ids']);
                    $deltaItemSearchDataToUpload = $this->run(new CreateDeltaItemUpdatesSearchDataJob($deltaUpdateItemIds));
                    $this->run(new UploadSearchDataJob($deltaItemSearchDataToUpload));
                }
                $httpResponseMessage = 'Delta updates uploaded to cloud search.';
                $successType = 'created';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validationResponse;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
