<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ValidateUserByIdJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\User\Jobs\UpdateUserJob;
use Log;

class UseEditFeature extends Feature
{
    public function handle(Request $request)
    {
        $userIdentifier = $request->route('user_id');
        $roleIdentifier = $request->input('role_id');
        $organizationId = $request->input('auth_user')['organization_id'];
        $identifier = ['user_id' => $userIdentifier, 'role_id' => $roleIdentifier];
        
        try{
            $validationStatus = $this->run(new ValidateUserByIdJob($identifier));
            
            if($validationStatus === true) {
        
                $requestData = $request->all();  
                $requestData['user_id'] = $userIdentifier;
                $requestData['organization_id'] = $organizationId;
                $requestInputs = $requestData;
                $user = $this->run(new UpdateUserJob($requestInputs));
                $response = $user;
                $successType = 'updated';
                $message = 'Data updated successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'User id is not valid.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }    
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
