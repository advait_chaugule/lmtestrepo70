<?php
namespace App\Services\Api\Features;

use App\Domains\Notifications\Jobs\GetStageNameById;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Project\Jobs\ValidateUpdateProjectWorkflowStageInputJob;
use App\Domains\Project\Jobs\UpdateProjectWorkflowStageJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Notifications\Jobs\GetTaxonomyNameById;
use App\Domains\Notifications\Jobs\NotificationJobForEmail;
use Log;

class UpdateProjectWorflowStageFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $validateInputs = $this->run(new ValidateUpdateProjectWorkflowStageInputJob($requestData));
            if($validateInputs===true){
                $requestUrl    = url('/');
                $serverName    = str_replace('server', '', $requestUrl);

                $workflowStage = $this->run(new UpdateProjectWorkflowStageJob($requestData));
                //In App Notification events
                $workflowStageData = $workflowStage->toArray();
                $eventType            = config("event_activity")["Notifications"]["PROJECT_STAGE_CHANGE_NOTIFICATION"];
                //I should get notifications when the overall status of a stage is updated
                $eventData = [
                    'project_id'         => $workflowStageData['project_id'],
                    'organization_id'    => $workflowStageData['organization_id'],
                    'created_by'         => $workflowStageData['created_by'],
                    'is_deleted'         => $workflowStageData['is_deleted'],
                    'event_type'         => $eventType,
                    'document_id'        => $workflowStageData['document_id'],
                    'project_name'       => $workflowStageData['project_name'],
                    'workflow_id'        => $workflowStageData['current_workflow_stage_id'],
                    'domain_name'        => $serverName,
                    "requestUserDetails" => $request->input("auth_user"),
                    "beforeEventRawData" => [],
                    "afterEventRawData"  => ''
                ];

                event(new NotificationsEvent($eventData));  //In App Notification ends

                //I should receive an in-app notification if the stage to which the role assigned to me is active in a project
                $eventType2  = config("event_activity")["Notifications"]["STAGE_ROLE_ASSIGN"];
                $workFlowData= ['project_id'  =>$workflowStageData['project_id'],
                                'workflow_id' =>$workflowStage['current_workflow_stage_id'],
                                'document_id' =>$workflowStage['document_id'],
                                'event_type'  =>$eventType2,
                                'domain_name'        => $serverName,
                                'requestUserDetails' => $request->input("auth_user"),
                                "beforeEventRawData" => [],
                                'afterEventRawData'  => ''
                                ];
                event(new NotificationsEvent($workFlowData));  //In App Notification ends

                $response = $workflowStage;
                $successType = 'updated';
                $message = 'Data updated successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else {
                $errors = $validateInputs;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
           
        }
        catch(\Exception $ex){

            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
