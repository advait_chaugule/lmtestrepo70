<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Metadata\Jobs\ValidateMetadataByIdJob;
use App\Domains\Metadata\Jobs\CheckMetadataIsDeleteJob;
use App\Domains\Metadata\Jobs\DeleteMetadataJob;

use App\Domains\Caching\Events\CachingEvent;
use Log;

class DeleteMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData        =   $request->all();
            $metadataIdentifier =   $request->route('metadata_id');
            $organizationIdentifier =   $request->all()['auth_user']['organization_id'];
            $identifier = ['metadata_id' => $metadataIdentifier, 'organization_id'  =>  $organizationIdentifier];
            
            $validationStatus = $this->run(new ValidateMetadataByIdJob($identifier));
            if($validationStatus === true) {
                $CheckMetadataIsDeleteJob = $this->run(new CheckMetadataIsDeleteJob($metadataIdentifier));
                if(!$CheckMetadataIsDeleteJob === true) {
                    $deleteMetadata = $this->run(new DeleteMetadataJob($metadataIdentifier, $organizationIdentifier));

                    // Caching Event
                    $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                    $eventData = [
                        "event_type"         => $eventType,
                        "organizationId"     => $requestData['auth_user']['organization_id'],
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];

                    event(new CachingEvent($eventData)); // caching ends here

                    $response = $deleteMetadata;
                    $successType = 'found';
                    $message = 'Data deleted successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Metadata is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid metadata.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
