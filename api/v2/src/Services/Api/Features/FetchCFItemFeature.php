<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Item\Events\ViewItemInsideProjectEvent;
use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;

use App\Domains\Asset\Jobs\GetAssetListJob;
use Log;
use DB;

class FetchCFItemFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $itemId                     =   $request->route('item_id');
            $requestUrl                 =   url('/');
            $requestData                =   $request->all();
            $organizationId             =   $requestData['auth_user']['organization_id'];
            $validationStatus = $this->run(new ValidateItemByIdJob([ 'item_id' => $itemId ]));
            
            if($validationStatus===true){

                $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemId));
                if(!$itemIsDeleted) {
                    if(!empty($request->input('project_id'))) {
                        $projectType       =   DB::table('projects')->select('project_type')->where('project_id', $request->input('project_id'))->first()->project_type;
                        if($projectType == 3)
                        {
                            $itemId       =   DB::table('items')->select('source_item_id')->where('item_id', $itemId)->first()->source_item_id;
                        }
                    }
                    $response = $this->run(new GetCFItemDetailsJob($itemId, $organizationId));

                    // raise event to track activities for reporting
                    if(!empty($request->input('project_id'))) {
                        $beforeEventRawData = [];
                        $afterEventRawData = [
                            'item_id' => $response['item_id'],
                            'human_coding_scheme' => $response['human_coding_scheme'],
                            'list_enumeration' => $response['list_enumeration']
                        ];
                        $afterEventRawData['project_id'] = $request->input('project_id');
                        $eventData = [
                            "beforeEventRawData" => (object) $beforeEventRawData,
                            "afterEventRawData" => (object) $afterEventRawData,
                            "requestUserDetails" => $request->input("auth_user")
                        ];
                        event(new ViewItemInsideProjectEvent($eventData));
                    }

                    // ACMT-821 (create a new job to fetch item exemplar associations with external url/asset-link)
                    $requestUserDetails = $request->input("auth_user");
                    $exemplarAssociationList = $this->run(new GetExemplarAssociationsJob($itemId, $requestUserDetails,$requestUrl));
                    // embed the exemplar association collection inside the current response body
                    $response["exemplar_associations"] = $exemplarAssociationList;

                    // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                    $itemAssets = $this->run(new GetAssetListJob($itemId, $requestUserDetails["organization_id"],$requestUrl));
                    $response["assets"] = $itemAssets;
                    $response['language_name'] = $response['language'];
                    unset($response['language']);
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Item is deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errorType = 'validation_error';
                $message = "Invalid id provided.";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
