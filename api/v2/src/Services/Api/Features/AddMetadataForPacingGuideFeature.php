<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Metadata\Jobs\AddMetadataForPacingGuideJob;
use Log;

class AddMetadataForPacingGuideFeature extends Feature
{
    use ErrorMessageHelper, ArrayHelper, StringHelper;

    public function handle(Request $request)
    {
        // set higher memory limit and execution time since this feature is resource intensive
        ini_set('memory_limit','4000M');
        ini_set('max_execution_time', 0);

        try{
            $requestInput           =   $request->all();
            $organizationId         =   $requestInput['organization_id'];
            $organizationIdentifier =   ['organization_id'  => $requestInput['organization_id']];
            
            //$httpResponseMessage    =   [];
            $addMetadata = $this->run(new AddMetadataForPacingGuideJob($requestInput['organization_id']));

            if($addMetadata ==  true) {
                $message = "Pacing Guide Type metadata is added for organization id:$organizationId";
            } else {
                $message = "Pacing Guide Type metadata is not added for organization id:$organizationId";
            }

            $httpResponseMessage    =   $message;

            $successType = 'created';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, [], $httpResponseMessage, $_status));
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
