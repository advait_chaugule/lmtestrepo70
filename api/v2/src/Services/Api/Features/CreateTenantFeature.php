<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Tenant\Jobs\CreateTenantJob;

class CreateTenantFeature extends Feature
{
    public function handle(Request $request)
    {
        //$this->run(new CreateTenantJob()); 
        //return view('tenant.createtenant');
        return view('api::tenant.createtenant');
    }
}
