<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\WorkflowStage\Jobs\ValidateWorkflowStageJob;
use App\Domains\WorkflowStage\Jobs\ExistingWorkflowStageRoleJob;
use App\Domains\WorkflowStage\Jobs\DeleteWorkflowStageJob;
use Log;

class DeleteStageFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;

    public function handle(Request $request)
    {
        
        try{
            $requestData = $request->all();
            $workflow_stage_id = $request->route('workflow_stage_id');
            $input["workflow_stage_id"] = $workflow_stage_id;
            $workflowStageArr = explode('||', $workflow_stage_id);
            $workflowId = $workflowStageArr[0];
            $identifier["workflow_id"] = $workflowId;
            $validationStatus = $this->run(new ValidateWorkflowStageJob($input));
            if($validationStatus===true) {

                $existingWorkflowStageRoleJob = $this->run(new ExistingWorkflowStageRoleJob($identifier));
                if($existingWorkflowStageRoleJob===true)
                {
                    $deleteStage = $this->run(new  DeleteWorkflowStageJob($input));
                    $successType = 'found';
                    $message = 'Workflow Stage deleted successfully.';
                    $_status = 'custom_status_here';
                   return $this->run(new RespondWithJsonJob($successType, $deleteStage, $message, $_status));
                }
                else
                {
                    $errorType = 'validation_error';
                    $message = $existingWorkflowStageRoleJob;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
             
                }
                
            }
            else {
                $errorType = 'validation_error';
                $message = $validationStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}

