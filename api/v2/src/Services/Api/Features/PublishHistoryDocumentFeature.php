<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\Document\Jobs\ValidateDocumentForATenantJob;
use App\Domains\Document\Jobs\GetPublishHistoryListJob;
use Log;

class PublishHistoryDocumentFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $organizationId = $requestData['auth_user']['organization_id'];
            $userId = $requestData['auth_user']['user_id'];
            $documentId = $request->route('document_id');
            $validateDocument = $this->run(new ValidateDocumentForATenantJob($documentId, $organizationId));
            if ($validateDocument === true) {
                $getUnPublishTaxonomy = $this->run(new GetPublishHistoryListJob($documentId,$organizationId,$userId));
                if(!empty($getUnPublishTaxonomy)) {
                    $successType = 'found';
                    $message = 'List found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $getUnPublishTaxonomy, $message, $_status));
                }else{
                    $successType = 'found';
                    $message = 'No List found';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $getUnPublishTaxonomy, $message, $_status));
                }
            } else {
                $errorType = 'validation_error';
                $message = "This taxonomy does not belong to this organization";
                $_status = '';
                return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}