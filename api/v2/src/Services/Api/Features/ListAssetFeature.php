<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Asset\Jobs\GetListOfAssetJob;
use Log;

class ListAssetFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $itemLinkedId = $request->route('item_linked_id');
            $requestUserDetails = $request->input("auth_user");
            $assetList = $this->run(new GetListOfAssetJob($itemLinkedId,$requestUserDetails));
            if(count($assetList) != 0) {
                $successType = 'found';
                $message = "Assets Found.";
                $_status = "found";
            }
            else {
                $successType = 'not_found';
                $message = "No Assets found.";
                $_status = "not_found";
            }

            return $this->run(new RespondWithJsonJob($successType,  $assetList, $message, $_status));
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
