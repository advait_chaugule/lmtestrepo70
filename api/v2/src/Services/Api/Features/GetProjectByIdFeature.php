<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Project\Jobs\GetProjectByIdJob;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class GetProjectByIdFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $input = $request->input();
            $input["project_id"] = $request->route('project_id');
            $projectDet = $this->run(new GetProjectByIdJob($input));
            $response = $projectDet;

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
        
    }
}
