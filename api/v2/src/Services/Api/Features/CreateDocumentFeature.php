<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Document\Jobs\ValidateCreateDocumentInputJob;
use App\Domains\Document\Jobs\CreateDocumentJob;
use Log;

class CreateDocumentFeature extends Feature
{
    use UuidHelperTrait;

    public function handle(Request $request)
    {
        try{
            $input = $request->input();

            $identifier = $this->createUniversalUniqueIdentifier();
            $input['document_id'] = $identifier;
            $input['source_document_id'] = $identifier;
            $validationStatus = $this->run(new ValidateCreateDocumentInputJob($input));
            if($validationStatus===true) {

                // add organization id of the user performing the action
                $userPerformingAction = $request->input("auth_user");
                $input['organization_id'] = $userPerformingAction["organization_id"];
                
                $document = $this->run(new CreateDocumentJob($input));
                $successType = 'created';
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $document, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please provide valid inputs.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
