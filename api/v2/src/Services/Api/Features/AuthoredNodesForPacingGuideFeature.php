<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\GetProjectByIdJob;

use App\Domains\PacingGuide\Jobs\GetFlattenedTaxonomyTreeForPacingGuideJob;
use Log;

class AuthoredNodesForPacingGuideFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $projectIdentifier      =   $request->route('project_id');
            $requestingUserDetails  =   $request->input("auth_user");
            $input                  =   [ 'project_id' => $projectIdentifier];
            $validateProject        =   $this->run(new ValidateProjectIdJob($input));
            if($validateProject === true) {
                $projectDet = $this->run(new GetProjectByIdJob($input));
                if($projectDet['organization_id'] == $requestingUserDetails['organization_id']) {
                    if($projectDet['project_type']  ==  '2') {
                        if(!empty($projectDet['document_id'])) {
                            $getTreeHierarchyForPacingGuide =   $this->run(new GetFlattenedTaxonomyTreeForPacingGuideJob($projectDet['document_id'], $requestingUserDetails));

                            //dd($getTreeHierarchyForPacingGuide);
        
                            /*** Create the success response ***/
                            $successType = 'found';
                            $message = 'Authored Nodes listed successfully.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $getTreeHierarchyForPacingGuide, $message, $_status));
                        } else {
                            $errorType = 'validation_error';
                            $message = "No taxonomy added to ".$projectDet['project_name'];
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                        }                        
                    } else {
                        $errorType = 'validation_error';
                        $message = "Please select valid Pacing Guide";
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                } else {
                    $errorType = 'validation_error';
                    $message = "Please select Pacing Guide from same organization";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }                
            }else {
                $errors = $validateProject;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
        }
    }
}
