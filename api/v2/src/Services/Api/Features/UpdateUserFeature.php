<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ValidateUserByIdJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\User\Jobs\UpdateUserJob;
use Log;

class UpdateUserFeature extends Feature
{
    public function handle(Request $request)
    {
        $requestData = $request->all();
        $organizationId = $requestData['auth_user']['organization_id'];
        $userIdentifier = $request->route('user_id');
        $isActive = $request->input('is_active');
        $requestData = ['user_id' => $userIdentifier, 'is_active' => $isActive,'organization_id' =>$organizationId];

        try{
            $validationStatus = $this->run(new ValidateUserByIdJob($requestData));
            
            if($validationStatus === true) {
        
                $updateUserStatus = $this->run(new UpdateUserJob($requestData));
                if($updateUserStatus->is_register == 0)
                    {
                             $response = false;
                             $successType = 'custom_not_found';
                             $message = 'You cannot activate/deactivate unregistered user';
                             $_status = 'custom_status_here';
                             return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status)); 
                    }

                $response = $updateUserStatus;
                $successType = 'updated';
                $message = 'Data updated successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'User id is not valid.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }    
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
