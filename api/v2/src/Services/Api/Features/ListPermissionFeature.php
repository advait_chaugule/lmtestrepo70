<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ValidateRoleByIdJob;
use App\Domains\Role\Jobs\CheckRoleIsDeleteJob;
use App\Domains\Role\Jobs\ListRolePermissionJob;

use App\Domains\Permission\Jobs\ListPermissionJob;
use Log;

class ListPermissionFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $rolePermissionList = [];
            $permissionList     = [];
            //Get Role Identifer from Route
            $roleId = $request->route('role_id');
            $identifier = ['role_id' => $roleId];
            //Validate the Role Existence
            $validateRole = $this->run(new ValidateRoleByIdJob($identifier));

            if($validateRole === true){
                //Validate the Role is Deleted
                $checkRoleIsDeleted = $this->run(new CheckRoleIsDeleteJob($identifier['role_id']));
                if($checkRoleIsDeleted === false) {
                    //Fetch Permission list for the Role
                    $rolePermissionList = $this->run(new ListRolePermissionJob($roleId));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Role is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid role.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

            //Fetch All Permission list
            $permissionList = $this->run(new ListPermissionJob());
            //Merge both the permission list into one array
            $mergedPermissionList = ['system_permissions' => $permissionList, 'role_permissions' => $rolePermissionList];
            $response = $mergedPermissionList;
            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
