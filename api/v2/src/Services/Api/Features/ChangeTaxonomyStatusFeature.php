<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Taxonomy\Jobs\ChangeOfStatusForOldTaxonomySentToPRJob;
use Log;


class ChangeTaxonomyStatusFeature extends Feature
{
    use ErrorMessageHelper, ArrayHelper, StringHelper;

    public function handle(Request $request)
    {
        // set higher memory limit and execution time since this feature is resource intensive
        ini_set('memory_limit','4000M');
        ini_set('max_execution_time', 0);

        try{
            $requestInput       =   $request->all();
            $documentId         =   $requestInput['document_id'];
            $documentIdentifier =   ['document_id'  => $requestInput['document_id']];
            $validateRequest    =   $this->run(new ValidateDocumentByIdJob($documentIdentifier));
            if($validateRequest===true) {
                //$httpResponseMessage    =   [];
                $changeOfStatus = $this->run(new ChangeOfStatusForOldTaxonomySentToPRJob($requestInput['document_id']));

                if($changeOfStatus ==  true) {
                    $message = "Status of Taxonomy with id:$documentId has been  successfully changed.";
                } else {
                    $message = "Status of Taxonomy with id:$documentId has not been  successfully changed.";
                }

                $httpResponseMessage    =   $message;

                $successType = 'created';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validateRequest;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
