<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;

use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithDownloadFileJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Asset\Jobs\ValidateExemplarAssetFileByNameJob;
use App\Domains\Asset\Jobs\ValidateAssetFileForDocumentOrItemByNameJob;
use App\Domains\Asset\Jobs\GetFileMetadataByAssetNameJob;
use Log;

class GetAssetFileFromS3Feature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $isFileNameValid = false;
            $currentRoutePath = $request->path();
            $fileResponseType = strpos($currentRoutePath, 'preview') !== false ? "preview" : "download";
            // validate for asset associated with exemplar only
            if($request->route()->named('exemplar')) {
                $isFileNameValid = $this->run(ValidateExemplarAssetFileByNameJob::class);
            }

            // validation rule for asset associated with document or item only
            if($request->route()->named('doc_item')) {
                $isFileNameValid = $this->run(ValidateAssetFileForDocumentOrItemByNameJob::class);
            }

            if($isFileNameValid===true) {
                $fileMetadata = $this->run(new GetFileMetadataByAssetNameJob($request->route('asset_target_file_name')));
                $assetFileContent = $fileMetadata["asset_file_content"];
                $responseFileName = $fileMetadata["asset_name"];
                $contentType = $fileMetadata["asset_content_type"];
                $assetSizeInBytes = $fileMetadata["asset_size"];
                if(!empty($assetFileContent)) {
                    $responseHeaders = [
                        'Content-type' => $contentType,
                        'Content-Length' => $assetSizeInBytes
                    ];
                    if($fileResponseType==="preview") {
                        $responseHeaders['Content-Disposition'] = 'inline; filename="'.$responseFileName.'"';
                    }
                    else {
                        $responseHeaders['Content-Disposition'] = 'attachment; filename="'.$responseFileName.'"';
                    }
                    return \Response::make($assetFileContent, 200, $responseHeaders);
                }
                else {  
                    $errorType = 'validation_error';
                    $message = "Asset package not available.";
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errorType = 'validation_error';
                $message = "Invalid asset package.";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
