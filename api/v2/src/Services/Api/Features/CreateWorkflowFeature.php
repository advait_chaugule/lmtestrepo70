<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Workflow\Jobs\ValidateCreateWorkflowInputJob;
use App\Domains\Workflow\Jobs\CreateWorkflowJob;
use Log;

class CreateWorkflowFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;

    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $validateCreateWorkflowInput = $this->run(new ValidateCreateWorkflowInputJob($requestData));
            if($validateCreateWorkflowInput===true) {
          
                $workflowInput['workflow_id']= $this->createUniversalUniqueIdentifier();
                $workflowInput['organization_id']= $requestData["auth_user"]["organization_id"];
                $workflowInput['name']= $requestData['name'];
                $workflowInput['updated_by'] = $requestData['auth_user']['user_id'];
                $workflowInput['updated_at']= now()->toDateTimeString();
                $workflowInput['created_at']= now()->toDateTimeString();
                $workflowInput['is_default']= 0;

               $worflowCreated = $this->run(new CreateWorkflowJob($workflowInput));
                $successType = 'created';
                $message = 'Workflow created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $worflowCreated, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validateCreateWorkflowInput;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}

