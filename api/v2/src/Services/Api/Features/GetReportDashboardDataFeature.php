<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Report\Jobs\ValidateRequestToGetEventActivityLogJob;
use App\Domains\Report\Jobs\GetEventActivityLogJob;
use App\Domains\Report\Jobs\CreateProjectRelevantActivityLogJob;
use App\Domains\Report\Jobs\CreateUserLoginSessionRelevantActivityLogJob;
use App\Domains\Report\Jobs\CreateGraphDataForUserSessionJob;
use App\Domains\Report\Jobs\CreateEventActivityCountJob;
use App\Domains\Report\Jobs\CreateTopContributorListJob;
use App\Domains\Report\Jobs\CreateEventActivityListJob;
use App\Domains\Report\Jobs\CreateNewEventActivityCountJob;
use App\Domains\Report\Jobs\CreateProjectListCountJob;
use Log;

class GetReportDashboardDataFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        // set higher memory limit since this feature hugely depends on in-memory Collection and mapreducers operating on them
        ini_set('memory_limit','600M');

        try{
            $requestData = $request->all();
            $type['type']=!empty($requestData['type']) ? ($requestData['type']) : '';
            $parameter['activity_log_start_date'] = date("Y-m-d",strtotime("-1 month"));
            $parameter['activity_log_end_date'] = date("Y-m-d");
            $parameter['calendar_group_by_type'] = 'day';
            $requestData = array_merge($requestData,$parameter);
            $getType = array();
            if(($type['type'])!="")
            {
                $getType=explode(',',$type['type']);
            }
            $validateRequest = $this->run(new ValidateRequestToGetEventActivityLogJob($requestData));

            if($validateRequest===true){
                // Job to fetch activity data from OLAP database based on request data passed
                $eventActivityLog = $this->run(new GetEventActivityLogJob($requestData));
                // Job to fetch project related event activities only
                $projectRelevantActivitiyLog = $this->run(new CreateProjectRelevantActivityLogJob($eventActivityLog));
                // Job to fetch user session data viz. login and logout activities
                $userLoginSessionActivityLog = $this->run(new CreateUserLoginSessionRelevantActivityLogJob($eventActivityLog));
                // Job to create graph data of user session from event activity logs
                if (count($getType) > 0) {
                    if (in_array('user_session_details', $getType)) {
                        $userSessionDetails = $this->run(new CreateGraphDataForUserSessionJob($userLoginSessionActivityLog, $requestData));
                    }
                    if (in_array('count_metrices', $getType)) {
                        // Job to extract event activity count metrices from event activity logs
                        $activityCountMetrices = $this->run(new CreateEventActivityCountJob($eventActivityLog));
                    }
                    if (in_array('top_contributor_list', $getType)) {
                        // Job to create contributor list from eventActivityLogs
                        $topContributorList = $this->run(new CreateTopContributorListJob($projectRelevantActivitiyLog));
                    }
                    if (in_array('activity_list', $getType)) {
                        // Job to parse and create activity list data from event project-specific activity logs
                        $activityList = $this->run(new CreateEventActivityListJob($projectRelevantActivitiyLog));
                    }
                    if (in_array('new_count_metrices', $getType)) {
                        // Job to parse and create activity list data from event project-specific activity logs
                        $newActivityCountMetrices = $this->run(new CreateNewEventActivityCountJob($requestData));
                    }
                    if (in_array('project_list', $getType)) {
                        // Job to parse and create activity list data from event project-specific activity logs
                        $projectList = $this->run(new CreateProjectListCountJob($requestData));
                    }
                }
                else
                {
                    $userSessionDetails = $this->run(new CreateGraphDataForUserSessionJob($userLoginSessionActivityLog, $requestData));
                    $activityCountMetrices = $this->run(new CreateEventActivityCountJob($eventActivityLog));
                    $topContributorList = $this->run(new CreateTopContributorListJob($projectRelevantActivitiyLog));
                    $activityList = $this->run(new CreateEventActivityListJob($projectRelevantActivitiyLog));
                    $newActivityCountMetrices = $this->run(new CreateNewEventActivityCountJob($requestData));
                    $projectList = $this->run(new CreateProjectListCountJob($requestData));
                }

                $userSessionDetails = !empty($userSessionDetails) ? $userSessionDetails : array();
                $activityCountMetrices = !empty($activityCountMetrices) ? $activityCountMetrices : array();
                $topContributorList = !empty($topContributorList) ? $topContributorList : array();
                $activityList = !empty($activityList) ? $activityList : array();
                $newActivityCountMetrices = !empty($newActivityCountMetrices) ? $newActivityCountMetrices : array();
                $projectList = !empty($projectList) ? $projectList : array();

                $arrayKeysToCheck = array_flip($getType);
                $data = [
                    "user_session_details" => $userSessionDetails,
                    "count_metrices" => $activityCountMetrices,
                    "top_contributor_list" => $topContributorList,
                    "activity_list" => $activityList,
                    "new_count_metrices" => $newActivityCountMetrices,
                    "project_list" => $projectList
                ];
                if($type['type']!="")
                {
                    $data = array_intersect_key($data, $arrayKeysToCheck);
                }
                if(!empty($data)) {
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
                else {
                    $errorType = 'not_found';
                    $message = 'Data not found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errorType = 'validation_error';
                $message = $this->messageArrayToString($validateRequest);
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
