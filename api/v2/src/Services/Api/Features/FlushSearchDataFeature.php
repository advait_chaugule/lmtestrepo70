<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Search\Jobs\ClearSearchDataJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class FlushSearchDataFeature extends Feature
{

    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        // set higher memory limit and execution time since this feature is resource intensive
        ini_set('memory_limit','4000M');
        ini_set('max_execution_time', 0);

        try{
            $this->run(ClearSearchDataJob::class);
            $successType = 'found';
            $_status = 'custom_status_here';
            $httpResponseMessage = 'Search data cleared successfully.';
            return $this->run(new RespondWithJsonJob($successType, [], $httpResponseMessage, $_status));
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
