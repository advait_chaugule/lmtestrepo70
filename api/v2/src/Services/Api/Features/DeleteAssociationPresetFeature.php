<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Log;
use App\Domains\Association\Jobs\DeleteAssociationPresetJob;
use App\Domains\Association\Jobs\ValidatePresetJob;
use Lucid\Foundation\Feature;


class DeleteAssociationPresetFeature extends Feature
{
    public function handle(Request $request)
    {
       try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];
            $sourceTaxonomyTypeId       = $requestData['source_taxonomy_type_id'];
            $targetTaxonomyTypeId       = $requestData['target_taxonomy_type_id'];
            $associationType            = $requestData['association_type'];
            $response = [];
           
            $validatePreset = $this->run(new ValidatePresetJob($sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType,$organizationId)); //Validate Association Preset
            if($validatePreset == true){
                $response = $this->run(new DeleteAssociationPresetJob($organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType));
                $successType = 'custom_found';
                $_status = 'custom_status_here';
                $message = ($response === true) ? ['Association Preset deleted successfully.'] : ['Association Preset is in use.'];
                $is_success = ($response === true) ? true : false;
                return $this->run(new RespondWithJsonJob($successType,[], $message, $_status,[], $is_success));
            }else{
                $errorType = 'custom_validation_error';
                $_status = 'custom_status_here';
                $message = ['Invalid Association Preset.'];
                return $this->run(new RespondWithJsonJob($errorType, null, $message , $_status,[], false));
            }
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}