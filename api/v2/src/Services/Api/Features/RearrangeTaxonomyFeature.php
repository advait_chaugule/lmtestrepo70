<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Taxonomy\Jobs\UpdateTaxonomyOrderJob;
use Log;

class RearrangeTaxonomyFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $taxonomyId = $request->route('id');
            $input = [ 'id' => $taxonomyId ];

            $targetId     = $request->input('target_id');

            if($targetId != 0){
                $arrayOfOrder = $request->input('arrayOfOrder');

                $arrayOfId = [];
                foreach($arrayOfOrder as $key => $unOrderedArray){
                    $arrayOfId[$key] = $unOrderedArray['id'] ;
                }

                $updateOrder = $this->run(new UpdateTaxonomyOrderJob($targetId, $arrayOfId));

                $successType = 'found';
                $message = 'Data found.';
                return $this->run(new RespondWithJsonJob($successType, '', $message));
            }
            else{

                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = "Invalid Node Assignment";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
