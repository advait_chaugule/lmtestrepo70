<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ValidateLoginInputJob;
use App\Domains\User\Jobs\LoginJob;
use App\Domains\Organization\Jobs\ValidateUsersOrganizationJob;
use App\Domains\User\Jobs\IsCheckUserCurrentStatusJob;
use App\Domains\User\Jobs\UpdateAccessTokenJob;

use App\Domains\User\Events\UserLoginEvent;
use App\Domains\User\Events\SessionRefreshedEvent;
use Illuminate\Routing\UrlGenerator;
use Log;
//use App\Services\Api\Traits\CloudWatchTrait;

class CISLoginFeature extends Feature
{
    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {

        try
        {   
            // input parameters
            $input = $request->all();
            $login_array['username'] = $input['username'];
            $login_array['password'] = $input['password'];
            $dataEmail = ['email' => $input['username']];
            
            // method to generate access token
            $userAuthenticatedResponse = $this->checkAuthIdentityUser($login_array);
            // dump($userAuthenticatedResponse);exit();
            // if access token generated successfully proceed with ACMT login else throw an error
            // if($userAuthenticatedResponse['allow_login'] == true)
            $loginStatus = true;
            if($loginStatus)
            {
                // input params for introspect api
                // $header[] = "Authorization: Basic " . env('AUTH_BASICTOKEN');
                // $intro_data['token'] = $userAuthenticatedResponse['central_auth_token'];

                // $curl = curl_init();
                // curl_setopt($curl, CURLOPT_URL, 'https://oauth-qa.learningmate.co/devoauth/connect/introspect');
                // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
                // curl_setopt($curl, CURLOPT_POSTFIELDS, $intro_data);
                // curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                // $introspect_result = curl_exec($curl);
                // $intro_result = json_decode($introspect_result, true);

                // dump($intro_result);exit();

                // ACMT login process starts
                $validationStatus = $this->run(new ValidateLoginInputJob($input));

                if($validationStatus===true)
                {
                    $domainUrl  = url('/'); // Get domain name
                    $domainPath = parse_url($domainUrl);
                    $domainName = '';

                    if($domainPath)
                    {
                        $domainName = $domainPath['host'];
                    }

                    if(strpos($domainName,'localhost')!==false)
                    {
                        $baseUrl = env("APP_URL");
                    }
                    else
                    {
                        if(strpos($domainName,'api.')!==false)
                        {
                            $baseUrl = str_replace('api.', '',$domainName);
                        }else{
                            $baseUrl = $domainName;
                        }
                    }

                    $checkUsersOrganization = $this->run(new ValidateUsersOrganizationJob($dataEmail,$baseUrl)); // Validate domain name

                    if($checkUsersOrganization!=1)
                    {
                        $successType = 'validation_error';
                        $message = 'Invalid username or password .';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($successType, $message, $_status));
                    }

                    $validationStatus = $this->run(new IsCheckUserCurrentStatusJob($dataEmail));

                    if($validationStatus != 1)
                    {
                        $loginResponse = $this->run(new LoginJob($input));
                        $loginStatus = $loginResponse["loginStatus"];

                        if($loginStatus!==false)
                        {
                            if(!isset($responseBody["organization"]["organization_id"]))

                            $responseBody = $loginResponse["responseBody"];
                            $userId = $responseBody["user_id"];

                            $this->run(new UpdateAccessTokenJob($userId));

                            $requestUserDetails = [
                                "user_id" => $responseBody["user_id"],
                                "first_name" => $responseBody["first_name"],
                                "last_name" => $responseBody["last_name"],
                                "organization" => $responseBody["organization"]
                            ];
    
                            $eventData = [
                                "beforeEventRawData" => [],
                                "afterEventRawData" => $requestUserDetails,
                                "requestUserDetails" => $requestUserDetails
                            ];
                            event(new UserLoginEvent($eventData));

                            // raise user session refreshed event ( to indicate session start time )
                            $dataForUserSessionRefreshedEvent = [
                                "user_id" => $userId,
                                "organization_id" => $responseBody["organization"]["organization_id"],
                                "first_name" => $responseBody["first_name"],
                                "last_name" => $responseBody["last_name"]
                            ];
                            $userSessionStartedEventData = [
                                "beforeEventRawData" => $dataForUserSessionRefreshedEvent,
                                "afterEventRawData" => $dataForUserSessionRefreshedEvent,
                                "requestUserDetails" => $dataForUserSessionRefreshedEvent
                            ];
                            event(new SessionRefreshedEvent($userSessionStartedEventData));
                            
                            // method to call introspect api

                            

                            $successType = 'found';
                            $message = 'Login successfull.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $responseBody, $message, $_status));


                        }
                        else
                        {
                            $errorType = 'authorization_error';
                            $message = $loginResponse["responseMessageList"];//$this->arrayContentToCommaeSeparatedString();
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                        }

                    }
                    else
                    {
                        $errorType = 'authorization_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'This account is not verified yet. Kindly verify your email';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }

                }
                else
                {
                    $errorType = 'validation_error';
                    $message = $this->messageArrayToString($validationStatus);
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }

            }
            else
            {
                $successType = 'validation_error';
                $message = 'Access token failed to generate.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($successType, $message, $_status));
            }

        }
        catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);

            ///$this->CloudWatchLogger($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }


    // function to generate access token
    public function checkAuthIdentityUser($login_array)
    {
        // input parameters required for calling the access token api
        $data = 
                [
                    'username' => $login_array['username'],
                    'password' => $login_array['password'], 
                    'client_id' => env('AUTH_CLIENT_ID'),
                    'client_secret' => env('AUTH_CLIENT_SECRET'),
                    'grant_type' => env('AUTH_GRANT_TYPE'),
                    'scopes' => env('AUTH_SCOPE') 
                ];

        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://oauth-qa.learningmate.co/devoauth/connect/token');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);            
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);
        $result =  json_decode($result, true);
        curl_close($curl);
        
        if( isset($result['access_token']) && !empty($result['access_token']) )
        {
            $output_array['central_auth_token'] =  $result['access_token'];
            $output_array['refresh_token'] = isset($result['refresh_token']) ? $result['refresh_token'] : '';
            $output_array['expires_in'] =  $result['expires_in'];
            $output_array['allow_login'] = true;

            return $output_array;
        }
        else
        {
            $output_array['allow_login'] = false;

        }
                        

    }


}
