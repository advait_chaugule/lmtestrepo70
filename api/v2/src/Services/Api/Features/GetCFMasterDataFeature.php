<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use Log;

class GetCFMasterDataFeature extends Feature {

    public function handle(Request $request) {
        try {
            $language = [["id" => "42ef69f9-8b02-43ee-85e2-83a5de6998b9", "name" => "English", "short_code" => "en"]];
            $item_type = [["id" => "a3a738cc-db86-4603-b729-6df44a229a8b", "title" => "Item Type 1"]];
            $adoption_status = [["id" => "2", "value" => "Private Draft"],
                    ["id" => "1", "value" => "Draft"],
                    ["id" => "3", "value" => "Adopted"],
                    ["id" => "4", "value" => "Deprecated"]];
            //$publisher = [["id" => "a6d6e9a1-d4dc-49de-ae3c-b29858aa5518", "name" => "Publisher 1"]];
            //$organization = [["id" => "933680a5-d116-4577-bb5b-4fc4ace437cb", "name" => "Organization 1"]];
            $response = ['language' => $language, 'adoption_status' => $adoption_status, 'item_type' => $item_type]; // 'publisher' => $publisher, 'organization' => $organization,
            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}

