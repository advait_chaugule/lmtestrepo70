<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\CaseAssociation\Jobs\DeleteCaseAssociationValidationJob;
use App\Domains\CaseAssociation\Jobs\DeleteCaseAssociationJob;

use App\Domains\CaseAssociation\Jobs\GetOriginNodeIdJob;
//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use Log;

class DeleteCaseAssociationFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $associationIdentifier = $request->route('association_id');
            $validation = $this->run(new DeleteCaseAssociationValidationJob(["association_id" => $associationIdentifier]));
            if($validation===true) {

                //get origin node id from acmt_item_associations
                $originNodeId=$this->run(new GetOriginNodeIdJob($associationIdentifier));
                //job to update project and document table after comment is added
                if(!empty($originNodeId)){
                    //event for project and document update(updated_at) start
                    event(new UpdateTaxonomyTimeEvent($originNodeId));
                    //event for project and document update(updated_at) end

                    $this->raiseEventToUploadTaxonomySearchDataToSqs($originNodeId);
                }    
                $this->run(new DeleteCaseAssociationJob($associationIdentifier));

                $successType = 'found';
                $message = 'Association deleted successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = "Invalid association id provided.";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $itemIdentifier) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_item"];
        $eventData = [
            "type_id" => $itemIdentifier,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
