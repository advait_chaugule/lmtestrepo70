<?php
namespace App\Services\Api\Features;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\CaseStandard\Jobs\ValidateDocumentBySourceIdJob;
use App\Domains\CaseStandard\Jobs\CustomMetadataJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use Log;
use App\Services\Api\Traits\SavingIMSResponseTrait;

class GetCustomMetadataFeature extends Feature
{
    use ErrorMessageHelper,StringHelper,UuidHelperTrait,SavingIMSResponseTrait;
    public function handle(Request $request)
    {
        try{
            $identifier         = $request->route('identifier');
            $documentIdentifier = isset($request['document_id']) ? $request['document_id'] :'';
            $orgCode            = isset($request['org_code'])?$request['org_code']:'';
            $requestUrl         = url('/');
            $validateUUID = $this->isUuidValid($identifier);
            if($validateUUID===true){
                $validationStatus = $this->run(new ValidateDocumentBySourceIdJob($identifier));
                if($validationStatus===true){
                    $customKey  = ['identifier' =>$identifier,'prefix' => 'customData'];
                    $metadata   = $this->run(new GetFromCacheJob($customKey));
                    if(isset($metadata['status']) && $metadata['status'] == "error") {
                        $metadata = $this->run(new CustomMetadataJob($identifier, $orgCode, $documentIdentifier,$requestUrl));
                        if (!empty($metadata) && count($metadata) > 0) {
                            $this->run(new DeleteFromCacheJob($customKey));
                            $this->run(new SetToCacheJob($metadata, $customKey));
                            $metadata = $this->run(new GetFromCacheJob($customKey));
                        }
                    }
                    if($metadata) {
                        return $this->respondWithCASEJsonSuccess($metadata);
                    }else{
                        return $this->respondWithCASEJsonError(404);
                    }

                }else{
                    return $this->respondWithCASEJsonError(404);
                }
            }else{
                return $this->respondWithCASEJsonError(404, false);
            }
        }catch(\Exception $ex){
            Log::error($ex);
            return $this->respondWithCASEJsonError(500);
        }
    }

    private function respondWithCASEJsonSuccess($data) {
        $responseReturn = response()->json($data, 200, [], JSON_UNESCAPED_SLASHES);
        $this->IMSResponse($responseReturn);
        return $responseReturn;
    }

    private function respondWithCASEJsonError($httpStatus, $validUUID = true)
    {
        $errorResponseBody = [
            "imsx_codeMajor" => "failure",
            "imsx_severity" => "error",
            "imsx_description" => ""
        ];

        switch ($httpStatus) {

            case '400':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                    "imsx_codeMinorFieldName" => "sourcedId",
                    "imsx_codeMinorFieldValue" => "invalid_selection_field"
                ];
                break;

            case '401':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                    "imsx_codeMinorFieldName" => "sourcedId",
                    "imsx_codeMinorFieldValue" => "unauthorisedrequest"
                ];
                break;

            case '403':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                    "imsx_codeMinorFieldName" => "sourcedId",
                    "imsx_codeMinorFieldValue" => "forbidden"
                ];
                break;

            case '404':
                $imsx_codeMinorFieldValue = $validUUID ? "unknownobject" : "invaliduuid";
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                    "imsx_codeMinorFieldName" => "sourcedId",
                    "imsx_codeMinorFieldValue" => $imsx_codeMinorFieldValue
                ];
                break;

            case '429':
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                    "imsx_codeMinorFieldName" => "sourcedId",
                    "imsx_codeMinorFieldValue" => "server_busy"
                ];
                break;

            default:
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                    "imsx_codeMinorFieldName" => "",
                    "imsx_codeMinorFieldValue" => "internal_server_error"
                ];
                break;
        }
        if (!$validUUID) {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES)
                ->setStatusCode(404, 'Invalid UUID');
        } else {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES);
        }
    }
}
