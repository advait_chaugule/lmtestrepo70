<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;   

use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Workflow\Jobs\CheckWorkflowStateJob;
use App\Domains\Workflow\Jobs\GetAllWorkflowRolesJob;
use Log;

class GetAllWorkflowRolesFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $workflowId = $request->route('workflow_id');
            $organizationId = $request->all();
            $orgIdentifier = $organizationId['auth_user']['organization_id'];
            $identifier = ['workflow_id' => $workflowId];

            $validateStatus = $this->run(new ValidateWorkflowIdJob($identifier));
            if($validateStatus === true){
                $deleteStatus = ['is_deleted' => '1'];
                $checkIsWorkflowDeleted = $this->run(new CheckWorkflowStateJob($identifier, $deleteStatus));
                if(!$checkIsWorkflowDeleted === true){
                    $activeStatus = ['is_active' => '1'];
                    $checkIsWorkflowActive = $this->run(new CheckWorkflowStateJob($identifier, $activeStatus));

                    if($checkIsWorkflowActive) {
                        $workFlowStageRoles = $this->run(new GetAllWorkflowRolesJob($identifier, $orgIdentifier));
                        
                        $response = $workFlowStageRoles;
                        $successType = 'found';
                        $message = 'Data found.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else{
                        // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                        $errorType = 'not_found_error';
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        $message = 'Workflow is inactive';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Workflow is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Workflow Id selected is invalid';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
