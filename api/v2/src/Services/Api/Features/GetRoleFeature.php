<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ValidateRoleByIdJob;
use App\Domains\Role\Jobs\CheckRoleIsDeleteJob;
use App\Domains\Role\Jobs\GetRoleDetailJob;
use Log;

class GetRoleFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $roleId = $request->route('role_id');
            $identifier = ['role_id' => $roleId];
            $validateRoleStatus = $this->run(new ValidateRoleByIdJob($identifier));

            if($validateRoleStatus === true) {
                $checkRoleDeletedStatus = $this->run(new CheckRoleIsDeleteJob($roleId));
                if(!$checkRoleDeletedStatus === true){
                    $roleDetail = $this->run(new GetRoleDetailJob($roleId));

                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $roleDetail, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Role is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid role.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
