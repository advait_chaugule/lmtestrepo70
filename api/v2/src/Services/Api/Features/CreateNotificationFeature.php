<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Notifications\Jobs\ValidateNotificationById;
use App\Domains\Notifications\Jobs\NotificationsJob;
use Log;

class CreateNotificationFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData       = $request->all();
            $userId    = ['user_id'=>$requestData['auth_user']['user_id']];
            $validationStatus  = $this->run(new ValidateNotificationById($userId));
            if($validationStatus === true) {
            $insertData = $this->run(new NotificationsJob($userId));
            if($insertData) {
                $successType = 'found';
                $message = 'Success.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, '', $message, $_status));
            }else{
                $errorType = 'not_found';
                $message = 'Failed to update notification';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            }else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Validation error';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message   = $ex->getMessage();
            $_status   = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}