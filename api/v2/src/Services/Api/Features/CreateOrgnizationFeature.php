<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\AddDefaultSettings;

use App\Domains\User\Jobs\ValidateCreateUserInputJob;
use App\Domains\User\Jobs\CreateOrganizationUserJob;

use App\Domains\Role\Jobs\ValidateCreateRoleInputJob;
use App\Domains\Role\Jobs\CreateRoleJob;

use App\Domains\Permission\Jobs\ListAllPermissionJob;
use App\Domains\Permission\Jobs\ListAllPermissionPRGLJob;
use App\Domains\Permission\Jobs\ListAllPermissionPRTLJob;
use App\Domains\Role\Jobs\RolePermissionSetJob;

use App\Domains\Organization\Jobs\ValidateCreateOrganizationInputJob;
use App\Domains\Organization\Jobs\CreateOrganizationJob;
use App\Domains\Workflow\Jobs\CreateWorkflowJob;
use App\Domains\Language\Jobs\CreateLanguageJob;
use App\Domains\WorkflowStage\Jobs\CreateWorkflowStageJob;
use App\Domains\Stage\Jobs\CreateStageJob;
use App\Domains\NodeType\Jobs\CreateDefaultNodeTypeJob;
use App\Domains\Metadata\Jobs\CreateDefaultMetaDataJob;
use App\Domains\NodeType\Jobs\SaveNodeTypeMetadataTenantJob;
use App\Domains\WorkflowStage\Jobs\CreateWorkflowStageRoleJob;

use App\Domains\Organization\Jobs\ValidateShortCodeInputJob;
use Log;
class CreateOrgnizationFeature extends Feature
{
    use UuidHelperTrait, ErrorMessageHelper,AddDefaultSettings;
    public function handle(Request $request)
    {
       
        try { 
       $requestVar = $request->all();
       $requestUserDetails = $request->input("auth_user");
        if($requestUserDetails['is_super_admin'] == 1)
        {
            $input['organization_id'] =  $this->createUniversalUniqueIdentifier();
            $input['current_organization_id'] =  $input['organization_id'];
            $organizationInput['organization_id'] = $input['organization_id'];
            $organizationInput['name'] = $requestVar['organization_name'];
            $organizationInput['org_code'] = $requestVar['shortcode'];
            $organizationInput['default'] = 0;
            $organizationInput['is_deleted'] = 0;
            $organizationInput['created_at'] = now()->toDateTimeString();
            $organizationInput['updated_at'] = now()->toDateTimeString();
            // Fields created by, updated by and is_active added
            $organizationInput['created_by'] = $requestUserDetails['user_id'];    
            $organizationInput['updated_by'] = $requestUserDetails['user_id'];         
            $organizationInput['is_active'] = 1;

            $validateOrganizationData = $this->run(new ValidateCreateOrganizationInputJob($organizationInput));
            if($validateOrganizationData == 1)
            {
                $shortCodeInput['org_code'] = $organizationInput['org_code'];
                $validateShortCodeData = $this->run(new ValidateShortCodeInputJob($shortCodeInput));
                if($validateShortCodeData===true) {
                $input['user_id'] = $this->createUniversalUniqueIdentifier();
                $input['active_access_token'] = $this->createUniversalUniqueIdentifier();
                $input['email'] = $requestVar['admin_account'];
                $validationStatus = $this->run(new ValidateCreateUserInputJob($input));
                if($validationStatus===true) {
                $orgCreated = $this->run(new CreateOrganizationJob($organizationInput));
                    $workflowInput['workflow_id']= $this->createUniversalUniqueIdentifier();
                    $workflowInput['organization_id']= $input['organization_id'];
                    $workflowInput['name']= 'Default workflow';
                    $workflowInput['updated_at']= now()->toDateTimeString();
                    $workflowInput['created_at']= now()->toDateTimeString();
                    $workflowInput['is_default']= 1;

                $worflowCreated = $this->run(new CreateWorkflowJob($workflowInput));
                    $stageNameArr = [
                        "Authoring",
                        "Internal Review",
                        "Stakeholder review",
                        "Ready for stakeholder review",
                        "Public review & feedback",
                        "Adjudication",
                        "Final stakeholder approval",
                        "Published"
            ];
        
            foreach ($stageNameArr as $key => $stage)
            {
                $stageInput['stage_id'] = $this->createUniversalUniqueIdentifier();
                $stageInput['organization_id'] = $input['organization_id'];
                $stageInput['name'] = $stage;
                $stageInput['order'] = $key + 1;
                $stageInput['is_deleted'] = 0;
            $stageCreated = $this->run(new CreateStageJob($stageInput));
                $StageIdArr[] = $stageInput['stage_id'];
                unset($stageInput);
            }
            
            $worflowStageNameArr = [
                "Authoring",
                "Internal Review",
                "External review",
                "Published"
    ];
    foreach ($worflowStageNameArr as $key1 => $stage1)
            {

                $workflowstageInput['workflow_stage_id'] = $workflowInput['workflow_id'].'||'.$StageIdArr[$key1];
                $workflowstageInput['workflow_id'] = $workflowInput['workflow_id'];
                $workflowstageInput['stage_id'] = $StageIdArr[$key1];
                $workflowstageInput['order'] = $key1 + 1;
                $workflowstageInput['stage_name'] = $stage1;
                $workflowstageInput['stage_description'] = $stage1;
            $worflowStageCreated = $this->run(new CreateWorkflowStageJob($workflowstageInput));
                unset($workflowstageInput);
            }

            $languageNameArr = [
                "English",
                "German",
                "Afrikaans"
    ];

    $shortCodeArr = [
        "en",
        "de",
        "af"
    ];

    foreach ($languageNameArr as $key2 => $language)
            {

                $languageInput['language_id'] = $this->createUniversalUniqueIdentifier();
                $languageInput['name'] = $language;
                $languageInput['short_code'] = $shortCodeArr[$key2];
                $languageInput['is_deleted'] = 0;
                $languageInput['organization_id'] = $input['organization_id'];
                $languageInput['created_at'] = now()->toDateTimeString();
                $languageInput['updated_at'] = now()->toDateTimeString();
                $langauageCreated = $this->run(new CreateLanguageJob($languageInput));
                unset($languageInput);

            }
            $nodTypeArr = [
                "Default",
                "Strand",
                "Conceptual Category",
                "Component",
                "Grade Level",
                "Cluster",
                "Document",
                "Standard",
                "Domain"
            ];
            $documentOrderArr=array("1"=>"Creator",
                                "2"=>"Title",
                                "3"=>"Official Source URL",
                                "4"=>"Publisher",
                                "5"=>"Description",
                                "6"=>"Subject Title",
                                "7"=>"Subject Hierarchy Code",
                                "8"=>"Subject Description",
                                "9"=>"Language Name",
                                "10"=>"Version",
                                "11"=>"Status Start Date",
                                "12"=>"Status End Date",
                                "13"=>"License Title",
                                "14"=>"License Description",
                                "15"=>"License Text",
                                "16"=>"Notes",
                                );
            $nondocumentOrderArr=array("1"=>"Full Statement",
                                "2"=>"Alternative Label",
                                "3"=>"Official Source URL",
                                "4"=>"Human Coding Scheme",
                                "5"=>"List Enumeration",
                                "6"=>"Abbreviated Statement",
                                "7"=>"Concept Title",
                                "8"=>"Concept Keywords",
                                "9"=>"Concept Hierarchy Code",
                                "10"=>"Concept Description",
                                "11"=>"Notes",
                                "12"=>"Language Name",
                                "13"=>"Education Level",
                                "14"=>"License Title",
                                "15"=>"License Description",
                                "16"=>"License Text",
                                "17"=>"Status Start Date",
                                "18"=>"Status End Date",
                                );
        
            foreach ($nodTypeArr as $key3 => $nodType)
            {
                $nodTypeInput['node_type_id'] = $this->createUniversalUniqueIdentifier();
                $nodTypeInput['organization_id'] = $input['organization_id'];
                $nodTypeInput['title'] = $nodType;
                $nodTypeInput['type_code'] = $nodType;
                $nodTypeInput['hierarchy_code'] = "";
                $nodTypeInput['description'] = "";
                $nodTypeInput['source_node_type_id'] = $nodTypeInput['node_type_id'];
                if($nodType == 'Document')
                {
                    $nodTypeInput['is_document'] = 1;
                }
                else
                {
                    $nodTypeInput['is_document'] = 0;
                }
                $nodTypeInput['is_custom'] = 0;
                $nodTypeInput['is_deleted'] = 0;
                if($nodType == 'Default')
                {
                    $nodTypeInput['is_default'] = 1;
                }
                else
                {
                    $nodTypeInput['is_default'] = 0;
                }
                $nodTypeInput['created_by'] = "";
                $nodTypeInput['updated_by'] = "";
                $nodTypeInput['created_at'] = now()->toDateTimeString();
                $nodTypeInput['updated_at'] = now()->toDateTimeString();
                $nodTypeCreated = $this->run(new CreateDefaultNodeTypeJob($nodTypeInput));
            $nodeTypeData[] = ['id' => $nodTypeInput['node_type_id'], 'is_document' => $nodTypeInput['is_document'], 'node_name' =>  $nodType];
                unset($nodTypeInput);
            }

            //Metadata Table Entry

            
    $metaDataInput1["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput1["organization_id"] = $input['organization_id']; 
    $metaDataInput1["parent_metadata_id"] = "";
    $metaDataInput1["name"] = "Creator";
    $metaDataInput1["internal_name"] = "creator";
    $metaDataInput1["field_type"] = "1";
    $metaDataInput1["field_possible_values"] = "";
    $metaDataInput1["order"] =  '1';
    $metaDataInput1["is_custom"] = "0";
    $metaDataInput1["is_document"] = "1";
    $metaDataInput1["is_mandatory" ] =  '1';
    $metaDataInput1["is_active"] = "1";
    $metaDataInput1["is_deleted"] = "0";
    $metaDataInput1["updated_by"] = "";
    $metaDataInput1["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput1)); 
    $metadataArr[0] = ['id' =>$metaDataInput1["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput1["name"]];


    $metaDataInput2["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput2["organization_id"] = $input['organization_id']; 
    $metaDataInput2["parent_metadata_id"] = "";
    $metaDataInput2["name"] = "Title";
    $metaDataInput2["internal_name"] = "title";
    $metaDataInput2["field_type"] = "1";
    $metaDataInput2["field_possible_values"] = "";
    $metaDataInput2["order"] =  '2';
    $metaDataInput2["is_custom"] = "0";
    $metaDataInput2["is_document"] = "1";
    $metaDataInput2["is_mandatory" ] =  '0';
    $metaDataInput2["is_active"] = "1";
    $metaDataInput2["is_deleted"] = "0";
    $metaDataInput2["updated_by"] = "";
    $metaDataInput2["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput2)); 
    $metadataArr[1] = ['id' =>$metaDataInput2["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput2["name"]];
            
    $metaDataInput3["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput3["organization_id"] = $input['organization_id']; 
    $metaDataInput3["parent_metadata_id"] = "";
    $metaDataInput3["name"] = "Official Source URL";
    $metaDataInput3["internal_name"] = "official_source_url";
    $metaDataInput3["field_type"] = "1";
    $metaDataInput3["field_possible_values"] = "";
    $metaDataInput3["order"] =  '3';
    $metaDataInput3["is_custom"] = "0";
    $metaDataInput3["is_document"] = "1";
    $metaDataInput3["is_mandatory" ] =  '0';
    $metaDataInput3["is_active"] = "1";
    $metaDataInput3["is_deleted"] = "0";
    $metaDataInput3["updated_by"] = "";
    $metaDataInput3["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput3)); 
    $metadataArr[2] = ['id' =>$metaDataInput3["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput3["name"]];
            
    $metaDataInput4["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput4["organization_id"] = $input['organization_id']; 
    $metaDataInput4["parent_metadata_id"] = "";
    $metaDataInput4["name"] = "Publisher";
    $metaDataInput4["internal_name"] = "publisher";
    $metaDataInput4["field_type"] = "1";
    $metaDataInput4["field_possible_values"] = "";
    $metaDataInput4["order"] =  '4';
    $metaDataInput4["is_custom"] = "0";
    $metaDataInput4["is_document"] = "1";
    $metaDataInput4["is_mandatory" ] =  '0';
    $metaDataInput4["is_active"] = "1";
    $metaDataInput4["is_deleted"] = "0";
    $metaDataInput4["updated_by"] = "";
    $metaDataInput4["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput4)); 
    $metadataArr[3] = ['id' =>$metaDataInput4["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput4["name"]];
            
    $metaDataInput5["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput5["organization_id"] = $input['organization_id']; 
    $metaDataInput5["parent_metadata_id"] = "";
    $metaDataInput5["name"] = "Description";
    $metaDataInput5["internal_name"] = "description";
    $metaDataInput5["field_type"] = "2";
    $metaDataInput5["field_possible_values"] = "";
    $metaDataInput5["order"] =  '5';
    $metaDataInput5["is_custom"] = "0";
    $metaDataInput5["is_document"] = "1";
    $metaDataInput5["is_mandatory" ] =  '0';
    $metaDataInput5["is_active"] = "1";
    $metaDataInput5["is_deleted"] = "0";
    $metaDataInput5["updated_by"] = "";
    $metaDataInput5["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput5)); 
    $metadataArr[4] = ['id' =>$metaDataInput5["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput5["name"]];
            
    $metaDataInput6["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput6["organization_id"] = $input['organization_id']; 
    $metaDataInput6["parent_metadata_id"] = "";
    $metaDataInput6["name"] = "Subject Title";
    $metaDataInput6["internal_name"] = "subject_title";
    $metaDataInput6["field_type"] = "1";
    $metaDataInput6["field_possible_values"] = "";
    $metaDataInput6["order"] =  '6';
    $metaDataInput6["is_custom"] = "0";
    $metaDataInput6["is_document"] = "1";
    $metaDataInput6["is_mandatory" ] =  '0';
    $metaDataInput6["is_active"] = "1";
    $metaDataInput6["is_deleted"] = "0";
    $metaDataInput6["updated_by"] = "";
    $metaDataInput6["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput6)); 
    $metadataArr[5] = ['id' =>$metaDataInput6["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput6["name"]];
                
    $metaDataInput7["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput7["organization_id"] = $input['organization_id']; 
    $metaDataInput7["parent_metadata_id"] = $metaDataInput6["metadata_id"];
    $metaDataInput7["name"] = "Subject Hierarchy Code";
    $metaDataInput7["internal_name"] = "subject_hierarchy_code";
    $metaDataInput7["field_type"] = "1";
    $metaDataInput7["field_possible_values"] = "";
    $metaDataInput7["order"] =  '7';
    $metaDataInput7["is_custom"] = "0";
    $metaDataInput7["is_document"] = "1";
    $metaDataInput7["is_mandatory" ] =  '0';
    $metaDataInput7["is_active"] = "1";
    $metaDataInput7["is_deleted"] = "0";
    $metaDataInput7["updated_by"] = "";
    $metaDataInput7["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput7)); 
    $metadataArr[6] = ['id' =>$metaDataInput7["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput7["name"]];
            
    $metaDataInput8["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput8["organization_id"] = $input['organization_id']; 
    $metaDataInput8["parent_metadata_id"] = $metaDataInput6["metadata_id"];
    $metaDataInput8["name"] = "Subject Description";
    $metaDataInput8["internal_name"] = "subject_description";
    $metaDataInput8["field_type"] = "2";
    $metaDataInput8["field_possible_values"] = "";
    $metaDataInput8["order"] =  '8';
    $metaDataInput8["is_custom"] = "0";
    $metaDataInput8["is_document"] = "1";
    $metaDataInput8["is_mandatory" ] =  '0';
    $metaDataInput8["is_active"] = "1";
    $metaDataInput8["is_deleted"] = "0";
    $metaDataInput8["updated_by"] = "";
    $metaDataInput8["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput8)); 
    $metadataArr[7] = ['id' =>$metaDataInput8["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput8["name"]];
            
    $metaDataInput9["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput9["organization_id"] = $input['organization_id']; 
    $metaDataInput9["parent_metadata_id"] = "";
    $metaDataInput9["name"] = "Language Name";
    $metaDataInput9["internal_name"] = "language";
    $metaDataInput9["field_type"] = "3";
    $metaDataInput9["field_possible_values"] = "English (en), German (gn)";
    $metaDataInput9["last_field_possible_values"] = "English (en), German (gn)";
    $metaDataInput9["order"] =  '9';
    $metaDataInput9["is_custom"] = "0";
    $metaDataInput9["is_document"] = "2";
    $metaDataInput9["is_mandatory" ] =  '0';
    $metaDataInput9["is_active"] = "1";
    $metaDataInput9["is_deleted"] = "0";
    $metaDataInput9["updated_by"] = "";
    $metaDataInput9["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput9)); 
    $metadataArr[8] = ['id' =>$metaDataInput9["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput9["name"]];
            
    $metaDataInput10["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput10["organization_id"] = $input['organization_id']; 
    $metaDataInput10["parent_metadata_id"] = "";
    $metaDataInput10["name"] = "Version";
    $metaDataInput10["internal_name"] = "version";
    $metaDataInput10["field_type"] = "1";
    $metaDataInput10["field_possible_values"] = "";
    $metaDataInput10["order"] =  '10';
    $metaDataInput10["is_custom"] = "0";
    $metaDataInput10["is_document"] = "1";
    $metaDataInput10["is_mandatory" ] =  '0';
    $metaDataInput10["is_active"] = "1";
    $metaDataInput10["is_deleted"] = "0";
    $metaDataInput10["updated_by"] = "";
    $metaDataInput10["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput10)); 
    $metadataArr[9] = ['id' =>$metaDataInput10["metadata_id"], 'is_document' => "1", 'metadate_name' => $metaDataInput10["name"]];
            
    $metaDataInput11["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput11["organization_id"] = $input['organization_id']; 
    $metaDataInput11["parent_metadata_id"] = "";
    $metaDataInput11["name"] = "Status Start Date";
    $metaDataInput11["internal_name"] = "status_start_date";
    $metaDataInput11["field_type"] = "4";
    $metaDataInput11["field_possible_values"] = "";
    $metaDataInput11["order"] =  '11';
    $metaDataInput11["is_custom"] = "0";
    $metaDataInput11["is_document"] = "2";
    $metaDataInput11["is_mandatory" ] =  '0';
    $metaDataInput11["is_active"] = "1";
    $metaDataInput11["is_deleted"] = "0";
    $metaDataInput11["updated_by"] = "";
    $metaDataInput11["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput11)); 
    $metadataArr[10] = ['id' =>$metaDataInput11["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput11["name"]];
            
    $metaDataInput12["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput12["organization_id"] = $input['organization_id']; 
    $metaDataInput12["parent_metadata_id"] = "";
    $metaDataInput12["name"] = "Status End Date";
    $metaDataInput12["internal_name"] = "status_end_date";
    $metaDataInput12["field_type"] = "4";
    $metaDataInput12["field_possible_values"] = "";
    $metaDataInput12["order"] =  '12';
    $metaDataInput12["is_custom"] = "0";
    $metaDataInput12["is_document"] = "2";
    $metaDataInput12["is_mandatory" ] =  '0';
    $metaDataInput12["is_active"] = "1";
    $metaDataInput12["is_deleted"] = "0";
    $metaDataInput12["updated_by"] = "";
    $metaDataInput12["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput12)); 
    $metadataArr[11] = ['id' =>$metaDataInput12["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput12["name"]];
            
    $metaDataInput13["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput13["organization_id"] = $input['organization_id']; 
    $metaDataInput13["parent_metadata_id"] = "";
    $metaDataInput13["name"] = "License Title";
    $metaDataInput13["internal_name"] = "license_title";
    $metaDataInput13["field_type"] = "1";
    $metaDataInput13["field_possible_values"] = "";
    $metaDataInput13["order"] =  '13';
    $metaDataInput13["is_custom"] = "0";
    $metaDataInput13["is_document"] = "2";
    $metaDataInput13["is_mandatory" ] =  '0';
    $metaDataInput13["is_active"] = "1";
    $metaDataInput13["is_deleted"] = "0";
    $metaDataInput13["updated_by"] = "";
    $metaDataInput13["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput13)); 
    $metadataArr[12] = ['id' =>$metaDataInput13["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput13["name"]];
            
    $metaDataInput14["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput14["organization_id"] = $input['organization_id']; 
    $metaDataInput14["parent_metadata_id"] = $metaDataInput13["metadata_id"];
    $metaDataInput14["name"] = "License Description";
    $metaDataInput14["internal_name"] = "license_description";
    $metaDataInput14["field_type"] = "2";
    $metaDataInput14["field_possible_values"] = "";
    $metaDataInput14["order"] =  '14';
    $metaDataInput14["is_custom"] = "0";
    $metaDataInput14["is_document"] = "2";
    $metaDataInput14["is_mandatory" ] =  '0';
    $metaDataInput14["is_active"] = "1";
    $metaDataInput14["is_deleted"] = "0";
    $metaDataInput14["updated_by"] = "";
    $metaDataInput14["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput14)); 
    $metadataArr[13] = ['id' =>$metaDataInput14["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput14["name"]];
            
    $metaDataInput15["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput15["organization_id"] = $input['organization_id']; 
    $metaDataInput15["parent_metadata_id"] = $metaDataInput13["metadata_id"];
    $metaDataInput15["name"] = "License Text";
    $metaDataInput15["internal_name"] = "license_text";
    $metaDataInput15["field_type"] = "2";
    $metaDataInput15["field_possible_values"] = "";
    $metaDataInput15["order"] =  '15';
    $metaDataInput15["is_custom"] = "0";
    $metaDataInput15["is_document"] = "2";
    $metaDataInput15["is_mandatory" ] =  '0';
    $metaDataInput15["is_active"] = "1";
    $metaDataInput15["is_deleted"] = "0";
    $metaDataInput15["updated_by"] = "";
    $metaDataInput15["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput15)); 
    $metadataArr[14] = ['id' =>$metaDataInput15["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput15["name"]];
            
    $metaDataInput16["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput16["organization_id"] = $input['organization_id']; 
    $metaDataInput16["parent_metadata_id"] = "";
    $metaDataInput16["name"] = "Notes";
    $metaDataInput16["internal_name"] = "notes";
    $metaDataInput16["field_type"] = "2";
    $metaDataInput16["field_possible_values"] = "";
    $metaDataInput16["order"] =  '16';
    $metaDataInput16["is_custom"] = "0";
    $metaDataInput16["is_document"] = "2";
    $metaDataInput16["is_mandatory" ] =  '0';
    $metaDataInput16["is_active"] = "1";
    $metaDataInput16["is_deleted"] = "0";
    $metaDataInput16["updated_by"] = "";
    $metaDataInput16["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput16)); 
    $metadataArr[15] = ['id' =>$metaDataInput16["metadata_id"], 'is_document' => "2", 'metadate_name' => $metaDataInput16["name"]];
            
    $metaDataInput17["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput17["organization_id"] = $input['organization_id']; 
    $metaDataInput17["parent_metadata_id"] = "";
    $metaDataInput17["name"] = "Full Statement";
    $metaDataInput17["internal_name"] = "full_statement";
    $metaDataInput17["field_type"] = "2";
    $metaDataInput17["field_possible_values"] = "";
    $metaDataInput17["order"] =  '17';
    $metaDataInput17["is_custom"] = "0";
    $metaDataInput17["is_document"] = "0";
    $metaDataInput17["is_mandatory" ] =  '1';
    $metaDataInput17["is_active"] = "1";
    $metaDataInput17["is_deleted"] = "0";
    $metaDataInput17["updated_by"] = "";
    $metaDataInput17["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput17)); 
    $metadataArr[16] = ['id' =>$metaDataInput17["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput17["name"]];
                
    $metaDataInput18["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput18["organization_id"] = $input['organization_id']; 
    $metaDataInput18["parent_metadata_id"] = "";
    $metaDataInput18["name"] = "Alternative Label";
    $metaDataInput18["internal_name"] = "alternative_label";
    $metaDataInput18["field_type"] = "1";
    $metaDataInput18["field_possible_values"] = "";
    $metaDataInput18["order"] =  '18';
    $metaDataInput18["is_custom"] = "0";
    $metaDataInput18["is_document"] = "0";
    $metaDataInput18["is_mandatory" ] =  '0';
    $metaDataInput18["is_active"] = "1";
    $metaDataInput18["is_deleted"] = "0";
    $metaDataInput18["updated_by"] = "";
    $metaDataInput18["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput18)); 
    $metadataArr[17] = ['id' =>$metaDataInput18["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput18["name"]];
            
    $metaDataInput19["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput19["organization_id"] = $input['organization_id']; 
    $metaDataInput19["parent_metadata_id"] = "";
    $metaDataInput19["name"] = "Human Coding Scheme";
    $metaDataInput19["internal_name"] = "human_coding_scheme";
    $metaDataInput19["field_type"] = "1";
    $metaDataInput19["field_possible_values"] = "";
    $metaDataInput19["order"] =  '19';
    $metaDataInput19["is_custom"] = "0";
    $metaDataInput19["is_document"] = "0";
    $metaDataInput19["is_mandatory" ] =  '0';
    $metaDataInput19["is_active"] = "1";
    $metaDataInput19["is_deleted"] = "0";
    $metaDataInput19["updated_by"] = "";
    $metaDataInput19["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput19)); 
    $metadataArr[18] = ['id' =>$metaDataInput19["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput19["name"]];
            
    $metaDataInput20["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput20["organization_id"] = $input['organization_id']; 
    $metaDataInput20["parent_metadata_id"] = "";
    $metaDataInput20["name"] = "List Enumeration";
    $metaDataInput20["internal_name"] = "list_enumeration";
    $metaDataInput20["field_type"] = "1";
    $metaDataInput20["field_possible_values"] = "";
    $metaDataInput20["order"] =  '20';
    $metaDataInput20["is_custom"] = "0";
    $metaDataInput20["is_document"] = "0";
    $metaDataInput20["is_mandatory" ] =  '0';
    $metaDataInput20["is_active"] = "1";
    $metaDataInput20["is_deleted"] = "0";
    $metaDataInput20["updated_by"] = "";
    $metaDataInput20["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput20)); 
    $metadataArr[19] = ['id' =>$metaDataInput20["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput20["name"]];
            
    $metaDataInput21["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput21["organization_id"] = $input['organization_id']; 
    $metaDataInput21["parent_metadata_id"] = "";
    $metaDataInput21["name"] = "Abbreviated Statement";
    $metaDataInput21["internal_name"] = "abbreviated_statement";
    $metaDataInput21["field_type"] = "1";
    $metaDataInput21["field_possible_values"] = "";
    $metaDataInput21["order"] =  '21';
    $metaDataInput21["is_custom"] = "0";
    $metaDataInput21["is_document"] = "0";
    $metaDataInput21["is_mandatory" ] =  '0';
    $metaDataInput21["is_active"] = "1";
    $metaDataInput21["is_deleted"] = "0";
    $metaDataInput21["updated_by"] = "";
    $metaDataInput21["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput21)); 
    $metadataArr[20] = ['id' =>$metaDataInput21["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput21["name"]];
            
    $metaDataInput22["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput22["organization_id"] = $input['organization_id']; 
    $metaDataInput22["parent_metadata_id"] = "";
    $metaDataInput22["name"] = "Concept Title";
    $metaDataInput22["internal_name"] = "concept_title";
    $metaDataInput22["field_type"] = "1";
    $metaDataInput22["field_possible_values"] = "";
    $metaDataInput22["order"] =  '22';
    $metaDataInput22["is_custom"] = "0";
    $metaDataInput22["is_document"] = "0";
    $metaDataInput22["is_mandatory" ] =  '0';
    $metaDataInput22["is_active"] = "1";
    $metaDataInput22["is_deleted"] = "0";
    $metaDataInput22["updated_by"] = "";
    $metaDataInput22["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput22)); 
    $metadataArr[21] = ['id' =>$metaDataInput22["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput22["name"]];
                
    $metaDataInput23["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput23["organization_id"] = $input['organization_id']; 
    $metaDataInput23["parent_metadata_id"] = $metaDataInput22["metadata_id"];
    $metaDataInput23["name"] = "Concept Keywords";
    $metaDataInput23["internal_name"] = "concept_keywords";
    $metaDataInput23["field_type"] = "1";
    $metaDataInput23["field_possible_values"] = "";
    $metaDataInput23["order"] =  '23';
    $metaDataInput23["is_custom"] = "0";
    $metaDataInput23["is_document"] = "0";
    $metaDataInput23["is_mandatory" ] =  '0';
    $metaDataInput23["is_active"] = "1";
    $metaDataInput23["is_deleted"] = "0";
    $metaDataInput23["updated_by"] = "";
    $metaDataInput23["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput23)); 
    $metadataArr[22] = ['id' =>$metaDataInput23["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput23["name"]];
                
    $metaDataInput24["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput24["organization_id"] = $input['organization_id']; 
    $metaDataInput24["parent_metadata_id"] = $metaDataInput22["metadata_id"];
    $metaDataInput24["name"] = "Concept Hierarchy Code";
    $metaDataInput24["internal_name"] = "concept_hierarchy_code";
    $metaDataInput24["field_type"] = "1";
    $metaDataInput24["field_possible_values"] = "";
    $metaDataInput24["order"] =  '24';
    $metaDataInput24["is_custom"] = "0";
    $metaDataInput24["is_document"] = "0";
    $metaDataInput24["is_mandatory" ] =  '0';
    $metaDataInput24["is_active"] = "1";
    $metaDataInput24["is_deleted"] = "0";
    $metaDataInput24["updated_by"] = "";
    $metaDataInput24["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput24)); 
    $metadataArr[23] = ['id' =>$metaDataInput24["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput24["name"]];
                
    $metaDataInput25["metadata_id"] = $this->createUniversalUniqueIdentifier();
    $metaDataInput25["organization_id"] = $input['organization_id']; 
    $metaDataInput25["parent_metadata_id"] = $metaDataInput22["metadata_id"];
    $metaDataInput25["name"] = "Concept Description";
    $metaDataInput25["internal_name"] = "concept_description";
    $metaDataInput25["field_type"] = "2";
    $metaDataInput25["field_possible_values"] = "";
    $metaDataInput25["order"] =  '25';
    $metaDataInput25["is_custom"] = "0";
    $metaDataInput25["is_document"] = "0";
    $metaDataInput25["is_mandatory" ] =  '0';
    $metaDataInput25["is_active"] = "1";
    $metaDataInput25["is_deleted"] = "0";
    $metaDataInput25["updated_by"] = "";
    $metaDataInput25["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput25)); 
    $metadataArr[24] = ['id' =>$metaDataInput25["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput25["name"]];
                
    $metaDataInput26["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput26["organization_id"] = $input['organization_id']; 
    $metaDataInput26["parent_metadata_id"] = "";
    $metaDataInput26["name"] = "Education Level";
    $metaDataInput26["internal_name"] = "education_level";
    $metaDataInput26["field_type"] = "2";
    $metaDataInput26["field_possible_values"] = "";
    $metaDataInput26["order"] =  '26';
    $metaDataInput26["is_custom"] = "0";
    $metaDataInput26["is_document"] = "0";
    $metaDataInput26["is_mandatory" ] =  '0';
    $metaDataInput26["is_active"] = "1";
    $metaDataInput26["is_deleted"] = "0";
    $metaDataInput26["updated_by"] = "";
    $metaDataInput26["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput26)); 
    $metadataArr[25] = ['id' =>$metaDataInput26["metadata_id"], 'is_document' => "0", 'metadate_name' => $metaDataInput26["name"]];   

    $metaDataInput27["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput27["organization_id"] = $input['organization_id']; 
    $metaDataInput27["parent_metadata_id"] = "";
    $metaDataInput27["name"] = "Pacing Guide Item Type";
    $metaDataInput27["internal_name"] = "pacing_guide_item_type";
    $metaDataInput27["field_type"] = "3";
    $metaDataInput27["field_possible_values"] = "Standard,Container";
    $metaDataInput27["order"] =  '27';
    $metaDataInput27["is_custom"] = "1";
    $metaDataInput27["is_document"] = "3";
    $metaDataInput27["is_mandatory" ] =  '0';
    $metaDataInput27["is_active"] = "1";
    $metaDataInput27["is_deleted"] = "0";
    $metaDataInput27["updated_by"] = "";
    $metaDataInput27["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput27)); 
    $metadataArr[26] = ['id' =>$metaDataInput27["metadata_id"], 'is_document' => "3", 'metadate_name' => $metaDataInput27["name"]];

    //to create new metadata field for each orgnizations
    $field_values = array("Generic", "Pacing Guide");
       $fieldvalues = array();
       $num = 1; 
        foreach($field_values as $key=>$value){
            
            $Valuesarray['key'] = $num;
            $Valuesarray['value'] = $value;
            $fieldvalues[] = $Valuesarray;
            $num++;
        }  
    $listValues = json_encode($fieldvalues);

    $metaDataInput28["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput28["organization_id"] = $input['organization_id']; 
    $metaDataInput28["parent_metadata_id"] = "";
    $metaDataInput28["name"] = "Taxonomy Type";
    $metaDataInput28["internal_name"] = "Taxonomy Type";
    $metaDataInput28["field_type"] = "7";
    $metaDataInput28["field_possible_values"] = $listValues;
    $metaDataInput28["order"] =  '28';
    $metaDataInput28["is_custom"] = "1";
    $metaDataInput28["is_document"] = "3";
    $metaDataInput28["is_mandatory" ] =  '0';
    $metaDataInput28["is_active"] = "1";
    $metaDataInput28["is_deleted"] = "0";
    $metaDataInput28["updated_by"] = "";
    $metaDataInput28["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput28)); 
    $metadataArr[27] = ['id' =>$metaDataInput28["metadata_id"], 'is_document' => "3", 'metadate_name' => $metaDataInput28["name"]];

    //to add Source_type metadata
    $field_values = array("CSV","JSON","API","Builder");
       $fieldvalues = array();
       $num = 1; 
        foreach($field_values as $key=>$value){             // to set key to field values
            
            $Valuesarray['key'] = $num;
            $Valuesarray['value'] = $value;
            $fieldvalues[] = $Valuesarray;
            $num++;
        }  
    $listValues = json_encode($fieldvalues);

    $metaDataInput29["metadata_id"] = $this->createUniversalUniqueIdentifier(); 
    $metaDataInput29["organization_id"] = $input['organization_id']; 
    $metaDataInput29["parent_metadata_id"] = "";
    $metaDataInput29["name"] = "Source Type";
    $metaDataInput29["internal_name"] = "Source_Type";
    $metaDataInput29["field_type"] = "7";           
    $metaDataInput29["field_possible_values"] = $listValues;
    $metaDataInput29["order"] =  '29';
    $metaDataInput29["is_custom"] = "1";
    $metaDataInput29["is_document"] = "3";          
    $metaDataInput29["is_mandatory" ] =  '0';
    $metaDataInput29["is_active"] = "0";        //to avoid adding source type to metadeta set
    $metaDataInput29["is_deleted"] = "0";
    $metaDataInput29["updated_by"] = "";
    $metaDataInput29["updated_at"] = now()->toDateTimeString();
    $this->run(new CreateDefaultMetaDataJob($metaDataInput29)); 
    $metadataArr[28] = ['id' =>$metaDataInput29["metadata_id"], 'is_document' => "3", 'metadate_name' => $metaDataInput29["name"]];

    $documentCnt = 0;
    $itemCnt = 0;
    $itemDocuemntCnt = 0;
    $ismandatory = 0;
    foreach ($nodeTypeData as $key4 => $nodeType)
    {
        foreach($metadataArr as $key5 => $metadata)
        {
        
            if($nodeType['node_name'] == "Document")
            {
                $order_no = array_search($metadata["metadate_name"],$documentOrderArr);
            }
            else
            {
                $order_no = array_search($metadata["metadate_name"],$nondocumentOrderArr);
            }
            if($nodeType['node_name'] == "Document" && $metadata["metadate_name"] == "Title")
            {
                $ismandatory = 1;
            }
            else if($nodeType['node_name'] == "Document" && $metadata["metadate_name"] == "Creator")
            {
                $ismandatory = 1;
            }
            else if($nodeType['node_name'] != "Document" && $metadata["metadate_name"] == "Full Statement")
            {
                $ismandatory = 1;
            }
            else
            {
                $ismandatory = 0;
            }
            if($metadata['is_document'] == "1" && $nodeType['is_document'] == "1")
            {
                
                $this->run(new SaveNodeTypeMetadataTenantJob($nodeType['id'],$metadata['id'], $order_no,$ismandatory));
            }
            else if($metadata['is_document'] == "0" && $nodeType['is_document'] == "0")
            {  
                $this->run(new SaveNodeTypeMetadataTenantJob($nodeType['id'],$metadata['id'], $order_no,$ismandatory));
            }
            else if($metadata['is_document'] == "2")
            { 
                $this->run(new SaveNodeTypeMetadataTenantJob($nodeType['id'],$metadata['id'], $order_no,$ismandatory));
            } 
        }
    }

                $input['username'] = $requestVar['admin_account'];
                $input['first_name'] = $requestVar['admin_name'];
                $input['last_name'] = ""; 
                $input['password']= bcrypt($requestVar['password']);
                $input['is_active']= 1;  
                $input["access_token_updated_at"] = now()->toDateTimeString();
                $inputData = ['name' =>'Admin','description' => "Admin", 'organization_id' => $input['organization_id'],"role_code" => config("selfRegistration")["ROLE"]["SYSTEM_DEFAULT_ADMIN_ROLE"]]; //,'permission_set' => $requestData['permission_set']
            
                $validateRoleData = $this->run(new ValidateCreateRoleInputJob($inputData));
                
                $roleId = $this->createUniversalUniqueIdentifier();
                if($validateRoleData===true) {
                
                    $inputData['role_id'] = $roleId;
        
                    $roleCreated = $this->run(new CreateRoleJob($inputData));
                    $input['role_id'] = $roleId;
                }
                else
                {
                    $input['role_id'] = $roleId;
                }
                //dd("1111");
        
            $permissionList = $this->run(new ListAllPermissionJob());
            foreach($permissionList as $permission)
            {
                $permissionArray[] = $permission['permission_id'];
                
            } 
            
            
            $identifier = ['role_id' => $roleId];
            $permissionsSaved = $this->run(new RolePermissionSetJob($identifier, $permissionArray));
            
            //Create Role For Public Review-Global Role
            $inputDataPRGL = ['name' =>'Platform review- Global role','description' => "Platform review- Global role", 'organization_id' => $input['organization_id'],"role_code" => config("selfRegistration")["ROLE"]["PROJECT_PUBLIC_REVIEW_ROLE"]]; //,'permission_set' => $requestData['permission_set']
            
            $validateRoleDataPRGL = $this->run(new ValidateCreateRoleInputJob($inputDataPRGL));
            
            $roleIdPRGL = $this->createUniversalUniqueIdentifier();
            if($validateRoleDataPRGL===true) {
                
                $inputDataPRGL['role_id'] = $roleIdPRGL;
    
                $roleCreatedPRGL = $this->run(new CreateRoleJob($inputDataPRGL));
                $inputPRGL['role_id'] = $roleIdPRGL;
            }
            else
            {
                $inputPRGL['role_id'] = $roleIdPRGL;
            }            
            
            $permissionListPRGL = $this->run(new ListAllPermissionPRGLJob());
            
            foreach($permissionListPRGL as $permission)
            {
                $permissionArrayPRGL[] = $permission['permission_id'];
                
            } 
            
            
            $identifierPRGL = ['role_id' => $roleIdPRGL];
            $permissionsSavedPRGL = $this->run(new RolePermissionSetJob($identifierPRGL, $permissionArrayPRGL));
            
            //Create Role For Platform review- Taxonomy role
            $inputDataPRTL = ['name' =>'Platform review- Taxonomy role','description' => "Platform review- Taxonomy role", 'organization_id' => $input['organization_id'],"role_code" => config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"]]; //,'permission_set' => $requestData['permission_set']
            
            $validateRoleDataPRTL = $this->run(new ValidateCreateRoleInputJob($inputDataPRTL));
            
            $roleIdPRTL = $this->createUniversalUniqueIdentifier();
            if($validateRoleDataPRTL===true) {
                
                $inputDataPRTL['role_id'] = $roleIdPRTL;
    
                $roleCreatedPRTL = $this->run(new CreateRoleJob($inputDataPRTL));
                $inputPRTL['role_id'] = $roleIdPRTL;
            }
            else
            {
                $inputPRTL['role_id'] = $roleIdPRTL;
            }            
            
            $permissionListPRTL = $this->run(new ListAllPermissionPRTLJob());
            foreach($permissionListPRTL as $permissionPRTL)
            {
                $permissionArrayPRTL[] = $permissionPRTL['permission_id'];
                
            } 
            
            
            $identifierPRTL = ['role_id' => $roleIdPRTL];
            $permissionsSavedPRTL = $this->run(new RolePermissionSetJob($identifierPRTL, $permissionArrayPRTL));
            // Create Workflow for Public Review 
            $workflow_code = config("selfRegistration")["WORKFLOW"]["DEFAULT_WORKFLOW_PROJECT_PUBLIC_REVIEW"];
            $workflowInputPR['workflow_id']= $this->createUniversalUniqueIdentifier();
            $workflowInputPR['organization_id']= $input['organization_id'];
            $workflowInputPR['name']= 'Public Review';
            $workflowInputPR['updated_at']= now()->toDateTimeString();
            $workflowInputPR['created_at']= now()->toDateTimeString();
            $workflowInputPR['is_default']= 0;
            $workflowInputPR['workflow_code']= $workflow_code;

            $worflowCreatedPR = $this->run(new CreateWorkflowJob($workflowInputPR));

            // Create Stage for Project Review
            $stageInputPR['stage_id'] = $this->createUniversalUniqueIdentifier();
            $stageInputPR['organization_id'] = $input['organization_id'];
            $stageInputPR['name'] = 'Stage 1';
            $stageInputPR['order'] = 1;
            $stageInputPR['is_deleted'] = 0;
            $stageCreatedPR = $this->run(new CreateStageJob($stageInputPR));

            $workflowstageInputPR['workflow_stage_id'] = $workflowInputPR['workflow_id'].'||'.$stageInputPR['stage_id'];
            $workflowstageInputPR['workflow_id'] = $workflowInputPR['workflow_id'];
            $workflowstageInputPR['stage_id'] = $stageInputPR['stage_id'];
            $workflowstageInputPR['order'] = '1';
            $workflowstageInputPR['stage_name'] = 'Stage 1';
            $workflowstageInputPR['stage_description'] = 'Stage 1';
            $worflowStageCreatedPR = $this->run(new CreateWorkflowStageJob($workflowstageInputPR));

            $workflowStageRoleData['workflow_stage_role_id'] = $workflowstageInputPR['workflow_stage_id']."||".$identifierPRTL["role_id"];
            $workflowStageRoleData['workflow_stage_id'] = $workflowstageInputPR['workflow_stage_id'];
            $workflowStageRoleData['role_id'] = $inputDataPRTL['role_id'];
            $worflowStageRoleCreated = $this->run(new CreateWorkflowStageRoleJob($workflowStageRoleData)); 


                        $user = $this->run(new CreateOrganizationUserJob($input));
                        $is_user_default = 1;
                        $default_for_all_user = 0;
                        $action = "Set";
                        $this->updatedefaultconfigsettings($input['organization_id'],$is_user_default,$default_for_all_user,$action);

                        $successType = 'created';
                        $message = 'Admin user created successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $user, $message, $_status));
                
                }
                else
                {
                    
                    $successType = 'found';
                        $data = ['result' => 'exist'];
                        // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                        //$message = 'Email Id already exists in system. Please try with different Email ID.';
            
                        if($validationStatus['email'][0] == "The email has already been taken.")
                        {
                            $message = 'Email ID already exists in system. Please try with different Email ID.';
                        }
                        else
                        {
                            $message = 'Email ID is Invalid';
                        }
                        
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                    
                
                }
            }
            else
            {
                $successType = 'found';
                $data = ['result' => 'exist'];
                $message = 'The tenant shortcode "'.$requestVar['shortcode'].'" already exists. Kindly enter new shortcode.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));

            }    

            }
            else
            {

            $successType = 'found';
                    $data = ['result' => 'exist'];
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Organization already exists in system. Please try with different Organization Name.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                

            } 
        }
        else
        {
            $successType = 'found';
                    $data = ['result' => 'exist'];
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'You do not have previledge to create a tenent..';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
        }
        
    }
    catch (\Exception $ex) {
        $errorType = 'internal_error';
        $message = $ex->getMessage();
        $_status = 'custom_status_here';
        Log::error($ex);
        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
    }

    }
}
