<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\LinkedServer\Jobs\ValidateLinkedServerIdJob;
use App\Domains\LinkedServer\Jobs\DeleteLinkedServerJob;
use Log;

class DeleteLinkedServerFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $linkedServerIdentifier =   $request->route('linked_server_id');
            $requestData        =   $request->all();
            $userId = $requestData['auth_user']['user_id'];
            $identifier     =   ['linked_server_id'  =>  $linkedServerIdentifier,'updated_by' => $userId];
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            
            $validateLinkedServer    =   $this->run(new ValidateLinkedServerIdJob($identifier));
            if($validateLinkedServer === true) {
                   
                        $deleteLinkedServer = $this->run(new DeleteLinkedServerJob($identifier));
                        $response = ''; 
                        $successType = 'found';
                        $message = 'Linked Server deleted successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

                   
            } else {
                $errors = $validateLinkedServer;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
