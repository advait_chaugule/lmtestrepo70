<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\ValidateUserByEmailJob;
use App\Domains\User\Jobs\CreateResetRequestJob;
use App\Domains\User\Jobs\SendResetPasswordEmailNotificationJob;

use App\Domains\Organization\Jobs\GetOrganizationDetailByIdJob;

use App\Domains\User\Jobs\GetUserDetailByEmailIdJob;
use App\Domains\Organization\Jobs\GetActiveOrganizationsByUserIdJob;
use Log;

class ForgotPasswordFeature extends Feature
{
    use UuidHelperTrait;

    public function handle(Request $request)
    {
        $requestData = $request->all();        
        $input = ['email' => $request['email']];
        $requestUrl    = url('/');
        $serverName    = str_replace('server', '', $requestUrl);
        $userDetail =   $this->run(new GetUserDetailByEmailIdJob($requestData['email']));
        $requestData['auth_user']['organization_id'] = $userDetail;
        try{
            $validationStatus = $this->run(new ValidateUserByEmailJob($input));
            // Validation response is array with messages and not false
            if(empty($userDetail) || is_array($validationStatus)) {
                $errorType = 'validation_error';
                $message = 'Email id does not exists or inactive.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message));
            }
            else{ 
                $organizationDetail =   $this->run(new GetOrganizationDetailByIdJob($requestData['auth_user']['organization_id']));       
        
                // Check if current organization if inactive
                if(!empty($organizationDetail) && $organizationDetail->is_active=='0' && !empty($userDetail)){
                    // Get all associated active organizations of this user through email
                    $currOrganization = $this->run(new GetActiveOrganizationsByUserIdJob('',$requestData['email']));
                    // If no organizations found, show error. Else continue resetting password
                    if(empty($currOrganization)){
                        $errorType = 'custom_found';
                        $message = ['This Tenant is deactivated and please contact admin'];
                        return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
                    }                
                }         
                $input["forgot_pasword_request_token"] = $this->createUniversalUniqueIdentifier();
                $input["forgot_pasword_request_token_expiry"] = date("Y-m-d H:i:s", strtotime("+30 minutes"));
                
                $resetPasswordDetails = $this->run(new CreateResetRequestJob($input));
                
                $email['email']                         =   $input['email'];
                $email['forgot_reset_request_token']    =   $resetPasswordDetails['reset_token'];
                $email['user_name']                     =   $resetPasswordDetails['user_name'];
                $email['org_code']                      =   $organizationDetail->org_code;
                $email['domain_name']                   =   $serverName;
                $this->run(new SendResetPasswordEmailNotificationJob($email));
                $successType = 'sent';
                $message = 'Reset password email sent successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));         
            }            
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
