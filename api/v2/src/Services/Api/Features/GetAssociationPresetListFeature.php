<?php
namespace App\Services\Api\Features;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use Illuminate\Http\Request;
use Log;
use App\Domains\Association\Jobs\GetAssociationPresetList;
use Lucid\Foundation\Feature;

class GetAssociationPresetListFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData                = $request->all();
            $userId                     = $requestData['auth_user']['user_id'];
            $organizationId             = $requestData['auth_user']['organization_id'];

            $response = $this->run(new GetAssociationPresetList($userId,$organizationId));
            $successType = 'custom_found';
            $_status = 'custom_status_here';
            $message = (!empty($response)) ? ['Data found.'] : ['Data Not found.'];
            $is_success = (!empty($response)) ? true : false;
            return $this->run(new RespondWithJsonJob($successType, $response, $message,$_status,[],$is_success));

          }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}