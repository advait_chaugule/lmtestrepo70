<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Services\Api\Traits\UuidHelperTrait;


use App\Domains\LinkedServer\Jobs\ValidateCreateLinkedServerInputJob;
use App\Domains\LinkedServer\Jobs\CheckedConnectionForLinkedServerJob;
use App\Domains\LinkedServer\Jobs\CreateLinkedServerJob;
use Log;

class CreateLinkedCaseServerFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;

    public function handle(Request $request)
    {
        try{

            $requestData = $request->all();
            $linkeServerArr["linked_server_id"] = $this->createUniversalUniqueIdentifier(); 
            $linkeServerArr["linked_type"] = isset($requestData["linked_type"]) ? $requestData["linked_type"] : "1";  
            $linkeServerArr["linked_name"] = isset($requestData["linked_name"]) ? $requestData["linked_name"] : ""; 
            $linkeServerArr["description"] = isset($requestData["description"]) ? $requestData["description"] : ""; 
            $linkeServerArr["server_url"] = isset($requestData["server_url"]) ? $requestData["server_url"] : ""; 
            $linkeServerArr["organization_id"] = $requestData["auth_user"]["organization_id"];
            $linkeServerArr["created_at"] = now()->toDateTimeString();
            $linkeServerArr["updated_at"] = now()->toDateTimeString();
            $linkeServerArr["created_by"] = $requestData["auth_user"]["user_id"];
            $linkeServerArr["updated_by"] = $requestData["auth_user"]["user_id"];
            $linkeServerArr["is_deleted"] = 0;
            
            $linkeServerArr["is_save"] = isset($requestData["is_save"]) ? $requestData["is_save"] : "0"; 
            $linkeServerArr["is_create"] = 1;

           
                if($linkeServerArr["is_save"] == 0)
                {
                    $checkedConnectionForLinkedServerStatus = $this->run(new CheckedConnectionForLinkedServerJob($linkeServerArr));
                    if($checkedConnectionForLinkedServerStatus === true)
                    {
                        $data = ['result' => 'success'];
                        $response = $data; 
                        $successType = 'found';
                        $message = 'Connection successful';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else
                    {
                        $data = ['result' => 'failed'];
                        $response = $data; 
                        $successType = 'found';
                        $message = 'Connection failed';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
           
                }
                else if($linkeServerArr["is_save"] == 1)
                {
                    $validateCreateLinkedServerInput = $this->run(new ValidateCreateLinkedServerInputJob($linkeServerArr));
                    if($validateCreateLinkedServerInput === true)
                    {
                        $linkeServerArr["connection_status"] = 1;
                        $createLinkedServerInput = $this->run(new CreateLinkedServerJob($linkeServerArr));
                        $successType = 'created';
                        $message = 'Linked Server created successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $createLinkedServerInput, $message, $_status));
                     }
                else
                {
                    $errorType = 'validation_error';
                    $message = $validateCreateLinkedServerInput;
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
          
                }

                }

           

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
