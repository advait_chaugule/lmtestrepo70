<?php
namespace App\Services\Api\Features;


use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\GetUserByIdJob;

use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;

use App\Domains\Thread\Jobs\GetThreadJob;
use App\Domains\Thread\Jobs\ValidateThreadCommentJob;
use App\Domains\Thread\Jobs\UpdateThreadJob;
use App\Domains\Thread\Jobs\UpdateThreadCommentJob;
use App\Domains\Thread\Jobs\ListCommentbyThreadIdJob;
use App\Domains\Thread\Jobs\GetThreadCommentJob;

//Permission Job
use App\Domains\Role\Jobs\ListRolePermissionJob;

//Email Notification Jobs
use App\Domains\Comment\Jobs\SendCommentNotificationToOwnerAndAssigneeJob;

//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use Log;

class UpdateCommentFeature extends Feature
{
    use DateHelpersTrait;
    
    public function handle(Request $request)
    {
        try{
            $permissionSet          =   [];

            $requestData                =   $request->all();
            $organizationIdentifier     =   $requestData['auth_user']['organization_id'];
            $threadCommentIdentifier    =   $request->route('thread_comment_id');
            $threadSourceIdentifier     =   $requestData['thread_source_id'];
            $threadSourceType           =   $requestData['thread_source_type'];
            $requestUrl                 = url('/');
            $serverName                 = str_replace('server', '', $requestUrl);
            //$roleId         =   $requestData['auth_user']['role_id'];
            $threadSourceDetails    =   '';
            if($threadSourceType    ==  '1') {
                //Fetch Thread Source Item Details
                $threadSourceDetails = $this->run(new GetCFItemDetailsJob($threadSourceIdentifier, $organizationIdentifier));
            }
            else{
                //Fetch Thread Source Document Details
                $threadSourceDetails = $this->run(new GetDocumentByIdJob(['id' => $threadSourceIdentifier]));                            
            }  

           

            //Get Parent Comment
            $parentComment  =   $this->run(new GetThreadJob($requestData['thread_id'])); 

            $validateThreadComment = $this->run(new ValidateThreadCommentJob(['thread_comment_id' => $threadCommentIdentifier]));

            $inputData  =   ['thread_id' => $requestData['thread_id']];
            
            if($validateThreadComment === true) {
                if(isset($requestData['status']) && !empty($requestData['status'])){
                    if($parentComment['thread_comment']['created_by'] == $requestData['auth_user']['user_id']){
                        //if (in_array("2d776856-c7da-417c-8f57-2c4e097692b7", $permissionSet)){
                            if($parentComment['status'] == $requestData['status']){
                                $errorType = 'validation_error';
                                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                                $message = 'The changed status is same as the previous one';
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                            }
                            else{
                                // to get status before update start
                                $getStatusBeforeUpdate  =   $this->run(new GetThreadJob($requestData['thread_id']));
                                // to get status before update start
                                $inputData['status']        =  $requestData['status'];
                                $inputData['updated_at']    =  $requestData['updated_at'];
                                //Update the assignee to main comment
                                $update       = $this->run(new UpdateThreadJob($inputData));
                                $updatedParentComment  =   $this->run(new GetThreadJob($requestData['thread_id'])); 
                                //In App Notification
                                $data             = $update->toArray();
                                $thread_source_id = $data['thread_source_id'];

                                $commentStatus    = $data['status'];
                                if($commentStatus==1) {
                                    $commentStatus = 'Reopen';
                                }else if($commentStatus==2){
                                    $commentStatus = 'need clarification';
                                }else if($commentStatus==3){
                                    $commentStatus = 'fixed';
                                }else if($commentStatus==4){
                                    $commentStatus = 'closed';
                                }
                                $inputData['updated_by'] =  $requestData['auth_user']['user_id'];
                                $eventType               = config('event_activity')["Notifications"]["COMMENT_STATUS_CHANGE_NOTIFICATION"];
                                $eventData = [
                                    'status'             =>$commentStatus,
                                    'owner_id'           =>$updatedParentComment['thread_comment']['created_by'],
                                    'assignee_id'        =>$updatedParentComment['assign_to'],
                                    'thread_source_id'   =>$thread_source_id,
                                    'organization_id'    =>$data['organization_id'],
                                    'updated_by'         =>$inputData['updated_by'] ,
                                    'thread_id'          =>$requestData['thread_id'],
                                    'event_type'         =>$eventType,
                                    'requestUserDetails' => $request->input("auth_user"),
                                    "beforeEventRawData" => [],
                                    'afterEventRawData'  => ''
                                ];
                                event(new NotificationsEvent($eventData));
                                
                                //Fetch comment list
                                $commentList    =   $this->run(new ListCommentbyThreadIdJob($requestData['thread_id'], $requestData['auth_user']['organization_id']));
                                
                                /**
                                 * Email Notification
                                 */
                                $organizationData = $this->run(new GetOrgNameByOrgIdJob($data['organization_id']));
                                $organizationName = $organizationData['organization_name'];
                                $organizationCode = $organizationData['organization_code'];
                                $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$updatedParentComment['thread_comment']['created_by'],'organization_id'=>$data['organization_id']]));
                                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                                if ($emailSettingStatus==1) {
                                //Send mail for status change
                                $mailSent    =   $this->sendMailForStatusChange($updatedParentComment, $inputData, $threadSourceDetails, $commentList,$getStatusBeforeUpdate,$organizationName,$organizationCode,$serverName);
                                $successType = 'updated';
                                $message     = 'Comment status updated successfully.';
                                $_status     = 'custom_status_here';
                                return $this->run(new RespondWithJsonJob($successType, $mailSent, $message, $_status));
                               }
                            }
                       
                    }
                    else{
                        //if (in_array("1ac2d6eb-71fb-48dd-b054-34832e9c5a9e", $permissionSet)){
                            if($parentComment['status'] == $requestData['status']){
                                $errorType = 'validation_error';
                                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                                $message = 'The changed status is same as the previous one';
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                            }
                            else{
                                // to get status before update start
                                $getStatusBeforeUpdate  =   $this->run(new GetThreadJob($requestData['thread_id']));
                                // to get status before update start
                                $inputData['status']        =  $requestData['status'];
                                $inputData['updated_at']    =  $requestData['updated_at'];
                                //Update the assignee to main comment
                                $update       = $this->run(new UpdateThreadJob($inputData));
                                //Get Parent Comment
                                $updatedParentComment  =   $this->run(new GetThreadJob($requestData['thread_id'])); 
                                //In App Notification
                                $data             = $update->toArray();
                                $thread_source_id = $data['thread_source_id'];

                                $commentStatus    = $data['status'];
                                if($commentStatus==1) {
                                    $commentStatus = 'Reopen';
                                }else if($commentStatus==2){
                                    $commentStatus = 'need clarification';
                                }else if($commentStatus==3){
                                    $commentStatus = 'fixed';
                                }else if($commentStatus==4){
                                    $commentStatus = 'closed';
                                }
                                $inputData['updated_by'] =  $requestData['auth_user']['user_id'];
                                $eventType               = config('event_activity')["Notifications"]["COMMENT_STATUS_CHANGE_NOTIFICATION"];
                                $eventData = [
                                    'status'             =>$commentStatus,
                                    'owner_id'           =>$updatedParentComment['thread_comment']['created_by'],
                                    'assignee_id'        =>$updatedParentComment['assign_to'],
                                    'thread_source_id'   =>$thread_source_id,
                                    'organization_id'    =>$data['organization_id'],
                                    'updated_by'         =>$inputData['updated_by'] ,
                                    'thread_id'          =>$requestData['thread_id'],
                                    'event_type'         =>$eventType,
                                    'requestUserDetails' => $request->input("auth_user"),
                                    "beforeEventRawData" => [],
                                    'afterEventRawData'  => ''
                                ];
                                event(new NotificationsEvent($eventData));
                                $inputData['updated_by']    =  $requestData['auth_user']['user_id'];
                                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($data['organization_id']));
                                $organizationName = $organizationData['organization_name'];
                                $organizationCode = $organizationData['organization_code'];
                                  //Fetch comment list
                                $commentList    =   $this->run(new ListCommentbyThreadIdJob($requestData['thread_id'], $requestData['auth_user']['organization_id']));
                                $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => !empty($updatedParentComment['assign_to']) ? $updatedParentComment['assign_to'] : $parentComment['thread_comment']['created_by'] , 'organization_id' => $data['organization_id']]));
                                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                                if ($emailSettingStatus==1) {
                                /**
                                 * Email Notification
                                 */
                                //Send mail for status change
                                $this->sendMailForStatusChange($updatedParentComment, $inputData, $threadSourceDetails, $commentList,$getStatusBeforeUpdate,$organizationName,$organizationCode,$serverName);
                              }
                            }
                        
                    }             
                }
                else{
                    
                        
                            $inputData['comment']      =   $requestData['comment'];
                            $inputData['updated_at']   =   $requestData['updated_at'];
                            //Update the comment text 
                            $this->run(new UpdateThreadCommentJob($threadCommentIdentifier, $inputData));

                            $inputData['updated_by']    =  $requestData['auth_user']['user_id'];

                            //Update the main comment with updated at
                            $updateThreadData               =   ['thread_id'   => $inputData['thread_id']];
                            $updateThreadData['updated_at'] =   $requestData['updated_at'];
                            $this->run(new UpdateThreadJob($updateThreadData));
                            
                            //Get Parent Comment
                            $updatedParentComment  =   $this->run(new GetThreadJob($requestData['thread_id'])); 
                            //Fetch comment list
                            $commentList    =   $this->run(new ListCommentbyThreadIdJob($requestData['thread_id'], $requestData['auth_user']['organization_id']));
                                             
                   
                }

                //event for project and document update(updated_at) start
                event(new UpdateTaxonomyTimeEvent($threadSourceIdentifier));
                //event for project and document update(updated_at) end
                
                $threadCommentDetail    =   $this->run(new GetThreadCommentJob($threadCommentIdentifier));
                
                $resultData     =   $threadCommentDetail;
                $successType    =   'created';
                $message        =   'Comment updated successfully.';
                $_status        =   'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status));

            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please provide valid Item Thread Comment identifier.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            
        }
        catch(\Exception $ex){
            dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    /* private function fetchRolePermissionSet($roleId) {
        
        $rolePermission =   $this->run(new ListRolePermissionJob($roleId));

        foreach($rolePermission['comment_permissions'] as $permission){
            if($permission['permission_id'] == 'f9b18a96-acc5-443b-a5c8-549951fa1606'){
                $permissionSet[] = 'f9b18a96-acc5-443b-a5c8-549951fa1606';
            }
            else if($permission['permission_id'] == '6a82bc56-c4e1-46e9-a504-c1bbf4dd3fc0'){
                $permissionSet[] = '6a82bc56-c4e1-46e9-a504-c1bbf4dd3fc0';
            }
            else if($permission['permission_id'] == '132dfb54-54c3-448e-a85d-3393568a554a'){
                $permissionSet[] = '132dfb54-54c3-448e-a85d-3393568a554a';
            }
            else if($permission['permission_id'] == '144719b5-cad1-47c3-b34f-e02e8e9750f5'){
                $permissionSet[] = '144719b5-cad1-47c3-b34f-e02e8e9750f5';
            }
            else if($permission['permission_id'] == 'a5fa39a3-e83c-4c28-a538-87020f669197'){
                $permissionSet[] = 'a5fa39a3-e83c-4c28-a538-87020f669197';
            }
            else if($permission['permission_id'] == '1ac2d6eb-71fb-48dd-b054-34832e9c5a9e'){
                $permissionSet[] = '1ac2d6eb-71fb-48dd-b054-34832e9c5a9e';
            }
            else if($permission['permission_id'] == '2d776856-c7da-417c-8f57-2c4e097692b7'){
                $permissionSet[] = '2d776856-c7da-417c-8f57-2c4e097692b7';
            }  
        }

        return $permissionSet;
    } */

    private function sendMailForStatusChange($parentComment, $inputData, $threadSourceDetails, $commentList,$getStatusBeforeUpdate,$organizationName,$organizationCode,$serverName){
        $statusValues               =   config("default_enum_setting.comment_status");

        //Get the Comment Owner Detail
        $ownerIdentifier            =   $parentComment['thread_comment']['created_by'];
        $ownerDetail                =   $this->run(new GetUserByIdJob(['user_id' => $ownerIdentifier]));

        //Get the Status Changer Detail
        $updaterIdentifier          =   $inputData['updated_by'];
        $updaterDetail              =   $this->run(new GetUserByIdJob(['user_id' => $updaterIdentifier]));

        //Get the Assignnee User Detail
        if($parentComment['assign_to'] !== ''){
            
            $assigneeIdentifier         =   $parentComment['assign_to'];
            $assigneeDetail             =   $this->run(new GetUserByIdJob(['user_id' => $assigneeIdentifier]));

            $emailInput['receiver_email']   =   [$ownerDetail['email'],$assigneeDetail['email']];
            $emailInput['parent_comment']   =   $ownerDetail['first_name'].' '.$ownerDetail['last_name']."||".$parentComment['thread_comment']['comment']."||".$assigneeDetail['email'];
            $emailInput['organization_name'] = $organizationName;
            $emailInput['organization_code'] = $organizationCode;
        }
        else
        {
            $emailInput['receiver_email']       =   [$ownerDetail['email']];
            $emailInput['parent_comment']       =   $ownerDetail['first_name'].' '.$ownerDetail['last_name']."||".$parentComment['thread_comment']['comment'];
            $emailInput['organization_name'] = $organizationName;
            $emailInput['organization_code'] = $organizationCode;
        }

       
        $emailInput['project_id']   =   $threadSourceDetails['projects_associated'][0]['project_id'];
        $emailInput['project_name'] =   $threadSourceDetails['projects_associated'][0]['project_name']; 

        if(isset($inputData['status'])){
            $emailInput['new_status']   =   $statusValues[$parentComment['status']]; 
        }
           
        $emailInput['changed_by']       =   !empty($updaterDetail->first_name)? $updaterDetail->first_name : "User";  

        
        if(sizeOf($commentList)>0){
            $emailInput['commentList']      =   $commentList[0]['comments']; 
        }
        else{
            $emailInput['commentList']      =   [];
        }

        if($getStatusBeforeUpdate['status']!==4)
        {
            $emailInput['subject']      =   'Comment status updated';
            $emailInput['statusBeforeUpdate']=$getStatusBeforeUpdate['status'];
        }
        else
        {
            $emailInput['subject']      =   'Comment '.$parentComment['thread_comment']['comment']. ' has been re-opened';
            $emailInput['statusBeforeUpdate']=$getStatusBeforeUpdate['status'];
        }
        
        $emailInput['mainComment'] = $parentComment['thread_comment']['comment'];
        $emailInput['domain_name'] = $serverName;
        //Send Mail to Assignee and Owner
        return $this->run(new SendCommentNotificationToOwnerAndAssigneeJob($emailInput));
    }
}
