<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Organization\Jobs\ValidateOrgCodeJob;
use Illuminate\Support\Facades\Storage;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Document\Jobs\ValidateDocumentJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageJob;

use App\Domains\CaseStandard\Jobs\ValidateDocumentBySourceIdJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageWithActiveMetadataJob;
use App\Domains\CaseStandard\Jobs\GetDocumentIdFromS3Job;
use Log;
use App\Services\Api\Traits\SavingIMSResponseTrait;
use App\Domains\CaseStandard\Jobs\GetLastUpdatedTimeJob;
use App\Domains\Organization\Jobs\GetOrgDetailsJob;
use App\Domains\CaseStandard\Jobs\InsertAccessActivityLogJob;
class GetCFPackageFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait, SavingIMSResponseTrait;

    public function handle(Request $request)
    {
        try{
            $orgCode = isset($request['org_code'])?$request['org_code']:'';
            $requestUrl    = url('/');
            $ipAddress     = request()->ip();
            $orgCodeArr=[];
            if ($orgCode) {
                $orgCodeArr['org_code'] = $orgCode;
                $validateOrgCode = $this->run(new ValidateOrgCodeJob($orgCodeArr));
                if ($validateOrgCode['org_code'][0] != false) {
                    return $this->respondWithCASEJsonError(404, false);
                }
            }
            $packageIdentifier = $request->route('identifier');
            $validateUUID = $this->isUuidValid($packageIdentifier);
            if($validateUUID===true){

                $validationStatus = $this->run(new ValidateDocumentBySourceIdJob($packageIdentifier));
             
                if($validationStatus===true){
                    $getOrganizationId  = $this->run(new GetOrgDetailsJob($orgCodeArr));
                    $organizationId     = $getOrganizationId['organization_id'];
                    $activityEventType  = config("event_activity")["event_type_value"]["CFPACKAGE_ACCESS_LOG"];
                    $this->run(new InsertAccessActivityLogJob($organizationId,$ipAddress,$activityEventType,$packageIdentifier));
                    $getLastUpdatedTime =  $this->run(new GetLastUpdatedTimeJob($packageIdentifier,$organizationId));
                    $updatedAt = $getLastUpdatedTime->updated_at;
                    $document_id = $getLastUpdatedTime->document_id;
                    $document_status = $getLastUpdatedTime->status;
                    
                    if($document_status == 1)
                    {
                        $packageData = $this->run(new GetCasePackageWithActiveMetadataJob($packageIdentifier,[],$orgCode,'','',$requestUrl));
                    }
                    else
                    {
                        $additionalParams       = ['versioning'=>true];
                        $keyInfo = ['identifier' => $packageIdentifier.'_'.$orgCode.'_'.$document_id, 'prefix' =>'cfpackages'];
                        $packageData = $this->run(new GetFromCacheJob($keyInfo));
                        if(isset($packageData['status']) && $packageData['status'] == "error") {
                            $packageData = $this->run(new GetCasePackageWithActiveMetadataJob($packageIdentifier,[],$orgCode,'','',$requestUrl,$additionalParams));
                            if(!empty($packageData) && count($packageData) > 0) {
                                $this->run(new DeleteFromCacheJob($keyInfo));
                                $this->run(new SetToCacheJob($packageData,$keyInfo));
                                $packageData = $this->run(new GetFromCacheJob($keyInfo));
                            }
                        }
                    }
                    return $this->respondWithCASEJsonSuccess($packageData);
                }
                else {
                    return $this->respondWithCASEJsonError(404);
                }
            }
            else {
                return $this->respondWithCASEJsonError(404, false);
            }
        }
        catch(\Exception $ex){
            Log::error($ex);
            return $this->respondWithCASEJsonError(500);
        }
    }

    private function respondWithCASEJsonSuccess($data) {
        $responseReturn =response()->json($data, 200, [], JSON_UNESCAPED_SLASHES);
        $this->IMSResponse($responseReturn);
        return $responseReturn;
    }

    private function respondWithCASEJsonError($httpStatus, $validUUID = true) {
        $errorResponseBody = [
            "imsx_codeMajor" => "failure",
            "imsx_severity" => "error",
            "imsx_description" => "",
        ];
        switch ($httpStatus) {
            case '404':
                $imsx_codeMinorFieldValue = $validUUID ? "unknownobject" : "invaliduuid";
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "sourcedId",
                                                                                    "imsx_codeMinorFieldValue" => $imsx_codeMinorFieldValue
                                                                                ];
                break;
            default:
                $errorResponseBody["imsx_codeMinor"]["imsx_codeMinorField"][] = [
                                                                                    "imsx_codeMinorFieldName" => "",
                                                                                    "imsx_codeMinorFieldValue" => "internal_server_error"
                                                                                ];
                break;
        }

        if (!$validUUID) {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES)
                        ->setStatusCode(404, 'Invalid UUID');
        } else {
            return response()->json($errorResponseBody, $httpStatus, [], JSON_UNESCAPED_SLASHES);
        }
        
    }
}
