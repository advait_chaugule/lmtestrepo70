<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportConnectionJob;
use App\Domains\JReport\Jobs\JasperDrillDownSubReportJob;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use App\Domains\JReport\Jobs\SetContentTypeJob;

class ShowJasperDrillDownSubReportFeature extends Feature
{
    public function handle(Request $request)
    {
        $connection= $this->run(new JReportConnectionJob());
        $input = $request->input();

        $organization_id = $input['organization_id'];
        $document_id = $input['document_id'];
        
        $file_type='html';                      
        $content=$this->run(new JasperDrillDownSubReportJob($connection,$organization_id,$file_type,$document_id));
        $file_name=time();
        $this->run(new SetContentTypeJob($file_type,$content,$file_name));
    }
}
