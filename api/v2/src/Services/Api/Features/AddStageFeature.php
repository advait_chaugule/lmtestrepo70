<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Workflow\Jobs\ValidateWorkflowIdJob;
use App\Domains\Stage\Jobs\ValidateStageInputJob;
use App\Domains\Stage\Jobs\CreateStageJob;
use App\Domains\WorkflowStage\Jobs\CreateWorkflowStageJob;
use App\Domains\WorkflowStage\Jobs\CreateWorkflowStageRoleJob;
use Log;

class AddStageFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, UuidHelperTrait;

    public function handle(Request $request)
    {

    try{

       
            $requestData = $request->all();
            $requestData['workflow_id'] = $request->route('workflow_id');

            $identifier     =   ['workflow_id'  =>  $requestData['workflow_id']];
            
            $validateWorkflow    =   $this->run(new ValidateWorkflowIdJob($identifier));
           if($validateWorkflow === true) {

            $validateCreateStageInput = $this->run(new ValidateStageInputJob($requestData));
            if($validateCreateStageInput===true) {
              
              $StageData["stage_id"] = $this->createUniversalUniqueIdentifier();
              $StageData["organization_id"] = $requestData["auth_user"]["organization_id"];
              $StageData["name"] = $requestData['stage_name'];
              $StageData["order"] = $requestData['stage_order'];
              $StageData["is_deleted"] = 0;
              $createStageJob = $this->run(new CreateStageJob($StageData));
               
              $workflowstageInput['workflow_stage_id'] = $requestData['workflow_id'].'||'.$StageData["stage_id"];
              $workflowstageInput['workflow_id'] = $requestData['workflow_id'];
              $workflowstageInput['stage_id'] = $StageData["stage_id"];
              $workflowstageInput['order'] = $requestData['stage_order'];
              $workflowstageInput['stage_name'] = $requestData['stage_name'];
              $workflowstageInput['stage_description'] = $requestData['stage_description'];
              $worflowStageCreated = $this->run(new CreateWorkflowStageJob($workflowstageInput));
                
              if(isset($requestData['workflow_stages_role']))
              {
                $roleIds = $requestData['workflow_stages_role'];
                foreach($roleIds as $roleId)
                {
                     
                      $workflowStageRoleData['workflow_stage_role_id'] = $workflowstageInput['workflow_stage_id']."||".$roleId["role_id"];
                      $workflowStageRoleData['workflow_stage_id'] = $workflowstageInput['workflow_stage_id'];
                      $workflowStageRoleData['role_id'] = $roleId['role_id'];
                      $worflowStageRoleCreated = $this->run(new CreateWorkflowStageRoleJob($workflowStageRoleData)); 
                }  
              }
             

              $successType = 'created';
              $message = 'Stage created successfully.';
              $_status = 'custom_status_here';
              return $this->run(new RespondWithJsonJob($successType, $worflowStageCreated, $message, $_status));

            }
            else {
                $errorType = 'validation_error';
                $message = $validateCreateStageInput;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

            } 
            else {
                $errors = $validateWorkflow;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
     
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
