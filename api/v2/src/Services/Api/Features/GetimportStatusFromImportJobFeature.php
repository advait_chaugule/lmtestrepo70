<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Import\Jobs\ValidatefileIdJob;
use App\Domains\Import\Jobs\ValidateUserIdOrBatchIdJob;
use App\Domains\Import\Jobs\GetimportStatusFromImportJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Import\Jobs\GetStatusAndDetailsFromImportJob;
use Log;
use Illuminate\Support\Facades\DB;

class GetimportStatusFromImportJobFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
                $fileId = $request->route('import_job_id');
                $pollingFor = (!empty($request->polling_for) && (in_array(strtolower($request->polling_for), ['csv','taxonomy']))) ? strtolower($request->polling_for) : "json";
                $organizationId = $request->auth_user['organization_id'];
                if($pollingFor == 'json') {
                    $ValidatefileId = $this->run(new ValidatefileIdJob($fileId));
                    if($ValidatefileId === true) {
                        $importStatus    =   $this->run(new GetimportStatusFromImportJob(['import_job_id' => $fileId]));
                        $response = ''; 
                        $successType = 'found';
                        $message = 'Data Found';
                        $_status = 'custom_status_here';

                        return $this->run(new RespondWithJsonJob($successType, $importStatus, $message, $_status));
                    }
                    else
                    {
                        $errors = $ValidatefileId;
                        $errorType = 'validation_error';
                        $message = $this->run(new MessageArrayToStringHelperJob($errors));
                        $_status = 'custom_status_here';

                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                } else if($pollingFor == 'csv') {
                    $type = (!empty($request->type) && strtolower($request->type) == "user") ? "user_id" : "batch_id";
                    $link_type = (!empty($request->link_type) && strtolower($request->link_type) == "summary") ? "summary" : "";
                    # $fileId can be user_id or batch_id
                    $ValidateId = $this->run(new ValidateUserIdOrBatchIdJob($fileId, $type));
                    if($ValidateId != 'not_found') {
                        if($link_type != '' && $type == 'batch_id') {
                            $settings_arr = [];
                            $settings_obj = DB::table('import_job')
                                            ->select('import_taxonomy_settings')
                                            ->where('batch_id',$fileId)
                                            ->where('is_deleted', 0)
                                            ->first();

                            if($settings_obj) {
                                $settings_arr = json_decode($settings_obj->import_taxonomy_settings, true);
                            }
                            if($settings_obj == null || (isset($settings_arr['summary_link_expired']) && $settings_arr['summary_link_expired'] == true)) {
                                $successType = 'custom_validation_error';
                                $message = ['This link has expired'];
                                $_status = 'custom_status_here';

                                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                            }
                        }
                        $importStatus    =   $this->run(new GetStatusAndDetailsFromImportJob(['identifier' => $fileId, 'type' => $type, 'organization_id' => $organizationId]));
                        $successType = 'custom_found';
                        $message = ['Data Found'];
                        $_status = 'custom_status_here';

                        return $this->run(new RespondWithJsonJob($successType, $importStatus, $message, $_status, [], true));
                    } else {
                        $successType = 'custom_validation_error';
                        $message = ['Invalid '.str_replace('_', ' ', $type)];
                        $_status = 'custom_status_here';

                        return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                    }
                }
                else if($pollingFor == 'taxonomy') {

                    $listofObject = DB::table('import_job')
                                            ->where('user_id',$fileId)
                                            ->where('batch_id','!=','')
                                            ->where('is_deleted', 0)
                                            ->orderBy('updated_at', 'desc')
                                            ->get()
                                            ->toArray();
                                            $taxoList = [];
                                            foreach($listofObject as $listofObjectK =>$listofObjectV)
                                            {
                                                $taxoList[] =  [
                                                    'import_id' => $listofObjectV->import_job_id,
                                                    'batch_id' => $listofObjectV->batch_id,
                                                    'status' =>  $listofObjectV->status,
                                                    'source_document_id' =>  $listofObjectV->source_document_id,
                                                    'import_taxonomy_setting' =>  ($listofObjectV->import_taxonomy_settings != '') ? json_decode($listofObjectV->import_taxonomy_settings, true) : ''
                                                ];
                            
                                            } 
                                            $successType = 'custom_found';
                                            $message = ['Data Found'];
                                            $_status = 'custom_status_here';
                    
                                            return $this->run(new RespondWithJsonJob($successType, $taxoList, $message, $_status, [], true));

                }
            }
            catch(\Exception $ex){
                $errorType = 'internal_error';
                $message = $ex->getMessage();
                $_status = 'custom_status_here';
                Log::error($ex);
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
    }
}
