<?php
namespace App\Services\Api\Features;

use App\Domains\Notifications\Jobs\GetProjectDetailByProjectId;
use App\Domains\Notifications\Jobs\GetTaxonomyNameById;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use App\Domains\Project\Jobs\CheckProjectUserMappingJob;
use App\Domains\Project\Jobs\AssignProjectUserJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Notifications\Jobs\NotificationJobForEmail;
use App\Domains\User\Jobs\GetUserByIdJob;
use App\Domains\Caching\Events\CachingEvent;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use Log;

class AssignProjectUserFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $projectId = $request->route('project_id');
            //$identifier = ['project_id' => $projectId];
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);
            $input = $request->all();
            $userId         = $request->input('user_id');
            $organizationId = $input['auth_user']['organization_id'];
            //print_r($organizationId);die;
            $loginUserId = $input['auth_user']['user_id'];
            $identifier = ['project_id' => $projectId, 'organization_id' => $organizationId];
            $workflowStageRoleId = $request->input('workflow_stage_role_id');
            $requestData    = ['user_id' => $userId, 'workflow_stage_role_id' => $workflowStageRoleId,'organization_id'=>$organizationId ];
            $validateStatus = $this->run(new ValidateProjectIdJob($identifier));
            if($validateStatus === true){
                $checkIsProjectDeleted = $this->run(new CheckIsProjectDeletedJob($identifier));
                if(!$checkIsProjectDeleted === true) {
                    $checkProjectUserRoleMappingIsExists = $this->run(new CheckProjectUserMappingJob($identifier, $requestData));

                    //$response = $checkProjectUserRoleMappingIsExists;
                    $workFlowStageIdRole    = $checkProjectUserRoleMappingIsExists[0]['workflow_stage_role_id'];
                    foreach($checkProjectUserRoleMappingIsExists as $data){
                        if($data['user_exists'] === false){
                            $mapUserToStageRole = $this->run(new AssignProjectUserJob($identifier, $data, $loginUserId));
                        }
                    }
                    // In App Notification
                    $role_name  = $checkProjectUserRoleMappingIsExists[0]['role_name'];
                    $eventType  = config("event_activity")["Notifications"]["ASSIGN_PROJECT_ROLE"];
                     $eventData = [
                                 'project_id'               => $projectId,
                                 'user_id'                  => $userId,
                                 'work_flow_stage_role_id'  => $workFlowStageIdRole,
                                 'event_type'               => $eventType,
                                 'role_name'                => $role_name,
                                 "beforeEventRawData"       => [],
                                 "afterEventRawData"        => '',
                                 "requestUserDetails"       => $request->input("auth_user")
                                 ];

                    event(new NotificationsEvent($eventData));  //In App Notification ends
                    // Sent Mail for assigned user to project
                    $projectDetails       = $this->run(new GetProjectDetailByProjectId(['project_id'=>$projectId]));
                    
                    $projectName          = isset($projectDetails[0]->project_name)?$projectDetails[0]->project_name:'';
                    //$messageNotify        = config('event_activity')['notification_message']["5"];
                    $notification         = "You have been assigned the role of  {$role_name} for the project {$projectName}.";
                    $getUserDetails = $this->run(new GetUserByIdJob(['user_id' => $checkProjectUserRoleMappingIsExists[0]['user_id']]));
                    $getUserDetails = $getUserDetails->toArray();
                    $email          = $getUserDetails['email'];
                    $firstName      = $getUserDetails['first_name'];
                    $lastName       = $getUserDetails['last_name'];
                    $currentOrgId   = $getUserDetails['current_organization_id'];
                    // Organization name and organization code send in email
                    $userOrganizationId = $this->run(new GetUserOrganizationIdWithUserJob($loginUserId,$organizationId));
                    //print_r($userOrganizationId);die;
                    $organizationData = $this->run(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                    $organizationName = $organizationData['organization_name'];
                    $organizationCode = $organizationData['organization_code'];
                   $userName       = $firstName.' '.$lastName;
                   $emailData = [
                                      'subject'=>$notification,
                                       'email'    => $email,
                                      'body'     => ['email_body'=>$notification,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode],
                                      'domain_name'=>$serverName
                                   ];
                   // Disable and Enable user email ACMT-1869->1962
                    $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>  $checkProjectUserRoleMappingIsExists[0]['user_id'], 'organization_id' => $currentOrgId]));
                    $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                    if ($emailSettingStatus==1) {
                        $this->run(new NotificationJobForEmail($emailData));
                    }
                     
                    $response = $checkProjectUserRoleMappingIsExists;
                    $successType = 'created';
                    $message = 'User created successfully.';
                    $_status = 'created';
                    return $this->run(new RespondWithJsonJob($successType,  $response, $message, $_status));
                }
                else{
                    // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                    $errorType = 'not_found_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Project is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                $errorType = 'not_found_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Project ID is invalid';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
