<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Taxonomy\Jobs\ValidateInputForCreateAndUploadTaxonomySearchDataJob;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Taxonomy\Jobs\CreateTaxonomySearchDataJob;
use App\Domains\Search\Jobs\UploadSearchDataJob;
use Log;

class CreateAndUploadTaxonomySearchDataFeature extends Feature
{

    use ErrorMessageHelper, ArrayHelper, StringHelper;

    public function handle(Request $request)
    {
        // set higher memory limit and execution time since this feature is resource intensive
        ini_set('memory_limit','8000M');
        ini_set('max_execution_time', 0);

        try{
            $requestInput = $request->all();
            $validateRequest = $this->run(new ValidateInputForCreateAndUploadTaxonomySearchDataJob($requestInput));
            if($validateRequest===true) {
                $documentIds = $this->createArrayFromCommaeSeparatedString($request->input('document_ids'));
                $httpResponseMessage = $this->executeCreateAndUploadSearchDataProcess($documentIds);
                $successType = 'created';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validateRequest;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function executeCreateAndUploadSearchDataProcess(array $documentIds): array {
        $httpResponseMessage = [];
        foreach($documentIds as $documentId) {
            $documentIdentifier = trim($documentId);
            $validateDocumentId = $this->run(new ValidateDocumentByIdJob([ "document_id" => $documentIdentifier ]));
            if($validateDocumentId===true) {
                $taxonomySearchData = $this->run(new CreateTaxonomySearchDataJob($documentIdentifier));
                if(!empty($taxonomySearchData)) {
                    $this->run(new UploadSearchDataJob($taxonomySearchData));
                    $message = "Search data for Document:$documentIdentifier has been uploaded successfully.";
                }
                else {
                    $message = "Search data for Document:$documentIdentifier was not uploaded successfully.";
                }
            }
            else {
                $message = "Invalid Document:$documentIdentifier.";
            }
            $httpResponseMessage[] = $message;
        }
        return $httpResponseMessage;
        // return $this->arrayContentToCommaeSeparatedString($httpResponseMessage);
    }
}
