<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Document\Jobs\ValidatePublicReviewDocumentJob;
use App\Domains\Role\Jobs\ListRolePermissionJob;
use App\Domains\Permission\Jobs\VerifyUserRolePermissionAccessJob;
use App\Domains\Comment\Jobs\GetDocumentUserCommentsJob;
use Illuminate\Support\Facades\DB;
use Log;

class GetDocumentUserCommentsFeature extends Feature
{

    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $documentId = $request->route('document_id');
            $validatePublicReviewDocument = $this->run(new ValidatePublicReviewDocumentJob($documentId));
            if($validatePublicReviewDocument===true){
                $authenticatedUserDetails = $request->input('auth_user');
                $organizationID = $authenticatedUserDetails['organization_id'];
                // get user role permissions
                // get Project Role permission
                $roleCode = config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"];
                $RolesArr = DB::table('roles')
                ->select('role_id')
                ->where('is_deleted','0')
                ->where('role_code',$roleCode)
                ->where('organization_id',$organizationID)
                ->get()
                ->toArray();

                $userRoleId = $RolesArr[0]->role_id;
                $rolePermissionsAvailable = $this->run(new ListRolePermissionJob($userRoleId));

                $checkForPermissionAccess = ['view_my_comments', 'view_others_comments'];
                $accessDetails = $this->run(new VerifyUserRolePermissionAccessJob($rolePermissionsAvailable, $checkForPermissionAccess));

                if(!empty($accessDetails)) {
                    $userId = $authenticatedUserDetails['user_id'];
                    $jobResponse = $this->run(new GetDocumentUserCommentsJob($documentId, $userId, $accessDetails));
                    $successType = 'found';
                    $message = !empty($jobResponse) ? 'Comments found.' : 'Comments not found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $jobResponse, $message, $_status));
                }
                else {
                    $errorType = 'authorization_error';
                    $message = 'Permission not set.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errorType = 'validation_error';
                $message = 'Document is not valid public review.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
