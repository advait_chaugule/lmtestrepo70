<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\GetUserDetailsJob;
use App\Domains\Organization\Jobs\GetOrganizationDetailByIdJob;
use App\Domains\Organization\Jobs\GetActiveOrganizationsByUserIdJob;
use Log;

class GetUserDetailsByTokenFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $projectlist = $this->run(GetUserDetailsJob::class, $request);
            if(!empty($projectlist)){
            $orgDetails = $this->run(new GetOrganizationDetailByIdJob($projectlist->organization_id));
            
            // Check if current organization if inactive
            if($orgDetails->is_active=='0'){

                // Get all associated active organizations of this user through email
                $currOrganization = $this->run(new GetActiveOrganizationsByUserIdJob($projectlist->user_id));
                // If no organizations found, show error. Else continue resetting password
                if(empty($currOrganization)){
                    $errorType = 'custom_found';
                    $message = ['This Tenant is deactivated and please contact admin'];
                    return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
                }                
            }
            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $projectlist, $message, $_status));
            }
            else{
                $errorType = 'custom_found';
                $message = ['This link is no longer valid'];
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
            }
        } catch (\Exception $ex) {
            
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
