<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Tenant\Jobs\checkIsRegisterJob;
use App\Domains\User\Jobs\SendRegistrationEmailNotificationJob;
use App\Domains\Organization\Jobs\GetOrganizationDetailByIdJob;

use Log;

class resendEmailFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $organizationId = $requestData['auth_user']['organization_id'];
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);

            $emailId    = $requestData['email'];
            $organizationDetail =   $this->run(new GetOrganizationDetailByIdJob($organizationId));

            $checkIsRegister = $this->run(new checkIsRegisterJob($emailId));
            
           if($checkIsRegister !==null){
           
            $email['email'] = $emailId;
            $email['active_access_token'] = $checkIsRegister[0]->active_access_token;
            $email['org_code']  =   $organizationDetail->org_code;
            $email['domain_name']  =  $serverName;
            $this->run(new SendRegistrationEmailNotificationJob($email));

            $user['email'] = "Sent verification mail.";
            $successType = 'custom_found';
            $_status = 'custom_status_here';
            $message =['Data created successfully.'];
            return $this->run(new RespondWithJsonJob($successType,$user, $message, $_status,[], true));
           }
           else{

            $errorType = 'custom_validation_error';
            $_status = 'custom_status_here';
            $message = ['The user is active or already registered'];
            return $this->run(new RespondWithJsonJob($errorType, null, $message , $_status,[], false));
           }

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message =  $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
