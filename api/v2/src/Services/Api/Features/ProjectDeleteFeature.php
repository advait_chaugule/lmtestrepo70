<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\ValidateCreateProjectInputJob;
use App\Domains\Project\Jobs\EditProjectJob;
use App\Domains\Project\Jobs\ProjectNodeMappingDeleteJob;
use App\Domains\Project\Events\DeleteProjectEvent;
use App\Domains\Project\Jobs\MappedUsersListJob;
use App\Domains\User\Jobs\UserRoleListJob;
use App\Domains\Project\Jobs\UnAssignProjectUserJob;
use App\Domains\Project\Jobs\GetUserRoleInfoJob;
use App\Domains\Project\Jobs\SendUserRemoveProjectAccessEmailNotificationJob;
use App\Domains\PacingGuide\Jobs\PacingGuideDocumentMappingDeleteJob;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Caching\Events\CachingEvent;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use Log;

class ProjectDeleteFeature extends Feature
{

    public function handle(Request $request)
    {
        try{
            $inputs = $request->all();
            $projectId = $request->route('project_id');
            $input = [ 'project_id' => $projectId ];
            $identifier = ['project_id' => $projectId]; 
            $organizationId = $inputs['auth_user']['organization_id'];
            $loginUser = $inputs['auth_user']['user_id'];
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);
            $validateExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateExistence===true) {
                
                // set attribute value to delete project
                $input["is_deleted"] = 1;
                // reset the document assigned to project
                $input["document_id"] = '';
                $mappedUsersList = $this->run(new MappedUsersListJob($identifier));
                $userNameAndRoleList = $this->run(new UserRoleListJob($mappedUsersList['user_id'], $projectId));
               if(count($userNameAndRoleList) > 0)
               {    
                    foreach($userNameAndRoleList as $singleUserNameAndRoleList) 
                    {
                        $projectAssignUser = $singleUserNameAndRoleList['user_id'];
                        $workflowStageRoleId = $singleUserNameAndRoleList['workflow_stage_role_id'];
                        
                        $requestData = ['user_id' => $projectAssignUser, 'workflow_stage_role_id' => $workflowStageRoleId ];
                        $getUserRoleInfo =   $this->run(new GetUserRoleInfoJob($projectId, $requestData));
                        $deleteUserToRoleInProject = $this->run(new UnAssignProjectUserJob($projectId, $requestData,$organizationId, $loginUser));
                        if($deleteUserToRoleInProject) {
                            foreach($getUserRoleInfo as $userRoleInfo){
                                $inputDetail['user_name'] = $userRoleInfo['user_name'];
                                $inputDetail['user_id'] = $userRoleInfo['user_id'];
                                $inputDetail['email'] = $userRoleInfo['user_email'];
                                $inputDetail['role_name'] = $userRoleInfo['role_name'];
                                $inputDetail['project_name'] = $userRoleInfo['project_name'];
                                $inputDetail['current_organization_id'] = $userRoleInfo['current_organization_id'];
                                $inputDetail['domain_name'] = $serverName;
                                // Check Email status enable and disable with org name and code
                                $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($loginUser,$organizationId));
                                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                                $inputDetail['organization_name'] = $organizationData['organization_name'];
                                $inputDetail['organization_code'] = $organizationData['organization_code'];
                                $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$inputDetail['user_id'], 'organization_id' => $inputDetail['current_organization_id']]));
                                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                                if ($emailSettingStatus==1) {
                                    unset($inputDetail['user_id']);
                                    unset($inputDetail['current_organization_id'] );
                                    $this->run(new SendUserRemoveProjectAccessEmailNotificationJob($inputDetail));
                                }
                            }
                        }
                        else{
                            // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                            $errorType = 'not_found_error';
                            // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                            $message = 'User is already deleted';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                        }

                    }
               } 
                $afterEventProject = $this->run(new EditProjectJob($input));
                
                //Soft Delete record from pacing guide and document mapping
                if($afterEventProject['project_type']==2){
                    $this->run(new PacingGuideDocumentMappingDeleteJob($projectId));
                }

                // Remove project item mapping, project document mapping,
                $this->run(new ProjectNodeMappingDeleteJob($projectId));

                if($afterEventProject!==false){
                    // raise event to track activities for reporting
                    $beforeEventRawData = $afterEventProject->toArray();
                    $beforeEventRawData['is_deleted'] = 0;
                    $afterEventRawData = $afterEventProject->toArray();
                    $eventData = [
                        "beforeEventRawData" => (object) $beforeEventRawData,
                        "afterEventRawData" => (object) $afterEventRawData,
                        "requestUserDetails" => $request->input("auth_user")
                    ];
                    event(new DeleteProjectEvent($eventData));

                    $this->raiseEventToUploadSearchDataToSqs($projectId);

                    $successType = 'updated';
                    $message = 'Data deleted successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $afterEventProject, $message, $_status));
                }
                else{
                    $errorType = 'internal_error';
                    return $this->run(new RespondWithJsonErrorJob($errorType));
                }
            }
            else {
                $errors = $validateExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function raiseEventToUploadSearchDataToSqs(string $projectId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations['update_project'];
        $eventData = [
            "type_id" => $projectId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
