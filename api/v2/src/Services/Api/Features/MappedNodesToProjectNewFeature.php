<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;

use App\Domains\Project\Jobs\GetFlattenedTaxonomyForProjectCompleteTreeJob;
use App\Domains\Project\Jobs\GetDocumentIdJob;
use App\Domains\Project\Jobs\GetProjectItemsJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class MappedNodesToProjectNewFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try {
            $treeData = [];
            //Get project identifier from route
            $projectId = $request->route('project_id');
            $input = [ 'project_id' => $projectId];
            //Validate the project identifier
            $validateProjectExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateProjectExistence===true) {
                $requestUser = $request->input("auth_user");
                //dd($request);
                if(!empty($request['document_id'])){
                    $documentIdentifier =   $request['document_id'];
                } else {
                    $documentIdentifier = $this->run(new GetDocumentIdJob($projectId));
                }
                
                $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyForProjectCompleteTreeJob($documentIdentifier, $projectId, $requestUser, $request));

                /*** Create the success response ***/
                $successType = 'found';
                $message = 'Mapped Node listed successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
            }
            else {
                $errors = $validateProjectExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        } catch (\Exception $ex) {
             $errorType = 'internal_error';
             $message = $this->createErrorMessageFromException($ex);
             $_status = 'custom_status_here';
             Log::error($ex);
             return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
