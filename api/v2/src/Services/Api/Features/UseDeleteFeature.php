<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\ValidateUserByIdJob;
use App\Domains\User\Jobs\DeleteUserJob;
use App\Domains\User\Jobs\GetProjectDetailsByIdJob;
use App\Domains\User\Jobs\UserRoleListJob;
use App\Domains\Project\Jobs\GetUserRoleInfoJob;
use App\Domains\Project\Jobs\UnAssignProjectUserJob;
use App\Domains\Project\Jobs\SendUserRemoveProjectAccessEmailNotificationJob;
use App\Domains\User\Jobs\RenameUsernameForHistoryBackupJob;
use App\Domains\User\Jobs\CheckUserOrganizationDeleteJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use Log;

class UseDeleteFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestUserData = $request->all();
            $requestUrl = url('/');
            $serverName = str_replace('server', '', $requestUrl);
            $organizationId = $requestUserData['auth_user']['organization_id'];
            $userIdentifier = $request->route('user_id');
            $loginUser = $requestUserData['auth_user']['user_id']; 

            $identifier = ['user_id' => $userIdentifier,'organization_id'=>$organizationId];

            $validationStatus = $this->run(new ValidateUserByIdJob($identifier));
            if($validationStatus === true) {
                $getUserProjectDetails = $this->run(new GetProjectDetailsByIdJob($identifier));
                  foreach($getUserProjectDetails[0]['projects'] as $projects)
                  {
                      if($projects['is_deleted'] == 0)
                      {
                        $mappedUsersList['user_id'][0] = $userIdentifier;
                        $userNameAndRoleList = $this->run(new UserRoleListJob($mappedUsersList['user_id'], $projects['project_id']));
                        unset($mappedUsersList['user_id'][0]);
                        if(count($userNameAndRoleList) > 0)
                        {
                             foreach($userNameAndRoleList as $singleUserNameAndRoleList)
                             {
                                 $projectAssignUser = $singleUserNameAndRoleList['user_id'];
                                 $workflowStageRoleId = $singleUserNameAndRoleList['workflow_stage_role_id'];

                                 $requestData = ['user_id' => $projectAssignUser, 'workflow_stage_role_id' => $workflowStageRoleId];
                                 $getUserRoleInfo =   $this->run(new GetUserRoleInfoJob($projects['project_id'], $requestData));
                                 $deleteUserToRoleInProject = $this->run(new UnAssignProjectUserJob($projects['project_id'],$requestData,$organizationId,$loginUser));
                                 if($deleteUserToRoleInProject) {
                                     foreach($getUserRoleInfo as $userRoleInfo){
                                         $inputDetail['user_name'] = $userRoleInfo['user_name'];
                                      $inputDetail['email'] = $userRoleInfo['user_email'];
                                      $inputDetail['user_id'] = $userRoleInfo['user_id'];
                                      $inputDetail['role_name'] = $userRoleInfo['role_name'];
                                      $inputDetail['project_name'] = $userRoleInfo['project_name'];
                                      $inputDetail['domain_name'] = $serverName;
                                         $userOrganizationId = $this->run(new GetUserOrganizationIdWithUserJob( $inputDetail['user_id'],$organizationId));
                                         $organizationData = $this->run(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                                         $inputDetail['organization_name'] = $organizationData['organization_name'];
                                         $inputDetail['organization_code'] = $organizationData['organization_code'];
                                         $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' =>$inputDetail['user_id'], 'organization_id' => $organizationId]));
                                         $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                                         if ($emailSettingStatus==1) {
                                             $this->run(new SendUserRemoveProjectAccessEmailNotificationJob($inputDetail));
                                             unset($inputDetail['organization_name']);
                                             unset($inputDetail['organization_code']);
                                         }
                                     }
                                 }
                                 else{
                                     // return $this->run(new RespondWithJsonErrorJob($validationStatus));
                                     $errorType = 'not_found_error';
                                     // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                                     $message = 'User is already deleted';
                                     $_status = 'custom_status_here';
                                     return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                 }

                             }
                        }
                      }


                  }
               $deleteUser = $this->run(new DeleteUserJob($identifier));
               $email = $deleteUser[0]['email'];
                $inputEmail = ['email' => $email];
               // $this->run(new RenameUsernameForHistoryBackupJob($inputEmail));
                $this->run(new CheckUserOrganizationDeleteJob($userIdentifier,$organizationId,$email));
                $response = ''; //$deleteUser
                $successType = 'found';
                $message = 'User deleted successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid user id.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
