<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Taxonomy\Jobs\ListUnmappedTaxonomyJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class ListUnmappedTaxonomyFeature extends Feature
{       
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try
        {
            $requestData                =   $request->all();
            $documentIdentifier['document_id']         =   $request->route('document_id');
            $validation = $this->run(new ValidateDocumentByIdJob($documentIdentifier));
            
            if($validation===true)
            {
                $unmappedTaxonomyList = $this->run(new ListUnmappedTaxonomyJob($documentIdentifier));
                $response = $unmappedTaxonomyList;
                $successType = 'found';
                $message = 'Data fetched successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else
            {
                $errorType = 'validation_error';
                $message = "Please provider valid document_id";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }    
        }
        catch(\Exception $ex)
        {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
