<?php
namespace App\Services\Api\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;

class SequenceNoFeature extends Feature
{
    public function handle(Request $request)
    {
        ini_set('memory_limit','80980M');
        ini_set('max_execution_time', 0);

        try {
            $output_response = [];
            $org_code = trim($request->org_code);
            $org = DB::table('organizations')->select('organization_id')->where('org_code', $org_code)->where('is_deleted', 0)->first();
            if(!$org) {
                $successType = 'custom_not_found';
                $message = 'Organization not found';
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], false));
            }

            $org_id = $org->organization_id;
            # exclude taxonomies with import type 1
            $document_id_arr = DB::select("select `document_id` from `acmt_documents` where `is_deleted` = 0 and `import_type` != 1 and organization_id='".$org_id."'");
            //*************************add this code: and organization_id='".$org_id."' '4cf054b7-5b44-4936-8f1d-c7daea383d0f'c8bdc1ac-5341-4449-9a5a-1061e58355c0

            # preparing array
            # assign sequence no to one taxonmy at a time
            foreach($document_id_arr as $doc_id_value) {
                if(isset($doc_id_value->document_id) && !empty($doc_id_value->document_id)) {
                    # can't use same select query for both migration as sequence nos is required for second migration
                    Log::error('------migration started for: '.$doc_id_value->document_id );
                    $this->nodeMigrationSequenceNo($doc_id_value->document_id);
                    Log::error('------migration in progress for: '.$doc_id_value->document_id );
                    $this->assoMigrationSequenceNo($doc_id_value->document_id);
                    Log::error('------migration finshed for: '.$doc_id_value->document_id );
                }
            }

            // assign sequence no to multiple taxonmy at a time
            // $tax_arr = [];
            // foreach($document_id_arr as $doc_id_value) {
            //     $tax_arr[] = $doc_id_value->document_id;
            // }
            // $this->nodeMigrationSequenceNo($tax_arr);
            // $this->assoMigrationSequenceNo($tax_arr);

            $successType = 'custom_found';
            $message = 'Execution completed successfully';
            $_status = 'custom_status_here';

            return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], true));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
            $_status = 'custom_status_here';
            Log::error($message);

            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function nodeMigrationSequenceNo($tax)
    {
        // $document_id_arr_chunk = array_chunk($tax, 10);

        # execute script for 10 documents/trees at a time
        // foreach ($document_id_arr_chunk as $chunk) {
            $item_child_asso = DB::select( "select `item_id`, `full_statement`, `human_coding_scheme`, `item_association_id`, `sequence_number`, `acmt_item_associations`.`source_item_id`, `acmt_item_associations`.`target_item_id`
                                            from `acmt_item_associations`
                                            left join `acmt_items` on `acmt_item_associations`.`source_item_id` = `acmt_items`.`item_id`
                                            where `association_type` = 1
                                            and `source_document_id` = '".$tax."'
                                            and `acmt_items`.`is_deleted` = 0
                                            and `acmt_item_associations`.`is_deleted` = 0
                                            and `acmt_item_associations`.`sequence_number` = 0
                                            order by `acmt_item_associations`.`created_at` asc" );
            // test for multiple doc_id's in $chunk in implode ----- and `source_document_id` in ('".implode("','",$chunk)."')

            $grouped_level_arr = [];
            # group nodes with the same parent
            foreach ($item_child_asso as $asso_key => $asso_value) {
                $grouped_level_arr[$asso_value->target_item_id][] = (array)$asso_value;
            }
            unset($item_child_asso);
            $sorted_arr = [];
            # call sort function on each group
            foreach($grouped_level_arr as $key => $value) {
                $sortBy = $this->checkSortType($value);
                # no need to update if sequence number found
                if($sortBy != 'sequence_number') {
                    $sorted_arr[] = array_values($this->arraySort($value, $sortBy));
                }
            }
            unset($grouped_level_arr);
            $sequence_arr = [];
            # assign sequence no to each child association
            foreach ($sorted_arr as $group_key => $group_value) {
                $counter = 1;
                foreach ($group_value as $asso_key => $asso_value) {
                    $sequence_arr[$asso_value['item_association_id']] = $counter;
                    $counter++;
                }
            }
            unset($sorted_arr);
            # prepare array such that sequence no is key and all the corresponding asso with that seq no as array
            $prep_sequence_arr = [];
            foreach($sequence_arr as $key => $value) {
                $prep_sequence_arr[$value][] = $key;
            }
            unset($sequence_arr);
            foreach($prep_sequence_arr as $seq_no => $asso_arr) {
                $statement = '';
                $arr_chunk = array_chunk($asso_arr, 1000); // change the chunk number
                foreach($arr_chunk as $chunk) {
                    $statement .= " UPDATE acmt_item_associations SET sequence_number =".$seq_no." where item_association_id in ('".implode("','",$chunk)."'); ";
                }
                DB::unprepared($statement);
                unset($statement);
            }
            unset($prep_sequence_arr);
        // }
    }

    public function checkSortType($data)
    {
        $row = (array) $data[0];
        if (isset($row['sequence_number']) && !empty(trim($row['sequence_number']))) {
            return 'sequence_number';
        } else {
            $sort_on = 'full_statement';
            #check if human_coding_scheme is not empty even for one of the nodes in one level
            foreach ($data as $key => $value) {
                if (isset($value['human_coding_scheme']) && trim($value['human_coding_scheme']) !== '') {
                    $sort_on = 'human_coding_scheme';
                }
            }

            return $sort_on;
            // if (isset($row['human_coding_scheme']) && trim($row['human_coding_scheme']) !== '') {
            //     return 'human_coding_scheme';
            // } else {
            //     return 'full_statement';
            // }
        }
    }

    public function arraySort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $final_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array((array)$v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }
            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }
            #sort if some have empty values
            $sort_arr = [];
            $sort_arr_1 = [];
            $sort_arr_2 = [];
            $sort_again_arr = [];
            foreach ($sortable_array as $k => $v) {
                if($on == 'human_coding_scheme' && $v == '') { # partial array values is empty in case of human_coding_scheme, then sort again with full_statement
                    $sort_arr_1[$k] = $v;
                } else {
                    $sort_arr_2[$k] = $v;
                }
                $sort_arr = $sort_arr_2;
            }

            # partial array values is empty in case of human_coding_scheme, then sort again with full_statement
            if(count($sort_arr_1)) {
                # generate the full array to pass to the sort function again
                foreach ($sort_arr_1 as $k => $v) {
                    $sort_again_arr[$k] = $array[$k];
                }
                # if human_coding_scheme is empty then, next criteria to sort on is full_statement
                $sort_arr_1 = $this->arraySort($sort_again_arr, 'full_statement');
            }
            foreach ($sort_arr as $k => $v) {
                $new_array[$k] = $array[$k];
            }
            $final_array = array_merge($sort_arr_1, $new_array); // do not change the order of merging of arrays
        }
        return $final_array;
    }

    public function assoMigrationSequenceNo($tax)
    {
        // $document_id_arr_chunk = array_chunk($tax, 10);
        # execute script for 10 documents/trees at a time
        # doing this so that when we make chunk of associations related assocaitions of one node dont go in another chunk
        // foreach ($document_id_arr_chunk as $chunk) {
            $item_child_asso = DB::select("select `item_association_id`, `sequence_number`, `association_type`, `source_item_id`, `created_at`
                                            from `acmt_item_associations`
                                            where `acmt_item_associations`.`is_deleted` = 0
                                            and `source_document_id` = '".$tax."'
                                            order by `acmt_item_associations`.`created_at` asc");
            // and `source_document_id` in ('".implode("','",$chunk)."')
            $grouped_asso_arr = [];
            foreach ($item_child_asso as $value) {
                $grouped_asso_arr[$value->source_item_id][] = [
                    'item_association_id' => $value->item_association_id,
                    'sequence_number' => (int)$value->sequence_number,
                    'association_type' => (int)$value->association_type,
                ];
            }
            unset($item_child_asso);
            $insert_arr = [];
            foreach ($grouped_asso_arr as $source_item_id => $group) {
                $counter_value = max(array_column($group, 'sequence_number')) + 1;
                foreach ($group as $value) {
                    if($value['association_type'] != 1 && $value['sequence_number'] == 0) {
                        $insert_arr[$value['item_association_id']] = $counter_value;
                        $counter_value++;
                    }
                }
            }
            unset($grouped_asso_arr);
            # prepare array such that sequence no is key and all the corresponding asso with that seq no as array
            $prep_sequence_arr = [];
            foreach($insert_arr as $key => $value) {
                $prep_sequence_arr[$value][] = $key;
            }
            unset($insert_arr);
            #insert into DB
            foreach($prep_sequence_arr as $seq_no => $asso_arr) {
                $statement = '';
                $arr_chunk = array_chunk($asso_arr, 1000); // change the chunk number
                foreach($arr_chunk as $chunk) {
                    $statement .= " UPDATE acmt_item_associations SET sequence_number =".$seq_no." where item_association_id in ('".implode("','",$chunk)."'); ";
                }
                DB::unprepared($statement);
                unset($statement);
            }
            unset($prep_sequence_arr);
        // }
    }
}
