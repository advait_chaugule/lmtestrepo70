<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Document\Jobs\ListPublishedDocumentRootJob;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use Log;

class SetDocumentListToCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $documents = $this->run(ListPublishedDocumentRootJob::class);
            //dd($documents);
            if(!empty($documents) && count($documents) > 0){

                $orgIdentifier = $request['auth_user']['organization_id'];
                $keyInfo = array('identifier'=>$orgIdentifier, 'prefix'=>'docs_');
                $status = $this->run(new SetToCacheJob($documents, $keyInfo));
                
                if(is_array($status) && $status['status']=="error"){
                    // handle failure, data already exists , delete and re-add all
                    $status = $this->run(new DeleteFromCacheJob($keyInfo));
                    $status = $this->run(new SetToCacheJob($documents, $keyInfo));
                }
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
            }
            else {
                $errorType = 'not_found';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Data not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
