<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\User\Jobs\CheckUserOrganizationJob;
use App\Domains\User\Jobs\UpdateUserInfoByOrganizationJob;
use Log;

class SetUserInfoByOrganizationFeature extends Feature
{
    use ErrorMessageHelper;
    public function handle(Request $request)
    {
        try {
            $requestData            =   $request->all();
            $userIdentifier         =   $request->route('user_id');
            $organizationIdentifier =   $requestData['auth_user']['organization_id'];
            $emailStatus            =   $requestData['enable_email'];
            $validateInput          =   ['user_id'  =>  $userIdentifier, 'organization_id'  => $organizationIdentifier];
            $validateUser           = $this->run(new CheckUserOrganizationJob($validateInput));
            if(!empty($validateUser[0]->user_id)) {
                $addUserInfoData    =   ['user_id'  => $userIdentifier, 'organization_id'   => $organizationIdentifier, 'user_info' => $emailStatus];
                $updateUserInfo     =   $this->run(new UpdateUserInfoByOrganizationJob($addUserInfoData));
                $successType = 'updated';
                $message = 'Email configuration set successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $updateUserInfo, $message, $_status));
            }            
        } catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
