<?php
namespace App\Services\Api\Features;
ini_set('max_execution_time', 0);
ini_set('memory_limit','256M');
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Services\Api\Traits\StringHelper;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Item\Jobs\GetChildrenNodesJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\MetaData\Jobs\GetCaseMetaDataJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\MetaData\Jobs\GetCustomMetaDataJob;
use App\Domains\MetaData\Jobs\GetAllowedMetaDataFieldsJob;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Domains\Metadata\Jobs\ValidateMutlipleItemDetailsJob;
use App\Domains\Helper\Jobs\CreateErrorMessageFromExceptionHelperJob;

class MultipleItemDetailsFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function __construct() {
        $this->items_arr = [];
    }

    public function handle(Request $request)
    {
        try {
            $input = $request->all();
            $organization_id = $input['auth_user']['organization_id'];
            $item_ids = $request->item_ids;
            $document_id = $request->document_id;
            $meta_data_field_names = $request->meta_data_field_names;
            $include_children = $request->include_children;
            
            // get item_id from source_document_id
            $item_ids = $this->getItemIdArrayFromSourceItemIdArray($item_ids,$organization_id);
            
            //store the request item ids in array
            $requestItemsIdArray = $item_ids;

            // get document_id from source_document_id
            $document_id = $this->getDocumentIdFromSourceDocumentId($document_id,$organization_id);
            
            // create the input data to validate
            $input = [
                        'document_id' => $document_id,
                        'item_ids' => $item_ids,
                        'meta_data_field_names' => $meta_data_field_names,
                        'include_children' => $include_children
                    ];
            $validationStatus = $this->run(new ValidateMutlipleItemDetailsJob($input));
            if($validationStatus === true) {
                if($request->include_children === true) {
                    #Get child items
                    $item_ids = $this->getAllChildItemsFromDocumentId($document_id, $item_ids);

                    #Merge array with child items
                    $item_ids = array_merge($requestItemsIdArray, $item_ids);
                }
                $item_ids = array_unique($item_ids);
                $input['item_ids'] = $item_ids;
                $input['meta_data_field_names'] = $request->meta_data_field_names;

                #mandatory fields
                $mandatory_fields = [
                    'node_type',
                    'parent_id'
                ];
                foreach ($mandatory_fields as $field) {
                    if(!in_array(strtolower($field), array_map('strtolower', $input['meta_data_field_names']))) {
                        array_push($input['meta_data_field_names'], $field);
                    }
                }

                #for adding metadata field names to be allowed in output
                $metadata_part_of_item_arr = $this->run(new GetAllowedMetaDataFieldsJob($input['item_ids'], $mandatory_fields));

                #Get case metadata field values from db query
                $data = $this->run(new GetCaseMetaDataJob($input));

                if(count($data['custom_meta_data_field_names'])) {
                    $data = $this->run(new GetCustomMetaDataJob($data));
                }

                $items_data = $data['items_data'];

                $filtered_output = [];
                $output_response = [];
                #filter output to contain only those metadata fields which are in the node/item
                foreach ($items_data as $item_id => $fields_arr) {
                    foreach ($fields_arr as $field_key => $field_value) {
                        if(in_array($field_key, $metadata_part_of_item_arr[$item_id])) {
                            $filtered_output[$item_id][$field_key] = $field_value;
                        }
                    }
                    $filtered_output[$item_id]['item_id'] = $item_id;

                    #if there is any other data other than item_id in array then append the data to final output
                    if(count($filtered_output[$item_id]) > 1){
                        $output_response[] = $filtered_output[$item_id];
                    }
                }
                /*** Create the success response ***/
                if(empty($output_response)) {
                    $successType = 'custom_not_found';
                    $message = 'Data not found';
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], true));
                } else {
                    $successType = 'custom_found';
                    $message = 'Data found';
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, $output_response, $message, $_status, [], true));
                }
            } else {
                $errorType = 'custom_validation_error';
                $message = $validationStatus;
                return $this->run(new RespondWithJsonErrorJob($errorType, $message));
            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->run(new CreateErrorMessageFromExceptionHelperJob($ex));
            $_status = 'custom_status_here';
            Log::error($message);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function getAllChildItemsFromDocumentId($document_id, $item_ids){
        // $items = DB::table('items')->select('item_id','parent_id')->where('is_deleted', 0)->where('document_id', $document_id)->get();

        $items = DB::table('item_associations')->selectRaw('source_item_id as item_id ,target_item_id as parent_id')->where('is_deleted', 0)->where('source_document_id', $document_id)->get();

        $this->items_arr = [];
        foreach($items as $item){
            $this->items_arr[$item->parent_id][] = $item->item_id;
        }
        $all_items = [];
        $this->getAllChildRecursive($item_ids, $all_items);

        return $all_items;
    }

    public function getAllChildRecursive($item_ids, &$all_items) {
        foreach ($item_ids as $item_id) {
            $new_child_items = isset($this->items_arr[$item_id]) ? $this->items_arr[$item_id] : [];
            if(!empty($new_child_items)) {
                foreach ($new_child_items as $item_id) {
                    $all_items[] = $item_id;
                }
                $this->getAllChildRecursive($new_child_items, $all_items);
            }
        }
    }

    public function getItemIdArrayFromSourceItemIdArray($sourceItemIdArray,$organization_id)
    {
        $items = DB::table('items')->select('item_id')->where('is_deleted', 0)->whereIn('source_item_id', $sourceItemIdArray)->where('organization_id',$organization_id)->get()->toArray();
        
        $itemIdArray = [];

        if(count($items))
        {
            foreach($items as $itemsK=>$itemsV)
            {
                $itemIdArray[] = $itemsV->item_id;
            }
        }
        else
        {
            $documentId = DB::table('documents')->select('document_id')->where('is_deleted', 0)->where('source_document_id', $sourceItemIdArray)->where('organization_id',$organization_id)->get()->toArray();

            if(count($documentId)>0)
            {
                $itemIdArray[] = $documentId[0]->document_id;
            }
            else
            {
                $itemIdArray = $sourceItemIdArray;
            }
            
        }
        
        return $itemIdArray;

    }

    public function getDocumentIdFromSourceDocumentId($sourceDocumentId,$organization_id)
    {
        $documentId = DB::table('documents')->select('document_id')->where('is_deleted', 0)->where('source_document_id', $sourceDocumentId)->where('organization_id',$organization_id)->get()->toArray();
        
        if(count($documentId)>0)
        {
            $documentID = $documentId[0]->document_id;
        }
        else
        {
            $documentID = $sourceDocumentId;
        }
        
        return $documentID;
    }

}
