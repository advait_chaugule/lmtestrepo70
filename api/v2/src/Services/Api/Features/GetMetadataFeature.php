<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Metadata\Jobs\ValidateMetadataByIdJob;
use App\Domains\Metadata\Jobs\CheckMetadataIsDeleteJob;
use App\Domains\Metadata\Jobs\GetMetadataDetailJob;

use Illuminate\Support\Facades\DB;
use Log;

class GetMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData    = $request->all();
            $metadataId     = $request->route('metadata_id');

            $input = ['metadata_id' => $metadataId, 'organization_id' => $requestData['auth_user']['organization_id']];
            $identifier = ['metadata_id' => $metadataId];
            
            // added new validation for field type 7 - ACMT-2856
            if(isset($requestData['internal_name'])){
                $field_values = $this->getfieldvalues($requestData['auth_user']['organization_id'],$requestData['internal_name']);
                $field_list = json_decode($field_values);
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $field_list, $message, $_status));
            }
            else{
            $validateMetadataStatus = $this->run(new ValidateMetadataByIdJob($identifier));

            if($validateMetadataStatus === true) {
                $checkMetadataDeletedStatus = $this->run(new CheckMetadataIsDeleteJob($metadataId));
                if(!$checkMetadataDeletedStatus === true){
                    $metadataDetail = $this->run(new GetMetadataDetailJob($input));
                   
                     // to giving response for new metadata field type (dictionary)
                    if($metadataDetail['field_type_id'] == 7){
                        $metadataResult = array();
                        $metadataResult['metadata_id'] = $metadataDetail['metadata_id'];
                        $metadataResult['name'] = $metadataDetail['name'];
                        $metadataResult['internal_name'] = $metadataDetail['internal_name'];
                        $metadataResult['field_type'] = $metadataDetail['field_type'];
                        $metadataResult['field_type_id'] = $metadataDetail['field_type_id'];
                        $metadataResult['field_possible_values'] = json_decode($metadataDetail['field_possible_values']);
                        $metadataResult['is_custom'] = $metadataDetail['is_custom'];
                        $metadataResult['is_active'] = $metadataDetail['is_active'];
                        $metadataResult['updated_at'] = $metadataDetail['updated_at'];
                        $metadataResult['updated_by'] = $metadataDetail['updated_by'];
                        $metadataDetail =  $metadataResult;
                    }
                    
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $metadataDetail, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Metadata is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid metadata.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
          }
        } catch (\Exception $ex) {
            dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
    
    private function getfieldvalues($orgnization_id,$internal_name){
    
        $query = DB::table('metadata')->select('field_possible_values')
        ->where('field_type','=',7)
        ->where('organization_id','=',$orgnization_id)
        ->where('internal_name','=',$internal_name)
        ->get()
        ->toArray();     
    
        return $query[0]->field_possible_values;
    }
}
