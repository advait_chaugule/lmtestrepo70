<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\UpdateGroupForTaxonomyJob;
use App\Domains\Taxonomy\Jobs\ValidateGroupByIdStatusJob;
use Illuminate\Support\Facades\Log;

class DeleteGroupForTaxonomyFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $groupId = (null!==$request->input('group_id')) ? trim($request->input('group_id')) : '';
            $status = (null!==$request->input('status')) ? trim($request->input('status')) : '1';
            $requestData = $request->all();
            $userId = $requestData['auth_user']['user_id'];
            $organizationId = $requestData['auth_user']['organization_id'];
            $grpDetails = ['group_id' => $groupId,'user_id' => $userId,'organization_id' => $organizationId,'status' => $status];
            $validateGrptatus = $this->run(new ValidateGroupByIdStatusJob($grpDetails)); //Check if group name exists
            //Save group name if it does not exists                
            if($validateGrptatus === true){
                $response = $this->run(new UpdateGroupForTaxonomyJob($grpDetails));
                $successType = 'custom_found';
                $rmessage = ['Group deleted successfully.'];
                return $this->run(new RespondWithJsonJob($successType, $response, $rmessage, '', [], true));
            }else{
                $errorType = 'custom_validation_error';
                return $this->run(new RespondWithJsonJob($errorType, null, $validateGrptatus, '', [], false));
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
