<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonFileJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonSpecificationJob;
use App\Domains\CaseStandard\Jobs\StoreExternalCaseJsonFileJob;
use App\Domains\CaseStandard\Jobs\ExtractCaseFrameworkDocumentFromUploadedJsonContentJob;
use App\Domains\CaseStandard\Jobs\GetDataMetricsFromUploadedCaseJSonContentJob;
use App\Domains\Document\Jobs\ValidateDocumentSourceIdJob;
use App\Domains\CaseStandard\Jobs\ImportCaseStandardDataFromJsonJob;

use App\Domains\CaseStandard\Jobs\CloneCasePackageDataJob;
use App\Domains\CaseStandard\Jobs\ImportCasePackageJob;
use Log;

class ImportCFPackadgeJsonFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait, FileHelper, ArrayHelper;

    public function handle(Request $request)
    {
        try{
            $requestUrl = url('/');
            // validate inputs for CASE json upload
            $validateCaseJsonFile = $this->run(new ValidateCaseStandardJsonFileJob($request));
            $requestingUserDetails = $request->input("auth_user");
            if($request->input('check')){
                $checkFlag = $request->input('check');
            }else{
                $checkFlag = 0;
            }
            if($checkFlag == 1){
               
            if($validateCaseJsonFile['status']!==false){
                // get uploaded json file
                $caseStandardFile = $validateCaseJsonFile['file'];
                // create a file identifier
                $fileIdentifier = $this->createUniversalUniqueIdentifier();
                // name of file to create
                $externalCaseFileName = $fileIdentifier.".json";
                // store the content of uploaded CASE json inside the file created above
                $externalCaseFile = $this->run(new StoreExternalCaseJsonFileJob($caseStandardFile, $externalCaseFileName));
                // extract the content of the duplicate CASE json file
                $fileContent = $this->getFileContent($externalCaseFile);
                $fileContentArr = $this->convertJsonStringToArray($fileContent);
                // verify if content is compliant with CASE specifications
                $validateCASESpecification = $this->run(new ValidateCaseStandardJsonSpecificationJob($fileContentArr));
                if(!empty($validateCASESpecification) && $validateCASESpecification["status"]===true){
                    // extract CFDocument object from content
                    $caseFrameWorkDocument = $this->run(new ExtractCaseFrameworkDocumentFromUploadedJsonContentJob($fileContent));
                    // set the CFDocument unique identifier
                    $caseFrameWorkDocumentIdentifier = trim($caseFrameWorkDocument->identifier);
                    $organizationId = $requestingUserDetails['organization_id'];

                    // check if imported package exists in our system
                    $packageExists = $this->run(new ValidateDocumentSourceIdJob($caseFrameWorkDocumentIdentifier, $organizationId));
                    // If package doesn't exist then import the package under the requesting organization
                    if($packageExists == 0 || $packageExists == 2) {
                       
                        $this->deleteFileFromLocalPath($externalCaseFile);

                        $successType = 'custom_found';
                        // please note client is using the message string to put conditions for clone popup ##so never change this
                        $message = "Please choose following option for upload";
                        $_status = 'custom_status_here';
                        if($packageExists == 0 )
                        {
                            $packageExists = 0;
                        }
                        else
                        {
                            $packageExists = 1;
                        }
                        $data = ['import_type' => [1,2,3],'package_exist' =>$packageExists];
                        
                        return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                    }
                    else if($packageExists == 1 || $packageExists == 3) 
                        {
                            $successType = 'custom_found';
                            // please note client is using the message string to put conditions for clone popup ##so never change this
                            $message = "This taxonomy already exist. Please choose following option for upload";
                            $_status = 'custom_status_here';
                            $data = ['import_type' => 2,'package_exist' => 1];
                            return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                     
                        }
                       
                }
                else {
                    $this->deleteFileFromLocalPath($externalCaseFile);
                    $errorType = 'validation_error';
                    $message = $validateCASESpecification['message'];
                    $_status = 'Invalid file.';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errorType = 'validation_error';
                $message = $validateCaseJsonFile['message']["case_json"]["file"];
                $_status = 'Invalid file.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));   
            }

            }
          
            if($request->input('import_type')){
                $importType = $request->input('import_type');
            }else{
                $importType = 0;
            }
            if($request->input('clone')){
                $copy = $request->input('clone');
            }else{
                $copy = 0;
            }
            if($copy == 0)
            {
                  // execute new import job
                  $caseStandardFile = $validateCaseJsonFile['file'];
                  // create a file identifier
                  $fileIdentifier = $this->createUniversalUniqueIdentifier();
                  // name of file to create
                  $externalCaseFileName = $fileIdentifier.".json";
                  // store the content of uploaded CASE json inside the file created above
                  $externalCaseFile = $this->run(new StoreExternalCaseJsonFileJob($caseStandardFile, $externalCaseFileName));
                  // extract the content of the duplicate CASE json file
                  $fileContent = $this->getFileContent($externalCaseFile);
                  $importAsClone = false;
                  $fileContentArr = $this->convertJsonStringToArray($fileContent);
                  $packageDataArray = [ "packageData" => $fileContentArr, "oldNewItemMapping" => [] ];
                  $requestingUserDetails['import_type'] = $importType;
                  $returnDocumentDetail = $this->run(new ImportCasePackageJob($packageDataArray, $requestingUserDetails, $importAsClone,'',$requestUrl));
            
                  // delete the temporary uploaded file
                  $this->deleteFileFromLocalPath($externalCaseFile);

                  // get the imported case metrics count
                  $uploadedPackageMetrics = $this->run(new GetDataMetricsFromUploadedCaseJSonContentJob($fileContentArr));
                  $uploadedPackageMetrics['document_id'] = $returnDocumentDetail["document_id"];
                  $uploadedPackageMetrics['document_title'] = $returnDocumentDetail["document_title"];
          
                  $successType = 'custom_found';
                  $message = 'Import Successfull.';
                  $_status = 'custom_status_here';
                  return $this->run(new RespondWithJsonJob($successType, $uploadedPackageMetrics, $message, $_status,[],true));
            } 
           else
           {
               $fileContentArr = $this->convertJsonStringToArray($fileContent);
               // first execute package cloning job
               $clonedCaseData = $this->run(new CloneCasePackageDataJob($fileContent));
               // then execute new import job
               $importAsClone = true;
               $requestingUserDetails['import_type'] = $importType;
               $returnDocumentDetail = $this->run(new ImportCasePackageJob($clonedCaseData, $requestingUserDetails, $importAsClone));
              
               // delete the temporary uploaded file
               $this->deleteFileFromLocalPath($externalCaseFile);
               // get the imported case metrics count
               $uploadedPackageMetrics = $this->run(new GetDataMetricsFromUploadedCaseJSonContentJob($fileContentArr));
               $uploadedPackageMetrics['document_id'] = $returnDocumentDetail["document_id"];
               $uploadedPackageMetrics['document_title'] = $returnDocumentDetail["document_title"];
               
               $successType = 'custom_found';
               $message = 'Import Successfull.';
               $_status = 'custom_status_here';
               return $this->run(new RespondWithJsonJob($successType, $uploadedPackageMetrics, $message, $_status,[],true));
           }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
    
}
