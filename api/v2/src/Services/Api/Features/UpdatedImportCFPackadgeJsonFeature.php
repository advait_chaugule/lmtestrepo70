<?php
namespace App\Services\Api\Features;

use Illuminate\Support\Facades\Log;
use Storage;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Services\Api\Traits\FileHelper;
//use Illuminate\Support\Facades\Storage;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Import\Events\ImportEvent;
use App\Domains\Import\Jobs\UploadFileS3Job;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;


use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Import\Jobs\CreateNotificationJob;

//use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\Import\Jobs\CreateImportProcessJob;

use App\Domains\Import\Jobs\UpdateImportProcessJob;
use App\Domains\Notifications\Jobs\ImportJobForEmailJob;
use App\Domains\Taxonomy\Jobs\ImportJsonWithNoChangeJob;
use App\Data\Repositories\Contracts\NotificationInterface;
use App\Domains\CaseStandard\Jobs\CloneCasePackageDataJob;

use App\Domains\Document\Jobs\ValidateDocumentSourceIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;

use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\CaseStandard\Jobs\StoreExternalCaseJsonFileJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonFileJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonSpecificationJob;
use App\Domains\CaseStandard\Jobs\GetDataMetricsFromUploadedCaseJSonContentJob;
use App\Domains\CaseStandard\Jobs\ExtractCaseFrameworkDocumentFromUploadedJsonContentJob;


class UpdatedImportCFPackadgeJsonFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait, FileHelper, ArrayHelper;

    public function handle(Request $request)
    {
        ini_set('memory_limit','8098M');
        ini_set('max_execution_time', 0);
        try{
            $requestingUserDetails  =   $request->input("auth_user");
            $organizationId         =   $requestingUserDetails['organization_id'];
            $requestImportType      =   $request->input('import_type');
            $isReadyToCommit        =   $request->input('is_ready_to_commit_changes');
            $sourceIdentifier       =   !empty($request->input('source_identifier')) ? $request->input('source_identifier') : "";
            $importRequestIdentifier =   !empty($request->input('import_identifier')) ? $request->input('import_identifier') : "";
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);
            $packageExists          =   0;
            $importStatus = ['2'];
            
             $importData = DB::table('import_job')->select('import_job_id')->where('source_document_id',$sourceIdentifier)->where('organization_id',$organizationId)->whereIn('status',$importStatus)->first();
                if($importData)
                {
                    $errorType = 'validation_error';
                    $message = "This taxonomy is already in process. Please select different file to import.";
                    $_status = 'Taxonomy Process already Started.';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
                }
            if(isset($isReadyToCommit) && $isReadyToCommit == 1) {
                // validate inputs for CASE json upload
                $validateCaseJsonFile = $this->run(new ValidateCaseStandardJsonFileJob($request));
                if($validateCaseJsonFile['status']!==false) {
                    // extract the size of CASE json file
                    $maxSize = config("event_activity")["Import_Size"]["JSON"];
                    # get file size from php instead of S3 bucket
                    $fileSize = $request->case_json->getSize();
                    # get taxonomy from file instead of S3
                    $fileContent = File::get($request->case_json);
                    // validate if content is json
                    $validJson = $this->validateJsonString($fileContent);
                    if($validJson===true){
                        $fileContentArr = $this->convertJsonStringToArray($fileContent);
                        unset($fileContent);
                        // verify if content is compliant with CASE specifications
                        $validateCASESpecification = $this->run(new ValidateCaseStandardJsonSpecificationJob($fileContentArr));
                        $uploadedPackageMetrics = $this->run(new GetDataMetricsFromUploadedCaseJSonContentJob($fileContentArr));
                    } else {
                        $validateCASESpecification = ["status" => false, "message" => "File content is not a CASE standard JSON."];
                    }

                    if(!empty($validateCASESpecification) && $validateCASESpecification["status"]===true) {
                        // get uploaded json file
                        $caseStandardFile           =   $validateCaseJsonFile['file'];
                        // create a file identifier
                        $fileIdentifier             =   $importRequestIdentifier;
                        // name of file to create
                        $externalCaseFileName       =   $fileIdentifier.".json";
                        // store the content of uploaded CASE json inside the file created above
                        $start_time_s3 = microtime(true);
                        $externalCaseFile                 =   $this->run(new UploadFileS3Job($caseStandardFile,$externalCaseFileName));
                        
                        // extract the content of the duplicate CASE json file

                        // extract CFDocument object from content
                        $caseFrameWorkDocument = (object)$fileContentArr["CFDocument"];
                        // set the CFDocument unique identifier
                        $caseFrameWorkDocumentIdentifier = trim($caseFrameWorkDocument->identifier);

                        //To set Forground and Background Process
                        if($fileSize > $maxSize)
                        {
                            // import events start here
                            $eventData = [
                                'caseFile'           => $externalCaseFile,
                                'sourceId'           => $sourceIdentifier,
                                'importType'         => $requestImportType,
                                'import_identifier'  => $importRequestIdentifier,
                                'domain_name'        => $serverName,
                                'filefullpath'       => $externalCaseFile,
                                'requestUserDetails' => $requestingUserDetails,
                                "organizationId"     => $organizationId,
                                "beforeEventRawData" => [],
                                'afterEventRawData'  => '',
                                'uploadedPackageMetrics' => $uploadedPackageMetrics,
                                'caseFrameWorkDocumentIdentifier' => $caseFrameWorkDocumentIdentifier
                            ];
                            # run queue work command the follwoing way: php artisan queue:work import_json_sqs --queue=acmt_dev_2
                            //dispatch(new ImportEvent($eventData))->onConnection('import_json_sqs'); // import events ends here
                            event(new ImportEvent($eventData));
                        }
                        else
                        {
                            // check if imported package exists in our system
                            $packageExists = $this->run(new ValidateDocumentSourceIdJob($caseFrameWorkDocumentIdentifier, $organizationId));

                            switch ($requestImportType)
                            {
                                case '1':
                                case '2':
                                case '3':
                                    $inputinupdateProcessInput['import_job_id']= $importRequestIdentifier;
                                    $inputinupdateProcessInput['results']= '';
                                    $inputinupdateProcessInput['filefullpath']= $externalCaseFile;
                                    $inputinupdateProcessInput['status']= 2;
                                    $inputinupdateProcessInput['updated_at']= now()->toDateTimeString();
                                    $this->run(new UpdateImportProcessJob($inputinupdateProcessInput));
                                    $packageDataArray = [ "packageData" => $fileContentArr, "oldNewItemMapping" => [] ];
                                    unset($fileContentArr);
                                    if($uploadedPackageMetrics['associations_count'] == 0 && $uploadedPackageMetrics['associations_group_count'] == 0)
                                    {
                                        $importFlag = 0;
                                    }
                                    else
                                    {
                                        $returnDocumentDetail = $this->run(new ImportJsonWithNoChangeJob($packageDataArray, $requestingUserDetails, $requestImportType, $packageExists,$requestUrl));
                                        $importFlag = 1;
                                    }
                                    unset($packageDataArray);
                                    break;
                                default:
                                    $repository = false;
                                    break;
                            }
                            /*
                            if($importFlag == 0)
                            {
                                $uploadedPackageMetrics['document_id'] = "";
                                $uploadedPackageMetrics['document_title'] = $uploadedPackageMetrics['source_taxonomy_name'];
                                $uploadedPackageMetrics['import_status'] = "failed";
                                $taxonomyName = $uploadedPackageMetrics['source_taxonomy_name'];
                                // Create Notification for Taxonomy Imported
                                $email_body = "Could not upload the taxonomy {$taxonomyName} as the JSON did not include associtions for the taxnonomy";
                                $user_subject = "The import process for the Taxonomy {$taxonomyName} is failed.";
                            }
                            else
                            {
                                $uploadedPackageMetrics['document_id'] = $returnDocumentDetail["document_id"];
                                $uploadedPackageMetrics['document_title'] = $returnDocumentDetail["document_title"];
                                $uploadedPackageMetrics['import_status'] = "successful";
                                $taxonomyName = $returnDocumentDetail["document_title"];
                                // Create Notification for Taxonomy Imported
                                $email_body = "The import process for the Taxonomy {$taxonomyName} is complete.";
                                $user_subject = "The import process for the Taxonomy {$taxonomyName} is complete.";
                            }

                            $owner_id     = $requestingUserDetails['user_id'];
                            $notificationId = $this->createUniversalUniqueIdentifier();
                            $notificationMsg = $email_body;
                            $targetContext         = ['document_id'=>$uploadedPackageMetrics['document_id'],'user_id'=>$owner_id];
                            $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                            $notificationCategory  = config('event_activity')["Notification_category"]["IMPORT_JSON"];
                            $getUserDetails = $this->getUserDetails($owner_id);
                            $user_email = $getUserDetails[0]->email;
                            $firstName = $getUserDetails[0]->first_name;
                            $lastName = $getUserDetails[0]->last_name;
                            $currentOrgId= $getUserDetails[0]->current_organization_id;
                            $userName = $firstName . ' ' . $lastName;
                            $userOrganizationId = $this->run(new GetUserOrganizationIdWithUserJob($owner_id,$organizationId));
                            $organizationData = $this->run(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                            $organizationName = $organizationData['organization_name'];
                            $organizationCode = $organizationData['organization_code'];
                            $emailData1 = [
                                'subject'=>$user_subject,
                                'email'    => $user_email,
                                'body'     => ['email_body'=>$email_body,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'document_id'=>$uploadedPackageMetrics['document_id']],
                                'domain_name'=>$serverName
                            ];
                            $activityLog = [
                            "notification_id"       => $notificationId,
                            "description"           => $notificationMsg,
                            "target_type"           => "15",
                            "target_id"             => $uploadedPackageMetrics['document_id'],
                            "user_id"               => $owner_id,
                            "organization_id"       => $organizationId,
                            "is_deleted"            => "",
                            "created_by"            => $owner_id, // logged in user Id
                            "updated_by"            => $owner_id,
                            "target_context"        => $targetContextJson,
                            "read_status"           => "0", //0=unread 1= read
                            "notification_category" => $notificationCategory
                            ];

                            $this->run(new CreateNotificationJob($activityLog));
                            unset($activityLog);
                            unset($targetContext);
                            unset($targetContextJson);
                            unset($notificationMsg);
                            $checkEmailStatus = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $owner_id, 'organization_id' => $organizationId]));
                            $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                            if ($emailSettingStatus == 1) {
                                $this->run(new ImportJobForEmailJob($emailData1));
                                unset($emailData1);
                            }
                            */
                            $successType = 'custom_found';
                            $message = 'Import Successfull.';
                            $_status = 'custom_status_here';
                            $this->deleteFileFromLocalPath($externalCaseFile);
                            if($importFlag == 0)
                            {
                                $result = $this->run(new RespondWithJsonErrorJob('validation_error', 'Import Failed','Invalid file .'));
                                $inputupdateProcessInput['status']= 4;
                            }
                            else
                            {
                                $result = $this->run(new RespondWithJsonJob($successType, $uploadedPackageMetrics, $message, $_status,[],true));
                                $inputupdateProcessInput['status']= 3;
                            }

                            $inputupdateProcessInput['import_job_id']= $importRequestIdentifier;
                            $inputupdateProcessInput['results']= json_encode($uploadedPackageMetrics);
                            $inputupdateProcessInput['updated_at']= now()->toDateTimeString();
                            $this->run(new UpdateImportProcessJob($inputupdateProcessInput));
                            return $result;
                        }
                    }
                    else
                    {
                        // $this->deleteFileFromLocalPath($externalCaseFile);
                        $errorType = 'validation_error';
                        $message = $validateCASESpecification['message'];
                        $_status = 'Invalid file.';

                        // Create Notification for Taxonomy Imported
                        $taxonomyName1 = $uploadedPackageMetrics['source_taxonomy_name'];
                        $email_body1 = "The import process for the Taxonomy {$taxonomyName1} is failed.";
                        $user_subject1 = "The import process for the Taxonomy {$taxonomyName1} is failed.";
                        $owner_id1     = $requestingUserDetails['user_id'];
                        $notificationId1 = $this->createUniversalUniqueIdentifier();
                        $notificationMsg1 = $email_body1;
                        $targetContext1         = ['document_id'=>'','user_id'=>$owner_id1];
                        $targetContextJson1     = json_encode($targetContext1,JSON_UNESCAPED_SLASHES);
                        $notificationCategory1  = config('event_activity')["Notification_category"]["IMPORT_JSON"];
                        $getUserDetails1 = $this->getUserDetails($owner_id1);
                        $user_email1 = $getUserDetails1[0]->email;
                        $firstName1 = $getUserDetails1[0]->first_name;
                        $lastName1 = $getUserDetails1[0]->last_name;
                        $currentOrgId1= $getUserDetails1[0]->current_organization_id;
                        $userName1 = $firstName1 . ' ' . $lastName1;
                        $userOrganizationId1 = $this->run(new GetUserOrganizationIdWithUserJob($owner_id1,$organizationId));
                        $organizationData1 = $this->run(new GetOrgNameByOrgIdJob($userOrganizationId1['organization_id']));
                        $organizationName1 = $organizationData1['organization_name'];
                        $organizationCode1 = $organizationData1['organization_code'];
                        $emailData2 = [
                            'subject'=>$user_subject1,
                            'email'    => $user_email1,
                            'body'     => ['email_body'=>$email_body1,'user_name'=>$userName1,'organization_name'=>$organizationName1,'organization_code'=>$organizationCode1,'document_id'=>''],
                            'domain_name'=>$serverName
                        ];
                        $activityLog1 = [
                        "notification_id"       => $notificationId1,
                        "description"           => $notificationMsg1,
                        "target_type"           => "15",
                        "target_id"             => '',
                        "user_id"               => $owner_id1,
                        "organization_id"       => $organizationId,
                        "is_deleted"            => "",
                        "created_by"            => $owner_id1, // logged in user Id
                        "updated_by"            => $owner_id1,
                        "target_context"        => $targetContextJson1,
                        "read_status"           => "0", //0=unread 1= read
                        "notification_category" => $notificationCategory1
                        ];

                        $this->run(new CreateNotificationJob($activityLog1));
                        unset($activityLog1);
                        unset($targetContext1);
                        unset($targetContextJson1);
                        unset($notificationMsg1);
                        $checkEmailStatus1 = $this->run(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $owner_id1, 'organization_id' => $organizationId]));
                        $emailSettingStatus1 = $checkEmailStatus1[0]['email_setting'];
                        if ($emailSettingStatus1 == 1) {
                            $this->run(new ImportJobForEmailJob($emailData2));
                            unset($emailData2);
                        }
                        $failedResult = $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                        $inputupdateProcessInput['import_job_id']= $importRequestIdentifier;
                        $inputupdateProcessInput['errors']= json_encode($failedResult);
                        $inputupdateProcessInput['status']= 4;
                        $inputupdateProcessInput['updated_at']= now()->toDateTimeString();
                        $this->run(new UpdateImportProcessJob($inputupdateProcessInput));
                        return $failedResult;
                    }
                }
                else 
                {
                    $errorType = 'validation_error';
                    $message = $validateCaseJsonFile['message']["case_json"]["file"];
                    $_status = 'Invalid file.';
                    $fileError = $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    $inputErrorProcessInput['import_job_id']= $importRequestIdentifier;
                    $inputErrorProcessInput['results']= '';
                    $inputErrorProcessInput['filefullpath']= '';
                    $inputErrorProcessInput['errors']= json_encode($message);
                    $inputErrorProcessInput['user_id']= $requestingUserDetails['user_id'];
                    $inputErrorProcessInput['organization_id']= $organizationId;
                    $inputErrorProcessInput['source_document_id']= $sourceIdentifier;
                    $inputErrorProcessInput['process_type']= 1;
                    $inputErrorProcessInput['status']= 4;
                    $inputErrorProcessInput['created_at']= now()->toDateTimeString();
                    $this->run(new UpdateImportProcessJob($inputErrorProcessInput));

                    return $fileError;
                }
            }
            else
            {
                $packageExists = $this->run(new ValidateDocumentSourceIdJob($sourceIdentifier, $organizationId));
                $fileIdentifier             =   $this->createUniversalUniqueIdentifier();
                if($packageExists == true) {
                    $inputProcessInput['import_job_id']= $fileIdentifier;
                    $inputProcessInput['results']= '';
                    $inputProcessInput['errors']= '';
                    $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                    $inputProcessInput['organization_id']= $organizationId;
                    $inputProcessInput['source_document_id']= $sourceIdentifier;
                    $inputProcessInput['process_type']= 1;
                    $inputProcessInput['status']= 1;
                    $inputProcessInput['created_at']= now()->toDateTimeString();
                    $this->run(new CreateImportProcessJob($inputProcessInput));
                    $successType = 'custom_found';
                    // please note client is using the message string to put conditions for clone popup ##so never change this
                    $message = "This taxonomy already exists. Please choose following option for upload";
                    $_status = 'custom_status_here';
                    $data = ['import_type' => [2],'package_exist' => 1,'import_identifier' => $fileIdentifier];
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                } else {
                    $inputProcessInput['import_job_id']= $fileIdentifier;
                    $inputProcessInput['results']= '';
                    $inputProcessInput['errors']= '';
                    $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                    $inputProcessInput['organization_id']= $organizationId;
                    $inputProcessInput['source_document_id']= $sourceIdentifier;
                    $inputProcessInput['process_type']= 1;
                    $inputProcessInput['status']= 1;
                    $inputProcessInput['created_at']= now()->toDateTimeString();
                    $this->run(new CreateImportProcessJob($inputProcessInput));
                    $successType = 'custom_found';
                    // please note client is using the message string to put conditions for clone popup ##so never change this
                    $message = "Please choose following option for upload";
                    $_status = 'custom_status_here';
                    $data = ['import_type' => [1,2,3],'package_exist' =>$packageExists,'import_identifier' => $fileIdentifier];
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                }
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function getUserDetails($userId)
    {
        $query = $user = DB::table('users')->where('user_id',$userId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}