<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Taxonomy\Jobs\SearchTaxonomyJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use Log;

class SearchTaxonomyFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $searchQuery = trim($request->query('query'));
            if(!empty($searchQuery)) {
                $searchQueryWithWildCard = "$searchQuery*";
                $requestUserOrganiationId = $request->input('auth_user.organization_id');
                $searchResults = $this->run(new SearchTaxonomyJob($searchQueryWithWildCard, $requestUserOrganiationId));
                $successType = 'found';
                $_status = 'custom_status_here';
                $httpResponseMessage = !empty($searchResults) ? 'Data Found.' : 'Data Not Found.';
                return $this->run(new RespondWithJsonJob($successType, $searchResults, $httpResponseMessage, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = "Search query can't be empty";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }

    }
}
