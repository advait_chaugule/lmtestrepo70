<?php
namespace App\Services\Api\Features;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Project\Jobs\ValidateProjectIdJob;
use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\User\Jobs\SaveUserSettings;
use Log;
use App\Domains\User\Jobs\SetTaxonomyConfigSettingsJob;
use App\Domains\User\Jobs\SaveProjectSettingsJob;
class SetSystemSettingFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData    = $request->all();
            $settingId      = $requestData['setting_id'];
            $userId         = $requestData['auth_user']['user_id'];
            $organizationId = $requestData['auth_user']['organization_id'];
            $documentId     = isset($requestData['document_id'])?$requestData['document_id']:"";
            $featureId      = isset($requestData['feature_id'])?$requestData['feature_id']:'';
            $jsonData       = isset($requestData['json_data']) ?$requestData['json_data']:'';
            $tableName      = isset($requestData['table_name']) ?$requestData['table_name']:'';

            $jsonConfigValue = isset($requestData['json_config_value'])?$requestData['json_config_value']:"";
            $projectId = isset($requestData['project_id'])?$requestData['project_id']:"";
            switch ($featureId)
            {
                case 1: // List view of taxonomies
                    $response = $this->run(new SetTaxonomyConfigSettingsJob($userId,$organizationId,$settingId,$jsonConfigValue,$featureId));
                    if($response){
                        $resultData = $response;
                        $successType = 'custom_created';
                        $message = 'Saved successfully.';
                        $_status = 'custom_status_here';
                        $is_success = 'true';
                        return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status,[],$is_success));
                    }
                    break;
                case 2: // List view of projects
                    break;
                case 3: // list view of a individual taxonomy
                    $documentValidation = $this->run(new ValidateDocumentByIdJob(['document_id'=>$documentId]));
                    if($documentValidation===true){
                        $response    = $this->run(new SaveUserSettings($userId,$organizationId,$documentId,$jsonData,$tableName));
                        $successType = 'custom_created';
                        $message     = 'Saved successfully.';
                        $_status     = 'custom_status_here';
                        $is_success = 'true';
                        return $this->run(new RespondWithJsonJob($successType,[], $message, $_status,[],$is_success));
                    }else{
                        $errorType = 'validation_error';
                        $message   = 'Invalid document Id';
                        $_status   = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                    break;
                case 4: // list view of individual a project
                    $projectValidation = $this->run(new ValidateProjectIdJob(['project_id'=>$projectId]));
                    if($projectValidation===true) {
                        $response    = $this->run(new SaveProjectSettingsJob($userId,$organizationId,$projectId,$jsonData,$tableName));
                        $successType = 'custom_created';
                        $message     = 'Saved successfully.';
                        $_status     = 'custom_status_here';
                        $is_success = 'true';
                        return $this->run(new RespondWithJsonJob($successType,[], $message, $_status,[],$is_success));
                    }else{
                        $errorType = 'validation_error';
                        $message   = 'Invalid document Id';
                        $_status   = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }

                    break;
                default:
                    break;
            }
        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}