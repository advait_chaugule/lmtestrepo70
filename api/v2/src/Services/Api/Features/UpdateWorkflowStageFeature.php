<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\WorkflowStage\Jobs\ValidateUpdateWorkflowStageInputJob;
use App\Domains\WorkflowStage\Jobs\CheckWorkflowRoleMappingJob;
use App\Domains\WorkflowStage\Jobs\UpdateWorkflowStageJob;
use Log;

class UpdateWorkflowStageFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            
            $validateInputs = $this->run(new ValidateUpdateWorkflowStageInputJob($requestData));
            
            if($validateInputs===true){
                if(isset($requestData['role_id'])) {
                    $checkWorkflowRoleMappingIsExists = $this->run(new CheckWorkflowRoleMappingJob($requestData));
                    if($checkWorkflowRoleMappingIsExists === null){
                        $workflowStage = $this->run(new UpdateWorkflowStageJob($requestData));
                    
                        $response = $workflowStage;
                        $successType = 'updated';
                        $message = 'Data updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else{
                        $errorType = 'validation_error';
                        $message = 'Role mapping exists';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                    }
                }   else    {
                    $workflowStage = $this->run(new UpdateWorkflowStageJob($requestData));
                    
                    $response = $workflowStage;
                    $successType = 'updated';
                    $message = 'Data updated successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
            }
            else {
                $errors = $validateInputs;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
           
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
