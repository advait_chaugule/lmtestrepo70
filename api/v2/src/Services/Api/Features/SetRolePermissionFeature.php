<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\StringHelper;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ValidateRoleByIdJob;
use App\Domains\Role\Jobs\CheckRoleIsDeleteJob;
use App\Domains\Role\Jobs\RolePermissionDeleteJob;
use App\Domains\Role\Jobs\RolePermissionSetJob;
use Log;

class SetRolePermissionFeature extends Feature
{
    use StringHelper;

    public function handle(Request $request)
    {
        try{
            $roleId = $request->route('role_id');
            $identifier = ['role_id' => $roleId];
            $permissionSet = $request->input('permission_set');
            //Validate Role Existence
            $validateRole = $this->run(new ValidateRoleByIdJob($identifier));
            if($validateRole === true){
                $checkRoleIsDeleted = $this->run(new CheckRoleIsDeleteJob($roleId));
                if($checkRoleIsDeleted === false) {
                    //Convert permission string to array
                    $permissionArray = $this->delimittedStringToArray($permissionSet);
                    
                    $this->run(new RolePermissionDeleteJob($roleId));

                    $permissionsSaved = $this->run(new RolePermissionSetJob($identifier, $permissionArray));
                    $resultData = $permissionsSaved;
                    $successType = 'created';
                    $message = 'Role created successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Role is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid role.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
