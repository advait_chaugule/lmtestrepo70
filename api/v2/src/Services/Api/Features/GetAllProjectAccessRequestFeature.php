<?php
namespace App\Services\API\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Jobs\ProjectAccessRequestDetailsJob;
use Log;

class GetAllProjectAccessRequestFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            
            $requestData = $request->all();
            $projectAccessRequestId = $request->route('project_access_request_id');
            
            $organizationId = $requestData['auth_user']['organization_id'];
             $input = ['project_access_request_id' => $projectAccessRequestId,
                        'organization_id' => $organizationId];

            $ProjectAccessRequestList = $this->run(new ProjectAccessRequestDetailsJob($input));
            if(count($ProjectAccessRequestList) == 0)
            {
                $successType = 'empty';
                $message = 'Data not found';
                $_status = 'custom_status_here';
                $data = ['result' => 'empty'];
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }else
            {
                $successType = 'found';
                $message = 'Data found';
                $_status = 'custom_status_here';
               // $data = ['result' => 'empty'];
                return $this->run(new RespondWithJsonJob($successType, $ProjectAccessRequestList, $message, $_status));
        

            }
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
