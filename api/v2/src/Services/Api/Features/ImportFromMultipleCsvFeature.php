<?php
namespace App\Services\Api\Features;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Maatwebsite\Excel\Facades\Excel;
use App\Services\Api\Traits\FileHelper;
use Illuminate\Support\Facades\Storage;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Import\Events\ImportEvent;
use App\Domains\Caching\Events\CachingEvent;

use App\Domains\Csv\Jobs\ValidateCsvFileJob;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Metadata\Jobs\ListMetadataJob;
use App\Domains\Import\Jobs\UploadCSVFileS3Job;

use App\Domains\Import\Jobs\ValidateBatchIdJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Import\Jobs\UploadJsonFileS3Job;
use App\Domains\Import\Jobs\ImportMultipleCsvJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\Csv\Jobs\CheckTaxonomyPublishedJob;
use App\Domains\Import\Jobs\CreateImportProcessJob;
use App\Domains\Import\Jobs\UpdateImportProcessJob;
use App\Domains\Import\Events\ImportMultipleCsvEvent;
use App\Domains\Import\Events\SummaryMultipleCsvEvent;
use App\Domains\Csv\Jobs\ValidateImportPass2MultipleCsv;
use App\Domains\Document\Jobs\ValidateDocumentSourceIdJob;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use Illuminate\Support\Facades\Mail;
use App\Domains\Csv\Jobs\SummaryMultipleCsvJob;
use App\Domains\Csv\Jobs\SummaryMultipleCsvCopyJob;
use App\Domains\Import\CustomClass\CommonEmail;

class ImportFromMultipleCsvFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait, FileHelper, ArrayHelper;
    public function __construct()
    {
        $this->commonEmail = new CommonEmail();
    }

    public function handle(Request $request, MetadataRepositoryInterface $metadataRepository)
    {
        ini_set('memory_limit','8098M');
        ini_set('max_execution_time', 0);
        ob_start();
        try{
            $requestData        =   $request->all();
            $this->metadataRepository = $metadataRepository;
            $requestingUserDetails      =   $request->input("auth_user");
            $organizationId             =   $requestingUserDetails['organization_id'];
            $requestImportType          =   $request->input('import_type');
            $importPass                 =   $request->input('import_pass');
            $sourceIdentifier           =   !empty($request->input('source_identifier')) ? $request->input('source_identifier') : "";
            $importRequestIdentifier    =   !empty($request->input('import_identifier')) ? $request->input('import_identifier') : "";
            $batchId                   =    !empty($request->input('batch_id')) ? $request->input('batch_id') : "";
            $requestUrl                 =   url('/');
            $serverName                 =   str_replace('server', '', $requestUrl);
            $packageExists              =   !empty($request->input('package_exists')) ? $request->input('package_exists') : "";
           if($sourceIdentifier != '')
           {
                $importData = DB::table('import_job')->select('import_job_id')->where('source_document_id',$sourceIdentifier)->where('organization_id',$organizationId)->where('status','2')->first();
                if($importData)
                {
                    $errorType = 'validation_error';
                    $message = "This taxonomy is already in process. Please select different file to import.";
                    $_status = 'Taxonomy Process already Started.';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
                }
           }
            /**
             * This code is commented for discussion on 6-4-2020
             *  Validation and get import type received in one pass
             */
            /*if($importPass == 1)
            {
                if($sourceIdentifier == '')
                {
                    $successType = 'custom_validation_error';
                    // please note client is using the message string to put conditions for clone popup ##so never change this
                    $message = ['Please enter source identifier'];
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
                else 
                {
                         $packageExists = $this->run(new ValidateDocumentSourceIdJob(trim($sourceIdentifier), $organizationId));
                        if($packageExists == true) 
                        {
                            $successType = 'custom_found';
                            // please note client is using the message string to put conditions for clone popup ##so never change this
                            $message = ['Please choose following option for upload'];
                            $_status = 'custom_status_here';

                            $data = ['import_type' => [4,2],'source_identifier'=> trim($sourceIdentifier),'package_exist' =>$packageExists];
                            return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                        }
                        else 
                        {
                                $successType = 'custom_found';
                                // please note client is using the message string to put conditions for clone popup ##so never change this
                                $message = ['Please choose following option for upload'];
                                $_status = 'custom_status_here';

                                $data = ['import_type' => [1],'source_identifier'=> trim($sourceIdentifier),'package_exist' =>$packageExists];
                                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                        
                        }

                }
            }
            else if($importPass == 2)
            */
            if($importPass == 2)
            {
                if(isset($request['csvFile']) && !empty($request['csvFile'])) {
                    $validateCsvFile = $this->run(new ValidateCsvFileJob($request));
                    if($validateCsvFile['status']!==false){
                        $validateOtherParams = $this->run(new ValidateImportPass2MultipleCsv($requestData));
                        if($validateOtherParams !== true) {
                            $errorType = 'custom_validation_error';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($errorType, [], $validateOtherParams, $_status, [], false));
                        }
                            // get uploaded csv file
                        $csvFile = $validateCsvFile['file'];
                        // create a file identifier
                        $fileIdentifier = $this->createUniversalUniqueIdentifier();
                        // name of file to create
                        $externalCsvFileName = $fileIdentifier.".csv";
                        //$externalJsonFileName = $fileIdentifier.".json";
                        // store the content of uploaded Csv inside the file created above
                        $externalCsvFile                 =   $this->run(new UploadCSVFileS3Job($csvFile,$externalCsvFileName));
                        $csvHeaderAndRowToArray = $this->csvToArray($externalCsvFile);
                        //$externalJsonFile = $this->run(new UploadJsonFileS3Job(json_encode($csvHeaderAndRowToArray),$externalJsonFileName));

                        $csvHeaderAndRowToArray = $this->trim_array_spaces($csvHeaderAndRowToArray);
                        $updateCSVColumn = $csvHeaderAndRowToArray['column'];
    
                        $csvContentToArray  =  $this->removeBlankArray($csvHeaderAndRowToArray['row']);
                        $updateCsvContentToArray  =  $csvHeaderAndRowToArray['row'];
                        $csvContentToArray = $this->array_change_key_case_recursive($csvContentToArray,CASE_LOWER);
                        $csvHeaderAndRowToArray['column'] = array_map('strtolower',$csvHeaderAndRowToArray['column']);
                        $errorMessage    =   [];
                        $searchActionArray  = ['a','u','d','add','update','delete',''];
                        $valuesOfAction     = array_column($csvContentToArray, 'action');

                        $csvContentToArray  =  $this->removeBlankArray($csvHeaderAndRowToArray['row']);
                        $updateCsvContentToArray  =  $csvHeaderAndRowToArray['row'];
                        $csvContentToArray = $this->array_change_key_case_recursive($csvContentToArray,CASE_LOWER);
                        $csvHeaderAndRowToArray['column'] = array_map('strtolower',$csvHeaderAndRowToArray['column']);
                        $errorMessage    =   [];
                        $searchActionArray  = ['a','u','d','add','update','delete',''];
                        $valuesOfAction     = array_column($csvContentToArray, 'action');
                            
                        foreach($valuesOfAction as $key => $action) {
                            if(!in_array(strtolower($action), array_map('strtolower', $searchActionArray))) {                           
                                $errorMessage[] = $action." as Action at row ".($key+2);
                            }
                        }

                        $errorMessageForAssociation    =   [];
                        $searchAssociationArray = array('ischildof','ispeerof','ispartof','exactmatchof','precedes','isrelatedto','replacedby','hasskilllevel');
                        $valuesOfAssociation     =   array_column($csvContentToArray, 'case_association_type');
                        foreach($valuesOfAssociation as $key => $association) {
                            if($association !== '' && !in_array(strtolower($association), $searchAssociationArray)) {
                                $errorMessageForAssociation[] = $association." as Association at row ".($key+2);
                            }
                        }

                        if(!empty($errorMessageForAssociation)) {
                            $errorMessageForAssociation = implode(' , ',$errorMessageForAssociation);
                            $errorMessageForAssociation .= " are not accepted. Please use ".implode(', ',$searchAssociationArray)." for associations respectively.";
                        }
                        else
                        {
                            $errorMessageForAssociation = '';
                        }


                        if(!empty($errorMessage)) {
                            $errorMessage = implode(' , ',$errorMessage);
                            $errorMessage .= " are not accepted. Please use A or Add, U or Update, D or Delete for Add, Update or Delete actions respectively.";
                        }
                        else{
                            $errorMessage = '';
                        }
                        if(count($csvContentToArray) && $requestImportType == 4) {
                            $sourceDocumentId = $csvContentToArray[0]['node_id'];
                            $CheckTaxonomyPublished = $this->run( new CheckTaxonomyPublishedJob($sourceDocumentId));
                            if($CheckTaxonomyPublished == true){
                                $errorType = 'custom_validation_error';
                                $message = ['ACMT cannot proceed with this request as the taxonomy you are trying to update is published'];
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonErrorJob($errorType,[], $message, $_status,[], false));
                            }
                        }
                        if(count($csvContentToArray) == 0 || !$this->is_multi_array($csvContentToArray)) {
                            $errorType = 'custom_validation_error';
                            $message = ['This file does not follow the CSV/delimited text format required by ACMT. Kindly download the sample csv to view the accepted format'];
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));  
                        }
                        else 
                        {
                            // Condition added to check if document id passed matches with document id in CSV
                            if($request->has('document_id')){
                                $documentIdToBeUpdated = (null!==$request->input('document_id')) ? $request->input('document_id') : '';
                                if(empty($documentIdToBeUpdated) || !isset($csvContentToArray[0]['node_id']) || trim($documentIdToBeUpdated)!=trim($csvContentToArray[0]['node_id'])){
                                    $errorType = 'custom_validation_error';
                                    $message = ["The CSV uploaded does not match the selected taxonomy. Please import the correct file"];
                                    $_status = 'Invalid file.';
                                    return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false)); 
                                }
                            }
                            // Condition added to acheck mandate columns are present even if case insensitive
                            $arrayMandateColumns = ['node_id','parent/destination_node_id','case_title','case_full_statement'];
                            if(count(array_intersect($arrayMandateColumns, array_map('strtolower', $csvHeaderAndRowToArray['column']))) != count($arrayMandateColumns)){
                                $errorType = 'custom_validation_error';
                                $message = ['node_id,parent/destination_node_id,case_title,case_full_statement columns are mandatory. Kindly download the sample csv to view the accepted format'];
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));  
                            }
                            if(count(array_intersect(['node_id','parent/destination_node_id'], $updateCSVColumn)) != count(['node_id','parent/destination_node_id'])){
                                $errorType = 'custom_validation_error';
                                $message = ['node_id,parent/destination_node_id should be in lowercase. Kindly download the sample csv to view the accepted format'];
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));  
                            }
                            if(!in_array('case_node_type', array_map('strtolower', $csvHeaderAndRowToArray['column'])) || !in_array('Document',array_column($csvContentToArray, 'case_node_type'))){
                                $errorType = 'custom_validation_error';
                                $message = ['The csv imported does not contain the document node. Please import the csv with document node.'];
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));  
                            }
                            if(isset($csvContentToArray[0]['action']) && (strtolower($csvContentToArray[0]['action']) == 'd' || strtolower($csvContentToArray[0]['action']) == 'delete')) {
                                $errorType = 'custom_validation_error';
                                $message = ['Could not delete the document type node'];
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));
                            }
                            if(!isset($csvContentToArray[0]['node_id']) || $csvContentToArray[0]['node_id'] == '') {
                                $errorType = 'custom_validation_error';
                                $message = ['The document identifier is blank.'];
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));
                            }
                            if(!empty($errorMessage) || !empty($errorMessageForAssociation)) {  
                                $message = [$errorMessage ." ". $errorMessageForAssociation];
                                $errorType = 'custom_validation_error';
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));
                            } 
                            else
                            {
                                
                                $packageExists = $this->run(new ValidateDocumentSourceIdJob(trim($csvHeaderAndRowToArray['row'][0]['node_id']), $organizationId));
                                
                                $metadataListForATenant = $this->run(new ListMetadataJob($request));
                                if($packageExists == 1) {

                                     $returnSummaryDetail = $this->run(new SummaryMultipleCsvJob($csvContentToArray, $organizationId, $requestingUserDetails,  2,  $csvHeaderAndRowToArray['column'], $metadataListForATenant, $packageExists, $request,'',true));
                                }
                                else
                                {
                                    $returnSummaryDetail = $this->run(new SummaryMultipleCsvCopyJob($csvContentToArray, $organizationId, $requestingUserDetails,  2,  $csvHeaderAndRowToArray['column'], $metadataListForATenant, $packageExists, $request,true));
                                }
                                $blockerErrorMessage = "";
                                if($returnSummaryDetail == false) {
                                    $blockerErrorMessage = 'The file contains mandatory field value blank or similar name and type for custom metadata. Please refer to the sample CSV and update the file and import again';
                                } else {
                                    //$success = false;
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    $response = explode('|mUy7V3e|', $returnSummaryDetail);
                                    if($response[0] == '2') {
                                        $blockerErrorMessage = 'The file contains : '.implode(',', json_decode($response[1], true)).' as non case metadata prefixed with case_/Case_. Please refer to sample CSV file';
                                    } else if($response[0] == '3') {
                                        $blockerErrorMessage = 'You have uploaded an older supported format of the CSV. Please update as per the current format';
                                    } else if($response[0] == '4') {
                                        $blockerErrorMessage = 'Could not find the parent id for nodes with the following parent id : '.implode(',', json_decode($response[1], true));
                                    } else if($response[0] == '5'){
                                        $blockerErrorMessage = 'You have duplicate node ids at row : '.implode(',', json_decode($response[1], true));
                                    } else if($response[0] == '6'){
                                        $blockerErrorMessage = 'Could not import this file as node id and parent id is same at row number : '.implode(',', json_decode($response[1], true));
                                    } 
                                }
                                 if($blockerErrorMessage != "")
                                 {
                                    $errorType = 'custom_validation_error';
                                    $message = [$blockerErrorMessage];
                                    $_status = 'Invalid file.';
                                    return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));
                                 }   
                                $error_message_column = '';
                                $import_taxonomy_settings = json_encode(['import_type'=> $requestImportType,'taxonomy_title' => trim($csvContentToArray[0]['case_title']),'package_exists' => $packageExists]);
                                if($import_taxonomy_settings == null) {
                                    $error_message_column = 'import_taxonomy_settings returned null';
                                    if(isset($csvContentToArray[0]['case_title'])) {
                                        $error_message_column .= ', case_title is '.$csvContentToArray[0]['case_title'];
                                    } else {
                                        $error_message_column .= ', case title is not valid';
                                    }

                                    $errorType = 'custom_validation_error';
                                    $message = ['The file should not contain special characters in document case title'];
                                    $_status = 'Invalid file.';
                                    return $this->run(new RespondWithJsonJob($errorType,[],$message, $_status,[],false));
                                }
                                if($packageExists == true) {
                                    $inputProcessInput['import_job_id']= $fileIdentifier;
                                    $inputProcessInput['batch_id']= $fileIdentifier;
                                    $inputProcessInput['results']= '';
                                    $inputProcessInput['errors']= $error_message_column;
                                    $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                                    $inputProcessInput['organization_id']= $organizationId;
                                    $inputProcessInput['source_document_id']= trim($csvContentToArray[0]['node_id']);
                                    $inputProcessInput['process_type']= 2;
                                    $inputProcessInput['status']= 1;
                                    $inputProcessInput['created_at']= now()->toDateTimeString();
                                    $inputProcessInput['batch_id'] = $fileIdentifier; //$batchId;
                                    $inputProcessInput['filefullpath'] = $externalCsvFile;
                                    $inputProcessInput['processed_csv_s3_file_path'] = '';//$externalJsonFile;
                                    $inputProcessInput['import_taxonomy_settings'] = $import_taxonomy_settings;
                                    $this->run(new CreateImportProcessJob($inputProcessInput));

                                    /**
                                     * 
                                     * This code code commented as per discussion on 6-4-2020 
                                     * Team meeting
                                     * 
                                     */
                                    /*
                                    $successType = 'custom_found';
                                    // please note client is using the message string to put conditions for clone popup ##so never change this
                                    $message = ['Please process the csv for summary'];
                                    $_status = 'custom_status_here';

                                    $data = ['source_identifier'=> trim($csvContentToArray[0]['node_id']),'import_identifier'=> $fileIdentifier,'batch_id'=>$batchId ,'package_exist' =>$packageExists];
                                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                                     */   
                                    $successType = 'custom_found';
                                    // please note client is using the message string to put conditions for clone popup ##so never change this
                                    $message = ['Please choose following option for upload'];
                                    $_status = 'custom_status_here';
        
                                    $data = ['import_type' => [4,2],'source_identifier'=> trim($csvContentToArray[0]['node_id']),'import_identifier'=> $fileIdentifier,'batch_id'=>$fileIdentifier,'package_exist' =>$packageExists,'source' => 'csv'];
                                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                                }
                                else 
                                {
                                    if($csvContentToArray[0]['node_id']!='') 
                                    {
                                        $inputProcessInput['import_job_id'] = $fileIdentifier;
                                        $inputProcessInput['batch_id'] = $fileIdentifier;
                                        $inputProcessInput['results']= '';
                                        $inputProcessInput['errors']= $error_message_column;
                                        $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                                        $inputProcessInput['organization_id']= $organizationId;
                                        $inputProcessInput['source_document_id']= trim($csvContentToArray[0]['node_id']);
                                        $inputProcessInput['process_type']= 2;
                                        $inputProcessInput['status']= 1;
                                        $inputProcessInput['created_at']= now()->toDateTimeString();
                                        $inputProcessInput['batch_id'] = $fileIdentifier; //$batchId;
                                        $inputProcessInput['filefullpath'] = $externalCsvFile;
                                        $inputProcessInput['processed_csv_s3_file_path'] = '';//$externalJsonFile;
                                        $inputProcessInput['import_taxonomy_settings'] = $import_taxonomy_settings;
                                        $this->run(new CreateImportProcessJob($inputProcessInput));
                                        /*
                                        $successType = 'custom_found';
                                        // please note client is using the message string to put conditions for clone popup ##so never change this
                                        $message = ['Please process the csv for summary'];
                                        $_status = 'custom_status_here';

                                        $data = ['source_identifier'=> trim($csvContentToArray[0]['node_id']),'import_identifier'=> $fileIdentifier,'batch_id'=>$batchId ,'package_exist' =>$packageExists];
                                        return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                                        */

                                        $successType = 'custom_found';
                                        // please note client is using the message string to put conditions for clone popup ##so never change this
                                        $message = ['Please choose following option for upload'];
                                        $_status = 'custom_status_here';
        
                                        $data = ['import_type' => [1],'source_identifier'=> trim($csvContentToArray[0]['node_id']),'import_identifier'=> $fileIdentifier,'batch_id'=>$fileIdentifier,'package_exist' =>$packageExists,'source' => 'csv'];
                                        return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status,[],true));
                                    }
                                    else
                                    {
                                        $successType = 'custom_validation_error';
                                        // please note client is using the message string to put conditions for clone popup ##so never change this
                                        $message = ['This taxonomy does not contain a document'];
                                        $_status = 'custom_status_here';
                    
                                        return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                                    }
                                }
                            }
                        }
                    }
                    else 
                    {
                        $errorType = 'custom_validation_error';
                        $message = !empty($validateCsvFile['message']["csvFile"]["file"])? empty($validateCsvFile['message']["csvFile"]["file"]) : ['Please provide a valid csv file'];
                        $_status = 'Invalid file.';
                        return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status, [], false));
                    }
                } 
                else 
                {
                    $errorType = 'custom_validation_error';
                    $message = ['Please provide a file to import'];
                    $_status = 'Invalid file.';
                    return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status, [], false));
                }
            }
            else if($importPass == 3)
            {
                try {
                    $validateBatchId = $this->run(new ValidateBatchIdJob($batchId));
                    if($validateBatchId !== true) {
                        $errorType = 'custom_validation_error';
                        $_status = 'custom_status_here';

                        return $this->run(new RespondWithJsonJob($errorType, [], $validateBatchId, $_status, [], false));
                    }
                    $eventData = [
                        'batchId' => $batchId,
                        'requestingUserDetails' => $requestingUserDetails,
                        'request' => $request->all(),
                        'domain_name' => $serverName,
                        'importType' => $requestImportType
                    ];
                    $csvPath = DB::table('import_job')->select('filefullpath')->where('batch_id', $batchId)->where('is_deleted', 0)->get()->first()->filefullpath;
                    // $header = [
                    //     'content-type' => 'application/json',
                    //     // 'x-api-key' => 'RqQQQDQt4O17INI8TRfPs10weqa99w9G5RqJRzv5'
                    // ];
                    // //application/x-www-form-urlencoded
                    // $client = new \GuzzleHttp\Client();
                    // $url = 'localhost:3000/importcsv';

                    // $response = $client->request('POST', $url, [
                    //     'headers' => $header,
                    //     'form_params' => json_encode([
                    //         'orgid' => $organizationId,
                    //         'batch_id' => $batchId,
                    //         'import_type' => $requestImportType,
                    //         'csv_path' => Storage::disk('s3')->url($csvPath),
                    //         'event_data' => json_encode($eventData)
                    //     ]),
                    //     'debug' => false
                    // ]);
                    $data = json_encode([
                        'orgid' => $organizationId,
                        'batch_id' => $batchId,
                        'import_type' => $requestImportType,
                        'csv_path' => $csvPath,
                        'event_data' => json_encode($eventData)
                    ]);

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "localhost:3000/importcsv",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json"
                        ),
                    ));

                    $response = curl_exec($curl);
                    curl_close($curl);
                    $response = json_decode($response, true);
                    if(!isset($response['success']) || (isset($response['success']) && $response['success'] !== true)) {
                        $this->sendErrorMail($batchId, $response);

                        $successType = 'custom_not_found';
                        $message = ['Something went wrong, Please try again later'];
                        $_status = 'custom_status_here';

                        return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                    }
                    $eventData['table_name'] = $response['table_name'];

                    event(new SummaryMultipleCsvEvent($eventData));

                    $successType = 'custom_found';
                    $message = ['You have successfully completed Step1. We will send you an email once the validation check is complete'];
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));
                } catch(\Exception $ex) {
                    $message = $this->createErrorMessageFromException($ex);
                    $batchId = !empty($request->input('batch_id')) ? $request->input('batch_id') : "";
                    $requestingUserDetails = $request->input("auth_user");
                    $organizationId = $requestingUserDetails['organization_id'];
                    $userId = $requestingUserDetails['user_id'];
                    $requestUrl = url('/');
                    $serverName = str_replace('server', '', $requestUrl);
                    if(!empty($batchId)) {
                        DB::table('import_job')
                            ->where('batch_id', $batchId)
                            ->where('process_type', 2)
                            ->where('is_deleted', 0)
                            ->update([
                                'status' => 4,
                                'updated_at' => now(),
                                'errors' => $message
                            ]);
                        $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $serverName);
                        $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                    }


                    $errorType = 'internal_error';
                    $_status = 'custom_status_here';
                    Log::error('batch_id:::'.$request->input('batch_id').':::'.$message);
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else if($importPass == 4) 
            {
                try {
                    $eventData = [
                        'batchId' => $batchId,
                        'requestingUserDetails' => $requestingUserDetails,
                        'request' => $request->all(),
                        'requestUrl' => $requestUrl,
                        'domain_name' => $serverName,
                        'importType' => $requestImportType
                    ];
                    /**
                     * Event for Import Multiple csv file in same batch
                     */

                    $settings_obj = DB::table('import_job')
                                        ->select('import_taxonomy_settings')
                                        ->where('batch_id',$batchId)
                                        ->where('is_deleted', 0)
                                        ->first()->import_taxonomy_settings;
                    $settings_arr = json_decode($settings_obj, true);
                    $settings_arr['summary_link_expired'] = true;

                    DB::table('import_job')
                        ->select('import_taxonomy_settings')
                        ->where('batch_id',$batchId)
                        ->where('is_deleted', 0)
                        ->update([
                            'import_taxonomy_settings' => json_encode($settings_arr),
                            'updated_at' => now()
                        ]);

                    event(new ImportMultipleCsvEvent($eventData));
                    $successType = 'custom_found';
                    $message = ['You have successfully completed Step2. We will send you an email once the import process is complete'];
                    $_status = 'custom_status_here';

                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], true));
                } catch(\Exception $ex) {
                    $message = $this->createErrorMessageFromException($ex);
                    $batchId = !empty($request->input('batch_id')) ? $request->input('batch_id') : "";
                    $requestingUserDetails = $request->input("auth_user");
                    $organizationId = $requestingUserDetails['organization_id'];
                    $userId = $requestingUserDetails['user_id'];
                    $requestUrl = url('/');
                    $serverName = str_replace('server', '', $requestUrl);
                    if(!empty($batchId)) {
                        DB::table('import_job')
                            ->where('batch_id', $batchId)
                            ->where('process_type', 2)
                            ->where('is_deleted', 0)
                            ->update([
                                'status' => 4,
                                'updated_at' => now(),
                                'errors' => $message
                            ]);
                        $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $serverName);
                        $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                    }

                    $errorType = 'internal_error';
                    $_status = 'custom_status_here';
                    Log::error('batch_id:::'.$request->input('batch_id').':::'.$message);
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $successType = 'custom_validation_error';
                $message = ['Import pass is required or not valid'];
                $_status = 'custom_status_here';

                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
            }

        }
        catch(\Exception $ex){
            $message = $this->createErrorMessageFromException($ex);
            $batchId = !empty($request->input('batch_id')) ? $request->input('batch_id') : "";
            if(!empty($batchId)) {
                DB::table('import_job')
                    ->where('batch_id', $batchId)
                    ->where('process_type', 2)
                    ->where('is_deleted', 0)
                    ->update([
                        'status' => 4,
                        'updated_at' => now(),
                        'errors' => $message
                    ]);
            }

            $errorType = 'internal_error';
            $_status = 'custom_status_here';
            Log::error('batch_id:::'.$request->input('batch_id').':::'.$message);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function csvToArray(string $filename, int $length = 0, string $delimiter = ','): array {
        header('Content-Type: text/html; charset=utf-8');
        $baseFolderPathForStorage = storage_path('app/public');
        $content = Storage::disk('s3')->get($filename);
        // currently we dont have function to read content from s3 as csv 
        $s3 = Storage::disk('public');
        $s3->put($filename, $content);
        $handle = fopen($baseFolderPathForStorage.'/'.$filename, 'r');
        $hashes         =       [];
        $values         =       [];
        $resultFromCSV  =       [];
        $header         =       null;
        $headerUnique   =       null;

        $header = fgetcsv($handle, $length, $delimiter);
        if (!$header) {
            return $values;
        }
    
        //$headerUnique = unique_columns($header);
        $header = str_replace('?', '', array_map("utf8_decode", $header));
        $header[0] = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $header[0]);
        $indexForCsv = 0;
        while (false !== ($data = fgetcsv($handle, $length, $delimiter))) {
            //$data = array_map("utf8_decode", $data);
            $hash = md5(serialize($data));
            
            if (!isset($hashes[$hash])) {
                $hashes[$hash]  = true;

                $lastIndexOfHeader  = sizeOf($header) - 1;
                $lastIndexOfData    = sizeOf($data) - 1;

                if($lastIndexOfData > $lastIndexOfHeader){
                    $keyIndex   =   $lastIndexOfHeader + 1;
                    while($keyIndex <= $lastIndexOfData){
                        unset($data[$keyIndex]);
                        $keyIndex++;
                    }
                }

                if(count($header) != count($data)) {
                    
                    $key =  0;
                    while($key < count($header)){
                        if(!(array_key_exists($key, $data))){
                            $data[$key] = '';
                        }
                        $key++;
                    }
                }
                
                /* foreach($data as $key => $valueFromCsv){
                    if($key > 1){
                        //$valueFromCsv = mb_convert_encoding($valueFromCsv, "UTF-8", "ASCII");
                        /* echo $key." and ".$valueFromCsv;
                        echo "\n"; 
                    }

                    $resultFromCSV[$key] = $valueFromCsv;
                } */
                $values[]  = array_combine($header, $data);
            }
        }
        $result = ['column' => $header, 'row' => $values];
        return $result;
    }

    public function sendErrorMail($batchId, $response)
    {
        $domain = parse_url(request()->root())['host'];
        if(strpos($domain,'local') === false) {
            $emails = ['sachin.jain@learningmate.com', 'amey.joshi@learningmate.com', 'noel.cardoza@learningmate.com', 'ganesh.s@learningmate.com', 'vishal.kanade@learningmate.com', 'ranveer.yadav@learningmate.com'];
            $subject = 'Node js script did not run properly on '.$domain;
            $body = 'Nodejs script did not run properly for '.$batchId.'. Here is the response: '.json_encode($response);

            Mail::send([], [], function ($message) use ($emails, $subject, $body) {
                $message->to($emails)
                ->subject($subject)
                ->setBody($body);
            });

            DB::table('import_job')->where('batch_id', $batchId)->update(['errors' => $body]);
        }

        return;
    }
}
