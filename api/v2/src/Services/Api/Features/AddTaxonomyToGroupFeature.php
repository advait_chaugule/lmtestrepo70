<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\AddTaxonomyToGroupJob;
use App\Domains\Taxonomy\Jobs\ValidateGroupByIdJob;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use Illuminate\Support\Facades\Log;

class AddTaxonomyToGroupFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $groupId = (null!==$request->input('group_id')) ? $request->input('group_id') : '';
            $documentId = (null!==$request->input('document_id')) ? $request->input('document_id') : '';
            $requestData = $request->all();
            $userId = $requestData['auth_user']['user_id'];
            $organizationId = $requestData['auth_user']['organization_id'];           
            
            $validateGrptatus = $this->run(new ValidateGroupByIdJob(['group_id' => $groupId])); //Check if group id exists
            $validateDoctatus = $this->run(new ValidateDocumentByIdJob(['document_id' => $documentId])); //Check if doc id exists
            //Check if group id exists or if no group id send group id as blank and get all groups                
            if($validateGrptatus === true && $validateDoctatus === true){
                $grpDetail = $this->run(new AddTaxonomyToGroupJob($groupId,$documentId));
                $successType = 'custom_found';
                $rmessage = ['Taxonomy added to group successfully.'];
                return $this->run(new RespondWithJsonJob($successType, [], $rmessage, '', [], true));
            }else{
                $errorStatus = ($validateGrptatus !== true) ? (($validateDoctatus === true) ? $validateGrptatus : array_merge($validateGrptatus, $validateDoctatus['document_id'])) : $validateDoctatus['document_id'];
                $errorType = 'custom_validation_error';
                return $this->run(new RespondWithJsonJob($errorType, null, $errorStatus, '', [], false));
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
