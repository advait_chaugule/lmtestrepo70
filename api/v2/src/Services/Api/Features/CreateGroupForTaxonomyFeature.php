<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Taxonomy\Jobs\CreateGroupForTaxonomyJob;
use App\Domains\Taxonomy\Jobs\ValidateGroupByNameJob;
use App\Domains\Taxonomy\Jobs\CheckGroupNameExistsJob;
use Illuminate\Support\Facades\Log;

class CreateGroupForTaxonomyFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $groupName = (null!==$request->input('group_name')) ? trim($request->input('group_name')) : '';
            $requestData = $request->all();
            $userId = $requestData['auth_user']['user_id'];
            $organizationId = $requestData['auth_user']['organization_id'];
            $grpDetails = ['name' => $groupName];
            $validateGrptatus = $this->run(new ValidateGroupByNameJob($grpDetails)); //Check if group name exists
            //Save group name if it does not exists                
            if($validateGrptatus === true){
                $validateGrpIfExists = $this->run(new CheckGroupNameExistsJob($groupName,'',$organizationId)); //Check if group name exists
                if($validateGrpIfExists==true){
                    $errorType = 'custom_validation_error';
                    return $this->run(new RespondWithJsonJob($errorType, null, ['Group name already exists.'], '', [], false));
                }
                else{
                    $response = $this->run(new CreateGroupForTaxonomyJob($groupName,$organizationId,$userId));
                    $successType = 'custom_found';
                    $rmessage = ['Group created successfully.'];
                    return $this->run(new RespondWithJsonJob($successType, $response, $rmessage, '', [], true));
                }                
            }else{
                $errorType = 'custom_validation_error';
                return $this->run(new RespondWithJsonJob($errorType, null, $validateGrptatus, '', [], false));
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
