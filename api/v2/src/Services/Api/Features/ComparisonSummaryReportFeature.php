<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithDownloadFileJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Document\Jobs\ValidateDocumentJob;

use App\Domains\CaseStandard\Jobs\CreateCaseStandardDataJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageJob;
use App\Domains\Organization\Jobs\GetOrganizationIdByOrgCodeJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageWithActiveMetadataJob;
use App\Domains\Taxonomy\Jobs\GetPublishedTaxonomyJsonJob;
use App\Domains\Document\Jobs\ValidateDocumentIdsAllExistsJob;
use App\Domains\Document\Jobs\ValidateSourceDocumentIdsAllExistsJob;
use App\Domains\Taxonomy\Jobs\GetVersionSubscriptionDetailsJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Domains\Taxonomy\Jobs\ComparisonSummaryCountJob;
use App\Domains\Taxonomy\Jobs\ComparisonSummaryDetailsJob;
use App\Services\Api\Traits\ComparisonSummaryHelperTrait;
use Log;

class ComparisonSummaryReportFeature extends Feature
{
    use StringHelper, ErrorMessageHelper, ComparisonSummaryHelperTrait;

    public function handle(Request $request)
    {
        try{
            $requestUserDetails = $request->input("auth_user");
            $organizationId     = (null!==$request->input("subscriber_org_id")) ? $request->input("subscriber_org_id") : (isset($requestUserDetails["organization_id"]) ? $requestUserDetails["organization_id"] : '');
            $userId             = (null!==$request->input("subscriber_user_id")) ? $request->input("subscriber_user_id") : (isset($requestUserDetails["user_id"]) ? $requestUserDetails["user_id"] : '');
            $comparisonId       = (null!==$request->input("comparison_id")) ? $request->input("comparison_id") : '';
            $type               = (null!==$request->input("type")) ? $request->input("type") : '';
            $internalName       = (null!==$request->input("internal_name")) ? $request->input("internal_name") : '';
            $nodeTypeId         = (null!==$request->input("node_type_id")) ? $request->input("node_type_id") : '';
            
            $error = $this->checkComparisonIdExists($comparisonId);

            if($error == true){
                $message = 'Comparison Id not found.';
                $successType = 'custom_not_found';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
            }
            $summaryReport = [];                
            if(empty($type)){
                $summaryReport = $this->run(new ComparisonSummaryCountJob($comparisonId));
                $metadataSummaryReport = $this->run(new ComparisonSummaryDetailsJob($comparisonId,'allMetadata','showCount'));
                if(!empty($metadataSummaryReport)){
                    $summaryReport["metadata"]['items'] = $metadataSummaryReport['items'];
                    $summaryReport["metadata"]["displayName"] = "Metadata";
                    // Check if document has metadata
                    if($metadataSummaryReport['document']>0){
                        // Check if document is not set, add default values for document
                        if(!isset($summaryReport['document'])){
                            $summaryReport['document'] = ['type'=>'document','new'=>0,'edit'=>0,'delete'=>0,'displayName'=>"Document"];
                        }
                        // if yes change edit count
                        $summaryReport['document']['edit'] = $metadataSummaryReport['document'];
                    }
                }
            }
            else{
                $summaryReport = $this->run(new ComparisonSummaryDetailsJob($comparisonId,$type,'displayDetails',$internalName,$nodeTypeId));
            }

            $successType = 'custom_found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $summaryReport, $message,$_status,[],true));
            
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
