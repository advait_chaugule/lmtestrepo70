<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\GetUserAllDetailByEmailIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Email\Jobs\ValidateEmailInputJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\Email\Jobs\SendEmailJob;
use Log;

class SendEmailFeature extends Feature
{
    use UuidHelperTrait;
    public function handle(Request $request)
    {
        
        
        try{
            $input = $request->input();
          
            $validationStatus = $this->run(new ValidateEmailInputJob($input));
           
            if($validationStatus===true) {
               
                $notificationId1 = $this->createUniversalUniqueIdentifier();
                $userDetail = $this->run(new GetUserAllDetailByEmailIdJob($input['recepient']));
                
                $org_name   = $this->run(new GetOrgNameByOrgIdJob($input['org_id']));
                
                $owner_id1 = $userDetail['user_id'];
                $taxonomyName = $input['taxonomy_name'];
                $taxonomyUrl = $input['url'];
                $targetContext1         = ['document_id'=>'','user_id'=>$owner_id1,'url' => $taxonomyUrl];
                $targetContextJson1     = json_encode($targetContext1,JSON_UNESCAPED_SLASHES);
                $notificationCategory1  = config('event_activity')["Notification_category"]["TAXONOMY_EXPORTED"];
                $organizationId         = $input['org_id'];
        
                $activityLog1 = [
                    "notification_id"       => $notificationId1,
                    "description"           => 'The Taxonomy '. $taxonomyName.' is ready to Export',
                    "target_type"           => "19",
                    "target_id"             => '',
                    "user_id"               => $owner_id1,
                    "organization_id"       => $organizationId,
                    "is_deleted"            => "",
                    "created_by"            => $owner_id1, // logged in user Id
                    "updated_by"            => $owner_id1,
                    "target_context"        => $targetContextJson1,
                    "read_status"           => "0", //0=unread 1= read
                    "notification_category" => $notificationCategory1
                    ];

                    
                    $domain_name    = str_replace('server', '', url('/'));
                    $this->run(new CreateNotificationJob($activityLog1));

                    $input['username'] = $userDetail['first_name'];
                    $input['downloadLink'] = $taxonomyUrl;
                    $input['orgname'] = $org_name['organization_name'];
                    $input['domainName'] = $domain_name;
                    $input['orgCode']   = $input['org_id'];
                    
                    $input['body'] = "  The  Export of the ".$input['taxonomy_name']." is completed. Please click on the following button to open the link and know more. You can also copy and paste the URL {$taxonomyUrl} in your browser to launch." ;  
                    $input['header'] = " The system successfully completed the export of the  ".$input['taxonomy_name'];
                    $input["subject"] = "[ACMT Notification] – Export Process completed successfully.";
               
                    $this->run(new SendEmailJob($input));

                

                $successType = 'found';
                $message = 'Mail send successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
            }
            else {
                $errors = $validationStatus;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
