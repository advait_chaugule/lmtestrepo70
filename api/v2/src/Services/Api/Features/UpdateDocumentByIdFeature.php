<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Document\Jobs\ValidateSaveDocumentAndRespectiveMetadataValuesInputJob;
use App\Domains\Document\Jobs\SaveDocumentAndRespectiveMetadataValuesJob;

use App\Domains\Document\Jobs\UpdateDocumentJob;
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Document\Jobs\GetDocumentByIdJob;
use Log;

class UpdateDocumentByIdFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            
            $document_id = $request->route('document_id');
            $input = [ 'document_id' => $document_id ];
            $auth_user = $request->all()['auth_user'];
            $requestData = $request->all();
            $requestUrl  = url("/");
            $documentDetail = $this->run(new GetDocumentByIdJob([ 'id' => $document_id]));
            
            if(!array_key_exists("title",$requestData))
            {
                $title['title']=$documentDetail['title'];
                $requestData=array_merge($requestData,$title);
            }
            if(!array_key_exists("adoption_status",$requestData))
            {
                $adoptionStatus['adoption_status']=$documentDetail['adoption_status'];
                $requestData=array_merge($requestData,$adoptionStatus);
            }

            if(!array_key_exists("title_html",$requestData))
            {
                $titleHtml['title_html']=$documentDetail['title_html'];
                $requestData=array_merge($requestData,$titleHtml);
            }
            if(!array_key_exists("creator",$requestData))
            {
                $creator['creator']=$documentDetail['creator'];
                $requestData=array_merge($requestData,$creator);
            }
            //$validateInputs = $this->run(new ValidateSaveDocumentAndRespectiveMetadataValuesInputJob($requestData));

            $requestInputs = $requestData + $input;
            
            if(array_key_exists('language_name',$requestInputs))
                {
                    $requestInputs['language'] = $requestInputs['language_name'];
                }
            
            // ACMT-1155 changes (keep everything same except call UpdateDocumentJob when title only update)
            $validation = $this->run(new ValidateDocumentByIdJob($input));
            if($validation!==false){
                if($this->isTitleOnlyUpdate($request)) {
                    $taxonomy = $this->run(new UpdateDocumentJob($requestInputs));
                }
                else {
                    // save document metadata values and prepare document data to update 
                    $taxonomy = $this->run(new SaveDocumentAndRespectiveMetadataValuesJob($requestInputs,$requestUrl));
                }
                
                // raise event to upload delta updates to cloud search
                $this->raiseEventToUploadTaxonomySearchDataToSqs($document_id);
          
                $response = $taxonomy;
                $successType = 'updated';
                $message = 'Data updated successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = "Please edit valid taxonomy";
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function isTitleOnlyUpdate(Request $request): bool {
        $requestData = $request->all();
        unset($requestData['auth_user']);
        $inputCount = count($requestData);
        return $inputCount===1 && !empty($requestData['title'] && !empty($requestData['adoption_status']));
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs(string $documentId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_document"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
