<?php
namespace App\Services\Api\Features;

use App\Data\Models\Asset;
use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Asset\Jobs\ValidateAssetCreateInputJob;
use App\Domains\Asset\Jobs\CreateAssetJob;

//use for updating columns in project and document table
use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Log;

class CreateAssetFeature extends Feature
{

    use StringHelper, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();

            $validateCreateAssetInput = $this->run(new ValidateAssetCreateInputJob($requestData));
            if($validateCreateAssetInput===true) {

                $savedAsset = $this->run(CreateAssetJob::class);
                //dump($savedAsset);
                if($requestData['asset_linked_type'] == 5) {
                    //set the asset order to 1
                    $assetID = $savedAsset['asset_id'];
                    $maxOrder= $this->getMaxOrder($requestData['auth_user']['organization_id']);
                   DB::update(DB::raw("Update acmt_assets set asset_order =".$maxOrder." where asset_id ='".$assetID."'"));
                  
                    // caching
                    $eventType1 = config("event_activity")["Cache_Activity"]["FILES_GETALL"];
                    $eventDetail = [
                        'event_type' => $eventType1,
                        'requestUserDetails' => $request->input("auth_user"),
                        "beforeEventRawData" => [],
                        'afterEventRawData' => ''
                    ];
                    event(new CachingEvent($eventDetail)); // notification ends here

                }
                
                //event for project and document update(updated_at) start
                event(new UpdateTaxonomyTimeEvent($requestData['item_linked_id']));
                //event for project and document update(updated_at) end

                $successType = 'created';
                $message = 'Asset created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $savedAsset, $message, $_status));
            }
            else {
                $errorType = 'validation_error';
                $message = $validateCreateAssetInput;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function getMaxOrder($organizationId){
        $getOrder = DB::select(DB::raw("SELECT IFNULL((MAX(asset_order)+1),1) as ao FROM acmt_assets WHERE item_linked_id='".$organizationId."'"));

        if(isset( $getOrder[0])){
            $return=  $getOrder[0]->ao;
        }else{
            $return=1;
        }

        return $return;
    }
}
