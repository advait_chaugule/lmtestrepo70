<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Metadata\Jobs\ListMetadataJob;

use App\Domains\Caching\Events\CachingEvent;
use Log;

class ListMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData    = $request->all();

            $metadataList = $this->run(new ListMetadataJob($request));

            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';

            return $this->run(new RespondWithJsonJob($successType, $metadataList, $message, $_status));
        } catch (\Exception $ex) { 
            Log::error($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
