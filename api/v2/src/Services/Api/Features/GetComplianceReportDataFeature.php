<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Report\Jobs\ValidateDocumentExistJob;
use App\Domains\Report\Jobs\GetItemDetailsJob;
use App\Domains\Report\Jobs\GetNodeTypeMetadataDetailsJob;
use App\Domains\Report\Jobs\GetMetadataDetailsJob;
use App\Domains\Report\Jobs\GetFieldDetailsJob;
use App\Domains\Report\Jobs\GetNodeNameJob;
use App\Domains\Report\Jobs\ValidateProjectExistJob;
use App\Domains\Report\Jobs\GetItemsMappedWithProjectJob;
use App\Domains\Report\Jobs\GetNodeTypeIdDetailJob;

use App\Domains\Report\Jobs\NodeIdMappedWithNodeNameJob;
use Log;

class GetComplianceReportDataFeature extends Feature
{
    use ErrorMessageHelper, StringHelper;

    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $organization_id=$requestData['auth_user']['organization_id'];
            $identifier=$request->route('identifier');
            $identifierType=$request->route('identifier_type');
            $identifierTypeValidator=array('1','2');
            if(in_array($identifierType,$identifierTypeValidator)){
                if($identifierType=="1"){
                    $documentIdentifier['document_id'] = $request->route('identifier');
                    $validateRequest = $this->run(new ValidateDocumentExistJob($documentIdentifier));
                    $projectIdentifier['project_id']="";
                }
                if($identifierType=="2"){
                    $projectIdentifier['project_id'] = $request->route('identifier');
                    $validateRequest = $this->run(new ValidateProjectExistJob($projectIdentifier));
                    $documentIdentifier['document_id']="";
                }
                if($validateRequest===true){
                    
                    //   $nodeIdMappedWithNodeName = $this->run(new NodeIdMappedWithNodeNameJob($organization_id));

                    if(!empty($documentIdentifier)){
                        $nodeDetails = $this->run(new GetItemDetailsJob($documentIdentifier['document_id']));
                    }    
                    if(!empty($projectIdentifier)){
                        $itemsMappedDetails = $this->run(new GetItemsMappedWithProjectJob($projectIdentifier['project_id']));
                    }
                    if(!empty($itemsMappedDetails)){
                        $nodeDetails = $this->run(new GetNodeTypeIdDetailJob($itemsMappedDetails));
                    }
                    if(!empty($nodeDetails)){
                        $metadataDetails = $this->run(new GetNodeTypeMetadataDetailsJob($nodeDetails));
                    }
                    if(!empty($metadataDetails)){
                        $FieldName = $this->run(new GetMetadataDetailsJob($metadataDetails));
                    }    
                    if(!empty($nodeDetails)){
                        $nodeName = $this->run(new GetNodeNameJob($nodeDetails));
                    }    
                    if(!empty($FieldName) && !empty($nodeName)){
                        $complianceReport = $this->run(new GetFieldDetailsJob($FieldName,$documentIdentifier,$nodeName,$nodeDetails,$itemsMappedDetails,$organization_id));
                    }    

                    if(!empty($complianceReport)) {
                        $successType = 'found';
                        $message = 'Data found.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $complianceReport, $message, $_status));
                    }
                    else{
                        $successType = 'Not found';
                        $complianceReport='No Compliance Report Found';
                        $message = 'No Data found.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $complianceReport, $message, $_status));
                    }

                }
                else{
                    $errorType = 'validation_error';
                    $message = $this->messageArrayToString($validateRequest);
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                    $errorType = 'validation_error';
                    $message = 'enter a valid identifier';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }

        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }    
    }
}
