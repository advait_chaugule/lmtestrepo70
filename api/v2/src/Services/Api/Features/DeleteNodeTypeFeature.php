<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\NodeType\Jobs\ValidateNodeTypeIdJob;
use App\Domains\NodeType\Jobs\CheckNodeTypeIsDeleteJob;
use App\Domains\NodeType\Jobs\DeleteNodeTypeJob;
use App\Domains\NodeType\Jobs\CheckNodeTypeIsUsedJob;
use App\Domains\NodeType\Jobs\CheckNodeTypeIsDocumentOrDefaultJob;

use App\Domains\Caching\Events\CachingEvent;
use Log;

class DeleteNodeTypeFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData            =   $request->all();
            $nodeTypeIdentifier     =   $request->route('node_type_id');
            $identifier             =   ['node_type_id' => $nodeTypeIdentifier];
            $organizationIdentifier =   $request->all()['auth_user']['organization_id'];

            $validationStatus = $this->run(new ValidateNodeTypeIdJob($identifier));
            if($validationStatus === true) {
                $CheckNodeTypeIsDocumentOrDefault = $this->run(new CheckNodeTypeIsDocumentOrDefaultJob($nodeTypeIdentifier));
                if($CheckNodeTypeIsDocumentOrDefault === true) {
                    $errorType = 'validation_error';
                    $message = 'Default and Document Node Types cannot be deleted.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
                $CheckNodeTypeIsDeleteJob = $this->run(new CheckNodeTypeIsDeleteJob($nodeTypeIdentifier));
                if(!$CheckNodeTypeIsDeleteJob === true) {
                    $nodeTypeIdIdentifier['node_type_id']=$nodeTypeIdentifier;
                    $CheckNodeTypeIsUsedJob = $this->run(new CheckNodeTypeIsUsedJob($nodeTypeIdIdentifier));
                    if($CheckNodeTypeIsUsedJob==1){
                        $response['node_usage']=$CheckNodeTypeIsUsedJob;
                        $successType = 'found';
                        $message = 'Node is currently been used.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else{
                        $deleteNodeType = $this->run(new DeleteNodeTypeJob($nodeTypeIdentifier, $organizationIdentifier));

                        // Caching Event
                        $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                        $eventData = [
                            "event_type"         => $eventType,
                            "organizationId"     => $requestData['auth_user']['organization_id'],
                            "beforeEventRawData" => [],
                            'afterEventRawData'  => ''
                        ];

                        event(new CachingEvent($eventData)); // caching ends here

                        $response = $deleteNodeType;
                        $successType = 'found';
                        $message = 'Data deleted successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'NodeType is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid nodetype.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
