<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Role\Jobs\ValidateRoleByIdJob;
use App\Domains\Role\Jobs\CheckRoleIsDeleteJob;
use App\Domains\Role\Jobs\UpdateRoleJob;
use App\Domains\Role\Jobs\ValidateRoleIdUniqueTitleJob;
use Log;

class UpdateRoleFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $roleId = $request->route('role_id');
            if(isset($requestData['name']))
            {
                $roleIdentifier =  ['role_id' => $roleId,'name' => $requestData['name'],'organization_id' => $requestData['auth_user']['organization_id']];
            }
            else
            {
                $roleIdentifier =  ['role_id' => $roleId,'organization_id' => $requestData['auth_user']['organization_id']];
            }        
            $validateRoleStatus = $this->run(new ValidateRoleByIdJob($roleIdentifier));
            if($validateRoleStatus === true){
                $validateRoleNameStatus = true;
                if(isset($requestData['name']))
                {
                    $validateRoleNameStatus = $this->run(new ValidateRoleIdUniqueTitleJob($roleIdentifier));
                }
               
                if($validateRoleNameStatus === true){
                $checkRoleIsDeleted = $this->run(new CheckRoleIsDeleteJob($roleId));
                if(!$checkRoleIsDeleted === true){
                    $updateRoleEntity = $this->run(new UpdateRoleJob($roleId, $requestData));
                    
                    $response = $updateRoleEntity;

                    $successType = 'custom_found';
                    if(isset($requestData['name']))
                    {
                        $messageText = 'Role Updated Succesfully.';
                    }
                    else
                    {
                        $messageText = 'Role Status Updated Succesfully.';
                    }
                    $message = [$messageText];
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status, [], true));
                }
                else{
                    $errorType = 'custom_validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Role Already Deleted';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
                }
            }
            else{
                $errorType = 'custom_validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = $validateRoleNameStatus;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
            }
        }
            else{
                $errorType = 'custom_validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please Select Valid Role';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
