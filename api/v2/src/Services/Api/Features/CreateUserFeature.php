<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\User\Jobs\ValidateCreateUserInputJob;
use App\Domains\User\Jobs\ValidateUserByEmailJob;
use App\Domains\User\Jobs\CreateUserJob;
use App\Domains\User\Jobs\SendRegistrationEmailNotificationJob;
use App\Domains\User\Jobs\IsCheckUserCurrentStatusJob;
use App\Domains\User\Jobs\RenameUsernameForHistoryBackupJob;

use App\Domains\Organization\Jobs\GetOrganizationDetailByIdJob;
use App\Domains\User\Jobs\IsCheckUserCurrentStatusInTenantJob;

use App\Domains\User\Jobs\AddUserInOrganizationJob;

use App\Domains\User\Jobs\SendInvitationEmailNotificationJob;
use Log;

class CreateUserFeature extends Feature
{
    use UuidHelperTrait, ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestVar = $request->all();

            $input['organization_id'] =  $requestVar['auth_user']['organization_id'];
            $input['user_id'] = $this->createUniversalUniqueIdentifier();
            $input['active_access_token'] = $this->createUniversalUniqueIdentifier();
            $input['email'] = $requestVar['email_id'];
            $input['username'] = $requestVar['email_id'];
            $input['role_id'] = $requestVar['role_id'];
            $inputEmail = ['email' => $requestVar['email_id']];
            $inputEmailTenant = ['email' => $requestVar['email_id'],'organization_id' => $input['organization_id'],'role_id' => $input['role_id']];
            $organizationDetail =   $this->run(new GetOrganizationDetailByIdJob($input['organization_id']));
            $validationStatus = $this->run(new IsCheckUserCurrentStatusInTenantJob($inputEmailTenant));
            $requestUrl    = url('/');
            $serverName    = str_replace('server', '', $requestUrl);
            if($validationStatus == 0) {
                //also set access token created/updated timestamp
                $input["access_token_updated_at"] = now()->toDateTimeString();
                
                $user = $this->run(new CreateUserJob($input));
                $email['email'] = $input['email'];
                $email['active_access_token'] = $input['active_access_token'];
                $email['org_code']  =   $organizationDetail->org_code;
                $email['domain_name']  =  $serverName;
                $this->run(new SendRegistrationEmailNotificationJob($email));
                $user['email'] = "Sent verification mail.";
                $successType = 'created';
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $user, $message, $_status));
            }
            else if($validationStatus == 1) {
                $successType = 'found';
                $data = ['result' => 'deactive'];
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Email already exists in system. But it is not active.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }
            else if($validationStatus == 2) {

                $this->run(new RenameUsernameForHistoryBackupJob($inputEmail));
               
                $input["access_token_updated_at"] = now()->toDateTimeString();
                $user = $this->run(new CreateUserJob($input));
                $email['email'] = $input['email'];
                $email['active_access_token'] = $input['active_access_token'];
                $email['org_code']  =   $organizationDetail->org_code;
                $email['domain_name']  =  $serverName;
                $this->run(new SendRegistrationEmailNotificationJob($email));
                $user['email'] = "Sent verification mail.";
                $successType = 'created';
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $user, $message, $_status));
            }
            else if($validationStatus == 3) {
                 $successType = 'found';
                $data = ['result' => 'exist'];
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Email already exists in system. Please try with different email address.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }
            else
            {
                $userInOrg = $this->run(new AddUserInOrganizationJob($inputEmailTenant));
                $tenantInvite['email'] = $input['email'];
                $tenantInvite['active_access_token'] = $userInOrg['active_access_token'];
                $tenantInvite['organization_id']  =   $organizationDetail->organization_id;
                $tenantInvite['org_code']  =   $organizationDetail->org_code;
                $tenantInvite['org_name']  = $organizationDetail->name;
                $tenantInvite['domain_name']  =  $serverName;
                $tenantInvite['name']  =   $userInOrg['first_name']." ".$userInOrg['last_name'];
                $this->run(new SendInvitationEmailNotificationJob($tenantInvite));
                $userInOrg['email'] = "Sent Invitation mail.";
                $successType = 'created';
                $message = 'Data created successfully.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $userInOrg, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
