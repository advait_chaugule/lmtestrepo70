<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\User\Jobs\GetUserOrganizationListJob;
use Log;

class GetUserOrganizationListFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestData    = $request->all();
            $userId         = $requestData['auth_user']['user_id'];
            $identifier     = ['user_id'=>$userId];
            //print_r($identifier);die;
            $userList = $this->run(new GetUserOrganizationListJob($identifier));
            if($userList) {
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $userList, $message, $_status));
            }else{
                $successType = 'found';
                $message = 'Data not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($successType,[], $message, $_status));
            }

        }catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}