<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\CheckProjectAssociatedToDocumentJob;
use Log;

class ListAssociatedProjectFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $documentIdentifier = $request->route('document_id');
            $identifier = ['document_id' => $documentIdentifier];
            $organizationIdentifier = $request->all()['auth_user']['organization_id'];

            $validationStatus = $this->run(new ValidateDocumentByIdJob($identifier));
            if($validationStatus === true) {
                $checkDocumentIsDeleted = $this->run(new CheckDocumentIsDeleteJob($documentIdentifier));
                if(!$checkDocumentIsDeleted === true) {

                    $checkProjectAssociated = $this->run(new CheckProjectAssociatedToDocumentJob($documentIdentifier, $organizationIdentifier));
                    if(sizeOf($checkProjectAssociated) == 0){
                        $checkProjectAssociated   =   '';  
                    }
                    
                    $response = ['projects' => $checkProjectAssociated];
                    $successType = 'found';
                    $message = 'Project list found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Document is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }   
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid role.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
