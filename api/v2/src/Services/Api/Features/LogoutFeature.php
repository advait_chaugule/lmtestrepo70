<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\User\Events\UserLogoutEvent;
use Log;

class LogoutFeature extends Feature {

    public function handle(Request $request) {
        try {
            // raise event to track activities for reporting
            $requestUserDetails = $request->input('auth_user');
            $eventData = [
                "beforeEventRawData" => $requestUserDetails,
                "afterEventRawData" => $requestUserDetails,
                "requestUserDetails" => $requestUserDetails
            ];
            event(new UserLogoutEvent($eventData));

            $successType = 'found';
            $message = 'Logout successfull.';
            $response = ['message' => 'Logout successfull.'];
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

}
