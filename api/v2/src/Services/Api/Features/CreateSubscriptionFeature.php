<?php
namespace App\Services\Api\Features;

use Log;
use Illuminate\Http\Request;

use Lucid\Foundation\Feature;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Organization\Jobs\GetOrganizationIdByOrgCodeJob;
use App\Domains\Document\Jobs\ValidateDocumentIdsAllExistsJob;
use App\Domains\Document\Jobs\ValidateSourceDocumentIdsAllExistsJob;
use App\Domains\Document\Jobs\ValidateVersionExistsJob;
use App\Domains\Taxonomy\Jobs\SubscribeTaxonomyJob;

class CreateSubscriptionFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $requestDetails = $request->all();
            $inputData['pubDocumentId']     = isset($requestDetails['document_id']) ? array($requestDetails['document_id']) : [];
            $inputData['subscriberUserId']  = isset($requestDetails['subscriber_user_id']) ? $requestDetails['subscriber_user_id'] : (isset($requestDetails['auth_user']['user_id']) ? $requestDetails['auth_user']['user_id'] : '');
            $inputData['subscriberOrgId']   = isset($requestDetails['subscriber_org_id']) ? $requestDetails['subscriber_org_id'] : (isset($requestDetails['auth_user']['organization_id']) ? $requestDetails['auth_user']['organization_id'] : '');
            $inputData['subscriberEndPoint']= isset($requestDetails['subscriber_end_point']) ? $requestDetails['subscriber_end_point'] : '';
            $inputData['subDocumentId']     = isset($requestDetails['sub_document_id']) ? $requestDetails['sub_document_id'] : [];
            $inputData['isInternal']        = isset($requestDetails['auth_user']['user_id']) ? true : false;
            $inputData['version']           = ($inputData['isInternal']===false && isset($requestDetails['version']))  ? $requestDetails['version'] : '';
            
            // Get organization_id based on org_code
            $publisherOrg = $this->run(new GetOrganizationIdByOrgCodeJob($request->route('org_code')));
            
            if(isset($publisherOrg->organization_id)){
                $inValidDocument = true; // Flag for invalid document

                // Check if document is valid and subscribe                
                if(!empty($inputData['pubDocumentId']) && !empty($inputData['subscriberOrgId'])){
                    $inputData['publisherOrgId'] = $publisherOrg->organization_id; // Save publisher's org_id
                    
                    // Check if publisher's source_document id is active and belong to the same tenant as publisher's org_id 
                    $validateDocuments = $this->run(new ValidateSourceDocumentIdsAllExistsJob($inputData['pubDocumentId'],$inputData['publisherOrgId']));
                    if($validateDocuments['status']=='false'){
                        // If status fasle, Check if publisher's document id is active and belong to the same tenant as publisher's org_id
                        $validateDocuments = $this->run(new ValidateDocumentIdsAllExistsJob($inputData['pubDocumentId'],$inputData['publisherOrgId']));
                    }
                    // If status = true from either source_doument_id or document_id, get details from documents table                               
                    if($validateDocuments['status']=='true'){
                        $inValidDocument = false; // Valid document_ids
                        if($inputData['isInternal']===false){
                            $invalidVersion = true;
                            if(!empty($inputData['version'])){
                                $checkVersion = $this->run(new ValidateVersionExistsJob($validateDocuments['data'][0]->document_id,$validateDocuments['data'][0]->organization_id,$inputData['version']));
                                if($checkVersion===true){
                                    $invalidVersion = false;
                                }        
                            }
                            if($invalidVersion == true){
                                $successType = 'custom_validation_error';
                                $message = 'Please provide valid Version.';
                                $_status = 'custom_status_here';
                                return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                            }
                        }                        
                        
                        // $validateDocuments['data'] contains records from documents table based on document_id or source_document_id passed
                        $createSubscription = $this->run(new SubscribeTaxonomyJob($validateDocuments['data'], $inputData));
                        if(!empty($createSubscription)){
                            $successType = 'custom_created';
                            $message = 'Taxonomy subscribed successfully.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, $createSubscription, $message, $_status, [], true));
                        }
                        else{ // If no record inserted, it means already exists in table
                            $successType = 'custom_not_found';
                            $message = 'Taxonomy already subscribed.';
                            $_status = 'custom_status_here';
                            return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                        }
                    }
                }

                if($inValidDocument == true){
                    $successType = 'custom_validation_error';
                    $message = 'Please provide valid Publisher Document Id and Subscriber Org Id.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status, [], false));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid tenant_id.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
