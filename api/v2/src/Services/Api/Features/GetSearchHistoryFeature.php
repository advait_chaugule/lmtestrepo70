<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Search\Jobs\FetchSearchHistoryJob;

use App\Services\Api\Traits\ErrorMessageHelper;

class GetSearchHistoryFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        $searchHistory = $this->run(FetchSearchHistoryJob::class);
        $successType = 'found';
        $_status = 'custom_status_here';
        $httpResponseMessage = !empty($searchHistory) ? 'Data Found.' : 'Data Not Found.';
        return $this->run(new RespondWithJsonJob($successType, $searchHistory, $httpResponseMessage, $_status));
    }
}
