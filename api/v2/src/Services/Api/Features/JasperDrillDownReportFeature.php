<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportJob;

class JasperDrillDownReportFeature extends Feature
{
    public function handle(Request $request)
    {
              
        $data['organization_id'] = ''; 
        //$data['access_token'] = ''; 

        if($request->input('token')){
            $organization_id = $this->run(new JReportJob($request->input('token')));            
        
            $data['organization_id'] = $organization_id;    
            //$data['access_token'] = $request->input('token');      
        }       
        
        return view('api::report.jasper_drill_down',$data);
    }
}
