<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;

use App\Domains\Report\Jobs\GetEventActivitiesCountJob;
use Log;

class GetActivitiesCountFeature extends Feature
{
    use ErrorMessageHelper;

    public function handle(Request $request)
    {
        try{
            $requestFilters = $request->all();
            $data = $this->run(new GetEventActivitiesCountJob($requestFilters));
            if(!empty($data)) {
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
            }
            else {
                $errorType = 'not_found';
                $message = 'Data not found.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
