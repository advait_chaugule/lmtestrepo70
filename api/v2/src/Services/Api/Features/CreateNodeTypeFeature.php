<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Services\Api\Traits\UuidHelperTrait;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Data\Models\Organization;
use App\Domains\NodeType\Jobs\ValidateNodeTypeInputJob;
use App\Domains\NodeType\Jobs\CreateNodeTypeJob;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Domains\Caching\Events\CachingEvent;
use App\Services\Api\Traits\StringHelper;
use Log;

class CreateNodeTypeFeature extends Feature
{
    use UuidHelperTrait,CaseFrameworkTrait,StringHelper;

    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $requestUrl  = url('/');
            $inputData = ['title' => $requestData['name'], 'organization_id' => $requestData['auth_user']['organization_id']];
            $orgCode = Organization::select('org_code')->where('organization_id', $requestData['auth_user']['organization_id'])->first()->org_code;
            $validateNodeTypeData = $this->run(new ValidateNodeTypeInputJob($inputData));
            if($validateNodeTypeData===true) {
                $nodeTypeId = $this->createUniversalUniqueIdentifier();
                $inputData['node_type_id']          = $nodeTypeId;
                $inputData['source_node_type_id']   = $nodeTypeId;
                $inputData['created_by']            = $requestData['auth_user']['user_id'];
                $inputData['updated_by']            = $requestData['auth_user']['user_id'];
                $inputData['organization_id']       = $requestData['auth_user']['organization_id'];
                $inputData['uri'] =$this->getCaseApiUri("CFNodeType", $nodeTypeId,true,$orgCode,$requestUrl);
                $nodeTypeCreated = $this->run(new CreateNodeTypeJob($inputData));
                
                $resultData = $nodeTypeCreated;

                // Caching Event
                $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                $eventData = [
                    "event_type"         => $eventType,
                    "organizationId"     => $requestData['auth_user']['organization_id'],
                    "beforeEventRawData" => [],
                    'afterEventRawData'  => ''
                ];

                event(new CachingEvent($eventData)); // caching ends here

                $successType = 'custom_created';
                $message = ['NodeType created successfully.'];
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($successType, $resultData, $message, $_status, [], true));
            }
            else{
                $errorType = 'custom_validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = $validateNodeTypeData;
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonJob($errorType, null, $message, '', [], false));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
