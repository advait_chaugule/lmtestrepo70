<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Note\Jobs\ValidateNoteIdJob;
use App\Domains\Note\Jobs\CheckNoteIsDeleteJob;
use App\Domains\Note\Jobs\DeleteNoteJob;
use App\Domains\Note\Jobs\CheckNoteIsUsedJob;
use Log;

class DeleteNoteFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $noteIdentifier         =   $request->route('note_id');
            $identifier             =   ['note_id' => $noteIdentifier];
            $organizationIdentifier =   $request->all()['auth_user']['organization_id'];

            $validationStatus = $this->run(new ValidateNoteIdJob($identifier));
            //dd($validationStatus);
            if($validationStatus === true) {
                $CheckNoteIsDeleteJob = $this->run(new CheckNoteIsDeleteJob($noteIdentifier));
                 // dd($CheckNoteIsDeleteJob);
                if(!$CheckNoteIsDeleteJob === true) {
                    $noteIdIdentifier['note_id']=$noteIdentifier;
                    $CheckNoteIsUsedJob = $this->run(new CheckNoteIsUsedJob($noteIdIdentifier));
                    if($CheckNoteIsUsedJob==1){
                        $response['note_usage']=$CheckNoteIsUsedJob;
                        $successType = 'found';
                        $message = 'Note is currently been used.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                    else{
                        $deleteNote = $this->run(new DeleteNoteJob($noteIdentifier, $organizationIdentifier));
                        // caching
                        $eventType1              = config("event_activity")["Cache_Activity"]["NOTES_GETALL"];
                        $eventDetail = [
                            'organization_id'    => $organizationIdentifier,
                            'event_type'         => $eventType1,
                            'requestUserDetails' => $request->input("auth_user"),
                            "beforeEventRawData" => [],
                            'afterEventRawData'  => ''
                        ];
                        event(new CachingEvent($eventDetail)); // notification ends here
                        $response = $deleteNote;
                        $successType = 'found';
                        $message = 'Data deleted successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                    }
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Note is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = 'Please select valid Note.';
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
