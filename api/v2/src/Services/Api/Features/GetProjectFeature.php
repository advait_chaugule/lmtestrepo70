<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Project\Jobs\GetProjectJob;
use Log;

class GetProjectFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $routeParam = $request->route('search_key');
            $input = [ 'search_key' => $routeParam ];
            $projectlist = $this->run(new GetProjectJob($input));
            $response = $projectlist;
            $successType = 'found';
            $message = 'Data found.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
