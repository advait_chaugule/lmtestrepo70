<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Domains\Project\Jobs\ValidateInputForAddingNodesUnderProjectJob;
use App\Domains\Project\Jobs\AddNodesUnderProjectJob;
use App\Domains\Project\Jobs\UpdateProjectParentItemToNonEditableJob;
use App\Domains\Project\Jobs\UpdatePacingGuideProjectParentItemToNonEditableJob;
use App\Domains\PacingGuide\Jobs\AddDocumentsUnderProjectJob;
use App\Domains\Project\Jobs\GetProjectByIdJob;
use App\Domains\Project\Jobs\GetFlattenedTaxonomyForProjectCompleteTreeJob;
use App\Domains\Caching\Events\CachingEvent;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use Log;

class ProjectItemMappingFeature extends Feature
{
    use ErrorMessageHelper, ArrayHelper, StringHelper;

    public function handle(Request $request)
    {   
        try {
            $input = $request->input();
            $requestUser = $request->input("auth_user");
            $projectDet = $this->run(new GetProjectByIdJob($input));
            
            if($projectDet['project_type']==1)
            {
                $validationStatus = $this->run(new ValidateInputForAddingNodesUnderProjectJob($input));
                if($validationStatus===true) {
                    $projectIdentifier = $input["project_id"];
                    $documentIdentifier = $input['document_id'];
                    $itemIds = $input["item_id"];
                    $parentIds = $input["parent_id"];
                    $rootSelectedStatus = (int) $input["root_selected_status"]===1 ? true : false;

                    // Optimised Part
                    $nodeIdsToAssign = [];
                    if(!empty( $itemIds)){
                      $nodeIdsToAssign = array_unique($this->createArrayFromCommaeSeparatedString($itemIds));
                    }
                    $this->run(new AddNodesUnderProjectJob($nodeIdsToAssign, $projectIdentifier, $rootSelectedStatus, $documentIdentifier));

                    /**
                     * Please do not remove this code 
                     */
                    $arrayParentId  =   $this->exportMultipleItemId($parentIds);
                    //All the semi selected parent nodes are set to non editable for a project
                    $this->run(new UpdateProjectParentItemToNonEditableJob($arrayParentId, $projectIdentifier,$documentIdentifier));

                    $this->raiseEventToUploadSearchDataToSqs($projectIdentifier, 'update_project');

                    $successType = 'found';
                    $message = 'Project taxonomy mapped successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, [], $message, $_status));
                }
                else {
                    $errorType = 'validation_error';
                    $message = $this->messageArrayToString($validationStatus);
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else{ 
                $validationStatus = $this->run(new ValidateInputForAddingNodesUnderProjectJob($input));
                if($validationStatus===true) {
                    $projectIdentifier = $input["project_id"];
                    $documentIdentifier = $input['document_id'];
                    $itemIds = $input["item_id"];
                    $parentIds = $input["parent_id"];
                    //$rootSelectedStatus = 1;
                    $rootSelectedStatus = (int) $input["root_selected_status"]===1 ? true : false;

                    // Optimised Part
                    $nodeIdsToAssign = array_unique($this->createArrayFromCommaeSeparatedString($itemIds . "," . $parentIds));
                    $this->run(new AddDocumentsUnderProjectJob($nodeIdsToAssign, $projectIdentifier, $rootSelectedStatus, $documentIdentifier));

                    $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyForProjectCompleteTreeJob($documentIdentifier, $projectIdentifier, $requestUser, $request));

                    /**
                     * Please do not remove this code 
                     */
                    $arrayParentId  =   $this->exportMultipleItemId($parentIds);
                    //All the semi selected parent nodes are set to non editable for a project
                    $this->run(new UpdatePacingGuideProjectParentItemToNonEditableJob($arrayParentId, $projectIdentifier));

                    $successType = 'found';
                    $message = 'Pacing Guide taxonomy mapped successfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $taxonomyHierarchy, $message, $_status));
                }
                else {
                    $errorType = 'validation_error';
                    $message = $this->messageArrayToString($validationStatus);
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }    
        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $this->createErrorMessageFromException($ex);
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function exportMultipleItemId($data) {
        $arrayItemId = explode(',', $data);
        return $arrayItemId;
    }

    private function raiseEventToUploadSearchDataToSqs(string $projectId, string $eventTypeKey='update_project') {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations[$eventTypeKey];
        $eventData = [
            "type_id" => $projectId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
