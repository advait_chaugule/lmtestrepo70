<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\MetaDataKey\Jobs\ValidateMetaDataKeyAssociationTypeJob;
use App\Domains\MetaDataKey\Jobs\ListMetaDataKeysJob;
use Log;

class ListMetaDataKeyFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $routeParam = $request->route('type');
            $input = [ 'type' => $routeParam ];
            $validationStatus = $this->run(new ValidateMetaDataKeyAssociationTypeJob($input));
            if($validationStatus===true) {
                $data = $this->run(new ListMetaDataKeysJob($routeParam));
                if(!empty($data)){
                    $successType = 'found';
                    $message = 'Data found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    $message = 'Data not found.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errors = $validationStatus;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }  
    }
}
