<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;

use App\Domains\Project\Jobs\ValidateProjectIdJob;
use App\Domains\Project\Jobs\ValidateCreateProjectInputJob;
use App\Domains\Project\Jobs\EditProjectJob;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Domains\Caching\Events\CachingEvent;
use Log;

class ProjectEditFeature extends Feature
{
    public function handle(Request $request)
    {
        try{
            $projectId = $request->route('project_id');
            $input = [ 'project_id' => $projectId ];
            $validateExistence = $this->run(new ValidateProjectIdJob($input));
            if($validateExistence===true) {
                $requestData = $request->all();
                $validateInputs = $this->run(new ValidateCreateProjectInputJob($requestData));
                if($validateInputs===true){
                    $requestInputs = $requestData + $input;
                    $requestInputs['updated_by'] = $requestData['auth_user']['user_id'];
                    $requestInputs = $this->cleanRequestInputsOfUnwantedFields($requestInputs);
                    $project = $this->run(new EditProjectJob($requestInputs));
                    if($project!==false){

                        $this->raiseEventToUploadSearchDataToSqs($projectId);

                        $successType = 'updated';
                        $message = 'Data updated successfully.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $project, $message, $_status));
                    }
                    else{
                        $errorType = 'internal_error';
                        return $this->run(new RespondWithJsonErrorJob($errorType));
                    }
                }
                else {
                    $errorType = 'validation_error';
                    $message = $this->run(new MessageArrayToStringHelperJob($validateInputs));
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }
            else {
                $errors = $validateExistence;
                $errorType = 'validation_error';
                $message = $this->run(new MessageArrayToStringHelperJob($errors));
                $_status = 'custom_status_here';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    public function cleanRequestInputsOfUnwantedFields(array $request) {
        $keysToSearchForAndRemove = ["auth_user", "user_id", "first_name", "last_name", "email"];
        foreach($keysToSearchForAndRemove as $delteIndex){
            if(isset($request[$delteIndex])){
                unset($request[$delteIndex]);
            }
        }
        return $request;
    }

    private function raiseEventToUploadSearchDataToSqs(string $projectId) {
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations['update_project'];
        $eventData = [
            "type_id" => $projectId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
