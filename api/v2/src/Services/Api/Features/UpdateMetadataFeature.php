<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Metadata\Jobs\ValidateMetadataByIdJob;
use App\Domains\Metadata\Jobs\CheckMetadataIsDeleteJob;
use App\Domains\Metadata\Jobs\UpdateMetadataJob;
use App\Domains\Metadata\Jobs\ValidateMetadataNameAndFieldJob;

use App\Domains\Caching\Events\CachingEvent;
use Log;

class UpdateMetadataFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData = $request->all();
            $metadataId = $request->route('metadata_id');
            $requestData['metadata_id'] = $metadataId;
            $metadataIdentifier =  ['metadata_id' => $metadataId];

            $validateMetadataStatus = $this->run(new ValidateMetadataByIdJob($metadataIdentifier));
            
            if($validateMetadataStatus === true){
                $validateMetadataFieldAndName=null;
                if(isset($requestData['name'])){
                    $validateMetadataFieldAndName = $this->run(new ValidateMetadataNameAndFieldJob($requestData,'update'));
                }

                if($validateMetadataFieldAndName==0) 
                {
                
                $checkMetadataIsDeleted = $this->run(new CheckMetadataIsDeleteJob($metadataId));
                if(!$checkMetadataIsDeleted === true){
                    $updateMetadataEntity = $this->run(new UpdateMetadataJob($metadataId, $requestData));
                    //dd($updateMetadataEntity);
                    $response = $updateMetadataEntity;
                     // to giving response for new metadata field type (dictionary)
                    if(isset($requestData['field_type']) == 7){
                        $updateMetadata = array();
                        $updateMetadata['metadata_id'] = $response['metadata_id'];
                        $updateMetadata['name'] = $response['name'];
                        $updateMetadata['internal_name'] = $response['internal_name'];
                        $updateMetadata['field_type'] = $response['field_type'];
                        $updateMetadata['field_possible_values'] = json_decode($response['field_possible_values']);
                        $updateMetadata['is_custom'] = $response['is_custom'];
                        $updateMetadata['is_active'] = $response['is_active'];
                        $updateMetadata['updated_at'] = $response['updated_at'];
                        $response =  $updateMetadata;
                    }

                    // Caching Event
                    $eventType   = config("event_activity")["Cache_Activity"]["NODE_TYPES"]; // set node type
                    $eventData = [
                        "event_type"         => $eventType,
                        "organizationId"     => $requestData['auth_user']['organization_id'],
                        "beforeEventRawData" => [],
                        'afterEventRawData'  => ''
                    ];

                    event(new CachingEvent($eventData)); // caching ends here

                    $successType = 'found';
                    $message = 'Metadata updated succesfully.';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }
                else{
                    $errorType = 'not_found';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Metadata is already deleted';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                }
            }else
            {
                    $errorType = 'validation_error';
                    // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                    $message = 'Metadata with same name and field type already exists';
                    $_status = 'custom_status_here';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
            }
            
        }
        else{
            $errorType = 'validation_error';
            // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
            $message = 'Please select valid metadata.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
