<?php
namespace App\Services\Api\Features;

//ini_set('mbstring.substitute_character', "none"); 

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\Metadata\Jobs\ListMetadataJob;

use App\Domains\Csv\Jobs\ValidateCsvFileJob;
use App\Domains\Csv\Jobs\StoreExternalCsvFileJob;
use App\Domains\Csv\Jobs\ValidateCsvSpecificationJob;
use App\Domains\Csv\Jobs\ChangeMetricsForCsvUploadJob;
use App\Domains\Csv\Jobs\ChangeMetricsForCopyFromCsvJob;
use App\Domains\Csv\Jobs\UpdateTaxonomyWithDataFromCsvJob;
use App\Domains\Import\Jobs\CreateImportProcessJob;
use App\Domains\Import\Jobs\UpdateImportProcessJob;
use App\Domains\Csv\Jobs\CheckTaxonomyPublishedJob;

use App\Domains\Document\Jobs\ValidateDocumentSourceIdJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ImportTaxonomyFromCSVFeature extends Feature
{
    use ErrorMessageHelper, StringHelper, UuidHelperTrait, FileHelper, ArrayHelper, CaseFrameworkTrait;

    public function handle(Request $request)
    {
        try{
            $requestData            =   $request->all();
            $requestUrl  = url("/");
            $importType = 0;
            $requestingUserDetails  = $request->input("auth_user");
            $organizationId         = $requestingUserDetails['organization_id'];
            $fileOriginalName = $request->file('csvFile')->getClientOriginalName();
            $ImportIdentifier             =   $this->createUniversalUniqueIdentifier();
            $importData = DB::table('import_job')->select('import_job_id')->where('source_document_id',$fileOriginalName)->where('organization_id',$organizationId)->where('status','2')->first();
            if($importData)
            {
                $errorType = 'validation_error';
                $message = "This taxonomy is already in process. Please select different file to import.";
                $_status = 'Taxonomy Process already Started.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }

            if(isset($request['csvFile']) && !empty($request['csvFile'])) {
                // validate inputs for CASE json upload
                $validateCsvFile = $this->run(new ValidateCsvFileJob($request));
                if($request->input('import_type')){
                    $importType = $request->input('import_type');
                }       
                
                
                if($validateCsvFile['status']!==false){
                    // get uploaded csv file
                    $csvFile = $validateCsvFile['file'];
                    // create a file identifier
                    $fileIdentifier = $this->createUniversalUniqueIdentifier();
                    // name of file to create
                    $externalCsvFileName = $fileIdentifier.".csv";
                    // store the content of uploaded Csv inside the file created above
                    $externalCsvFile = $this->run(new StoreExternalCsvFileJob($csvFile, $externalCsvFileName));
                    // extract content from uploaded csv file
                    $csvHeaderAndRowToArray = $this->csvToArray($externalCsvFile);
                    $csvHeaderAndRowToArray = $this->trim_array_spaces($csvHeaderAndRowToArray);
                    $updateCSVColumn = $csvHeaderAndRowToArray['column'];

                    $csvContentToArray  =  $this->removeBlankArray($csvHeaderAndRowToArray['row']);
                    $updateCsvContentToArray  =  $csvHeaderAndRowToArray['row'];
                    $csvContentToArray = $this->array_change_key_case_recursive($csvContentToArray,CASE_LOWER);
                    $csvHeaderAndRowToArray['column'] = array_map('strtolower',$csvHeaderAndRowToArray['column']);
                    $errorMessage    =   [];
                    $searchActionArray  = ['a','u','d','add','update','delete',''];
                    $valuesOfAction     = array_column($csvContentToArray, 'action');
                        
                    foreach($valuesOfAction as $key => $action) {
                        if(!in_array(strtolower($action), array_map('strtolower', $searchActionArray))) {                           
                            $errorMessage[] = $action." as Action at row ".($key+2);
                        }
                    }
                    //dd($errorMessage);

                    $errorMessageForAssociation    =   [];
                    $searchAssociationArray = array('ischildof','ispeerof','ispartof','exactmatchof','precedes','isrelatedto','replacedby','hasskilllevel');
                    $valuesOfAssociation     =   array_column($csvContentToArray, 'case_association_type');
                    foreach($valuesOfAssociation as $key => $association) {
                        if($association !== '' && !in_array(strtolower($association), $searchAssociationArray)) {
                            $errorMessageForAssociation[] = $association." as Association at row ".($key+2);
                        }
                    }

                    if(!empty($errorMessageForAssociation)) {
                        $errorMessageForAssociation = implode(' , ',$errorMessageForAssociation);
                        $errorMessageForAssociation .= " are not accepted. Please use ".implode(', ',$searchAssociationArray)." for associations respectively.";
                    }
                    else{
                        $errorMessageForAssociation = '';
                    }


                    if(!empty($errorMessage)) {
                        $errorMessage = implode(' , ',$errorMessage);
                        $errorMessage .= " are not accepted. Please use A or Add, U or Update, D or Delete for Add, Update or Delete actions respectively.";
                    }
                    else{
                        $errorMessage = '';
                    }
                    if(count($csvContentToArray) == 0 || !$this->is_multi_array($csvContentToArray)) {
                        $errorType = 'validation_error';
                        $message = "This file does not follow the CSV/delimited text format required by ACMT. Kindly download the sample csv to view the accepted format";
                        $_status = 'Invalid file.';
                        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                    } else {

                        // Condition added to check if document id passed matches with document id in CSV
                        if($request->has('document_id')){
                            $documentIdToBeUpdated = (null!==$request->input('document_id')) ? $request->input('document_id') : '';
                            if(empty($documentIdToBeUpdated) || !isset($csvContentToArray[0]['node_id']) || trim($documentIdToBeUpdated)!=trim($csvContentToArray[0]['node_id'])){
                                $errorType = 'validation_error';
                                $message = "The CSV uploaded does not match the selected taxonomy. Please import the correct file";
                                $_status = 'Invalid file.';
                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                            }
                        }
                        // Condition added to acheck mandate columns are present even if case insensitive
                        $arrayMandateColumns = ['node_id','parent/destination_node_id','case_title','case_full_statement'];
                        if(count(array_intersect($arrayMandateColumns, array_map('strtolower', $csvHeaderAndRowToArray['column']))) != count($arrayMandateColumns)){
                            $errorType = 'validation_error';
                            $message = "node_id,parent/destination_node_id,case_title,case_full_statement columns are mandatory. Kindly download the sample csv to view the accepted format";
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                        }
                        if(count(array_intersect(['node_id','parent/destination_node_id'], $updateCSVColumn)) != count(['node_id','parent/destination_node_id'])){
                            $errorType = 'validation_error';
                            $message = "node_id,parent/destination_node_id should be in lowercase. Kindly download the sample csv to view the accepted format";
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                        }
                        if(!in_array('case_node_type', array_map('strtolower', $csvHeaderAndRowToArray['column'])) || !in_array('Document',array_column($csvContentToArray, 'case_node_type'))){
                            $errorType = 'validation_error';
                            $message = "The csv imported does not contain the document node. Please import the csv with document node.";
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                        }
                        if(isset($csvContentToArray[0]['action']) && (strtolower($csvContentToArray[0]['action']) == 'd' || strtolower($csvContentToArray[0]['action']) == 'delete')) {
                            $errorType = 'validation_error';
                            $message = "Could not delete the document type node";
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                        }
                        if(!isset($csvContentToArray[0]['node_id']) || $csvContentToArray[0]['node_id'] == '') {
                            $errorType = 'validation_error';
                            $message = "The document identifier is blank.";
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                        }
                        if(!empty($errorMessage) || !empty($errorMessageForAssociation)) {  
                            $message = $errorMessage ." ". $errorMessageForAssociation;
                            $errorType = 'validation_error';
                            $_status = 'Invalid file.';
                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                        } else {
                            
                            $metadataList = $this->run(new ListMetadataJob($request));
                    
                            /**
                             * Check if the taxonomy exported is imported in the same organization, because data will be updated only, not created
                             */
                            $packageExists = $this->run(new ValidateDocumentSourceIdJob(trim($csvContentToArray[0]['node_id']), $organizationId));
                            //dd($packageExists);

                            if($packageExists == true) {
                                /**
                                    * Validate the content of the CSV
                                    * Check if the taxonomy exported is imported in the same organization, because data will be updated only, not created
                                */
                                $validateCsvSpecification = $this->run(new ValidateCsvSpecificationJob($csvContentToArray, $organizationId));
                                if(!empty($validateCsvSpecification) && $validateCsvSpecification["status"]===true){ 
                                    if(isset($request['is_ready_to_commit_changes']) && $request['is_ready_to_commit_changes'] == '1') {
    
                                        $inputProcessInput['import_job_id']= $ImportIdentifier;
                                        $inputProcessInput['results']= '';
                                        $inputProcessInput['filefullpath']= '';
                                        $inputProcessInput['errors']= '';
                                        $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                                        $inputProcessInput['organization_id']= $organizationId;
                                        $inputProcessInput['source_document_id']= $fileOriginalName;
                                        $inputProcessInput['process_type']= 1;
                                        $inputProcessInput['status']= 2;
                                        $inputProcessInput['created_at']= now()->toDateTimeString();
                                        $this->run(new CreateImportProcessJob($inputProcessInput));
                                        if($importType == 4) {
                                            $reportNodeChangesNeededForTheCsvUpload =   $this->run(new ChangeMetricsForCsvUploadJob($csvContentToArray, $organizationId, $requestingUserDetails,  $importType,  $csvHeaderAndRowToArray['column'], $metadataList, 1));
                                            if($reportNodeChangesNeededForTheCsvUpload != false){
                                                $response = explode('||', $reportNodeChangesNeededForTheCsvUpload);
                                                $countUpdatedDataFromCsvToDb = json_decode($response[1
                                                    ]);
                                                if(!empty($countUpdatedDataFromCsvToDb)){
                                                    unset($countUpdatedDataFromCsvToDb->metadata);
                                                    unset($countUpdatedDataFromCsvToDb->errorData);
                                                }
                                                    
                                                $this->run(new UpdateTaxonomyWithDataFromCsvJob($updateCsvContentToArray, $organizationId, $requestingUserDetails,  $importType, $updateCSVColumn, $metadataList, $packageExists,$requestUrl));
                                            }
                                        }
                                        if(!isset($countUpdatedDataFromCsvToDb) && $importType != 4)
                                            $countUpdatedDataFromCsvToDb = $this->run(new UpdateTaxonomyWithDataFromCsvJob($updateCsvContentToArray, $organizationId, $requestingUserDetails,  $importType, $updateCSVColumn, $metadataList, $packageExists,$requestUrl));

                                        $this->deleteFileFromLocalPath($externalCsvFile);
                                        $inputCompleteProcessInput['import_job_id']= $ImportIdentifier;
                                        $inputCompleteProcessInput['results']= json_encode($countUpdatedDataFromCsvToDb);
                                        $inputCompleteProcessInput['status']= 3;
                                        $inputCompleteProcessInput['updated_at']= now()->toDateTimeString();
                                        $this->run(new UpdateImportProcessJob($inputCompleteProcessInput));
                                        $successType = 'found';
                                        $message = 'Succesfully imported';
                                        $_status = 'custom_status_here';
                                        return $this->run(new RespondWithJsonJob($successType, $countUpdatedDataFromCsvToDb, $message, $_status));
                                    } else {
                                        if($importType == 4) {
                                                $sourceDocumentId = $csvContentToArray[0]['node_id'];
                                                $CheckTaxonomyPublished = $this->run( new CheckTaxonomyPublishedJob($sourceDocumentId));
                                                if($CheckTaxonomyPublished == true){
                                                    $errorType = 'validation_error';
                                                    $message = 'ACMT cannot proceed with this request as the taxonomy you are trying to update is published';
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                };
                                            $this->deleteFileFromLocalPath($externalCsvFile);
                                            $reportNodeChangesNeededForTheCsvUpload =   $this->run(new ChangeMetricsForCsvUploadJob($csvContentToArray, $organizationId, $requestingUserDetails,  $importType,  $csvHeaderAndRowToArray['column'], $metadataList, 1));
                                            if($reportNodeChangesNeededForTheCsvUpload == false){
                                                $errorType = 'validation_error';
                                                $message = 'The file contains mandatory field value blank or similar name and type for custom metadata. Please refer to the sample CSV and update the file and import again';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            } else {
                                                $response = explode('||', $reportNodeChangesNeededForTheCsvUpload);
                                                if($response[0] == '2'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'The file contains : '.implode(',', json_decode($response[1], true)).' as non case metadata prefixed with case_/Case_. Please refer to sample CSV file';
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                } else if($response[0] == '3'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'You have uploaded an older supported format of the CSV. Please update as per the current format';
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }else if($response[0] == '4'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'Could not find the parent id for nodes with the following parent id : '.implode(',', json_decode($response[1], true));
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }
                                                else if($response[0] == '5'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'You have duplicate node ids at row : '.implode(',', json_decode($response[1], true));
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }
                                                else if($response[0] == '6'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'Could not import this file as node id and parent id is same at row number : '.implode(',', json_decode($response[1], true));
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }
                                                else {
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $successType = 'found';
                                                    $message = 'Data found';
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonJob($successType, json_decode($response[1
                                                ]), $message, $_status));
                                                
                                                }
                                            }
                                        } else if ($importType == 2) {
                                            $this->deleteFileFromLocalPath($externalCsvFile);
                                            $reportNodeChangesNeededForTheCsvUpload =   $this->run(new ChangeMetricsForCopyFromCsvJob($csvContentToArray, $organizationId, $requestingUserDetails,$importType, $csvHeaderAndRowToArray['column'], $metadataList, $packageExists));
                                            if($reportNodeChangesNeededForTheCsvUpload == false){
                                                $errorType = 'validation_error';
                                                $message = 'The file contains mandatory field value blank or similar name and type for custom metadata. Please refer to the sample CSV and update the file and import again';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            } else {
                                                $response = explode('||', $reportNodeChangesNeededForTheCsvUpload);
                                                if($response[0] !== '1' && $response[0] !== '4' && $response[0] !== '5' && $response[0] !== '6'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'The file contains : '.implode(',', json_decode($response[1], true)).' as non case metadata prefixed with case_/Case_. Please refer to sample CSV file';
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                } 
                                                else if($response[0] == '4'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'Could not find the parent id for nodes with the following parent id : '.implode(',', json_decode($response[1], true));
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }
                                                else if($response[0] == '5'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'You have duplicate node ids at row : '.implode(',', json_decode($response[1], true));
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }
                                                else if($response[0] == '6'){
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $errorType = 'validation_error';
                                                    $message = 'Could not import this file as node id and parent id is same at row number : '.implode(',', json_decode($response[1], true));
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                                
                                                }
                                                else {
                                                    //dd($reportNodeChangesNeededForTheCsvUpload);
                                                    $successType = 'found';
                                                    $message = 'Data found';
                                                    $_status = 'custom_status_here';
                                                    return $this->run(new RespondWithJsonJob($successType, json_decode($response[1
                                                ]), $message, $_status));
                                                
                                                }
                                            }
                                        } else {
                                            $successType = 'found';
                                            // please note client is using the message string to put conditions for clone popup ##so never change this
                                            $message = "Please choose following option for upload";
                                            $_status = 'custom_status_here';
                                            
                                            $data = ['import_type' => [4,2],'package_exist' =>$packageExists];
                                            return $this->run(new RespondWithJsonJob($successType, $data, $message, $_status));
                                        }
                                    }
                                } else {
                                    $this->deleteFileFromLocalPath($externalCsvFile);
        
                                    $errorType = 'validation_error';
                                    $message = "This taxonomy does not contain a document";
                                    $_status = '';
                                    return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                                }
                                
                            } else {
                                if($csvContentToArray[0]['node_id']!='') {
                                    if(isset($request['is_ready_to_commit_changes']) && $request['is_ready_to_commit_changes'] == '1') {
                                        if($importType == 2) {
                                            $inputProcessInput['import_job_id']= $ImportIdentifier;
                                            $inputProcessInput['results']= '';
                                            $inputProcessInput['filefullpath']= '';
                                            $inputProcessInput['errors']= '';
                                            $inputProcessInput['user_id']= $requestingUserDetails['user_id'];
                                            $inputProcessInput['organization_id']= $organizationId;
                                            $inputProcessInput['source_document_id']= $fileOriginalName;
                                            $inputProcessInput['process_type']= 1;
                                            $inputProcessInput['status']= 2;
                                            $inputProcessInput['created_at']= now()->toDateTimeString();
                                            $this->run(new CreateImportProcessJob($inputProcessInput));
                                            $countUpdatedDataFromCsvToDb =   $this->run(new UpdateTaxonomyWithDataFromCsvJob($updateCsvContentToArray, $organizationId, $requestingUserDetails, $importType, $updateCSVColumn,$metadataList, $packageExists,$requestUrl));
                                            if($countUpdatedDataFromCsvToDb!='InValidCaseHeader')
                                            {
                                                $this->deleteFileFromLocalPath($externalCsvFile);
                                                $inputCompleteProcessInput['import_job_id']= $ImportIdentifier;
                                                $inputCompleteProcessInput['results']= json_encode($countUpdatedDataFromCsvToDb);
                                                $inputCompleteProcessInput['status']= 3;
                                                $inputCompleteProcessInput['updated_at']= now()->toDateTimeString();
                                                $this->run(new UpdateImportProcessJob($inputCompleteProcessInput));
                                                $successType = 'found';
                                                $message = 'Succesfully imported';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonJob($successType, $countUpdatedDataFromCsvToDb, $message, $_status));
                                            }
                                            else
                                            {
                                                $this->deleteFileFromLocalPath($externalCsvFile);
                                                $inputCompleteProcessInput['import_job_id']= $ImportIdentifier;
                                                $inputCompleteProcessInput['errors']= json_encode($countUpdatedDataFromCsvToDb);
                                                $inputCompleteProcessInput['status']= 4;
                                                $inputCompleteProcessInput['updated_at']= now()->toDateTimeString();
                                                $this->run(new UpdateImportProcessJob($inputCompleteProcessInput));
                                                $errorType = 'validation_error';
                                                $message = 'You have uploaded an older supported format of the CSV. Please update as per the current format';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            }    
                                        } else {
                                            $this->deleteFileFromLocalPath($externalCsvFile);
        
                                            $errorType = 'validation_error';
                                            $message = "Please select valid choice to import";
                                            $_status = 'Invalid file.';
                                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                                        }
                                    } else {
                                        $this->deleteFileFromLocalPath($externalCsvFile);
                                        $reportNodeChangesNeededForTheCsvUpload =   $this->run(new ChangeMetricsForCsvUploadJob($csvContentToArray, $organizationId, $requestingUserDetails,  $importType,  $csvHeaderAndRowToArray['column'], $metadataList, 0));
                                        if($reportNodeChangesNeededForTheCsvUpload == false){
                                            $errorType = 'validation_error';
                                            $message = 'The file contains mandatory field value blank or similar name and type for custom metadata. Please refer to the sample CSV and update the file and import again';
                                            $_status = 'custom_status_here';
                                            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                        } else {
                                            $response = explode('||', $reportNodeChangesNeededForTheCsvUpload);
                                            if($response[0] == '2'){
                                                //dd($reportNodeChangesNeededForTheCsvUpload);
                                                $errorType = 'validation_error';
                                                $message = 'The file contains : '.implode(',', json_decode($response[1], true)).' as non case metadata prefixed with case_/Case_. Please refer to sample CSV file';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            
                                            } else if($response[0] == '3'){
                                                //dd($reportNodeChangesNeededForTheCsvUpload);
                                                $errorType = 'validation_error';
                                                $message = 'You have uploaded an older supported format of the CSV. Please update as per the current format';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            
                                            } else if($response[0] == '4'){
                                                //dd($reportNodeChangesNeededForTheCsvUpload);
                                                $errorType = 'validation_error';
                                                $message = 'Could not find the parent id for nodes with the following parent id : '.implode(',', json_decode($response[1], true));
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            
                                            }
                                            else if($response[0] == '5'){
                                                //dd($reportNodeChangesNeededForTheCsvUpload);
                                                $errorType = 'validation_error';
                                                $message = 'You have duplicate node ids at row : '.implode(',', json_decode($response[1], true));
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            
                                            }
                                            else if($response[0] == '6'){
                                                //dd($reportNodeChangesNeededForTheCsvUpload);
                                                $errorType = 'validation_error';
                                                $message = 'Could not import this file as node id and parent id is same at row number : '.implode(',', json_decode($response[1], true));
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
                                            
                                            }
                                             else {
                                                $successType = 'found';
                                                $message = 'Data found';
                                                $_status = 'custom_status_here';
                                                return $this->run(new RespondWithJsonJob($successType, json_decode($response[1
                                            ]), $message, $_status));
                                            
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                    }
                }
                else {
                    $errorType = 'validation_error';
                    $message = !empty($validateCsvFile['message']["csvFile"]["file"])? empty($validateCsvFile['message']["csvFile"]["file"]) : "Please provide a valid csv file";
                    $_status = 'Invalid file.';
                    return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
                }
            } else {
                $errorType = 'validation_error';
                $message = "Please provide a file to import";
                $_status = 'Invalid file.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));  
            }
        }
        catch(\Exception $ex){
        $errorType = 'internal_error';
        $message = $this->createErrorMessageFromException($ex);
        $_status = 'custom_status_here';
        Log::error($ex);
        $inputCompleteProcessInput['import_job_id']= $ImportIdentifier;
        $inputCompleteProcessInput['errors']= $message;
         $inputCompleteProcessInput['status']= 4;
         $inputCompleteProcessInput['updated_at']= now()->toDateTimeString();
          $this->run(new UpdateImportProcessJob($inputCompleteProcessInput));
        return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
    private function csvToArray(string $filename, int $length = 0, string $delimiter = ','): array {
        header('Content-Type: text/html; charset=utf-8');
        $baseFolderPathForStorage = storage_path('app');

        $handle = fopen($baseFolderPathForStorage.'/'.$filename, 'r');
        $hashes         =       [];
        $values         =       [];
        $resultFromCSV  =       [];
        $header         =       null;
        $headerUnique   =       null;
    
        if (!$handle) {
            return $values;
        }
    
        $header = fgetcsv($handle, $length, $delimiter);
    
        if (!$header) {
            return $values;
        }
    
        //$headerUnique = unique_columns($header);
        $header = preg_replace('/[\r\n]/','', $header);
        $header = str_replace('?', '', array_map("utf8_decode", $header));
        $header[0] = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $header[0]);
        $indexForCsv = 0;
        while (false !== ($data = fgetcsv($handle, $length, $delimiter))) {
            //$data = array_map("utf8_decode", $data);
            $hash = md5(serialize($data));
            
            if (!isset($hashes[$hash])) {
                $hashes[$hash]  = true;

                $lastIndexOfHeader  = sizeOf($header) - 1;
                $lastIndexOfData    = sizeOf($data) - 1;

                if($lastIndexOfData > $lastIndexOfHeader){
                    $keyIndex   =   $lastIndexOfHeader + 1;
                    while($keyIndex <= $lastIndexOfData){
                        unset($data[$keyIndex]);
                        $keyIndex++;
                    }
                }

                if(count($header) != count($data)) {
                    
                    $key =  0;
                    while($key < count($header)){
                        if(!(array_key_exists($key, $data))){
                            $data[$key] = '';
                        }
                        $key++;
                    }
                }
                
                /* foreach($data as $key => $valueFromCsv){
                    if($key > 1){
                        //$valueFromCsv = mb_convert_encoding($valueFromCsv, "UTF-8", "ASCII");
                        /* echo $key." and ".$valueFromCsv;
                        echo "\n"; 
                    }

                    $resultFromCSV[$key] = $valueFromCsv;
                } */
                $values[]  = array_combine($header, $data);
            }
        }
        $result = ['column' => $header, 'row' => $values];
        fclose($handle);
    
        return $result;
    }
}
