<?php
namespace App\Services\API\Features;

use App\Domains\Caching\Events\CachingEvent;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Note\Jobs\ValidateNoteIdJob;
use App\Domains\Note\Jobs\ReorderNoteJob;
use App\Domains\Helper\Jobs\MessageArrayToStringHelperJob;
use Log;

class ReorderNotesFeature extends Feature
{
    public function handle(Request $request)
    {

        try {
            $requestData = $request->all();
            $organizationIdentifier = $requestData['auth_user']['organization_id'];
            $updatedNote = $this->run(new ReorderNoteJob($organizationIdentifier, $requestData));
            // caching
            $eventType1 = config("event_activity")["Cache_Activity"]["NOTES_GETALL"];
            $eventDetail = [
                'organization_id' => $organizationIdentifier,
                'event_type' => $eventType1,
                'requestUserDetails' => $request->input("auth_user"),
                "beforeEventRawData" => [],
                'afterEventRawData' => ''
            ];
            event(new CachingEvent($eventDetail)); // notification ends here
            $response = $updatedNote;
            $successType = 'found';
            $message = 'Note ordered.';
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

        } catch (\Exception $ex) {
            //dd($ex);
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
