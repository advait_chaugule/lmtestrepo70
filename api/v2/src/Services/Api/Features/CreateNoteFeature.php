<?php
namespace App\Services\Api\Features;

use App\Domains\Caching\Events\CachingEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Note\Jobs\ValidateNoteInputJob;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Note\Jobs\CreateNoteJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use Log;

class CreateNoteFeature extends Feature
{
    use UuidHelperTrait,ArrayHelper, StringHelper;
    public function handle(Request $request)
    {
        try{
            $requestData = $request->all();
            $inputData = ['title' => $requestData['title'],
                        'description' => $requestData['description'],
                        'note_status' => $requestData['note_status'],
                        'organization_id' => $requestData['auth_user']['organization_id']];

            $validateNoteData = $this->run(new ValidateNoteInputJob($inputData));
            if($validateNoteData===true) {
                $noteId = $this->createUniversalUniqueIdentifier();
                $noteTitle = Input::get('title');
                $noteDescription = Input::get('description');
                $noteStatus = Input::get('note_status');
                $organizationId = $inputData['organization_id'];
                $inputData['note_id']     = $noteId;
                $inputData['title']       = $noteTitle;
                $inputData['note_status'] = $noteStatus;
                $inputData['description'] = $noteDescription;
                $inputData['created_by']  = $requestData['auth_user']['user_id'];
               $inputData['note_order'] =  $this->getMaxOrder($inputData['organization_id']);
                $noteCreated = $this->run(new CreateNoteJob($inputData));
                // caching
                $eventType1              = config("event_activity")["Cache_Activity"]["NOTES_GETALL"];
                $eventDetail = [
                    'organization_id'    => $organizationId,
                    'event_type'         => $eventType1,
                    'requestUserDetails' => $request->input("auth_user"),
                    "beforeEventRawData" => [],
                    'afterEventRawData'  => ''
                ];
                event(new CachingEvent($eventDetail)); // notification ends here

                $resultData = $noteCreated;

                $successType = 'created';
                $message = 'Note created successfully.';
                return $this->run(new RespondWithJsonJob($successType, $resultData, $message));
            }
            else{
                $errorType = 'validation_error';
                // we can extract the validation message(s) from $validationStatus which has been passed from the validate job
                $message = $this->messageArrayToString($validateNoteData);
               // $message = 'Please provide valid inputs.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message));
            }
       }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }

    public function getMaxOrder($organizationId){
        $getOrder = DB::select(  DB::raw("SELECT IFNULL((MAX(note_order)+1),1) as no FROM acmt_notes WHERE is_deleted=0 AND organization_id='".$organizationId."'"));

        if(isset( $getOrder[0])){
        $return=  $getOrder[0]->no;
      }else{
            $return=1;
      }

        return $return;
    }

}
