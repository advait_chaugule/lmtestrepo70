<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Organization\Jobs\ValidateUpdateOrgInputJob;
use App\Domains\Organization\Jobs\CheckOrganizationIsDeletedJob;
use App\Domains\Organization\Jobs\UpdateOrganizationByIdJob;
use App\Domains\Organization\Jobs\GetOrganizationByIdJob;
use App\Domains\Organization\Jobs\CheckOrganizationIsDefaultJob;
use Illuminate\Support\Facades\Log;

class UpdateOrganizationByIdFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestUserDetails = $request->input("auth_user");

            $errorType = 'validation_error';
            $rmessage = 'You do not have priviledge to update tenant.';
            if($requestUserDetails['is_super_admin'] == 1)
            {
                $organizationId = $request->input('organization_id');
                $organizationName = $request->input('organization_name');
                $organizationStatus = $request->input('status');
                
                $orgDetails = ['organization_id' => $organizationId, 'name' => trim($organizationName), 'is_active' => $organizationStatus,'user_id'=>$requestUserDetails['user_id']];
                $validateOrgStatus = $this->run(new ValidateUpdateOrgInputJob($orgDetails));
                
                $errorType = 'validation_error';
                $rmessage = isset($validateOrgStatus['organization_id'][0]) ? $validateOrgStatus['organization_id'][0] : $validateOrgStatus['name'][0];
                if(!empty($organizationStatus) && $organizationStatus!='0' && $organizationStatus!='1'){
                    $errorType = 'validation_error';
                    $rmessage = 'Invalid Organization Status';
                    $validateOrgStatus = false;
                }
                if($validateOrgStatus === true){
                    $checkOrgIsDeleted = $this->run(new CheckOrganizationIsDeletedJob($organizationId));
                    $errorType = 'not_found';
                    $rmessage = 'Organization is already deleted';

                    if($checkOrgIsDeleted === false){
						// Do not allow default org to be deactivated
                        $checkOrgIsDefault = $this->run(new CheckOrganizationIsDefaultJob($organizationId));
                        $errorType = 'not_found';
                        $rmessage = 'You cannot change status of default organization';
                        if($checkOrgIsDefault === false){
                            $updateOrgEntity = $this->run(new UpdateOrganizationByIdJob($orgDetails));                        
                            $errorType = 'error';
                            $rmessage = 'Organization Details could not be updated.';

                            if($updateOrgEntity === true){
                                $response = $this->run(new GetOrganizationByIdJob($organizationId));
                                $successType = 'found';
                                $rmessage = 'Data found.';
                                return $this->run(new RespondWithJsonJob($successType, $response, $rmessage));
                            }
                        }                        
                    }
                }
            }
            // Modified header to send header with status 200 for all errors & remove unused variable $_status
            // To identify validation error, we have sent ['result' => 'exist']
            return $this->run(new RespondWithJsonJob($errorType, ['result' => 'exist'], $rmessage));
        }
        catch(\Exception $ex){
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message));
        }
    }
}
