<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\ValidateDocumentForATenantJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;

use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV3Job;
use App\Domains\Caching\Jobs\SetToCacheJob;

use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use Log;

class SetPublishDocumentToCacheFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            $requestData            =   $request->all();
            $documentIdentifier     =   $request->route('document_id');
            $requestingUserDetail   =   $requestData['auth_user'];
            $taxonomySetType        =   $requestData['type'];
            
            $validateDocument   =   $this->run(new ValidateDocumentByIdJob(['document_id'   =>  $documentIdentifier]));

            if($validateDocument == true) {
                $validateDocumentForTheTenant   =   $this->run(new ValidateDocumentForATenantJob($documentIdentifier, $requestingUserDetail['organization_id']));

                if($validateDocumentForTheTenant == true) {
                    $validateDocumentIsDeleted  =   $this->run(new CheckDocumentIsDeleteJob($documentIdentifier));

                    if(!$validateDocumentIsDeleted == true) {
                        
                        $taxonomyHierarchy = $this->run(new GetFlattenedTaxonomyTreeV3Job($documentIdentifier, $requestingUserDetail));
                        
                        if($taxonomySetType=='hierarchy'){
                            $dataToSend = $taxonomyHierarchy;
                        }else{
                            foreach($taxonomyHierarchy['nodes'] as $node) {

                                $itemId = $node['id'];
                                $validationStatus = $this->run(new ValidateItemByIdJob([ 'item_id' => $itemId ]));

                                if($validationStatus===true){

                                    $itemIsDeleted = $this->run(new CheckItemIsDeletedJob($itemId));
                                    
                                    if(!$itemIsDeleted) {
                                        $taxonomyHierarchy['details'][] = $this->run(new GetCFItemDetailsJob($itemId, $requestingUserDetail['organization_id']));
                                    }
                                }
                            }
                            $dataToSend['details'] = $taxonomyHierarchy['details'];
                        }
                        $keyInfo = array('identifier'=>$documentIdentifier, 'prefix'=>'doc_'.$taxonomySetType);
                        $status = $this->run(new SetToCacheJob($dataToSend, $keyInfo));
                        if(is_array($status) && $status['status']=="error"){
                            // handle failure, data already exists
                        }
                        $status = $status['message'];
                        
                        $successType = 'found';
                        $message = 'Data found.';
                        $_status = 'custom_status_here';
                        return $this->run(new RespondWithJsonJob($successType, $status, $message, $_status));


                    } else {
                        $errorType = 'validation_error';
                        $message = "This taxonomy is deleted";
                        $_status = '';
                        return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                    }
                } else {
                    $errorType = 'validation_error';
                    $message = "This taxonomy does not belong to this organization";
                    $_status = '';
                    return $this->run(new RespondWithJsonJob($errorType, [], $message, $_status));
                }
            } else {
                $errorType = 'validation_error';
                $message = "Please select a valid taxonomy";
                $_status = 'Invalid Taxonomy.';
                return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
