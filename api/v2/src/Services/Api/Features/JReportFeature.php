<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\JReport\Jobs\JReportJob;
use App\Domains\JReport\Jobs\JReportTaxonomyJob;
use App\Domains\JReport\Jobs\JReportConnectionJob;
use App\Domains\JReport\Jobs\SetContentTypeJob;
use App\Domains\JReport\Jobs\JReportUserCommentJob;
use App\Domains\JReport\Jobs\JReportUserDemographicJob;
use App\Domains\JReport\Jobs\JReportAssociationCoverageJob;

class JReportFeature extends Feature
{
    public function handle(Request $request)
    {        
        $connection= $this->run(new JReportConnectionJob());
        $input = $request->input();

        $organization_id = $request->input('organization_id');
        $document_id = $request->input('document_id');  
        $file_type=$request->input('file_type');
        $report_type=  $request->input('report_type');
        $from_date=  $request->input('from_date');
        $to_date=  $request->input('to_date');
        $target_taxonomy=  $request->input('target_taxonomy');
        $source_taxonomy=  $request->input('source_taxonomy');

        if(empty($from_date))
            $from_date='2018-01-01';
        if(empty($to_date))
            $to_date='2030-01-01';
        
        //validation job for file_type
        if($report_type=='user_comment')
            $content=$this->run(new JReportUserCommentJob($connection,$file_type,$report_type,$organization_id,$document_id,$from_date,$to_date));
        else if($report_type=='user_demographic')
            $content=$this->run(new JReportUserDemographicJob($connection,$file_type,$report_type,$organization_id,$from_date,$to_date));
        else if($report_type=='association_coverage')
            $content=$this->run(new JReportAssociationCoverageJob($connection,$file_type,$report_type,$organization_id,$target_taxonomy,$source_taxonomy));
        else
            $content=$this->run(new JReportTaxonomyJob($connection,$file_type,$report_type,$organization_id,$document_id));    
        

        $file_name=time();
        $this->run(new SetContentTypeJob($file_type,$content,$file_name)); 
        
        
    }
}