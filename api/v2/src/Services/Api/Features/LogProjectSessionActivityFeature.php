<?php
namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Domains\Project\Events\SessionStartedEvent;
use Log;

class LogProjectSessionActivityFeature extends Feature
{
    public function handle(Request $request)
    {
        try {
            //Get project identifier from route
            $projectId = $request->route('project_id');
            $input = ['project_id' => $projectId];
            // raise event to track activities for reporting
            $requestUserDetails = $request->input('auth_user');
            $eventData = [
                "beforeEventRawData" => (object)[],
                "afterEventRawData" => (object)$input,
                "requestUserDetails" => $requestUserDetails
            ];
            event(new SessionStartedEvent($eventData));

            $successType = 'found';
            $message = 'Project session activity logged successfully..';
            $response = "";
            $_status = 'custom_status_here';
            return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));

        } catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }
}
