<?php
namespace App\Services\Api\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use Lucid\Foundation\ServesFeaturesTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Features\CreateAndUploadTaxonomySearchDataFeature;

class CreateAndUploadTaxonomySearchData extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, ArrayHelper, StringHelper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taxonomy:create-upload-taxonomy-search-data {document_ids : Comma separated document_id(s)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script will create and upload search data for taxonomy(s).';

    private $searchClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request)
    {
        $commandInputParameter = $this->argument('document_ids');
        $inputParams = $this->createArrayFromCommaeSeparatedString($commandInputParameter);
        $this->info("Command execution started.....\n");
        try {
            foreach($inputParams as $key=>$documentId) {
                $this->info("Processing Document: $documentId........");
                    $documentIdTrimmed = trim($documentId);
                    $requestInput = [ "document_ids" => $documentId ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(CreateAndUploadTaxonomySearchDataFeature::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                             $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                             $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }
}
