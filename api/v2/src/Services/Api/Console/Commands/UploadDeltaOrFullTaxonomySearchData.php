<?php
namespace App\Services\Api\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Lucid\Foundation\ServesFeaturesTrait;
use Illuminate\Support\Collection;

use App\Foundation\BaseSqsHandler;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Services\Api\Features\CreateAndUploadTaxonomySearchDataFeature as FullUpdate;
use App\Services\Api\Features\CreateAndUploadTaxonomyDeltaUpdatesSearchDataFeature as DeltaUpdate;
use App\Services\Api\Features\CreateAndUploadProjectSearchDataFeature as ProjectUpdate;

class UploadDeltaOrFullTaxonomySearchData extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, ArrayHelper, StringHelper;

    private $sqsBaseHandler;
    private $searchConfigurations;
    private $sqsUploadEvents;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $projectRepository;

    private $extractedMessagesToProcess;
    private $fullUpdateMessages;
    private $deltaUpdateMessages;
    private $projectUpdateMessages;
    private $pacingGuideUpdateMessages;

    private $sqsMessagesToDelete = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taxonomy:upload-delta-or-full-taxonomy-search-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script will process taxonomy search metadata from sqs and upload to cloud search.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setSearchConfigurations();
        $this->setSqsUploadEvents();
    }

    public function setSearchConfigurations() {
        $searchConfigurations = config('_search');
        $this->searchConfigurations = $searchConfigurations;
    }

    public function getSearchConfigurations(): array {
        return $this->searchConfigurations;
    }

    public function setSqsUploadEvents() {
        $sqsUploadEventsConfiguration = $this->getSearchConfigurations()['taxonomy']['sqs_upload_events'];
        $this->sqsUploadEvents = $sqsUploadEventsConfiguration;
    }

    public function getSqsUploadEvents(): array {
        return $this->sqsUploadEvents;
    }

    /****************************************Setters and getters for sqs messages************************************************/

    public function setExtractedMessagesToProcess(array $data) {
        $this->extractedMessagesToProcess = $data;
    }

    public function getExtractedMessagesToProcess(): array {
        // return
        // [
        //   [
        //     "MessageId" => "c54a5a1d-0738-43c8-b1c6-bbd8edb50bf6",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"46601278-44bd-4a6c-8e31-a9a48a848731","event_type":"UpdateDocument"}',
        //     "_parsed_event" => "UpdateDocument",
        //     "_parsed_type_id" => "46601278-44bd-4a6c-8e31-a9a48a848731"
        //   ],
        //   [
        //     "MessageId" => "c54a5a1d-0738-43c8-b1c6-bbd8edb50bf6",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"06601278-44bd-4a6c-8e31-a9a48a848732","event_type":"UpdateItem"}',
        //     "_parsed_event" => "UpdateItem",
        //     "_parsed_type_id" => "06601278-44bd-4a6c-8e31-a9a48a848732"
        //   ],
        //   [
        //     "MessageId" => "c54a5a1d-0738-43c8-b1c6-bbd8edbfirst",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"76601278-44bd-4a6c-8e31-a9a48a848733","event_type":"NewDocument"}',
        //     "_parsed_event" => "NewDocument",
        //     "_parsed_type_id" => "76601278-44bd-4a6c-8e31-a9a48a848733"
        //   ],
        //   [
        //     "MessageId" => "d54a5a1d-0738-43c8-b1c6-bbd8edbfirst",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"86601278-44bd-4a6c-8e31-a9a48a848733","event_type":"NewDocument"}',
        //     "_parsed_event" => "NewDocument",
        //     "_parsed_type_id" => "86601278-44bd-4a6c-8e31-a9a48a848733"
        //   ],
        //   [
        //     "MessageId" => "c54a5a1d-0738-43c8-b1c6-bbd8edbsecond",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"99601278-44bd-4a6c-8e31-a9a48a848734","event_type":"NewProject"}',
        //     "_parsed_event" => "NewProject",
        //     "_parsed_type_id" => "99601278-44bd-4a6c-8e31-a9a48a848734"
        //   ],
        //   [
        //     "MessageId" => "954a5a1d-0738-43c8-b1c6-bbd8edbsecond",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"610fc381-ef11-4a6c-8a97-a6cef219e374","event_type":"NewProject"}',
        //     "_parsed_event" => "NewProject",
        //     "_parsed_type_id" => "610fc381-ef11-4a6c-8a97-a6cef219e374"
        //   ],
        //   [
        //     "MessageId" => "954a5a1d-0738-43c8-b1c6-bbd8edbsecond",
        //     "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //     "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //     "Body" => '{"type_id":"49601278-44bd-4a6c-8e31-a9a48a848734","event_type":"NewProject"}',
        //     "_parsed_event" => "NewProject",
        //     "_parsed_type_id" => "49601278-44bd-4a6c-8e31-a9a48a848734"
        //   ]
        // ];

        // return [    
        //       [
        //         "MessageId" => "954a5a1d-0738-43c8-b1c6-bbd8edbsecond",
        //         "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //         "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //         "Body" => '{"type_id":"89601278-44bd-4a6c-8e31-a9a48a848734","event_type":"NewPacingGuide"}',
        //         "_parsed_event" => "NewPacingGuide",
        //         "_parsed_type_id" => "89601278-44bd-4a6c-8e31-a9a48a848734"
        //       ],
        //       [
        //         "MessageId" => "854a5a1d-0738-43c8-b1c6-bbd8edbsecond",
        //         "ReceiptHandle" => "QasyrDS7yEtiKDJBQjHkExOFbj",
        //         "MD5OfBody" => "2416fb3dab62cbc939538b281791d3cc",
        //         "Body" => '{"type_id":"99601278-44bd-4a6c-8e31-a9a48a848734","event_type":"NewPacingGuide"}',
        //         "_parsed_event" => "NewPacingGuide",
        //         "_parsed_type_id" => "99601278-44bd-4a6c-8e31-a9a48a848734"
        //       ]
        //     ];
        return $this->extractedMessagesToProcess;
    }

    public function setFullUpdateMessages(array $data) {
        $this->fullUpdateMessages = $data;
    }

    public function getFullUpdateMessages(): array {
        return $this->fullUpdateMessages;
    }

    public function setDeltaUpdateMessages(array $data) {
        $this->deltaUpdateMessages = $data;
    }

    public function getDeltaUpdateMessages(): array {
        return $this->deltaUpdateMessages;
    }

    public function setProjectUpdateMessages(array $data) {
        $this->projectUpdateMessages = $data;
    }

    public function getProjectUpdateMessages(): array {
        return $this->projectUpdateMessages;
    }

    public function setPacingGuideUpdateMessages(array $data) {
        $this->pacingGuideUpdateMessages = $data;
    }

    public function getPacingGuideUpdateMessages(): array {
        return $this->pacingGuideUpdateMessages;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle( BaseSqsHandler $sqsBaseHandler, Request $request )
    {
        $this->info("Command execution started.....\n");

        $this->sqsBaseHandler = $sqsBaseHandler;

        try {
            $this->fetchAndSetTaxonomySearchRelatedMessages();
            $this->extractAndSetFullUpdateMessages();
            $this->extractAndSetDeltaUpdateMessages();
            $this->extractAndSetProjectUpdateMessages();
            // $this->extractAndSetPacingGuideUpdateMessages();

            $this->processAndUploadDeltaTaxonomyUpdatesToCloudSearch($request);
            $this->processAndUploadFullTaxonomyUpdatesToCloudSearch($request);
            $this->processAndUploadProjectUpdatesToCloudSearch($request);
            // $this->processAndUploadPacingGuideUpdatesToCloudSearch($request);
            $this->deleteFetchedMessagesFromSqs();
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }

        $this->info("\n Command execution complete.....");
    }

    private function fetchAndSetTaxonomySearchRelatedMessages() {
        // for each iteration, 10 messages will be fetched
        $iterationCount = 100;
        $extractedMessagesToProcess = [];
        $queueUrl = $this->sqsBaseHandler->getSqsQueueUrl();
        $sqsClient = $this->sqsBaseHandler->getSqsClient();
        $sqsClientParamsToFetchMessages = [
            'MaxNumberOfMessages' => 10, // maximum number of messages that can be returned in a single call to sqs
            'MessageAttributeNames' => ['All'],
            'QueueUrl' => $queueUrl, // REQUIRED
            'WaitTimeSeconds' => 0
        ];
        $maxRetryCount = 0;
        for($start=0; $start <= $iterationCount; $start++) {

            // if no messages are received consecutively 6 times then break the loop
            if($maxRetryCount===6) {
                break;
            }

            $result = $sqsClient->receiveMessage($sqsClientParamsToFetchMessages);
            $messagesResponse = $result->get('Messages');
            if (!empty($messagesResponse) && is_array($messagesResponse)) {
                foreach($messagesResponse as $message) {
                    $messageDetails = $this->_helperToParseAndReturnMessageDetails($message);
                    $taxonomySearchStatus = $messageDetails['_taxonomy_search_status'];
                    if($taxonomySearchStatus!==false) {
                        // push sqs messages to delete that are related to taxonomy search only
                        array_push($this->sqsMessagesToDelete, $message);

                        // override the existing message with the extracted information
                        $message['_parsed_event'] = $messageDetails['_parsed_event'];
                        $message['_parsed_type_id'] = $messageDetails['_parsed_type_id'];
                        $extractedMessagesToProcess[] = $message;
                    }
                }
                // reset the max retry count when messages are received
                $maxRetryCount = 0;
            }
            else {
                // increment the max retry count when no messages received
                $maxRetryCount++;
            }
        }
        $this->setExtractedMessagesToProcess($extractedMessagesToProcess);
    }

    private function extractAndSetFullUpdateMessages() {
        $uploadEvents = $this->getSqsUploadEvents();
        $fullTaxonomyUpdateSqsEvent = $uploadEvents['taxonomy_created'];
        $sqsMessagesCollection = collect($this->getExtractedMessagesToProcess());
        $fullUpdateMessageCollection = $sqsMessagesCollection->where('_parsed_event', $fullTaxonomyUpdateSqsEvent);
        $fullUpdateMessages = $fullUpdateMessageCollection->toArray();
        $this->setFullUpdateMessages($fullUpdateMessages);
    }

    private function extractAndSetDeltaUpdateMessages() {
        $uploadEvents = $this->getSqsUploadEvents();
        $itemDeltaUpdateEvent = $uploadEvents['update_item'];
        $documentDeltaUpdateEvent = $uploadEvents['update_document'];
        $sqsMessagesCollection = collect($this->getExtractedMessagesToProcess());
        $deltaUpdateMessageCollection = $sqsMessagesCollection->whereIn('_parsed_event', [$itemDeltaUpdateEvent, $documentDeltaUpdateEvent]);
        $deltaUpdateMessages = $deltaUpdateMessageCollection->toArray();
        $this->setDeltaUpdateMessages($deltaUpdateMessages);
    }

    private function extractAndSetProjectUpdateMessages() {
        $uploadEvents = $this->getSqsUploadEvents();
        $projectUpdateEvent = $uploadEvents['update_project'];
        $sqsMessagesCollection = collect($this->getExtractedMessagesToProcess());
        $projectUpdateMessageCollection = $sqsMessagesCollection->where('_parsed_event', $projectUpdateEvent);
        $projectUpdateMessages = $projectUpdateMessageCollection->toArray();
        $this->setProjectUpdateMessages($projectUpdateMessages);
    }

    private function extractAndSetPacingGuideUpdateMessages() {
        $uploadEvents = $this->getSqsUploadEvents();
        $pacingGuideUpdateEvent = $uploadEvents['update_pacing_guide'];
        $sqsMessagesCollection = collect($this->getExtractedMessagesToProcess());
        $pacingGuideUpdateMessageCollection = $sqsMessagesCollection->where('_parsed_event', $pacingGuideUpdateEvent);
        $pacingGuideUpdateMessages = $pacingGuideUpdateMessageCollection->toArray();
        $this->setPacingGuideUpdateMessages($pacingGuideUpdateMessages);
    }

    private function processAndUploadDeltaTaxonomyUpdatesToCloudSearch(Request $request) {
        $this->info("Processing Of Delta Updates Started........");
        
        $uploadEvents = $this->getSqsUploadEvents();
        $itemDeltaUpdateEvent = $uploadEvents['update_item'];
        $documentDeltaUpdateEvent = $uploadEvents['update_document'];
        $deltaUpdateMessageCollection = collect($this->getDeltaUpdateMessages());

        $itemIdsToProcess = $deltaUpdateMessageCollection->where('_parsed_event', $itemDeltaUpdateEvent)
                                                         ->unique('_parsed_type_id')
                                                         ->pluck('_parsed_type_id')
                                                         ->toArray();
        $commaSeparatedItemIds = $this->arrayContentToCommaeSeparatedString($itemIdsToProcess);

        $documentIdsToProcess = $deltaUpdateMessageCollection->where('_parsed_event', $documentDeltaUpdateEvent)
                                                             ->unique('_parsed_type_id')
                                                             ->pluck('_parsed_type_id')
                                                             ->toArray();
        $commaSeparatedDocumentIds = $this->arrayContentToCommaeSeparatedString($documentIdsToProcess);
        if($commaSeparatedItemIds!=='' || $commaSeparatedDocumentIds!==''){
            // prepare the request data for the feature to work
            $requestInput = [ "item_ids" => $commaSeparatedItemIds, "document_ids" => $commaSeparatedDocumentIds ];
            $request->replace($requestInput);
            // returns Illuminate\Http\JsonResponse 
            $featureResponse = $this->serve(DeltaUpdate::class);
            $responseBody = $featureResponse->getData(true);
            $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
            $dataTypeOfResponseMessage = gettype($responseMessage);
            $parsedResponseMessage = ($dataTypeOfResponseMessage==='array' ? 
                                     $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                     $responseMessage). "\n";
        }
        else {
            $parsedResponseMessage = "No relevant message found in sqs queue.";
        }

        $this->info($parsedResponseMessage);
        $this->info("Processing Of Delta Updates Ended........\n");
    }

    private function processAndUploadFullTaxonomyUpdatesToCloudSearch(Request $request) {
        $this->info("Processing Of Full Taxonomy Updates Started........");

        $sqsMessageCollection = collect($this->getFullUpdateMessages());
        $identifiersForFullTaxonomyUpdate = $sqsMessageCollection->unique('_parsed_type_id')->pluck('_parsed_type_id')->toArray();

        if(!empty($identifiersForFullTaxonomyUpdate)) {
            foreach($identifiersForFullTaxonomyUpdate as $documentId) {
                $this->info("Processing Document: $documentId........");
                    // prepare the request data for the feature to work
                    $documentIdTrimmed = trim($documentId);
                    $requestInput = [ "document_ids" => $documentId ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(FullUpdate::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                                $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                                $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        else {
            $this->info("No relevant message found in sqs queue.");
        }

        $this->info("Processing Of Full Taxonomy Updates Ended........\n");
    }

    private function processAndUploadProjectUpdatesToCloudSearch(Request $request) {
        $this->info("Processing Of Project Updates Started........");
        $projectUpdateMessageCollection = collect($this->getProjectUpdateMessages());
        $projectIdsToProcess = $projectUpdateMessageCollection->unique('_parsed_type_id')->pluck('_parsed_type_id')->toArray();
        if(!empty($projectIdsToProcess)) {
            foreach($projectIdsToProcess as $projectId) {
                $this->info("Processing Project: $projectId........");
                    // prepare the request data for the feature to work
                    $requestInput = [ "project_ids" => $projectId ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(ProjectUpdate::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                                $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                                $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        else {
            $this->info("No relevant message found in sqs queue.");
        }
        $this->info("Processing Of Project Updates Ended........");
    }

    private function processAndUploadPacingGuideUpdatesToCloudSearch(Request $request) {
        $this->info("Processing Of Pacing Guide Updates Started........");
        $pacingGuideUpdateMessageCollection = collect($this->getPacingGuideUpdateMessages());
        $pacingGuideIdsToProcess = $pacingGuideUpdateMessageCollection->unique('_parsed_type_id')->pluck('_parsed_type_id')->toArray();
        if(!empty($pacingGuideIdsToProcess)) {
            foreach($pacingGuideIdsToProcess as $pacingGuideId) {
                $this->info("Processing Pacing guide: $pacingGuideId........");
                    // prepare the request data for the feature to work
                    $requestInput = [ "project_ids" => $pacingGuideId ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(ProjectUpdate::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                                $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                                $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        else {
            $this->info("No relevant message found in sqs queue.");
        }
        $this->info("Processing Of Pacing Guide Updates Ended........");
    }

    private function deleteFetchedMessagesFromSqs() {
        $sqsClient = $this->sqsBaseHandler->getSqsClient();
        $queueUrl = $this->sqsBaseHandler->getSqsQueueUrl();
        foreach($this->sqsMessagesToDelete as $messageToDelete) {
            if(!empty($messageToDelete['ReceiptHandle'])) {
                $messageId = $messageToDelete['MessageId'];
                $receiptHandle = $messageToDelete['ReceiptHandle'];
                $sqsClientParamsToDeleteMessage = [
                    'QueueUrl' => $queueUrl, // REQUIRED
                    'ReceiptHandle' => $receiptHandle // REQUIRED
                ];
                $result = $sqsClient->deleteMessage($sqsClientParamsToDeleteMessage);
                $this->info("Message ". $messageId ." deleted from sqs \n");
            }
        }
    }

    private function _helperToParseAndReturnMessageDetails(array $sqsMessage): array {
        $dataToReturn = [
            "_taxonomy_search_status" => false,
            "_parsed_event" => '',
            "_parsed_type_id" => ''
        ];
        if(!empty($sqsMessage['Body'])) {
            $messageBody = json_decode($sqsMessage["Body"], true);
            $eventType = !empty($messageBody['event_type']) ? $messageBody['event_type'] : "";
            $typeId = !empty($messageBody['type_id']) ? $messageBody['type_id'] : "";
            $sqsUploadEventList = array_values($this->getSqsUploadEvents());
            $status = in_array($eventType, $sqsUploadEventList);
            $dataToReturn = [
                "_taxonomy_search_status" => $status,
                "_parsed_event" => $eventType,
                "_parsed_type_id" => $typeId
            ];
        }
        return $dataToReturn;
    }
}
