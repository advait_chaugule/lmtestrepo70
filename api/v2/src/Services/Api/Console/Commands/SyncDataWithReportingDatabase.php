<?php
namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Database\Eloquent\Collection;

use App\Services\Api\Traits\ErrorMessageHelper;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

use App\Data\Repositories\Contracts\DimUserRepositoryInterface;
use App\Data\Repositories\Contracts\DimProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DimItemRepositoryInterface;

class SyncDataWithReportingDatabase extends Command
{
    use ErrorMessageHelper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event-tracking:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will sync data from acmt application database with reporting database.';

    private $userRepository;
    private $projectRepository;
    private $itemRepository;
    private $dimUserRepository;
    private $dimProjectRepository;
    private $dimItemRepository;

    private $chunkCount;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        ProjectRepositoryInterface $projectRepository,
        ItemRepositoryInterface $itemRepository,
        DimUserRepositoryInterface $dimUserRepository,
        DimProjectRepositoryInterface $dimProjectRepository,
        DimItemRepositoryInterface $dimItemRepository
    )
    {
        parent::__construct();

        // set report application database repositories
        $this->userRepository = $userRepository;
        $this->projectRepository = $projectRepository;
        $this->itemRepository = $itemRepository;

        // set report database repositories
        $this->dimUserRepository = $dimUserRepository;
        $this->dimProjectRepository = $dimProjectRepository;
        $this->dimItemRepository = $dimItemRepository;

        // set the record count which will be fetched as one single chunk and processed
        $this->chunkCount = config('event_activity.sync_batch_count');
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->deleteDimTableData();
            $this->startSyncing();
        }
        catch(\Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            dd($errorMessage);
        }
    }

    private function deleteDimTableData() {
        $this->info("Flushing old data.....");
        $this->dimUserRepository->deleteAllRecords();
        $this->dimProjectRepository->deleteAllRecords();
        $this->dimItemRepository->deleteAllRecords();
    }

    private function startSyncing() {
        $this->info("Syncing started.....");

        $this->syncUserRecords();

        $this->syncProjectRecords();

        $this->syncItemRecords();

        $this->info("Syncing ended.....");
    }

    private function syncUserRecords() {
        $userModel = $this->userRepository->getModel();
        // start fetching chunk of records
        $userModel::chunk($this->chunkCount, function ($users) {
            $this->processDimUserBatch($users);
        });
    }

    private function processDimUserBatch(Collection $users) {
        $parsedDimUserData = [];
        foreach($users as $user) {
            $parsedDimUserData[] = [
                "user_id" => $user->user_id,
                "first_name" => $user->first_name,
                "last_name" => $user->last_name
            ];
        } 
        $this->dimUserRepository->saveMultipleRecords($parsedDimUserData);
    }

    private function syncProjectRecords() {
        $projectModel = $this->projectRepository->getModel();
        // start fetching chunk of records
        $projectModel::chunk($this->chunkCount, function ($projects) {
            $this->processDimProjectBatch($projects);
        });
    }

    private function processDimProjectBatch(Collection $projects) {
        $parsedDimProjectData = [];
        foreach($projects as $project) {
            $parsedDimProjectData[] = [
                "project_id" => $project->project_id,
                "project_display_name" => $project->project_name
            ];
        } 
        $this->dimProjectRepository->saveMultipleRecords($parsedDimProjectData);
    }

    private function syncItemRecords() {
        $itemModel = $this->itemRepository->getModel();
        // start fetching chunk of records
        $itemModel::chunk($this->chunkCount, function ($items) {
            $this->processDimItemBatch($items);
        });
    }

    private function processDimItemBatch(Collection $items) {
        $parsedDimItemData = [];
        foreach($items as $item) {
            $itemDisplayName = $item->human_coding_scheme ?: $item->list_enumeration;
            $parsedDimItemData[] = [
                "item_id" => $item->item_id,
                "item_display_name" => $itemDisplayName
            ];
        } 
        $this->dimItemRepository->saveMultipleRecords($parsedDimItemData);
    }
}
