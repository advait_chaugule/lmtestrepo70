<?php
namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Database\Eloquent\Collection;

use App\Services\Api\Traits\ErrorMessageHelper;

use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Aws\CloudSearchDomain\Exception\CloudSearchDomainException as Exception;

class TestCommand extends Command
{
    use ErrorMessageHelper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will be used for testing purposes.';

    private $searchClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $cloudSearchCredentials = config('cloud_search');
        $awsKey = $cloudSearchCredentials['AWS_KEY'];
        $awsSecret = $cloudSearchCredentials['AWS_SECRET'];
        $endpoint = $cloudSearchCredentials['SEARCH_ENDPOINT'];
        $version = $cloudSearchCredentials['API_VERSION'];
        $configurations = [
            'credentials' => [
                'key' => $awsKey,
                'secret' => $awsSecret,
                'token' => ''
            ],
            'endpoint' => $endpoint,
            'version' => $version
        ];
        $this->searchClient = new CloudSearchDomainClient($configurations);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Command execution started.....");
        try {
           $this->sampleToFetchSearchResult();
        //    $this->prepareAndUploadSearchDocuments();
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            dd($errorMessage);
        }
        $this->info("Command execution complete.....");
    }

    private function sampleToFetchSearchResult() {
        $searchQuery = "test*";
        $organizationIdentifier = 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff';
        $documentIdentifier = '34df9a1c-9595-b582-e1e4-e232a3869410';
        //$staticFilterString = "organization_identifier:'$organizationIdentifier'&document_id: '$documentIdentifier'";
        $staticFilterString = "(and (term field=organization_identifier '$organizationIdentifier') (term field=document_id '$documentIdentifier'))";
        $fields = [
            "title", 
            "official_source_url", 
            "notes", 
            "publisher", 
            "description", 
            "human_coding_scheme", 
            "list_enumeration", 
            "full_statement", 
            "alternative_label", 
            "abbreviated_statement",
            "project_name",
            "taxonomy_name",
            "node_type"
        ];
        $searchFieldsJsonString = json_encode(['fields' => $fields]);
        $sampleSearchQueryParameters = [
            'query' => $searchQuery,
            'filterQuery' => $staticFilterString,
            'queryOptions' => $searchFieldsJsonString,
            'partial' => true

            // 'cursor' => '<string>',
            // 'expr' => '<string>',
            // 'facet' => '<string>',
            // 'filterQuery' => '<string>',
            // 'highlight' => '<string>',
            // 'partial' => true || false,
            // 'query' => '<string>', // REQUIRED
            // 'queryOptions' => '<string>',
            // 'queryParser' => 'simple|structured|lucene|dismax',
            // 'return' => '<string>',
            // 'size' => <integer>,
            // 'sort' => '<string>',
            // 'start' => <integer>,
            // 'stats' => '<string>',

        ];
        $searchResult = $this->searchClient->search($sampleSearchQueryParameters);
        $searchHitsCollection = $searchResult->get('hits');
        dd($searchHitsCollection['found']);
    }

    private function prepareAndUploadSearchDocuments() {
        $sampleDocument[] = [
            'fields' => [
              'identifier' => 'b6720ed3-701f-4155-8c29-700208015e97',
              'parent_identifier' => '',
              'organization_identifier' => 'f59081b9-c828-43fd-a98c-0eb54de65b4c',
              'document_id' => 'b6720ed3-701f-4155-8c29-700208015e97',
              'taxonomy_name' => 'South Carolina Professional Educator Standards update',
              'project_name' => 'Test Project',
              'title' => 'South Carolina Professional Educator Standards update',
              'official_source_url' => 'https://ed.sc.gov/scdoe/assets/file/programs-services/59/documents/ELA2015SCCCRStandards.pdf',
              'notes' => 'org 1 These Standards define what students should understand and be able to do in their study of mathematics.',
              'publisher' => 'DEMO',
              'description' => 'DEMO',
              'human_coding_scheme' => '',
              'list_enumeration' => '',
              'full_statement' => '',
              'alternative_label' => '',
              'abbreviated_statement' => '',
              'node_type' => 'cluster',
              'type' => 'document'
            ],
            'id' => 'b6720ed3-701f-4155-8c29-700208015e97',
            'type' => 'add'
        ];
        $jsonString = json_encode($sampleDocument);
        $upload =   $this->searchClient->uploadDocuments([
                        'contentType' => 'application/json', // REQUIRED
                        'documents' => $jsonString, // REQUIRED
                    ]);
        dd($upload);
    }
}