<?php
namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;
use Lucid\Foundation\ServesFeaturesTrait;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Services\Api\Features\FlushSearchDataFeature;

class FlushAllSearchData extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, StringHelper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:flush-search-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will be used for deleting all search data uploaded to cloud search.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Command execution started.....\n");
        try {
            // returns Illuminate\Http\JsonResponse 
            $featureResponse = $this->serve(FlushSearchDataFeature::class);
            $responseBody = $featureResponse->getData(true);
            $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
            $dataTypeOfResponseMessage = gettype($responseMessage);
            $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                        $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                        $responseMessage;
            $this->info($parsedResponseMessage."\n");
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }
}
