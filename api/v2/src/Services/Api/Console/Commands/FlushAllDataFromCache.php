<?php

namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;

use Lucid\Foundation\ServesFeaturesTrait;

use App\Services\Api\Features\FlushAllDataFromCacheFeature;

class FlushAllDataFromCache extends Command
{
    use ServesFeaturesTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flush:all-cache-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'script will flush all data from the linked cache instance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Command execution started.....\n");
        try {
            $featureResponse = $this->serve(FlushAllDataFromCacheFeature::class);
            $responseMsg = $featureResponse->getData(true);
            if($responseMsg["message"])
                $message = "Flushed successfully";
            else
                $message = "Flush failed";
            $this->info($message."\n");
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }
}
