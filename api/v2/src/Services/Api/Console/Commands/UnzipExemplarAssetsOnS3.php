<?php
namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Database\Eloquent\Collection;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;
use App\Data\Models\Asset;

use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ZipHelperTrait;
use App\Services\Api\Traits\FileHelper;


class UnzipExemplarAssetsOnS3 extends Command
{
    use ErrorMessageHelper, ZipHelperTrait, FileHelper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exemplar:unzip-assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will be used to unzip all exemplar assets uploaded to s3. 
                              It was designed to enable backward compatibility for exemplar asset data.
                              ##Disclaimer : GIT Tag Ref - Tag-QA-08/10/2018-3 . It must be run once before CD process starts.';

    private $assetRepository;

    private $chunkCount;

    private $s3bucketName;
    private $localBaseStoragePath;
    private $s3StorageDisk;
    private $localArchiveFolder;
    private $s3ExemplarAssetUploadFolder;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AssetRepositoryInterface $assetRepository)
    {
        parent::__construct();

        // set database repositories
        $this->assetRepository = $assetRepository;

        // set the record count which will be fetched as one single chunk and processed
        $this->chunkCount = 50;

        $assetRelatedConfigurations = config("asset");
        $this->setS3BucketName();
        $this->overrideCurrentFileSystemConfigurationWithS3Bucket();
        $this->setlocalBaseStoragePath();
        $this->setS3StorageDisk();
        $this->setLocalArchiveFolder($assetRelatedConfigurations);
        $this->setLocalStorageDisk();
        $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations);
    }

     /********************************************Setters and getters for file storage related stuff*****************************************/

    private function setS3BucketName() {
        $this->s3bucketName = env('AWS_S3_BUCKET', 'acmthost');
    }

    private function getS3BucketName() : string {
        return $this->s3bucketName;
    }

    public function overrideCurrentFileSystemConfigurationWithS3Bucket() {
        config(['filesystems.disks.s3.bucket' => $this->getS3BucketName()]);
    }

    public function setlocalBaseStoragePath() {
        $this->localBaseStoragePath = storage_path(). "/app";
    }
    
    public function getlocalBaseStoragePath(): string {
        return $this->localBaseStoragePath;
    }

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setLocalArchiveFolder(array $data) {
        $this->localArchiveFolder = $data["LOCAL"]["LOCAL_TEMPORARY_ARCHIVE_PATH"];
    }

    private function getLocalArchiveFolder(): string {
        return $this->localArchiveFolder;
    }

    private function setLocalStorageDisk() {
        $this->localStorageDisk = Storage::disk('local');
    }

    private function getLocalStorageDisk(): FilesystemAdapter {
        return $this->localStorageDisk;
    }

    private function setS3ExemplarAssetUploadFolder(array $data) {
        $s3Details = $data["S3"];
        $this->s3ExemplarAssetUploadFolder = $s3Details["MAIN_ARCHIVE_FOLDER"]."/".$s3Details["SUB_FOLDER_EXEMPLAR"];
    }

    private function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->initiateProcess();
        }
        catch(\Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            dd($errorMessage);
        }
    }

    private function initiateProcess() {
        $this->info("Unzip Process Started.....");

        // fetch chunked asset records
        $assetModel = $this->assetRepository->getModel();
        // start fetching chunk of records
        $assetModel::chunk($this->chunkCount, function ($assets) {
            $this->startAssetUnzipProcess($assets);
        });

        $this->info("Unzip Process ended.....");
    }

    private function startAssetUnzipProcess(Collection $assetCollection) {
        foreach($assetCollection as $asset) {
            if(!empty($asset->asset_target_file_name)) {
                $this->info("Processing asset.... $asset->asset_target_file_name");
                $this->unzipAsset($asset);
            }
        }
    }

    private function unzipAsset(Asset $asset) {
        $assetPackageFileName = "{$asset->asset_id}.zip";
        $pathToDownloadedArchive = $this->downloadAssetArchiveToLocalStorage($assetPackageFileName);
        if($pathToDownloadedArchive!=="" && !empty($asset->asset_target_file_name)) {
            $assetTargetFileName = $asset->asset_target_file_name;
            $pathToUnzippedContent = $this->unzipArchiveContentsToLocalStorageAndDeleteArchive($pathToDownloadedArchive);
            $unzippedDownloadedAssetPath = "{$pathToUnzippedContent}/{$assetTargetFileName}";
            $uploadedStatus = $this->uploadUnzippedAssetFileToS3($assetTargetFileName, $unzippedDownloadedAssetPath);
            if($uploadedStatus===true) {
                // delete the unzipped asset file from local storage
                $this->deleteFile($unzippedDownloadedAssetPath);
                // delete zip archive on s3
                $this->deleteArchiveAssetFromS3($assetPackageFileName);
            }
        } 
    }

    private function downloadAssetArchiveToLocalStorage(string $assetPackageName): string {
        $downloadedPath = "";
        $s3StorageDisk = $this->getS3StorageDisk();
        $s3AssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
        $s3FullPackageFilePath = $s3AssetUploadFolder. "/" . $assetPackageName;
        if($s3StorageDisk->exists($s3FullPackageFilePath)){
            $s3FullPackageFileContents= $s3StorageDisk->get($s3FullPackageFilePath);
            $localBasePath = $this->getlocalBaseStoragePath();
            $localTemporaryArchiveFolder = $this->getLocalArchiveFolder();
            $localStorageDisk = $this->getLocalStorageDisk();
            $localStoragePath = "{$localTemporaryArchiveFolder}/{$assetPackageName}";
            $downloadStatus = $localStorageDisk->put($localStoragePath, $s3FullPackageFileContents);
            $downloadedPath = $downloadStatus===true ? $localStoragePath : "";
        }
        return $downloadedPath;
    }

    private function unzipArchiveContentsToLocalStorageAndDeleteArchive(string $sourceArchivePath): string {
        $localBasePath = $this->getlocalBaseStoragePath();
        $localTemporaryArchiveFolder = $this->getLocalArchiveFolder();
        $destinationPath = "{$localBasePath}/{$localTemporaryArchiveFolder}";
        $absoluteSourceArchivePath = "{$localBasePath}/{$sourceArchivePath}";
        $this->unzipArchive($absoluteSourceArchivePath, $destinationPath);
        $this->deleteFile($absoluteSourceArchivePath);
        return $destinationPath;
    }

    private function uploadUnzippedAssetFileToS3(string $assetTargetFileName, string $unzippedDownloadedAssetPath): bool {
        $s3StorageDisk = $this->getS3StorageDisk();
        $s3ExemplarAssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
        $fileContent = $this->getContent($unzippedDownloadedAssetPath);
        $s3Destination = "{$s3ExemplarAssetUploadFolder}/{$assetTargetFileName}";
        $uploadedStatus = $s3StorageDisk->put($s3Destination, $fileContent);
        return $uploadedStatus;
    }

    private function deleteArchiveAssetFromS3(string $assetPackageName) {
        $s3StorageDisk = $this->getS3StorageDisk();
        $s3AssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
        $s3FullPackageFilePath = "{$s3AssetUploadFolder}/{$assetPackageName}";
        if($s3StorageDisk->exists($s3FullPackageFilePath)===true) {
            $s3StorageDisk->delete($s3FullPackageFilePath);
        }
    }

}
