<?php
namespace App\Services\Api\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use Lucid\Foundation\ServesFeaturesTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Services\Api\Features\CreateAndUploadTaxonomySearchDataFeature;

class CreateAndUploadAllTaxonomiesSearchData extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, ArrayHelper, StringHelper;

    private $documentRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taxonomy:create-upload-all-taxonomies-search-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script will create and upload search data for all taxonomy(s) present in the system. For one time use only.';

    private $searchClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request, DocumentRepositoryInterface $documentRepository)
    {
        // set database repository
        $this->documentRepository = $documentRepository;

        $documentIdsToProcess = $this->fetchDocumentIdsToProcess();
        $this->info("Command execution started.....\n");
        try {
            foreach($documentIdsToProcess as $key=>$documentId) {
                $this->info("Processing Document: $documentId........");
                    $documentIdTrimmed = trim($documentId);
                    $requestInput = [ "document_ids" => $documentId ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(CreateAndUploadTaxonomySearchDataFeature::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                             $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                             $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }

    private function fetchDocumentIdsToProcess(): array {
        $conditionalClause = [ 'document_type' => 1 ];
        $allDocumentIdCollection = $this->documentRepository->findByAttributes($conditionalClause)->pluck('document_id');
        return $allDocumentIdCollection->toArray();
    }
}
