<?php

namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use DateTime;
use DateTimeZone;

class AutomaticallyStopPublicReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automaticallystop:publicreview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The script will automatically stop the public review which have expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        date_default_timezone_set('America/New_York');
        $currentDate = date("Y-m-d H:i:s",time());
        
        // script to stop expired public review start
        $getPublicReviewInProgressHistoryData = DB::table('public_review_histories')
                ->where('status','2')  // replace 0 with 2 : represents in progress
                ->get()
                ->toArray();

        $publicReviewHistory = array ();
        if(!empty($getPublicReviewInProgressHistoryData))
        {
            $i=0;
            foreach($getPublicReviewInProgressHistoryData as $getPublicReviewInProgressHistoryDataK=>$getPublicReviewInProgressHistoryDataV)
            {
                
                $publicReviewHistory[$i]['document_id']=$getPublicReviewInProgressHistoryDataV->document_id;
                $publicReviewHistory[$i]['project_id']=$getPublicReviewInProgressHistoryDataV->project_id;
                $publicReviewHistory[$i]['review_number']=$getPublicReviewInProgressHistoryDataV->review_number;
                $publicReviewHistory[$i]['end_date']=$getPublicReviewInProgressHistoryDataV->end_date;
                $i=$i+1;
            }
            
            $storePublicReviewToBeClosed = array();
            $j=0;
            foreach($publicReviewHistory as $publicReviewHistoryK=>$publicReviewHistoryV)
            {
                // $date = new DateTime($publicReviewHistoryV['end_date'],new DateTimeZone('UTC'));
                // $date->setTimezone(new DateTimeZone('America/New_York'));
                // $convertedDatabaseDateToSC=$date->format('Y-m-d H:i:s');
                $convertedDatabaseDateToSC=$publicReviewHistoryV['end_date'];
                
                if(strtotime($convertedDatabaseDateToSC)<strtotime($currentDate) && $publicReviewHistoryV['end_date']!='0000-00-00 00:00:00')
                {
                    $storePublicReviewToBeClosed[$j]['document_id']=$publicReviewHistoryV['document_id'];
                    $storePublicReviewToBeClosed[$j]['project_id']=$publicReviewHistoryV['project_id'];
                    $storePublicReviewToBeClosed[$j]['review_number']=$publicReviewHistoryV['review_number'];
                }
                $j=$j+1;    

            }

            foreach($storePublicReviewToBeClosed as $storePublicReviewToBeClosedK=>$storePublicReviewToBeClosedV)
            {
                DB::table('public_review_histories')
                ->where($storePublicReviewToBeClosedV)
                ->update(['status' => 3]);

                DB::table('documents')
                ->where('document_id',$storePublicReviewToBeClosedV['document_id'])
                ->update(['adoption_status'=>2]);// Adoption Status changed to 2 for Draft as per UF-3498
            }

        }
        // script to stop expired public review end

        // script to start scheduled public review start
        $getPublicReviewScheduledHistoryData = DB::table('public_review_histories')
                ->where('status','1') 
                ->get()
                ->toArray();

        $startScheduledPublicReview = array();
        if(!empty($getPublicReviewScheduledHistoryData))
        {
            $k=0;
            foreach($getPublicReviewScheduledHistoryData as $getPublicReviewScheduledHistoryDataK=>$getPublicReviewScheduledHistoryDataV)
            {
                
                $startScheduledPublicReview[$k]['document_id']=$getPublicReviewScheduledHistoryDataV->document_id;
                $startScheduledPublicReview[$k]['project_id']=$getPublicReviewScheduledHistoryDataV->project_id;
                $startScheduledPublicReview[$k]['review_number']=$getPublicReviewScheduledHistoryDataV->review_number;
                $startScheduledPublicReview[$k]['start_date']=$getPublicReviewScheduledHistoryDataV->start_date;
                $k=$k+1;
            }
            
            $storePublicReviewToBeStarted = array();
            $l=0;
            foreach($startScheduledPublicReview as $startScheduledPublicReviewK=>$startScheduledPublicReviewV)
            {
                // $sdate = new DateTime($startScheduledPublicReviewV['start_date'],new DateTimeZone('UTC'));
                // $sdate->setTimezone(new DateTimeZone('America/New_York'));
                // $convertedDatabaseDateToSC=$sdate->format('Y-m-d H:i:s');
                $convertedDatabaseDateToSC=$startScheduledPublicReviewV['start_date'];
                
                if(strtotime($convertedDatabaseDateToSC)<strtotime($currentDate))
                {
                    $storePublicReviewToBeStarted[$l]['document_id']=$startScheduledPublicReviewV['document_id'];
                    $storePublicReviewToBeStarted[$l]['project_id']=$startScheduledPublicReviewV['project_id'];
                    $storePublicReviewToBeStarted[$l]['review_number']=$startScheduledPublicReviewV['review_number'];
                }
                $l=$l+1;    

            }

            foreach($storePublicReviewToBeStarted as $storePublicReviewToBeStartedK=>$storePublicReviewToBeStartedV)
            {
                DB::table('public_review_histories')
                ->where($storePublicReviewToBeStartedV)
                ->update(['status' => 2]);

                DB::table('documents')
                ->where('document_id',$storePublicReviewToBeStartedV['document_id'])
                ->update(['adoption_status'=>5]);
            }

        }
        // script to start scheduled public review end        
                    
    }
}
