<?php
namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Contracts\Queue\Queue;

use App\Services\Api\Traits\ErrorMessageHelper;

class ProcessEventTrackingLog extends Command
{
    use ErrorMessageHelper;

    private $queueInstance;

    private $jobCount = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event-tracking:process-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will process event tracking jobs stored in the queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Queue $queueInstance)
    {
        parent::__construct();
        $this->queueInstance = $queueInstance;
        $this->setJobsCount();
    }

    public function setJobsCount() {
        $this->jobCount = $this->queueInstance->size();
    }

    public function getJobsCount(): int {
        return $this->jobCount;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->startProcessingQueuedJobs();
        }
        catch(\Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            // dd($errorMessage);
        }
    }

    private function startProcessingQueuedJobs() {
        $jobCount = $this->getJobsCount();
        if($jobCount > 0) {
            $commandParameters = [
                '--once' => true
            ];
            for($i = 0; $i < $jobCount; $i++) {
                $displayCount = $i + 1;
                $this->info("Processing Job $displayCount");
                \Artisan::call("queue:work", $commandParameters);
            }
        }
        else {
            $this->info('No Jobs To Process');
        }
    }
}
