<?php
namespace App\Services\Api\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use Lucid\Foundation\ServesFeaturesTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Services\Api\Features\CreateAndUploadProjectSearchDataFeature;

class CreateAndUploadAllProjectSearchData extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, ArrayHelper, StringHelper;

    private $projectRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:sync-all-search-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script will create and upload search data for all project(s) present in the system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request, ProjectRepositoryInterface $projectRepository)
    {
        // set database repository
        $this->projectRepository = $projectRepository;

        $projectIdsToProcess = $this->fetchProjectIdsToProcess();
        $this->info("Command execution started.....\n");
        try {
            foreach($projectIdsToProcess as $key=>$projectId) {
                $this->info("Processing Project: $projectId........");
                    $projectIdTrimmed = trim($projectId);
                    $requestInput = [ "project_ids" => $projectIdTrimmed ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(CreateAndUploadProjectSearchDataFeature::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                             $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                             $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }

    private function fetchProjectIdsToProcess(): array {
        $conditionalClause = [ 'project_type' => 1 ];
        $allProjectIdCollection = $this->projectRepository->findByAttributes($conditionalClause)->pluck('project_id');
        return $allProjectIdCollection->toArray();
    }
}
