<?php

namespace App\Services\Api\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;

use Lucid\Foundation\ServesFeaturesTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

use App\Services\Api\Features\AddMetadataForPacingGuideFeature;

class AddMetadataForPrevCreatedTenant extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, ArrayHelper, StringHelper;

    private $organizationRepository;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:add-metadata-for-pacing-guide';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script will add Pacing Guide Type Metadata for existing organizations.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request, MetadataRepositoryInterface $metadataRepository, OrganizationRepositoryInterface $organizationRepository )
    {
        // set database repository
        $this->metadataRepository       =   $metadataRepository;
        $this->organizationRepository   =   $organizationRepository;

        $organizationIdsToProcess = $this->fetchOrganizationIdsToProcess();
        //dd($organizationIdsToProcess);
        $this->info("Command execution started.....\n");
        try {
            foreach($organizationIdsToProcess as $organizationId) {
                //echo $documentId->original_document_id;
                $this->info("Processing Organization: $organizationId........");
                    $organizationIdTrimmed = trim($organizationId);
                    $requestInput = [ "organization_id" => $organizationIdTrimmed ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(AddMetadataForPacingGuideFeature::class);
                    //dd($featureResponse);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                             $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                             $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }

    private function fetchOrganizationIdsToProcess() : array {
        $arrayOfOrganizationId      =   [];

        $organizationIdsToProcess    =   $this->organizationRepository->all();

        foreach($organizationIdsToProcess as $identifier) {
            $arrayOfOrganizationId[]  = $identifier->organization_id;   
        }

        return $arrayOfOrganizationId;
    }
}
