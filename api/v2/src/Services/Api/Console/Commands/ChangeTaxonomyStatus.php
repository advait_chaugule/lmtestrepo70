<?php
namespace App\Services\Api\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use Lucid\Foundation\ServesFeaturesTrait;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

use App\Services\Api\Features\ChangeTaxonomyStatusFeature;

class ChangeTaxonomyStatus extends Command
{
    use ServesFeaturesTrait, ErrorMessageHelper, ArrayHelper, StringHelper;

    private $documentRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taxonomy:change-old-taxonomy-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script will change the adoption status to 5 of older taxonomies which are already set for public review.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request, DocumentRepositoryInterface $documentRepository)
    {
        // set database repository
        $this->documentRepository = $documentRepository;

        $documentIdsToProcess = $this->fetchDocumentIdsToProcess();
        $this->info("Command execution started.....\n");
        try {
            foreach($documentIdsToProcess as $documentId) {
                //echo $documentId->original_document_id;
                $this->info("Processing Taxonomy: $documentId->original_document_id........");
                    $documentIdTrimmed = trim($documentId->original_document_id);
                    $requestInput = [ "document_id" => $documentIdTrimmed ];
                    $request->replace($requestInput);
                    // returns Illuminate\Http\JsonResponse 
                    $featureResponse = $this->serve(ChangeTaxonomyStatusFeature::class);
                    $responseBody = $featureResponse->getData(true);
                    $responseMessage = !empty($responseBody['message']) ? $responseBody['message'] : "No Reponse message provided.";
                    $dataTypeOfResponseMessage = gettype($responseMessage);
                    $parsedResponseMessage = $dataTypeOfResponseMessage==='array' ? 
                                             $this->arrayContentToCommaeSeparatedString($responseMessage) : 
                                             $responseMessage;
                $this->info($parsedResponseMessage."\n");
            }
        }
        catch(Exception $ex) {
            $errorMessage = $this->createErrorMessageFromException($ex);
            $this->info($errorMessage);
        }
        $this->info("Command execution complete.....");
    }

    private function fetchDocumentIdsToProcess() : array {
        $documentIdsToProcess    =   $this->documentRepository->findDocumentIdsForWhichStatusToBeChanged();

        return $documentIdsToProcess->toArray();
    }
}
