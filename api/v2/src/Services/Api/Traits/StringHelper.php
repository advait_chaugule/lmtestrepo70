<?php
namespace App\Services\Api\Traits;

trait StringHelper {

    public function delimittedStringToArray($delimittedString = "", $separator = ","): array {
        $data = explode($separator, $delimittedString);
        foreach($data as $key => $element) {
            $data[$key] = trim($element);
        }
        return $data;
    }

    public function messageArrayToString($messages = []): string {
        $messageString = "";
        foreach ($messages as $attrName=>$messages) {
            $messageString.="$attrName: ";
            foreach($messages as $key=>$message ) {
                $messageString.="#$key -> $message ";
            }
        }
        return trim($messageString);
    }

    public function validateJsonString(string $string) : bool {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    public function arrayContentToCommaeSeparatedString(array $data): string {
        return implode(",",$data);
    }

    public function validatorMessageParser($messages = []): string {
        $messageArray = [];
        foreach ($messages as $attrName=>$messages) {
            foreach($messages as $key=>$message) {
                $messageArray[] = $message;
            }
        }
        return implode($messageArray, ',');
    }

    public function arrayContentToDelimittedString(array $data, $separator = ","): string {
        return implode($separator, $data);
    }

    public function replaceSpecialCharactersFromString(string $string, string $replaceWith=''): string {
        return preg_replace('/[^A-Za-z0-9]/', $replaceWith, $string);
    }

    public function replaceMultipleSpacesWithSingleSpace(string $string): string {
        return preg_replace('!\s+!', ' ', $string);
    }

}