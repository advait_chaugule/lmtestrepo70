<?php
namespace App\Services\Api\Traits;
use DB;
//use App\Services\Api\Traits\NodeLevelTrait;


trait MetadataHelperTrait
{
    use NodeLevelTrait;
    // Function to get metadata name in an array as per internal names/metadata ids passed
    public function getmetadataName($orgnizationIdentifier,$searchArrayList, $fieldName='internal_name'){
        if(empty($searchArrayList) || empty($orgnizationIdentifier))
            return [];
        // Fetch  internal name/metadata id & metadata name as per organization id and internal name passed
        $queryData = DB::table('metadata')->select($fieldName,'name')
       ->where('organization_id','=', $orgnizationIdentifier)
       ->whereIn($fieldName,$searchArrayList)
       ->get()
       ->toArray();

       // Map internal name/metadata id to metadata name
        $fieldMetadataMapping = array_column($queryData,'name',$fieldName);

        // Loop through internal names/metadata ids and set metadata names to blank for which record is not present in db
        foreach($searchArrayList as $key=>$field){
            // No record for Node Type metadata. Hard coded if passed in array
            if($field=='node_type' || $field=='node_type_1234'){
                $fieldMetadataMapping[$field] = "Node Type";
            }
            if(!in_array($field, array_keys($fieldMetadataMapping))){
                $fieldMetadataMapping[$field] = "";
            }                            
        }        
       
        return $fieldMetadataMapping;
    }
    
    // Function to get updated metadata as per metadata in table config for configure table
    public function getUpdatedConfigMetadata($tableConfig, $organizationId, $view_type="metadata",$documentId=""){
        // Convert object to array if object passed
        $tableConfig = json_decode(json_encode($tableConfig), true);
        if($view_type == "metadata"){
            $metadataIdList = array_column($tableConfig, 'metadata_id'); // Get all internal names of config            
            // Get metadata name in an array as per internal names passed            
            $metadataNames = $this->getmetadataName($organizationId,$metadataIdList,'metadata_id');
            
            // Update name, display_name of config as per the new updated
            foreach($tableConfig as $metaKey=>$metaValue){
                $metadataId = $metaValue['metadata_id'];
                if($tableConfig[$metaKey]['display_name'] != $metadataNames[$metadataId]){
                    $tableConfig[$metaKey]['name'] = $tableConfig[$metaKey]['display_name'] = $metadataNames[$metadataId];
                }                    
            }
        }
        if($view_type == "node_type"){
            $nodeDepth = !empty($documentId) ? $this->getNodesDepth($documentId) : [];
        
            $defaultNodeTypes = [];
            if(isset($nodeDepth['sequence'])){
                $defaultNodeTypes = array_unique(array_values($nodeDepth['sequence']));
                // Get metadata id of all nodetypes in config
                $metadataIdList = [];
                foreach($tableConfig as $configKey=>$configValue){
                    $metadataIdList = array_unique(array_merge($metadataIdList, array_column($configValue['metadata'], 'metadata_id')));                                
                }
                // Get metadata name in an array as per metadata ids passed
                $metadataNames = $this->getmetadataName($organizationId,$metadataIdList,'metadata_id');
                
                foreach($tableConfig as $configKey=>$configValue){
                    // Unset node type if nodetype is renamed and remove old node type
                    if(!in_array($configValue['node_type_id'],$defaultNodeTypes)){
                        unset($tableConfig[$configKey]);
                        continue;
                    }
                    // Loop through config to update metadata's display_name, value
                    $configMetadata = $tableConfig[$configKey]['metadata'];     
                    foreach($configMetadata as $metaKey=>$metaValue){
                        $metadataId = $metaValue['metadata_id'];                                    
                        if($metadataNames[$metadataId]!=$metaValue['display_name']){
                            $configMetadata[$metaKey]['display_name'] = $metadataNames[$metadataId];
                            if(isset($configMetadata[$metaKey]['value']))
                                $configMetadata[$metaKey]['value'] = $metadataNames[$metadataId];
                        }                                    
                    }
                    // Loop through config to update selectedMetadataList's display_name, value
                    $configSelectedMetadataList = $tableConfig[$configKey]['selectedMetadataList']; 
                    foreach($configSelectedMetadataList as $metaKey=>$metaValue){
                        $metadataId = $metaValue['metadata_id'];                                    
                        if($metadataNames[$metadataId]!=$metaValue['display_name']){
                            $configSelectedMetadataList[$metaKey]['display_name'] = $metadataNames[$metadataId];
                            if(isset($configSelectedMetadataList[$metaKey]['value']))
                                $configSelectedMetadataList[$metaKey]['value'] = $metadataNames[$metadataId];
                        }                                    
                    }
                    
                    $tableConfig[$configKey]['metadata'] = $configMetadata;
                    $tableConfig[$configKey]['selectedMetadataList'] = $configSelectedMetadataList;
                }
            }                            
        }
                   
        return !empty($tableConfig) ? array_values($tableConfig) : [];
    }

}






