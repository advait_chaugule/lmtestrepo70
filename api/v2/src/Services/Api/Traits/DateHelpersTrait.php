<?php
namespace App\Services\Api\Traits;
use \DateTime;

trait DateHelpersTrait {

    /**
     * Method to return ISO8601 formatted dateTime
     */
    public  function formatDateTimeToISO8601($dateTimeString): string {
        $dateTimeString = new DateTime($dateTimeString);
        return $dateTimeString->format(\DateTime::ATOM); // Updated ISO8601
    }

    /**
     * Method to return timezone formatted dateTime
     */
    public  function formatDateTimeOnTimezone($dateTimeString): string {
        $dateTimeString = new DateTime($dateTimeString);
       
        $timeZone   =   $dateTimeString->getTimezone();
        $dateTimeString->setTimezone($timeZone);
        return $dateTimeString->format('Y-m-d H:i:s');

        //return $dateTimeString->format(\DateTime::ATOM); // Updated ISO8601
    }

    /**
     * This method is to create dateTime
     */
    public function createDateTime($dateFormat="Y-m-d H:i:s") {
        $dateObj = new DateTime();
        return $dateObj->format($dateFormat);
    }

    /**
     * This method will convert a date timestring to the date only string according to the format specified
     */
    public function formatDateTimeToDateOnly(string $dateTimeString, $dateFormat="Y-m-d"): string {
        $dateObj = new DateTime($dateTimeString);
        return $dateObj->format($dateFormat);
    }

    /**
     * This method is a duplicate of above method
     */
    public function formatDateTime(string $dateTimeString, $dateFormat="Y-m-d H:i:s") {
        $dateObj = new DateTime($dateTimeString);
        return $dateObj->format($dateFormat);
    }

    /**
     * Method to return cloud search date compatible formatted dateTime
     */
    public  function formatDateTimeToCloudSarchCompatibleFormat(string $dateTimeString): string {
        $dateTimeString = new DateTime($dateTimeString);
        return $dateTimeString->format('Y-m-d\TH:i:s').'Z';
    }

    /**
     * Method to return timezone formatted dateTime
     */
    public  function formatDateTimeToGivenTimezone($dateTimeString, $timeZone): string {
        date_default_timezone_set($timeZone);
        $date = date('Y-m-d H:i:s',strtotime($dateTimeString));
        return $date;

        //return $dateTimeString->format(\DateTime::ATOM); // Updated ISO8601
    }

}