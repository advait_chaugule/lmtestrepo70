<?php
namespace App\Services\Api\Traits;

trait ErrorMessageHelper {

    public function createErrorMessageFromException(\Exception $exception): string {
        $systemEnvironment = strtolower(env("APP_ENV", "dev"));
        $message =  "Line Number -> ".$exception->getLine()."\n".
                    "File Path-> ".$exception->getFile()."\n".
                    "Exception Message -> ".$exception->getMessage();
        //return ($systemEnvironment=="dev" || $systemEnvironment=="local") ? $message : "Internal Error occurred.";
        return $message;
    }

}