<?php
namespace App\Services\Api\Traits;

use Illuminate\Support\Facades\DB;

trait AddDefaultSettings {

    public function updatedefaultconfigsettings($orgnizationIdentifier,$is_user_default,$default_for_all_user,$action){
       // $orgnizationIdentifier = $this->getOrgnizationIdentifier();
        $default_taxonomy_list = array(array("Name","title","120"),array("Type","document_type","120"),array("Status","status","120"),array("Projects","associated_projects_count","120"),array("Group","group","120"),array("Last Change","last_active_date","120"),array("Active since","created_at","120"),array("Import type","actual_import_type","120"),array("Imported by","imported_by","120"));
        $taxonomy_list = array();
        $num = 1; 
        $countx = count($default_taxonomy_list);
        for($i =0;$i<$countx;$i++){
            
            $Valuesarray['display_name'] =  $default_taxonomy_list[$i][0];
            $Valuesarray['internal_name'] = $default_taxonomy_list[$i][1];
            $Valuesarray['order'] = $num;
            $Valuesarray['width'] = $default_taxonomy_list[$i][2];
            $Valuesarray['metadata_id'] = $this->getmetadataid($orgnizationIdentifier,$default_taxonomy_list[$i][1]);
            $Valuesarray['is_custom'] = 0;
            $Valuesarray['is_default'] = true;
            $taxonomy_list[] = $Valuesarray;
            $num++;
        } 
        $setting_arr = array("version"=>"1","is_user_default"=>$is_user_default,"default_for_all_user"=>$default_for_all_user,"feature_id"=>1,"setting_id"=>1,"taxonomy_list_setting"=>$taxonomy_list);
        $setting_arr_json = json_encode($setting_arr);

        if($action == "Set"){
             DB::table('organizations')
            ->where('organization_id', $orgnizationIdentifier)
            ->update(['json_config_value' => $setting_arr_json]);   
        }
            $default_taxonomy_list[]=array("Source Type","source_type","120");
            $Valuesarray['display_name'] =  $default_taxonomy_list[$i][0];
            $Valuesarray['internal_name'] = $default_taxonomy_list[$i][1];
            $Valuesarray['order'] = $num;
            $Valuesarray['width'] = $default_taxonomy_list[$i][2];
            $Valuesarray['metadata_id'] = $this->getmetadataid($orgnizationIdentifier,$default_taxonomy_list[$i][1]);
            $Valuesarray['is_custom'] = 1;
            $Valuesarray['is_default'] = false;             //extra parameter to identify source_type in super_default_json for Angular, bug id = UF-2600.
            $taxonomy_list[] = $Valuesarray;
            
            $setting_arr['taxonomy_list_setting'] = $taxonomy_list;
            $setting_arr_json = json_encode($setting_arr);    
        
        return $setting_arr_json;
    }

    private function getmetadataid($orgnizationIdentifier,$metadataname){
        $queryData = DB::table('metadata')->select('metadata_id')
       ->where('organization_id','=', $orgnizationIdentifier)
       ->where('internal_name','=',$metadataname)
       ->get()
       ->toArray();

       if(!empty($queryData)){
           $metadata_id = $queryData[0]->metadata_id;
       }else{
           $metadata_id = "";
       }
       return $metadata_id;
    }


}