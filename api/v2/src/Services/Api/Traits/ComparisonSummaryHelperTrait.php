<?php
namespace App\Services\Api\Traits;
use DB;

trait ComparisonSummaryHelperTrait{

    private $metadataMapping;
    private $CFDefinitionsMapping;

    public function setComparisonVariables(){        
        $this->metadataMapping = [
            'creator'=> 'Creator',
            'title'=> 'Document Title',
            'officialSourceURL'=> 'Official Source Url',
            'publisher'=> 'Publisher',
            'description'=> 'Description',
            'subject'=> 'Subject Title',
            'subject_title'=> 'Subject Title',
            'subject_hierarchy_code'=> 'Subject Hierarchy Code',
            'subject_description'=> 'Subject Description',
            'language'=> 'Language',
            'version'=> 'Version',
            'statusStartDate'=> 'Status Start Date',
            'statusEndDate'=> 'Status End Date',
            'licenseuri'=> 'License URI',
            'licensetitle'=> 'License Title',
            'licensedescription'=> 'License Description',
            'licenseText'=> 'License Text',
            'notes'=> 'Notes',
            'fullStatement'=> 'Full Statement',
            'alternativeLabel'=> 'Alternative Label',
            'humanCodingScheme'=> 'Human Coding Scheme',
            'listEnumeration'=> 'List Enumeration',
            'sequence_number'=> 'Sequence Number',
            'abbreviatedStatement'=> 'Abbreviated Statement',
            'concept_title'=> 'Concept Title',
            'conceptKeywords'=> 'Concept Keywords',
            'concept_hierarchy_code'=> 'Concept Hierarchy Code',
            'concept_description'=> 'Concept Description',
            'educationLevel'=> 'Education Level',
            'adoptionStatus'=> 'Case Status',
            'uri' => 'CFItem URI',
            'subjectURI,0,uri' => 'Subject URI'
        ];
        $this->CFDefinitionsMapping   = ["CFConcepts"=>"Concept","CFSubjects"=>"Subject","CFLicenses"=>"License","CFItemTypes"=>"Node Type"];
        $this->action = ['E' => 'edit','N' => 'new','D' => 'delete']; // Map action to full forms
    }

    public function checkComparisonIdExists($comparisonId){
        $comparionIderror = true;
        if(!empty($comparisonId)){
            $checkComparionId = DB::table('version_comparison')->where('comparison_id',$comparisonId)->first();
            if(isset($checkComparionId->comparison_id)){
                $comparionIderror = false;
            }
        }
        return $comparionIderror;
    }

    public function getNodeDetailsFromNodeId($comparisonId,$nodeId){
        $query = DB::table('version_node_type')
                            ->select('comparison_id', 'node_detail')
                            ->where('comparison_id',$comparisonId)
                            ->where('node_id',$nodeId)
                            ->whereIn('type',['CFItems','CFDocument']);
        $nodeDetailsArray = $query->first();
        $nodeDetails = [];
        if(isset($nodeDetailsArray->node_detail) && !empty($nodeDetailsArray->node_detail)){
            $nodeDetails = json_decode($this->stripSpecialCharacters($nodeDetailsArray->node_detail),true);// UF-3379 Quotes causimg json decode fail resolved
        }
        return $nodeDetails;
    }
    
	// UF-3379 Quotes causimg json decode fail resolved
    public function stripSpecialCharacters($jsonvalue){
        $jsonvalue = str_replace(' "',' \"',$jsonvalue);
        $jsonvalue = str_replace('" ','\" ',$jsonvalue);
        return preg_replace('/[[:cntrl:]]/', '',$jsonvalue);
    }
}