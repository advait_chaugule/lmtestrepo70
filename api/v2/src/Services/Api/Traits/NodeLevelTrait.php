<?php
namespace App\Services\Api\Traits;

use Illuminate\Support\Facades\DB;

trait NodeLevelTrait {      

    /**
     * @param $documentId
     * @return array
     * @FunctionName getNodesDepth
     * @Purpose This function is used to return level of nodes type id with sequence and compatibility status
     */

    private function getNodesDepth($documentId, $itemIdArray=[], $showNodeLevel=false)
    {
        $itemId = !empty($itemIdArray) ? " and ai.item_id in ('".implode("','",$itemIdArray)."')" : "";
        $nodeDepth = DB::select(DB::raw("WITH RECURSIVE 
        mini_aia AS (
            SELECT aia.*, ai.human_coding_scheme, ai.full_statement, ai.list_enumeration, (ROW_NUMBER() OVER (PARTITION BY document_id) ) as rownum, item_id 
            FROM acmt_items ai left join acmt_item_associations aia on ai.item_id = aia.source_item_id and not aia.is_deleted and association_type = 1
            where ai.document_id='".$documentId."' and not ai.is_deleted and aia.target_item_id != aia.source_item_id
        ), 
        get_tree (item_association_id, source_item_id, association_type, target_item_id, lvl, seq, sort) AS (
            select item_association_id, source_item_id, association_type, target_item_id,1 lvl, sequence_number, 
                 CAST(CONCAT_WS('/', LPAD(sequence_number, 5, '0'),'-',RPAD(human_coding_scheme, 20,' '),'-',RPAD(full_statement, 20,' '),'-',LPAD(rownum, 5, '0')) AS char(400)) as sort
            from mini_aia where target_item_id = '".$documentId."'
            Union All
            select '', ai.item_id, 'O', '".$documentId."' target_item_id,1 lvl, 1 sequence_number, 
            CAST(CONCAT_WS('/', LPAD(sequence_number, 5, '0'),'-',RPAD(human_coding_scheme, 20,' '),'-',RPAD(full_statement, 20,' '),'-',LPAD(rownum, 5, '0')) AS char(400)) as sort
            from mini_aia ai where target_item_id is null
            Union All
            select aia.item_association_id, aia.source_item_id, aia.association_type, aia.target_item_id, lvl+1, sequence_number, 
            CONCAT_WS('/',gt.Sort,LPAD(aia.sequence_number, 5, '0'),'-',RPAD(human_coding_scheme, 20,' '),'-',RPAD(full_statement, 20,' '),'-',LPAD(rownum, 5, '0'))
            from mini_aia aia 
            inner join get_tree gt on aia.target_item_id = gt.source_item_id
            )
        select gt.*, ai.item_id, ai.node_type_id, ai.full_statement, ai.full_statement_html, ai.human_coding_scheme, ai.human_coding_scheme_html, ai.list_enumeration from get_tree gt
        left join acmt_items ai on gt.source_item_id = ai.item_id $itemId
        order by Sort"));
        if($showNodeLevel==true)
            return $nodeDepth;

        $nodeDepthArray = [];
        $compatible = true;
        foreach($nodeDepth as $key => $nodeDepthVal){
            if(!isset($nodeDepthArray['sequence'][$nodeDepthVal->lvl]))
                $nodeDepthArray['sequence'][$nodeDepthVal->lvl]=$nodeDepthVal->node_type_id;
            if(isset($nodeDepthArray['sequence'][$nodeDepthVal->lvl]) && $nodeDepthArray['sequence'][$nodeDepthVal->lvl]!=$nodeDepthVal->node_type_id)
                $compatible = false;
        }
        $nodeDepthArray['compatibility'] = $compatible;
        return $nodeDepthArray;
    }

}