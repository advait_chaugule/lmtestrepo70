<?php
namespace App\Services\Api\Traits;

use \ZipArchive;

trait ZipHelperTrait {

    /** 
     * Add files and sub-directories in a folder to zip file. 
     * @param string $folder 
     * @param ZipArchive $zipFile 
     * @param int $exclusiveLength Number of text to be exclusived from the file path. 
     */ 
    private function folderToZip($folder, &$zipFile, $exclusiveLength) {
        $handle = opendir($folder); 
            while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..') {
                $filePath = "$folder/$f";
                // Remove prefix from file path before add to zip. 
                $localPath = substr($filePath, $exclusiveLength); 
                if (is_file($filePath)) {
                    $zipFile->addFile($filePath, $localPath); 
                }
                elseif (is_dir($filePath)) {
                    // Add sub-directory. 
                    $zipFile->addEmptyDir($localPath); 
                    $this->folderToZip($filePath, $zipFile, $exclusiveLength); 
                }
            }
        } 
        closedir($handle); 
    } 

    /** 
     * Zip files in a folder. 
     * Usage: 
     *   zipDir('/path/to/sourceDir', '/path/to/out.zip'); 
     * 
     * @param string $sourcePath Path of directory to be zip. 
     * @param string $outZipPath Path of output zip file. 
     */ 
    public function zipDir(string $sourcePath, string $outZipPath) 
    { 
        $pathInfo = pathInfo($sourcePath); 
        $parentPath = $pathInfo['dirname']; 
        $dirName = $pathInfo['basename']; 
        $zipArchive = new ZipArchive(); 
        $zipArchive->open($outZipPath, ZipArchive::CREATE); 
        $this->folderToZip($sourcePath, $zipArchive, strlen("$parentPath/"));
        $zipArchive->close(); 
    } 

    /** 
     * Archive a single file. 
     * @param string $archiveFilePathToSet - Path to store the archive.
     * @param string $filePathToReadFrom - File to Add. 
     * @param string $fileNameToSet - Archived file name to set. 
     */ 
    public function zipFile(string $archiveFilePathToSet, string $filePathToReadFrom, string $fileNameToSet)
    {
        $zipArchive = new ZipArchive();
        $zipArchive->open($archiveFilePathToSet, ZIPARCHIVE::CREATE);
        $zipArchive->addFile($filePathToReadFrom, $fileNameToSet);
        $zipArchive->close();
    }

    public function unzipArchive(string $archiveFilePath, string $destinationPath) {
        $zipArchive = new ZipArchive;
        $zipArchive->open($archiveFilePath);
        $zipArchive->extractTo($destinationPath);
        $zipArchive->close();
    }

}