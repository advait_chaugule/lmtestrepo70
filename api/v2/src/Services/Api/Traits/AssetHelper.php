<?php
namespace App\Services\Api\Traits;

trait AssetHelper {

    private function helperToReturnAssetLinkedTypeEnumValue(string $linkedTypeText): int {
        $assetEnumPairs = config("asset")["ENUM_KEY_VALUE_PAIR"]["ASSET_LINKED_TYPE"];
        return !empty($assetEnumPairs[$linkedTypeText]) ? $assetEnumPairs[$linkedTypeText] : 0;
    }

    private function helperToReturnAssetLinkedTypeText(int $linkedTypeNumber): string {
        $assetEnumPairs = config("asset")["ENUM_KEY_VALUE_PAIR"]["ASSET_LINKED_TYPE"];
        $enumTextToReturn = "";
        foreach($assetEnumPairs as $assetEnumText=>$assetEnumPairNumber) {
            if($linkedTypeNumber===$assetEnumPairNumber) {
                $enumTextToReturn = $assetEnumText;
                break;
            }
        }
        return !empty($assetEnumText) ? $assetEnumText : "";
    }

}