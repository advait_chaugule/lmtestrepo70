<?php
namespace App\Services\Api\Traits;
use App\Data\Models\Project;
use DB;
trait ProjectSettingTrait
{

    public function mappedProjectSettingData($userId,$projectId,$organizationId,$decodedJson,$tableName)
    {
        //$decodedJson = json_decode($jsonData);
       // print_r($decodedJson);die();
        $dateTime = date('Y-m-d H:i:s');
        $version   = $decodedJson['version'];
        $settingId = $decodedJson['setting_id'];
        $featureId = $decodedJson['feature_id'];
        $arrayNodeType = ['version' => $version, 'feature_id' => $featureId, 'setting_id' => $settingId,'last_table_view'=>$tableName];

        if ($decodedJson) {
            $jsonDataProject = $decodedJson['project_table_views'];
            foreach ($jsonDataProject as $jsonDataProjectK => $jsonDataProjectV) {
                $jsonDataProject[$jsonDataProjectK]['created_by'] = $userId;
            }
            $jsonDataTable['project_table_views'] = $jsonDataProject;
            $mergeProjectData = array_merge($arrayNodeType, $jsonDataTable);
            DB::table('user_settings')->updateOrInsert(['user_id' => $userId, 'organization_id' => $organizationId, 'id' => $projectId, 'is_deleted' => 0], [
                'user_id' => $userId,
                'organization_id' => $organizationId,
                'id' => $projectId,
                'id_type' => 4,
                'updated_by' => $userId,
                'is_deleted' => 0,
                'json_config_value' => json_encode($mergeProjectData),
                'created_at' => $dateTime,
                'updated_at' => $dateTime,
            ]);
			// Get default project config
			$projectSettings = DB::table('projects')
            ->select('display_options')
            ->where('organization_id', $organizationId)
            ->where('project_id', $projectId)
            ->where('is_deleted' , 0)
            ->first();
            $displayOptions = !empty($projectSettings) ? json_decode($projectSettings->display_options) : [];
            $docAllDefaultTableName = "";
            $docAllDefaultCreatedBy = "";
            if(!empty($displayOptions))
            {
                $docAllDefaultTableName = $displayOptions->project_table_views->table_name;
                $docAllDefaultCreatedBy = isset($displayOptions->project_table_views->created_by) ? $displayOptions->project_table_views->created_by : "";
            }
            foreach ($jsonDataProject as $jsonDataProjectK => $jsonDataProjectV) {
                $adminSetting = $jsonDataProjectV['default_for_all_user'];
				// Check if default project config is switched off                            
				if ($adminSetting == 1 || ($adminSetting == 0  && $jsonDataProjectV['table_name']==$docAllDefaultTableName && isset($jsonDataProjectV['created_by']) && $jsonDataProjectV['created_by']==$docAllDefaultCreatedBy)) {
                    $jsonDataProject = $jsonDataProjectV;
                    $arrDocumentData['project_table_views'] =$jsonDataProject;
                    $mergeProjectData = array_merge($arrayNodeType, $arrDocumentData);
                    DB::table('projects')->updateOrInsert(['organization_id' => $organizationId, 'project_id' => $projectId, 'is_deleted' => 0], [
                        'display_options' => json_encode($mergeProjectData),
                        //'updated_at' => $dateTime,
                    ]);
                }
            }
        }

    }

}