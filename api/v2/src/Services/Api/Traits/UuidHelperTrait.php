<?php
namespace App\Services\Api\Traits;

use \Ramsey\Uuid\Uuid;

trait UuidHelperTrait {

    public function createUniversalUniqueIdentifier(): string{
        return Uuid::uuid4()->toString();
    }

    public function isUuidValid(string $uuid): bool {
        return Uuid::isValid($uuid);
    }

}