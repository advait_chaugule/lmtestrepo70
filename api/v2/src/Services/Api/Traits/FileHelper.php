<?php
namespace App\Services\Api\Traits;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;

trait FileHelper {

    public function getFileContent(string $filePath) {        
        $fullFilePath = storage_path('app')."/".$filePath;
        $fileContent = File::get($fullFilePath);
        return $fileContent;
    }

    public function deleteFileFromLocalPath(string $localPath) {
        $fullFilePath = storage_path('app')."/".$localPath;
        File::delete($fullFilePath);
    }

    public function createFile(string $filePath, $contentToSave) {
        File::put($filePath, $contentToSave);
    }

    public function createFolder(string $folderPath) {
        File::makeDirectory($folderPath, 0755, true, true);
    }

    public function deleteFolder(string $folderPath) {
        File::deleteDirectory($folderPath);
    }

    public function deleteFile(string $filePath) {
        File::delete($filePath);
    }

    public function getContent(string $filePath) {
        $fileContent = File::get($filePath);
        return $fileContent;
    }

    public function getSize(string $filePath) {
        $fullFilePath = storage_path('app')."/".$filePath;
        $fileSize = Storage::get($fullFilePath);
        return $fileSize;
    }
    
}