<?php
namespace App\Services\Api\Traits;

trait CaseFrameworkTrait {

    public function getCaseSpecificationAdoptionStatus(string $adoptionStatus = null): string {
        $systemPresetCaseAdoptionStatusMapping = [
            "private_draft" => "Private Draft",
            "draft" => "Draft", 
            "adopted" => "Adopted", 
            "deprecated" => "Deprecated"
        ];
        return !empty($systemPresetCaseAdoptionStatusMapping[$adoptionStatus]) ? 
               $systemPresetCaseAdoptionStatusMapping[$adoptionStatus] : 
                "";
    }

    public function getSystemSpecificationAdoptionStatus(string $caseAdoptionStatus = null): string {
        $systemPresetCaseAdoptionStatusMapping = [
            "Private Draft" => "private_draft",
            "Draft" => "draft", 
            "Adopted" => "adopted", 
            "Deprecated" => "deprecated"
        ];
        return !empty($systemPresetCaseAdoptionStatusMapping[$caseAdoptionStatus]) ? 
               $systemPresetCaseAdoptionStatusMapping[$caseAdoptionStatus] : 
                "";
    }

    public function getCaseApiUri($endPoint, string $identifier=null, $hasParam=true, $orgCode='',$domainName=''): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $baseUrl = env("APP_URL", "http://34.201.107.214/server");
        }else{
                $baseUrl = $domainName;
        }

        $acmtApiVersionPrefix = "api/v1";
        $caseConformanceEndpointPrefix = env("CASE_ENDPOINT_PREFIX", "ims/case/v1p0");
        if($orgCode != ''){
            $baseUrlWithEndPoint = $baseUrl."/".$acmtApiVersionPrefix."/".$orgCode."/".$caseConformanceEndpointPrefix."/".$endPoint;
        }
        else{
            $baseUrlWithEndPoint = $baseUrl."/".$acmtApiVersionPrefix."/".$caseConformanceEndpointPrefix."/".$endPoint;
        }
       
        $fullyResolvedUri = $baseUrlWithEndPoint.($hasParam ? "/$identifier" : "");
        return $fullyResolvedUri;
    }

    public function returnAssociationType($associationType): string{
        $type = [
            1 => 'isChildOf', 2 => 'isPeerOf', 3 => 'isPartOf', 4 => 'exactMatchOf', 5 => 'precedes', 6 => 'isRelatedTo', 7 => 'replacedBy', 8 => 'exemplar', 9 => 'hasSkillLevel', 10 => 'hasPartOf', 11 => 'succeeds', 12 => 'replaces'
        ];
        return $type[($associationType)];
    }

    public function getSystemSpecifiedAssociationTypeNumber(string $associationType): int{
        $availableTypes = [
            'isChildOf'     =>  1, 
            'isPeerOf'      =>  2, 
            'isPartOf'      =>  3, 
            'exactMatchOf'  =>  4, 
            'precedes'      =>  5, 
            'isRelatedTo'   =>  6, 
            'replacedBy'    =>  7, 
            'exemplar'      =>  8, 
            'hasSkillLevel' =>  9,
            'hasPartOf'     =>  10,
            'succeeds'      =>  11,
            'replaces'      =>  12
        ];
        return $availableTypes[$associationType];
    }

    public function getSystemSpecifiedAdoptionStatusText(int $adoptionStatusNumber): string{
        $systemPresetCaseAdoptionStatusMapping = [
            2 => "Draft", 
            3 => "Adopted",
            4 => "Deprecated",
            //4 => "Deprecated"
        ];
        return isset($systemPresetCaseAdoptionStatusMapping[$adoptionStatusNumber]) && !empty($systemPresetCaseAdoptionStatusMapping[$adoptionStatusNumber]) ? 
            $systemPresetCaseAdoptionStatusMapping[$adoptionStatusNumber] : 
                "Draft";
    }

    public function getAllSystemSpecifiedAssociationType(){
        $availableTypes = [
            ['type_id' => 1, "type_name" => "isChildOf", "display_name" => "Is Child Of"], 
            ['type_id' => 2, "type_name" => "isPeerOf", "display_name" => "Is Peer Of"],
            ['type_id' => 3, "type_name" => "isPartOf", "display_name" => "Is Part Of"],
            ['type_id' => 4, "type_name" => "exactMatchOf", "display_name" => "Exact Match Of"],
            ['type_id' => 5, "type_name" => "precedes", "display_name" => "Precedes"],
            ['type_id' => 6, "type_name" => "isRelatedTo", "display_name" => "Is Related To"],
            ['type_id' => 7, "type_name" => "replacedBy", "display_name" => "Is Replaced By"],
            ['type_id' => 8, "type_name" => "exemplar", "display_name" => "Exemplar"],
            ['type_id' => 9, "type_name" => "hasSkillLevel", "display_name" => "Has Skill Level"],
            ['type_id' => 10, "type_name" => "hasPartOf", "display_name" => "Has Part"],
            ['type_id' => 11, "type_name" => "succeeds", "display_name" => "Succeeds"],
            ['type_id' => 12, "type_name" => "replaces", "display_name" => "Replaces"],

        ];
        return $availableTypes;
    }
    
    public function getSystemSpecifiedAdoptionStatusNumber(string $adoptionStatusString): int {
        $systemSpecifiedMapping = [
            "Draft" => 2,
            "Adopted" => 3,
            "Deprecated" => 4
        ];
        return isset($systemSpecifiedMapping[$adoptionStatusString]) && !empty($systemSpecifiedMapping[$adoptionStatusString]) ? 
                $systemSpecifiedMapping[$adoptionStatusString] : 
                0;
    }

    
}