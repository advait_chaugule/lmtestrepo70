<?php
namespace App\Services\Api\Traits;

use Monolog\Logger;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogHandler;
use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;

trait CloudWatchTrait {
    public function CloudWatchLogger($message){

        $awsCredentials = [ 
                    'name' => 'acmt-dev-php-applogs',
                    'region' => 'us-east-1',
                    'credentials' => [
                        'key' => 'AKIAXHMIMWVJ3FYZ33PY',
                        'secret' => '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'
                    ],
                    'stream_name' => 'acmt_dev_laravel_app',
                    'retention' =>  7,
                    'group_name' =>  'acmt-dev-php-applogs',
                    'version' => 'latest',
                    'formatter' => \Monolog\Formatter\JsonFormatter::class  
        ];
        $logFile = "acmt-dev-php-applogs";
        $appName = "ACMT";
        $facility = "local0";

        // Get instance ID: 
        //$url = "http://169.254.169.254/latest/meta-data/instance-id";
        //$instanceId = file_get_contents($url);
        $instanceId ='i-064c29f2947cad032';

        $cwClient = new CloudWatchLogsClient($awsCredentials);
        // Log group name, will be created if none
        $cwGroupName = 'acmt-dev-php-applogs';
        // Log stream name, will be created if none
        $cwStreamNameInstance = "error";
        // Instance ID as log stream name
        // $cwStreamNameApp = "error";
        // Days to keep logs, 14 by default
        $cwRetentionDays = 7;

        // $cwHandlerInstanceNotice = new CloudWatch($cwClient, $cwGroupName, $cwStreamNameInstance, $cwRetentionDays, 10000, [ 'application' => 'php-testapp01' ],Logger::NOTICE);
        $cwHandlerInstanceError = new CloudWatch($cwClient, $cwGroupName, $cwStreamNameInstance, $cwRetentionDays, 10000, [ 'application' => 'php-testapp01' ],Logger::ERROR);
        //$cwHandlerAppNotice = new CloudWatch($cwClient, $cwGroupName, $cwStreamNameApp, $cwRetentionDays, 10000, [ 'application' => 'php-testapp01' ],Logger::NOTICE);

        $logger = new Logger('PHP Logging');

        $formatter = new LineFormatter(null, null, false, true);
        $syslogFormatter = new LineFormatter("%channel%: %level_name%: %message% %context% %extra%",null,false,true);
        $infoHandler = new StreamHandler(__DIR__."/".$logFile, Logger::INFO);
        $infoHandler->setFormatter($formatter);

        $warnHandler = new SyslogHandler($appName, $facility, Logger::WARNING);
        $warnHandler->setFormatter($syslogFormatter);

        //$cwHandlerInstanceNotice->setFormatter($formatter);
        $cwHandlerInstanceError->setFormatter($formatter);
        //$cwHandlerAppNotice->setFormatter($formatter);

        $logger->pushHandler($warnHandler);
        $logger->pushHandler($infoHandler);
        //$logger->pushHandler($cwHandlerInstanceNotice);
        $logger->pushHandler($cwHandlerInstanceError);
        //$logger->pushHandler($cwHandlerAppNotice);

        // $logger->info('Initial test of application logging.');
        // $logger->warn('Test of the warning system logging.');
        // $logger->notice('Application Auth Event: ',[ 'function'=>'login-action','result'=>'login-success' ]);
        // $logger->notice('Application Auth Event: ',[ 'function'=>'login-action','result'=>'login-failure' ]);
        $logger->error($message);

    }

}