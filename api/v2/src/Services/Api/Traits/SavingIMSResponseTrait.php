<?php
namespace App\Services\Api\Traits;

use Illuminate\Support\Facades\DB;
use Storage;

trait SavingIMSResponseTrait {
    
    public function IMSResponse($data){
        $appDebugger = env("QUERY_DEBUGGER_MODE");
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $searchString = 'ims/case/v1p0';
            if(strpos($link, $searchString) == true && $appDebugger == true)
            {
               $IMSResponse = 'Request: '.$link.PHP_EOL.'Response: '.$data;          
               Storage::append('IMSRequestResponse.log',$IMSResponse);
            }
    }
}