<?php
namespace App\Services\Api\Traits;
use DB;
trait GroupDocumentTrait
{
    public function getDocumentGroups($documentIdList=[],$userId){
        $groups = array();
        if(!empty($documentIdList)){
            $getDocumentGroup = DB::table('group_document')
            ->join('group', 'group.group_id', '=', 'group_document.group_id')
            ->select('group.name as group_name','group_document.group_id','document_id','group.is_default')
            ->whereIn('document_id', $documentIdList)
            ->where('group.is_deleted','=','0')
            ->where(function($query) use ($userId)
            {
                $query->where('group.is_default', '=', 1)
                      ->orwhere('group.user_id', '=', $userId);
            })
            ->get()
            ->toArray();
            if(!empty($getDocumentGroup)){
                foreach ($getDocumentGroup as $groupDocument) {
                $groups[$groupDocument->document_id][] = ['group_id'=>$groupDocument->group_id, 'group_name'=>$groupDocument->group_name, 'is_default'=>$groupDocument->is_default];
                }
            }
        }     
        return $groups;
    }

}






