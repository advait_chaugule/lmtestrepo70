<?php
namespace App\Services\Api\Traits;

trait CurlHelper {

    public function checkUrl($endpoint , $method, $parameter = []) {
        //For Get Url must be complete with specific url.
        // For Post  You can pass parrameter as array
        if($method == "GET")
        {
            $url = $endpoint;

            $ch = curl_init();
            // curl_setopt($ch, CURLOPT_TIMEOUT,60); 
            curl_setopt($ch, CURLOPT_URL, $url);
           // curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       
            $response = curl_exec ($ch);
             $err = curl_error($ch);  //if you need
             $errno = curl_errno($ch);
            
            curl_close ($ch); 
            $data = ["error_no" => $errno,"error_name" => $err,"result" => $response];
            return $data;
        }
        else
        {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $endpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($parameter),
                CURLOPT_HTTPHEADER => array(
                    // Set here requred headers
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $errno = curl_errno($ch);
            
            curl_close ($ch); 
            $data = ["error_no" => $errno,"error_name" => $err,"result" => $response];
            return $data;
        }
        
    }
}