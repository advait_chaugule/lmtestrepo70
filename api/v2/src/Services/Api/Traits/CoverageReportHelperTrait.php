<?php
namespace App\Services\Api\Traits;

use Illuminate\Support\Facades\DB;

trait CoverageReportHelperTrait { 

    private function getItemSort($documentId, $itemIdArray=[])
    {
        $itemId = !empty($itemIdArray) ? "  gt.source_item_id in ('".implode("','",$itemIdArray)."')" : "";
        DB::connection()->enableQueryLog();
        $nodeDepth = DB::select(DB::raw("WITH RECURSIVE
        mini_aia AS (
            SELECT aia.*, ai.human_coding_scheme, ai.full_statement, ai.list_enumeration, (ROW_NUMBER() OVER (PARTITION BY document_id) ) as rownum, item_id
            FROM acmt_items ai left join acmt_item_associations aia on ai.item_id = aia.source_item_id and not aia.is_deleted and association_type = 1
            where ai.document_id = '$documentId'
            and not ai.is_deleted and aia.target_item_id != aia.source_item_id 
        ),
        get_tree (item_association_id, source_item_id, association_type, target_item_id, lvl, seq, sort) AS (
            select item_association_id, source_item_id, association_type, target_item_id,1 lvl, sequence_number,
                 CAST(CONCAT_WS('/', LPAD(sequence_number, 5, '0'),'-',RPAD(human_coding_scheme, 20,' '),'-',RPAD(full_statement, 20,' '),'-',LPAD(rownum, 5, '0')) AS char(400)) as sort
            from mini_aia where target_item_id ='$documentId'
            Union All
            select '', ai.item_id, 'O', source_document_id target_item_id,1 lvl, 1 sequence_number,
            CAST(CONCAT_WS('/', LPAD(sequence_number, 5, '0'),'-',RPAD(human_coding_scheme, 20,' '),'-',RPAD(full_statement, 20,' '),'-',LPAD(rownum, 5, '0')) AS char(400)) as sort
            from mini_aia ai where target_item_id is null
            Union All
            select aia.item_association_id, aia.source_item_id, aia.association_type, aia.target_item_id, lvl+1, sequence_number,
            CONCAT_WS('/',gt.Sort,LPAD(aia.sequence_number, 5, '0'),'-',RPAD(human_coding_scheme, 20,' '),'-',RPAD(full_statement, 20,' '),'-',LPAD(rownum, 5, '0'))
            from mini_aia aia
            inner join get_tree gt on aia.target_item_id = gt.source_item_id
            )
            
        select (ROW_NUMBER() OVER () ) as sortnum,ai.item_id , gt.sort,ai.full_statement,ai.human_coding_scheme        
        from get_tree gt
        left join acmt_items ai on gt.source_item_id = ai.item_id where  $itemId
        order by Sort "));

        $queries = DB::getQueryLog();
      
        
       return $nodeDepth;
      
    }




}

?>