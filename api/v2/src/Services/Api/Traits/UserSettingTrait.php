<?php
namespace App\Services\Api\Traits;
use DB;
trait UserSettingTrait
{

    public function mappedUserSettingData($userId, $documentId, $organizationId, $jsonData,$tableName='')
    {
        $dateTime  = date('Y-m-d H:i:s');
        $version   = $jsonData['version'];
        $settingId = $jsonData['setting_id'];
        $featureId = $jsonData['feature_id'];
        $arrayNodeType = ['version' => $version, 'feature_id' => $featureId, 'setting_id' => $settingId,'last_table_view'=>$tableName];
        if ($jsonData) {
            $jsonDataTaxonomy = $jsonData['taxonomy_table_views'];            
            foreach ($jsonDataTaxonomy as $jsonDataTaxonomyK => $jsonDataTaxonomyV) {
                $jsonDataTaxonomy[$jsonDataTaxonomyK]['created_by'] = $userId;
            }
            $jsonDataTable['taxonomy_table_views'] = $jsonDataTaxonomy;            
            $mergeData = array_merge($arrayNodeType, $jsonDataTable);
            DB::table('user_settings')->updateOrInsert(['user_id' => $userId, 'organization_id' => $organizationId, 'id' => $documentId, 'is_deleted' => 0], [
                'user_id' => $userId,
                'organization_id' => $organizationId,
                'id' => $documentId,
                'id_type' => 3,
                'updated_by' => $userId,
                'is_deleted' => 0,
                'json_config_value' => json_encode($mergeData),
                'created_at' => $dateTime,
                'updated_at' => $dateTime,
            ]);
			// Get default document config
            $documentSettings = DB::table('documents')
            ->select('display_options')
            ->where('organization_id', $organizationId)
            ->where('document_id', $documentId)
            ->where('is_deleted' , 0)
            ->first();
            $displayOptions = !empty($documentSettings) ? json_decode($documentSettings->display_options) : [];
            $docAllDefaultTableName = "";
            $docAllDefaultCreatedBy = "";
            if(!empty($displayOptions))
            {
                $docAllDefaultTableName = $displayOptions->taxonomy_table_views->table_name;
                $docAllDefaultCreatedBy = isset($displayOptions->taxonomy_table_views->created_by) ? $displayOptions->taxonomy_table_views->created_by : "";
            }
            
            foreach ($jsonDataTaxonomy as $jsonDataTaxonomyK => $jsonDataTaxonomyV) {
                $adminSetting = $jsonDataTaxonomyV['default_for_all_user'];
                // Check if document default config is switched off
				if ($adminSetting == 1 || ($adminSetting == 0  && $jsonDataTaxonomyV['table_name']==$docAllDefaultTableName && isset($jsonDataTaxonomyV['created_by']) && $jsonDataTaxonomyV['created_by']==$docAllDefaultCreatedBy)) {
                    $jsonDataTaxonomy = $jsonDataTaxonomyV;
                    $arrDocumentData['taxonomy_table_views'] =$jsonDataTaxonomy;
                    $mergedData = array_merge($arrayNodeType, $arrDocumentData);
                    DB::table('documents')->updateOrInsert(['organization_id' => $organizationId, 'document_id' => $documentId, 'is_deleted' => 0], [
                        'display_options' => json_encode($mergedData),
                        'updated_at' => $dateTime,
                    ]);
                }
            }
        }
    }

}






