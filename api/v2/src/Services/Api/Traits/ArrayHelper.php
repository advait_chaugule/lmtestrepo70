<?php
namespace App\Services\Api\Traits;

trait ArrayHelper {

    public function removeSpecifiedElementsFromArray($data = [], $dataKeys = []): array {
        foreach($dataKeys as $key){
            if(array_key_exists($key, $data)) {
                unset($data[$key]);
            }
        }
        return $data;
    }

    public function convertJsonStringToArray(string $jsonString, bool $returnAssociativeArray = true): array{
        $caseData = json_decode($jsonString, $returnAssociativeArray);
        if($caseData)
        {
            return $caseData;
        }
        else
        {
            return [];
        }
        
    }

    public function createArrayFromCommaeSeparatedString(string $string): array {
        return explode(",",$string);
    }

    // converts all keys in a multidimensional array to lower or upper case
    public function array_change_key_case_recursive($arr, $case=CASE_LOWER)
    {
	    return array_map(function($item)use($case){
	        if(is_array($item))
	            $item = $this->array_change_key_case_recursive($item, $case);
	        return $item;
	    },array_change_key_case($arr, $case));
    }
    // Check if multi array is empty or not
    public function is_multi_array( $arr ) {
        rsort( $arr );
        if(isset( $arr[0] ) && is_array( $arr[0] )){
            if(!empty(array_filter($arr[0])))
                return true;
        }
        return false;        
    }
    
    // removes all extra spaces of an array
    public function trim_array_spaces($inputData)
    {
	    if (!is_array($inputData)){
            $inputData = str_replace(array("\t", "\r"), '', $inputData);
            return trim($inputData);
        }
            
        return array_map(array($this, 'trim_array_spaces'), $inputData);
    }
	// Removed blank array of all values of array in multi is empty
    public function removeBlankArray($array){
        foreach($array as $key => $value){
            if(is_array($value)){            
                if(empty(array_filter(array_values($value))))
                    unset($array[$key]);
            }
        }
        return $array;        
    }

}