<?php
namespace App\Domains\PacingGuide\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;

class GetFlattenedTaxonomyTreeForPacingGuideJob extends Job
{

    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $requestUserDetails;

    private $document;
    private $items;
    private $itemAssociationsUnderDocument;
    private $itemRelationsUnderDocument;
    private $projectIdsAssociated;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;
    private $standardNodes;
    private $itemAssociationArray;
    private $itemAssociationDestinationArray;
    private $parentAndDocumentId;

    private $transformedDocument;
    private $transformedItems;
    private $transformedStandardItems;
    private $transformedItemAssociationsUnderDocument;
    private $transformedItemRelationsUnderDocument;

    private $tree;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setRequestUserDetails($requestUserDetails);
    }

    public function setDocumentIdentifier(string $data) {
        $this->documentIdentifier = $data;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setDocument(Document $data) {
        $this->document = $data;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setItems(Collection $data) {
        $this->items = $data;
    }

    public function getItems(): Collection {
        return $this->items;
    }

    public function setItemAssociationsUnderDocument(Collection $data) {
        $this->itemAssociationsUnderDocument = $data;
    }

    public function getItemAssociationsUnderDocument(): Collection {
        return $this->itemAssociationsUnderDocument;
    }

    public function setItemRelationsUnderDocument(Collection $data) {
        $this->itemRelationsUnderDocument = $data;
    }

    public function getItemRelationsUnderDocument(): Collection{
        return $this->itemRelationsUnderDocument;
    }

    public function setIdsOfProjectsAssociated(array $data) {
        $this->projectIdsAssociated = $data;
    }

    public function getIdsOfProjectsAssociated(): array {
        return $this->projectIdsAssociated;
    }

    public function setProjectItemMapping(Collection $data) {
        $this->projectItemMapping = $data;
    }

    public function getProjectItemMapping(): Collection {
        return $this->projectItemMapping;
    }

    public function setParentAndDocumentId($data) {
        $this->parentAndDocumentId = $data;
    }

    public function getParentAndDocumentId() {
        return $this->parentAndDocumentId;
    }

    public function setProjectsAssociated(Collection $data) {
        $this->projectsAssociated = $data;
    }

    public function getProjectsAssociated(): Collection {
        return $this->projectsAssociated;
    }

    public function setNodeTypes(Collection $data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes(): Collection {
        return $this->nodeTypes;
    }

    public function setStandardNodesList($data) {
        $this->standardNodes    = $data;
    }

    public function getStandardNodesList() {
        return $this->standardNodes;
    }

    public function setItemAssociationIdArray($data) {
        $this->itemAssociationArray    = $data;
    }

    public function getItemAssociationIdArray() {
        return $this->itemAssociationArray;
    }

    public function setItemDestinationIdArray($data) {
        $this->itemAssociationDestinationArray    = $data;
    }

    public function getItemDestinationIdArray() {
        return $this->itemAssociationDestinationArray;
    }

    

    public function setTransformedDocument(array $data) {
        $this->transformedDocument = $data;
    }

    public function getTransformedDocument(): array {
        return $this->transformedDocument;
    }

    public function setTransformedItems(array $data) {
        $this->transformedItems = $data;
    }

    public function getTransformedItems(): array {
        return $this->transformedItems;
    }

    public function setTransformedStandardItems(array $data) {
        $this->transformedStandardItems = $data;
    }

    public function getTransformedStandardItems(): array {
        return $this->transformedStandardItems;
    }
    

    public function setTransformedItemAssociationsUnderDocument(array $data) {
        $this->transformedItemAssociationsUnderDocument = $data;
    }

    public function getTransformedItemAssociationsUnderDocument(): array {
        return $this->transformedItemAssociationsUnderDocument;
    }

    public function setTransformedItemRelationsUnderDocument(array $data) {
        $this->transformedItemRelationsUnderDocument = $data;
    }

    public function getTransformedItemRelationsUnderDocument(): array {
        return $this->transformedItemRelationsUnderDocument;
    }

    public function setTree(array $data) {
        $this->tree = $data;
    }

    public function getTree(): array {
        return $this->tree;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;

        // start fetching document, items and item associations etc
        $this->fetchAndSetAllTaxonomyRelatedData();

        // start transforming of data to ui compatible structure
        $this->transformTaxonomyRelatedData();

        // create the ui compatible tree response structure
        $this->buildTree();

        return $this->getTree();
        
    }

    /**
     * start fetching document, items and item associations from database
     */
    private function fetchAndSetAllTaxonomyRelatedData() {
        $this->fetchAndSetDocument();
        $this->fetchAndSetItems();
        $this->fetchAndSetItemAssociationsUnderDocument();
        $this->fetchAndSetItemRelationsUnderDocument();
        $this->fetchAndSetStandardNodes();
        $this->fetchAndSetProjectItemMapping();
        $this->fetchAndSetItemParentAndDocumentId();
        // $this->fetchAndSetProjectsAssociated();
        // $this->fetchAndSetNodeTypeDetailsForAllItemsAndDocument();
    }

    private function fetchAndSetDocument() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $input = $this->getRequestUserDetails();
        $organizationId     =   $input["organization_id"];
        
        $returnCollection           =   false;
        $fieldsToReturn             =   ["document_id", "title", "node_type_id", "project_id", "node_type_id", "adoption_status","title_html"];
        $relations                  =   ['nodeType'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];
        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $this->setDocument($document);
    }

    private function fetchAndSetItems() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $organizationId     =   $this->getRequestUserDetails()["organization_id"];
        
        $returnCollection           =   true;
        $fieldsToReturn             =   ["item_id", "human_coding_scheme", "list_enumeration","full_statement", "node_type_id", "source_item_id","human_coding_scheme_html","full_statement_html"];
        $relations                  =   ['nodeType', 'customMetadata','itemAssociations'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];
        
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);

        //dd($items->toArray());
        $this->setItems($items);
    }

    private function fetchAndSetItemAssociationsUnderDocument() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $organizationId     =   $this->getRequestUserDetails()["organization_id"];
        $returnCollection           =   true;
        $fieldsToReturn             =   ["origin_node_id", "destination_node_id"];
        $conditionalKeyValuePair    =   [ 
            "document_id" =>  $documentIdentifier,
            "organization_id" => $organizationId,
            "is_deleted" => 0,
            "association_type" => 1 // 1 for isChildOf relation
        ];

        //dd($conditionalKeyValuePair);

        $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                         );
          //  dd($itemAssociationsUnderDocument);
        $this->setItemAssociationsUnderDocument($itemAssociationsUnderDocument);
    }

    private function fetchAndSetItemRelationsUnderDocument() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $organizationId     =   $this->getRequestUserDetails()["organization_id"];
        $returnCollection           =   true;
        $fieldsToReturn             =   ["item_association_id", "origin_node_id", "destination_node_id", "is_reverse_association"];
        $conditionalKeyValuePair    =   [ 
            "document_id" =>  $documentIdentifier,
            "organization_id" => $organizationId,
            "is_deleted" => 0,
            "association_type" => 3 // 6 for isRelatedTo relationa
        ];
        $relations                  =   ['originNode', 'originDocumentNode', 'destinationNode', 'destinationDocumentNode'];

        $itemRelationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations
                                         );
        //dd($itemRelationsUnderDocument->toArray());
        $this->setItemRelationsUnderDocument($itemRelationsUnderDocument);
    }

    private function fetchAndSetStandardNodes() {
        $itemAssociationIdArray             =   [];
        $standardItemArray                  =   [];
        $itemAssociationDestinationIdArray  =   [];
        
        $itemRelationsForStandard   =   $this->getItemRelationsUnderDocument();
        //dd($itemRelationsForStandard->toArray());
        if($itemRelationsForStandard->count() > 0) {
            foreach($itemRelationsForStandard as $standardNodes) {
                $itemAssociationIdArray[$standardNodes->origin_node_id][$standardNodes->destination_node_id]     =  $standardNodes->item_association_id;

                $standardItemArray[$standardNodes->item_association_id][]    =   $standardNodes->originNode;

                $itemAssociationDestinationIdArray[$standardNodes->item_association_id][]    =   !empty($standardNodes->destinationNode) ? $standardNodes->destinationNode: $standardNodes->destinationDocumentNode  ;
            }            
        }         
        //dd($itemAssociationDestinationIdArray);
        $this->setStandardNodesList($standardItemArray);
        $this->setItemAssociationIdArray($itemAssociationIdArray);
        $this->setItemDestinationIdArray($itemAssociationDestinationIdArray);
        
        

    }

    private function fetchAndSetItemParentAndDocumentId() {
        $hierarchyIdSet =   [];
        $parentIdSet    =   [];
        $items = $this->getItems();

        $itemSourceIdArray    =   [];
        foreach($items as $item) {
            $itemSourceIdArray[]  =   $item->item_id;
        }

        //dd($itemSourceIdArray);
        $attributeInAssociation         =   'origin_node_id';
        $containedInValuesAssociation   =   $itemSourceIdArray;
        $returnCollectionAssociation    =   true;
        $fieldsToReturnAssociation      =   ["origin_node_id", "destination_node_id", "association_type"];

        $parentIdSetForItem = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeInAssociation, $containedInValuesAssociation, $returnCollectionAssociation, $fieldsToReturnAssociation)->toArray();
        
        foreach($parentIdSetForItem as $parentId) {
            if($parentId['association_type'] == 1) {
                $parentIdSet[$parentId['origin_node_id']]   =   $parentId['destination_node_id'];
            }
            
        }

        //dd($parentIdSet);

        //$parentIdSetForItemArray    =   array_reverse($parentIdSetForItem);

        foreach($itemSourceIdArray as $item) {
            $returnCollection   =   false;
            $fieldsToReturn     =   ['document_id', 'item_id'];
            $keyValuePairs      =   ['item_id'   => $item];

            $documentSetForItem =   $this->itemRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs );

            $hierarchyIdSet[$item] =   [
                'document_id'   => !empty($documentSetForItem->document_id) ? $documentSetForItem->document_id : "", 
                'parent_id'    =>  !empty($parentIdSet[$item])? $parentIdSet[$item]: $documentSetForItem->document_id];
        }
        $this->setParentAndDocumentId($hierarchyIdSet);
    }



    private function fetchAndSetProjectItemMapping() {
        $itemIds = $this->createArrayFromCommaeSeparatedString($this->getItems()->implode("item_id", ","));
        $projectItemMapping = $this->projectRepository->getProjectItemMappingOfMultipleItems($itemIds);
        $this->setProjectItemMapping($projectItemMapping);
    }

    /**
     * start transforming of data to ui compatible structure
     */
    private function transformTaxonomyRelatedData() {
        $this->transformDocument();
        $this->transformItems();
        $this->transformStandards();
        $this->transformItemAssociations();
    }

    private function transformDocument() {
        $document = $this->getDocument();
        $nodeType = !empty($document->nodeType->node_type_id) ? $document->nodeType->title : "";
        //dd($document);
        if(!empty($document->project_id)) {

            $organizationId             =   $this->getRequestUserDetails()["organization_id"];
            $returnCollection           =   false;
            $fieldsToReturn             =   ["project_type"];
            $conditionalKeyValuePair    =   [ "project_id" =>  $document->project_id, "organization_id" => $organizationId ];
            
            $project = $this->projectRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
            if($project->project_type == 1)
            {
                $projectEnabled = 1;
            }
            else
            {
                $projectEnabled = 0;
            }  
        }
        else {
            $projectEnabled = 0;
        }

        $projectName            =   "";
        $documentNodeTypeId     =   $document->node_type_id;
        $documentNodeTypeTitle  =   $nodeType;
        $documentStatus         =   $document->adoption_status;

        $parsedDocument = [
            "id"                    =>  $document->document_id,
            "title"                 =>  !empty($document->title) ? $document->title : "",
            "title_html"            =>  !empty($document->title_html) ? $document->title_html : "",
            "human_coding_scheme"   =>  "",
            "human_coding_scheme_html"   =>  "",
            "list_enumeration"      =>  "",
            "full_statement"        =>  "",
            "full_statement_html"   =>  "",
            "status"                =>  $documentStatus,    
            "node_type"             =>  $documentNodeTypeTitle,
            "metadataType"          =>  $documentNodeTypeTitle,
            "node_type_id"          =>  $documentNodeTypeId,
            "project_enabled"       =>  (string) $projectEnabled,
            "project_name"          =>  $projectName,
            "is_document"           =>  1
        ];
        
        $this->setTransformedDocument($parsedDocument);
    }

    private function transformItems() {
        $items                  =   $this->getItems();
        $projectItemMapping     =   $this->getProjectItemMapping()->toArray();
        $parentAndDocumentId    =   $this->getParentAndDocumentId();
        $parsedItems            =   [];

        //dd($parentAndDocumentId);

        foreach($items as $item) {
            $itemId                 = $item->item_id;
            $projectEnabled         = $this->helperToReturnProjectEnabledStatus($itemId, $projectItemMapping);
            $listEnumeration        = !empty($item->list_enumeration) ? $item->list_enumeration : "";
            $humanCodingScheme      = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "";
            $humanCodingSchemeHtml  = !empty($item->human_coding_scheme_html) ? $item->human_coding_scheme_html : "";
            $fullStatememnt         = !empty($item->full_statement) ? $item->full_statement : "";
            $fullStatememntHtml     = !empty($item->full_statement_html) ? $item->full_statement_html : "";
            $nodeTypeId             = !empty($item->node_type_id) ? $item->node_type_id : "";
            $nodeType               = !empty($item->nodeType) ? $item->nodeType->title : "";

            $typeMetadataDet        =   $item->customMetadata->where('is_document', '3')->first();      
            
            $itemAssociations        =   array_values($item->itemAssociations->where('association_type','1')->toArray());

            $sourceDocumentDetail   =   $this->documentRepository->find($parentAndDocumentId[$item->item_id]['document_id']);

            //dd($sourceDocumentDetail);
            $projectName = "";
            $parsedItems[] = [
                "id" => $itemId,
                "title" => "",
                "title_html" => "",
                "human_coding_scheme"   => $humanCodingScheme,
                "human_coding_scheme_html"   => $humanCodingSchemeHtml,
                "list_enumeration"      => $listEnumeration,
                "full_statement"        => $fullStatememnt,
                "full_statement_html"   => $fullStatememntHtml,
                "node_type"             => $nodeType,
                "metadataType"          => $nodeType,
                "node_type_id"          => $nodeTypeId,
                "item_type"             => "container",//!empty($typeMetadataDet->metadata_id) ? $typeMetadataDet->pivot->metadata_value : "",
                "project_enabled"       => $projectEnabled,
                "project_name"          => $projectName,
                "is_document"           => 0,
                "parent_id"             =>  $parentAndDocumentId[$itemId]['parent_id'],
                "hierarchy_id"          =>  $parentAndDocumentId[$itemId]['parent_id'],
                "document_id"           =>  $parentAndDocumentId[$itemId]['document_id'],
                "document_title"        =>  !empty($sourceDocumentDetail->title) ? $sourceDocumentDetail->title : "",
                "item_association_id"   =>  $itemAssociations[0]['item_association_id'] 
            ];
        }
        $this->setTransformedItems($parsedItems);
    }

    private function transformStandards() {
        $parsedStandards    =   [];

        $standardNodes              =   $this->getStandardNodesList();
        $setOfItemAssociationArray  =   $this->getItemAssociationIdArray();
        $setOfItemDestinationArray  =   $this->getItemDestinationIdArray();
        //dd($setOfItemDestinationArray);
        if(sizeOf($standardNodes) > 0) {
            foreach($standardNodes as $key => $item) {
                //dd($item[0]->item_id);
                //echo ($item->destination_node_id);
                $itemId                 =   $item[0]->item_id;
                $listEnumeration        =   !empty($item[0]->list_enumeration) ? $item[0]->list_enumeration : "";
                $humanCodingScheme      =   !empty($item[0]->human_coding_scheme) ? $item[0]->human_coding_scheme : "";
                $humanCodingSchemeHtml  =   !empty($item[0]->human_coding_scheme_html) ? $item[0]->human_coding_scheme_html : "";
                $fullStatememnt         =   !empty($item[0]->full_statement) ? $item[0]->full_statement : "";
                $fullStatememntHtml     =   !empty($item[0]->full_statement_html) ? $item[0]->full_statement_html : "";
                $nodeTypeId             =   !empty($item[0]->node_type_id) ? $item[0]->node_type_id : "";
                $nodeType               =   !empty($item[0]->nodeType) ? $item[0]->nodeType->title : "";
                $sourceDocumentDetail   =   $this->documentRepository->find($item[0]->document_id);

                $parsedStandards[] = [
                    "id" => $itemId,
                    "title" => "",
                    "human_coding_scheme"   =>  $humanCodingScheme,
                    "human_coding_scheme_html"   =>  $humanCodingSchemeHtml,
                    "list_enumeration"      =>  $listEnumeration,
                    "full_statement"        =>  $fullStatememnt,
                    "full_statement_html"   =>  $fullStatememntHtml,
                    "node_type"             =>  $nodeType,
                    "metadataType"          =>  $nodeType,
                    "node_type_id"          =>  $nodeTypeId,
                    "item_type"             =>  "standard",
                    "project_enabled"       =>  '1',
                    "is_document"           =>  0,
                    "parent_id"             =>  $item[0]->parent_id,
                    "hierarchy_id"          =>  isset($setOfItemDestinationArray[$key][0]->item_id) ? $setOfItemDestinationArray[$key][0]->item_id : $setOfItemDestinationArray[$key][0]->document_id,
                    "document_id"           =>  $item[0]->document_id,
                    "document_title"        =>  !empty($sourceDocumentDetail->title) ? $sourceDocumentDetail->title : "",
                    "document_title_html"   =>  !empty($sourceDocumentDetail->title_html) ? $sourceDocumentDetail->title_html : "",
                    "item_association_id"   =>  $key
                ];
            }
        }
        $this->setTransformedStandardItems($parsedStandards);
    }
    

    private function transformItemAssociations() {
        $itemAssociationsUnderDocument = $this->getItemAssociationsUnderDocument();
        
        $parsedItemAssociations = [];
        foreach($itemAssociationsUnderDocument as $itemAssociation) {
            $originNodeId = $itemAssociation->origin_node_id;
            $destinationNodeId = $itemAssociation->destination_node_id;
            if(!empty($originNodeId) && !empty($destinationNodeId)) {
                $parsedItemAssociations[] = [
                    "child_id" => $itemAssociation->origin_node_id,
                    "parent_id" => $itemAssociation->destination_node_id
                ];
            }
        }
        $this->setTransformedItemAssociationsUnderDocument($parsedItemAssociations);


        $itemRelationsUnderDocument = $this->getItemRelationsUnderDocument();
        
        $parsedItemRelations = [];
        foreach($itemRelationsUnderDocument as $itemAssociation) {
            $originNodeId = $itemAssociation->origin_node_id;
            $destinationNodeId = $itemAssociation->destination_node_id;
            if(!empty($originNodeId) && !empty($destinationNodeId)) {
                $parsedItemRelations[] = [
                    "child_id" => $itemAssociation->origin_node_id,
                    "parent_id" => $itemAssociation->destination_node_id
                ];
            }
        }

        $this->setTransformedItemRelationsUnderDocument($parsedItemRelations);
    }

    /**
     * create the ui compatible tree response structure
     */
    private function buildTree () {
        $transformedDocument        =   $this->getTransformedDocument();
        $transformedItems           =   $this->getTransformedItems();
        $transformedStandardItems   =   $this->getTransformedStandardItems();

        $transformedItemAssociationsUnderDocument   = $this->getTransformedItemAssociationsUnderDocument();
        $transformedItemRelationsUnderDocument      =   $this->getTransformedItemRelationsUnderDocument();   

        //dd($transformedItems);
        $nodes =    array_prepend(array_merge($transformedItems, $transformedStandardItems), $transformedDocument);
        $relations  =   array_merge($transformedItemAssociationsUnderDocument, $transformedItemRelationsUnderDocument);

        
        $treeData = [
            "nodes" => $nodes,
            "relations" => $relations,
        ];

        $this->setTree($treeData);
    }


    private function helperToReturnProjectEnabledStatus(string $identifierToSearchFor, array &$projectItemMapping): string {
        $statusToReturn = "0";
        if(!empty($projectItemMapping)) {
            foreach($projectItemMapping as $key=>$projectItemMap) {
                if($projectItemMap->item_id===$identifierToSearchFor && $projectItemMap->is_editable === 1) {
                    $statusToReturn = "1";
                    unset($projectItemMapping[$key]);
                    break;
                }
            }
        }
        return $statusToReturn;
    }
}
