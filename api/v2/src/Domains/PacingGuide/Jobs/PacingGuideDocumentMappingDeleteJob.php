<?php
namespace App\Domains\PacingGuide\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class PacingGuideDocumentMappingDeleteJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectId)
    {
        $this->setProjectId($projectId);
    }

    public function setProjectId(string $projectId) {
        $this->projectId = $projectId;
    }

    public function getProjectId(): string {
        return $this->projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->removeProjectIdsFromDocumentId();
    }

    public function removeProjectIdsFromDocumentId()
    {
        $projectId = $this->getProjectId();
        $this->projectRepository->deleteProjectIdAndDocumentId($projectId);
    }

    
}
