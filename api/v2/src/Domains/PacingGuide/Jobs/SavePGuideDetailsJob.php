<?php
namespace App\Domains\PacingGuide\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\PacingGuideRepositoryInterface;

class SavePGuideDetailsJob extends Job
{
    private $pGuideData;
    private $pGuideRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputData)
    {
        //Set the private attribute
        $this->pGuideData   =   $inputData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PacingGuideRepositoryInterface $pGuideRepository)
    {
        //Set the repo handler
        $this->pGuideRepository =   $pGuideRepository;

        $savePacingGuideDetail  =   $this->savePacingGuideDetail();

        return $savePacingGuideDetail;
    }

    private function savePacingGuideDetail() {
        $pGuideData =    $this->pGuideData;

        return $this->pGuideRepository->fillAndSave($pGuideData);

    }
}
