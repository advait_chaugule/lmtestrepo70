<?php
namespace App\Domains\PacingGuide\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Data\Models\Project;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class AddDocumentsUnderProjectJob extends Job
{

    private $projectRepository;
    private $documentRepository;
    private $itemRepository;

    private $itemNodeIdsToAssign;
    private $projectId;
    private $rootSelectedStatus;
    private $documentId;

    private $project;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemNodeIdsToAssign, string $projectId, bool $rootSelectedStatus, string $documentId)
    {   
        $this->setItemNodeIdsToAssign($itemNodeIdsToAssign);
        $this->setProjectId($projectId);
        $this->setRootSelectedStatus($rootSelectedStatus);
        $this->setDocumentId($documentId);
    }

    public function setItemNodeIdsToAssign(array $data) {
        $this->itemNodeIdsToAssign = $data;
    }

    public function getItemNodeIdsToAssign(): array {
        return $this->itemNodeIdsToAssign;
    }

    public function setProjectId(string $data) {
        $this->projectId = $data;
    }

    public function getProjectId(): string {
        return $this->projectId;
    }

    public function setRootSelectedStatus(bool $data) {
        $this->rootSelectedStatus = $data;
    }

    public function getRootSelectedStatus(): bool {
        return $this->rootSelectedStatus;
    }

    public function setDocumentId(string $data) {
        $this->documentId = $data;
    }

    public function getDocumentId(): string {
        return $this->documentId;
    }

    public function setProject(Project $data) {
        $this->project = $data;
    }

    public function getProject(): Project {
        return $this->project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ProjectRepositoryInterface $projectRepository,
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository
    )
    {
        // set db repositories
        $this->projectRepository    = $projectRepository;
        $this->documentRepository   = $documentRepository;
        $this->itemRepository       = $itemRepository;

        $this->removeDocumentIdFromItemNodeIdsToAssign();
        $this->fetchAndSetProject();
        
            $this->deleteItemNodesProjectMapping();
            //$this->deleteDocumentNodeProjectMapping();
            $this->mapItemNodeIdsWithProject();
            $this->mapDocumentIdsWithProject();
            //$this->mapDocumentIdWithProjectIfRootSelected();
            //$this->assignDocumentToProject();
        
    }

    private function removeDocumentIdFromItemNodeIdsToAssign() {
        $itemNodesToAssign = $this->getItemNodeIdsToAssign();
        $documentId = $this->getDocumentId();
        foreach($itemNodesToAssign as $key => $nodeId) {
            if($nodeId===$documentId){
                unset($itemNodesToAssign[$key]);
                break;
            }
        }
        $this->setItemNodeIdsToAssign($itemNodesToAssign);
    }

    private function fetchAndSetProject() {
        $projectIdentifier = $this->getProjectId();
        $project = $this->projectRepository->find($projectIdentifier);
        $this->setProject($project);
    }

    private function deleteItemNodesProjectMapping() {
        $project                = $this->getProject();
        $projectIdentifier      = $this->getProjectId();
        $documentId             = $this->getDocumentId();
        $itemNodeIdsToAssign    = $this->getItemNodeIdsToAssign();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_id'];
        $keyValuePairs      =   ['document_id'  =>  $documentId];
        
        $itemForDocumentSelected = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs);

        $getMappedNodes =   $this->projectRepository->getMappedNodes($projectIdentifier);

        foreach($itemForDocumentSelected as $item) {
            if(in_array($item['item_id'], $itemNodeIdsToAssign) == false){
                foreach($getMappedNodes as $mappedNodes) {
                    if($item['item_id'] == $mappedNodes['item_id']) {
                        $this->projectRepository->deleteProjectItem($item['item_id']);
                    }   
                }  
            }
        }
    }

    private function mapItemNodeIdsWithProject() {
        $project = $this->getProject();
        $itemNodesToAssign = $this->getItemNodeIdsToAssign();
        $project->items()->attach($itemNodesToAssign);
    }

    private function mapDocumentIdWithProjectIfRootSelected() {
        if($this->getRootSelectedStatus()) {
            $projectIdentifier = $this->getProjectId();
            $documentId = $this->getDocumentId();
            $conditionalKeyValuePairs = [ "document_id" => $documentId ];
            $dataToUpdate = [ "project_id" => $projectIdentifier ];
            $this->documentRepository->editWithCustomizedFields($conditionalKeyValuePairs, $dataToUpdate);
        }
    }

    private function mapDocumentIdsWithProject() {
        $project            = $this->getProject();
        $projectIdentifier  = $this->getProjectId();
        /* $DocumentIdToAssign = $this->getDocumentId();
        //$this->addDocumentIdToProject($DocumentIdToAssign);
        DB::table('project_pref_documents')->insert(
            ['project_id' => $project['project_id'],
             'document_id' => $DocumentIdToAssign]
        ); */

        $documentIdToAssign = $this->getDocumentId();

        $getMappedDocument  =   $this->projectRepository->getMappedDocument($projectIdentifier);

        foreach($getMappedDocument as $mappedDocument) {
            if($documentIdToAssign == $mappedDocument['document_id']){
                $project->prefDocument()->detach($documentIdToAssign);
            }
        }

        $project->prefDocument()->attach($documentIdToAssign);
    }

}
