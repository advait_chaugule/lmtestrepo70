<?php
namespace App\Domains\PacingGuide\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Workflow;

class ValidatePGuideInputJob extends Job
{
    public $input;
    protected $organizationId;
    protected $errorMessagesToSet;
    protected $errorMessagesAvailable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input                    =   $input;
        $this->organizationId           =   $this->input['organization_id'];
        $this->errorMessagesToSet       =   [];
        $this->errorMessagesAvailable   =   [
            "required" => [
                "name" => "Name is mandatory.",
                "workflow_id"   =>  "Workflow is mandatory."
            ],
            "exists"    =>  [
                "workflow_id"   =>  "Workflow is not correct."
            ]
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pGuideNameRequiredStatus       =   $this->getRequiredStatusForAttribute("name");
        $this->setErrorMessageOfValidationTypeForAttribute($pGuideNameRequiredStatus, "required", "name");

        $pGuideWorkflowRequiredStatus   =   $this->getRequiredStatusForAttribute("workflow_id");
        $this->setErrorMessageOfValidationTypeForAttribute($pGuideWorkflowRequiredStatus, "required", "workflow_id");

        $pGuideWorkflowExistsStatus   =   $this->getExistsStatusForAttribute("workflow_id");
        

        $overalStatus   =   $pGuideNameRequiredStatus && $pGuideWorkflowRequiredStatus;
        $jobResponse    =   $overalStatus===true ? $overalStatus : implode(",", $this->errorMessagesToSet);

        return $jobResponse;
    }

    private function getRequiredStatusForAttribute(string $attribute): bool {
        return !empty($this->input[$attribute]);
    }

    private function getExistsStatusForAttribute(string $attribute): bool {
        $conditionalClause      = [ [ "organization_id", "=", $this->organizationId] , [ "workflow_id" , "=", $this->input['workflow_id'] ]];
        
        $recordCount    =   Workflow::where($conditionalClause)->count();
        $status         =   $recordCount > 0 ? true : false;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "workflow_id");
        return $status;
    }

    private function setErrorMessageOfValidationTypeForAttribute(bool $validationStatus, string $validationType, string $attribute) {
        if($validationStatus===false) {
            $messageToPush = !empty($this->errorMessagesAvailable[$validationType][$attribute]) ?
                             $this->errorMessagesAvailable[$validationType][$attribute] : 
                             "";
            if(!empty($messageToPush)) {
                $this->errorMessagesToSet[] = $messageToPush;
            }
        }
    }
}
