<?php
namespace App\Domains\PacingGuide\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class PGuideCreateValidator extends BaseValidator {

    protected $rules = [
        'name' => 'required',
        'workflow_id' => 'required|exists:workflows'
    ];

     protected $messages = [
        'required'  =>  ":attribute is required.",
        'exists:workflows' => ':attribute is invalid.'
     ];
    
}