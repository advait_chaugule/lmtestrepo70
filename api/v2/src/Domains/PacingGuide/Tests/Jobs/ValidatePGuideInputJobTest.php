<?php
namespace App\Domains\PacingGuide\Tests\Jobs;

use App\Domains\PacingGuide\Jobs\ValidatePGuideInputJob;
use Tests\TestCase;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ValidatePGuideInputJobTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() {
        parent::setUp();
        // run seeder if required after environment has been setup
        Artisan::call('db:seed');

    }

    public function test_validate_p_guide_input_job()
    {
        //$this->markTestIncomplete();
        $pGuideInput        =   ['name' =>  '', 'description' =>  'This is a test pacing guide', 'workflow_id'  =>  'a428e24e-bdbd-46a2-af60-f37b261235c3', 'organization_id'   => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff'];

        $job         = $this->app->make('App\Domains\PacingGuide\Jobs\ValidatePGuideInputJob', ['input' => $pGuideInput]);

        $jobResponse = $job->handle();
        
        $this->assertContains('Name is mandatory.', $jobResponse);
        //$this->assertContains(':workflow is mandatory.', $jobResponse);
        $this->assertContains('Workflow is not correct.', $jobResponse);
    }
}
