<?php
namespace App\Domains\PacingGuide\Tests\Jobs;

use App\Domains\PacingGuide\Jobs\AddDocumentsUnderProjectJob;
use Tests\TestCase;

class AddDocumentsUnderProjectJobTest extends TestCase
{
    public function test_ProjectId()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new AddDocumentsUnderProjectJob($data,'ad323249-1bf7-4321-ab84-d345aa9c06d1',1,'0ad827ae-96ca-46a0-bf63-9084f2594943');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setProjectId($value);
        $this->assertEquals($value,$assets->getProjectId());
    }

    public function test_RootSelectedStatus()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new AddDocumentsUnderProjectJob($data,'ad323249-1bf7-4321-ab84-d345aa9c06d1',1,'0ad827ae-96ca-46a0-bf63-9084f2594943');
        $value = true;
        $assets->setRootSelectedStatus($value);
        $this->assertEquals($value,$assets->getRootSelectedStatus());
    }

    public function test_DocumentId()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new AddDocumentsUnderProjectJob($data,'ad323249-1bf7-4321-ab84-d345aa9c06d1',1,'0ad827ae-96ca-46a0-bf63-9084f2594943');
        $value = true;
        $assets->setDocumentId($value);
        $this->assertEquals($value,$assets->getDocumentId());
    }
}
