<?php
namespace App\Domains\PacingGuide\Tests\Jobs;

use App\Domains\PacingGuide\Jobs\GetFlattenedTaxonomyTreeForPacingGuideJob;
use Tests\TestCase;

class GetFlattenedTaxonomyTreeForPacingGuideJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new GetFlattenedTaxonomyTreeForPacingGuideJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_ParentAndDocumentId()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new GetFlattenedTaxonomyTreeForPacingGuideJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setParentAndDocumentId($value);
        $this->assertEquals($value,$assets->getParentAndDocumentId());
    }

    public function test_StandardNodesList()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new GetFlattenedTaxonomyTreeForPacingGuideJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setStandardNodesList($value);
        $this->assertEquals($value,$assets->getStandardNodesList());
    }

    public function test_ItemAssociationIdArray()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new GetFlattenedTaxonomyTreeForPacingGuideJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemAssociationIdArray($value);
        $this->assertEquals($value,$assets->getItemAssociationIdArray());
    }

    public function test_ItemDestinationIdArray()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new GetFlattenedTaxonomyTreeForPacingGuideJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemDestinationIdArray($value);
        $this->assertEquals($value,$assets->getItemDestinationIdArray());
    }
}
