<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;
use Jaspersoft\Client\Client;
use Illuminate\Http\Request;
use Config;

class JReportTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,String $file_type, String $report_type,String $organization_id, String $document_id)
    {
        $this->connection=$connection;
        $this->file_type=$file_type;
        $this->report_type=$report_type;
        $this->organization_id=$organization_id;
        $this->document_id=$document_id;        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {  

        $valid_file_types= config('jasper.jasper_file_type');

          if(!in_array($this->file_type,$valid_file_types)){
              echo 'Not valid file type';
              die;
          }                
         $controls = array(
            'organization_id' =>$this->organization_id,    
            'document_id' =>$this->document_id,
            );      
                   
            
         $report = $this->connection->reportService()->runReport('/reports/interactive/'.$this->report_type, $this->file_type, null, null, $controls);
         
         return  $report;         
          
    }
}
