<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class ViewJasperReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,$organization_id,$file_type,$report_type,$controls)
    {
        $this->connection = $connection;
        $this->organization_id = $organization_id;
        $this->file_type = $file_type;
        $this->report_type = $report_type;
        $this->controls = $controls;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $report = $this->connection->reportService()->runReport('/reports/interactive/'.$this->report_type, $this->file_type, null, null, $this->controls);
        return $report;
    }
}
