<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class JReportUserDemographicJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,String $file_type, String $report_type,String $organization_id,String $from_date, String $to_date)
    {
        $this->connection=$connection;
        $this->file_type=$file_type;
        $this->report_type=$report_type;
        $this->organization_id=$organization_id;
        
        $this->from_date=$from_date;
        $this->to_date=$to_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $valid_file_types= config('jasper.jasper_file_type');

        if(!in_array($this->file_type,$valid_file_types)){
            echo 'Not valid file type';
            die;
        }                
       $controls = array(
          'organization_id' =>$this->organization_id,  
          'from_date' =>$this->from_date,
          'to_date' =>$this->to_date,
          );   
          
          //print_r($controls); die;
                 
          
       $report = $this->connection->reportService()->runReport('/reports/interactive/'.$this->report_type, $this->file_type, null, null, $controls);
       
       return  $report;  
    }
}
