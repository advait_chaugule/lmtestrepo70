<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class ExportCustomViewJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,$organization_id,$document_id,$file_type)
    {
        $this->connection = $connection;
        $this->organization_id = $organization_id;
        $this->document_id = $document_id;
        $this->file_type = $file_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $report_type=  'World_Languages_Full_Taxonomy';          

        $controls = array(
        'orgid' =>$this->organization_id,    
        'docid' =>$this->document_id,
        'itemid' =>$this->document_id,
        );             
       
        $report = $this->connection->reportService()->runReport('/reports/interactive/'.$report_type, $this->file_type, null, null, $controls);

        return $report;
    }
}
