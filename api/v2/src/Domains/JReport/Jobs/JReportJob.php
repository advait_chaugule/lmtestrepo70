<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;
use Jaspersoft\Client\Client;
use Jaspersoft\Dto\Organization;
use Illuminate\Http\Request;
use App\Data\Repositories\Contracts\JReportRepositoryInterface;

class JReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $input)
    {        
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(JReportRepositoryInterface $JReportRepository)
    {
        $this->JReportRepository = $JReportRepository;
        $attributes = ['active_access_token' => $this->identifier, 'is_deleted'=>0];
        $organizationDetails = $this->JReportRepository->findByAttributes($attributes); 
        if(isset($organizationDetails[0])){
            return  $organizationDetails[0]->organization_id; 
        }else{
            return 0;
        }
            
    }
}
