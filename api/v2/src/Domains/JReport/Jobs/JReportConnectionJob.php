<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;
use Jaspersoft\Client\Client;
use Config;


class JReportConnectionJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $c = new Client(
            config('jasper.jasper_host'),
            config('jasper.jasper_username'),
            config('jasper.jasper_password')
          );
        return $c;
    }
}
