<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;
use Config;

class SetContentTypeJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $file_type,string $content,string $file_name)
    {
        $this->file_type=$file_type;
        $this->content=$content;
        $this->file_name=$file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {               
         
          $valid_file_types=   config('jasper.jasper_file_type') ;

          $file_type=  $this->file_type;
          
          
          if(!in_array($file_type,$valid_file_types)){
              echo 'Not valid file type';
              die;
          }             
                 
                  
          $file_name=$this->file_name.'.'.$file_type;
          
          switch ($file_type){
              case 'html':
                  header('Access-Control-Allow-Origin: *');
                  echo $this->content;
                  die;// no header required, don't execute code further
              break;
              case 'pdf':
                  
                  $content_type='application/pdf';
              break;
          
              case 'xls':
                  
                  $content_type='application/vnd.ms-excel; charset=utf-8';
              break;
          
              case 'csv':
                  
                  $content_type='text/csv';
              break;
          
              case 'docx':
          
                  $content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document';
              break;
          
              case 'rtf':
              
                  $content_type='application/rtf';
              break;
          
              case 'odt':
                  
                  $content_type='application/vnd.oasis.opendocument.text';
              break;
          
              case 'ods':
                          
                  $content_type='application/vnd.oasis.opendocument.spreadsheet';
              break;
          
              case 'xlsx':
                  
                  $content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
              break;
          
              case 'pptx':
                  
                  $content_type='application/vnd.openxmlformats-officedocument.presentationml.presentation';
              break;
          
              default:
          
          }
         
         //workaround to make download work for big files 
         if(strlen($this->content) > config('jasper.jasper_report_download_size'))
            header('Access-Control-Allow-Origin: *');  

          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Description: File Transfer');
          header("Content-disposition: attachment; filename=\"".$file_name."\"");
          header('Content-Transfer-Encoding: binary');
          header('Content-Length: ' . strlen($this->content));
          header('Content-Type: '.$content_type); 
          
          echo $this->content;
    }
}
