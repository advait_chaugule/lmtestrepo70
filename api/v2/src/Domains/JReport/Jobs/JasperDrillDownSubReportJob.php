<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class JasperDrillDownSubReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,$organization_id,$file_type,$document_id)
    {
        $this->connection = $connection;
        $this->organization_id = $organization_id;        
        $this->file_type = $file_type;
        $this->document_id = $document_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $report_type=  'drill_down_sub_report';          

        $controls = array(
        'organization_id' =>$this->organization_id,            
        'document_id' =>$this->document_id,
        );             

        $report = $this->connection->reportService()->runReport('/reports/interactive/'.$report_type, $this->file_type, null, null, $controls);

        return $report;
    }
}
