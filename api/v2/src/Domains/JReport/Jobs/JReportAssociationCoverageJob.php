<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class JReportAssociationCoverageJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,String $file_type, String $report_type,String $organization_id,String $target_taxonomy, String $source_taxonomy)
    {
        $this->connection=$connection;
        $this->file_type=$file_type;
        $this->report_type=$report_type;
        $this->organization_id=$organization_id;
        
        $this->target_taxonomy=$target_taxonomy;
        $this->source_taxonomy=$source_taxonomy;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $valid_file_types= config('jasper.jasper_file_type');

        if(!in_array($this->file_type,$valid_file_types)){
            echo 'Not valid file type';
            die;
        }            
    
        $source_taxonomy="'".str_replace(",","','",$this->source_taxonomy)."'";       
        
        $controls = array(
         'organization_id' =>$this->organization_id,  
          'target_document_id' =>"'".$this->target_taxonomy."'",
          'source_document_id' =>$source_taxonomy,
          );   
          
        //print_r($controls); die;
                 
          
       $report = $this->connection->reportService()->runReport('/reports/interactive/'.$this->report_type, $this->file_type, null, null, $controls);
       
       return  $report;  
    }
}
