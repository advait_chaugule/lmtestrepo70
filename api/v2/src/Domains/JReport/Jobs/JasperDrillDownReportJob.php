<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class JasperDrillDownReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,$organization_id,$file_type)
    {
        $this->connection = $connection;
        $this->organization_id = $organization_id;        
        $this->file_type = $file_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $report_type=  'drill_down_main';          

        $controls = array(
        'organization_id' =>$this->organization_id,            
        );             

        $report = $this->connection->reportService()->runReport('/reports/interactive/'.$report_type, $this->file_type, null, null, $controls);

        return $report;
    }
}
