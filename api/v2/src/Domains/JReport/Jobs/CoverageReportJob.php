<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class CoverageReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connection,$file_type,$controls)
    {
        $this->connection = $connection;
        $this->file_type = $file_type;
        $this->controls = $controls;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $report_type=  'AssociationCoverageV2';
        
        $report = $this->connection->reportService()->runReport('/reports/interactive/'.$report_type, $this->file_type, null, null, $this->controls);

        return $report;
        
    }

    
}
