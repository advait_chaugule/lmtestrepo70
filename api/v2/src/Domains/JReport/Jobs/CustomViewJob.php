<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;

class CustomViewJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $connection,$organization_id,$document_id, $item_id,$file_type,$report_type)
    {        
        $this->connection = $connection;
        $this->organization_id = $organization_id;
        $this->document_id = $document_id;
        $this->item_id = $item_id;
        $this->file_type = $file_type;
        $this->report_type = $report_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {                   
                  
        //$report_type=  'World_Languages_Full_Taxonomy';          
        $report_type=$this->report_type;

        $controls = array(
        'orgid' =>$this->organization_id,    
        'docid' =>$this->document_id,
        'itemid' =>$this->item_id,
        );             

        $report = $this->connection->reportService()->runReport('/reports/interactive/'.$report_type, $this->file_type, null, null, $controls);

        return $report;          
    }
}
