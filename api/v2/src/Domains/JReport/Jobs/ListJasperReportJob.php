<?php
namespace App\Domains\JReport\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Data\Models\ReportConfig;

class ListJasperReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->requestData = $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $requestData = $this->requestData;
        
        $getInternalName = DB::table('permissions as p')
                ->select('internal_name')
                ->leftJoin('organization_permissions as op', 'p.permission_id', '=', 'op.permission_id')
                ->where(['op.organization_id' => $requestData['auth_user']['organization_id']])
                ->get()
                ->toArray();

        if(!empty($getInternalName))
        {
            $listInternalName = array();
            foreach($getInternalName as $getInternalNameK=>$getInternalNameV)
            {
                $listInternalName[$getInternalNameK]=$getInternalNameV->internal_name;
            }
            
            $getReportConfigData = ReportConfig::Select('report_config_id','internal_name','title','parameter_1_display_name','parameter_1_subtype',
                                                'parameter_1_prebuild_type','parameter_2_display_name','parameter_2_subtype','parameter_2_prebuild_type',
                                                'parameter_3_display_name','parameter_3_subtype','parameter_3_prebuild_type','parameter_4_display_name',
                                                'parameter_4_subtype','parameter_4_prebuild_type','parameter_5_display_name','parameter_5_subtype','parameter_5_prebuild_type','endpoint_url')
                                                ->whereIn('internal_name',$listInternalName)
                                                ->where('is_deleted','=',0)
                                                ->get()
                                                ->toArray();
            return $getReportConfigData;

        }
        else
        {
            $getReportConfigData = array();
            return $getReportConfigData;
        }

    }
}
