<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class GetAllWorkflowRolesJob extends Job
{
    private $identifier;
    private $roleIdentifier;
    private $organizationIdentifier;
    
    private $workflowRepository;
    private $workflowStageId;

    private $roleList;
    private $roleListEntity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $identifier, string $organizationIdentifier)
    {
        //Set the identifier
        $this->setIdentifier($identifier['workflow_id']);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepository, RoleRepositoryInterface $roleRepository)
    {
        //Set the workflow repo handler
        $this->workflowRepository = $workflowRepository;
        $this->roleRepository     = $roleRepository;

        $workflowStageList = $this->fetchWorkflowStageList();
        $workflowStageListRoles = $this->fetchWorkflowStageListRoles();
        
        $this->parseRoleListData();
        $responseData = $this->getRoleListEntity();
        
        return $responseData;
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setRoleIdentifier($roleIdentifier) {
        $this->roleIdentifier = $roleIdentifier;
    }

    public function getRoleIdentifier() {
        return $this->roleIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setWorkflowStageIdData($data) {
        $this->workflowStageId = $data;
    }

    public function getWorkflowStageIdData() {
        return $this->workflowStageId;
    }

    public function setRoleList($data) {
        $this->roleList = $data;
    }

    public function getRoleList() {
        return $this->roleList;
    }

    public function setRoleListEntity($data) {
        $this->roleListEntity = $data;
    }

    public function getRoleListEntity() {
        return $this->roleListEntity;
    }

    //Method to fetch list of stage for a workflow
    private function fetchWorkflowStageList(){
        $workflowStageIDArray = [];
        $workflowId = $this->getIdentifier();
        $organizationIdentifer = $this->getOrganizationIdentifier();
        $workflowStageList = $this->workflowRepository->geWorkflowDetails($organizationIdentifer, $workflowId);

        foreach($workflowStageList['stages'] as $workflowStages){
            $workflowStageIDArray[] = $workflowStages->workflow_stage_id;
        }

        $this->setWorkflowStageIdData($workflowStageIDArray);
    }

    //Method to list distinct RoleIds for all stages of a workflow
    private function fetchWorkflowStageListRoles(){
        $workflowStageIDArray = $this->getWorkflowStageIdData();

        $roleList = $this->workflowRepository->fetchRoleList($workflowStageIDArray);
        
        $this->setRoleList($roleList);
    }

    //Method to check role in the list is active and not deleted
    private function checkRoleExists() {
        $roleIdentifier = $this->getRoleIdentifier();

        $attributes = [
            'role_id' => $roleIdentifier,
            'is_active' => '1',
            'is_deleted' => '0'
        ];

        $roleDetail = $this->roleRepository->findByAttributes($attributes)->first();
        
        if(!empty($roleDetail->role_id)){
            return true;
        }
        else{
            return false;
        }

    }

    // Method to get role name
    private function getRoleName() {
        $roleIdentifier = $this->getRoleIdentifier();

        $roleDetail = $this->roleRepository->find($roleIdentifier);
        
        return $roleDetail->name;
    }

    private function parseRoleListData(){
        $roleEntity = [];
        $roleListData = $this->getRoleList();

        foreach($roleListData as $roles){
            $this->setRoleIdentifier($roles->role_id);
            $roleExists = $this->checkRoleExists();
            if($roleExists === true){
                $roleEntity[] = [
                    'workflow_stage_role_id' => $roles->workflow_stage_role_id,
                    'role_name'                   => $this->getRoleName(),
                ];
            } 
        }

        $this->setRoleListEntity($roleEntity);
    }
}
