<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;

class GetWorkflowDetailsJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepository)
    {
        
        
        $organizationId = $this->input['auth_user']['organization_id']; 
        $workflowId = $this->input['workflowId']; 
       
        // set user data repository
        $this->workflowRepository = $workflowRepository;

        // get user details
        $workflowDetails = $this->workflowRepository->geWorkflowDetails($organizationId, $workflowId);

        // prepare the response
        return ['workflow' => $workflowDetails];
    }
}
