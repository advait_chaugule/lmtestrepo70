<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;

class UpdateWorkflowJob extends Job
{
    private $workflowIdentifier;
    private $organizationIdentifier;
    private $requestData;

    private $workflowRepository;
    private $workflowStageRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, string $organizationIdentifier, array $requestData)
    {
        //Set the private identifier attribute
        $this->setWorkflowIdentifier($identifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->requestData  =   $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepository, WorkflowStageRepositoryInterface $workflowStageRepository)
    {
        //set the workflow repository handler
        $this->workflowRepository       =   $workflowRepository;
        $this->workflowStageRepository  =   $workflowStageRepository;
        
        $editWorkflowStage  =   $this->editWorkflowStageOrder();

        return $this->parseResponse();
    }

    //Public Setter and Getter methods
    public function setWorkflowIdentifier($identifier) {
        $this->workflowIdentifier   =   $identifier;
    }

    public function getWorkflowIdentifier() {
        return $this->workflowIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    private function editWorkflowStageOrder()   {
        $workflowIdentifier =   $this->getWorkflowIdentifier();
        $requestData        =   $this->requestData; 
        foreach($requestData as $workflowStage) {
            $columnValueFilterPairs =   ['workflow_id'  =>  $workflowIdentifier, 'workflow_stage_id'    => $workflowStage['workflow_stage_id']];
            $attributes             =   ['order'    =>  $workflowStage['order']];

            $workflowDetails    =   $this->workflowStageRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        }
        
    }

    private function parseResponse() {
        $workflowIdentifier =   $this->getWorkflowIdentifier();
        $organizationId     =   $this->getOrganizationIdentifier();

        $workflowDetails    =   $this->workflowRepository->geWorkflowDetails($organizationId, $workflowIdentifier);

        // prepare the response
        return ['workflow' => $workflowDetails];

    }
}
