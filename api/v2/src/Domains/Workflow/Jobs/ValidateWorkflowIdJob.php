<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Workflow\Validators\WorkflowIdValidator;

class ValidateWorkflowIdJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle(WorkflowIdValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
