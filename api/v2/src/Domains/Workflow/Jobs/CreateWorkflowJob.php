<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;

class CreateWorkflowJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepository)
    {
        return $workflowRepository->fillAndSave($this->input);
    }
}
