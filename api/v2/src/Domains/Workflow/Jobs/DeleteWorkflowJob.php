<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;

class DeleteWorkflowJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepo)
    {
        $workflowId = $this->input['workflow_id'];

        $queryToExecute = DB::table('workflow_stage')->select('workflow_stage_id','order')->where('workflow_id', $workflowId);
        $workflowrStageresult = $queryToExecute->get()->toArray();
        foreach($workflowrStageresult as $key => $workflowrStage)
        {
            $workflowstageId = $workflowrStage->workflow_stage_id;
            DB::table('workflow_stage_role')->where('workflow_stage_id', '=', $workflowstageId)->delete();
            DB::table('workflow_stage')->where('workflow_stage_id', '=', $workflowstageId)->delete();   
        }
        // remove id from the input array since it has been assigned to $id variable
        $input['is_deleted']= 1;
        return $workflowRepo->edit($workflowId, $input);
    }
}
