<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Models\Workflow;

class ValidateCreateWorkflowInputJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $requestObject;
    protected $organizationId;
    protected $dataToValidate;
    protected $errorMessagesAvailable;
    protected $errorMessagesToSet;

    public function __construct(array $requestData)
    {
        //
        $this->setRequestUserOrganizationId($requestData);
        $this->dataToValidate = $requestData;
        $this->errorMessagesAvailable = [
            "required" => [
                "name" => "Workflow name is mandatory.",
            ],
            "exists" => [
                "name" => "Workflow name already exists."
            ]
        ];
        $this->errorMessagesToSet = [];
    }

    public function setRequestUserOrganizationId(array $requestData) {
        $this->organizationId = $requestData["auth_user"]["organization_id"];
    }

    public function getRequestUserOrganizationId(): string {
        return $this->organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        // set the injected laravel request object
        $this->requestObject = $request;


        $nameRequiredStatus = $this->getRequiredStatusForAttribute("name");
        $this->setErrorMessageOfValidationTypeForAttribute($nameRequiredStatus, "required", "name");

        $nameExistsStatus =  $nameRequiredStatus ? 
                                    $this->getNameExistsStatusAndSetErrorMessageAccordingly() : 
                                    true;
                                    
        $overallStatus = $nameRequiredStatus && $nameExistsStatus;


        
        $jobResponse = $overallStatus===true ? $overallStatus : implode(",", $this->errorMessagesToSet);

        return $jobResponse;
        //
    }

    private function getNameExistsStatusAndSetErrorMessageAccordingly(): bool {
        $organizationId = $this->getRequestUserOrganizationId();
        $workflowName = $this->dataToValidate["name"];

        $conditionalClause = [ [ "organization_id", "=", $organizationId] , ["is_deleted", "=", 0]];
        $conditionalClause[] = [ "name" , "=", $workflowName ];
        $recordCount = Workflow::where($conditionalClause)->count();
        $status = $recordCount > 0 ? false : true;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "name");
        return $status;
    }

    private function getRequiredStatusForAttribute(string $attribute): bool {
        return !empty($this->dataToValidate[$attribute]);
    }

    
    private function setErrorMessageOfValidationTypeForAttribute(bool $validationStatus, string $validationType, string $attribute) {
        if($validationStatus===false) {
            $messageToPush = !empty($this->errorMessagesAvailable[$validationType][$attribute]) ?
                             $this->errorMessagesAvailable[$validationType][$attribute] : 
                             "";
            if(!empty($messageToPush)) {
                $this->errorMessagesToSet[] = $messageToPush;
            }
        }
    }

}
