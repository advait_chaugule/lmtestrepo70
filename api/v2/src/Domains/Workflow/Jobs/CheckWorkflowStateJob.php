<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;

class CheckWorkflowStateJob extends Job
{
    private $identifier;
    private $attribute;
    private $workflowRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $identifier, array $attribute)
    {
        //Set the workflow identifier
        $this->setIdentifier($identifier['workflow_id']);
        $this->attribute = $attribute;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepository)
    {
        //Set the workflow repo handler
        $this->workflowRepository = $workflowRepository;
        $workflowId = $this->getIdentifier();

        $attributes = ['workflow_id' => $workflowId];
        $columnToSearch = array_keys($this->attribute);
        $attributes[$columnToSearch[0]] = 1;
        $workflowDetail = $this->workflowRepository->findByAttributes($attributes);

        if($columnToSearch[0] == 'is_deleted') {
            if(!empty($workflowDetail->workflow_id)) {
                return true;
            }
            else{
                return false;
            }
        }

        if($columnToSearch[0] == 'is_active'){
            if(!empty($workflowDetail[0]->workflow_id)) {
                return true;
            }
            else{
                return false;
            }
        }
        
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }
}
