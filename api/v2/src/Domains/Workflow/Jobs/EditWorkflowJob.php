<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;

class EditWorkflowJob extends Job
{

    private $workflowIdentifier;
    private $organizationIdentifier;
    private $requestData;

    private $workflowRepository;
    private $workflowStageRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, string $organizationIdentifier, array $requestData)
    {
        //Set the private identifier attribute
        $this->setWorkflowIdentifier($identifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->requestData  =   $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepository)
    {
        //
        $workflowIdentifier =   $this->getWorkflowIdentifier();
        $workflowName        =   $this->requestData['name'];
        $input['name'] = $workflowName;
        $input['updated_by'] = $this->requestData['auth_user']['user_id'];
        $input['updated_at'] = now()->toDateTimeString();
        return $workflowRepository->edit($workflowIdentifier, $input);
    }

    public function setWorkflowIdentifier($identifier) {
        $this->workflowIdentifier   =   $identifier;
    }

    public function getWorkflowIdentifier() {
        return $this->workflowIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }
}
