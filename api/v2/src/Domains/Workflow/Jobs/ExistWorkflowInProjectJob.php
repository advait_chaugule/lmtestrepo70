<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Project;
use Illuminate\Support\Facades\DB;

class ExistWorkflowInProjectJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $workflowId = $this->input['workflow_id'];
        $tableName = "projects";
        $queryToExecute = DB::table($tableName)->select('project_name')->where([['workflow_id', '=', $workflowId],['is_deleted', '=', 0]]);
        $projectresult = $queryToExecute->get()->toArray();

        if(count($projectresult) > 0)
        {
            foreach($projectresult as $project)
            {
                $projectArr[] = $project->project_name;
            }
            $projectList = implode(',', $projectArr);
            return $projectList;
        }
        else
        {
            return "";
        }
        
    }
}
