<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;

use App\Services\Api\Traits\DateHelpersTrait;

class GetWorkflowJob extends Job
{
    use DateHelpersTrait;

    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowRepositoryInterface $workflowRepo)
    {
        $organizationId = $this->data['auth_user']['organization_id'];
        $workflows = $workflowRepo->getWorkflowList($organizationId);

        $parsedWorkflow = $this->parseResponse($workflows);
        $count = sizeOf($parsedWorkflow);
        //$parsedWorkflow = (object) $parsedWorkflow;

        $response = ['workflows' => $parsedWorkflow, 'count' => $count ];

        return $response;
    }

    private function parseResponse($data){
        $createdBy = '';
        $workFlowEntity = [];

        foreach($data as $workflow){
            if($workflow->updated_by == null){
                $createdBy = 'System';
            }
            else{
                $createdBy = isset($workflow->user->name)?$workflow->user->name:'';
            }
            
            $workFlowEntity[] = [
                "workflow_id" => $workflow->workflow_id,
                "organization_id" => $workflow->organization_id,
                "name" => $workflow->name, 
                "workflow_code"=> $workflow->workflow_code,
                "is_default" => $workflow->is_default,
                "is_active" => $workflow->is_active,
                "is_deleted" => $workflow->is_deleted,
                "updated_by" => $createdBy,
                "created_at" => $this->formatDateTimeToISO8601($workflow->created_at),
                "updated_at" => $this->formatDateTimeToISO8601($workflow->updated_at),
            ];
        }

        return $workFlowEntity;        
    }
}
