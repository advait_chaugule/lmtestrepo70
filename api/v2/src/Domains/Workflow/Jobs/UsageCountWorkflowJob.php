<?php
namespace App\Domains\Workflow\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class UsageCountWorkflowJob extends Job
{
    private $projectRepository;
    private $workflowIdentifierSet;
    private $organizationIdentifier;
    private $usageCount;
    private $countArray;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $workflowIdentifierArray, string $organizationIdentifier)
    {
        //Set identifier array from input
        $this->setWorkflowIdentifierSet($workflowIdentifierArray);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set project repository handler
        $this->projectRepository = $projectRepository;

        $usageCountDetail = $this->fetchUsageCount();
        $this->setUsageCount($usageCountDetail);

        $this->parseUsageCountResponse();

        $countArray = $this->getParsedUsageCount();
        return $countArray;

    }

    // Public Getter and Setter methods
    public function setWorkflowIdentifierSet(array $identifier) {
        $this->workflowIdentifierSet = $identifier;
    }

    public function getWorkflowIdentifierSet() {
        return $this->workflowIdentifierSet;
    }

    public function setOrganizationIdentifier(string $identifier) {
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setUsageCount($usageCountDetail) {
        $this->usageCount = $usageCountDetail;
    }

    public function getUsageCount() {
        return $this->usageCount;
    }

    public function setParsedUsageCount($countArray) {
        $this->countArray = $countArray;
    }

    public function getParsedUsageCount() {
        return $this->countArray;
    }

    private function fetchUsageCount(){
        $workflowIdentifierSet = $this->getWorkflowIdentifierSet();
        $organizationIdentifier = $this->getOrganizationIdentifier();

        $model = 'projects';
        $mapColumn = 'workflow_id';

        $queryParam = ['projects.organization_id' => $organizationIdentifier, 'projects.is_deleted' => '0'];
        $fieldsToCount = 'project_id';
        $attributesToGroupBy = ['workflow_id'];

        $usageCount = $this->projectRepository->projectCount($model, $mapColumn, $workflowIdentifierSet, $fieldsToCount, $queryParam);
        return $usageCount;
    }

    private function parseUsageCountResponse(){
        $count = [];
        $usageCount = $this->getUsageCount();
        
        foreach($usageCount as $key => $countArray){
            $count[$key] = !empty($countArray) ? $countArray : 0 ; 
        }

        $countArray = $this->setParsedUsageCount($count);

        return $countArray;
    }
}
