<?php
namespace App\Domains\Workflow\Tests\Jobs;

use App\Domains\Workflow\Jobs\DuplicateWorkflowJob;
use Tests\TestCase;

class DuplicateWorkflowJobTest extends TestCase
{
    public function test_WorkflowIdentifier()
    {
        $data1[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new DuplicateWorkflowJob('a42c92f9-52b9-4bd9-9241-ae1f54e10472','9ffcbc1c-7df4-438c-bbfe-92e519d05466',$data1);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setWorkflowIdentifier($value);
        $this->assertEquals($value,$assets->getWorkflowIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $data1[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new DuplicateWorkflowJob('a42c92f9-52b9-4bd9-9241-ae1f54e10472','9ffcbc1c-7df4-438c-bbfe-92e519d05466',$data1);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
