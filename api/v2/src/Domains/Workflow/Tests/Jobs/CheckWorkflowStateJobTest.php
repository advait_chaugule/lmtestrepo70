<?php
namespace App\Domains\Workflow\Tests\Jobs;

use App\Domains\Workflow\Jobs\CheckWorkflowStateJob;
use Tests\TestCase;

class CheckWorkflowStateJobTest extends TestCase
{
    public function test_Identifier()
    {
        $data1['workflow_id'] = 'a42c92f9-52b9-4bd9-9241-ae1f54e10472';
        $data2[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new CheckWorkflowStateJob($data1,$data2);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
