<?php
namespace App\Domains\Workflow\Tests\Jobs;

use App\Domains\Workflow\Jobs\ValidateCreateWorkflowInputJob;
use Tests\TestCase;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Illuminate\Http\Request;
use App\Services\Api\Traits\StringHelper;

use Illuminate\Support\Facades\DB;

class ValidateCreateWorkflowInputJobTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp() {
    parent::setUp();
        // run seeder if required after environment has been setup
   Artisan::call('db:seed');
    }

    public function test_validate_create_workflow_input_job()
    {
       // $this->markTestIncomplete();
    
       $data = array(
        array('workflow_id' => '2d939319-27b1-4c88-adaa-a73b0fd24dc8','organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff','name' => 'ACMT','is_active' => '1','is_deleted' => '0','is_default' => '0','updated_by' => NULL,'created_at' => '2018-12-04 06:30:53','updated_at' => '2018-12-04 06:30:53')
    );
    DB::table('workflows')->insert($data);

       $request = new Request();

       $requestData = ['name' => ""];
       $requestData["auth_user"]["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff"; 
       
       $obj = new ValidateCreateWorkflowInputJob($requestData);

       $request->merge($requestData);
       $validateCreateWorkflowInput = $obj->handle($request);
       
      $this->assertContains('Workflow name is mandatory.', $validateCreateWorkflowInput);



      $request1 = new Request();

      $requestData1 = ['name' => "Default workflow"];
      $requestData1["auth_user"]["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff"; 
      
      $obj1 = new ValidateCreateWorkflowInputJob($requestData1);

      $request1->merge($requestData1);
      $validateCreateWorkflowInput1 = $obj1->handle($request1);
      
     $this->assertContains('Workflow name already exists.', $validateCreateWorkflowInput1);

    
     $request2 = new Request();

      $requestData2 = ['name' => "PHP TEST"];
      $requestData2["auth_user"]["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff"; 
      
      $obj2 = new ValidateCreateWorkflowInputJob($requestData2);

      $request2->merge($requestData2);
      $validateCreateWorkflowInput2 = $obj2->handle($request2);
      
      $this->assertTrue((boolean)$validateCreateWorkflowInput2);

    }
}
