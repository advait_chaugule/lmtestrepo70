<?php
namespace App\Domains\Workflow\Tests\Jobs;

use App\Domains\Workflow\Jobs\UsageCountWorkflowJob;
use Tests\TestCase;

class UsageCountWorkflowJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $data1[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UsageCountWorkflowJob($data1,'3585a768-d90c-4936-9386-05992088154b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_UsageCount()
    {
        $data1[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UsageCountWorkflowJob($data1,'3585a768-d90c-4936-9386-05992088154b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUsageCount($value);
        $this->assertEquals($value,$assets->getUsageCount());
    }

    public function test_ParsedUsageCount()
    {
        $data1[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UsageCountWorkflowJob($data1,'3585a768-d90c-4936-9386-05992088154b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setParsedUsageCount($value);
        $this->assertEquals($value,$assets->getParsedUsageCount());
    }
}
