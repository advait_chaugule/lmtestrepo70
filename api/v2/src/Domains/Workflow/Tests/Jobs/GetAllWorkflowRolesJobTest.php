<?php
namespace App\Domains\Workflow\Tests\Jobs;

use App\Domains\Workflow\Jobs\GetAllWorkflowRolesJob;
use Tests\TestCase;

class GetAllWorkflowRolesJobTest extends TestCase
{
    public function test_Identifier()
    {
        $data1['workflow_id'] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new GetAllWorkflowRolesJob($data1,'9ffcbc1c-7df4-438c-bbfe-92e519d05466');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_RoleIdentifier()
    {
        $data1['workflow_id'] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new GetAllWorkflowRolesJob($data1,'9ffcbc1c-7df4-438c-bbfe-92e519d05466');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleIdentifier($value);
        $this->assertEquals($value,$assets->getRoleIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $data1['workflow_id'] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new GetAllWorkflowRolesJob($data1,'9ffcbc1c-7df4-438c-bbfe-92e519d05466');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_WorkflowStageIdData()
    {
        $data1['workflow_id'] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new GetAllWorkflowRolesJob($data1,'9ffcbc1c-7df4-438c-bbfe-92e519d05466');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setWorkflowStageIdData($value);
        $this->assertEquals($value,$assets->getWorkflowStageIdData());
    }

    public function test_RoleList()
    {
        $data1['workflow_id'] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new GetAllWorkflowRolesJob($data1,'9ffcbc1c-7df4-438c-bbfe-92e519d05466');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleList($value);
        $this->assertEquals($value,$assets->getRoleList());
    }

    public function test_RoleListEntity()
    {
        $data1['workflow_id'] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new GetAllWorkflowRolesJob($data1,'9ffcbc1c-7df4-438c-bbfe-92e519d05466');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleListEntity($value);
        $this->assertEquals($value,$assets->getRoleListEntity());
    }
}
