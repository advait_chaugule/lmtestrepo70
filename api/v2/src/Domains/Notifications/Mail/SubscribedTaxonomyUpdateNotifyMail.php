<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 20-02-2019
 * Time: 11:57
 */

namespace App\Domains\Notifications\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class SubscribedTaxonomyUpdateNotifyMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $email;
    public $body;
    public $userName;
    public $organizationName;
    public $orgCode;
    public $domainName;
    public $linkDomainName;

    public function __construct($data = []){
        $this->subject  = '[ACMT Notification] - '.$data['subject'];
        $this->header   = $data['subject'];
        $this->userName = $data['body']['user_name'];
        $this->organizationName = $data['body']['organization_name'];
        $this->orgCode  = $data['body']['organization_code'];
        $this->email    = $data['email'];
        $this->domainName = str_replace('server','',$data['domain_name']);
        $this->linkDomainName = str_replace('api.','',$data['domain_name']);
        if(strpos($this->linkDomainName,'localhost')!==false)
        {
            $this->taxonomyDetailsUrl = env('BASE_URL') . '#/org/'.$data['body']['organization_code'].'/app/taxonomy/detail/' . $data['body']['document_id'];
        }else{
            $this->taxonomyDetailsUrl = $this->linkDomainName . '#/org/'.$data['body']['organization_code'].'/app/taxonomy/detail/' . $data['body']['document_id'];
        }
        $this->body = str_replace(array("{{taxonomy_details_url}}"), array($this->taxonomyDetailsUrl), $data['body']['email_body']);
        
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('email.SubscribedTaxonomyUpdateNotify')
            ->with([
                "email"     => $this->email,
                "body"      => $this->body,
                "header"      => $this->header,
                "subject"      => $this->subject,
                'userName'  =>$this->userName,
                'organizationName'=>$this->organizationName,
                'orgCode' =>$this->orgCode,
                "domainName"=>$this->domainName,
                "linkDomainName"=>$this->linkDomainName,
                "taxonomyDetailsUrl" => $this->taxonomyDetailsUrl 
            ])
            ->subject($this->subject);
    }
}
