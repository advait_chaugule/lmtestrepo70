<?php

namespace App\Domains\Notifications\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CopyTaxonomyEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $email;
    public $body;
    public $userName;
    public $organizationName;
    public $orgCode;
    public $documentId;
    public $docLink;
    public $domainName;
    public $linkDomainName;

    public function __construct($data = [])
    {
        $this->subject  = $data['subject'];
        $this->body     = $data['body']['email_body'];
        $this->userName = $data['username'];
        $this->domainName = $data['domain_name'];
        $this->organizationName = $data['body']['organization_name'];
        $this->orgCode  = $data['body']['organization_code'];
        $this->linkDomainName = str_replace('api.','',$data['domain_name']);
        if($data['body']['document_id'] != '' )
        {
            if(strpos($this->linkDomainName,'localhost')!==false)
            {
                $this->docLink = env('BASE_URL') . '#/org/'.$data['body']['organization_code'].'/app/taxonomy/detail/' . $data['body']['document_id'];
            }else{
                $this->docLink = $this->linkDomainName . '#/org/'.$data['body']['organization_code'].'/app/taxonomy/detail/' . $data['body']['document_id'];
            }

        }
        else
        {
            $this->docLink = '';
        }
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('email.copytaxonomy')
                    ->with([
                        "body"      => $this->body,
                        "userName"  => $this->userName,
                        "domainName" => $this->domainName,
                        "linkDomainName" => $this->linkDomainName,
                        "organizationName" => $this->organizationName,
                        "orgCode" => $this->orgCode,
                        "docLink" => $this->docLink
                    ])
                    ->subject($this->subject);
    }
}
