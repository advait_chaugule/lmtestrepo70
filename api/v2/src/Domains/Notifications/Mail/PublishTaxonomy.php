<?php
namespace App\Domains\Notifications\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PublishTaxonomy extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $email;
    public $body;

    public function __construct($data = []){
        $this->subject = $data['subject'];
        $this->body    = $data['body']['notification'];
        $this->email   = $data['email'];
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('email.publishTaxonomy')
            ->with([
                "email" => $this->email,
                "body"  => $this->body,
            ])
            ->subject($this->subject);
    }
}
