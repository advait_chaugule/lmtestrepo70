<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 20-02-2019
 * Time: 11:57
 */

namespace App\Domains\Notifications\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class ImportMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $email;
    public $body;
    public $userName;
    public $organizationName;
    public $orgCode;
    public $documentId;
    public $docLink;
    public $domainName;
    public $linkDomainName;

    public function __construct($data = []){
        $this->subject  = $data['subject'];
        $this->body     = $data['body']['email_body'];
        $this->userName = $data['body']['user_name'];
        $this->organizationName = $data['body']['organization_name'];
        $this->orgCode  = $data['body']['organization_code'];
        $this->email    = $data['email'];
        $this->documentId    = $data['body']['document_id'];
        $this->domainName    = $data['domain_name'];
        $this->linkDomainName = str_replace('api.','',$data['domain_name']);
        if($data['body']['document_id'] != '' )
        {
            if(strpos($this->linkDomainName,'localhost')!==false)
            {
                $this->docLink = env('BASE_URL') . '#/org/'.$data['body']['organization_code'].'/app/taxonomy/detail/' . $data['body']['document_id'];
            }else{
                $this->docLink = $this->linkDomainName . '#/org/'.$data['body']['organization_code'].'/app/taxonomy/detail/' . $data['body']['document_id'];
            }

        }
        else
        {
            $this->docLink = '';
        }
        
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('email.Import')
            ->with([
                "email"     => $this->email,
                "body"      => $this->body,
                'userName'  =>$this->userName,
                'organizationName'=>$this->organizationName,
                'orgCode' =>$this->orgCode,
                'documentId' =>$this->documentId,
                'docLink' =>$this->docLink,
                'domainName'=>$this->domainName,
                'linkDomainName'=>$this->linkDomainName
            ])
            ->subject($this->subject);
    }
}
