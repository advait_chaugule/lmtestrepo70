<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 20-02-2019
 * Time: 11:57
 */

namespace App\Domains\Notifications\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class PublishedTaxonomyMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $email;
    public $body;
    public $userName;
    public $organizationName;
    public $orgCode;
    public $domainName;
    public $linkDomainName;
    public $document_id;
    public $header;

    public function __construct($data = []){
        $this->subject  = $data['subject'];
        $this->body     = $data['body']['email_body'];
        $this->userName = $data['body']['user_name'];
        $this->organizationName = $data['body']['organization_name'];
        $this->orgCode  = $data['body']['organization_code'];
        $this->document_id= $data['body']['document_id'];
        $this->header   = $data['body']['header'];
        $this->email    = $data['email'];
        $this->domainName = str_replace('server','',$data['domain_name']);
        $this->linkDomainName = str_replace('api.','',$data['domain_name']);

        if(strpos($this->linkDomainName,'localhost')!==false)
        {
            $this->publishLink = env('BASE_URL') . '#/org/'.$data['body']['organization_code'].'/taxonomies/detail/' . $data['body']['document_id'];
        }else{
            $this->publishLink = $this->linkDomainName . '#/org/'.$data['body']['organization_code'].'/taxonomies/detail/' . $data['body']['document_id'];
        }
        $this->body = str_replace(array("{{publish_link}}"), array($this->publishLink), $data['body']['email_body']);
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('email.PublishedTaxonomyEmail')
            ->with([
                "email"     => $this->email,
                "body"      => $this->body,
                'userName'  =>$this->userName,
                'organizationName'=>$this->organizationName,
                'orgCode' =>$this->orgCode,
                "domainName"=>$this->domainName,
                "linkDomainName"=>$this->linkDomainName,
                "publishLink"=>$this->publishLink,
                "header"    =>$this->header,
                "subject"   =>$this->subject
            ])
            ->subject($this->subject);
    }
}
