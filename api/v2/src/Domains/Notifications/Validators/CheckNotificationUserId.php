<?php
namespace App\Domains\Notifications\Validators;
use App\Foundation\BaseValidator;

class CheckNotificationUserId extends BaseValidator
{
    protected $rules = [
        'user_id' => "required|exists:users,user_id"
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
    ];
}