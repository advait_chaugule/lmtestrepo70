<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 12-02-2019
 * Time: 11:42
 */

namespace App\Domains\Notifications\Validators;
use App\Foundation\BaseValidator;

class CheckValidationNotificationId extends BaseValidator
{
    protected $rules = [
        'user_id' => "required",
    ];

    protected $messages = [
        "required" => ":attribute is required.",
    ];
}