<?php
namespace App\Domains\Notifications\Jobs;


use App\Domains\Notifications\Validators\CheckNotificationUserId;
use Lucid\Foundation\Job;

class ValidateNotificationId extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CheckNotificationUserId $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}