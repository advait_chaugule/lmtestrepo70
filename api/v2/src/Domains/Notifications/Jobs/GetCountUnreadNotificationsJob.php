<?php

namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use App\Data\Models\AppNotification;
class GetCountUnreadNotificationsJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['user_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( ){
        $count= $this->getCountUnreadNotification($this->identifier);
        return ['count'=>$count];
    }

    public function getCountUnreadNotification($userId)
    {
        $query = AppNotification::where('user_id',$userId)->where('read_status',0)->get();
        if($query) {
            $data  = $query->toArray();
            $count = count($data);
            return $count;
        }
    }
}