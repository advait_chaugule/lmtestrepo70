<?php
namespace App\Domains\Notifications\Jobs;

use Lucid\Foundation\Job;
use DB;
class GetProjectUserDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $projectId;
    public function __construct($data)
    {
        $this->projectId = $data['project_id'];
        $this->workflowStageRoleId = $data['workflow_stage_role_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $projectId = $this->projectId;
        $workflowStageRoleId = $this->workflowStageRoleId;
        $getProjectUserDetails = DB::table('project_users')
            ->select('user_id')
            ->where('project_id',$projectId)
            ->where('workflow_stage_role_id',$workflowStageRoleId)
            ->where('is_deleted',0)
            ->get();
        $projectItemDetails = json_decode($getProjectUserDetails, true);
        return $projectItemDetails;
    }
}