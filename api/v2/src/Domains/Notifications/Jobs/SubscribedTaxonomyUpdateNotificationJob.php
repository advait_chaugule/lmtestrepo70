<?php
/**
 * User: Gincy Mathai
 * Date: 01-07-2020
 * Time: 18:53
 */

namespace App\Domains\Notifications\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Mail;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Data\Repositories\Contracts\NotificationInterface;
use App\Domains\Notifications\Mail\SubscribedTaxonomyUpdateNotifyMail;
use App\Data\Models\Organization;
use DB;

class SubscribedTaxonomyUpdateNotificationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    use UuidHelperTrait;
    private $documentIdentifier;
    private $orgIdentifier;
    private $notificationRepository;
    private $orgName;
    public function __construct($documentIdentifier, $orgIdentifier,$domainName,$loginUserId)
    {        
        $this->documentIdentifier = $documentIdentifier;
        $this->orgIdentifier = $orgIdentifier;
        $this->domainName = $domainName;
        $this->loginUserId = $loginUserId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NotificationInterface $notificationActivity)
    {
        $this->notificationRepository = $notificationActivity;
        $documentIdentifier    = $this->documentIdentifier;
        $orgIdentifier         = $this->orgIdentifier;
        $domainName            = $this->domainName;
        $loginUserId           = $this->loginUserId;
        //get taxonomy name from document id
        $notificationCategory  = config('event_activity')["Notification_category"]["SUBSCRIBED_TAXONOMY_UPDATED"];
        $messageNotify         = config('event_activity')['notification_message']["17"];
        $emailMessage          = config('event_activity')['email_message']["16"];
        $emailSubjectData      = config('event_activity')['email_notification']["16"];
        
        $targetType            = 20;
        $readStatus            = 0;
        // Sent mail to published Taxonomy
        $query = DB::table('version_subscriber_detail as vsd')
                ->join('version_subscription as vs', 'vs.subscriber_detail_id', '=', 'vsd.subscriber_detail_id')
                ->select('vs.subscription_id','vsd.subscriber_id','vsd.subscriber_org_id','vs.pub_source_document_id','vsd.subscriber_end_point')
                ->where(['pub_document_id'=>$documentIdentifier,'vsd.publisher_org_id'=>$orgIdentifier,'vs.is_active'=>1])
                ->whereNull('deleted_at');
        $versionSubscription = $query->get()->toArray();
        $latestVersion = DB::table('taxonomy_pubstate_history')->select('publish_number')->where('document_id',$documentIdentifier)->where('organization_id',$orgIdentifier)->orderby('publish_number','desc')->first();
        $latestVersion = $latestVersion->publish_number;
        if(!empty($versionSubscription))
        {
            foreach($versionSubscription as $subscriber)
            {
                $taxonomyExists = false;
                if(!empty($subscriber->subscriber_org_id)){
                    $userExists = false;
                    if(!empty($subscriber->subscriber_id)){
                        $user = $this->fetchUserDetails($subscriber->subscriber_id,$subscriber->subscriber_org_id);
                        if(isset($user->user_id)){
                            $getTaxonomyDetails = $this->getDocumentTitle($subscriber->pub_source_document_id,$user->organization_id);
                            if(isset($getTaxonomyDetails->title)){
                                $taxonomyName = $getTaxonomyDetails->title;
                                $notification = str_replace(array("{{taxonomy_name}}"),array($getTaxonomyDetails->title),$messageNotify);
                                $email_body = str_replace(array("{{taxonomy_name}}"), array($taxonomyName), $emailMessage);
                                $user_subject = str_replace(array("{{taxonomy_name}}"), array($taxonomyName), $emailSubjectData);   
                                $taxonomyExists = true;
                            }
                            $userExists = true;
                        }
                    }
                    if(!empty($subscriber->subscriber_end_point) && $userExists == false){
                        $endPoint = $subscriber->subscriber_end_point; 
                        $postData = json_encode(['UUID'=>$subscriber->pub_source_document_id,
                        'source' => 'ACMT',
                        'type' => 'taxonomy',
                        'tenant_id' => $subscriber->subscriber_org_id,
                        'version' => $latestVersion
                        ]);
                        $response = $this->callEndPointOnPublish($endPoint,$postData);
                    }
                }
                if($taxonomyExists==false){
                    continue;
                }

                $user_id = $user->user_id;
                $targetId              = $getTaxonomyDetails->document_id;        
                $targetContext         = ['document_id'=>$getTaxonomyDetails->document_id,'user_id'=>$user_id];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                $user_name = $user->first_name." ".$user->last_name;
                $user_email = $user->email;
                $currentOrgId = $user->current_organization_id;
                $organizationId = $user->organization_id;                                  
                
                $organizationData = $this->getOrgNameByOrgId($organizationId);
                $organizationName = $organizationData['organization_name'];
                $organizationCode = $organizationData['organization_code'];
                $emailData1 = [
                    'subject'=>$user_subject,
                    'email'    => $user_email,
                    'body'     => ['email_body'=>$email_body,'user_name'=>$user_name,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'document_id'=>$targetId],
                    'domain_name'=> $domainName
                    ];             
                    // Email notifications enable/disable ACMT-1869->1962
                $emailSettingStatus = $user->email_setting;
                if ($emailSettingStatus==1) {
                    Mail::to($user_email)->send(new SubscribedTaxonomyUpdateNotifyMail($emailData1));
                    unset($emailData1);
                }
                //Prepare data
                $notificationId = $this->createUniversalUniqueIdentifier();            
                $activityLog = [
                "notification_id"       => $notificationId,
                "description"           => $notification,
                "target_type"           => $targetType,
                "target_id"             => $targetId,
                "user_id"               => $user_id,
                "organization_id"       => $organizationId,
                "is_deleted"            => 0,
                "created_by"            => $loginUserId, // logged in user Id
                "updated_by"            => $loginUserId,
                "target_context"        => $targetContextJson,
                "read_status"           => $readStatus, //0=unread 1= read
                "notification_category" => $notificationCategory
                ];

                $this->notificationRepository->createNotification($activityLog);
                unset($activityLog);
                unset($targetContext);
                unset($targetContextJson);
            }  
        }
        return true;
    }    

    public function callEndPointOnPublish($endPoint,$postData){
            
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        return $response;
    }

    public function fetchUserDetails($userId,$orgId){
        $user = DB::table('users as u')
                ->select('first_name','last_name','email','current_organization_id','uo.organization_id','u.user_id','enable_email_notification as email_setting')
                ->join('users_organization as uo','uo.user_id','=','u.user_id')
                ->where(['u.user_id'=>$userId,
                    'u.organization_id'=>$orgId,
                    'u.is_active'=>"1",
                    'u.is_deleted'=>"0",
                    'uo.is_active'=>"1",
                    'uo.is_deleted'=>"0"])
                ->first();
        return $user;
    }

    public function getDocumentTitle($sourceDocumentId,$orgId){
        $query = DB::table('documents')->select('title','document_id')->where(['source_document_id'=>$sourceDocumentId,'organization_id'=>$orgId,'is_deleted'=>0])->first();
        return $query;
    }

    public function getOrgNameByOrgId($organizationId){
        if(!isset($this->orgName[$organizationId])){
            $orgName = Organization::select('name as organization_name','org_code as organization_code')
                    ->where('organization_id','=',$organizationId)
                    ->first()
                    ->toArray();
            $this->orgName[$organizationId] = $orgName;
        }
        
        return $this->orgName[$organizationId];
    }
}
