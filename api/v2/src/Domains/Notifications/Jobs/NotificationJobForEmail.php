<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 20-02-2019
 * Time: 11:53
 */

namespace App\Domains\Notifications\Jobs;


use App\Domains\Notifications\Mail\NotificationMail;
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Mail;

class NotificationJobForEmail extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->input['email'])->send(new NotificationMail($this->input));
    }
}
