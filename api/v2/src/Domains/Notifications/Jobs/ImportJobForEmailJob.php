<?php
namespace App\Domains\Notifications\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Notifications\Mail\ImportMail;
use Illuminate\Support\Facades\Mail;

class ImportJobForEmailJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->input['email'])->send(new ImportMail($this->input));
    }
}
