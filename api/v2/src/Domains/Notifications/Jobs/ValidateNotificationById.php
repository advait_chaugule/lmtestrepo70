<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 12-02-2019
 * Time: 11:40
 */

namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use App\Domains\Notifications\Validators\CheckValidationNotificationId;
class ValidateNotificationById extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CheckValidationNotificationId $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}