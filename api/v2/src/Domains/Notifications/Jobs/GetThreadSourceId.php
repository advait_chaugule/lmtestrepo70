<?php
namespace App\Domains\Notifications\Jobs;

use Lucid\Foundation\Job;
use DB;
class GetThreadSourceId extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['thread_comment_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( ){
        $data= $this->getNotification($this->identifier);
        return $data;
    }

    public function getNotification($threadCommentId)
    {
        $query = DB::table('thread_comments')->where('thread_comment_id',$threadCommentId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}