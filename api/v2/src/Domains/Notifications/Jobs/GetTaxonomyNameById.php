<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 11-02-2019
 * Time: 15:04
 */

namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use DB;
class GetTaxonomyNameById extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['document_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $data= $this->getDocumentDetails($this->identifier);
        return $data;
    }

    public function getDocumentDetails($documentId)
    {
        $query = $user = DB::table('documents')->where('document_id',$documentId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}