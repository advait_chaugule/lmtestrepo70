<?php
namespace App\Domains\Notifications\Jobs;
use Lucid\Foundation\Job;

class GetPacingGuideDetails extends Job
{
    private $identifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['project_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->getPacingGuideDetails($this->identifier);
        return $data;
    }
    // Get Pacing guide details
    public function getPacingGuideDetails($projectId)
    {
        $query = $user = DB::table('projects')->where('project_id', $projectId)->get();
        if ($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}