<?php

namespace App\Domains\Notifications\Jobs;
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Mail;
use Lucid\Foundation\QueueableJob;
use App\Domains\Notifications\Mail\PublishTaxonomy;
class TaxonomyPublishJob extends Job
{
    /**
 * Create a new job instance.
 *
 * @return void
 */
    private $input;
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->input['subject'] = config('event_activity')["email_notification"]["13"];
        Mail::to($this->input['email'])->send(new PublishTaxonomy ($this->input));
    }

}




