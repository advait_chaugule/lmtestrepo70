<?php
namespace App\Domains\Notifications\Jobs;
use Lucid\Foundation\Job;
use DB;
class NotificationsJob extends Job
{
    private $identifiers;

    public function __construct(array $input){
        $this->identifiers  = $input;
    }

    public function handle() {
        $status = $this->updateNotificationStatus($this->identifiers);
        return $status;
    }

    public function updateNotificationStatus($userId)
    {
        $query = DB::table('notifications')
            ->where('user_id',$userId)
            ->update(['read_status'=>1]);
        return $query;
    }
}