<?php

namespace App\Domains\Notifications\Jobs;

use App\Domains\Notifications\Mail\PublishedTaxonomyMail;
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Mail;

class PublishNotificationJobForEmail extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->input['email'])->send(new PublishedTaxonomyMail($this->input));
    }
}
