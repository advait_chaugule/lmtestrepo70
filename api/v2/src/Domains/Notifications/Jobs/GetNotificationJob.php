<?php

namespace App\Domains\Notifications\Jobs;


use App\Data\Models\AppNotification;
use Lucid\Foundation\Job;
use DB;
class GetNotificationJob extends Job
{
    private $identifier;
    private $organizationId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier     = $input['user_id'];
        $this->organizationId = $input['organization_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( ){
       $data= $this->getNotification($this->identifier,$this->organizationId);
       return $data;
    }

    public function getNotification($userId,$organizationId)
    {
        $query = AppNotification::where('user_id',$userId)->where('organization_id',$organizationId)->where('read_status', '=', 0)->get();
        if($query) {
            $data = $query->toArray();
            foreach ($data as $arrKey=>$arrData) {
               $createdBy[]   = $arrData['created_by'];
                $targetContext = $arrData['target_context'];
                $arrTargetContext = json_decode($targetContext,true);
                $data[$arrKey]['target_context'] = $arrTargetContext;
                $name   = $this->getUserName($createdBy);
                $data[$arrKey]['user_name']=$name;
                $data[$arrKey]['new_version_notification'] = ($arrData['target_type']=="20") ? true : false; 
            }
            return $data;

        }
    }

    public function getUserName($createdBy){
        $query = $user = DB::table('users')->select('first_name','last_name')->whereIn('user_id',$createdBy)->get();
        if($query) {
            $result =[];
            foreach ($query as $data) {
              $result = $data->first_name.' '.$data->last_name;
            }
            return $result;
        }
    }

}