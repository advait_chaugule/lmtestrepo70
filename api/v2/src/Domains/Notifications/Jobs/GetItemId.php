<?php

namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use DB;

// Need to verify
class GetItemId extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['thread_source_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( ){
        $data= $this->getMessageData($this->identifier);
        return $data;
    }

    public function getMessageData($threadSourceId){
        $query = DB::table('project_items')->where('item_id',$threadSourceId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}