<?php
namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use DB;
class GetProjectIdFromItemId extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct($threadId)
    {
        $this->input = $threadId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $threadId = $this->input;
        $getProjectItemDetails = DB::table('thread_comments')
            ->select('thread_comment_id')
            ->where('thread_id',$threadId)
            ->get();
        $projectItemDetails = json_decode($getProjectItemDetails, true);
        return $projectItemDetails;
    }
}

