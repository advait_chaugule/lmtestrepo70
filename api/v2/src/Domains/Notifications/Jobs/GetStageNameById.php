<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 11-02-2019
 * Time: 15:16
 */

namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use DB;
class GetStageNameById extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['workflow_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $data= $this->getWorkflowDetails($this->identifier);
        return $data;
    }

    public function getWorkflowDetails($workFlowId)
    {
        $query = $user = DB::table('workflow_stage')->where('workflow_stage_id',$workFlowId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}