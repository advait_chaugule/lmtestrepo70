<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 11-02-2019
 * Time: 14:40
 */

namespace App\Domains\Notifications\Jobs;


use Lucid\Foundation\Job;
use DB;
class GetProjectDetailByProjectId extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input['project_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( ){
        $data= $this->getProjectDetails($this->identifier);
        return $data;
    }

    public function getProjectDetails($projectId)
    {
       $query = $user = DB::table('projects')->where('project_id', $projectId)->get();
       if($query) {
           $data = $query->toArray();
           return $data;
       }
    }
}