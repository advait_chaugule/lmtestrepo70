<?php
namespace App\Domains\Notifications\Listeners;
use App\Data\Repositories\Contracts\NotificationInterface;
use App\Domains\Notifications\Jobs\GetProjectUserDetailsJob;
use App\Foundation\ReportListenerHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\Notifications\Events\NotificationsEvent as Event;
use Illuminate\Support\Facades\Mail;
use Lucid\Foundation\QueueableJob;
use App\Domains\Notifications\Jobs\GetPacingGuideDetails;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Notifications\Jobs\NotificationJobForEmail;
use App\Domains\Notifications\Jobs\GetTaxonomyNameById;
use App\Domains\Notifications\Jobs\GetStageNameById;
use App\Domains\Notifications\Jobs\GetProjectDetailByProjectId;
use App\Domains\User\Jobs\GetUserByPermissionJob;
use App\Domains\Project\Jobs\ProjectAccessRequestViewersListJob;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use App\Domains\Notifications\Jobs\PublishNotificationJobForEmail;
use DB;
use Log;
class NotificationsEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue, UuidHelperTrait,DispatchesJobs;
    private $targetType;
    private $requestUserDetails;
    private $beforeEventRawData;
    private $afterEventRawData;
    private $beforeEventParsedData;
    private $afterEventParsedData;
    private $notificationId;
    private $notification;
    private $data;
    private $notificationInterface;
    private $activityLogData;
    private $notificationRepository;
    private $requestProjectDetails;
    private $setEventType;
    public function __construct(NotificationInterface $notificationActivity)
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
        $this->notificationRepository = $notificationActivity;
        // set the database repositories
        $this->setNotificationId($this->createUniversalUniqueIdentifier());
    }
    public function setNotificationId(string $data) {
        $this->notificationId = $data;
    }
    public function getNotificationId(): string {
        return $this->notificationId;
    }
    public function setRequestUserDetails(array $data) {
        $user = [
            "user_id"         => $data["user_id"],
            "first_name"      => $data["first_name"],
            "last_name"       => $data["last_name"],
            "organization_id" => $data["organization_id"]
        ];
        $this->requestUserDetails = $user;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setBeforeEventRawData($data) {
        $this->beforeEventRawData = $data;
    }

    public function getBeforeEventRawData() {
        return $this->beforeEventRawData;
    }

    public function setEventType(array $data){
        $this->setEventType = $data;
    }

    public function getEventType(){
        return $this->setEventType;
    }

    public function setAfterEventRawData(array $data) {
        $project=[
            'notification'         =>  isset($data['notification'])?$data['notification']:'',
            'is_deleted'           =>  isset($data['is_deleted'])?$data['is_deleted']:'',
            'updated_by'           =>  isset($data['updated_by'])?$data['updated_by']:'',
            'created_by'           =>  isset($data['created_by'])?$data['created_by']:'',
            'target_id'            =>  isset($data['project_id'])?$data['project_id']:'',
            'event_type'           =>  isset($data['event_type'])?$data['event_type']:'',
            'target_context'       =>  isset($data['target_context'])?$data['target_context']:'',
            'notification_category'=>  isset($data['notification_category'])?$data['notification_category']:'',
          ];
        $this->requestProjectDetails = $project;
    }

    public function getAfterEventRawData(): array {
        return $this->requestProjectDetails;
    }

    public function setBeforeEventParsedData(array $data) {
        $this->beforeEventParsedData = $data;
    }

    public function getBeforeEventParsedData(): array {
        return $this->beforeEventParsedData;
    }

    public function setAfterEventParsedData(array $data) {
        $this->afterEventParsedData = $data;
    }

    public function getAfterEventParsedData(): array {
        return $this->afterEventParsedData;
    }

    private function parseBeforeEventRawData() {
        $beforeEventRawData = $this->getBeforeEventRawData();
        $this->setBeforeEventParsedData($beforeEventRawData);
    }

    private function parseAfterEventRawData() {
        $afterEventRawData[] = $this->getAfterEventRawData();
        $this->setAfterEventParsedData($afterEventRawData);
    }

    public function setActivityLogData($data) {
        $this->activityLogData = $data;
    }

    public function getActivityLogData() {
        return $this->activityLogData;
    }

    public function handle(Event $event)
    {
        try {
            // set before and after event raw data
            $eventData = $event->eventData;
            $this->setBeforeEventRawData((array) $eventData['beforeEventRawData']);
            $this->setAfterEventRawData ((array) $eventData['afterEventRawData']);
            $this->checkEventType($eventData);

            // set request user details
            $this->setRequestUserDetails($eventData['requestUserDetails']);
            // parse before and after event raw data and set them
            $this->parseBeforeEventRawData();
            $this->parseAfterEventRawData();
            $this->prepareNotificationData();
            $notificationInterface =  $this->notificationRepository;
            $data = $this->getActivityLogData();
            $notificationInterface->createNotification($data);
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
            // cleanup folder and archive from local storage
            $this->cleanLocalStorage();
        }
    }
// Preparing request data in an array and send that array data to handle
    public function prepareNotificationData() {
        // $afterEventRawData  = $this->getAfterEventRawData();
        $requestUserDetails    = $this->getRequestUserDetails();
        $requestProjectDetails = $this->getAfterEventRawData();
        $notificationId        = $this->getNotificationId();
        $loginUserId           = $requestUserDetails["user_id"];
        $organizationId        = $requestUserDetails["organization_id"];
        $notificationMsg       = $requestProjectDetails['notification'];
        $targetId              = $requestProjectDetails['target_id']; // project Id
        $updatedBy             = $requestProjectDetails['updated_by'];
        $createdBy             = $requestProjectDetails['created_by'];
        $targetTypeData        = $requestProjectDetails['event_type'];
        $targetContext         = $requestProjectDetails['target_context'];
        $notificationCategory  = $requestProjectDetails['notification_category'];
        $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
        $targetType            = $targetTypeData;
        $readStatus            = 0;
        $isDeleted             = isset($requestProjectDetails['is_deleted'])?$requestProjectDetails['is_deleted']:0;

        $activityLog = [
            "notification_id"       => $notificationId,
            "description"           => $notificationMsg,
            "target_type"           => $targetType,
            "target_id"             => $targetId,
            "user_id"               => $createdBy,
            "organization_id"       => $organizationId,
            "is_deleted"            => $isDeleted,
            "created_by"            => $loginUserId, // logged in user Id
            "updated_by"            => $loginUserId,
            "target_context"        => $targetContextJson,
            "read_status"           => $readStatus, //0=unread 1= read
            "notification_category" => $notificationCategory
        ];
        $this->setActivityLogData($activityLog);
    }

    public function failed(Event $event, $exception ){
        $this->logErrors($exception);
        // cleanup folder and archive from local storage
        $this->cleanLocalStorage();
    }

    private function cleanLocalStorage() {
        $notificationId = $this->getNotificationId();
        $this->flushReportEventLocalStorage($notificationId);
    }

    public function checkEventType($data)
    {

        $eventType = isset($data['event_type'])?$data['event_type']:'';
        switch ($eventType) {
            case "1": // I should get notifications when the overall status of a stage is updated
                $loginUserId = $data['requestUserDetails']['user_id'];
                $loginOrgId  = $data['requestUserDetails']['organization_id'];

                $eventType  = $data['event_type'];
                $domainName = $data['domain_name'];
                // fetch document_id from project_id
                $projectId = $data['project_id'];
                $documentDetail = DB::table('project_items as pi')
                    ->join('items as i', 'pi.item_id', '=', 'i.item_id')
                    ->select('i.document_id')
                    ->where('pi.project_id', '=', $projectId)
                    ->limit(1)->get();
                $documentDetail = json_decode(json_encode($documentDetail[0]), true);
                $projectName = $data['project_name'];
                $organizationId = $data['organization_id'];
                $createdBy = $data['requestUserDetails']['user_id'];
                $isDeleted = $data['is_deleted'];
                $workFlowId = ['workflow_id' => $data['workflow_id']];
                //Get Stage Name
                $stageData = $this->dispatch(new GetStageNameById($workFlowId));
                $stageName = isset($stageData[0]->stage_name) ? $stageData[0]->stage_name : '';
                //get taxonomy name from document details
                $getDocumentDetails = $this->dispatch(new GetTaxonomyNameById($documentDetail));
                $taxonomyName = isset($getDocumentDetails[0]->title) ? $getDocumentDetails[0]->title : '';

                $notificationCategory = config('event_activity')['Notification_category']['PROJECT_STAGE_CHANGE_NOTIFICATION'];
                $messageNotify = config('event_activity')['notification_message']["1"];
                $notification = str_replace(array("{{project_name}}", "{{taxonomy_name}}", "{{stage_name}}"), array($projectName, $taxonomyName, $stageName), $messageNotify);
                $targetContext = ['project_id' => $projectId];

                $arrayData = ['project_id' => $projectId, // target id
                    'created_by' => $createdBy,
                    'organization_id' => $organizationId,
                    'notification' => $notification,
                    'event_type' => $eventType,
                    'target_context' => $targetContext,
                    'is_deleted' => $isDeleted,
                    'updated_by' => $loginUserId,
                    'notification_category' => $notificationCategory
                ];
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData" => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                ];
                // Email notifications starts
                $getUserDetails = $this->getUserDetails($createdBy);
                $email = $getUserDetails[0]->email;
                $firstName = $getUserDetails[0]->first_name;
                $lastName = $getUserDetails[0]->last_name;
                $currentOrgId= $getUserDetails[0]->current_organization_id;
                $userName = $firstName . ' ' . $lastName;
                $emailMessage = config('event_activity')['email_message']["1"];
                $emailBody = str_replace(array("{{project_name}}"), array($projectName), $emailMessage);
                $emailSubject = config('event_activity')['email_notification']["1"];
                $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($loginUserId,$loginOrgId));
                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                $organizationName = $organizationData['organization_name'];
                $organizationCode = $organizationData['organization_code'];
                $emailData = [
                    'email' => $email,
                    'body' => ['user_name' => $userName, 'email_body' => $emailBody,'organization_name'=> $organizationName,'organization_code'=>$organizationCode],
                    'subject' => $emailSubject,
                    'domain_name'=>$domainName
                ];
                //Check user email notification setting configuration ACMT-1869
                $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $createdBy, 'organization_id' => $currentOrgId]));
                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                if ($emailSettingStatus==1) {
                $this->dispatch(new NotificationJobForEmail($emailData)); // email notification ends here
               }
                return $eventData;
                break;

            case "10":  // When project request accepted
                $projectId            = $data['project_id'];
                $projectName          = $data['project_name'];
                $user_id              = $data['user_id'];
                $accessStatus         = $data['access_status'];
                $organizationId       = $data['organization_id'];
                $notificationCategory = $data['notification_category'];
                $documentDetail =DB::table('project_items as pi')
                    ->join('items as i', 'pi.item_id', '=', 'i.item_id')
                    ->select('i.document_id')
                    ->where('pi.project_id', '=',$projectId)
                    ->limit(1)->get();
                 
                $documentDetail = json_decode(json_encode($documentDetail[0]), true);
             
                // Get Project details based on project Id
                $getProjectDetails    = $this->getProjectDetails($projectId);
                //print_r($getProjectDetails);
                $isDeleted            = isset($getProjectDetails[0]->is_deleted)?$getProjectDetails[0]->is_deleted:'';
                $documentId           = isset($documentDetail['document_id'])?$documentDetail['document_id']:'';
                $updatedBy            = isset($getProjectDetails[0]->updated_by)?$getProjectDetails[0]->updated_by:'';
                $createdBy            = isset($getProjectDetails[0]->created_by)?$getProjectDetails[0]->created_by:'';
                // Get document details based on document Id
                $getTaxonomyDetails   = $this->getDocumentDetails($documentId);
                $taxonomyName         = $getTaxonomyDetails[0]->title;
                $messageNotify        = config('event_activity')['notification_message']["8"];
                $notification         = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);
                $targetContext        = ['project_id'=>$projectId,'document_id'=>$documentId,'access_status'=>$accessStatus];

                $arrayData = [
                    'project_id'            =>$projectId,
                    'created_by'            =>$user_id,
                    'organization_id'       =>$organizationId,
                    'notification'          =>$notification,
                    'event_type'            =>$eventType,
                    'target_context'        =>$targetContext,
                    'is_deleted'            =>$isDeleted,
                    'updated_by'            =>$updatedBy,
                    'notification_category' =>$notificationCategory
                ];
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData" => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                   ];
                return $eventData;
            break;
            case "6":  // when comment status change
                $status             = $data['status'];
                $loginUserId        = $data['requestUserDetails']['user_id'];
                $owner_id           = $data['owner_id'];
                $assignee_id        = $data['assignee_id'];
                $threadSourceId     = $data['thread_source_id'];
                $eventType          = $data['event_type'];
                $organizationId     = $data['organization_id'];
                $threadId           = $data['thread_id'];
                $getCommentData     = $this->getProjectData($threadSourceId);
                if(isset($getCommentData[0]))
                {
                    $projectId          = $getCommentData[0]->project_id;
                }
                else
                {
                    $getCommentData     = $this->getDocumentDetails($threadSourceId);
                    $projectId     = $getCommentData[0]->project_id;
                }
                $getProjectDetails  = $this->getProjectDetails($projectId);
                $projectName        = isset($getProjectDetails[0]->project_name)?$getProjectDetails[0]->project_name:'';
                $isDeleted          = isset($getProjectDetails[0]->is_deleted)?$getProjectDetails[0]->is_deleted:'';
                $documentId         = isset($getProjectDetails[0]->document_id)?$getProjectDetails[0]->document_id:'';
                $updatedBy          = isset($getProjectDetails[0]->updated_by)?$getProjectDetails[0]->updated_by:'';
                $createdBy          = isset($getProjectDetails[0]->created_by)?$getProjectDetails[0]->created_by:$getProjectDetails[0]->updated_by;
                // get taxonomy name from document id
                $getTaxonomyDetails    = $this->getDocumentDetails($documentId);
                $taxonomyName          = $getTaxonomyDetails[0]->title;
                $getCommentData        = $this->getProjectIdFromItemId($threadId);
                $thread_comment_id     =  $getCommentData[0]['thread_comment_id'];
                $notificationCategory  = config('event_activity')["Notification_category"]["COMMENT_STATUS_CHANGE_NOTIFICATION"];
                $messageNotify         = config('event_activity')['notification_message']["9"];
                $notification          = str_replace(array("{{status_node}}"),array($status),$messageNotify);
                $loginUserDetails = $this->getUserDetails($loginUserId);
                $loginUserName    = $loginUserDetails[0]->first_name." ".$loginUserDetails[0]->last_name;
                if($status=='Reopen') {  
                    $otherUserDetails = $this->getUserDetails($owner_id);
                    $userName         = $otherUserDetails[0]->first_name." ".$otherUserDetails[0]->last_name;
                    $messageNotify    = config('event_activity')['notification_message']["10"];
                    $notification     = "A comment has been reopened by ".$loginUserName." is assigned to you on ".$projectName." of ".$taxonomyName.".";
                }
                $targetContext      = ['project_id'=>$projectId,'user_id'=>$owner_id,'thread_comment_id'=>$thread_comment_id];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
               // in notification
               //Prepare data
               $notificationId = $this->createUniversalUniqueIdentifier();
               $notificationMsg = $notification;
             
               $activityLog = [
                  "notification_id"       => $notificationId,
                  "description"           => $notificationMsg,
                  "target_type"           => "6",
                  "target_id"             => $projectId,
                  "user_id"               => $owner_id,
                  "organization_id"       => $organizationId,
                  "is_deleted"            => $isDeleted,
                  "created_by"            => $loginUserId, // logged in user Id
                  "updated_by"            => $loginUserId,
                  "target_context"        => $targetContextJson,
                  "read_status"           => "0", //0=unread 1= read
                  "notification_category" => $notificationCategory
               ];
              
              $this->notificationRepository->createNotification($activityLog);
              unset($activityLog);
              unset($targetContext);
              unset($targetContextJson);
              unset($notificationMsg); 
               if($assignee_id != "")
               {

                if($status=='Reopen') {
                    $otherUserDetails = $this->getUserDetails($assignee_id);
                    $userName         = $otherUserDetails[0]->first_name." ".$otherUserDetails[0]->last_name;
                    $messageNotify    = config('event_activity')['notification_message']["10"];
                    $notification     = "A comment has been reopened by ".$loginUserName." is assigned to you on ".$projectName." of ".$taxonomyName.".";
                }
                $targetContext      = ['project_id'=>$projectId,'user_id'=>$assignee_id,'thread_comment_id'=>$thread_comment_id];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
           
                $notificationId = $this->createUniversalUniqueIdentifier();
                $notificationMsg = $notification;
              
                $activityLog1 = [
                   "notification_id"       => $notificationId,
                   "description"           => $notificationMsg,
                   "target_type"           => "6",
                   "target_id"             => $projectId,
                   "user_id"               => $assignee_id,
                   "organization_id"       => $organizationId,
                   "is_deleted"            => $isDeleted,
                   "created_by"            => $loginUserId, // logged in user Id
                   "updated_by"            => $loginUserId,
                   "target_context"        => $targetContextJson,
                   "read_status"           => "0", //0=unread 1= read
                   "notification_category" => $notificationCategory
                ];
               
               $this->notificationRepository->createNotification($activityLog1);
               unset($activityLog);
               unset($targetContext);
               unset($targetContextJson);
               }

                return "";
                break;
            case "4": //get a notification if a comment is assigned to user
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $organizationId  = $data['organization_id'];
                $threadCommentId = $data['thread_comment_id'];
                $getCommentData  = $this->getCommentSourceId($threadCommentId);
                $threadSourceId  = $getCommentData[0]['thread_source_id'];
                $threadId  = $getCommentData[0]['thread_id'];
                $threadData = $this->getAssigneeFromThread($threadId);
                $assign_to          = isset($threadData[0]->assign_to)?$threadData[0]->assign_to:'';

                $getCommentData  = $this->getProjectData($threadSourceId);
                if(count($getCommentData) == 0)
                {
                    $getCommentData  = $this->getProjectDataFromDoc($threadSourceId);
                    $nodeType = "Document";
                }
                else
                {
                    $nodeType = "Item";
                }
                $projectId          = isset($getCommentData[0]->project_id)?$getCommentData[0]->project_id:'';
                
                $getProjectDetails  = $this->getProjectDetails($projectId);
                
                $projectName        = isset($getProjectDetails[0]->project_name)?$getProjectDetails[0]->project_name:'';
                $isDeleted          = isset($getProjectDetails[0]->is_deleted)?$getProjectDetails[0]->is_deleted:'';
                $documentId         = isset($getProjectDetails[0]->document_id)?$getProjectDetails[0]->document_id:'';
                $updatedBy          = isset($getProjectDetails[0]->updated_by)?$getProjectDetails[0]->updated_by:'';
                $createdBy          = isset($getProjectDetails[0]->created_by)?$getProjectDetails[0]->created_by:'';
                // get taxonomy name from document id
                

                $getTaxonomyDetails    = $this->getDocumentDetails($documentId);
                $taxonomyName          = $getTaxonomyDetails[0]->title;
                $notificationCategory  = config('event_activity')["Notification_category"]["COMMENT_ASSIGN_TO_NOTIFICATION"];
                $messageNotify         = config('event_activity')['notification_message']["2"];
                $notification          = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);
                $targetContext         = ['project_id'=>$projectId,'user_id'=>$assign_to,'thread_comment_id'=>$threadCommentId,'item_id'=>$threadSourceId,'node_type'=>$nodeType];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                $readStatus            = 0;
                $arrayData = ['project_id'  =>$projectId,
                    'created_by'            =>$createdBy,
                    'organization_id'       =>$organizationId,
                    'notification'          =>$notification,
                    'event_type'            =>$eventType,
                    'target_context'        =>$targetContext,
                    'is_deleted'            =>$isDeleted,
                    'updated_by'            =>$updatedBy,
                    'notification_category' => $notificationCategory
                ];
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData" => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                ];
                $notificationId = $this->createUniversalUniqueIdentifier(); 
                $activityLog = [
                    "notification_id"       => $notificationId,
                    "description"           => $notification,
                    "target_type"           => $eventType,
                    "target_id"             => $projectId,
                    "user_id"               => $assign_to,
                    "organization_id"       => $organizationId,
                    "is_deleted"            => $isDeleted,
                    "created_by"            => $loginUserId, // logged in user Id
                    "updated_by"            => $loginUserId,
                    "target_context"        => $targetContextJson,
                    "read_status"           => $readStatus, //0=unread 1= read
                    "notification_category" => $notificationCategory
                 ];
                $this->notificationRepository->createNotification($activityLog);
                unset($activityLog);

                return "";
                break;
                case "3": //get a notification if a comment is assignee is change 
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $organizationId  = $data['organization_id'];
                $threadCommentId = $data['thread_comment_id'];
                $getCommentData  = $this->getCommentSourceId($threadCommentId);
                $threadSourceId  = $getCommentData[0]['thread_source_id'];
                $threadId  = $getCommentData[0]['thread_id'];
                $threadData = $this->getAssigneeFromThread($threadId);
                $assign_to          = isset($threadData[0]->assign_to)?$threadData[0]->assign_to:'';
                $old_assign_to      =  $data['old_assign_to'];
                $getCommentData  = $this->getProjectData($threadSourceId);
                if(count($getCommentData) == 0)
                {
                    $getCommentData  = $this->getProjectDataFromDoc($threadSourceId);
                    $nodeType = "Document";
                }
                else
                {
                    $nodeType = "Item";
                }
                $projectId          = isset($getCommentData[0]->project_id)?$getCommentData[0]->project_id:'';
                
                $getProjectDetails  = $this->getProjectDetails($projectId);
                
                $projectName        = isset($getProjectDetails[0]->project_name)?$getProjectDetails[0]->project_name:'';
                $isDeleted          = isset($getProjectDetails[0]->is_deleted)?$getProjectDetails[0]->is_deleted:'';
                $documentId         = isset($getProjectDetails[0]->document_id)?$getProjectDetails[0]->document_id:'';
                $updatedBy          = isset($getProjectDetails[0]->updated_by)?$getProjectDetails[0]->updated_by:'';
                $createdBy          = isset($getProjectDetails[0]->created_by)?$getProjectDetails[0]->created_by:'';
                // get taxonomy name from document id
                

                $getTaxonomyDetails    = $this->getDocumentDetails($documentId);
                $taxonomyName          = $getTaxonomyDetails[0]->title;
                $notificationCategory  = config('event_activity')["Notification_category"]["COMMENT_NOTIFICATION_ASSIGNEE_CHANGE"];
                $messageNotify         = config('event_activity')['notification_message']["2"];
                $notification          = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);
                $targetContext         = ['project_id'=>$projectId,'user_id'=>$assign_to,'thread_comment_id'=>$threadCommentId,'item_id'=>$threadSourceId,'node_type'=>$nodeType];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                $readStatus            = 0;
                $arrayData = ['project_id'  =>$projectId,
                    'created_by'            =>$createdBy,
                    'organization_id'       =>$organizationId,
                    'notification'          =>$notification,
                    'event_type'            =>$eventType,
                    'target_context'        =>$targetContext,
                    'is_deleted'            =>$isDeleted,
                    'updated_by'            =>$updatedBy,
                    'notification_category' =>$notificationCategory
                ];
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData" => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                ];
                $notificationId = $this->createUniversalUniqueIdentifier(); 
                $activityLog = [
                    "notification_id"       => $notificationId,
                    "description"           => $notification,
                    "target_type"           => $eventType,
                    "target_id"             => $projectId,
                    "user_id"               => $assign_to,
                    "organization_id"       => $organizationId,
                    "is_deleted"            => $isDeleted,
                    "created_by"            => $loginUserId, // logged in user Id
                    "updated_by"            => $loginUserId,
                    "target_context"        => $targetContextJson,
                    "read_status"           => $readStatus, //0=unread 1= read
                    "notification_category" => $notificationCategory
                 ];
                $this->notificationRepository->createNotification($activityLog);
                unset($activityLog);
                unset($targetContext);
                unset($targetContextJson);
                
                // old assignee    
                $notificationCategory  = config('event_activity')["Notification_category"]["COMMENT_NOTIFICATION_ASSIGNEE_CHANGE"];
                $otherUserDetails = $this->getUserDetails($assign_to);
                $userName         = $otherUserDetails[0]->first_name." ".$otherUserDetails[0]->last_name;
                $notification          = "The comment assigned to you previously has now been assigned to ".$userName." on ".$projectName." of ".$taxonomyName.".";
                $targetContext         = ['project_id'=>$projectId,'user_id'=>$old_assign_to,'thread_comment_id'=>$threadCommentId,'item_id'=>$threadSourceId,'node_type'=>$nodeType];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                    
                $notificationId = $this->createUniversalUniqueIdentifier(); 
                $activityLog = [
                    "notification_id"       => $notificationId,
                    "description"           => $notification,
                    "target_type"           => $eventType,
                    "target_id"             => $projectId,
                    "user_id"               => $old_assign_to,
                    "organization_id"       => $organizationId,
                    "is_deleted"            => $isDeleted,
                    "created_by"            => $loginUserId, // logged in user Id
                    "updated_by"            => $loginUserId,
                    "target_context"        => $targetContextJson,
                    "read_status"           => $readStatus, //0=unread 1= read
                    "notification_category" => $notificationCategory
                 ];
                $this->notificationRepository->createNotification($activityLog);
                unset($activityLog);
                unset($targetContext);
                unset($targetContextJson);
                
                return "";
                break;    
            case "5": // replies are added
          
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $owner_id        = $data['owner_id'];
                $assign_to       = $data['assign_to'];
                $eventType       = $data['event_type'];
                $organizationId  = $data['organization_id'];
                $threadCommentId = $data['thread_comment_id'];
                $getCommentData  = $this->getCommentSourceId($threadCommentId);
                $threadSourceId  = isset($getCommentData[0]['thread_source_id'])?$getCommentData[0]['thread_source_id']:'';
                $getCommentData  = $this->getProjectData($threadSourceId);
                if(count($getCommentData) == 0)
                {
                    $getCommentData  = $this->getProjectDataFromDoc($threadSourceId);
                    $nodeType = "Document";
                }
                else
                {
                    $nodeType = "Item";
                }
                $projectId       = $getCommentData[0]->project_id;
                $getProjectDetails  = $this->getProjectDetails($projectId);
                $projectName        = isset($getProjectDetails[0]->project_name)?$getProjectDetails[0]->project_name:'';
                $isDeleted          = isset($getProjectDetails[0]->is_deleted)?$getProjectDetails[0]->is_deleted:'';
                $documentId         = isset($getProjectDetails[0]->document_id)?$getProjectDetails[0]->document_id:'';
                $updatedBy          = isset($getProjectDetails[0]->updated_by)?$getProjectDetails[0]->updated_by:'';
                $createdBy          = isset($getProjectDetails[0]->created_by)?$getProjectDetails[0]->created_by:'';
                // get taxonomy name from document id
                $getTaxonomyDetails    = $this->getDocumentDetails($documentId);
                $taxonomyName          = $getTaxonomyDetails[0]->title;
                
                $notificationCategory  = config('event_activity')["Notification_category"]["COMMENT_REPLY_TO_NOTIFICATION"];
                $messageNotify         = config('event_activity')['notification_message']["4"];
                $notification          = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);
                $targetContext         = ['project_id'=>$projectId,'user_id'=>$owner_id,'thread_comment_id'=>$threadCommentId,'item_id'=>$threadSourceId];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                $notificationId = $this->createUniversalUniqueIdentifier();
                         $notificationMsg = $notification;
                       
                         $activityLog = [
                            "notification_id"       => $notificationId,
                            "description"           => $notificationMsg,
                            "target_type"           => "5",
                            "target_id"             => $projectId,
                            "user_id"               => $owner_id,
                            "organization_id"       => $organizationId,
                            "is_deleted"            => $isDeleted,
                            "created_by"            => $loginUserId, // logged in user Id
                            "updated_by"            => $loginUserId,
                            "target_context"        => $targetContextJson,
                            "read_status"           => "0", //0=unread 1= read
                            "notification_category" => $notificationCategory
                         ];
                    
                        $this->notificationRepository->createNotification($activityLog);
                        unset($activityLog);
                        unset($targetContext);
                        unset($targetContextJson);

                        if($assign_to != "")
                        {
                            $messageNotify         = config('event_activity')['notification_message']["3"];
                            $notification          = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);
                            $targetContext         = ['project_id'=>$projectId,'user_id'=>$assign_to,'thread_comment_id'=>$threadCommentId,'item_id'=>$threadSourceId];
                            $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                            $notificationId = $this->createUniversalUniqueIdentifier();
                                    $notificationMsg = $notification;
                                
                                    $activityLog = [
                                        "notification_id"       => $notificationId,
                                        "description"           => $notificationMsg,
                                        "target_type"           => "5",
                                        "target_id"             => $projectId,
                                        "user_id"               => $assign_to,
                                        "organization_id"       => $organizationId,
                                        "is_deleted"            => $isDeleted,
                                        "created_by"            => $loginUserId, // logged in user Id
                                        "updated_by"            => $loginUserId,
                                        "target_context"        => $targetContextJson,
                                        "read_status"           => "0", //0=unread 1= read
                                        "notification_category" => $notificationCategory
                                    ];
                                  
                        $this->notificationRepository->createNotification($activityLog);
                        unset($activityLog);
                        unset($targetContext);
                        unset($targetContextJson);
                        }
               
                return "";
                break;

            case "11": // Taxonomy published
                $eventType       = $data['event_type']; 
                $documentId      = $data['document_id'];
                $domainName      = $data['domain_name'];
                //get taxonomy name from document id
                $getTaxonomyDetails    = $this->getDocumentDetails($documentId);
                $taxonomyName          = isset($getTaxonomyDetails[0]->title)?$getTaxonomyDetails[0]->title:'';
                $organizationId        = isset($getTaxonomyDetails[0]->organization_id)?$getTaxonomyDetails[0]->organization_id:'';
                $isDeleted             = isset($getTaxonomyDetails[0]->is_deleted)?$getTaxonomyDetails[0]->is_deleted:'';
                $updatedBy             = isset($getTaxonomyDetails[0]->updated_by)?$getTaxonomyDetails[0]->updated_by:'';
                $createdBy             = isset($getTaxonomyDetails[0]->created_by)?$getTaxonomyDetails[0]->created_by:'';
                $notificationCategory  = config('event_activity')["Notification_category"]["TAXONOMY_PUBLISHED"];
                $messageNotify         = config('event_activity')['notification_message']["11"];
                $notification          = str_replace(array("{{taxonomy_name}}"),array($taxonomyName),$messageNotify);
                $targetType = 11;
                $targetId = $documentId;
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $readStatus            = 0;
                // Sent mail to published Taxonomy
                $listUser = $this->dispatch(new GetUserByPermissionJob(['organization_id' => $organizationId, 'permission_name' => "View Published Taxonomies"]));
                if($listUser != 0)
                {
                    foreach($listUser as $user)
                    {
                        $user_id = $user->user_id;
                        $targetContext         = ['document_id'=>$documentId,'user_id'=>$user_id];
                        $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                        $user_name = $user->first_name." ".$user->last_name;
                        $user_email = $user->email;
                        $currentOrgId = $user->current_organization_id;
                        $user_subject = "The {$taxonomyName} taxonomy has been published.";
                        $email_body = "The {$taxonomyName} taxonomy has been published. Please click on the following button to open the link and know more. You can also copy and paste the URL {{publish_link}} in your browser to launch.";
                        $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($loginUserId,$organizationId));

                        $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                        $organizationName = $organizationData['organization_name'];
                        $organizationCode = $organizationData['organization_code'];
                        $emailData1 = [
                            'subject'=>$user_subject,
                             'email'    => $user_email,
                            'body'     => ['email_body'=>$email_body,'user_name'=>$user_name,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'document_id'=>$documentId,'header' =>$user_subject,],
                            'domain_name'=> $domainName
                         ];             
                         // Email notifications enable/disable ACMT-1869->1962
                        $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $user_id, 'organization_id' => $organizationId]));
                        $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                        if ($emailSettingStatus==1) {
                            $this->dispatch(new PublishNotificationJobForEmail($emailData1));
                            unset($emailData1);
                        }

                         //send Notification

                         //Prepare data
                         $notificationId = $this->createUniversalUniqueIdentifier();
                         
                         $notificationMsg = $user_subject;        //to display correct text in notification. Bug UF-3351.
                         
                         $activityLog = [
                            "notification_id"       => $notificationId,
                            "description"           => $notificationMsg,
                            "target_type"           => $targetType,
                            "target_id"             => $targetId,
                            "user_id"               => $user_id,
                            "organization_id"       => $organizationId,
                            "is_deleted"            => $isDeleted,
                            "created_by"            => $loginUserId, // logged in user Id
                            "updated_by"            => $loginUserId,
                            "target_context"        => $targetContextJson,
                            "read_status"           => $readStatus, //0=unread 1= read
                            "notification_category" => $notificationCategory
                         ];
                        $this->notificationRepository->createNotification($activityLog);
                        unset($activityLog);
                        unset($targetContext);
                        unset($targetContextJson);
                    }
                } 

                return "";
                break;

            case "12": // Public review publish
                $eventType       = $data['event_type'];
                $documentId      = $data['document_id'];
                $projectId       = $data['project_id'];
                $domainName      = $data['domain_name'];
                //get taxonomy name from document details
                $getTaxonomyDetails = $this->getDocumentDetailsSourceId();
                $taxonomyName       = $getTaxonomyDetails->title;
                $organizationId     = $getTaxonomyDetails->organization_id;
                $isDeleted          = $getTaxonomyDetails->is_deleted;
                $updatedBy          = $getTaxonomyDetails->updated_by;
                $createdBy          = $getTaxonomyDetails->created_by;

                $notificationCategory  = config('event_activity')["Notification_category"]["PUBLIC_REVIEW_PUBLISH"];
                $messageNotify         = config('event_activity')['notification_message']["12"];
                $notification          = str_replace(array("{{taxonomy_name}}"),array($taxonomyName),$messageNotify);
                $targetContext         = ['project_id'=>$projectId,'document_id'=>$documentId,'user_id'=>$createdBy];
                $getReviewDetails      = $this->getUserPublicReview($projectId);

                $arrUserId = $eventData = [];
                foreach ($getReviewDetails as $review) {
                    $arrUserId = $review['user_id'];
                }
                $arrUserIds = array_unique($arrUserId);
                foreach ($arrUserIds as $user)
                {
                    $userId = $user;
                    $arrayData = ['project_id'  =>$documentId, // target id
                        'created_by'            =>$userId,
                        'organization_id'       =>$organizationId,
                        'notification'          =>$notification,
                        'event_type'            =>$eventType,
                        'target_context'        =>$targetContext,
                        'is_deleted'            =>$isDeleted,
                        'updated_by'            =>$updatedBy,
                        'notification_category' =>$notificationCategory
                    ];
                    $this->setAfterEventRawData($arrayData);
                    $eventData = [
                        "beforeEventRawData" => [],
                        "afterEventRawData" => ($this->getAfterEventRawData($arrayData)),
                        "requestUserDetails" => $data['requestUserDetails']
                    ];

                        $getUserDetails = $this->getUserDetails($userId);
                        $email = $getUserDetails[0]->email;
                        $firstName = $getUserDetails[0]->first_name;
                        $lastName = $getUserDetails[0]->last_name;
                        $currentOrgId=$getUserDetails[0]->current_organization_id;
                        $userName = $firstName . ' ' . $lastName;
                        $emailMessage = config('event_activity')['email_message']["12"];
                        $emailBody = str_replace(array("{{taxonomy}}"), array($taxonomyName), $emailMessage);
                        $emailSubjectData = config('event_activity')['email_notification']["12"];
                        $emailSubject = str_replace(array("{{taxonomy}}"), array($taxonomyName), $emailSubjectData);
//                        $emailData = [
//                            'email' => $email,
//                            'body' => ['user_name' => $userName, 'email_body' => $emailBody,'organization_name'=>$organizationName,'organization_code'=>$organizationCode],
//                            'subject' => $emailSubject
//                        ];
//                        // Email Notification
//                        $this->dispatch(new NotificationJobForEmail($emailData));


                    //user who has access for view public review 

                    $listUser = $this->dispatch(new GetUserByPermissionJob(['organization_id' => $organizationId, 'permission_name' => "View Active Public Review Taxonomies"]));
                    //print_r($listUser);die;
                    if($listUser != 0)
                    {
                        foreach($listUser as $user)
                        {
                            $user_id = $user->user_id;
                            $user_name = $user->first_name." ".$user->last_name;
                            $user_email = $user->email;
                            $currentOrgId= $user->current_organization_id;
                            $user_subject = "The {$taxonomyName} taxonomy published for public review.";
                            $email_body = "The {$taxonomyName} taxonomy has been published for public review.";
                            $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($currentOrgId));
                            $organizationName = $organizationData['organization_name'];
                            $organizationCode = $organizationData['organization_code'];
                            $emailData1 = [
                                'subject'=>$user_subject,
                                 'email'    => $user_email,
                                'body'     => ['email_body'=>$email_body,'user_name'=>$user_name,'organization_name'=>$organizationName,'organization_code'=>$organizationCode],
                                'domain_name'=>$domainName
                             ];             
                             // Email notifications
                            //Check user email notification setting configuration ACMT-1869 of Sub part ACMT-1954
                            $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $user_id, 'organization_id' => $organizationId]));
                            $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                            if ($emailSettingStatus == 1) {
                                $this->dispatch(new NotificationJobForEmail($emailData1));
                                unset($emailData1);
                            }
                            
                        }
                    } 

                }
                return $eventData;
                break;

            case "13": //  receive an in-app notification if the stage to which the role assigned to me is active in a project
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $loginOrgId      = $data['requestUserDetails']['organization_id'];
                $domainName      = $data['domain_name'];
                $eventType       = $data['event_type'];
                $projectId       = $data['project_id'];
                $workFlowId      = $data['workflow_id'];
                $documentDetail =DB::table('project_items as pi')
                    ->join('items as i', 'pi.item_id', '=', 'i.item_id')
                    ->select('i.document_id')
                    ->where('pi.project_id', '=',$projectId)
                    ->limit(1)->get();
                $documentDetail = json_decode(json_encode($documentDetail[0]), true);
                // get project details
                $getProjectDetails  = $this->getProjectDetails($projectId);
                $organizationId     = isset($getProjectDetails[0]->organization_id)?$getProjectDetails[0]->organization_id:'';
                $projectName        = isset($getProjectDetails[0]->project_name)?$getProjectDetails[0]->project_name:'';
                $isDeleted          = isset($getProjectDetails[0]->is_deleted)?$getProjectDetails[0]->is_deleted:'';
                $documentId         = $documentDetail;
                //$updatedBy          = isset($getProjectDetails[0]->updated_by)?$getProjectDetails[0]->updated_by:'';
                $createdBy          = isset($getProjectDetails[0]->created_by)?$getProjectDetails[0]->created_by:'';
                // get taxonomy name
                $getTaxonomyDetails = $this->getDocumentDetails($documentId);
                $taxonomyName       = $getTaxonomyDetails[0]->title;
                $getWorkflowStage   = $this->getWorkFlowDetails($workFlowId);
                $stageName          = $getWorkflowStage[0]['stage_name'];


                $workFlowStageId    = $getWorkflowStage[0]['workflow_stage_id'];

                $getRoleId          = $this->getWorkFlowRoleId($workFlowStageId);
                if(count($getRoleId) > 0)
                {
                    $arrayRoleIdData = [];
                    $arrayRoleNameData = [];
                    foreach($getRoleId as $getRoleIdV)
                    {

                        $role_id            = $getRoleIdV['role_id'];
                        $getRoleDetails     = $this->getUserRole($role_id);
                        $role_name          = $getRoleDetails[0]['name'];
                        $arrayRoleIdData[] = $role_id;
                        $arrayRoleNameData[$role_id] = $role_name;
                    }
                }

                $notificationCategory  = config('event_activity')["Notification_category"]["STAGE_ROLE_ASSIGN"];
                $messageNotify         = config('event_activity')['notification_message']["14"];

                $getProjectUserDetails = DB::table('project_users')
                ->select('user_id','workflow_stage_role_id')
                ->where('project_id',$projectId)
                ->where('is_deleted',0)
                ->get();
                 $projectItemDetails = json_decode($getProjectUserDetails, true);



                 foreach ($projectItemDetails as $projectItemDetailsK => $projectItemDetailsV) {

                    $project_role = substr($projectItemDetailsV['workflow_stage_role_id'],76);

                     if(in_array($project_role, $arrayRoleIdData))
                     {


                        $role_name = $arrayRoleNameData[$project_role];

                        $user_id = $projectItemDetailsV['user_id'];

                        $notification          = str_replace(array("{{project_name}}","{{taxonomy_name}}","{{review}}","{{role}}","{{role_activity}}"),array($projectName,$taxonomyName,$stageName,$role_name,$stageName),$messageNotify);
                        $targetContext         = ['project_id'=>$projectId,'document_id'=>$documentId,'user_id'=>$user_id];
                        $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                        $getUserDetails = $this->getUserDetails($user_id);
                        $email          = $getUserDetails[0]->email;
                        $firstName      = $getUserDetails[0]->first_name;
                        $lastName       = $getUserDetails[0]->last_name;
                        $currentOrgId   = $getUserDetails[0]->current_organization_id;
                        $userName       = $firstName.' '.$lastName;

                        $emailMessage   = config('event_activity')['email_message']["2"];
                        if($isDeleted==1) {
                            $active = 'deactive';
                            $emailBody      = str_replace(array("{{project_name}}","{{active}}"),array($projectName,$active),$emailMessage);
                        }else{
                            $active='active';
                            $emailBody      = str_replace(array("{{project_name}}","{{active}}"),array($projectName,$active),$emailMessage);
                        }

                         $emailSubject   = config('event_activity')['email_notification']["2"];
                         $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($loginUserId,$loginOrgId));
                         $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                         $organizationName = $organizationData['organization_name'];
                         $organizationCode = $organizationData['organization_code'];
                        $emailData = [
                            'email'    => $email,
                            'body'     => ['user_name'=>$userName,'email_body'=>$emailBody,'organization_name' =>$organizationName,'organization_code'=>$organizationCode],
                            'subject'  => $emailSubject,
                            'domain_name'=>$domainName
                        ];
                         //Check user email notification setting configuration ACMT-1869 of Sub part ACMT-1954
                         $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $user_id, 'organization_id' => $currentOrgId]));
                         $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                         if ($emailSettingStatus==1) {
                             // Email Notification
                             $this->dispatch(new NotificationJobForEmail($emailData));
                             unset($emailData);
                         }
                        //send Notification

                        //Prepare data
                        $notificationId = $this->createUniversalUniqueIdentifier();
                        $notificationMsg = $emailBody;

                        $activityLog = [
                           "notification_id"       => $notificationId,
                           "description"           => $notificationMsg,
                           "target_type"           => "13",
                           "target_id"             => $projectId,
                           "user_id"               => $user_id,
                           "organization_id"       => $organizationId,
                           "is_deleted"            => $isDeleted,
                           "created_by"            => $loginUserId, // logged in user Id
                           "updated_by"            => $loginUserId,
                           "target_context"        => $targetContextJson,
                           "read_status"           => "0", //0=unread 1= read
                           "notification_category" => $notificationCategory
                        ];
                       $this->notificationRepository->createNotification($activityLog);


                     }
                     unset($activityLog);
                     unset($targetContext);
                     unset($targetContextJson);
                 }

                return "";
                break;
            case "8": // If I have been removed from a project
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $projectId       = $data['project_id'];
                $user_id         = $data['user_id'];
                $domainName      = $data['domain_name'];
                $projectDetails  = $this->dispatch(new GetProjectDetailByProjectId(['project_id'=>$projectId]));
                $organizationId     = isset($projectDetails[0]->organization_id)?$projectDetails[0]->organization_id:'';
                $documentId         = isset($projectDetails[0]->document_id)?$projectDetails[0]->document_id:'';
                $createdBy          = isset($projectDetails[0]->created_by)?$projectDetails[0]->created_by:'';
                $isDeleted          = isset($projectDetails[0]->is_deleted)?$projectDetails[0]->is_deleted:'';
                $projectName        = isset($projectDetails[0]->project_name)?$projectDetails[0]->project_name:'';

                $getTaxonomyDetails   = $this->dispatch(new GetTaxonomyNameById(['document_id'=>$documentId]));
                $taxonomyName         = $getTaxonomyDetails[0]->title;
                $eventType            = config("event_activity")["Notifications"]["REVOKE_PROJECT_ROLE"];
                $notificationCategory = config("event_activity")["Notification_category"]["REVOKE_PROJECT_ROLE"];
                $messageNotify        = config('event_activity')['notification_message']["6"];
                $notification         = str_replace(array("{{project_name}}","{{taxonomy_name}}"),array($projectName,$taxonomyName),$messageNotify);
                $targetContext        = ['project_id'=>$projectId,'user_id'=>$user_id,'document_id'=>$documentId];

                $arrayData = ['project_id'  => $projectId, // target id
                    'created_by'            => $user_id,
                    'organization_id'       => $organizationId,
                    'notification'          => $notification,
                    'event_type'            => $eventType,
                    'target_context'        => $targetContext,
                    'is_deleted'            => $isDeleted,
                    'updated_by'            => $loginUserId,
                    'notification_category' => $notificationCategory
                ];
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData"  => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                ];
                $getUserDetails   = $this->getUserDetails($createdBy);
                $email            = $getUserDetails[0]->email;
                $firstName        = $getUserDetails[0]->first_name;
                $lastName         = $getUserDetails[0]->last_name;
                $currentOrgId     = $getUserDetails[0]->current_organization_id;
                $userName         = $firstName.' '.$lastName;
                $emailSubjectData = config('event_activity')['email_notification']["11"];
                $emailSubject     = str_replace(array("{{project_name}}"),array($projectName,),$emailSubjectData);

                $emailBodyData = config('event_activity')['email_notification']["11"];
                $emailBody     = str_replace(array("{{project_name}}"),array($projectName,),$emailBodyData);
                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($currentOrgId));
                $organizationName = $organizationData['organization_name'];
                $organizationCode = $organizationData['organization_code'];
                $emailData = [
                    'email'    => $email,
                    'body'     => ['user_name'=>$userName,'email_body'=>$emailBody,'organization_name'=>$organizationName,'organization_code'=>$organizationCode],
                    'subject'  => $emailSubject,
                    'domain_name'=>$domainName
                ];
                // Email notifications
                $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $createdBy, 'organization_id' => $organizationId]));
                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                if ($emailSettingStatus==1) {
                    $this->dispatch(new NotificationJobForEmail($emailData));
                }

                return $eventData;
                break;

            case "9": // If I have the permission to accept applications to ACMT/projects in ACMT, I will receive a notification when new request are received
                $projectId      = $data['project_id'];
                $permissionId   = $data['permission_id'];
                $role           = $data['role'];
                $userName       = $data['user_name'];
                $loginUserId     = $data['requestUserDetails']['user_id'];
                $projectDetails     = $this->dispatch(new GetProjectDetailByProjectId(['project_id'=>$projectId]));
                $organizationId     = isset($projectDetails[0]->organization_id)?$projectDetails[0]->organization_id:'';
                $documentId         = isset($projectDetails[0]->document_id)?$projectDetails[0]->document_id:'';
                $createdBy          = isset($projectDetails[0]->created_by)?$projectDetails[0]->created_by:'';
                $updatedBy          = isset($projectDetails[0]->updated_by)?$projectDetails[0]->updated_by:'';
                $isDeleted          = isset($projectDetails[0]->is_deleted)?$projectDetails[0]->is_deleted:'';
                $projectName        = isset($projectDetails[0]->project_name)?$projectDetails[0]->project_name:'';

                $notificationCategory = config("event_activity")["Notification_category"]["PROJECT_NEW_REQUEST_RECEIVED"];
                $messageNotify        = config('event_activity')['notification_message']["7"];
                $notification         = str_replace(array("{{username}}","{{list_of_roles}}","{{project_name}}"),array($userName,$role,$projectName,),$messageNotify);
               // $targetContext        = ['project_id'=>$projectId,'user_id'=>$createdBy,'permission_id'=>$permissionId,'document_id'=>$documentId];
                
                $permisionId = "fd3c70eb-b6a2-453d-9812-70331eeebe28";
                $inputList = ['permission_id' => $permisionId, 'organization_id' => $organizationId];
                $ProjectAccessRequestViewersList = $this->dispatch(new ProjectAccessRequestViewersListJob($inputList));
                foreach($ProjectAccessRequestViewersList as $ViewersListdata){
                    $user_id = $ViewersListdata->user_id;
                    $notificationId = $this->createUniversalUniqueIdentifier();
                    $targetContext        = ['project_id'=>$projectId,'user_id'=>$user_id,'permission_id'=>$permissionId,'document_id'=>$documentId];
                    $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                    $activityLog = [
                       "notification_id"       => $notificationId,
                       "description"           => $notification,
                       "target_type"           => "9",
                       "target_id"             => $projectId,
                       "user_id"               => $user_id,
                       "organization_id"       => $organizationId,
                       "is_deleted"            => $isDeleted,
                       "created_by"            => $loginUserId, // logged in user Id
                       "updated_by"            => $loginUserId,
                       "target_context"        => $targetContextJson,
                       "read_status"           => "0", //0=unread 1= read
                       "notification_category" => $notificationCategory
                    ];
                   $this->notificationRepository->createNotification($activityLog);
                   unset($activityLog);
                   unset($targetContextJson);
                   unset($targetContext);
                }
//                $getUserDetails = $this->getUserDetails($createdBy);
//                $email          = $getUserDetails[0]->email;
//                $firstName      = $getUserDetails[0]->first_name;
//                $lastName       = $getUserDetails[0]->last_name;
//                $userName       = $firstName.' '.$lastName;
//                $emailData = [
//                    'email'    => $email,
//                    'body'     => ['notification'=>$notification,'user_name'=>$userName],
//                ];
//                // Email notifications
//                $this->dispatch(new NotificationJobForEmail($emailData));
                return "";
                break;

            case "7": //If I have been assigned roles to a project
                $projectId           = $data['project_id'];
                $role_name           = $data['role_name'];
                $workFlowStageIdRole = $data['work_flow_stage_role_id'];
                $user_id             = $data['user_id'];
                $projectDetails       = $this->dispatch(new GetProjectDetailByProjectId(['project_id'=>$projectId]));
                $organizationId       = isset($projectDetails[0]->organization_id)?$projectDetails[0]->organization_id:'';
                $documentId           = isset($projectDetails[0]->document_id)?$projectDetails[0]->document_id:'';
                $createdBy            = isset($projectDetails[0]->created_by)?$projectDetails[0]->created_by:'';
                $updatedBy            = isset($projectDetails[0]->updated_by)?$projectDetails[0]->updated_by:'';
                $isDeleted            = isset($projectDetails[0]->is_deleted)?$projectDetails[0]->is_deleted:'';
                $projectName          = isset($projectDetails[0]->project_name)?$projectDetails[0]->project_name:'';
                $getTaxonomyDetails   = $this->dispatch(new GetTaxonomyNameById(['document_id'=>$documentId]));
                $taxonomyName         = $getTaxonomyDetails[0]->title;

                $notificationCategory = config("event_activity")["Notification_category"]["ASSIGN_PROJECT_ROLE"];
                $messageNotify        = config('event_activity')['notification_message']["5"];
                $notification         = str_replace(array("{{role_name}}","{{project_name}}"),array($role_name.$projectName),$messageNotify);
                $targetContext        = ['project_id'=>$projectId,'workflow_stage_role_id'=>$workFlowStageIdRole];
                $arrayData = [
                                'project_id'            => $projectId, // target id
                                'created_by'            => $user_id,
                                'organization_id'       => $organizationId,
                                'notification'          => $notification,
                                'event_type'            => $eventType,
                                'target_context'        => $targetContext,
                                'is_deleted'            => $isDeleted,
                                'updated_by'            => $updatedBy,
                                'notification_category' => $notificationCategory
                              ];
                          
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData"  => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                ];

                
//                $getUserDetails = $this->getUserDetails($createdBy);
//                $email          = $getUserDetails[0]->email;
//                $firstName      = $getUserDetails[0]->first_name;
//                $lastName       = $getUserDetails[0]->last_name;
//                $userName       = $firstName.' '.$lastName;
//                $emailData = [
//                    'email'    => $email,
//                    'body'     => ['notification'=>$notification,'user_name'=>$userName],
//                ];
//                // Email notifications
//                $this->dispatch(new NotificationJobForEmail($emailData));
                return $eventData;
                break;

            case "14": //I should receive in app notification when a pacing guide is published
                //$loginUserId           = $data['requestUserDetails']['user_id'];
                $projectId             = $data['project_id'];
                $getPacingGuideDetails = $this->dispatch(new GetPacingGuideDetails(['project_id'=>$projectId]));
                $pacingGuideName       = $getPacingGuideDetails[0]->title;
                $organizationId        = isset($getPacingGuideDetails[0]->organization_id)?$getPacingGuideDetails[0]->organization_id:'';
                $documentId            = isset($getPacingGuideDetails[0]->document_id)?$getPacingGuideDetails[0]->document_id:'';
                $createdBy             = isset($getPacingGuideDetails[0]->created_by)?$getPacingGuideDetails[0]->created_by:'';
                $updatedBy             = isset($getPacingGuideDetails[0]->updated_by)?$getPacingGuideDetails[0]->updated_by:'';
                $isDeleted             = isset($getPacingGuideDetails[0]->is_deleted)?$getPacingGuideDetails[0]->is_deleted:'';

                $notificationCategory  = config("event_activity")["Notification_category"]["PACING_GUIDES"];
                $messageNotify         = config('event_activity')['notification_message']["13"];
                $notification          = str_replace(array("{{pacing_guide_name}}"),array($pacingGuideName),$messageNotify);
                $targetContext         = ['project_id'=>$projectId,'document_id'=>$documentId];
                $arrayData = [
                    'project_id'            => $projectId, // target id
                    'created_by'            => $createdBy,
                    'organization_id'       => $organizationId,
                    'notification'          => $notification,
                    'event_type'            => $eventType,
                    'target_context'        => $targetContext,
                    'is_deleted'            => $isDeleted,
                    'updated_by'            => $updatedBy,
                    'notification_category' => $notificationCategory
                ];
                $this->setAfterEventRawData($arrayData);
                $eventData = [
                    "beforeEventRawData" => [],
                    "afterEventRawData"  => ($this->getAfterEventRawData($arrayData)),
                    "requestUserDetails" => $data['requestUserDetails']
                ];
                $getUserDetails = $this->getUserDetails($createdBy);
                $email          = $getUserDetails[0]->email;
                $firstName      = $getUserDetails[0]->first_name;
                $lastName       = $getUserDetails[0]->last_name;
                $currentOrgId   = $getUserDetails[0]->current_organization_id;
                $userName       = $firstName.' '.$lastName;
                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($currentOrgId));
                $organizationName = $organizationData['organization_name'];
                $organizationCode = $organizationData['organization_code'];
                $emailData = [
                    'email'    => $email,
                    'body'     => ['notification'=>$notification,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode],
                ];
                //Check user email notification setting configuration ACMT-1869 of Sub part ACMT-1962
                $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $createdBy, 'organization_id' => $organizationId]));
                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                if ($emailSettingStatus==1) {
                    // Email notifications
                    $this->dispatch(new NotificationJobForEmail($emailData));
                }
                return $eventData;
                break;

                case "16": // Taxonomy unpublished 
                    $eventType       = $data['event_type'];
                    $documentId      = $data['document_id'];
                    $domainName      = $data['domain_name'];
                    //get taxonomy name from document id
                    $getTaxonomyDetails    = $this->getDocumentDetails($documentId);
                    $taxonomyName          = isset($getTaxonomyDetails[0]->title)?$getTaxonomyDetails[0]->title:'';
                    $organizationId        = isset($getTaxonomyDetails[0]->organization_id)?$getTaxonomyDetails[0]->organization_id:'';
                    $isDeleted             = isset($getTaxonomyDetails[0]->is_deleted)?$getTaxonomyDetails[0]->is_deleted:'';
                    $updatedBy             = isset($getTaxonomyDetails[0]->updated_by)?$getTaxonomyDetails[0]->updated_by:'';
                    $createdBy             = isset($getTaxonomyDetails[0]->created_by)?$getTaxonomyDetails[0]->created_by:'';
                    $notificationCategory  = config('event_activity')["Notification_category"]["TAXONOMY_UNPUBLISHED"];
                    $messageNotify         = config('event_activity')['notification_message']["16"];
                    $notification          = str_replace(array("{{taxonomy_name}}"),array($taxonomyName),$messageNotify);
                    $targetType = 16;
                    $targetId = $documentId;
                    $loginUserId     = $data['requestUserDetails']['user_id'];
                    $readStatus            = 0;
                    // Sent mail to published Taxonomy
                    $listUser = $this->dispatch(new GetUserByPermissionJob(['organization_id' => $organizationId, 'permission_name' => "View Published Taxonomies"]));
                    if($listUser != 0)
                    {
                        foreach($listUser as $user)
                        {
                            $user_id = $user->user_id;
                            $targetContext         = ['document_id'=>$documentId,'user_id'=>$user_id];
                            $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                            $user_name = $user->first_name." ".$user->last_name;
                            $user_email = $user->email;
                            $currentOrgId = $user->current_organization_id;
                            $user_subject = "The {$taxonomyName} taxonomy has been unpublished.";
                            $email_body = "The {$taxonomyName} taxonomy has been unpublished.";
                            $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($loginUserId,$organizationId));
    
                            $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                            $organizationName = $organizationData['organization_name'];
                            $organizationCode = $organizationData['organization_code'];
                            $emailData1 = [
                                'subject'=>$user_subject,
                                 'email'    => $user_email,
                                'body'     => ['email_body'=>$email_body,'user_name'=>$user_name,'organization_name'=>$organizationName,'organization_code'=>$organizationCode],
                                'domain_name'=> $domainName
                             ];             
                             // Email notifications enable/disable ACMT-1869->1962
                            $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $user_id, 'organization_id' => $organizationId]));
                            $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                            if ($emailSettingStatus==1) {
                                $this->dispatch(new NotificationJobForEmail($emailData1));
                                unset($emailData1);
                            }
    
                             //send Notification
    
                             //Prepare data
                             $notificationId = $this->createUniversalUniqueIdentifier();
                             $notificationMsg = $email_body;
                           
                             $activityLog = [
                                "notification_id"       => $notificationId,
                                "description"           => $notificationMsg,
                                "target_type"           => $targetType,
                                "target_id"             => $targetId,
                                "user_id"               => $user_id,
                                "organization_id"       => $organizationId,
                                "is_deleted"            => $isDeleted,
                                "created_by"            => $loginUserId, // logged in user Id
                                "updated_by"            => $loginUserId,
                                "target_context"        => $targetContextJson,
                                "read_status"           => $readStatus, //0=unread 1= read
                                "notification_category" => $notificationCategory
                             ];
                            $this->notificationRepository->createNotification($activityLog);
                            unset($activityLog);
                            unset($targetContext);
                            unset($targetContextJson);
                        }
                    } 
    
                    return "";
                    break;

            default:
               // by default call when switch case is not available
        }
    }

    /**
     * @param $projectId
     * @return mixed
     * @purpose This function is used to get project details based on project id
     */

    public function getProjectDetails($projectId)
    {
        $query = $user = DB::table('projects')->where('project_id', $projectId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    /**
     * @param $documentId
     * @return mixed
     * @purpose This function is used to get document details based on document id
     */
    public function getDocumentDetails($documentId)
    {
        $query = $user = DB::table('documents')->where('document_id',$documentId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    public function getDocumentDetailsSourceId()
    {
        //$query = $user = DB::table('documents')->where('source_document_id',$documentId)->latest('updated_at')->first();
        $query = DB::table('documents')->latest('source_document_id')->first();
        return $query;
    }


    /**
     * @param $threadSourceId
     * @return mixed
     * @purpose This function is used to get project id based on item id
     */
    public function getProjectData($threadSourceId)
    {
        $query = $user = DB::table('project_items')->where('item_id',$threadSourceId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    public function getProjectDataFromDoc($threadSourceId)
    {
        $query = $user = DB::table('documents')->where('document_id',$threadSourceId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    public function getAssigneeFromThread($threadId)
    {
        $query = $user = DB::table('threads')->where('thread_id',$threadId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    /**
     * @param $userId
     * @return mixed
     * @purpose This function return user's detail based on used id
     */
    public function getUserDetails($userId)
    {
        $query = $user = DB::table('users')->where('user_id',$userId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    /**
     * @param $threadId
     * @return array|mixed|object
     * @purpose This function is used to get comment details
     */
    public function getProjectIdFromItemId($threadId){
        $getCommentDetails = DB::table('thread_comments')
            ->select('thread_comment_id')
            ->where('thread_id',$threadId)
            ->get();
        $threadComment = json_decode($getCommentDetails, true);
        return $threadComment;
    }

    /**
     * @param $threadCommentId
     * @return array|mixed|object
     */
    public function getCommentSourceId($threadCommentId){
        $getProjectItemDetails = DB::table('thread_comments')
            ->where('thread_comment_id',$threadCommentId)
            ->get();
        $commentData = json_decode($getProjectItemDetails, true);
        return $commentData;
    }

    /**
     * @param $workflowId
     * @return array|mixed|object
     * @purpose This function is used to return workflow details based on work flow stage id
     */
    public function getWorkFlowDetails($workflowId){
        $getWorkflowDetails = DB::table('workflow_stage')
            ->where('workflow_stage_id',$workflowId)
            ->get();
        $workflow = json_decode($getWorkflowDetails, true);
        return $workflow;
    }

    /**
     * @param $workflowStageId
     * @return array|mixed|object
     * @purpose This function is used to return role Id based on workflow stage role
     */
    public function getWorkFlowRoleId($workflowStageId){
        $getRoleId = DB::table('workflow_stage_role')
            ->where('workflow_stage_id',$workflowStageId)
            ->get();
        $roleId = json_decode($getRoleId, true);
        return $roleId;
    }

    /**
     * @param $roleId
     * @return array|mixed|object
     * @purpose This function is used to get user's role based on role id
     */
    public function getUserRole($roleId){
        $getRole = DB::table('roles')
            ->where('role_id',$roleId)
            ->get();
        $roleData = json_decode($getRole, true);
        return $roleData;
    }

    /**
     * @param $projectId
     * @return array|mixed|object
     * @purpose This function is used to return user's public review
     */
    public function getUserPublicReview($projectId){
        $getDetails = DB::table('project_user_status')
            ->where('project_id',$projectId)
            ->get();
        $data = json_decode($getDetails, true);
        return $data;
    }

    public function getDocumentID($document_id){

}

}
