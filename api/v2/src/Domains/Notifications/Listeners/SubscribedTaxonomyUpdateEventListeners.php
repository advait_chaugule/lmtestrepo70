<?php
namespace App\Domains\Notifications\Listeners;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Notifications\Jobs\SubscribedTaxonomyUpdateNotificationJob;
use App\Domains\Notifications\Events\SubscribedTaxonomyUpdateEvent as Event;
use Log;

class SubscribedTaxonomyUpdateEventListeners implements ShouldQueue
{
    use InteractsWithQueue, DispatchesJobs;

    public function handle(Event $event)
    {
        try {
            $data = $event->eventData;
            $subscriptionStatus = $this->dispatch(new SubscribedTaxonomyUpdateNotificationJob($data['document_id'], $data['organization_id'], $data['domain_name'],$data['requestUserDetails']['user_id']));
        }
        catch(\Exception $exception) {
            Log::error($exception);
        }
    }
}