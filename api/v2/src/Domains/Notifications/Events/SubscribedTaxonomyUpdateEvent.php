<?php
namespace  App\Domains\Notifications\Events;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
class SubscribedTaxonomyUpdateEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $eventData;
    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($eventData){
        $this->eventData = $eventData;

    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }

}

