<?php
namespace App\Domains\Permission\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\PermissionRepositoryInterface;


class ListAllPermissionJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PermissionRepositoryInterface $permissionRepo)
    {
        //
        $this->permissionRepository = $permissionRepo;
        $allPermissions = $this->permissionRepository->all();

        return $allPermissions->toArray();
    }
}
