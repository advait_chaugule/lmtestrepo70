<?php
namespace App\Domains\Permission\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\PermissionRepositoryInterface;
use App\Data\Models\Permission;

class ListAllPermissionPRTLJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PermissionRepositoryInterface $permissionRepo)
    {
        //
        $this->permissionRepository = $permissionRepo;
        $permissionCategory = ['View_public_review_taxonomies','View_public_review_nodes','view_public_review_associations','view_my_comments','Create_comment','Edit_comment','submit_public_review'];
        $allPermissions = Permission::where('permission_group',10)->whereIn('internal_name',$permissionCategory)->get();

        return $allPermissions->toArray();
    }
}
