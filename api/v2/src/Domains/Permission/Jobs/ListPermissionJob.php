<?php
namespace App\Domains\Permission\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\PermissionRepositoryInterface;

class ListPermissionJob extends Job
{
    private $permissionRepository;
    private $permissionList; 

    private $groupedSystemRolePermissions;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PermissionRepositoryInterface $permissionRepo)
    {
        //Set Repository handler
        $this->permissionRepository = $permissionRepo;
        $allPermissions = $this->permissionRepository->all();

        $this->setPermissionList($allPermissions);
        $this->groupSystemRolePermissions();

        $allPermissionsGroupedList = $this->getGroupedPermissions();
        return $allPermissionsGroupedList;
    }

    public function setPermissionList($data) {
        $this->permissionList = $data;
    }

    public function getPermissionList() {
        return $this->permissionList;
    }

    public function setGroupedPermissions(array $data) {
        $this->groupedSystemRolePermissions = $data;
    }

    public function getGroupedPermissions(): array {
        return $this->groupedSystemRolePermissions;
    }

    private function groupSystemRolePermissions() {
        $groupByPermissionsName = [
            "project_permissions"       => $this->helperToReturnParsedOutPermissions(1),
            "taxonomy_permissions"      => $this->helperToReturnParsedOutPermissions(2),
            "comment_permissions"       => $this->helperToReturnParsedOutPermissions(7),
            "metadata_permissions"      => $this->helperToReturnParsedOutPermissions(5),
            "node_type_permissions"     => $this->helperToReturnParsedOutPermissions(6),
            "node_template_permissions" => $this->helperToReturnParsedOutPermissions(8),
            "workflow_permissions"      => $this->helperToReturnParsedOutPermissions(3),
            "role_user_permissions"     => $this->helperToReturnParsedOutPermissions(4),
            "notification_permissions"  => $this->helperToReturnParsedOutPermissions(9),
            "public_review_permissions" => $this->helperToReturnParsedOutPermissions(10),
            "note_permissions"          => $this->helperToReturnParsedOutPermissions(11),
            "pacing_guide_permissions"  => $this->helperToReturnParsedOutPermissions(12)
        ];
        $this->setGroupedPermissions($groupByPermissionsName);
    }

    private function helperToReturnParsedOutPermissions($permissionGroupType): array {
        $permissions = $this->getPermissionList();
        $groupedPermissions = $permissions->filter(function ($item, $key) use($permissionGroupType) { 
            if($item->permission_group===$permissionGroupType) { 
                return $item; 
            } 
        });

        $parsedPermissions = [];
        foreach($groupedPermissions as $permission) {
            if($permission->permission_id != 'acee9a40-f0c0-4576-9f5c-0becfc177907'){
                $parsedPermissions[] = [
                    "permission_id" => $permission->permission_id,
                    "parent_permission_id" => $permission->parent_permission_id,
                    "display_name" => $permission->display_name,
                    "display_order" => $permission->order
                ];
            }
            
        }

        array_multisort(array_column($parsedPermissions, 'display_order'), SORT_ASC, $parsedPermissions);
        return $parsedPermissions;
    }
}
