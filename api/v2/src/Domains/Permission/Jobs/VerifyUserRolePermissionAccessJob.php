<?php
namespace App\Domains\Permission\Jobs;

use Lucid\Foundation\Job;

class VerifyUserRolePermissionAccessJob extends Job
{

    private $rolePermissionsAvailable;
    private $checkForPermissionAccess;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $rolePermissionsAvailable, array $checkForPermissionAccess)
    {
        $this->rolePermissionsAvailable = collect($rolePermissionsAvailable)->flatten(1);
        $this->checkForPermissionAccess = $checkForPermissionAccess;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $accessDetails = [];
        foreach($this->checkForPermissionAccess as $permissionInternalName) {
            $searchResult = $this->rolePermissionsAvailable->where('internal_name', $permissionInternalName);
            if($searchResult->isNotEmpty()) {
                $isAllowed = $searchResult->isNotEmpty();
                $accessDetails[] = [
                    'is_allowed' => $isAllowed,
                    'internal_name' => $permissionInternalName
                ];
            }
        }
        return $accessDetails;
    }
}
