<?php
namespace App\Domains\Permission\Tests\Jobs;

use App\Domains\Permission\Jobs\ListPermissionJob;
use Tests\TestCase;

class ListPermissionJobTest extends TestCase
{
    public function test_PermissionList()
    { 
        $assets = new ListPermissionJob();
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setPermissionList($value);
        $this->assertEquals($value,$assets->getPermissionList());
    }
}
