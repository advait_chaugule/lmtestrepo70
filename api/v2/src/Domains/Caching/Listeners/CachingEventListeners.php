<?php
namespace App\Domains\Caching\Listeners;

use App\Foundation\ReportListenerHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\Caching\Events\CachingEvent as Event;
use Illuminate\Support\Facades\Mail;
use Lucid\Foundation\QueueableJob;
use Illuminate\Foundation\Bus\DispatchesJobs;


use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;

use App\Domains\Asset\Jobs\GetListOfAssetJob;
use App\Domains\Asset\Jobs\GetAssetListJob;

use App\Domains\Note\Jobs\CheckNoteIsDeleteJob;
use App\Domains\Note\Jobs\DeleteNoteJob;
use App\Domains\Note\Jobs\GetAllNoteJob;
use App\Domains\Note\Jobs\GetNoteDetailsJob;
use App\Domains\Note\Jobs\UpdateNoteJob;
use App\Domains\Note\Jobs\ValidateNoteIdJob;

use App\Domains\Item\Jobs\ValidateItemByIdJob;
use App\Domains\Item\Jobs\CheckItemIsDeletedJob;
use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use App\Domains\Item\Jobs\CheckItemHasProjectJob;

use App\Domains\Language\Jobs\GetListOfLanguagesJob; 
use App\Domains\NodeType\Jobs\GetNodeTypeAndRelatedMetadataSetJob;
use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;

use App\Domains\Taxonomy\Jobs\GetFlattenedTaxonomyTreeV3Job;

use App\Domains\Document\Jobs\GetAllFileJob;
use App\Domains\Document\Jobs\ListPublishedDocumentRootJob; 
use App\Domains\Document\Jobs\ValidateDocumentByIdJob;
use App\Domains\Document\Jobs\CheckDocumentIsDeleteJob;
use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use App\Domains\Document\Jobs\CheckProjectAssociatedToDocumentJob;

use App\Domains\Project\Jobs\GetProjectByIdJob;
use App\Domains\Project\Jobs\GetNodesAssignedToProjectInTreeCompatibleFormJob;
use Illuminate\Support\Facades\Storage;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Notifications\Events\NotificationsEvent;
use App\Domains\Notifications\Events\SubscribedTaxonomyUpdateEvent;

use Log;
use DB;

class CachingEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue, UuidHelperTrait,DispatchesJobs;
  
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
     
    }
    

    public function handle(Event $event)
    {
        try {
            $eventData = $event->eventData;
            $this->cacheActionEvent($eventData);
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheActionEvent($data)
    {
        $eventType = isset($data['event_type'])?$data['event_type']:'';
        switch ($eventType) {
            case "PUBTAX": 
            $documentIdentifier     =  $data['document_id'];
            $organizationId         =  $data['organizationId'];
            $requestingUserDetail   =  $data['requestUserDetails'];
            $requestUrl             =  $data['domain_name'];
            $documents = $this->dispatch(new ListPublishedDocumentRootJob(['organization_id'=>$organizationId]));
            $taxonomyHierarchy          = $this->dispatch(new GetFlattenedTaxonomyTreeV3Job($documentIdentifier, $requestingUserDetail));

            // Set Hierarchy Of Taxonomy In S3
            $dataToSend1['status']  = 200;                                 // to set JSON format while saving file in S3
            $dataToSend1['data']    = $taxonomyHierarchy;                  // to set JSON format while saving file in S3
            $dataToSend1['message'] = 'Data Found';                        // to set JSON format while saving file in S3
            $dataToSend1['success'] = "";                                  // to set JSON format while saving file in S3
           
            $s3FileNameHierarchy      =  $documentIdentifier.'-hierarchy'.'.json';                         // set filename e.g. ba6f3ef5-0ca8-4e74-877d-0348358adc78-Hierarchy.json
            $s3BucketHierarchy        =  config("event_activity")["HIERARCHY_FOLDER"];                     // event activity
            $destinationS3Hierarchy   = "{$s3BucketHierarchy["MAIN_ARCHIVE_FOLDER"]}/$s3FileNameHierarchy";// s3 destination
            Storage::disk('s3')->put($destinationS3Hierarchy,json_encode($dataToSend1),'public');          // Saving file in s3
          
            // Set Details of Taxonomy In cache
            foreach($taxonomyHierarchy['nodes'] as $node) {

                $itemId = $node['id'];
                $isDocument = $node['is_document'];
                if($isDocument == 1)
                {
                    $validateDocument   =   $this->dispatch(new ValidateDocumentByIdJob(['document_id'   =>  $itemId]));  
                    if($validateDocument===true){
                        $validateDocumentIsDeleted  =   $this->dispatch(new CheckDocumentIsDeleteJob($itemId));    
                        if(!$validateDocumentIsDeleted) {
                            $documentDetails = $this->dispatch(new GetDocumentAndRespectiveMetadataValuesJob($itemId));

                            $documentAssets = $this->dispatch(new GetAssetListJob($itemId, $requestingUserDetail["organization_id"],$requestUrl));
                            $documentDetails["assets"] = $documentAssets;
                            if(isset($documentDetails['display_text'])){

                                $languageWithCode = $documentDetails['display_text'];       // get languege with its language code
                                $language = explode(' (',$languageWithCode);                // explode and get only the language
                                $documentDetails['language_name'] = $language[0];           // set language in 'langauge_name' for Table view(UF-1820)
                            }
                            $taxonomyHierarchy['details'][] = $documentDetails;
                            unset($documentDetails);
                        }
                    }
                }
                else
                {
                    $validationStatus = $this->dispatch(new ValidateItemByIdJob([ 'item_id' => $itemId ]));

                    if($validationStatus===true){
    
                        $itemIsDeleted = $this->dispatch(new CheckItemIsDeletedJob($itemId));
                        
                        if(!$itemIsDeleted) {
                            
                            $response = $this->dispatch(new GetCFItemDetailsJob($itemId, $organizationId));
                            $exemplarAssociationList = $this->dispatch(new GetExemplarAssociationsJob($itemId, $requestingUserDetail,$requestUrl));
                            // embed the exemplar association collection inside the current response body
                            $response["exemplar_associations"] = $exemplarAssociationList;

                            // ACMT-795 - Create separate job to fetch asset list and embed the same in the response body
                            $itemAssets = $this->dispatch(new GetAssetListJob($itemId, $requestingUserDetail["organization_id"],$requestUrl));
                            $response["assets"] = $itemAssets;
                            if(isset($response['display_text'])){

                                $languageWithCode = $response['display_text'];              // get languege with its language code
                                $language = explode(' (',$languageWithCode);                // explode and get only the language
                                $response['language_name'] = $language[0];                  // set language in 'langauge_name' for Table view(UF-1820)
                            }
                            $taxonomyHierarchy['details'][] = $response;
                            unset($response);
                        }
                    }
                }
            }
            //Set Details of Taxonomy In s3
            $dataToSend2['status']  = 200;                                                      // to set JSON format while saving file in S3
            $dataToSend2['data']    = ['details' => $taxonomyHierarchy['details']];             // to set JSON format while saving file in S3
            $dataToSend2['message'] = "Data Found";                                             // to set JSON format while saving file in S3
            $dataToSend2['success'] = "";                                                       // to set JSON format while saving file in S3
            
            $s3FileNameDetails      =  $documentIdentifier.'-details'.'.json';                       // set filename e.g. ba6f3ef5-0ca8-4e74-877d-0348358adc78-details.json
            $s3BucketDetails        =  config("event_activity")["HIERARCHY_FOLDER"];                 // event activity
            $destinationS3Details   = "{$s3BucketDetails["MAIN_ARCHIVE_FOLDER"]}/$s3FileNameDetails";// s3 destination
            Storage::disk('s3')->put($destinationS3Details,json_encode($dataToSend2),'public');      // Saving file in s3
                        
            $existsHeirarchy = Storage::disk('s3')->exists($destinationS3Hierarchy);                 // check if the taxonomyHeirarchy file exists in s3
            $existsDetails = Storage::disk('s3')->exists($destinationS3Details);                     // check if the taxonomyDetail file exists in s3
            
            //if file exists in s3 then entry in DB 
            if($existsHeirarchy && $existsDetails){
                    
                $update=[
                            'treehierarchy_json_file_s3' => $s3FileNameHierarchy ,
                            'treedetails_json_file_s3'    => $s3FileNameDetails,
                        ];

                $query = DB::table('taxonomy_pubstate_history')             // DB query
                        ->where('document_id',$documentIdentifier)
                        ->where('organization_id',$organizationId)
                        ->update($update);
            
                // set taxonomy list in cache     
                if(!empty($documents) && count($documents) > 0){

                        $orgIdentifier = $data['organizationId'];
                        
                        $keyInfo3 = array('identifier'=>$orgIdentifier, 'prefix'=>'docs');
                        
                        $status = $this->dispatch(new DeleteFromCacheJob($keyInfo3));
                        $status = $this->dispatch(new SetToCacheJob($documents, $keyInfo3));                        
                        if(is_array($status) && $status['status']=="error"){
                            // handle failure, data already exists , delete and re-add all
                            $status = $this->dispatch(new DeleteFromCacheJob($keyInfo3));
                            $status = $this->dispatch(new SetToCacheJob($documents, $keyInfo3));
                        }
                        $this->callNotification($data);                        
                    }

            }else{
                    $response       = [];
                    $successType    = 'custom_not_found';
                    $message        = 'File does not exist in S3';
                    $_status        = 'custom_status_here';
                    return $this->run(new RespondWithJsonJob($successType, $response, $message, $_status));
                }

            //return $data;                    
            break;
            case "PUBPROJECT": 
                       break;
            
            
            /** Get all note from cache */
            case "NOTEGETALL":
                $requestingUserDetail['note_id']     =  '';
                $requestingUserDetail['organization_id']   =  $data['organization_id'];
                $getNote     = $this->dispatch(new GetAllNoteJob($requestingUserDetail));
                // Set created note in cache
                $keyInfo = array('identifier'=> $data['organization_id'], 'prefix'=>'notes_');
                $status = $this->dispatch(new DeleteFromCacheJob($keyInfo));
                $status1 = $this->dispatch(new SetToCacheJob($getNote, $keyInfo));
                $status3 = $this->dispatch(new GetFromCacheJob($keyInfo));

                if(is_array($status1) && $status1['status']=="error"){
                    // handle failure, data already exists
                }
                //return $status3;
                break;

            /** Get all file from cache */
            case "FILESGETALL":
                $requestingUserDetail['organization_id']  = $data['requestUserDetails']['organization_id'];
                $getNote     = $this->dispatch(new GetListOfAssetJob($requestingUserDetail['organization_id'],$requestingUserDetail));
                // Set created note in cache
                $keyInfo = array('identifier'=> $requestingUserDetail['organization_id'], 'prefix'=>'FILES_');
                $status = $this->dispatch(new DeleteFromCacheJob($keyInfo));
                $status1 = $this->dispatch(new SetToCacheJob($getNote, $keyInfo));
                if(is_array($status1) && $status1['status']=="error"){
                    // handle failure, data already exists
                }
                break;

            /** Set the updated nodetype and its related data */
            case "NODETYPES":
                $organizationIdentifier     =   $data['organizationId'];
                $getNodeTypeDetail          =   $this->dispatch(new GetNodeTypeAndRelatedMetadataSetJob($organizationIdentifier));
                $languageList               =   $this->dispatch(new GetListOfLanguagesJob($organizationIdentifier));

                $dataToCache    =  ['nodetype'  => $getNodeTypeDetail, 'language' => $languageList]; 

                // Set created note in cache
                $keyInfo = array('identifier'=> $organizationIdentifier, 'prefix'=>'node_types_');
                $status = $this->dispatch(new DeleteFromCacheJob($keyInfo));
                $status1 = $this->dispatch(new SetToCacheJob($dataToCache, $keyInfo));
                $status3 = $this->dispatch(new GetFromCacheJob($keyInfo));

                if(is_array($status1) && $status1['status']=="error"){
                    // handle failure, data already exists
                }
                
                break;

            /** Set taxonomy in cache */
            case "TAXOINS":
            
                break;

            default:
        }

    }
   
    public function callNotification($data){
        //In App Notification when user publish taxonomy
        $eventType               = config("event_activity")["Notifications"]["TAXONOMY_PUBLISHED"]; // 11
        $eventData = [
            'document_id'        => $data['document_id'],
            'event_type'         => $eventType,
            'requestUserDetails' => $data['requestUserDetails'],
            'domain_name'        => $data['domain_name'],
            "beforeEventRawData" => [],
            'afterEventRawData'  => ''
        ];
        event(new NotificationsEvent($eventData)); // notification ends here

        //Email/In App Notification when publisher publishes taxonomy
        $eventType               = config("event_activity")["Notifications"]["SUBSCRIBED_TAXONOMY_UPDATED"]; // 20
        $eventData = [
            'document_id'        => $data['document_id'],
            'event_type'         => $eventType,
            'requestUserDetails' => $data['requestUserDetails'],
            'organization_id'    => $data['organizationId'],
            'domain_name'        => $data['domain_name'],
            "beforeEventRawData" => [],
            'afterEventRawData'  => ''
        ];
        event(new SubscribedTaxonomyUpdateEvent($eventData)); // notification ends here        
    }

}
