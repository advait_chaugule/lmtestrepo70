<?php
namespace App\Domains\Caching\Jobs;

use Lucid\Foundation\Job;

use Memcache;

class FlushCacheJob extends Job
{
    private $cacheObject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * connect to the cache endpoint using Memcache extension
     */
    private function connectToCache() {
        $this->cacheObject = new Memcache();
        try{
            $this->cacheObject->connect(env('MEMCACHED_ENDPOINT'),11211);
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
        }
    }

    private function flush(){
        $status = $this->cacheObject->flush();
        //sleep(1);
        usleep(1 * 1000);
        return $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->connectToCache();
        return $this->flush();
    }
}
