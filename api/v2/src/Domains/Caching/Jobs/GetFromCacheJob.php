<?php
namespace App\Domains\Caching\Jobs;

use Lucid\Foundation\Job;

use Memcache;
use Log;
use Framework\Exceptions\MyGzException;

class GetFromCacheJob extends Job
{
    private $preparedKey;
    private $cacheObject;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $keyInfo)
    {
        $this->setKey($keyInfo);
    }

    public function setKey(array $data) {
        $this->preparedKey = $data['prefix'].'_'.$data['identifier'];
    }

    private function connectToCache() {
        $this->cacheObject = new Memcache();
        try{
            $this->cacheObject->connect(env('MEMCACHED_ENDPOINT'),11211); //Cache::store('memcached')['servers']['host']
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            //return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function getItems() {
        set_error_handler(
            function($errno, $errstr, $errfile, $errline) {
                if ($errno == E_WARNING && $errstr == 'gzuncompress(): data error') {
                    throw new MyGzException($errstr);
                }
            }
        );
        try{
            $chunks = [];
            $listSize = '';
            $isSuccessful = true;
            $data_blank = [];
            $listSize = $this->cacheObject->get($this->preparedKey.'_index');
            if($listSize > 0){
                
                for($i=0; $i < $listSize; $i++) {

                    $indexKey = $this->preparedKey.'_'.$i;

                    //Log::debug(__METHOD__.' : Storing ' . $indexKey . ' for ' . $cacheTime . ' minutes');
                    //Cache::tags($tag)->put($indexKey, $store, $cacheTime);
                    $chunks[] = $this->cacheObject->get($indexKey);
                    if($chunks[$i] === false){
                        $isSuccessful = false;
                        break;
                    }
                }

                if(!$isSuccessful){
                    return array("status"=>"error", "data" => $data_blank, "message"=>"Entry not found. Delete and sync again.");
                }

                $compressed = implode('', $chunks);
                return unserialize(gzuncompress($compressed));

            }else{

                return array("status"=>"error", "data" => $data_blank ,"message"=>"index entry not found. Delete and sync again.");
            }
        }
        catch (MyGzException $ex) {
            Log::error('GZException: '.$this->preparedKey);
            Log::error($ex);
            return array("status"=>"error", "data" => [] ,"message"=>"index entry not found. Delete and sync again.");
        }
        catch (\Exception $ex) {
            Log::error('GetFromCache Exception: '.$this->preparedKey);
            Log::error($ex);
            return array("status"=>"error", "data" => [] ,"message"=>"index entry not found. Delete and sync again.");
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->connectToCache();
        return $this->getItems();
    }
}
