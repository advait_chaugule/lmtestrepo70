<?php
namespace App\Domains\Caching\Jobs;

use Lucid\Foundation\Job;

use Memcache;
use Log;

class DeleteFromCacheJob extends Job
{
    private $preparedKey;
    private $cacheObject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $keyInfo)
    {
        $this->setKey($keyInfo);
    }

    public function setKey(array $data) {
        $this->preparedKey = $data['prefix'].'_'.$data['identifier'];
    }

    private function connectToCache() {
        $this->cacheObject = new Memcache();
        try{
            $this->cacheObject->connect(env('MEMCACHED_ENDPOINT'),11211);
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            //return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    private function deleteItems() {
        try{
            $listSize = '';
            $isSuccessful = true;
            
            $listSize = $this->cacheObject->get($this->preparedKey.'_index');
            if($listSize > 0){
                
                for($i=0; $i < $listSize; $i++) {

                    $indexKey = $this->preparedKey.'_'.$i;

                    if(!$this->cacheObject->delete($indexKey)){
                    
                        $isSuccessful = false;
                        break;
                    }
                }

                if(!$isSuccessful){
                    return array("status"=>"error", "message"=>"Few entries were not found.");
                }

                return array("status"=>200, "message"=>"Deleted successfully");

            }else{
                return array("status"=>"error", "message"=>"index entry not found.");
            }
        }
        catch (\Exception $ex) {
            Log::error('DeleteFromCache Exception: '.$this->preparedKey);
            Log::error($ex);
            return array("status"=>"error", "message"=>"index entry not found.");
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->connectToCache();
        return $this->deleteItems();
    }
}
