<?php
namespace App\Domains\Caching\Jobs;

use Lucid\Foundation\Job;

use Memcache;
use Log;

class SetToCacheJob extends Job
{
    private $valueToSet;
    private $preparedKey;
    private $cacheObject;
    private $isSuccessful;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $valueToSet, array $keyInfo)
    {
        $this->setValue($valueToSet);
        $this->setKey($keyInfo);
    }

    public function setValue(array $data) {
        $this->valueToSet = $data;
    }

    public function setKey(array $data) {
        $this->preparedKey = $data['prefix'].'_'.$data['identifier'];
    }
    
    /**
     * connect to the cache endpoint using Memcache extension
     */
    private function connectToCache() {
        $this->cacheObject = new Memcache();
        try{
            $this->cacheObject->connect(env('MEMCACHED_ENDPOINT'),11211); //Cache::store('memcached')['servers']['host']
        }catch (\Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            //return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }

    /**
     * add the data to Memcache endpoint by creating chunks based on certain length
     * caching time is set to zero
     */
    private function addValueByChunks() {
        try{
            $this->isSuccessful = true;
            $splitSize = 1000*900;    // 900kb (less than 1 meg)
            $serializedData = gzcompress(serialize($this->valueToSet));
            $chunks = str_split($serializedData, $splitSize);
            $cacheTime = 0;//86400;

            foreach ($chunks as $index => $chunk) {

                // generating the key like "{componentType}_{component_id}_{increemntalIndex}
                // sample value "doc_ce8355e2-9f7d-0862-0cfd-4186600851d3_1
                $keyWithIndex = $this->preparedKey.'_'.$index;
                
                if(!$this->cacheObject->add($keyWithIndex, $chunk, $cacheTime)){
                    Log::error(__METHOD__.' : memcache set failure Storing ' . $keyWithIndex . ' for ' . $cacheTime . ' minutes');
                    $this->isSuccessful = false;
                    break;
                }
                //Log::debug(__METHOD__.' : Storing ' . $keyWithIndex . ' for ' . $cacheTime . ' minutes');
            }

            if(!$this->isSuccessful)
                return array('status'=>"error", "message"=>"data already exists");
            else{
                // generating the index key like "{componentType}_{component_id}_index
                // sample value "doc_ce8355e2-9f7d-0862-0cfd-4186600851d3_index
                $this->cacheObject->add($this->preparedKey.'_index', sizeof($chunks));
                return array('status'=>200, "message"=>"successfully added data");
            }
        }
        catch (\Exception $ex) {
            Log::error('SetToCache Exception: '.$this->preparedKey);
            Log::error($ex);
            return array('status'=>"error", "message"=>"data already exists");
        }
    }

    private function flush(){
        $this->cacheObject->flush();
    }
  

    /**
     * Execute the job.
     *
     * @return the status of adding the data
     */
    public function handle()
    {
        
        $this->connectToCache();
        return $this->addValueByChunks();
        //$this->flush();
        //return 'flushed';
    }
}
