<?php
namespace App\Domains\Association\Jobs;
use Lucid\Foundation\Job;
use DB;
use App\Services\Api\Traits\CaseFrameworkTrait;

class GetAssociationPresetList extends Job
{
    use CaseFrameworkTrait;
    private $userId;
    private $organizationId;

    public function __construct($userId,$organizationId)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $response = $this->getAssociationPresetList($this->userId,$this->organizationId);
        return $response;
    }

    private function getAssociationPresetList($userId,$organizationId){
        $finalResponse = [];
        $presetResponse = [];

        $fields = ['title','source_document_type','destination_document_type','item_association_type','node_types.node_type_id'];
        $getAllConfiguration = DB::table('item_association_nodetype')
                            ->select($fields)
                            ->leftJoin('node_types', 'item_association_nodetype.node_type_id', '=', 'node_types.node_type_id')
                            ->where('item_association_nodetype.organization_id','=',$organizationId)
                            ->where('node_types.used_for','=',1)
                            ->where('item_association_nodetype.is_deleted','=',0)
                            ->get()
                            ->toarray();
    
        if(!empty($getAllConfiguration)){
            foreach($getAllConfiguration as $singleConfiguration){
                $nodeTypeId = $singleConfiguration->node_type_id;
                $presetResponse = (array)$singleConfiguration;
                $taxonomyType = DB::table('metadata')
                                ->select('field_possible_values')
                                ->where('organization_id','=',$organizationId)
                                ->where('internal_name','=','taxonomy type')
                                ->where('is_deleted','=',0)
                                ->get()
                                ->toarray();
                $taxonomyValues = json_decode($taxonomyType[0]->field_possible_values);
              
                foreach($taxonomyValues as $taxonomyValuesV){
                    if($taxonomyValuesV->key == $singleConfiguration->source_document_type){
                        $presetResponse['source_document_name'] = $taxonomyValuesV->value;
                    }
                    if($taxonomyValuesV->key == $singleConfiguration->destination_document_type){
                        $presetResponse['destination_document_name'] = $taxonomyValuesV->value;
                    }
                }

                $list = $this->getAllSystemSpecifiedAssociationType();
                foreach($list as $row){
                    if($row['type_id'] == $singleConfiguration->item_association_type){
                        $presetResponse['item_association_name'] =  $row['display_name'];
                    }
                }
              
                $metadataquery = DB::table('node_type_metadata')
                            ->select('node_type_metadata.metadata_id','node_type_metadata.is_mandatory','sort_order','field_possible_values','name','internal_name')
                            ->leftJoin('metadata', 'node_type_metadata.metadata_id', '=', 'metadata.metadata_id')
                            ->where('node_type_id','=',$nodeTypeId)
                            ->where('organization_id','=',$organizationId)
                            ->where('is_deleted','=',0)
                            ->get()
                            ->toarray();
                $presetResponse['metadata'] = (array)$metadataquery;
                array_push($finalResponse,$presetResponse);
            }
        }
        return $finalResponse;

    }

}
