<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class ValidatePresetJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $name;
    
    public function __construct($sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType,$organizationId)
    {
        $this->sourceTaxonomyTypeId = $sourceTaxonomyTypeId;
        $this->targetTaxonomyTypeId = $targetTaxonomyTypeId;
        $this->associationType = $associationType;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('item_association_nodetype')
                ->where('source_document_type','=',$this->sourceTaxonomyTypeId)
                ->where('destination_document_type','=',$this->targetTaxonomyTypeId)
                ->where('item_association_type','=',$this->associationType)
                ->where('organization_id','=',$this->organizationId)
                ->where('is_deleted','=',0)->get();
            
        return $query->first() ? true : false;
    }
}