<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Association\Validators\CFAssociationValidator;

class ValidateAssociationByIdJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CFAssociationValidator $validator)
    {
        //
        $validation = $validator->validate($this->identifier);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
