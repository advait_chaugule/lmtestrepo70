<?php
namespace App\Domains\Association\Jobs;
use Lucid\Foundation\Job;
use DB;
use App\Services\Api\Traits\UuidHelperTrait;

class CreateAssociationPreset extends Job
{
    use UuidHelperTrait;

    private $userId;
    private $organizationId;

    public function __construct($userId,$organizationId,$name,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType,$metadata)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->name = $name;
        $this->sourceTaxonomyTypeId = $sourceTaxonomyTypeId;
        $this->targetTaxonomyTypeId = $targetTaxonomyTypeId;
        $this->associationType = $associationType;
        $this->metadata = $metadata;
    }

    public function handle()
    {
        $nodeType_Id  = $this->addNewNodeTypeAndMapMetadata($this->userId,$this->organizationId,$this->name,$this->metadata);
        if(!empty($nodeType_Id)){
            $addResponse  = $this->addAssociationPreset($nodeType_Id,$this->userId,$this->organizationId,$this->sourceTaxonomyTypeId,$this->targetTaxonomyTypeId,$this->associationType);
        }
       return $addResponse;
    }

    private function addNewNodeTypeAndMapMetadata($userId,$organizationId,$name,$metadata){
        $nodeTypeId = $this->createUniversalUniqueIdentifier();

        $nodeTypeCreated = DB::table('node_types')->insert(
            ['node_type_id' => $nodeTypeId, 'organization_id' => $organizationId, 'title' => $name,  'source_node_type_id'  => $nodeTypeId, 'created_by' => $userId, 'updated_by' => $userId ,'used_for' => 1]
        );

        if($nodeTypeCreated == true) {
            foreach($metadata as $metadataK => $metadataV) {
                $nodeMetaData[] = [
                    'node_type_id'  =>  $nodeTypeId,
                    'metadata_id'   =>  $metadataV['metadata_id'],
                    'sort_order'    =>  $metadataV['sort_order'],
                    'is_mandatory'  =>  $metadataV['is_mandatory'],
                ];
            }

            DB::table('node_type_metadata')->insert($nodeMetaData);
        }
        
        return $nodeTypeId;

    }

    private function addAssociationPreset($nodeType_Id,$userId,$organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType){
      
        $date = date('Y-m-d H:i:s');
        $associationPreset = DB::table('item_association_nodetype')->insert(
            ['source_document_type' => $sourceTaxonomyTypeId, 'destination_document_type' => $targetTaxonomyTypeId, 'item_association_type' => $associationType,  'node_type_id'  => $nodeType_Id, 'organization_id' => $organizationId ,'is_deleted' => 0 ,'created_by' => $userId, 'updated_by' => $userId, 'created_at'=> $date, 'updated_at'=>$date]
        );

        return $associationPreset;

    }

}