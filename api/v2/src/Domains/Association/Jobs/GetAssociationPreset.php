<?php
namespace App\Domains\Association\Jobs;
use Lucid\Foundation\Job;
use DB;

class GetAssociationPreset extends Job
{
    private $userId;
    private $organizationId;

    public function __construct($userId,$organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->sourceTaxonomyTypeId = $sourceTaxonomyTypeId;
        $this->targetTaxonomyTypeId = $targetTaxonomyTypeId;
        $this->associationType = $associationType;
    }

    public function handle()
    {
        $response = $this->getAssociationPreset($this->userId,$this->organizationId,$this->sourceTaxonomyTypeId,$this->targetTaxonomyTypeId,$this->associationType);
        return $response;
    }

    private function getAssociationPreset($userId,$organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType){

        $fields = ['title','source_document_type','destination_document_type','item_association_type','node_types.node_type_id'];
        $getConfiguration = DB::table('item_association_nodetype')
                            ->select($fields)
                            ->leftJoin('node_types', 'item_association_nodetype.node_type_id', '=', 'node_types.node_type_id')
                            ->where('item_association_nodetype.source_document_type','=',$sourceTaxonomyTypeId)
                            ->where('item_association_nodetype.destination_document_type','=',$targetTaxonomyTypeId)
                            ->where('item_association_nodetype.item_association_type','=',$associationType)
                            ->where('item_association_nodetype.organization_id','=',$organizationId)
                            ->where('node_types.used_for','=',1)
                            ->get()
                            ->toarray();
       
        if(!empty($getConfiguration)){
            $nodeTypeId = $getConfiguration[0]->node_type_id;
            $presetResponse = (array)$getConfiguration[0];
            
            $metadataquery = DB::table('node_type_metadata')
                            ->select('node_type_metadata.metadata_id','node_type_metadata.is_mandatory','sort_order','field_possible_values','name','internal_name')
                            ->leftJoin('metadata', 'node_type_metadata.metadata_id', '=', 'metadata.metadata_id')
                            ->where('node_type_id','=',$nodeTypeId)
                            ->where('organization_id','=',$organizationId)
                            ->where('is_deleted','=',0)
                            ->get()
                            ->toarray();
         

            $presetResponse['metadata'] = (array)$metadataquery;
            return $presetResponse;
        }
       
    }
}