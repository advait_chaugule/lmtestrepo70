<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;
use DB;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetAssociationMetadataNodetypeIdJob extends Job
{
    private $userId;
    private $organizationId;
    private $itemAssociationId;
    private $associationTypeId;

    public function __construct($userId,$organizationId,$itemAssociationId, $associationTypeId,$itemId)
    {
        $this->userId = $userId;
        $this->organizationId = $organizationId;
        $this->itemAssociationId = $itemAssociationId;
        $this->associationTypeId = $associationTypeId;
        $this->itemId = $itemId;
    }

    public function handle(ItemRepositoryInterface $itemRepository)
    {
        $reverseAssociationType = [3, 5, 7];
        $documentIdsList = DB::table('item_associations')
                            ->select('target_item_id','source_document_id', 'target_document_id')
                            ->where('item_association_id', $this->itemAssociationId)
                            ->where('is_deleted', 0)
                            ->first();

        $documentIds = [
            $documentIdsList->source_document_id,
            $documentIdsList->target_document_id
        ];
        $Item               = $itemRepository->getItemDetailsWithProjectAndCaseAssociation($this->itemId);
        $documentTypes = DB::table('documents')
                            ->select('document_id', 'document_type')
                            ->whereIn('documents.document_id', $documentIds)
                            ->get();
        foreach($documentTypes as $documentType) {
            $docArr[$documentType->document_id] = $documentType->document_type;
        }

        # if not reverse association
        $sourceDocumentType = $docArr[$documentIds[0]];
        $targetDocumentType = $docArr[$documentIds[1]];
        $associationTypeNumber = $this->associationTypeId;
        if(in_array($this->associationTypeId, $reverseAssociationType)) {
            if($documentIdsList->target_item_id==$this->itemId){
                $sourceDocumentType = $docArr[$documentIds[1]];
                $targetDocumentType = $docArr[$documentIds[0]];
                if($associationTypeNumber == 3) {
                    $associationTypeNumber = 10;
                } else if($associationTypeNumber == 5) {
                    $associationTypeNumber = 11;
                } else if($associationTypeNumber == 7) {
                    $associationTypeNumber = 12;
                }
            }
        }

        $nodetypeid = DB::table('item_association_nodetype')->select('node_type_id')
                        ->where('item_association_nodetype.organization_id', $this->organizationId)
                        ->where('item_association_nodetype.is_deleted', 0)
                        ->where('item_association_nodetype.source_document_type', $sourceDocumentType)
                        ->where('item_association_nodetype.destination_document_type', $targetDocumentType)
                        ->where('item_association_nodetype.item_association_type', $associationTypeNumber)
                        ->first();
        if(!isset($nodetypeid->node_type_id)) {
            return '';
        }

        return $nodetypeid->node_type_id;
    }
}