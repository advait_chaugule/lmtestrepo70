<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;
use DB;

class GetAssociationMetadataJob extends Job
{
    private $organizationId;
    private $itemAssociationId;
    private $nodetypeid;

    public function __construct($organizationId, $itemAssociationId, $nodetypeid='')
    {
        $this->organizationId = $organizationId;
        $this->itemAssociationId = $itemAssociationId;
        $this->nodetypeid = $nodetypeid;
    }

    public function handle()
    {   
        $asso_metadata = array();
        $asso_metadata = DB::table('item_association_metadata')
                            ->select([
                                'item_association_metadata.metadata_id',
                                'metadata.name',
                                'item_association_metadata.metadata_value',
                                'item_association_metadata.is_additional as is_mandatory',
                                'item_association_metadata.sort_order'
                                ])
                            ->leftJoin('metadata', 'item_association_metadata.metadata_id', '=', 'metadata.metadata_id')
                            ->where('item_association_metadata.item_association_id','=',$this->itemAssociationId)
                            ->where('item_association_metadata.is_deleted', 0)
                            ->get()
                            ->toArray();
        
        $metadataPreset = array(); 
		// Condition handled if preset is empty
        if(!empty($this->nodetypeid)){
            $metadataPreset = DB::table('node_type_metadata')
                    ->select('node_type_metadata.metadata_id','node_type_metadata.is_mandatory','sort_order','name')
                    ->leftJoin('metadata', 'node_type_metadata.metadata_id', '=', 'metadata.metadata_id')
                    ->where('node_type_id', $this->nodetypeid)
                    ->where('organization_id', $this->organizationId)
                    ->where('is_deleted', 0)
                    ->get()
                    ->toArray();
        }                   
        // return if both empty
        if(empty($asso_metadata)  && empty($metadataPreset))
            return [];

        $itemAssociationMetadataWithNodeMetadata = array();                
        if(!empty($asso_metadata)  && !empty($metadataPreset))
        {
            foreach($asso_metadata as $asso_metadataK=>$asso_metadataV)
            {
               foreach($metadataPreset as $metadataPresetK=>$metadataPresetV)
               {
                   if($asso_metadataV->metadata_id  == $metadataPresetV->metadata_id)
                   {
                     $asso_metadataV->sort_order = $metadataPresetV->sort_order;
                     $asso_metadataV->is_mandatory = $metadataPresetV->is_mandatory;
                     $asso_metadataV->can_delete = 0;
                     $itemAssociationMetadataWithNodeMetadata[$asso_metadataK] =  $asso_metadataV;
                     if($asso_metadataV->metadata_value==null){
                        $asso_metadataV->metadata_value = "";
                    }
                     unset($asso_metadata[$asso_metadataK]);
                     unset($metadataPreset[$metadataPresetK]);
                   }
               }
            }
			// Condition added to show association metadata as non-mandatory if it does not belong to preset
            foreach($asso_metadata as $asso_metadataK=>$asso_metadataV)
            {
                $asso_metadata[$asso_metadataK]->is_mandatory = 0;
                $asso_metadata[$asso_metadataK]->can_delete = 1;
                if($asso_metadataV->metadata_value==null){
                    $asso_metadataV->metadata_value = "";
                }   
            }

            foreach($metadataPreset as $metadataPresetValue) 
            { 
                $metadataPresetValue->can_delete=0;
                $metadataPresetValue->metadata_value = "";
            }
			// Merged preset & assoication metadata 
            $itemAssociationMetadataWithNodeMetadata = array_merge($itemAssociationMetadataWithNodeMetadata,$asso_metadata,$metadataPreset);
            return $itemAssociationMetadataWithNodeMetadata;
        } 
        
                   
        if(!empty($asso_metadata)) {
            foreach($asso_metadata as $asso_metadataValue){
                if($asso_metadataValue->metadata_value==null){
                    $asso_metadataValue->metadata_value = "";
                }
                $asso_metadataValue->can_delete=1;
            }
            return $asso_metadata;
        } else {
            $metadataPreset = DB::table('node_type_metadata')
                            ->select('node_type_metadata.metadata_id','node_type_metadata.is_mandatory','sort_order','name')
                            ->leftJoin('metadata', 'node_type_metadata.metadata_id', '=', 'metadata.metadata_id')
                            ->where('node_type_id', $this->nodetypeid)
                            ->where('organization_id', $this->organizationId)
                            ->where('is_deleted', 0)
                            ->get();
                  
                foreach($metadataPreset as $metadataPresetValue) 
                { 
                    $metadataPresetValue->can_delete=0;
                    $metadataPresetValue->metadata_value="";
                }     
            return $metadataPreset;
        }
    }
}