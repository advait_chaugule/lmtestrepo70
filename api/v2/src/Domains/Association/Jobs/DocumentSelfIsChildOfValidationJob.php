<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;
use DB;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class DocumentSelfIsChildOfValidationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         $request = $this->request;
         $sourceItemId = $request['origin_node_id'];
         $targetItemId = $request['destination_node_ids'];
         $associationType = $request['association_type'];
        
         $response = $this->getDocumentIdsBasedOnItemId($sourceItemId,$targetItemId,$associationType);

         return $response;
    }

    public function getDocumentIdsBasedOnItemId($sourceItemId,$targetItemId,$associationType)
    {
        $getDocumentIds = DB::table('items')
        ->select('document_id')
        ->whereIn('item_id',$targetItemId)
        ->get()
        ->toArray();

        $documentIdList = array_column($getDocumentIds, 'document_id');



        if(in_array($sourceItemId,$documentIdList) && $associationType==1)
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }

}
