<?php
namespace App\Domains\Association\Jobs;
use Lucid\Foundation\Job;
use DB;

class UpdateAssociationPreset extends Job
{
    private $userId;
    private $organizationId;

    public function __construct($userId,$organizationId,$name,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType,$metadata,$nodeTypeId)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->name = $name;
        $this->sourceTaxonomyTypeId = $sourceTaxonomyTypeId;
        $this->targetTaxonomyTypeId = $targetTaxonomyTypeId;
        $this->associationType = $associationType;
        $this->metadata = $metadata;
        $this->nodeTypeId = $nodeTypeId;
    }

    public function handle()
    {
        $response = $this->updatePreset($this->userId,$this->organizationId,$this->name,$this->sourceTaxonomyTypeId,$this->targetTaxonomyTypeId,$this->associationType,$this->metadata,$this->nodeTypeId);  
        return $response;
    }

    private function updatePreset($userId,$organizationId,$name,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType,$metadata,$nodeTypeId){
        DB::beginTransaction();

        try{

            $nodetypeId = DB::table('item_association_nodetype')
            ->where('node_type_id',$nodeTypeId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->toArray();
            
            if(!empty($nodetypeId)){
                $nodeType_id = $nodetypeId[0]->node_type_id;
                $sourceTypeId = $nodetypeId[0]->source_document_type;
                $targetTypeId = $nodetypeId[0]->destination_document_type;
                $association_type = $nodetypeId[0]->item_association_type;
            
                if(($sourceTaxonomyTypeId!= $sourceTypeId) || ($targetTaxonomyTypeId !=$targetTypeId) || ($associationType!= $association_type)){
                    $checkPreset = DB::table('item_association_nodetype')
                                    ->where('source_document_type',$sourceTaxonomyTypeId)
                                    ->where('destination_document_type',$targetTaxonomyTypeId)
                                    ->where('item_association_type',$associationType)
                                    ->where('organization_id',$organizationId)
                                    ->where('is_deleted',0)
                                    ->get()
                                    ->toArray();
                    if(empty($checkPreset)){
                        DB::table('item_association_nodetype')
                            ->where('node_type_id',$nodeTypeId)
                            ->where('organization_id',$organizationId)
                            ->where('is_deleted',0) 
                            ->update(['source_document_type' => $sourceTaxonomyTypeId,'destination_document_type' =>$targetTaxonomyTypeId,'item_association_type'=> $associationType]);   
                    }else{
                        return false;
                    }
                }
                
                DB::table('node_types')
                    ->where('node_type_id',$nodeType_id)
                    ->where('used_for',1)
                    ->where('organization_id',$organizationId)
                    ->where('is_deleted',0)
                    ->update(['title' => $name]); 
                // Get existing preset metadata
                $existingMetadata = DB::table('node_type_metadata')
                    ->where('node_type_id','=',$nodeType_id)->get()->toArray();
                // Get new preset metadata
                $metadataIds = array_column($metadata,'metadata_id');
                $metadataToBeDeleted = array();
				// compare old and new preset metadata
                foreach($existingMetadata as $existkey =>$existValue){
                    if(!in_array($existValue->metadata_id,$metadataIds)){
                        $metadataToBeDeleted[] = $existValue->metadata_id;
                    }
                }

                // Delete all applicable metadata which are removed from preset on updation of preset
                DB::table('item_association_metadata')
                    ->where('node_type_id',$nodeType_id)
                    ->whereIn('metadata_id',$metadataToBeDeleted)
                    ->where(function ($query) {
                        $query->where('metadata_value','=','')->orWhereNull('metadata_value');
                    })
                    ->update(['is_deleted' => 1]);

                DB::table('node_type_metadata')
                  ->where('node_type_id','=',$nodeType_id)->delete();

                  foreach($metadata as $metadataK => $metadataV) {
                    $nodeMetaData[] = [
                        'node_type_id'  =>  $nodeType_id,
                        'metadata_id'   =>  $metadataV['metadata_id'],
                        'sort_order'    =>  $metadataV['sort_order'],
                        'is_mandatory'  =>  $metadataV['is_mandatory'],
                    ];
                }
    
                DB::table('node_type_metadata')->insert($nodeMetaData);

                DB::commit();
                return true;

            }

        }catch(\Exception $ex){
               
            DB::rollback();
            throw $ex;
        }
        
    }
}
