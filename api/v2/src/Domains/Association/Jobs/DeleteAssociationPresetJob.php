<?php
namespace App\Domains\Association\Jobs;
use Lucid\Foundation\Job;
use DB;

class DeleteAssociationPresetJob extends Job
{

    public function __construct($organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType)
    {
        $this->organizationId = $organizationId;
        $this->sourceTaxonomyTypeId = $sourceTaxonomyTypeId;
        $this->targetTaxonomyTypeId = $targetTaxonomyTypeId;
        $this->associationType = $associationType;
    }

    public function handle()
    {
        $is_configused = $this->checkConfigurationUsed($this->organizationId,$this->sourceTaxonomyTypeId,$this->targetTaxonomyTypeId,$this->associationType);
		// Empty condition added to resolve already in use error
        if(empty($is_configused)){
           $response =  $this->deleteConfiguration($this->organizationId,$this->sourceTaxonomyTypeId,$this->targetTaxonomyTypeId,$this->associationType);
        }else{
           $response = false;
        }
       return $response;
    }

    private function checkConfigurationUsed($organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType){
       
        $metadataArr = [];
        $metadatavalues = [];

        $getnodetype = DB::table('item_association_nodetype')
        ->where('source_document_type',$sourceTaxonomyTypeId)
        ->where('destination_document_type',$targetTaxonomyTypeId)
        ->where('item_association_type',$associationType)
        ->where('organization_id',$organizationId)
        ->where('is_deleted',0)
        ->get()
        ->toArray();
        if(!empty($getnodetype)){
            $nodeType_id = $getnodetype[0]->node_type_id;
        
            $metadataArr =  DB::table('node_type_metadata')
            ->where('node_type_id',$nodeType_id)
            ->get()
            ->toArray();
        }
        if(!empty($metadataArr)){
            $metadata = array_column($metadataArr,'metadata_id');
            $metadatavalues = DB::table('item_association_metadata')
                                ->where('node_type_id',$nodeType_id)
                                ->whereIn('metadata_id',$metadata)
                                ->where('is_deleted',0)
                                ->get()
                                ->toArray();
			// Code to ignore itemassociations which are deleted
            $itemAssociationIds = array_column($metadatavalues,'item_association_id');
            $itemAssociationIds = !empty($itemAssociationIds) ? array_unique($itemAssociationIds) : [];
            
            $itemAssociationIdsList = DB::table('item_associations')
                                ->whereIn('item_association_id',$itemAssociationIds)
                                ->where('is_deleted',0)
                                ->get()
                                ->toArray();
            if(empty($itemAssociationIdsList)){
                $metadatavalues = [];
            }
        }
        return $metadatavalues;
    }

    private function deleteConfiguration($organizationId,$sourceTaxonomyTypeId,$targetTaxonomyTypeId,$associationType){

        DB::beginTransaction();

        try{

            $nodetypeId = DB::table('item_association_nodetype')
            ->where('source_document_type',$sourceTaxonomyTypeId)
            ->where('destination_document_type',$targetTaxonomyTypeId)
            ->where('item_association_type',$associationType)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->toArray();
            
            if(!empty($nodetypeId)){
                $nodeType_id = $nodetypeId[0]->node_type_id;
            
                DB::table('node_types')
                    ->where('node_type_id',$nodeType_id)
                    ->where('used_for',1)
                    ->where('organization_id',$organizationId)
                    ->where('is_deleted',0)
                    ->update(['is_deleted' => 1]); 
                
                
                DB::table('item_association_nodetype')
                    ->where('source_document_type',$sourceTaxonomyTypeId)
                    ->where('destination_document_type',$targetTaxonomyTypeId)
                    ->where('item_association_type',$associationType)
                    ->where('organization_id',$organizationId)
                    ->where('is_deleted',0)
                    ->update(['is_deleted' => 1]); 
                
            
                DB::table('node_type_metadata')
                    ->where('node_type_id','=',$nodeType_id)->delete();

            }
                
            }catch(\Exception $ex){
               
                DB::rollback();
                throw $ex;
            }
            
            DB::commit();
            return true;
               
    }

}