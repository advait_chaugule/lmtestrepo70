<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class ValidateNodeTypeIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $name;
    
    public function __construct($nodeTypeId,$organizationId)
    {
        $this->nodeTypeId = $nodeTypeId;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('item_association_nodetype')
                ->where('node_type_id','=',$this->nodeTypeId)
                ->where('organization_id','=',$this->organizationId)
                ->where('is_deleted','=',0)->get();
            
        return $query->first() ? true : false;
    }
}