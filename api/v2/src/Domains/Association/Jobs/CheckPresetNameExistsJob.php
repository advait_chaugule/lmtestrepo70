<?php
namespace App\Domains\Association\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class CheckPresetNameExistsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $name;
    
    public function __construct($name, $organizationId, $nodetypeid = '')
    {
        $this->name = $name;
        $this->organizationId = $organizationId;
        $this->nodetypeid = $nodetypeid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->name;
        $query = DB::table('node_types')
        ->select('title')
        ->where('title','=',$name)
        ->where('is_deleted','=',0)
        ->where('organization_id', $this->organizationId)
        ->where('used_for','=',1);

        #if nodetypeid is not empty that means its update else its create
        if(!empty($this->nodetypeid)) {
            $query = $query->where('node_type_id', '!=', $this->nodetypeid);
        }

        $query = $query->get();
        
        return $query->first() ? true : false;
    }
}