<?php
namespace App\Domains\Association\Tests\Jobs;

use App\Domains\Association\Jobs\DocumentSelfIsChildOfValidationJob;
use Tests\TestCase;

class DocumentSelfIsChildOfValidationJobTest extends TestCase
{
    public function test_document_self_is_child_of_validation_job()
    {
        $this->markTestIncomplete();
    }
}
