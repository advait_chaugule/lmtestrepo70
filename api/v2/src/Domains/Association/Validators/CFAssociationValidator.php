<?php
namespace App\Domains\Association\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CFAssociationValidator extends BaseValidator {

    protected $rules = [
        'item_association_id' => 'required|exists:item_associations,item_association_id',
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}