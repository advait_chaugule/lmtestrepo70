<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class ListRolePermissionJob extends Job
{
    private $roleIdentifier;
    private $roleRepository;

    private $rolePermissionList;
    private $groupedPermission;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set the role identifier
        $this->setRoleIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //Get the Role identifier
        $identifier = $this->getRoleIdentifier();
        //Set the Role Repository handler
        $this->roleRepository = $roleRepository;
        //Fetch Role permissions from DB
        $rolePermissionList = $this->fetchRolePermisionList($identifier);
        $this->setRolePermissionData($rolePermissionList);
        $this->groupRolePermissions();

        //Grouped permission List for a role
        $rolePermissionsGroupedList = $this->getGroupedPermissions();
        return $rolePermissionsGroupedList;
    }

    /**
     * Public Getters and Setters method for Private Attributes
     */
    public function setRoleIdentifier($identifier) {
        $this->roleIdentifier = $identifier;
    }

    public function getRoleIdentifier() {
        return $this->roleIdentifier;
    }

    public function setRolePermissionData($roleData) {
        $this->rolePermissionList = $roleData;
    }

    public function getRolePermissionData() {
        return $this->rolePermissionList;
    }

    public function setGroupedPermissions($groupedPermission) {
        $this->groupedPermission = $groupedPermission;
    }

    public function getGroupedPermissions() {
        return $this->groupedPermission;
    }
    
    private function fetchRolePermisionList(string $identifier) {
        $roleDetail = $this->roleRepository->getRelatedModels($identifier);
        return $roleDetail;        
    }

    //Method to call the Group Helper method
    private function groupRolePermissions() {
        $groupByPermissionsName = [
            "project_permissions"       => $this->helperToReturnParsedOutPermissions(1),
            "taxonomy_permissions"      => $this->helperToReturnParsedOutPermissions(2),
            "comment_permissions"       => $this->helperToReturnParsedOutPermissions(7),
            "metadata_permissions"      => $this->helperToReturnParsedOutPermissions(5),
            "node_type_permissions"     => $this->helperToReturnParsedOutPermissions(6),
            "node_template_permissions" => $this->helperToReturnParsedOutPermissions(8),
            "workflow_permissions"      => $this->helperToReturnParsedOutPermissions(3),
            "role_user_permissions"     => $this->helperToReturnParsedOutPermissions(4),
            "notification_permissions"  => $this->helperToReturnParsedOutPermissions(9),
            "public_review_permissions" => $this->helperToReturnParsedOutPermissions(10),
            "note_permissions"          => $this->helperToReturnParsedOutPermissions(11),
            "pacing_guide_permissions"  => $this->helperToReturnParsedOutPermissions(12)
        ];
        $this->setGroupedPermissions($groupByPermissionsName);
    }

    //Helper method to group the role permission list
    private function helperToReturnParsedOutPermissions($permissionGroupType): array {
        $rolePermissions = $this->getRolePermissionData();
        $permissions = $rolePermissions->permissions;
        $groupedPermissions = $permissions->filter(function ($item, $key) use($permissionGroupType) { 
            if($item->permission_group===$permissionGroupType) { 
                return $item; 
            } 
        });
        $parsedPermissions = [];
        foreach($groupedPermissions as $permission) {
            if($permission->permission_id != 'acee9a40-f0c0-4576-9f5c-0becfc177907'){
                $parsedPermissions[] = [
                    "permission_id" => $permission->permission_id,
                    "parent_permission_id" => $permission->parent_permission_id,
                    "display_name" => $permission->display_name,
                    "display_order" => $permission->order,
                    "internal_name" => $permission->internal_name
                ];
            }
            
        }

        array_multisort(array_column($parsedPermissions, 'display_order'), SORT_ASC, $parsedPermissions);
        return $parsedPermissions;
    }
}
