<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Role\Validators\RoleExistsValidator;

class ValidateRoleByIdJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //set private attribute to input data
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleExistsValidator $validator)
    {
        //Check the existence for the role id
        $validation = $validator->validate($this->identifier);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
