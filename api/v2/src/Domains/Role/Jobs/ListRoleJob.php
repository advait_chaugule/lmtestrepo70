<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class ListRoleJob extends Job
{
    private $requestData;
    private $roleDetail;
    private $roleRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository, Request $request)
    {
        // set role repository handler
        $this->roleRepository = $roleRepository;

        // set pagination and sort filters here when required

        // get all roles
        $roles = $this->roleRepository->getRoleList($request);
        // prepare the response
        return ['roles' => $roles, 'count' => $roles->count()];
    }
}
