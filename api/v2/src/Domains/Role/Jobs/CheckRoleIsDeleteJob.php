<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class CheckRoleIsDeleteJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepo)
    {
        //Check whether Role deleted
        $attributes = ['role_id' => $this->identifier, 'is_deleted' => '1'];
        return $roleRepo->findByAttributes($attributes)->isNotEmpty();
    }
}
