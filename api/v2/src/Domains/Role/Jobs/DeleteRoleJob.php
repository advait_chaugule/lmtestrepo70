<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class DeleteRoleJob extends Job
{
    private $identifier;
    private $roleRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set Role Identifier
        $this->setIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //Set Role Repo handler
        $this->roleRepository = $roleRepository;
        //Get the identifier
        $roleIdentifier = $this->getIdentifier();
        //set the attribute to update
        $attribute = ['is_deleted' => '1'];

        return $this->roleRepository->edit($roleIdentifier, $attribute);
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }
}
