<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class UsageCountRoleJob extends Job
{
    private $userRepository;
    private $roleIdentifierSet;
    private $organizationIdentifier;
    private $usageCount;
    private $countArray;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $roleIdentifierArray, string $organizationIdentifier)
    {
        //Set identifier array from input
        $this->setRoleIdentifierSet($roleIdentifierArray);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        //Set role repository handler
        $this->userRepository = $userRepository;

        $usageCountDetail = $this->fetchUsageCount();
        $this->setUsageCount($usageCountDetail);

        $this->parseUsageCountResponse();

        $countArray = $this->getParsedUsageCount();
        return $countArray;

    }

    // Public Getter and Setter methods
    public function setRoleIdentifierSet(array $identifier) {
        $this->roleIdentifierSet = $identifier;
    }

    public function getRoleIdentifierSet() {
        return $this->roleIdentifierSet;
    }

    public function setOrganizationIdentifier(string $identifier) {
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setUsageCount($usageCountDetail) {
        $this->usageCount = $usageCountDetail;
    }

    public function getUsageCount() {
        return $this->usageCount;
    }

    public function setParsedUsageCount($countArray) {
        $this->countArray = $countArray;
    }

    public function getParsedUsageCount() {
        return $this->countArray;
    }

    private function fetchUsageCount(){
        $roleIdentifierSet = $this->getRoleIdentifierSet();
        $organizationIdentifier = $this->getOrganizationIdentifier();

        $model = 'users_organization';
        $mapColumn = 'role_id';

        $queryParam = ['users_organization.organization_id' => $organizationIdentifier, 'users_organization.is_deleted' => '0'];
        $fieldsToCount = 'user_id';
        $attributesToGroupBy = ['role_id'];

        $usageCount = $this->userRepository->usageCountOnRoles($model, $mapColumn, $roleIdentifierSet, $fieldsToCount, $queryParam);
        return $usageCount;
    }

    private function parseUsageCountResponse(){
        $count = [];
        $usageCount = $this->getUsageCount();

        foreach($usageCount as $key => $countArray){
            $count[$key] = !empty($countArray[0]->usage_count) ? $countArray[0]->usage_count : 0 ; 
        }

        $countArray = $this->setParsedUsageCount($count);

        return $countArray;
    }
}
