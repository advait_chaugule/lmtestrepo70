<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class ExistingRoleInWorkflowJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $roleId = $this->input["role_id"];
        $query= DB::table('workflow_stage_role')->select('workflow_stage_role_id')->where([['role_id', '=', $roleId]]);
        if($query->count() > 0)    
        {
           return "You cannot delete this role as it is currently used in workflow";
        }
        else
        {
            return true;
        }
        
    }
}
