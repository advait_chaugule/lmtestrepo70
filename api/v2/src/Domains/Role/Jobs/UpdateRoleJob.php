<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use  App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class UpdateRoleJob extends Job
{
    use DateHelpersTrait;

    private $identifier;
    private $roleRepository;
    private $requestData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($identifier, $data)
    {
        //Set identifier
        $this->setIdentifier($identifier);
        //Set request data
        $this->setRequestData($data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //Set Role repository handle
        $this->roleRepository = $roleRepository;
        //Get role identifier
        $roleIdentifier = $this->getIdentifier();
        //Get the data to be updated
        $dataToUpdate = $this->getRequestData();
        $updateRequest = $this->roleRepository->edit($roleIdentifier, $dataToUpdate);
        
        $responseData = $this->parsedResponseData($updateRequest);
        return $responseData;

    }

    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setRequestData($data){
        $this->requestData = $data;
    }

    public function getRequestData() {
        return $this->requestData;
    }

    private function parsedResponseData($data){
        $roleEntity = [
            'role_id' => $data['role_id'],
            'role_name' => $data['name'],
            'role_description' => $data['description'],
            'is_active' => $data['is_active'],
            'last_updated_at' => $this->formatDateTimeToISO8601($data['updated_at'])
        ];

        return $roleEntity;
    }
}
