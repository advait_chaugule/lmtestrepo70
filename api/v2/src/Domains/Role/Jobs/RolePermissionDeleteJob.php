<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class RolePermissionDeleteJob extends Job
{
    private $identifier;
    private $roleRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the identifier
        $this->setIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //Set the role repos handler
        $this->roleRepository = $roleRepository;
        $roleIdentifier = $this->getIdentifier();

        $deletedPermissions = $this->roleRepository->deleteRolePermission($roleIdentifier);
    }

    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }
}
