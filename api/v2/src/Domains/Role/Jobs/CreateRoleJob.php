<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class CreateRoleJob extends Job
{
    private $roleRepository;
    private $roleDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //assign private attribute to input data
        $this->roleDetail = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //set roleRepository variable
        $this->roleRepository = $roleRepository;
        $savedRoleData = $this->roleRepository->fillAndSave($this->roleDetail);
        return $savedRoleData;
    }
}
