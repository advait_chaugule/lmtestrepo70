<?php
namespace App\Domains\Role\Jobs;

use App\Domains\Role\Validators\RoleExistsValidator;
use Lucid\Foundation\Job;

class ValidateRoleIdUniqueTitleJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);

        if ($validation === true) {
            # Check if metadataset title is unique and not deleted, as deleted ones are allowed to be created again and not same id
            if ($validator->validateIsDeletedNotCurrentId($this->input)) {
                return $validation;
            } else {
                return [
                    'Role with same name already exists.'
                ];
            }
        } else {
            return $validation->errors()->all();
        }
    }
}
