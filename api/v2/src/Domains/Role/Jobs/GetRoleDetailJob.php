<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class GetRoleDetailJob extends Job
{
    private $identifier;
    private $roleRepository;
    private $roleDetail;
    private $roleEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the private attribute to Role Identifier
        $this->setIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //Set Role Repo handler
        $this->roleRepository = $roleRepository;
        //get Role detail from database
        $roleDetail = $this->getRoleDetailFromModel();

        if(!empty($roleDetail->role_id)){
            // set role details globally
            $this->setRoleDetail($roleDetail);
            // transform to and set the Role Structure
            $this->parseResponse();
            // finally return the CASE standard Item structure
            return $this->getRoleEntity();
        }
        else{
            return false;
        }
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }

    public function setRoleDetail($data){
        $this->roleDetail = $data;
    }

    public function getRoleDetail(){
        return $this->roleDetail;
    }

    public function setRoleEntity($roleEntity) {
        $this->roleEntity = $roleEntity;
    }

    public function getRoleEntity() {
        return $this->roleEntity;
    }

    private function getRoleDetailFromModel() {
        $identifier = $this->getIdentifier();

        $roleDetail = $this->roleRepository->getRelatedModels($identifier);

        return $roleDetail;
    }

    private function parseResponse(){
        $roleDetail = $this->getRoleDetail();

        $roleEntity = [
            'name' => !empty($roleDetail->name)? $roleDetail->name : "",
            'description' => !empty($roleDetail->description) ? $roleDetail->description: "",
        ];

        $this->setRoleEntity($roleEntity);
    }
}
