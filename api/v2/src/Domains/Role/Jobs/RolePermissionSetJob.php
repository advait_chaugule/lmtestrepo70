<?php
namespace App\Domains\Role\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class RolePermissionSetJob extends Job
{
    private $roleRepository;
    private $permissionData;
    private $roleIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($identifier, $input)
    {
        //set private attribute to input data
        $this->permissionData = $input;
        $this->setIdentifier($identifier['role_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        //set roleIdentifier
        $identifier = $this->getIdentifier();
        $this->roleRepository = $roleRepository;

        $setPermissionForRole = $this->roleRepository->associatePermissionWithRole($identifier, $this->permissionData);
        $responseData = $this->parseRolePermission($setPermissionForRole);

        return $responseData;
    }

    //Public Setter and Getter methods for Identifier
    public function setIdentifier($identifier){
        $this->roleIdentifier = $identifier;
    }

    public function getIdentifier() : string {
        return $this->roleIdentifier;
    }

    private function parseRolePermission($data) {
        $permissionSet = [];

        foreach($data->permissions as $permission)
        {
            $permissionSet[] = ['permission_id' => $permission['permission_id']];
        }

        $roleEntity = [
            'role_id' => $data['role_id'],
            'role_permission' => $permissionSet,
        ];

        return $roleEntity;
    }
}
