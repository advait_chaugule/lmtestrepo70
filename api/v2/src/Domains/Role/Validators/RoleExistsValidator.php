<?php
namespace App\Domains\Role\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;
use Illuminate\Support\Facades\DB;
class RoleExistsValidator extends BaseValidator {

    protected $rules = [
        'role_id' => 'required|exists:roles,role_id'
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
    ];
    
    # Check if metadataset title is unique and not deleted, as deleted ones are allowed to be created again, and not current one
    public function validateIsDeletedNotCurrentId($input)
    {
        $row_count = DB::table('roles')
                        ->where('is_deleted', 0)
                        ->where('name', $input['name'])
                        ->where('role_id', '!=', $input['role_id'])
                        ->where('organization_id', $input['organization_id'])
                        ->get()
                        ->count();

        if ($row_count) {
            return false;
        }
        return true;
    }
}