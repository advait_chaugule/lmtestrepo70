<?php
namespace App\Domains\Role\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class RoleValidator extends BaseValidator {

    protected $rules = [
        'organization_id' => 'required|exists:organizations,organization_id',
        'name' => 'required|string|max:50',
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'string' => ':attribute should be of type string.',
        'max:50' => ':attribute should be of maximum 50 characters.',
     ];
    
}