<?php
namespace App\Domains\Role\Tests\Jobs;

use App\Domains\Role\Jobs\RolePermissionDeleteJob;
use Tests\TestCase;

class RolePermissionDeleteJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new RolePermissionDeleteJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
