<?php
namespace App\Domains\Role\Tests\Jobs;

use App\Domains\Role\Jobs\UpdateRoleJob;
use Tests\TestCase;

class UpdateRoleJobTest extends TestCase
{
    public function test_Identifier()
    {
        $data['role_id'] = 'f30b9d7a-edf9-4f07-b510-eca225d6e9d8';
        $assets = new UpdateRoleJob($data,'f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_RequestData()
    {
        $data['role_id'] = 'f30b9d7a-edf9-4f07-b510-eca225d6e9d8';
        $assets = new UpdateRoleJob($data,'f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRequestData($value);
        $this->assertEquals($value,$assets->getRequestData());
    }
}
