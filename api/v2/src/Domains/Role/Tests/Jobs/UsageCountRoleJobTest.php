<?php
namespace App\Domains\Role\Tests\Jobs;

use App\Domains\Role\Jobs\UsageCountRoleJob;
use Tests\TestCase;

class UsageCountRoleJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $data[] = 'f30b9d7a-edf9-4f07-b510-eca225d6e9d8';
        $assets = new UsageCountRoleJob($data,'f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_UsageCount()
    {
        $data[] = 'f30b9d7a-edf9-4f07-b510-eca225d6e9d8';
        $assets = new UsageCountRoleJob($data,'f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUsageCount($value);
        $this->assertEquals($value,$assets->getUsageCount());
    }

    public function test_ParsedUsageCount()
    {
        $data[] = 'f30b9d7a-edf9-4f07-b510-eca225d6e9d8';
        $assets = new UsageCountRoleJob($data,'f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setParsedUsageCount($value);
        $this->assertEquals($value,$assets->getParsedUsageCount());
    }
}
