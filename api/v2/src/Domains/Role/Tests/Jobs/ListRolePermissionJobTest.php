<?php
namespace App\Domains\Role\Tests\Jobs;

use App\Domains\Role\Jobs\ListRolePermissionJob;
use Tests\TestCase;

class ListRolePermissionJobTest extends TestCase
{
    public function test_RoleIdentifier()
    {
        $assets = new ListRolePermissionJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleIdentifier($value);
        $this->assertEquals($value,$assets->getRoleIdentifier());
    }

    public function test_RolePermissionData()
    {
        $assets = new ListRolePermissionJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRolePermissionData($value);
        $this->assertEquals($value,$assets->getRolePermissionData());
    }

    public function test_GroupedPermissions()
    {
        $assets = new ListRolePermissionJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setGroupedPermissions($value);
        $this->assertEquals($value,$assets->getGroupedPermissions());
    }
}
