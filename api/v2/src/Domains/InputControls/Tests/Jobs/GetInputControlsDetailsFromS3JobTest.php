<?php
namespace App\Domains\InputControls\Tests\Jobs;

use App\Domains\InputControls\Jobs\GetInputControlsDetailsFromS3Job;
use Tests\TestCase;

class GetInputControlsDetailsFromS3JobTest extends TestCase
{
    public function test_BucketName()
    {
        $assets = new GetInputControlsDetailsFromS3Job('c4424f55-8900-446c-9c41-2f2c25d58c08','989d3047-c369-4b09-b4f1-dee410aa84be','56b543bf-2839-4b97-a83e-e291c89f2374','ac5349f7-3771-4501-b7ea-28292526d7a0');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setBucketName($value);
        $this->assertEquals($value,$assets->getBucketName());
    }
}
