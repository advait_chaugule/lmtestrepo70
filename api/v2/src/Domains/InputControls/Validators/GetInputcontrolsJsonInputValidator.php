<?php
namespace App\Domains\Inputcontrols\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class GetInputcontrolsJsonInputValidator extends BaseValidator {

    protected $rules = [
        's3_bucket_name' => 'required|in:acmthost,acmtqa,acmtstag,scdprod'
    ];

     protected $messages = [

     ];
    
}