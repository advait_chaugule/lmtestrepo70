<?php
namespace App\Domains\InputControls\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\FileHelper;
use Illuminate\Support\Facades\Storage;

use App\Services\Api\Traits\ArrayHelper;

class GetInputControlsDetailsFromS3Job extends Job
{

    use ArrayHelper;

    private $S3Storage;
    private $bucketName;
    private $inputControlsFolderName;
    private $inputControlsFileName;
    private $inputControlsDetails;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $bucketName)
    {
        $this->setBucketName($bucketName);
        $this->setS3BucketConfiguration();
        $this->setS3FileStorage();
        $this->setInputControlsFolderName();
        $this->setInputControlsFileName();
    }

    public function setBucketName(string $data) {
        $this->bucketName = $data;
    }

    public function getBucketName(): string {
        return $this->bucketName;
    }

    public function setS3BucketConfiguration() {
        config(['filesystems.disks.s3.bucket' => $this->getBucketName()]);
    }

    public function setS3FileStorage() {
        $this->S3Storage = Storage::disk('s3');
    }

    public function getS3FileStorage() {
        return $this->S3Storage;
    }

    public function setInputControlsFolderName() {
        $this->inputControlsFolderName = config("inputControls.S3_FOLDER_NAME");
    }

    public function getInputControlsFolderName(): string {
        return $this->inputControlsFolderName;
    }

    public function setInputControlsFileName() {
        $this->inputControlsFileName = config("inputControls.FILE_NAME");
    }

    public function getInputControlsFileName(): string {
        return $this->inputControlsFileName;
    }

    public function setInputControlsDetails(array $data) {
        $this->inputControlsDetails = $data;
    }

    public function getInputControlsDetails(): array {
        return $this->inputControlsDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->readAndSetInputControlsDetails();
        
        return $this->getInputControlsDetails();
    }

    private function readAndSetInputControlsDetails() {
        $inputControlsDetails = [];
        $s3Disk = $this->getS3FileStorage();
        $inputControlsFolderName = $this->getInputControlsFolderName();
        $inputControlsFileName = $this->getInputControlsFileName();
        $pathToInputControlsJson = $inputControlsFolderName . "/" . $inputControlsFileName;
        if($s3Disk->exists($pathToInputControlsJson)===true) {
            $inputControlsDetails = $this->convertJsonStringToArray($s3Disk->get($pathToInputControlsJson));
        }
        $this->setInputControlsDetails($inputControlsDetails);
    }
}
