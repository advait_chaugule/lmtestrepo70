<?php
namespace App\Domains\InputControls\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Inputcontrols\Validators\GetInputcontrolsJsonInputValidator as Validator;

class ValidateGetInputControlsJsonInputJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
