<?php
namespace App\Domains\WorkflowStage\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ValidateWorkflowStageRoleId extends BaseValidator {

    protected $rules = [
        'workflow_stage_role_id'=>'required|exists:workflow_stage_role'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}