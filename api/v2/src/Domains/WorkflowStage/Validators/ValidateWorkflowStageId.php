<?php
namespace App\Domains\WorkflowStage\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ValidateWorkflowStageId extends BaseValidator {

    protected $rules = [
        'workflow_id'=>'required|exists:workflows'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}