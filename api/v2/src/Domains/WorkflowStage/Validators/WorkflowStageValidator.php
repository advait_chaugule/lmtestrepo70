<?php
namespace App\Domains\WorkflowStage\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class WorkflowStageValidator extends BaseValidator {

    protected $rules = [
        'workflow_stage_id'=>'required|exists:workflow_stage',
        'stage_name'=>'required'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}