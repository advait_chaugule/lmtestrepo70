<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Models\WorkflowStage;

class ValidateWorkflowStageJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $requestObject;
    protected $workflowStageId;
    protected $dataToValidate;
    protected $errorMessagesAvailable;
    protected $errorMessagesToSet;

    public function __construct(array $requestData)
    {
        //
        $this->dataToValidate = $requestData;
        $this->errorMessagesAvailable = [
            "required" => [
                "workflow_stage_id" => "Workflow Stage Id is mandatory.",
            ],
            "exists" => [
                "workflow_stage_id" => "Workflow Stage Id is not Exist."
            ]
        ];
        $this->errorMessagesToSet = [];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        // set the injected laravel request object
        $this->requestObject = $request;
       

        $idRequiredStatus = $this->getRequiredStatusForAttribute("workflow_stage_id");
        $this->setErrorMessageOfValidationTypeForAttribute($idRequiredStatus, "required", "workflow_stage_id");
       

        $idExistsStatus =  $idRequiredStatus ? 
                                    $this->getIdExistsStatusAndSetErrorMessageAccordingly() : 
                                    true;
                                    
        $overallStatus = $idRequiredStatus && $idExistsStatus;


        
        $jobResponse = $overallStatus===true ? $overallStatus : implode(",", $this->errorMessagesToSet);

        return $jobResponse;
        //
    }

    private function getIdExistsStatusAndSetErrorMessageAccordingly(): bool {
        $workflowStageId = $this->dataToValidate["workflow_stage_id"];
      

        $conditionalClause = [ [ "workflow_stage_id", "=", $workflowStageId] ];
       
        $recordCount = WorkflowStage::where($conditionalClause)->count();
        $status = $recordCount > 0 ? true : false;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "workflow_stage_id");
        return $status;
    }

    private function getRequiredStatusForAttribute(string $attribute): bool {
        return !empty($this->dataToValidate[$attribute]);
    }

    
    private function setErrorMessageOfValidationTypeForAttribute(bool $validationStatus, string $validationType, string $attribute) {
        if($validationStatus===false) {
            $messageToPush = !empty($this->errorMessagesAvailable[$validationType][$attribute]) ?
                             $this->errorMessagesAvailable[$validationType][$attribute] : 
                             "";
            if(!empty($messageToPush)) {
                $this->errorMessagesToSet[] = $messageToPush;
            }
        }
    }

}
