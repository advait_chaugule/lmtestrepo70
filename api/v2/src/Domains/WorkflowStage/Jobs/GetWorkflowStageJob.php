<?php
namespace App\Domains\WorkflowStage\Jobs;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;

use Lucid\Foundation\Job;

class GetWorkflowStageJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowStageRepositoryInterface $workflowStageRepo)
    {
        
        $workflowId = $this->data['workflow_id'];        
        return $workflowStageRepo->getWorkflowStageList($workflowId);
    }
}
