<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;


class DeleteStageRoleJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowStageRepositoryInterface $workflowStageRepo)
    {
       
        $workflow_stage_role_id = $this->input['workflow_stage_role_id'];
        return $workflowStageRepo->removeWorkflowStageRole($workflow_stage_role_id);
    }
}
