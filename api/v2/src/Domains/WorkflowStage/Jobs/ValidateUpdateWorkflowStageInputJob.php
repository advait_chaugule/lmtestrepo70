<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;

use App\Domains\WorkflowStage\Validators\WorkflowStageValidator;

class ValidateUpdateWorkflowStageInputJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle(WorkflowStageValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}

