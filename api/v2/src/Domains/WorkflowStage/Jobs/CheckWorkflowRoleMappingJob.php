<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;

class CheckWorkflowRoleMappingJob extends Job
{
    private $roleIdentifier;
    private $workflowStageIdentifier;
    private $workflowStageRepo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //Set private input
        $this->roleIdentifier = $input['role_id'];
        $this->workflowStageIdentifier = $input['workflow_stage_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowStageRepositoryInterface $workflowStageRepo)
    {
        //Set workflowStageRepo handler
        $this->workflowStageRepo = $workflowStageRepo;
    
        $workflowStageIdentifier = $this->workflowStageIdentifier;
        $roleIdentifier = $this->roleIdentifier;

        $returnCollection = false;
        $fieldsToReturn = ['workflow_stage_role_id'];
        $keyValuePairs  = ['role_id' => $roleIdentifier, 'workflow_stage_id' => $workflowStageIdentifier];
        
        return $this->workflowStageRepo->findByWorkflowStageAndRoleId($returnCollection, $fieldsToReturn, $keyValuePairs);
    }
}
