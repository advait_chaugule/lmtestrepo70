<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\WorkflowStage;
use Illuminate\Support\Facades\DB;

class CheckRoleIsExistInWorkflowJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $roleId = $this->input["role_id"];
        $flag = 0;

        $query= DB::table('workflow_stage_role')->select('workflow_stage_role_id')->where('role_id', $roleId);
                if($query->count() > 0)    
                {
                    $flag = 1;
                }

                if($flag > 0)
                {
                    return "Role is used in Workflow(s).";
                }
                else
                {
                    return true;
                }

    }
}
