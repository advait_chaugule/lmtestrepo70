<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;

class UpdateWorkflowStageJob extends Job
{
    private $input;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowStageRepositoryInterface $workflowStageRepo)
    {
        
        $workflowStageId = $this->input['workflow_stage_id'];
        
        $input['stage_name']            =   $this->input['stage_name'];
        $input['stage_description']     =   $this->input['stage_description'];
        
        
        $workflowStage              =   $workflowStageRepo->edit($workflowStageId, $input)->toArray();
    
        if(isset($this->input['role_id'])) {
            $data['role_id']            =   $this->input['role_id'];
            $workflow_stage_role_id     =   $workflowStageRepo->deleteandadd($workflowStageId, $data);
        }

        $workflowStage['workflow_stage_role'] = $workflowStageRepo->fetchRolesForAStage($workflowStage['workflow_stage_id']);
        return $workflowStage;
    }
}
