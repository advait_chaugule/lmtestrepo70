<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\WorkflowStageRepositoryInterface;


class CreateWorkflowStageJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WorkflowStageRepositoryInterface $workflowStageRepo)
    {
        //
        //dd($this->input);
        return $workflowStageRepo->fillAndSave($this->input);
    }
}
