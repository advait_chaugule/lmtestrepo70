<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\WorkflowStage;
use Illuminate\Support\Facades\DB;

class ExistingWorkflowStageRoleJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $workflowId = $this->input["workflow_id"];
        $tableName = "projects";
        $queryToExecute = DB::table($tableName)->select('project_id')->where([['workflow_id', '=', $workflowId],['is_deleted', '=', 0]]);
        $workflowroleresult = $queryToExecute->get()->toArray();
       if(count($workflowroleresult) > 0)
        {
            return "This stage cannot be deleted as the roles assigned to this stage are being utilised in active projects";
        }
        else
        {
            return true;
        }
        
    }
}
