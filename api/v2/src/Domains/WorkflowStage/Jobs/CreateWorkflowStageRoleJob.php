<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class CreateWorkflowStageRoleJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        
        $workflowStageRoleId = $this->input["workflow_stage_role_id"];
        $workflowStageId = $this->input["workflow_stage_id"];
        $roleId = $this->input["role_id"];
        $data[] = [
            'workflow_stage_role_id'  =>  $workflowStageRoleId,
            'workflow_stage_id'   =>  $workflowStageId,
            'role_id'    =>  $roleId,
        ];
        $return[] = [
            'workflow_stage_role_id' => $workflowStageRoleId
        ];  
DB::table('workflow_stage_role')->insert($data);
return $return;
    }
}
