<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\WorkflowStage;
use Illuminate\Support\Facades\DB;

class ExistingStageRoleJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $workflowId = $this->input["workflow_id"];
        $flag = 0;

        $query= DB::table('projects')->select('project_id')->where([['workflow_id', '=', $workflowId],['is_deleted', '=', 0]]);
                if($query->count() > 0)    
                {
                   return "You cannot delete this role as it is currently used in an active project";
                }
                else
                {
                    return true;
                }
               /* $query= DB::table('project_access_requests')->select('project_id')->where('workflow_stage_role_ids', 'like', '%' . $workflowStageRoleId . '%');
                if($query->count() > 0)    
                {
                    $flag = 1;
                }*/
               

    }
}
