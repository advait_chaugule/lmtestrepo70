<?php
namespace App\Domains\WorkflowStage\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class DeleteWorkflowStageJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $workflowStageId = $this->input["workflow_stage_id"];
        
        
     
        $tableName = "workflow_stage_role";
        DB::table($tableName)->where('workflow_stage_id', '=', $workflowStageId)->delete();
        DB::table('workflow_stage')->where('workflow_stage_id', '=', $workflowStageId)->delete();
        $workflowStagArr = explode("||", $workflowStageId);
        $workflowId = $workflowStagArr[0];
        
        $queryToExecute = DB::table('workflow_stage')->select('workflow_stage_id','order')->where('workflow_id', $workflowId);
        $workflowrStageresult = $queryToExecute->orderBy('order', 'ASC')->get()->toArray();
        foreach($workflowrStageresult as $key => $workflowrStage)
        {
             $workflowstageId = $workflowrStage->workflow_stage_id;
             $neworder = $key + 1;

            $update = \DB::table('workflow_stage') ->where('workflow_stage_id', $workflowstageId) ->limit(1) ->update( [ 'order' => $neworder ]);
             
        }

    }
}
