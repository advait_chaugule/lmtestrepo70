<?php
namespace App\Domains\Walkthrough\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Walkthrough\Validators\GetWalkthroughInputValidator as Validator;

class ValidateGetWalkthroughJsonInputJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
