<?php
namespace App\Domains\Walkthrough\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\FileHelper;
use Illuminate\Support\Facades\Storage;

use App\Services\Api\Traits\ArrayHelper;

class GetWalkthroughDetailsFromS3Job extends Job
{

    use ArrayHelper;

    private $S3Storage;
    private $bucketName;
    private $walkthroughFolderName;
    private $walkthroughFileName;
    private $walkthroughDetails;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $bucketName)
    {
        $this->setBucketName($bucketName);
        $this->setS3BucketConfiguration();
        $this->setS3FileStorage();
        $this->setWalkthroughFolderName();
        $this->setWalkthroughFileName();
    }
    
    public function setBucketName(string $data) {
        $this->bucketName = $data;
    }

    public function getBucketName(): string {
        return $this->bucketName;
    }

    public function setS3BucketConfiguration() {
        config(['filesystems.disks.s3.bucket' => $this->getBucketName()]);
    }

    public function setS3FileStorage() {
        $this->S3Storage = Storage::disk('s3');
    }

    public function getS3FileStorage() {
        return $this->S3Storage;
    }

    public function setWalkthroughFolderName() {
        $this->walkthroughFolderName = config("walkthrough.S3_FOLDER_NAME");
    }

    public function getWalkthroughFolderName(): string {
        return $this->walkthroughFolderName;
    }

    public function setWalkthroughFileName() {
        $this->walkthroughFileName = config("walkthrough.FILE_NAME");
    }

    public function getWalkthroughFileName(): string {
        return $this->walkthroughFileName;
    }

    public function setWalkthroughDetails(array $data) {
        $this->walkthroughDetails = $data;
    }

    public function getWalkthroughDetails(): array {
        return $this->walkthroughDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->readAndSetWalkthroughDetails();
        
        return $this->getWalkthroughDetails();
    }

    private function readAndSetWalkthroughDetails() {
        $walkthroughDetails = [];
        $s3Disk = $this->getS3FileStorage();
        $walkthroughFolderName = $this->getWalkthroughFolderName();
        $walkthroughFileName = $this->getWalkthroughFileName();
        $pathToWalkthroughJson = $walkthroughFolderName . "/" . $walkthroughFileName;
        if($s3Disk->exists($pathToWalkthroughJson)===true) {
            $walkthroughDetails = $this->convertJsonStringToArray($s3Disk->get($pathToWalkthroughJson));
        }
        $this->setWalkthroughDetails($walkthroughDetails);
    }
}
