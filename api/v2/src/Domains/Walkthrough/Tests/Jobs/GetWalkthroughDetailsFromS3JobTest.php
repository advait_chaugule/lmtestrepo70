<?php
namespace App\Domains\Walkthrough\Tests\Jobs;

use App\Domains\Walkthrough\Jobs\GetWalkthroughDetailsFromS3Job;
use Tests\TestCase;

class GetWalkthroughDetailsFromS3JobTest extends TestCase
{
    public function test_BucketName()
    {
        $assets = new GetWalkthroughDetailsFromS3Job('a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setBucketName($value);
        $this->assertEquals($value,$assets->getBucketName());
    }
}
