<?php
namespace  App\Domains\Import\Events;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SummaryMultipleCsvEvent implements ShouldQueue
{
    use InteractsWithQueue, Queueable;
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $summaryEventData;
    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($summaryEventData) {
        $this->summaryEventData = $summaryEventData;
    }

    public function handle() {}
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    // public function broadcastOn(){
    //     return new PrivateChannel('channel-name');
    // }

}

