<?php
namespace  App\Domains\Import\Events;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class ImportEvent implements ShouldQueue
{
    use InteractsWithQueue, Queueable;
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $importEventData;
    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($importEventData){
        $this->importEventData = $importEventData;
    }

    public function handle() {}
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }

}

