<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\Api\Traits\ArrayHelper;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Import\Events\ImportMultipleCsvEvent;
use App\Domains\Csv\Jobs\UpdateTaxonomyWithDataFromCsvJob;
use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use App\Domains\Notifications\Jobs\EmailForBatchSummaryJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Services\Api\Traits\UuidHelperTrait;

class ImportMultipleCsvJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */

    use InteractsWithQueue, DispatchesJobs, ArrayHelper,UuidHelperTrait;

    public function __construct(array $input)
    {
        //set private attribute to input data
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $batchId = $this->identifier['batchId'];
            $requestingUserDetails = $this->identifier['requestingUserDetails'];
            $this->userId = $requestingUserDetails['user_id'];
            $organizationId = $requestingUserDetails['organization_id'];
            $requestData = $this->identifier['request'];
            $request = new Request();
            $request->merge($requestData);
            $metadataListForATenant = $this->identifier['metadataListForATenant'];
            $domainName = $this->identifier['domain_name'];
            $requestUrl = $this->identifier['requestUrl'];
            $importJobQuery = DB::table('import_job')
                                ->where('batch_id', $batchId)
                                ->where('process_type', 2)
                                ->where('status', 6)
                                ->where('is_deleted', 0);
            $importjobs = $importJobQuery->get();        
            // dump('batches:',$batches);
            $multipleCsvData = [];
            $packagetData = [];
            $packageDataArr = [];
            foreach($importjobs as $job) {
                if(!empty($job->processed_csv_s3_file_path)) {
                    $jsonContent = Storage::disk('s3')->get($job->processed_csv_s3_file_path);
                    $multipleCsvData[$job->import_job_id] = json_decode($jsonContent, true);
                    $importTaxonomySettings = json_decode($job->import_taxonomy_settings, true);
                    $packageDataArr[$job->import_job_id]['package_exists'] = $importTaxonomySettings['package_exists'];
                    $packageDataArr[$job->import_job_id]['import_type'] = $importTaxonomySettings['import_type'];

                }
            }
          //  dd($packageDataArr,$multipleCsvData);die("stop");
            $resultArr = [];
            if(count($multipleCsvData) > 0)
            {
               $importJobQuery->update(['status' => 2,'updated_at'=>now()]);
                foreach($multipleCsvData as $jobId => $csvDataHeaderRow) {
                    $importType = $packageDataArr[$jobId]['import_type'];
                    $packageExists = $packageDataArr[$jobId]['package_exists'];
    
     
                    $csvDataHeaderRow = $this->trim_array_spaces($csvDataHeaderRow);
                    $csvContentArray  =  $this->removeBlankArray($csvDataHeaderRow['row']);
                    $updateCsvContentToArray  =  $csvDataHeaderRow['row'];
                    $csvContentArray = $this->array_change_key_case_recursive($csvContentArray,CASE_LOWER);
                    $csvDataHeaderRow['column'] = array_map('strtolower',$csvDataHeaderRow['column']);
                   // dd('multipleCsvData:',$updateCsvContentToArray, $organizationId, $requestingUserDetails,  $importType, $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists,$requestUrl);
                    $returnImportDetail = $this->dispatch(new UpdateTaxonomyWithDataFromCsvJob($updateCsvContentToArray, $organizationId, $requestingUserDetails,  $importType, $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists,$requestUrl));
                }
                //die("stop");
                DB::table('import_job')
                ->where('batch_id', $batchId)
                ->where('process_type', 2)
                ->where('is_deleted', 0)
                            ->update(['status'=>3,'updated_at'=>now()]);


                   # In-App Notification
                $activityLog = [
                    "notification_id"       => $this->createUniversalUniqueIdentifier(),
                    "description"           => 'The import for batch is completed. Please click here to check',
                    "target_type"           => config('event_activity')["Notifications"]["AFTER_IMPORT_CSV_SUMMARY"],
                    "target_id"             => $batchId,
                    "user_id"               => $this->userId,
                    "organization_id"       => $organizationId,
                    "is_deleted"            => 0,
                    "created_by"            => $this->userId, // logged in user Id
                    "updated_by"            => $this->userId,
                    "target_context"        => json_encode(['batch_id'=>$batchId, 'user_id'=>$this->userId, 'organization_id'=>$organizationId], JSON_UNESCAPED_SLASHES),
                    "read_status"           => "0", //0=unread 1= read
                    "notification_category" => config('event_activity')["Notification_category"]["AFTER_IMPORT_CSV_SUMMARY"]
                    ];

                $this->dispatch(new CreateNotificationJob($activityLog));

                //Notification via Email for Summary of Batch
                $getUserDetails = $this->getUserDetails($this->userId);
                $user_email = $getUserDetails[0]->email;
                $firstName = $getUserDetails[0]->first_name;
                $lastName = $getUserDetails[0]->last_name;
                $currentOrgId= $getUserDetails[0]->current_organization_id;
                $userName = $firstName . ' ' . $lastName;
                $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($this->userId,$organizationId));
                $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                $organizationName = $organizationData['organization_name'];
                $organizationCode = $organizationData['organization_code'];
                $email_body = 'The import for batch is completed.';    
                $emailData1 = [
                    'subject'=>'The import for batch is completed',
                     'email'    => $user_email,
                    'body'     => ['email_body'=>$email_body,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'batch_id'=>$batchId],
                    'domain_name'=>$domainName
                 ]; 

                 $this->dispatch(new EmailForBatchSummaryJob($emailData1));          
            }
            
           
        }
        catch(\Exception $exception) {
            dd('Exception in ImportMultipleCsvEventListeners', $exception->getMessage());
            $this->logErrors($exception);
        }
    }

    public function getUserDetails($userId)
    {
        $query = $user = DB::table('users')->where('user_id',$userId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}
