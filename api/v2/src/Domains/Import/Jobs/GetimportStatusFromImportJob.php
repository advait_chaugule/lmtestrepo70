<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ImportJobRepositoryInterface;

class GetimportStatusFromImportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImportJobRepositoryInterface $importJobRepository)
    {

        $returnCollection = false;
        $keyValuePairs = ['import_job_id' => $this->input['import_job_id']];

        $fieldsToReturn = ['import_job_id', 
        'results',
        'errors',
        'user_id', 
        'process_type',  
        'status',
        ];
        $importStatusDetails = $importJobRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs);

        if($importStatusDetails->status == 1)
        {
            $importStatusMessage = "Not Yet Started";
        }
        else if($importStatusDetails->status == 2)
        {
            $importStatusMessage = "WIP";
        }
        else if($importStatusDetails->status == 3)
        {
            $importStatusMessage = "Complete";
        }
        else if($importStatusDetails->status == 4)
        {
            $importStatusMessage = "Error";
        }
        $resultArr = [
        'import_job_id' => $importStatusDetails->import_job_id, 
        'results' => json_decode($importStatusDetails->results),
        'errors' => json_decode($importStatusDetails->errors),
        'user_id' => $importStatusDetails->user_id, 
        'process_type' => $importStatusDetails->process_type, 
        'status' => $importStatusDetails->status,
        'status_message' => $importStatusMessage
        ];
        return $resultArr;
    }
}
