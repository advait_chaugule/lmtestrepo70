<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ImportJobRepositoryInterface;

class CreateImportProcessJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImportJobRepositoryInterface $importJobRepository)
    {
        return $importJobRepository->fillAndSave($this->input);
    }
}
