<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class ValidateUserIdOrBatchIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $identifier;

    public function __construct(string $identifier, string $type)
    {
        $this->identifier = $identifier;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->type == 'user_id' && DB::table('users')->where('user_id', $this->identifier)->get()->count()) {
            return 'user_id';
        } else if($this->type == 'batch_id' && DB::table('import_job')->where('batch_id', $this->identifier)->get()->count()){
            return 'batch_id';
        } else {
            return 'not_found';
        }
    }
}
