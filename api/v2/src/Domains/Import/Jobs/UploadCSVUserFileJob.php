<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\FileHelper;


class UploadCSVUserFileJob extends Job
{
    use ArrayHelper, StringHelper, FileHelper;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($csvFile, $csvFileName)
    {
        $this->csvFile = $csvFile;
        $this->csvFileName = $csvFileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileContent = $this->getContent($this->csvFile);
        $s3Bucket = config("event_activity")["Import_Folder"];
        $s3Destination = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/{$s3Bucket["SUB_FOLDER_USER_CSV"]}/$this->csvFileName";
        Storage::disk('s3')->put($s3Destination, $fileContent);
        return $s3Destination;

    }
}
