<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ImportJobRepositoryInterface;

class UpdateImportProcessJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImportJobRepositoryInterface $importJobRepository)
    {
        $import_job_id = $this->input['import_job_id'];

        // remove id from the input array since it has been assigned to $id variable
        unset($this->input['import_job_id']);
        
        return $importJobRepository->edit($import_job_id, $this->input);
    }
}
