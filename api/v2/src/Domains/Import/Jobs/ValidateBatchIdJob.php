<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Import\Validators\BatchIdExistsValidator as Validator;

class ValidateBatchIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
     private $batchIdentifier;

     public function __construct(string $batchIdentifier)
     {
         $this->batchIdentifier = $batchIdentifier;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
     public function handle(Validator $validator)
    {
        $dataToValidate = [ "batch_id" => $this->batchIdentifier, 'is_deleted' => 0 ];
        $validation = $validator->validate($dataToValidate);

        return $validation===true ? $validation : $validation->errors()->all();
    }
}
