<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GetStatusAndDetailsFromImportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->identifier = $input['identifier'];
        $this->type = $input['type'];
        $this->organization_id = $input['organization_id'];
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $jobs = DB::table('import_job')
                    ->where($this->type, $this->identifier)
                    ->where('organization_id', $this->organization_id)
                    ->where('process_type', 2)
                    ->where('is_deleted', 0)
                    ->where('batch_id', '!=', '')
                    ->get();
        $result = [];
        foreach ($jobs as $job) {
            $taxonomy_settings = json_decode($job->import_taxonomy_settings);
            $summary_percentage = isset($taxonomy_settings->summary_percentage) ? $taxonomy_settings->summary_percentage : 0;
            $import_percentage = isset($taxonomy_settings->import_percentage) ? $taxonomy_settings->import_percentage : 0;
            if($job->results != '')
            {
                $jsonContentBeforeImportSummary = Storage::disk('s3')->get($job->results);
            }
            else
            {
                $jsonContentBeforeImportSummary = $job->results;
            }
            $before_import_summary = preg_replace("/[\r\n]+/", " ", $jsonContentBeforeImportSummary);
            $before_import_summary = utf8_encode($before_import_summary);
            $before_import_summary = json_decode($before_import_summary);

            $summaryStatus = true;
            if($job->status == 6)
            {
                if(isset($before_import_summary->success) && $before_import_summary->success == false)
                {
                    $summaryStatus = false;
                }
            }
            if($job->success_message_data != '')
            {
                $jsonContentAfterImportSummary = Storage::disk('s3')->get($job->success_message_data);
            }
            else
            {
                $jsonContentAfterImportSummary = $job->success_message_data;
            }
            $after_import_summary = preg_replace("/[\r\n]+/", " ", $jsonContentAfterImportSummary);
            $after_import_summary = utf8_encode($after_import_summary);
            $after_import_summary = json_decode($after_import_summary);
            $result[] = [
                'import_job_id' => $job->import_job_id,
                'batch_id' => $job->batch_id,
                'user_id' => $job->user_id,
                'status' => $job->status, // 1 - Not Started, 2 - Import In Progress, 3 - Import Complete, 4 - Failed, 5 - summary in progress, 6 - summary complete
                'summary_progress' => $summary_percentage,
                'before_import_summary' => $before_import_summary,
                'import_progress' => $import_percentage,
                'after_import_summary' => $after_import_summary,
                'errors' => json_decode($job->errors),
                'summary_status' => $summaryStatus
            ];
        }

        return $result;
    }
}
