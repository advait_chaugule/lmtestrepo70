<?php
namespace App\Domains\Import\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Import\Validators\FileIdExistsValidator as Validator;

class ValidatefileIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
     private $fileIdentifier;

     public function __construct(string $fileIdentifier)
     {
         $this->fileIdentifier = $fileIdentifier;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
     public function handle(Validator $validator)
    {
        $dataToValidate = [ "import_job_id" => $this->fileIdentifier ];
        $validation = $validator->validate($dataToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
