<?php
namespace App\Domains\Import\Jobs;

use App\Data\Repositories\Contracts\NotificationInterface;

use Lucid\Foundation\Job;

class CreateNotificationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NotificationInterface $notificationActivity)
    {
        $notificationActivity->createNotification($this->input);
    }
}
