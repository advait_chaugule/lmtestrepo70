<?php
namespace App\Domains\Import\CustomClass;

use Illuminate\Support\Facades\DB;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\Notifications\Jobs\EmailForBatchImportJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;

class CommonEmail
{
    use DispatchesJobs, UuidHelperTrait;
    public function __construct()
    {
        
    }

    public function createEmailNotification($batchId, $userId, $organizationId, $domainName)
    {
        $userDetails = $this->getUserDetails($userId);
        $user_email = $userDetails->email;
        $userName = $userDetails->first_name;
        $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($userId,$organizationId));
        $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
        $organizationName = $organizationData['organization_name'];
        $organizationCode = $organizationData['organization_code'];
        $domainName1 = str_replace('api.','',$domainName);
        $summaryLink = $this->generateSummaryLink($batchId, $domainName1, $organizationCode);
        $taxonomy_name = $this->getTaxonomyName($batchId);

        $email_body = 'The import process for the '.$taxonomy_name.' failed. Please click on the following button to open the link to import the file again.  You can also copy and paste the URL '.$summaryLink.' in your browser to launch.';
        $email_header = 'The system failed to import the “'.$taxonomy_name.'”.';
        $email_subject = '[ACMT Notification] – Import Process failed.';

        $emailData1 = [
            'subject'=> $email_subject,
             'email'    => $user_email,
             'body'     => ['email_body'=>$email_body, 'email_header'=>$email_header,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'batch_id'=>$batchId, 'taxonomy_name' => $taxonomy_name],
            'domain_name'=>$domainName,
         ];
         $this->dispatch(new EmailForBatchImportJob($emailData1));
    }

    public function createAppNotification($batchId, $userId, $organizationId)
    {
        $taxonomy_name = $this->getTaxonomyName($batchId);
        $description  = 'The import process for the Taxonomy '.$taxonomy_name.' is failed.';

        $activityLog = [
            "notification_id"       => $this->createUniversalUniqueIdentifier(),
            "description"           => $description,
            "target_type"           => config('event_activity')["Notifications"]["AFTER_IMPORT_CSV_SUMMARY"],
            "target_id"             => $batchId,
            "user_id"               => $userId,
            "organization_id"       => $organizationId,
            "is_deleted"            => 0,
            "created_by"            => $userId, // logged in user Id
            "updated_by"            => $userId,
            "target_context"        => json_encode(['batch_id'=>$batchId, 'user_id'=>$userId, 'organization_id'=>$organizationId], JSON_UNESCAPED_SLASHES),
            "read_status"           => "0", //0=unread 1= read
            "notification_category" => config('event_activity')["Notification_category"]["AFTER_IMPORT_CSV_SUMMARY"]
            ];

        $this->dispatch(new CreateNotificationJob($activityLog));
    }

    public function generateSummaryLink($batchId, $domainName1, $organizationCode)
    {
        $summaryLink = '';
        if($batchId != '' ) {
            if(strpos($domainName1,'localhost')!==false) {
                $summaryLink = env('BASE_URL') . '#/org/'.$organizationCode.'/app/postSummaryReport/' . $batchId;
            } else {
                $summaryLink = $domainName1 . '#/org/'.$organizationCode.'/app/postSummaryReport/' . $batchId;
            }
        }

        return $summaryLink;
    }

    public function getUserDetails($userId)
    {
        $user = DB::table('users')->where('user_id',$userId)->get()->first();
        return $user;
    }

    public function getTaxonomyName($batchId)
    {
        $importJob = DB::table('import_job')
                        ->where('batch_id', $batchId)
                        ->where('process_type', 2)
                        ->where('is_deleted', 0)
                        ->get()->first();
        $importTaxonomySettings = json_decode($importJob->import_taxonomy_settings);

        return $importTaxonomySettings->taxonomy_title;
    }
}