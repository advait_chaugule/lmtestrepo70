<?php
namespace App\Domains\Import\Listeners;
use App\Data\Repositories\Contracts\NotificationInterface;
use App\Foundation\ReportListenerHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\Import\Events\ImportEvent as Event;
use Illuminate\Support\Facades\Mail;
use Lucid\Foundation\QueueableJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;

use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\ArrayHelper;

use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonFileJob;
use App\Domains\CaseStandard\Jobs\StoreExternalCaseJsonFileJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseStandardJsonSpecificationJob;

use App\Domains\CaseStandard\Jobs\ExtractCaseFrameworkDocumentFromUploadedJsonContentJob;
use App\Domains\Document\Jobs\ValidateDocumentSourceIdJob;


use App\Domains\CaseStandard\Jobs\GetDataMetricsFromUploadedCaseJSonContentJob;

use App\Domains\CaseStandard\Jobs\CloneCasePackageDataJob;
use App\Domains\Taxonomy\Jobs\ImportJsonWithNoChangeJob;

use App\Domains\Import\Jobs\CreateImportProcessJob;
use App\Domains\Import\Jobs\UpdateImportProcessJob;
use App\Domains\Import\Jobs\CreateNotificationJob;

use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use Storage;

use DB;

use App\Domains\Notifications\Jobs\ImportJobForEmailJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use Illuminate\Support\Facades\Log;

class ImportEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue, UuidHelperTrait,DispatchesJobs,ErrorMessageHelper, StringHelper, FileHelper, ArrayHelper;
    
    public $queue ;
    public $connection = 'import_json_sqs';

    public function __construct(NotificationInterface $notificationActivity)
    {
        $this->queue=config('queue.connections.import_json_sqs.queue');
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
        $this->notificationRepository = $notificationActivity;
    }
    

    public function handle(Event $event)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        try {
            $start_time = microtime(true);
            $eventImport = $event->importEventData;
            $this->importActionEvent($eventImport);
            Log::error('i00000000:Execution time of script:'.(microtime(true) - $start_time));
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function importActionEvent($data)
    {
        $externalCaseFile = $data['caseFile'];
        $sourceIdentifier = $data['sourceId'];
        $importRequestIdentifier = $data['import_identifier'];
        $requestImportType = $data['importType'];
        $requestingUserDetail = $data['requestUserDetails'];
        $organizationId = $data['organizationId'];
        $domainName = $data['domain_name'];
        $s3PathUrl = $data['filefullpath'];
        $uploadedPackageMetrics = $data['uploadedPackageMetrics'];
        $caseFrameWorkDocumentIdentifier = $data['caseFrameWorkDocumentIdentifier'];
        $importData = DB::table('import_job')->select('import_job_id')->where('source_document_id',$caseFrameWorkDocumentIdentifier)->where('organization_id',$organizationId)->where('status','2')->first();
            if($importData)
            {
                $errorType = 'validation_error';
                $message = "This taxononomy already in process,please select different taxonomy to import";
                $_status = ' Taxonomy Process already Started.';
                return $this->dispatch(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }
            else {

                    $inputinupdateProcessInput['import_job_id']= $importRequestIdentifier;
                    $inputinupdateProcessInput['results']= '';
                    $inputinupdateProcessInput['process_type']= 2;
                    $inputinupdateProcessInput['status']= 2;
                    $inputinupdateProcessInput['filefullpath']= $s3PathUrl;
                    $inputinupdateProcessInput['updated_at']= now()->toDateTimeString();
                    $this->dispatch(new UpdateImportProcessJob($inputinupdateProcessInput));

                    // extract the content of the duplicate CASE json file
                    $fileContent = Storage::disk('s3')->get($externalCaseFile);
                    $fileContentArr = $this->convertJsonStringToArray($fileContent);
                    unset($fileContent);

                    // check if imported package exists in our system
                    $packageExists = $this->dispatch(new ValidateDocumentSourceIdJob($caseFrameWorkDocumentIdentifier, $organizationId));
                    switch ($requestImportType)
                    {
                        case '1':
                        case '2':
                        case '3':
                            // $inputinupdateProcessInput['import_job_id']= $importRequestIdentifier;
                            // $inputinupdateProcessInput['results']= '';
                            // $inputinupdateProcessInput['process_type']= 2;
                            // $inputinupdateProcessInput['status']= 2;
                            // $inputinupdateProcessInput['filefullpath']= $s3PathUrl;
                            // $inputinupdateProcessInput['updated_at']= now()->toDateTimeString();
                            // $this->dispatch(new UpdateImportProcessJob($inputinupdateProcessInput));
                            $packageDataArray = [ "packageData" => $fileContentArr, "oldNewItemMapping" => [] ];
                            unset($fileContentArr);
                            if($uploadedPackageMetrics['associations_count'] == 0 && $uploadedPackageMetrics['associations_group_count'] == 0)
                            {
                                $importFlag = 0;
                            }
                            else
                            {
                                $returnDocumentDetail = $this->dispatch(new ImportJsonWithNoChangeJob($packageDataArray, $requestingUserDetail, $requestImportType, $packageExists,$domainName));
                                $importFlag = 1;
                            }
                            unset($packageDataArray);
                            break;
                        default:
                            $repository = false;
                            break;
                    }

                    if($importFlag == 0)
                    {
                        $uploadedPackageMetrics['document_id'] = "";
                        $uploadedPackageMetrics['document_title'] = $uploadedPackageMetrics['source_taxonomy_name'];
                        $uploadedPackageMetrics['import_status'] = "failed";
                        $taxonomyName = $uploadedPackageMetrics['source_taxonomy_name'];
                            // Create Notification for Taxonomy Imported
                        $email_body = "Could not upload the taxonomy {$taxonomyName} as the JSON did not include associtions for the taxnonomy";
                        $user_subject = "The import process for the Taxonomy {$taxonomyName} is failed.";
                    }
                    else
                    {
                        $uploadedPackageMetrics['document_id'] = $returnDocumentDetail["document_id"];
                        $uploadedPackageMetrics['document_title'] = $returnDocumentDetail["document_title"];
                        $taxonomyName = $returnDocumentDetail["document_title"];
                        // Create Notification for Taxonomy Imported
                        $email_body = "The import process for the Taxonomy {$taxonomyName} is completed.";
                        $user_subject = "The import process for the Taxonomy {$taxonomyName} is completed.";
                    }

                    $owner_id     = $requestingUserDetail['user_id'];
                    $notificationId = $this->createUniversalUniqueIdentifier();
                    $notificationMsg = $email_body;
                    $targetContext         = ['document_id'=>$uploadedPackageMetrics['document_id'],'user_id'=>$owner_id];
                    $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                    $notificationCategory  = config('event_activity')["Notification_category"]["IMPORT_JSON"];
                    $getUserDetails = $this->getUserDetails($owner_id);
                    $user_email = $getUserDetails[0]->email;
                    $firstName = $getUserDetails[0]->first_name;
                    $lastName = $getUserDetails[0]->last_name;
                    $currentOrgId= $getUserDetails[0]->current_organization_id;
                    $userName = $firstName . ' ' . $lastName;
                    $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($owner_id,$organizationId));
                    $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                    $organizationName = $organizationData['organization_name'];
                    $organizationCode = $organizationData['organization_code'];
                    $emailData1 = [
                        'subject'=>$user_subject,
                            'email'    => $user_email,
                        'body'     => ['email_body'=>$email_body,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'document_id'=>$uploadedPackageMetrics['document_id']],
                        'domain_name'=>$domainName
                    ];

                    $activityLog = [
                        "notification_id"       => $notificationId,
                        "description"           => $notificationMsg,
                        "target_type"           => "15",
                        "target_id"             => $uploadedPackageMetrics['document_id'],
                        "user_id"               => $owner_id,
                        "organization_id"       => $organizationId,
                        "is_deleted"            => "",
                        "created_by"            => $owner_id, // logged in user Id
                        "updated_by"            => $owner_id,
                        "target_context"        => $targetContextJson,
                        "read_status"           => "0", //0=unread 1= read
                        "notification_category" => $notificationCategory
                    ];

                    $this->notificationRepository->createNotification($activityLog);
                    unset($activityLog);
                    unset($targetContext);
                    unset($targetContextJson);
                    unset($notificationMsg);

                    $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $owner_id, 'organization_id' => $organizationId]));
                    $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                    if ($emailSettingStatus == 1) {
                        $this->dispatch(new ImportJobForEmailJob($emailData1));
                        unset($emailData1);
                    }
                    $this->deleteFileFromLocalPath($externalCaseFile);
                    $successType = 'found';
                    $message = 'Import Successfull.';
                    $_status = 'custom_status_here';

                    $this->dispatch(new RespondWithJsonJob($successType, $uploadedPackageMetrics, $message, $_status));
                    $inputupdateProcessInput['import_job_id']= $importRequestIdentifier;
                    $inputupdateProcessInput['results']= json_encode($uploadedPackageMetrics);
                    if($importFlag == 0)
                    {
                        $inputupdateProcessInput['status']= 4;
                    }
                    else
                    {
                        $inputupdateProcessInput['status']= 3;
                    }
                    $inputupdateProcessInput['updated_at']= now()->toDateTimeString();
                    $this->dispatch(new UpdateImportProcessJob($inputupdateProcessInput));
            }

    }

    public function getUserDetails($userId)
    {
        $query = $user = DB::table('users')->where('user_id',$userId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

}
