<?php
namespace App\Domains\Import\Listeners;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Services\Api\Traits\ArrayHelper;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Import\Events\ImportMultipleCsvEvent;
use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use App\Domains\Notifications\Jobs\EmailForBatchImportJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Domains\Csv\Jobs\UpdateMultipleTaxonomyWithDataFromCsvJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use Illuminate\Support\Facades\Schema;
use App\Domains\Metadata\Jobs\ListMetadataJob;
use App\Domains\Csv\Jobs\SummaryMultipleCsvJob;
use App\Domains\Import\Jobs\UploadJsonFileS3Job;
use App\Domains\Import\CustomClass\CommonEmail;
use Exception;

class ImportMultipleCsvEventListeners implements ShouldQueue
{
    use InteractsWithQueue, DispatchesJobs, ArrayHelper, UuidHelperTrait, ErrorMessageHelper;

    public $connection = 'import_json_sqs';

    public function __construct()
    {
        $this->queue = config('queue.connections.import_json_sqs.queue');
        $this->commonEmail = new CommonEmail();
    }

    public function handle(ImportMultipleCsvEvent $event)
    {
        try {
            $batchId = $event->importCSVEventData['batchId'];
            $importType = ($event->importCSVEventData['importType'] == 1) ? 2 : $event->importCSVEventData['importType'];
            $requestingUserDetails = $event->importCSVEventData['requestingUserDetails'];
            $userId = $requestingUserDetails['user_id'];
            $organizationId = $requestingUserDetails['organization_id'];
            $requestData = $event->importCSVEventData['request'];
            $request = new Request();
            $request->merge($requestData);
            $metadataListForATenant = $this->dispatch(new ListMetadataJob($request));
            $domainName = $event->importCSVEventData['domain_name'];
            $requestUrl = $event->importCSVEventData['requestUrl'];

            /**
             * get all taxonomy job from current batch which summary process is completed 
             */
            $importJobQuery = DB::table('import_job')
                                ->where('batch_id', $batchId)
                                ->where('process_type', 2)
                                ->where('status', 6)
                                ->where('is_deleted', 0);
            $importjobs = $importJobQuery->get();        
            $multipleCsvData = [];
            $packagetData = [];
            
            /**
             *  prepare a data for pass value to import job
             */
            foreach($importjobs as $job) {
                $csv_table_name = str_replace('-', '_', 'import_'.$job->import_job_id);
                $csv_data = $this->csvTableToArray($csv_table_name);
                if($csv_data === false) {
                    DB::table('import_job')
                        ->where('batch_id', $batchId)
                        ->where('import_job_id', $job->import_job_id)
                        ->where('process_type', 2)
                        ->where('is_deleted', 0)
                        ->update([
                            'status' => 4,
                            'updated_at' => now(),
                            'errors' => json_encode("From import, Csv table (acmt_import_".$csv_table_name.") not found")
                            ]);
                    Log::error("From import, Csv table (acmt_import_".$csv_table_name.") not found");
                    $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
                    $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                    return;
                }
                $multipleCsvData[$job->import_job_id] = $csv_data;
                $importTaxonomySettings = json_decode($job->import_taxonomy_settings);
                $packageDataArr[$job->import_job_id]['package_exists'] = ($importTaxonomySettings->package_exists === 'false' || $importTaxonomySettings->package_exists === false) ? false : true;
                $packageDataArr[$job->import_job_id]['import_type'] = $importType;
                $packageDataArr[$job->import_job_id]['taxonomy_name'] = $importTaxonomySettings->taxonomy_title;
            }

            $resultArr = [];
            if(count($multipleCsvData) > 0)
            {
                $importJobQuery->update(['status' => 2,'updated_at'=>now()]);
                foreach($multipleCsvData as $jobId => $csvDataHeaderRow) {
                try {
                    // dd('$csvDataHeaderRow',$csvDataHeaderRow);
                    $importType = $packageDataArr[$jobId]['import_type'];
                    $packageExists = $packageDataArr[$jobId]['package_exists'];
                    $this->updateTaxonomyLockStatus($jobId,$organizationId,$importType);
                    $csvDataHeaderRow = $this->trim_array_spaces($csvDataHeaderRow);
                    $csvContentArray  =  $this->removeBlankArray($csvDataHeaderRow['row']);
                    $updateCsvContentToArray  =  $csvDataHeaderRow['row'];
                    $csvContentArray = $this->array_change_key_case_recursive($csvContentArray,CASE_LOWER);
                    $csvDataHeaderRow['column'] = array_map('strtolower',$csvDataHeaderRow['column']);
                    /**
                     * start import process for each taxonomy
                     * 
                     */
                    $failsImportId = [];
                    $passedImportId = [];
                    
                    // to match count with pre and post sumamry
                    if($importType == 4) {
                        $reportNodeChangesNeededForTheCsvUpload =   $this->dispatch(new SummaryMultipleCsvJob($csvContentArray, $organizationId, $requestingUserDetails,  $importType,  $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists, $request, $jobId));
                        if($reportNodeChangesNeededForTheCsvUpload != false){
                            # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                            $response = explode('|mUy7V3e|', $reportNodeChangesNeededForTheCsvUpload);
                            $countUpdatedDataFromCsvToDb = json_decode($response[1]);
                            if(!empty($countUpdatedDataFromCsvToDb)){
                                unset($countUpdatedDataFromCsvToDb->metadata);
                                unset($countUpdatedDataFromCsvToDb->errorData);
                            }
                                
                        }
                    }
                    $returnImportDetail = $this->dispatch(new UpdateMultipleTaxonomyWithDataFromCsvJob($updateCsvContentToArray, $organizationId, $requestingUserDetails,  $importType, $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists,$requestUrl, $request, $jobId));
                    if($importType == 4)
                    {
                        $returnImportDetail = $countUpdatedDataFromCsvToDb;
                    }
                    
                    $csv_table_name = str_replace('-', '_', 'import_'.$jobId);
                    $this->deleteTableAfterImpor($csv_table_name);
                    // if($importType == 2) { #for import type 2(:create a copy)
                    //     $returnSummaryDetail = $this->dispatch(new SummaryMultipleCsvCopyJob($csvContentArray, $organizationId, $requestingUserDetails,  $importType,  $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists, $request));
                    // } else { #for import type 1(:first import) and 4(:update)
                    //     if($importType == 1) {
                    //         $packageExists = false;
                    //     }
                    //     $returnSummaryDetail = $this->dispatch(new SummaryMultipleCsvJob($csvContentArray, $organizationId, $requestingUserDetails,  $importType,  $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists, $request, $jobId));
                    // }

                    if($returnImportDetail == false) {
                        $message = 'The file contains mandatory field value blank or similar name and type for custom metadata. Please refer to the sample CSV and update the file and import again';
                        $resultArr[$jobId] = [
                            'message' => $message,
                            'import_detail' => ""
                        ];
                        array_push($failsImportId,$jobId);
                    } else {
                        // $response = explode('||', $returnImportDetail);
                        // if($response[0] == '2') {
                        //     $message = 'The file contains : '.implode(',', json_decode($response[1], true)).' as non case metadata prefixed with case_/Case_. Please refer to sample CSV file';
                        //     array_push($failsImportId,$jobId);
                        // } else if($response[0] == '3') {
                        //     $message = 'You have uploaded an older supported format of the CSV. Please update as per the current format';
                        //     array_push($failsImportId,$jobId);
                        // } else if($response[0] == '4') {
                        //     $message = 'Could not find the parent id for nodes with the following parent id : '.implode(',', json_decode($response[1], true));
                        //     array_push($failsImportId,$jobId);
                        // } else {
                        //     $message = 'Succesfully imported';
                        //     array_push($passedImportId,$jobId);
                        // }
                        array_push($passedImportId,$jobId);
                        $resultArr[$jobId] = [
                            'message' => 'File imported successfully',
                            'import_detail' => $returnImportDetail
                        ];
                    }
                         /**
                     * end import process for each taxonomy
                     * 
                     */
                } catch(Exception $exception){
                    $error_message = $this->createErrorMessageFromException($exception);
                    DB::table('import_job')
                        ->where('batch_id', $batchId)
                        ->where('import_job_id', $jobId)
                        ->where('process_type', 2)
                        ->where('is_deleted', 0)
                        ->update([
                            'status' => 4,
                            'updated_at' => now(),
                            'errors' => $error_message
                        ]);
                    $this->updateTaxonomyLockStatus($jobId,$organizationId,'');
                    $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
                    $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                    Log::error($error_message);
                    return;
                }
                }
                $db_prefix = DB::getTablePrefix();
                #raw query to update multiple rows at once, to avoid query in foreach loop
                if(count($resultArr)) {
                    $statement = "UPDATE ".$db_prefix."import_job SET success_message_data = CASE import_job_id ";
                    $importIds = [];
                    foreach($resultArr as $jobId => $row) {
                        $this->updateTaxonomyLockStatus($jobId,$organizationId,'');
                        //save file in s3
                        $externalJsonFileName = 'importCSV_'.$jobId.".json";
                        // store the content of uploaded Csv inside the file created above     
                        $externalJsonFile = $this->dispatch(new UploadJsonFileS3Job(json_encode($row,true),$externalJsonFileName));
                     
                        $statement .= " WHEN '".$jobId."' THEN '".$externalJsonFile."' ";
                        $importIds[] = "'".$jobId."'";
                    }
                    $statement .= " END , updated_at = '".now()."' ";
                    $statement .= " WHERE import_job_id IN (".implode(',', $importIds).") AND batch_id = '".$batchId."'";
                    DB::statement($statement);
                }
                if(isset($passedImportId) && !empty($passedImportId)) {
                    $statement1 = "UPDATE ".$db_prefix."import_job SET status =  3  WHERE import_job_id IN ('".implode("','", $passedImportId)."') AND batch_id = '".$batchId."'";
                    DB::statement($statement1);
                }
                if(isset($failsImportId) && !empty($failsImportId)) {
                    $statement2 = "UPDATE ".$db_prefix."import_job SET status =  4  WHERE import_job_id IN ('".implode("','", $failsImportId)."') AND batch_id = '".$batchId."'";
                    DB::statement($statement2);
                }

                // mail and notification for sucessfully import
                foreach($multipleCsvData as $jobId => $csvDataHeaderRow)
                 {
                    $taxonomy_name = $packageDataArr[$jobId]['taxonomy_name'];

                    //Notification via Email for Import of Batch
                    if (isset($passedImportId) && !empty($passedImportId) && in_array($jobId, $passedImportId))
                    {
                        $userDetails = $this->commonEmail->getUserDetails($userId);
                        $user_email = $userDetails->email;
                        $userName = $userDetails->first_name;
                        $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($userId,$organizationId));
                        $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                        $organizationName = $organizationData['organization_name'];
                        $organizationCode = $organizationData['organization_code'];
                        $domainName1 = str_replace('api.','',$domainName);
                        $summaryLink = $this->commonEmail->generateSummaryLink($batchId, $domainName1, $organizationCode);

                        $description  = 'The import process for the Taxonomy '.$taxonomy_name.' is completed.';
                        $email_body = 'The Import of the '.$taxonomy_name.' is completed. Please click on the following button to open the link and know more. You can also copy and paste the URL '.$summaryLink.' in your browser to launch.';
                        $email_header = 'The system successfully completed the import of the '.$taxonomy_name.'.';
                        $email_subject = '[ACMT Notification] – Import Process completed successfully.';

                        $activityLog = [
                            "notification_id"       => $this->createUniversalUniqueIdentifier(),
                            "description"           => $description,
                            "target_type"           => config('event_activity')["Notifications"]["AFTER_IMPORT_CSV_SUMMARY"],
                            "target_id"             => $batchId,
                            "user_id"               => $userId,
                            "organization_id"       => $organizationId,
                            "is_deleted"            => 0,
                            "created_by"            => $userId, // logged in user Id
                            "updated_by"            => $userId,
                            "target_context"        => json_encode(['batch_id'=>$batchId, 'user_id'=>$userId, 'organization_id'=>$organizationId], JSON_UNESCAPED_SLASHES),
                            "read_status"           => "0", //0=unread 1= read
                            "notification_category" => config('event_activity')["Notification_category"]["AFTER_IMPORT_CSV_SUMMARY"]
                            ];

                        $this->dispatch(new CreateNotificationJob($activityLog));
                        unset($activityLog);

                        $emailData1 = [
                            'subject'=> $email_subject,
                             'email'    => $user_email,
                             'body'     => ['email_body'=>$email_body, 'email_header'=>$email_header,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'batch_id'=>$batchId, 'taxonomy_name' => $taxonomy_name],
                            'domain_name'=>$domainName,
                         ];

                         $this->dispatch(new EmailForBatchImportJob($emailData1));
                         unset($emailData1);
                    }
                    else
                    {
                        $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
                        $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                    }
                }
            }
            // dd('$resultArr',$resultArr);
        } catch(Exception $exception) {
            $error_message = $this->createErrorMessageFromException($exception);
            DB::table('import_job')
            ->where('batch_id', $batchId)
            ->where('process_type', 2)
            ->where('is_deleted', 0)
            ->update([
                'status' => 4,
                'updated_at' => now(),
                'errors' => $error_message
            ]);
            $this->updateTaxonomyLockStatus($batchId,$organizationId,'');
            $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
            $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
            Log::error($error_message);
        }
    }

    private function csvTableToArray($table_name)
    {
        if(!Schema::hasTable($table_name)) {
            return false;
        }
        // commenting this line as the order of columns is not correct, to solve UF-1246
        // $headers = DB::getSchemaBuilder()->getColumnListing($table_name);
        $importData = DB::table($table_name)->get();

        foreach($importData as $val) {
            $rowData[] = (array) $val;
        }
        $headers = array_keys($rowData[0]);
        return ['column' => $headers, 'row' => $rowData];
    }

    private function deleteTableAfterImpor($table_name)
    {
        DB::table($table_name)->delete();
    }

    private function updateTaxonomyLockStatus($jobId,$organizationId,$importType = '')  
    {
        
            $sourceDocObj = DB::table('import_job')->select('source_document_id')->where('import_job_id', $jobId)->first();
            if($sourceDocObj)
            {
                $sourceDocumentId = $sourceDocObj->source_document_id;
                $documentIdObj       =   DB::table('documents')->select('document_id')->where('organization_id', $organizationId)->where('source_document_id',$sourceDocumentId)->first();
                if($documentIdObj)
                {
                    $documentId = $documentIdObj->document_id;
                    if($importType == 4)
                    {
                        DB::table('documents')->where('document_id', $documentId)->update(['is_locked' => 1]);
                    }
                    else
                    {
                        DB::table('documents')->where('document_id', $documentId)->update(['is_locked' => 0]);
                    }
                }

            }
    }
}
