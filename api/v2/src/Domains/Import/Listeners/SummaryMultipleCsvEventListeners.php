<?php
namespace App\Domains\Import\Listeners;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use App\Services\Api\Traits\ArrayHelper;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Metadata\Jobs\ListMetadataJob;
use App\Domains\Csv\Jobs\SummaryMultipleCsvJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Domains\Import\Jobs\CreateNotificationJob;
use App\Domains\Csv\Jobs\SummaryMultipleCsvCopyJob;
use App\Domains\Import\Events\SummaryMultipleCsvEvent;
use App\Domains\Notifications\Jobs\EmailForBatchSummaryJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use App\Domains\Import\Jobs\UploadJsonFileS3Job;
use App\Domains\Import\CustomClass\CommonEmail;

class SummaryMultipleCsvEventListeners implements ShouldQueue
{
    use InteractsWithQueue, DispatchesJobs, ArrayHelper, UuidHelperTrait, ErrorMessageHelper;
    //****uncomment before final build
    public $connection = 'import_json_sqs';

    public function __construct()
    {
        //****uncomment before final build
        $this->queue = config('queue.connections.import_json_sqs.queue');
        $this->commonEmail = new CommonEmail();
    }

    public function handle(SummaryMultipleCsvEvent $event)
    {
        try {
            $batchId = $event->summaryEventData['batchId'];
            $importType = $event->summaryEventData['importType'];
            $csv_table_name = $event->summaryEventData['table_name'];
            $requestingUserDetails = $event->summaryEventData['requestingUserDetails'];
            $userId = $requestingUserDetails['user_id'];
            $organizationId = $requestingUserDetails['organization_id'];
            $requestData = $event->summaryEventData['request'];
            $request = new Request();
            $request->merge($requestData);
            $metadataListForATenant = $this->dispatch(new ListMetadataJob($request));
            // $metadataListForATenant = $event->summaryEventData['metadataListForATenant'];
            $domainName = $event->summaryEventData['domain_name'];

            $importjobsQuery = DB::table('import_job')
                                    ->where('batch_id', $batchId)
                                    ->where('process_type', 2)
                                    ->where('status', 1)
                                    ->where('is_deleted', 0);
            $importjobs = $importjobsQuery->get();
            $multipleCsvData = [];
            foreach($importjobs as $job) {
                $csv_table_name = str_replace('acmt_', '', $csv_table_name);
                $csv_data = $this->csvTableToArray($csv_table_name);
                if($csv_data === false) {
                    DB::table('import_job')
                    ->where('batch_id', $batchId)
                    ->where('import_job_id', $job->import_job_id)
                    ->where('process_type', 2)
                    ->where('is_deleted', 0)
                    ->update([
                        'status' => 4,
                        'updated_at' => now(),
                        'errors' => json_encode("From summary, Csv table (acmt_import_".$csv_table_name.") not found")
                        ]);
                    $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
                    $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                    Log::error("From summary, Csv table (acmt_import_".$csv_table_name.") not found");
                    return;
                }
                $multipleCsvData[$job->import_job_id] = $csv_data;
                $importTaxonomySettings = json_decode($job->import_taxonomy_settings);
                $packagetData[$job->import_job_id]['package_exists'] = ($importTaxonomySettings->package_exists === 'false' || $importTaxonomySettings->package_exists === false) ? false : true;
                $packagetData[$job->import_job_id]['import_type'] = $importType;//$importTaxonomySettings->import_type;
                $packagetData[$job->import_job_id]['taxonomy_name'] = $importTaxonomySettings->taxonomy_title;
            }

            $resultArr = [];
            if(count($multipleCsvData)) {
                // ****uncomment this line before final
                $importjobsQuery->update(['status' => 5, 'updated_at' => now()]);
                foreach($multipleCsvData as $jobId => $csvDataHeaderRow) {
                    try {
                        $importType = $packagetData[$jobId]['import_type'];
                        $packageExists = $packagetData[$jobId]['package_exists'];
                        $taxonomyName = $packagetData[$jobId]['taxonomy_name'];
                        $csvDataHeaderRow = $this->trim_array_spaces($csvDataHeaderRow);
                        $csvContentArray  =  $this->removeBlankArray($csvDataHeaderRow['row']);
                        $csvContentArray = $this->array_change_key_case_recursive($csvContentArray,CASE_LOWER);
                        $csvDataHeaderRow['column'] = array_map('strtolower',$csvDataHeaderRow['column']);
                        if($importType == 2) { #for import type 2(:create a copy)
                            $returnSummaryDetail = $this->dispatch(new SummaryMultipleCsvCopyJob($csvContentArray, $organizationId, $requestingUserDetails,  $importType,  $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists, $request));
                        } else { #for import type 1(:first import) and 4(:update)
                            if($importType == 1) {
                                $packageExists = false;
                            }
                            $returnSummaryDetail = $this->dispatch(new SummaryMultipleCsvJob($csvContentArray, $organizationId, $requestingUserDetails,  $importType,  $csvDataHeaderRow['column'], $metadataListForATenant, $packageExists, $request, $jobId));
                        }
                        if($returnSummaryDetail == false) {
                            $message = 'The file contains mandatory field value blank or similar name and type for custom metadata. Please refer to the sample CSV and update the file and import again';
                            $resultArr[$jobId] = [
                                'message' => [$message],
                                'summary_detail' => "",
                                'taxonomy_name' => $taxonomyName,
                                'success' => false
                            ];
                        } else {
                            $success = false;
                            # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                            $response = explode('|mUy7V3e|', $returnSummaryDetail);
                            if($response[0] == '2') {
                                $message = 'The file contains : '.implode(',', json_decode($response[1], true)).' as non case metadata prefixed with case_/Case_. Please refer to sample CSV file';
                            } else if($response[0] == '3') {
                                $message = 'You have uploaded an older supported format of the CSV. Please update as per the current format';
                            } else if($response[0] == '4') {
                                $message = 'Could not find the parent id for nodes with the following parent id : '.implode(',', json_decode($response[1], true));
                            } else if($response[0] == '5'){
                                $message = 'You have duplicate node ids at row : '.implode(',', json_decode($response[1], true));
                            } else if($response[0] == '6'){
                                $message = 'Could not import this file as node id and parent id is same at row number : '.implode(',', json_decode($response[1], true));
                            } else {
                                $message = 'Data found';
                                $success = true;
                            }
                            $resultArr[$jobId] = [
                                'message' => [$message],
                                'summary_detail' => json_decode($response[1], true),
                                'taxonomy_name' => $taxonomyName,
                                'import_type' => $importType,
                                'success' => $success
                            ];
                        }
                    } catch(\Exception $exception){
                        $error_message = $this->createErrorMessageFromException($exception);
                        DB::table('import_job')
                            ->where('batch_id', $batchId)
                            ->where('import_job_id', $jobId)
                            ->where('process_type', 2)
                            ->where('is_deleted', 0)
                            ->update([
                                'status' => 4,
                                'updated_at' => now(),
                                'errors' => $error_message
                            ]);
                        $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
                        $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
                        Log::error($error_message);
                    }
                }
                if(count($resultArr)) {
                    $db_prefix = DB::getTablePrefix();
                    #raw query to update multiple rows at once, to avoid query in foreach loop
                    $statement = "UPDATE ".$db_prefix."import_job SET results = CASE import_job_id ";
                    $taxonomy_name = '';
                    foreach($resultArr as $jobId => $row) {
                        //save file in s3
                        $externalJsonFileName = 'summary_'.$jobId.".json";
                        // store the content of uploaded Csv inside the file created above     
                        $externalJsonFile = $this->dispatch(new UploadJsonFileS3Job(json_encode($row,true),$externalJsonFileName));
                        $statement .= " WHEN '".$jobId."' THEN '".$externalJsonFile."' ";
                        $importIds[] = "'".$jobId."'";
                        $taxonomy_name = $row['taxonomy_name'];
                    }
                    $statement .= " END , status = 6 , updated_at = '".now()."' ";
                    $statement .= " WHERE import_job_id IN (".implode(',', $importIds).") AND batch_id = '".$batchId."' AND status = 5 ";
                    DB::statement($statement);

                    # In-App Notification
                    $activityLog = [
                        "notification_id"       => $this->createUniversalUniqueIdentifier(),
                        "description"           => 'Import process of '.$taxonomy_name.' has been initialized.',
                        "target_type"           => config('event_activity')["Notifications"]["BEFORE_IMPORT_CSV_SUMMARY"],
                        "target_id"             => $batchId,
                        "user_id"               => $userId,
                        "organization_id"       => $organizationId,
                        "is_deleted"            => 0,
                        "created_by"            => $userId, // logged in user Id
                        "updated_by"            => $userId,
                        "target_context"        => json_encode(['batch_id'=>$batchId, 'user_id'=>$userId, 'organization_id'=>$organizationId, 'taxonomy_name' => $taxonomy_name], JSON_UNESCAPED_SLASHES),
                        "read_status"           => "0", //0=unread 1= read
                        "notification_category" => config('event_activity')["Notification_category"]["BEFORE_IMPORT_CSV_SUMMARY"]
                        ];

                    $this->dispatch(new CreateNotificationJob($activityLog));

                    //Notification via Email for Summary of Batch
                    $getUserDetails = $this->getUserDetails($userId);
                    $user_email = $getUserDetails[0]->email;
                    $firstName = $getUserDetails[0]->first_name;
                    $lastName = $getUserDetails[0]->last_name;
                    $currentOrgId= $getUserDetails[0]->current_organization_id;
                    $userName = $firstName;
                    $userOrganizationId = $this->dispatch(new GetUserOrganizationIdWithUserJob($userId,$organizationId));
                    $organizationData = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                    $organizationName = $organizationData['organization_name'];
                    $organizationCode = $organizationData['organization_code'];
                    $domainName1 = str_replace('api.','',$domainName);
                    if($batchId != '' )
                    {
                        if(strpos($domainName1,'localhost')!==false)
                        {
                            $summaryLink = env('BASE_URL') . '#/org/'.$organizationCode.'/app/preSummaryReport/' . $batchId;
                        }else{
                            $summaryLink = $domainName1 . '#/org/'.$organizationCode.'/app/preSummaryReport/' . $batchId;
                        }

                    }
                    else
                    {
                        $summaryLink = '';
                    }
                    $email_body = 'Import process of '.$taxonomy_name.' has been initialized and it is available for review. Please click on the following button to open the link and know more. You can also copy and paste the URL '.$summaryLink.' in your browser to launch.';
                    $email_header = 'Import process of '.$taxonomy_name.' has been initialized.';
                    $emailData1 = [
                        'subject'=> '[ACMT Notification] – Import process of '.$taxonomy_name.' has been initialized.',
                        'email'    => $user_email,
                        'body'     => ['email_body'=>$email_body,'email_header' => $email_header,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'batch_id'=>$batchId, 'taxonomy_name' => $taxonomy_name],
                        'domain_name'=>$domainName
                    ];

                    $this->dispatch(new EmailForBatchSummaryJob($emailData1));
                }
            }
            // ****comment this line before final
            // dd('after job:', $resultArr);
        } catch(\Exception $exception) {
            $error_message = $this->createErrorMessageFromException($exception);
            $batchId = isset($event->summaryEventData['batchId']) ? $event->summaryEventData['batchId'] : '';
            if(!empty($batchId)) {
                DB::table('import_job')
                    ->where('batch_id', $batchId)
                    ->where('process_type', 2)
                    ->where('is_deleted', 0)
                    ->update([
                        'status' => 4,
                        'updated_at' => now(),
                        'errors' => $error_message
                    ]);
                $this->commonEmail->createEmailNotification($batchId, $userId, $organizationId, $domainName);
                $this->commonEmail->createAppNotification($batchId, $userId, $organizationId);
            }
            Log::error($error_message);
        }
    }

    public function getUserDetails($userId)
    {
        $query = $user = DB::table('users')->where('user_id',$userId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }

    private function csvTableToArray($table_name)
    {
        if(!Schema::hasTable($table_name)) {
            return false;
        }
        // commenting this line as the order of columns is not correct, to solve UF-1246
        // $headers = DB::getSchemaBuilder()->getColumnListing($table_name);
        $importData = DB::table($table_name)->get();

        foreach($importData as $val) {
            $rowData[] = (array) $val;
        }
        $headers = array_keys($rowData[0]);
        return ['column' => $headers, 'row' => $rowData];
    }
}
