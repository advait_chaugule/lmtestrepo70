<?php
namespace App\Domains\Import\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class FileIdExistsValidator extends BaseValidator {

    protected $rules = [
        'import_job_id' => 'required|exists:import_job,import_job_id',
    ];

     protected $messages = [
        'import_job_id' => ':attribute should be provided'
     ];
    
}