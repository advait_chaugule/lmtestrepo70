<?php
namespace App\Domains\Import\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class BatchIdExistsValidator extends BaseValidator {

    protected $rules = [
        'batch_id' => 'required|exists:import_job,batch_id,is_deleted,0',
    ];

     protected $messages = [
        'required' => ':attribute should be provided.',
        "exists"  =>  ":attribute is invalid."
     ];
    
}