<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;
use App\Domains\LinkedServer\Validators\LinkedServerIdValidator;

class ValidateLinkedServerIdJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LinkedServerIdValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
