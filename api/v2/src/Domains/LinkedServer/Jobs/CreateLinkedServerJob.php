<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LinkedServerRepositoryInterface;


class CreateLinkedServerJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LinkedServerRepositoryInterface $linkedServerRepository)
    {
        return $linkedServerRepository->fillAndSave($this->input);
    }
}
