<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\LinkedServerRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class GetLinkedServerDetailJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    use DateHelpersTrait;

    public function __construct(array $input)
    {
        $this->input = $input;
       
        $organizationIdentifier = $this->input['organization_id'];
        $linkedServerId = $this->input['linked_server_id'];
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->setLinkedServerId($linkedServerId);
    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LinkedServerRepositoryInterface $linkedServer,UserRepositoryInterface $userRepo)
    {
        //
        $this->linkedServer  = $linkedServer;

        
        $linkedServerDetails = $this->getLinkedServerDetailFromModel(); 
        $getAllLinkedServerDetail = array();
        
        foreach($linkedServerDetails as $linkedServerDetail)
        {
            if($linkedServerDetail->connection_status == 1)
            {
                $connectionStatus = "Active";
            }
            else
            {
                $connectionStatus = "Failed";
            }
            if($linkedServerDetail->linked_type == 1)
            {
                $linkedType = "CASE";
            }
            else
            {
                $linkedType = "ACMT";
            }
$updatedBy = $userRepo->find($linkedServerDetail->updated_by);
$configuredBy = $updatedBy->first_name." ".$updatedBy->last_name;
            $getAllLinkedServerDetail[] = [
                'linked_server_id' => $linkedServerDetail->linked_server_id, 
                'organization_id' => $linkedServerDetail->organization_id,
                'linked_name' => $linkedServerDetail->linked_name,
                'server_url' => $linkedServerDetail->server_url, 
                'description' => $linkedServerDetail->description, 
                'linked_type' => $linkedType, 
                'connection_status' => $connectionStatus,
                'is_deleted' => $linkedServerDetail->is_deleted,
                'updated_by' =>  $configuredBy,
                'updated_at' =>  $this->formatDateTime($linkedServerDetail->updated_at)
            ];
        }
      
          return $getAllLinkedServerDetail;                 
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }
    public function setLinkedServerId($identifier){
        $this->linkedServerId = $identifier;
    }

    public function getLinkedServerId(){
        return $this->linkedServerId;
    }
    
    public function getLinkedServerDetailFromModel()
    {
        $organizationId = $this->getOrganizationIdentifier();
        $LinkeServerId = $this->getLinkedServerId();


        if($LinkeServerId == "")
        {
            $returnCollection = true;
            $keyValuePairs = ['organization_id' => $organizationId, "is_deleted" => 0];
          
        }
        else
        {
            $returnCollection = true;
            $keyValuePairs = ['organization_id' => $organizationId, "linked_server_id" => $LinkeServerId, "is_deleted" => 0];
        }
        
        $fieldsToReturn = ['linked_server_id', 
                               'organization_id',
                               'linked_name',
                               'server_url', 
                               'description', 
                               'linked_type', 
                               'connection_status',
                               'is_deleted',
                               'updated_by',
                               'updated_at' 
                               ];
        $linkedServerDetails = $this->linkedServer->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs);
                               
            return $linkedServerDetails;   

    }
}
