<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LinkedServerRepositoryInterface;

class DeleteLinkedServerJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LinkedServerRepositoryInterface $linkedServerRepo)
    {
        $linkedId = $this->input['linked_server_id'];
        // remove id from the input array since it has been assigned to $id variable
        $input['is_deleted']= 1;
        $input['updated_by']= $this->input['updated_by'];
        $input['updated_at']= now()->toDateTimeString();
        return $linkedServerRepo->edit($linkedId, $input);
    }
}
