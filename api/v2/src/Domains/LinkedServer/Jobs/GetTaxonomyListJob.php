<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\CurlHelper;

use App\Data\Repositories\Contracts\LinkedServerRepositoryInterface;


class GetTaxonomyListJob extends Job
{
    use CurlHelper;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LinkedServerRepositoryInterface $linkedServer)
    {
        //
        $linkedServerId = $this->input['linked_server_id'];
        $linkServerArr = $linkedServer->find($linkedServerId);
        $apiUrl = $linkServerArr->server_url;
        $endPoint = $apiUrl."/CFDocuments";
        $serverReponse = $this->checkUrl($endPoint,"GET"); 
        if($serverReponse['error_no'] == 0)
        {
            $cfdocuments = json_decode($serverReponse['result']);
            if(isset($cfdocuments->CFDocuments))
            {
                $listOFtaxonomy = array();
                foreach($cfdocuments->CFDocuments as $cfdocument)
                {
                    $listOFtaxonomy[] = [
                        'taxonomy_name' => $cfdocument->title,
                        'document_id' => $cfdocument->identifier
                    ];
                }
                return $listOFtaxonomy;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        } 
        
        
    }
}
