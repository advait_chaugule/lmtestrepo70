<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LinkedServerRepositoryInterface;


class UpdateLinkedServerInputJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LinkedServerRepositoryInterface $linkedServer)
    {
        
        $id = $this->input['linked_server_id'];
        $linkeServerArr["linked_type"] = $this->input['linked_type'];
        $linkeServerArr["linked_name"] = $this->input['linked_name'];
        $linkeServerArr["description"] = $this->input['description'];
        $linkeServerArr["server_url"] = $this->input['server_url'];
        $linkeServerArr["updated_at"] = $this->input['updated_at'];
        $linkeServerArr["updated_by"] = $this->input['updated_by'];
        $linkeServerArr["connection_status"] = $this->input['connection_status'];
        
        return $linkedServer->edit($id, $linkeServerArr);
       
    }
}
