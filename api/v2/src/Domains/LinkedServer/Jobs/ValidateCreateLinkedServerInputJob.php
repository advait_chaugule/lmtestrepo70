<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Models\LinkedServer;

class ValidateCreateLinkedServerInputJob extends Job
{
    protected $requestObject;
    protected $organizationId;
    protected $dataToValidate;
    protected $errorMessagesAvailable;
    protected $errorMessagesToSet;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->dataToValidate = $requestData;
        $this->errorMessagesAvailable = [
            "required" => [
                "linked_name" => "Server name is mandatory.",
                "server_url" => "Api url is mandatory."
            ],
            "in" => [
                "linked_type" => "linked_type is invalid."
            ],
            "exists" => [
                "linked_name" => "Server name is already used in Current Tenant.",
                "server_url" => "Api url is already used in Current Tenant."
            ],
        ];
        $this->errorMessagesToSet = [];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        

         $linkedTypeRequiredStatus = $this->getRequiredStatusForAttribute("linked_type");
        $this->setErrorMessageOfValidationTypeForAttribute($linkedTypeRequiredStatus, "required", "linked_type");
         $linkedTypeInStatus = $linkedTypeRequiredStatus ? $this->getInStatusForLinkedTypeAndSetErrorMessageAccordingly() : false;

         $linkedNameRequiredStatus = $this->getRequiredStatusForAttribute("linked_name");
                                    $this->setErrorMessageOfValidationTypeForAttribute($linkedNameRequiredStatus, "required", "linked_name");
                                     
        $linkedNameExistsStatus = $linkedNameRequiredStatus ? $this->getItemLinkedNameExistsStatusAndSetErrorMessageAccordingly() : false;

        $serverUrlRequiredStatus = $this->getRequiredStatusForAttribute("server_url");
        $this->setErrorMessageOfValidationTypeForAttribute($serverUrlRequiredStatus, "required", "server_url");
         
        $serverUrlExistsStatus = $serverUrlRequiredStatus ? $this->getItemServerUrlExistsStatusAndSetErrorMessageAccordingly() : false;

        $overallStatus = $linkedTypeInStatus && $linkedNameRequiredStatus && $linkedNameExistsStatus && 
        $serverUrlRequiredStatus && $serverUrlExistsStatus;

$jobResponse = $overallStatus===true ? $overallStatus : implode(",", $this->errorMessagesToSet);

return $jobResponse;
                                    
    }

    private function getRequiredStatusForAttribute(string $attribute): bool {
        return !empty($this->dataToValidate[$attribute]);
    }

    private function setErrorMessageOfValidationTypeForAttribute(bool $validationStatus, string $validationType, string $attribute) {
        if($validationStatus===false) {
            $messageToPush = !empty($this->errorMessagesAvailable[$validationType][$attribute]) ?
                             $this->errorMessagesAvailable[$validationType][$attribute] : 
                             "";
            if(!empty($messageToPush)) {
                $this->errorMessagesToSet[] = $messageToPush;
            }
        }
    }

    private function getInStatusForLinkedTypeAndSetErrorMessageAccordingly(): bool {
        $allowedInValues = [1, 2];
        $status = in_array($this->dataToValidate["linked_type"], $allowedInValues);
        if($status===false) {
            $this->setErrorMessageOfValidationTypeForAttribute(false, "in", "linked_type");
        } 
        return $status;
    }

    private function getItemLinkedNameExistsStatusAndSetErrorMessageAccordingly() : bool {

        $organizationId = $this->dataToValidate["organization_id"];
        $linkedName = $this->dataToValidate["linked_name"];
        
       if($this->dataToValidate["is_create"] == 1)
       {
        $conditionalClause = [[ "organization_id" , "=", $organizationId ], ["linked_name" , "=" , $linkedName], ["is_deleted" , "=" , "0"]];
       }
       else
       {
        $linkedServerId = $this->dataToValidate["linked_server_id"];
        $conditionalClause = [[ "organization_id" , "=", $organizationId ], ["linked_name" , "=" , $linkedName], ["is_deleted" , "=" , "0"], ["linked_server_id", "!=", $linkedServerId]];
       }
        $recordCount = LinkedServer::where($conditionalClause)->count();
        $status = $recordCount > 0 ? false : true;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "linked_name");
        return $status;
    }


    private function getItemServerUrlExistsStatusAndSetErrorMessageAccordingly() : bool {

        $organizationId = $this->dataToValidate["organization_id"];
        $serverUrl = $this->dataToValidate["server_url"];
       
        if($this->dataToValidate["is_create"] == 1)
       {
            $conditionalClause = [[ "organization_id" , "=", $organizationId ], ["server_url" , "=" , $serverUrl], ["is_deleted" , "=" , "0"]];
       }
       else
       {
        $linkedServerId = $this->dataToValidate["linked_server_id"];
        $conditionalClause = [[ "organization_id" , "=", $organizationId ], ["server_url" , "=" , $serverUrl], ["is_deleted" , "=" , "0"], ["linked_server_id", "!=", $linkedServerId]];
       }
        $recordCount = LinkedServer::where($conditionalClause)->count();
        $status = $recordCount > 0 ? false : true;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "server_url");
        return $status;
    }

}
