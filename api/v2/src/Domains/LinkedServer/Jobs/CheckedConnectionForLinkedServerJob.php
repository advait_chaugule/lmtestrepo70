<?php
namespace App\Domains\LinkedServer\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\CurlHelper;


class CheckedConnectionForLinkedServerJob extends Job
{
    use CurlHelper;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
         $endPoint = $this->input['server_url']."/CFDocuments";
         $serverReponse = $this->checkUrl($endPoint,"GET");  
         if($serverReponse['error_no'] == 0)
         {
            $obj = json_decode($serverReponse['result']);
            if(isset($obj->CFDocuments))
            {
                return true;
            }
            else
            {
               return false;
            }  
         }
         else
         {
            return false;
         }  
    }
}
