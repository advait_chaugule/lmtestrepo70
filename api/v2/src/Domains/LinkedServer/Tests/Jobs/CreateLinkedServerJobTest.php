<?php
namespace App\Domains\LinkedServer\Tests\Jobs;

use App\Domains\LinkedServer\Jobs\CreateLinkedServerJob;
use Tests\TestCase;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Illuminate\Http\Request;
use App\Services\Api\Traits\StringHelper;

use Illuminate\Support\Facades\DB;

use App\Services\Api\Traits\UuidHelperTrait;


class CreateLinkedServerJobTest extends TestCase
{
    use UuidHelperTrait,DatabaseMigrations;

 

    public function setUp() {
      parent::setUp();
      // run seeder if required after environment has been setup
  Artisan::call('db:seed');
          $this->linkedServerRepository = resolve('App\Data\Repositories\Contracts\LinkedServerRepositoryInterface');
    }
    public function test_create_linked_server_job()
    {
     
   
      $linkeServerArr["linked_server_id"] = "9d6775bf-0d09-4616-b90c-b49ddae0ff75"; 
      $linkeServerArr["linked_type"] = "1";  
      $linkeServerArr["linked_name"] = "Texas Gateway"; 
      $linkeServerArr["description"] =  "Texas Gateway"; 
      $linkeServerArr["server_url"] = "https://teks-api.texasgateway.org/ims/case/v1p0"; 
      $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";
      $linkeServerArr["created_at"] = "2018-12-17 10:09:36";
      $linkeServerArr["updated_at"] = "2018-12-17 10:09:36";
      $linkeServerArr["created_by"] = "4fa46dbd-e314-40cb-9439-929b94779fd6";
      $linkeServerArr["updated_by"] = "4fa46dbd-e314-40cb-9439-929b94779fd6";
      $linkeServerArr["is_deleted"] = 0;
      
      $obj = new CreateLinkedServerJob($linkeServerArr);
      
      $jobResponse = $obj->handle($this->linkedServerRepository);

     
      $this->assertTrue($jobResponse->count()===1);

    }
}
