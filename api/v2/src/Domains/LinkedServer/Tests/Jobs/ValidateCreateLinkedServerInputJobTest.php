<?php
namespace App\Domains\LinkedServer\Tests\Jobs;

use App\Domains\LinkedServer\Jobs\ValidateCreateLinkedServerInputJob;
use Tests\TestCase;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Illuminate\Http\Request;
use App\Services\Api\Traits\StringHelper;

use Illuminate\Support\Facades\DB;


class ValidateCreateLinkedServerInputJobTest extends TestCase
{

    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();
        // run seeder if required after environment has been setup
    Artisan::call('db:seed');
    }

    public function test_validate_create_linked_server_input_job()
    {
        $linkeServerArr["is_create"] = "1";
        $linkeServerArr["linked_type"] = "3";
       $linkeServerArr["linked_name"] = "Open Salt";
       $linkeServerArr["description"] = "";
       $linkeServerArr["server_url"] = "https://opensalt.net/ims/case/v1p0";
       $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";

       $obj = new ValidateCreateLinkedServerInputJob($linkeServerArr);

       $validateLinkedServerInput = $obj->handle();
        
      // For Link Type must be in 1 0r 2
       $this->assertContains('linked_type is invalid.', $validateLinkedServerInput);
       unset($linkeServerArr);
       unset($obj);
       unset($validateLinkedServerInput);

       $linkeServerArr["is_create"] = "1";
       $linkeServerArr["linked_type"] = "2";
       $linkeServerArr["linked_name"] = "";
       $linkeServerArr["description"] = "";
       $linkeServerArr["server_url"] = "https://opensalt.net/ims/case/v1p0";
       $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";

       $obj = new ValidateCreateLinkedServerInputJob($linkeServerArr);

       $validateLinkedServerInput = $obj->handle();
        
     //  $this->assertTrue($validateCreateAssetInput);
      // For Server Name is mandatory

       $this->assertContains('Server name is mandatory.', $validateLinkedServerInput);

       unset($linkeServerArr);
       unset($obj);
       unset($validateLinkedServerInput);

       $linkeServerArr["is_create"] = "1";
       $linkeServerArr["linked_type"] = "2";
       $linkeServerArr["linked_name"] = "Open Salt";
       $linkeServerArr["description"] = "";
       $linkeServerArr["server_url"] = "";
       $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";

       $obj = new ValidateCreateLinkedServerInputJob($linkeServerArr);

       $validateLinkedServerInput = $obj->handle();
        
     //  $this->assertTrue($validateCreateAssetInput);
      // For Api url is mandatory
       $this->assertContains('Api url is mandatory.', $validateLinkedServerInput);

       unset($linkeServerArr);
       unset($obj);
       unset($validateLinkedServerInput);

       $linkeServerArr["is_create"] = "1";
       $linkeServerArr["linked_type"] = "1";
       $linkeServerArr["linked_name"] = "Open Salt";
       $linkeServerArr["description"] = "";
       $linkeServerArr["server_url"] = "https://opensalt.net/ims/case/v1p0";
       $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";

       $obj = new ValidateCreateLinkedServerInputJob($linkeServerArr);

       $validateLinkedServerInput = $obj->handle();
        
     //  
      // For Server Name is mandatory
      
      $this->assertTrue($validateLinkedServerInput);

       unset($linkeServerArr);
       unset($obj);
       unset($validateLinkedServerInput);

//

$data = array(
  array("linked_server_id" => '9d6775bf-0d09-4616-b90c-b49ddae0ff75',"organization_id" => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',"linked_name" => 'Texas Gateway',"server_url" => 'https://teks-api.texasgateway.org/ims/case/v1p0',"description" => 'Texas Gateway',"linked_type" => '1',"connection_status" => '1',"is_deleted" => '0',"created_by" => '4fa46dbd-e314-40cb-9439-929b94779fd6',"updated_by" => '4fa46dbd-e314-40cb-9439-929b94779fd6',"created_at" => '2018-12-17 10:09:36','updated_at' => '2018-12-17 10:09:36')
);
    DB::table('linked_servers')->insert($data);


    $linkeServerArr["is_create"] = "1";
    $linkeServerArr["linked_server_id"] = "9d6775bf-0d09-4616-b90c-b49ddae0ff75";
    $linkeServerArr["linked_type"] = "1";
    $linkeServerArr["linked_name"] = "Texas Gateway";
    $linkeServerArr["description"] = "";
    $linkeServerArr["server_url"] = "http://acmt-dev.learningmate.com/server/api/v1/ims/case/v1p0";
    $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";

    $obj = new ValidateCreateLinkedServerInputJob($linkeServerArr);

    $validateLinkedServerInput = $obj->handle();
  
  //  $this->assertTrue($validateCreateAssetInput);
   // For Api url is mandatory
    //$this->assertContains('Server name is already used in Current Tenant.', $validateLinkedServerInput);

    unset($linkeServerArr);
    unset($obj);
    unset($validateLinkedServerInput);


    $linkeServerArr["is_create"] = "1";
    $linkeServerArr["linked_server_id"] = "9d6775bf-0d09-4616-b90c-b49ddae0ff75";
    $linkeServerArr["linked_type"] = "1";
    $linkeServerArr["linked_name"] = "ACMT-QA";
    $linkeServerArr["description"] = "";
    $linkeServerArr["server_url"] = "https://teks-api.texasgateway.org/ims/case/v1p0";
    $linkeServerArr["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";

    $obj = new ValidateCreateLinkedServerInputJob($linkeServerArr);

    $validateLinkedServerInput = $obj->handle();
     
  //  $this->assertTrue($validateCreateAssetInput);
   // For Api url is mandatory
    $this->assertContains('Api url is already used in Current Tenant.', $validateLinkedServerInput);

    unset($linkeServerArr);
    unset($obj);
    unset($validateLinkedServerInput);


    }
}
