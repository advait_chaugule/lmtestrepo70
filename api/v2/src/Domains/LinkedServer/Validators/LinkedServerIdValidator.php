<?php
namespace App\Domains\LinkedServer\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class LinkedServerIdValidator extends BaseValidator {

    protected $rules = [
        'linked_server_id'=>'required|exists:linked_servers,linked_server_id,is_deleted,0',        
    ];

     protected $messages = [
        'linked_server_id' => ':attribute is required.'
     ];
    
}