<?php
namespace App\Domains\CFEntityType\Jobs;

use Lucid\Foundation\Job;

class UpdateInputWithCFEntityTypeJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->input['cf_entity_type'] = (!empty($this->input['parent_id'])) ? "cf_item" : "cf_doc";
        return $this->input;
    }
}
