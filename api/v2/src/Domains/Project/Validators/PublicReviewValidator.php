<?php
namespace App\Domains\Project\Validators;
use Illuminate\Validation\ValidationException;
use App\Foundation\BaseValidator;

class PublicReviewValidator extends BaseValidator
{
    protected $rules = [
        'project_id'      => 'required|exists:projects,project_id',
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
    ];
}