<?php
namespace App\Domains\Project\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class InputsForAddingNodesUnderProjectValidator extends BaseValidator {

    protected $rules = [
        'project_id' => 'required|exists:projects',
        'document_id' => 'required|exists:documents',
        'item_id' => 'nullable',
        'parent_id' => 'nullable',
        'root_selected_status' => 'required|in:1,0'
    ];

    protected $messages = [];
    
}