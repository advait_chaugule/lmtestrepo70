<?php
namespace App\Domains\Project\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateProjectValidator extends BaseValidator {

    protected $rules = [
        'project_name' => 'required',
        'workflow_id' => 'required|exists:workflows'
    ];

     protected $messages = [
        'required'  =>  ":attribute is required.",
        'exists:workflows' => ':attribute is invalid.'
     ];
    
}