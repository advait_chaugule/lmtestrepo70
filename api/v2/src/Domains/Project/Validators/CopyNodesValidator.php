<?php
namespace App\Domains\Project\Validators;
use Illuminate\Validation\ValidationException;
use App\Foundation\BaseValidator;

class CopyNodesValidator extends BaseValidator
{
    protected $rules = [
        'origin_node_id'      => 'required|exists:items,item_id',
        'project_id'          => 'required|exists:projects',
      //  'node_ids_in_csv'     => 'required|array|exists:items,item_id'

    ];

    protected $messages = [
        "required" => ":attribute is required.",
      //  "array"    => ":attribute must be a array",
    ];
}