<?php
namespace App\Domains\Project\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ProjectWorkflowStageValidator extends BaseValidator {

    protected $rules = [
        'project_id' => 'required|exists:projects',
        'workflow_stage_id' => 'required|exists:workflow_stage'
    ];

     protected $messages = [
        'required'  =>  ":attribute is required.",
        'exists:workflow_stage' => ':attribute is invalid.'
     ];
    
}