<?php
namespace App\Domains\Project\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateAndUploadProjectSearchDataValidator extends BaseValidator {

    protected $rules = [
        'project_ids' => 'required',
        // 'called_from_http_client' => 'required|in:1,0'
    ];

     protected $messages = [];
    
}