<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;
use App\Data\Models\Project;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class DeleteAssociatedNodesJob extends Job
{
    private $projectIdentifier;
    private $documentIdentifier;
    private $organizationIdentifier;

    private $projectRepository;
    private $documentRepository;
    private $itemRepository;

    private $itemNodeIdsToDelete;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier, string $documentIdentifier, array $requestData)
    {
        //Set the private variables
        $this->setProjectIdentifier($projectIdentifier);
        $this->setDocumentIdentifier($documentIdentifier);

        $this->setOrganizationIdentifier($requestData['auth_user']['organization_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository, DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository)
    {
        //Set the repo handlers
        $this->projectRepository    =   $projectRepository;
        $this->documentRepository   =   $documentRepository;
        $this->itemRepository       =   $itemRepository;

        $this->fetchAndSetProject();
        $this->fetchAndSetItemNodeIdsToDelete();

        
            $this->deleteItemNodesProjectMapping();
            $this->deleteDocumentNodeProjectMapping();
        
    }

    /**
     * Public Setter and Getter methods
     */
    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier    =   $identifier;
    }

    public function getProjectIdentifier() {
        return $this->projectIdentifier;
    }

    public function setDocumentIdentifier(string $identifier) {
        $this->documentIdentifier    =   $identifier;
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier(string $identifier) {
        $this->organizationIdentifier    =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setProject(Project $data) {
        $this->project = $data;
    }

    public function getProject(): Project {
        return $this->project;
    }

    public function setItemNodeIdsToDelete(array $itemNodeIdsToDelete) {
        $this->itemNodeIdsToDelete  =   $itemNodeIdsToDelete;
    }

    public function getItemNodeIdsToDelete(){
        return $this->itemNodeIdsToDelete;
    }

    private function fetchAndSetProject() {
        $projectIdentifier = $this->getProjectIdentifier();
        $project = $this->projectRepository->find($projectIdentifier);
        $this->setProject($project);
    }

    private function fetchAndSetItemNodeIdsToDelete() {
        $arrayOfSelectedNodes   =   [];
        $project                =   $this->getProject();
        $projectIdentifier      =   $this->getProjectIdentifier();
        $documentId             =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_id'];
        $keyValuePairs      =   ['document_id'  =>  $documentId, 'organization_id'  =>  $organizationIdentifier];
        
        $itemForDocumentSelected = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs);

        $getMappedNodes =   $this->projectRepository->getMappedNodes($projectIdentifier);

        foreach($itemForDocumentSelected as $item) {
            foreach($getMappedNodes as $mappedNodes) {
                if($item['item_id'] == $mappedNodes['item_id']) {
                    array_push($arrayOfSelectedNodes, $item['item_id']);
                }   
            }
        }

        $this->setItemNodeIdsToDelete($arrayOfSelectedNodes);

    }

    private function deleteItemNodesProjectMapping() {
        $itemNodeIdsToDelete    = $this->getItemNodeIdsToDelete();

        foreach($itemNodeIdsToDelete as $deleteItem) {
            $this->projectRepository->deleteProjectItem($deleteItem);
        }
    }

    private function deleteDocumentNodeProjectMapping() {
        $project                =   $this->getProject();
        $projectIdentifier      =   $this->getProjectIdentifier();
        $documentId             =   $this->getDocumentIdentifier();

        $project->prefDocument()->detach($documentId);

    }
}
