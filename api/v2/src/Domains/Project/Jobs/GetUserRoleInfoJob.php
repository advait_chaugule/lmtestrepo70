<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class GetUserRoleInfoJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input, $requestData)
    {
        //Set the private identifier attribute
        $this->setIdentifier($input);
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository,UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $getUserProjectRoleInfo = $this->getUserProjectRoleInfo();

        return $getUserProjectRoleInfo;

    }

    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    private function getUserDetail() {
        $userIdentifier = $this->getUserIdentifier();
        $userDetail = $this->userRepository->find($userIdentifier)->toArray();

        $userEntity = [
            'user_name' => (!empty($userDetail['first_name']) ? $userDetail['first_name'] : " ").' '.(!empty($userDetail['last_name']) ? $userDetail['last_name'] : " "),
            'user_email' => !empty($userDetail['email']) ? $userDetail['email'] : " ",
            'user_id' => !empty($userDetail['user_id']) ? $userDetail['user_id'] : " ",
            'current_organization_id' => !empty($userDetail['current_organization_id']) ? $userDetail['current_organization_id'] : " ",

        ];
        return $userEntity;
    }

    private function getRoleName() {
        $roleIdentifier = $this->getRoleIdentifier();

        $roleDetail = $this->roleRepository->find($roleIdentifier)->toArray();

        return !empty($roleDetail['name']) ? $roleDetail['name'] : " ";
    }

    private function getProjectName() {
        $projectIdentifier = $this->getIdentifier();

        $projectDetail = $this->projectRepository->find($projectIdentifier)->toArray();

        return !empty($projectDetail['project_name']) ? $projectDetail['project_name'] : " ";
    }

    private function getUserProjectRoleInfo(){
        $projectIdentifier = $this->getIdentifier();
        $requestData = $this->requestData;
        $user_id = $requestData['user_id'];
        $this->setUserIdentifier($user_id);
        $this->setRoleIdentifier(explode('||', $requestData['workflow_stage_role_id'])[2]);

        $userEntity = $this->getUserDetail();
        $roleEntity = $this->getRoleName();
        $projectEntity = $this->getProjectName();

        $projectRoleEntity[] = [
            'user_name' => $userEntity['user_name'],
            'user_id' => $userEntity['user_id'],
            'current_organization_id' => $userEntity['current_organization_id'],
            'user_email' => $userEntity['user_email'],
            'role_name' => $roleEntity,
            'project_name' => $projectEntity
        ];
        return $projectRoleEntity;
       
    }

    public function setUserIdentifier($userId) {
        $this->userId = $userId;
    }

    public function getUserIdentifier(){
        return $this->userId;
    }

    public function setRoleIdentifier($roleId) {
        $this->roleId = $roleId;
    }

    public function getRoleIdentifier(){
        return $this->roleId;
    }


}
