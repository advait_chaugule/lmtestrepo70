<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ValidateProjectForPublishJob extends Job
{
    private $projectIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier, string $organizationId)
    {
        $this->projectIdentifier = $projectIdentifier;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $repo)
    {
        $dataToValidate = [ "project_id" => $this->projectIdentifier,  "organization_id" => $this->organizationId, "is_deleted" => '0', "adoption_status" => '7'];
        $pacingGuide       = $repo->findByAttributes($dataToValidate);
        return $pacingGuide->isNotEmpty();
    }
}
