<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use DB;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Support\Facades\Storage;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;

class CopyNodeJob extends Job
{
    use ArrayHelper, CaseFrameworkTrait, DateHelpersTrait, UuidHelperTrait;

    private $itemRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $domainName;
    public function __construct(array $copyNodes,array $nodesHierarchy,string $orginiNodeId,string $projectId,string $hierarchyCopy,string $documentId, $iscopy,$isCopyAsso,$userReq,$requestUrl='')
    {
        $this->copyNodes = $copyNodes;
        $this->nodesHierarchy = $nodesHierarchy;
        $this->orginiNodeId = $orginiNodeId;
        $this->projectId = $projectId;
        $this->hierarchyCopy = $hierarchyCopy;
        $this->documentId =  DB::table('items')->select('document_id')->where('is_deleted',0)->where('item_id',$orginiNodeId)->first()->document_id;

        $this->iscopy = $iscopy;
        $this->isCopyAsso = $isCopyAsso;
        $this->requestingUserDetails = $userReq;
        $this->userId = $userReq['user_id'];
        $this->organizationId = $userReq['organization_id'];
        $organizationCode       =   DB::table('organizations')->select('org_code')->where('organization_id', $userReq['organization_id'])->first()->org_code;

        $this->organizationCode = $organizationCode;
        $this->caseAssociationsToSave = [];
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
        if($this->hierarchyCopy==1)
        {
            if(count($this->nodesHierarchy) == 0)
            {
                $this->getNodeHierarchy($this->copyNodes);
            }


            $nodeArrWithChild = $this->getNodesArrWithChild($this->nodesHierarchy);
            $this->updateCopyNodesForHierachy();
        }
        else
        {
            $nodeArrWithChild = $this->copyNodes;
        }
        $this->getAllItemCaseData($nodeArrWithChild);
        $this->getAllItemCustomData($nodeArrWithChild);
        $this->getAllItemAssetData($nodeArrWithChild);
        $this->getAllItemExemplarData($nodeArrWithChild);
        $this->getlastAssociationcntInParent();
        if($this->hierarchyCopy==1)
        {
            $this->getSeqNumberForNewCopyNodes();
        }
        else
        {
            $this->getSeqNumberForCopyNodes();
        }

        $this->getAllItemRelationWithParent();
        $this->getRemainingChildItemAssociation();
        if($this->iscopy==1)
        {
            $this->getAllItemRelationWithOriginal();
        }
        if($this->isCopyAsso==1)
        {
            $this->getAllItemAssociation($nodeArrWithChild);
        }
        $this->getAllItemForProject();
        $this->InsertAllDataForCopy();
        return $this->createItemArr;
    }

    /**
     * Get all the child's from the item id
     */

    public function getNodesArrWithChild($nodeArrWithChild)
    {
        $nodeArrChild = [];
        foreach($nodeArrWithChild as $nodeArrWithChildV)
        {
            array_push($nodeArrChild,$nodeArrWithChildV['item_id']);
        }

        return array_unique($nodeArrChild);
    }

    /**
     * Get all case data from item id
     */
    public function getAllItemCaseData($nodeArrWithChild)
    {
        $getItemCase = DB::table('items')
            ->select('items.*')
            ->where('is_deleted',0)
            ->whereIn('item_id',$nodeArrWithChild)
            ->get()->toArray();

        /**
         * This logic is implemented for UF-1400 to handle parent id of item which is pass for copy nodes
         * Developer - Amey Joshi
         * Date - 02/06/2020
         *  */    
        $getItemParent = DB::table('item_associations')
            ->select('target_item_id', 'source_item_id')
            ->where('is_deleted', 0)
            ->where('association_type', 1)
            ->whereIn('source_item_id', $nodeArrWithChild)
            ->groupBy('source_item_id')
            ->havingRaw('min(created_at)')
            ->get()->toArray();

        $parentIdForOriginalId  =   [];
        $createItemArr = [];
        $createConceptArr = [];
        $createLincenseArr = [];
        $createNodeTypeArr = [];
        $conceptArr = [];
        $licenseArr = [];
        $nodTypeArr = [];
        $createConceptURI = [];
        $createLicenseURI = [];
        $createNodeTypeURI = [];
        $parentIdForItemId = [];
        $parentOfItem = "";

        /**
         * Create Array for stored source item as item id and target item id as parent id for used in paren id relation ship in copy nodes
         * Developer - Amey Joshi
         * Date - 02/06/2020
         */
        foreach($getItemParent as $getItemParentK=>$getItemParentV)
        {
            $parentIdForItemId[$getItemParentV->source_item_id] = $getItemParentV->target_item_id;
        }

        foreach($getItemCase as  $getItemCaseK=> $getItemCaseV)
        {
            // only if condition is mention because blank valuse is already set before loop not required else condtion 
            if (array_key_exists($getItemCaseV->item_id,$parentIdForItemId))
            {
                $parentOfItem = $parentIdForItemId[$getItemCaseV->item_id];
            }
            
            // logic is change for UF-1400 earlier logic was used of parent_id from item table
            $parentIdForOriginalId[$getItemCaseV->item_id] = $parentOfItem;
            $itemIdentifier =   $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFItems", $itemIdentifier, true, $this->organizationCode,$this->domainName);

            $createItemArr[$getItemCaseV->item_id] = [
                "item_id" => $itemIdentifier,
                "parent_id" => '',
                "document_id" => $this->documentId,
                "full_statement" => $getItemCaseV->full_statement,
                "alternative_label" => $getItemCaseV->alternative_label,
                "item_type_id" => $getItemCaseV->item_type_id,
                "human_coding_scheme" => $getItemCaseV->human_coding_scheme,
                "list_enumeration" => $getItemCaseV->list_enumeration,
                "abbreviated_statement" => $getItemCaseV->abbreviated_statement,
                "concept_id" => $getItemCaseV->concept_id,
                "notes" => $getItemCaseV->notes,
                "language_id" => $getItemCaseV->language_id,
                "education_level" => $getItemCaseV->education_level,
                "license_id" => $getItemCaseV->license_id,
                "source_license_uri_object" => '',
                "status_start_date" => $getItemCaseV->status_start_date,
                "status_end_date" => $getItemCaseV->status_end_date,
                "is_deleted" => 0,
                "updated_by" => '',
                "organization_id" => $this->organizationId,
                "source_item_id" => $itemIdentifier,/* To show new generated UUID for the copied node resolving  ACMT-2812 *///$getItemCaseV->item_id,
                "node_type_id" =>  $getItemCaseV->node_type_id,
                "created_at" =>  now()->toDateTimeString(),
                "updated_at" =>  now()->toDateTimeString(),
                "uri" => $uri,
                "source_node_type_uri_object" => '',
                "source_concept_uri_object" => '',
                "full_statement_html" => $getItemCaseV->full_statement_html,
                "alternative_label_html" => $getItemCaseV->alternative_label_html,
                "human_coding_scheme_html" =>  $getItemCaseV->human_coding_scheme_html,
                "abbreviated_statement_html" =>  $getItemCaseV->abbreviated_statement_html,
                "notes_html" =>  $getItemCaseV->notes_html,
                "custom_view_visibility" => $getItemCaseV->custom_view_visibility,
                "custom_view_data" =>  $getItemCaseV->custom_view_data,
                "import_type" => 2
            ];
            if($getItemCaseV->concept_id != "")
            {
                array_push($conceptArr,$getItemCaseV->concept_id);
            }
            if($getItemCaseV->license_id != "")
            {
                array_push($licenseArr,$getItemCaseV->license_id);
            }
            if($getItemCaseV->node_type_id != "")
            {
                array_push($nodTypeArr,$getItemCaseV->node_type_id);
            }
        }

        $this->parentIdForOriginalId = $parentIdForOriginalId;

        $getItemConcepts = DB::table('concepts')
            ->select('concepts.*')
            ->whereIn('concept_id',$conceptArr)
            ->get()->toArray();
        foreach($getItemConcepts as  $getItemConceptsK=> $getItemConceptsV)
        {
            $newConceptId = $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFConcepts", $newConceptId, true, $this->organizationCode,$this->domainName);
            $createConceptArr[$getItemConceptsV->concept_id] =
                [
                    "concept_id" => $newConceptId,
                    "title" => $getItemConceptsV->title,
                    "keywords" => $getItemConceptsV->keywords,
                    "hierarchy_code" => $getItemConceptsV->hierarchy_code,
                    "description" => $getItemConceptsV->description,
                    "is_deleted" => $getItemConceptsV->is_deleted,
                    "source_concept_id" => $getItemConceptsV->concept_id,
                    "organization_id" => $this->organizationId,
                    "created_at" => now()->toDateTimeString(),
                    "updated_at" => now()->toDateTimeString(),
                    "uri" => $uri,
                    "title_html" => $getItemConceptsV->title_html,
                    "description_html" => $getItemConceptsV->description_html,
                ];
            $createConceptURI[$getItemConceptsV->concept_id] =
                [
                    "identifier" => $newConceptId,
                    "uri" => $uri,
                    "title" => $getItemConceptsV->keywords
                ];
        }
        $getItemLicenses = DB::table('licenses')
            ->select('licenses.*')
            ->whereIn('license_id',$licenseArr)
            ->get()->toArray();

        foreach($getItemLicenses as  $getItemLicensesK=> $getItemLicensesV)
        {
            $newLicenseId = $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFLicenses", $newLicenseId, true, $this->organizationCode,$this->domainName);
            $createLincenseArr[$getItemLicensesV->license_id] =
                [
                    "license_id" => $newLicenseId,
                    "title" => $getItemLicensesV->title,
                    "description" => $getItemLicensesV->description,
                    "license_text" => $getItemLicensesV->license_text,
                    "is_deleted" => $getItemLicensesV->is_deleted,
                    "source_license_id" => $getItemLicensesV->license_id,
                    "organization_id" => $this->organizationId,
                    "created_at" => now()->toDateTimeString(),
                    "updated_at" => now()->toDateTimeString(),
                    "uri" => $uri,
                    "title_html" => $getItemLicensesV->title_html,
                    "description_html" => $getItemLicensesV->description_html,
                    "license_text_html" => $getItemLicensesV->license_text_html,
                ];
            $createLicenseURI[$getItemLicensesV->license_id] =
                [
                    "identifier" => $newLicenseId,
                    "uri" => $uri,
                    "title" => $getItemLicensesV->title
                ];

        }

        $getItemNodeType = DB::table('node_types')
            ->select('node_types.*')
            ->whereIn('node_type_id',$nodTypeArr)
            ->where('used_for',0)
            ->get()->toArray();

        foreach($getItemNodeType as  $getItemNodeTypeK=> $getItemNodeTypeV)
        {
            $uri = $this->getCaseApiUri("CFItemTypes", $getItemNodeTypeV->node_type_id, true, $this->organizationCode,$this->domainName);
            $createNodeTypeArr[$getItemNodeTypeV->node_type_id] =
                [
                    "node_type_id" => $getItemNodeTypeV->node_type_id,
                    "organization_id" => $getItemNodeTypeV->organization_id,
                    "title" => $getItemNodeTypeV->title,
                    "type_code" => $getItemNodeTypeV->type_code,
                    "hierarchy_code" => $getItemNodeTypeV->hierarchy_code,
                    "description" => $getItemNodeTypeV->description,
                    "source_node_type_id" => $getItemNodeTypeV->source_node_type_id,
                    "is_document" => $getItemNodeTypeV->is_document,
                    "is_custom" => $getItemNodeTypeV->is_custom,
                    "is_deleted" => $getItemNodeTypeV->is_deleted,
                    "is_default" => $getItemNodeTypeV->is_default,
                    "created_by" => $getItemNodeTypeV->created_by,
                    "updated_by" => $getItemNodeTypeV->updated_by,
                    "created_at" => $getItemNodeTypeV->created_at,
                    "updated_at" => $getItemNodeTypeV->updated_at,
                    "uri" => $getItemNodeTypeV->uri,
                ];
            $createNodeTypeURI[$getItemNodeTypeV->node_type_id] =
                [
                    "identifier" => $getItemNodeTypeV->node_type_id,
                    "uri" => $uri,
                    "title" => $getItemNodeTypeV->title
                ];
        }
        foreach($createItemArr as $createItemArrK => $createItemArrV)
        {
            if($createItemArrV['concept_id'] != "")
            {
                $searchConceptID = $createItemArrV['concept_id'];
                $createItemArr[$createItemArrK]['concept_id'] = $createConceptArr[$searchConceptID]['concept_id'];
                $createItemArr[$createItemArrK]['source_concept_uri_object'] = json_encode($createConceptURI[$searchConceptID]);
            }
            if($createItemArrV['license_id'] != "")
            {
                $searchLicenseID = $createItemArrV['license_id'];
                $createItemArr[$createItemArrK]['license_id'] = $createLincenseArr[$searchLicenseID]['license_id'];
                $createItemArr[$createItemArrK]['source_license_uri_object'] = json_encode($createLicenseURI[$searchLicenseID]);
            }
            if($createItemArrV['node_type_id'] != "")
            {
                $searchNodeTypeID = $createItemArrV['node_type_id'];
                $createItemArr[$createItemArrK]['source_node_type_uri_object'] = json_encode($createNodeTypeURI[$searchNodeTypeID]);
            }
        }
        $this->createItemArr = $createItemArr;
        $this->createConceptArr = $createConceptArr;
        $this->createLicenseArr = $createLincenseArr;
    }

    /**
     * Get all custom data from item id
     */
    public function getAllItemCustomData($nodeArrWithChild)
    {
        $getItemCustom = DB::table('item_metadata')
            ->select('item_metadata.*')
            ->where('is_deleted',0)
            ->whereIn('item_id',$nodeArrWithChild)
            ->get()->toArray();

        $createCustomItemArr = [];
        $AllItemArr = $this->createItemArr;
        foreach($getItemCustom as $getItemCustomK=>$getItemCustomV)
        {

            $createCustomItemArr[$getItemCustomV->item_id.'||'.$getItemCustomV->metadata_id] = [
                "item_id" => $AllItemArr[$getItemCustomV->item_id]['item_id'],
                "metadata_id" => $getItemCustomV->metadata_id,
                "metadata_value" => $getItemCustomV->metadata_value,
                "is_deleted" => 0,
                "is_additional" =>$getItemCustomV->is_additional,
                "metadata_value_html" => $getItemCustomV->metadata_value_html,
            ];
        }
        $this->createCustomArr = $createCustomItemArr;
    }

    /**
     * Get all asset data with the item_linked_id id
     */
    public function getAllItemAssetData($nodeArrWithChild)
    {
        $getItemAsset = DB::table('assets')
            ->select('assets.*')
            ->where('asset_linked_type','=',2)
            ->whereIn('item_linked_id',$nodeArrWithChild)
            ->get()->toArray();
        $createItemAsset = [];
        $AllItemArr = $this->createItemArr;
        foreach($getItemAsset as $getItemAssetK => $getItemAssetV)
        {
            $newAssetId = $this->createUniversalUniqueIdentifier();
            $name             = $getItemAssetV->asset_target_file_name;
            $ext=$this->get_file_extension($name);
            $targetFile = $newAssetId.'.'.$ext;
            $bucketName = config("filesystems")["disks"]["s3"]["bucket"];
            $path = "ASSET/ITEM/";
            $exists = Storage::disk('s3')->exists($path.$name);
            if($exists)
            {
                if($name!=="")
                {
                    Storage::disk('s3')->copy($path.$name,$path.$targetFile);
                }
                $itemLinkedId = $getItemAssetV->item_linked_id;
                $createItemAsset[$newAssetId] = [
                    "asset_id" =>  $newAssetId,
                    "organization_id" =>  $this->organizationId,
                    "document_id" =>  $this->documentId,
                    "asset_name" =>  $getItemAssetV->asset_name,
                    "asset_target_file_name" =>  $targetFile,
                    "asset_content_type" =>  $getItemAssetV->asset_content_type,
                    "asset_size" =>  $getItemAssetV->asset_size,
                    "title" =>  $getItemAssetV->title,
                    "description" =>  $getItemAssetV->description,
                    "asset_linked_type" =>  $getItemAssetV->asset_linked_type,
                    "item_linked_id" =>  $AllItemArr[$itemLinkedId]['item_id'],
                    "uploaded_at" =>  now()->toDateTimeString(),
                    "asset_order" =>  $getItemAssetV->asset_order,
                    "status" =>  $getItemAssetV->status,
                ];
            }
        }

        $this->createItemAsset = $createItemAsset;
    }

    function getAllItemExemplarData($nodeArrWithChild)
    {
        $AllItemArr = $this->createItemArr;

        $getItemExemplar = DB::table('item_associations')
            ->select('item_associations.*')
            ->where('association_type',8)
            ->whereIn('source_item_id',$nodeArrWithChild)
            ->get()->toArray();

        $createItemExemplar = [];
        $exemplarAssetId = [];
        foreach($getItemExemplar as $getItemExemplarK => $getItemExemplarV)
        {
            $newExemplarId = $this->createUniversalUniqueIdentifier();

            $createItemExemplar[$getItemExemplarV->item_association_id] = [
                "item_association_id"           => $newExemplarId,
                "association_type"              => $getItemExemplarV->association_type,
                "document_id"                   => $this->documentId,
                "association_group_id"          => '',
                "origin_node_id"                => $AllItemArr[$getItemExemplarV->source_item_id]['item_id'],//$getItemExemplarV->source_item_id, the old itemIdentifier was used, hence changed to the created itemIdentifier
                "destination_node_id"           => '',
                "destination_document_id"       => '',
                "sequence_number"               => $getItemExemplarV->sequence_number,
                "external_node_title"           => $getItemExemplarV->external_node_title,
                "external_node_url"             => $getItemExemplarV->external_node_url,
                "description"                   => $getItemExemplarV->description,
                "created_at"                    => now()->toDateTimeString(),
                "updated_at"                    => now()->toDateTimeString(),
                "source_item_association_id"    => $newExemplarId,
                "organization_id"               => $this->organizationId,
                "is_reverse_association"        => $getItemExemplarV->is_reverse_association,
                "uri"                           => !empty($getItemExemplarV->uri) ? $getItemExemplarV->uri : '',
                "has_asset"                     => $getItemExemplarV->description,
                "source_document_id"            => $this->documentId,
                "source_item_id"                => $AllItemArr[$getItemExemplarV->source_item_id]['item_id'],
                "target_document_id"            => '',
                "target_item_id"                => '',
            ];

            if($getItemExemplarV->has_asset == 1)
            {
                array_push($exemplarAssetId,$getItemExemplarV->item_association_id);
            }

        }

        $getExamplarAsset = DB::table('assets')
            ->select('assets.*')
            ->where('asset_linked_type','=',3)
            ->whereIn('item_linked_id',$exemplarAssetId)
            ->get()->toArray();

        $createExemplarAsset = [];
        foreach($getExamplarAsset as $getExamplarAssetK => $getExamplarAssetV)
        {
            $newExemplarAssetId = $this->createUniversalUniqueIdentifier();
            $exemplarname       = $getExamplarAssetV->asset_target_file_name;
            $exemplarext=$this->get_file_extension($exemplarname);
            $exemplartargetFile = $newExemplarAssetId.'.'.$exemplarext;
            $exemplarbucketName = config("filesystems")["disks"]["s3"]["bucket"];
            $exemplarpath = "ASSET/EXEMPLAR/";
            $exemplarexists = Storage::disk('s3')->exists($exemplarpath.$exemplarname);
            if($exemplarexists)
            {
                if($exemplarname!=="")
                {
                    Storage::disk('s3')->copy($exemplarpath.$exemplarname,$exemplarpath.$exemplartargetFile);
                }

                $createExemplarAsset[$newExemplarAssetId] = [
                    "asset_id" =>  $newExemplarAssetId,
                    "organization_id" =>  $this->organizationId,
                    "document_id" =>  $this->documentId,
                    "asset_name" =>  $getExamplarAssetV->asset_name,
                    "asset_target_file_name" =>  $exemplartargetFile,
                    "asset_content_type" =>  $getExamplarAssetV->asset_content_type,
                    "asset_size" =>  $getExamplarAssetV->asset_size,
                    "title" =>  $getExamplarAssetV->title,
                    "description" =>  $getExamplarAssetV->description,
                    "asset_linked_type" =>  $getExamplarAssetV->asset_linked_type,
                    "item_linked_id" =>  $createItemExemplar[$getExamplarAssetV->item_linked_id]['item_association_id'],
                    "uploaded_at" =>  '',
                    "asset_order" =>  $getExamplarAssetV->asset_order,
                    "status" =>  $getExamplarAssetV->status,
                ];
                $baseUrl = env("APP_URL");
                $acmtApiVersionPrefix = "api/v1";
                $endPoint = "asset/preview/exemplar";
                $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
                $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$exemplartargetFile}";
                $external_node_url = $fullyResolvedUri;
                $createItemExemplar[$getExamplarAssetV->item_linked_id]['has_asset'] = 1;
                $createItemExemplar[$getExamplarAssetV->item_linked_id]['external_node_url'] = $external_node_url;
            }
        }
        $this->createItemExemplar = $createItemExemplar;
        $this->createExamplarAsset = $createExemplarAsset;
    }

    function getlastAssociationcntInParent()
    {
        $mainParent_Id = $this->orginiNodeId;
        $getItemAsso = DB::table('item_associations')
            ->select('item_associations.sequence_number')
            ->where('association_type',1)
            ->where('target_item_id',$mainParent_Id)
            ->where('source_document_id',$this->documentId)
            ->orderBy('sequence_number', 'desc')
            ->first();
        if($getItemAsso == "")
        {
            $this->maxSeq = 0;
        }
        else
        {
            $this->maxSeq = $getItemAsso->sequence_number;
        }
    }

    function getSeqNumberForNewCopyNodes()
    {
        $maxSeqCnt = $this->maxSeq;
        $copyNodesArr = $this->copyNodes;
        $copyNodesHierarchyArr =  $this->nodesHierarchy;
        $newItemCreated = $this->createItemArr;
        foreach($copyNodesHierarchyArr as $copyNodesHierarchyArrK => $copyNodesHierarchyArrV)
        {
            if(in_array($copyNodesHierarchyArrV['item_id'],$copyNodesArr))
            {
                $maxSeqCnt = $maxSeqCnt + 1;
                $copyNodesHierarchyArr[$copyNodesHierarchyArrK]['sequence_number'] = $maxSeqCnt;
                $newItemCreated[$copyNodesHierarchyArrV['item_id']]['list_enumeration'] = $maxSeqCnt;
            }

        }
        $this->createItemArr = $newItemCreated;
        $this->nodesHierarchy = $copyNodesHierarchyArr;
    }
    function getAllItemRelationWithParent()
    {
        $caseAssociationsToSave = $this->caseAssociationsToSave;
        $this->remainingChildAsso = [];
        $nodesTobeCopy = $this->copyNodes;
        $nodeTree = array_unique($this->nodesHierarchy, SORT_REGULAR);

        $newItemCreated = $this->createItemArr;
        $nodeAssocTreeArr = [];
        $nodeParentTreeArr = [];
        foreach($nodeTree as $nodeTreeK => $nodeTreeV)
        {
            array_push($nodeAssocTreeArr,$nodeTreeV['item_id']);
        }
        $nodeOldRelation = DB::table('items')
            ->select('items.*')
            ->where('is_deleted',0)
            ->whereIn('item_id',$nodeAssocTreeArr)
            ->get()->toArray();
        foreach($nodeOldRelation as $nodeOldRelationK => $nodeOldRelationV)
        {
            $nodeParentTreeArr[$nodeOldRelationV->item_id] = $nodeOldRelationV->parent_id;
        }
        foreach($nodeTree as $nodeTreeK => $nodeTreeV)
        {
            if( isset($nodeParentTreeArr[$nodeTreeV['item_id']]) && $nodeTreeV['parent_id'] == $nodeParentTreeArr[$nodeTreeV['item_id']])
            {
                if(in_array($nodeTreeV['item_id'],$nodesTobeCopy) && !in_array($nodeTreeV['parent_id'],$nodesTobeCopy)) // Need to check the parent_id in the nodesToCopy array
                {
                    $destinationId = $this->orginiNodeId;
                }else {
                    $parent_id = $nodeTreeV['parent_id'];
                    if(isset($newItemCreated[$parent_id])) {
                        $destinationId = $newItemCreated[$parent_id]['item_id'];
                    }
                }

                $oldItemId = $nodeTreeV['item_id'];
                $originId = $newItemCreated[$oldItemId]['item_id'];
                $originIdNodeTitle = $newItemCreated[$oldItemId]['full_statement'];
                $newItemCreated[$oldItemId]['parent_id'] = $destinationId;
                $sequenceNumber = $nodeTreeV['sequence_number'];
                $itemAssociationId = $this->createUniversalUniqueIdentifier();
                $uri = $this->getCaseApiUri("CFAssociations", $itemAssociationId, true, $this->organizationCode,$this->domainName);
                $externalNodeuri = $this->getCaseApiUri("CFItems", $destinationId, true, $this->organizationCode,$this->domainName);
                // get target document id based on destination node id start
                $documentIdentifier = $this->itemRepository->find($destinationId);
                $targetDocumentIdentifier = $documentIdentifier['document_id'];
                $caseAssociationsToSave[] = [
                    "item_association_id"           => $itemAssociationId,
                    "association_type"              => 1,
                    "document_id"                   => $this->documentId,
                    "association_group_id"          => '',
                    "origin_node_id"                => $originId,
                    "destination_node_id"           => $destinationId,
                    "destination_document_id"       => $this->documentId,
                    "sequence_number"               => $sequenceNumber,
                    "external_node_title"           => $originIdNodeTitle,
                    "external_node_url"             => $externalNodeuri,
                    "created_at"                    => now()->toDateTimeString(),
                    "updated_at"                    => now()->toDateTimeString(),
                    "source_item_association_id"    => $itemAssociationId,
                    "organization_id"               => $this->organizationId,
                    "is_reverse_association"        => 0,
                    "uri"                           => $uri,
                    "description"                   => '', /**added blank to resolve column count issue ofr sql**/
                    "has_asset"                     => 0, /**added 0 to resolve column count issue ofr sql **/
                    "source_document_id"            => $this->documentId,
                    "source_item_id"                => $originId,
                    "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$destinationId,
                    "target_item_id"                => $destinationId,
                ];
            } else {
                array_push($this->remainingChildAsso, $nodeTreeV);
            }
        }
        $this->createItemArr          = $newItemCreated;
        $this->caseAssociationsToSave = $caseAssociationsToSave;
    }

    public function getRemainingChildItemAssociation()
    {
        $caseAssociationsToSave = $this->caseAssociationsToSave;
        $AllItemArr = $this->createItemArr;
        $remainingChildAssoc = $this->remainingChildAsso;
        $remining_item_id = array_column($remainingChildAssoc, 'item_id');
        $remining_item_id_doc = [];

        $oldItemSeqArr = [];
        foreach($remainingChildAssoc as $row) {
            $oldItemSeqArr[$row['item_id']] = $row['sequence_number'];
        }
        $this->allChildAssociationToSave = [];

        $remainingChildItemAssociationDoc = DB::table('items')
            ->select('item_id','document_id')
            ->whereIn('item_id', $remining_item_id)
            ->where('is_deleted', 0)
            ->get();
        foreach($remainingChildItemAssociationDoc as $row) {
            $remining_item_id_doc[$row->item_id] = $row->document_id;
        }
        $remainingChildItemAssociation = DB::table('item_associations')
            ->select('item_associations.*')
            ->whereIn('source_item_id', $remining_item_id)
            ->where('association_type', 1)
            ->where('is_deleted', 0)
            ->get();

        foreach($remainingChildItemAssociation as $asso) {
            if(isset($remining_item_id_doc[$asso->source_item_id]) && $asso->target_item_id == $remining_item_id_doc[$asso->source_item_id]) {
                $destinationId = $this->orginiNodeId;
            } else {
                if(isset($AllItemArr[$asso->target_item_id])) {
                    $destinationId = $AllItemArr[$asso->target_item_id]['item_id'];
                } else {
                    #while making copy of refrence node, solved issue of duplicating nodes
                    if($asso->source_document_id == $remining_item_id_doc[$asso->source_item_id]){
                        $destinationId = $this->orginiNodeId;
                    }
                }
            }
            $AllItemArr[$asso->source_item_id]['parent_id'] = $destinationId;

            $sequenceNumber = $oldItemSeqArr[$asso->source_item_id];
            $itemAssociationId = $this->createUniversalUniqueIdentifier();
            $originId = $AllItemArr[$asso->source_item_id]['item_id'];
            $originIdNodeTitle = $AllItemArr[$asso->source_item_id]['full_statement'];
            $uri = $this->getCaseApiUri("CFAssociations", $itemAssociationId, true, $this->organizationCode,$this->domainName);
            $externalNodeuri = $this->getCaseApiUri("CFItems", $destinationId, true, $this->organizationCode,$this->domainName);
            // get target document id based on destination node id start
            $documentIdentifier = $this->itemRepository->find($destinationId);
            $targetDocumentIdentifier = $documentIdentifier['document_id'];
            $caseAssociationsToSave[] = [
                "item_association_id"           => $itemAssociationId,
                "association_type"              => 1,
                "document_id"                   => $this->documentId,
                "association_group_id"          => '',
                "origin_node_id"                => $originId,
                "destination_node_id"           => $destinationId,
                "destination_document_id"       => $this->documentId,
                "sequence_number"               => $sequenceNumber,
                "external_node_title"           => $originIdNodeTitle,
                "external_node_url"             => $externalNodeuri,
                "created_at"                    => now()->toDateTimeString(),
                "updated_at"                    => now()->toDateTimeString(),
                "source_item_association_id"    => $itemAssociationId,
                "organization_id"               => $this->organizationId,
                "is_reverse_association"        => 0,
                "uri"                           => $uri,
                "description"                   => '', /**added blank to resolve column count issue ofr sql**/
                "has_asset"                     => 0, /**added 0 to resolve column count issue ofr sql **/
                "source_document_id"            => $this->documentId,
                "source_item_id"                => $originId,
                "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$destinationId,
                "target_item_id"                => $destinationId
            ];
        }
        $this->createItemArr = $AllItemArr;
        $this->caseAssociationsToSave = $caseAssociationsToSave;
    }

    function getAllItemForProject()
    {
        $newItemForProject = $this->createItemArr;
        $projectId = $this->projectId;
        $projectDataToInsert = [];
        foreach($newItemForProject as $newItemForProjectK => $newItemForProjectV)
        {
            $projectDataToInsert[] = [
                "project_id" =>$projectId,
                "item_id" => $newItemForProjectV['item_id'],
                "is_editable" => 1,
                "is_deleted" => 0,
                "created_at" => now()->toDateTimeString(),
                "updated_at" => now()->toDateTimeString()
            ];
        }
        $this->projectDataToInsert = $projectDataToInsert;
    }

    function InsertAllDataForCopy()
    {
        $itemConcept = $this->createConceptArr;
        $itemLicense = $this->createLicenseArr;
        $itemInsert = $this->createItemArr;

        $itemCustom = $this->createCustomArr;
        $itemAsso = $this->caseAssociationsToSave;
        $itemAsset = $this->createItemAsset;
        $itemExemplar = $this->createItemExemplar;
        $itemExemplarAsset = $this->createExamplarAsset;
        $projectItem = $this->projectDataToInsert;

        /**
         *  array_values is used to change the index of array from item identifiers to
         *  same index as parent array
         */

        $allAssociation = array_values(array_merge($itemAsso,$itemExemplar));
        if($this->iscopy)
        {
            $exactMatchAssociation = $this->exactMatchAssociationsToSave;
            $allAssociation = array_values(array_merge($allAssociation,$exactMatchAssociation));
        }
        if($this->isCopyAsso==1)
        {
            $itemAllAsso = $this->copyAssociationsToSave;
            $allAssociation = array_values(array_merge($allAssociation,$itemAllAsso));
        }

        #When copying same taxonomy into itself remove child associations of source taxonomy
        foreach ($allAssociation as $assoK => $assoV) {
            if($assoV['association_type'] == 1 && $assoV['target_item_id'] == $assoV['source_document_id']) {
                unset($allAssociation[$assoK]);
            }
        }

        $allAsset = array_merge($itemAsset,$itemExemplarAsset);

        $itemInsertArry = array_chunk($itemInsert,1000);
        foreach($itemInsertArry as $itemInsertArryK => $itemInsertArryV)
        {
            DB::table('items')->insert($itemInsertArryV);
        }

        $itemConceptArry = array_chunk($itemConcept,1000);
        foreach($itemConceptArry as $itemConceptArryK => $itemConceptArryV)
        {
            DB::table('concepts')->insert($itemConceptArryV);
        }

        $itemLicenseArry = array_chunk($itemLicense,1000);
        foreach($itemLicenseArry as $itemLicenseArryK => $itemLicenseArryV)
        {
            DB::table('licenses')->insert($itemLicenseArryV);
        }

        $itemCustomArry = array_chunk($itemCustom,1000);
        foreach($itemCustomArry as $itemCustomArryK => $itemCustomArryV)
        {
            DB::table('item_metadata')->insert($itemCustomArryV);
        }

        if(!empty($allAsset))
        {
            $allAssetArry = array_chunk($allAsset,1000);
            foreach($allAssetArry as $allAssetArryK => $allAssetArryV)
            {
                DB::table('assets')->insert($allAssetArryV);
            }
        }

        $allAssociationArry = array_chunk($allAssociation,1000);
        foreach($allAssociationArry as $allAssociationArryK => $allAssociationArryV)
        {
            DB::table('item_associations')->insert($allAssociationArryV);
        }

        $projectItemArry = array_chunk($projectItem,1000);
        foreach($projectItemArry as $projectItemArryK => $projectItemArryV)
        {
            DB::table('project_items')->insert($projectItemArryV);
        }

        DB::table('projects')->where('project_id', $this->projectId)->update(['updated_at' => date('Y-m-d H:i:s')]);
        DB::table('documents')->where('document_id', $this->documentId)->update(['updated_at' => date('Y-m-d H:i:s')]);

        // raise delta item update to cloud search each time when new item is added in taxonomy
        $this->itemSyncWithCloudSearch();

        // raise delta document update to cloud search each time document date is updated
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_document"];
        $eventData = [
            "type_id" => $this->documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }

    function getSeqNumberForCopyNodes()
    {
        $maxSeqCnt = $this->maxSeq;
        $copyNodesArr = $this->copyNodes;
        $parentIdForOriginalId = $this->parentIdForOriginalId;
        $newItemCreated = $this->createItemArr;
        $copyNodesHierarchyForOnlyNodesArr = [];

        foreach($copyNodesArr as $copyNodesArrV)
        {
            $maxSeqCnt = $maxSeqCnt + 1;

            $copyNodesHierarchyForOnlyNodesArr[] = [
                "item_id" => $copyNodesArrV,
                "parent_id" => $parentIdForOriginalId[$copyNodesArrV],
                "sequence_number" => $maxSeqCnt
            ];
            $newItemCreated[$copyNodesArrV]['list_enumeration'] = $maxSeqCnt;
        }
        $this->createItemArr = $newItemCreated;
        $this->nodesHierarchy = $copyNodesHierarchyForOnlyNodesArr;
    }

    function getAllItemRelationWithOriginal()
    {
        $nodesTobeCopy = $this->copyNodes;
        $nodeTree = $this->nodesHierarchy;
        $newItemCreated = $this->createItemArr;
        $originalDocId = DB::table('items')
            ->select('document_id')
            ->where('is_deleted',0)
            ->whereIn('item_id',$nodesTobeCopy)
            ->first()->document_id;
        $originalDocName = DB::table('documents')
            ->select('title')
            ->where('is_deleted',0)
            ->where('document_id',$originalDocId)
            ->first()->title;
        $nodeAssocTreeArr = [];
        foreach($newItemCreated as $newItemCreatedK => $newItemCreatedV)
        {
            $destinationId = $newItemCreatedK;
            $originId = $newItemCreatedV['item_id'];
            $originIdNodeTitle = $newItemCreatedV['full_statement'];
            $sequenceNumber = 0;
            $itemAssociationId = $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFAssociations", $itemAssociationId, true, $this->organizationCode,$this->domainName);
            $externalNodeuri = $this->getCaseApiUri("CFItems", $destinationId, true, $this->organizationCode,$this->domainName);
            // get target document id based on destination node id start
            $documentIdentifier = $this->itemRepository->find($destinationId);
            $targetDocumentIdentifier = $documentIdentifier['document_id'];
            $exactMatchAssociationsToSave[] = [
                "item_association_id"           => $itemAssociationId,
                "association_type"              => 4,
                "document_id"                   => $this->documentId,
                "association_group_id"          => '',
                "origin_node_id"                => $originId,
                "destination_node_id"           => $destinationId,
                "destination_document_id"       => $this->documentId,
                "sequence_number"               => $sequenceNumber,
                "external_node_title"           => $originIdNodeTitle,
                "external_node_url"             => $externalNodeuri,
                "created_at"                    => now()->toDateTimeString(),
                "updated_at"                    => now()->toDateTimeString(),
                "source_item_association_id"    => $itemAssociationId,
                "organization_id"               => $this->organizationId,
                "is_reverse_association"        => 0,
                "uri"                           => $uri,
                "description"                   => '', /**added blank to resolve column count issue ofr sql */
                "has_asset"                     => 0, /**added 0 to resolve column count issue ofr sql */
                "source_document_id"            => $this->documentId,
                "source_item_id"                => $originId,
                "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$destinationId,
                "target_item_id"                => $destinationId

            ];
        }
        $this->exactMatchAssociationsToSave = $exactMatchAssociationsToSave;
    }

    //Added this method definition to resolve ACMT-2808
    function get_file_extension($file_name) {
        return substr(strrchr($file_name,'.'),1);
    }

    //Added this method to resolve ACMT-2813
    function updateCopyNodesForHierachy()
    {
        $nodesTobeCopyData = $this->copyNodes;
        $nodeTree = $this->nodesHierarchy;
        $itemArrHierarchy = [];
        $newArrCopyNodes = [];
        foreach($nodeTree as $nodeTree1K => $nodeTree1V)
        {
            array_push($itemArrHierarchy,$nodeTree1V['item_id']);
        }
        foreach($nodeTree as $nodeTree2K => $nodeTree2V)
        {
            if(!in_array($nodeTree2V['parent_id'],$itemArrHierarchy))
            {
                array_push($newArrCopyNodes,$nodeTree2V['item_id']);
            }
        }
        if(count($newArrCopyNodes) > 0)
        {
            $this->copyNodes = $newArrCopyNodes;
        }
    }

    function getAllItemAssociation($nodeArrWithChild)
    {
        $AllItemArr = $this->createItemArr;
        $itemAssoCheck = $this->caseAssociationsToSave;
        $itemAssoCheckArr = [];
        foreach($itemAssoCheck as $itemAssoCheckK => $itemAssoCheckV)
        {
            $itemAssoCheckArr[$itemAssoCheckV['source_item_id']] = $itemAssoCheckV['target_item_id'];
        }

        $getItemAssociation = DB::table('item_associations')
            ->select('item_associations.*')
            ->whereIn('source_item_id',$nodeArrWithChild)
            ->whereNotIn('association_type',[8])
            ->where('is_deleted',0)
            ->get()->toArray();
        $createItemAssociation = [];
        $destinationArr = [];
        $destinationDocArr = [];
        foreach($getItemAssociation as $getItemAssociationK => $getItemAssociationV)
        {
            array_push($destinationArr,$getItemAssociationV->target_item_id);
        }
        $getDestinationDocument = DB::table('items')
            ->select('items.*')
            ->whereIn('item_id',$destinationArr)
            ->where('is_deleted',0)
            ->get()->toArray();
        foreach($getDestinationDocument as $getDestinationDocumentK => $getDestinationDocumentV)
        {
            $destinationDocArr[$getDestinationDocumentV->item_id] = $getDestinationDocumentV->document_id;
        }
        foreach($getItemAssociation as $getItemAssociationK => $getItemAssociationV)
        {
            $newAssociationId = $this->createUniversalUniqueIdentifier();
            $originId = (isset($AllItemArr[$getItemAssociationV->source_item_id])) ? $AllItemArr[$getItemAssociationV->source_item_id]['item_id'] : "";
            $destinationId = (isset($AllItemArr[$getItemAssociationV->target_item_id]['item_id'])) ? $AllItemArr[$getItemAssociationV->target_item_id]['item_id'] : $getItemAssociationV->target_item_id;

            $destinationDocument = (isset($destinationDocArr[$getItemAssociationV->target_item_id])) ? $destinationDocArr[$getItemAssociationV->target_item_id] : $getItemAssociationV->target_item_id;

            $uri = $this->getCaseApiUri("CFAssociations", $newAssociationId, true, $this->organizationCode,$this->domainName);
            # check if $destinationId exists in newly created item or in db
            if($this->itemExists($destinationId)) {
                $externalNodeuri = $this->getCaseApiUri("CFItems", $destinationId, true, $this->organizationCode,$this->domainName);
            } else {
                $externalNodeuri = $getItemAssociationV->external_node_url;
            }
            if($getItemAssociationV->association_type == 1)
            {
                if(isset($itemAssoCheckArr[$originId]) && $itemAssoCheckArr[$originId] ==  $destinationId)
                {
                    $flag = 1;
                }
                else if($destinationDocument == $getItemAssociationV->source_document_id)
                {
                    #changed this to solve duplicate associations of copied nodes getting created
                    $flag = 1;
                }
                else {
                    $flag = 0;
                }
            }
            else {
                $flag = 0;
            }
            if($flag == 0)
            {

                // get target document id based on destination node id start
                $documentIdentifier = $this->itemRepository->find($destinationId);
                $targetDocumentIdentifier = $documentIdentifier['document_id'];

                $createItemAssociation[$getItemAssociationV->item_association_id] = [
                    "item_association_id"           => $newAssociationId,
                    "association_type"              => $getItemAssociationV->association_type,
                    "document_id"                   => $this->documentId,
                    "association_group_id"          => '',
                    "origin_node_id"                => $originId,//$getItemExemplarV->origin_node_id, the old itemIdentifier was used, hence changed to the created itemIdentifier
                    "destination_node_id"           => $destinationId,
                    "destination_document_id"       => $this->documentId,
                    "sequence_number"               => $getItemAssociationV->sequence_number,
                    "external_node_title"           => $getItemAssociationV->external_node_title,
                    "external_node_url"             => $externalNodeuri,
                    "description"                   => $getItemAssociationV->description,
                    "created_at"                    => now()->toDateTimeString(),
                    "updated_at"                    => now()->toDateTimeString(),
                    "source_item_association_id"    => $newAssociationId,
                    "organization_id"               => $this->organizationId,
                    "is_reverse_association"        => $getItemAssociationV->is_reverse_association,
                    "uri"                           => $uri,
                    "has_asset"                     => 0,
                    "source_document_id"            => $this->documentId,
                    "source_item_id"                => $originId,
                    "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$destinationId,
                    "target_item_id"                => $destinationId
                ];

            }

        }
        $getItemAssociation1 = DB::table('item_associations')
            ->select('item_associations.*')
            ->whereIn('target_item_id',$nodeArrWithChild)
            // ->where('is_reverse_association',1)
            ->where('is_deleted',0)
            ->get()->toArray();
        foreach($getItemAssociation1 as $getItemAssociation1K => $getItemAssociation1V)
        {
            $newAssociationId1 = $this->createUniversalUniqueIdentifier();
            $originId1 = (isset($AllItemArr[$getItemAssociation1V->source_item_id]['item_id'])) ? $AllItemArr[$getItemAssociation1V->source_item_id]['item_id'] : $getItemAssociation1V->source_item_id;
            $destinationId1 = (isset($AllItemArr[$getItemAssociation1V->target_item_id]['item_id'])) ? $AllItemArr[$getItemAssociation1V->target_item_id]['item_id'] : $getItemAssociation1V->target_item_id;
            $uri = $this->getCaseApiUri("CFAssociations", $newAssociationId1, true, $this->organizationCode,$this->domainName);
            # check if $destinationId1 exists in newly created item or in db
            if($this->itemExists($destinationId1)) {
                $externalNodeuri = $this->getCaseApiUri("CFItems", $destinationId1, true, $this->organizationCode,$this->domainName);
            } else {
                $externalNodeuri = $getItemAssociation1V->external_node_url;
            }
            // get target document id based on destination node id start
            $documentIdentifier1 = $this->itemRepository->find($destinationId1);
            $targetDocumentIdentifier1 = $documentIdentifier1['document_id'];
            if(!($getItemAssociation1V->association_type == 1))
            {
                $createItemAssociation[$getItemAssociation1V->item_association_id] = [
                    "item_association_id"           => $newAssociationId1,
                    "association_type"              => $getItemAssociation1V->association_type,
                    "document_id"                   => $getItemAssociation1V->document_id,
                    "association_group_id"          => '',
                    "origin_node_id"                => $originId1,//$getItemExemplarV->origin_node_id, the old itemIdentifier was used, hence changed to the created itemIdentifier
                    "destination_node_id"           => $destinationId1,
                    "destination_document_id"       => $this->documentId,
                    "sequence_number"               => $getItemAssociation1V->sequence_number,
                    "external_node_title"           => $getItemAssociation1V->external_node_title,
                    "external_node_url"             => $externalNodeuri,
                    "description"                   => $getItemAssociation1V->description,
                    "created_at"                    => now()->toDateTimeString(),
                    "updated_at"                    => now()->toDateTimeString(),
                    "source_item_association_id"    => $newAssociationId1,
                    "organization_id"               => $this->organizationId,
                    "is_reverse_association"        => $getItemAssociation1V->is_reverse_association,
                    "uri"                           => $uri,
                    "has_asset"                     => 0,
                    "source_document_id"            => $getItemAssociation1V->document_id,
                    "source_item_id"                => $originId1,
                    "target_document_id"            => $this->documentId,
                    "target_item_id"                => $destinationId1
                ];

            }

        }
        $this->copyAssociationsToSave = $createItemAssociation;
    }

    public function itemExists($source_item_id)
    {
        $new_item_arr = $this->createItemArr;
        $new_item_arr = array_column($new_item_arr,'source_item_id');
        if(in_array($source_item_id, $new_item_arr)) {
            return true;
        }

        $itemExistsCount = DB::table('items')
                                ->where('source_item_id', $source_item_id)
                                ->where('document_id', $this->documentId)
                                ->where('is_deleted', 0)
                                ->get()->count();

        if($itemExistsCount) {
            return true;
        } else {
            return false;
        }
    }

    function getNodeHierarchy($copyNodes)
    {
        $sortedNodeForChild = array_unique($copyNodes);
        $input_item_ids = [];
        $getItemAssociationArr = [];
        $getItemSourceArr = [];
        $getItemTargetArr = [];
        $getItemOriginArr = [];
        $getItemDestArr = [];
        $getSourceArr = [];
        $getDestArr = [];
        foreach ($sortedNodeForChild as $item_id) {
            $this->getAllChildItemsForSearch($item_id, $input_item_ids);
        }
        $input_item_ids = array_unique(array_merge($input_item_ids,$sortedNodeForChild));
        $getItemAssociation = DB::table('item_associations')
            ->select('item_associations.*')
            ->whereIn('source_item_id',$input_item_ids)
            ->where('association_type','=',1)
            ->where('is_deleted',0)
            ->get()->toArray();
        foreach($getItemAssociation as $getItemAssociationK => $getItemAssociationV)
        {
            array_push($getItemSourceArr,$getItemAssociationV->source_item_id);
            array_push($getItemTargetArr,$getItemAssociationV->target_item_id);
        }
        $getItemSArr = DB::table('items')
            ->select('items.*')
            ->whereIn('item_id',$getItemSourceArr)
            ->where('is_deleted',0)
            ->get()->toArray();
        foreach($getItemSArr as $getItemSArrK => $getItemSArrV)
        {
            array_push($getSourceArr,$getItemSArrV->item_id);

        }
        $getItemDArr = DB::table('items')
            ->select('items.*')
            ->whereIn('item_id',$getItemTargetArr)
            ->where('is_deleted',0)
            ->get()->toArray();
        foreach($getItemDArr as $getItemDArrK => $getItemDArrV)
        {
            array_push($getDestArr,$getItemDArrV->item_id);

        }
        $documentIdsSArr = array_diff($getItemSourceArr,$getSourceArr);
        $documentIdsDArr = array_diff($getItemTargetArr,$getDestArr);
        // code not finished yet
        $ItemDocRelationSArr = [];
        $ItemDocRelationDArr = [];
        if(count($documentIdsSArr) > 0)
        {
            foreach($documentIdsSArr as $documentIdsSArrK => $documentIdsSArrV)
            {
                $ItemDocRelationSArr[$documentIdsSArrV] = $documentIdsSArrV;
            }
        }
        foreach($getItemSArr as $getItemSArrK => $getItemSArrV)
        {
            $ItemDocRelationSArr[$getItemSArrV->item_id] = $getItemSArrV->document_id;
        }

        if(count($documentIdsDArr) > 0)
        {
            foreach($documentIdsDArr as $documentIdsDArrK => $documentIdsDArrV)
            {
                $ItemDocRelationDArr[$documentIdsDArrV] = $documentIdsDArrV;
            }
        }
        foreach($getItemDArr as $getItemDArrK => $getItemDArrV)
        {
            $ItemDocRelationDArr[$getItemDArrV->item_id] = $getItemDArrV->document_id;
        }

        foreach($getItemAssociation as $getItemAssociationK => $getItemAssociationV)
        {
            if(in_array($getItemAssociationV->target_item_id,$input_item_ids) || $getItemAssociationV->target_item_id  == $getItemAssociationV->source_document_id)
            {
                if($ItemDocRelationSArr[$getItemAssociationV->source_item_id] == $ItemDocRelationDArr[$getItemAssociationV->target_item_id])
                {

                    $getItemAssociationArr[] = [
                        "parent_id" => $getItemAssociationV->target_item_id,
                        "sequence_number" => 0,
                        "external_link" => false,
                        "present" => 1,
                        "item_id" => $getItemAssociationV->source_item_id,
                    ];
                }
            }
        }
        $this->nodesHierarchy = $getItemAssociationArr;
    }

    public function getAllChildItemsForSearch($parentId, & $return ){
        $items = DB::select( DB::raw("SELECT source_item_id FROM acmt_item_associations WHERE is_deleted = 0 AND association_type = 1 AND target_item_id = '$parentId'") );
        foreach($items as $item){
            $return[]=$item->source_item_id;
            $this->getAllChildItemsForSearch($item->source_item_id,$return);
        }
    }

    public function itemSyncWithCloudSearch()
    {
        // I am commenting code for faster performance on nodes to be added while copy in cloud search
        $documentId = $this->documentId;
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["taxonomy_created"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
        
    }
}
