<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Project\Validators\CreateProjectValidator;

class ValidateCreateProjectInputJob extends Job
{
    public $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateProjectValidator $validator)
    {   
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
