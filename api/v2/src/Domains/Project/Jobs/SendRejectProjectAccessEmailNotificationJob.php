<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Lucid\Foundation\QueueableJob;

use Illuminate\Support\Facades\Mail;
use App\Domains\Project\Mail\SendRejectProjectAccessEmailToUser;

class SendRejectProjectAccessEmailNotificationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->input['subject'] = 'Project Access Request Status - Rejected';        
        Mail::to($this->input['email'])->send(new SendRejectProjectAccessEmailToUser($this->input));
    }
}
