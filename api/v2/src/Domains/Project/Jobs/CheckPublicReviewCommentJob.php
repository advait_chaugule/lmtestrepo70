<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use DB;
class checkPublicReviewCommentJob extends Job
{
    private   $itemId;
    protected $userId;
    public function __construct(array $input){
        $this->itemId = $input['itemId'];
        $this->userId = $input['userId'];
    }
    public function handle(){
        $array = $this->getCommentStatus();
        return $array;
    }

    /**
     * @return array|mixed|object
     * @purpose This function is used to return all data which have adoption_status 6(public)
     */
    public function getCommentStatus(){
        $query = DB::table('items')
            ->join('thread_comments','thread_comments.thread_source_id','=','items.item_id')
            ->join('documents','documents.document_id','=','items.document_id')
            ->where('documents.adoption_status','=',6)
            ->where('thread_comments.created_by', $this->userId)
            ->where('items.item_id', $this->itemId)
            ->get();
        $data = json_decode(json_encode($query,true));
        return $data;
    }
}