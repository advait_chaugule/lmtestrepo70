<?php
namespace App\Domains\Project\Jobs;

use App\Data\Models\PublicReview;
use Lucid\Foundation\Job;

class CheckInsertedDataJob extends Job
{
    private $organizationId;
    private $identifier;
    private $userId;
    private $isDeleted;

    public function __construct(array $input)
    {
        $this->organizationId = $input['organization_id'];
        $this->identifier     = $input['project_id'];
        $this->userId         = $input['user_id'];
        $this->isDeleted      = $input['is_deleted'];
    }
    public function handle(){
    $arrData = $this->checkInsertedData();
    return $arrData;
    }

    public function checkInsertedData(){
        $query = PublicReview::firstOrCreate(['project_id'    => $this->identifier,'user_id'=>$this->userId,'organization_id'=>$this->organizationId],
                                            ['organization_id'=> $this->organizationId,
                                            'project_id'      => $this->identifier,
                                            'user_id'         => $this->userId,
                                            'review_status'   => 2,
                                            'is_deleted'      => $this->isDeleted,
                                            'created_by'      => $this->userId,
                                            'updated_by'      => $this->userId]
                                            );
        return $query;
    }
}