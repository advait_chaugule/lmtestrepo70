<?php
namespace App\Domains\Project\Jobs;

use App\Data\Models\PublicReview;
use Illuminate\Http\Request;

use Lucid\Foundation\Job;
use DB;
use Auth;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\GroupDocumentTrait;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Models\Role;
use App\Data\Models\Project;
class ListProjectsJob extends Job
{
    
    use ArrayHelper,GroupDocumentTrait;
    private $userId;
    private $projectRepository;
    private $documentRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ProjectRepositoryInterface $projectRepository,
        DocumentRepositoryInterface $documentRepository,
        Request $request)
    {
        // set database repositories
        $this->projectRepository = $projectRepository;
        $this->documentRepository = $documentRepository;

        $this->orgnizationId    =   $request['auth_user']['organization_id'];
        $this->userId = $request['auth_user']['user_id'];
        // set pagination and sort filters here when required

        // get all projects
        $request['sort'] = 'created_at:desc';

        $projects = $this->projectRepository->getProjectList($request);
        if(count($projects)>0) {
            
            $parsedProjects = $this->transFormProjectsAddTheirDependencies($projects);
            $totalProjects = 0;
            $totalPacingGuide = 0;

            foreach ($parsedProjects as $response) {
                if ($response['project_type'] == "1") {
                    $totalProjects++;
                } else {
                    $totalPacingGuide++;
                }
            }
            
            if ($request['project_type'] == "3"){ 
                $userId         = $request['auth_user']['user_id'];
                $status         = $this->getStatus($userId); 

                $publicReviewProjectList  =   $this->fetchAndSetProjectListWithPRStatus($this->orgnizationId);

                $filteredProjectArray = array_filter($parsedProjects, function($project) use ($publicReviewProjectList){
                    return !in_array($project['project_id'], $publicReviewProjectList);
                });

                $projectPRStatusOfFilteredProject   =   $this->fetchProjectPRStatusOfFilteredProject($filteredProjectArray);


                foreach ($filteredProjectArray as $key1=> $arrResp) {
                    $filteredProjectArray[$key1]['review_status'] = 1;
                    $filteredProjectArray[$key1]['public_review_status'] = !empty($projectPRStatusOfFilteredProject[$arrResp['project_id']]) ? $projectPRStatusOfFilteredProject[$arrResp['project_id']] : 3;

                    foreach ($status as $key => $status1) { 
                        if ($arrResp['project_id'] == $status1['project_id']) { 
                            $filteredProjectArray[$key1]['review_status'] = $status1['review_status'];
                        } 
                    } 
                }
                $parsedProjects =   array_values($filteredProjectArray); 
            }
            // prepare the response
            return ['projects' => $parsedProjects, 'count' => sizeof($parsedProjects), 'totalProjects' => $totalProjects, 'totalPacingGuide'    =>  $totalPacingGuide];
        }
    }
    private function transFormProjectsAddTheirDependencies($projects): array {
        $parsedProjects = [];
        if($projects->isNotEmpty()){
            $documentIds = $this->createArrayFromCommaeSeparatedString($projects->implode('document_id', ','));
            $projectsDocuments = $this->fetchProjectsDocuments($documentIds);
           foreach($projects as $project){
            $projectId[] = $project->project_id; 
           }
           $getRole = DB::table('project_users')
            ->join('projects', 'projects.project_id','=','project_users.project_id')
            ->select('project_users.workflow_stage_role_id','project_users.project_id')
            ->whereIn('project_users.project_id',$projectId)
            ->where('project_users.user_id',$this->userId)
            ->where('projects.is_deleted',0)
            ->where('project_users.is_deleted',0)
            ->get()
            ->toArray();
           
            $getData = array();
            foreach($getRole as $getRoleV)
            {
                $getData[] = ['role_id'=>substr($getRoleV->workflow_stage_role_id,-36),'project_id'=>$getRoleV->project_id];    
            }
           
             $roleIds = array_column($getData,'role_id');
             $uniqueRoleId  = array_unique($roleIds);
             $projectIds = array_column($getData,'project_id'); 
                $roleName = Role::select('role_id','name')->whereIn('role_id',$uniqueRoleId)->get();

                if($roleName)
                {
                    $data = $roleName->toArray();
                }

                $getProjectRelatedData = array();
                $getProjectIdHavingRoles = array();
                foreach($getData as $getDataK=>$getDataV)
                {
                   foreach($data as $dataK=>$dataV)
                   {
                        if($getDataV['role_id']==$dataV['role_id'])
                        {
                            $getDataV['name'] = $dataV['name'];
                            $getProjectRelatedData[]=$getDataV;
                        }
                   } 
                }
            $getProjectRelatedData = array_map("unserialize", array_unique(array_map("serialize", $getProjectRelatedData)));

            $mapProjectsWithRoleIdAndRoleName = array();
                
                    foreach($getProjectRelatedData as $getProjectRelatedDataKey=>$getProjectRelatedDataValue)
                    {
                            $mapProjectsWithRoleIdAndRoleName[$getProjectRelatedDataValue['project_id']][]=
                            [
                            'role_id'=>$getProjectRelatedDataValue['role_id'],
                            'name'=>$getProjectRelatedDataValue['name']
                            ];

                    }

            // added to get group of documents
            $groups = $this->getDocumentGroups($documentIds,$this->userId);
            foreach($projects as $project) {
                $taxonomyName   =   "";
                $taxonomyStatus =   "";
                $taxonomyType   =   "";
                $taxonomyLocked =   0;

                if(!empty($project->document_id) && !empty($projectsDocuments[$project->document_id])){
                    $taxonomyName   =   $projectsDocuments[$project->document_id]['title'];
                    $taxonomyStatus =   $projectsDocuments[$project->document_id]['status'];
                    $taxonomyType   =   $projectsDocuments[$project->document_id]['type'];
                    $taxonomyLocked =   $projectsDocuments[$project->document_id]['is_locked'];
                }

                $parsedProjects[] = [
                    'project_id'    => $project->project_id,
                    'project_name'  => !empty($project->project_name) ? $project->project_name : '',
                    'description'   => !empty($project->description) ? $project->description : '',
                    'workflow_id'   => $project->workflow_id,
                    'project_type'  => $project->project_type,
                    'created_at'    => !empty($project->created_at) ? $project->created_at->toDateTimeString() : "",
                    'updated_at'    => !empty($project->updated_at) ? $project->updated_at->toDateTimeString() : "",
                    'workflow' => [
                        'workflow_id'   => $project->workflow->workflow_id,
                        'title'         => !empty($project->workflow->name) ? $project->workflow->name : "",
                        'current_workflow_stage_id'=> !empty($project->project_status->workflow_stage_id) ? $project->project_status->workflow_stage_id : "",
                        'current_stage_id'  => !empty($project->project_status->workflow_stage_id) ? explode("||",$project->project_status->workflow_stage_id)[1] : ""

                    ],
                    'taxonomy_name'     =>  $taxonomyName,
                    'taxonomy_status'   =>  $taxonomyStatus,
                    'taxonomy_type'     =>  $taxonomyType,
                    'project_status'    =>  !empty($project->project_status->stage_name) ? $project->project_status->stage_name : "",
                    'document_id'       =>  $project->document_id,
                    'is_locked'         =>  $taxonomyLocked,

                    // only project level last_changed by added, will be dynamic after workflow and LRS implementation
                    'last_changed_by'   => !empty($project->user->name) ? $project->user->name : "",
                    'created_by'        => !empty($project->user->name) ? $project->user->name : "",
                    'access_right'      => isset($mapProjectsWithRoleIdAndRoleName[$project->project_id])?$mapProjectsWithRoleIdAndRoleName[$project->project_id]:[],
                    "groups"            => isset($groups[$project->document_id]) ? $groups[$project->document_id] : []
                    
                ];
            }
        }
        return $parsedProjects;
    }

    private function fetchProjectsDocuments(array $documentIds): array {
        $attributeIn = "document_id";
        $containedInValues = $documentIds;
        $returnCollection = true;
        $fieldsToReturn = ["document_id", "title", "adoption_status", "status", "document_type", "is_locked"];
        $projectsDocuments = $this->documentRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn
        );
        $parsedData = [];
        foreach($projectsDocuments as $document) {
            $title = !empty($document->title) ? $document->title : "";
            $parsedData[$document->document_id] = ['title' => $title, 'status'=>$document->status,'adoption_status' => $document->adoption_status, 'type' => $document->document_type, 'is_locked' => $document->is_locked];
        }
        return $parsedData;
    }

    public function getStatus($userId)
    {
        $query = PublicReview::where('user_id', $userId)->get();
        if (!$query) {
            $res = [];
            return $res;
        }
        $data = $query->toArray();
        return $data;
    }

    public function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['project_id'] === $id) {
                return $key;
            }
        }
        return null;
    }

    private function fetchAndSetProjectListWithPRStatus($organizationIdentifier) {
        //$organizationIdentifier =   $this->getOr
        $toBeFilteredProjectIdArray           = [];
        $statusArray                =   ['1'];
        $projectListWithPRStatus    =     $this->projectRepository->fetchPublicReviewHistoryProjectList($organizationIdentifier, $statusArray);

        if($projectListWithPRStatus->count() > 0 ) {
            foreach($projectListWithPRStatus as $projectList) {
                $toBeFilteredProjectIdArray[]   =   $projectList->project_id;
            }
        }

        return $toBeFilteredProjectIdArray;

    }

    public function fetchProjectPRStatusOfFilteredProject($filteredProjectArray) {
        $filteredProjectWithStatusArray =   [];
        $projectIdArray = [];
        foreach($filteredProjectArray as $projects) {
            $projectIdArray[]   =   $projects['project_id'];
        }
        if(count($projectIdArray) > 0)
        {
            $filteredProjectPRStatus    =   $this->projectRepository->fetchFilteredProjectPRList($projectIdArray);

            foreach($filteredProjectPRStatus as $project) {
                $filteredProjectWithStatusArray[$project->project_id]   =   $project->status;
            }
        }
        return $filteredProjectWithStatusArray;
    }
}