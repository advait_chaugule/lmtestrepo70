<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Lucid\Foundation\QueueableJob;

use Illuminate\Support\Facades\Mail;
use App\Domains\Project\Mail\SendAcceptProjectAccessEmailToUser;

class SendAcceptProjectAccessEmailNotificationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->input['subject'] = 'You have been granted access for the project '.$this->input['projectName'];        
        Mail::to($this->input['email'])->send(new SendAcceptProjectAccessEmailToUser($this->input));
    }
}
