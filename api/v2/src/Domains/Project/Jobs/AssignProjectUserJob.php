<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use DB;
class AssignProjectUserJob extends Job
{
    private $projectIdentifier;
    private $organizationIdentifier;
    private $requestData;
    private $workflowStageRoleIdentifier;
    private $userIdentifier;
    private $projectRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input, array $requestData, string $userId)
    {
        //Set project identifier
        $this->setProjectIdentifier($input['project_id']);
        $this->setOrganizationIdentifier($input['organization_id']);
        $this->setUserIdentifier($userId);
        $this->requestData = $requestData; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set the project repo handler
        $this->projectRepository = $projectRepository;

        $assignUserToProjectRole = $this->saveUserProjectRole();

        return $assignUserToProjectRole;
    }

    //Public Getter and Setter methods
    public function setProjectIdentifier($identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(){
        return $this->projectIdentifier;
    }
    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }
    public function setUserIdentifier($userId) {
        $this->userIdentifier = $userId;
    }

    public function getUserIdentifier(){
        return $this->userIdentifier;
    }


    //Method to assign user to project role 
    private function saveUserProjectRole(){
        $projectIdentifier = $this->getProjectIdentifier();
        $organizationIdentifier = $this->getOrganizationIdentifier();
        $inputData = $this->requestData;
        $userIdentifier = $this->getUserIdentifier();

        $workflowStageId    = explode('||', $inputData['workflow_stage_role_id']);
        $workflowId         =  $workflowStageId[0];
        $roleId             =  $workflowStageId[2];
        $getWorkflow        = $this->projectRepository->getWorkflowDetails($workflowId);
        $workflowStageId    = array_column($getWorkflow,'workflow_stage_id');
        $getWorkflowRole    =  $this->projectRepository->getStageWorkflow($workflowStageId,$roleId);
        $workFlowStageRole  = array_column($getWorkflowRole,'workflow_stage_role_id');
        foreach ($workFlowStageRole as $key=>$value) {
            $attributes = [
                'project_id' => $projectIdentifier,
                'user_id'    => $inputData['user_id'],
                'workflow_stage_role_id' => $value,
                'organization_id' => $organizationIdentifier,
                'created_by' => $userIdentifier,
            ];
            $this->projectRepository->assignUserToProject($attributes);
        }
    }
}
