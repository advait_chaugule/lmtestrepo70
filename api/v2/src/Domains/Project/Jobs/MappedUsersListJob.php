<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class MappedUsersListJob extends Job
{
    private $identifier;
    private $userList;
    private $projectRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //Set the identifier
        $this->setIdentifier($input['project_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set the project repo handler
        $this->projectRepository = $projectRepository;
        $userList = $this->fetchUserAssociatedList();
        
        return $userList;
    }

    // Public Getter and Setter methods
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }


    private function fetchUserAssociatedList() {
        $projectIdentifier = $this->getIdentifier();

        $userList = $this->projectRepository->getMappedUsers($projectIdentifier);
        return $userList;
    }
}
