<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class PublishPacingGuideProjectJob extends Job
{   
    private $projectIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier, string $userId)
    {
        $this->setIdentifier($projectIdentifier);
        $this->setUser($userId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        $this->documentRepository           =   $documentRepository;
        

            $this->publishPacingGuide();
        
    }

    public function setIdentifier($projectIdentifier){
        $this->projectIdentifier = $projectIdentifier;
    }

    public function getIdentifier(){
        return $this->projectIdentifier;
    }

    public function setUser($userId){
        $this->userId = $userId;
    }

    public function getUser(){
        return $this->userId;
    }

    private function publishPacingGuide() {
        $projectIdentifier =   $this->getIdentifier();
        $userId =   $this->getUser();

        $columnValueFilterPairs =   ['project_id'  =>  $projectIdentifier];
        $attributes             =   ['adoption_status'   =>  '7',
                                     'updated_at' => now()->toDateTimeString(),
                                     'updated_by' =>  $userId  
                                    ];
        $publishPacingGuide      =    $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs,  $attributes);
        return $publishPacingGuide;
    }

}
