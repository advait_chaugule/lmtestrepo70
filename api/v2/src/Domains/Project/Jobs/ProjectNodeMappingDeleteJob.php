<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Data\Models\Project;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ProjectNodeMappingDeleteJob extends Job
{

    private $projectRepository;
    private $documentRepository;

    private $projectId;
    private $project;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectId)
    {
        $this->setProjectId($projectId);
    }

    public function setProjectId(string $data) {
        $this->projectId = $data;
    }

    public function getProjectId(): string {
        return $this->projectId;
    }

    public function setProject(Project $data) {
        $this->project = $data;
    }

    public function getProject(): Project {
        return $this->project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ProjectRepositoryInterface $projectRepository,
        DocumentRepositoryInterface $documentRepository
    )
    {
        // set db repositories
        $this->projectRepository = $projectRepository;
        $this->documentRepository = $documentRepository;

        // fetch and set the project
        $this->fetchAndSetProject();

        // start deleting entities associated to the project
        
            $this->deleteItemNodesProjectMapping();
            $this->deleteDocumentNodeProjectMapping();
        
    }

    private function fetchAndSetProject() {
        $projectIdentifier = $this->getProjectId();
        $project = $this->projectRepository->find($projectIdentifier);
        $this->setProject($project);
    }

    private function deleteItemNodesProjectMapping() {
        $project = $this->getProject();
        $project->items()->detach();
    }

    private function deleteDocumentNodeProjectMapping() {
        $projectId = $this->getProjectId();
        $conditionalKeyValuePairs = [ "project_id" => $projectId ];
        $dataToUpdate = [ "project_id" => '' ];
        $this->documentRepository->editWithCustomizedFields($conditionalKeyValuePairs, $dataToUpdate);
    }
}
