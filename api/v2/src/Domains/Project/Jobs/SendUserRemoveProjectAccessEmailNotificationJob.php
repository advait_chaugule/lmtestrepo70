<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Lucid\Foundation\QueueableJob;

use Illuminate\Support\Facades\Mail;
use App\Domains\Project\Mail\SendUserRemoveProjectAccessEmailToUser;


class SendUserRemoveProjectAccessEmailNotificationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->input['subject'] = 'You can no longer access ' .$this->input['project_name']. ' project';        
        Mail::to($this->input['email'])->send(new SendUserRemoveProjectAccessEmailToUser($this->input));
    }
}
