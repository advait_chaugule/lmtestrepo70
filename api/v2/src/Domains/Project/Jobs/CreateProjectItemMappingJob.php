<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class CreateProjectItemMappingJob extends Job
{
    private $projectRepository;
    private $itemIdentifier;
    private $projectIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, string $projectIdentifier)
    {
        $this->setItemIdentifier($itemIdentifier);
        $this->setProjectIdentifier($projectIdentifier);
    }

    public function setItemIdentifier(string $identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(): string {
        return $this->itemIdentifier;
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->mapItemWithProject();
    }

    private function mapItemWithProject() {
        $itemIdentifier = $this->getItemIdentifier();
        $projectIdentifier = $this->getProjectIdentifier();
        $this->projectRepository->associateItemWithProject($itemIdentifier, $projectIdentifier);
    }
}
