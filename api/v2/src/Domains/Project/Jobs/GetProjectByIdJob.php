<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class GetProjectByIdJob extends Job
{
    private $projectRepository;
    private $documentRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the private attribute
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo, DocumentRepositoryInterface $documentRepo)
    {
        $this->projectRepository    =   $projectRepo;  
        $this->documentRepository   =   $documentRepo;
        
        $projectDetail              =   $this->projectRepository->getProjectData($this->data);

        if(isset($projectDetail['document_id']) && ($projectDetail['document_id'] != null)) {
            $documentAssociatedDetail   =   $this->documentRepository->find($projectDetail['document_id']);
        } else {
            $documentAssociatedDetail   =   '';
        }

        return $this->parseResponse($projectDetail, $documentAssociatedDetail);
    }

    private function getDocumentDetailForItemsMappedWithProject() {
        $requestData    =   $this->data;
        $projectDetail  =   $this->projectRepository->getProjectDetailsWithRelations($requestData['project_id']);

        if(isset($projectDetail->document_id) && !empty($projectDetail->document_id)) {
            $documentAssociatedDetail   =   $this->documentRepository->find($projectDetail->document_id);

            return $documentAssociatedDetail;
        }
        
    }

    /**
     * Method to parse response
     *
     * @param [type] $projectDetail
     * @param [type] $documentAssociatedDetail
     * @return void
     */
    private function parseResponse($projectDetail, $documentAssociatedDetail) {
        $projectData                    =   $projectDetail;

        if(!empty($documentAssociatedDetail)) {
            $projectData['document_status'] =   $documentAssociatedDetail->status;
        }

        return  $projectData;
    }
}
