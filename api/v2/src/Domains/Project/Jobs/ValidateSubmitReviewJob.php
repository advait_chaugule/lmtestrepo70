<?php
namespace App\Domains\Project\Jobs;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\StringHelper;
use App\Domains\Project\Validators\PublicReviewValidator;

class ValidateSubmitReviewJob extends Job
{
    use StringHelper;
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PublicReviewValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}