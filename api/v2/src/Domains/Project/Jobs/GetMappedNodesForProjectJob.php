<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class GetMappedNodesForProjectJob extends Job
{
    private $projectId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //
        $this->projectId = $input['project_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        //
        return $projectRepo->getMappedNodes($this->projectId);
    }
}
