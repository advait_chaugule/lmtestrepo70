<?php
namespace App\Domains\Project\Jobs;
use App\Domains\Project\Validators\ProjectExistsValidator;

use Lucid\Foundation\Job;

class ValidateProjectIdJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectExistsValidator $validator)
    {   
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
