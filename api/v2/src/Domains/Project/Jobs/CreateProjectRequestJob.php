<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectAccessRequestRepositoryInterface;

class CreateProjectRequestJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectAccessRequestRepositoryInterface $projectRepo)
    {
        //
        //dd($this->input);
        return $projectRepo->fillAndSave($this->input);
    }
}
