<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class CheckProjectItemMappingExistsJob extends Job
{
    private $projectRepository;
    private $projectIdentifier;
    private $itemIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($projectIdentifier, $itemIdentifier)
    {
        // set the identifiers
        $this->setProjectIdentifier($projectIdentifier);
        $this->setItemIdentifier($itemIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        // set project repository
        $this->projectRepository = $projectRepository;
        // return the mapping status
        return $this->getProjectItemMappingStatus();

    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    public function setItemIdentifier(string $identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(): string {
        return $this->itemIdentifier;
    }

    private function getProjectItemMappingStatus(): bool {
        $status = false;
        $projectIdentifier = $this->getProjectIdentifier();
        $itemIdentifier = $this->getItemIdentifier();
        $project = $this->projectRepository->findBy("project_id", $projectIdentifier);
        if(!empty($project->project_id)){
            $mappingCollection = $project->items;
            if($mappingCollection->count() > 0) {
                foreach($mappingCollection as $item){
                    if ($item->item_id===$itemIdentifier) {
                        return true;
                    }
                    
                }
            }
        }
        return $status;
    }

}
