<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Models\Document;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadRepositoryInterface;


class GetNodesAssignedToProjectInTreeCompatibleFormJob extends Job
{

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;
    private $threadRepository;

    private $documentIdentifier;
    private $projectIdentifier;
    private $requestUserDetails;

    private $documentAssignedToProject;
    private $nodeIdsAssignedToProject; 
    private $nodesAssignedToProject;
    private $itemAssociations;
    private $itemEditable;
    private $itemDocumentIdentifier;
    private $itemUsageCount;
    private $documentOpenCommentCount;
    private $openCommentCountPerProject;

    private $transformedDocument;
    private $transformedItems;
    private $transformedItemAssociations;

    private $treeData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier, array $requestUserDetails, string $documentIdentifier)
    {
        $this->setProjectIdentifier($projectIdentifier);
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setRequestUserDetails($requestUserDetails);
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    public function setDocumentIdentifier(string $identifier) {
        $this->documentIdentifier = $identifier;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setDocumentAssignedToProject($data) {
        $this->documentAssignedToProject = $data;
    }

    public function getDocumentAssignedToProject() {
        return $this->documentAssignedToProject;
    }

    public function setNodeIdsAssignedToProject(array $data) {
        $this->nodeIdsAssignedToProject = $data;
    }

    public function getNodeIdsAssignedToProject(): array {
        return $this->nodeIdsAssignedToProject;
    }

    public function setNodesAssignedToProject(Collection $data) {
        $this->nodesAssignedToProject = $data;
    }

    public function getNodesAssignedToProject() {
        return $this->nodesAssignedToProject;
    }

    public function setItemAssociations(Collection $data) {
        $this->itemAssociations = $data;
    }

    public function getItemAssociations() {
        return $this->itemAssociations;
    }

    public function setItemEditableData(array $data) {
        $this->itemEditable = $data;
    }

    public function getItemEditableData() {
        return $this->itemEditable;
    }

    public function setItemDocumentIdentifier(string $data) {
        $this->itemDocumentIdentifier = $data;
    }

    public function getItemDocumentIdentifier(): string {
        return $this->itemDocumentIdentifier;
    }

    public function setTransformedDocument(array $data) {
        $this->transformedDocument = $data;
    }

    public function getTransformedDocument() {
        return $this->transformedDocument;
    }

    public function setTransformedItems(array $data) {
        $this->transformedItems = $data;
    }

    public function getTransformedItems() {
        return $this->transformedItems;
    }

    public function setTransformedItemAssociations(array $data) {
        $this->transformedItemAssociations = $data;
    }

    public function getTransformedItemAssociations() {
        return $this->transformedItemAssociations;
    }

    public function setItemUsedCount(array $data) {
        $this->itemUsageCount = $data;
    }

    public function getItemUsedCount() {
        return $this->itemUsageCount;
    }

    public function setOpenCommentCountPerProject($data) {
        $this->openCommentCountPerProject   =   $data;
    }

    public function getOpenCommentCountPerProject() {
        return $this->openCommentCountPerProject;
    }

    public function setDocumentOpenCommentCount($count) {
        $this->documentOpenCommentCount =   $count;
    }

    public function getDocumentOpenCommentCount() {
        return $this->documentOpenCommentCount;
    }

    public function setTree(array $data) {
        $this->treeData = $data;
    }

    public function getTree(): array {
        return $this->treeData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ThreadRepositoryInterface $threadRepository
    )
    {
        // set db repositories
        $this->documentRepository           = $documentRepository;
        $this->itemRepository               = $itemRepository;
        $this->itemAssociationsRepository   = $itemAssociationsRepository;
        $this->projectRepository    = $projectRepository;
        $this->nodeTypeRepository   = $nodeTypeRepository;
        $this->threadRepository     = $threadRepository;

        $this->fetchAndSetDocumentAssignedToProject();
        $this->fetchAndSetNodeIdsAssignedToProject();
        $this->fetchAndSetNodesAssignedToProject();
        $this->extractAndSetItemDocumentIdentifier();
        $this->fetchAndSetItemAssociations();
        $this->fetchAndSetItemIsEditable();
        $this->fetchAndSetCountForItemsUtilized();
        $this->fetchAndSetProjectOpenComments();

        $this->transformDocument();
        $this->transformItems();
        $this->transformItemAssociations();

        $this->buildTree();

        return $this->getTree();
    }

    private function fetchAndSetDocumentAssignedToProject() {
        $projectId          =   $this->getProjectIdentifier();
        $documentIdentifier =   $this->getDocumentIdentifier();

        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $returnCollection = true;
        $fieldsToReturn = [ "document_id", "title", "creator", "node_type_id", "project_id", "adoption_status" ];
        $relations      =   ['nodeType'];
        
        if(!empty($documentIdentifier)) {
            $conditionalKeyValuePair = [
                "organization_id" => $organizationId,
                "document_id" => $documentIdentifier,
            ];
        } else {
            $conditionalKeyValuePair = [
                "organization_id" => $organizationId,
                "project_id" => $projectId,
            ];
        } 
        
        //dd($conditionalKeyValuePair);
        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations)->first();

        $this->setDocumentAssignedToProject($document);
    }

    private function fetchAndSetNodeIdsAssignedToProject() {
        $projectId = $this->getProjectIdentifier();
        $nodeIdsAssignedToProject = $this->projectRepository->getItemIdsAssignedToProject($projectId);

        $this->setNodeIdsAssignedToProject($nodeIdsAssignedToProject);
    }

    private function fetchAndSetNodesAssignedToProject() {
        $projectId = $this->getProjectIdentifier();
        $documentIdentifier =   $this->getDocumentIdentifier();

        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $attributeIn = "item_id";
        $nodeIdsAssignedToProject = $this->getNodeIdsAssignedToProject();

        $returnCollection = true;
        $fieldsToReturn = [ "item_id", "document_id", "list_enumeration", "human_coding_scheme", "full_statement", "node_type_id" ];

        if(!empty($documentIdentifier)) {
            $conditionalKeyValuePair = [ "organization_id" => $organizationId, 'document_id' => $documentIdentifier, 'is_deleted' => '0'];
        } else {
             $conditionalKeyValuePair = [ "organization_id" => $organizationId, 'is_deleted' => '0'];
        }

        $nodesAssignedtoProject = $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                                        $attributeIn, $nodeIdsAssignedToProject, $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                    );
        $this->setNodesAssignedToProject($nodesAssignedtoProject);
    }

    private function extractAndSetItemDocumentIdentifier() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $nodesAssignedtoProject = $this->getNodesAssignedToProject();
        $documentIdOfAtleastOneItem = "";

        if(!empty($documentIdentifier)) {
            $documentIdOfAtleastOneItem = $documentIdentifier;
        } else {
            if( $nodesAssignedtoProject->isNotEmpty() ) {
                $documentIdOfAtleastOneItem = $nodesAssignedtoProject->first()->document_id;
            }
        }
        
        $this->setItemDocumentIdentifier($documentIdOfAtleastOneItem);
    }

    private function fetchAndSetItemAssociations() {
        $documentIdentifier = $this->getItemDocumentIdentifier();
        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $nodeIdsAssignedToProject = $this->getNodeIdsAssignedToProject();
        $returnCollection = true;
        $fieldsToReturn = ["origin_node_id", "destination_node_id"];
        $attributeIn = "origin_node_id";
        $conditionalKeyValuePair = [
            "document_id" =>  $documentIdentifier,
            "organization_id" => $organizationId,
            "is_deleted" => 0,
            "association_type" => 1 // 1 for isChildOf relation
        ];
        $itemAssociations = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                                            $attributeIn, $nodeIdsAssignedToProject, $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                        );
        $this->setItemAssociations($itemAssociations);
    }

    /**
     * Please do not remove this code
     */
    //Method to fetch whether item is editable for a project 
    private function fetchAndSetItemIsEditable() {
        $itemEditableArray          =   [];
        $projectIdentifier          =   (array) $this->getProjectIdentifier();
        $nodeIdsAssignedToProject   =   $this->getNodeIdsAssignedToProject();
        
        foreach($nodeIdsAssignedToProject as $itemId) {
            $itemEditableArray[$itemId] = ['is_editable' => $this->projectRepository->checkIsItemEditable($itemId, $projectIdentifier)];
        }

        $this->setItemEditableData($itemEditableArray);  
    }

    private function fetchAndSetCountForItemsUtilized() {
        $countItemUsed              =   [];
        $projectIdentifier          =   $this->getProjectIdentifier();
        $documentIdentifier         =   $this->getDocumentIdentifier();
        $items                      =   $this->getNodesAssignedToProject();

        $projectDetail              =   $this->projectRepository->find($projectIdentifier);

        if(!empty($documentIdentifier)) {
            foreach($items as $item) {
                $returnCollection   =   true;
                $fieldsToReturn     =   ['origin_node_id'];
                $keyValuePairs      =   ['origin_node_id'   =>  $item->item_id, 'document_id'  => $projectDetail->document_id, 'association_type' => '3', 'is_deleted' => '0'];
                
                $itemsUsedInPacingGuide =   $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                    $returnCollection,
                    $fieldsToReturn,
                    $keyValuePairs );

                if($itemsUsedInPacingGuide->count() > 0) {
                    //print_r($itemsUsedInPacingGuide->toArray());
                    $countItemUsed[$item->item_id]    =   count($itemsUsedInPacingGuide->toArray());
                } else {
                    $countItemUsed[$item->item_id]    =   0;
                }
            }
        }
        
        $this->setItemUsedCount($countItemUsed);
    }

    private function fetchAndSetProjectOpenComments() {
        
        $openCommentList            =   [];
        $openDocumentCommentList    =   [];

        $items                          =   $this->getNodesAssignedToProject();
        $documentAssociatedToProject    =   $this->getDocumentAssignedToProject();
        //dd($documentAssociatedToProject);
        if($documentAssociatedToProject !=null) {
            if($documentAssociatedToProject->count() > 0 ) {
                $attributeIn        =   'thread_source_id';
                $containedInValues  =   [$documentAssociatedToProject->document_id];
                $returnCollection   =   true;
                $fieldsToReturn     =   ['thread_id'];
                $keyValuePairs      =   ['is_deleted'    => '0'];
                $notInKeyValuePairs =   ['status'   =>  '4'];
    
                $openDocumentComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                        $attributeIn, 
                        $containedInValues,
                        $returnCollection,
                        $fieldsToReturn,
                        $keyValuePairs,
                        $notInKeyValuePairs)->toArray();
    
                //dd(count($openDocumentComments));
    
                $openDocumentCommentList[$documentAssociatedToProject->document_id]   =   count($openDocumentComments);
            }
        }
       

        $this->setDocumentOpenCommentCount($openDocumentCommentList);


        foreach($items as $key => $itemIds) {
            $openCommentsCount  =   0;
            $attributeIn        =   'thread_source_id';
            $containedInValues  =   [$itemIds->item_id];
            $returnCollection   =   true;
            $fieldsToReturn     =   ['thread_id', 'status'];
            $keyValuePairs      =   ['is_deleted'    => '0'];
            $notInKeyValuePairs =   ['status'   => 4];

            $openComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                $attributeIn, 
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $notInKeyValuePairs )->toArray();

            $openCommentList[$itemIds->item_id]    =   count($openComments);
        }

        $this->setOpenCommentCountPerProject($openCommentList);
    }


    private function transformDocument() {
        $document                   =   $this->getDocumentAssignedToProject();
        $documentOpenCommentCount   =   $this->getDocumentOpenCommentCount();
        //dd($document);
        if(!empty($document->document_id)) {
            $documentProjectIdentifier = $document->project_id;
            $projectIdentifier = $this->getProjectIdentifier();

            $isDocumentEditable = 0;
            if(!empty($documentProjectIdentifier) && $documentProjectIdentifier===$projectIdentifier) {
                $isDocumentEditable = 1;
            }

            //dd($document->nodeType);

            $parsedDocument = [
                "id"                    =>  $document->document_id,
                "title"                 =>  !empty($document->title) ? $document->title : "",
                "creator"               =>  $document->creator,
                "list_enumeration"      =>  "",
                "human_coding_scheme"   =>  "",
                "full_statement"        =>  "",
                "status"                =>  $document->adoption_status,
                "node_type"             =>  $document->nodeType->title,
                "metadataType"          =>  $document->nodeType->title,
                "node_type_id"          =>  !empty($document->node_type_id) ? $document->node_type_id : "",
                "is_editable"           =>  $isDocumentEditable,
                "is_document"           =>  1,
                "open_comment_count"    =>  $documentOpenCommentCount[$document->document_id]
            ];

            //dd($parsedDocument);
            $this->setTransformedDocument($parsedDocument);
        }
    }

    private function transformItems() {
        $projectIdentifier          =   $this->getProjectIdentifier();
        $items                      =   $this->getNodesAssignedToProject();
        $nodeIdsAssignedToProject   =   $this->getNodeIdsAssignedToProject();
        $itemEditable               =   $this->getItemEditableData();
        $itemUsageCount             =   $this->getItemUsedCount();
        $openCommentCount           =   $this->getOpenCommentCountPerProject();
        $parsedItems                =   [];
        
        foreach($items as $item) {
            $itemId = $item->item_id;
            $listEnumeration = !empty($item->list_enumeration) ? $item->list_enumeration : "";
            $humanCodingScheme = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "";
            $fullStatememnt = !empty($item->full_statement) ? $item->full_statement : "";
            $nodeTypeId = !empty($item->node_type_id) ? $item->node_type_id : "";

            /* $attributes =   ['source_item_id'   => $item->item_id];
            $itemDetail =   $this->itemRepository->findByAttributes($attributes); */
            //$itemEditableFlag = 1;
            $parsedItems[] = [
                "id" => $itemId,
                "title" => "",
                "list_enumeration"      => $listEnumeration,
                "human_coding_scheme"   => $humanCodingScheme,
                "full_statement"        => $fullStatememnt,
                "node_type"             => $item->nodeType->title,
                "metadataType"          => $item->nodeType->title,
                "node_type_id"          => $nodeTypeId,
                "is_editable"           => $itemEditable[$item->item_id]['is_editable'], //Please do not remove this code
                "is_document"           => 0,
                "item_usage_count"      =>  !empty($itemUsageCount[$itemId]) ? $itemUsageCount[$itemId] : 0,
                "open_comment_count"    =>  $openCommentCount[$item->item_id]
            ];
        }
        $this->setTransformedItems($parsedItems);
    }

    private function transformItemAssociations() {
        $itemAssociations = $this->getItemAssociations();
        $parsedItemAssociations = [];
        foreach($itemAssociations as $itemAssociation) {
            $parsedItemAssociations[] = [
                "child_id" => $itemAssociation['origin_node_id'],
                "parent_id" => $itemAssociation['destination_node_id']
            ];
        }
        $this->setTransformedItemAssociations($parsedItemAssociations);
    }

    /**
     * create the ui compatible tree response structure
     */
    private function buildTree () {
        $transformedItems = $this->getTransformedItems();
        $transformedItemAssociations = $this->getTransformedItemAssociations();
        $nodes = $transformedItems;

        $transformedDocument = $this->getTransformedDocument();
        //dd($transformedDocument);
        if(!empty($transformedDocument)) {
            if($transformedDocument['is_editable'] == '1') {
                $nodes = array_prepend($transformedItems, $transformedDocument);
            }
        }

        $treeData = [
            "nodes" => $nodes,
            "relations" => $transformedItemAssociations,
        ];
        
        $this->setTree($treeData);
    }

}
