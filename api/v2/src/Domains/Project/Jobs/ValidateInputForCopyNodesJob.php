<?php
namespace App\Domains\Project\Jobs;
use App\Domains\Project\Validators\CopyNodesValidator;

use Lucid\Foundation\Job;

class ValidateInputForCopyNodesJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CopyNodesValidator $validator)
    {   
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
