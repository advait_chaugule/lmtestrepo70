<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;


class AssignedUserToPublicReviewProjectJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $projectList , array $userList ,string $organizationIdentifier)
    {
        $this->projectList = $projectList;
        $this->userList = $userList;
        $this->organizationIdentifier = $organizationIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $roleCode = config("selfRegistration")["ROLE"]["TAXANOMY_PUBLIC_REVIEW_ROLE"];
        $workflow_code = config("selfRegistration")["WORKFLOW"]["DEFAULT_WORKFLOW_PROJECT_PUBLIC_REVIEW"];
        $oragnization_id = $this->organizationIdentifier;
        $workflowArr = DB::table('workflows')->select('workflow_id')
        ->where('organization_id',$oragnization_id)
        ->where('workflow_code',$workflow_code)
        ->limit(1)->get()->toArray();
        $workflowId= $workflowArr[0]->workflow_id;

       $workflowstageArr = DB::table('workflow_stage')->select('workflow_stage_id')
       ->where('workflow_id',$workflowId)
       ->where('order',"1")
       ->limit(1)->get()->toArray();
      $workflowstageId= $workflowstageArr[0]->workflow_stage_id;

        
       $roleArr = DB::table('roles')->select('role_id')
       ->where('organization_id',$oragnization_id)
       ->where('role_code',$roleCode)
       ->limit(1)->get()->toArray();
      $roleId= $roleArr[0]->role_id;

      $workflowstageroleArr = DB::table('workflow_stage_role')->select('workflow_stage_role_id')
      ->where('workflow_stage_id',$workflowstageId)
      ->where('role_id',$roleId)
      ->limit(1)->get()->toArray();
     $workflowStageRoleId= $workflowstageroleArr[0]->workflow_stage_role_id;

        if(!empty($this->projectList) && !empty($this->userList))
        {
            foreach($this->userList as $userK=>$userV)
            {
              $userId = $userV; 
              foreach($this->projectList as $projectK=>$projectV)
              {
                $projectId = $projectV;
                $projectUser = DB::table('project_users')->select('project_id')
                ->where('project_id',$projectId)
                ->where('user_id',$userId)
                ->where('workflow_stage_role_id',$workflowStageRoleId)
                ->where('is_deleted',"0")
                ->limit(1)->get()->toArray();
                $pucnt = count($projectUser);
                    if($pucnt == 0)
                    {
                        $projectUserarr = array(
                            array('project_id' => $projectId,
                            'user_id' => $userId,
                            'workflow_stage_role_id' => $workflowStageRoleId,
                            'is_deleted' => '0',
                            'created_at' => now()->toDateTimeString(),
                            'updated_at' => now()->toDateTimeString()
                          ));
                          DB::table('project_users')->insert($projectUserarr);

                    }
    
              }
            }

        }
    }
}
