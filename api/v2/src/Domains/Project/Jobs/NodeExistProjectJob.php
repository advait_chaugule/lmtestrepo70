<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\ProjectItem;

class NodeExistProjectJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $projectId = $this->input['project_id'];
        $itemId = $this->input['item_id'];
        $query = ProjectItem::select('*')
                 ->where('project_id',$projectId)
                 ->where('item_id',$itemId)
                 ->where('is_deleted','=',0)
                 ->get()
                 ->toArray();  
       if(count($query) == 0)
       {
           return 0;
       }
       else
       {
           return 1;
       }
    }
}
