<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class PreviewPacingGuideProjectJob extends Job
{
    private $pacingGuideIdentifier;
    private $organizationIdentifier;

    private $documentRepository;
    private $itemRepository;
    private $projectRepository;
    private $threadRepository;
    private $itemAssociationRepository;

    private $projectDetail;
    private $itemsCreatedForPacingGuide;
    private $parentAndDocumentId;
    private $setItemsOfPacingGuideForUnpublishedTaxonomy;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $pacingGuideIdentifier, string $organizationIdentifier)
    {
        //Set the private data
        $this->setPacingGuideIdentifier($pacingGuideIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository, DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository, ThreadRepositoryInterface $threadRepository, ItemAssociationRepositoryInterface $itemAssociationsRepository)
    {
        //Set the db handlers
        $this->projectRepository                =   $projectRepository;
        $this->documentRepository               =   $documentRepository;
        $this->itemRepository                   =   $itemRepository;
        $this->itemAssociationsRepository       =   $itemAssociationsRepository;
        $this->threadRepository                 =   $threadRepository;

        $this->fetchAndSetProject();
        $this->fetchAndSetItemsAssociatedToPacingGuide();
        $this->fetchAndSetItemParentAndDocumentId();
        $this->fetchAndSetItemsForUnpublishedTaxonomies();
        $this->fetchAndSetCountForOpenComments();

        return $this->parseResponse();
    }

    /**
     * Public Getter and Setter methods
     */
    public function setPacingGuideIdentifier($identifier) {
        $this->pacingGuideIdentifier    =   $identifier;
    }

    public function getPacingGuideIdentifier() {
        return $this->pacingGuideIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setProjectDetail($data) {
        $this->projectDetail    =   $data;
    }

    public function getProjectDetail() {
        return $this->projectDetail;
    }

    public function setItemsCreatedForPacingGuide($data) {
        $this->itemsCreatedForPacingGuide   =   $data;
    }

    public function getItemsCreatedForPacingGuide() {
        return $this->itemsCreatedForPacingGuide;
    }

    public function setParentAndDocumentId($data) {
        $this->parentAndDocumentId = $data;
    }

    public function getParentAndDocumentId() {
        return $this->parentAndDocumentId;
    }

    public function setItemsOfPacingGuideForUnpublishedTaxonomy($data) {
        $this->itemsOfPacingGuideForUnpublishedTaxonomy  =   $data;
    }

    public function getItemsOfPacingGuideForUnpublishedTaxonomy() {
        return $this->itemsOfPacingGuideForUnpublishedTaxonomy;
    }

    private function fetchAndSetProject() {
        $pacingGuideIdentifier  =   $this->getPacingGuideIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $projectDetail  =   $this->projectRepository->getProjectDetailsWithRelations($pacingGuideIdentifier);

        $projectData    =   $this->projectRepository->getProjectData($projectDetail);
        //dd($projectDetail);

        $this->setProjectDetail($projectData);
    }

    private function fetchAndSetItemsAssociatedToPacingGuide() {
        $projectDetail  =   $this->getProjectDetail();
        $documentIdentifierOfThePacingGuide  =   $projectDetail['document_id'];

        $returnCollection   =   true;
        $fieldsToReturn     =   ['*'];
        $keyValuePairs      =   ['document_id'  => $documentIdentifierOfThePacingGuide, 'is_deleted'    => '0'];
        $relations          =   ['customMetadata'];

        $itemCollectionAssociatedWithPacingGuide    =   $this->itemRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations);

        $this->setItemsCreatedForPacingGuide($itemCollectionAssociatedWithPacingGuide);
    }

    private function fetchAndSetItemParentAndDocumentId() {
        $hierarchyIdSet =   [];
        $items = $this->getItemsCreatedForPacingGuide();

        $itemSourceIdArray      =   [];
        $itemIdArray            =   [];
        foreach($items as $item) {
            $itemSourceIdArray[]    =   $item->source_item_id;
            $itemIdArray[]          =   $item->item_id;
        }

        foreach($itemSourceIdArray as $key => $item) {
            $returnCollection   =   false;
            $fieldsToReturn     =   ['document_id', 'item_id'];
            $keyValuePairs      =   ['item_id'   => $item];

            $documentSetForItem =   $this->itemRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs );

            $hierarchyIdSet[$item] =   ['item_id'   => $itemIdArray[$key], 'document_id' => !empty($documentSetForItem->document_id) ? $documentSetForItem->document_id : ""];
        }

        $this->setParentAndDocumentId($hierarchyIdSet);
    }

    private function fetchAndSetItemsForUnpublishedTaxonomies() {
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $referredItemDetail     =   $this->getParentAndDocumentId();

        //dd($referredItemDetail);

        foreach($referredItemDetail as $key => $item) {
            $documentDetails    =   $this->documentRepository->find($item['document_id']);
            if($documentDetails->adoption_status != '7') {
                $itemIdOfTheItemForPacingGuide[$key]  =   $item['item_id'];
            }
        }
        
        $attributeIn        =   'item_id';
        $containedInValues  =   $itemIdOfTheItemForPacingGuide;
        $returnCollection   =   true;
        $fieldsToReturn     =   ['*'];
        $keyValuePairs      =   ['organization_id'  => $organizationIdentifier,'is_deleted' => '0'];
        $returnPaginatedData    = false;
        $paginationFilters      = ['limit' => 10, 'offset' => 0];
        $returnSortedData       = false;
        $sortfilters            = ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator               = 'AND';
        $relations              =  ['nodeType', 'customMetadata'];

        //dd($sourceItemIdOfTheItemForPacingGuide);
        $listOfItemOfUnpublishedTaxonomyForPacingGuide  =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator,
            $relations);

        //dd($listOfItemOfUnpublishedTaxonomyForPacingGuide);

        $this->setItemsOfPacingGuideForUnpublishedTaxonomy($listOfItemOfUnpublishedTaxonomyForPacingGuide);     
    }

    private function fetchAndSetCountForOpenComments() {
        $itemsCreatedForPacingGuide =   $this->getItemsCreatedForPacingGuide();

        foreach($itemsCreatedForPacingGuide as $itemsCreated) {
            $itemIdArray[]  =   $itemsCreated->item_id;
        }

        $attributeIn        =   'thread_source_id';
        $containedInValues  =   $itemIdArray;
        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_id'];
        $keyValuePairs      =   ['is_deleted'    => '0'];
        $notInKeyValuePairs =   ['status'   =>  '4'];

        $openComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                $attributeIn, 
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $notInKeyValuePairs)->toArray();

        return count($openComments);
    }

    public function parseResponse() {
        $parsedItems    =   [];

        $projectDetail                  =   $this->getProjectDetail();
        $itemsofUnpublishedTaxonomy     =   $this->getItemsOfPacingGuideForUnpublishedTaxonomy();
        $parentAndDocumentId            =   $this->getParentAndDocumentId();

        $pacingGuidePreview =   [
            'project_id'        =>  $projectDetail['project_id'],
            'workflow_id'       =>  $projectDetail['workflow_id'],
            'workflow_status'   =>  $projectDetail['current_workflow_stage']->current_workflow_stage_name,
            'open_comments_count'   =>  $this->fetchAndSetCountForOpenComments(),
        ];

        foreach($itemsofUnpublishedTaxonomy as $item) {
            $itemId     =   $item->item_id;
            $parentId   =   $item->parent_id;

            $listEnumeration = !empty($item->list_enumeration) ? $item->list_enumeration : "";
            $humanCodingScheme = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "";
            $fullStatememnt = !empty($item->full_statement) ? $item->full_statement : "";

            $nodeTypeId = !empty($item->node_type_id) ? $item->node_type_id : "";
            $nodeType = !empty($item->nodeType) ? $item->nodeType->title : "";

            $typeMetadataDet    =   $item->customMetadata->where('is_document', '3')->first();      
            $sourceDocumentDetail   =   $this->documentRepository->find($parentAndDocumentId[$item->source_item_id]['document_id']);

            //$projectName = "";
            if(!empty($typeMetadataDet->metadata_id) && $typeMetadataDet->pivot->metadata_value == 'Standard') {
                $parsedItems[] = [
                    "id" => $itemId,
                    "title" => "",
                    "human_coding_scheme"   => $humanCodingScheme,
                    "list_enumeration"      => $listEnumeration,
                    "full_statement"        => $fullStatememnt,
                    "node_type"             => $nodeType,
                    "metadataType"          => $nodeType,
                    "node_type_id"          => $nodeTypeId,
                    "item_type"             =>  !empty($typeMetadataDet->metadata_id) ? $typeMetadataDet->pivot->metadata_value : "",
                    "parent_id"             =>  $parentId,
                    "document_id"           =>  $parentAndDocumentId[$item->source_item_id]['document_id'],
                    "document_title"        =>  !empty($sourceDocumentDetail->title) ? $sourceDocumentDetail->title : "",
                ];
            }            
        }

        $pacingGuidePreview['items']    =   $parsedItems;
        return $pacingGuidePreview;
    }
}
