<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class UpdateProjectWorkflowStageJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        $project['project_id'] = $this->data['project_id'];
        $input['current_workflow_stage_id'] = $this->data['workflow_stage_id'];
        return $projectRepo->editWithCustomizedFields($project, $input);
    }
}
