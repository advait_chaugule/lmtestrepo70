<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Document;
use App\Data\Models\ProjectItem;

use Illuminate\Support\Collection;

use Illuminate\Http\Request;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;
use DB;
class GetFlattenedTaxonomyForProjectCompleteTreeJob extends Job
{

    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $projectIdentifier;
    private $requestUserDetails;

    private $document;
    private $nodeIdsAssignedToProject;
    private $items;
    private $itemAssociationsUnderDocument;
    private $projectIdsAssociatedWithEachItem;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;

    private $transformedDocument = [];
    private $transformedItems = [];
    private $transformedItemAssociationsUnderDocument = [];
    private $tree;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, string $projectIdentifier, array $requestUserDetails)
    {
        $this->setDocumentIdentifier($identifier);
        $this->setProjectIdentifier($projectIdentifier);
        $this->setRequestUserDetails($requestUserDetails);
    }

    public function setDocumentIdentifier(string $identifier) {
        $this->documentIdentifier = $identifier;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setDocument(Document $data) {
        $this->document = $data;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setNodeIdsAssignedToProject(array $data) {
        $this->nodeIdsAssignedToProject = $data;
    }

    public function getNodeIdsAssignedToProject(): array {
        return $this->nodeIdsAssignedToProject;
    }

    public function setItems(Collection $data) {
        $this->items = $data;
    }

    public function getItems() {
        return $this->items;
    }

    public function setItemAssociationsUnderDocument(Collection $data) {
        $this->itemAssociationsUnderDocument = $data;
    }

    public function getItemAssociationsUnderDocument() {
        return $this->itemAssociationsUnderDocument;
    }

    public function setIdsOfProjectsAssociated(array $data) {
        $this->projectIdsAssociatedWithEachItem = $data;
    }

    public function getIdsOfProjectsAssociated(): array {
        return $this->projectIdsAssociatedWithEachItem;
    }

    public function setProjectItemMapping($data) {
        $this->projectItemMapping = $data;
    }

    public function getProjectItemMapping() {
        return $this->projectItemMapping;
    }

    public function setProjectsAssociated($data) {
        $this->projectsAssociated = $data;
    }

    public function getProjectsAssociated() {
        return $this->projectsAssociated;
    }

    public function setNodeTypes(Collection $data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes(): Collection {
        return $this->nodeTypes;
    }

    public function setTransformedDocument(array $data) {
        $this->transformedDocument = $data;
    }

    public function getTransformedDocument(): array {
        return $this->transformedDocument;
    }

    public function setTransformedItems(array $data) {
        $this->transformedItems = $data;
    }

    public function getTransformedItems(): array {
        return $this->transformedItems;
    }

    public function setTransformedItemAssociationsUnderDocument(array $data) {
        $this->transformedItemAssociationsUnderDocument = $data;
    }

    public function getTransformedItemAssociationsUnderDocument(): array {
        return $this->transformedItemAssociationsUnderDocument;
    }

    public function setTree(array $data) {
        $this->tree = $data;
    }

    public function getTree(): array {
        return $this->tree;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;

        // start fetching document, items and item associations
        $this->fetchAndSetTaxonomyRelatedData();

        // start transforming of data to ui compatible structure
        $this->transformTaxonomyRelatedData();

        // create the ui compatible tree response structure
        $this->buildTree();

        return $this->getTree();
    }

    /**
     * start fetching document, items and item associations from database
     */
    private function fetchAndSetTaxonomyRelatedData() {
        $this->fetchAndSetDocument();
        $this->fetchAndSetNodeIdsAssignedToProject();
        $this->fetchAndSetItems();
        $this->fetchAndSetItemAssociationsUnderDocument();
    }

    private function fetchAndSetDocument() {
        $documentIdentifier = $this->getDocumentIdentifier();
        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $returnCollection = false;
        $fieldsToReturn = [ "document_id", "title", "node_type_id", "project_id", "adoption_status"];
        $conditionalKeyValuePair = [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];
        
        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
        $this->setDocument($document);
    }

    private function fetchAndSetNodeIdsAssignedToProject() {
        $projectId = $this->getProjectIdentifier();
        $nodeIdsAssignedToProject = $this->projectRepository->getItemIdsAssignedToProject($projectId);
        $this->setNodeIdsAssignedToProject($nodeIdsAssignedToProject);
    }

    private function fetchAndSetItems() {
        $documentIdentifier = $this->getDocumentIdentifier();
        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $returnCollection = true;
        $fieldsToReturn = ["item_id", "human_coding_scheme", "list_enumeration","full_statement", "node_type_id"];
        $conditionalKeyValuePair = [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
        $this->setItems($items);
    }

    private function fetchAndSetItemAssociationsUnderDocument() {
        $documentIdentifier = $this->getDocumentIdentifier();
        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $returnCollection = true;
        $fieldsToReturn = ["origin_node_id", "destination_node_id", "sequence_number"];
        $conditionalKeyValuePair = [ 
            "document_id" =>  $documentIdentifier,
            "organization_id" => $organizationId,
            "is_deleted" => 0,
            "association_type" => 1 // 1 for isChildOf relation
        ];
        $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                         );
        $this->setItemAssociationsUnderDocument($itemAssociationsUnderDocument);
    }

    /**
     * start transforming of data to ui compatible structure
     */
    private function transformTaxonomyRelatedData() {
        $this->transformDocument();
        $this->transformItems();
        $this->transformItemAssociations();
    }

    private function transformDocument() {
        $document = $this->getDocument();
        $projectIdentifier = $this->getProjectIdentifier();
        $isDocumentEditable = 0;
        $projectEnabled = "0";
        if(!empty($document->project_id) )
        {
            $projectEnabled = "1";
            if($document->project_id===$projectIdentifier) {
                $isDocumentEditable = 1;    
            }      
        }
        
        $projectName = "";
        $documentNodeTypeId = $document->node_type_id;
        $documentNodeTypeTitle = "";

        $parsedDocument = [
            "id"                    => $document->document_id,
            "title"                 => !empty($document->title) ? $document->title : "",
            "human_coding_scheme"   => "",
            "list_enumeration"      => "",
            "full_statement"        => "",
            "status"                => $document->adoption_status,   
            "node_type"             => $documentNodeTypeTitle,
            "node_type_id"          => $documentNodeTypeId,
            "project_name"          => $projectName,
            "project_enabled"       => $projectEnabled,
            "is_editable"           => $isDocumentEditable,
            "is_document"           => 1
        ];
        $this->setTransformedDocument($parsedDocument);
    }

    private function transformItems() {
        $projectIdentifier = $this->getProjectIdentifier();
        $items = $this->getItems();
        $isEditable=[];
        $itemIdsAssignedToProject = $this->getNodeIdsAssignedToProject();
        $projectItem = $this->getProjectItemDetails($projectIdentifier,$itemIdsAssignedToProject);
        $itemArr = $items->toArray();
        $itemData = array_column($itemArr,'item_id');
        foreach ($projectItem as $projectItemV) {
            if(in_array($projectItemV->item_id,$itemData)) {
                $isEditable[$projectItemV->item_id]['is_enable']=$projectItemV->is_editable;
            }else{
                $isEditable[$projectItemV->item_id]['is_enable']=2;
            }
        }
        $parsedItems = [];
        $searchItemArray = [];
        $searchProjectItemArray = [];
        $itemEdit=NULL;
        foreach($items as $item) {
            $itemId = $item->item_id;
            $organizationId     = $this->getRequestUserDetails()["organization_id"];
            $documentIdData     = $this->getItemsDetails($itemId,$organizationId);
            $documentId         = $documentIdData->document_id;
            $projectIdentifier  = $this->getProjectDetails($documentId,$organizationId);
            $projectId =[];
            foreach ($projectIdentifier as $projectIdentifierV) {
                $projectId[] = $projectIdentifierV->project_id;
            }
            $itemEditableFlag   = $this->helperToReturnItemEditableFlagStatus($itemId, $projectId);
            $projectEnabled = $itemEditableFlag===1 ? "1" : 0;
            $projectName = "";
            $listEnumeration = !empty($item->list_enumeration) ? $item->list_enumeration : "";
            $humanCodingScheme = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "";
            $fullStatememnt = !empty($item->full_statement) ? $item->full_statement : "";
            $nodeTypeId = !empty($item->node_type_id) ? $item->node_type_id : "";
            $nodeType = "";
            //$itemArr = $item->toArray();
            $parsedItems[$itemId] = [
                "id" => $itemId,
                "title" => "",
                "human_coding_scheme" => $humanCodingScheme,
                "list_enumeration" => $listEnumeration,
                "full_statement" => $fullStatememnt,
                "node_type" => $nodeType,
                "node_type_id" => $nodeTypeId,
                "is_editable" => '',
                "project_enabled" => $projectEnabled,
                "project_name" => $projectName,
                "is_document" => 0
            ];
            array_push($searchItemArray,$item->item_id);
        }
        // Mapped Data with is editable
        $parsedItems2=[];
        // Foreach iseditable issue resolved 500 error
        if (is_array($isEditable) || is_object($isEditable)){
            foreach ($isEditable as $isEditableK=>$isEditableV)
            {
                foreach ($parsedItems as $parsedItemsV)
                {
                    if($parsedItemsV['id']==$isEditableK)
                    {
                        $parsedItems2[] = [
                            "id" => $parsedItemsV['id'],
                            "title" => "",
                            "human_coding_scheme" => $parsedItemsV['human_coding_scheme'],
                            "list_enumeration" => $parsedItemsV['list_enumeration'],
                            "full_statement"   => $parsedItemsV['full_statement'],
                            "node_type"        => $parsedItemsV['node_type'],
                            "node_type_id"     => $parsedItemsV['node_type_id'],
                            "is_editable"      => $isEditableV['is_enable'],
                            "project_enabled"  => $isEditableV['is_enable'],
                            "project_name"     => $parsedItemsV['project_name'],
                            "is_document"      => 0
                        ];
                    }
                }
            }
        }
        //code for project usable item
        $projectItems = ProjectItem::whereIn('item_id',$searchItemArray)->where('is_deleted','=',0)->where('is_editable','=',1)->get();
        if(count($projectItems) > 0)
        {
            foreach($projectItems as $projectItem) {
                $projectId[]    =   $projectItem->project_id;
            }

            $projectType = $this->helperToReturnProjectType($projectId);
            
            foreach($projectItems as $projectItem) {
                if($projectType[$projectItem->project_id] == 1) {
                    array_push($searchProjectItemArray,$projectItem->item_id);
                }
            }
        }
        // Check item editable status when not in Project
       $getItemIds = array_column($parsedItems2,'id');
       $parsedItems3=[];
       foreach ($parsedItems as $parsedItemsK=>$parsedItemsV2){
               if(!in_array($parsedItemsK,$getItemIds))
               {
                    $itemEnabled = in_array($parsedItemsV2['id'],$searchProjectItemArray) ? 1 : 0;
                    $parsedItems3[] = [
                        "id" => $parsedItemsV2['id'],
                        "title" => "",
                        "human_coding_scheme" => $parsedItemsV2['human_coding_scheme'],
                        "list_enumeration" => $parsedItemsV2['list_enumeration'],
                        "full_statement"   => $parsedItemsV2['full_statement'],
                        "node_type"        => $parsedItemsV2['node_type'],
                        "node_type_id"     => $parsedItemsV2['node_type_id'],
                        "is_editable"      => 2,
                        "project_enabled"  => $itemEnabled,
                        "project_name"     => $parsedItemsV2['project_name'],
                        "is_document"      => 0
                    ];
               }
        
               }

       $finalArr = array_merge($parsedItems2,$parsedItems3);
        $this->setTransformedItems($finalArr);
    }

    private function transformItemAssociations() {
        $itemAssociationsUnderDocument = $this->getItemAssociationsUnderDocument();
        $parsedItemAssociations = [];
        foreach($itemAssociationsUnderDocument as $itemAssociation) {
            $originNodeId = $itemAssociation->origin_node_id;
            $destinationNodeId = $itemAssociation->destination_node_id;
            if(!empty($originNodeId) && !empty($destinationNodeId)) {
                $parsedItemAssociations[] = [
                    "child_id" => $itemAssociation->origin_node_id,
                    "parent_id" => $itemAssociation->destination_node_id,
                    "sequence_number" => $itemAssociation->sequence_number
                ];
            }
        }
        $this->setTransformedItemAssociationsUnderDocument($parsedItemAssociations);
    }

    /**
     * create the ui compatible tree response structure
     */
    private function buildTree () {
        $transformedDocument = $this->getTransformedDocument();
        $transformedItems = $this->getTransformedItems();
        $transformedItemAssociationsUnderDocument = $this->getTransformedItemAssociationsUnderDocument();
        $nodes = array_prepend($transformedItems, $transformedDocument);
        $allOrginNodeArr = [];
        $allDestinationNodeArr = [];
        $nodesDocument = "";
        foreach($transformedItemAssociationsUnderDocument as $transformedItemAssociationsUnderDocumentK => $transformedItemAssociationsUnderDocumentV)
        {
            array_push($allOrginNodeArr,$transformedItemAssociationsUnderDocumentV['child_id']);
            array_push($allDestinationNodeArr,$transformedItemAssociationsUnderDocumentV['parent_id']);
        }
        if(count($allOrginNodeArr) > 0)
        {
            $allOrginNodeArr = array_unique($allOrginNodeArr);
        }
        if(count($allDestinationNodeArr) > 0)
        {
            $allDestinationNodeArr = array_unique($allDestinationNodeArr);
        }
        foreach($nodes as $nodesK => $nodesV)
        {
            if(in_array($nodesV['id'],$allOrginNodeArr) &&  $nodesV['is_document'] == 0)
            {
                $nodes[$nodesK]['is_orphan'] = 0;
            }
            else if(!in_array($nodesV['id'],$allOrginNodeArr) &&  $nodesV['is_document'] == 1) 
            {
                $nodes[$nodesK]['is_orphan'] = 0;
                $nodesDocument = $nodesV['id'];
            }
            else 
            {
                $nodes[$nodesK]['is_orphan'] = 1;
            }
        }
        array_push($allOrginNodeArr,$nodesDocument);
        foreach($transformedItemAssociationsUnderDocument as $transformedItemAssociationsUnderDocumentK => $transformedItemAssociationsUnderDocumentV)
        {
            if(!in_array($transformedItemAssociationsUnderDocumentV['parent_id'],$allOrginNodeArr))
            {
                unset($transformedItemAssociationsUnderDocument[$transformedItemAssociationsUnderDocumentK]);
            }
        }
        $treeData = [
            "nodes" => $nodes,
            "relations" => $transformedItemAssociationsUnderDocument,
        ];
        $this->setTree($treeData);
    }

    private function helperToReturnItemEditableFlagStatus(string $itemIdentifier,$projectIdentifier): int {
        $statusToReturn     = $this->projectRepository->checkIsItemEditable($itemIdentifier, $projectIdentifier);
        return $statusToReturn;
    }

    /**
     * @param $itemIds
     * @param $organizationId
     * @return mixed
     * @purpose This function return document Id based on Item Ids
     */
    private function getItemsDetails($itemIds,$organizationId){
        $documentId= DB::table('items')->select('document_id')
            ->where('item_id',$itemIds)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $documentId;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @Purpose This function return project Id Based on documentId
     */
    private function getProjectDetails($documentId,$organizationId){
        $projectIds= DB::table('projects')->select('project_id')
            ->where('document_id',$documentId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->toArray();
        return $projectIds;
    }

    /**
     * @param $projectId
     * @param $itemIds
     * @return mixed
     * @purpose This function get is_editable based on projectId and ItemId
     */
    private function getProjectItemDetails($projectId,$itemIds)
    {
        $getProjectItem = DB::table('project_items')->select('is_editable','item_id')
                          ->where('project_id',$projectId)
                          ->whereIn('item_id',$itemIds)
                          ->where('is_editable',1)
                          ->get()
                          ->toArray();
        return $getProjectItem;
    }

    private function helperToReturnProjectType(array $projectId) {
        $projectType            =   [];
        $attributeIn            =   'project_id'; 
        $containedInValues      =   $projectId;
        $returnCollection       =   true;
        $fieldsToReturn         =   ['project_id', 'project_type'];
        $keyValuePairs          =   ['is_deleted' => 0];

        $projectTypeDetail = $this->projectRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs );

        foreach($projectTypeDetail as $project) {
            $projectType[$project->project_id] = $project->project_type;
        }

        return $projectType;
    }
    
}
