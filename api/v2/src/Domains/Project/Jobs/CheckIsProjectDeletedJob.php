<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class CheckIsProjectDeletedJob extends Job
{  
    private $identifier;
    private $projectRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $identifier)
    {
        //Set the identifier
        $this->setIdentifier($identifier['project_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set the project repo handler
        $this->projectRepository = $projectRepository;
        //Get Project detail from DB
        $projectDetail = $this->findProjectStatus();

        if($projectDetail->is_deleted == 1) {
            return true;
        }
        else{
            return false;
        }
        
    }

    // Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    private function findProjectStatus(){
        $identifier = $this->getIdentifier();

        return $this->projectRepository->find($identifier);
    }
}
