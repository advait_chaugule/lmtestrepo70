<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Project\Validators\CreateAndUploadProjectSearchDataValidator as Validator;

use App\Services\Api\Traits\StringHelper;

class ValidateInputForCreateAndUploadProjectSearchDataJob extends Job
{

    use StringHelper;

    private $requestInputToValidate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestInputToValidate)
    {
        $this->requestInputToValidate = $requestInputToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->requestInputToValidate);
        return $validation===true ? $validation : $this->validatorMessageParser($validation->messages()->getMessages());
    }
}
