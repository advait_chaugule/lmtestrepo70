<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class DeleteProjectItemJob extends Job
{

    private $projectRepository;

    private $taxonomyIdentifier;
    private $projectIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $taxonomyIdentifier, string $projectIdentifier)
    {
        $this->setTaxonomyIdentifier($taxonomyIdentifier);
        $this->setProjectIdentifier($projectIdentifier);
    }

    public function setTaxonomyIdentifier(string $identifier) {
        $this->taxonomyIdentifier = $identifier;
    }

    public function getTaxonomyIdentifier(): string {
        return $this->taxonomyIdentifier;
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        // set the project datbase repository
       $this->projectRepository = $projectRepository;
       // remove the project-taxonomy mapping
       $this->removeProjectTaxonomyMapping();
    }

    private function removeProjectTaxonomyMapping() {
        $projectIdentifier = $this->getProjectIdentifier();
        $project = $this->projectRepository->find($projectIdentifier);
        if(!empty($project->project_id)) {
            $taxonomyIdentifier = $this->getTaxonomyIdentifier();
            $this->projectRepository->deleteProjectItem($taxonomyIdentifier); 
        }
    }
}
