<?php
namespace App\Domains\Project\Jobs;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\NodeLevelTrait;
use App\Services\Api\Traits\MetadataHelperTrait;
use DB;
class GetProjectNodeDetailsJob extends Job
{
    use NodeLevelTrait;
    use MetadataHelperTrait;
    private $projectId;
    private $organizationId;
    private $viewType;
    private $returnOrder;

    public function __construct($projectId,$organizationId,$viewType,$returnOrder){
        $this->projectId      = $projectId;
        $this->organizationId = $organizationId;
        $this->viewType       = $viewType;
        $this->returnOrder    = $returnOrder;
    }

    public function handle(){
        if($this->returnOrder == '0'){
            $getSetting = $this->getProjectSetting($this->projectId,$this->organizationId);
            if(!empty($getSetting)){
                $projectDetails   = $this->prepareProjectNodeData($this->projectId,$this->organizationId,$this->viewType);
            }else{
                $projectDetails   = $this->prepareDefaultProjectJson();
            }
        }else{
            $projectDetails = $this->prepareProjectNodeData($this->projectId,$this->organizationId,$this->viewType);
        }
        return $projectDetails;
    }

    private function prepareProjectNodeData($projectId,$organizationId,$viewType){
     $projectItemIds = $this->getProjectItemId($projectId);
     $itemIds        = array_column($projectItemIds,'item_id');
     $getNodeTypeIds = $this->getNodeTypeIds($itemIds,$organizationId);
     $nodeTypeIds    = array_column($getNodeTypeIds,'node_type_id');
        $nodeDetails = $this->getNodeTypesDetails($nodeTypeIds);
        $nodeTypeArr =[];
        $documentId  = array_column($getNodeTypeIds,'document_id');
        // Get node level of document with order and compatibility
        $nodeDepth = !empty($documentId) ? $this->getNodesDepth($documentId[0],$itemIds) : [];
        
        $default = [];
		if(isset($nodeDepth['sequence'])){
			$default = array_values($nodeDepth['sequence']);
			$nodeDepth['sequence'] = array_unique($nodeDepth['sequence']);
        }
        $compatibility = isset($nodeDepth['compatibility']) ? $nodeDepth['compatibility'] : true;
		// Add missing nodetypes in sequence for sorting
        foreach ($nodeDetails as $nodeDetailsK=>$nodeDetailsV) {
            $nodeTypeId =  $nodeDetailsV->node_type_id;
            if(!isset($nodeDepth['sequence']) || !in_array($nodeTypeId,$nodeDepth['sequence']))
                $nodeDepth['sequence'][] = $nodeTypeId;
        }
        $metadataNodeMapping=[];
        /*Get metadata ids from node ids*/
        $metadataIdsList  = $this->getMetadataIds($nodeTypeIds);
        if($metadataIdsList) {
            // To sort nodetype and metadata mapping array in a same sequence
            foreach ($nodeDepth['sequence'] as $value) {
                foreach ($nodeDetails as $nodeDetailsK=>$nodeDetailsV) {
                $nodeTypeId = $nodeDetailsV->node_type_id;                
                    if($nodeTypeId == $value){
                        $nodeTypeArr[$nodeTypeId] = ['display_name' =>$nodeDetailsV->title];
                    }
                }
                foreach ($metadataIdsList as $metadataIdsK => $metadataIdsV) {
                    $metadataNodeTypeId = $metadataIdsV->node_type_id;
                    if($metadataNodeTypeId == $value){
                        $metadataNodeMapping[$metadataNodeTypeId][] = ['metadata_id' => $metadataIdsV->metadata_id];
                    }
                }
            }
            $metadataIds = array_column($metadataIdsList,'metadata_id');
            $getMetadataDetails = $this->getMetadataDetailsList($metadataIds,$organizationId);
            $metadataArr = [];
            if($getMetadataDetails) {
                foreach ($getMetadataDetails as $getMetadataDetailsK => $getMetadataDetailsV) {
                    $metadataId = $getMetadataDetailsV->metadata_id;
                    $metadataArr[$metadataId] = ['display_name' => $getMetadataDetailsV->name,
                        'internal_name' => $getMetadataDetailsV->internal_name,
                        'is_custom' => $getMetadataDetailsV->is_custom
                    ];
                }
            }
            /*Prepare metadata details*/
            $nodeMetadataValues =[];
            foreach ($metadataArr as $metadataArrK=>$metadataArrV){
                foreach ($metadataNodeMapping as $metadataNodeMappingK=>$metadataNodeMappingV)
                {
                    foreach ($metadataNodeMappingV as $key=>$metadataNodeMappingData)
                        if($metadataArrK==$metadataNodeMappingData['metadata_id']) {
                            if($metadataArrV['internal_name'] =='language')
                            {
                                $metadataArrV['internal_name']= 'language_name';
                            }
                            $nodeMetadataValues[$metadataNodeMappingK]['metadata'][]=
                                [
                                    'display_name'  => isset($metadataArrV['display_name'])?$metadataArrV['display_name']:"",
                                    'internal_name' => isset($metadataArrV['internal_name'])?$metadataArrV['internal_name']:'',
                                    'metadata_id'   => $metadataArrK,
                                    'is_custom'     => isset($metadataArrV['is_custom'])?$metadataArrV['is_custom']:"",
                                ];
                        }
                }
            }
            //Mapping Node Id with their node name
            $nodeNameMappingWithNodeIds=[];
            foreach ($nodeTypeArr as $nodeTypeArrK=>$nodeTypeArrV) {
                $nodeNameMappingWithNodeIds[$nodeTypeArrK] = [
                    'node_type_id' => isset($nodeTypeArrK)?$nodeTypeArrK:"",
                    'internal_name'=>isset($nodeTypeArrV['display_name'])?$nodeTypeArrV['display_name']:"",
                    'display_name' =>isset($nodeTypeArrV['display_name'])?$nodeTypeArrV['display_name']:"",
                ];
            }
            //Finally merge data of node and metadata
            $NodeWithMetaData=[];
            foreach ($nodeMetadataValues as $nodeMetadataValK => $nodeMetadataValV) {
                foreach ($nodeNameMappingWithNodeIds as $nodeNameMappingWithNodeIdK=>$nodeNameMappingWithNodeIdV) {
                    if($nodeNameMappingWithNodeIdK==$nodeMetadataValK) {
                        $NodeWithMetaData['nodeTypes'][] = array_merge($nodeNameMappingWithNodeIdV,$nodeMetadataValV);
                    }
                }
            }
            $NodeWithMetaData['defaultNodeTypesOrder'] = $default;
            $NodeWithMetaData['compatibility'] = $compatibility;
            $prepareArr=[];
            if($viewType=='node_type') {
                return $NodeWithMetaData;
            }else if($viewType=='metadata'){
                foreach ($nodeMetadataValues as $metadataKey => $metadataValue) {
                    $metadataList = $metadataValue['metadata'];
                    foreach ($metadataList as $metadataListV) {
                        array_push($prepareArr,$metadataListV);
                    }
                }
                $metadataId            = array_column($prepareArr,'metadata_id');
                $uniqueMetadataId[]    = array_unique($metadataId);
                $uniqueMetadataList = array_map("unserialize", array_unique(array_map("serialize", $prepareArr)));
                $uniqueMetadataVal=[];
                foreach ($uniqueMetadataList as $uniqueMetadataListV) {
                    $uniqueMetadataVal['metadata'][] = $uniqueMetadataListV;
                }
                return $uniqueMetadataVal;
            }

        }

    }

    private function getProjectItemId($projectId)
    {
        $itemIds = DB::table('project_items')
                  ->select('item_id')
                  ->where('project_id',$projectId)
                  ->where('is_deleted',0)
                  ->distinct('item_id')
                  ->get();
        if($itemIds) {
            $itemIds = $itemIds->toArray();
            return $itemIds;
        }
    }

    /**
     * @param $itemIds
     * @param $organizationId
     * @return mixed
     * @FunctionName getNodeTypeIds
     * @Purpose This function is used to return node_type_id from Item table based on Item Ids
     */
    private function getNodeTypeIds($itemIds,$organizationId)
    {
        $nodeTypeId = DB::table('items')
                      ->select('node_type_id','document_id')
                      ->whereIn('item_id',$itemIds)
                      ->where('organization_id',$organizationId)
                      ->where('is_deleted',0)
                      ->distinct('node_type_id')
                      ->get();
        if($nodeTypeId) {
            $nodeTypeId = $nodeTypeId->toArray();
            return $nodeTypeId;
        }
    }

    /**
     * @param $nodeIds
     * @return mixed
     * @FunctionName getNodeTypesDetails
     * @Purpose This function is used to return all node details based on node Ids
     */
    private function getNodeTypesDetails($nodeIds)
    {
        $nodeDetails = DB::table('node_types')
            ->select('node_type_id','title')
            ->whereIn('node_type_id',$nodeIds)
            ->where('is_deleted',0)
            ->where('used_for',0)
            ->get()
            ->toArray();
        return $nodeDetails;
    }

    /**
     * @param $nodeIds
     * @return mixed
     * @FunctionName getMetadataIds
     * @Purpose This function is used to return all metadata Ids based on node type Ids
     */
    private function getMetadataIds($nodeIds)
    {
        $metadataIds = DB::table('node_type_metadata')
            ->select('metadata_id','node_type_id')
            ->whereIn('node_type_id',$nodeIds)
            ->distinct('metadata_id')
            ->get()
            ->toArray();
        return $metadataIds;
    }

    /**
     * @param $metadataIds
     * @param $organizationId
     * @return mixed
     * @FunctionName getMetadataDetailsList
     * @purpose This function is used to metadata details based on metadata Ids
     */
    private function getMetadataDetailsList($metadataIds,$organizationId)
    {
        $metadataList = DB::table('metadata')
            ->select('metadata_id','name','internal_name','is_custom')
            ->whereIn('metadata_id',$metadataIds)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->toArray();
        return $metadataList;
    }

    public function prepareDefaultProjectJson()
    {
        // Removed hard coded metadata and added code to fetch from db - Start
        $defaultInternalNames = array("full_statement","node_type","human_coding_scheme");
        $metadataName = $this->getmetadataName($this->organizationId,$defaultInternalNames);
        $default_project_list = array(array($metadataName["full_statement"],"full_statement","120"),array($metadataName["node_type"],"node_type","120"),array($metadataName["human_coding_scheme"],"human_coding_scheme","120"));
        // Removed hard coded metadata and added code to fetch from db - End
        $num = 1;
        $valuesArray =[];
        foreach ($default_project_list as $default_project_listK=>$default_project_listV)
        {
            $valuesArray[] = 
               ['metadata_id'=> $this->getmetadataid($this->organizationId,$default_project_listV[1]),
                    'display_name' => $default_project_listV[0],
                    'internal_name'=> $default_project_listV[1],
                    'order' => $num,
                    'width' => $default_project_listV[2],
                    'is_custom' => 0,
                ];
                
            $num++;
            // $finalArr  = array_merge($valueArr,$valuesArray);
        }
        //$tableConfig = ['table_config'=>$valuesArray];
        $valueArr = ['node_type_id'=>'',
        'display_name' => '',
        'internal_name'=> '',
           ];
      $finalArr  = array_merge($valueArr,['metadata'=>$valuesArray]);
        return $finalArr;
    }


    private function getmetadataid($orgnizationIdentifier,$metadataname){
        $queryData = DB::table('metadata')->select('metadata_id')
       ->where('organization_id','=', $orgnizationIdentifier)
       ->where('internal_name','=',$metadataname)
       ->get()
       ->toArray();

       if(!empty($queryData)){
           $metadata_id = $queryData[0]->metadata_id;
       }else{
           $metadata_id = "";
       }
       return $metadata_id;
    }

    /**
     * @param $projectId
     * @param $organizationId
     * @return mixed
     * @FunctionName getProjectSetting
     * @this function is used to get project setting
     */
    private function getProjectSetting($projectId,$organizationId)
    {
          $projectSetting = DB::table('user_settings')->select('json_config_value')
                              ->where('id',$projectId)
                              ->where('organization_id',$organizationId)
                              ->where('id_type',4)
                              ->where('is_deleted',0)
                              ->get()
                              ->first();
          return $projectSetting;
    }
}