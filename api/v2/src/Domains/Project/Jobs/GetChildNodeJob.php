<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Models\Item;

class GetChildNodeJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputs,string $documentId)
    {
        $this->inputs = $inputs;
        $this->documentID = $documentId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $allInput = $this->inputs;
        $allinput_item_ids=array();
        foreach($allInput as $itemId)
        {
            $input_item_ids=array();
            $itemRepo->getHierarchyAllChildItems($this->documentID,$itemId, $input_item_ids); 
            array_push($input_item_ids,['item_id'=>$itemId,'parent_id' =>$itemId]);
            $allinput_item_ids = array_merge($allinput_item_ids,$input_item_ids);
        }
       return $allinput_item_ids;
    }
}
