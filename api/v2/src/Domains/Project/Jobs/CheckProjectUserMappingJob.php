<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class CheckProjectUserMappingJob extends Job
{
    private $projectIdentifier;

    private $userId;
    private $roleId;

    private $requestData;
    private $workflowStageRoleIdentifier;

    private $projectRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input, array $requestData)
    {
        //Set project identifier
        $this->setProjectIdentifier($input['project_id']);
        $this->requestData = $requestData; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository, UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository)
    {
        //Set the project repo handler
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        $checkUserToProjectRoleExists = $this->checkUserProjectRoleIsExists();

        return $checkUserToProjectRoleExists;

    }

    //Public Getter and Setter methods
    public function setProjectIdentifier($identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(){
        return $this->projectIdentifier;
    }

    public function setUserIdentifier($userId) {
        $this->userId = $userId;
    }

    public function getUserIdentifier(){
        return $this->userId;
    }

    public function setRoleIdentifier($roleId) {
        $this->roleId = $roleId;
    }

    public function getRoleIdentifier(){
        return $this->roleId;
    }

    private function getUserName() {
        $userIdentifier = $this->getUserIdentifier();

        $userDetail = $this->userRepository->find($userIdentifier)->toArray();

        return (!empty($userDetail['first_name']) ? $userDetail['first_name'] : " ").' '.(!empty($userDetail['last_name']) ? $userDetail['last_name'] : " ");
    }

    private function getRoleName() {
        $roleIdentifier = $this->getRoleIdentifier();

        $roleDetail = $this->roleRepository->find($roleIdentifier)->toArray();

        return !empty($roleDetail['name']) ? $roleDetail['name'] : " ";
    }

    //Method to assign user to project role 
    private function checkUserProjectRoleIsExists(){
        $userExists = [];
        $projectIdentifier = $this->getProjectIdentifier();
        $inputData = $this->requestData;

        // foreach($inputData['input'] as $data){
            
        // }

        $this->setUserIdentifier($inputData['user_id']);
        $this->setRoleIdentifier(explode('||', $inputData['workflow_stage_role_id'])[2]);

        $checkUserExists = $this->projectRepository->checkUserToProjectExists($projectIdentifier, $inputData['user_id'], $inputData['workflow_stage_role_id']);

        if($checkUserExists === true){
            $userExists[] = [
                'user_id' => $inputData['user_id'],
                'workflow_stage_role_id' => $inputData['workflow_stage_role_id'],
                'user_name' => $this->getUserName(),
                'role_name' => $this->getRoleName(),
                'user_exists' => true,
            ];
        }
        else{
            $userExists[] = [
                'user_id' => $inputData['user_id'],
                'workflow_stage_role_id' => $inputData['workflow_stage_role_id'],
                'user_name' => $this->getUserName(),
                'role_name' => $this->getRoleName(),
                'user_exists' => false, 
            ];  
        }

        return $userExists;
    }
}
