<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ProjectAccessRequestRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class ProjectAccessRequestDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    use DateHelpersTrait;

    private $projectRepo;
    private $organizationIdentifier;
    private $projectAccessRequestDetails;
    private $projectAccessRequestDetail;
    private $updatedByName;
    private $userName; 
    private $userEmail; 
    private $status;
    private $statusText;
    private $workflowStageRoleIds;
    private $arrtWorkflowStageRoles;
    private $workflowStageRoles;
    private $statusChangedAt;
    private $projectName;



    
    public function __construct(array $input)
    {
        $this->input = $input;
        $organizationIdentifier = $this->input['organization_id'];
        $projectAccessRequestId = $this->input['project_access_request_id'];
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->setProjectAccessRequestId($projectAccessRequestId);
    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectAccessRequestRepositoryInterface $projectRepo, RoleRepositoryInterface $roleRepository)
    {
        //

        $this->projectAccessRequestRepository  = $projectRepo;
        $this->roleRepository = $roleRepository;

        $projectAccessRequestDetails = $this->getProjectAccessRequestDetailFromModel();  
        $allProjectAccessRequestDetail = array();
        foreach($projectAccessRequestDetails as $projectAccessRequestDetail)
        {
           if(isset($projectAccessRequestDetail->project->project_id))
           {
            if($projectAccessRequestDetail->requestAccessUser->is_active == 1 && $projectAccessRequestDetail->requestAccessUser->is_deleted == 0)
            {
                $userName = $projectAccessRequestDetail->requestAccessUser->first_name.' '.$projectAccessRequestDetail->requestAccessUser->last_name;     
                $userEmail = $projectAccessRequestDetail->requestAccessUser->email; 
                $userId = $projectAccessRequestDetail->user_id;
            }
            else
            {
                $userName = "";
                $userEmail = "";
                $userId = "";
            } 
            if($projectAccessRequestDetail->requestAccessUpdatedBy->is_active == 1 && $projectAccessRequestDetail->requestAccessUpdatedBy->is_deleted == 0)
            {
                $updatedByName = $projectAccessRequestDetail->requestAccessUpdatedBy->first_name.' '.$projectAccessRequestDetail->requestAccessUpdatedBy->last_name; 
            }
            else
            {
                $updatedByName = "";
            }
            if($projectAccessRequestDetail->project->is_deleted == 0)
            {
                $projectName = $projectAccessRequestDetail->project->project_name;
                $projectId = $projectAccessRequestDetail->project_id;
            }
            else
            {
                $projectName = "";
                $projectId = "";
            }
            
            $status = $projectAccessRequestDetail->status;
            $workflowStageRoleIds = explode(",",$projectAccessRequestDetail->workflow_stage_role_ids);
            
            foreach($workflowStageRoleIds as $workflowStageRoleId)
            {
                $this->setRoleIdentifier(explode('||', $workflowStageRoleId)[2]);
                $role_name = $this->getRoleName();
                if($role_name != "")
                {
                    $arrtWorkflowStageRoles [] = $this->getRoleName();
                    $arrtWorkflowStageIds [] = $workflowStageRoleId;
                }
               

            }
           
            //array_filter($arr)
           // $workflowStageRoles =implode(',', $arrtWorkflowStageRoles);
          //  unset($arrtWorkflowStageRoles);
            if($status == 1)
            {
                $statusText = "pending";
                $statusChangedAt = "";
                
            }
            else if($status == 2)
            {
                $statusText = "accepted";
                $statusChangedAt = $this->formatDateTime($projectAccessRequestDetail->status_changed_at);
            }
            else if($status == 3)
            {
                $statusText = "rejected";
                $statusChangedAt = $this->formatDateTime($projectAccessRequestDetail->status_changed_at);
            }
            if($projectAccessRequestDetail->project->is_deleted == 0 && $projectAccessRequestDetail->requestAccessUser->is_deleted == 0) 
            {
                $allProjectAccessRequestDetail[] = [
                    "project_access_request_id"    =>  $projectAccessRequestDetail->project_access_request_id,
                    "project_id"    =>  $projectId,
                    "project_name"    =>  $projectName,
                    'user_id' => $userId,
                    'user_name' => $userName,
                    'user_email' => $userEmail,
                    'workflow_stage_role_ids' => $arrtWorkflowStageIds,
                    'workflow_stage_roles' => $arrtWorkflowStageRoles,
                    'status' => $status,
                    'status_text' => $statusText,
                    'comment' => $projectAccessRequestDetail->comment,
                    'date_of_application' => $this->formatDateTime($projectAccessRequestDetail->created_at),
                    'date_of_last_status_change' => $this->formatDateTime($projectAccessRequestDetail->updated_at),
                    'updated_by' => $projectAccessRequestDetail->updated_by,
                    'updated_by_name' => $updatedByName,
                ];
             }
            unset($arrtWorkflowStageRoles);
            unset($arrtWorkflowStageIds);
           }  
        }
        return $allProjectAccessRequestDetail;

    }


    private function getProjectAccessRequestDetailFromModel() 
        {
            
            $organizationIdentifier =   $this->getOrganizationIdentifier();
            $projectAccessRequestId =   $this->getProjectAccessRequestId();
            $relations      = ['project','requestAccessUser','requestAccessUpdatedBy'];
           
           if($projectAccessRequestId == "")
           {
            $returnCollection = true;
            $keyValuePairs = ['organization_id' => $organizationIdentifier];
           }
           else
           {
            $returnCollection = true;
            $keyValuePairs = ['organization_id' => $organizationIdentifier, 'project_access_request_id' => $projectAccessRequestId];
           }
            $fieldsToReturn = ['project_access_request_id', 
                               'project_id',
                               'user_id',
                               'workflow_stage_role_ids', 
                               'status', 
                               'comment', 
                               'created_at',
                               'updated_at', 
                               'status_changed_at', 
                               'updated_by'];
            
             $projectRequestDetails = $this->projectAccessRequestRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

            return $projectRequestDetails;
            

        }

        public function setOrganizationIdentifier($identifier){
            $this->organizationIdentifier = $identifier;
        }

        public function getOrganizationIdentifier(){
            return $this->organizationIdentifier;
        }
        public function setProjectAccessRequestId($identifier){
            $this->projectAccessRequestId = $identifier;
        }

        public function getProjectAccessRequestId(){
            return $this->projectAccessRequestId;
        }
        
        public function setRoleIdentifier($roleId) {
            $this->roleId = $roleId;
        }
    
        public function getRoleIdentifier(){
            return $this->roleId;
        }

        private function getRoleName() {
            $roleIdentifier = $this->getRoleIdentifier();
    
            $roleDetail = $this->roleRepository->find($roleIdentifier)->toArray();
            if($roleDetail['is_active'] == 1 && $roleDetail['is_deleted'] == 0)
            {
                $roleName = $roleDetail['name'];
            }
            else
            {
                $roleName = "";
            }
            return !empty($roleName) ? $roleName : " ";
        }
}
