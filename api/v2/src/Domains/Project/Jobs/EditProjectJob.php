<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class EditProjectJob extends Job
{
    private $dataToUpdate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputs)
    {
        $this->dataToUpdate = $inputs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        $project_id = $this->dataToUpdate["project_id"];
        $columnValueFilterPairs = [ "project_id" => $project_id];
        return $projectRepo->editWithCustomizedFields($columnValueFilterPairs, $this->dataToUpdate);
    }
}
