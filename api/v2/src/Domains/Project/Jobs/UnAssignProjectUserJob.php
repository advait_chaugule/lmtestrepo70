<?php
namespace App\Domains\Project\Jobs;
ini_set('max_execution_time', 0);
use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class UnAssignProjectUserJob extends Job
{
    private $identifier;
    private $requestData;
    private $organizationId;
    private $userIdentifier;
    private $projectRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input, $requestData,$organizationId,$userId)
    {
        //Set the private identifier attribute
        $this->setIdentifier($input);
        $this->requestData = $requestData;
        $this->organizationId= $organizationId;
        $this->setUserIdentifier($userId);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set project repository handler
        $this->projectRepository = $projectRepository;

        $deleteUserToProjectRole = $this->deleteUserToProjectRole();

        return $deleteUserToProjectRole;
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }
    public function setUserIdentifier($userId) {
        $this->userIdentifier = $userId;
    }

    public function getUserIdentifier(){
        return $this->userIdentifier;
    }

    private function deleteUserToProjectRole(){
        $projectIdentifier = $this->getIdentifier();
        $requestData    = $this->requestData;
        $organizationId = $this->organizationId;
        $userIdentifier = $this->getUserIdentifier();

        $workflowStageId    = explode('||',$requestData['workflow_stage_role_id']);
        $workflowId         =  $workflowStageId[0];
        $roleId             =  $workflowStageId[2];

        $getWorkflow        = $this->projectRepository->getWorkflowDetails($workflowId);
        $workflowStageId    = array_column($getWorkflow,'workflow_stage_id');
        $getWorkflowRole    =  $this->projectRepository->getStageWorkflow($workflowStageId,$roleId);
        $workFlowStageRole  = array_column($getWorkflowRole,'workflow_stage_role_id');

        $deleteRole=NULL;
        foreach ($workFlowStageRole as $key=>$workflowStageRoleId) {
            $attributes = [
                'user_id'    => $requestData['user_id'],
                'workflow_stage_role_id' => $workflowStageRoleId,
                'deleted_by' => $userIdentifier,
            ];
            $deleteRole = $this->projectRepository->unAssignUserFromProject($projectIdentifier,$attributes,$organizationId);

        }
        return $deleteRole;
    }
}
