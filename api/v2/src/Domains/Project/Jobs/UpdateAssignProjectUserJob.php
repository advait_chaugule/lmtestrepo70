<?php
namespace App\Domains\Project\Jobs;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use Lucid\Foundation\Job;

class UpdateAssignProjectUserJob extends Job
{
    private $projectIdentifier;
    private $requestData;
    private $workflowStageRoleIdentifier;

    private $projectRepository;
    private $userIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input, array $requestData, string $userId)
    {
        //Set project identifier
        $this->setProjectIdentifier($input['project_id']);
        $this->setUserIdentifier($userId);
        $this->requestData = $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set the project repo handler
        $this->projectRepository = $projectRepository;

        $assignUserToProjectRole = $this->saveUserProjectRole();

        return $assignUserToProjectRole;
    }

    public function setUserIdentifier($userId){
        $this->userIdentifier = $userId;
    }
    //Public Getter and Setter methods
    public function setProjectIdentifier($identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(){
        return $this->projectIdentifier;
    }
    public function getUserIdentifier(){
        return $this->userIdentifier;
    }

    //Method to assign user to project role
    private function saveUserProjectRole(){
        $projectIdentifier = $this->getProjectIdentifier();
        $userIdentifier = $this->getUserIdentifier();

        $inputData = $this->requestData;

        $attributes = [
            'project_id' => $projectIdentifier,
            'user_id'    => $inputData['user_id'],
            'workflow_stage_role_id' => $inputData['workflow_stage_role_id'],
            'updated_by' => $userIdentifier
        ];

        return $this->projectRepository->updateAssignUserToProject($attributes);
    }
}
