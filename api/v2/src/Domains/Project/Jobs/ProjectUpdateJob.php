<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class ProjectUpdateJob extends Job
{
    private $projectId;
    private $deleteStatusData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input, array $attributes)
    {
        $this->projectId = $input;
        $this->updateStatusData = $attributes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        return $projectRepo->editWithCustomizedFields($this->projectId, $this->updateStatusData);
    }
}
