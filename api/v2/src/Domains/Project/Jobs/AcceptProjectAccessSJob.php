<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectAccessRequestRepositoryInterface;


class AcceptProjectAccessSJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectAccessRequestRepositoryInterface $projectRepo)
    {
        //
        $id = $this->input['project_access_request_id'];
        $input['status']=  $this->input['status'];
      //  $input['comment']= $this->input['comment'];
        $input['workflow_stage_role_ids']= $this->input['workflow_stage_role_ids'];
        $input['project_id']= $this->input['project_id'];
        $input['updated_at']= now()->toDateTimeString();
        $input['status_changed_at']= now()->toDateTimeString();
        $input['updated_by']= $this->input['updated_by'];
        return $projectRepo->edit($id, $input);

        
    }
}
