<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Project;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class CreateProjectSearchDataJob extends Job
{

    use StringHelper, DateHelpersTrait;
    
    private $projectRepository;

    private $projectId;
    private $project;

    private $parsedProjectUpdatesSearchData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectId)
    {
        $this->projectId = $projectId;
    }

    public function setProject(Project $data) {
        $this->project = $data;
    }

    public function getProject(): Project {
        return $this->project;
    }

    public function setParsedProjectUpdatesSearchData(Collection $data) {
        $this->parsedProjectUpdatesSearchData = $data;
    }

    public function getParsedProjectUpdatesSearchData(): Collection {
        return $this->parsedProjectUpdatesSearchData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;

        $this->fetchAndSetProject();
        $this->prepareAndSetProjectSearchData();
        
        return $this->getParsedProjectUpdatesSearchData();
    }

    private function fetchAndSetProject() {
        $project = $this->projectRepository->find($this->projectId);
        $this->setProject($project);
    }

    private function prepareAndSetProjectSearchData() {
        $project = $this->getProject();
        $identifier = $project->project_id;
        
        $deleteOrAddStatusTextForCloudSearch = $project->is_deleted===1 ? 'delete' : 'add';
        $organizationIdentifier = $project->organization_id;
        $type = $project->project_type===1 ? 'project' : 'pacing_guide';
        $projectWorkflowId = $project->workflow_id;
        $projectName = $project->project_name;
        $updatedBy = $project->updated_by ?: "";
                    
        // modify updateOn cloud search index logic
        if(!empty($project->updated_at) && $project->updated_at!=="0000-00-00 00:00:00") {
            $updateOn = $this->formatDateTimeToCloudSarchCompatibleFormat($project->updated_at->toDateTimeString());
        }
        else if(!empty($project->created_at) && $project->created_at!=="0000-00-00 00:00:00") {
            $updateOn = $this->formatDateTimeToCloudSarchCompatibleFormat($project->created_at->toDateTimeString());
        }
        else {
            $updateOn = "1970-01-01T00:00:00Z";
        }

        $parsedData = [
            'id' => $identifier,
            'type' => $deleteOrAddStatusTextForCloudSearch,
            'fields' => [
                'identifier' => $identifier,
                'organization_identifier' => $organizationIdentifier,
                'project_name' => $projectName,
                'type' => $type,
                'project_id' => $identifier,
                'project_workflow_id' => $projectWorkflowId,
                'adoption_status' => 0, // set default value for project
                'item_association_type' => [],
                'updated_by' => $updatedBy,
                'update_on' => $updateOn
            ]
        ];

        $collection = collect([$parsedData]); // wrap single document inside an array
        $this->setParsedProjectUpdatesSearchData($collection);
    }

    
}
