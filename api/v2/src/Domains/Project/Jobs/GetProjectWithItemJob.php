<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class GetProjectWithItemJob extends Job
{

    private $projectRepository;

    private $projectIdentifier;
    private $project;
    private $associatedItems;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier)
    {
        $this->projectIdentifier = $projectIdentifier;
    }

    public function setProjectIdentifier(string $projectIdentifier) {
        $this->projectIdentifier = $projectIdentifier;
    }

    public function getProjectIdentifier(): string {
        return  $this->projectIdentifier;
    }

    public function setProject($data) {
        $this->project = $data;
    }

    public function getProject() {
        return  $this->project;
    }

    public function setAssociatedItem($data) {
        $this->associatedItems = $data;
    }

    public function getAssociatedItems() {
        return $this->associatedItems;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        // set project data repository
        $this->projectRepository = $projectRepository;
        // find project ans set it
        $this->fetchAndSetProject();
        // find the associated item of this project and set it
        $this->fetchAndSetAssociatedItem();
        
        // prepare to return
        return $this->prepareAndReturndata(); 
    }

    /**
     * Fetch project data from database and set it
     */
    private function fetchAndSetProject() {
        $projectIdentifier = $this->getProjectIdentifier();
        $project = $this->projectRepository->find($projectIdentifier);
        $projectData = !empty($project) ? $project : [];
        $this->setProject($projectData);
    }

    private function fetchAndSetAssociatedItem() {
        $project = $this->getProject();
        // currenty db schema is designed to return many items
        $items = $project->items;
        // if found then fetch the first one
        $item = $items->count() > 0 ? $items : [];
        $this->setAssociatedItem($item);
    }

    private function prepareAndReturndata(): array {
        return [
            "project" => $this->getProject(),
            "associatedItems" => $this->getAssociatedItems()
        ];
    }
}
