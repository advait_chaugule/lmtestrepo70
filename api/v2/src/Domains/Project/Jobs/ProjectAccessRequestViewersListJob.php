<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class ProjectAccessRequestViewersListJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //
        $this->input = $input;
        $permissionIdentifier = $this->input['permission_id'];
        $organizationIdentifier = $this->input['organization_id'];

        $this->setPermissionIdentifier($permissionIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        //
        $this->userRepository = $userRepository;
        $permissionId = $this->getPermissionIdentifier();
        $organizationId = $this->getOrganizationIdentifier();
        $accessPermissionUserList = $this->userRepository->getTenentUserForPermission($organizationId, $permissionId);
        return $accessPermissionUserList;
    }

    public function setPermissionIdentifier($identifier){
        $this->permissionIdentifier = $identifier;
    }

    public function getPermissionIdentifier(){
        return $this->permissionIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }
}
