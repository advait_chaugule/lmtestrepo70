<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Item;
use App\Data\Models\Document;

use Illuminate\Database\Eloquent\Collection;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;

class GetProjectTaxonomyTreeJob extends Job
{

    use ArrayHelper;

    private $itemRepository;
    private $projectRepository;
    private $documentRepository;

    private $projectIdentifier;
    private $mappedNodes;

    private $itemAssociations;

    private $transformedItems = [];
    private $transformedItemAssociations = [];

    private $tree;

    private $document;
    private $transformedDocument = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier, array $mappedNodes)
    {
        $this->setProjectIdentifier($projectIdentifier);
        $this->setMappedNodes($mappedNodes);
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    public function setMappedNodes($data) {
        $this->mappedNodes = $data;
    }

    public function getMappedNodes() {
        return $this->mappedNodes;
    }

    public function setItemIdentifier(string $identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(): string {
        return $this->itemIdentifier;
    }

    public function setItemAssociations($data) {
        $this->itemAssociations = $data;
    }

    public function getItemAssociations() {
        return $this->itemAssociations;
    }

    public function setTransformedItems(array $data) {
        $this->transformedItems = $data;
    }

    public function getTransformedItems(): array {
        return $this->transformedItems;
    }

    public function setTransformedItemAssociations(array $data) {
        $this->transformedItemAssociations = $data;
    }

    public function getTransformedItemAssociations(): array {
        return $this->transformedItemAssociations;
    }

    public function setTree(array $data) {
        $this->tree = $data;
    }

    public function getTree(): array {
        return $this->tree;
    }

    public function setDocument($data) {
        $this->document = $data;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setTransformedDocument(array $data) {
        $this->transformedDocument = $data;
    }

    public function getTransformedDocument(): array {
        return $this->transformedDocument;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ItemRepositoryInterface $itemRepository,
        ProjectRepositoryInterface $projectRepository,
        DocumentRepositoryInterface $documentRepository
    )
    {
        // set db repositories
        $this->itemRepository = $itemRepository;
        $this->projectRepository = $projectRepository;
        $this->documentRepository = $documentRepository;

        // start fetching document, items and item associations
        $this->fetchAndSetTaxonomyRelatedData();

        // start transforming of data to ui compatible structure
        $this->transformTaxonomyRelatedData();

        // create the ui compatible tree response structure
        $this->buildTree();

        return $this->getTree();
    }

    /**
     * start fetching document, items and item associations from database
     */
    private function fetchAndSetTaxonomyRelatedData() {
        $this->fetchAndSetDocument();
        $this->fetchAndSetItemAssociations();
    }

    private function fetchAndSetDocument() {
        $projectIdentifier = $this->getProjectIdentifier();
        $returnCollection = false;
        $fieldsToReturn = [ "document_id", "title", "node_type_id", "project_id" ];
        $conditionalKeyValuePair = [ "project_id" => $projectIdentifier ];
        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
        $this->setDocument($document);
    }

    private function fetchAndSetItemAssociations() {

        $itemAssociations = [];

        $mappedNodes = $this->getMappedNodes();
        $queryConditions = [
            ["is_deleted", "=", 0],
            ["association_type", "=", 1], // 1 for isChildOf relation
        ];
        $fieldsToReturn = ["origin_node_id", "destination_node_id"];

        foreach($mappedNodes as $nodes){
            $nodeDetail = $this->itemRepository->getItemDetailsWithProjectAndCaseAssociation($nodes['item_id']);
            $itemAssociations[] = $nodeDetail->itemAssociations()->select($fieldsToReturn)->where($queryConditions)->orderBy('source_item_association_id')->get();
        }

        $this->setItemAssociations($itemAssociations);
    }

    private function transformDocument() {
        $document = $this->getDocument();
        if(!empty($document->document_id)) {
            $projectIdentifier = $this->getProjectIdentifier();
            $isDocumentEditable = 0;
            if(!empty($document->project_id) && $document->project_id===$projectIdentifier) {
                $isDocumentEditable = 1;
            }
            $parsedDocument = [
                "id" => $document->document_id,
                "title" => !empty($document->title) ? $document->title : "",
                "list_enumeration" => "",
                "human_coding_scheme" => "",
                "full_statement" => "",
                "node_type" => !empty($document->nodeType->title) ? $document->nodeType->title : "",
                "node_type_id" => !empty($document->node_type_id) ? $document->node_type_id : "",
                "is_editable" => $isDocumentEditable,
                "is_document" => 1
            ];
            $this->setTransformedDocument($parsedDocument);
        }
    }

    private function transformItems() {
        $projectIdentifier = $this->getProjectIdentifier();
        $items = $this->getMappedNodes();
        $parsedItems = [];
        foreach($items as $item) {

            $itemDetail = $this->itemRepository->find($item['item_id']);

            $itemEditableFlag = $this->projectRepository->checkIsItemEditable($item['item_id'], $projectIdentifier);
            $listEnumeration = !empty($itemDetail->list_enumeration) ? $itemDetail->list_enumeration : "";
            $humanCodingScheme = !empty($itemDetail->human_coding_scheme) ? $itemDetail->human_coding_scheme : "";
            $fullStatememnt = !empty($itemDetail->full_statement) ? $itemDetail->full_statement : "";
            $nodeType = !empty($itemDetail->nodeType->title) ? $itemDetail->nodeType->title : "";
            $nodeTypeId = !empty($itemDetail->node_type_id) ? $itemDetail->node_type_id : "";
            $itemId = $itemDetail->item_id;
            $parsedItems[] = [
                "id" => $itemId,
                "title" => "",
                "list_enumeration" => $listEnumeration,
                "human_coding_scheme" => $humanCodingScheme,
                "full_statement" => $fullStatememnt,
                "node_type" => $nodeType,
                "node_type_id" => $nodeTypeId,
                "is_editable" => $itemEditableFlag,
                "is_document" => 0
            ];
        }
        $this->setTransformedItems($parsedItems);
    }

    private function transformItemAssociations() {
        $itemAssociations = $this->getItemAssociations();

        $parsedItemAssociations = [];
        foreach($itemAssociations as $itemAssociation) {
            //dd($itemAssociation->toArray());
            $parsedItemAssociations[] = [
                "child_id" => $itemAssociation[0]['origin_node_id'],
                "parent_id" => $itemAssociation[0]['destination_node_id']
            ];
        }
        $this->setTransformedItemAssociations($parsedItemAssociations);
    }

    /**
     * start transforming of data to ui compatible structure
     */
    private function transformTaxonomyRelatedData() {
        $this->transformDocument();
        $this->transformItems();
        $this->transformItemAssociations();
    }

    /**
     * create the ui compatible tree response structure
     */
    private function buildTree () {
        $transformedItems = $this->getTransformedItems();
        $transformedItemAssociations = $this->getTransformedItemAssociations();
        $nodes = $transformedItems;

        $transformedDocument = $this->getTransformedDocument();
        if(!empty($transformedDocument)) {
            $nodes = array_prepend($transformedItems, $transformedDocument);
        }

        $treeData = [
            "nodes" => $nodes,
            "relations" => $transformedItemAssociations,
        ];
        $this->setTree($treeData);
    }
}
