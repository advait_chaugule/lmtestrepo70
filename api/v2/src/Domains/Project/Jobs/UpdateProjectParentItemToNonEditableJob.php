<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class UpdateProjectParentItemToNonEditableJob extends Job
{
    private $projectRepository;
    private $parentId;
    private $projectIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $parentId, string $projectIdentifier,$documentId)
    {
        $this->setParentIdentifierArray($parentId);
        $this->setProjectIdentifier($projectIdentifier);
        $this->documentId = $documentId;
    }

    public function setParentIdentifierArray(array $parentId) {
        $this->parentId = $parentId;
    }

    public function getParentIdentifierArray(): array {
        return $this->parentId;
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->updateParentItemToNonEditable();
    }

    // private function mapItemWithProject() {
    //     $itemIdentifier = $this->getItemIdentifier();
    //     $projectIdentifier = $this->getProjectIdentifier();
    //     $this->projectRepository->associateItemWithProject($itemIdentifier, $projectIdentifier);
    // }
    
    private function updateParentItemToNonEditable() {
        $arrayOfParentId = $this->getParentIdentifierArray();
        $projectIdentifier = $this->getProjectIdentifier();
        $this->projectRepository->updateAllParentItemToNonEditable($arrayOfParentId, $projectIdentifier,$this->documentId);
    }

}
