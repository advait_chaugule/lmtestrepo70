<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Project\Validators\ProjectItemMappingValidator;

class ValidateInputsForProjectItemMappingJob extends Job
{
    public $inputToValidate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputToValidate)
    {
        $this->inputToValidate = $inputToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectItemMappingValidator $validator)
    {
        $validation = $validator->validate($this->inputToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
