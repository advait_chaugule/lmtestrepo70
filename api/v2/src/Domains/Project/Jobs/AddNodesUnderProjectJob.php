<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Data\Models\Project;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class AddNodesUnderProjectJob extends Job
{

    private $projectRepository;
    private $documentRepository;

    private $itemNodeIdsToAssign;
    private $projectId;
    private $rootSelectedStatus;
    private $documentId;

    private $project;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemNodeIdsToAssign, string $projectId, bool $rootSelectedStatus, string $documentId)
    {   
        $this->setItemNodeIdsToAssign($itemNodeIdsToAssign);
        $this->setProjectId($projectId);
        $this->setRootSelectedStatus($rootSelectedStatus);
        $this->setDocumentId($documentId);
    }

    public function setItemNodeIdsToAssign(array $data) {
        $this->itemNodeIdsToAssign = $data;
    }

    public function getItemNodeIdsToAssign(): array {
        return $this->itemNodeIdsToAssign;
    }

    public function setProjectId(string $data) {
        $this->projectId = $data;
    }

    public function getProjectId(): string {
        return $this->projectId;
    }

    public function setRootSelectedStatus(bool $data) {
        $this->rootSelectedStatus = $data;
    }

    public function getRootSelectedStatus(): bool {
        return $this->rootSelectedStatus;
    }

    public function setDocumentId(string $data) {
        $this->documentId = $data;
    }

    public function getDocumentId(): string {
        return $this->documentId;
    }

    public function setProject(Project $data) {
        $this->project = $data;
    }

    public function getProject(): Project {
        return $this->project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ProjectRepositoryInterface $projectRepository,
        DocumentRepositoryInterface $documentRepository
    )
    {
        // set db repositories
        $this->projectRepository = $projectRepository;
        $this->documentRepository = $documentRepository;

        $this->removeDocumentIdFromItemNodeIdsToAssign();
        $this->fetchAndSetProject();
        
            $this->mapItemNodeIdsWithProject();
            $this->mapDocumentIdWithProjectIfRootSelected();
            $this->assignDocumentToProject();
        
    }

    private function removeDocumentIdFromItemNodeIdsToAssign() {
        $itemNodesToAssign = $this->getItemNodeIdsToAssign();
        $documentId = $this->getDocumentId();
        $itemArr = [];
        $itemAlreadyAsignArr = [];
        foreach($itemNodesToAssign as $itemNodesToAssignK => $itemNodesToAssignV) {
            if($itemNodesToAssignV != $documentId){
              array_push($itemArr,$itemNodesToAssignV);
            }
        }

        $alreadyAssignedNoded = DB::table('project_items')
        ->rightjoin('projects', 'projects.project_id', '=', 'project_items.project_id')
            ->select('project_items.item_id')
            ->where('project_items.is_deleted',0)
            ->where('project_items.is_editable',1)
            ->where('projects.project_type',1)
            ->whereIn('item_id',$itemArr)
            ->get()->toArray();
        foreach($alreadyAssignedNoded as $alreadyAssignedNodedK => $alreadyAssignedNodedV)
        {
            array_push($itemAlreadyAsignArr,$alreadyAssignedNodedV->item_id);
            
        }
        foreach($itemNodesToAssign as $key => $nodeId) {
            if(in_array($nodeId,$itemAlreadyAsignArr))
            {
                unset($itemNodesToAssign[$key]);
            }
            if($nodeId===$documentId){
                unset($itemNodesToAssign[$key]);
            }
        }
        $this->setItemNodeIdsToAssign($itemNodesToAssign);
    }

    private function fetchAndSetProject() {
        $projectIdentifier = $this->getProjectId();
        $project = $this->projectRepository->find($projectIdentifier);
        $this->setProject($project);
    }

    private function deleteItemNodesProjectMapping() {
        $project = $this->getProject();
        $project->items()->detach();
    }

    private function deleteDocumentNodeProjectMapping() {
        $documentId = $this->getDocumentId();
        $conditionalKeyValuePairs = [ "document_id" => $documentId ];
        $dataToUpdate = [ "project_id" => "" ];
        $this->documentRepository->editWithCustomizedFields($conditionalKeyValuePairs, $dataToUpdate);
    }

    private function mapItemNodeIdsWithProject() {
        $project = $this->getProject();
        $itemNodesToAssign = $this->getItemNodeIdsToAssign();
        $project->items()->attach($itemNodesToAssign);
    }

    private function mapDocumentIdWithProjectIfRootSelected() {
        if($this->getRootSelectedStatus()) {
            $projectIdentifier = $this->getProjectId();
            $documentId = $this->getDocumentId();
            $conditionalKeyValuePairs = [ "document_id" => $documentId ];
            $dataToUpdate = [ "project_id" => $projectIdentifier ];
            $this->documentRepository->editWithCustomizedFields($conditionalKeyValuePairs, $dataToUpdate);
        }
    }

    private function assignDocumentToProject() {
        $projectIdentifier = $this->getProjectId();
        $documentId = $this->getDocumentId();
        $conditionalKeyValuePairs = [ "project_id" => $projectIdentifier ];
        $dataToUpdate = [ "document_id" => $documentId ];

        $this->projectRepository->editWithCustomizedFields($conditionalKeyValuePairs, $dataToUpdate);
    }
}
