<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class GetProjectRelatedTaxonomyCommentsJob extends Job
{ 
    use DateHelpersTrait;

    private $documentRepository;
    private $itemRepository;
    
    private $documentIdentifier;
    private $organizationIdentifier;

    private $threadIdentifier;

    private $threadCommentRepository;
    private $threadRepository;

    private $threadDetail;
    private $threadEntity;

    private $threadCommentDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, string $organizationId)
    {
        //Set the private variables
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setOrganizationIdentifier($organizationId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository, ThreadRepositoryInterface $threadRepository, ThreadCommentRepositoryInterface $threadCommentRepository)
    {
        //Set the db handlers
        $this->itemRepository               =   $itemRepository;
        $this->threadCommentRepository      =   $threadCommentRepository;
        $this->threadRepository             =   $threadRepository;

        $this->fetchAndSetDocumentItemIds();
        $this->parseAndSetThreadSourceIds();
        
        //get Thread detail from database
        $threadDetail = $this->getThreadDetailFromModel();

        // set Thread details globally
        $this->setThreadDetail($threadDetail);

        //Set parsed Thread details
        $this->createParsedThreadResponseDetail();

        // finally return the standard Thread and ThreadComment 
        return $this->getThreadEntity();
    }

    public function setDocumentIdentifier($identifier) {
        $this->documentIdentifier   =   $identifier;
    }
    
    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier    =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setThreadDetail($data){
        $this->threadDetail = $data;
    }

    public function getThreadDetail(){
        return $this->threadDetail;
    }

    public function setThreadCommentDetail($data){
        $this->threadCommentDetail = $data;
    }

    public function getThreadCommentDetail(){
        return $this->threadCommentDetail;
    }

    public function setThreadIdentifier($threadIdentifier){
        $this->threadIdentifier =   $threadIdentifier;
    }

    public function getThreadIdentifier(){
        return $this->threadIdentifier;
    }

    public function setThreadEntity($threadEntity) {
        $this->threadEntity = $threadEntity;
    }

    public function getThreadEntity() {
        return $this->threadEntity;
    }

    private function fetchAndSetDocumentItemIds() {
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();   
        
        $returnCollection               = true;
        $fieldsToReturn                 = ["item_id"];
        $dataFetchingConditionalClause  = [ "document_id" => $documentIdentifier, 'organization_id' => $organizationIdentifier];

        $itemIdsCollection = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                        $returnCollection, 
                                                        $fieldsToReturn, 
                                                        $dataFetchingConditionalClause
                                                    );
        $this->itemIds = $itemIdsCollection->pluck('item_id')->toArray();
    }

    private function parseAndSetThreadSourceIds() {
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $this->threadSourceIds  =   $this->itemIds;
        array_push($this->threadSourceIds, $documentIdentifier);
    }

    private function getThreadDetailFromModel() {
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $threadSourceIds        =   $this->threadSourceIds;


        $attributeIn        =   'thread_source_id';
        $containedInValues  =   $threadSourceIds;
        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_id', 'thread_source_id', 'assign_to', 'status'];
        $keyValuePairs      =   ['organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
    
        
        $relations      = ['assignTo', 'threadCommentDetail'];

        $returnPaginatedData = false;
        $paginationFilters = ['limit' => 10, 'offset' => 0];
        $returnSortedData = false;
        $sortfilters = ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator = 'AND';


        $threadDetail = $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator, 
            $relations);

        return $threadDetail;
    }

    private function parsedParentThreadDetailforThread(){
        $parentCommentEntity = [];

        //Get the list of ThreadDetail from Thread
        $threadCommentDetail    =   $this->getThreadCommentDetail();   

        foreach($threadCommentDetail as $parentComment)
        {
            if($parentComment->parent_id === ''){
                $createdBy = $parentComment->createdBy->first_name." ".$parentComment->createdBy->last_name;

                $createdById = $parentComment->createdBy->user_id;

                $parentCommentEntity = [
                    "thread_comment_id"         =>  $parentComment->thread_comment_id,
                    "source_comment"            =>  $parentComment->comment,
                    "created_by"                =>  $createdBy, 

                    "created_by_id"             =>  $createdById,

                    "updated_at"                =>  $this->formatDateTime($parentComment->updated_at)
                ];
            }
            
        }

        return $parentCommentEntity;
        
    }

    private function createParsedThreadResponseDetail(){
        $threadEntity = [];

        $threadDetail = $this->getThreadDetail();
        foreach($threadDetail as $thread)
        {
            $this->setThreadCommentDetail($thread->threadCommentDetail);
            
            $parentCommentEntity = (object) $this->parsedParentThreadDetailforThread();
            
            if($thread->status != '4'){
                $this->setThreadIdentifier($thread->thread_id);

                $assignTo = !empty($thread->assign_to) ? ($thread->assignTo->first_name." ".$thread->assignTo->last_name) : '';
                $threadEntity[] = [
                    "thread_id"                 =>  $thread->thread_id,
                    "thread_source_id"          =>  $thread->thread_source_id,
                    "thread_comment_id"         =>  $parentCommentEntity->thread_comment_id,
                    "item_comment"              =>  $parentCommentEntity->source_comment,
                    "created_by"                =>  $parentCommentEntity->created_by,

                    "created_by_id"             =>  $parentCommentEntity->created_by_id,

                    "updated_at"                =>  $parentCommentEntity->updated_at,
                    "assign_to"                 =>  !empty($assignTo) ? $assignTo : "",
                    "assign_to_id"              =>  !empty($thread->assign_to) ? $thread->assignTo->user_id : "",
                    "status"                    =>  $thread->status,
                    "updated_at"                =>  $parentCommentEntity->updated_at,
                ];
            }
            
        }

        $this->setThreadEntity($threadEntity);
    }


}
