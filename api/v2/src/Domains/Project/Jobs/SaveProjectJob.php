<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class SaveProjectJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        $workflow_id = $this->input['workflow_id'];
        $project = $projectRepo->findWokflowStageID($workflow_id)->first();
        $this->input['current_workflow_stage_id'] = !empty($project->workflow_stage_id) ? $project->workflow_stage_id : "" ;
       
        return $projectRepo->fillAndSave($this->input);
    }
}
