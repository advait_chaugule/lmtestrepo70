<?php
namespace App\Domains\Project\Jobs;


use App\Data\Models\PublicReview;
use Lucid\Foundation\Job;

class GetCommentStatusJob extends Job
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;

    }

    public function handle()
    {
        $query = PublicReview::where('user_id', $this->userId)->get();
        if (!$query) {
            $res = [];
            return $res;
        }
        $data = $query->toArray();
        return $data;
    }
}