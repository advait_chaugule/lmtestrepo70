<?php
namespace App\Domains\Project\Jobs;
use Lucid\Foundation\Job;
use App\Data\Models\PublicReview;

class InsertPublicReview extends Job
{
    private $organizationId;
    private $identifier;
    private $userId;
    private $isDeleted;

    /**
     * Create a new job instance.
     *
     * @param array $input
     */
    public function __construct(array $input)
    {
        $this->organizationId = $input['organization_id'];
        $this->identifier     = $input['project_id'];
        $this->userId         = $input['user_id'];
        $this->isDeleted      = $input['is_deleted'];
    }

    public function handle(){
        $insertData = $this->insert();
        if($insertData) {
            $insertData = $insertData->toArray();

        }
        return $insertData;
    }

    /**
     * @return mixed
     * @purspose: This function used to insert data in table project user status if data is available then it will update
     */
    public function insert()
    {
//        if ($this->identifier) {
            $query = PublicReview::updateOrCreate(['project_id' => $this->identifier, 'user_id' => $this->userId, 'organization_id' => $this->organizationId],
                ['organization_id' => $this->organizationId,
                    'project_id' => $this->identifier,
                    'user_id' => $this->userId,
                    'review_status' => 3,
                    'is_deleted' => $this->isDeleted,
                    'created_by' => $this->userId,
                    'updated_by' => $this->userId]
            );
            return $query;
        }
//    }

    public function fetchRecords()
    {
        $query1 = PublicReview::where('project_id',$this->identifier)->get();
        if(!empty($query1))
        {
            $query1 = $query1->toArray();
        }
        return $query1;
    }

}
