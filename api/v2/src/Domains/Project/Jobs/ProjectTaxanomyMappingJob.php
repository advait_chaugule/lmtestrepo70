<?php
namespace App\Domains\Project\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class ProjectTaxanomyMappingJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        $project = $projectRepo->find($this->data['project_id']);
        //return $project;
        $project->taxonomy_id = $this->data['taxonomy_id'];
        $project->modified_by = $this->data['user_id'];
        $project->save();
        return $project;
    }
}
