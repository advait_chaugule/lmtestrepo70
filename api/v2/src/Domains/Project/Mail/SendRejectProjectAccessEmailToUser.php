<?php

namespace App\Domains\Project\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRejectProjectAccessEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    //public $body;
    public $email;
    public $username;
    public $comment;
    public $projectName;
    public $updatedBy;
    public $orgCode;
    public $organizationName;
    public $domainName;
    public $linkDomainName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];
        $this->comment = $data['comment'];
        $this->updatedBy = $data['updatedBy'];  
        $this->projectName = $data['projectName'];  
        $this->email = $data['email'];        
        $this->username = $data['userName'];        
        $this->organizationName = $data['organization_name'];
        $this->orgCode = $data['organization_code'];
        $this->domainName = $data['domain_name'];
        $this->linkDomainName     = str_replace('api.','',$data['domain_name']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.rejectprojectaccessrequest')
                    ->with([
                        "projectName" => $this->projectName,
                        "comment" => $this->comment,
                        "updatedBy" => $this->updatedBy,
                        "email" => $this->email,
                        "user_name" => $this->username,
                         'organizationName'=>$this->organizationName,
                         'orgCode' =>$this->orgCode,
                         'domainName' =>$this->domainName,
                         "linkDomainName" => $this->linkDomainName
                    ])
                    ->subject($this->subject);
    }
}
