<?php

namespace App\Domains\Project\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRequestProjectAccessEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    //public $body;
    public $email;
    public $username;
    public $projectName;
    public $senderName;
    public $reviewerName;
    public $orgCode;
    public $organizationName;
    public $domainName;
    public $linkDomainName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];
        $this->projectName = $data['projectName'];  
        $this->email = $data['email'];        
        $this->username = $data['userName'];      
        $this->reviewerName = $data['reviewerName'];
        $this->organizationName = $data['organization_name'];
        $this->orgCode = $data['organization_code'];
        $this->domainName = $data['domain_name'];
        $this->linkDomainName = str_replace('api.','',$data['domain_name']);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.requestprojectaccessrequest')
                    ->with([
                        "projectName" => $this->projectName,
                        "email" => $this->email,
                        "user_name" => $this->username,
                        "reviewerName" => $this->reviewerName,
                        'organizationName'=>$this->organizationName,
                        'orgCode' =>$this->orgCode,
                        'domainName'=> $this->domainName,
                        'linkDomainName'=> $this->linkDomainName
                    ])
                    ->subject($this->subject);
    }
}
