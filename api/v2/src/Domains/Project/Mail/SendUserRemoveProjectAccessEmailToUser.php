<?php

namespace App\Domains\Project\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserRemoveProjectAccessEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $email;
    public $username;
    public $projectName;
    public $roleName;
    public $organizationName;
    public $orgCode;
    public $domainName;    
    public $linkDomainName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];
        $this->projectName = $data['project_name'];  
        $this->email = $data['email'];        
        $this->username = $data['user_name'];      
        $this->roleName = $data['role_name'];
        $this->organizationName= $data['organization_name'];
        $this->orgCode  = $data['organization_code'];
        $this->domainName  = $data['domain_name'];
        $this->linkDomainName = str_replace('api.','',$data['domain_name']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.userremoveprojectaccessrequest')
                    ->with([
                        "projectName" => $this->projectName,
                        "email" => $this->email,
                        "user_name" => $this->username,
                        "roleName" => $this->roleName,
                        "organization_name"=> $this->organizationName,
                        'organization_code'=>$this->orgCode,
                        'domainName'=>$this->domainName,
                        'linkDomainName'=>$this->linkDomainName
                    ])
                    ->subject($this->subject);
    }
}
