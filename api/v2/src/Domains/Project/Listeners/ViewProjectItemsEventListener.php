<?php

namespace App\Domains\Project\Listeners;

use App\Domains\Project\Events\ViewProjectItemsEvent as Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

// parent class which actually changes the database connection to Reporting DB during runtime
use App\Foundation\ReportListenerHandler;
// database repository interface
use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
// custom helpers
use App\Services\Api\Traits\UuidHelperTrait;

class ViewProjectItemsEventListener extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue, UuidHelperTrait;

    public $eventType = "ITEMS_VIEWED_UNDER_PROJECT";
    private $activityName;
    private $activitySubtypeActivityName;

    private $activityLogRepository;
    private $projectRepository;

    private $activityLogId;

    private $requestUserDetails;
    private $beforeEventRawData;
    private $afterEventRawData;
    private $projectAssociated;
    private $activityLogData;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        ActivityLogRepositoryInterface $activityLogRepository,
        ProjectRepositoryInterface $projectRepository
    )
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

        // set the database repositories
        $this->activityLogRepository = $activityLogRepository;
        $this->projectRepository = $projectRepository;

        $this->setActivityLogId($this->createUniversalUniqueIdentifier());
    }

    public function setActivityLogId(string $data) {
        $this->activityLogId = $data;
    }

    public function getActivityLogId(): string {
        return $this->activityLogId;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setBeforeEventRawData($data) {
        $this->beforeEventRawData = $data;
    }

    public function getBeforeEventRawData() {
        return $this->beforeEventRawData;
    }

    public function setAfterEventRawData($data) {
        $this->afterEventRawData = $data;
    }

    public function getAfterEventRawData() {
        return $this->afterEventRawData;
    }

    public function setActivityName() {
        $projectAssociated = $this->getProjectAssociated();
        $this->activityName = $projectAssociated->project_name;
    }

    public function getActivityName(): string {
        return $this->activityName;
    }

    public function setActivitySubtypeActivityName() {
        $this->activitySubtypeActivityName = "NA";
    }

    public function getActivitySubtypeActivityName(): string {
        return $this->activitySubtypeActivityName;
    }

    public function setUserDisplayName() {
        $userDetails = $this->getRequestUserDetails();
        $firstName = $userDetails["first_name"] ?: "";
        $lastName = $userDetails["last_name"] ?: "";
        $this->userDisplayName = $firstName . " " . $lastName;
    }

    public function getUserDisplayName(): string {
        return $this->userDisplayName;
    }

    public function setActivityLogData(array $data) {
        $this->activityLogData = $data;
    }

    public function getActivityLogData(): array {
        return $this->activityLogData;
    }

    public function setProjectAssociated() {
        $afterEventRawData = $this->getAfterEventRawData();
        $projectId = $afterEventRawData->project_id;
        $returnCollection = false;
        $fieldsToReturn = [ "project_id", "project_name" ];
        $conditionalAttributePair = [ "project_id" => $projectId ];
        $project = $this->projectRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalAttributePair);
        $this->projectAssociated = $project;
    }

    public function getProjectAssociated() {
        return $this->projectAssociated;
    }

    public function getS3PackageFileName(): string {
        return "";
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        try{
            // set before and after event raw data
            $eventData = $event->eventData;
            $this->setBeforeEventRawData($eventData['beforeEventRawData']);
            $this->setAfterEventRawData($eventData['afterEventRawData']);

            // set request user details
            $this->setRequestUserDetails($eventData['requestUserDetails']);

            // set project associated under which item is created
            $this->setProjectAssociated();

            if(!empty($this->getProjectAssociated()->project_id)) {
                // set display strings that need to be cached
                $this->setActivityName();
                $this->setActivitySubtypeActivityName();
                $this->setUserDisplayName();
    
                // prepare the activity log data
                $this->prepareActivityLogData();
    
                // save activity data to activity log
                $this->logActivityData();
            }
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
        }        
    }

    /**
     * Handle a task failure.
     *
     * @param  \App\Events\Event  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Event $event, $exception)
    {
        $this->logErrors($exception);
    }

    private function prepareActivityLogData() {
        $afterEventRawData = $this->getAfterEventRawData();
        $requestUserDetails = $this->getRequestUserDetails();
        
        $activityLogId = $this->getActivityLogId();
        $userId = $requestUserDetails["user_id"];
        $organizationId = $requestUserDetails["organization_id"];
        $activityEventType = $this->getEventTypeValue($this->eventType);
        $activityId = $afterEventRawData->project_id;
        $activityEventSubType = null;
        $activityEventSubTypeActivityId = null;
        $activityPackageS3FileId = $this->getS3PackageFileName();
        $cacheActivityName = $this->getActivityName();
        $cacheActivitySubtypeActivityName = $this->getActivitySubtypeActivityName();
        $cacheUserDisplayName = $this->getUserDisplayName();
        $activityLogTimestamp = now()->toDateTimeString();
        
        $activityLog = [
            "activity_log_id" => $activityLogId,
            "user_id" => $userId,
            "organization_id" => $organizationId,
            "activity_event_type" => $activityEventType,
            "activity_id" => $activityId,
            "activity_event_sub_type" => $activityEventSubType,
            "activity_event_sub_type_activity_id" => $activityEventSubTypeActivityId,
            "activity_package_s3_file_id" => $activityPackageS3FileId,
            "cache_activity_name" => $cacheActivityName,
            "cache_activity_subtype_activity_name" => $cacheActivitySubtypeActivityName,
            "cache_user_display_name" => $cacheUserDisplayName,
            "activity_log_timestamp" => $activityLogTimestamp
        ];

        $this->setActivityLogData($activityLog);
    }

    private function logActivityData() {
        $activityLog = $this->getActivityLogData();
        $this->activityLogRepository->saveSingleRecord($activityLog);
    }
}
