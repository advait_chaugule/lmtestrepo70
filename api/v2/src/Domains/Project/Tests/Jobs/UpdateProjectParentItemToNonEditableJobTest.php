<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\UpdateProjectParentItemToNonEditableJob;
use Tests\TestCase;

class UpdateProjectParentItemToNonEditableJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $data1[] = '2d0326f2-551d-49fc-96a5-bdc4d02efb8c';
        $assets = new UpdateProjectParentItemToNonEditableJob($data1,'f30b9d7a-edf9-4f07-b510-eca225d6e9d8','957bc048-13f8-400f-90fd-9bfe6703d9d5');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }
}
