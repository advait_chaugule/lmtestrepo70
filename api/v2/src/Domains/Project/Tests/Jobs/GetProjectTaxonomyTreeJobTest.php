<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\GetProjectTaxonomyTreeJob;
use Tests\TestCase;

class GetProjectTaxonomyTreeJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $assets = new GetProjectTaxonomyTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_MappedNodes()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $assets = new GetProjectTaxonomyTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setMappedNodes($value);
        $this->assertEquals($value,$assets->getMappedNodes());
    }

    public function test_ItemIdentifier()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $assets = new GetProjectTaxonomyTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }

    public function test_ItemAssociations()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $assets = new GetProjectTaxonomyTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemAssociations($value);
        $this->assertEquals($value,$assets->GetItemAssociations());
    }

    public function test_Document()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $assets = new GetProjectTaxonomyTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocument($value);
        $this->assertEquals($value,$assets->GetDocument());
    }
}
