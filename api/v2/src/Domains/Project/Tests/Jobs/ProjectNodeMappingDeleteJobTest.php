<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\ProjectNodeMappingDeleteJob;
use Tests\TestCase;

class ProjectNodeMappingDeleteJobTest extends TestCase
{
    public function test_ProjectId()
    {
        $assets = new ProjectNodeMappingDeleteJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectId($value);
        $this->assertEquals($value,$assets->getProjectId());
    }
}
