<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\GetProjectWithItemJob;
use Tests\TestCase;

class GetProjectWithItemJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $assets = new GetProjectWithItemJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_Project()
    {
        $assets = new GetProjectWithItemJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProject($value);
        $this->assertEquals($value,$assets->getProject());
    }

    public function test_AssociatedItem()
    {
        $assets = new GetProjectWithItemJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setAssociatedItem($value);
        $this->assertEquals($value,$assets->getAssociatedItems());
    }
}
