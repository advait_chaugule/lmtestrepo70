<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\CheckProjectItemMappingExistsJob;
use Tests\TestCase;

class CheckProjectItemMappingExistsJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $assets = new CheckProjectItemMappingExistsJob('7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b','2d0326f2-551d-49fc-96a5-bdc4d02efb8c');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_ItemIdentifier()
    {
        $assets = new CheckProjectItemMappingExistsJob('7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b','2d0326f2-551d-49fc-96a5-bdc4d02efb8c');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }
}
