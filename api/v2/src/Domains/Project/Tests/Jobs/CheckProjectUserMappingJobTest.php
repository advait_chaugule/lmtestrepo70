<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\CheckProjectUserMappingJob;
use Tests\TestCase;

class CheckProjectUserMappingJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $data1['project_id'] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $data2[] = '2d0326f2-551d-49fc-96a5-bdc4d02efb8c';
        $assets = new CheckProjectUserMappingJob($data1,$data2);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_UserIdentifier()
    {
        $data1['project_id'] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $data2[] = '2d0326f2-551d-49fc-96a5-bdc4d02efb8c';
        $assets = new CheckProjectUserMappingJob($data1,$data2);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserIdentifier($value);
        $this->assertEquals($value,$assets->getUserIdentifier());
    }

    public function test_RoleIdentifier()
    {
        $data1['project_id'] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b';
        $data2[] = '2d0326f2-551d-49fc-96a5-bdc4d02efb8c';
        $assets = new CheckProjectUserMappingJob($data1,$data2);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleIdentifier($value);
        $this->assertEquals($value,$assets->getRoleIdentifier());
    }
}
