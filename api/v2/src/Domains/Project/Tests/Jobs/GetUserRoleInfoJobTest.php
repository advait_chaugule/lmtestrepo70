<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\GetUserRoleInfoJob;
use Tests\TestCase;

class GetUserRoleInfoJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetUserRoleInfoJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_UserIdentifier()
    {
        $assets = new GetUserRoleInfoJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserIdentifier($value);
        $this->assertEquals($value,$assets->getUserIdentifier());
    }

    public function test_RoleIdentifier()
    {
        $assets = new GetUserRoleInfoJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleIdentifier($value);
        $this->assertEquals($value,$assets->getRoleIdentifier());
    }
}
