<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\GetProjectRelatedTaxonomyCommentsJob;
use Tests\TestCase;

class GetProjectRelatedTaxonomyCommentsJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new GetProjectRelatedTaxonomyCommentsJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new GetProjectRelatedTaxonomyCommentsJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ThreadDetail()
    {
        $assets = new GetProjectRelatedTaxonomyCommentsJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadDetail($value);
        $this->assertEquals($value,$assets->getThreadDetail());
    }

    public function test_ThreadCommentDetail()
    {
        $assets = new GetProjectRelatedTaxonomyCommentsJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentDetail($value);
        $this->assertEquals($value,$assets->getThreadCommentDetail());
    }

    public function test_ThreadIdentifier()
    {
        $assets = new GetProjectRelatedTaxonomyCommentsJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadIdentifier($value);
        $this->assertEquals($value,$assets->getThreadIdentifier());
    }
    
    public function test_ThreadEntity()
    {
        $assets = new GetProjectRelatedTaxonomyCommentsJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadEntity($value);
        $this->assertEquals($value,$assets->getThreadEntity());
    }
}
