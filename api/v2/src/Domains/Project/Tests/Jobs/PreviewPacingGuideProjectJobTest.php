<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\PreviewPacingGuideProjectJob;
use Tests\TestCase;

class PreviewPacingGuideProjectJobTest extends TestCase
{
    public function test_PacingGuideIdentifier()
    {
        $assets = new PreviewPacingGuideProjectJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setPacingGuideIdentifier($value);
        $this->assertEquals($value,$assets->getPacingGuideIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new PreviewPacingGuideProjectJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ProjectDetail()
    {
        $assets = new PreviewPacingGuideProjectJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectDetail($value);
        $this->assertEquals($value,$assets->getProjectDetail());
    }

    public function test_ItemsCreatedForPacingGuide()
    {
        $assets = new PreviewPacingGuideProjectJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemsCreatedForPacingGuide($value);
        $this->assertEquals($value,$assets->getItemsCreatedForPacingGuide());
    }

    public function test_ParentAndDocumentId()
    {
        $assets = new PreviewPacingGuideProjectJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setParentAndDocumentId($value);
        $this->assertEquals($value,$assets->getParentAndDocumentId());
    }

    public function test_ItemsOfPacingGuideForUnpublishedTaxonomy()
    {
        $assets = new PreviewPacingGuideProjectJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemsOfPacingGuideForUnpublishedTaxonomy($value);
        $this->assertEquals($value,$assets->getItemsOfPacingGuideForUnpublishedTaxonomy());
    }
}
