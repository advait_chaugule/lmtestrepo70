<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\UpdateAssignProjectUserJob;
use Tests\TestCase;

class UpdateAssignProjectUserJobTest extends TestCase
{
    public function test_UserIdentifier()
    {
        $data1['project_id'] = '2d0326f2-551d-49fc-96a5-bdc4d02efb8c';
        $data2[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new UpdateAssignProjectUserJob($data1,$data2,'f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserIdentifier($value);
        $this->assertEquals($value,$assets->getUserIdentifier());
    }
}
