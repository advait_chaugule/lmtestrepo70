<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\DeleteAssociatedNodesJob;
use Tests\TestCase;

class DeleteAssociatedNodesJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $data['auth_user']['organization_id'] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new DeleteAssociatedNodesJob  ('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_DocumentIdentifier()
    {
        $data['auth_user']['organization_id'] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new DeleteAssociatedNodesJob  ('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $data['auth_user']['organization_id'] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new DeleteAssociatedNodesJob  ('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
