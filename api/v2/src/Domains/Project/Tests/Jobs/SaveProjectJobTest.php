<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\SaveProjectJob;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Services\Api\Traits\UuidHelperTrait;

class SaveProjectJobTest extends TestCase
{
    use RefreshDatabase, UuidHelperTrait;

    public function setUp() {
        parent::setUp();
        Artisan::call('db:seed');
    }
    public function test_save_project_job()
    {
        //$this->markTestIncomplete();

        $projectId  =   $this->createUniversalUniqueIdentifier();
        
        $createProjectArray = [
            'project_id'        =>  $projectId, 
            'project_name'      => 'Pacing GUIDE',
            'description'       =>  'Test project',
            'workflow_id'       =>  'a428e24e-bdbd-46a2-af60-f37b261235c2',
            'project_type'      =>  '2',
            'organization_id'   =>  'a83db6c0-1a5e-428e-8384-c8d58d2a83ff'
        ];

        $projectRepo         =   resolve('App\Data\Repositories\Contracts\ProjectRepositoryInterface');

        $job         = $this->app->make('App\Domains\Project\Jobs\SaveProjectJob', ['input' =>  $createProjectArray]);

        $jobResponse    =   $job->handle($projectRepo);
        
        $this->assertEquals($projectId, $jobResponse->project_id);
    }
}
