<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\MappedUsersListJob;
use Tests\TestCase;

class MappedUsersListJobTest extends TestCase
{
    public function test_Identifier()
    {
        $data['project_id'] ='2d0326f2-551d-49fc-96a5-bdc4d02efb8c';
        $assets = new MappedUsersListJob($data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
