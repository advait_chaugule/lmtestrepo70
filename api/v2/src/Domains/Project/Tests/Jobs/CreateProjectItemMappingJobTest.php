<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\CreateProjectItemMappingJob;
use Tests\TestCase;

class CreateProjectItemMappingJobTest extends TestCase
{
    public function test_ItemIdentifier()
    {
        $assets = new CreateProjectItemMappingJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }

    public function test_ProjectIdentifier()
    {
        $assets = new CreateProjectItemMappingJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }
}
