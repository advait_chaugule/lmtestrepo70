<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\CheckIsProjectDeletedJob;
use Tests\TestCase;

class CheckIsProjectDeletedJobTest extends TestCase
{
    public function test_Identifier()
    {
        $data['project_id'] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new CheckIsProjectDeletedJob($data);
        $value = true;
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
