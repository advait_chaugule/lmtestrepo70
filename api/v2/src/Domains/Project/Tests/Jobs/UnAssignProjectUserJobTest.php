<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\UnAssignProjectUserJob;
use Tests\TestCase;

class UnAssignProjectUserJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new UnAssignProjectUserJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d','f30b9d7a-edf9-4f07-b510-eca225d6e9d8','262b5df2-836d-4554-b657-9f1ffb3947eb');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_UserIdentifier()
    {
        $assets = new UnAssignProjectUserJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','9af1d4f2-1618-41f5-b14f-8712c5487b9d','f30b9d7a-edf9-4f07-b510-eca225d6e9d8','262b5df2-836d-4554-b657-9f1ffb3947eb');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserIdentifier($value);
        $this->assertEquals($value,$assets->getUserIdentifier());
    }
}
