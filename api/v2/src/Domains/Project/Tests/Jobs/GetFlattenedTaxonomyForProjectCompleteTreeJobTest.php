<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\GetFlattenedTaxonomyForProjectCompleteTreeJob;
use Tests\TestCase;

class GetFlattenedTaxonomyForProjectCompleteTreeJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetFlattenedTaxonomyForProjectCompleteTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_ProjectIdentifier()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetFlattenedTaxonomyForProjectCompleteTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_ProjectItemMapping()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetFlattenedTaxonomyForProjectCompleteTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectItemMapping($value);
        $this->assertEquals($value,$assets->getProjectItemMapping());
    }

    public function test_ProjectsAssociated()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetFlattenedTaxonomyForProjectCompleteTreeJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectsAssociated($value);
        $this->assertEquals($value,$assets->getProjectsAssociated());
    }
}
