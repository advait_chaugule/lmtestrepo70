<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\GetNodesAssignedToProjectInTreeCompatibleFormJob;
use Tests\TestCase;

class GetNodesAssignedToProjectInTreeCompatibleFormJobTest extends TestCase
{
    public function test_ProjectIdentifier()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetNodesAssignedToProjectInTreeCompatibleFormJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data,'7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectIdentifier($value);
        $this->assertEquals($value,$assets->getProjectIdentifier());
    }

    public function test_DocumentIdentifier()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetNodesAssignedToProjectInTreeCompatibleFormJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data,'7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_DocumentAssignedToProject()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetNodesAssignedToProjectInTreeCompatibleFormJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data,'7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentAssignedToProject($value);
        $this->assertEquals($value,$assets->getDocumentAssignedToProject());
    }

    public function test_ItemDocumentIdentifier()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetNodesAssignedToProjectInTreeCompatibleFormJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data,'7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getItemDocumentIdentifier());
    }

    public function test_OpenCommentCountPerProject()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetNodesAssignedToProjectInTreeCompatibleFormJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data,'7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOpenCommentCountPerProject($value);
        $this->assertEquals($value,$assets->getOpenCommentCountPerProject());
    }

    public function test_DocumentOpenCommentCount()
    {
        $data[] = '9af1d4f2-1618-41f5-b14f-8712c5487b9d';
        $assets = new GetNodesAssignedToProjectInTreeCompatibleFormJob('2d0326f2-551d-49fc-96a5-bdc4d02efb8c',$data,'7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentOpenCommentCount($value);
        $this->assertEquals($value,$assets->getDocumentOpenCommentCount());
    }
}
