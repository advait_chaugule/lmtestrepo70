<?php
namespace App\Domains\Project\Tests\Jobs;

use App\Domains\Project\Jobs\UpdatePacingGuideProjectParentItemToNonEditableJob;
use Tests\TestCase;

class UpdatePacingGuideProjectParentItemToNonEditableJobTest extends TestCase
{
    public function test_update_pacing_guide_project_parent_item_to_non_editable_job()
    {
        $this->markTestIncomplete();
    }
}
