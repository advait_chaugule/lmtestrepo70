<?php
namespace App\Domains\Subject\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Subject\Validators\SubjectValidator;

class ValidateSubjectByIdJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SubjectValidator $validator)
    {
        //
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
