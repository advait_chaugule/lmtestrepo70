<?php
namespace App\Domains\Subject\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class SubjectValidator extends BaseValidator {

    protected $rules = [
        'subject_id' => 'required|exists:subjects,subject_id,is_deleted,0'
    ];

     protected $messages = [
        'subject_id' => ':attribute is required.'
     ];
    
}