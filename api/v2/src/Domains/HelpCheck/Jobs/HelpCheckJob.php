<?php
namespace App\Domains\HelpCheck\Jobs;

use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Lucid\Foundation\Job;
use Jaspersoft\Client\Client;
use Aws\Sqs\SqsClient;
use Illuminate\Contracts\Queue\Factory;
use Memcache;

class HelpCheckJob extends Job
{
    private $sqsClient;
    private $sqsClientData;
    private $searchClient;
    private $cacheObject;
    public function __construct(){

    }

    public function handle(){
        $this->RDS();
        $this->cloudSQS();
        $this->SQS();
        $this->CloudSearch();
        $this->elasticCache();
        $this->jasperServer();
        $this->Nginx();
        $this->phpFpm();
        $this->supervisor();
    }

    public function RDS()
    {

        $dbConnection = env('DB_CONNECTION');
        $dbHost       = env('DB_HOST');
        $dbName       = env('DB_DATABASE');
        $dbUserName   = env('DB_USERNAME');
        $dbPassword   = env('DB_PASSWORD');
        $dbPrefix     = env('DB_PREFIX');

        $conn = mysqli_connect($dbHost,$dbUserName,$dbPassword,$dbName);
        // Check connection
        if(!$conn ){
            die('Could not connect: ' . mysqli_error($conn));
        }
        echo nl2br("RDS Ok".'<br>');
        mysqli_close($conn);
    }

    public function cloudSQS()
    {
        $sqsData     = config('sqs_search_upload');
        $awsKey      = $sqsData['AWS_KEY'];
        $awsSecret   = $sqsData['AWS_SECRET'];
        $awsRegion   = $sqsData['REGION'];
        $version     = $sqsData['API_VERSION'];

        $clientConfigurations = [
            'credentials' => [
                'key' => $awsKey,
                'secret' => $awsSecret
            ],
            'version' => $version,
            //'profile' => $profile,
            'region' => $awsRegion
        ];
        // load configurations, initialise and set sqs client
        $this->sqsClient = new SqsClient($clientConfigurations);
        if($this->sqsClient){
            echo "Cloud SQS ok.<br>";
        }else{
            echo "Cloud SQS failed.<br>";
        }

    }

    public function SQS(){
        $sqsInfo     = config('queue');
        $sqsCloud    = config('sqs_search_upload');
        $SqsVersion  = $sqsCloud['API_VERSION'];
        $sqsData     = $sqsInfo['connections']['sqs'];
        $sqsKey      = $sqsData['key'];
        $sqsSecret   = $sqsData['secret'];
        $sqsQueue    = $sqsData['queue'];
        $sqsRegion   = $sqsData['region'];

        $sqsConfiguration = [
            'credentials' => [
                'key' => $sqsKey,
                'secret' => $sqsSecret
            ],
            'version' => $SqsVersion,
            //'profile' => $profile,
            'region' => $sqsRegion
        ];
        $this->sqsClientData = new SqsClient($sqsConfiguration);
        if($this->sqsClientData) {
            echo "SQS ok.".'<br>';
        }else{
            echo "SQS failed.".'<br>';
        }

    }

    public function CloudSearch()
    {
        $cloudCredential = config('cloud_search');
        $key          = $cloudCredential['AWS_KEY'];
        $awsSecret    = $cloudCredential['AWS_SECRET'];
        $endPoint    = $cloudCredential['SEARCH_ENDPOINT'];
        $apiVersion   = $cloudCredential['API_VERSION'];

        $configurations = [
            'credentials' => [
                'key' => $key,
                'secret' => $awsSecret,
                'token' => ''
            ],
            'endpoint' => $endPoint,
            'version' => $apiVersion
        ];
        $this->searchClient = new CloudSearchDomainClient($configurations);
        if($this->searchClient){
            echo "Cloud Search ok".'<br>';
        }else{
            echo "Failed cloud search".'<br>';
        }
    }

    public function elasticCache()
    {
        $this->cacheObject = new Memcache();
        $cacheData        = config('cache');
        $memCacheEndPoint = env('MEMCACHED_ENDPOINT');
        $cacheInfo        = $cacheData['stores']['memcached']['servers'][0];
        $port             = $cacheInfo['port'];
        $elasticCache     = $this->cacheObject->connect($memCacheEndPoint,$port);
        if($elasticCache===true){
            echo "Elastic cache ok".'<br>';
        }else{
            echo "Failed elastic cache".'<br>';
        }
    }

    public function jasperServer()
    {
        $connect = new Client(
            config('jasper.jasper_host'),
            config('jasper.jasper_username'),
            config('jasper.jasper_password')
        );
        if(!$connect) {
            echo "Jasper couldn't connect".'<br>';
        }else{
            echo nl2br("Jasper ok").'<br>';
        }
    }

    public function Nginx(){
        exec('/sbin/service nginx  status 2>&1',$output,$return_var);
        if(strpos($output[0],'running...')){
            echo "Nginx ok".'<br>';
        }else{
            echo "Failed Nginx".'<br>';
        }
    }

    public function phpFpm(){
        exec('/sbin/service php-fpm status 2>&1',$output,$return_var);
        if(strpos($output[0],'running...')){
            echo "php fpm ok".'<br>';
        }else{
            echo "Failed php fpm".'<br>';
        }
    }

    public function supervisor(){
        exec('/sbin/service supervisord status 2>&1',$output,$return_var);
        if(strpos($output[0],'running...')){
            echo "Supervisor ok".'<br>';
        }else{
            echo "Failed Supervisor".'<br>';
        }
    }





}