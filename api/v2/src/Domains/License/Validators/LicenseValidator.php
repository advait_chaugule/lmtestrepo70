<?php
namespace App\Domains\License\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class LicenseValidator extends BaseValidator {

    protected $rules = [
        'license_id' => 'required|exists:licenses,license_id,is_deleted,0',
    ];

     protected $messages = [
        'license_id' => ':attribute should be provided'
     ];
    
}