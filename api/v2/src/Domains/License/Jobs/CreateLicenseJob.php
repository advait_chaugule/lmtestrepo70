<?php
namespace App\Domains\License\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LicenseRepositoryInterface;

class CreateLicenseJob extends Job
{
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LicenseRepositoryInterface $licenseRepo)
    {
        return $licenseRepo->fillAndSave($this->data);
    }
}
