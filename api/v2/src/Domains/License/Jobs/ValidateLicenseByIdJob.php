<?php
namespace App\Domains\License\Jobs;

use Lucid\Foundation\Job;

use App\Domains\License\Validators\LicenseValidator;

class ValidateLicenseByIdJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LicenseValidator $validator)
    {
        //validate $identifier to exist in licenses schema
        $validation = $validator->validate($this->identifier);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
