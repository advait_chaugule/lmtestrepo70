<?php
namespace App\Domains\License\Jobs;

use Lucid\Foundation\Job;

class ValidateLicenseInputJob extends Job
{

    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!empty($this->input['license_title'])){
             $validationDetail = true;
        }
        else{
            $validationDetail = [
                'title' => "Title can't be empty."
            ];
        }
        return $validationDetail;
    }
}
