<?php
namespace App\Domains\License\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LicenseRepositoryInterface;

class UpdateLicenseJob extends Job
{
    
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LicenseRepositoryInterface $licenseRepo)
    {
        $id = $this->data['license_id'];
        // remove id from the input array since it has been assigned to $id variable
        unset($this->data['license_id']);
        return $licenseRepo->edit($id, $this->data);
    }
}
