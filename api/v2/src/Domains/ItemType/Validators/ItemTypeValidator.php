<?php
namespace App\Domains\ItemType\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ItemTypeValidator extends BaseValidator {

    protected $rules = [
        'item_type_id' => 'required|exists:item_types,item_type_id,is_deleted,0'
    ];

    protected $messages = [
        'item_type_id' => ':attribute is required'
    ];
    
}