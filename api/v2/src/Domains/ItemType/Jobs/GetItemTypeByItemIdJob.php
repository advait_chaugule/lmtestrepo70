<?php
namespace App\Domains\ItemType\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemTypeRepositoryInterface;

class GetItemTypeByItemIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemTypeRepositoryInterface $itemTypeRepo)
    {
      
        $itemType = $itemTypeRepo->find($this->data['id']);       
        return $itemType;
    }
}
