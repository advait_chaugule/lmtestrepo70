<?php
namespace App\Domains\ItemType\Jobs;

use Lucid\Foundation\Job;

use App\Domains\ItemType\Validators\ItemTypeValidator;

class ValidateItemTypeByIdJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemTypeValidator $validator)
    {
        //
        $validation = $validator->validate($this->identifier);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
