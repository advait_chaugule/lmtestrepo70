<?php
namespace App\Domains\Stage\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Models\WorkflowStage;
use App\Data\Models\Role;

class ValidateStageInputJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $requestObject;
    protected $workflowId;
    protected $dataToValidate;
    protected $errorMessagesAvailable;
    protected $errorMessagesToSet;

    public function __construct(array $requestData)
    {
        //
        $this->dataToValidate = $requestData;
        $this->errorMessagesAvailable = [
            "required" => [
                "stage_name" => "Stage name is mandatory.",
              //  "stage_order" => "Stage Order is mandatory.",
           //     "workflow_stages_role" => "Role is mandatory"
            ],
            "exists" => [
                "stage_name" => "Stage name already exists in current workflow.",
             //   "stage_order" => "Stage Order is already exists in current workflow.",
             //   "workflow_stages_role" => "Some Role is invalid."
            ]
        ];
        $this->errorMessagesToSet = [];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        // set the injected laravel request object
        $this->requestObject = $request;
       

        $nameRequiredStatus = $this->getRequiredStatusForAttribute("stage_name");
        $this->setErrorMessageOfValidationTypeForAttribute($nameRequiredStatus, "required", "stage_name");

        $nameExistsStatus =  $nameRequiredStatus ? 
                                    $this->getNameExistsStatusAndSetErrorMessageAccordingly() : 
                                    true;
         
      /*  $orderRequiredStatus = $this->getRequiredStatusForAttribute("stage_order");
        $this->setErrorMessageOfValidationTypeForAttribute($nameRequiredStatus, "required", "stage_order");

        $orderExistsStatus =  $orderRequiredStatus ? 
                                    $this->getOrderExistsStatusAndSetErrorMessageAccordingly() : 
                                    true;
        if (empty($this->dataToValidate['workflow_stages_role'])) 
        {
            $roleRequiredStatus = "";
        }
        else
        {
            $roleRequiredStatus = 1;
        }                          
        
        $this->setErrorMessageOfValidationTypeForAttribute($roleRequiredStatus, "required", "workflow_stages_role");

        $roleExistStatus = $this->getRoleExistsStatusAndSetErrorMessageAccordingly();
        */                   
       // $overallStatus = $nameRequiredStatus && $nameExistsStatus && $orderRequiredStatus && $orderExistsStatus && $roleRequiredStatus && $roleExistStatus;
       //Only Stage name is mandatory
       
       $overallStatus = $nameRequiredStatus && $nameExistsStatus; 

        
        $jobResponse = $overallStatus===true ? $overallStatus : implode(",", $this->errorMessagesToSet);

        return $jobResponse;
        //
    }

    private function getNameExistsStatusAndSetErrorMessageAccordingly(): bool {
        $workflowId = $this->dataToValidate["workflow_id"];
        $workflowName = $this->dataToValidate["stage_name"];

        $conditionalClause = [ [ "workflow_id", "=", $workflowId] ];
        $conditionalClause[] = [ "stage_name" , "=", $workflowName ];
        $recordCount = WorkflowStage::where($conditionalClause)->count();
        $status = $recordCount > 0 ? false : true;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "stage_name");
        return $status;
    }

    private function getOrderExistsStatusAndSetErrorMessageAccordingly(): bool {
        $workflowId = $this->dataToValidate["workflow_id"];
        $stageOrder = $this->dataToValidate["stage_order"];

        $conditionalClause = [ [ "workflow_id", "=", $workflowId] ];
        $conditionalClause[] = [ "order" , "=", $stageOrder ];
        $recordCount = WorkflowStage::where($conditionalClause)->count();
        $status = $recordCount > 0 ? false : true;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "stage_order");
        return $status;
    }

    private function getRoleExistsStatusAndSetErrorMessageAccordingly(): bool {
        $workflowStagesRole = $this->dataToValidate["workflow_stages_role"];
       foreach($workflowStagesRole as $key =>$role)
       {
           $roleArr[$key] = $role['role_id'];
       }
        
       $roleArrCnt = count($roleArr);
        $recordCount = Role::whereIn('role_id',$roleArr)->count();
       
        $status = $recordCount < $roleArrCnt ? false : true;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "workflow_stages_role");
        return $status;
    }
    

    private function getRequiredStatusForAttribute(string $attribute): bool {
        return !empty($this->dataToValidate[$attribute]);
    }

    
    private function setErrorMessageOfValidationTypeForAttribute(bool $validationStatus, string $validationType, string $attribute) {
        if($validationStatus===false) {
            $messageToPush = !empty($this->errorMessagesAvailable[$validationType][$attribute]) ?
                             $this->errorMessagesAvailable[$validationType][$attribute] : 
                             "";
            if(!empty($messageToPush)) {
                $this->errorMessagesToSet[] = $messageToPush;
            }
        }
    }

}
