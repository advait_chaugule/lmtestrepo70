<?php
namespace App\Domains\Stage\Jobs;
use App\Data\Repositories\Contracts\StageRepositoryInterface;

use Lucid\Foundation\Job;

class CreateStageJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(StageRepositoryInterface $stageRepo)
    {
        return $stageRepo->fillAndSave($this->input);
    }
}
