<?php
namespace App\Domains\Concept\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ConceptValidator extends BaseValidator {

    protected $rules = [
        'concept_id' => 'required|exists:concepts,concept_id,is_deleted,0'
    ];

    protected $messages = [
        'required' => ':attribute is required.'
    ];
    
}