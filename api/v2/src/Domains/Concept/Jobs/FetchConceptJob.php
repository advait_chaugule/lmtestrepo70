<?php
namespace App\Domains\Concept\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ConceptRepositoryInterface;

class FetchConceptJob extends Job
{
    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $id)
    {
        //
        $this->data = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ConceptRepositoryInterface $conceptRepo)
    {
        //
        $id = $this->data;
        $conceptData = $conceptRepo->getConceptData($id);
        return $conceptData;
    }
}
