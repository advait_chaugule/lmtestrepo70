<?php
namespace App\Domains\Concept\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ConceptRepositoryInterface;

class UpdateConceptJob extends Job
{
    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ConceptRepositoryInterface $conceptRepo)
    {
        $conceptId = $this->data['concept_id'];
        // remove concept_id from the input array since it has been assigned to $conceptId variable
        unset($this->data['concept_id']);
        return $conceptRepo->edit($conceptId, $this->data);
    }
}
