<?php
namespace App\Domains\Concept\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Concept\Validators\ConceptValidator;

class ValidateConceptByIdJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $identifier)
    {
        //Assign the private attribute
        $this->identifier = $identifier;
    }

    /**
     * Execute the job.
     *
     * @return boolean
     */
    public function handle(ConceptValidator $validator)
    {
        //Validate the identifier set in API route
        $validation = $validator->validate($this->identifier);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
