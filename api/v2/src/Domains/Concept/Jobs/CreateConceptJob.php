<?php
namespace App\Domains\Concept\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ConceptRepositoryInterface;

class CreateConceptJob extends Job
{

    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ConceptRepositoryInterface $conceptRepo)
    {
        return $conceptRepo->fillAndSave($this->input);
    }
}
