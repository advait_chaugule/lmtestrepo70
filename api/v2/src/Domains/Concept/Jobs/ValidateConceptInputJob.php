<?php
namespace App\Domains\Concept\Jobs;

use Lucid\Foundation\Job;

class ValidateConceptInputJob extends Job
{

    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!empty($this->input['concept_title'])){
             $validationDetail = true;
        }
        else{
            $validationDetail = [
                'concept_title' => "Title can't be empty."
            ];
        }
        return $validationDetail;
    }
}
