<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ItemThreadCommentRepositoryInterface;

class CreateItemThreadCommentJob extends Job
{
    use DateHelpersTrait;

    private $itemThreadCommentRepository;

    private $itemIdentifier;
    private $requestData;

    private $itemThreadCommentData;
    private $itemThreadCommentEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, array $requestData)
    {
        //Set public data attribute
        $this->setItemIdentifier($itemIdentifier);
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadCommentRepositoryInterface $itemThreadCommentRepository)
    {
        //Set the repo handler
        $this->itemThreadCommentRepository = $itemThreadCommentRepository;
        $identifier = $this->getItemIdentifier();

        $inputData = $this->requestData;
        
        $savedItemThreadCommentData = $this->itemThreadCommentRepository->fillAndSave($inputData);
        $this->setItemThreadCommentData($savedItemThreadCommentData);
        //return $savedItemThreadCommentData;

        $this->parseResponse();

        return $this->getItemThreadCommentEntity();
    }

    //Public Setter and Getter methods
    public function setItemIdentifier($identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(){
        return $this->itemIdentifier;
    }

    public function setItemThreadCommentData($data){
        $this->itemThreadCommentData = $data;
    }

    public function getItemThreadCommentData() {
        return $this->itemThreadCommentData;
    }

    public function setItemThreadCommentEntity($itemThreadCommentEntity){
        $this->itemThreadCommentEntity = $itemThreadCommentEntity;
    }

    public function getItemThreadCommentEntity(){
        return $this->itemThreadCommentEntity;
    }

    private function parseResponse(){
        $itemThreadCommentData = $this->getItemThreadCommentData();

        $itemThreadCommentEntity = [
            "item_thread_comment_id"    =>  $itemThreadCommentData->item_thread_comment_id,
            "parent_id"                 =>  $itemThreadCommentData->parent_id,
            "comment"                   =>  $itemThreadCommentData->comment,
            "updated_at"                =>  $this->formatDateTimeToISO8601($itemThreadCommentData->updated_at)
        ];

        $this->setItemThreadCommentEntity($itemThreadCommentEntity);
    }
}
