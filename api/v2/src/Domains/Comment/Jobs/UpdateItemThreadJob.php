<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemThreadRepositoryInterface;

class UpdateItemThreadJob extends Job
{
    private $itemThreadRepository;

    private $itemIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        //Set public data attribute
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadRepositoryInterface $itemThreadRepository)
    {
        //Set the repo handler
        $this->itemThreadRepository = $itemThreadRepository;
        $identifier =   $this->getItemIdentifier();
        $inputData  =   $this->requestData;

        $columnValueFilterPairs = ['item_thread_id' =>  $inputData['item_thread_id']];
        $attributes             = ['updated_at' =>  $inputData['updated_at']];             

        if(isset($inputData['status'])) {
            $attributes['status'] = $inputData['status'];
        }

        if(isset($inputData['assign_to'])){
            $attributes['assign_to']    =   $inputData['assign_to'];
        }

        if(isset($inputData['is_deleted'])){
            $attributes['is_deleted']    =   $inputData['is_deleted'];
        }

        $updateItemThreadData = $this->itemThreadRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        return $updateItemThreadData;
    }

    public function setItemIdentifier($identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(){
        return $this->itemIdentifier;
    }
}
