<?php
namespace App\Domains\Comment\Jobs;
use Illuminate\Http\Request;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

use App\Data\Models\ThreadComment;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class GetDocumentUserCommentsJob extends Job
{
    private $itemRepository;
    private $userRepository;
    private $organizationId;

    private $documentId;
    private $userId;
    private $accessDetails;
    private $hasCreatedBySelfPermission;
    private $hasCreatedByOtherPermission;

    private $itemIds;
    private $threadSourceIds;

    private $threadComments;
    private $createdByUserIds;
    private $createdByUsers;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentId, string $userId, array $accessDetails)
    {
        $this->documentId = $documentId;
        $this->userId = $userId;
        $this->accessDetails = collect($accessDetails);
        $this->hasCreatedBySelfPermission = $this->_helperToReturnHasPermissionStatusOfType('view_my_comments');
        $this->hasCreatedByOtherPermission = $this->_helperToReturnHasPermissionStatusOfType('view_others_comments');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        Request $request,
        ItemRepositoryInterface $itemRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->itemRepository = $itemRepository;
        $this->userRepository = $userRepository;
        $this->organizationId = $request->input('auth_user.organization_id');

        $this->fetchAndSetDocumentItemIds();
        $this->parseAndSetThreadSourceIds();
        $this->fetchAndSetThreadComments();
        $this->parseAndSetThreadCommentCreatedByUserIds();
        $this->fetchAndSetCreatedByUsers(); 

        return $this->parseAndReturnThreadComments();
    }

    private function fetchAndSetDocumentItemIds() {
        $returnCollection = true;
        $fieldsToReturn = ["item_id"];
        $dataFetchingConditionalClause = [ "document_id" => $this->documentId ];

        $itemIdsCollection = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                        $returnCollection, 
                                                        $fieldsToReturn, 
                                                        $dataFetchingConditionalClause
                                                    );
        $this->itemIds = $itemIdsCollection->pluck('item_id')->toArray();
    }

    private function parseAndSetThreadSourceIds() {
        $this->threadSourceIds = $this->itemIds;
        array_push($this->threadSourceIds, $this->documentId);
    }

    private function fetchAndSetThreadComments() {
        // prepare the query based on permissions and thread_source_ids available
        $mainQuery = ThreadComment::where([
            [ 'organization_id', '=', $this->organizationId ],
            ['is_deleted', '=', 0],
            ['parent_id', '=', '']
        ]);

        // if user has permission to view own comments
        if($this->hasCreatedBySelfPermission===true) {
            $mainQuery->where('created_by', $this->userId);
        }
        
        // if user has permission to view other's comments
        if($this->hasCreatedByOtherPermission===true) {
            $mainQuery->orWhere('created_by', '!=' , $this->userId);
        }

        $mainQuery->whereIn('thread_source_id', $this->threadSourceIds);

        $this->threadComments = ($this->hasCreatedBySelfPermission || $this->hasCreatedByOtherPermission) ? 
                                $mainQuery->get() : 
                                collect([]);
    }

    private function parseAndSetThreadCommentCreatedByUserIds() {
        $userIds = $this->threadComments->unique('created_by')->pluck('created_by')->toArray();
        $this->createdByUserIds = $userIds;
    }

    private function fetchAndSetCreatedByUsers() {
        $attributeIn = 'user_id';
        $containedInValues = $this->createdByUserIds;
        $this->createdByUsers = $this->userRepository->findByAttributeContainedIn($attributeIn, $containedInValues, []);
    }

    private function parseAndReturnThreadComments(): array {
        $parsedData = [];
        foreach($this->threadComments as $threadComment) {
            $updatedAt = $threadComment->updated_at ? $threadComment->updated_at->toDateTimeString() : "";
            $threadCommentId = $threadComment->thread_comment_id;
            $createdById = $threadComment->created_by;
            $createdByUserFullName = $this->_helperToReturnCreatedByUserFullName($createdById);
            $threadSourceId = $threadComment->thread_source_id ?? "";
            $commentText = $threadComment->comment ?? "";
            $threadId = $threadComment->thread_id ?? "";
            $parsedData[] = [
                'thread_comment_id' => $threadCommentId,
                'thread_id' => $threadId,
                'thread_source_id' => $threadSourceId,
                'created_by' => $createdByUserFullName,
                'created_by_id' => $createdById,
                'comment_text' => $commentText,
                'updated_at' => $updatedAt
            ];
        }
        return $parsedData;
    }

    private function _helperToReturnCreatedByUserFullName(string $userId): string {
        $userFullName = '';
        $searchResult = $this->createdByUsers->where('user_id', $userId);
        if($searchResult->isNotEmpty()) {
            $user = $searchResult->first();
            $userFirstName = $user->first_name ?? "";
            $userLastName = $user->last_name ?? "";
            $userFullName = trim($userFirstName." ".$userLastName);
        }
        return $userFullName;
    }

    private function _helperToReturnHasPermissionStatusOfType(string $internalName): bool {
        $allowedRolePermissionCollection = $this->accessDetails;
        $searchResult = $allowedRolePermissionCollection->where('internal_name', $internalName);
        return $searchResult->isNotEmpty();
    }
}
