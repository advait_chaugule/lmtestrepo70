<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Comment\Validators\ItemThreadCommentExistsValidator;

class ValidateItemThreadCommentJob extends Job
{

    public $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadCommentExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
