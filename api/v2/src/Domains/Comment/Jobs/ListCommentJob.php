<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class ListCommentJob extends Job
{
    use DateHelpersTrait;

    private $status;
    private $threadSourceIdentifier;
    private $organizationIdentifier;
    private $threadIdentifier;

    private $threadCommentRepository;
    private $threadRepository;

    private $threadDetail;
    private $threadEntity;

    private $threadCommentDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadSourceIdentifier, string $organizationIdentifier, string $status)
    {
        //Set the private attribute to Role Identifier
        $this->setThreadSourceIdentifier($threadSourceIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);

        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadRepositoryInterface $threadRepository, ThreadCommentRepositoryInterface $threadCommentRepository)
    {
        //Set Thread and ThreadComment Repo handler
        $this->threadCommentRepository      = $threadCommentRepository;
        $this->threadRepository             = $threadRepository;

        //get Thread detail from database
        $threadDetail = $this->getThreadDetailFromModel();

        // set Thread details globally
        $this->setThreadDetail($threadDetail);

        //Set parsed Thread details
        $this->createParsedThreadResponseDetail();

        // finally return the standard Thread and ThreadComment 
        return $this->getThreadEntity();
    }

    //Public Getter and Setter methods
    public function setThreadSourceIdentifier($identifier){
        $this->threadSourceIdentifier = $identifier;
    }

    public function getThreadSourceIdentifier(){
        return $this->threadSourceIdentifier;
    }

    public function setThreadIdentifier($threadIdentifier){
        $this->threadIdentifier =   $threadIdentifier;
    }

    public function getThreadIdentifier(){
        return $this->threadIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }

    public function setThreadDetail($data){
        $this->threadDetail = $data;
    }

    public function getThreadDetail(){
        return $this->threadDetail;
    }

    public function setThreadCommentDetail($data){
        $this->threadCommentDetail = $data;
    }

    public function getThreadCommentDetail(){
        return $this->threadCommentDetail;
    }

    public function setThreadEntity($threadEntity) {
        $this->threadEntity = $threadEntity;
    }

    public function getThreadEntity() {
        return $this->threadEntity;
    }

    private function getThreadCommentDetailFromModel() {
        $threadIdentifier       =   $this->getThreadIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection = true;
        $fieldsToReturn = ['thread_id', 'thread_comment_id', 'parent_id', 'comment', 'created_by', 'updated_at'];
        $keyValuePairs  = ['thread_id' => $threadIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations      = ['createdBy'];

        $threadCommentDetail = $this->threadCommentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

        // set ThreadComment details
        $this->setThreadCommentDetail($threadCommentDetail);
    }

    private function getThreadDetailFromModel() {
        $threadSourceIdentifier =   $this->getThreadSourceIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection = true;
        $fieldsToReturn = ['thread_id', 'assign_to', 'status'];
        $keyValuePairs  = ['thread_source_id' => $threadSourceIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0' ];
        
        if($this->status === '4'){
            $keyValuePairs['status']   =   $this->status;   
        }
        
        $relations      = ['assignTo', 'threadCommentDetail'];


        $threadDetail = $this->threadRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

        return $threadDetail;
    }

    private function parsedParentThreadDetailforThread(){
        $parentCommentEntity = [];

        //Get the list of ThreadDetail from Thread
        $threadCommentDetail    =   $this->getThreadCommentDetail();  
       if(count($threadCommentDetail) > 0)
       {
        foreach($threadCommentDetail as $parentComment)
        {
            if($parentComment->parent_id === ''){
                $createdBy = $parentComment->createdBy->first_name." ".$parentComment->createdBy->last_name;

                $createdById = $parentComment->createdBy->user_id;

                $parentCommentEntity = [
                    "thread_comment_id"         =>  $parentComment->thread_comment_id,
                    "source_comment"            =>  $parentComment->comment,
                    "created_by"                =>  $createdBy, 

                    "created_by_id"             =>  $createdById,

                    "updated_at"                =>  $this->formatDateTime($parentComment->updated_at)
                ];
            }
            
            
        }
       }
       else
            {
                $parentCommentEntity = [
                    "thread_comment_id"         =>  "",
                    "source_comment"            =>  "",
                    "created_by"                =>  "", 

                    "created_by_id"             =>  "",

                    "updated_at"                =>  ""
                ];
            }
        

        return $parentCommentEntity;
        
    }

    private function parsedThreadCommentResponseDetailforThread(){
        $threadCommentEntity = [];

        //get ThreadComment detail from database
        $threadComments             =   $this->getThreadCommentDetailFromModel();
        $threadCommentDetail        =   $this->getThreadCommentDetail();   

        foreach($threadCommentDetail as $threadComment)
        {
            if($threadComment->parent_id !== ''){
                $createdBy = $threadComment->createdBy->first_name." ".$threadComment->createdBy->last_name;

                $createdById = isset($threadComment->createdBy->user_id) ? $threadComment->createdBy->user_id : "";

                $threadCommentEntity[] = [
                    "thread_comment_id"         =>  $threadComment->thread_comment_id,
                    "parent_id"                 =>  !empty($threadComment->parent_id) ? $threadComment->parent_id : "",
                    "comment"                   =>  !empty($threadComment->comment) ? $threadComment->comment : "",
                    "created_by"                =>  !empty($createdBy) ? $createdBy : "",

                    "created_by_id"             =>  $createdById,

                    "updated_at"                =>  $this->formatDateTime($threadComment->updated_at)
                ];
            }
            
        }

        return $threadCommentEntity;
        
    }

    private function createParsedThreadResponseDetail(){
        $threadEntity = [];

        $threadDetail = $this->getThreadDetail();
    
        foreach($threadDetail as $thread)
        {
           
            $this->setThreadCommentDetail($thread->threadCommentDetail);
            
            $parentCommentEntity = (object) $this->parsedParentThreadDetailforThread();
           
            if($this->status !== '4'){
                if($thread->status != '4'){
                    $this->setThreadIdentifier($thread->thread_id);
                
                    $assignTo = !empty($thread->assign_to) ? ($thread->assignTo->first_name." ".$thread->assignTo->last_name) : '';
                    $threadEntity[] = [
                        "thread_id"                 =>  $thread->thread_id,
                        "thread_comment_id"         =>  $parentCommentEntity->thread_comment_id,
                        "item_comment"              =>  $parentCommentEntity->source_comment,
                        "created_by"                =>  $parentCommentEntity->created_by,

                        "created_by_id"             =>  $parentCommentEntity->created_by_id,

                        "updated_at"                =>  $parentCommentEntity->updated_at,
                        "assign_to"                 =>  !empty($assignTo) ? $assignTo : "",
                        "assign_to_id"              =>  !empty($thread->assign_to) ? $thread->assignTo->user_id : "",
                        "status"                    =>  $thread->status,
                        "updated_at"                =>  $parentCommentEntity->updated_at,
                        "comments"                  =>  $this->parsedThreadCommentResponseDetailforThread()
                    ];
                }
            }
            else{
                $this->setThreadIdentifier($thread->thread_id);

                $assignTo = !empty($thread->assign_to) ? ($thread->assignTo->first_name." ".$thread->assignTo->last_name) : '';
                $threadEntity[] = [
                    "thread_id"                 =>  $thread->thread_id,
                    "thread_comment_id"         =>  $parentCommentEntity->thread_comment_id,
                    "item_comment"              =>  $parentCommentEntity->source_comment,
                    "created_by"                =>  $parentCommentEntity->created_by,

                    "created_by_id"             =>  $parentCommentEntity->created_by_id,

                    "updated_at"                =>  $parentCommentEntity->updated_at,
                    "assign_to"                 =>  !empty($assignTo) ? $assignTo : "",
                    "assign_to_id"              =>  !empty($thread->assign_to) ? $thread->assignTo->user_id : "",
                    "status"                    =>  $thread->status,
                    "comments"                  =>  $this->parsedThreadCommentResponseDetailforThread()
                ];
            }
          
        }

        $this->setThreadEntity($threadEntity);
    }
}
