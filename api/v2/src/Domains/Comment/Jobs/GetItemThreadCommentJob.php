<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ItemThreadCommentRepositoryInterface;

class GetItemThreadCommentJob extends Job
{
    use DateHelpersTrait;

    private $statusValues;

    private $itemThreadCommentIdentifier;
    private $itemThreadCommentRepository;

    private $itemThreadDetail;

    private $itemThreadCommentDetail;
    private $itemThreadCommentEntity;   

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the private identifier
        $this->setItemThreadCommentIdentifier($identifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadCommentRepositoryInterface $itemThreadCommentRepository)
    {
        //Set the ItemThreadComment Repository handler
        $this->itemThreadCommentRepository  =   $itemThreadCommentRepository;
        $this->setDefaultStatusMatrix();
        $itemThreadCommentIdentifier        =   $this->getItemThreadCommentIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_thread_comment_id', 'item_thread_id', 'parent_id','comment','created_by', 'updated_at'];
        $keyValuePairs      =   ['item_thread_comment_id' => $itemThreadCommentIdentifier];
        $relations          =   ['itemThreadDetail'];

        $itemThreadCommentDetail   =   $this->itemThreadCommentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations);

        $this->setItemThreadCommentDetail($itemThreadCommentDetail);

        $this->parseResponse();

        $itemThreadCommentDetailResponse = $this->getItemThreadCommentEntity();

        return $itemThreadCommentDetailResponse;
    }

    public function setItemThreadCommentIdentifier($identifier){
        $this->itemThreadCommentIdentifier = $identifier;
    }

    public function getItemThreadCommentIdentifier(){
        return $this->itemThreadCommentIdentifier;
    }

    public function setItemThreadCommentDetail($data){
        $this->itemThreadDetail = $data;
    }

    public function getItemThreadCommentDetail(){
        return $this->itemThreadDetail;
    }

    public function setItemThreadCommentEntity($entity) {
        $this->itemThreadEntity = $entity;
    }

    public function getItemThreadCommentEntity(){
        return $this->itemThreadEntity;
    }

    public function setDefaultStatusMatrix() {
        $this->statusValues = config("default_enum_setting.comment_status");
    }

    public function getDefaultStatusMatrix(){
        return $this->statusValues;
    }

    private function parseResponse(){
        $statusValues               =   $this->getDefaultStatusMatrix();
        $itemThreadCommentDetail    =   $this->getItemThreadCommentDetail();

        //dd($itemThreadCommentDetail);

        $itemThreadCommentEntity =  [
        
            "item_thread_comment_id"    =>  $itemThreadCommentDetail[0]->item_thread_comment_id,
            "parent_id"                 =>  $itemThreadCommentDetail[0]->parent_id,
            "item_thread_id"            =>  $itemThreadCommentDetail[0]->itemThreadDetail->item_thread_id,
            "comment"                   =>  $itemThreadCommentDetail[0]->comment,
            "assign_to"                 =>  $itemThreadCommentDetail[0]->itemThreadDetail->assign_to,
            "status"                    =>  $statusValues[$itemThreadCommentDetail[0]->itemThreadDetail->status],
            "created_by"                =>  $itemThreadCommentDetail[0]->created_by,
            "updated_at"                =>  $this->formatDateTimeToISO8601($itemThreadCommentDetail[0]->updated_at)         
        ];

        $this->setItemThreadCommentEntity($itemThreadCommentEntity);
    }
}
