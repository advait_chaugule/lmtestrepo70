<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

class GetItemThreadJob extends Job
{
    private $itemThreadIdentifier;
    private $itemThreadRepository;

    private $itemThreadDetail;
    private $itemThreadEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the private identifier
        $this->setItemThreadIdentifier($identifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadRepositoryInterface $itemThreadRepository)
    {
        //Set the ItemThreadComment Repository handler
        $this->itemThreadRepository  =   $itemThreadRepository;
        $itemThreadIdentifier        =   $this->getItemThreadIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_thread_id','assign_to','status','created_at'];
        $keyValuePairs      =   ['item_thread_id' => $itemThreadIdentifier];
        $relations          =   ['itemThreadCommentDetail'];

        $itemThreadDetail   =   $this->itemThreadRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations);

        $this->setItemThreadDetail($itemThreadDetail);

        $this->parseResponse();

        $itemThreadDetailResponse = $this->getItemThreadEntity();

        return $itemThreadDetailResponse;
    }

    public function setItemThreadIdentifier($identifier){
        $this->itemThreadIdentifier = $identifier;
    }

    public function getItemThreadIdentifier(){
        return $this->itemThreadIdentifier;
    }

    public function setItemThreadDetail($data){
        $this->itemThreadDetail = $data;
    }

    public function getItemThreadDetail(){
        return $this->itemThreadDetail;
    }

    public function setItemThreadEntity($entity) {
        $this->itemThreadEntity = $entity;
    }

    public function getItemThreadEntity(){
        return $this->itemThreadEntity;
    }

    private function parseResponse(){
        $itemThreadEntity   =   [];
        $itemThreadDetail   =   $this->getItemThreadDetail();
        
        foreach($itemThreadDetail as $itemThread){
            $itemThreadEntity = [
                'item_thread_id'    =>  $itemThread->item_thread_id,
                'assign_to'         =>  $itemThread->assign_to,
                'status'            =>  $itemThread->status,
            ];
    
            foreach($itemThread->itemThreadCommentDetail as $itemThreadComment){
                if($itemThreadComment->parent_id === ''){
                    $itemThreadEntity['item_thread_comment'] = [
                        'item_thread_comment_id'    =>  $itemThreadComment->item_thread_comment_id,
                        'comment'                   =>  $itemThreadComment->comment,
                        'created_by'                =>  $itemThreadComment->created_by,
                    ];
                }
            }
        }

        $this->setItemThreadEntity($itemThreadEntity);
    }
}
