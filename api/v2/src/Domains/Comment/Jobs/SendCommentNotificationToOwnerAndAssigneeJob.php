<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;
use Lucid\Foundation\QueueableJob;

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

use Illuminate\Support\Facades\Mail;
use App\Domains\Comment\Mail\SendMailForStatusChange;

class SendCommentNotificationToOwnerAndAssigneeJob extends Job
{
    private $receiverEmail;

    public $subject;
    public $body;
    public $parentComment;
    public $projectLink;
    public $commentList;
    public $organizationName;
    public $orgCode;
    public $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->receiverEmail = $data['receiver_email'];
        $this->comment = $data['parent_comment'];
        $mainComment = $data['mainComment'];
        $this->subject  =   $data['subject'];
        $statusBeforeUpdate  =   $data['statusBeforeUpdate'];
        $this->organizationName = $data['organization_name'];
        $this->orgCode          = $data['organization_code'];
        $this->domainName          = $data['domain_name'];
        /*
        if(isset($data['new_status'])){
            $this->body     =   $data['changed_by'].' has changed the status to '.$data['new_status'].' of an action item in ';       
        }
        else{
            $this->body     =   $data['changed_by'].' has modified the reply to an action item in ';
        }
        
        $this->project  =   $data['project_name'];

        $parentCommentDetail = explode('||', $data['parent_comment']);

        if(isset($parentCommentDetail[2])) {
            $this->parentComment = '<div style="font-size:14px;"><strong style="color: #000000; font-size:15px;">'.$parentCommentDetail[0].'</strong><br/><span style="color:#72809A; display:inline-block; margin: 5px 0;">'.$parentCommentDetail[1].'</span><br/>+<span style="color: #006eff;text-decoration: underline;">'.$parentCommentDetail[2].'</span><br/><em style="color: #72809A;">Assigned to you</em></div>';
        }
        else{
            $this->parentComment = '<div style="font-size:14px;"><strong style="color: #000000; font-size:15px;">'.$parentCommentDetail[0].'</strong><br/><span style="color:#72809A; display:inline-block; margin: 5px 0;">'.$parentCommentDetail[1].'</span><br/></div>';
        }
        
        if(sizeOf($data['commentList']) > 0){
            unset($data['commentList'][sizeOf($data['commentList'])-1]);
            $this->commentList  =   '';
            foreach($data['commentList'] as $comments){
                $this->commentList .= '<tr><td aria-label="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." style="border-top:1px solid #dddddd; padding:15px; background:#F7F9FA;"><div><strong style="color: #000000;">Avalon Saldanha</strong><br/><p style="margin-top:0; font-size: 14px;">'.$comments['comment'].'</p></div></td></tr>';
            }
        }
        else{
            $this->commentList  =   '';
        }
                
              
        $this->projectLink    =   env('BASE_URL') . '#/app/projectauthoring/' . $data['project_id'] ;
        */
        if($statusBeforeUpdate!==4)
        {
            //$mainComment = $this->get_string_between($this->comment, '||', '||');
            $this->body = 'Your ' .$mainComment. ' status has been updated.';
        }
        else
        {
            //$mainComment = $this->get_string_between($this->comment, '||', '||');
            $this->body = 'Your comment ' .$mainComment. ' has been re-opened.';
        }
        
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Create an SesClient. Change the value of the region parameter if you're 
        // using an AWS Region other than US West (Oregon). Change the value of the
        // profile parameter if you want to use a profile in your credentials file
        // other than the default.
        $SesClient = new SesClient([
            //'profile' => 'default',
            'version' => '2010-12-01',
            'region'  => 'us-east-1',
            'credentials' => [
                    'key' => 'AKIAJYLGG5IQHTQG4QQQ',
                    'secret' => '4zjJeGDWIFC9mZeL0gsK+ta482SDbIklVOPWOjCK',
            ]

        ]);

        // Replace sender@example.com with your "From" address.
        // This address must be verified with Amazon SES.
        $sender_email = 'acmtadmin@learningmate.com';
        if(strpos($this->domainName,'localhost')!==false)
        {
            $url = env('BASE_URL');
        }else{
            $url = $this->domainName;
        }
        $linkUrl = str_replace('api.','',$url);
        // Specify a configuration set. If you do not want to use a configuration
        // set, comment the following variable, and the
        // 'ConfigurationSetName' => $configuration_set argument below.
        //$configuration_set = 'ConfigSet';

        $subject = 'Comment Status Update';
        $plaintext_body = 'This email was sent with Amazon SES using the AWS SDK for PHP.' ;
        $html_body =  '';
        $char_set = 'UTF-8';

        try {
            $result = $SesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => $this->receiverEmail,
                ],
                'ReplyToAddresses' => [$sender_email],
                'Source' => $sender_email,
                'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => $char_set,
                        'Data' => '<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: Nunito, sans-serif, Arial, Helvetica, sans-serif;  font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><head><meta name="viewport" content="width=device-width" /><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600" rel="stylesheet" type=\'text/css\'><style>@import url(\'https://fonts.googleapis.com/css?family=Nunito:300,400,600\');@media screen{body{margin: 0;padding: 0;min-width: 100%;width: 100% !important;}img{border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;}table{border-collapse: collapse !important;}.min-width{min-width: 100% !important;}}@media screen and (min-width: 600px){.email_txt{padding-right: 15%;width: 100%;}}</style></head><body style="font-family: Nunito, sans-serif, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0; background: none repeat scroll 0 0 #ffffff; color:#000000;"><table width="100%" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed;" class="min-width" role="presentation" ><tr><td><div style="width:auto; max-width:600px; margin-left:auto; margin-right:auto; margin-top:0; margin-bottom:0; padding-left: 10%; padding-right: 10%; font-size: 14px;"><table width="100%" cellpadding="0" cellpadding="0" align="center" border="0" role="presentation"><tr><td style="height: 40px;">&#160;</td></tr><tr><td><img src="'.$url.'server/logo/logo.png" width="91" height="20" alt="ACMT" role="img" aria-label="ACMT" /></td></tr><tr><td style="height: 40px;">&#160;</td></tr><tr><td style="border-bottom:1px solid #dddddd; padding-bottom: 15px;"><p style="font-family: Nunito, sans-serif, Arial, Helvetica, sans-serif; font-size:14px;" role="heading" aria-label="Avalon Saldanha assigned you an action item in taxonomy builder">'.$this->body.'</p></td></tr><tr> <td style="font-family: \'Nunito\', sans-serif, Arial, Helvetica, sans-serif;font-size:12px;" aria-label="No worries! Someone might have added your email address by mistake. You can ignore this email for now."><b>Didn’t expect this email?</b><br><br>No worries! Someone might have added your email address by mistake. You can ignore this email for now.<br>Don\'t want to see these emails from the ACMT account for.'.$this->organizationName.'?<b>
                            <a href='.$linkUrl.'#/org/'.$this->orgCode.'/app/notifications>Notification center</a></b>
                            <br><br><br></tr><tr><td style="height: 100px;">&#160;</td></tr><tr><td style="margin-bottom: 3px;" aria-label="Team COMET">Team ACMT</td></tr><tr><td style="height: 3px;"></td></tr><tr><td style="margin-top: 0;" aria-label="A LearningMate product.">A LearningMate product.</td></tr><tr><td style="height: 100px;">&#160;</td></tr></table></div></td></tr></table></body></html>',
                    ],
                ],
                'Subject' => [
                    'Charset' => $char_set,
                    'Data' => $this->subject,
                ],
                ],
                // If you aren't using a configuration set, comment or delete the
                // following line
                //'ConfigurationSetName' => $configuration_set,
            ]);
            $messageId = $result['MessageId'];
            return $messageId;
        } catch (AwsException $e) {
            // output error message if fails
            return $e->getMessage();
        } 
    }

    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
