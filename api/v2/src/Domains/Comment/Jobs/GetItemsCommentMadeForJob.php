<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class GetItemsCommentMadeForJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemsDetails)
    {
        $this->setRequestData($itemsDetails);
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadComment)
    {
        $this->threadComment = $threadComment;
        $itemsDetails=$this->requestData;
        return $this->threadComment->getItemsCommentMadeFor($itemsDetails);
    }
}
