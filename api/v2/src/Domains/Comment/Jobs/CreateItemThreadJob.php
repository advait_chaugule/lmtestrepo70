<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemThreadRepositoryInterface;

class CreateItemThreadJob extends Job
{
    private $itemThreadRepository;

    private $itemIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, array $requestData)
    {
        //Set public data attribute
        $this->setItemIdentifier($itemIdentifier);
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadRepositoryInterface $itemThreadRepository)
    {
        //Set the repo handler
        $this->itemThreadRepository = $itemThreadRepository;
        $identifier = $this->getItemIdentifier();

        $inputData = $this->requestData;
        unset($inputData['comment']);
        //dd($inputData);

        $savedItemThreadData = $this->itemThreadRepository->fillAndSave($inputData);
        return $savedItemThreadData;
    }

    public function setItemIdentifier($identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(){
        return $this->itemIdentifier;
    }
}
