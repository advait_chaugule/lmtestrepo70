<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ItemThreadRepositoryInterface;
use App\Data\Repositories\Contracts\ItemThreadCommentRepositoryInterface;

class ListCommentbyItemThreadIdJob extends Job
{
    use DateHelpersTrait;

    private $organizationIdentifier;
    private $itemThreadIdentifier;

    private $itemThreadCommentRepository;
    private $itemThreadRepository;

    private $itemThreadDetail;
    private $itemThreadEntity;

    private $itemThreadCommentDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemThreadIdentifier, string $organizationIdentifier)
    {
        //Set the private attribute to Role Identifier
        $this->setItemThreadIdentifier($itemThreadIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadRepositoryInterface $itemThreadRepository, ItemThreadCommentRepositoryInterface $itemThreadCommentRepository)
    {
        //Set ItemThread and ItemThreadComment Repo handler
        $this->itemThreadCommentRepository  = $itemThreadCommentRepository;
        $this->itemThreadRepository         = $itemThreadRepository;

        //get ItemThread detail from database
        $itemThreadDetail = $this->getItemThreadDetailFromModel();

        // set ItemThread details globally
        $this->setItemThreadDetail($itemThreadDetail);

        //Set parsed ItemThread details
        $this->createParsedItemThreadResponseDetail();

        // finally return the standard ItemThread and ItemThreadComment 
        return $this->getItemThreadEntity();
    }

    //Public Getter and Setter method
    public function setItemThreadIdentifier($itemThreadIdentifier){
        $this->itemThreadIdentifier =   $itemThreadIdentifier;
    }

    public function getItemThreadIdentifier(){
        return $this->itemThreadIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }

    public function setItemThreadDetail($data){
        $this->itemThreadDetail = $data;
    }

    public function getItemThreadDetail(){
        return $this->itemThreadDetail;
    }

    public function setItemThreadCommentDetail($data){
        $this->itemThreadCommentDetail = $data;
    }

    public function getItemThreadCommentDetail(){
        return $this->itemThreadCommentDetail;
    }

    public function setItemThreadEntity($itemThreadEntity) {
        $this->itemThreadEntity = $itemThreadEntity;
    }

    public function getItemThreadEntity() {
        return $this->itemThreadEntity;
    }

    private function getItemThreadCommentDetailFromModel() {
        $itemThreadIdentifier   =   $this->getItemThreadIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection = true;
        $fieldsToReturn = ['item_thread_id', 'item_thread_comment_id', 'parent_id', 'comment', 'created_by', 'updated_at'];
        $keyValuePairs  = ['item_thread_id' => $itemThreadIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations      = ['createdBy'];


        $itemThreadCommentDetail = $this->itemThreadCommentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

        // set ItemThreadComment details
        $this->setItemThreadCommentDetail($itemThreadCommentDetail);
    }

    private function getItemThreadDetailFromModel() {
        $itemThreadIdentifier   =   $this->getItemThreadIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection = true;
        $fieldsToReturn = ['item_thread_id', 'assign_to', 'status'];
        $keyValuePairs  = ['item_thread_id' => $itemThreadIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations      = ['assignTo'];


        $itemThreadDetail = $this->itemThreadRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

        return $itemThreadDetail;
    }

    private function parsedItemThreadCommentResponseDetailforItemThread(){
        $itemThreadCommentEntity = [];

        //get ItemThreadComment detail from database
        $itemThreadComments         =   $this->getItemThreadCommentDetailFromModel();

        $itemThreadCommentDetail    =   $this->getItemThreadCommentDetail();   

        foreach($itemThreadCommentDetail as $itemThreadComment)
        {
            $createdBy = $itemThreadComment->createdBy->first_name." ".$itemThreadComment->createdBy->last_name;
            $itemThreadCommentEntity[] = [
                "item_thread_comment_id"    =>  $itemThreadComment->item_thread_comment_id,
                "parent_id"                 =>  !empty($itemThreadComment->parent_id) ? $itemThreadComment->parent_id : "",
                "comment"                   =>  !empty($itemThreadComment->comment) ? $itemThreadComment->comment : "",
                "created_by"                =>  !empty($createdBy) ? $createdBy : "",
                "updated_at"                =>  $this->formatDateTimeToISO8601($itemThreadComment->updated_at)
            ];
        }

        return $itemThreadCommentEntity;
        
    }

    private function createParsedItemThreadResponseDetail(){
        $itemThreadEntity = [];

        $itemThreadDetail = $this->getItemThreadDetail();

        foreach($itemThreadDetail as $itemThread)
        {
            $assignTo = !empty($itemThread->assign_to) ? ($itemThread->assignTo->first_name." ".$itemThread->assignTo->last_name) : '';
            $itemThreadEntity[] = [
                "item_thread_id"    =>  $itemThread->item_thread_id,
                "assign_to"         =>  !empty($assignTo) ? $assignTo : "",
                "status"            =>  $itemThread->status,
                "comments"          =>  $this->parsedItemThreadCommentResponseDetailforItemThread()
            ];
        }

        $this->setItemThreadEntity($itemThreadEntity);
    }
}
