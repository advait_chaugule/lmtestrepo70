<?php
namespace App\Domains\Comment\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemThreadCommentRepositoryInterface;

class UpdateItemThreadCommentJob extends Job
{
    private $itemThreadCommentRepository;

    private $itemThreadCommentIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemThreadCommentIdentifier, array $requestData)
    {
        //Set public data attribute
        $this->requestData = $requestData;
        $this->setItemThreadCommentIdentifier($itemThreadCommentIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemThreadCommentRepositoryInterface $itemThreadCommentRepository)
    {
        //Set the repo handler
        $this->itemThreadCommentRepository = $itemThreadCommentRepository;
        $identifier =   $this->getItemThreadCommentIdentifier();
        $inputData  =   $this->requestData;

        $columnValueFilterPairs = ['item_thread_comment_id' =>  $identifier];
        $attributes             = ['comment' => $inputData['comment'], 'updated_at' =>  $inputData['updated_at']];             

        $updateItemThreadCommentData = $this->itemThreadCommentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        return $updateItemThreadCommentData;
    }

    public function setItemThreadCommentIdentifier($identifier) {
        $this->itemThreadCommentIdentifier = $identifier;
    }

    public function getItemThreadCommentIdentifier(){
        return $this->itemThreadCommentIdentifier;
    }
}
