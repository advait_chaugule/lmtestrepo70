<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\DeleteCommentJob;
use Tests\TestCase;

class DeleteCommentJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new DeleteCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
