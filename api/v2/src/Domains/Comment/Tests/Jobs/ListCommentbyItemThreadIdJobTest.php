<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\ListCommentbyItemThreadIdJob;
use Tests\TestCase;

class ListCommentbyItemThreadIdJobTest extends TestCase
{
    public function test_ItemThreadIdentifier()
    {
        $assets = new ListCommentbyItemThreadIdJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadIdentifier($value);
        $this->assertEquals($value,$assets->getItemThreadIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new ListCommentbyItemThreadIdJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ItemThreadDetail()
    {
        $assets = new ListCommentbyItemThreadIdJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadDetail($value);
        $this->assertEquals($value,$assets->getItemThreadDetail());
    }

    public function test_ItemThreadCommentDetail()
    {
        $assets = new ListCommentbyItemThreadIdJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentDetail($value);
        $this->assertEquals($value,$assets->getItemThreadCommentDetail());
    }

    public function test_ItemThreadEntity()
    {
        $assets = new ListCommentbyItemThreadIdJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadEntity($value);
        $this->assertEquals($value,$assets->getItemThreadEntity());
    }
}
