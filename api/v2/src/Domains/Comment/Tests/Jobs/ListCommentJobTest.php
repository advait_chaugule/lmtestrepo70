<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\ListCommentJob;
use Tests\TestCase;

class ListCommentJobTest extends TestCase
{
    public function test_ThreadSourceIdentifier()
    {
        $assets = new ListCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setThreadSourceIdentifier($value);
        $this->assertEquals($value,$assets->getThreadSourceIdentifier());
    }

    public function test_ThreadIdentifier()
    {
        $assets = new ListCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setThreadIdentifier($value);
        $this->assertEquals($value,$assets->getThreadIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new ListCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ThreadDetail()
    {
        $assets = new ListCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setThreadDetail($value);
        $this->assertEquals($value,$assets->getThreadDetail());
    }

    public function test_ThreadCommentDetail()
    {
        $assets = new ListCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setThreadCommentDetail($value);
        $this->assertEquals($value,$assets->getThreadCommentDetail());
    }

    public function test_ThreadEntity()
    {
        $assets = new ListCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','8bc041fe-d5ab-4860-8c17-f7deb805d3d4','8bc041fe-d5ab-4860-8c17-f7deb805d3d4');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setThreadEntity($value);
        $this->assertEquals($value,$assets->getThreadEntity());
    }
}
