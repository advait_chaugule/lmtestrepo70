<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\CreateItemThreadCommentJob;
use Tests\TestCase;

class CreateItemThreadCommentJobTest extends TestCase
{
    public function test_ItemIdentifier()
    {
        $data[] = 'ee96ccc7-b6fd-4c71-a5e5-66fd20530f78';
        $assets = new CreateItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }
    public function test_ItemThreadCommentData()
    {
        $data[] = 'ee96ccc7-b6fd-4c71-a5e5-66fd20530f78';
        $assets = new CreateItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentData($value);
        $this->assertEquals($value,$assets->getItemThreadCommentData());
    }

    public function test_ItemThreadCommentEntity()
    {
        $data[] = 'ee96ccc7-b6fd-4c71-a5e5-66fd20530f78';
        $assets = new CreateItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentEntity($value);
        $this->assertEquals($value,$assets->getItemThreadCommentEntity());
    }
}
