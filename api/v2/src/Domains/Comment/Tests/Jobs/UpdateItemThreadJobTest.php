<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\UpdateItemThreadJob;
use Tests\TestCase;

class UpdateItemThreadJobTest extends TestCase
{
    public function test_ItemIdentifier()
    {
        $data[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';
        $assets = new UpdateItemThreadJob($data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }
}
