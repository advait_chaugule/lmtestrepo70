<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\GetItemThreadCommentJob;
use Tests\TestCase;

class GetItemThreadCommentJobTest extends TestCase
{
    public function test_ItemThreadCommentIdentifier()
    {
        $assets = new GetItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentIdentifier($value);
        $this->assertEquals($value,$assets->getItemThreadCommentIdentifier());
    }

    public function test_ItemThreadCommentDetail()
    {
        $assets = new GetItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentDetail($value);
        $this->assertEquals($value,$assets->getItemThreadCommentDetail());
    }

    public function test_ItemThreadCommentEntity()
    {
        $assets = new GetItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentEntity($value);
        $this->assertEquals($value,$assets->getItemThreadCommentEntity());
    }
}
