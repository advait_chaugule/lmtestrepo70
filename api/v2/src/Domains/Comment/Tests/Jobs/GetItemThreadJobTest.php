<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\GetItemThreadJob;
use Tests\TestCase;

class GetItemThreadJobTest extends TestCase
{
    public function test_ItemThreadIdentifier()
    {
        $assets = new GetItemThreadJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadIdentifier($value);
        $this->assertEquals($value,$assets->getItemThreadIdentifier());
    }

    public function test_ItemThreadDetail()
    {
        $assets = new GetItemThreadJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadDetail($value);
        $this->assertEquals($value,$assets->getItemThreadDetail());
    }

    public function test_ItemThreadEntity()
    {
        $assets = new GetItemThreadJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadEntity($value);
        $this->assertEquals($value,$assets->getItemThreadEntity());
    }
}
