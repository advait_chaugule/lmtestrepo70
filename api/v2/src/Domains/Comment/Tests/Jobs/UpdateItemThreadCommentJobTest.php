<?php
namespace App\Domains\Comment\Tests\Jobs;

use App\Domains\Comment\Jobs\UpdateItemThreadCommentJob;
use Tests\TestCase;

class UpdateItemThreadCommentJobTest extends TestCase
{
    public function test_ItemThreadCommentIdentifier()
    {
        $data[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';
        $assets = new UpdateItemThreadCommentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemThreadCommentIdentifier($value);
        $this->assertEquals($value,$assets->getItemThreadCommentIdentifier());
    }
}
