<?php
namespace App\Domains\Comment\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ItemThreadCommentExistsValidator extends BaseValidator {

    protected $rules = [
        'thread_comment_id' => 'required|exists:thread_comments'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];
    
}