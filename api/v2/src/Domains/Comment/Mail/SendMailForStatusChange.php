<?php

namespace App\Domains\Comment\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailForStatusChange extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $parentComment;
    public $projectLink;
    public $commentList;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject  =   $data['subject'];
        if(isset($data['new_status'])){
            $this->body     =   $data['changed_by'].' has changed the status to '.$data['new_status'].' of an action item in ';       
        }
        else{
            $this->body     =   $data['changed_by'].' has modified the reply to an action item in ';
        }
        $this->project  =   $data['project_name'];

        $parentCommentDetail = explode('||', $data['parent_comment']);

        if(isset($parentCommentDetail[2])) {
            $this->parentComment = '<div style="font-size:14px;"><strong style="color: #000000; font-size:15px;">'.$parentCommentDetail[0].'</strong><br/><span style="color:#72809A; display:inline-block; margin: 5px 0;">'.$parentCommentDetail[1].'</span><br/>+<span style="color: #006eff;text-decoration: underline;">'.$parentCommentDetail[2].'</span><br/><em style="color: #72809A;">Assigned to you</em></div>';
        }
        else{
            $this->parentComment = '<div style="font-size:14px;"><strong style="color: #000000; font-size:15px;">'.$parentCommentDetail[0].'</strong><br/><span style="color:#72809A; display:inline-block; margin: 5px 0;">'.$parentCommentDetail[1].'</span><br/></div>';
        }
        
        if(sizeOf($data['commentList']) > 0){
            unset($data['commentList'][sizeOf($data['commentList'])-1]);
            $this->commentList  =   '';
            foreach($data['commentList'] as $comments){
                $this->commentList .= '<tr><td aria-label="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." style="border-top:1px solid #dddddd; padding:15px; background:#F7F9FA;"><div><strong style="color: #000000;">Avalon Saldanha</strong><br/><p style="margin-top:0; font-size: 14px;">'.$comments['comment'].'</p></div></td></tr>';
            }
        }
        else{
            $this->commentList  =   '';
        }
                
              
        $this->projectLink    =   env('BASE_URL') . '#/app/projectauthoring/' . $data['project_id'] ;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        // return $this->view('view.name');
        return $this->view('email.emailcommentupdate')
                    ->with([
                        "body"          =>  $this->body,
                        "project"       =>  $this->project,
                        "parentComment" =>  $this->parentComment,
                        "projectLink"   =>  $this->projectLink,
                        "commentList"   =>  $this->commentList
                    ])
                    ->subject($this->subject);
    }
}
