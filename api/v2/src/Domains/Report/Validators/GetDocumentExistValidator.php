<?php
namespace App\Domains\Report\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class GetDocumentExistValidator extends BaseValidator {

    protected $rules = [
        'document_id' => 'required|exists:documents'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];
    
}