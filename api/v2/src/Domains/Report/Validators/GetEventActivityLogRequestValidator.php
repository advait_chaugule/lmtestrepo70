<?php
namespace App\Domains\Report\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class GetEventActivityLogRequestValidator extends BaseValidator {

    protected $rules = [
        'limit' => 'integer|required_with:offset',
        'offset' => 'integer|required_with:limit',
        'orderBy' => 'string|in:activity_log_timestamp|required_with:sorting',
        'sorting' => 'string|in:desc,asc|required_with:orderBy',
        'activity_log_start_date' => 'required|date|date_format:Y-m-d',
        'activity_log_end_date' => 'required|date|date_format:Y-m-d|after_or_equal:activity_log_start_date',
        'calendar_group_by_type' => 'in:day,week,month'
    ];

     protected $messages = [];
    
}