<?php
namespace App\Domains\Report\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class GetProjectExistValidator extends BaseValidator {

    protected $rules = [
        'project_id' => 'required|exists:projects'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];
    
}