<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetEventActivitiesJob extends Job
{
    private $activityLogRepository;
    private $requestFilters;
    private $requestUserDetails;
    private $activityTemplates;

    private $paginationFilters;
    private $sortFilters;
    private $otherFilters;
    private $rawActivityData;
    private $parsedActivityData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->setRequestFilters($requestData);
        $this->setRequestUserDetails($requestData["auth_user"]);
        $this->setActivityTemplates();
    }

    public function setRequestFilters(array $data) {
        $this->requestFilters = $data;
    }

    public function getRequestFilters(): array {
        return $this->requestFilters;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setActivityTemplates() {
        $this->activityTemplates = config("event_activity.activity_templates");
    }

    public function getActivityTemplates(): array {
        return $this->activityTemplates;
    }

    public function setPaginationFilters(array $data) {
        $this->paginationFilters = $data;
    }

    public function getPaginationFilters(): array {
        return $this->paginationFilters;
    }

    public function setSortFilters(array $data) {
        $this->sortFilters = $data;
    }

    public function getSortFilters(): array {
        return !empty($this->sortFilters) ? $this->sortFilters : [ "orderBy" => "activity_log_timestamp", "sorting" => "desc" ];
    }

    public function setOtherFilters(array $data) {
        $this->otherFilters = $data;
    }

    public function getOtherFilters(): array {
        return $this->otherFilters;
    }

    public function setRawActivityData(Collection $data) {
        $this->rawActivityData = $data;
    }

    public function getRawActivityData(): Collection {
        return $this->rawActivityData;
    }

    public function setParsedActivityData(array $data) {
        $this->parsedActivityData = $data;
    }

    public function getParsedActivityData(): array {
        return $this->parsedActivityData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $activityLogRepository)
    {
        // set database repositories
        $this->activityLogRepository = $activityLogRepository;

        // set pagination, sorting and other filters
        $this->setFilters();

        // fetch and set raw activity data
        $this->fetchAndSetRawActivityData();

        // parse and set raw activity data
        $this->parseAndSetRawActivityData();

        return $this->getParsedActivityData();
    
    }

    private function setFilters() {
        $requestFilters = $this->getRequestFilters();

        $paginationFilters = [];
        if( array_key_exists("limit", $requestFilters) && array_key_exists("offset", $requestFilters) ) {
            $paginationFilters = [ "limit" => (int) $requestFilters["limit"], "offset" => (int) $requestFilters["offset"] ];
        }
        $this->setPaginationFilters($paginationFilters);

        $sortFilters = [];
        $allowedOrderByFields = ["activity_log_timestamp"];
        $allowedSortingDirection = ["desc", "asc"];
        if(
            array_key_exists("orderBy", $requestFilters) && in_array($requestFilters["orderBy"], $allowedOrderByFields) &&
            array_key_exists("sorting", $requestFilters) && in_array($requestFilters["sorting"], $allowedSortingDirection)
        ) {
            $sortFilters = [ "orderBy" => $requestFilters["orderBy"], "sorting" => $requestFilters["sorting"] ];
        }
        $this->setSortFilters($sortFilters);

        $otherFilters = [];
        $this->setOtherFilters($otherFilters);
    }

    private function fetchAndSetRawActivityData() {
        $requestUserDetails = $this->getRequestUserDetails();
        $organizationId = $requestUserDetails["organization_id"];
        $returnCollection = true;
        $paginationFilters = $this->getPaginationFilters();
        $returnPaginatedData = !empty($paginationFilters);
        $fieldsToReturn = [
            'activity_log_id', 
            'user_id',
            'organization_id',
            'activity_event_type', 
            'activity_id', 
            'activity_event_sub_type',
            'activity_event_sub_type_activity_id',
            'cache_activity_name',
            'cache_activity_subtype_activity_name',
            'cache_user_display_name',
            'activity_log_timestamp'
        ];
        $keyValuePairs = [ 'organization_id' => $organizationId ];
        $sortfilters = $this->getSortFilters();
        $returnSortedData = !empty($sortfilters);
        $data = $this->activityLogRepository->find(
                                                $returnCollection,
                                                $fieldsToReturn,
                                                $keyValuePairs,
                                                $returnPaginatedData,
                                                $paginationFilters,
                                                $returnSortedData,
                                                $sortfilters
                                            );

        $this->setRawActivityData($data);
    }

    private function parseAndSetRawActivityData() {
        $rawActivityData = $this->getRawActivityData();
        $parsedData = [];
        foreach($rawActivityData as $activity) {
            $displayActivityText = $this->resolveActivityDisplayText($activity);
            $activityLogId = $activity->activity_log_id;
            $userId = $activity->user_id;
            $activityEventType = $activity->activity_event_type;
            $activityId = $activity->activity_id;
            $activityEventSubType = $activity->activity_event_sub_type ?: "";
            $activityEventSubTypeActivityId = $activity->activity_event_sub_type_activity_id ?: "";
            $cacheActivityName = $activity->cache_activity_name ?: "";
            $cacheActivitySubtypeActivityName = $activity->cache_activity_subtype_activity_name ?: "";
            $cacheUserDisplayName = $activity->cache_user_display_name ?: "";
            $activityLogTimestamp = $activity->activity_log_timestamp;
            $parsedData[] = [
                'display_activity_text' => $displayActivityText,
                'activity_log_id' => $activityLogId, 
                'user_id' => $userId, 
                'activity_event_type' => $activityEventType, 
                'activity_id' => $activityId, 
                'activity_event_sub_type' => $activityEventSubType,
                'activity_event_sub_type_activity_id' => $activityEventSubTypeActivityId,
                'cache_activity_name' => $cacheActivityName,
                'cache_activity_subtype_activity_name' => $cacheActivitySubtypeActivityName,
                'cache_user_display_name' => $cacheUserDisplayName,
                'activity_log_timestamp' => $activityLogTimestamp
            ];
        }
        $this->setParsedActivityData($parsedData);
    }

    private function resolveActivityDisplayText(FactActivityLog $activity): string {
        // set default display text
        $displayText = "";
        // get templates for report activities
        $activityTemplates = $this->getActivityTemplates();

        $activityEventType = $activity->activity_event_type;
        $activityEventSubType = $activity->activity_event_sub_type ?: "";
        // create the key to fetch activity specific template
        $templateKey = $activityEventSubType!=="" ? ($activityEventType . "-" . $activityEventSubType) : $activityEventType;
        // fetch the activity template
        $activityTemplate = !empty($activityTemplates[$templateKey]) ? $activityTemplates[$templateKey] : "";

        if($activityTemplate != "") {
            // set all template variables to resolve
            $cache_activity_name = $activity->cache_activity_name ?: "";
            $cache_activity_subtype_activity_name = $activity->cache_activity_subtype_activity_name ?: "";
            $cache_user_display_name = $activity->cache_user_display_name ?: "";
            // exract template variables pattern from activity template
            preg_match_all('/{{(.*?)}}/', $activityTemplate, $matches);
            if(!empty($matches[1])) {
                $extractedTemplateVariables = $matches[1];
                $displayText = $activityTemplate;
                // loop through each template variables and resolve activity template
                foreach($extractedTemplateVariables as $templateVariable) {
                    $searchString = "{{" . $templateVariable . "}}";
                    $replaceWithString = $templateVariable;
                    $displayText = str_replace($searchString, $replaceWithString, $displayText);
                }
            }
            
        }
        return $displayText;
    }
}
