<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetEventActivityLogJob extends Job
{
    private $activityLogRepository;
    private $requestFilters;
    private $requestUserDetails;

    private $paginationFilters;
    private $sortFilters;
    private $dateRangeFilters;

    private $activityLogData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->setRequestFilters($requestData);
        $this->setRequestUserDetails($requestData["auth_user"]);
    }

    public function setRequestFilters(array $data) {
        $this->requestFilters = $data;
    }

    public function getRequestFilters(): array {
        return $this->requestFilters;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setPaginationFilters(array $data) {
        $this->paginationFilters = $data;
    }

    public function getPaginationFilters(): array {
        return $this->paginationFilters;
    }

    public function setSortFilters(array $data) {
        $this->sortFilters = $data;
    }

    public function getSortFilters(): array {
        return !empty($this->sortFilters) ? $this->sortFilters : [ "orderBy" => "activity_log_timestamp", "sorting" => "desc" ];
    }

    public function setDateRangeFilters(array $data) {
        $this->dateRangeFilters = $data;
    }

    public function getDateRangeFilters(): array {
        return $this->dateRangeFilters;
    }

    public function setActivityLogData(Collection $data) {
        $this->activityLogData = $data;
    }

    public function getActivityLogData(): Collection {
        return $this->activityLogData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $activityLogRepository)
    {
        // set database repositories
        $this->activityLogRepository = $activityLogRepository;

        // set pagination, sorting and other filters
        $this->setFilters();

        // fetch and set activity log datafrom OLAP database
        $this->fetchAndSetActivityLogData();

        // return the activity log data
        return $this->getActivityLogData();
    }

    private function setFilters() {
        $requestFilters = $this->getRequestFilters();

        // pagination filters are optional
        $paginationFilters = [];
        if( array_key_exists("limit", $requestFilters) && array_key_exists("offset", $requestFilters) ) {
            $paginationFilters = [ "limit" => (int) $requestFilters["limit"], "offset" => (int) $requestFilters["offset"] ];
        }
        $this->setPaginationFilters($paginationFilters);

        // sort filters are optional
        $sortFilters = [];
        if( array_key_exists("orderBy", $requestFilters) && array_key_exists("sorting", $requestFilters) ) {
            $sortFilters = [ "orderBy" => $requestFilters["orderBy"], "sorting" => $requestFilters["sorting"] ];
        }
        $this->setSortFilters($sortFilters);

        // date range filters are mandatory
        $startDateTime = $requestFilters["activity_log_start_date"]." 00:00:00";
        $endDateTime = $requestFilters["activity_log_end_date"]." 23:59:59";
        $dateRangeFilters = [
            "start" => $startDateTime,
            "end" => $endDateTime
        ];
        $this->setDateRangeFilters($dateRangeFilters);
    }

    private function fetchAndSetActivityLogData() {
        $requestUserDetails = $this->getRequestUserDetails();
        $organizationId = $requestUserDetails["organization_id"];
        $paginationFilters = $this->getPaginationFilters();
        $returnPaginatedData = !empty($paginationFilters);
        $fieldsToReturn = [
            'activity_log_id', 
            'user_id',
            'organization_id',
            'activity_event_type', 
            'activity_id', 
            'activity_event_sub_type',
            'activity_event_sub_type_activity_id',
            'cache_activity_name',
            'cache_activity_subtype_activity_name',
            'cache_user_display_name',
            'activity_log_timestamp'
        ];
        $keyValuePairs = [ 'organization_id' => $organizationId ];
        $sortfilters = $this->getSortFilters();
        $returnSortedData = !empty($sortfilters);
        
        $rangeFilters = $this->getDateRangeFilters();
        $rangeQueryClauseField = "activity_log_timestamp";

        $data = $this->activityLogRepository->getActivityLogs(
                                                $fieldsToReturn,
                                                $keyValuePairs,
                                                $returnPaginatedData,
                                                $paginationFilters,
                                                $returnSortedData,
                                                $sortfilters,
                                                $rangeFilters,
                                                $rangeQueryClauseField
                                            );
        $this->setActivityLogData($data);
    }
}
