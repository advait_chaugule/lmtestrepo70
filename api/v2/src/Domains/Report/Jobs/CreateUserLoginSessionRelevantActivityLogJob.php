<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

class CreateUserLoginSessionRelevantActivityLogJob extends Job
{
    private $relevantEventTypes;
    private $relevantEventTypeValues;
    private $relevantActivityLogs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $activityLogs)
    {
        $this->setRelevantEventTypes();
        $this->setRelevantEventTypeValues();
        $this->setRelevantActivityLogs($activityLogs);
    }

    public function setRelevantEventTypes() {
        $this->relevantEventTypes = [
            "USER_LOGIN",
            "USER_LOGOUT",
            "USER_SESSION_REFRESHED"
        ];
    }

    public function getRelevantEventTypes(): array {
        return $this->relevantEventTypes;
    }

    public function setRelevantEventTypeValues() {
        $availableEventTypeValue = config("event_activity.event_type_value");
        $relevantEventTypes = $this->getRelevantEventTypes();
        $relevantEventTypeValues = [];
        foreach($relevantEventTypes as $eventType) {
            $relevantEventTypeValues[] = $availableEventTypeValue[$eventType];
        }
        $this->relevantEventTypeValues = $relevantEventTypeValues;
    }

    public function getRelevantEventTypeValues(): array {
        return $this->relevantEventTypeValues;
    }

    /**
     * Only set those activities which are relevant to the activity section
     */
    public function setRelevantActivityLogs(Collection $data) {
        $relevantEventTypeValues = $this->getRelevantEventTypeValues();
        $relevantActivityLogs = $data->whereIn('activity_event_type', $relevantEventTypeValues);
        $this->relevantActivityLogs = $relevantActivityLogs;
    }

    public function getRelevantActivityLogs(): Collection {
        return $this->relevantActivityLogs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->getRelevantActivityLogs();
    }
}
