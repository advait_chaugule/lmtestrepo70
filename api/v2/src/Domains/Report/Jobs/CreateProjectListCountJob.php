<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;
use App\Data\Models\Project;
use App\Data\Models\Stage;
use App\Data\Models\Thread;
use App\Data\Models\ThreadComment;
use App\Data\Models\User;
use DB;
use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;
use App\Data\Models\Role;

class CreateProjectListCountJob extends Job
{
    private $projectCreatedCount;
    private $projectAssignedCount;
    private $projectList;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->setData($data);    
    }
    
     public function setData($data) {
         $this->data = $data;
     }

     public function getData(): array {
         return $this->data;
     }
    
     public function setCountMetricsData(array $data) {
        $this->countMetricsData = $data;
     }

     public function getCountMetricsData(): array {
         return $this->countMetricsData;
     }

    public function setProjectlist(array $data) {
        $this->projectList = $data;
    }

    public function getProjectlist(): array {
        return $this->projectList;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->parseAndSetCountMetrices();
        return $this->getCountMetricsData();
    }

    private function parseAndSetCountMetrices() {
        $data = $this->getData();
        $this->getAllAssignedProject($data);
    }
    
    public function getAllAssignedProject($data){
        $userId =  $data['auth_user']['user_id'];
        $organization = $data['auth_user']['organization_id'];
       
        /**
         * get all the projects created by the user
         */
        $getProject = Project::where('created_by', $userId)->where('organization_id',$organization)->where('is_deleted',0)->where('project_type',1)->orderBy('updated_at','desc')->get()->toArray();
        $projectId= [];
        $workflowId = [];
        $wfStageId = [];
        $projectName = [];
        $documentId = [];
        foreach($getProject as  $getProjects)
        {
            $projectId[] = $getProjects['project_id'];
            $projectName[]= $getProjects['project_name'];
            $workflowId[] = $getProjects['workflow_id'];
            $wfStageId[] = $getProjects['current_workflow_stage_id'];
            $documentId[] = $getProjects['document_id'];     
        }
        
        /**
         * get the workflow stage name of projects created by the user
         */
        $stage = [];
        foreach($wfStageId as  $wfStageIds){
            $stage[] = substr($wfStageIds, strpos($wfStageIds, "||") + 2);
        }
        $stageName = Stage::select('stage_id','name')->whereIn('stage_id', $stage)->get()->toArray();

        $stageRoleID = array();
        $stageRoleID = DB::table('workflow_stage_role')
        ->select('role_id','workflow_stage_id')
        ->whereIn('workflow_stage_id',$wfStageId)
        ->get()->toArray();
    
        $stageRoleIDArr = [];
        foreach ($stageRoleID as $stageRoleIDs)
         {
             $stageRoleIDArr[] = $stageRoleIDs->role_id; 
             
        }

        $stageRoleName = [];
        $stageRoleName = Role::select('role_id','name')->whereIn('role_id', $stageRoleIDArr)->get()->toArray();
        
        $userDetail = DB::table('users')
        ->select('user_id','first_name','last_name')
        ->where('user_id',$userId)
        ->get()
        ->toArray();

        /** 
         * get the open comment of projects created by the user in documents
         */
        $getOpenCommentFromDocuments = array();
        $getOpenCommentFromDocuments = DB::table('thread_comments')
        ->join('threads','threads.thread_id','=','thread_comments.thread_id')
        ->join('projects','projects.document_id','=','thread_comments.thread_source_id')
        ->select('thread_comments.*','projects.project_id')
        ->where('thread_comments.is_deleted',0)
        ->where('threads.status',1)
        ->where('thread_comments.created_by',$userId)
        ->where('thread_comments.organization_id',$organization)
        ->get()->toArray();

         /** 
         * get the open comment of projects created by the user in items
         */
        $getOpenCommentFromItems = array();
        $getOpenCommentFromItems = DB::table('thread_comments')
        ->join('threads','threads.thread_id','=','thread_comments.thread_id')
        ->join('project_items','project_items.item_id','=','thread_comments.thread_source_id')
        ->select('thread_comments.*','project_items.project_id')
        ->where('thread_comments.is_deleted',0)
        ->where('threads.status',1)
        ->where('thread_comments.created_by',$userId)
        ->where('thread_comments.organization_id',$organization)
        ->get()->toArray();

        /**
         * Total comments by the user who created the projects
         */
        $totalCommentForProject = array_merge($getOpenCommentFromDocuments,$getOpenCommentFromItems);
        $getCommentProjectId = array();
        foreach($totalCommentForProject as $totalCommentForProjectK=>$totalCommentForProjectV)
        {
            $getCommentProjectId[] = $totalCommentForProjectV->project_id;
        }
        $getUniqueCommentProjectId = array();
        $getUniqueCommentProjectId = array_unique($getCommentProjectId);
        $getProjectCommentCount = array();
        foreach($getUniqueCommentProjectId as $getUniqueCommentProjectIdK=>$getUniqueCommentProjectIdV)
        {
            $count = 1;
            foreach($totalCommentForProject as $totalCommentForProjectK=>$totalCommentForProjectV)
            {
                if($getUniqueCommentProjectIdV==$totalCommentForProjectV->project_id)
                {
                    $getProjectCommentCount[$totalCommentForProjectV->project_id] = $count;
                    $count = $count+1;
                }
            }
        }
        /** Get the stage name for the project created by the user */
        $projectAndStageName = array();
        $dm=0;
        foreach($getProject as $getProjectK=>$getProjectV)
        {
            foreach($stageName as $stageNameK=>$stageNameV)
            {
                if(substr($getProjectV['current_workflow_stage_id'], strpos($getProjectV['current_workflow_stage_id'], "||") + 2)==$stageNameV['stage_id'])
                {
                    $projectAndStageName[$dm]['project_id'] = $getProjectV['project_id']; 
                    $projectAndStageName[$dm]['workflow_id'] = $getProjectV['workflow_id'];
                    $projectAndStageName[$dm]['organization_id'] = $getProjectV['organization_id'];
                    $projectAndStageName[$dm]['project_type'] = $getProjectV['project_type'];
                    $projectAndStageName[$dm]['project_name'] = $getProjectV['project_name'];
                    $projectAndStageName[$dm]['description'] = $getProjectV['description'];
                    $projectAndStageName[$dm]['document_id'] = $getProjectV['document_id'];
                    $projectAndStageName[$dm]['current_workflow_stage_id'] = $getProjectV['current_workflow_stage_id'];
                    $projectAndStageName[$dm]['is_deleted'] = $getProjectV['is_deleted'];
                    $projectAndStageName[$dm]['created_by'] = $getProjectV['created_by'];
                    $projectAndStageName[$dm]['updated_by'] = $getProjectV['updated_by'];
                    $projectAndStageName[$dm]['workflow_stage'] = $stageNameV['name'];
                    $dm=$dm+1;
                }
            }    
        }
        /** Get the user detail from the above loop */
        $ProjectStageNameUserDetail = array();
        $pd =0;
        foreach($projectAndStageName as $projectAndStageNameK=>$projectAndStageNameV)
        {
            foreach($userDetail as $userDetailK=>$userDetailV)
            {
                if($projectAndStageNameV['created_by']==$userDetailV->user_id)
                {
                    $ProjectStageNameUserDetail[$pd]['project_id'] = $projectAndStageNameV['project_id'];
                    $ProjectStageNameUserDetail[$pd]['workflow_id'] = $projectAndStageNameV['workflow_id'];
                    $ProjectStageNameUserDetail[$pd]['organization_id'] = $projectAndStageNameV['organization_id'];
                    $ProjectStageNameUserDetail[$pd]['project_type'] = $projectAndStageNameV['project_type'];
                    $ProjectStageNameUserDetail[$pd]['project_name'] = $projectAndStageNameV['project_name'];
                    $ProjectStageNameUserDetail[$pd]['description'] = $projectAndStageNameV['description'];
                    $ProjectStageNameUserDetail[$pd]['document_id'] = $projectAndStageNameV['document_id'];
                    $ProjectStageNameUserDetail[$pd]['current_workflow_stage_id'] = $projectAndStageNameV['current_workflow_stage_id'];
                    $ProjectStageNameUserDetail[$pd]['is_deleted'] = $projectAndStageNameV['is_deleted'];
                    $ProjectStageNameUserDetail[$pd]['created_by'] = $projectAndStageNameV['created_by'];
                    $ProjectStageNameUserDetail[$pd]['updated_by'] = $projectAndStageNameV['updated_by'];
                    $ProjectStageNameUserDetail[$pd]['workflow_stage'] = $projectAndStageNameV['workflow_stage'];
                    $ProjectStageNameUserDetail[$pd]['created_by_name'] = $userDetailV->first_name.$userDetailV->last_name;
                    $ProjectStageNameUserDetail[$pd]['top_contributor'] = $userDetailV->first_name.$userDetailV->last_name;     
                    $pd = $pd+1;
                }
            }
        }
    
        /** Get the comments in the project */
        $getCommentsInProject = array();
        $cd=0;
        foreach($ProjectStageNameUserDetail as $ProjectStageNameUserDetailK=>$ProjectStageNameUserDetailV)
        {
            foreach($getProjectCommentCount as  $getProjectCommentCountK=> $getProjectCommentCountV)
            {
                if($ProjectStageNameUserDetailV['project_id']==$getProjectCommentCountK)
                {
                    $getCommentsInProject[$cd]['project_id'] = $ProjectStageNameUserDetailV['project_id'];
                    $getCommentsInProject[$cd]['workflow_id'] = $ProjectStageNameUserDetailV['workflow_id'];
                    $getCommentsInProject[$cd]['organization_id'] = $ProjectStageNameUserDetailV['organization_id'];
                    $getCommentsInProject[$cd]['project_type'] = $ProjectStageNameUserDetailV['project_type'];
                    $getCommentsInProject[$cd]['project_name'] = $ProjectStageNameUserDetailV['project_name'];
                    $getCommentsInProject[$cd]['description'] = $ProjectStageNameUserDetailV['description'];
                    $getCommentsInProject[$cd]['document_id'] = $ProjectStageNameUserDetailV['document_id'];
                    $getCommentsInProject[$cd]['current_workflow_stage_id'] = $ProjectStageNameUserDetailV['current_workflow_stage_id'];
                    $getCommentsInProject[$cd]['is_deleted'] = $ProjectStageNameUserDetailV['is_deleted'];
                    $getCommentsInProject[$cd]['created_by'] = $ProjectStageNameUserDetailV['created_by'];
                    $getCommentsInProject[$cd]['updated_by'] = $ProjectStageNameUserDetailV['updated_by'];
                    $getCommentsInProject[$cd]['workflow_stage'] = $ProjectStageNameUserDetailV['workflow_stage'];
                    $getCommentsInProject[$cd]['created_by_name'] = $ProjectStageNameUserDetailV['created_by_name'];
                    $getCommentsInProject[$cd]['top_contributor'] = $ProjectStageNameUserDetailV['top_contributor'];  
                    $getCommentsInProject[$cd]['issue'] = $getProjectCommentCountV;    
                    $cd = $cd+1;
                }
            }    
        }
        $projectIdsWithComment = array_column($getCommentsInProject,'project_id');
        $projectIdsTotal = array_column($ProjectStageNameUserDetail,'project_id');

        $projectIdsWithoutComment = array();
        $projectIdsWithoutComment = array_diff($projectIdsTotal,$projectIdsWithComment);

        $projectsWithoutCommentData = array();
        if(!empty($projectIdsWithoutComment))
        {
            $pw =0;
            foreach($projectIdsWithoutComment as $projectIdsWithoutCommentK=>$projectIdsWithoutCommentV)
            {
                foreach($ProjectStageNameUserDetail as $ProjectStageNameUserDetailData=>$ProjectStageNameUserDetailKey)
                {
                    if($projectIdsWithoutCommentV==$ProjectStageNameUserDetailKey['project_id'])
                    {
                        $projectsWithoutCommentData[$pw]['project_id'] = $ProjectStageNameUserDetailKey['project_id'];
                        $projectsWithoutCommentData[$pw]['workflow_id'] = $ProjectStageNameUserDetailKey['workflow_id'];
                        $projectsWithoutCommentData[$pw]['organization_id'] = $ProjectStageNameUserDetailKey['organization_id'];
                        $projectsWithoutCommentData[$pw]['project_type'] = $ProjectStageNameUserDetailKey['project_type'];
                        $projectsWithoutCommentData[$pw]['project_name'] = $ProjectStageNameUserDetailKey['project_name'];
                        $projectsWithoutCommentData[$pw]['description'] = $ProjectStageNameUserDetailKey['description'];
                        $projectsWithoutCommentData[$pw]['document_id'] = $ProjectStageNameUserDetailKey['document_id'];
                        $projectsWithoutCommentData[$pw]['current_workflow_stage_id'] = $ProjectStageNameUserDetailKey['current_workflow_stage_id'];
                        $projectsWithoutCommentData[$pw]['is_deleted'] = $ProjectStageNameUserDetailKey['is_deleted'];
                        $projectsWithoutCommentData[$pw]['created_by'] = $ProjectStageNameUserDetailKey['created_by'];
                        $projectsWithoutCommentData[$pw]['updated_by'] = $ProjectStageNameUserDetailKey['updated_by'];
                        $projectsWithoutCommentData[$pw]['workflow_stage'] = $ProjectStageNameUserDetailKey['workflow_stage'];
                        $projectsWithoutCommentData[$pw]['created_by_name'] = $ProjectStageNameUserDetailKey['created_by_name'];
                        $projectsWithoutCommentData[$pw]['top_contributor'] = $ProjectStageNameUserDetailKey['top_contributor'];  
                        $projectsWithoutCommentData[$pw]['issue'] = 0;  
                        $pw = $pw+1;  
                    }
                }
            }
        }

        /** Get all the projects created by the user with and without comment */
        $getAllTheProjectCreated = array();
        $getAllTheProjectCreated = array_merge($getCommentsInProject,$projectsWithoutCommentData);

        $getRole = DB::table('project_users')
            ->join('projects', 'projects.project_id','=','project_users.project_id')
            ->select('project_users.workflow_stage_role_id','project_users.project_id')
            ->where('projects.is_deleted',0)
            ->where('project_users.is_deleted',0)
            ->whereIn('project_users.project_id',$projectIdsTotal)
            ->get()
            ->toArray();
            
            $getData = array();
            foreach($getRole as $getRoleV)
            {
                $getData[] = ['role_id'=>substr($getRoleV->workflow_stage_role_id,-36),'project_id'=>$getRoleV->project_id];    
            }
           
             $roleIds = array_column($getData,'role_id');
             $projectIds = array_column($getData,'project_id'); 
                $roleName = Role::select('role_id','name')->whereIn('role_id',$roleIds)->get();

                if($roleName)
                {
                    $data = $roleName->toArray();
                }

                $getProjectRelatedData = array();
                foreach($getData as $getDataK=>$getDataV)
                {
                   foreach($data as $dataK=>$dataV)
                   {
                        if($getDataV['role_id']==$dataV['role_id'])
                        {
                            $getDataV['name'] = $dataV['name'];
                            $getProjectRelatedData[]=$getDataV;
                        }
                   } 
                }
                $mapProjectsWithRoleIdAndRoleName = array();
                
                    foreach($getProjectRelatedData as $getProjectRelatedDataKey=>$getProjectRelatedDataValue)
                    {
                        
                       
                            $mapProjectsWithRoleIdAndRoleName[$getProjectRelatedDataValue['project_id']][]=
                            [
                            'role_id'=>$getProjectRelatedDataValue['role_id'],
                            'name'=>$getProjectRelatedDataValue['name']
                            ];
                        
                    }

                    $addTheRoleNameAssocitedWithProject = array();
                    $userRoleProjectId = array();
                    foreach($mapProjectsWithRoleIdAndRoleName as $mapProjectsWithRoleIdAndRoleNameK=>$mapProjectsWithRoleIdAndRoleNameV)
                    {
                        $addTheRoleNameAssocitedWithProject[$mapProjectsWithRoleIdAndRoleNameK] = implode(",",array_column($mapProjectsWithRoleIdAndRoleNameV,'name'));
                        $userRoleProjectId[] = $mapProjectsWithRoleIdAndRoleNameK;
                    
                    }

                    /** Projects with role */
                    $getAllTheProjectCreatedWithRoleName = array();
                    $lm=0;
                    foreach($getAllTheProjectCreated as $getAllTheProjectCreatedK=>$getAllTheProjectCreatedV)
                    {
                        foreach($mapProjectsWithRoleIdAndRoleName as $mapProjectsWithRoleIdAndRoleNameK=>$mapProjectsWithRoleIdAndRoleNameV)
                        {
                            if($getAllTheProjectCreatedV['project_id']==$mapProjectsWithRoleIdAndRoleNameK)
                            {
                                $getAllTheProjectCreatedWithRoleName[$lm]['project_id'] = $getAllTheProjectCreatedV['project_id'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['workflow_id'] = $getAllTheProjectCreatedV['workflow_id'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['organization_id'] = $getAllTheProjectCreatedV['organization_id'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['project_type'] = $getAllTheProjectCreatedV['project_type'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['project_name'] = $getAllTheProjectCreatedV['project_name'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['description'] = $getAllTheProjectCreatedV['description'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['document_id'] = $getAllTheProjectCreatedV['document_id'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['current_workflow_stage_id'] = $getAllTheProjectCreatedV['current_workflow_stage_id'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['is_deleted'] = $getAllTheProjectCreatedV['is_deleted'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['created_by'] = $getAllTheProjectCreatedV['created_by'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['updated_by'] = $getAllTheProjectCreatedV['updated_by'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['workflow_stage'] = $getAllTheProjectCreatedV['workflow_stage'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['created_by_name'] = $getAllTheProjectCreatedV['created_by_name'];
                                $getAllTheProjectCreatedWithRoleName[$lm]['top_contributor'] = $getAllTheProjectCreatedV['top_contributor'];  
                                $getAllTheProjectCreatedWithRoleName[$lm]['issue'] = $getAllTheProjectCreatedV['issue'];  
                                $getAllTheProjectCreatedWithRoleName[$lm]['assigned_role'] = $mapProjectsWithRoleIdAndRoleNameV;
                                $lm = $lm+1;  
                            }
                        }
                    }


                    /** Projects without role */
                    $projectIdsWithoutRoleName = array();
                    $projectIdsWithoutRoleName = array_diff($projectIdsTotal,$userRoleProjectId);


                    $projectsWithoutRoleData = array();

                    if(!empty($projectIdsWithoutRoleName))
                    {
                        $wl =0;
                        foreach($projectIdsWithoutRoleName as $projectIdsWithoutRoleNameK=>$projectIdsWithoutRoleNameV)
                        {
                            foreach($getAllTheProjectCreated as $getAllTheProjectCreatedK=>$getAllTheProjectCreatedV)
                            {
                                if($projectIdsWithoutRoleNameV==$getAllTheProjectCreatedV['project_id'])
                                {
                                    $projectsWithoutRoleData[$wl]['project_id'] = $getAllTheProjectCreatedV['project_id'];
                                    $projectsWithoutRoleData[$wl]['workflow_id'] = $getAllTheProjectCreatedV['workflow_id'];
                                    $projectsWithoutRoleData[$wl]['organization_id'] = $getAllTheProjectCreatedV['organization_id'];
                                    $projectsWithoutRoleData[$wl]['project_type'] = $getAllTheProjectCreatedV['project_type'];
                                    $projectsWithoutRoleData[$wl]['project_name'] = $getAllTheProjectCreatedV['project_name'];
                                    $projectsWithoutRoleData[$wl]['description'] = $getAllTheProjectCreatedV['description'];
                                    $projectsWithoutRoleData[$wl]['document_id'] = $getAllTheProjectCreatedV['document_id'];
                                    $projectsWithoutRoleData[$wl]['current_workflow_stage_id'] = $getAllTheProjectCreatedV['current_workflow_stage_id'];
                                    $projectsWithoutRoleData[$wl]['is_deleted'] = $getAllTheProjectCreatedV['is_deleted'];
                                    $projectsWithoutRoleData[$wl]['created_by'] = $getAllTheProjectCreatedV['created_by'];
                                    $projectsWithoutRoleData[$wl]['updated_by'] = $getAllTheProjectCreatedV['updated_by'];
                                    $projectsWithoutRoleData[$wl]['workflow_stage'] = $getAllTheProjectCreatedV['workflow_stage'];
                                    $projectsWithoutRoleData[$wl]['created_by_name'] = $getAllTheProjectCreatedV['created_by_name'];
                                    $projectsWithoutRoleData[$wl]['top_contributor'] = $getAllTheProjectCreatedV['top_contributor'];  
                                    $projectsWithoutRoleData[$wl]['issue'] = 0;
                                    $projectsWithoutRoleData[$lm]['assigned_role'] = [];  
                                    $wl = $wl+1;  
                                }
                            }
                        }
                    }

                    $finalDataOfAllTheProjectCreated = array();
                    $finalDataOfAllTheProjectCreated = array_merge($getAllTheProjectCreatedWithRoleName,$projectsWithoutRoleData);
    
        /** ###################################################################### */
         /**
         * get all the projects assigned to the user
         */
        $projectAssigned = array();
        $projectAssigned = DB::table('project_users')
        ->join('projects','projects.project_id','=','project_users.project_id')
        ->select('projects.*')
        ->where('project_users.is_deleted',0)
        ->where('projects.project_type',1)
        ->where('project_users.user_id',$userId)
        ->where('project_users.organization_id', $organization)
        ->orderBy('projects.updated_at', 'desc')
        ->get()->toArray();
        /**
         * get the workflow stage name of projects assigned to the user
         */
        $projectIdPA= [];
        $workflowIdPA = [];
        $wfStageIdPA = [];
        $projectNamePA = [];
        $documentIdPA = [];
        $createdByPA = [];
        foreach($projectAssigned as $projectAssignee)
        {
            $projectIdPA[] = $projectAssignee->project_id;
            $projectNamePA[]= $projectAssignee->project_name;
            $workflowIdPA[] = $projectAssignee->workflow_id;
            $wfStageIdPA[] = $projectAssignee->current_workflow_stage_id;
            $documentIdPA[] = $projectAssignee->document_id; 
            $createdByPA[] =  $projectAssignee->created_by;
        }
        
        $assigneeDetail = DB::table('project_users')
       ->join('users','users.user_id','=','project_users.created_by')
       ->select('users.user_id','users.first_name','users.last_name')
       ->where('project_users.is_deleted',0)
       ->whereIn('project_users.created_by',$createdByPA)
       ->distinct('project_users.created_by')
       ->get()
       ->toArray(); 
        /**
         * get the workflow stage name of projects assigned to the user
         */
        $stagePA = [];
        foreach($wfStageIdPA as  $wfStageIdPAIs){
            $stagePA[] = substr($wfStageIdPAIs, strpos($wfStageIdPAIs, "||") + 2);
        }
        $stageNamePA = Stage::select('stage_id','name')->whereIn('stage_id', $stagePA)->get()->toArray();

        $stageRoleIDPA = array();
        $stageRoleIDPA = DB::table('workflow_stage_role')
        ->select('role_id','workflow_stage_id')
        ->whereIn('workflow_stage_id',$wfStageIdPA)
        ->get()->toArray();
    
        $stageRoleIDPAArr = [];
        foreach ($stageRoleIDPA as $stageRoleIDPAs)
         {
             $stageRoleIDPAArr[] = $stageRoleIDPAs->role_id; 
             
        }

        $stageRoleNamePA = [];
        $stageRoleNamePA = Role::select('role_id','name')->whereIn('role_id', $stageRoleIDPAArr)->get()->toArray(); 
        /** 
         * get the open comment of projects assigned to the user
         */
        $getOpenCommentFromDocumentsPA = array();
        $getOpenCommentFromDocumentsPA = DB::table('thread_comments')
        ->join('threads','threads.thread_id','=','thread_comments.thread_id')
        ->join('projects','projects.document_id','=','thread_comments.thread_source_id')
        ->select('thread_comments.*','projects.project_id')
        ->where('thread_comments.is_deleted',0)
        ->where('threads.status',1)
        ->where('threads.assign_to',$userId)
        ->get()->toArray();

        $getOpenCommentFromItemsPA = array();
        $getOpenCommentFromItemsPA = DB::table('thread_comments')
        ->join('threads','threads.thread_id','=','thread_comments.thread_id')
        ->join('project_items','project_items.item_id','=','thread_comments.thread_source_id')
        ->select('thread_comments.*','project_items.project_id')
        ->where('thread_comments.is_deleted',0)
        ->where('threads.status',1)
        ->where('threads.assign_to',$userId)
        ->get()->toArray();
        
        /**
         * Total comments by the user on which project they were been assigned to
         */
        $totalCommentForProjectPA = array_merge($getOpenCommentFromDocumentsPA,$getOpenCommentFromItemsPA);

        $getCommentProjectIdPA = array();
        foreach($totalCommentForProjectPA as $totalCommentForProjectPAK=>$totalCommentForProjectPAV)
        {
            $getCommentProjectIdPA[] = $totalCommentForProjectPAV->project_id;
        }
        $getUniqueCommentProjectIdPA = array();
        $getUniqueCommentProjectIdPA = array_unique($getCommentProjectIdPA);
        $getProjectCommentCountPA = array();
        foreach($getUniqueCommentProjectIdPA as $getUniqueCommentProjectIdPAK=>$getUniqueCommentProjectIdPAV)
        {
            $getCount = 1;
            foreach($totalCommentForProjectPA as $totalCommentForProjectPAK=>$totalCommentForProjectPAV)
            {
                if($getUniqueCommentProjectIdPAV==$totalCommentForProjectPAV->project_id)
                {
                    $getProjectCommentCountPA[$totalCommentForProjectPAV->project_id] = $getCount;
                    $getCount = $getCount+1;
                }
            }
        }
        /** Get the stage name for the project assigned to a user */
        $projectAndStageNamePA = array();
        $psn=0;
        foreach($projectAssigned as $projectAssignedK=>$projectAssignedV)
        {
            foreach($stageNamePA as $stageNamePAK=>$stageNamePAV)
            {
                if(substr($projectAssignedV->current_workflow_stage_id, strpos($projectAssignedV->current_workflow_stage_id, "||") + 2)==$stageNamePAV['stage_id'])
                {
                    $projectAndStageNamePA[$psn]['project_id'] = $projectAssignedV->project_id; 
                    $projectAndStageNamePA[$psn]['workflow_id'] = $projectAssignedV->workflow_id;
                    $projectAndStageNamePA[$psn]['organization_id'] = $projectAssignedV->organization_id;
                    $projectAndStageNamePA[$psn]['project_type'] = $projectAssignedV->project_type;
                    $projectAndStageNamePA[$psn]['project_name'] = $projectAssignedV->project_name;
                    $projectAndStageNamePA[$psn]['description'] = $projectAssignedV->description;
                    $projectAndStageNamePA[$psn]['document_id'] = $projectAssignedV->document_id;
                    $projectAndStageNamePA[$psn]['current_workflow_stage_id'] = $projectAssignedV->current_workflow_stage_id;
                    $projectAndStageNamePA[$psn]['is_deleted'] = $projectAssignedV->is_deleted;
                    $projectAndStageNamePA[$psn]['created_by'] = $projectAssignedV->created_by;
                    $projectAndStageNamePA[$psn]['updated_by'] = $projectAssignedV->updated_by;
                    $projectAndStageNamePA[$psn]['workflow_stage'] = $stageNamePAV['name'];
                    $psn=$psn+1;
                }
            }    
        }
        /** Get the user detail from the above loop */
        $ProjectStageNameUserDetailPA = array();
        $pd =0;
        foreach($projectAndStageNamePA as $projectAndStageNamePAK=>$projectAndStageNamePAV)
        {
           foreach($assigneeDetail as $assigneeDetailK=>$assigneeDetailV)
            {
                //if($projectAndStageNamePAV['created_by']!=$assigneeDetailV->user_id)
                //{
                    $ProjectStageNameUserDetailPA[$pd]['project_id'] = $projectAndStageNamePAV['project_id'];
                    $ProjectStageNameUserDetailPA[$pd]['workflow_id'] = $projectAndStageNamePAV['workflow_id'];
                    $ProjectStageNameUserDetailPA[$pd]['organization_id'] = $projectAndStageNamePAV['organization_id'];
                    $ProjectStageNameUserDetailPA[$pd]['project_type'] = $projectAndStageNamePAV['project_type'];
                    $ProjectStageNameUserDetailPA[$pd]['project_name'] = $projectAndStageNamePAV['project_name'];
                    $ProjectStageNameUserDetailPA[$pd]['description'] = $projectAndStageNamePAV['description'];
                    $ProjectStageNameUserDetailPA[$pd]['document_id'] = $projectAndStageNamePAV['document_id'];
                    $ProjectStageNameUserDetailPA[$pd]['current_workflow_stage_id'] = $projectAndStageNamePAV['current_workflow_stage_id'];
                    $ProjectStageNameUserDetailPA[$pd]['is_deleted'] = $projectAndStageNamePAV['is_deleted'];
                    $ProjectStageNameUserDetailPA[$pd]['created_by'] = $projectAndStageNamePAV['created_by'];
                    $ProjectStageNameUserDetailPA[$pd]['updated_by'] = $projectAndStageNamePAV['updated_by'];
                    $ProjectStageNameUserDetailPA[$pd]['workflow_stage'] = $projectAndStageNamePAV['workflow_stage'];
                    $ProjectStageNameUserDetailPA[$pd]['created_by_name'] = $assigneeDetailV->first_name.$assigneeDetailV->last_name;
                    $ProjectStageNameUserDetailPA[$pd]['top_contributor'] = $assigneeDetailV->first_name.$assigneeDetailV->last_name;     
                    $pd = $pd+1;
                //}
            }
        }
        /** Get the comments in the project */
        $getCommentsInProjectPA = array();
        $cpa=0;
        foreach($ProjectStageNameUserDetailPA as $ProjectStageNameUserDetailPAK=>$ProjectStageNameUserDetailPAV)
        {
            foreach($getProjectCommentCountPA as  $getProjectCommentCountPAK=> $getProjectCommentCountPAV)
            {
                if($ProjectStageNameUserDetailPAV['project_id']==$getProjectCommentCountPAK)
                {
                    $getCommentsInProjectPA[$cpa]['project_id'] = $ProjectStageNameUserDetailPAV['project_id'];
                    $getCommentsInProjectPA[$cpa]['workflow_id'] = $ProjectStageNameUserDetailPAV['workflow_id'];
                    $getCommentsInProjectPA[$cpa]['organization_id'] = $ProjectStageNameUserDetailPAV['organization_id'];
                    $getCommentsInProjectPA[$cpa]['project_type'] = $ProjectStageNameUserDetailPAV['project_type'];
                    $getCommentsInProjectPA[$cpa]['project_name'] = $ProjectStageNameUserDetailPAV['project_name'];
                    $getCommentsInProjectPA[$cpa]['description'] = $ProjectStageNameUserDetailPAV['description'];
                    $getCommentsInProjectPA[$cpa]['document_id'] = $ProjectStageNameUserDetailPAV['document_id'];
                    $getCommentsInProjectPA[$cpa]['current_workflow_stage_id'] = $ProjectStageNameUserDetailPAV['current_workflow_stage_id'];
                    $getCommentsInProjectPA[$cpa]['is_deleted'] = $ProjectStageNameUserDetailPAV['is_deleted'];
                    $getCommentsInProjectPA[$cpa]['created_by'] = $ProjectStageNameUserDetailPAV['created_by'];
                    $getCommentsInProjectPA[$cpa]['updated_by'] = $ProjectStageNameUserDetailPAV['updated_by'];
                    $getCommentsInProjectPA[$cpa]['workflow_stage'] = $ProjectStageNameUserDetailPAV['workflow_stage'];
                    $getCommentsInProjectPA[$cpa]['created_by_name'] = $ProjectStageNameUserDetailPAV['created_by_name'];
                    $getCommentsInProjectPA[$cpa]['top_contributor'] = $ProjectStageNameUserDetailPAV['top_contributor'];  
                    $getCommentsInProjectPA[$cpa]['issue'] = $getProjectCommentCountPAV;    
                    $cpa = $cpa+1;
                }
            }    
        }
        $projectIdsWithCommentInPA = array_column($getCommentsInProjectPA,'project_id');
        $projectIdsTotalInPA = array_column($ProjectStageNameUserDetailPA,'project_id');
        $projectIdsWithoutCommentInPA = array();
        $projectIdsWithoutCommentInPA = array_diff($projectIdsTotalInPA,$projectIdsWithCommentInPA);

        $projectsWithoutCommentDataPA = array();
        if(!empty($projectIdsWithoutCommentInPA))
        {
            $pwtc =0;
            foreach($projectIdsWithoutCommentInPA as $projectIdsWithoutCommentInPAK=>$projectIdsWithoutCommentInPAV)
            {
                foreach($ProjectStageNameUserDetailPA as $ProjectStageNameUserDetailPAK=>$ProjectStageNameUserDetailPAV)
                {
                    if($projectIdsWithoutCommentInPAV==$ProjectStageNameUserDetailPAV['project_id'])
                    {
                        $projectsWithoutCommentDataPA[$pwtc]['project_id'] = $ProjectStageNameUserDetailPAV['project_id'];
                        $projectsWithoutCommentDataPA[$pwtc]['workflow_id'] = $ProjectStageNameUserDetailPAV['workflow_id'];
                        $projectsWithoutCommentDataPA[$pwtc]['organization_id'] = $ProjectStageNameUserDetailPAV['organization_id'];
                        $projectsWithoutCommentDataPA[$pwtc]['project_type'] = $ProjectStageNameUserDetailPAV['project_type'];
                        $projectsWithoutCommentDataPA[$pwtc]['project_name'] = $ProjectStageNameUserDetailPAV['project_name'];
                        $projectsWithoutCommentDataPA[$pwtc]['description'] = $ProjectStageNameUserDetailPAV['description'];
                        $projectsWithoutCommentDataPA[$pwtc]['document_id'] = $ProjectStageNameUserDetailPAV['document_id'];
                        $projectsWithoutCommentDataPA[$pwtc]['current_workflow_stage_id'] = $ProjectStageNameUserDetailPAV['current_workflow_stage_id'];
                        $projectsWithoutCommentDataPA[$pwtc]['is_deleted'] = $ProjectStageNameUserDetailPAV['is_deleted'];
                        $projectsWithoutCommentDataPA[$pwtc]['created_by'] = $ProjectStageNameUserDetailPAV['created_by'];
                        $projectsWithoutCommentDataPA[$pwtc]['updated_by'] = $ProjectStageNameUserDetailPAV['updated_by'];
                        $projectsWithoutCommentDataPA[$pwtc]['workflow_stage'] = $ProjectStageNameUserDetailPAV['workflow_stage'];
                        $projectsWithoutCommentDataPA[$pwtc]['created_by_name'] = $ProjectStageNameUserDetailPAV['created_by_name'];
                        $projectsWithoutCommentDataPA[$pwtc]['top_contributor'] = $ProjectStageNameUserDetailPAV['top_contributor'];  
                        $projectsWithoutCommentDataPA[$pwtc]['issue'] = 0;  
                        $pwtc = $pwtc+1;  
                    }
                }
            }
        }

         /** Get all the projects assigned to the user with and without comment */
         $getAllTheProjectCreatedPA = array();
         $getAllTheProjectCreatedPA = array_merge($getCommentsInProjectPA,$projectsWithoutCommentDataPA);
 
         $getRoleInPA = DB::table('project_users')
             ->join('projects', 'projects.project_id','=','project_users.project_id')
             ->select('project_users.workflow_stage_role_id','project_users.project_id')
             ->where('projects.is_deleted',0)
             ->where('project_users.is_deleted',0)
             ->whereIn('project_users.project_id',$projectIdsTotalInPA)
             ->get()
             ->toArray();
             $getDataPA = array();
             foreach($getRoleInPA as $getRoleInPAV)
             {
                 $getDataPA[] = ['role_id'=>substr($getRoleInPAV->workflow_stage_role_id,-36),'project_id'=>$getRoleInPAV->project_id];    
             }
              $roleIdPAs = array_column($getDataPA,'role_id');
              $projectIdPAs = array_column($getDataPA,'project_id'); 
              $roleNamePA = Role::select('role_id','name')->whereIn('role_id',$roleIdPAs)->get();
 
                 if($roleNamePA)
                 {
                     $dataPA = $roleNamePA->toArray();
                 }
 
                 $getProjectRelatedDataPA = array();
                 foreach($getDataPA as $getDataPAK=>$getDataPAV)
                 {
                    foreach($dataPA as $dataPAK=>$dataPAV)
                    {
                         if($getDataPAV['role_id']==$dataPAV['role_id'])
                         {
                             $getDataPAV['name'] = $dataPAV['name'];
                             $getProjectRelatedDataPA[]=$getDataPAV;
                         }
                    } 
                 }
                 $mapProjectsWithRoleIdAndRoleNamePA = array();
                 
                     foreach($getProjectRelatedDataPA as $getProjectRelatedDataPAKey=>$getProjectRelatedDataPAValue)
                     {
                         
                         
                             $mapProjectsWithRoleIdAndRoleNamePA[$getProjectRelatedDataPAValue['project_id']][]=
                             [
                             'role_id'=>$getProjectRelatedDataPAValue['role_id'],
                             'name'=>$getProjectRelatedDataPAValue['name']
                             ];
                         
                     }
 
                     $addTheRoleNameAssocitedWithProjectAssigned = array();
                     $userRoleProjectIdPA = array();
                     foreach($mapProjectsWithRoleIdAndRoleNamePA as $mapProjectsWithRoleIdAndRoleNamePAK=>$mapProjectsWithRoleIdAndRoleNamePAV)
                     {
                         $addTheRoleNameAssocitedWithProjectAssigned[$mapProjectsWithRoleIdAndRoleNamePAK] = implode(",",array_column($mapProjectsWithRoleIdAndRoleNamePAV,'name'));
                         $userRoleProjectIdPA[] = $mapProjectsWithRoleIdAndRoleNamePAK;
                     
                     }

                      /** Projects with role */
                    $getAllTheProjectCreatedWithRoleNamePA = array();
                    $pcrn=0;
                    foreach($getAllTheProjectCreatedPA as $getAllTheProjectCreatedPAK=>$getAllTheProjectCreatedPAV)
                    {
                        foreach($mapProjectsWithRoleIdAndRoleNamePA as $mapProjectsWithRoleIdAndRoleNamePAK=>$mapProjectsWithRoleIdAndRoleNamePAV)
                        {
                            if($getAllTheProjectCreatedPAV['project_id']==$mapProjectsWithRoleIdAndRoleNamePAK)
                            {
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['project_id'] = $getAllTheProjectCreatedPAV['project_id'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['workflow_id'] = $getAllTheProjectCreatedPAV['workflow_id'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['organization_id'] = $getAllTheProjectCreatedPAV['organization_id'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['project_type'] = $getAllTheProjectCreatedPAV['project_type'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['project_name'] = $getAllTheProjectCreatedPAV['project_name'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['description'] = $getAllTheProjectCreatedPAV['description'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['document_id'] = $getAllTheProjectCreatedPAV['document_id'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['current_workflow_stage_id'] = $getAllTheProjectCreatedPAV['current_workflow_stage_id'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['is_deleted'] = $getAllTheProjectCreatedPAV['is_deleted'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['created_by'] = $getAllTheProjectCreatedPAV['created_by'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['updated_by'] = $getAllTheProjectCreatedPAV['updated_by'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['workflow_stage'] = $getAllTheProjectCreatedPAV['workflow_stage'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['created_by_name'] = $getAllTheProjectCreatedPAV['created_by_name'];
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['top_contributor'] = $getAllTheProjectCreatedPAV['top_contributor'];  
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['issue'] = $getAllTheProjectCreatedPAV['issue'];  
                                $getAllTheProjectCreatedWithRoleNamePA[$pcrn]['assigned_role'] = $mapProjectsWithRoleIdAndRoleNamePAV;
                                $pcrn = $pcrn+1;  
                            }
                        }
                    }

                    /** Projects without role */
                    $projectIdsWithoutRoleNamePA = array();
                    $projectIdsWithoutRoleNamePA = array_diff($projectIdsTotalInPA,$userRoleProjectIdPA);


                    $projectsWithoutRoleDataPA = array();

                    if(!empty($projectIdsWithoutRoleNamePA))
                    {
                        $pwtrn =0;
                        foreach($projectIdsWithoutRoleNamePA as $projectIdsWithoutRoleNamePAK=>$projectIdsWithoutRoleNamePAV)
                        {
                            foreach($getAllTheProjectCreatedPA as $getAllTheProjectCreatedPAK=>$getAllTheProjectCreatedPAV)
                            {
                                if($projectIdsWithoutRoleNamePAV==$getAllTheProjectCreatedPAV['project_id'])
                                {
                                    $projectsWithoutRoleDataPA[$pwtrn]['project_id'] = $getAllTheProjectCreatedPAV['project_id'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['workflow_id'] = $getAllTheProjectCreatedPAV['workflow_id'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['organization_id'] = $getAllTheProjectCreatedPAV['organization_id'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['project_type'] = $getAllTheProjectCreatedPAV['project_type'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['project_name'] = $getAllTheProjectCreatedPAV['project_name'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['description'] = $getAllTheProjectCreatedPAV['description'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['document_id'] = $getAllTheProjectCreatedPAV['document_id'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['current_workflow_stage_id'] = $getAllTheProjectCreatedPAV['current_workflow_stage_id'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['is_deleted'] = $getAllTheProjectCreatedPAV['is_deleted'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['created_by'] = $getAllTheProjectCreatedPAV['created_by'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['updated_by'] = $getAllTheProjectCreatedPAV['updated_by'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['workflow_stage'] = $getAllTheProjectCreatedPAV['workflow_stage'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['created_by_name'] = $getAllTheProjectCreatedPAV['created_by_name'];
                                    $projectsWithoutRoleDataPA[$pwtrn]['top_contributor'] = $getAllTheProjectCreatedPAV['top_contributor'];  
                                    $projectsWithoutRoleDataPA[$pwtrn]['issue'] = 0;
                                    $projectsWithoutRoleDataPA[$pwtrn]['assigned_role'] = [];  
                                    $pwtrn = $pwtrn+1;  
                                }
                            }
                        }
                    }

                    $finalDataOfAllTheProjectAssigned = array();
                    $finalDataOfAllTheProjectAssigned = array_merge($getAllTheProjectCreatedWithRoleNamePA,$projectsWithoutRoleDataPA);
                    $totalProjectList = array();
                    $totalProjectList=array_merge($finalDataOfAllTheProjectCreated,$finalDataOfAllTheProjectAssigned);
                    return $this->setCountMetricsData($totalProjectList);
    }
}
