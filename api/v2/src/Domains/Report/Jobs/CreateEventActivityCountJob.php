<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

class CreateEventActivityCountJob extends Job
{
    private $activityLogs;
    private $eventTypeValues;

    private $projectCreateEventTypeText;
    private $projectDeleteEventTypeText;
    private $itemCreatedUnderProjectEventTypeText;
    private $itemDeletedUnderProjectEventTypeText;

    private $projectCreatedCount;
    private $projectDeletedCount;
    private $taxonomyNodesAddedCount;
    private $taxonomyNodesDeletedCount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $activityLogs)
    {
        $this->setActivityLogs($activityLogs);
        $this->setEventTypeValues();

        $this->projectCreateEventTypeText = "PROJECT_CREATED";
        $this->projectDeleteEventTypeText = "PROJECT_DELETED";
        $this->itemCreatedUnderProjectEventTypeText = "ITEM_CREATED_UNDER_PROJECT";
        $this->itemDeletedUnderProjectEventTypeText = "ITEM_DELETED_UNDER_PROJECT";
    }

    public function setActivityLogs(Collection $data) {
        $this->activityLogs = $data;
    }

    public function getActivityLogs(): Collection {
        return $this->activityLogs;
    }

    public function setEventTypeValues() {
        $this->eventTypeValues = config("event_activity.event_type_value");
    }

    public function getEventTypeValues(): array {
        return $this->eventTypeValues;
    }

    public function setProjectCreatedCount(int $data) {
        $this->projectCreatedCount = $data;
    }

    public function getProjectCreatedCount(): int {
        return $this->projectCreatedCount;
    }

    public function setProjectDeletedCount(int $data) {
        $this->projectDeletedCount = $data;
    }

    public function getProjectDeletedCount(): int {
        return $this->projectDeletedCount;
    }

    public function setTaxonomyNodesAddedCount(int $data) {
        $this->taxonomyNodesAddedCount = $data;
    }

    public function getTaxonomyNodesAddedCount(): int {
        return $this->taxonomyNodesAddedCount;
    }

    public function setTaxonomyNodesDeletedCount(int $data) {
        $this->taxonomyNodesDeletedCount = $data;
    }

    public function getTaxonomyNodesDeletedCount(): int {
        return $this->taxonomyNodesDeletedCount;
    }

    public function setCountMetricsData(array $data) {
        $this->countMetricsData = $data;
    }

    public function getCountMetricsData(): array {
        return $this->countMetricsData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // parse and set various count metrices
        $this->parseAndSetCountMetrices();

        // prepare and set various
        $this->prepareCountMetricsData();

        // retrun the response data
        return $this->getCountMetricsData();
    }

    private function parseAndSetCountMetrices() {
        $activityLogs = $this->getActivityLogs();
        $this->parseAndSetNewProjectCreatedCount($activityLogs);
        $this->parseAndSetProjectDeletedCount($activityLogs);
        $this->parseAndSetNewTaxonomyNodesAddedCount($activityLogs);
        $this->parseAndSetNewTaxonomyNodesDeletedCount($activityLogs);
    }

    private function parseAndSetNewProjectCreatedCount(Collection $activityLogs) {
        $count = $this->getActivityEventTypeSpecificCountHelper($activityLogs, $this->projectCreateEventTypeText);
        $this->setProjectCreatedCount($count);
    }

    private function parseAndSetProjectDeletedCount(Collection $activityLogs) {
        $count = $this->getActivityEventTypeSpecificCountHelper($activityLogs, $this->projectDeleteEventTypeText);
        $this->setProjectDeletedCount($count);
    }

    private function parseAndSetNewTaxonomyNodesAddedCount(Collection $activityLogs) {
        $count = $this->getActivityEventTypeSpecificCountHelper($activityLogs, $this->itemCreatedUnderProjectEventTypeText);
        $this->setTaxonomyNodesAddedCount($count);
    }

    private function parseAndSetNewTaxonomyNodesDeletedCount(Collection $activityLogs) {
        $count = $this->getActivityEventTypeSpecificCountHelper($activityLogs, $this->itemDeletedUnderProjectEventTypeText);
        $this->setTaxonomyNodesDeletedCount($count);
    }

    private function prepareCountMetricsData() {
        $data = [
            "projects_created" => $this->getProjectCreatedCount(),
            "projects_deleted" => $this->getProjectDeletedCount(),
            "taxonomy_nodes_added" => $this->getTaxonomyNodesAddedCount(),
            "taxonomy_nodes_removed" => $this->getTaxonomyNodesDeletedCount()
        ];
        $this->setCountMetricsData($data);
    }

    private function getActivityEventTypeSpecificCountHelper(Collection $activityLogs, string $activityEventTypeText): int {
        $eventTypeValues = $this->getEventTypeValues();
        $activityEventTypeValue = (integer)$eventTypeValues[$activityEventTypeText];
        $count = $activityLogs->whereStrict('activity_event_type', $activityEventTypeValue)->count();
        return $count;
    }
}
