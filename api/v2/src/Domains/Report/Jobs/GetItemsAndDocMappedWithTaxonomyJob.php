<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetItemsAndDocMappedWithTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {
        $this->data = $documentIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemComplianceReport)
    {
        $documentIdentifier = $this->data;
        $this->itemComplianceReport = $itemComplianceReport;

        return $this->itemComplianceReport->getItemsAndDocMappedWithTaxonomyDetails($documentIdentifier);
    }
}
