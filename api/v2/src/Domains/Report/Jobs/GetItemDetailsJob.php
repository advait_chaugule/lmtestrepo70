<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetItemDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {
        $this->data = $documentIdentifier;
        $this->setDocumentIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $documentIdentifier = $this->data;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getItemDetails($documentIdentifier);

    }

    public function setDocumentIdentifier(string $identifier) {
        $this->documentIdentifier = $identifier; 
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

}
