<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class GetCommentReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData , array $input)
    {
        $this->data = $requestData;
        $this->setUserIdentifier($this->data);

        $this->projectId = $input;
        $this->setProjectIdentifier($this->projectId);
    }

    public function setUserIdentifier(array $requestData) {
        $this->requestData = $requestData; 
    }

    public function getUserIdentifier() {
        return $this->requestData;
    }

    public function setProjectIdentifier(array $input) {
        $this->input = $input; 
    }

    public function getProjectIdentifier() {
        return $this->input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $commentReport)
    {
        $userData = $this->requestData;
        $projectId = $this->input;
        $this->commentReport = $commentReport;

        return $this->commentReport->getCommentReport($userData,$projectId);
    }
}
