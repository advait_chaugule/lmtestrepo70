<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class NodeIdMappedWithNodeNameJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier , string $organizationIdentifier)
    {
        $this->data = $documentIdentifier;
        $this->setDocumentIdentifier($this->data);

        $this->organizationData = $organizationIdentifier;
        $this->setOrganizationIdentifier($this->organizationData);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $documentIdentifier = $this->data;
        $organizationIdentifier = $this->organizationIdentifier;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getNodeIdMappedWithNodeName($documentIdentifier,$organizationIdentifier);
    }

    public function setDocumentIdentifier(string $identifier) {
        $this->documentIdentifier = $identifier; 
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier(string $organizationID) {
        $this->organizationIdentifier = $organizationID; 
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

}
