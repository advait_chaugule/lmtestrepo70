<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use Carbon\Carbon;
use \DateTime;
use \DateInterval;
use \DatePeriod;

class CreateGraphDataForUserSessionJob extends Job
{
    private $activityLogs;
    private $calendarGroupByType;
    private $activityLogStartDate;
    private $activityLogEndDate;
    private $defaultCalendarGroupByType;
    private $loginActivityTypeText;
    private $userSessionRefreshActivityTypeText;
    private $loginActivityTypeValue;
    private $userSessionRefreshActivityTypeValue;
    private $activityLogsGroupedByDate;
    private $dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions;
    private $dateWiseUserSessionGraphData;
    private $userSessionGraphData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $activityLogs, array $requestInputs)
    {
        $this->defaultCalendarGroupByType = "day";

        $this->setActivityLogs($activityLogs);
        $this->setCalendarGroupByType($requestInputs);
        $this->setActivityLogStartDate($requestInputs);
        $this->setActivityLogEndDate($requestInputs);

        $this->loginActivityTypeText = "USER_LOGIN";
        $this->userSessionRefreshActivityTypeText = "USER_SESSION_REFRESHED";

        $this->setLoginActivityTypeValue();
        $this->setUserSessionRefreshActivityTypeValue();
    }

    public function setActivityLogs(Collection $data) {
        $this->activityLogs = $data;
    }

    public function getActivityLogs(): Collection {
        return $this->activityLogs;
    }

    public function setCalendarGroupByType(array $data) {
        $this->calendarGroupByType = !empty($data["calendar_group_by_type"]) ? $data["calendar_group_by_type"] : $this->defaultCalendarGroupByType;
    }

    public function getCalendarGroupByType(): string {
        return $this->calendarGroupByType;
    }

    public function setActivityLogStartDate(array $data) {
        $this->activityLogStartDate = $data["activity_log_start_date"];
    }

    public function getActivityLogStartDate(): string {
        return $this->activityLogStartDate;
    }

    public function setActivityLogEndDate(array $data) {
        $this->activityLogEndDate = $data["activity_log_end_date"];
    }

    public function getActivityLogEndDate(): string {
        return $this->activityLogEndDate;
    }

    public function setLoginActivityTypeValue() {
        $this->loginActivityTypeValue = $this->helperToSearchAndReturnActivityTypeValue($this->loginActivityTypeText);
    }

    public function getLoginActivityTypeValue(): int {
        return $this->loginActivityTypeValue;
    }

    public function setUserSessionRefreshActivityTypeValue() {
        $this->userSessionRefreshActivityTypeValue = $this->helperToSearchAndReturnActivityTypeValue($this->userSessionRefreshActivityTypeText);
    }

    public function getUserSessionRefreshActivityTypeValue(): int {
        return $this->userSessionRefreshActivityTypeValue;
    }

    public function setActivityLogsGroupedByDate(Collection $data) {
        $this->activityLogsGroupedByDate = $data;
    }

    public function getActivityLogsGroupedByDate(): Collection {
        return $this->activityLogsGroupedByDate;
    }

    public function setDateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions(Collection $data) {
        $this->dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions = $data;
    }

    public function getDateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions(): Collection {
        return $this->dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions;
    }

    public function setUserSessionGraphDataBasedOnDate(Collection $data) {
        $this->dateWiseUserSessionGraphData = $data;
    }

    public function getUserSessionGraphDataBasedOnDate(): Collection {
        return $this->dateWiseUserSessionGraphData;
    }

    public function setUserSessionGraphData(array $data) {
        $this->userSessionGraphData = $data;
    }

    public function getUserSessionGraphData(): array {
        return $this->userSessionGraphData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // group activities by log date and set them
        $this->groupActivitiesByLogDate();
        // separate the date wise grouped activities by relevant event types viz. login and user session refresh
        $this->segregateActivityLogsByLoginAndUserSessionRefreshActions();
        // calculate and set user session graph data based on date
        $this->calculateUserSessionGraphDataBasedOnDate();
        // calculate user session graph data based on calendar group by type
        $this->calculateUserSessionGraphDataBasedOnCalendarGroupByType();
        // return the calculated data points for user session data
        return $this->getUserSessionGraphData();
    }

    private function groupActivitiesByLogDate() {
        $activityLogs = $this->getActivityLogs();
        $activityLogsGroupedbyDate = $activityLogs->groupBy(function($item) {
             return substr($item->activity_log_timestamp, 0, 10);
        });
        $this->setActivityLogsGroupedByDate($activityLogsGroupedbyDate);
    }

    private function segregateActivityLogsByLoginAndUserSessionRefreshActions() {
        $activityLogsGroupedByDate = $this->getActivityLogsGroupedByDate();
        $loginActivityTypeValue = $this->getLoginActivityTypeValue();
        $userSessionRefreshActivityTypeValue = $this->getUserSessionRefreshActivityTypeValue();
        $dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions = [];
        $activityLogsGroupedByDate->each(function ($activityLogsGroup, $date)
                                         use($loginActivityTypeValue, $userSessionRefreshActivityTypeValue, &$dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions) {
            $sortedLoginActivityLogs = $activityLogsGroup
                                        ->where("activity_event_type", $loginActivityTypeValue)
                                        ->sortBy("activity_log_timestamp")
                                        ->values()
                                        ->groupBy("user_id");
            $sortedUserSessionRefreshActivityLogs = $activityLogsGroup
                                        ->where("activity_event_type", $userSessionRefreshActivityTypeValue)
                                        ->sortBy("activity_log_timestamp")
                                        ->values()
                                        ->groupBy("user_id");
            $dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions[$date] = [
                "user_specific_login_activities" => $sortedLoginActivityLogs,
                "user_specific_session_refresh_activities" => $sortedUserSessionRefreshActivityLogs,
            ];
        });
        $this->setDateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions(
            collect($dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions)
        );
    }

    private function calculateUserSessionGraphDataBasedOnDate() {
        $dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions = $this->getDateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions();
        $userSessionGraphData = [];
        // iterate through datewise actvities
        $dateWiseActivityLogsSegregatedByLoginAndUserSessionRefreshActions->each(function($activityLogsGroup, $date) use(&$userSessionGraphData) {
            $userSpecificLoginActivities = $activityLogsGroup["user_specific_login_activities"];
            $userSpecificSessionRefreshActivities = $activityLogsGroup["user_specific_session_refresh_activities"];
            $userSessionGraphData[] = [
                "date" => $date,
                "day" => Carbon::parse($date)->format("l"),
                "user_visits" => $this->helperToCalculateUserVisitCount($userSpecificLoginActivities),
                "time_spent" => $this->helperToCalculateDailyTimeSpent($userSpecificSessionRefreshActivities)
            ];
        });
        $this->setUserSessionGraphDataBasedOnDate(collect($userSessionGraphData));
    }

    private function calculateUserSessionGraphDataBasedOnCalendarGroupByType() {
        $userSessionGraphDataBasedOnDate = $this->getUserSessionGraphDataBasedOnDate();
        $calendarGroupByType = $this->getCalendarGroupByType();
        $userSessionGraphData = $this->helperToCalculateSessionGraphDataByCalendarGroupType($userSessionGraphDataBasedOnDate, $calendarGroupByType);
        $this->setUserSessionGraphData($userSessionGraphData);
    }

    private function helperToSearchAndReturnActivityTypeValue(string $activityType): int {
        $activityTypeValueMapping = config("event_activity.event_type_value");
        $activityTypeValue = $activityTypeValueMapping[$activityType];
        return $activityTypeValue;
    }

    private function helperToCalculateUserVisitCount(Collection $userSpecificLoginActivities): int {
        $visitCount = 0;
        foreach($userSpecificLoginActivities as $userId => $loginActivities) {
            $visitCount += $loginActivities->count();
        }
        return $visitCount;
    }

    private function helperToCalculateDailyTimeSpent(Collection $userSpecificSessionRefreshActivities): array {
        $configuredUserSessionTimeoutInMinutes = config("session.access_token_timeout_in_minutes");
        $configuredSessionTimeOutInSeconds = $configuredUserSessionTimeoutInMinutes * 60;
        $timeSpentInSeconds = 0;
        $userSpecificSessionRefreshActivities->each(function($sessionActivitiesOfSingleUser, $userId) 
                                                    use(&$timeSpentInSeconds, $configuredSessionTimeOutInSeconds) {
            $sessionActivitiesArray = $sessionActivitiesOfSingleUser->toArray();
            foreach($sessionActivitiesArray as $currentIndex=>$sessionActivity) {
                $currentActivityTimestamp = Carbon::parse($sessionActivity["activity_log_timestamp"]);
                $nextActivityIndex = $currentIndex + 1;
                if(!empty($sessionActivitiesArray[$nextActivityIndex]["activity_log_timestamp"])) {
                    $nextActivityTimestamp = Carbon::parse($sessionActivitiesArray[$nextActivityIndex]["activity_log_timestamp"]);
                    $differenceInSeconds = $currentActivityTimestamp->diffInSeconds($nextActivityTimestamp);
                    if($differenceInSeconds <= $configuredSessionTimeOutInSeconds){
                        $timeSpentInSeconds += $differenceInSeconds;
                    }
                }
            }
        });
        $timeSpentInMinutes = $timeSpentInSeconds > 0 ? floor($timeSpentInSeconds/60) : 0;
        $timeSpentInHours = $timeSpentInMinutes > 0 ? floor($timeSpentInMinutes/60) : 0;
        return [
            "seconds" => $timeSpentInSeconds,
            "minutes" => $timeSpentInMinutes,
            "hours" => $timeSpentInHours
        ];
    }

    private function helperToCalculateSessionGraphDataByCalendarGroupType(
        Collection $userSessionGraphDataBasedOnDate, string $calendarGroupByType
    ): array {

        if($calendarGroupByType==="day") {
            return $userSessionGraphDataBasedOnDate->toArray();
        }
        else {
            $activityLogStartDate = new DateTime( $this->getActivityLogStartDate() ); // convert the start date time to DateTimeobject
            $activityLogEndDate = new DateTime( $this->getActivityLogEndDate() ); // convert the end date time to DateTimeobject
            $activityLogEndDate = $activityLogEndDate->modify( '+1 day' ); // call modify method to include the end date itself
            $iso8601IntervalFormat = $calendarGroupByType==="week" ? "P7D" : "P1M";
            $interval = new DateInterval($iso8601IntervalFormat); // set interval for month/week in ISO 8601 format
            $dateRange = new DatePeriod($activityLogStartDate, $interval ,$activityLogEndDate); // get date range separated by 7 days
    
            // this had to be done since in DatePeriod class object while iterating through the range, 
            // getting next date based on index calculation is not working
            $calendarGroupByTypeDates = [];
            foreach($dateRange as $currentIndex => $dateObject){
                $calendarGroupByTypeDates[$currentIndex] = $dateObject->format("Y-m-d");
            }
    
            $sessionGraphData = [];
            foreach($calendarGroupByTypeDates as $currentIndex => $currentDate) {
                $nextIndex = $currentIndex + 1;
                $endDate = !empty($calendarGroupByTypeDates[$nextIndex]) ? $calendarGroupByTypeDates[$nextIndex] : $activityLogEndDate->format("Y-m-d");
                // filter collection based on calculated date range
                $filteredSessionDataCollection =  $userSessionGraphDataBasedOnDate->filter(function($item, $key) use($currentDate, $endDate){
                                                return $item["date"] >= $currentDate && $item["date"] <= $endDate;
                                          });
                if($filteredSessionDataCollection->isNotEmpty()) {
                    $userVisits = $filteredSessionDataCollection->sum("user_visits");
                    $timeSpentInSeconds = $filteredSessionDataCollection->sum("time_spent.seconds");
                    $timeSpentInMinutes = $filteredSessionDataCollection->sum("time_spent.minutes");
                    $timeSpentInHours = $filteredSessionDataCollection->sum("time_spent.hours");

                    $date = new DateTime( $currentDate );
                    $month = $date->format('F');
    
                    $sessionGraphData[] = [
                        $calendarGroupByType => $nextIndex,
                        "start_date" => $currentDate,
                        "end_date" => $endDate,
                        "month_name" => $month,
                        "user_visits" => $userVisits,
                        "time_spent" => [
                            "seconds" => $timeSpentInSeconds,
                            "minutes" => $timeSpentInMinutes,
                            "hours" => $timeSpentInHours
                        ],
                    ];
                }
            }
    
            return $sessionGraphData;
        }
    }
}
