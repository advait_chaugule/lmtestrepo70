<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetMetadataDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $metadata)
    {
        $this->data = $metadata;
        $this->setMetadataIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $metadata = $this->data;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getMetadataDetails($metadata);
    }

    public function setMetadataIdentifier(array $identifier) {
        $this->metadata = $identifier; 
    }

    public function getMetadataIdentifier() {
        return $this->metadata;
    }
}
