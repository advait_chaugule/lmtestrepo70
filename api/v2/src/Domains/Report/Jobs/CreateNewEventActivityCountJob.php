<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;
use App\Data\Models\ThreadComment;
use App\Data\Models\Document;
use App\Data\Models\Thread;
use App\Data\Models\Project;
use DB;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class CreateNewEventActivityCountJob extends Job
{
    private $projectCreatedCount;
    private $commentCreatedCount;
    private $documentCreatedCount;
    private $commentAsignedCount;
    private $threadComment;
    private $projectAssignedCount;
    private $publishedTaxonomies;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->setData($data);
    }
    
    public function getProjectCreatedCount(): int {
        return $this->projectCreatedCount;
    }
    
    public function setProjectCreatedCount(int $data) {
        $this->projectCreatedCount = $data;
    }
    public function setCountMetricsData(array $data) {
        $this->countMetricsData = $data;
    }

    public function getCountMetricsData(): array {
        return $this->countMetricsData;
    }

    public function getDocumentCreatedCount(): int {
        return $this->documentCreatedCount;
    }
    
    public function setDocumentCreatedCount(int $data) {
        $this->documentCreatedCount = $data;
    }
    public function getCommentCreatedCount(): int {
        return $this->commentCreatedCount;
    }
    
    public function setCommentCreatedCount(int $data) {
        $this->commentCreatedCount = $data;
    }

    public function getDocumentPublishedCount(): int {
        return $this->publishedTaxonomies;
    }
    
    public function setDocumentPublishedCount(int $data) {
        $this->publishedTaxonomies = $data;
    }

    public function getCommentAssignedCount(): int {
        return $this->commentAsignedCount;
    }
    
    public function setCommentAssignedCount(int $data) {
        $this->commentAsignedCount = $data;
    }

    public function getProjectAssignedCount(): int {
        return $this->projectAssignedCount;
    }
    
    public function setProjectAssignedCount(int $data) {
        $this->projectAssignedCount = $data;
    }

    public function setData(array $data) {
        $this->data = $data;
    }

    public function getData(): array {
        return $this->data;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadComment)
    {
        // parse and set various count metrices
        $this->parseAndSetCountMetrices();
        $this->threadComment = $threadComment;
        // prepare and set various
        $this->prepareCountMetricsData();

        // retrun the response data
        return $this->getCountMetricsData();
    }

    private function parseAndSetCountMetrices() {
        $data = $this->getData();
        $this->getAllComment($data);
        $this->getAllTaxonomyCreated($data);
        $this->getAllTaxonomyPublished($data);
        $this->commentsAssigned($data);
        $this->projectAssigned($data);
    }

    private function getAllComment($data){
        $userId =  $data['auth_user']['user_id'];
        $organization = $data['auth_user']['organization_id'];

        $getProjectDocumentId = array();
        $getProjectDocumentId = DB::table('projects')
                            ->select('document_id')
                            ->where('created_by', $userId)
                            ->where('organization_id',$organization)
                            ->where('is_deleted',0)->get()->toArray();
        
        $documentIdArrayLists = array();                            
        foreach($getProjectDocumentId as $getProjectDocumentIdsK=>$getProjectDocumentIdsV)
        {
            $documentIdArrayLists[]=$getProjectDocumentIdsV->document_id;
        }
        
        $getItemIdsOfAllProjects = array();
        $getItemIdsOfAllProjects = DB::table('items')
                            ->select('item_id')
                            ->whereIn('document_id', $documentIdArrayLists)
                            ->where('organization_id',$organization)
                            ->where('is_deleted',0)->get()->toArray();

        $itemIdArrayList = array();                            
        foreach($getItemIdsOfAllProjects as $getItemIdsOfAllProjectK=>$getItemIdsOfAllProjectV)
        {
            $itemIdArrayList[]=$getItemIdsOfAllProjectV->item_id;
        }                            

        $documentIdAndItemIdArrayLists = array();
        $documentIdAndItemIdArrayLists = array_merge($documentIdArrayLists,$itemIdArrayList);                            
        
        $getThreadSourceId = array();
        $getThreadSourceId = DB::table('thread_comments')
        ->select('thread_source_id')
        ->where('created_by', $userId)
        ->where('organization_id',$organization)
        ->where('is_deleted','0')
        ->whereIn('thread_source_id',$documentIdAndItemIdArrayLists)
        ->get()
        ->toArray();
        
        $getThreadSourceIdArrays = array();
        foreach($getThreadSourceId as $getThreadSourceIdK=>$getThreadSourceIdV)
        {
            $getThreadSourceIdArrays[]=$getThreadSourceIdV->thread_source_id;
        } 
        
        $getOpenCommentDatas = array();
        $getOpenCommentDatas = DB::table('threads')
        ->select('status')
        ->where('organization_id',$organization)
        ->where('is_deleted','0')
        ->where('status','1')
        ->whereIn('thread_source_id',$getThreadSourceIdArrays)
        ->get()
        ->toArray();
        
        $openCounts = count($getOpenCommentDatas);

        if(empty($openCounts)){
            $openCounts = 0;
        }
        
        $this->setCommentCreatedCount($openCounts);

    }
    public function getAllTaxonomyCreated($data){
        $userId =  $data['auth_user']['user_id'];
        $organization = $data['auth_user']['organization_id'];
        $getDocument = Document::where('created_by', $userId)->where('organization_id',$organization)->where('document_type',1)->where('is_deleted',0)->get();      
        $documents = $getDocument->count();
        if(empty($documents)){
            $documents = 0;
        }
        $this->setDocumentCreatedCount($documents);
    }

    public function getAllTaxonomyPublished($data){
        $userId =  $data['auth_user']['user_id'];
        $organization = $data['auth_user']['organization_id'];
        $getPublishedDocument = Document::where('updated_by', $userId)->where('organization_id',$organization)->where('is_deleted',0)->where('status',3)->get();      
        $published = $getPublishedDocument->count();
        if(empty($published)){
            $published = 0;
        }
        $this->setDocumentPublishedCount($published);
    }

    public function commentsAssigned($data){
        $userId =  $data['auth_user']['user_id'];
        $organization = $data['auth_user']['organization_id'];

        $getProjectDocumentIds = array();
        $getProjectDocumentIds = DB::table('projects')
                            ->select('document_id')
                            ->where('organization_id',$organization)
                            ->where('is_deleted',0)->get()->toArray();
        
        $documentIdArrayList = array();                            
        foreach($getProjectDocumentIds as $getProjectDocumentIdsK=>$getProjectDocumentIdsV)
        {
            $documentIdArrayList[]=$getProjectDocumentIdsV->document_id;
        }
        
        $getItemIdsOfAllProject = array();
        $getItemIdsOfAllProject = DB::table('items')
                            ->select('item_id')
                            ->whereIn('document_id', $documentIdArrayList)
                            ->where('organization_id',$organization)
                            ->where('is_deleted',0)->get()->toArray();

        $itemIdArrayList = array();                            
        foreach($getItemIdsOfAllProject as $getItemIdsOfAllProjectK=>$getItemIdsOfAllProjectV)
        {
            $itemIdArrayList[]=$getItemIdsOfAllProjectV->item_id;
        }                            

        $documentIdAndItemIdArrayList = array();
        $documentIdAndItemIdArrayList = array_merge($documentIdArrayList,$itemIdArrayList);                            
        
        $getThreadSourceId = array();
        $getThreadSourceId = DB::table('thread_comments')
        ->select('thread_source_id')
        ->where('organization_id',$organization)
        ->where('is_deleted','0')
        ->whereIn('thread_source_id',$documentIdAndItemIdArrayList)
        ->get()
        ->toArray();
        
        $getThreadSourceIdArray = array();
        foreach($getThreadSourceId as $getThreadSourceIdK=>$getThreadSourceIdV)
        {
            $getThreadSourceIdArray[]=$getThreadSourceIdV->thread_source_id;
        } 
        
        $getOpenCommentData = array();
        $getOpenCommentData = DB::table('threads')
        ->select('status')
        ->where('organization_id',$organization)
        ->where('is_deleted','0')
        ->where('status','1')
        ->where('assign_to',$userId)
        ->whereIn('thread_source_id',$getThreadSourceIdArray)
        ->get()
        ->toArray();
        
        $openCount = count($getOpenCommentData);

        if(empty($openCount)){
            $openCount = 0;
        }

        $this->setCommentAssignedCount($openCount);
    }

    public function projectAssigned($data){
        $userId =  $data['auth_user']['user_id'];
        $organization = $data['auth_user']['organization_id'];
        $getProject = Project::where('created_by', $userId)->where('organization_id',$organization)->where('is_deleted',0)->where('project_type',1)->get();
        $createdProject =$getProject->count();
        if(empty($createdProject)){
            $createdProject = 0;
        }
        $this->setProjectCreatedCount($createdProject);
        $getProjectArr =$getProject->toArray(); 
        foreach($getProjectArr as  $getProjects){
            $projectId[] = $getProjects['project_id'];
        }
       
        $getProjectAssigned = DB::table('project_users')
                            ->join('projects','projects.project_id','=','project_users.project_id')
                            ->select('projects.*','project_users.*')
                            ->where('projects.is_deleted',0)
                            ->where('projects.project_type',1)
                            ->where('project_users.is_deleted',0)
                            ->where('project_users.user_id',$userId)
                            ->where('project_users.organization_id',$organization)
                            ->get();
            $assigned = $getProjectAssigned->count();
            if(empty($assigned)){
                $assigned = 0;
            }
        $this->setProjectAssignedCount($assigned);
    }

    private function prepareCountMetricsData() {
        $data = [
            "projects_created_by_me" => $this->getProjectCreatedCount(),
            "comments_created_by_me" => $this->getCommentCreatedCount(),
            "taxonomies_created_by_me" => $this->getDocumentCreatedCount(),
            "comments_assigned_to_me"=> $this->getCommentAssignedCount(),
            "projects_assigned_to_me" => $this->getProjectAssignedCount(),
            "published_taxonomies"    => $this->getDocumentPublishedCount()
        ];
        $this->setCountMetricsData($data);
    }
}
