<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Report\Validators\GetEventActivityLogRequestValidator as Validator;

class ValidateRequestToGetEventActivityLogJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
