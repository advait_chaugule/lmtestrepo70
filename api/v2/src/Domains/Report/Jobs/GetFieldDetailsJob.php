<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetFieldDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $fieldName , array $documentIdentifier ,array $nodeName ,array $nodeDetails , array $itemsMappedDetails , string $organizationIdentifier)
    {
        $this->data = $fieldName;
        $this->setFieldIdentifier($this->data);

        $this->documentIdentifier = $documentIdentifier;
        $this->setDocumentIdentifier($this->documentIdentifier);

        $this->nodeName = $nodeName;
        $this->setNodeNameIdentifier($this->nodeName);

        $this->nodeDetails = $nodeDetails;
        $this->setNodeIdentifier($this->nodeDetails);

        $this->itemsMappedDetails = $itemsMappedDetails;
        $this->setItemMappedIdentifier($this->itemsMappedDetails);

        $this->organizationData = $organizationIdentifier;
        $this->setOrganizationIdentifier($this->organizationData);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $fieldName = $this->data;
        $documentIdentifier = $this->documentIdentifier;
        $nodeDetails = $this->nodeDetails;
        $nodeName = $this->nodeName;
        $itemsMappedDetails = $this->itemsMappedDetails;
        $organizationIdentifier = $this->organizationIdentifier;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getFieldsFilledDetails($fieldName,$documentIdentifier,$nodeName,$nodeDetails,$itemsMappedDetails,$organizationIdentifier);
    }

    public function setFieldIdentifier(array $fieldName) {
        $this->fieldName = $fieldName; 
    }

    public function getFieldIdentifier() {
        return $this->fieldName;
    }

    public function setDocumentIdentifier(array $documentIdentifier) {
        $this->documentIdentifier = $documentIdentifier; 
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setNodeNameIdentifier(array $nodeName) {
        $this->nodeName = $nodeName; 
    }

    public function getNodeNameIdentifier() {
        return $this->nodeName;
    }

    public function setNodeIdentifier(array $nodeDetails) {
        $this->nodeDetails = $nodeDetails; 
    }

    public function getNodeIdentifier() {
        return $this->nodeDetails;
    }

    public function setItemMappedIdentifier(array $itemsMappedDetails) {
        $this->itemsMappedDetails = $itemsMappedDetails; 
    }

    public function getItemMappedIdentifier() {
        return $this->itemsMappedDetails;
    }

    public function setOrganizationIdentifier(string $organizationId) {
        $this->organizationIdentifier = $organizationId; 
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

}
