<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Models\Report\FactActivityLog;

class CreateEventActivityListJob extends Job
{

    private $activityLogs;
    private $parsedActivityData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $activityLogs)
    {
        $this->setActivityLogs($activityLogs);
        $this->setActivityTemplates();
    }

    public function setActivityLogs(Collection $data) {
        $this->activityLogs = $data;
    }

    public function getActivityLogs(): Collection {
        return $this->activityLogs;
    }

    public function setActivityTemplates() {
        $this->activityTemplates = config("event_activity.activity_templates");
    }

    public function getActivityTemplates(): array {
        return $this->activityTemplates;
    }

    public function setParsedActivityData(array $data) {
        $this->parsedActivityData = $data;
    }

    public function getParsedActivityData(): array {
        return $this->parsedActivityData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // parse and set activity list data
        $this->parseAndSetActivityData();
        // return the parsed activity list
        return $this->getParsedActivityData();
    }
    private function parseAndSetActivityData() {
        $rawActivityData = $this->getActivityLogs();
        $parsedData = [];
        foreach($rawActivityData as $activity) {
            $displayActivityText = $this->resolveActivityDisplayText($activity);
            $activityLogId = $activity->activity_log_id;
            $userId = $activity->user_id;
            $activityEventType = $activity->activity_event_type;
            $activityId = $activity->activity_id;
            $activityEventSubType = $activity->activity_event_sub_type ?: "";
            $activityEventSubTypeActivityId = $activity->activity_event_sub_type_activity_id ?: "";
            $cacheActivityName = $activity->cache_activity_name ?: "";
            $cacheActivitySubtypeActivityName = $activity->cache_activity_subtype_activity_name ?: "";
            $cacheUserDisplayName = $activity->cache_user_display_name ?: "";
            $activityLogTimestamp = $activity->activity_log_timestamp;
            if(!empty($displayActivityText)){
                $parsedData[] = [
                    'display_activity_text' => $displayActivityText,
                    'activity_log_id' => $activityLogId, 
                    'user_id' => $userId, 
                    'activity_event_type' => $activityEventType, 
                    'activity_id' => $activityId, 
                    'activity_event_sub_type' => $activityEventSubType,
                    'activity_event_sub_type_activity_id' => $activityEventSubTypeActivityId,
                    'cache_activity_name' => $cacheActivityName,
                    'cache_activity_subtype_activity_name' => $cacheActivitySubtypeActivityName,
                    'cache_user_display_name' => $cacheUserDisplayName,
                    'activity_log_timestamp' => $activityLogTimestamp
                ];
            }
        }
        $this->setParsedActivityData($parsedData);
    }

    private function resolveActivityDisplayText(FactActivityLog $activity): string {
        // set default display text
        $displayText = "";
        // get templates for report activities
        $activityTemplates = $this->getActivityTemplates();

        $activityEventType = $activity->activity_event_type;
        $activityEventSubType = $activity->activity_event_sub_type ?: "";
        // create the key to fetch activity specific template
        $templateKey = $activityEventSubType!=="" ? ($activityEventType . "-" . $activityEventSubType) : $activityEventType;
        // fetch the activity template
        $activityTemplate = !empty($activityTemplates[$templateKey]) ? $activityTemplates[$templateKey] : "";

        if($activityTemplate != "") {
            // set all template variables to resolve
            $cache_activity_name = $activity->cache_activity_name ?: "";
            $cache_activity_subtype_activity_name = $activity->cache_activity_subtype_activity_name ?: "";
            $cache_user_display_name = ucwords($activity->cache_user_display_name) ?: "";
            // exract template variables pattern from activity template
            preg_match_all('/{{(.*?)}}/', $activityTemplate, $matches);
            if(!empty($matches[1])) {
                $extractedTemplateVariables = $matches[1];
                $displayText = $activityTemplate;
                // loop through each template variables and resolve activity template
                foreach($extractedTemplateVariables as $templateVariable) {
                    $searchString = "{{" . $templateVariable . "}}";
                    $replaceWithString = $$templateVariable;
                    $displayText = str_replace($searchString, $replaceWithString, $displayText);
                }
            }
            
        }
        return $displayText;
    }
}
