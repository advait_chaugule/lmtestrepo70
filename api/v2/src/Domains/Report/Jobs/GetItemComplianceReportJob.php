<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class GetItemComplianceReportJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemsAndDocMappedWithProject,string $oragnization_id)
    {
        $this->data = $itemsAndDocMappedWithProject;
        $this->organization_id = $oragnization_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepo)
    {
        $itemsAndDocMappedWithProject = $this->data;
        $this->projectRepo = $projectRepo;

        return $this->projectRepo->getItemComplianceReport($itemsAndDocMappedWithProject,$this->organization_id);
    }
}
