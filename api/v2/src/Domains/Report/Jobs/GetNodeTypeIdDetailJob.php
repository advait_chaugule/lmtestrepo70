<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetNodeTypeIdDetailJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemsMappedDetails)
    {
        $this->data = $itemsMappedDetails;
        $this->setItemsMappedIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $itemsMappedDetails = $this->data;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getNodeIdDetails($itemsMappedDetails);
    }

    public function setItemsMappedIdentifier(array $identifier) {
        $this->itemsMappedDetails = $identifier; 
    }

    public function getItemsMappedIdentifier() {
        return $this->itemsMappedDetails;
    }

}
