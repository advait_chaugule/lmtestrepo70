<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Carbon\Carbon;

use Illuminate\Support\Collection;

use App\Data\Models\Report\FactActivityLog;

class CreateTopContributorListJob extends Job
{
    private $activityLogs;

    private $projectCrudEventTypes;
    private $itemCrudEventTypes;

    private $userIds;
    private $userProjectActivities;
    private $contributorList;
    private $topContributorList;

    private $userCachedDisplayNameMapping = [];
    private $projectCachedDisplayNameMapping = [];

    private $projectSessionStartedEventTypeText;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $activityLogs)
    {
        $this->setActivityLogs($activityLogs);

        $this->setProjectCrudEventTypes();
        $this->setItemCrudEventTypes();

        $this->projectSessionStartedEventTypeText = "PROJECT_SESSION_STARTED";
    }

    /**
     * Only set those activities which are relevant to the activity section
     */
    public function setActivityLogs(Collection $data) {
        $this->activityLogs = $data;
    }

    public function getActivityLogs(): Collection {
        return $this->activityLogs;
    }

    public function setProjectCrudEventTypes() {
        $this->projectCrudEventTypes = [
            "ITEMS_VIEWED_UNDER_PROJECT",
            "PROJECT_SESSION_STARTED"
        ];
    }

    public function getProjectCrudEventTypes(): array {
        return $this->projectCrudEventTypes;
    }

    public function setItemCrudEventTypes() {
        $this->itemCrudEventTypes = [
            "ITEM_CREATED_UNDER_PROJECT",
            "ITEM_EDITED_UNDER_PROJECT",
            "ITEM_DELETED_UNDER_PROJECT",
            "ITEM_VIEWED_UNDER_PROJECT"
        ];
    }

    public function getItemCrudEventTypes(): array {
        return $this->itemCrudEventTypes;
    }

    public function setUserIds(array $data) {
        $this->userIds = $data;
    }

    public function getUserIds(): array {
        return $this->userIds;
    }

    public function setUserProjectActivities(array $data) {
        $this->userProjectActivities = $data;
    }

    public function getUserProjectActivities(): array {
        return $this->userProjectActivities;
    }

    public function setContributorList(Collection $data) {
        $this->contributorList = $data;
    }

    public function getContributorList(): Collection {
        return $this->contributorList;
    }

    public function setTopContributorList(array $data) {
        $this->topContributorList = $data;
    }

    public function getTopContributorList(): array {
        return $this->topContributorList;
    }

    public function setUserCachedDisplayNameMapping(string $userId, string $cachedName) {
        $this->userCachedDisplayNameMapping[$userId] = $cachedName;
    }

    public function getUserCachedDisplayNameMapping(): array {
        return $this->userCachedDisplayNameMapping;
    }
    public function setProjectCachedDisplayNameMapping(string $projectId, string $cachedName) {
        $this->projectCachedDisplayNameMapping[$projectId] = $cachedName;
    }

    public function getProjectCachedDisplayNameMapping(): array {
        return $this->projectCachedDisplayNameMapping;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // extract and set all activity's user-id first
        $this->extractAndSetUserIds();
        // parse and set user-project activities
        $this->parseAndSetUserProjectActivities();
        // create and set contributor list
        $this->createAndSetContributorList();
        // create and set top contributor list
        $this->createAndSetTopContributorList();
        // finally return the top contributor list
        return $this->getTopContributorList();
    }

    /**
     * Parse unique user ids for all activities and set them
     */
    private function extractAndSetUserIds() {
        $userIds = [];
        $activityLogs = $this->getActivityLogs();
        $uniqueUserActivityLogs = $activityLogs->unique("user_id");
        if($uniqueUserActivityLogs->isNotEmpty()){
            $userIds = explode(",", $uniqueUserActivityLogs->implode("user_id", ","));
        }
        $this->setUserIds($userIds);
    }

    private function parseAndSetUserProjectActivities() {
        $userIds = $this->getUserIds();
        $activityLogs = $this->getActivityLogs();
        $userProjectActivities = [];
        foreach($userIds as $userId){
            $userActivities = $activityLogs->where("user_id", $userId);
            $userProjectActivities[$userId] = $this->helperToGroupProjectSpecificActivities($userActivities);
        }
        $this->setUserProjectActivities($userProjectActivities);
    }

    private function createAndSetContributorList() {
        $contributorList = [];
        $userProjectActivities = $this->getUserProjectActivities();
        
        $eventTypeValueMapping = config("event_activity.event_type_value");
        $projectSessionStartedEventTypeValue = $eventTypeValueMapping[$this->projectSessionStartedEventTypeText];

        foreach($userProjectActivities as $userId => $projectActivities) {
            $contributedToProjects = [];
            foreach($projectActivities as $projectId => $activities) {
                $activityCollection = collect($activities);
                $projectSessionActivityCollection = $activityCollection->where("activity_event_type", $projectSessionStartedEventTypeValue);
                $otherProjectActivityCollection =  $activityCollection->whereNotIn('activity_event_type', [$projectSessionStartedEventTypeValue]);
                $actionCount = $otherProjectActivityCollection->count();
                // we won't consider project with single activitiy for contributor list
                if($actionCount > 1){
                    $timeSpentData = $this->helperToCalculateTimeSpent($projectSessionActivityCollection);
                    $contributedToProjects[] = [
                        "project_id" => $projectId,
                        "project_display_name" => $this->helperToSearchAndReturnCachedNameForType($projectId, "project"),
                        "time_spent_seconds" => $timeSpentData["time_spent_seconds"],
                        "time_spent_minutes" => $timeSpentData["time_spent_minutes"],
                        "time_spent_hours" => $timeSpentData["time_spent_hours"],
                        "time_spent_days" => $timeSpentData["time_spent_days"],
                        "action_count" => $actionCount
                    ];
                }
            }

            // parse, calculate ranking values to create contributor list only if user has contributed to atleast one project
            if(count($contributedToProjects) > 0){
                $contributorList[] = $this->helperToParseContributorListAndCalculateRankings($userId, $contributedToProjects);
            }
        }
        $this->setContributorList(collect($contributorList));
    }

    private function createAndSetTopContributorList() {
        $contributorList = $this->getContributorList();
        $topContributorList = $contributorList->sortByDesc("total_time_spent_in_seconds")->values()->all();
        $this->setTopContributorList($topContributorList);
    }

    /**
     * Group project-activities only.
     */
    private function helperToGroupProjectSpecificActivities(Collection $userActivityLogs): array {
        $projectEventTypeValues = $this->helperToReturnEventTypeGroupValues("project");
        $itemEventTypeValues = $this->helperToReturnEventTypeGroupValues("item");
        $activities = [];
        foreach($userActivityLogs as $activity) {
            $userId = $activity->user_id;
            $cachedUserDisplayName = $activity->cache_user_display_name ?: "";
            $activityEventType = $activity->activity_event_type;
            if(in_array($activityEventType, $projectEventTypeValues)){
                $projectId = $activity->activity_id;
                $projectCachedName = $activity->cache_activity_name ?: "";
                $activities[$projectId][] = $activity->toArray();
            }
            else if(in_array($activityEventType, $itemEventTypeValues)){
                $projectId = $activity->activity_event_sub_type_activity_id;
                $projectCachedName = $activity->cache_activity_subtype_activity_name ?: "";
                $activities[$projectId][] = $activity->toArray();
            }
            // store the user cached name
            $this->setUserCachedDisplayNameMapping($userId, $cachedUserDisplayName);
            // store the project cached name
            if(!empty($projectId)) {
                $this->setProjectCachedDisplayNameMapping($projectId, $projectCachedName);
            }
        }
        return $activities;
    }

    private function helperToReturnEventTypeGroupValues(string $typeGroup): array {
        $eventTypeValueMapping = config("event_activity.event_type_value");
        switch ($typeGroup) {
            case 'project':
                $eventTypes = $this->getProjectCrudEventTypes();
                break;
            case 'item':
                $eventTypes = $this->getItemCrudEventTypes();
                break;
            default:
                $eventTypes = [];
                break;
        }
        $data = [];
        foreach($eventTypes as $eventType) {
            $data[] = $eventTypeValueMapping[$eventType];
        }
        return $data;
    }

    private function helperToSearchAndReturnCachedNameForType(string $searchIdentifier, string $type): string {
        switch ($type) {
            case 'user':
                $searchHayStack = $this->getUserCachedDisplayNameMapping();
                break;
            case 'project':
                $searchHayStack = $this->getProjectCachedDisplayNameMapping();
                break;
            default:
                $searchHayStack = [];
                break;
        }
        return !empty($searchHayStack[$searchIdentifier]) ? ucwords($searchHayStack[$searchIdentifier]) : "";
    }

    private function helperToCalculateTimeSpent(Collection $projectSessionActivityCollection): array {
        $sortedProjectSessionActivities = $projectSessionActivityCollection->sortByDesc("activity_log_timestamp")->toArray();
        $minimumTimeDifferenceBetweenProjectSessionActivityInSeconds = 
        config("event_activity.minimum_time_difference_between_project_session_activity_in_seconds");
        $timeSpentInSeconds = 0;
        foreach($sortedProjectSessionActivities as $currentIndex=>$projectSessionActivity) {
            $currentActivityTimestamp = Carbon::parse($projectSessionActivity["activity_log_timestamp"]);
            $nextActivityIndex = $currentIndex + 1;
            if(!empty($sortedProjectSessionActivities[$nextActivityIndex]["activity_log_timestamp"])) {
                $nextActivityTimestamp = Carbon::parse($sortedProjectSessionActivities[$nextActivityIndex]["activity_log_timestamp"]);
                $differenceInSeconds = $currentActivityTimestamp->diffInSeconds($nextActivityTimestamp);
                if($differenceInSeconds <= $minimumTimeDifferenceBetweenProjectSessionActivityInSeconds){
                    $timeSpentInSeconds += $differenceInSeconds;
                }
            }
        }
        $timeSpentInMinutes = $timeSpentInSeconds > 0 ? floor($timeSpentInSeconds/60) : 0;
        $timeSpentInHours = $timeSpentInMinutes > 0 ? floor($timeSpentInMinutes/60) : 0;
        $timeSpentInDays = $timeSpentInHours > 0 ? floor($timeSpentInHours/24) : 0;
        return [
            "time_spent_seconds" => $timeSpentInSeconds,
            "time_spent_minutes" => $timeSpentInMinutes,
            "time_spent_hours" => $timeSpentInHours,
            "time_spent_days" => $timeSpentInDays
        ];
    }

    private function helperToParseContributorListAndCalculateRankings(string $userId, array $contributedToProjects): array {
        $contributedToProjectsCollection = collect($contributedToProjects);
        $commaSeparatedProjectsDisplayName = $contributedToProjectsCollection->implode('project_display_name', ', ');
        $totalTimeSpentInSeconds = $contributedToProjectsCollection->sum("time_spent_seconds");
        $totalTimeSpentInMinutes = $contributedToProjectsCollection->sum("time_spent_minutes");
        $totalTimeSpentInHours = $contributedToProjectsCollection->sum("time_spent_hours");
        $totalTimeSpentInDays = $contributedToProjectsCollection->sum("time_spent_days");
        $totalActionCount = $contributedToProjectsCollection->sum("action_count");
        $parsedContributorListWithRankings = [
            "user_id" => $userId,
            "user_display_name" => $this->helperToSearchAndReturnCachedNameForType($userId, "user"),
            "projects_display_name" => $commaSeparatedProjectsDisplayName,
            "total_time_spent_in_seconds" => $totalTimeSpentInSeconds,
            "total_time_spent_in_minutes" => $totalTimeSpentInMinutes,
            "total_time_spent_in_hours" => $totalTimeSpentInHours,
            "total_time_spent_in_days" => $totalTimeSpentInDays,
            "total_action_count" => $totalActionCount,
            "contributed_projects" => $contributedToProjects
        ];
        return $parsedContributorListWithRankings;
    }
}
