<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetNodeTypeMetadataDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $nodeType)
    {
        $this->data = $nodeType;
        $this->setNodeIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $nodeType = $this->data;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getNodeTypeMetadataDetails($nodeType);
    }

    public function setNodeIdentifier(array $identifier) {
        $this->nodeType = $identifier; 
    }

    public function getNodeIdentifier() {
        return $this->nodeType;
    }
}
