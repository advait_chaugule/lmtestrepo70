<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetItemsMappedWithProjectJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier)
    {
        $this->data = $projectIdentifier;
        $this->setProjectIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $complianceReport)
    {
        $projectIdentifier = $this->data;
        $this->complianceReport = $complianceReport;

        return $this->complianceReport->getItemsMappedWithProjectDetails($projectIdentifier);
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier; 
    }

    public function getProjectIdentifier() {
        return $this->projectIdentifier;
    }
}
