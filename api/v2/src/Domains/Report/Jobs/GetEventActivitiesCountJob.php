<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Models\Report\FactActivityLog;

use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;

class GetEventActivitiesCountJob extends Job
{

    private $activityLogRepository;
    private $requestFilters;
    private $requestUserDetails;
    private $eventTypeValues;

    private $projectCreateEventTypeText;
    private $projectDeleteEventTypeText;
    private $itemCreatedUnderProjectEventTypeText;
    private $itemDeletedUnderProjectEventTypeText;

    private $paginationFilters;
    private $sortFilters;
    private $otherFilters;
    private $rawActivityData;

    private $projectCreatedCount;
    private $projectDeletedCount;
    private $taxonomyNodesAddedCount;
    private $taxonomyNodesDeletedCount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->setRequestFilters($requestData);
        $this->setRequestUserDetails($requestData["auth_user"]);
        $this->setEventTypeValues();

        $this->projectCreateEventTypeText = "PROJECT_CREATED";
        $this->projectDeleteEventTypeText = "PROJECT_DELETED";
        $this->itemCreatedUnderProjectEventTypeText = "ITEM_CREATED_UNDER_PROJECT";
        $this->itemDeletedUnderProjectEventTypeText = "ITEM_DELETED_UNDER_PROJECT";
    }

    public function setRequestFilters(array $data) {
        $this->requestFilters = $data;
    }

    public function getRequestFilters(): array {
        return $this->requestFilters;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setEventTypeValues() {
        $this->eventTypeValues = config("event_activity.event_type_value");
    }

    public function getEventTypeValues(): array {
        return $this->eventTypeValues;
    }

    public function setPaginationFilters(array $data) {
        $this->paginationFilters = $data;
    }

    public function getPaginationFilters(): array {
        return $this->paginationFilters;
    }

    public function setSortFilters(array $data) {
        $this->sortFilters = $data;
    }

    public function getSortFilters(): array {
        return !empty($this->sortFilters) ? $this->sortFilters : [ "orderBy" => "activity_log_timestamp", "sorting" => "desc" ];
    }

    public function setOtherFilters(array $data) {
        $this->otherFilters = $data;
    }

    public function getOtherFilters(): array {
        return $this->otherFilters;
    }

    public function setRawActivityData(Collection $data) {
        $this->rawActivityData = $data;
    }

    public function getRawActivityData(): Collection {
        return $this->rawActivityData;
    }

    public function setProjectCreatedCount(int $data) {
        $this->projectCreatedCount = $data;
    }

    public function getProjectCreatedCount(): int {
        return $this->projectCreatedCount;
    }

    public function setProjectDeletedCount(int $data) {
        $this->projectDeletedCount = $data;
    }

    public function getProjectDeletedCount(): int {
        return $this->projectDeletedCount;
    }

    public function setTaxonomyNodesAddedCount(int $data) {
        $this->taxonomyNodesAddedCount = $data;
    }

    public function getTaxonomyNodesAddedCount(): int {
        return $this->taxonomyNodesAddedCount;
    }

    public function setTaxonomyNodesDeletedCount(int $data) {
        $this->taxonomyNodesDeletedCount = $data;
    }

    public function getTaxonomyNodesDeletedCount(): int {
        return $this->taxonomyNodesDeletedCount;
    }

    public function setCountMetricsData(array $data) {
        $this->countMetricsData = $data;
    }

    public function getCountMetricsData(): array {
        return $this->countMetricsData;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivityLogRepositoryInterface $activityLogRepository)
    {
        // set database repositories
        $this->activityLogRepository = $activityLogRepository;

        // set pagination, sorting and other filters
        $this->setFilters();

        // fetch and set raw activity data
        $this->fetchAndSetRawActivityData();

        // parse and set various count metrices
        $this->parseAndSetCountMetrices();

        // prepare and set various
        $this->prepareCountMetricsData();

        // retrun the response data
        return $this->getCountMetricsData();
    }

    private function setFilters() {
        $requestFilters = $this->getRequestFilters();

        $paginationFilters = [];
        if( array_key_exists("limit", $requestFilters) && array_key_exists("offset", $requestFilters) ) {
            $paginationFilters = [ "limit" => (int) $requestFilters["limit"], "offset" => (int) $requestFilters["offset"] ];
        }
        $this->setPaginationFilters($paginationFilters);

        $sortFilters = [];
        $allowedOrderByFields = ["activity_log_timestamp"];
        $allowedSortingDirection = ["desc", "asc"];
        if(
            array_key_exists("orderBy", $requestFilters) && in_array($requestFilters["orderBy"], $allowedOrderByFields) &&
            array_key_exists("sorting", $requestFilters) && in_array($requestFilters["sorting"], $allowedSortingDirection)
        ) {
            $sortFilters = [ "orderBy" => $requestFilters["orderBy"], "sorting" => $requestFilters["sorting"] ];
        }
        $this->setSortFilters($sortFilters);

        $otherFilters = [];
        $this->setOtherFilters($otherFilters);
    }

    private function fetchAndSetRawActivityData() {
        $requestUserDetails = $this->getRequestUserDetails();
        $organizationId = $requestUserDetails["organization_id"];
        $returnCollection = true;
        $paginationFilters = $this->getPaginationFilters();
        $returnPaginatedData = !empty($paginationFilters);
        $fieldsToReturn = [
            'activity_log_id',
            'activity_event_type', 
            'activity_id',
            'activity_event_sub_type',
            'activity_event_sub_type_activity_id'
        ];
        $keyValuePairs = [ 'organization_id' => $organizationId ];
        $sortfilters = $this->getSortFilters();
        $returnSortedData = !empty($sortfilters);
        $data = $this->activityLogRepository->find(
                                                $returnCollection,
                                                $fieldsToReturn,
                                                $keyValuePairs,
                                                $returnPaginatedData,
                                                $paginationFilters,
                                                $returnSortedData,
                                                $sortfilters
                                            );

        $this->setRawActivityData($data);
    }

    private function parseAndSetCountMetrices() {
        $rawActivityData = $this->getRawActivityData();
        $this->parseAndSetNewProjectCreatedCount($rawActivityData);
        $this->parseAndSetProjectDeletedCount($rawActivityData);
        $this->parseAndSetNewTaxonomyNodesAddedCount($rawActivityData);
        $this->parseAndSetNewTaxonomyNodesDeletedCount($rawActivityData);
    }

    private function parseAndSetNewProjectCreatedCount(Collection $rawActivityData) {
        $count = $this->getActivityEventTypeSpecificCountHelper($rawActivityData, $this->projectCreateEventTypeText);
        $this->setProjectCreatedCount($count);
    }

    private function parseAndSetProjectDeletedCount(Collection $rawActivityData) {
        $count = $this->getActivityEventTypeSpecificCountHelper($rawActivityData, $this->projectDeleteEventTypeText);
        $this->setProjectDeletedCount($count);
    }

    private function parseAndSetNewTaxonomyNodesAddedCount(Collection $rawActivityData) {
        $count = $this->getActivityEventTypeSpecificCountHelper($rawActivityData, $this->itemCreatedUnderProjectEventTypeText);
        $this->setTaxonomyNodesAddedCount($count);
    }

    private function parseAndSetNewTaxonomyNodesDeletedCount(Collection $rawActivityData) {
        $count = $this->getActivityEventTypeSpecificCountHelper($rawActivityData, $this->itemDeletedUnderProjectEventTypeText);
        $this->setTaxonomyNodesDeletedCount($count);
    }

    private function prepareCountMetricsData() {
        $data = [
            "projects_created" => $this->getProjectCreatedCount(),
            "projects_deleted" => $this->getProjectDeletedCount(),
            "taxonomy_nodes_added" => $this->getTaxonomyNodesAddedCount(),
            "taxonomy_nodes_removed" => $this->getTaxonomyNodesDeletedCount()
        ];
        $this->setCountMetricsData($data);
    }

    private function getActivityEventTypeSpecificCountHelper(Collection $rawActivityData, string $activityEventTypeText): int {
        $eventTypeValues = $this->getEventTypeValues();
        $activityEventTypeValue = (integer)$eventTypeValues[$activityEventTypeText];
        $count = $rawActivityData->whereStrict('activity_event_type', $activityEventTypeValue)->count();
        return $count;
    }
}
