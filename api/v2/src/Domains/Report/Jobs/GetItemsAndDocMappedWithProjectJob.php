<?php
namespace App\Domains\Report\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetItemsAndDocMappedWithProjectJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier)
    {
        $this->data = $projectIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemComplianceReport)
    {
        $projectIdentifier = $this->data;
        $this->itemComplianceReport = $itemComplianceReport;

        return $this->itemComplianceReport->getItemsAndDocMappedWithProjectDetails($projectIdentifier);
    }
}
