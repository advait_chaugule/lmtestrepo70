<?php
namespace App\Domains\Report\Tests\Jobs;

use App\Domains\Report\Jobs\GetItemDetailsJob;
use Tests\TestCase;

class GetItemDetailsJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new GetItemDetailsJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }
}
