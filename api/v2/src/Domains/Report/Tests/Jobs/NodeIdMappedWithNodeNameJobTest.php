<?php
namespace App\Domains\Report\Tests\Jobs;

use App\Domains\Report\Jobs\NodeIdMappedWithNodeNameJob;
use Tests\TestCase;

class NodeIdMappedWithNodeNameJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new NodeIdMappedWithNodeNameJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8','cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_setOrganizationIdentifier()
    {
        $assets = new NodeIdMappedWithNodeNameJob('f30b9d7a-edf9-4f07-b510-eca225d6e9d8','cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
