<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class CreateNodeTypeJob extends Job
{
    use DateHelpersTrait,CaseFrameworkTrait;
    
    private $nodeTypeRepository;
    private $nodeTypeDetail;
    private $nodeTypeData;
    private $nodeTypeEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //assign private attribute to input data
        $this->nodeTypeDetail = $input;

    }

    //Public Setters and Getters method
    public function setSavedNodeTypeData($data){
        $this->nodeTypeData = $data ;
    }

    public function getSavedNodeTypeData(){
        return $this->nodeTypeData;
    }

    public function setNodeTypeEntity($nodeTypeEntity){
        $this->nodeTypeEntity = $nodeTypeEntity;
    }

    public function getNodeTypeEntity(){
        return $this->nodeTypeEntity;
    }
   

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //set nodeTypeRepository variable
        $this->nodeTypeRepository = $nodeTypeRepository;
        //save the node type input data to database
        $savedNodeTypeData = $this->nodeTypeRepository->fillAndSave($this->nodeTypeDetail);
        
        $this->setSavedNodeTypeData($savedNodeTypeData);

        $this->parseResponse();

        return $this->getNodeTypeEntity();
    }

    private function parseResponse(){
        $savedNodeTypeData = $this->getSavedNodeTypeData();
        
        $nodeTypeEntity = [
            "node_type_id"  =>  $savedNodeTypeData->node_type_id,
            "name"          =>  $savedNodeTypeData->title,
            "created_at"    =>  $this->formatDateTimeToISO8601($savedNodeTypeData->created_at),
            "upadated_at"   =>  $this->formatDateTimeToISO8601($savedNodeTypeData->updated_at),        
        ];
        $this->setNodeTypeEntity($nodeTypeEntity);
    }
}
