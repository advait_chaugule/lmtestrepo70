<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class ListNodeTypeMetadataJob extends Job
{
    private $nodeTypeRepository;
    private $identifier;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nodeTypeId)
    {
        $this->setIdentifier($nodeTypeId);
    }

    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        $arrayOfMetadataId  =   [];
        $this->nodeTypeRepository = $nodeTypeRepository;
        $identifier = $this->getIdentifier();

        $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($identifier);

        return $this->transFormNodeTypeMetadata($metadataList);
    }

    private function transFormNodeTypeMetadata($metadataList): array
    {
        $metadataarr = ['concept_keywords','concept_hierarchy_code','education_level','status_start_date','status_end_date'];
        $nodeTypeMetadata = [];
        if($metadataList->metadata->isNotEmpty()) {
            foreach($metadataList->metadata as $metadata) {
                
                if($metadata->internal_name == 'language')
                {
                    $metadata->internal_name = 'language_name';
                }

                $nodeTypeMetadataarr =  [
                    "metadata_id"           =>  $metadata->metadata_id,
                    "parent_metadata_id"    =>  $metadata->parent_metadata_id,
                    "name"                  =>  $metadata->name,
                    "field_type"            =>  $metadata->field_type,
                    "field_possible_values" =>  $metadata->field_possible_values,
                    "is_custom"             =>  $metadata->is_custom,
                    "is_mandatory"          =>  $metadata->pivot->is_mandatory,
                    "order"                 =>  $metadata->pivot->sort_order,
                    "is_mandatory"          =>  $metadata->pivot->is_mandatory,
                    "internal_name"         =>  $metadata->internal_name,
                    "internal_name"."_html"  =>  $metadata->internal_name."_html",
                ];
                if(in_array($metadata->internal_name,$metadataarr))
                {
                    unset($nodeTypeMetadataarr['internal_name_html']);
                }
                $nodeTypeMetadata[]   =   $nodeTypeMetadataarr;
            }
        }

        $sortedArrayOfNodeTypeMetadata = array();
        foreach ($nodeTypeMetadata as $key => $row)
        {
            $sortedArrayOfNodeTypeMetadata[$key] = $row['order'];
        }
        
        array_multisort($sortedArrayOfNodeTypeMetadata, SORT_ASC, $nodeTypeMetadata);

        return $nodeTypeMetadata;
    }
}
