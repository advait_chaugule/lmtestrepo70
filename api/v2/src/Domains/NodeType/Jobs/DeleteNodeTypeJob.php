<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class DeleteNodeTypeJob extends Job
{
    private $nodeTypeId;
    private $organizationId;

    private $itemRepository;
    private $nodeTypeRepo;

    private $deleteStatusData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nodeTypeIdentifier, $organizationIdentifier)
    {
        $this->nodeTypeId           =   $nodeTypeIdentifier;
        $this->organizationId       =   $organizationIdentifier;
        $this->deleteStatusData     =   ["is_deleted" => 1];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepo, ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository   =   $itemRepository;
        $this->nodeTypeRepo     =   $nodeTypeRepo;

        $this->updateItemNodeTypeToDefaultNodeType();
        
        return $nodeTypeRepo->edit($this->nodeTypeId, $this->deleteStatusData);
    }

    private function updateItemNodeTypeToDefaultNodeType() {
        $nodeTypeIdentifier =   $this->nodeTypeId;
        $attributes         =   ['organization_id'  =>  $this->organizationId, 'is_default'  =>  '1'];
        $defaultNodeType    =   $this->nodeTypeRepo->findByAttributes($attributes)->first();

        $columnValueFilterPairs =   ['node_type_id' =>  $nodeTypeIdentifier];   
        $attributesToUpdate     =   ['node_type_id' =>  $defaultNodeType->node_type_id];

        $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributesToUpdate); 
    }
}
