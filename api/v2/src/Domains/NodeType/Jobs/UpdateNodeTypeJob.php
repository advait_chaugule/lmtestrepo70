<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class UpdateNodeTypeJob extends Job
{
    use DateHelpersTrait;

    private $nodeTypeIdentifier;
    private $requestData;
    private $nodeTypeRepository;

    private $fieldTypeValues;
    private $nodeTypeEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, array $input)
    {
        //Set the Identifier
        $this->setNodeTypeIdentifier($identifier);
        $this->setInputData($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //Set the nodeType repository handler
        $this->nodeTypeRepository = $nodeTypeRepository;

        $nodeTypeIdentifier         = $this->getNodeTypeIdentifier();
        $attribute['title']         = $this->getInputData()['name'];
        $attribute['updated_by']    = $this->getInputData()['auth_user']['user_id'];
        
        $updatedNodeType = $this->nodeTypeRepository->edit($nodeTypeIdentifier, $attribute);
            
        $this->parseResponse();

        $nodeTypeEntity = $this->getNodeTypeEntity();
        return $nodeTypeEntity;     
    }

    public function setNodeTypeIdentifier($identifier){
        $this->nodeTypeIdentifier = $identifier;
    }

    public function getNodeTypeIdentifier(){
        return $this->nodeTypeIdentifier;
    }

    public function setInputData($input){
        $this->requestData = $input;
    }

    public function getInputData(){
        return $this->requestData;
    }

    public function setNodeTypeEntity($nodeTypeEntity){
        $this->nodeTypeEntity = $nodeTypeEntity;
    }

    public function getNodeTypeEntity(){
        return $this->nodeTypeEntity;
    }

    private function parseResponse(){
        $nodeTypeIdentifier = $this->getNodeTypeIdentifier();
        $nodeTypeDetail = $this->nodeTypeRepository->find($nodeTypeIdentifier);
        
        $nodeTypeEntity = [
            "node_type_id"          =>  $nodeTypeIdentifier,
            "name"                  =>  !empty($nodeTypeDetail->title) ? $nodeTypeDetail->title : "",
            "updated_at"            =>  $this->formatDateTimeToISO8601($nodeTypeDetail->updated_at)
        ];
        
        $this->setNodeTypeEntity($nodeTypeEntity);
    }
}
