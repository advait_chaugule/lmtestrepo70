<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class CheckNodeTypeIsUsedJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $nodeTypeIdIdentifier)
    {   
        $this->identifier = $nodeTypeIdIdentifier;
        $this->setNodeTypeIdentifier($this->identifier);
    }

    public function setNodeTypeIdentifier(array $identifier) {
        $this->identifier = $identifier; 
    }

    public function getNodeTypeIdentifier() {
        return $this->identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {   
        $nodeTypeIdentifier = $this->identifier;
        $this->nodeTypeRepository=$nodeTypeRepository;
        return $this->nodeTypeRepository->getNodeTypeIsUsedData($nodeTypeIdentifier);
    }
}
