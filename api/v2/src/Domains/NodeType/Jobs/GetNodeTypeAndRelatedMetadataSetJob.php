<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class GetNodeTypeAndRelatedMetadataSetJob extends Job
{
    private $organizationIdentifier;
    private $nodeTypeRepository;
    private $metadataRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $organizationIdentifier)
    {
        //Set the private data
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository, NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //Set the db handlers
        $this->metadataRepository   =   $metadataRepository;
        $this->nodeTypeRepository   =   $nodeTypeRepository;

        $this->fetchAndSetNodeTypeDetail();
        return $this->parseAndSetData();
    }

    public function setOrganizationIdentifier(string $identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() : string {
        return $this->organizationIdentifier;
    }

    public function setNodeTypeFromDb($data) {
        $this->nodeTypeDetail = $data;
    }

    public function getNodeTypeFromDb() {
        return $this->nodeTypeDetail;
    }

    private function fetchAndSetNodeTypeDetail() {
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['*'];
        $keyValuePairs      =   ['organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations          =   ['metadata'];

        $nodeTypeDetail =   $this->nodeTypeRepository->findByAttributesWithSpecifiedFields(
        $returnCollection,
        $fieldsToReturn,
        $keyValuePairs,
        $relations);

        $this->setNodeTypeFromDb($nodeTypeDetail);
    }

    private function parseAndSetData() {
        $dataToSet      =   [];
        $nodeTypeDetail =   $this->getNodeTypeFromDb();
        //dd($nodeTypeDetail);
        foreach($nodeTypeDetail as $key => $nodeType) {
            $metadataList         = [];
            $metadataWithNodeType = $nodeType->metadata->toArray();

            //dd($metadataWithNodeType);
            $dataToSet[$key]    =   [
                "node_type_name"    =>  $nodeType->title,
                "node_type_id"      =>  $nodeType->node_type_id,
            ];

            foreach($metadataWithNodeType as $metadataAdded) {
                $metadataList[] =   [
                    "metadata_id"           =>  $metadataAdded['metadata_id'],
                    "parent_metadata_id"    =>  $metadataAdded['parent_metadata_id'],
                    "name"                  =>  $metadataAdded['name'],
                    "field_type"            =>  $metadataAdded['field_type'],
                    "field_possible_values" =>  $metadataAdded['field_possible_values'],
                    "is_custom"             =>  $metadataAdded['is_custom'],
                    "is_mandatory"          =>  $metadataAdded['is_mandatory'],
                    "order"                 =>  $metadataAdded['order'],
                    "internal_name"         =>  $metadataAdded['internal_name']
                ];
            }

            $dataToSet[$key]['metadata'] = $metadataList;
        }

        return $dataToSet;
    }
}
