<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use Illuminate\Support\Facades\DB;

class SaveNodeTypeMetadataJob extends Job
{
    private $nodeTypeRepository;
    private $identifier;
    private $requestData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($identifier, $data)
    {
        $this->setIdentifier($identifier);
        $this->setRequestData($data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        $this->nodeTypeRepository = $nodeTypeRepository;
        
            $this->saveNodeTypeMetadata();
        
    }

    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setRequestData($data){
        $this->requestData = $data;
    }

    public function getRequestData() {
        return $this->requestData;
    }

    public function saveNodeTypeMetadata(){
        
        $identifier = $this->getIdentifier();
        $requestData = $this->getRequestData();
        $this->nodeTypeRepository->saveNodeTypeMetadata($identifier, $requestData);
    }
}
