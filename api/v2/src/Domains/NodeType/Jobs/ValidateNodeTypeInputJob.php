<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Domains\NodeType\Validators\CreateUpdateNodeTypeValidator;

class ValidateNodeTypeInputJob extends Job
{
    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //assign input data to private attribute
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateUpdateNodeTypeValidator $validator)
    {
        //validate the input data based on DB
        $validation = $validator->validate($this->data);

        if($validation === true) {
            # Check if metadataset title is unique and not deleted, as deleted ones are allowed to be created again
            if($validator->validateIsDeleted($this->data)) {
                return $validation;
            } else {
                return [
                    'Metadataset with same name already exists.'
                ];
            }
        } else {
            return $validation->errors()->all();
        }
    }
}
