<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class ListNodeTypeJob extends Job
{
    use DateHelpersTrait;

    private $nodetypeRepository;
    private $requestData;
    private $organizationIdentifier;
    private $nodeType;
    private $nodeTypeList;
    private $fieldTypeValues;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the input data
        $this->requestData = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodetypeRepository)
    {
        //Set the nodetype repo handler
        $this->nodetypeRepository = $nodetypeRepository;

        $requestData                =   $this->requestData;
        $requestData['sort']        =   'title:asc';

        // set the fieldTypes from config
        $this->setFieldTypeValues();
        
        // get all nodetypes
        $nodeType = $this->nodetypeRepository->getNodetypeList($requestData);
        $this->setNodetype($nodeType);
        // prepare the response
        $this->transFormNodetypeResponse();
        $parsedNodetype = $this->getNodetypeList();
        
        return ['nodetype' => $parsedNodetype];
    }

    public function setFieldTypeValues() {
        $this->fieldTypeValues = config("default_enum_setting.metadata_field_type");
    }

    public function getFieldTypeValues() {
        return $this->fieldTypeValues;
    }

    public function setNodetype($nodeType){
        $this->nodeType = $nodeType;
    }

    public function getNodetype() {
        return $this->nodeType;
    }

    public function setNodetypeList($nodeTypeList){
        $this->nodeTypeList = $nodeTypeList;
    }

    public function getNodetypeList() {
        return $this->nodeTypeList;
    }

    private function transFormNodetypeResponse(){
        $nodeTypeList = $this->getNodetype();

        $fieldTypeValues = $this->getFieldTypeValues();
        $parsedNodetype = [];
        if($nodeTypeList->isNotEmpty()){
            foreach($nodeTypeList as $nodeType) {
                
                $parsedNodetype[] = [
                    'node_type_id'          => $nodeType->node_type_id,
                    'title'                 => !empty($nodeType->title) ? $nodeType->title : '',
                    'is_document'           => $nodeType->is_document,
                    'is_deleted'            => $nodeType->is_deleted,
                    'is_default'            => $nodeType->is_default, 
                    'created_by'            => !empty($nodeType->createdBy->name) ? $nodeType->createdBy->name : "",
                    'updated_at'            => $this->formatDateTimeToISO8601($nodeType->updated_at),
                ];
            }
        }

        $this->setNodetypeList($parsedNodetype);
    }
}
