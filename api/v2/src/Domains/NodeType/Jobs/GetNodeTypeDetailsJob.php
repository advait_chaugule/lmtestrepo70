<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class GetNodeTypeDetailsJob extends Job
{
    use DateHelpersTrait;

    private $nodeTypeIdentifier;
    private $organizationIdentifier;
    private $nodeTypeRepository;

    private $nodeTypeDetail;
    private $nodeEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, string $organizationIdentifier)
    {
        //Set the identifier 
        $this->setNodeTypeIdentifier($identifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //Set the node type repo handler
        $this->nodeTypeRepository = $nodeTypeRepository;
        $nodeTypeIdentifier = $this->getNodeTypeIdentifier();

        $nodeTypeDetail = $this->getNodeType();
        $this->setNodeTypeDetail($nodeTypeDetail);

        $this->parseNodeTypeResponse();

        $nodeTypeEntity = $this->getNodeTypeEntity();
        return $nodeTypeEntity;
        
    }

    public function setNodeTypeIdentifier($nodeTypeIdentifier){
        $this->nodeTypeIdentifier = $nodeTypeIdentifier;
    }

    public function getNodeTypeIdentifier() {
        return $this->nodeTypeIdentifier;
    }

    public function setOrganizationIdentifier($organizationIdentifier){
        $this->organizationIdentifier = $organizationIdentifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setNodeTypeDetail($nodeTypeDetail) {
        $this->nodeTypeDetail = $nodeTypeDetail;
    }

    public function getNodeTypeDetail() {
        return $this->nodeTypeDetail;
    }

    public function setNodeTypeEntity($nodeTypeEntity) {
        $this->nodeTypeEntity = $nodeTypeEntity;
    }

    public function getNodeTypeEntity() {
        return $this->nodeTypeEntity;
    }

    private function getNodeType() {
        $identifier              =   $this->getNodeTypeIdentifier();
        $organizationIdentifier  =   $this->getOrganizationIdentifier();

        $nodeTypeDetail = $this->nodeTypeRepository->getNodeType($identifier, $organizationIdentifier);
        return $nodeTypeDetail;
    }

    private function parseNodeTypeResponse() {
        $nodeTypeDetail = $this->getNodeTypeDetail();

        $nodeTypeEntity = [
            'node_type_id'          => $nodeTypeDetail->node_type_id,
            'name'                  => !empty($nodeTypeDetail->title) ? $nodeTypeDetail->title : '',
            'updated_by'            => !empty($nodeTypeDetail->updatedBy->name) ? $nodeTypeDetail->updatedBy->name : "",
            'updated_at'            => $this->formatDateTimeToISO8601($nodeTypeDetail->updated_at),
        ];

        $this->setNodeTypeEntity($nodeTypeEntity);
    }
}
