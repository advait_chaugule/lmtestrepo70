<?php
namespace App\Domains\NodeType\Jobs;
use Illuminate\Support\Facades\DB;

use Lucid\Foundation\Job;

class SaveNodeTypeMetadataTenantJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nodeType, $metadata, $order, $isMandatory)
    {
        $this->setnodeType($nodeType);
        $this->setmetadata($metadata);
        $this->setorder($order);
        $this->setIsMandatory($isMandatory);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        
            $this->saveNodeTypeMetadata();
        
    }

    public function setnodeType($identifier) {
        $this->nodeType = $identifier;
    }

    public function getnodeType() {
        return $this->nodeType;
    }

    public function setmetadata($metadata){
        $this->metadata = $metadata;
    }

    public function getmetadata() {
        return $this->metadata;
    }

    public function setorder($order){
        $this->order = $order;
    }

    public function getorder() {
        return $this->order;
    }

    public function setIsMandatory($isMandatory){
        $this->isMandatory = $isMandatory;
    }

    public function getIsMandatory() {
        return $this->isMandatory;
    }    
     
    public function saveNodeTypeMetadata(){
        
        $nodeType = $this->getnodeType();
        $metadata = $this->getmetadata();
        $order = $this->getorder();
        $isMandatory = $this->getIsMandatory();

                $data[] = [
                    'node_type_id'  =>  $nodeType,
                    'metadata_id'   =>  $metadata,
                    'sort_order'    =>  $order,
                    'is_mandatory' => $isMandatory
                ];
                $return[] = [
                    'metadata_id' => $metadata
                ];  
        DB::table('node_type_metadata')->insert($data);
       return $return;

    }

}
