<?php
namespace App\Domains\NodeType\Jobs;
use App\Domains\NodeType\Validators\NodeTypeExistsValidator;

use Lucid\Foundation\Job;

class ValidateNodeTypeIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
