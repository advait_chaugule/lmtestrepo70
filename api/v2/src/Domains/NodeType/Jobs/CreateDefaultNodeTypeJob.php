<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class CreateDefaultNodeTypeJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //
        return $nodeTypeRepository->fillAndSave($this->input);
    }
}
