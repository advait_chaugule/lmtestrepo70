<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class CheckNodeTypeIsDeleteJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //Check whether Role deleted
        $attributes = ['node_type_id' => $this->identifier, 'is_deleted' => '1'];
        return $nodeTypeRepository->findByAttributes($attributes)->isNotEmpty();
    }
}
