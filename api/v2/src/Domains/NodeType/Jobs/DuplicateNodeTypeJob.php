<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class DuplicateNodeTypeJob extends Job
{
    use UuidHelperTrait, DateHelpersTrait;
    
    private $identifier;
    private $requestData;
    private $nodeTypeRepository;
    private $metadataSetTobeDuplicatedDetails;
    private $metadataSetDuplicatedDetails;
    private $nodeTypeEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $nodeTypeIdentifier, array $requestData)
    {
        //Set the node Type Identifier
        $this->setIdentifier($nodeTypeIdentifier);
        $this->requestData  =   $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        //Set the repo handler
        $this->nodeTypeRepository   =   $nodeTypeRepository;
        $nodeTypeDetails            =   $this->getNodeTypeToBeDuplicatedDetails();

        $savedNodeTypeId    =   $this->duplicateNodeTypeWithMetadata();
        $this->associateMetadataWithDuplicatedSet();
        
        $this->parseResponse();

        return $this->getNodeTypeEntity();
    }

    /**
     * Public Getter and Setter Methods
     */
    public function setIdentifier($identifier) {
        $this->identifier   =   $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setMetadataSetTobeDuplicatedDetails(array $data) {
        $this->metadataSetTobeDuplicatedDetails  =   $data;
    }

    public function getMetadataSetTobeDuplicatedDetails() {
        return $this->metadataSetTobeDuplicatedDetails;
    }

    public function setMetadataDuplicatedDetails($data) {
        $this->metadataSetDuplicatedDetails  =   $data;
    }

    public function getMetadataDuplicatedDetails() {
        return $this->metadataSetDuplicatedDetails;
    }

    public function setNodeTypeEntity($nodeTypeEntity){
        $this->nodeTypeEntity = $nodeTypeEntity;
    }

    public function getNodeTypeEntity(){
        return $this->nodeTypeEntity;
    }

    private function getNodeTypeToBeDuplicatedDetails() {
        $arrayOfMetadata    =   [];
        $nodeTypeIdentifier =   $this->getIdentifier();

        $returnCollection   =   false;
        $fieldsToReturn     =   ['node_type_id', 'title', 'organization_id'];
        $keyValuePairs      =   ['node_type_id' => $nodeTypeIdentifier];
        $relations          =   ['metadata'];

        $nodetypeToBeDuplicatedDetail   =   $this->nodeTypeRepository->findByAttributesWithSpecifiedFields(
            $returnCollection, $fieldsToReturn, $keyValuePairs, $relations);

        foreach($nodetypeToBeDuplicatedDetail->metadata as $nodeType) {
            $arrayOfMetadata[] = [
                'metadata_id'   =>  $nodeType->pivot->metadata_id,
                'sort_order'    =>  $nodeType->pivot->sort_order
            ];
        }

        $nodeTypeDetail =   [
            'title'             =>  $nodetypeToBeDuplicatedDetail->title,
            'organization_id'   =>  $nodetypeToBeDuplicatedDetail->organization_id,
            'metadata'          =>  $arrayOfMetadata
        ];

        $this->setMetadataSetTobeDuplicatedDetails($nodeTypeDetail);
    }

    private function duplicateNodeTypeWithMetadata() {
        $nodetypeToBeDuplicatedDetail   =   $this->getMetadataSetTobeDuplicatedDetails();
       
        $nodeTypeId = $this->createUniversalUniqueIdentifier();
        $inputData['node_type_id']          = $nodeTypeId;
        $inputData['organization_id']       = $nodetypeToBeDuplicatedDetail['organization_id'];
        $inputData['title']                 = 'Copy of '.$nodetypeToBeDuplicatedDetail['title'];
        $inputData['source_node_type_id']   = $nodeTypeId;
        $inputData['created_by']            = $this->requestData['auth_user']['user_id'];
        $inputData['updated_by']            = $this->requestData['auth_user']['user_id'];

        $savedNodeTypeData = $this->nodeTypeRepository->fillAndSave($inputData);
        $this->setMetadataDuplicatedDetails($savedNodeTypeData);

        return $savedNodeTypeData->node_type_id;
    }

    private function associateMetadataWithDuplicatedSet() {
        $metadataSetTobeDuplicatedDetails   =   $this->getMetadataSetTobeDuplicatedDetails();

        $metadataSetDuplicatedDetails       =   $this->getMetadataDuplicatedDetails();

        foreach($metadataSetTobeDuplicatedDetails['metadata'] as $metadata) {
            $metadataSetDuplicatedDetails->metadata()->attach($metadata['metadata_id'],['sort_order' => $metadata['sort_order']]);
        }
    }

    private function parseResponse() {
        $savedNodeTypeData = $this->getMetadataDuplicatedDetails();

        $nodeTypeEntity = [
            "node_type_id"  =>  $savedNodeTypeData->node_type_id,
            "name"          =>  $savedNodeTypeData->title,
            "created_at"    =>  $this->formatDateTimeToISO8601($savedNodeTypeData->created_at),
            "upadated_at"   =>  $this->formatDateTimeToISO8601($savedNodeTypeData->updated_at)
        ];

        $this->setNodeTypeEntity($nodeTypeEntity);
    }
}
