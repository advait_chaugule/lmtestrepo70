<?php
namespace App\Domains\NodeType\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class CheckNodeTypeIsDocumentOrDefaultJob extends Job
{
    private $nodeTypeId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->nodeTypeId = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('node_types')
                        ->where('is_deleted', 0)
                        ->where('used_for', 0)
                        ->where('node_type_id', '=', $this->nodeTypeId);
        $query->where(function ($query) {
            $query->where('is_default','=','1')->orWhere('is_document', '=', '1');
        });
        $nodeType = $query->get()->toArray();
        if (empty($nodeType)) {
            return false;
        }
        return true;
    }
}
