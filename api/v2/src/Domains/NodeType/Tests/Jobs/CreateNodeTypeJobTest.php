<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\CreateNodeTypeJob;
use Tests\TestCase;

class CreateNodeTypeJobTest extends TestCase
{
    public function test_SavedNodeTypeData()
    {
        $assets = new CreateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setSavedNodeTypeData($value);
        $this->assertEquals($value,$assets->getSavedNodeTypeData());
    }

    public function test_NodeTypeEntity()
    {
        $assets = new CreateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeEntity($value);
        $this->assertEquals($value,$assets->getNodeTypeEntity());
    }
}
