<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\SaveNodeTypeMetadataTenantJob;
use Tests\TestCase;

class SaveNodeTypeMetadataTenantJobTest extends TestCase
{
    public function test_nodeType()
    {
        $assets = new SaveNodeTypeMetadataTenantJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1','ec78811f-2822-457c-84e3-94e1ac7f2f7e','23878b09-96e1-4fd5-b913-823163ded50e');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setnodeType($value);
        $this->assertEquals($value,$assets->getnodeType());
    }

    public function test_metadata()
    {
        $assets = new SaveNodeTypeMetadataTenantJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1','ec78811f-2822-457c-84e3-94e1ac7f2f7e','23878b09-96e1-4fd5-b913-823163ded50e');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setmetadata($value);
        $this->assertEquals($value,$assets->getmetadata());
    }

    public function test_order()
    {
        $assets = new SaveNodeTypeMetadataTenantJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1','ec78811f-2822-457c-84e3-94e1ac7f2f7e','23878b09-96e1-4fd5-b913-823163ded50e');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setorder($value);
        $this->assertEquals($value,$assets->getorder());
    }

    public function test_IsMandatory()
    {
        $assets = new SaveNodeTypeMetadataTenantJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1','ec78811f-2822-457c-84e3-94e1ac7f2f7e','23878b09-96e1-4fd5-b913-823163ded50e');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIsMandatory($value);
        $this->assertEquals($value,$assets->getIsMandatory());
    }
}
