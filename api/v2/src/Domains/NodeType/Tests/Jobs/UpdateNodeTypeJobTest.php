<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\UpdateNodeTypeJob;
use Tests\TestCase;

class UpdateNodeTypeJobTest extends TestCase
{
    public function test_NodeTypeIdentifier()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new UpdateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeIdentifier($value);
        $this->assertEquals($value,$assets->getNodeTypeIdentifier());
    }

    public function test_InputData()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new UpdateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setInputData($value);
        $this->assertEquals($value,$assets->getInputData());
    }

    public function test_NodeTypeEntity()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new UpdateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeEntity($value);
        $this->assertEquals($value,$assets->getNodeTypeEntity());
    }
}
