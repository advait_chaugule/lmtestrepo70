<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\DuplicateNodeTypeJob;
use Tests\TestCase;

class DuplicateNodeTypeJobTest extends TestCase
{
    public function test_Identifier()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new DuplicateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_MetadataDuplicatedDetails()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new DuplicateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataDuplicatedDetails($value);
        $this->assertEquals($value,$assets->getMetadataDuplicatedDetails());
    }

    public function test_NodeTypeEntity()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new DuplicateNodeTypeJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeEntity($value);
        $this->assertEquals($value,$assets->getNodeTypeEntity());
    }
}
