<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\GetNodeTypeDetailsJob;
use Tests\TestCase;

class GetNodeTypeDetailsJobTest extends TestCase
{
    public function test_NodeTypeIdentifier()
    {
        $assets = new GetNodeTypeDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeIdentifier($value);
        $this->assertEquals($value,$assets->getNodeTypeIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new GetNodeTypeDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_NodeTypeDetail()
    {
        $assets = new GetNodeTypeDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeDetail($value);
        $this->assertEquals($value,$assets->getNodeTypeDetail());
    }

    public function test_NodeTypeEntity()
    {
        $assets = new GetNodeTypeDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeEntity($value);
        $this->assertEquals($value,$assets->getNodeTypeEntity());
    }
}
