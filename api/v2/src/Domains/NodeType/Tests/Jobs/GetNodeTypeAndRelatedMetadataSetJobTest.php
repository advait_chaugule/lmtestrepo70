<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\GetNodeTypeAndRelatedMetadataSetJob;
use Tests\TestCase;

class GetNodeTypeAndRelatedMetadataSetJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $assets = new GetNodeTypeAndRelatedMetadataSetJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_NodeTypeFromDb()
    {
        $assets = new GetNodeTypeAndRelatedMetadataSetJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeFromDb($value);
        $this->assertEquals($value,$assets->getNodeTypeFromDb());
    }
}
