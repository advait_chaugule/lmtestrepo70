<?php
namespace App\Domains\NodeType\Tests\Jobs;

use App\Domains\NodeType\Jobs\SaveNodeTypeMetadataJob;
use Tests\TestCase;

class SaveNodeTypeMetadataJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new SaveNodeTypeMetadataJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_RequestData()
    {
        $assets = new SaveNodeTypeMetadataJob('c4424f55-8900-446c-9c41-2f2c25d58c08','ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setRequestData($value);
        $this->assertEquals($value,$assets->getRequestData());
    }
}
