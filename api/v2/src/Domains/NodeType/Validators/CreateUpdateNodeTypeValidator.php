<?php
namespace App\Domains\NodeType\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;
use Illuminate\Support\Facades\DB;

class CreateUpdateNodeTypeValidator extends BaseValidator {

    protected $rules = [
        'organization_id' => 'required|exists:organizations,organization_id',
        'title' => 'required|string|max:50',
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'string' => ':attribute should be of type string.',
        'max:50' => ':attribute should be of maximum 50 characters.',
     ];

    # Check if metadataset title is unique and not deleted, as deleted ones are allowed to be created again
    public function validateIsDeleted($input)
    {
        $row_count = DB::table('node_types')
                        ->where('is_deleted', 0)
                        ->where('used_for', 0)
                        ->where('title', $input['title'])
                        ->where('organization_id', $input['organization_id'])
                        ->get()
                        ->count();
        if($row_count) {
            return false;
        }
        return true;
    }

}