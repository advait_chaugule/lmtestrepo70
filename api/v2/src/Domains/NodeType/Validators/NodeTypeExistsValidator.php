<?php
namespace App\Domains\NodeType\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;
use Illuminate\Support\Facades\DB;

class NodeTypeExistsValidator extends BaseValidator {

    protected $rules = [
        'node_type_id' => "required|exists:node_types,node_type_id,is_deleted,0"
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];

    # Check if metadataset title is unique and not deleted, as deleted ones are allowed to be created again, and not current one
    public function validateIsDeletedNotCurrentId($input)
    {
        $row_count = DB::table('node_types')
                        ->where('is_deleted', 0)
                        ->where('used_for', 0)
                        ->where('title', $input['title'])
                        ->where('node_type_id', '!=', $input['node_type_id'])
                        ->where('organization_id', $input['organization_id'])
                        ->get()
                        ->count();

        if ($row_count) {
            return false;
        }
        return true;
    }
}