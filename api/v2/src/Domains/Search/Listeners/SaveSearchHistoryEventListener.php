<?php

namespace App\Domains\Search\Listeners;

use App\Domains\Search\Events\SaveSearchHistoryEvent as Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

use App\Data\Repositories\Contracts\SearchHistoryEloquentRepositoryInterface;

class SaveSearchHistoryEventListener
{
    private $searchHistoryRepository;

    private $searchHistoryToSave;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SearchHistoryEloquentRepositoryInterface $searchHistoryRepository)
    {
        $this->searchHistoryRepository = $searchHistoryRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        try {
            $eventData = $event->eventData;
            $this->searchHistoryToSave = $eventData['search_history'];
            $this->searchHistoryRepository->saveData($this->searchHistoryToSave);
        }
        catch(\Exception $exception) {
            // since it's scyncronous flow, don't break the script if any exception occurs
            // dd($exception->getMessage());
            return true;
        }
    }
}
