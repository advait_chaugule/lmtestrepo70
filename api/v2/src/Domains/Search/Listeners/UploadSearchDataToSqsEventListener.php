<?php

namespace App\Domains\Search\Listeners;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent as Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

// base class containing AWS sdk SQS client related stuff
use App\Foundation\BaseSqsHandler;

class UploadSearchDataToSqsEventListener extends BaseSqsHandler
{

    private $dataToUpload;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function setDataToUpload(array $data) {
        $this->dataToUpload = $data;
    }

    public function getDataToUpload(): array {
        return $this->dataToUpload;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        try {
            $eventData = $event->eventData;
            $this->setDataToUpload($eventData);
            $this->uploadDataToSqs();
        }
        catch(\Exception $exception) {
            // since it's scyncronous flow, don't break the script if any exception occurs
            // dd($exception->getMessage());
            return true;
        }
    }

    private function uploadDataToSqs() {
        $sqsQueueUrl = $this->getSqsQueueUrl();
        $sqsClient = $this->getSqsClient();
        $dataToUpload = $this->getDataToUpload();
        if(!empty($dataToUpload)) {
            $messageBody = json_encode($dataToUpload);
            $sqsClientParams = [
                'MessageBody' => $messageBody,
                'QueueUrl' => $sqsQueueUrl
            ];
            $uploadResponse = $sqsClient->sendMessage($sqsClientParams);
            // only for POC testing (will delete later)
            // $responseMetadata = $uploadResponse->get('@metadata');
            // if(!empty($responseMetadata["statusCode"]) && $responseMetadata["statusCode"]===200) {
            // }
            //Log::info($uploadResponse->get('@metadata'));
        }
    }
}
