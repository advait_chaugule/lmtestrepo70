<?php
namespace App\Domains\Search\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class SaveSearchInputValidator extends BaseValidator {

    protected $rules = [
        'title' => 'nullable|string',
        'description' => 'nullable|string',
        'search_term' => 'required|string',
        // 'search_type' => 'nullable|string|in:document,item,project,project_pacing_guide',
        //'search_optional_filters.*.filter_type' => 'nullable|in:type,document_id,node_type_id,adoption_status,item_association_type,item_metadata,document_metadata,item_custom_metadata,updated_by,update_on',
        'search_optional_filters.*.filter_key' => 'required_if:search_optional_filters.*.filter_type,item_metadata,document_metadata,item_custom_metadata',
        'search_optional_filters.*.filter_value' => 'nullable', //'required_with:search_optional_filters.*.filter_type',
    ];

     protected $messages = [

     ];
    
}