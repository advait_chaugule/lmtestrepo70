<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Services\Api\Traits\StringHelper;

use App\Domains\Search\Validators\SearchInputValidator as Validator;

class ValidateSearchInputParametersJob extends Job
{

    use StringHelper;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request, Validator $validator)
    {
        $requestData = $request->all();
        $validation = $validator->validate($requestData);
        return $validation===true ? $validation : $this->validatorMessageParser($validation->messages()->getMessages());
    }
}
