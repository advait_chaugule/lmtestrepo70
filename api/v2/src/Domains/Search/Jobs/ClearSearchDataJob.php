<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;

use App\Foundation\BaseCloudSearchHandler;

use Aws\CloudSearchDomain\CloudSearchDomainClient;

class ClearSearchDataJob extends Job
{

    private $cloudSearchHandler;
    private $searchConfigurations;
    protected $searchClient;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function setSearchConfigurations() {
        $this->searchConfigurations = $this->cloudSearchHandler->getSearchConfigurations();
    }

    public function getSearchConfigurations(): array {
        return $this->searchConfigurations;
    }

    public function setSearchClient() {
        $this->searchClient = $this->cloudSearchHandler->getSearchClient();
    }

    public function getSearchClient(): CloudSearchDomainClient {
        return $this->searchClient;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BaseCloudSearchHandler $cloudSearchHandler)
    {
        $this->cloudSearchHandler = $cloudSearchHandler;
        $this->setSearchConfigurations();
        $this->setSearchClient();
    }
}
