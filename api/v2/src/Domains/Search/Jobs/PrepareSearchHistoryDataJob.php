<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Http\Request;

class PrepareSearchHistoryDataJob extends Job
{
    use UuidHelperTrait;

    // private $query;
    private $filterQuery;
    private $title;
    private $description;
    private $userId;
    private $organizationId;
    private $isExplicitSaved;
    private $searchFilters;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $searchDetails)
    {
        // $this->query = $searchDetails['query'];
        $this->filterQuery = $searchDetails['filter_query'];
        $this->title = $searchDetails['title'];
        $this->description = $searchDetails['description'];
        $this->userId = $searchDetails['request_user']['user_id'];
        $this->organizationId = $searchDetails['request_user']['organization_id'];
        $this->isExplicitSaved = $searchDetails['is_explicit_saved'];
        $this->searchFilters = $searchDetails['request_filters'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {

        $searchTerm = !empty($request->input('search_term')) ? $request->input('search_term') : "";

        $searchContextPage = !empty($request->input('context_page')) ? $request->input('context_page') : "";

        $searchId = $this->createUniversalUniqueIdentifier();
        $currentDateTime = now()->toDateTimeString();
        $filterJson = json_encode(
            [
                'search_term' => $searchTerm,
                'filter_query' => $this->filterQuery,
                'search_optional_filters' => $this->searchFilters
            ]
        );
        $searchHistoryData = [
            'search_id' => $searchId,
            'title' => $this->title,
            'description' => $this->description,
            'search_filter' => $filterJson,
            'is_explicit_saved' => $this->isExplicitSaved,
            'context_page' => $searchContextPage,
            'organization_id' => $this->organizationId,
            'created_by' => $this->userId,
            'updated_at' => $currentDateTime,
        ];
        return $searchHistoryData;
    }
}
