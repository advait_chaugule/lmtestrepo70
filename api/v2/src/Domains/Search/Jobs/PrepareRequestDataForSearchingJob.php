<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class PrepareRequestDataForSearchingJob extends Job
{
    use StringHelper, DateHelpersTrait;
    
    private $allowedTypeFilterTypeValue;
    private $allowedUpdatedOnFilterTypeValue;
    private $allowedAdoptionStatusFilterTypeValue;
    private $allowedItemAssociationTypeFilterTypeValue;
    private $allowedDefaultFilterTypeValue;
    private $searchableFields;

    private $requestData;
    private $requestUserOrganizationId;
    private $searchTerm;
    // private $searchType;
    private $searchFilters;
    private $structuredFilterQuery;
    private $queryOptions;
    private $jobResponse;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->allowedTypeFilterTypeValue = [ 'document', 'item', 'project' ];
        $this->allowedUpdatedOnFilterTypeValue = ['today', 'last_seven_days', 'last_thirty_days'];
        $this->allowedAdoptionStatusFilterTypeValue = range(1, 4);
        $this->allowedItemAssociationTypeFilterTypeValue = range(1, 9);
        $this->allowedDefaultFilterTypeValue = [ 'organization_identifier' ];
        $this->setSearchableFields();
    }

    public function setSearchableFields() {
        $this->searchableFields = config('_search.taxonomy.search_fields');
    }

    public function getSearchableFields(): array {
        return $this->searchableFields;
    }

    /**********************************Setters and Getters for search request inputs*************************************/

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    public function setRequestUserOrganizationId(string $data) {
        $this->requestUserOrganizationId = $data;
    }

    public function getRequestUserOrganizationId(): string {
        return $this->requestUserOrganizationId;
    }

    public function setSearchTerm(string $data) {
        $this->searchTerm = $data;
    }

    public function getSearchTerm(): string {
        return trim($this->searchTerm);
    }

    // public function setSearchType(string $data) {
    //     $this->searchType = $data;
    // }

    // public function getSearchType(): string {
    //     return $this->searchType;
    // }

    public function setSearchFilters(array $data) {
        $this->searchFilters = $data;
    }

    public function getSearchFilters(): array {
        return $this->searchFilters;
    }

    /*********************************Setters and getters for structured queries and options****************************************8*/

    public function setStructuredFilterQuery(string $data) {
        $this->structuredFilterQuery = $data;
    }

    public function getStructuredFilterQuery(): string {
        return $this->structuredFilterQuery;
    }

    public function setQueryOptions(array $data){
        $this->queryOptions = $data;
    }
    
    public function getQueryOptions(): array{
        return $this->queryOptions;
    }
    
    public function setJobResponse(array $data){
        $this->jobResponse = $data;
    }
    
    public function getJobResponse(): array{
        return $this->jobResponse;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $requestData = $request->all();
        $this->setRequestData($requestData);
        $this->extractAndSetSearchRequestDetails();
        $this->prepareStructuredQueryFromFilters();
        $this->prepareJobResponse();
        return $this->getJobResponse();
    }

    private function extractAndSetSearchRequestDetails() {
        $requestData = $this->getRequestData();

        // set request user organization id
        $organizationId = !empty($requestData['auth_user']['organization_id']) ? $requestData['auth_user']['organization_id'] : '';
        $this->setRequestUserOrganizationId($organizationId);

        // set search term after replacing all special characters with # and finally replacing hash with space
        if(!empty($requestData['search_term'])) {
            //$cleanSearchTermFromSepcialCharacters = $this->replaceSpecialCharactersFromString($requestData['search_term'], "#");
           // $properlySpacesSearchString = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $cleanSearchTermFromSepcialCharacters ));
            $properlySpacesSearchString = $requestData['search_term'];
            //$searchTerm = $properlySpacesSearchString."*";
           $searchTerm = '"'.$properlySpacesSearchString.'"';
        }
        else {
            $searchTerm = $organizationId;
        }
        $this->setSearchTerm($searchTerm);

        // set search type
        // $searchType = !empty($requestData['search_type']) ? $requestData['search_type'] : "";
        // $this->setSearchType($searchType);

        // set request search filters
        $searchFilters = !empty($requestData['search_optional_filters']) ? $requestData['search_optional_filters'] : [];
        $this->setSearchFilters($searchFilters);
    }

    private function prepareStructuredQueryFromFilters() {
        $extraFilters = [];
        $organizationId = $this->getRequestUserOrganizationId();
        // $searchType = $this->getSearchType();
        // prepare default filters
        $organizationFilter = [
            'filter_type' => 'organization_identifier',
            'filter_key' => '',
            'filter_value' => $organizationId
        ];
        array_push($extraFilters, $organizationFilter);

        // if($searchType!=='') {
        //     $typeFilter = [
        //         'filter_type' => 'type',
        //         'filter_key' => '',
        //         'filter_value' => $searchType
        //     ];
        //     array_push($extraFilters, $typeFilter);
        // }

        $searchFilters = $this->getSearchFilters();
        $filters = array_merge($extraFilters, $searchFilters);

        $typeFilterQueries = [];
        $documentIdFilterQueries = [];
        $nodeTypeIdFilterQueries = [];
        $adoptionStatusFilterQueries = [];
        $itemAssociationTypeFilterQueries = [];
        $itemMetadataFilterQueries = [];
        $documentMetadataFilterQueries = [];
        $itemCustomMetadataFilterQueries = [];
        $updatedByFilterQueries = [];
        $updatedOnFilterQueries = [];
        $projectIdFilterQueries = [];
        $projectWorkflowIdFilterQueries = [];
        $defaultFilterQueries = [];
        $fiterQuerySets = [];
        foreach($filters as $filter) {
            $filterType = !empty($filter['filter_type']) ? $filter['filter_type'] : "";
            $filterKey = !empty($filter['filter_key']) ? $filter['filter_key'] : "";
            $filterValue = isset($filter['filter_value']) ? $filter['filter_value'] : "";
            if($filterType!=='') {
                switch ($filterType) {
                    case 'type':
                        if(in_array($filterValue, $this->allowedTypeFilterTypeValue)){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($typeFilterQueries, $query);
                        }
                        break;
                    case 'document_id':
                        if($filterValue!==''){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($documentIdFilterQueries, $query);
                        }
                        break;
                    case 'node_type_id':
                        if($filterValue!==''){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($nodeTypeIdFilterQueries, $query);
                        }
                        break;
                    case 'adoption_status':
                        if(in_array($filterValue, $this->allowedAdoptionStatusFilterTypeValue)) {
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($adoptionStatusFilterQueries, $query);
                        }
                        break;
                    case 'item_association_type':
                        if(in_array($filterValue, $this->allowedItemAssociationTypeFilterTypeValue)){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($itemAssociationTypeFilterQueries, $query);
                        }
                        break;
                    case 'item_metadata':
                        if($filterKey!==''){
                            $filterValue = $filterValue!=='' ? $filterValue."*" : "*";
                            $query = "(term field=$filterType '$filterKey:$filterValue')";
                            array_push($itemMetadataFilterQueries, $query);
                        }
                        break;
                    case 'document_metadata':
                        if($filterKey!==''){
                            $filterValue = $filterValue!=='' ? $filterValue."*" : "*";
                            $query = "(term field=$filterType '$filterKey:$filterValue')";
                            array_push($documentMetadataFilterQueries, $query);
                        }
                        break;
                    case 'item_custom_metadata':
                        if($filterKey!==''){
                            $filterValue = $filterValue!=='' ? $filterValue."*" : "*";
                            $query = "(term field=$filterType '$filterKey:$filterValue')";
                            array_push($itemCustomMetadataFilterQueries, $query);
                        }
                        break;
                    case 'updated_by':
                        if($filterValue!==''){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($updatedByFilterQueries, $query);
                        }
                        break;
                    case 'update_on':
                        if(in_array($filterValue, $this->allowedUpdatedOnFilterTypeValue)) {
                            $query = $this->_helperToCreateUpdatedOnQuery($filterType, $filterValue);
                            array_push($updatedOnFilterQueries, $query);
                        }
                        break;
                    case 'project_id':
                        if($filterValue!==''){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($projectIdFilterQueries, $query);
                        }
                        break;
                    case 'project_workflow_id':
                        if($filterValue!==''){
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($projectWorkflowIdFilterQueries, $query);
                        }
                        break;
                    default:
                        // set filter query for default organization and type filter
                        if(in_array($filterType, $this->allowedDefaultFilterTypeValue) && $filterValue!=='') {
                            $query = "(term field=$filterType '$filterValue')";
                            array_push($defaultFilterQueries, $query);
                        }
                        break;
                }
            }
        }

        $typeFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($typeFilterQueries);
        if($typeFilterQueryString!=='')
            array_push($fiterQuerySets, $typeFilterQueryString);

        // parse all indivdial filters and build the query set
        $documentIdFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($documentIdFilterQueries);
        if($documentIdFilterQueryString!=='')
            array_push($fiterQuerySets, $documentIdFilterQueryString);

        $nodeTypeIdFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($nodeTypeIdFilterQueries);
        if($nodeTypeIdFilterQueryString!=='')
            array_push($fiterQuerySets, $nodeTypeIdFilterQueryString);

        $adoptionStatusFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($adoptionStatusFilterQueries);
        if($adoptionStatusFilterQueryString!=='')
            array_push($fiterQuerySets, $adoptionStatusFilterQueryString);

        $itemAssociationTypeFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($itemAssociationTypeFilterQueries);
        if($itemAssociationTypeFilterQueryString!=='')
            array_push($fiterQuerySets, $itemAssociationTypeFilterQueryString);

        $itemMetadataFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($itemMetadataFilterQueries);
        if($itemMetadataFilterQueryString!=='')
            array_push($fiterQuerySets, $itemMetadataFilterQueryString);

        $documentMetadataFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($documentMetadataFilterQueries);
        if($documentMetadataFilterQueryString!=='')
            array_push($fiterQuerySets, $documentMetadataFilterQueryString);

        $itemCustomMetadataFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($itemCustomMetadataFilterQueries);
        if($itemCustomMetadataFilterQueryString!=='')
            array_push($fiterQuerySets, $itemCustomMetadataFilterQueryString);

        $updatedByFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($updatedByFilterQueries);
        if($updatedByFilterQueryString!=='')
            array_push($fiterQuerySets, $updatedByFilterQueryString);

        $updatedOnFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($updatedOnFilterQueries);
        if($updatedOnFilterQueryString!=='')
            array_push($fiterQuerySets, $updatedOnFilterQueryString);

        $projectIdFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($projectIdFilterQueries);
        if($projectIdFilterQueryString!=='')
            array_push($fiterQuerySets, $projectIdFilterQueryString);

        $projectWorkflowIdFilterQueryString = $this->_helperToGroupSimilarQueryWithOrOperator($projectWorkflowIdFilterQueries);
        if($projectWorkflowIdFilterQueryString!=='')
            array_push($fiterQuerySets, $projectWorkflowIdFilterQueryString);

        $defaultQueryString = trim($this->arrayContentToDelimittedString($defaultFilterQueries, ' '));
        array_push($fiterQuerySets, $defaultQueryString);

        // finally build the filter query with and operator
        $filterQueryString = $this->arrayContentToDelimittedString($fiterQuerySets, ' ');
        $structuredFilteredQuery = "(and $filterQueryString)";
        $this->setStructuredFilterQuery($structuredFilteredQuery);
    }

    private function prepareJobResponse() {
        $searchTerm = $this->getSearchTerm();
        $structuredFilteredQuery = $this->getStructuredFilterQuery();
        $searchableFields = $this->getSearchableFields();
        $jobResponse = [
            'query' => $searchTerm,
            'filter_query' => $structuredFilteredQuery,
            'searchable_fields' => $searchableFields
        ];
        $this->setJobResponse($jobResponse);
    }

    private function _helperToCreateUpdatedOnQuery(string $filterType, string $filterValue): string {
        $currentDateTimeCarbonObject = now();
        switch ($filterValue) {
            case 'today':
                    $startDateTime = $currentDateTimeCarbonObject->today()->toDateTimeString();
                    $cloudSearchCompatibleStartDateTime = $this->formatDateTimeToCloudSarchCompatibleFormat($startDateTime);
                    $dateRange = "['$cloudSearchCompatibleStartDateTime',}";
                    break;
            case 'last_seven_days':
                    $startDateTime = $currentDateTimeCarbonObject->subDays(7);
                    //$endDateTime = $currentDateTimeCarbonObject::today()->toDateTimeString();
                    $cloudSearchCompatibleStartDateTime = $this->formatDateTimeToCloudSarchCompatibleFormat($startDateTime);
                    //$cloudSearchCompatibleEndDateTime = $this->formatDateTimeToCloudSarchCompatibleFormat($endDateTime);
                    //$dateRange = "['$cloudSearchCompatibleStartDateTime', '$cloudSearchCompatibleEndDateTime']";
                    $dateRange = "['$cloudSearchCompatibleStartDateTime',}";
                    break;
            case 'last_thirty_days':
                    $startDateTime = $currentDateTimeCarbonObject->subDays(30);
                    $cloudSearchCompatibleStartDateTime = $this->formatDateTimeToCloudSarchCompatibleFormat($startDateTime);
                    $dateRange = "['$cloudSearchCompatibleStartDateTime',}";
                    break;
            default:
                    $startDateTime = $currentDateTimeCarbonObject->subDays(365);
                    $cloudSearchCompatibleStartDateTime = $this->formatDateTimeToCloudSarchCompatibleFormat($startDateTime);
                    $dateRange = "['$cloudSearchCompatibleStartDateTime',}";
                    break;
        }
        $query = "(range field=$filterType $dateRange)";
        return $query;
    }

    private function _helperToGroupSimilarQueryWithOrOperator(array $querySets): string {
        $queryCount = count($querySets);
        $queryString = $this->arrayContentToDelimittedString($querySets, ' ');
        $queryToReturn = $queryCount > 1 ? "(or $queryString)" : $queryString;
        return trim($queryToReturn);
    }
}
