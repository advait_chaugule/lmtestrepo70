<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\SearchHistoryEloquentRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

class FetchSearchHistoryJob extends Job
{
    use ArrayHelper, StringHelper;

    private $searchRepository;

    private $requestUserId;
    private $limit;
    private $contextPageFilter;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request, SearchHistoryEloquentRepositoryInterface $searchRepository)
    {
        $this->searchRepository = $searchRepository;
        $this->limit = (!empty($request->input('record_count')) && is_numeric($request->input('record_count'))) ? 
                    $request->input('record_count') : 
                    0;
        $this->requestUserId = $request->input('auth_user.user_id');
        $this->contextPageFilter = !empty($request->input('context_page')) ? $request->input('context_page') : "";

        $implicitlySavedSearchHistory = $this->fetchImplicitlySavedSearches();
        $explicitlySavedSearchHistory = $this->fetchExplicitlySavedSearches();

        $parsedImplicitlySavedSearches = $this->parseAndReturnSearchHistory($implicitlySavedSearchHistory);
        $parsedExplicitlySavedSearches = $this->parseAndReturnSearchHistory($explicitlySavedSearchHistory);

        $jobResponse = [
            'recent_searches' => $parsedImplicitlySavedSearches,
            'saved_searches' => $parsedExplicitlySavedSearches
        ];

        return $jobResponse;
    }

    private function fetchImplicitlySavedSearches(): Collection {
        $conditionalClause = [ 'created_by' => $this->requestUserId , 'is_explicit_saved' => 0 ];
        
        if(!empty($this->contextPageFilter)) {
            $conditionalClause['context_page'] = $this->contextPageFilter;
        }

        $returnPaginatedData = $this->limit > 0;
        $paginationFilters = $returnPaginatedData ?  ['limit' => $this->limit, 'offset' => 0] : [];
        $returnSortedData = true;
        return $this->searchRepository->findByAttributes($conditionalClause, $returnPaginatedData, $paginationFilters, $returnSortedData);
    }

    private function fetchExplicitlySavedSearches(): Collection {
        $conditionalClause = [ 'created_by' => $this->requestUserId, 'is_explicit_saved' => 1 ];
        
        if(!empty($this->contextPageFilter)) {
            $conditionalClause['context_page'] = $this->contextPageFilter;
        }
        
        $returnPaginatedData = $this->limit > 0;
        $paginationFilters = $returnPaginatedData ?  ['limit' => $this->limit, 'offset' => 0] : [];
        $returnSortedData = true;
        return $this->searchRepository->findByAttributes($conditionalClause, $returnPaginatedData, $paginationFilters, $returnSortedData);
    }

    private function parseAndReturnSearchHistory(Collection $searchHistory): array {
        $parsedDataToReturn = [];
        foreach($searchHistory as $searchHistory) {
            $searchId = $searchHistory->search_id;
            $title = $searchHistory->title ?: "";
            $description = $searchHistory->description ?: "";
            $organizationId = $searchHistory->organization_id ?: "";
            $createdBy = $searchHistory->created_by ?: "";
            $updatedAt = !empty($searchHistory->updated_at) ? $searchHistory->updated_at : "";
            $searchFilter = $searchHistory->search_filter;
            $searchQueryDetails = $this->parseAndReturnSearchQueryDetails($searchFilter);
            if(!empty($searchQueryDetails)) {
                $parsedSearchHistory = [
                    'search_id' => $searchId,
                    'title' => $title,
                    'description' => $description,
                    'organization_id' => $organizationId,
                    'created_by' => $createdBy,
                    'updated_at' => $updatedAt,
                    'search_details' => $searchQueryDetails
                ];
                $parsedDataToReturn[] = $parsedSearchHistory;
            }
        }
        return $parsedDataToReturn;
    }

    private function parseAndReturnSearchQueryDetails(string $searchQueryDetailsJson): array {
        $dataToReturn = [];
        $isjsonValid = $this->validateJsonString($searchQueryDetailsJson);
        if($isjsonValid===true) {
            $dataToReturn = $this->convertJsonStringToArray($searchQueryDetailsJson);
            if(!empty($dataToReturn['query']) && !empty($dataToReturn['search_filters'])) {
                $dataToReturn['query'] = str_replace("*", "", $dataToReturn['query']);
                $dataToReturn['search_filters'] = array_values($dataToReturn['search_filters']);
            }
        }
        return $dataToReturn;
    }
}
