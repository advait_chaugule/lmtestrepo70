<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Foundation\BaseCloudSearchHandler;
use Aws\CloudSearchDomain\CloudSearchDomainClient;
// use Aws\CloudSearchDomain\Exception\CloudSearchDomainException as Exception;

class UploadSearchDataJob extends Job
{

    private $cloudSearchHandler;
    private $searchConfigurations;
    private $searchClient;
    private $chunkCount;
    private $processHaltTimeInSeconds;
    private $searchDataMainBatch;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $searchDataMainBatch)
    {
        // set the main batch to process and upload
        $this->setSearchDataMainBatch($searchDataMainBatch);

    }

    public function setSearchConfigurations() {
        $this->searchConfigurations = $this->cloudSearchHandler->getSearchConfigurations();
    }

    public function getSearchConfigurations(): array {
        return $this->searchConfigurations;
    }

    public function setSearchClient() {
        $this->searchClient = $this->cloudSearchHandler->getSearchClient();
    }

    public function getSearchClient(): CloudSearchDomainClient {
        return $this->searchClient;
    }

    public function setChunkCount(array $data) {
        $this->chunkCount = $data['BATCH_CHUNK_COUNT'];
    }

    public function getChunkCount(): int {
        return $this->chunkCount;
    }

    public function setProcessHaltTimeInSeconds(array $data) {
        $this->processHaltTimeInSeconds = $data['CHUNKED_BATCH_EXECUTION_DELAY_IN_SECONDS'];
    }

    public function getProcessHaltTimeInSeconds(): int {
        return $this->processHaltTimeInSeconds;
    }

    public function setSearchDataMainBatch(Collection $data) {
        $this->searchDataMainBatch = $data;
    }

    public function getSearchDataMainBatch(): Collection {
        return $this->searchDataMainBatch;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BaseCloudSearchHandler $cloudSearchHandler)
    {
        // set the cloud search handler
        $this->cloudSearchHandler = $cloudSearchHandler;

        // set cloud search configs and client
        $this->setSearchConfigurations();
        $this->setSearchClient();

        // set non-cloud-search configs
        $searchConfigs = $this->getSearchConfigurations();
        $this->setChunkCount($searchConfigs);
        $this->setProcessHaltTimeInSeconds($searchConfigs);

        // start the upload process
        $this->startUploadProcess();
    }

    private function startUploadProcess() {
        $chunkCount = $this->getChunkCount();
        $processHaltTimeInSeconds = $this->getProcessHaltTimeInSeconds();
        $searchDataMainBatch = $this->getSearchDataMainBatch();
        foreach($searchDataMainBatch->chunk($chunkCount) as $chunk) {
            $this->uploadChunkedBatchDocument($chunk);
            // hold upload process after each chunk execution for search optimization
            //sleep($processHaltTimeInSeconds);
            usleep($processHaltTimeInSeconds * 1000);
        }
    }

    private function uploadChunkedBatchDocument(Collection $chunkedData) {
        $searchClient = $this->getSearchClient();
        $searchBatchDataChunk = json_encode($chunkedData->values()->all());
        $upload =   $searchClient->uploadDocuments([
                        'contentType' => 'application/json',
                        'documents' => $searchBatchDataChunk,
                    ]);
        // dd($upload);
    }
}
