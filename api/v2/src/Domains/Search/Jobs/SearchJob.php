<?php
namespace App\Domains\Search\Jobs;

use Lucid\Foundation\Job;

use App\Foundation\BaseCloudSearchHandler;

use Aws\CloudSearchDomain\CloudSearchDomainClient;

class SearchJob extends Job
{

    private $cloudSearchHandler;

    private $searchQuery;
    private $filterQuery;
    private $searchableFields;
    private $searchClient;
    private $searchResult;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $searchData)
    {
        $this->setSearchQuery($searchData);
        $this->setFilterQuery($searchData);
        $this->setSearchableFields($searchData);
    }

    public function setSearchQuery(array $data) {
        $this->searchQuery = $data['query'];
    }

    public function getSearchQuery(): string {
        return $this->searchQuery;
    }

    public function setFilterQuery(array $data) {
        $this->filterQuery = $data['filter_query'];
    }

    public function getFilterQuery(): string {
        return $this->filterQuery;
    }

    public function setSearchableFields(array $data) {
        $this->searchableFields = $data['searchable_fields'];
    }

    public function getSearchableFields(): array {
        return $this->searchableFields;
    }

    public function setSearchClient() {
        $this->searchClient = $this->cloudSearchHandler->getSearchClient();
    }

    public function getSearchClient(): CloudSearchDomainClient {
        return $this->searchClient;
    }

    public function setSearchResult(array $data) {
        $this->searchResult = $data;
    }

    public function getSearchResult(): array {
        return $this->searchResult;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BaseCloudSearchHandler $cloudSearchHandler)
    {
        // set the cloud search handler
        $this->cloudSearchHandler = $cloudSearchHandler;
        // set cloud search client 
        $this->setSearchClient();
        // search cloud search domain
        $this->searchAndSetSearchResult();
        // return the results
        return $this->getsearchResult();
    }

    private function searchAndSetSearchResult() {
        $searchClient           = $this->getSearchClient();
        $searchQuery            = $this->getSearchQuery();
        $filterQuery            = $this->getFilterQuery();
        $fieldsToSearch         = $this->getSearchableFields();
        $searchFieldsJsonString = json_encode(['fields' => $fieldsToSearch]);
        //dd($searchQuery, $filterQuery);
        $searchRequestParams = [
            'query' => $searchQuery,
            'filterQuery' => $filterQuery,
            'queryOptions' => $searchFieldsJsonString,

            // setting partial true will return non-corrupt partial data from any partition if it finds
            'partial' => true,

            // set static page params
            'start' => 0,
            'size' => 10000,

            'return' => '_all_fields,_score',
            'queryParser' => 'simple',
            //'sort' => '_score desc'

            // 'cursor' => '<string>',
            // 'expr' => '<string>',
            // 'facet' => '<string>',
            // 'filterQuery' => '<string>',
            // 'highlight' => '<string>',
            // 'partial' => true || false,
            // 'query' => '<string>', // REQUIRED
            // 'queryOptions' => '<string>',
            // 'queryParser' => 'simple|structured|lucene|dismax',
            // 'return' => '<string>',
            // 'size' => <integer>,
            // 'sort' => '<string>',
            // 'start' => <integer>,
            // 'stats' => '<string>',
        ];
        //dd($searchClient);
        $searchAction = $searchClient->search($searchRequestParams);
        //dd($searchAction);
        $searchResult = $searchAction->get('hits');

        //dd($searchResult);
        $this->setSearchResult($searchResult);
    }
}
