<?php
namespace App\Domains\Search\Tests\Jobs;

use App\Domains\Search\Jobs\ClearSearchDataJob;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Data\Models\User;

class ClearSearchDataJobTest extends TestCase
{
    // this migrates DB for this test case execution and rolls back DB when test class object is destroyed
    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();
        // run seeder if required after environment has been setup
        Artisan::call('db:seed');
    }

    public function test_clear_search_data_job()
    {
        //$this->markTestIncomplete();
        $testModel = new User();
        $userCount = $testModel->all()->count();
        $this->assertGreaterThanOrEqual(5, $userCount);
    }
}
