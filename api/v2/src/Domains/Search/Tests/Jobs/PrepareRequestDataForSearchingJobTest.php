<?php
namespace App\Domains\Search\Tests\Jobs;

use App\Domains\Search\Jobs\PrepareRequestDataForSearchingJob;
use Tests\TestCase;

class PrepareRequestDataForSearchingJobTest extends TestCase
{
    public function test_RequestUserOrganizationId()
    {
        $assets = new PrepareRequestDataForSearchingJob();
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRequestUserOrganizationId($value);
        $this->assertEquals($value,$assets->getRequestUserOrganizationId());
    }

    public function test_SearchTerm()
    {
        $assets = new PrepareRequestDataForSearchingJob();
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setSearchTerm($value);
        $this->assertEquals($value,$assets->getSearchTerm());
    }

    public function test_StructuredFilterQuery()
    {
        $assets = new PrepareRequestDataForSearchingJob();
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setStructuredFilterQuery($value);
        $this->assertEquals($value,$assets->getStructuredFilterQuery());
    }
}
