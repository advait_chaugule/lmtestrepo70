<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class CreateThreadCommentJob extends Job
{
    use DateHelpersTrait;

    private $threadCommentRepository;

    private $threadSourceIdentifier;
    private $requestData;

    private $threadCommentData;
    private $threadCommentEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadSourceIdentifier, array $requestData)
    {
        //Set public data attribute
        $this->setThreadSourceIdentifier($threadSourceIdentifier);
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadCommentRepository)
    {
        //Set the repo handler
        $this->threadCommentRepository = $threadCommentRepository;
        $identifier = $this->getThreadSourceIdentifier();

        $inputData = $this->requestData;
        
        $savedThreadCommentData = $this->threadCommentRepository->fillAndSave($inputData);
        $this->setThreadCommentData($savedThreadCommentData);
        //return $savedThreadCommentData;

        $this->parseResponse();

        return $this->getThreadCommentEntity();
    }

    //Public Setter and Getter methods
    public function setThreadSourceIdentifier($identifier) {
        $this->threadSourceIdentifier = $identifier;
    }

    public function getThreadSourceIdentifier(){
        return $this->threadSourceIdentifier;
    }

    public function setThreadCommentData($data){
        $this->threadCommentData = $data;
    }

    public function getThreadCommentData() {
        return $this->threadCommentData;
    }

    public function setThreadCommentEntity($threadCommentEntity){
        $this->threadCommentEntity = $threadCommentEntity;
    }

    public function getThreadCommentEntity(){
        return $this->threadCommentEntity;
    }

    private function parseResponse(){
        $threadCommentData = $this->getThreadCommentData();

        $threadCommentEntity = [
            "thread_comment_id"         =>  $threadCommentData->thread_comment_id,
            "parent_id"                 =>  $threadCommentData->parent_id,
            "comment"                   =>  $threadCommentData->comment,
            "updated_at"                =>  $this->formatDateTimeToISO8601($threadCommentData->updated_at)
        ];

        $this->setThreadCommentEntity($threadCommentEntity);
    }
}
