<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

class GetThreadJob extends Job
{
    private $threadIdentifier;
    private $threadRepository;

    private $threadDetail;
    private $threadEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the private identifier
        $this->setThreadIdentifier($identifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadRepositoryInterface $threadRepository)
    {
        //Set the ThreadComment Repository handler
        $this->threadRepository     =   $threadRepository;
        $threadIdentifier           =   $this->getThreadIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_id','assign_to','status','created_at'];
        $keyValuePairs      =   ['thread_id' => $threadIdentifier];
        $relations          =   ['threadCommentDetail'];

        $threadDetail   =   $this->threadRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations);
        $this->setThreadDetail($threadDetail);

        $this->parseResponse();

        $threadDetailResponse = $this->getThreadEntity();

        return $threadDetailResponse;
    }

    public function setThreadIdentifier($identifier){
        $this->threadIdentifier = $identifier;
    }

    public function getThreadIdentifier(){
        return $this->threadIdentifier;
    }

    public function setThreadDetail($data){
        $this->threadDetail = $data;
    }

    public function getThreadDetail(){
        return $this->threadDetail;
    }

    public function setThreadEntity($entity) {
        $this->threadEntity = $entity;
    }

    public function getThreadEntity(){
        return $this->threadEntity;
    }

    private function parseResponse(){
        $threadEntity   =   [];
        $threadDetail   =   $this->getThreadDetail();
        
        foreach($threadDetail as $thread){
            $threadEntity = [
                'thread_id'         =>  $thread->thread_id,
                'assign_to'         =>  $thread->assign_to,
                'status'            =>  $thread->status,
            ];
    
            foreach($thread->threadCommentDetail as $threadComment){
                if($threadComment->parent_id === ''){
                    $threadEntity['thread_comment'] = [
                        'thread_comment_id' =>  $threadComment->thread_comment_id,
                        'comment'           =>  $threadComment->comment,
                        'created_by'        =>  $threadComment->created_by,
                    ];
                }
            }
        }

        $this->setThreadEntity($threadEntity);
    }
}
