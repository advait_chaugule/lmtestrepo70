<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Thread\Validators\ThreadCommentExistsValidator;

class ValidateThreadCommentJob extends Job
{

    public $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
