<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class CheckThreadSourceIsDeletedJob extends Job
{
    private $threadSourceIdentifier;
    private $threadSourceType;

    private $itemRepository;
    private $documentRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadSourceIdentifier, string $threadSourceType)
    {
        //Set the source identifier and source type
        $this->setThreadSourceIdentifier($threadSourceIdentifier);
        $this->setThreadSourceType($threadSourceType);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository)
    {
        //Set the item and document repo handler
        $this->itemRepository       =   $itemRepository;
        $this->documentRepository   =   $documentRepository;

        $threadSourceType           =   $this->getThreadSourceType();
        $threadSourceIdentifier     =   $this->getThreadSourceIdentifier();
        
        if($threadSourceType    ==  '1') {
            $attributes                  =   ['item_id'  =>  $threadSourceIdentifier, 'is_deleted'   =>  '1'];
            return $this->itemRepository->findByAttributes($attributes)->isNotEmpty();
        }
        else{
            $attributes                  =   ['document_id'  =>  $threadSourceIdentifier,  'is_deleted'   =>  '1'];
            return $this->documentRepository->findByAttributes($attributes)->isNotEmpty();            
        }
    }

    /**
     * Public Setter and Getter methods
     */
    public function setThreadSourceIdentifier($threadSourceIdentifier) {
        $this->threadSourceIdentifier    =   $threadSourceIdentifier;
    }

    public function getThreadSourceIdentifier() {
        return $this->threadSourceIdentifier;
    }

    public function setThreadSourceType($threadSourceType) {
        $this->threadSourceType =   $threadSourceType;
    }
    
    public function getThreadSourceType() {
        return $this->threadSourceType;
    }
}
