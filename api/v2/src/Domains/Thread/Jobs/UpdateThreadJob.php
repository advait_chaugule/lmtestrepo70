<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

class UpdateThreadJob extends Job
{
    private $threadRepository;

    private $threadSourceIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        //Set public data attribute
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadRepositoryInterface $threadRepository)
    {
        //Set the repo handler
        $this->threadRepository = $threadRepository;
        //$identifier =   $this->getItemIdentifier();
        $inputData  =   $this->requestData;

        $columnValueFilterPairs = ['thread_id' =>  $inputData['thread_id']];
        $attributes             = ['updated_at' =>  $inputData['updated_at']];             

        if(isset($inputData['status'])) {
            $attributes['status'] = $inputData['status'];
        }

        if(isset($inputData['assign_to'])){
            $attributes['assign_to']    =   $inputData['assign_to'];
        }

        if(isset($inputData['is_deleted'])){
            $attributes['is_deleted']    =   $inputData['is_deleted'];
        }

        $updateThreadData = $this->threadRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        return $updateThreadData;
    }

    public function setItemIdentifier($identifier) {
        $this->itemIdentifier = $identifier;
    }

    public function getItemIdentifier(){
        return $this->itemIdentifier;
    }
}
