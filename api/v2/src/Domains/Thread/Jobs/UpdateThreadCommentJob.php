<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class UpdateThreadCommentJob extends Job
{
    private $threadCommentRepository;
    private $threadCommentIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadCommentIdentifier, array $requestData)
    {
        //Set public data attribute
        $this->requestData = $requestData;
        $this->setThreadCommentIdentifier($threadCommentIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadCommentRepository)
    {
        //Set the repo handler
        $this->threadCommentRepository = $threadCommentRepository;
        $identifier =   $this->getThreadCommentIdentifier();
        $inputData  =   $this->requestData;

        $columnValueFilterPairs = ['thread_comment_id' =>  $identifier];
        $attributes             = ['comment' => $inputData['comment'], 'updated_at' =>  $inputData['updated_at']];             

        $updateThreadCommentData = $this->threadCommentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        return $updateThreadCommentData;
    }

    public function setThreadCommentIdentifier($identifier) {
        $this->threadCommentIdentifier = $identifier;
    }

    public function getThreadCommentIdentifier(){
        return $this->threadCommentIdentifier;
    }
}
