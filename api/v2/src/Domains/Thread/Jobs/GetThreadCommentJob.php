<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class GetThreadCommentJob extends Job
{
    use DateHelpersTrait;

    private $statusValues;

    private $threadCommentIdentifier;
    private $threadCommentRepository;

    private $threadDetail;

    private $threadCommentDetail;
    private $threadCommentEntity;   

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the private identifier
        $this->setThreadCommentIdentifier($identifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadCommentRepository)
    {
        //Set the ThreadComment Repository handler
        $this->threadCommentRepository  =   $threadCommentRepository;
        $this->setDefaultStatusMatrix();
        $threadCommentIdentifier        =   $this->getThreadCommentIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_comment_id','thread_source_id' ,'thread_id', 'parent_id','comment','created_by', 'updated_at'];
        $keyValuePairs      =   ['thread_comment_id' => $threadCommentIdentifier];
        $relations          =   ['threadDetail'];

        $threadCommentDetail   =   $this->threadCommentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations);

        $this->setThreadCommentDetail($threadCommentDetail);

        $this->parseResponse();

        $threadCommentDetailResponse = $this->getThreadCommentEntity();

        return $threadCommentDetailResponse;
    }

    public function setThreadCommentIdentifier($identifier){
        $this->threadCommentIdentifier = $identifier;
    }

    public function getThreadCommentIdentifier(){
        return $this->threadCommentIdentifier;
    }

    public function setThreadCommentDetail($data){
        $this->threadDetail = $data;
    }

    public function getThreadCommentDetail(){
        return $this->threadDetail;
    }

    public function setThreadCommentEntity($entity) {
        $this->threadEntity = $entity;
    }

    public function getThreadCommentEntity(){
        return $this->threadEntity;
    }

    public function setDefaultStatusMatrix() {
        $this->statusValues = config("default_enum_setting.comment_status");
    }

    public function getDefaultStatusMatrix(){
        return $this->statusValues;
    }

    private function parseResponse(){
        $statusValues           =   $this->getDefaultStatusMatrix();
        $threadCommentDetail    =   $this->getThreadCommentDetail();

        //dd($itemThreadCommentDetail);

        $threadCommentEntity =  [
        
            "thread_comment_id"         =>  $threadCommentDetail[0]->thread_comment_id,
            "thread_source_id"         =>  $threadCommentDetail[0]->thread_source_id,
            "parent_id"                 =>  $threadCommentDetail[0]->parent_id,
            "thread_id"                 =>  $threadCommentDetail[0]->threadDetail->thread_id,
            "comment"                   =>  $threadCommentDetail[0]->comment,
            "assign_to"                 =>  $threadCommentDetail[0]->threadDetail->assign_to,
            "status"                    =>  $statusValues[$threadCommentDetail[0]->threadDetail->status],
            "created_by"                =>  $threadCommentDetail[0]->created_by,
            "updated_at"                =>  $this->formatDateTimeToISO8601($threadCommentDetail[0]->updated_at)         
        ];

        $this->setThreadCommentEntity($threadCommentEntity);
    }
}
