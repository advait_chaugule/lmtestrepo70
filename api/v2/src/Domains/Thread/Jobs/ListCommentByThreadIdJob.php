<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class ListCommentbyThreadIdJob extends Job
{
    use DateHelpersTrait;

    private $organizationIdentifier;
    private $threadIdentifier;

    private $threadCommentRepository;
    private $threadRepository;

    private $threadDetail;
    private $threadEntity;

    private $threadCommentDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadIdentifier, string $organizationIdentifier)
    {
        //Set the private attribute to Thread Identifier
        $this->setThreadIdentifier($threadIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadRepositoryInterface $threadRepository, ThreadCommentRepositoryInterface $threadCommentRepository)
    {
        //Set Thread and ThreadComment Repo handler
        $this->threadCommentRepository  = $threadCommentRepository;
        $this->threadRepository         = $threadRepository;

        //get Thread detail from database
        $threadDetail = $this->getThreadDetailFromModel();

        // set Thread details globally
        $this->setThreadDetail($threadDetail);

        //Set parsed Thread details
        $this->createParsedThreadResponseDetail();

        // finally return the standard Thread and ThreadComment 
        return $this->getThreadEntity();
    }

    //Public Getter and Setter method
    public function setThreadIdentifier($threadIdentifier){
        $this->threadIdentifier =   $threadIdentifier;
    }

    public function getThreadIdentifier(){
        return $this->threadIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }

    public function setThreadDetail($data){
        $this->threadDetail = $data;
    }

    public function getThreadDetail(){
        return $this->threadDetail;
    }

    public function setThreadCommentDetail($data){
        $this->threadCommentDetail = $data;
    }

    public function getThreadCommentDetail(){
        return $this->threadCommentDetail;
    }

    public function setThreadEntity($threadEntity) {
        $this->threadEntity = $threadEntity;
    }

    public function getThreadEntity() {
        return $this->threadEntity;
    }

    private function getThreadCommentDetailFromModel() {
        $threadIdentifier   =   $this->getThreadIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection = true;
        $fieldsToReturn = ['thread_id', 'thread_comment_id', 'parent_id', 'comment', 'created_by', 'updated_at'];
        $keyValuePairs  = ['thread_id' => $threadIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations      = ['createdBy'];


        $threadCommentDetail = $this->threadCommentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

        // set ThreadComment details
        $this->setThreadCommentDetail($threadCommentDetail);
    }

    private function getThreadDetailFromModel() {
        $threadIdentifier       =   $this->getThreadIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection = true;
        $fieldsToReturn = ['thread_id', 'assign_to', 'status'];
        $keyValuePairs  = ['thread_id' => $threadIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations      = ['assignTo'];


        $threadDetail = $this->threadRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,$relations);

        return $threadDetail;
    }

    private function parsedThreadCommentResponseDetailforThread(){
        $threadCommentEntity = [];

        //get ThreadComment detail from database
        $threadComments         =   $this->getThreadCommentDetailFromModel();

        $threadCommentDetail    =   $this->getThreadCommentDetail();   

        foreach($threadCommentDetail as $threadComment)
        {
            $createdBy = $threadComment->createdBy->first_name." ".$threadComment->createdBy->last_name;
            $threadCommentEntity[] = [
                "thread_comment_id"         =>  $threadComment->thread_comment_id,
                "parent_id"                 =>  !empty($threadComment->parent_id) ? $threadComment->parent_id : "",
                "comment"                   =>  !empty($threadComment->comment) ? $threadComment->comment : "",
                "created_by"                =>  !empty($createdBy) ? $createdBy : "",
                "updated_at"                =>  $this->formatDateTimeToISO8601($threadComment->updated_at)
            ];
        }

        return $threadCommentEntity;
        
    }

    private function createParsedThreadResponseDetail(){
        $threadEntity = [];

        $threadDetail = $this->getThreadDetail();

        foreach($threadDetail as $thread)
        {
            $assignTo = !empty($thread->assign_to) ? ($thread->assignTo->first_name." ".$thread->assignTo->last_name) : '';
            $threadEntity[] = [
                "thread_id"         =>  $thread->thread_id,
                "assign_to"         =>  !empty($assignTo) ? $assignTo : "",
                "status"            =>  $thread->status,
                "comments"          =>  $this->parsedThreadCommentResponseDetailforThread()
            ];
        }

        $this->setThreadEntity($threadEntity);
    }
}
