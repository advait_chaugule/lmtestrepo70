<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

class CreateThreadJob extends Job
{
    private $threadRepository;

    private $threadSourceIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadSourceIdentifier, array $requestData)
    {
        //Set public data attribute
        $this->setThreadSourceIdentifier($threadSourceIdentifier);
        $this->requestData = $requestData;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadRepositoryInterface $threadRepository)
    {
        //Set the thread repo handler
        $this->threadRepository = $threadRepository;
        $threadSourceIdentifier = $this->getThreadSourceIdentifier();

        $inputData = $this->requestData;
        unset($inputData['comment']);
        //dd($inputData);

        $savedThreadData = $this->threadRepository->fillAndSave($inputData);
        return $savedThreadData;
    }

    public function setThreadSourceIdentifier($identifier) {
        $this->threadSourceIdentifier = $identifier;
    }

    public function getThreadSourceIdentifier(){
        return $this->threadSourceIdentifier;
    }
}
