<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Item\Validators\ItemExistsValidator;
use App\Domains\Document\Validators\DocumentExistsValidator;

class ValidateThreadSourceByIdJob extends Job
{
    private $threadSourceIdentifier;
    private $threadSourceType;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $threadSourceIdentifier, string $threadSourceType)
    {
        //Set the source identifier and source type
        $this->setThreadSourceIdentifier($threadSourceIdentifier);
        $this->setThreadSourceType($threadSourceType);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemExistsValidator $itemValidator, DocumentExistsValidator $documentValidator)
    {
        $threadSourceType           =   $this->getThreadSourceType();
        $threadSourceIdentifier     =   $this->getThreadSourceIdentifier();
        
        if($threadSourceType    ==  '1') {
            $inputData                  =   ['item_id'  =>  $threadSourceIdentifier];
            //validate the thread_source_id as Item
            $validation = $itemValidator->validate($inputData);
            return $validation===true ? $validation : $validation->messages()->getMessages();
        }
        else{
            $inputData                  =   ['document_id'  =>  $threadSourceIdentifier];
            //validate the thread_source_id as Document
            $validation = $documentValidator->validate($inputData);
            return $validation===true ? $validation : $validation->messages()->getMessages();
        }
    }

    /**
     * Public Setter and Getter methods
     */
    public function setThreadSourceIdentifier($threadSourceIdentifier) {
        $this->threadSourceIdentifier    =   $threadSourceIdentifier;
    }

    public function getThreadSourceIdentifier() {
        return $this->threadSourceIdentifier;
    }

    public function setThreadSourceType($threadSourceType) {
        $this->threadSourceType =   $threadSourceType;
    }
    
    public function getThreadSourceType() {
        return $this->threadSourceType;
    }
}
