<?php
namespace App\Domains\Thread\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;


use App\Data\Repositories\Contracts\ThreadRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class DeleteCommentJob extends Job
{
    use DateHelpersTrait;

    private $identifier;
    private $threadCommentRepository;
    private $threadRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set Role Identifier
        $this->setIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadCommentRepository, ThreadRepositoryInterface $threadRepository)
    {
        //Set Comment Repo handler
        $this->threadCommentRepository  =   $threadCommentRepository;
        $this->threadRepository         =   $threadRepository;    
        //Get the identifier
        $threadCommentIdentifier = $this->getIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_comment_id', 'thread_id', 'parent_id','comment','updated_at'];
        $keyValuePairs      =   ['thread_comment_id' => $threadCommentIdentifier];
        $relations          =   ['threadDetail'];

        $threadCommentDetail   =   $this->threadCommentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations);     

        //set the attribute to update
        $attribute = ['is_deleted' => '1'];

        if($threadCommentDetail[0]->parent_id === ''){
            //set the attribute to update
            $attribute = ['is_deleted' => '1'];

            $this->threadRepository->edit($threadCommentDetail[0]->thread_id, $attribute);
        }

        $this->threadCommentRepository->edit($threadCommentIdentifier, $attribute);

        return $this->parseResponse();
        
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }

    private function parseResponse(){
        $threadCommentIdentifier = $this->getIdentifier();
        
        //Get deleted Comment details
        $attributes = ['thread_comment_id' => $threadCommentIdentifier, 'is_deleted' => '1'];
        
        $threadCommentDetail = $this->threadCommentRepository->findByAttributes($attributes);

        $threadEntity = [
            'thread_comment_id'    =>  $threadCommentDetail[0]->thread_comment_id,
            'thread_id'            =>  $threadCommentDetail[0]->thread_id,
            'comment'              =>  $threadCommentDetail[0]->comment,
            'parent_id'            =>  $threadCommentDetail[0]->parent_id,
            'is_deleted'           =>  $threadCommentDetail[0]->is_deleted,
            'updated_at'           =>  $this->formatDateTimeToISO8601($threadCommentDetail[0]->updated_at)
        ];

        return $threadEntity;
    }
}
