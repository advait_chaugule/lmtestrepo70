<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\CreateThreadCommentJob;
use Tests\TestCase;

class CreateThreadCommentJobTest extends TestCase
{
    public function test_ThreadSourceIdentifier()
    {
        $data[] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadSourceIdentifier($value);
        $this->assertEquals($value,$assets->getThreadSourceIdentifier());
    }

    public function test_ThreadCommentData()
    {
        $data[] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentData($value);
        $this->assertEquals($value,$assets->getThreadCommentData());
    }

    public function test_ThreadCommentEntity()
    {
        $data[] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentEntity($value);
        $this->assertEquals($value,$assets->getThreadCommentEntity());
    }
}
