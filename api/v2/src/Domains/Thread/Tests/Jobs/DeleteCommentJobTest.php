<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\DeleteCommentJob;
use Tests\TestCase;

class DeleteCommentJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new DeleteCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
