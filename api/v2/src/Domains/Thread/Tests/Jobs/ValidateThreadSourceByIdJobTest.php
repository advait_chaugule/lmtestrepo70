<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\ValidateThreadSourceByIdJob;
use Tests\TestCase;

class ValidateThreadSourceByIdJobTest extends TestCase
{
    public function test_ThreadSourceIdentifier()
    {
        $assets = new ValidateThreadSourceByIdJob('8465fd5a-78e0-4301-a8d1-4de079fa0fc5','a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadSourceIdentifier($value);
        $this->assertEquals($value,$assets->getThreadSourceIdentifier());
    }

    public function test_ThreadSourceType()
    {
        $assets = new ValidateThreadSourceByIdJob('8465fd5a-78e0-4301-a8d1-4de079fa0fc5','a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadSourceType($value);
        $this->assertEquals($value,$assets->getThreadSourceType());
    }
}
