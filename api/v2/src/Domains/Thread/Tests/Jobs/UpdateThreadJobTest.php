<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\UpdateThreadJob;
use Tests\TestCase;

class UpdateThreadJobTest extends TestCase
{
    public function test_ItemIdentifier()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UpdateThreadJob($data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }
}
