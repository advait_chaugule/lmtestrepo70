<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\GetThreadCommentJob;
use Tests\TestCase;

class GetThreadCommentJobTest extends TestCase
{
    public function test_ThreadCommentIdentifier()
    {
        $assets = new GetThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentIdentifier($value);
        $this->assertEquals($value,$assets->getThreadCommentIdentifier());
    }

    public function test_ThreadCommentDetail()
    {
        $assets = new GetThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentDetail($value);
        $this->assertEquals($value,$assets->getThreadCommentDetail());
    }

    public function test_ThreadCommentEntity()
    {
        $assets = new GetThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentEntity($value);
        $this->assertEquals($value,$assets->getThreadCommentEntity());
    }
}
