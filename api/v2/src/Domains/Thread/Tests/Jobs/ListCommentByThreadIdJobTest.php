<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\ListCommentByThreadIdJob;
use Tests\TestCase;

class ListCommentByThreadIdJobTest extends TestCase
{
    public function test_ThreadIdentifier()
    {
        $assets = new ListCommentByThreadIdJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','8465fd5a-78e0-4301-a8d1-4de079fa0fc5');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadIdentifier($value);
        $this->assertEquals($value,$assets->getThreadIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new ListCommentByThreadIdJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','8465fd5a-78e0-4301-a8d1-4de079fa0fc5');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ThreadDetail()
    {
        $assets = new ListCommentByThreadIdJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','8465fd5a-78e0-4301-a8d1-4de079fa0fc5');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadDetail($value);
        $this->assertEquals($value,$assets->getThreadDetail());
    }

    public function test_ThreadCommentDetail()
    {
        $assets = new ListCommentByThreadIdJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','8465fd5a-78e0-4301-a8d1-4de079fa0fc5');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentDetail($value);
        $this->assertEquals($value,$assets->getThreadCommentDetail());
    }

    public function test_ThreadEntity()
    {
        $assets = new ListCommentByThreadIdJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','8465fd5a-78e0-4301-a8d1-4de079fa0fc5');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadEntity($value);
        $this->assertEquals($value,$assets->getThreadEntity());
    }
}
