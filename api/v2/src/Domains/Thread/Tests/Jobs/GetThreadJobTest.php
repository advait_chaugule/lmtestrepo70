<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\GetThreadJob;
use Tests\TestCase;

class GetThreadJobTest extends TestCase
{
    public function test_ThreadIdentifier()
    {
        $assets = new GetThreadJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadIdentifier($value);
        $this->assertEquals($value,$assets->getThreadIdentifier());
    }

    public function test_ThreadDetail()
    {
        $assets = new GetThreadJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadDetail($value);
        $this->assertEquals($value,$assets->getThreadDetail());
    }

    public function test_ThreadEntity()
    {
        $assets = new GetThreadJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadEntity($value);
        $this->assertEquals($value,$assets->getThreadEntity());
    }
}
