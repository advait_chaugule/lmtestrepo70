<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\CheckThreadSourceIsDeletedJob;
use Tests\TestCase;

class CheckThreadSourceIsDeletedJobTest extends TestCase
{
    public function test_ThreadSourceIdentifier()
    {
        $assets = new CheckThreadSourceIsDeletedJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadSourceIdentifier($value);
        $this->assertEquals($value,$assets->getThreadSourceIdentifier());
    }

    public function test_ThreadSourceType()
    {
        $assets = new CheckThreadSourceIsDeletedJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadSourceType($value);
        $this->assertEquals($value,$assets->getThreadSourceType());
    }
}
