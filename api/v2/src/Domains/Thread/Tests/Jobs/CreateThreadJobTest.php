<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\CreateThreadJob;
use Tests\TestCase;

class CreateThreadJobTest extends TestCase
{
    public function test_ThreadSourceIdentifier()
    {
        $data[] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateThreadJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadSourceIdentifier($value);
        $this->assertEquals($value,$assets->getThreadSourceIdentifier());
    }
}
