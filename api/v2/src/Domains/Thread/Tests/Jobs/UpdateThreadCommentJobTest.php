<?php
namespace App\Domains\Thread\Tests\Jobs;

use App\Domains\Thread\Jobs\UpdateThreadCommentJob;
use Tests\TestCase;

class UpdateThreadCommentJobTest extends TestCase
{
    public function test_ThreadCommentIdentifier()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UpdateThreadCommentJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setThreadCommentIdentifier($value);
        $this->assertEquals($value,$assets->getThreadCommentIdentifier());
    }
}
