<?php
namespace App\Domains\AwsLog\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Storage;

use Aws\S3\S3Client;
use Aws\Iam\IamClient;


class FetchLogDataForGivenTimeJob extends Job
{

    private $fromDate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $fromDate, string $organizationIdentifier)
    {
        //Set the private attributes
        $this->fromDate =   $fromDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileInfoForFromDate          =   [];
        $fileInfoForToDate            =   [];
        $fileInfo                     =   [];
        $parsedDataFromLogFile        =   [];
        $responseData                 =   [];
        /* $client = new IamClient([
            'credentials' => array(
                'key'    => 'AKIAXHMIMWVJ2FVHL3GR',
                'secret' => '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',
            ),
            'region' => 'us-east-2',
            'version' => '2010-05-08'
        ]);
        
        $userDetail = $client->getUser();
        var_dump($result);
        dd(); */

        $config = [ 
            's3-access' => [ 
                'key' => 'AKIAXHMIMWVJ2FVHL3GR', 
                'secret' => '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', 
                'bucket' => 'acmtlogs', 
                'region' => 'us-east-1', 
                'version' => '2006-03-01',
            ] 
        ]; 
        
        # initializing s3 
        $s3Client = S3Client::factory([ 
            'credentials' => [ 
                'key' => $config['s3-access']['key'], 
                'secret' => $config['s3-access']['secret'] 
            ], 
            'version' => $config['s3-access']['version'], 
            'region' => $config['s3-access']['region'] 
        ]); 

        
        $baseFolderPathForStorage = storage_path('app').'/temporary/awslogs/';
        if (!file_exists($baseFolderPathForStorage.'unzipped')) {
            mkdir($baseFolderPathForStorage.'unzipped', 0777);
        }

        if (!file_exists($baseFolderPathForStorage.'zipped')) {
            mkdir($baseFolderPathForStorage.'zipped', 0777);
        }

        $bucket     = $config['s3-access']['bucket'];
        $basePath   = $baseFolderPathForStorage;

        $s3Client->registerStreamWrapper();
    
        try{ 
            # initializing our object 
            $folderList = $s3Client->getIterator('ListObjects', [ # this is a Generator Object (its yields data rather than returning) 
                'Bucket' => $bucket,
                "Prefix" => 'dev/elb/AWSLogs/496891639123/elasticloadbalancing/us-east-1/'.$this->fromDate.'/'
            ]);

            # printing our data 
            foreach($folderList as $file){ 
                if(sizeOf($file) > 0) {
                    if(strpos($file['Key'], 'dev/elb/AWSLogs/496891639123/elasticloadbalancing/us-east-1/'.$this->fromDate) !== false){
                        $fileInfoForFromDate[]   =   pathinfo($file['Key']);                    
                        $result = $s3Client->getObject([ 
                            'Bucket' => $bucket, 
                            'Key' => $file['Key'], 
                            'SaveAs' => $basePath.'zipped/'.pathinfo($file['Key'])['basename']
                        ]); 
                    }
                }
            }

            if(sizeOf($fileInfoForFromDate) > 0) {
                foreach($fileInfoForFromDate as $filesToBeUnzipped) {
                    $file_name = $filesToBeUnzipped['basename'];//'2013-07-16.dump.gz';
    
                    // Raising this value may increase performance
                    $buffer_size = 4096; // read 4kb at a time
                    $out_file_name = str_replace('.gz', '', $file_name); 
    
                    // Open our files (in binary mode)
                    $file = gzopen($basePath .'zipped/'. $file_name, 'rb');
    
                    $out_file = fopen($basePath. 'unzipped/'.$out_file_name, 'wb'); 
    
                    // Keep repeating until the end of the input file
                    while (!gzeof($file)) {
                        // Read buffer-size bytes
                        // Both fwrite and gzread and binary-safe
                        fwrite($out_file, gzread($file, $buffer_size));
                    }
    
                    // Files are done, close files
                    fclose($out_file);
                    gzclose($file);
                }            
    
                $files = glob($basePath. "unzipped/*.log");
                
                foreach($files as $file) {
                    $content = file_get_contents($file);
                    //$items = explode(" ",$content);
                    //echo "<pre>".$logEntry."</pre><br />";
                    preg_match('^(http|https|h2) ([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{6}Z) (app/acmtdev/d98687a58d9a22ce) (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:[0-9]{5}) (-|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:[0-9]{2}) (-1|[0-9]{1}.[0-9]{3}) (-1|[0-9]{1}.[0-9]{3}) (-1|[0-9]{1}.[0-9]{3}) (-|200|301|404) (-|200|301|404) (-|[0-9]{1,18}|[1-8][0-9]{18}|9(?:[01][0-9]{17}|2(?:[01][0-9]{16}|2(?:[0-2][0-9]{15}|3(?:[0-2][0-9]{14}|3(?:[0-6][0-9]{13}|7(?:[01][0-9]{12}|20(?:[0-2][0-9]{10}|3(?:[0-5][0-9]{9}|6(?:[0-7][0-9]{8}|8(?:[0-4][0-9]{7}|5(?:[0-3][0-9]{6}|4(?:[0-6][0-9]{5}|7(?:[0-6][0-9]{4}|7(?:[0-4][0-9]{3}|5(?:[0-7][0-9]{2}|80[0-7])))))))))))))))) (-|[0-9]{1,18}|[1-8][0-9]{18}|9(?:[01][0-9]{17}|2(?:[01][0-9]{16}|2(?:[0-2][0-9]{15}|3(?:[0-2][0-9]{14}|3(?:[0-6][0-9]{13}|7(?:[01][0-9]{12}|20(?:[0-2][0-9]{10}|3(?:[0-5][0-9]{9}|6(?:[0-7][0-9]{8}|8(?:[0-4][0-9]{7}|5(?:[0-3][0-9]{6}|4(?:[0-6][0-9]{5}|7(?:[0-6][0-9]{4}|7(?:[0-4][0-9]{3}|5(?:[0-7][0-9]{2}|80[0-7])))))))))))))))) (".*?") (".*?")^', $content, $matches);
                    
                    $parsedDataFromLogFile[] = $matches;
    
                }

                $responseData = $parsedDataFromLogFile;
            }
            return $responseData;
        }catch(Exception $ex){ 
            echo "Error Occurred\n", $ex->getMessage(); 
        }
    }
}
