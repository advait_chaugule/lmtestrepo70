<?php
namespace App\Domains\AwsLog\Jobs;

use Lucid\Foundation\Job;

class CloudFrontSignedCookiesJob extends Job
{

    private $COOKIE_DOMAIN = '';
    private $COOKIE_SECURE = '';
    private $COOKIE_HTTP = '';
    private $CLOUDFRONT_KEY_PAIR_ID = '';
    private $CLOUDFRONT_KEY_PATH = '';
    private $CDN_HOST = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->COOKIE_DOMAIN = env('COOKIE_DOMAIN');  //This should be configurable
        $this->COOKIE_SECURE = env('COOKIE_SECURE','SECURE'); //This should be configurable  Values are  SECURE for true  / false for  false
        $this->COOKIE_HTTP = env('COOKIE_HTTP','https://');  //This should be configurable.  For Local→  http://  for remote→   https://
        $this->CLOUDFRONT_KEY_PAIR_ID = env('CLOUDFRONT_KEY_PAIR_ID','APKAIR42DUXDVEV6UU3Q.pem');  //This should be configurable. Get it from Dev ops
        $this->CLOUDFRONT_KEY_PATH = env('CLOUDFRONT_KEY_PATH','usr/share/nginx/html/lmp70/api/v2');  //This should be configurable. Get it from Dev ops
        $this->CDN_HOST = env('CDN_HOST');   //This should be configurable. Get it from Dev ops
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->setCloudFrontCookies();
    }

    public function setCloudFrontCookies()
    {
        $COOKIE_HTTP = $this->COOKIE_HTTP;  //This should be configurable.  For Local→  http://  for remote→   https://
        $CLOUDFRONT_KEY_PAIR_ID = $this->CLOUDFRONT_KEY_PAIR_ID;  //This should be configurable. Get it from Dev ops
        $CLOUDFRONT_KEY_PATH = $this->CLOUDFRONT_KEY_PATH;  //This should be configurable. Get it from Dev ops
        $CDN_HOST = $this->CDN_HOST;   //This should be configurable. Get it from Dev ops
        try {
            $cloudFrontCookieExpiry = $this->getNowPlus2HoursInUTC();
            $cloudFrontCookieExpiryBy48Hours = $this->getNowPlus48HoursInUTC();
                        
            //Preparing Cookie Parameters
            $customPolicy = '{"Statement":[{"Resource":"' . $COOKIE_HTTP . $CDN_HOST . '/*","Condition":{"DateLessThan":{"AWS:EpochTime":' . $cloudFrontCookieExpiry . '}}}]}';
            $encodedCustomPolicy = $this->url_safe_base64_encode($customPolicy);
            $customPolicySignature = $this->getSignedPolicy($CLOUDFRONT_KEY_PATH, $customPolicy);

            //Setting Cookie From which Cloudfront or S3 content will be accessible
            $this->setCookie("CloudFront-Policy", $encodedCustomPolicy, $CDN_HOST);
            $this->setCookie("CloudFront-Signature", $customPolicySignature, $CDN_HOST);
            $this->setCookie("CloudFront-Key-Pair-Id", $CLOUDFRONT_KEY_PAIR_ID, $CDN_HOST);
            $this->setCookie("CloudFront-Expires", $cloudFrontCookieExpiryBy48Hours, $CDN_HOST);
             
        } catch (Exception $ex) {
            $errorType = 'internal_error';
            $message = $ex->getMessage();
            $_status = 'custom_status_here';
            Log::error($ex);
            return $this->run(new RespondWithJsonErrorJob($errorType, $message, $_status));
        }
    }



    public function rsa_sha1_sign($policy, $private_key_filename)
    {
        $signature = "";
        //echo 'policy'.$policy;
        try {
            openssl_sign($policy, $signature, file_get_contents($private_key_filename));
        } catch (Exception $ex) {
            logError($ex);
        }
        return $signature;
    }

    public function url_safe_base64_encode($value)
    {
        $encoded = base64_encode($value);
        return str_replace(array('+', '=', '/'), array('-', '_', '~'), $encoded);
    }

    public function getSignedPolicy($private_key_filename, $policy)
    {
        $signature = $this->rsa_sha1_sign($policy, $private_key_filename);
        $encoded_signature = $this->url_safe_base64_encode($signature);
        return $encoded_signature;
    }

     /*
     * 24 hours PT24H
     * One day P1D
     */
    public function getNowPlus2HoursInUTC()
    {
        //$dt = new DateTime('now', new DateTimeZone('UTC'));
        $dt = new \DateTime('now');
        $dt->add(new \DateInterval('PT48H')); //48 Hours
        //echo $dt->format('Y-m-d H:i:s A') . "<br/>";
        //echo date('Y-m-d h:i:s A', $dt->format('U'));
        //echo "<br/>";
        return $dt->format('U');
    }

       /*
     * 48 hours PT48H
     * TWO day P2D
     */
    public function getNowPlus48HoursInUTC()
    {
        //$dt = new DateTime('now', new DateTimeZone('UTC'));
        $dt = new \DateTime('now');
        $dt->add(new \DateInterval('PT48H')); //48 Hours
        //echo $dt->format('Y-m-d H:i:s A') . "<br/>";
        //echo date('Y-m-d H:i:s', '1572614434'); //'1572527143');
        //echo "<br/>";
        return $dt->format('U');
    }

    public function setCookie($name, $val, $domain)
    {
        // using our own implementation because
        // using php setcookie means the values are URL encoded and then AWS CF fails
        // header ( "Set-Cookie: $name=$val; path=/; domain=$domain; secure; httpOnly", false );
        $COOKIE_DOMAIN = $this->COOKIE_DOMAIN;  //This should be configurable
        $COOKIE_SECURE = $this->COOKIE_SECURE; //This should be configurable  Values are  SECURE for true  / false for  false
        $SAME_SITE = 'None';  
        header("Set-Cookie: $name=$val; path=/; domain=$COOKIE_DOMAIN; $COOKIE_SECURE; httpOnly;SameSite=$SAME_SITE;", false);
    }


    public function destroyCloudFrontCookies()
    {
	$CDN_HOST =  $this->CDN_HOST;   //This should be configurable. Get it from Dev ops;

	$encodedCustomPolicy = ' ;Expires=-84600;Max-Age=-84600';
	$customPolicySignature = ' ;Expires=-84600;Max-Age=-84600';
	$CLOUDFRONT_KEY_PAIR_ID = ' ;Expires=-84600;Max-Age=-84600';

	$this->setCookie("CloudFront-Policy", $encodedCustomPolicy, $CDN_HOST);
	$this->setCookie("CloudFront-Signature", $customPolicySignature, $CDN_HOST);
	$this->setCookie("CloudFront-Key-Pair-Id", $CLOUDFRONT_KEY_PAIR_ID, $CDN_HOST);
    }


}
