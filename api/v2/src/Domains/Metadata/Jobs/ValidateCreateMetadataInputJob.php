<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Metadata\Validators\MetadataValidator;

class ValidateCreateMetadataInputJob extends Job
{
    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //assign input data to private attribute
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataValidator $validator)
    {
        //validate the input data based on DB
        $validation = $validator->validate($this->data);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
