<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Metadata;

class ValidateMetadataNameAndFieldJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input,$mode)
    {
        $this->data = $input;
        $this->mode = $mode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $input = $this->data;
        $mode = $this->mode;
        
        $organization_id= isset($input['auth_user']['organization_id'])?$input['auth_user']['organization_id']:$input['organization_id'];
        if($mode=='create'){
            $count = Metadata::where(['name'=>$input['name'],'field_type'=>$input['field_type'],'organization_id'=>$organization_id,'is_deleted'=>'0'])->count();
        }else{
            $count = Metadata::where(['name'=>$input['name'],'field_type'=>$input['field_type'],'organization_id'=>$organization_id,'is_deleted'=>'0'])->where('metadata_id', '!=', $input['metadata_id'])->count();
        }
        
        return $count;
        
    }
}
