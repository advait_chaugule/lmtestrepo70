<?php
namespace App\Domains\MetaData\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class GetCaseMetaDataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->item_ids = $input['item_ids'];
        $this->meta_data_field_names = $input['meta_data_field_names'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $items = DB::table('items')
                    ->select(
                        'items.*',
                        'concepts.title as concept_title',
                        'concepts.description as concept_description',
                        'concepts.hierarchy_code as concept_hierarchy_code',
                        'concepts.keywords as concept_keywords',
                        DB::raw('IFNULL(acmt_languages.name, "English") as language'),
                        'licenses.title as license_title',
                        'licenses.description as license_description',
                        'licenses.license_text as license_text',
                        'node_types.title as node_type'
                        )
                    ->leftJoin('concepts', 'items.concept_id', '=', 'concepts.concept_id')
                    ->leftJoin('languages', 'items.language_id', '=', 'languages.language_id')
                    ->leftJoin('licenses', 'items.license_id', '=', 'licenses.license_id')
                    ->leftJoin('node_types', 'items.node_type_id', '=', 'node_types.node_type_id')
                    ->whereIn('item_id', $this->item_ids)
                    ->get();

        $items_data = [];
        $custom_meta_data_field_names = [];

        # these fields are timestamp fields so returning null from db, if there is no value, then isset is returning false in "if()"
        $timestamp_fields = ['status_start_date', 'status_end_date'];

        # arrange case metadata field values in the items_data array
        foreach ($items as $item) {
            foreach ($this->meta_data_field_names as $field_name) {
                $field_name_slug = str_slug($field_name,'_');
                if(isset($item->$field_name_slug) && !empty($item->$field_name_slug)) {
                    $items_data[$item->item_id][$field_name] = $item->$field_name_slug;
                } else {
                    if(!in_array($field_name_slug, $timestamp_fields)) {
                        array_push($custom_meta_data_field_names, $field_name);
                    }
                }
            }
        }
        $custom_meta_data_field_names = array_unique($custom_meta_data_field_names);

        return [
            'items_data' => $items_data,
            'custom_meta_data_field_names' => $custom_meta_data_field_names,
            'item_ids' => $this->item_ids
        ];
    }
}
