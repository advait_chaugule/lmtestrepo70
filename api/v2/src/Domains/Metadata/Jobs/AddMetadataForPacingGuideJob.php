<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

use App\Services\Api\Traits\UuidHelperTrait;


class AddMetadataForPacingGuideJob extends Job
{
    use UuidHelperTrait;

    private $organizationIdentifier;
    private $metadataRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $organizationIdentifier)
    {
        //Set the private variable
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //Set the db handler
        $this->metadataRepository   =   $metadataRepository;

        return $this->fetchAndSetMetadataForTheOrganization();
    }

    public function setOrganizationIdentifier($identifier) { 
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    /**
     * Method to find out and Set the metadata of pacing guide for organizations
     *
     * @return void
     */
    private function fetchAndSetMetadataForTheOrganization() {
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $attributes     =   ['organization_id' => $organizationIdentifier, 'internal_name'  => 'pacing_guide_item_type', 'is_deleted'   => '0'];
        $metadataDetail =   $this->metadataRepository->findByAttributes($attributes);

        if($metadataDetail->count() == 0) {
            $metaDataInput["metadata_id"]           =   $this->createUniversalUniqueIdentifier(); 
            $metaDataInput["organization_id"]       =   $organizationIdentifier; 
            $metaDataInput["parent_metadata_id"]    =   "";
            $metaDataInput["name"]                  =   "Pacing Guide Item Type";
            $metaDataInput["internal_name"]         =   "pacing_guide_item_type";
            $metaDataInput["field_type"]            =   "3";
            $metaDataInput["field_possible_values"] =   "Standard,Container";
            $metaDataInput["order"]                 =   '27';
            $metaDataInput["is_custom"]             =   "0";
            $metaDataInput["is_document"]           =   "3";
            $metaDataInput["is_mandatory" ]         =   '0';
            $metaDataInput["is_active"]             =   "1";
            $metaDataInput["is_deleted"]            =   "0";
            $metaDataInput["updated_by"]            =   "";
            $metaDataInput["updated_at"]            =   now()->toDateTimeString();

            //return $metaDataInput;
            
            $addedMetadata  =   $this->metadataRepository->fillAndSave($metaDataInput);
            return true;
        } else {
            return false;
        }
    }
}
