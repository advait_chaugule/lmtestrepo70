<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class GetMetadataDetailJob extends Job
{
    use DateHelpersTrait;

    private $metadataIdentifier;
    private $orgnizationIdentifier;
    private $metadataRepository;
    private $metadataDetail;
    private $metadataEntity;

    private $fieldTypeValues;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        /**
         * Set the private attribute to Metadata Identifier
         * and the organization identifier
         */

        $this->setIdentifier($input['metadata_id']);
        $this->setOrganizationIdentifier($input['organization_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //Set Role Repo handler
        $this->metadataRepository = $metadataRepository;

        //Set default settings for field_type
        $this->setFieldTypeValues();

        //get Role detail from database
        $metadataDetail = $this->getMetadataDetailFromModel();
        
        if(!empty($metadataDetail->metadata_id)){
            // set role details globally
            $this->setMetadataDetail($metadataDetail);
            // transform to and set the Role Structure
            $this->parseResponse();
            // finally return the CASE standard Item structure
            return $this->getMetadataEntity();
        }
        else{
            return false;
        }
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->metadataIdentifier = $identifier;
    }

    public function getIdentifier(){
        return $this->metadataIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->orgnizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->orgnizationIdentifier;
    }

    public function setMetadataDetail($data){
        $this->metadataDetail = $data;
    }

    public function getMetadataDetail(){
        return $this->metadataDetail;
    }

    public function setMetadataEntity($metadataEntity) {
        $this->metadataEntity = $metadataEntity;
    }

    public function getMetadataEntity() {
        return $this->metadataEntity;
    }

    public function setFieldTypeValues() {
        $this->fieldTypeValues = config("default_enum_setting.metadata_field_type");
    }

    public function getFieldTypeValues() {
        return $this->fieldTypeValues;
    }

    private function getMetadataDetailFromModel() {
        $metadataIdentifier = $this->getIdentifier();
        $organizationIdentifier = $this->getOrganizationIdentifier();

        $metadataDetail = $this->metadataRepository->getMetadataDetail($metadataIdentifier, $organizationIdentifier);

        return $metadataDetail;
    }

    private function parseResponse(){//$metadataDetail->field_possible_values
        $fieldTypeValues = $this->getFieldTypeValues();
        $metadataDetail = $this->getMetadataDetail();
        $organizationIdentifier = $this->getOrganizationIdentifier();
        
        /**
         * Get Field Possible Values 
         * for following 4 tables
         */
        $tempFieldType = ['subject','license','language','concept'];
        $fieldPossibleValues = "";
        if (in_array($metadataDetail->internal_name, $tempFieldType)){
            $fieldPossibleValues = $this->metadataRepository->getFieldPossibleValues($metadataDetail->internal_name, $organizationIdentifier);
        }else{
            $fieldPossibleValues = $metadataDetail->field_possible_values;
        }
        
        $metadataEntity = [
            "metadata_id"           =>  $metadataDetail->metadata_id,
            "name"                  =>  !empty($metadataDetail->name) ? $metadataDetail->name : "",
            "internal_name"         =>  !empty($metadataDetail->internal_name) ? $metadataDetail->internal_name : "",
            "field_type"            =>  !empty($metadataDetail->field_type) ? $fieldTypeValues[$metadataDetail->field_type] : '',
            "field_type_id"         =>  !empty($metadataDetail->field_type) ? $metadataDetail->field_type : '',
            "field_possible_values" =>  !empty($fieldPossibleValues) ? $fieldPossibleValues : "",
            "is_custom"             =>  $metadataDetail->is_custom,
            "is_active"             =>  $metadataDetail->is_active,
            "updated_at"            =>  $this->formatDateTimeToISO8601($metadataDetail->updated_at),
            "updated_by"            =>  !empty($metadataDetail->users->name) ? $metadataDetail->users->name : ""
        ];

        $this->setMetadataEntity($metadataEntity);
    }
}
