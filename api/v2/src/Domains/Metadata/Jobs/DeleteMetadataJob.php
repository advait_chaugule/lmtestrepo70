<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class DeleteMetadataJob extends Job
{
    private $metadataIdentifier;
    private $organizationIdentifier;
    private $metadataRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $metadataIdentifier, string $organizationIdentifier)
    {
        //Set the private identifier attribute
        $this->setMetadataIdentifier($metadataIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //Set the repository handler
        $this->metadataRepository   =   $metadataRepository;

        $deleteMetadata =   $this->deleteMetadataFromDb();

        return $this->parseResponse();
    }

    /**
     * Public Getter and Setter methods
     */
    public function setMetadataIdentifier(string $input) {
        $this->metadataIdentifier = $input;
    }

    public function getMetadataIdentifier() {
        return $this->metadataIdentifier;
    }

    public function setOrganizationIdentifier(string $input) {
        $this->organizationIdentifier = $input;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    private function deleteMetadataFromDb() {
        $metadataIdentifier     =   $this->getMetadataIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $columnValueFilterPairs =   ['metadata_id'  =>  $metadataIdentifier, 'organization_id' => $organizationIdentifier ];
        $attributes             =   ['is_deleted' => '1'];
        $deleteMetadata         =   $this->metadataRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
       
        return $deleteMetadata;
    }

    private function parseResponse() {
        $fieldTypeValues    =   config("default_enum_setting.metadata_field_type");
        $metadataIdentifier =   $this->getMetadataIdentifier();
        $metadataDetail     =   $this->metadataRepository->find($metadataIdentifier);

        $metadataEntity =   [
            "metadata_id"           =>  $metadataIdentifier,
            "name"                  =>  $metadataDetail->name,
            "field_type"            =>  $fieldTypeValues[$metadataDetail->field_type],
            "field_possible_values" =>  $metadataDetail->field_possible_values,
            "is_custom"             =>  $metadataDetail->is_custom,
            "is_active"             =>  $metadataDetail->is_active,
            "is_deleted"            =>  $metadataDetail->is_deleted,
            "updated_at"            =>  $metadataDetail->updated_at
        ];

        return $metadataEntity;
    }
}
