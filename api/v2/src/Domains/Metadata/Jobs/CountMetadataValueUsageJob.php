<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

use Illuminate\Support\Facades\DB;

class CountMetadataValueUsageJob extends Job
{
    private $metadataIdentifier;
    private $fieldPossibleValue;
    private $organizationId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $metadataIdentifier, string $fieldPossibleValue, string $organizationId)
    {
        //Set the private attributes
        $this->metadataIdentifier   =   $metadataIdentifier;
        $this->fieldPossibleValue   =   $fieldPossibleValue;
        $this->organizationId       =   $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo, ItemRepositoryInterface $itemRepo)
    {
        //
        $metadataIdentifier =   $this->metadataIdentifier;
        $fieldPossibleValue =   $this->fieldPossibleValue;
        $organizationId     =   $this->organizationId;

        // added new validation for field type 7 - ACMT-2856
        $field_type = $this->getfieldtype($metadataIdentifier);
       
        if($field_type == 7){
            $totalUsage = 1;
            if(!($fieldPossibleValue == 1 && $fieldPossibleValue == 2 ))
            {
                $documentUsageCount = DB::table('documents')
                ->where('organization_id','=',$organizationId)
                ->where('document_type','=',$fieldPossibleValue)
                ->where('is_deleted','=',0)
                ->get();
                $totalUsage = $documentUsageCount->count();
            }
        }else{
        $documentUsageCount =   $documentRepo->countMetadataValueUsage($metadataIdentifier, $fieldPossibleValue);

        $itemUsageCount =   $itemRepo->countMetadataValueUsage($metadataIdentifier, $fieldPossibleValue);

        $totalUsage =   $documentUsageCount + $itemUsageCount;
        }
        
        return $totalUsage;
        
    }

    private function getfieldtype($metadataIdentifier){
       
        $query = DB::table('metadata')->select('field_type')
        ->where('metadata_id','=',$metadataIdentifier)
        ->get()
        ->toArray();      
    
        return $query[0]->field_type;
    }
}
