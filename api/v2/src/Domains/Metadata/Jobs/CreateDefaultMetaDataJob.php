<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class CreateDefaultMetaDataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //Set the input variables
        $this->input    =   $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //
        return $metadataRepository->fillAndSave($this->input);
    }
}
