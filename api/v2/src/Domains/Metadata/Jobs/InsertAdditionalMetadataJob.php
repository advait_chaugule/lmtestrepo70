<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class InsertAdditionalMetadataJob extends Job
{
    private $itemRepository;
    private $documentRepository;
    private $metadataRepository;
    private $identifier;
    private $requestData;
    private $entityToAdd;
    private $fieldTypeValues;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($identifier, $requestData, $entityToAdd)
    {
        //Set the private data
        $this->setIdentifier($identifier);
        $this->requestData      =   $requestData;
        $this->entityToAdd      =   $entityToAdd;
        $this->fieldTypeValues  =   config("default_enum_setting.metadata_field_type");
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository, DocumentRepositoryInterface $documentRepository, MetadataRepositoryInterface $metadataRepository)
    {
        //Set the repository handler
        $this->itemRepository       =   $itemRepository;
        $this->documentRepository   =   $documentRepository;
        $this->metadataRepository   =   $metadataRepository;

        $requestData    =   $this->requestData;
        

        $addAdditionalMetadata      =   $this->addAdditionalMetadata();
        if($addAdditionalMetadata == true) {
            $metadataDetail = $this->metadataRepository->find($requestData['metadata_id']);

            $metadataEntity = [
                "metadata_id"           =>  $metadataDetail->metadata_id,
                "name"                  =>  !empty($metadataDetail->name) ? $metadataDetail->name : "",
                "internal_name"         =>  !empty($metadataDetail->internal_name) ? $metadataDetail->internal_name : "",
                "field_type"            =>  !empty($metadataDetail->field_type) ? $this->fieldTypeValues[$metadataDetail->field_type] : '',
                "field_type_id"         =>  !empty($metadataDetail->field_type) ? $metadataDetail->field_type : '',
                "is_custom"             =>  $metadataDetail->is_custom,
            ];

            return $metadataEntity;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Public Getter and Setter methods
     */
    public function setIdentifier($identifier) {
        $this->identifier  =    $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    private function addAdditionalMetadata() {
        $identifier =   $this->getIdentifier();
        
        $requestData    =   $this->requestData;
        $entityToAdd    =   $this->entityToAdd;

        $metadataDetail =   $this->metadataRepository->find($requestData['metadata_id']);
        $metadataHtmlValue = !empty($requestData['metadata_value_html']) ?$requestData['metadata_value_html']:"";
        if($entityToAdd == 'item')
        {
            /**
             * Update respective column of item table if additional metadata is a CASE metadata
             */
            
            if($metadataDetail->is_custom === 0) {
                $attributes[$metadataDetail->internal_name] = $requestData['metadata_value'];

                $this->itemRepository->edit($identifier, $attributes);
            }
            
            return $this->itemRepository->addOrUpdateCustomMetadataValue($identifier, $requestData['metadata_id'], $requestData['metadata_value'], $metadataHtmlValue, $requestData['is_additional'],'insert');
        }
        else{
            /**
             * Update respective column of document table if additional metadata is a CASE metadata
             */
            if($metadataDetail->is_custom === 0) {
                $attributes[$metadataDetail->internal_name] = $requestData['metadata_value'];

                $this->documentRepository->edit($identifier, $attributes);
            }

            return $this->documentRepository->addOrUpdateCustomMetadataValue($identifier, $requestData['metadata_id'], $requestData['metadata_value'], $metadataHtmlValue, $requestData['is_additional'], 'insert');
        }
        
    }
    
}
