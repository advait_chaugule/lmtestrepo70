<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class UpdateMetadataJob extends Job
{
    use DateHelpersTrait,UuidHelperTrait;

    private $metadataIdentifier;
    private $organizationIdentifier;

    private $requestData;
    private $metadataRepository;

    private $fieldTypeValues;
    private $metadataEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, array $input)
    {
        //Set the Identifier
        $this->setMetadataIdentifier($identifier);
        $this->setOrganizationIdentifier($input['auth_user']['organization_id']);
        $this->setInputData($input);

        //Set config for default field_type 
        $this->setFieldTypeValues();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //Set the metadata repository handler
        $this->metadataRepository = $metadataRepository;

        $metadataIdentifier     = $this->getMetadataIdentifier();
        $organizationIdentifier = $this->getOrganizationIdentifier();

        $requestData = $this->getInputData();

        $metadataDetail = $this->metadataRepository->findBy('metadata_id', $metadataIdentifier);

        if(isset($requestData['is_active'])){
            $attribute      =   ['is_active' => $requestData['is_active']];
            $checkAttribute =   ['parent_metadata_id'   =>  $metadataIdentifier];

            if( !empty($metadataDetail->parent_metadata_id) ) {
                $findParentMetadata    =   $this->metadataRepository->findBy('metadata_id', $metadataDetail->parent_metadata_id);

                if($findParentMetadata->is_active == '0' && $requestData['is_active'] == '1'){
                    $updatedParentMetadata = $this->metadataRepository->edit($findParentMetadata->metadata_id, $attribute);
                }
            }
            else {
                $findChildMetadataId    =   $this->metadataRepository->findByAttributes($checkAttribute);

                if(sizeOf($findChildMetadataId) > 0){
                    foreach($findChildMetadataId as $childMetadata) {
                        $updatedChildMetadata = $this->metadataRepository->edit($childMetadata->metadata_id, $attribute);
                    }
                }  
            }

            

            $updatedMetadata = $this->metadataRepository->edit($metadataIdentifier, $attribute);
            
            $this->parseResponse();

            $metadataEntity = $this->getMetadataEntity();
            return $metadataEntity;
        }
        else{
            if(isset($requestData['is_active']) && $requestData['is_active'] === null){
                $this->parseResponse();

                $metadataEntity = $this->getMetadataEntity();
                return $metadataEntity;
            }
            else{
                /**
                 * Get Field Possible Values 
                 * for following 4 tables
                 */
                
                $attributeSet['name']               =   $requestData['name'];
                $attributeSet['field_type']         =   $requestData['field_type'];
                $attributeSet['updated_by']         =   $requestData['auth_user']['user_id'];
                $attributeSet['organization_id']    =   $organizationIdentifier;
                
                $updatedMetadata = $this->metadataRepository->edit($metadataIdentifier, $attributeSet);

                if($requestData['field_type'] == '3'){
                    if($metadataDetail->internal_name == 'language'){
                        $attributes = ['is_deleted' => '1'];
                        $this->metadataRepository->updateAllFieldPossibleValues($organizationIdentifier, $attributes);

                        foreach($requestData['field_possible_values'] as $values){
                            if($values['language_id'] === null){
                                $this->createNewListValue($values);
                            }
                            else{
                                $attributes = ['name' => $values['name'], 'short_code' => $values['short_code'],  'is_deleted' => '0'];
                                $this->metadataRepository->updateFieldPossibleValuesById($values['language_id'], $attributes);
                            }
                        }
                    } else {
                        $result =   []; 
                        $attributeSet['field_possible_values']       = implode(',',$requestData['field_possible_values']);

                        $updatedMetadata = $this->metadataRepository->edit($metadataIdentifier, $attributeSet);

                        $metadataDetail = $this->metadataRepository->find($metadataIdentifier);

                        $fieldPossibleValues        =   explode(',', $metadataDetail->field_possible_values);

                        $lastFieldPossibleValues    =   explode(',', $metadataDetail->last_field_possible_values);

                        $updatedFieldPossibleValues =   array_diff($fieldPossibleValues,$lastFieldPossibleValues);

                        foreach($lastFieldPossibleValues as $key=>$values){
                            foreach($updatedFieldPossibleValues as $updatedKey => $updatedValues) {
                                if($key == $updatedKey) {
                                    $result[$values] =   $updatedValues;
                                }
                            }
                        }                 

                        foreach($result as $key => $metadataValueUpdated){
                            $this->metadataRepository->updateSelectedFieldPossibleValueOnChangingMetadata($metadataIdentifier, $key, $metadataValueUpdated);
                        }

                        $attributeSet['last_field_possible_values']       = implode(',',$requestData['field_possible_values']);
                        $updatedMetadata = $this->metadataRepository->edit($metadataIdentifier, $attributeSet);
                    }
                }else if($requestData['field_type'] == '7'){
                    // added for creating new metadata field(dictonary type)
                    $fieldPossibleValues = json_encode($requestData['field_possible_values']);
                    $attributeSet['field_possible_values']  = $fieldPossibleValues;
                    $attributeSet['last_field_possible_values'] = $fieldPossibleValues;
                    $updatedMetadata = $this->metadataRepository->edit($metadataIdentifier, $attributeSet);

                }


                $this->parseResponse();

                $metadataEntity = $this->getMetadataEntity();
                return $metadataEntity;
            }   
            
        }     
    }

    public function setMetadataIdentifier($identifier){
        $this->metadataIdentifier = $identifier;
    }

    public function getMetadataIdentifier(){
        return $this->metadataIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }

    public function setInputData($input){
        $this->requestData = $input;
    }

    public function getInputData(){
        return $this->requestData;
    }

    public function setFieldTypeValues() {
        $this->fieldTypeValues = config("default_enum_setting.metadata_field_type");
    }

    public function getFieldTypeValues() {
        return $this->fieldTypeValues;
    }

    public function setMetadataEntity($metadataEntity){
        $this->metadataEntity = $metadataEntity;
    }

    public function getMetadataEntity(){
        return $this->metadataEntity;
    }

    private function createNewListValue($values){
        $generatedValueId = $this->createUniversalUniqueIdentifier();
        $organizationId = $this->getOrganizationIdentifier();

        $createData = ['language_id' => $generatedValueId, 'organization_id' => $organizationId, 'name' => $values['name'], 'short_code' => $values['short_code']];

        $this->metadataRepository->createFieldPossibleValues($createData);
    }

    private function parseResponse(){
        $fieldTypeValues = $this->getFieldTypeValues();
        
        $metadataIdentifier = $this->getMetadataIdentifier();
        $metadataDetail = $this->metadataRepository->find($metadataIdentifier);

        $metadataEntity = [
            "metadata_id"           =>  $metadataIdentifier,
            "name"                  =>  !empty($metadataDetail->name) ? $metadataDetail->name : "",
            "internal_name"         =>  !empty($metadataDetail->internal_name) ? $metadataDetail->internal_name : "",
            "field_type"            =>  !empty($metadataDetail->field_type) ? $fieldTypeValues[$metadataDetail->field_type]: "",
            "field_possible_values" =>  !empty($metadataDetail->field_possible_values) ? $metadataDetail->field_possible_values : "",
            "is_custom"             =>  $metadataDetail->is_custom,
            "is_active"             =>  $metadataDetail->is_active,
            "updated_at"            =>  $this->formatDateTimeToISO8601($metadataDetail->updated_at)
        ];
        
        $this->setMetadataEntity($metadataEntity);
    }
}
