<?php
namespace App\Domains\MetaData\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class GetCustomMetaDataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->item_ids = $input['item_ids'];
        $this->custom_meta_data_field_names = $input['custom_meta_data_field_names'];
        $this->items_data = $input['items_data'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        # add custom_meta_data_field_names to the case metadata array
        $custom_items = DB::table('item_metadata')
                            ->select('item_metadata.*', 'metadata.name as metadata_name')
                            ->leftJoin('metadata','item_metadata.metadata_id', '=', 'metadata.metadata_id')
                            ->whereIn('item_metadata.item_id', $this->item_ids)
                            ->whereIn('metadata.name', $this->custom_meta_data_field_names)
                            ->get();

        foreach ($custom_items as $custom_item) {
            foreach ($this->custom_meta_data_field_names as $custom_field_name) {
                if(!empty($custom_item->metadata_value)) {
                    $this->items_data[$custom_item->item_id][$custom_item->metadata_name] = $custom_item->metadata_value;
                }
            }
        }

        return ['items_data' => $this->items_data];
    }
}
