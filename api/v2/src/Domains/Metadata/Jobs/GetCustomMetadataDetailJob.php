<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class GetCustomMetadataDetailJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nodeIdIdentifier)
    {
        $this->nodeIdIdentifier = $nodeIdIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        $this->metadataRepository   =   $metadataRepository;

        $getCustomMetadataId      =   $this->getCustomMetadataDetail();

        return $getCustomMetadataId;
    }

    public function getCustomMetadataDetail()
    {
        $nodeIdIdentifier = $this->nodeIdIdentifier;
        
        return $this->metadataRepository->getCustomMetadataDetailInfo($nodeIdIdentifier);

    }
}
