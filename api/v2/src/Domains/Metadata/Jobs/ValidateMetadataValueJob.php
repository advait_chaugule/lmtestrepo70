<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class ValidateMetadataValueJob extends Job
{
    private $metadataIdentifier;
    private $fieldPossibleValue;
    private $organizationId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $metadataIdentifier, string $fieldPossibleValue, string $organizationId)
    {
        //Set the private attributes
        $this->metadataIdentifier   =   $metadataIdentifier;
        $this->fieldPossibleValue   =   $fieldPossibleValue;
        $this->organizationId       =   $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //
        $metadataIdentifier =   $this->metadataIdentifier;
        $fieldPossibleValue =   $this->fieldPossibleValue;
        $organizationId     =   $this->organizationId;

        $attributes =   ['metadata_id'  =>  $metadataIdentifier, 'organization_id' => $organizationId];

        $metadataDetail =   $metadataRepository->findByAttributes($attributes)->first()->toArray();
        
        // added new validation for field type 7 - ACMT-2856
        if($metadataDetail['field_type'] == 7)
        {
            $data = json_decode($metadataDetail['field_possible_values']);
            $flag = 0;

            foreach($data as $datak => $dataV)
            {
                if($dataV->key == $fieldPossibleValue)
                {
                    $flag = 1;
                }
            }

            if($flag == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            $metadataFieldPossibleValue =   explode(',', $metadataDetail['field_possible_values']);

            if(in_array($fieldPossibleValue, $metadataFieldPossibleValue) == true){
                return true;
            }
            else {
                return false;
            }
        }
        
    }
}
