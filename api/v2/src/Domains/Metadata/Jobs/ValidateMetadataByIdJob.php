<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Metadata\Validators\MetadataExistsValidator;

class ValidateMetadataByIdJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //set private attribute to input data
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataExistsValidator $validator)
    {   
        //Check the existence for the metadata id
        $validation = $validator->validate($this->identifier);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
