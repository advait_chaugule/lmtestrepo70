<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Metadata\Validators\MultipleItemDetailsValidator;

class ValidateMutlipleItemDetailsJob extends Job
{

    public $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MultipleItemDetailsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->errors()->all();
    }
}
