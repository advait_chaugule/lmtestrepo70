<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class DestroyAdditionalMetadataJob extends Job
{
    private $metadataRepository;
    private $identifier;
    private $metadataIdentifier;
    private $entityFromDelete;
    private $fieldTypeValues;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($identifier, $metadataIdentifier, $entityFromDelete)
    {
        //Set the private data
        $this->setIdentifier($identifier);
        $this->setMetadataIdentifier($metadataIdentifier);
        $this->entityFromDelete  =   $entityFromDelete;
        $this->fieldTypeValues  =   config("default_enum_setting.metadata_field_type");
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        //Set the repository handler
        $this->metadataRepository       =   $metadataRepository;
        $deleteAdditionalMetadata       =   $this->deleteAdditionalMetadata();
        if($deleteAdditionalMetadata == true) {
            $metadataIdentifier = $this->getMetadataIdentifier();  
            $metadataDetail     = $this->metadataRepository->find($metadataIdentifier);
            
            $metadataEntity = [
                "metadata_id"           =>  $metadataDetail->metadata_id,
                "name"                  =>  !empty($metadataDetail->name) ? $metadataDetail->name : "",
                "internal_name"         =>  !empty($metadataDetail->internal_name) ? $metadataDetail->internal_name : "",
                "field_type"            =>  !empty($metadataDetail->field_type) ? $this->fieldTypeValues[$metadataDetail->field_type] : '',
                "field_type_id"         =>  !empty($metadataDetail->field_type) ? $metadataDetail->field_type : '',
                "is_custom"             =>  $metadataDetail->is_custom,
            ];

            return $metadataEntity;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Public Getter and Setter methods
     */
    public function setIdentifier($identifier) {
        $this->identifier  =    $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setMetadataIdentifier($identifier) {
        $this->metadataIdentifier  =    $identifier;
    }

    public function getMetadataIdentifier() {
        return $this->metadataIdentifier;
    }

    private function deleteAdditionalMetadata() {
        $identifier         =   $this->getIdentifier();
        $metadataIdentifier =   $this->getMetadataIdentifier();

        $entityFromDelete   =   $this->entityFromDelete;
        
        return $this->metadataRepository->deleteAdditionalMetadata($identifier, $metadataIdentifier, $entityFromDelete);
        
    }
    
}
