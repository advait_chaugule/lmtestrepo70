<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

use Illuminate\Support\Facades\DB;

class ListMetadataJob extends Job
{
    use DateHelpersTrait;

    private $metadataRepository;
    private $requestData;
    private $organizationIdentifier;
    private $fieldTypeValues;
    private $usageCount;
    private $itemRepository;
    private $documentRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the input data
        $this->requestData = $input;
        $this->setOrganizationIdentifier($input->all()['auth_user']['organization_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository, ItemRepositoryInterface $itemRepository, DocumentRepositoryInterface $documentRepository)
    {
        // set database repositories
        $this->metadataRepository   =   $metadataRepository;
        $this->itemRepository       =   $itemRepository;
        $this->documentRepository   =   $documentRepository;

        $requestData                =   $this->requestData;
        $requestData['sort']        =   'name:asc';
        
        // set the fieldTypes from config
        $this->setFieldTypeValues();
        
        // get all metadata
        $metadata = $this->metadataRepository->getMetadataList($requestData);
        $parsedMetadata = $this->transFormMetadataResponse($metadata);
        // prepare the response
        return ['metadata' => $parsedMetadata];
    }

    public function setFieldTypeValues() {
        $this->fieldTypeValues = config("default_enum_setting.metadata_field_type");
    }

    public function getFieldTypeValues() {
        return $this->fieldTypeValues;
    }

    public function setMetadataIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getMetadataIdentifier(){
        return $this->identifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier(){
        return $this->organizationIdentifier;
    }

    public function setUsageCount($usageCount) {
        $this->usageCount = $usageCount;
    }

    public function getUsageCount(){
        return $this->usageCount;
    }

    private function fetchUsageCount(){
        $metadataIdentifier = $this->getMetadataIdentifier();
        $nodeTypeList   = $this->metadataRepository->fetchNodeTypeList($metadataIdentifier);

        $itemCount      = $this->calculateUsageCount('item', $nodeTypeList);
        $documentCount  = $this->calculateUsageCount('document', $nodeTypeList);
        
        $totalCount = (int) $itemCount + (int) $documentCount;

        $this->setUsageCount($totalCount);

    }

    private function calculateUsageCount($type, $nodeTypeList){
        if($type === 'item'){
            $attributeIn = 'node_type_id';
            $containedInValues = $nodeTypeList;
            $returnCollection = false;
            $fieldsToReturn = ['item_id'];
            return $this->itemRepository->whereCount($fieldsToReturn,$attributeIn, $containedInValues);            
        }
        else{
            $attributeIn = 'node_type_id';
            $containedInValues = $nodeTypeList;
            $returnCollection = false;
            $fieldsToReturn = ['document_id'];
            return $this->documentRepository->whereCount($fieldsToReturn,$attributeIn, $containedInValues);
        }
    }

    private function transFormMetadataResponse($metadataList){
        $fieldTypeValues = $this->getFieldTypeValues();
        $organizationIdentifier = $this->getOrganizationIdentifier();
        $parsedMetadata = [];
        $input=$this->requestData->all();
        if($metadataList->isNotEmpty()){
            foreach($metadataList as $metadata) {
                $this->setMetadataIdentifier($metadata->metadata_id);

                if(isset($input['show_count'])){               
                    $usageCount = $this->fetchUsageCount();
                }

                // added for field type 7 for - ACMT-2856
                if($metadata->field_type == 7){
                  $fieldPossibleValues = !empty($metadata->field_possible_values) ? json_decode($metadata->field_possible_values) : '';  
                }else{
                  $fieldPossibleValues = !empty($metadata->field_possible_values) ? explode(',' , $metadata->field_possible_values) : '';
                }
                
                $showUsageCount= 0;
                if($metadata->field_type == 7)
                {
                    # UF-504: optimized code by directly fetching count instead of first fetching data and then calculating count, removed unwanted code
                    $showUsageCount = DB::table('documents')
                        ->where('organization_id','=',$organizationIdentifier)
                        ->where('is_deleted','=',0)
                        ->count();
                }
                else
                {
                    $showUsageCount = isset($input['show_count'])?$this->getUsageCount():0;
                }
                //print_r($usageCount);
                $parsedMetadata[] = [
                    'metadata_id'           => $metadata->metadata_id,
                    'parent_metadata_id'    => $metadata->parent_metadata_id,
                    'name'                  => !empty($metadata->name) ? $metadata->name : '',
                    'internal_name'         => !empty($metadata->internal_name) ? $metadata->internal_name : '',
                    'type'                  => !empty($metadata->field_type) ? $fieldTypeValues[$metadata->field_type] : '',
                    'field_type_id'         => !empty($metadata->field_type) ? $metadata->field_type : '',
                    'field_possible_values' => $fieldPossibleValues,
                    'order'                 => $metadata->order,
                    'is_custom'             => $metadata->is_custom,
                    'is_document'           => $metadata->is_document,
                    'is_mandatory'          => $metadata->is_mandatory,
                    'is_active'             => $metadata->is_active,
                    'updated_by'            => !empty($metadata->user->name) ? $metadata->user->name : "",
                    'updated_at'            => $this->formatDateTimeToISO8601($metadata->updated_at),
                    'usage_count'           => $showUsageCount,
                ];
            }
        }

        return $parsedMetadata;
    }

}
