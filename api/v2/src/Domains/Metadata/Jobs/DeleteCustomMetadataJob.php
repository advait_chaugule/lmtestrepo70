<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class DeleteCustomMetadataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nodeIdIdentifier,$customMetadataIdIdentifier,$metaDataIdSaved)
    {
        $this->nodeIdIdentifier = $nodeIdIdentifier;
        $this->customMetadataIdIdentifier = $customMetadataIdIdentifier;
        $this->metaDataIdSaved = $metaDataIdSaved;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        $this->metadataRepository   =   $metadataRepository;

        $deleteCustomMetadata      =   $this->deleteCustomMetadata();
    }

    public function deleteCustomMetadata()
    {
        $nodeIdIdentifier = $this->nodeIdIdentifier;
        $customMetadataIdIdentifier = $this->customMetadataIdIdentifier;
        $metaDataIdSaved = $this->metaDataIdSaved;
        
        $this->metadataRepository->deleteCustomMetadataInfo($nodeIdIdentifier,$customMetadataIdIdentifier,$metaDataIdSaved);

    }

}
