<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class SaveCustomMetadataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $itemsArr;
    public function __construct($customMetadata,$importDocumentId,$organizationId,$items,$newNodeCreated,$copy)
    {
        $this->customMetadata     = $customMetadata;
        $this->import_document_id = $importDocumentId;
        $this->organizationId     = $organizationId;
        $this->itemsArr           = $items;
        $this->copy           = $copy;
        $this->newNodeCreated           = $newNodeCreated;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository)
    {
        $this->metadataRepository   =   $metadataRepository;
        $customMetadata = $this->customMetadata;
        $importDocumentId = $this->import_document_id;
        $organizationId = $this->organizationId;
        $itemData       = $this->itemsArr;
        $copy       = $this->copy;
        $newNodeCreated       = $this->newNodeCreated;

        
        $savedCustomMetadataData          =   $this->metadataRepository->saveCustomMetadata($customMetadata,$importDocumentId,$organizationId,$itemData,$newNodeCreated,$copy);
        // return $savedMetadataData;
    }
}
