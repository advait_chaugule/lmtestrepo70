<?php
namespace App\Domains\MetaData\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class GetAllowedMetaDataFieldsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input, $mandatory_fields)
    {
        $this->item_ids = $input;
        $this->mandatory_fields = $mandatory_fields;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $metadata_part_of_item_arr = [];
        #for adding metadata field names to be allowed in output
        $metadata_part_of_item = DB::table('items')
                                    ->select('items.item_id', 'metadata.internal_name','metadata.name')
                                    ->leftJoin('node_type_metadata','items.node_type_id','=','node_type_metadata.node_type_id')
                                    ->leftJoin('metadata','metadata.metadata_id','=','node_type_metadata.metadata_id')
                                    ->whereIn('items.item_id', $this->item_ids)
                                    ->get();
        foreach ($metadata_part_of_item  as $row) {
            if($row->internal_name !== null) {
                $metadata_part_of_item_arr[$row->item_id][] = $row->internal_name;
            } else {
                $metadata_part_of_item_arr[$row->item_id][] = $row->name;
            }
        }

        #for adding additional metadata field names to field names allowed in output
        $additional_metadata_part_of_item = DB::table('item_metadata')
                                                ->select('item_metadata.item_id', 'metadata.name')
                                                ->leftJoin('metadata','item_metadata.metadata_id', '=', 'metadata.metadata_id')
                                                ->whereIn('item_id', $this->item_ids)
                                                ->where('item_metadata.is_additional', 1)
                                                ->get();
        foreach ($additional_metadata_part_of_item as $row) {
            array_push($metadata_part_of_item_arr[$row->item_id], $row->name);
        }

        #add mandatory fields
        foreach ($this->mandatory_fields as $field) {
            foreach ($metadata_part_of_item_arr as $item_id => $fields) {
                array_push($metadata_part_of_item_arr[$item_id], $field);
            }
        }

        return $metadata_part_of_item_arr;
    }
}
