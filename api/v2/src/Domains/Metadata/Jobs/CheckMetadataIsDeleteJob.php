<?php
namespace App\Domains\Metadata\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class CheckMetadataIsDeleteJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepo)
    {
        //Check whether Role deleted
        $attributes = ['metadata_id' => $this->identifier, 'is_deleted' => '1'];
        return $metadataRepo->findByAttributes($attributes)->isNotEmpty();
    }
}
