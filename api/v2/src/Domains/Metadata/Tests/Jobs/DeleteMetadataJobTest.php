<?php
namespace App\Domains\Metadata\Tests\Jobs;

use App\Domains\Metadata\Jobs\DeleteMetadataJob;
use Tests\TestCase;

class DeleteMetadataJobTest extends TestCase
{
    public function test_MetadataIdentifier()
    {
        $assets = new DeleteMetadataJob('c4424f55-8900-446c-9c41-2f2c25d58c08','989d3047-c369-4b09-b4f1-dee410aa84be');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataIdentifier($value);
        $this->assertEquals($value,$assets->getMetadataIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new DeleteMetadataJob('c4424f55-8900-446c-9c41-2f2c25d58c08','989d3047-c369-4b09-b4f1-dee410aa84be');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
