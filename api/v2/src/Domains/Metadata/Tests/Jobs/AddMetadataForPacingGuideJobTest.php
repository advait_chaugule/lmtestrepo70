<?php
namespace App\Domains\Metadata\Tests\Jobs;

use App\Domains\Metadata\Jobs\AddMetadataForPacingGuideJob;
use Tests\TestCase;

class AddMetadataForPacingGuideJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $assets = new AddMetadataForPacingGuideJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
