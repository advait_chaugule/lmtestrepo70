<?php
namespace App\Domains\Metadata\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class MetadataExistsValidator extends BaseValidator {

    protected $rules = [
        'metadata_id'       =>  'required|exists:metadata,metadata_id'
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
    ];
    
}