<?php
namespace App\Domains\Metadata\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class MultipleItemDetailsValidator extends BaseValidator {

    protected $rules = [
        'document_id' => 'required|string',
        'item_ids' => 'required|array|min:1',
        'meta_data_field_names' => 'required|array|min:1',
        'include_children' => 'required'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "array" => ":attribute should be an array.",
        "string" => ":attribute should be a string.",
        "min" => ":attribute should contain minimum one element."
    ];
}