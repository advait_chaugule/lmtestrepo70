<?php
namespace App\Domains\CfDoc\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CFDocumentValidator extends BaseValidator {

    protected $rules = [
        'title' => 'required|max:120',
        'creator' => 'required|max:255',
        'official_uri' => 'url|max:255',
        'organization_id' => 'string|exists:organizations,id',
        'publisher_id' => 'string|exists:publishers,id',
        'url_name' => 'max:255',
        'version' => 'max:50',
        'description' => 'max:255',
        'note'      => 'max:255',
        'subject_id' => 'string|exists:subjects,id',
        'language_id' => 'string|exists:languages,id',
        'adoption_status' => 'in:private_draft,draft,adopted,deprecated',
        'status_start' => "date_format:Y-m-d",
        'status_end' => "date_format:Y-m-d"
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'string' => ':attribute should be an string.', 
        'url' => ':attribute should be of type url.',
        'max:255' => ':attribute should be of mazimum 255 characters.',
        'max:120' => ':attribute should be of mazimum 120 characters.',
        'max:50' => ':attribute should be of mazimum 50 characters.',
        'max:10' => ':attribute should be of mazimum 10 characters.',
        'in' => ':attribute must be of type Private Draft, Draft, Adopted or Deprecated.'
     ];
    
}