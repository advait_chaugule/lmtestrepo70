<?php
namespace App\Domains\CfDoc\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CFDocumentIdsValidator extends BaseValidator {

    protected $rules = [
        'ids.*' => 'required|exists:taxonomies,id'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}