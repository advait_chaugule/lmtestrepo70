<?php
namespace App\Domains\Cfdoc\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\TaxonomyRepositoryInterface;

class GetCFDocByIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxonomyRepositoryInterface $taxonomyRepo)
    {
        $cfdoc = $taxonomyRepo->find($this->data['id']);
        return $cfdoc;
    }
}
