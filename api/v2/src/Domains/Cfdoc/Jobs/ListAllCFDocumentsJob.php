<?php
namespace App\Domains\Cfdoc\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\TaxonomyRepositoryInterface;

class ListAllCFDocumentsJob extends Job
{

    private $params;
    private $defaultParams;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->params = $input;
        $this->defaultParams = ["parent_id" => 0];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxonomyRepositoryInterface $taxonomyRepo)
    {
        // set the filters and their respective statuses to default
        $paginationFilters = $sortfilters = [];
        $returnPaginatedData = $returnSortedData = false;
        
        // check and set parameters if pagination and sorting parameters are present
        if(!empty($this->params)){
            $returnPaginatedData = (array_key_exists('limit', $this->params) && array_key_exists('offset', $this->params)) ? true : false;
            $returnSortedData = (array_key_exists('orderBy', $this->params) && array_key_exists('sorting', $this->params)) ? true : false;
            // assign the input param array to both the filters
            $paginationFilters = $sortfilters = $this->params;
        }
        // fetch and return the data
        return $taxonomyRepo->findByAttributes($this->defaultParams, $returnPaginatedData, $paginationFilters, $returnSortedData);
    }
}
