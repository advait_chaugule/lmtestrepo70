<?php
namespace App\Domains\Cfdoc\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\TaxonomyRepositoryInterface;

class ValidateTaxonomyIsCFDocumentJob extends Job
{
    private $identifier;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxonomyRepositoryInterface $taxonomyRepository)
    {
        $taxonomy = $taxonomyRepository->find($this->identifier);
        return $taxonomy->cf_entity_type==="cf_doc";
    }
}
