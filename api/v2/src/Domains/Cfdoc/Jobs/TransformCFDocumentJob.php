<?php
namespace App\Domains\Cfdoc\Jobs;

use Lucid\Foundation\Job;

class TransformCFDocumentJob extends Job
{

    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cfDocListData = [];
        foreach($this->data as $cfDoc){
            $cfDocListData[] = [
                'id' => $cfDoc->id,
                'identifier' => $cfDoc->identifier,
                'uri' => $cfDoc->uri,
                'title' => $cfDoc->title,
                'created_at' => $cfDoc->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $cfDoc->updated_at->format('Y-m-d H:i:s')
            ];
        }
        return $cfDocListData;
    }
}
