<?php
namespace App\Domains\Organization\Jobs;

use Illuminate\Support\Facades\DB;
use Lucid\Foundation\Job;

class GetOrganizationIdByOrgCodeJob extends Job
{
    private $orgCode;
    public function __construct($orgCode)
    {
        $this->orgCode = $orgCode;
    }

    public function handle()
    {
        $organizationId = DB::table('organizations')->select('organization_id')
                   ->where('org_code',$this->orgCode)
                   ->where('is_deleted',0)
                   ->first();
        return $organizationId;
    }
}