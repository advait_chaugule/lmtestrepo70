<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

class CreateOrganizationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrganizationRepositoryInterface $oragnizationRepo)
    {
        //
        //dd($this->input);
        return $oragnizationRepo->fillAndSave($this->input);
    }
}
