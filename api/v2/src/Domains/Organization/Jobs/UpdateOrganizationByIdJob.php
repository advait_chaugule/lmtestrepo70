<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateOrganizationByIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $organizationId;
    private $organizationName;
    private $isActive;
    public function __construct($input)
    {
        $this->organizationId = $input['organization_id'];
        $this->organizationName = $input['name'];
        $this->isActive = $input['is_active'];
        $this->user_id = $input['user_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $organizationId = $this->organizationId;        
        $updateFields = [];
        if(!empty($this->organizationName)){
            $updateFields['name'] = $this->organizationName;
        }
        if($this->isActive=='0' || $this->isActive=='1'){
            $updateFields['is_active'] = $this->isActive;
        }
        $organizationName = $this->organizationName;
        $isActive = $this->isActive;

        if(!empty($updateFields)){
            $updateFields['updated_by'] = $this->user_id;
            $updateFields['updated_at'] = now()->toDateTimeString();
            try {
                DB::table('organizations')
                ->where('organization_id','=',$organizationId)
                ->where('organizations.is_deleted','=','0')
                ->update($updateFields);

                return true;
            } catch (\Illuminate\Database\QueryException $ex) {
                Log::error($ex);
                return false;
            }
        }
        return false;
    }
}
