<?php
namespace App\Domains\Organization\Jobs;
use Lucid\Foundation\Job;
use DB;
class GetOrgMappingDetails extends Job
{
    public function handle(){
        $org1    = $this->organization1();
        $data1   = json_decode(json_encode($org1), true);// 654 data
        $dataArr = [];
        //list of data from org1 org2 and org 3 and check node_org is not equal to org_id
        foreach ($data1 as $ArrOrg1 ) {
           if($ArrOrg1['node_organization_id']!=$ArrOrg1['organization_id']) {
               $dataArr[] = $ArrOrg1;
           }
        } // return 173 data
        //$count = count($dataArr);
        $myData = [];
        // get metadata_id list // 173 data list
        foreach ($dataArr as $orgFinalData) {
            $myData[]  = $orgFinalData['metadata_id']; // meta data list

        }
//      print_r($myData);die;
        // get unique metadata id  // 43 data
        $resultsMetaDataId = array_unique($myData);
        //print_r(count($resultsMetaDataId));die;
        // check unique meta data id in all metadata id list
        foreach ($dataArr as $orgFinalData) {
            if(in_array($orgFinalData['metadata_id'],$resultsMetaDataId)) {
                $myDatas[]  = $orgFinalData;
            }
        }
        //print_r($myDatas);die;
        //print_r($dataArr);die;
        //echo "Only org2 data";
        // get org 2 list
        $org2    = $this->getOrg2NodeType();
        $data2   = json_decode(json_encode($org2), true);
        //print_r($data2);die;
        $dataArr2=[];
        foreach ($data2 as $dataOrg2) {
            $dataArr2[]  = $dataOrg2['node_type_id'];
        }
        //print_r($dataArr2);die;
        // check org 2 data in first condition and other will return all remaining data
        $other2Arr=[];
        $org2Arr =[];
        foreach ($dataArr as $myData) {
            if(in_array($myData['node_type_id'],$dataArr2))
            {
                $org2Arr[] = $myData;      // 17
            }else{
                $other2Arr[] = $myData;   // 157
            }
        }
      //print_r(($org2Arr));die;
//       echo "<br>";
//       print_r(count($other2Arr));die;

        // Get All metadata list // 289 data
        $metaList=$this->getMetaDataList();
        $dataMetaList= json_decode(json_encode($metaList), true);

        $org1Pair =[];
        $org3Pair =[];
        $org3Name =[];
        //print_r($other2Arr);die;
       foreach ($other2Arr as $defaultArr) {
           if($defaultArr['node_organization_id']=='a83db6c0-1a5e-428e-8384-c8d58d2a83ff') { // org 1 data
                $org1Pair[] =  $defaultArr;
           }
           //print_r($org1Pair);die; //produce blank array
           if($defaultArr['node_organization_id']=='9518d0fe-e174-4b93-aad8-6295d34287a1') { // org 3 data
               $org3Pair[] = $defaultArr;
               $org3Name[] = $defaultArr['internal_name'];
           }

       }
       //print_r($org3Pair);//die; // 156 data
       //print_r($org1Pair);die; // empty array
//       echo "<br>";
        $resultName = array_unique($org3Name);// org3
        $resultArr=[];
        if(count($org3Pair>0)) {
           foreach ($dataMetaList as $metaList)
           {
               if(in_array($metaList['internal_name'],$resultName)&& $metaList['organization_id']=='9518d0fe-e174-4b93-aad8-6295d34287a1') // org 3 check
               {
                 $resultArr[$metaList['internal_name']] = $metaList['metadata_id'];
               }
           }
        }
    // print_r($org3Pair); // 156 data
//      echo "<br>";
//    print_r(count($resultArr));die; // 26

        //print_r($org2Arr);die;
        $filterArrData = [];
        $filterArrData2 = [];
        //print_r($org2Arr);die;
        foreach ($org2Arr as $filterArr)
        {
            if($filterArr['organization_id']=='9518d0fe-e174-4b93-aad8-6295d34287a1')// org3
            {
//                echo "org3";
//                print_r($filterArr);
                $filterArrData[]= $filterArr;
            }
            if($filterArr['organization_id']=='a83db6c0-1a5e-428e-8384-c8d58d2a83ff') // org1
            {
//                echo "org1";
//                print_r($filterArr);
                 $filterArrData2[]= $filterArr;
            }
        }
//        echo "org3 =";
      // print_r(count($filterArrData2));die;
//        echo "<br>";
        //echo "org1=";

        // get node type list
        $nodeList = $this->getNodeTypeDataList();
        $dataNodeTypeList= json_decode(json_encode($nodeList), true);
        $nodeOrg1=[];
        $nodeOrg3=[];
//        print_r(count($dataArr));die;
        $org1RightData=[];
        foreach ($data1 as $allData)
        {
            if($allData['node_organization_id']=='9518d0fe-e174-4b93-aad8-6295d34287a1'){ // org3
               $nodeOrg3[]=$allData;
            }

            if($allData['node_organization_id']=='a83db6c0-1a5e-428e-8384-c8d58d2a83ff'){ //org1
                $nodeOrg1[]=$allData;
            }
        }
//        echo "org3=";
//        print_r(count($nodeOrg3));
//        echo "<br>";
//        echo "org1=";
     //  print_r($nodeOrg1);die();
      //  print_r($org2Arr);die;
     foreach ($nodeOrg1 as $org1Data)
     {
         foreach ($org2Arr as $filter6)
         {
             if($org1Data['internal_name']==$filter6['internal_name'] && $org1Data['title'] == $filter6['title'] )
             {
               $org1RightData[] =  $org1Data;
             }
         }
     }
     $internalName=[];
     foreach ($org1RightData as $nameOrg1)
     {
         $internalName[] = $nameOrg1['internal_name'];
     }

        if(count($internalName>0)) {
            foreach ($dataMetaList as $metaList)
            {
                if(in_array($metaList['internal_name'],$internalName)&& $metaList['organization_id']=='f068a10c-96a7-48dd-9366-b71a199da48e') // check internal name with org 2
                {
                    $resultArr1[$metaList['internal_name']] = $metaList['metadata_id'];
                }
            }
        }
        //print_r($resultArr1);
        //print_r($org2Arr);
        //print_r($internalName);
        //print_r($org1RightData);die;
}

    public function checkDelete()
    {
        $query = DB::table('node_type_metadata')
            ->where('node_type_id','=','07893e8f-2a07-47ad-a213-419922a15b6c')
            ->where('metadata_id','=','cc86e1ff-3f72-4399-ba84-7d256400145d')
            ->get();
    }

    public function organization1(){
        $queryData = DB::table('metadata')->select('metadata.metadata_id','metadata.internal_name','node_types.title','node_types.node_type_id','node_types.organization_id as node_organization_id','metadata.organization_id','node_type_metadata.node_type_id')
            ->join('node_type_metadata','metadata.metadata_id','=','node_type_metadata.metadata_id')
            ->join('node_types','node_types.node_type_id','=','node_type_metadata.node_type_id')
            ->whereIn('node_types.organization_id',array('a83db6c0-1a5e-428e-8384-c8d58d2a83ff','f068a10c-96a7-48dd-9366-b71a199da48e','9518d0fe-e174-4b93-aad8-6295d34287a1')
                      )
            ->where('node_types.is_custom','=',0)
            ->where('used_for',0)
            ->get()
            ->toArray();
        return $queryData;
    }

    public function getOrg2NodeType(){
        $query = DB::table('node_types')->select('organization_id','node_type_id')
                    ->where('organization_id','=','f068a10c-96a7-48dd-9366-b71a199da48e')
                    ->where('used_for',0)
                    ->get()
                    ->toArray();
        return $query;
    }
    // this function is used get metadata lists
    public function getMetaDataList()
    {
        $query = DB::table('metadata')->select('metadata_id','organization_id','internal_name')
            ->where('is_custom','=',0)
            ->get()
            ->toArray();
        return $query;
    }
    // This function is used to get Node type list
    public function getNodeTypeDataList()
    {
        $query = DB::table('node_types')->select('node_type_id','organization_id','title')
            ->where('is_custom','=',0)
            ->where('used_for',0)
            ->get()
            ->toArray();
        return $query;
    }

}