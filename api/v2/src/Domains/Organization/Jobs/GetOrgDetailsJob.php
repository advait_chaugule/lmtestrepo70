<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class GetOrgDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $orgCode;
    public function __construct($input)
    {
        //
        $this->orgCode = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
         $serverUrl       =   url('/');
    if($serverUrl=='https://api.standards.isbe.net/server'){

        $orgarr['organization_id'] = '887fb76a-c93c-4124-9ef6-b2030ef05675'; 
        $orgarr['org_code'] =  'ISBOE';
        $orgarr['default'] =  0;
        return $orgarr;
    }
        
        $orgCode = $this->orgCode;
        $organizationDetail = DB::table('organizations')
    ->select('organization_id','org_code','default')
    ->where('org_code','=',$orgCode)
    ->where('is_deleted','=','0')
    ->where('is_active','=','1')
    ->limit(1)
    ->get();

    if(count($organizationDetail)> 0) 
    { 
        $organizationDetail = json_decode(json_encode($organizationDetail), true);
        $orgarr['organization_id'] = $organizationDetail[0]['organization_id']; 
        $orgarr['org_code'] =  $organizationDetail[0]['org_code'];
        $orgarr['default'] =  $organizationDetail[0]['default'];
    }
    else
    {       
            $organizationDetail = DB::table('organizations')
        ->select('organization_id','org_code','default')
        ->where('default','=','1')
        ->where('is_deleted','=','0')
        ->where('is_active','=','1')
        ->limit(1)
        ->get();
        $organizationDetail = json_decode(json_encode($organizationDetail), true);
        $orgarr['organization_id'] = $organizationDetail[0]['organization_id']; 
        $orgarr['org_code'] =  $organizationDetail[0]['org_code'];
        $orgarr['default'] =  $organizationDetail[0]['default'];
    }
 
     return $orgarr;
    }
}
