<?php
namespace App\Domains\Organization\Jobs;
use Lucid\Foundation\Job;
use App\Domains\Organization\Validators\CheckOrganizationIdValidator;

class ValidateOrganizationByIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input){
        $this->input = $input;
    }

    public function handle(CheckOrganizationIdValidator $validator){
       $validation = $validator->validate($this->input);
        return $validation === true ? $validation : $validation->messages()->getMessages();
    }
}
