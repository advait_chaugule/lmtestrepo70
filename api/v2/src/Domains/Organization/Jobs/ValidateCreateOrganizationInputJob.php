<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Organization\Validators\OrganizationInputValidator;

class ValidateCreateOrganizationInputJob extends Job
{
    private $inputs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrganizationInputValidator $validator)
    {
        $validation = $validator->validate($this->inputs);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
