<?php
namespace App\Domains\Organization\Jobs;
use Lucid\Foundation\Job;
use DB;
class ValidateUsersOrganizationJob extends Job
{
    private $email;
    private $serverDomain;
    private $envDomain;
    private $hostEnvUrl;

    public function __construct($email, $serverDomain)
    {
        $this->email = $email;
        $this->serverDomain = $serverDomain;
        //$this->serverDomain = 'acmtlearningmate.com';
        //$this->serverDomain = 'acmt-learningmate.com';
        $this->envDomain  = env('BASE_URL');
        $envUrl           = parse_url($this->envDomain);
        $this->hostEnvUrl = $envUrl['host'];
    }

    public function handle()
    {

        $orgTable = DB::table('organizations')->select('domain_name')
            ->where('domain_name', $this->serverDomain)
            ->where('is_deleted',0)
            ->get()
            ->first();

        if (isset($orgTable->domain_name)) {
            $getDomainInfo = DB::table('users')
                ->join('users_organization', 'users_organization.user_id', '=', 'users.user_id')
                ->join('organizations', 'users_organization.organization_id', '=', 'organizations.organization_id')
                ->where('users.email', '=', $this->email)
                ->where('organizations.domain_name', $this->serverDomain)
                ->where('users.is_active', 1)
                ->where('users.is_deleted', 0)
                ->where('users_organization.is_deleted', 0)
                ->where('users_organization.is_active', 1)
                ->get()
                ->first();
            if($getDomainInfo) {
                return true;
            }
        } else if ($orgTable == '' ) {
            // common domain
            // user has empty domain_name for organization then allow login
            $checkDomain = DB::table('users')
                ->join('users_organization', 'users_organization.user_id', '=', 'users.user_id')
                ->join('organizations','users_organization.organization_id', '=', 'organizations.organization_id')
                ->where('users.email','=',$this->email)
                ->whereNull('organizations.domain_name')
                ->where('users.is_active',1)
                ->where('users.is_deleted',0)
                ->where('users_organization.is_deleted',0)
                ->where('users_organization.is_active',1)
                ->get()
                ->first();
            if($checkDomain){
                return 1;
            }
        }else{
            return 2;
        }
    }
}