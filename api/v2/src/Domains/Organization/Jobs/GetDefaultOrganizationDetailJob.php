<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

class GetDefaultOrganizationDetailJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrganizationRepositoryInterface $oragnizationRepo)
    {   
        return $oragnizationRepo->getDefaultOrganizationDetail();
    }
}
