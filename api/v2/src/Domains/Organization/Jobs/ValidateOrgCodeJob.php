<?php
namespace App\Domains\Organization\Jobs;
use Lucid\Foundation\Job;
use App\Domains\Organization\Validators\CheckOrgCodeValidator;

class ValidateOrgCodeJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $orgCode;
    public function __construct(array $orgCode){
        $this->orgCode = $orgCode;
    }

    public function handle(CheckOrgCodeValidator $validator){
        $validation = $validator->validate($this->orgCode);
        return $validation === true ? $validation : $validation->messages()->getMessages();
    }
}
