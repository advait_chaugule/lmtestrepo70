<?php
namespace App\Domains\Organization\Jobs;

use App\Data\Models\Organization;
use Lucid\Foundation\Job;

class GetOrgCodeByOrgId extends Job
{
    private $organizationId;
    public function __construct($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $orgCode = Organization::select('org_code')
                   ->where('organization_id',$this->organizationId)
                   ->where('is_deleted',0)
                   ->first();
        return $orgCode;


    }
}