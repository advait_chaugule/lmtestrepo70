<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class GetActiveOrganizationsByUserIdJob extends Job
{
    private $userId;
    private $emailId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $userId='',string $emailId='')
    {
        //Set the private variable
        $this->userId = $userId;
        $this->emailId = $emailId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        if(!empty($this->emailId)){
            $userDetail = DB::table('users')
            ->where('email',$this->emailId)
            ->where('is_deleted','0')
            ->get()
            ->toArray();
            $this->userId = $userDetail[0]->user_id;
        }
        
        $organization = DB::table('users_organization')
            ->select('users_organization.user_id','users_organization.organization_id')
            ->join('organizations','users_organization.organization_id', '=', 'organizations.organization_id')
            ->where('users_organization.user_id',$this->userId)
            ->where('users_organization.is_deleted',0)
            ->where('users_organization.is_active',1)
            ->where('organizations.is_active',1)
            ->get()
            ->first();
        return $organization;
    }
}
