<?php
namespace App\Domains\Organization\Jobs;
use Lucid\Foundation\Job;
use App\Domains\Organization\Validators\CheckUpdateOrgInputValidator;

class ValidateUpdateOrgInputJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input){
        $this->input = $input;
    }

    public function handle(CheckUpdateOrgInputValidator $validator){
        $validator->rules['name'] = $validator->rules['name'].',name,'.$this->input['organization_id'].',organization_id';
        $validation = $validator->validate($this->input);
        return $validation === true ? $validation : $validation->messages()->getMessages();
    }
}
