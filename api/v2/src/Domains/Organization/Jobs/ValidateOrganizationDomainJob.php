<?php
namespace App\Domains\Organization\Jobs;
use Lucid\Foundation\Job;
use DB;
class ValidateOrganizationDomainJob extends Job
{
    private $orgCode;
    private $serverUrl;
    public function __construct($orgCode,$serverUrl){
        $this->orgCode   = $orgCode;
        $this->serverUrl = $serverUrl;
    }

    public function handle()
    {
        $orgDetails = DB::table('organizations')
                      ->where('org_code',$this->orgCode)
                      ->where('domain_name',$this->serverUrl)
                      ->orWhere('domain_name','=','')
                      ->where('is_deleted',0)
                      ->get()
                      ->toArray();
        if($orgDetails) {
            return true;
        }else{
            return false;
        }
    }
}