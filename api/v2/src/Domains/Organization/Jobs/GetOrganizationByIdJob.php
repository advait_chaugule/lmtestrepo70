<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class GetOrganizationByIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $organizationId;
    public function __construct($input)
    {
        $this->organizationId = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $organizationId = $this->organizationId;
        $organizationDetail = DB::table('organizations')
        ->select('name as organization_name','users.username as admin_account','users.first_name as admin_name','org_code as short_code','organizations.is_active as status')
        ->join('users_organization','users_organization.organization_id','=','organizations.organization_id')
        ->join('users','users.user_id','=','users_organization.user_id')
        ->where('organizations.organization_id','=',$organizationId)
        ->where('organizations.is_deleted','=','0')
        ->orderBy('users_organization.created_at')
        ->limit(1)
        ->get();
 
        return !empty($organizationDetail) ? $organizationDetail[0] : [];
    }
}
