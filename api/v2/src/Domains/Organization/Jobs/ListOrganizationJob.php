<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class ListOrganizationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $organizationDetail = DB::table('organizations')
        ->select('organization_id','name as organization_name','org_code as short_code','organizations.created_at','organizations.is_active as status','organizations.created_by','organizations.domain_name')
        ->where('organizations.is_deleted','=','0')
        ->get()
        ->toArray();        
        $createdByList = array_column($organizationDetail,'created_by');

        $getUserList = DB::table('users')
                ->select('user_id','first_name','last_name')
                ->whereIn('user_id', $createdByList)
                ->where('is_deleted','0')
                ->get()
                ->toArray();

        $userList = [];
        foreach($getUserList as $key => $value) {
            $userList[$value->user_id]= $value->first_name." ".$value->last_name;          
        }

        foreach($organizationDetail as $orgKey=>$orgValue){
            $shortcode = $orgValue->short_code;
            $domainName = $orgValue->domain_name;
            unset($organizationDetail[$orgKey]->domain_name);
            if(strpos($domainName,'localhost')!==false || empty($domainName))
                $domainName = env('BASE_URL');
            
            $organizationDetail[$orgKey]->tenant_link = $domainName. '#/org/'.$shortcode;

            $domainNameArray = parse_url($domainName);
            if(!empty($domainNameArray) && isset($domainNameArray['host'])){
                $domainNameArray['host'] = str_replace('api.','',$domainNameArray['host']);
                $domainName = $domainNameArray['scheme'].'://api.'.$domainNameArray['host'].(isset($domainNameArray['port']) ? ':'.$domainNameArray['port'] : '').(isset($domainNameArray['path']) ? $domainNameArray['path'] : '');
            
            }
            $organizationDetail[$orgKey]->case_api_link = $domainName . 'server/api/v1/'.$shortcode.'/ims/case/v1p0';
            
            $organizationDetail[$orgKey]->created_by = isset($userList[$orgValue->created_by]) ? $userList[$orgValue->created_by] : $orgValue->created_by;
        }
        return $organizationDetail;
    }
}
