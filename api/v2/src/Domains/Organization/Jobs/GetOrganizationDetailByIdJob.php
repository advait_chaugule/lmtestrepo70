<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

class GetOrganizationDetailByIdJob extends Job
{
    private $organizationIdentifier;
    private $organizationRepository;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the private variable
        $this->organizationIdentifier   =   $identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrganizationRepositoryInterface $organizationRepository)
    {
        //Set the db handler
        $this->organizationRepository = $organizationRepository;

        $organizationDetail =   $this->organizationRepository->find($this->organizationIdentifier);

        return $organizationDetail;
    }
}
