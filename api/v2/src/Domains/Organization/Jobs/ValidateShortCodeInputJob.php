<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Organization\Validators\CreateOrgCodeValidator;

class ValidateShortCodeInputJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateOrgCodeValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
