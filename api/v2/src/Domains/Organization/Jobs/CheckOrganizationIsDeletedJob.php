<?php
namespace App\Domains\Organization\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class CheckOrganizationIsDeletedJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $organizationId;
    public function __construct($input)
    {
        $this->organizationId = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $organizationId = $this->organizationId;
        $organizationDetail = DB::table('organizations')
        ->select('org_code')
        ->where('organizations.organization_id','=',$organizationId)
        ->where('organizations.is_deleted','=','1')
        ->get();
 
        return $organizationDetail->first() ? true : false;
    }
}
