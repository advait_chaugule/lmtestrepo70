<?php
namespace App\Domains\Organization\Validators;

use App\Foundation\BaseValidator;

class CheckOrgCodeValidator extends BaseValidator
{
    protected $rules = [
        "org_code" => "required|exists:organizations,org_code",
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"   => "false"
    ];
}