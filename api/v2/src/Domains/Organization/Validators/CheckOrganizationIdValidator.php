<?php
namespace App\Domains\Organization\Validators;

use App\Foundation\BaseValidator;

class CheckOrganizationIdValidator extends BaseValidator
{
    protected $rules = [
        "organization_id" => "required|exists:organizations,organization_id"
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"   => ":attribute does not exist."
    ];
}