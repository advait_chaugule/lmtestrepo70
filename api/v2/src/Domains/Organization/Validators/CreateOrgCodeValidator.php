<?php
namespace App\Domains\Organization\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateOrgCodeValidator extends BaseValidator {

    protected $rules = [
        'org_code' => 'required|unique:organizations'
    ];
    protected $messages = [
        'required' => ':attribute is required.'
    ];  
    
}