<?php
namespace App\Domains\Organization\Validators;

use App\Foundation\BaseValidator;

class CheckUpdateOrgInputValidator extends BaseValidator
{
    public $rules = [
        "organization_id" => "required|exists:organizations,organization_id",
        'name' => 'required_without:is_active|unique:organizations',
        'is_active' => 'required_without:name'
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"   => 'Organization Id does not exist.',
        "unique"  => "This Organization name already exist."
    ];
}