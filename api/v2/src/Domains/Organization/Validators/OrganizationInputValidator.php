<?php
namespace App\Domains\Organization\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class OrganizationInputValidator extends BaseValidator {

    protected $rules = [
        'name' => 'required|unique:organizations'
    ];
    protected $messages = [
        'required' => ':attribute is required.'
    ];  
    
}