<?php

namespace App\Domains\Http\Jobs;

use Lucid\Foundation\Job;

use App\Foundation\ResponseHandler;

use Storage;

class RespondWithJsonJob extends Job
{
    private $errorType;
    private $data;
    private $message;
    private $_status;
    private $headers;
    private $is_success;
    private $successType;

    public function __construct(
        $successType = '',
        $data = [],
        $message = 'An error occurred', 
        $_status = 'custom_status', 
        $headers = [],
        $is_success = ''
    )
    {
        $this->successType = $successType;
        $this->data = $data;
        $this->message = $message;
        $this->_status = $_status;
        $this->headers = $headers;
        $this->is_success = $is_success;
    }

    public function handle(ResponseHandler $response)
    {
        switch($this->successType) {
            case "created": 
                 $response = $response->respondCreated($this->data, $this->message, $this->_status, $this->headers);
                 break;
            case "custom_created": 
                 $response = $response->respondCustomCreated($this->data, $this->message, $this->_status, $this->headers, $this->is_success);
                 break;
            case "updated": 
                 $response = $response->respondUpdated($this->data, $this->message, $this->_status, $this->headers);
                 break;
            case "deleted": 
                 $response = $response->respondDeleted($this->message, $this->_status, $this->headers);
                 break;
            case "found": 
                 $response = $response->respondFound($this->data, $this->message, $this->_status, $this->headers);
                 break;
            case "custom_found":
                 $response = $response->respondCustomFound($this->data, $this->message, $this->_status, $this->headers, $this->is_success);
                 break;
            case "custom_not_found":
                 $response = $response->respondCustomNotFound($this->message, $this->_status, $this->headers, $this->is_success);
                 break;
            case "custom_validation_error":
                 $response = $response->respondCustomValidationError($this->data, $this->message, $this->_status, $this->headers, $this->is_success);
                 break;
            default: 
                 $response = $response->respondFound($this->data, $this->message, $this->_status, $this->headers);
                 break;
        }
          
        return $response;
    }
}
