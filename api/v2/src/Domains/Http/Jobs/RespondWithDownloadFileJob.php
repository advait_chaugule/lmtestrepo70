<?php
namespace App\Domains\Http\Jobs;

use Lucid\Foundation\Job;

class RespondWithDownloadFileJob extends Job
{
    private $filePath;
    private $responseFileName;
    private $fileHeader;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $filePath, string $responseFileName, array $fileHeader = [])
    {
        $this->filePath = $filePath;
        $this->responseFileName = $responseFileName;
        $this->fileHeader = count($fileHeader) > 0 ? $fileHeader : ['Content-type' => 'application/json'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this->fileHeader);
        return response()->download($this->filePath, $this->responseFileName, $this->fileHeader);
    }
}
