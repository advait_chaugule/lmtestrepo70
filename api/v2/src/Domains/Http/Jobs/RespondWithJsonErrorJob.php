<?php

namespace App\Domains\Http\Jobs;
use Lucid\Foundation\Job;

use App\Foundation\ResponseHandler;

class RespondWithJsonErrorJob extends Job
{
    private $errorType;
    private $message;
    private $_status;
    private $headers;
    private $is_success;

    public function __construct(
        $errorType = '',
        $message = 'An error occurred', 
        $_status = 'custom_status', 
        $headers = [],
        $is_success = ''
    )
    {    
        $this->errorType = $errorType;
        $this->message = $message;
        $this->_status = $_status;
        $this->headers = $headers;
        $this->is_success = $is_success;
    }

    public function handle(ResponseHandler $response)
    {
        switch($this->errorType) {
            case "not_found": 
                 $response = $response->respondNotFound($this->message, $this->_status, $this->headers);
                 break;
            case "custom_not_found": 
                 $response = $response->respondCustomNotFound($this->message, $this->_status, $this->headers, $this->is_success);
                 break;
            case "internal_error": 
                 $response = $response->respondInternalError($this->message, $this->_status, $this->headers);
                 break;
            case "validation_error": 
                 $response = $response->respondValidationError($this->message, $this->_status, $this->headers);
                 break;
            case "authorization_error": 
                 $response = $response->respondAuthorizationError($this->message, $this->_status, $this->headers);
                 break;
            case "permission_error": 
                 $response = $response->respondPermissionError($this->message, $this->_status, $this->headers);
                 break;
            default: 
                 $response = $response->respondInternalError($this->message, $this->_status, $this->headers);
                 break;
        }
        return $response;
    }

}
