<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\AssetHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class GetItemExemplarAssociationsV2Job extends Job
{
    use CaseFrameworkTrait, ArrayHelper, AssetHelper, DateHelpersTrait;

    private $itemAssociationRepository;
    private $assetRepository;

    private $itemIdentifier;
    private $requestUserOrganizationIdentifier;

    private $savedExemplarAssociations;
    private $savedExemplarAssociationIds;
    private $savedAssets;

    private $parsedExemplarAssociations;
    private $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, array $requestUserDetails,$requestUrl='')
    {
        $this->itemIdentifier = $itemIdentifier;
        $this->requestUserOrganizationIdentifier = $requestUserDetails["organization_id"];
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( ItemAssociationRepositoryInterface $itemAssociationRepository,AssetRepositoryInterface  $assetRepository)
    {
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->assetRepository = $assetRepository;

        $documentId = $this->itemIdentifier;
        $exemplarAssociationTypeEnumValue = $this->getSystemSpecifiedAssociationTypeNumber("exemplar");
        $organizationId = $this->requestUserOrganizationIdentifier;
        $conditionalAttributes = [
            "document_id" => $documentId,
            "association_type" => $exemplarAssociationTypeEnumValue,
            "organization_id" => $organizationId
        ];
        $exemplarAssociations = $this->itemAssociationRepository->findByAttributes($conditionalAttributes);
        $associationIds = [];
        foreach($exemplarAssociations as $examplar)
        {
            array_push($associationIds,$examplar->item_association_id);
        }
        $conditionalKeyValuePairs = [
            "organization_id" => $organizationId,
            "document_id" => $documentId,
            "asset_linked_type" => 3
        ];
        $assetCollection = $this->assetRepository->findByAttributes($conditionalKeyValuePairs);
        $parsedAsset = [];
        foreach($assetCollection as $asset) {
            $assetId = $asset->asset_id;
            $title = !empty($asset->title) ? $asset->title : "";
            $description = !empty($asset->description) ? $asset->description : "";
            $assetFileName = !empty($asset->asset_name) ? $asset->asset_name : "";
            $assetTargetFileName = !empty($asset->asset_target_file_name) ? $asset->asset_target_file_name : "";
            $previewUrl =  $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
            $downloadUrl = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
            $assetContentType = !empty($asset->asset_content_type) ? $asset->asset_content_type : "";
            $assetSizeInBytes = isset($asset->asset_size) ? $asset->asset_size : 0;
            $assetUploadedDateTime = !empty($asset->uploaded_at) ? $asset->uploaded_at : "";
          
            $parsedAsset[$asset->item_linked_id] = [
                "asset_id" => $assetId,
                "title" => $title,
                "description" => $description,
                "preview_url" => $previewUrl,
                "download_url" => $downloadUrl,
                "asset_file_name" => $assetFileName,
                "asset_content_type" => $assetContentType,
                "asset_size_in_bytes" => $assetSizeInBytes,
                "asset_target_file_name" => $assetTargetFileName,
                "asset_uploaded_date_time" => $assetUploadedDateTime 
            ];   
        }

        $parsedExemplarAssociations = [];
        foreach($exemplarAssociations as $association)
        {
            $associationId = $association->item_association_id;
            $hasAsset = $association->has_asset;
            if(!empty($parsedAsset[$associationId]) && $hasAsset == 1)
            {
                $examplarAsset = $parsedAsset[$associationId];
                $assetFileName = $examplarAsset['asset_file_name'];
                $assetTargetFileName = $examplarAsset['asset_target_file_name'];  
                $externalPreviewUrl  = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
                $externalDownloadUrl =$this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
                $assetId = $examplarAsset['asset_id'];
                $assetContentType = $examplarAsset['asset_content_type'];
                $assetSizeInBytes = $examplarAsset['asset_size_in_bytes'];
                $assetUploadedDateTime = $examplarAsset['asset_uploaded_date_time'];
            } 
            else
            {
                $assetFileName = "";
                $assetTargetFileName = "";    
                $externalPreviewUrl  = $association->external_node_url; 
                $externalDownloadUrl = $association->external_node_url;
                $assetId = "";
                $assetContentType = "";
                $assetSizeInBytes = 0;
                $assetUploadedDateTime = !empty($association->created_at)?$this->formatDateTime($association->created_at) : null;
            }
            $title = !empty($association->external_node_title) ? $association->external_node_title : "";
            $description = !empty($association->description) ? $association->description : "";
            $parsedExemplarAssociations[$association->origin_node_id]['exemplar_associations'][] = 
            [
                 "association_id" => $associationId,
                "title" => $title,
                "description" => $description,
                "has_asset" => $hasAsset,
                "external_url" => $externalPreviewUrl,
                "external_download_url" => $externalDownloadUrl,
                "asset_id" => $assetId,
                "asset_file_name" => $assetFileName,
                "asset_content_type" => $assetContentType,
                "asset_size_in_bytes" => $assetSizeInBytes,
                "asset_target_file_name" => $assetTargetFileName,
                "asset_uploaded_date_time" => $assetUploadedDateTime
            ];
        }
        return $parsedExemplarAssociations;
    }

    private function createAndReturnAssetLinkForType(string $assetPackageName, string $type,$domainName): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $baseUrl = env("APP_URL");
        }else{
            $baseUrl = $domainName;
        }
        $fullyResolvedUri = "";
        if(!empty($baseUrl)) {
            $acmtApiVersionPrefix = "api/v1";
            $endPoint = "asset/$type";
            $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
            $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$assetPackageName}";
        }
        return $fullyResolvedUri;
    }
}
