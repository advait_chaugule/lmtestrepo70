<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Domains\CaseAssociation\Validators\CreateStandardValidator as Validator;

use App\Services\Api\Traits\StringHelper;

class CreateStandardValidationJob extends Job
{

    use StringHelper;

    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $dataToValidate)
    {
        $this->prepareDataBeforeValidation($dataToValidate);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }

    private function prepareDataBeforeValidation(array $requestData) {
        $requestData['destination_node_ids'] = $this->delimittedStringToArray($requestData['destination_node_ids']);
        $this->input = $requestData;
    }
}
