<?php
namespace App\Domains\CaseAssociation\Jobs;
use App\Domains\CaseAssociation\Validators\CreateItemStandardValidator as Validator;
use Lucid\Foundation\Job;

class CreateItemStandardValidationJob extends Job
{
    private $requestData;

    public function __construct(array $dataToValidate)
    {
        $this->requestData= $dataToValidate;
    }

    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->requestData);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }

}