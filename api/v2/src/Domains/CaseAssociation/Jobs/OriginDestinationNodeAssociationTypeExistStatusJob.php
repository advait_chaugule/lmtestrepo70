<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class OriginDestinationNodeAssociationTypeExistStatusJob extends Job
{
    private $itemAssociationRepository;

    private $originNodeId;
    private $destinationNodeId;
    private $associationTypeId;
    private $isReverse;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        // set the relevant identifiers
        $this->setOriginNodeId($data['source_item_id']);
        $this->setDestinationNodeId($data['target_item_id']);
        $this->setAssociationTypeId($data['association_type']);
        //$this->setIsReverse($data['is_reverse_association']);
    }

    public function setOriginNodeId(string $nodeId) {
        $this->originNodeId = $nodeId;
    }

    public function getOriginNodeId(): string {
        return $this->originNodeId;
    }

    public function setDestinationNodeId(string $nodeId) {
        $this->destinationNodeId = $nodeId;
    }

    public function getDestinationNodeId(): string {
        return $this->destinationNodeId;
    }

    public function setAssociationTypeId(string $associationTypeId) {
        $this->associationTypeId = $associationTypeId;
    }

    public function getAssociationTypeId(): string {
        return $this->associationTypeId;
    }

    public function setIsReverse(string $isReverse) {
        $this->isReverse = $isReverse;
    }

    public function getIsReverse(): string {
        return $this->isReverse;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        // set the database repository
        $this->itemAssociationRepository = $itemAssociationRepository;

        // return origin and destination node association exists status
        return $this->originDestinationNodeAssociationTypeExistStatus();
    }

    private function originDestinationNodeAssociationTypeExistStatus(): bool {
        $keyValueAttributePairToSearchWith = [
            'source_item_id'            => $this->getOriginNodeId(),
            'target_item_id'       => $this->getDestinationNodeId(),
            'association_type'          => $this->getAssociationTypeId(),
            //'is_reverse_association'    => $this->getIsReverse()     
        ];

        //if($keyValueAttributePairToSearchWith['association_type'] != 6) {
            return $this->itemAssociationRepository->findByAttributes($keyValueAttributePairToSearchWith)->isNotEmpty();
//        } else {
//            return false;
//        }
        
        
    }
}
