<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class CreateItemAssociationsJob extends Job
{
    private $itemAssociationRepository;
    private $dataToSave;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $dataToSave, $projectType, $projectId)
    {
        $this->dataToSave   =   $dataToSave;
        $this->projectType  =   $projectType;
        $this->projectId    =   $projectId;    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo,ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        $this->itemRepo             =   $itemRepo;
        $itemAssociationRepository->saveMultiple($this->dataToSave);
        if($this->projectType == '2' && !empty($this->projectId)) {
            foreach($this->dataToSave as $items) {
                $this->itemRepo->addProjectItem($items['source_item_id'], $this->projectId);
            }
        }
    }
}
