<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;


class CreateExemplarAssociationWithExternalUrlJob extends Job
{

    use CaseFrameworkTrait, UuidHelperTrait, DateHelpersTrait;

    private $itemAssociationRepository;

    private $organizationId;
    private $originNodeId;
    private $documentId;
    private $externalLink;
    private $title;
    private $description;

    private $assocationDataToSave;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->setOrganizationId($requestData);
        $this->setOriginNodeId($requestData);
        $this->setDocumentId($requestData);
        $this->setExternalLink($requestData);
        $this->setTitle($requestData);
        $this->setDescription($requestData);
    }

    public function setOrganizationId(array $data) {
        $this->organizationId = $data["auth_user"]["organization_id"];
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }

    public function setOriginNodeId(array $data) {
        $this->originNodeId = $data["origin_node_id"];
    }

    public function getOriginNodeId(): string {
        return $this->originNodeId;
    }

    public function setDocumentId(array $data) {
        $this->documentId = $data["document_id"];
    }

    public function getDocumentId(): string {
        return $this->documentId;
    }

    public function setExternalLink(array $data) {
        $this->externalLink = $data["external_link"];
    }

    public function getExternalLink(): string {
        return $this->externalLink;
    }

    public function setTitle(array $data) {
        $this->title = !empty($data["title"]) ? $data["title"] : "";
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function setDescription(array $data) {
        $this->description = !empty($data["description"]) ? $data["description"] : "";
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setAssocationDataToSave(array $data) {
        $this->assocationDataToSave = $data;
    }

    public function getAssocationDataToSave(): array {
        return $this->assocationDataToSave;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        // set db repository
        $this->itemAssociationRepository = $itemAssociationRepository;
        // prepare the assocation data to save
        $this->prepareAndSetExemplarAssociationDataToSave();
        // save and return the association
        return $this->saveAndReturnExemplarAssociation();
    }

    private function prepareAndSetExemplarAssociationDataToSave() {
        $itemAssociationId = $this->createUniversalUniqueIdentifier();
        $associationTypeEnumValue = $this->getSystemSpecifiedAssociationTypeNumber('exemplar');
        $originNodeId = $this->getOriginNodeId();
        $documentId = $this->getDocumentId();
        $externalLink = $this->getExternalLink();
        $organizationId = $this->getOrganizationId();
        $title = $this->getTitle();
        $description = $this->getDescription();
        $exemplarAssociationToSave = [
            "item_association_id" => $itemAssociationId,
            "association_type" => $associationTypeEnumValue,
            "document_id" => $documentId,
            "origin_node_id" => $originNodeId,
            "destination_node_id" => "",
            "external_node_url" => $externalLink,
            "organization_id" => $organizationId,
            "source_item_association_id" => $itemAssociationId,
            "external_node_title" => $title,
            "description" => $description,
            "created_at" => $this->createDateTime(),
            "source_item_id" => $originNodeId,
            "target_item_id" => '',
            "source_document_id" => $documentId,
            "target_document_id" => ''
        ];
        $this->setAssocationDataToSave($exemplarAssociationToSave);
    }

    private function saveAndReturnExemplarAssociation() {
        $associationDataToSave = $this->getAssocationDataToSave();
        return $this->itemAssociationRepository->saveData($associationDataToSave);
    }
}
