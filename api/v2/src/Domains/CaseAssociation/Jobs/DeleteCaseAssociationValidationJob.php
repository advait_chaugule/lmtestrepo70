<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Domains\CaseAssociation\Validators\DeleteCaseAssociationValidator as Validator;

class DeleteCaseAssociationValidationJob extends Job
{

    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $dataToValidate)
    {
        $this->input = $dataToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
