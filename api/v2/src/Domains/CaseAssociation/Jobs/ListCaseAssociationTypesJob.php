<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\CaseAssociationTypeRepositoryInterface;

class ListCaseAssociationTypesJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CaseAssociationTypeRepositoryInterface $caseAssociationTypeRepository)
    {
        return $caseAssociationTypeRepository->all()->toArray();
    }
}
