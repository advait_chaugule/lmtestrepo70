<?php
namespace App\Domains\CaseAssociation\Jobs;

use App\Domains\CaseAssociation\Validators\CreateItemAssociationValidationJob as Validator;
use Lucid\Foundation\Job;

class CreateItemAssociationValidationJob extends Job
{
    private $requestData;
    public function __construct(array $requestData)
    {
        $this->requestData = $requestData;
    }

    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->requestData);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}