<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

use Illuminate\Support\Facades\DB;

use App\Data\Models\ItemAssociation;
use App\Data\Models\Asset;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\AssetHelper;

class DeleteExemplarAssociationWithExternalResourceJob extends Job
{
    use AssetHelper;

    private $itemAssociationRepository;
    private $assetRepository;

    private $associationIdentifier;
    private $organizationId;

    private $s3StorageDisk;
    private $s3ExemplarAssetUploadFolder;
    private $s3AssetTargetFileName;

    private $exemplarAssociationToDelete;
    private $assetToDelete;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $associationIdentifier, array $requestUserDetails)
    {
        $assetRelatedConfigurations = config("asset");
        $this->setAssociationIdentifier($associationIdentifier);
        $this->setOrganizationId($requestUserDetails);
        $this->setS3StorageDisk();
        $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations);
    }

    /********************************************Setters and getters for request data*****************************************/

    public function setAssociationIdentifier(string $data) {
        $this->associationIdentifier = $data;
    }

    public function getAssociationIdentifier(): string {
        return $this->associationIdentifier;
    }

    public function setOrganizationId(array $data) {
        $this->organizationId = $data["organization_id"];
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }
    
    /********************************************Setters and getters for file storage related stuff*****************************************/

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    public function setS3ExemplarAssetUploadFolder(array $data) {
        $s3Details = $data["S3"];
        $this->s3ExemplarAssetUploadFolder = $s3Details["MAIN_ARCHIVE_FOLDER"]."/".$s3Details["SUB_FOLDER_EXEMPLAR"];
    }

    public function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    public function setS3AssetTargetFileName(string $data) {
        $this->s3AssetTargetFileName = $data;
    }

    public function getS3AssetTargetFileName(): string {
        return $this->s3AssetTargetFileName;
    }

    /********************************Set Setters and getters for saved exemplar related stuff********************************/

    public function setExemplarAssociationToDelete($data) {
        $this->exemplarAssociationToDelete = $data;
    }

    public function getExemplarAssociationToDelete() {
        return $this->exemplarAssociationToDelete;
    }

    public function setAssetToDelete($data) {
        $this->assetToDelete = $data;
    }

    public function getAssetToDelete() {
        return $this->assetToDelete;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssetRepositoryInterface $assetRepository
    )
    {
        // set db repository
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->assetRepository = $assetRepository;

        /****************************Fetch and set the necessary asset and exemplar informations*****************************/
        $exemplarAssociationToDelete = $this->fetchSetAndReturnExemplarAssociationToDelete();
        $exemplarHasAsset = isset($exemplarAssociationToDelete->has_asset) ? 
                            $exemplarAssociationToDelete->has_asset===1 ? true : false : 
                            false;
        if($exemplarHasAsset) {
            $this->fetchAndSetAssetToDelete();
            $this->extractAndSetAssetTargetFileName();
        }

        /**************************Delete the asset target file from s3*****************************/
        $this->deleteAssetTargetFileFromS3AndSetStatus($exemplarHasAsset);

        /**************************Finally delete the records*****************************/
        DB::transaction(function() use($exemplarHasAsset){
            $this->getExemplarAssociationToDelete()->delete();
            if($exemplarHasAsset) {
                $this->getAssetToDelete()->delete();
            }
        });
    }

    private function fetchSetAndReturnExemplarAssociationToDelete() {
        $organizationId = $this->getOrganizationId();
        $associationIdentifier = $this->getAssociationIdentifier();
        $conditionalAttributes = [
            "organization_id" => $organizationId,
            "item_association_id" => $associationIdentifier
        ];
        $result = $this->itemAssociationRepository->findByAttributes($conditionalAttributes);
        $exemplarAssociations = $result->isNotEmpty() ? $result->first() : [];
        $this->setExemplarAssociationToDelete($exemplarAssociations);
        return $exemplarAssociations;
    }

    private function fetchAndSetAssetToDelete() {
        $organizationId = $this->getOrganizationId();
        $associationIdentifier = $this->getAssociationIdentifier();
        $assetLinkedType = $this->helperToReturnAssetLinkedTypeEnumValue("ITEM_ASSOCIATION");
        $conditionalAttributes = [
            "organization_id" => $organizationId,
            "asset_linked_type" => $assetLinkedType,
            "item_linked_id" => $associationIdentifier
        ];
        $result = $this->assetRepository->findByAttributes($conditionalAttributes);
        $asset = $result->isNotEmpty() ? $result->first() : [];
        $this->setAssetToDelete($asset);
    }

    private function extractAndSetAssetTargetFileName() {
        $assetToDelete = $this->getAssetToDelete();
        $dataToSet = !empty($assetToDelete->asset_target_file_name) ? $assetToDelete->asset_target_file_name : "";
        $this->setS3AssetTargetFileName($dataToSet);
    }

    private function deleteAssetTargetFileFromS3AndSetStatus(bool $exemplarHasAssetStatus) {
        if($exemplarHasAssetStatus) {
            $s3StorageDisk = $this->getS3StorageDisk();
            $s3AssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
            $s3AssetTargetFileName = $this->gets3AssetTargetFileName();
            $s3TargetAssetFilePath = "{$s3AssetUploadFolder}/{$s3AssetTargetFileName}";
            if($s3StorageDisk->exists($s3TargetAssetFilePath)){
                $status = $s3StorageDisk->delete($s3TargetAssetFilePath);
            }
        }
    }
}
