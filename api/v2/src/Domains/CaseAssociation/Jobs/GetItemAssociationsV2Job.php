<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\AssetHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class GetItemAssociationsV2Job extends Job
{
    use CaseFrameworkTrait, ArrayHelper, AssetHelper, DateHelpersTrait;

    private $itemAssociationRepository;
    private $assetRepository;

    private $itemIdentifier;
    private $requestUserOrganizationIdentifier;

    private $savedExemplarAssociations;
    private $savedExemplarAssociationIds;
    private $savedAssets;

    private $parsedExemplarAssociations;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, array $requestUserDetails)
    {
        $this->itemIdentifier = $itemIdentifier;
        $this->requestUserOrganizationIdentifier = $requestUserDetails["organization_id"];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        $this->itemAssociationRepository = $itemAssociationRepository;
        $documentId = $this->itemIdentifier;
        $organizationId = $this->requestUserOrganizationIdentifier;
        $exemplarAssociationTypeEnumValue = $this->getSystemSpecifiedAssociationTypeNumber("exemplar");
        $conditionalAttributes = [
            "document_id" => $documentId,
            "organization_id" => $organizationId
        ];
        $associationsList = $this->itemAssociationRepository->findByAttributes($conditionalAttributes);
        $parsedAssociations = [];
        foreach($associationsList as $association)
        {
            if($association->association_type != $exemplarAssociationTypeEnumValue)
            {
                
            }
        }
    }
}
