<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

class CreateCFAssociationUriJob extends Job
{
    private $identifier;
    private $baseUrl;
    private $caseSpecifiedRoutePath;
    private $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier,$requestUrl='')
    {
        $this->identifier = $identifier;
        $this->domainName = $requestUrl;
        if(strpos( $this->domainName,'localhost')!==false)
        {
            $this->baseUrl = env("APP_URL", "http://34.201.107.214/server");
        }else{
            if(strpos( $this->domainName,'server')!==false)
            {
                $this->baseUrl    = str_replace('server', '',  $this->domainName);
            }else{
                $this->baseUrl =  $this->domainName;
            }

        }
        $this->caseApiRoutePrefix = env("CASE_ENDPOINT_PREFIX", "ims/case/v1p0");
        $this->caseSpecifiedRoutePath = "CFItemAssociations";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->baseUrl."/".$this->caseApiRoutePrefix."/".$this->caseSpecifiedRoutePath."/".$this->identifier;
    }
}
