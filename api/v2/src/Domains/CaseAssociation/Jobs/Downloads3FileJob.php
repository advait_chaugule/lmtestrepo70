<?php
namespace App\Domains\CaseAssociation\Jobs;
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Storage;

class Downloads3FileJob extends Job
{
    private $uuid;
    private $s3bucketName;
    public function __construct($uuid){
        $this->uuid = $uuid;

    }

    public function handle()
    {
        $this->s3bucketName = env('AWS_S3_BUCKET', 'acmthost');
        $s3Bucket = config("event_activity")["Taxonomy_Folder"];
        $fileName = $this->uuid . '.json';
        $destinationS3 = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/$fileName";
        $checkExist = Storage::disk('s3')->exists($destinationS3);
        if ($checkExist == true) {
            $getS3Data = Storage::disk('s3')->get($destinationS3);
            $size = Storage::disk('s3')->size($destinationS3);

            $parsedData = [
                "json_file_content" => $getS3Data,
                "json_name" => $fileName,
                "json_content_type" => "application/json",
                "json_size" => $size,
            ];

            return $parsedData;

            //Storage::disk('s3')->download($destinationS3,$fileName,$headers);
        }else{
            return false;
        }
    }


}