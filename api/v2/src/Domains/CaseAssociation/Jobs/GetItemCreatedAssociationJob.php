<?php
namespace App\Domains\CaseAssociation\Jobs;
use App\Data\Models\Item;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class GetItemCreatedAssociationJob extends Job
{
    use CaseFrameworkTrait,DateHelpersTrait;

    private $itemRepository;
    private $documentRepository;
    private $documentIdentifier;
    private $itemIdentifier;
    private $projectType;
    private $associationIdentifier;
    private $organizationId;
    private $itemDetailsFetchedFromDatabase;

    public function __construct(array $associationSaved)
    {
        foreach ($associationSaved as $associationSavedK => $associationSavedV)
        {
            //Set the private identifier attribute
            $this->setProjectType($associationSavedV['project_type']);
            $this->setItemIdentifier($associationSavedV['source_item_id']);
            $this->setAssociationIdentifier($associationSavedV['item_association_id']);
            $this->setRequestUserOrganizationId($associationSavedV['organization_id']);
        }
    }

    public function handle(ItemRepositoryInterface $itemRepository, DocumentRepositoryInterface $documentRepository)
    {
        //Set the repository handler
        $this->itemRepository       =   $itemRepository;
        $this->documentRepository   =   $documentRepository;

        $this->fetchItemDetailsFromDatabaseAndSetIt();

        $responseData           =   $this->parseSavedAssociationData();
        return $responseData;
    }
    public function setProjectType($projectType) {
        $this->projectType   =   $projectType;
    }

    public function getProjectType() {
        return $this->projectType;
    }

    public function setItemIdentifier($itemIdentifier) {
        $this->itemIdentifier   =   $itemIdentifier;
    }

    public function getItemIdentifier() {
        return $this->itemIdentifier;
    }

    public function setAssociationIdentifier($associationIdentifier) {
        $this->associationIdentifier[]   =   $associationIdentifier;
    }

    public function getAssociationIdentifier() {
        return $this->associationIdentifier;
    }

    public function setItemDetailsFetchedFromDatabase($data) {
        $this->itemDetailsFetchedFromDatabase = $data;
    }

    public function getItemDetailsFetchedFromDatabase() {
        return $this->itemDetailsFetchedFromDatabase;
    }

    public function setRequestUserOrganizationId($organizationIdentifier) {
        $this->organizationId = $organizationIdentifier;
    }

    public function getRequestUserOrganizationId(): string {
        return $this->organizationId;
    }

    private function fetchItemDetailsFromDatabaseAndSetIt() {
        $projectType    =   $this->getProjectType();
        $identifier     =   $this->getItemIdentifier();

        $data =  $this->itemRepository->getItemDetailsWithProjectAndCaseAssociation($identifier);

        $this->setItemDetailsFetchedFromDatabase($data);
    }

    private function parseSavedAssociationData(): array {
        $projectType            =   $this->getProjectType();
        $associationIdentifier  =   $this->getAssociationIdentifier();
        $Item                   =   $this->getItemDetailsFetchedFromDatabase();

        if($Item===null)
        {

            // this case is to create associations at doc level start
            $identifier     =   $this->getItemIdentifier();
            $Doc =  $this->documentRepository->getDocDetailsWithProjectAndCaseAssociation($identifier);

                // ACMT-821 (filter out exemplar associations from the item association list)

                $conditionArray = [
                    [ "organization_id", "=", $this->getRequestUserOrganizationId()],
                    [ "association_type", "!=", $this->getSystemSpecifiedAssociationTypeNumber("exemplar")]
                ];

                $itemAssociations = $Doc->itemAssociations()->whereIn('item_association_id',$associationIdentifier)->where($conditionArray)->get();
                // Commented above and rewrite the same ( to fix ACMT-1000 bug )
                $parsedAssociationData = [];
                $data = $itemAssociations->toArray();
                $type_count = 0;
                foreach($data as $association) {
                    if(array_key_exists('target_item_id',$association))
                        $type_count++;
                }
                $parsedData=[];
                foreach($itemAssociations as $association) {
                    $itemAssociationId = $association->item_association_id;
                    $createdAt = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                    $associationTypeNumber = $association->association_type;
                    $associatedDocument = $association->document;
                    $originNode = $association->sourceDocumentNode;
                    $destinationNode = !empty($association->targetItemId) ? $association->targetItemId : $association->destinationDocumentNode;

                    $externalNodeTitle = !empty($association->external_node_title) ? $association->external_node_title : "";
                    $externalNodeUrl = !empty($association->external_node_url) ? $association->external_node_url : "";
                    $description = !empty($association->description) ? $association->description : "";

                    $destinationNodeId = !empty($association->target_item_id) ?
                        $association->target_item_id :
                        $associatedDocument->document_id;

                    if ($associationTypeNumber === 4) {
                        $humanCodingScheme = $externalNodeTitle;
                        $fullStatement = !empty($destinationNode->full_statement) ? $destinationNode->full_statement : $externalNodeUrl;


                        if ($associatedDocument->document_id === $association->target_item_id) {
                            $destinationDocumentId = $associatedDocument->document_id;
                            $destinationDocumentTitle = !empty($associatedDocument->title) ? $associatedDocument->title : "";
                        } else {
                            $destinationDocumentData = $this->fetchDestinationDocumentDetails($association->target_item_id);
                            $destinationDocumentId = $destinationDocumentData["document_id"];
                            $destinationDocumentTitle = $destinationDocumentData["document_title"];
                        }

                    } else {
                        if (!empty($destinationNode->human_coding_scheme)) {
                            $humanCodingScheme = $destinationNode->human_coding_scheme;
                        } else {
                            $humanCodingScheme = !empty($associatedDocument->title) ? $associatedDocument->title : "";
                        }
                        $fullStatement = !empty($destinationNode->full_statement) ?
                            $destinationNode->full_statement :
                            (!empty($associatedDocument->title) ? $associatedDocument->title : "");

                        if (!empty($destinationNode)) {
                            $destinationDocumentData = $this->fetchDestinationDocumentDetails($association->target_item_id);
                            $destinationDocumentId = $destinationDocumentData["document_id"];
                            $destinationDocumentTitle = $destinationDocumentData["document_title"];
                        } else {
                            $destinationDocumentId = $associatedDocument->document_id;
                            $destinationDocumentTitle = !empty($associatedDocument->title) ? $associatedDocument->title : "";
                        }
                    }

                    $destinationDocumentData = [
                        "document_id" => $destinationDocumentId,
                        "document_title" => $destinationDocumentTitle
                    ];

                    $associationParseData[] = ["item_id" => $destinationNodeId,
                            "human_coding_scheme" => $humanCodingScheme,
                            "full_statement" => $fullStatement,
                            "node_type_id" => $destinationNode->node_type_id,
                            "document" => $destinationDocumentData
                    ];

                    $parsedData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" => $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $this->returnAssociationType($associationTypeNumber),
                            "display_name" => $this->returnAssociationType($associationTypeNumber)
                        ],
                        "document" => $associatedDocument,
                        "origin_node" => $originNode,
                        "destination_node" => $associationParseData,
                        "description" => $description
                    ];
                }
                $parsedAssociationData[] = $parsedData;

                $sorted_array = array();
                foreach ($parsedAssociationData as $key => $row)
                {
                    $sorted_array[$key] = $row['created_at'];
                }
                array_multisort($sorted_array, SORT_DESC, $parsedAssociationData);

                return $parsedAssociationData;
        }
        else
        {
            // ACMT-821 (filter out exemplar associations from the item association list)

            $conditionArray = [
                [ "organization_id", "=", $this->getRequestUserOrganizationId()],
                [ "association_type", "!=", $this->getSystemSpecifiedAssociationTypeNumber("exemplar")]
            ];

            $itemAssociations = $Item->itemAssociations()->whereIn('item_association_id',$associationIdentifier)->where($conditionArray)->get();
            // Commented above and rewrite the same ( to fix ACMT-1000 bug )
            $parsedAssociationData = [];
            $data = $itemAssociations->toArray();
            $type_count = 0;
            foreach($data as $association) {
                if(array_key_exists('target_item_id',$association))
                    $type_count++;
            }
            $parsedData=[];
            foreach($itemAssociations as $association) {
                $itemAssociationId = $association->item_association_id;
                $createdAt = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                $associationTypeNumber = $association->association_type;
                $associatedDocument = $association->document;
                $originNode = $association->sourceItemId;
                $destinationNode = !empty($association->targetItemId) ? $association->targetItemId : $association->destinationDocumentNode;

                $externalNodeTitle = !empty($association->external_node_title) ? $association->external_node_title : "";
                $externalNodeUrl = !empty($association->external_node_url) ? $association->external_node_url : "";
                $description = !empty($association->description) ? $association->description : "";

                $destinationNodeId = !empty($association->target_item_id) ?
                    $association->target_item_id :
                    $associatedDocument->document_id;

                if ($associationTypeNumber === 4) {
                    $humanCodingScheme = $externalNodeTitle;
                    $fullStatement = !empty($destinationNode->full_statement) ? $destinationNode->full_statement : $externalNodeUrl;


                    if ($associatedDocument->document_id === $association->target_item_id) {
                        $destinationDocumentId = $associatedDocument->document_id;
                        $destinationDocumentTitle = !empty($associatedDocument->title) ? $associatedDocument->title : "";
                    } else {
                        $destinationDocumentData = $this->fetchDestinationDocumentDetails($association->target_item_id);
                        $destinationDocumentId = $destinationDocumentData["document_id"];
                        $destinationDocumentTitle = $destinationDocumentData["document_title"];
                    }

                } else {
                    if (!empty($destinationNode->human_coding_scheme)) {
                        $humanCodingScheme = $destinationNode->human_coding_scheme;
                    } else {
                        $humanCodingScheme = !empty($associatedDocument->title) ? $associatedDocument->title : "";
                    }
                    $fullStatement = !empty($destinationNode->full_statement) ?
                        $destinationNode->full_statement :
                        (!empty($associatedDocument->title) ? $associatedDocument->title : "");

                    if (!empty($destinationNode)) {
                        $destinationDocumentData = $this->fetchDestinationDocumentDetails($association->target_item_id);
                        $destinationDocumentId = $destinationDocumentData["document_id"];
                        $destinationDocumentTitle = $destinationDocumentData["document_title"];
                    } else {
                        $destinationDocumentId = $associatedDocument->document_id;
                        $destinationDocumentTitle = !empty($associatedDocument->title) ? $associatedDocument->title : "";
                    }
                }

                $destinationDocumentData = [
                    "document_id" => $destinationDocumentId,
                    "document_title" => $destinationDocumentTitle
                ];

                $associationParseData[] = ["item_id" => $destinationNodeId,
                        "human_coding_scheme" => $humanCodingScheme,
                        "full_statement" => $fullStatement,
                        "node_type_id" => $destinationNode->node_type_id,
                        "document" => $destinationDocumentData
                ];

                $parsedData = [
                    "item_association_id" => $itemAssociationId,
                    "created_at" => $createdAt,
                    "association_type" => [
                        "type_id" => $associationTypeNumber,
                        "type_name" => $this->returnAssociationType($associationTypeNumber),
                        "display_name" => $this->returnAssociationType($associationTypeNumber)
                    ],
                    "document" => $associatedDocument,
                    "origin_node" => $originNode,
                    "destination_node" => $associationParseData,
                    "description" => $description
                ];
            }
            $parsedAssociationData[] = $parsedData;

            $sorted_array = array();
            foreach ($parsedAssociationData as $key => $row)
            {
                $sorted_array[$key] = $row['created_at'];
            }
            array_multisort($sorted_array, SORT_DESC, $parsedAssociationData);

            return $parsedAssociationData;
        }

    }

    private function fetchDestinationDocumentDetails(string $identifier): array {
        $itemAlreadyQueried = $this->helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb($identifier, "item");

        if(!empty($itemAlreadyQueried)) {
            $item = $itemAlreadyQueried;
        }
        else {
            $preparedItemQuery = Item::where("item_id", $identifier)->orWhere("source_item_id", $identifier);

            $itemCollection = $preparedItemQuery->get();
            $item = $itemCollection->isNotEmpty() ? $itemCollection->first() : [];
        }

        $this->helperToSetDocumentAndItemAlreadyQueriedFromDb($item, $identifier, "item");


        if(!empty($item)){
            $documentId = $item->document_id;
            $documentAlreadyQueried = $this->helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb($documentId, "document");
            $document = !empty($documentAlreadyQueried) ? $documentAlreadyQueried : $this->documentRepository->find($documentId);
            $this->helperToSetDocumentAndItemAlreadyQueriedFromDb($document, $documentId, "document");
            $documentTitle = !empty($document->title) ? $document->title : "";
        }
        else {
            $documentId = "";
            $documentTitle = "";
        }
        $dataToReturn = [
            "document_id" => $documentId,
            "document_title" => $documentTitle
        ];
        return $dataToReturn;
    }

    private function helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb(string $searchIdentifier, string $type) {
        if($type==="document") {
            return !empty($this->_tempStorageForSavedDocument[$searchIdentifier]) ?
                $this->_tempStorageForSavedDocument[$searchIdentifier] :
                [];
        }
        else {
            return !empty($this->_tempStorageForSavedItem[$searchIdentifier]) ?
                $this->_tempStorageForSavedItem[$searchIdentifier] :
                [];
        }
    }

    private function helperToSetDocumentAndItemAlreadyQueriedFromDb($dataToSet, string $identifier, string $type) {
        if($type==="document") {
            $this->_tempStorageForSavedDocument[$identifier] = $dataToSet;
        }
        else {
            $this->_tempStorageForSavedItem[$identifier] = $dataToSet;
        }
    }
}