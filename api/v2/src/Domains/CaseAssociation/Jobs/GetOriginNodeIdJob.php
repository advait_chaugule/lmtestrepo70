<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\ItemAssociation;
use App\Data\Models\Asset;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

class GetOriginNodeIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $associationId)
    {
        $this->data = $associationId;
        $this->setNodeIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        $associationId = $this->data;

        $this->itemAssociationRepository =  $itemAssociationRepository;

        return $this->itemAssociationRepository->getOriginNodeId($associationId);
    }

    public function setNodeIdentifier(string $identifier) {
        $this->nodeIdentifier = $identifier; 
    }

    public function getNodeIdentifier() {
        return $this->nodeitemIdentifier;
    }

}
