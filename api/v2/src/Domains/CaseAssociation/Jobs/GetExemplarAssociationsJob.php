<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\AssetHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class GetExemplarAssociationsJob extends Job
{
    use CaseFrameworkTrait, ArrayHelper, AssetHelper, DateHelpersTrait;

    private $itemAssociationRepository;
    private $assetRepository;

    private $itemIdentifier;
    private $requestUserOrganizationIdentifier;

    private $savedExemplarAssociations;
    private $savedExemplarAssociationIds;
    private $savedAssets;

    private $parsedExemplarAssociations;
    private $domainName;

    /**
     * GetExemplarAssociationsJob constructor.
     * @param string $itemIdentifier
     * @param array $requestUserDetails
     * @param $requestUrl
     */
    public function __construct(string $itemIdentifier, array $requestUserDetails,$requestUrl)
    {
        $this->setItemIdentifier($itemIdentifier);
        $this->setRequestUserOrganizationIdentifier($requestUserDetails["organization_id"]);
        $this->domainName = $requestUrl;
    }

    /*************************************Setters and getters for input params**************************************/

    public function setItemIdentifier(string $data) {
        $this->itemIdentifier = $data;
    }

    public function getItemIdentifier(): string {
        return $this->itemIdentifier;
    }

    public function setRequestUserOrganizationIdentifier(string $data) {
        $this->requestUserOrganizationIdentifier = $data;
    }

    public function getRequestUserOrganizationIdentifier(): string {
        return $this->requestUserOrganizationIdentifier;
    }

    /********************************Set Setters and getters for saved exemplar related stuff********************************/

    public function setSavedExemplarAssociations(Collection $data) {
        $this->savedExemplarAssociations = $data;
    }

    public function getSavedExemplarAssociations(): Collection {
        return $this->savedExemplarAssociations;
    }

    public function setSavedExemplarAssociationIds(array $data) {
        $this->savedExemplarAssociationIds = $data;
    }

    public function getSavedExemplarAssociationIds(): array {
        return $this->savedExemplarAssociationIds;
    }

   /********************************Set Setters and getters for saved asset related stuff********************************/

    public function setSavedAssets(Collection $data) {
        $this->savedAssets = $data;
    }

    public function getSavedAssets(): Collection {
        return $this->savedAssets;
    }

    public function setParsedExemplarAssociations(array $data) {
        $this->parsedExemplarAssociations = $data;
    }

    public function getParsedExemplarAssociations(): array {
        return $this->parsedExemplarAssociations;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssetRepositoryInterface  $assetRepository
    )
    {
        // set db repositories
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->assetRepository = $assetRepository;

        // fetch associations of type exemplar
        $this->fetchAndSetExemplarAssociations();
        // extract the item association ids from saved associations
        $this->extractAndSetAssociationIdsFromSavedExemplarAssociations();
        // fetch all asset records for all exemplar associations
        $this->fetchAndSetAssets();
        // parse and set saved exemplar associations with assets 
        $this->parseAndSetExemplarAssociations();

        // return the parsed exemplar associations
        return $this->getParsedExemplarAssociations();
    }

    private function fetchAndSetExemplarAssociations() {
        $originNodeId = $this->getItemIdentifier();
        $exemplarAssociationTypeEnumValue = $this->getSystemSpecifiedAssociationTypeNumber("exemplar");
        $organizationId = $this->getRequestUserOrganizationIdentifier();
        $conditionalAttributes = [
            "origin_node_id" => $originNodeId,
            "association_type" => $exemplarAssociationTypeEnumValue,
            "organization_id" => $organizationId
        ];
        $exemplarAssociations = $this->itemAssociationRepository->findByAttributes($conditionalAttributes);
        
        $this->setSavedExemplarAssociations($exemplarAssociations);
    }

    private function extractAndSetAssociationIdsFromSavedExemplarAssociations() {
        $exemplarAssociationCollection = $this->getSavedExemplarAssociations();
        $savedExemplarAssociationIdsCsv = $exemplarAssociationCollection->implode("item_association_id", ",");
        $savedExemplarAssociationIds = $this->createArrayFromCommaeSeparatedString($savedExemplarAssociationIdsCsv);
        $this->setSavedExemplarAssociationIds($savedExemplarAssociationIds);
    }

    private function fetchAndSetAssets() {
        $attributeIn = "item_linked_id";
        $savedExemplarAssociationIds = $this->getSavedExemplarAssociationIds();
        $organizationId = $this->getRequestUserOrganizationIdentifier();
        $assetLinkedType = $this->helperToReturnAssetLinkedTypeEnumValue("ITEM_ASSOCIATION");
        $conditionalAttributes = [ "organization_id" => $organizationId, "asset_linked_type" => $assetLinkedType ];
        $savedAssets =  $this->assetRepository->findByAttributeContainedIn(
                            $attributeIn,
                            $savedExemplarAssociationIds,
                            $conditionalAttributes
                        );
        $this->setSavedAssets($savedAssets);
    }

    private function parseAndSetExemplarAssociations() {
        $savedExemplarAssociations = $this->getSavedExemplarAssociations();
        $parsedExemplarAssociations = [];
        foreach($savedExemplarAssociations as $association) {
            $associationId = $association->item_association_id;
            $hasAsset = $association->has_asset;
            $associatedAsset = $this->searchForAndReturnAsset($associationId);
            $assetFileName = !empty($associatedAsset['asset_name']) ? $associatedAsset['asset_name'] : "";
            $assetTargetFileName = !empty($associatedAsset['asset_target_file_name']) ? $associatedAsset['asset_target_file_name'] : "";
            $externalPreviewUrl =  (!empty($associatedAsset) && $hasAsset===1) ?
                                    $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName) :
                                    ($association->external_node_url ?: "");
                                    
            $externalDownloadUrl = (!empty($associatedAsset) && $hasAsset===1) ?
                                    $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName) :
                                    ($association->external_node_url ?: "");
            
            $assetId = !empty($associatedAsset['asset_id']) ? $associatedAsset['asset_id'] : "";
            $assetContentType = !empty($associatedAsset['asset_content_type']) ? $associatedAsset['asset_content_type'] : "";
            $assetSizeInBytes = isset($associatedAsset['asset_size']) ? $associatedAsset['asset_size'] : 0;
            $title = !empty($association->external_node_title) ? $association->external_node_title : "";
            $description = !empty($association->description) ? $association->description : "";

            if (!empty($associatedAsset['uploaded_at'])) {
                $assetUploadedDateTime = $associatedAsset['uploaded_at'];
            }else {
                if(!is_null($association->created_at)){
                    $assetUploadedDateTime = $this->formatDateTime($association->created_at);
                }
                else {
                    $assetUploadedDateTime = '';
                }
            }


            $parsedExemplarAssociations[] = [
                "association_id" => $associationId,
                "title" => $title,
                "description" => $description,
                "has_asset" => $hasAsset,
                "external_url" => $externalPreviewUrl,
                "external_download_url" => $externalDownloadUrl,
                "asset_id" => $assetId,
                "asset_file_name" => $assetFileName,
                "asset_content_type" => $assetContentType,
                "asset_size_in_bytes" => $assetSizeInBytes,
                "asset_target_file_name" => $assetTargetFileName,
                "asset_uploaded_date_time" => $assetUploadedDateTime
            ];
        }

        $sorted_array = array();
        foreach ($parsedExemplarAssociations as $key => $row)
        {
            $sorted_array[$key] = $row['asset_uploaded_date_time'];
        }
        array_multisort($sorted_array, SORT_DESC, $parsedExemplarAssociations);
        
        $this->setParsedExemplarAssociations($parsedExemplarAssociations);
    }

    /*******************************************Helper Methods*********************************************/

    private function searchForAndReturnAsset(string $associationIdentifierToSearchFor): array {
        $savedAssets = $this->getSavedAssets();
        $searchResult = $savedAssets->where("item_linked_id", $associationIdentifierToSearchFor);
        return $searchResult->isNotEmpty() ? $searchResult->first()->toArray() : [];
    }

    private function createAndReturnAssetLinkForType(string $assetPackageName, string $type,$domainName): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $baseUrl = env("APP_URL", "http://34.201.107.214/server");
        }else{
            $baseUrl = $domainName;

        }
        $acmtApiVersionPrefix = "api/v1";
        $endPoint = "asset/$type/exemplar";
        $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
        $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$assetPackageName}";
        return $fullyResolvedUri;
    }
}
