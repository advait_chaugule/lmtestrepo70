<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Domains\CaseAssociation\Validators\CreateExemplarAssociationInputValidator as Validator;

class ValidateCreateExemplarAssociationInputsJob extends Job
{

    private $requestDataToValidate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestDataToValidate)
    {
        $this->requestDataToValidate = $requestDataToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->requestDataToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
