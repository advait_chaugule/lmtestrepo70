<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class EditCaseAssociatonJob extends Job
{
    private $dataToUpdate;

    /**
     * Create a new job instance.
     * @param - [association_id, destination_node_id,association_type_id,origin_node_id]
     * @return void
     */
    public function __construct(array $dataToUpdate)
    {
        $this->dataToUpdate = $dataToUpdate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $caseAssociationRepository)
    {
        $associationIdentifier = $this->dataToUpdate['association_id'];
        return $caseAssociationRepository->edit($associationIdentifier, $this->dataToUpdate);
    }
}
