<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class DeleteCaseAssociationJob extends Job
{
    private $caseAssociationIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $caseAssociationIdentifier)
    {
        $this->caseAssociationIdentifier = $caseAssociationIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $caseAssociationRepository)
    {
       $caseAssociationRepository->remove($this->caseAssociationIdentifier);
    }
}
