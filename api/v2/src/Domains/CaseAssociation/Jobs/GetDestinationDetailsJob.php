<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetDestinationDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $cfItem = $itemRepo->find($this->data)->toArray();
        
        $external['external_node_title'] = $cfItem['human_coding_scheme'];
        $external['external_node_url'] = env('BASE_URL','http://acmt-dev.learningmate.com/').'api/v1/cfitem/fetch/'.$cfItem['item_id'];
       
    
        return $external;
    }
}
