<?php
namespace App\Domains\CaseAssociation\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Filesystem\FilesystemAdapter;

use Illuminate\Support\Facades\DB;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Data\Models\ItemAssociation;
use App\Data\Models\Asset;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\AssetHelper;

class CreateExemplarAssociationWithExternalAssetJob extends Job
{

    use CaseFrameworkTrait, UuidHelperTrait, FileHelper, AssetHelper;

    private $itemAssociationRepository;
    private $assetRepository;

    private $assetIdToSave;

    private $localBaseStoragePath;
    private $s3StorageDisk;
    private $localArchiveFolder;
    private $s3ExemplarAssetUploadFolder;
    private $s3ArchiveFileName;

    private $organizationId;
    private $originNodeId;
    private $documentId;
    private $assetFileToUpload;
    private $assetTitleToSave;
    private $assetDescriptionToSave;

    private $originalUploadedAssetFileName;
    private $assetFileExtension;
    private $assetFileMimeType;
    private $assetFileSize;
    private $temporaryAssetFileName;
    private $s3UploadedStatus;

    private $assocationDataToSave;
    private $assetDataToSave;
    private $savedExemplarAssociation;
    private $savedAsset;
    private $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData,$requestUrl='')
    {
        $this->setAssetIdToSave();

        $assetRelatedConfigurations = config("asset");
        $this->setlocalBaseStoragePath();
        $this->setS3StorageDisk();
        $this->setLocalArchiveFolder($assetRelatedConfigurations);
        $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations);
        $this->setS3ArchiveFileName();

        $this->setOrganizationId($requestData);
        $this->setOriginNodeId($requestData);
        $this->setDocumentId($requestData);
        $this->setAssetFileToUpload($requestData);
        $this->setAssetDescriptionToSave($requestData);
        $this->setAssetTitleToSave($requestData);

        $this->setOriginalAssetFileName();
        $this->setAssetFileExtension();
        $this->setAssetFileMimeType();
        $this->setAssetFileSize();
        $this->setTemporaryAssetFileName();
        $this->domainName = $requestUrl;
    }

    private function setAssetIdToSave() {
        $this->assetIdToSave = $this->createUniversalUniqueIdentifier();
    }

    private function getAssetIdToSave(): string {
        return $this->assetIdToSave;
    }

    /********************************************Setters and getters for file storage related stuff*****************************************/

    public function setlocalBaseStoragePath() {
        $this->localBaseStoragePath = storage_path(). "/app";
    }
    
    public function getlocalBaseStoragePath(): string {
        return $this->localBaseStoragePath;
    }

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setLocalArchiveFolder(array $data) {
        $this->localArchiveFolder = $data["LOCAL"]["LOCAL_TEMPORARY_ARCHIVE_PATH"];
    }

    private function getLocalArchiveFolder(): string {
        return $this->localArchiveFolder;
    }

    private function setS3ExemplarAssetUploadFolder(array $data) {
        $s3Details = $data["S3"];
        $this->s3ExemplarAssetUploadFolder = $s3Details["MAIN_ARCHIVE_FOLDER"]."/".$s3Details["SUB_FOLDER_EXEMPLAR"];
    }

    private function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    private function setS3ArchiveFileName() {
        $this->s3ArchiveFileName = $this->getAssetIdToSave().".zip";
    }

    private function getS3ArchiveFileName(): string {
        return $this->s3ArchiveFileName;
    }

    /********************************************Setters and getters for request data*****************************************/

    public function setOrganizationId(array $data) {
        $this->organizationId = $data["auth_user"]["organization_id"];
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }

    public function setOriginNodeId(array $data) {
        $this->originNodeId = $data["origin_node_id"];
    }

    public function getOriginNodeId(): string {
        return $this->originNodeId;
    }

    public function setDocumentId(array $data) {
        $this->documentId = $data["document_id"];
    }

    public function getDocumentId(): string {
        return $this->documentId;
    }

    public function setAssetFileToUpload(array $data) {
        $this->assetFileToUpload = $data["asset"];
    }

    public function getAssetFileToUpload(): UploadedFile {
        return $this->assetFileToUpload;
    }

    public function setAssetTitleToSave(array $data) {
        $this->assetTitleToSave = !empty($data["title"]) ? $data["title"] : "";
    }

    public function getAssetTitleToSave(): string {
        return $this->assetTitleToSave;
    }

    public function setAssetDescriptionToSave(array $data) {
        $this->assetDescriptionToSave = !empty($data["description"]) ? $data["description"] : "";
    }

    public function getAssetDescriptionToSave(): string {
        return $this->assetDescriptionToSave;
    }

    /***************************Setter and getter for uploaded file details*******************************/

    public function setOriginalAssetFileName() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $this->originalUploadedAssetFileName = $assetFileToUpload->getClientOriginalName();
    }

    public function getOriginalAssetFileName(): string {
        return $this->originalUploadedAssetFileName;
    }

    public function setAssetFileExtension() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $clientProvidedExtension = $assetFileToUpload->clientExtension();
        $guessedFileExtensionFromContent = $assetFileToUpload->extension();
        // fallbacks to extract file extension with client provided extension being the priority
        $uploadedFileExtension = !empty($clientProvidedExtension) ? $clientProvidedExtension : $guessedFileExtensionFromContent;
        // if still empty, then final fallback is custom php
        if(empty($uploadedFileExtension)) {
            $uploadedFileExtension = pathinfo($this->getOriginalAssetFileName(), PATHINFO_EXTENSION);
        }
        $this->assetFileExtension = $uploadedFileExtension;
    }

    public function getAssetFileExtension(): string {
        return $this->assetFileExtension;
    }

    public function setAssetFileMimeType() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $this->assetFileMimeType = $assetFileToUpload->getClientMimeType();
    }

    public function getAssetFileMimeType(): string {
        return $this->assetFileMimeType;
    }

    public function setAssetFileSize() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $this->assetFileSize = $assetFileToUpload->getClientSize();
    }

    public function getAssetFileSize(): int {
        return $this->assetFileSize;
    }

    public function setTemporaryAssetFileName() {
        $this->temporaryAssetFileName = $this->getAssetIdToSave() . "." . $this->getAssetFileExtension();
    }

    public function getTemporaryAssetFileName(): string {
        return $this->temporaryAssetFileName;
    }

    public function setTemporaryUploadedAssetLocation(string $data) {
        $this->temporaryUploadedAssetLocation = $data;
    }

    public function getTemporaryUploadedAssetLocation(): string {
        return $this->temporaryUploadedAssetLocation;
    }

    public function setS3UploadedStatus(bool $data) {
        $this->s3UploadedStatus = $data;
    }

    public function getS3UploadedStatus(): bool {
        return $this->s3UploadedStatus;
    }

    /***************************Setter and getter for case association (of type exemplar) related stuff*******************************/

    public function setAssocationDataToSave(array $data) {
        $this->assocationDataToSave = $data;
    }

    public function getAssocationDataToSave(): array {
        return $this->assocationDataToSave;
    }
    
    public function setAssetDataToSave(array $data) {
        $this->assetDataToSave = $data;
    }

    public function getAssetDataToSave(): array {
        return $this->assetDataToSave;
    }

    public function setSavedExemplarAssociation(ItemAssociation $data) {
        $this->savedExemplarAssociation = $data;
    }

    public function getSavedExemplarAssociation(): ItemAssociation {
        return $this->savedExemplarAssociation;
    }

    public function setSavedAsset(Asset $data) {
        $this->savedAsset = $data;
    }

    public function getSavedAsset(): Asset {
        return $this->savedAsset;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssetRepositoryInterface $assetRepository
    )
    {
        // set db repository
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->assetRepository = $assetRepository;

        // first save uploaded file temporarily
        $this->storeUploadedFileTemporarily();

        // upload asset file to s3
        $this->uploadAssetFileToS3();

        // if asset uploaded to s3 success fully, then create and save association
        if($this->getS3UploadedStatus()) {
            $this->prepareAndSetExemplarAssociationDataToSave();
            $this->prepareAndSetAssetDataToSave();
            // save association and asset record inside a transaction block
            $this->saveAndSetExemplarAssociation();
            $this->saveAndSetAsset();
        }

        // remove temporary uploaded asset file
        $this->removeTemporaryUploadedAssetFile();

        return $this->prepareAndReturnJobResponse();
    }

    private function storeUploadedFileTemporarily() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $temporaryFileName = $this->getTemporaryAssetFileName();
        $localArchiveFolder = $this->getLocalArchiveFolder();
        $temporaryUploadedAssetLocation = $assetFileToUpload->storeAs($localArchiveFolder, $temporaryFileName);
        $this->setTemporaryUploadedAssetLocation($temporaryUploadedAssetLocation);
    }

    private function uploadAssetFileToS3() {
        $localBasePath = $this->getlocalBaseStoragePath();
        $s3Disk = $this->getS3StorageDisk();
        $assetTargetName = $this->getTemporaryAssetFileName();
        $s3ExemplarAssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
        $temporaryUploadedAssetFilePath = "{$localBasePath}/{$this->getTemporaryUploadedAssetLocation()}";
        $fileContent = $this->getContent($temporaryUploadedAssetFilePath);
        $s3Destination = "{$s3ExemplarAssetUploadFolder}/{$assetTargetName}";
        $uploadedStatus = $s3Disk->put($s3Destination, $fileContent);
        $this->setS3UploadedStatus($uploadedStatus);
    }

    private function prepareAndSetExemplarAssociationDataToSave() {
        $itemAssociationId = $this->createUniversalUniqueIdentifier();
        $associationTypeEnumValue = $this->getSystemSpecifiedAssociationTypeNumber('exemplar');
        $originNodeId = $this->getOriginNodeId();
        $documentId = $this->getDocumentId();
        $organizationId = $this->getOrganizationId();
        
        $assetTargetName = $this->getTemporaryAssetFileName();
        $externalNodeUrl = $this->createAndReturnAssetLinkForType($assetTargetName, 'preview',$this->domainName);

        $externalNodeTitleToSave = $this->getAssetTitleToSave();
        $descriptionToSave = $this->getAssetDescriptionToSave();

        $exemplarAssociationToSave = [
            "item_association_id" => $itemAssociationId,
            "association_type" => $associationTypeEnumValue,
            "document_id" => $documentId,
            "origin_node_id" => $originNodeId,
            "destination_node_id" => "",
            "has_asset" => 1,
            "organization_id" => $organizationId,
            "source_item_association_id" => $itemAssociationId,
            "external_node_url" => $externalNodeUrl,
            "external_node_title" => $externalNodeTitleToSave,
            "description" => $descriptionToSave,
            "source_item_id" => $originNodeId,
            "target_item_id" => '',
            "source_document_id" => $documentId,
            "target_document_id" => ''
        ];
        $this->setAssocationDataToSave($exemplarAssociationToSave);
    }

    private function prepareAndSetAssetDataToSave() {
        $assetId = $this->getAssetIdToSave();
        $documentId = $this->getDocumentId();
        $organizationId = $this->getOrganizationId();
        $assetName = $this->getOriginalAssetFileName();
        $assetTargetName = $this->getTemporaryAssetFileName();
        $assetContentType = $this->getAssetFileMimeType();
        $assetSize = $this->getAssetFileSize();
        $assetTitleToSave = $this->getAssetTitleToSave();
        $assetDesriptionToSave = $this->getAssetDescriptionToSave();
        $assocationDataToSave = $this->getAssocationDataToSave();
        $itemAssociationId = $assocationDataToSave["item_association_id"];
        $assetLinkedType = $this->helperToReturnAssetLinkedTypeEnumValue("ITEM_ASSOCIATION");
        $dataToSave = [
            'asset_id' => $assetId, 
            "organization_id" => $organizationId,
            "document_id" => $documentId,
            'asset_name' => $assetName, 
            'asset_target_file_name' => $assetTargetName, 
            'asset_content_type' => $assetContentType, 
            'asset_size' => $assetSize,
            'title' => $assetTitleToSave,
            'description' => $assetDesriptionToSave,
            'item_linked_id' => $itemAssociationId,
            'asset_linked_type' => $assetLinkedType,
            'uploaded_at' => now()->toDateTimeString()
        ];
        $this->setAssetDataToSave($dataToSave);
    }

    private function saveAndSetExemplarAssociation() {
        $associationDataToSave = $this->getAssocationDataToSave();
        $savedExemplarItemAssociation = $this->itemAssociationRepository->saveData($associationDataToSave);
        $this->setSavedExemplarAssociation($savedExemplarItemAssociation);
    }

    private function saveAndSetAsset() {
        $assetDataToSave = $this->getAssetDataToSave();
        $savedAsset = $this->assetRepository->saveData($assetDataToSave);
        $this->setSavedAsset($savedAsset);
    }

    private function removeTemporaryUploadedAssetFile() {
        $localBaseStoragePath = $this->getlocalBaseStoragePath();
        $temporaryUploadedAssetLocation = $localBaseStoragePath . "/" . $this->getTemporaryUploadedAssetLocation();
        $this->deleteFile($temporaryUploadedAssetLocation);
    }

    private function prepareAndReturnJobResponse() {
        $savedExemplarAssociation = $this->getSavedExemplarAssociation()->toArray();
        $assetId = $this->getAssetIdToSave();
        // add the asset id to the response
        $savedExemplarAssociation["asset_id"] = $assetId;
        return $savedExemplarAssociation;
    }

    private function createAndReturnAssetLinkForType(string $assetPackageName, string $type,$domainName): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $baseUrl = env("APP_URL","http://34.201.107.214/server");
        }else{
            $baseUrl = $domainName;
        }
        $acmtApiVersionPrefix = "api/v1";
        $endPoint = "asset/$type/exemplar";
        $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
        $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$assetPackageName}";
        return $fullyResolvedUri;
    }
}
