<?php
namespace App\Domains\CaseAssociation\Tests\Jobs;

use App\Domains\CaseAssociation\Jobs\DeleteExemplarAssociationWithExternalResourceJob;
use Tests\TestCase;

class DeleteExemplarAssociationWithExternalResourceJobTest extends TestCase
{
    public function test_AssociationIdentifier()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new DeleteExemplarAssociationWithExternalResourceJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssociationIdentifier($value);
        $this->assertEquals($value,$assets->getAssociationIdentifier());
    }

    public function test_S3AssetTargetFileName()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new DeleteExemplarAssociationWithExternalResourceJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setS3AssetTargetFileName($value);
        $this->assertEquals($value,$assets->getS3AssetTargetFileName());
    }

    public function test_ExemplarAssociationToDelete()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new DeleteExemplarAssociationWithExternalResourceJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setExemplarAssociationToDelete($value);
        $this->assertEquals($value,$assets->getExemplarAssociationToDelete());
    }

    public function test_AssetToDelete()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new DeleteExemplarAssociationWithExternalResourceJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssetToDelete($value);
        $this->assertEquals($value,$assets->getAssetToDelete());
    }
}
