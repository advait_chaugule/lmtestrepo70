<?php
namespace App\Domains\CaseAssociation\Tests\Jobs;

use App\Domains\CaseAssociation\Jobs\GetExemplarAssociationsJob;
use Tests\TestCase;

class GetExemplarAssociationsJobTest extends TestCase
{
    public function test_ItemIdentifier()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new GetExemplarAssociationsJob('f8634526-2674-47df-ac33-714237e3f565',$data,'https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }

    public function test_RequestUserOrganizationIdentifier()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new GetExemplarAssociationsJob('f8634526-2674-47df-ac33-714237e3f565',$data,'https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setRequestUserOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getRequestUserOrganizationIdentifier());
    }
}
