<?php
namespace App\Domains\CaseAssociation\Tests\Jobs;

use App\Domains\CaseAssociation\Jobs\OriginDestinationNodeAssociationTypeExistStatusJob;
use Tests\TestCase;

class OriginDestinationNodeAssociationTypeExistStatusJobTest extends TestCase
{
    public function test_OriginNodeId()
    {
        
        $data = [
            'source_item_id'            =>'c6be0382-dc19-47da-b97d-b00a8cf2d055',
            'target_item_id'       =>'3c990ccf-c5fc-408e-9d40-333740f1459b' , 
            'association_type'          =>'5e5a4b22-67bc-40ad-824e-d83cc166fcea',
            // 'is_reverse_association'    =>'15e11d90-8d2d-4a68-9b8d-f68f14a98d89'     
        ];

        $assets = new OriginDestinationNodeAssociationTypeExistStatusJob($data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOriginNodeId($value);
        $this->assertEquals($value,$assets->getOriginNodeId());
    }

    public function test_DestinationNodeId()
    {
        $data = [
            'source_item_id'            =>'c6be0382-dc19-47da-b97d-b00a8cf2d055',
            'target_item_id'       =>'3c990ccf-c5fc-408e-9d40-333740f1459b' , 
            'association_type'          =>'5e5a4b22-67bc-40ad-824e-d83cc166fcea',
            // 'is_reverse_association'    =>'15e11d90-8d2d-4a68-9b8d-f68f14a98d89'     
        ];
        $assets = new OriginDestinationNodeAssociationTypeExistStatusJob($data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDestinationNodeId($value);
        $this->assertEquals($value,$assets->getDestinationNodeId());
    }

    public function test_AssociationTypeId()
    {
        $data = [
            'source_item_id'            =>'c6be0382-dc19-47da-b97d-b00a8cf2d055',
            'target_item_id'       =>'3c990ccf-c5fc-408e-9d40-333740f1459b' , 
            'association_type'          =>'5e5a4b22-67bc-40ad-824e-d83cc166fcea',
            // 'is_reverse_association'    =>'15e11d90-8d2d-4a68-9b8d-f68f14a98d89'     
        ];
        $assets = new OriginDestinationNodeAssociationTypeExistStatusJob($data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssociationTypeId($value);
        $this->assertEquals($value,$assets->getAssociationTypeId());
    }

    public function test_IsReverse()
    {
        $data = [
            'source_item_id'            =>'c6be0382-dc19-47da-b97d-b00a8cf2d055',
            'target_item_id'       =>'3c990ccf-c5fc-408e-9d40-333740f1459b' , 
            'association_type'          =>'5e5a4b22-67bc-40ad-824e-d83cc166fcea',
            // 'is_reverse_association'    =>'15e11d90-8d2d-4a68-9b8d-f68f14a98d89'     
        ];
        $assets = new OriginDestinationNodeAssociationTypeExistStatusJob($data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIsReverse($value);
        $this->assertEquals($value,$assets->getIsReverse());
    }

}
