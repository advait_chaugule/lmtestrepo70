<?php
namespace App\Domains\CaseAssociation\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class EditCaseAssociationValidator extends BaseValidator {

    protected $rules = [
        'association_id' => 'required|exists:item_associations,item_association_id',
        'association_type' => 'required|integer|between:1,9',
        'origin_node_id' => 'required|exists:items,item_id',
        'destination_node_id' => 'required|exists:items,item_id',
    ];

     protected $messages = [
        'required' => ':attribute is mandatory.',
     ];
    
}