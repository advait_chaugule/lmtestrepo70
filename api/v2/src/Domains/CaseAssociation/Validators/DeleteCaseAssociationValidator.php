<?php
namespace App\Domains\CaseAssociation\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class DeleteCaseAssociationValidator extends BaseValidator {

    protected $rules = [
        'association_id' => 'required|string|exists:item_associations,item_association_id'
    ];

     protected $messages = [
        'string' => ':attribute must be a string.', 
        'required' => ':attribute is mandatory.',
     ];
    
}