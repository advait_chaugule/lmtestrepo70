<?php
namespace App\Domains\CaseAssociation\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateCaseAssociationValidator extends BaseValidator {

    protected $rules = [
        'origin_node_id' => 'required|string|exists:items,item_id',
        'association_type' => 'required'
    ];

     protected $messages = [
        'string' => ':attribute must be a string.', 
        'required' => ':attribute is mandatory.'
     ];
    
}