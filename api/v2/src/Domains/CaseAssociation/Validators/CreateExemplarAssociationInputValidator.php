<?php
namespace App\Domains\CaseAssociation\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateExemplarAssociationInputValidator extends BaseValidator {

    protected $rules = [
        'origin_node_id' => 'required|exists:items,item_id',
        'document_id' => 'required|exists:documents,document_id',
        'has_asset' => 'required|in:0,1',
        'external_link' => 'required_if:has_asset,0',
        'asset' => 'required_if:has_asset,1|file|max:20000',
        'title' => 'required|string|max:100',
        'description' => 'nullable|string|max:1000'
        // we might need to enable file type validation
        // 'asset' => 'required_with:has_asset,1|mimes:jpeg,bmp,png,docx,pptx,dotx,json,jsonml,zip,xls,ppt',
    ];
    
     protected $messages = [
        'origin_node_id.required' => 'Item node id is mandatory.',
        'document_id.required' => 'Document node id is mandatory.',
        'has_asset.required' => ':attribute is required ',
        'asset.max' => "Asset file can't exceeed 20MB.",
        'title.required' => "Title is mandatory.",
        'title.max' => ":attribute can't exceeed 100 characters.",
        'description.max' => ":attribute can't exceeed 1000 characters.",
     ];
    
}