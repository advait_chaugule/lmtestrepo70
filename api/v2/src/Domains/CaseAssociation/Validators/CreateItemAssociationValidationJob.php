<?php
namespace App\Domains\CaseAssociation\Validators;
use App\Foundation\BaseValidator;

class CreateItemAssociationValidationJob extends BaseValidator
{
    protected $rules = [
        'origin_node_id'         => 'required|string|exists:items,item_id',
        'association_type'       => 'required',
        'project_id'             => 'required|string|exists:projects,project_id'
    ];

    protected $messages = [
        'string'   => ':attribute must be a string.',
        'array'    => ':attribute must be a array.',
        'required' => ':attribute is mandatory.'
    ];
}