<?php
namespace App\Domains\CaseAssociation\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateStandardValidator extends BaseValidator {

    protected $rules = [
        'origin_node_id' => 'required|string|exists:items,item_id',
        'destination_node_ids.*' => 'required|string',
        'association_type' => 'required'
    ];

     protected $messages = [
        'string' => ':attribute must be a string.', 
        'required' => ':attribute is mandatory.'
     ];
    
}