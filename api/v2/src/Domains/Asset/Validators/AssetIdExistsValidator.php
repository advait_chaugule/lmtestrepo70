<?php
namespace App\Domains\Asset\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class AssetIdExistsValidator extends BaseValidator {

    protected $rules = [
        'asset_id' => 'required|exists:assets,asset_id',
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}