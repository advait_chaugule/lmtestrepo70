<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\GetFileMetadataByAssetNameJob;
use Tests\TestCase;

class GetFileMetadataByAssetNameJobTest extends TestCase
{
    public function test_AssetTargetName()
    {
        $assets = new GetFileMetadataByAssetNameJob('f8634526-2674-47df-ac33-714237e3f565');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssetTargetName($value);
        $this->assertEquals($value,$assets->getAssetTargetName());
    }

    public function test_SavedAsset()
    {
        $assets = new GetFileMetadataByAssetNameJob('f8634526-2674-47df-ac33-714237e3f565');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setSavedAsset($value);
        $this->assertEquals($value,$assets->getSavedAsset());
    }

    public function test_AssetTargetFileContent()
    {
        $assets = new GetFileMetadataByAssetNameJob('f8634526-2674-47df-ac33-714237e3f565');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssetTargetFileContent($value);
        $this->assertEquals($value,$assets->getAssetTargetFileContent());
    }
}
