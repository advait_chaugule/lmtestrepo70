<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\ValidateAssetCreateInputJob;
use Tests\TestCase;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Illuminate\Http\Request;
use App\Services\Api\Traits\StringHelper;

use Illuminate\Support\Facades\DB;

class ValidateAssetCreateInputJobTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();
        // run seeder if required after environment has been setup
        Artisan::call('db:seed');
    }
            
    public function test_validate_asset_create_input_job()
    {
        $request = new Request();
             
        $data = array(
            array('project_id' => 'd981a1df-ba3b-426e-a6bb-49fcf85b62d6','workflow_id' => 'a428e24e-bdbd-46a2-af60-f37b261235c2','organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff','project_name' => 'sample 1','description' => 'sample 1','document_id' => '9c54aaeb-43d5-1e3a-0561-1db9fa8fd98b','current_workflow_stage_id' => 'a428e24e-bdbd-46a2-af60-f37b261235c2||97abdc88-0b08-4d76-88ca-6fb09fd08544','is_deleted' => '0','updated_by' => '4fa46dbd-e314-40cb-9439-929b94779fd6','created_at' => '2018-11-20 12:38:25','updated_at' => '2018-11-20 12:38:27')
        );
        DB::table('projects')->insert($data);
     // For Testing Link id Mandatory   
        $requestData = ['asset_linked_type' => 4,
        'item_linked_id' => 'd981a1df-ba3b-426e-a6bb-49fcf85b62d6',
        'title' => 'text 124'];
        $requestData["auth_user"]["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff"; 

        $obj = new ValidateAssetCreateInputJob($requestData);

        $request->merge($requestData);
        $validateCreateAssetInput= $obj->handle($request);

       $this->assertContains(':asset is mandatory.', $validateCreateAssetInput);

       // For Testing Valid Result
       $request1 = new Request();

       $requestData1 = ['asset_linked_type' => 4,
        'item_linked_id' => '',
        'title' => 'text 124'];
        $requestData1["auth_user"]["organization_id"] = "a83db6c0-1a5e-428e-8384-c8d58d2a83ff"; 

        $obj1 = new ValidateAssetCreateInputJob($requestData1);

        $request1->merge($requestData1);
        $validateCreateAssetInput1= $obj1->handle($request1);

       $this->assertContains(':item_linked_id is mandatory.', $validateCreateAssetInput1);


    }
}
