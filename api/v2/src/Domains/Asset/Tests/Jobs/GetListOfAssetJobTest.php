<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\GetListOfAssetJob;
use Tests\TestCase;

class GetListOfAssetJobTest extends TestCase
{
    public function test_setItemLinkedId()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new GetListOfAssetJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemLinkedId($value);
        $this->assertEquals($value,$assets->getItemLinkedId());
    }
}
