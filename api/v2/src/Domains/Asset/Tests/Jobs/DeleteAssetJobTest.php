<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\DeleteAssetJob;
use Tests\TestCase;

class DeleteAssetJobTest extends TestCase
{
        public function test_AssetIdentifier()
        {
            $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
            $assets = new DeleteAssetJob('f8634526-2674-47df-ac33-714237e3f565', $data);
            $value = '8944e6c7-c49d-4875-a277-2757c32e1610';
            $assets->setAssetIdentifier($value);
            $this->assertEquals($value,$assets->getAssetIdentifier());
        }     

        public function test_S3FileDeleteStatus()
    {
        $data["organization_id"]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new DeleteAssetJob('f8634526-2674-47df-ac33-714237e3f565', $data);
        $value = 0;
        $assets->setS3FileDeleteStatus($value);
        $this->assertEquals($value,$assets->getS3FileDeleteStatus());
    }
}
