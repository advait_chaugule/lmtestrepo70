<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\CreateAssetJob;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Data\Models\User;

class CreateAssetJobTest extends TestCase
{
    // this migrates DB for this test case execution and rolls back DB when test class object is destroyed
    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();
        // run seeder if required after environment has been setup
        Artisan::call('db:seed');
    }

    public function test_create_asset_job()
    {
        //$this->markTestIncomplete();
        $testModel = new User();
        $userCount = $testModel->all()->count();
        $this->assertGreaterThan(5, $userCount);
    }

    public function test_organizationId()
    {
        $assets = new CreateAssetJob();
        $value = 'ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets->setOrganizationId($value);
        $this->assertEquals($value,$assets->getOrganizationId());
    }  

    public function test_AssetLinkedTypeToSave()
    {
        $assets = new CreateAssetJob();
        $value = 1;
        $assets->setAssetLinkedTypeToSave($value);
        $this->assertEquals($value,$assets->getAssetLinkedTypeToSave());
    }

    public function test_AssetLinkedIdToSave()
    {
        $assets = new CreateAssetJob();
        $value = '0cf19d83-3739-4624-97b1-17016849bc37';
        $assets->setAssetLinkedIdToSave($value);
        $this->assertEquals($value,$assets->getAssetLinkedIdToSave());
    }

    public function test_TitleToSave()
    {
        $assets = new CreateAssetJob();
        $value = '0cf19d83-3739-4624-97b1-17016849bc37';
        $assets->setTitleToSave($value);
        $this->assertEquals($value,$assets->getTitleToSave());
    }

    public function test_DescriptionToSave()
    {
        $assets = new CreateAssetJob();
        $value = '0cf19d83-3739-4624-97b1-17016849bc37';
        $assets->setDescriptionToSave($value);
        $this->assertEquals($value,$assets->getDescriptionToSave());
    }

    public function test_TemporaryUploadedAssetLocation()
    {
        $assets = new CreateAssetJob();
        $value = '0cf19d83-3739-4624-97b1-17016849bc37';
        $assets->setTemporaryUploadedAssetLocation($value);
        $this->assertEquals($value,$assets->getTemporaryUploadedAssetLocation());
    }

    public function test_S3UploadedStatus()
    {
        $assets = new CreateAssetJob();
        $value = true;
        $assets->setS3UploadedStatus($value);
        $this->assertEquals($value,$assets->getS3UploadedStatus());
    }
    
    public function test_AssetOrder()
    {
        $assets = new CreateAssetJob();
        $value = '0cf19d83-3739-4624-97b1-17016849bc37';
        $assets->setAssetOrder($value);
        $this->assertEquals($value,$assets->getAssetOrder());
    }

}
