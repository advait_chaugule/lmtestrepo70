<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\GetAssetListJob;
use Tests\TestCase;

class GetAssetListJobTest extends TestCase
{
    public function test_ItemLinkedIdToSearchWith()
    {
        $assets = new GetAssetListJob('f8634526-2674-47df-ac33-714237e3f565', 'ebd09afb-35f2-429f-83bf-dd4154abbb91','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemLinkedIdToSearchWith($value);
        $this->assertEquals($value,$assets->getItemLinkedIdToSearchWith());
    }

    public function test_RequestUserOrganizationId()
    {
        $assets = new GetAssetListJob('f8634526-2674-47df-ac33-714237e3f565', 'ebd09afb-35f2-429f-83bf-dd4154abbb91','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setRequestUserOrganizationId($value);
        $this->assertEquals($value,$assets->getRequestUserOrganizationId());
    }
}
