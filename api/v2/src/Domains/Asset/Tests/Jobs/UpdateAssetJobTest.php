<?php
namespace App\Domains\Asset\Tests\Jobs;

use App\Domains\Asset\Jobs\UpdateAssetJob;
use Tests\TestCase;

class UpdateAssetJobTest extends TestCase
{
    public function test_AssetIdentifier()
    {
        $data[]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new UpdateAssetJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssetIdentifier($value);
        $this->assertEquals($value,$assets->getAssetIdentifier());
    }

    public function test_InputData()
    {
        $data[]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new UpdateAssetJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setInputData($value);
        $this->assertEquals($value,$assets->getInputData());
    }

    public function test_AssetEntity()
    {
        $data[]='ebd09afb-35f2-429f-83bf-dd4154abbb91';
        $assets = new UpdateAssetJob('f8634526-2674-47df-ac33-714237e3f565',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssetEntity($value);
        $this->assertEquals($value,$assets->getAssetEntity());
    }
}
