<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;

use App\Data\Models\Asset;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\AssetHelper;

class CreateAssetJob extends Job
{
    use UuidHelperTrait, FileHelper, AssetHelper;

    private $assetRepository;
    private $itemRepository;
    private $projectRepository;

    private $assetIdToSave;

    private $organizationId;
    private $assetLinkedTypeToSave;
    private $assetLinkedTypeText;
    private $assetLinkedIdToSave;
    private $titleToSave;
    private $descriptionToSave;
    private $assetFileToUpload;

    private $localBaseStoragePath;
    private $s3StorageDisk;
    private $localArchiveFolder;
    private $s3ExemplarAssetUploadFolder;
    private $s3ArchiveFileName;

    private $originalUploadedAssetFileName;
    private $assetFileExtension;
    private $assetFileMimeType;
    private $assetFileSize;
    private $temporaryAssetFileName;
    private $s3UploadedStatus;

    private $assetDataToSave;
    private $savedAsset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    private function setAssetIdToSave() {
        $this->assetIdToSave = $this->createUniversalUniqueIdentifier();
    }

    private function getAssetIdToSave(): string {
        return $this->assetIdToSave;
    }

    /********************************************Setters and getters for request data*****************************************/

    public function setOrganizationId(string $data) {
        $this->organizationId = $data;
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }

    public function setAssetLinkedTypeToSave(int $data) {
        $this->assetLinkedTypeToSave = $data;
    }

    public function getAssetLinkedTypeToSave(): int {
        return $this->assetLinkedTypeToSave;
    }

    public function setAssetLinkedTypeText() {
        $assetLinkedTypeNumber = $this->getAssetLinkedTypeToSave();
        $this->assetLinkedTypeText = $this->helperToReturnAssetLinkedTypeText($assetLinkedTypeNumber);
    }

    public function getAssetLinkedTypeText(): string {
        return $this->assetLinkedTypeText;
    }

    public function setAssetLinkedIdToSave(string $data) {
        $this->assetLinkedIdToSave = $data;
    }

    public function getAssetLinkedIdToSave(): string {
        return $this->assetLinkedIdToSave;
    }

    public function setTitleToSave(string $data) {
        $this->titleToSave = $data;
    }

    public function getTitleToSave(): string {
        return $this->titleToSave;
    }

    public function setDescriptionToSave(string $data) {
        $this->descriptionToSave = $data;
    }

    public function getDescriptionToSave(): string {
        return $this->descriptionToSave;
    }

    public function setAssetFileToUpload(UploadedFile $data) {
        $this->assetFileToUpload = $data;
    }

    public function getAssetFileToUpload(): UploadedFile {
        return $this->assetFileToUpload;
    }

    /********************************************Setters and getters for file storage related stuff*****************************************/

    public function setlocalBaseStoragePath() {
        $this->localBaseStoragePath = storage_path(). "/app";
    }
    
    public function getlocalBaseStoragePath(): string {
        return $this->localBaseStoragePath;
    }

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setLocalArchiveFolder(array $data) {
        $this->localArchiveFolder = $data["LOCAL"]["LOCAL_TEMPORARY_ARCHIVE_PATH"];
    }

    private function getLocalArchiveFolder(): string {
        return $this->localArchiveFolder;
    }

    private function setS3ExemplarAssetUploadFolder(array $data) {
        $s3Details = $data["S3"];
        $s3SubFolderKey = "SUB_FOLDER_".$this->getAssetLinkedTypeText();
        $this->s3ExemplarAssetUploadFolder = "{$s3Details["MAIN_ARCHIVE_FOLDER"]}/{$s3Details[$s3SubFolderKey]}";
    }

    private function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    /***************************Setter and getter for uploaded file details*******************************/

    public function setOriginalAssetFileName() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $this->originalUploadedAssetFileName = $assetFileToUpload->getClientOriginalName();
    }

    public function getOriginalAssetFileName(): string {
        return $this->originalUploadedAssetFileName;
    }

    public function setAssetFileExtension() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $clientProvidedExtension = $assetFileToUpload->clientExtension();
        $guessedFileExtensionFromContent = $assetFileToUpload->extension();
        // fallbacks to extract file extension with client provided extension being the priority
        $uploadedFileExtension = !empty($clientProvidedExtension) ? $clientProvidedExtension : $guessedFileExtensionFromContent;
        // if still empty, then final fallback is custom php
        if(empty($uploadedFileExtension)) {
            $uploadedFileExtension = pathinfo($this->getOriginalAssetFileName(), PATHINFO_EXTENSION);
        }
        $this->assetFileExtension = $uploadedFileExtension;
    }

    public function getAssetFileExtension(): string {
        return $this->assetFileExtension;
    }

    public function setAssetFileMimeType() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $this->assetFileMimeType = $assetFileToUpload->getClientMimeType();
    }

    public function getAssetFileMimeType(): string {
        return $this->assetFileMimeType;
    }

    public function setAssetFileSize() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $this->assetFileSize = $assetFileToUpload->getClientSize();
    }

    public function getAssetFileSize(): int {
        return $this->assetFileSize;
    }

    public function setTemporaryAssetFileName() {
        $this->temporaryAssetFileName = $this->getAssetIdToSave() . "." . $this->getAssetFileExtension();
    }

    public function getTemporaryAssetFileName(): string {
        return $this->temporaryAssetFileName;
    }

    public function setTemporaryUploadedAssetLocation(string $data) {
        $this->temporaryUploadedAssetLocation = $data;
    }

    public function getTemporaryUploadedAssetLocation(): string {
        return $this->temporaryUploadedAssetLocation;
    }

    public function setS3UploadedStatus(bool $data) {
        $this->s3UploadedStatus = $data;
    }

    public function getS3UploadedStatus(): bool {
        return $this->s3UploadedStatus;
    }

    /******************************************Setters and getters for asset data*****************************************/

    public function setAssetDataToSave(array $data) {
        $this->assetDataToSave = $data;
    }

    public function getAssetDataToSave(): array {
        return $this->assetDataToSave;
    }

    public function setSavedAsset(Asset $data) {
        $this->savedAsset = $data;
    }

    public function getSavedAsset(): Asset {
        return $this->savedAsset;
    }

     public function setAssetOrder($data) {
    $this->savedAsseOrder = $data;
    }

    public function getAssetOrder(){
    return $this->savedAsseOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        Request $request,
        AssetRepositoryInterface $assetRepository,
        ItemRepositoryInterface $itemRepository,
        ProjectRepositoryInterface $projectRepository,
        OrganizationRepositoryInterface $organizationRepository
    )
    {
        // set the db repository
        $this->assetRepository = $assetRepository;
        $this->itemRepository = $itemRepository;
        $this->projectRepository = $projectRepository;
        $this->organizationRepository = $organizationRepository;
        $this->setAssetIdToSave();

        $this->setOrganizationId($request->input("auth_user")["organization_id"]);
        $this->setAssetLinkedTypeToSave($request->input("asset_linked_type"));
        $this->setAssetLinkedTypeText();
        $this->setAssetLinkedIdToSave($request->input("item_linked_id"));
        $this->setTitleToSave($request->input("title"));
        $this->setDescriptionToSave($request->input("description") ?: "");
        $this->setAssetOrder($request->input("asset_order")?: 0);
        $this->setAssetFileToUpload($request->file("asset"));

        $assetRelatedConfigurations = config("asset");
        $this->setlocalBaseStoragePath();
        $this->setS3StorageDisk();
        $this->setLocalArchiveFolder($assetRelatedConfigurations);
        $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations);

        $this->setOriginalAssetFileName();
        $this->setAssetFileExtension();
        $this->setAssetFileMimeType();
        $this->setAssetFileSize();
        $this->setTemporaryAssetFileName();

        // first save uploaded file temporarily
        $this->storeUploadedFileTemporarily();

        // upload asset file to s3
        $this->uploadAssetFileToS3();

        // if asset uploaded to s3 success fully, then create and save association
        if($this->getS3UploadedStatus()) {
            $this->prepareAndSetAssetDataToSave();
            // save association and asset record inside a transaction block
            $this->saveAndSetAsset();
        }

        // remove temporary uploaded asset file
        $this->removeTemporaryUploadedAssetFile();

        // return the saved asset
        return $this->getSavedAsset();
    }

    private function storeUploadedFileTemporarily() {
        $assetFileToUpload = $this->getAssetFileToUpload();
        $temporaryFileName = $this->getTemporaryAssetFileName();
        $localArchiveFolder = $this->getLocalArchiveFolder();
        $temporaryUploadedAssetLocation = $assetFileToUpload->storeAs($localArchiveFolder, $temporaryFileName);
        $this->setTemporaryUploadedAssetLocation($temporaryUploadedAssetLocation);
    }

    private function uploadAssetFileToS3() {
        $localBasePath = $this->getlocalBaseStoragePath();
        $s3Disk = $this->getS3StorageDisk();
        $assetTargetName = $this->getTemporaryAssetFileName();
        $s3ExemplarAssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
        $temporaryUploadedAssetFilePath = "{$localBasePath}/{$this->getTemporaryUploadedAssetLocation()}";
        $fileContent = $this->getContent($temporaryUploadedAssetFilePath);
        $s3Destination = "{$s3ExemplarAssetUploadFolder}/{$assetTargetName}";
        $uploadedStatus = $s3Disk->put($s3Destination, $fileContent);
        $this->setS3UploadedStatus($uploadedStatus);
    }

    private function prepareAndSetAssetDataToSave() {
        $assetId = $this->getAssetIdToSave();
        $organizationId = $this->getOrganizationId();
        $assetName = $this->getOriginalAssetFileName();
        $assetTargetName = $this->getTemporaryAssetFileName();
        $assetContentType = $this->getAssetFileMimeType();
        $assetSize = $this->getAssetFileSize();
        $assetTitleToSave = $this->getTitleToSave();
        $assetDesriptionToSave = $this->getDescriptionToSave();
        $itemLinkedId = $this->getAssetLinkedIdToSave();
        $assetLinkedType = $this->getAssetLinkedTypeToSave();
        $documentId = $this->fetchDocumentId();
        $assetAssetOrder = $this->getAssetOrder();
        
        $dataToSave = [
            'asset_id' => $assetId, 
            "organization_id" => $organizationId,
            "document_id" => $documentId,
            'asset_name' => $assetName, 
            'asset_target_file_name' => $assetTargetName, 
            'asset_content_type' => $assetContentType, 
            'asset_size' => $assetSize,
            'title' => $assetTitleToSave,
            'description' => $assetDesriptionToSave,
            'item_linked_id' => $itemLinkedId,
            'asset_linked_type' => $assetLinkedType,
            'asset_order' =>$assetAssetOrder,
            'uploaded_at' => now()->toDateTimeString(),
            'status'=> 0
        ];
        $this->setAssetDataToSave($dataToSave);
    }

    private function fetchDocumentId(): string {
        $itemLinkedId = $this->getAssetLinkedIdToSave();
        $assetLinkedTypeText = $this->getAssetLinkedTypeText();
        switch ($assetLinkedTypeText) {
            case 'DOCUMENT':
                        $documentIdToReturn = $itemLinkedId;
                        break;
            case 'ITEM':
                        $savedItem = $this->itemRepository->find($itemLinkedId);
                        $documentIdToReturn = $savedItem->document_id;
                        break;
            case 'PROJECT':
                        $savedItem = $this->projectRepository->find($itemLinkedId);
                        $documentIdToReturn = $savedItem->project_id;
                        break;

            default:
                        $documentIdToReturn = "";
                        break;
        }
        return $documentIdToReturn;
    }

    private function saveAndSetAsset() {
        $assetDataToSave = $this->getAssetDataToSave();
        $savedAsset = $this->assetRepository->saveData($assetDataToSave);
        $this->setSavedAsset($savedAsset);
    }

    private function removeTemporaryUploadedAssetFile() {
        $localBaseStoragePath = $this->getlocalBaseStoragePath();
        $temporaryUploadedAssetLocation = $localBaseStoragePath . "/" . $this->getTemporaryUploadedAssetLocation();
        $this->deleteFile($temporaryUploadedAssetLocation);
    }
}
