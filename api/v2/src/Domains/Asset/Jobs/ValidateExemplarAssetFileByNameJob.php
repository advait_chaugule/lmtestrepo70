<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

class ValidateExemplarAssetFileByNameJob extends Job
{

    private $s3StorageDisk;
    private $s3ExemplarAssetUploadFolder;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $assetRelatedConfigurations = config("asset");
        $this->setS3StorageDisk();
        $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations);
    }

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setS3ExemplarAssetUploadFolder(array $data) {
        $s3Details = $data["S3"];
        $this->s3ExemplarAssetUploadFolder = $s3Details["MAIN_ARCHIVE_FOLDER"]."/".$s3Details["SUB_FOLDER_EXEMPLAR"];
    }

    private function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $validationStatus = false;
        $assetFileName = $request->route('asset_target_file_name');
        if(!empty($assetFileName)) {
            $s3StorageDisk = $this->getS3StorageDisk();
            $s3AssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
            $s3FullPackageFilePath = "{$s3AssetUploadFolder}/{$assetFileName}";
            $validationStatus =  $s3StorageDisk->exists($s3FullPackageFilePath);
        }
        return $validationStatus;
    }
        

    
}
