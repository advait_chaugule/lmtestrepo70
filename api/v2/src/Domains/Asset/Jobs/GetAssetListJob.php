<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;

class GetAssetListJob extends Job
{

    protected $assetRepository;

    private $itemLinkedIdToSearchWith;
    private $requestUserOrganizationId;

    private $savedAssets;
    private $parsedAssets;
    private $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $requestIdentifier, string $requestUserOrganizationId,$requestUrl)
    {
        $this->setItemLinkedIdToSearchWith($requestIdentifier);
        $this->setRequestUserOrganizationId($requestUserOrganizationId);
        $this->domainName  = $requestUrl;
    }

    public function setItemLinkedIdToSearchWith(string $data) {
        $this->itemLinkedIdToSearchWith = $data;
    }

    public function getItemLinkedIdToSearchWith(): string {
        return $this->itemLinkedIdToSearchWith;
    }

    public function setRequestUserOrganizationId(string $data) {
        $this->requestUserOrganizationId = $data;
    }

    public function getRequestUserOrganizationId(): string {
        return $this->requestUserOrganizationId;
    }

    public function setSavedAssets(Collection $data) {
        $this->savedAssets = $data;
    }

    public function getSavedAssets(): Collection {
        return $this->savedAssets;
    }

    public function setParsedAssets(array $data) {
        $this->parsedAssets = $data;
    }

    public function getParsedAssets(): array {
        return $this->parsedAssets;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        // set the db repositories
        $this->assetRepository = $assetRepository;

        $this->fetchAndSetSavedAssets();
        $this->parseAndSetAssets();

        return $this->getParsedAssets();
    }

    private function fetchAndSetSavedAssets() {
        $requestUserOrganizationId = $this->getRequestUserOrganizationId();
        $itemLinkedIdToSearchWith = $this->getItemLinkedIdToSearchWith();
        $conditionalKeyValuePairs = [
            "organization_id" => $requestUserOrganizationId,
            "item_linked_id" => $itemLinkedIdToSearchWith
        ];
        $assetCollection = $this->assetRepository->findByAttributes($conditionalKeyValuePairs);
        $this->setSavedAssets($assetCollection);
    }

    private function parseAndSetAssets() {
        $savedAssets = $this->getSavedAssets();
        $parsedAsset = [];
        foreach($savedAssets as $asset) {
            $assetId = $asset->asset_id;
            $title = !empty($asset->title) ? $asset->title : "";
            $description = !empty($asset->description) ? $asset->description : "";
            $assetFileName = !empty($asset->asset_name) ? $asset->asset_name : "";
            $assetTargetFileName = !empty($asset->asset_target_file_name) ? $asset->asset_target_file_name : "";
            $previewUrl =  $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
            $downloadUrl = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
            $assetContentType = !empty($asset->asset_content_type) ? $asset->asset_content_type : "";
            $assetSizeInBytes = isset($asset->asset_size) ? $asset->asset_size : 0;
            $assetUploadedDateTime = !empty($asset->uploaded_at) ? $asset->uploaded_at : "";
            $parsedAsset[] = [
                "asset_id" => $assetId,
                "title" => $title,
                "description" => $description,
                "preview_url" => $previewUrl,
                "download_url" => $downloadUrl,
                "asset_file_name" => $assetFileName,
                "asset_content_type" => $assetContentType,
                "asset_size_in_bytes" => $assetSizeInBytes,
                "asset_target_file_name" => $assetTargetFileName,
                "asset_uploaded_date_time" => $assetUploadedDateTime
            ];
        }
        $this->setParsedAssets($parsedAsset);
    }

    /*******************************************Helper Methods*********************************************/

    private function createAndReturnAssetLinkForType(string $assetPackageName, string $type,$domainName=''): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $baseUrl = env("APP_URL");
        }else{
            $baseUrl = $domainName;

        }
        $fullyResolvedUri = "";
        if(!empty($baseUrl)) {
            $acmtApiVersionPrefix = "api/v1";
            $endPoint = "asset/$type";
            $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
            $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$assetPackageName}";
        }
        return $fullyResolvedUri;
    }
}
