<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use App\Data\Models\Asset;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

class GetAssetDetailJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $assetId)
    {
        $this->data = $assetId;
        $this->setAssetIdIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        $assetId = $this->data;

        $this->assetRepository =  $assetRepository;

        return $this->assetRepository->getAssetId($assetId);
    }

    public function setAssetIdIdentifier(string $identifier) {
        $this->nodeIdentifier = $identifier; 
    }

    public function getAssetIdIdentifier() {
        return $this->nodeitemIdentifier;
    }


}
