<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\FileHelper;
use App\Services\Api\Traits\AssetHelper;

class GetFileMetadataByAssetNameJob extends Job
{

    use FileHelper, AssetHelper;

    private $assetRepository;

    private $s3bucketName;
    private $s3ExemplarAssetUploadFolder;
    private $s3StorageDisk;

    private $assetTargetFileName;
    private $temporaryDownloadedFilePath;
    private $savedAsset;
    private $assetTargetFileContent;
    private $parsedFileMetadata;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $assetTargetFileName)
    {
        $this->setAssetTargetName($assetTargetFileName);
    }

    private function setS3BucketName() {
        $this->s3bucketName = env('AWS_S3_BUCKET', 'acmthost');
    }

    private function getS3BucketName() : string {
        return $this->s3bucketName;
    }

    public function overrideCurrentFileSystemConfigurationWithS3Bucket() {
        config(['filesystems.disks.s3.bucket' => $this->getS3BucketName()]);
    }

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setS3ExemplarAssetUploadFolder(array $data) {
        $s3Details = $data["S3"];
        $subFolderKey = $this->helperToReturnSubFolderNameKeyBasedOnType();
        $subFolderName = $s3Details[$subFolderKey];
        $this->s3ExemplarAssetUploadFolder = "{$s3Details['MAIN_ARCHIVE_FOLDER']}/{$subFolderName}";
    }

    private function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    public function setAssetTargetName(string $data) {
        $this->assetTargetFileName = $data;
    }

    public function getAssetTargetName(): string {
        return $this->assetTargetFileName;
    }

    public function setSavedAsset($data) {
        $this->savedAsset = $data;
    }

    public function getSavedAsset() {
        return $this->savedAsset;
    }

    public function setAssetTargetFileContent($data) {
        $this->assetTargetFileContent = $data;
    }

    public function getAssetTargetFileContent() {
        return $this->assetTargetFileContent;
    }

    public function setParsedFileMetadata(array $data) {
        $this->parsedFileMetadata = $data;
    }

    public function getParsedFileMetadata(): array {
        return $this->parsedFileMetadata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        //set database repository
        $this->assetRepository = $assetRepository;

        // fetch asset by target name
        $this->fetchAndSetSavedAsset();

        $assetRelatedConfigurations = config("asset");
        $this->setS3BucketName();
        $this->overrideCurrentFileSystemConfigurationWithS3Bucket();
        $this->setS3StorageDisk();
        $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations);

        // extract the file contents of asset target located at s3
        $this->fetchFromS3AndSetAssetFileContents();
        // parse saved asset and set asset file metadata values
        $this->parseAndSetAssetFileMetadata();
        // finally return the asset file metadata
        return $this->getParsedFileMetadata();
    }

    private function fetchAndSetSavedAsset() {
        $assetTargetFileName = $this->getAssetTargetName();
        $fieldToSearchFor = "asset_target_file_name";
        $searchResult = $this->assetRepository->findBy($fieldToSearchFor, $assetTargetFileName);
        $this->setSavedAsset($searchResult);
    }

    private function fetchFromS3AndSetAssetFileContents() {
        $assetTargetFileName = $this->getAssetTargetName();
        $s3StorageDisk = $this->getS3StorageDisk();
        $s3AssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
        $s3FullPackageFilePath = "{$s3AssetUploadFolder}/{$assetTargetFileName}";
        $fileContent= $s3StorageDisk->get($s3FullPackageFilePath);
        $this->setAssetTargetFileContent($fileContent);
    }

    private function parseAndSetAssetFileMetadata() {
        $setSavedAsset = $this->getSavedAsset();
        $assetName = !empty($setSavedAsset->asset_name) ? $setSavedAsset->asset_name : "";
        $assetContentType = !empty($setSavedAsset->asset_content_type) ? $setSavedAsset->asset_content_type : "";
        $assetSize = !empty($setSavedAsset->asset_size) ? $setSavedAsset->asset_size : "";
        $assetFileContent = $this->getAssetTargetFileContent();
        $parsedData = [
            "asset_file_content" => $assetFileContent,
            "asset_name" => $assetName,
            "asset_content_type" => $assetContentType,
            "asset_size" => $assetSize,
        ];
        $this->setParsedFileMetadata($parsedData);
    }

    private function helperToReturnSubFolderNameKeyBasedOnType(): string {
        $savedAsset = $this->getSavedAsset();
        $assetLinkedTypeText = $this->helperToReturnAssetLinkedTypeText($savedAsset->asset_linked_type);
        $subFolderKey = "SUB_FOLDER_EXEMPLAR";
        if(!empty($assetLinkedTypeText) && $assetLinkedTypeText!=="ITEM_ASSOCIATION") {
            $subFolderKey = "SUB_FOLDER_$assetLinkedTypeText";
        }
        return $subFolderKey;
    }
}
