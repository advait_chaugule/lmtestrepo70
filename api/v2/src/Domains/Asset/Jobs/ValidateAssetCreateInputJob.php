<?php
namespace App\Domains\Asset\Jobs;

use App\Data\Models\Organization;
use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Models\Document;
use App\Data\Models\Item;
use App\Data\Models\Project;

// use Illuminate\Http\UploadedFile;

class ValidateAssetCreateInputJob extends Job
{
    protected $requestObject;
    protected $organizationId;
    protected $dataToValidate;
    protected $errorMessagesAvailable;
    protected $errorMessagesToSet;

    protected $fileLimitInBytes = 20000000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->setRequestUserOrganizationId($requestData);
        $this->dataToValidate = $requestData;
        $this->errorMessagesAvailable = [
            "required" => [
                "asset_linked_type" => ":asset_linked_type is mandatory.",
                "item_linked_id" => ":item_linked_id is mandatory.",
                "title" => ":title is mandatory.",
                "asset" => ":asset is mandatory."
            ],
            "in" => [
                "asset_linked_type" => ":asset_linked_type is invalid."
            ],
            "exists" => [
                "item_linked_id" => ":item_linked_id is invalid."
            ],
            "file_valid" => [
                "asset" => ":asset uploaded is invalid."
            ],
            "file_size_limit" => [
                "asset" => ":asset file size can't exceed 20MB."
            ]
        ];
        $this->errorMessagesToSet = [];
    }

    public function setRequestUserOrganizationId(array $requestData) {
        $this->organizationId = $requestData["auth_user"]["organization_id"];
    }

    public function getRequestUserOrganizationId(): string {
        return $this->organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        // set the injected laravel request object
        $this->requestObject = $request;

        $assetLinkedTypeRequiredStatus = $this->getRequiredStatusForAttribute("asset_linked_type");
        $this->setErrorMessageOfValidationTypeForAttribute($assetLinkedTypeRequiredStatus, "required", "asset_linked_type");
        $assetLinkedTypeInStatus = $assetLinkedTypeRequiredStatus ? 
                                    $this->getInStatusForAssetLinkedTypeAndSetErrorMessageAccordingly() : 
                                    false;

        $itemLinkedIdRequiredStatus = $this->getRequiredStatusForAttribute("item_linked_id");
        $this->setErrorMessageOfValidationTypeForAttribute($itemLinkedIdRequiredStatus, "required", "item_linked_id");

        $titleRequiredStatus = $this->getRequiredStatusForAttribute("title");
        $this->setErrorMessageOfValidationTypeForAttribute($titleRequiredStatus, "required", "title");

        $assetRequiredStatus = $this->getRequiredStatusForAttribute("asset");
        $this->setErrorMessageOfValidationTypeForAttribute($assetRequiredStatus, "required", "asset");

        $itemLinkedIdExistsStatus = $assetLinkedTypeRequiredStatus && $itemLinkedIdRequiredStatus && $assetLinkedTypeInStatus ? 
                                    $this->getItemLinkedIdExistsStatusAndSetErrorMessageAccordingly() : 
                                    false;
        
        $assetFileStatus = $assetRequiredStatus ? 
                           $this->getAssetFileStatusAndSetErrorMessageAccordingly() : 
                           false;
        
        $overallStatus = $assetLinkedTypeRequiredStatus && $itemLinkedIdRequiredStatus && $titleRequiredStatus && 
                         $assetRequiredStatus && $itemLinkedIdExistsStatus && $assetFileStatus;
        
        $jobResponse = $overallStatus===true ? $overallStatus : implode(",", $this->errorMessagesToSet);

        return $jobResponse;
    }

    private function getRequiredStatusForAttribute(string $attribute): bool {
        return !empty($this->dataToValidate[$attribute]);
    }

    private function getInStatusForAssetLinkedTypeAndSetErrorMessageAccordingly(): bool {
        $allowedInValues = [1, 2, 4, 5];
        $status = in_array($this->dataToValidate["asset_linked_type"], $allowedInValues);
        if($status===false) {
            $this->setErrorMessageOfValidationTypeForAttribute(false, "in", "asset_linked_type");
        } 
        return $status;
    }

    private function getItemLinkedIdExistsStatusAndSetErrorMessageAccordingly(): bool {
        $organizationId = $this->getRequestUserOrganizationId();
        $assetLinkedTypeNumber = $this->dataToValidate["asset_linked_type"];
        $itemLinkedId = $this->dataToValidate["item_linked_id"];
        $typeIdAttribute = $assetLinkedTypeNumber==1 ? "document_id" : "item_id";
        $conditionalClause = [ [ "organization_id", "=", $organizationId] ];
        switch ($assetLinkedTypeNumber) {
            case 1:
                    $conditionalClause[] = [ "document_id" , "=", $itemLinkedId ];
                    $recordCount = Document::where($conditionalClause)->count();
                    break;
            case 2:
                    $conditionalClause[] = [ "item_id" , "=", $itemLinkedId ];
                    $recordCount = Item::where($conditionalClause)->count();
                    break;
            case 4:
                    $conditionalClause[] = [ "project_id" , "=", $itemLinkedId ];
                    $recordCount = Project::where($conditionalClause)->count();
                    break;
            case 5:
                $conditionalClause[] = [ "organization_id" , "=", $itemLinkedId ];
                $recordCount = Organization::where($conditionalClause)->count();
                break;
            default:
                    $recordCount = 0;
                    break;
        }
        $status = $recordCount > 0 ? true : false;
        $this->setErrorMessageOfValidationTypeForAttribute($status, "exists", "item_linked_id");
        return $status;
    }

    private function getAssetFileStatusAndSetErrorMessageAccordingly(): bool {
        $isFileOk = $this->requestObject->file("asset")->isValid();
        $statusToReturn = $isFileOk;
        if($isFileOk) {
            $fileSizeInBytes = $this->requestObject->file("asset")->getClientSize();
            $fileSizeLimitStatus = $fileSizeInBytes <= $this->fileLimitInBytes;
            $statusToReturn = $fileSizeLimitStatus;
            if(!$fileSizeLimitStatus) {
                $this->setErrorMessageOfValidationTypeForAttribute(false, "file_size_limit", "asset");
            }
        }
        else {
            $this->setErrorMessageOfValidationTypeForAttribute(false, "file_valid", "asset");
        }
        return $statusToReturn;
    }

    private function setErrorMessageOfValidationTypeForAttribute(bool $validationStatus, string $validationType, string $attribute) {
        if($validationStatus===false) {
            $messageToPush = !empty($this->errorMessagesAvailable[$validationType][$attribute]) ?
                             $this->errorMessagesAvailable[$validationType][$attribute] : 
                             "";
            if(!empty($messageToPush)) {
                $this->errorMessagesToSet[] = $messageToPush;
            }
        }
    }
}
