<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;

class GetAssetListV2Job extends Job
{

    protected $assetRepository;

    private $itemLinkedIdToSearchWith;
    private $requestUserOrganizationId;
    private $domainName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $requestIdentifier, string $requestUserOrganizationId,$requestUrl='')
    {
        $this->itemLinkedIdToSearchWith =  $requestIdentifier;
        $this->requestUserOrganizationId = $requestUserOrganizationId;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        // set the db repositories
        $this->assetRepository = $assetRepository;
        $requestUserOrganizationId = $this->requestUserOrganizationId;
        $itemLinkedIdToSearchWith = $this->itemLinkedIdToSearchWith;
        $conditionalKeyValuePairs = [
            "organization_id" => $requestUserOrganizationId,
            "item_linked_id" => $itemLinkedIdToSearchWith
        ];
        $assetCollection = $this->assetRepository->findByAttributes($conditionalKeyValuePairs);
        $parsedAsset = [];
        foreach($assetCollection as $asset) {
            $assetId = $asset->asset_id;
            $title = !empty($asset->title) ? $asset->title : "";
            $description = !empty($asset->description) ? $asset->description : "";
            $assetFileName = !empty($asset->asset_name) ? $asset->asset_name : "";
            $assetTargetFileName = !empty($asset->asset_target_file_name) ? $asset->asset_target_file_name : "";
            $previewUrl =  $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
            $downloadUrl = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
            $assetContentType = !empty($asset->asset_content_type) ? $asset->asset_content_type : "";
            $assetSizeInBytes = isset($asset->asset_size) ? $asset->asset_size : 0;
            $assetUploadedDateTime = !empty($asset->uploaded_at) ? $asset->uploaded_at : "";
            $parsedAsset[] = [
                "asset_id" => $assetId,
                "title" => $title,
                "description" => $description,
                "preview_url" => $previewUrl,
                "download_url" => $downloadUrl,
                "asset_file_name" => $assetFileName,
                "asset_content_type" => $assetContentType,
                "asset_size_in_bytes" => $assetSizeInBytes,
                "asset_target_file_name" => $assetTargetFileName,
                "asset_uploaded_date_time" => $assetUploadedDateTime
            ];
        }

        return $parsedAsset;
    }

    private function createAndReturnAssetLinkForType(string $assetPackageName, string $type,$domainName=''): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $baseUrl = env("APP_URL");
        }else{
            $baseUrl = $domainName;

        }
        $fullyResolvedUri = "";
        if(!empty($baseUrl)) {
            $acmtApiVersionPrefix = "api/v1";
            $endPoint = "asset/$type";
            $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
            $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$assetPackageName}";
        }
        return $fullyResolvedUri;
    }
}
