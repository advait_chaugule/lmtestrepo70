<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\AssetHelper;

class ValidateAssetFileForDocumentOrItemByNameJob extends Job
{
    use AssetHelper;

    private $s3StorageDisk;
    private $s3ExemplarAssetUploadFolder;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setS3ExemplarAssetUploadFolder(array $data, string $subFolderNameKey) {
        $s3Details = $data["S3"];
        $this->s3ExemplarAssetUploadFolder = $s3Details["MAIN_ARCHIVE_FOLDER"]."/".$s3Details[$subFolderNameKey];
    }

    private function getS3ExemplarAssetUploadFolder(): string {
        return $this->s3ExemplarAssetUploadFolder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        Request $request,
        AssetRepositoryInterface $assetRepository
    )
    {
        $validationStatus = false;
        $assetTargetFileName = $request->route('asset_target_file_name');
        $fieldToSearchFor = "asset_target_file_name";
        $asset = $assetRepository->findBy($fieldToSearchFor, $assetTargetFileName);
        if(!empty($asset)){
            $subFolderNameKey = "SUB_FOLDER_".$this->helperToReturnAssetLinkedTypeText($asset->asset_linked_type);
            $assetRelatedConfigurations = config("asset");
            $this->setS3StorageDisk();
            $this->setS3ExemplarAssetUploadFolder($assetRelatedConfigurations, $subFolderNameKey);
            if(!empty($assetTargetFileName)) {
                $s3StorageDisk = $this->getS3StorageDisk();
                $s3AssetUploadFolder = $this->getS3ExemplarAssetUploadFolder();
                $s3FullPackageFilePath = "{$s3AssetUploadFolder}/{$assetTargetFileName}";
                $validationStatus =  $s3StorageDisk->exists($s3FullPackageFilePath);
            }
        }
        return $validationStatus;
    }
}
