<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Asset\Validators\AssetIdExistsValidator as Validator;

class ValidateAssetByIdJob extends Job
{

    private $dataToValidate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestData)
    {
        $this->dataToValidate = $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->dataToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
