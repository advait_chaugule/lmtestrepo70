<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

use Illuminate\Support\Facades\DB;

use App\Data\Models\Asset;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\AssetHelper;

class DeleteAssetJob extends Job
{

    use AssetHelper;

    private $assetRepository;

    private $assetIdentifier;
    private $organizationId;

    private $s3StorageDisk;
    private $s3AssetUploadFolder;
    private $s3AssetTargetFileName;

    private $assetToDelete;

    private $s3FileDeleteStatus;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $assetId, array $requestUserDetails )
    {
        $this->setAssetIdentifier($assetId);
        $this->setOrganizationId($requestUserDetails);
    }
    
    /********************************************Setters and getters for request data*****************************************/

    public function setAssetIdentifier(string $data) {
        $this->assetIdentifier = $data;
    }

    public function getAssetIdentifier(): string {
        return $this->assetIdentifier;
    }

    public function setOrganizationId(array $data) {
        $this->organizationId = $data["organization_id"];
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }
    
    /********************************************Setters and getters for file storage related stuff*****************************************/

    private function setS3StorageDisk() {
        $this->s3StorageDisk = Storage::disk('s3');
    }

    private function getS3StorageDisk(): FilesystemAdapter {
        return $this->s3StorageDisk;
    }

    private function setS3AssetUploadFolder() {
        $assetConfigurationData = config("asset");
        $s3Details = $assetConfigurationData["S3"];
        $subFolderKey = $this->helperToReturnSubFolderNameKeyBasedOnType();
        $subFolderName = $s3Details[$subFolderKey];
        $this->s3AssetUploadFolder = "{$s3Details['MAIN_ARCHIVE_FOLDER']}/{$subFolderName}";
    }

    private function getS3AssetUploadFolder(): string {
        return $this->s3AssetUploadFolder;
    }

    private function setS3AssetTargetFileName() {
        $savedAsset = $this->getAssetToDelete();
        $data = $savedAsset->asset_target_file_name;
        $this->s3AssetTargetFileName = $data;
    }

    private function getS3AssetTargetFileName(): string {
        return $this->s3AssetTargetFileName;
    }

    /******************************************Setters and getters for saved asset************************************************/

    public function setAssetToDelete(Asset $data) {
        $this->assetToDelete = $data;
    }

    public function getAssetToDelete(): Asset {
        return $this->assetToDelete;
    }

    public function setS3FileDeleteStatus(bool $status) {
        $this->s3FileDeleteStatus = $status;
    }

    public function getS3FileDeleteStatus(): bool {
        return $this->s3FileDeleteStatus;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        // set db repository
        $this->assetRepository = $assetRepository;
        $this->fetchAndSetAssetToDelete();

        $this->setS3StorageDisk();
        $this->setS3AssetUploadFolder();
        $this->setS3AssetTargetFileName();

        /**************************Delete the asset target file from s3*****************************/
        $this->deleteAssetTargetFileFromS3AndSetStatus();

        $assetDeleteFromS3Status = $this->getS3FileDeleteStatus();
        if($assetDeleteFromS3Status===true) {
            $this->getAssetToDelete()->delete();
        }
    }

    private function fetchAndSetAssetToDelete() {
        $organizationId = $this->getOrganizationId();
        $assetIdentifier = $this->getAssetIdentifier();
        $conditionalAttributes = [
            "organization_id" => $organizationId,
            "asset_id" => $assetIdentifier
        ];
        $result = $this->assetRepository->findByAttributes($conditionalAttributes);
        $asset = $result->first();
        $this->setAssetToDelete($asset);
    }

    private function deleteAssetTargetFileFromS3AndSetStatus() {
        $assetDeleteStatus = false;
        $s3StorageDisk = $this->getS3StorageDisk();
        $s3AssetUploadFolder = $this->getS3AssetUploadFolder();
        $s3AssetTargetFileName = $this->gets3AssetTargetFileName();
        $s3TargetAssetFilePath = "{$s3AssetUploadFolder}/{$s3AssetTargetFileName}";
        if($s3StorageDisk->exists($s3TargetAssetFilePath)){
            $assetDeleteStatus = $s3StorageDisk->delete($s3TargetAssetFilePath);
        }
        $this->setS3FileDeleteStatus($assetDeleteStatus);
    }

    private function helperToReturnSubFolderNameKeyBasedOnType(): string {
        $savedAsset = $this->getAssetToDelete();
        $assetLinkedTypeText = $this->helperToReturnAssetLinkedTypeText($savedAsset->asset_linked_type);
        $subFolderKey = "_NA";
        if(!empty($assetLinkedTypeText) && $assetLinkedTypeText!=="ITEM_ASSOCIATION") {
            $subFolderKey = "SUB_FOLDER_$assetLinkedTypeText";
        }
        return $subFolderKey;
    }
}
