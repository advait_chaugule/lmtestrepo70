<?php
namespace App\Domains\Asset\Jobs;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class UpdateAssetJob extends Job
{
    private $assetRepository;
    private $assetIdentifier;
    private $requestData;
    private $assetEntity;
    /**
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, array $input)
    {
        $this->setAssetIdentifier($identifier);
        $this->setInputData($input);
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        $this->assetRepository = $assetRepository;
        $getAllFileDate =$this->getInputData();
        $assetIdentifier = $this->getAssetIdentifier();
        if(isset($getAllFileDate['title']))
        $attribute['title'] = $getAllFileDate['title'];
        if(isset($getAllFileDate['description']))
            $attribute['description'] = $getAllFileDate['description'];
        if(isset($getAllFileDate['status']))
            $attribute['status'] = $getAllFileDate['status'];
        if(isset($getAllFileDate['asset_order']))
            $attribute['asset_order'] = $getAllFileDate['asset_order'];
        $updatedAsset = $this->assetRepository->edit($assetIdentifier, $attribute);
        $this->parseResponse();
        return $this->getAssetEntity();
    }

    public function setAssetIdentifier($identifier){
        $this->assetIdentifier = $identifier;
    }

    public function getAssetIdentifier(){
        return $this->assetIdentifier;
    }

    public function setInputData($input){
        $this->requestData = $input;
    }

    public function getInputData(){
        return $this->requestData;
    }

    public function setAssetEntity($assetEntity){
        $this->assetEntity = $assetEntity;
    }

    public function getAssetEntity(){
        return $this->assetEntity;
    }

    private function parseResponse(){
        $assetIdentifier = $this->getAssetIdentifier();
        $data = DB::table('assets')->where('asset_id', $assetIdentifier)->first();

        $assetEntity = [
            "asset_id"    =>  $assetIdentifier,
            "title"       =>   $data->title,
            "description" =>   $data->description,
            "asset_order" =>   $data->asset_order,
            "status"      =>   $data->status,
        ];
        $this->setAssetEntity($assetEntity);
    }
}
