<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\AssetHelper;
use App\Services\Api\Traits\DateHelpersTrait;


use App\Data\Repositories\Contracts\AssetRepositoryInterface;

class GetListOfAssetJob extends Job
{
    use ArrayHelper, AssetHelper, DateHelpersTrait;


    private $assetRepository;
    private $itemLinkedId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemLinkedId, array $requestUserDetails)
    {
        $this->setItemLinkedId($itemLinkedId);
        $this->setOrganizationId($requestUserDetails);
    }

    public function setItemLinkedId(string $data) {
        $this->itemLinkedId = $data;
    }

    public function getItemLinkedId(): string {
        return $this->itemLinkedId;
    }

    public function setOrganizationId(array $data) {
        $this->organizationId = $data["organization_id"];
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        //
        $itemLinkedId = $this->getItemLinkedId();
        $organizationId = $this->getOrganizationId();
//dd("111");
        $conditionalClauseKeyValuePairs = [
            "item_linked_id" => $itemLinkedId,
            "organization_id" => $organizationId,
        ];
    
        $itemList = $assetRepository->findByAttributes($conditionalClauseKeyValuePairs);
        $itemListArr = [];
        if(count($itemList->toArray()) > 0)
        {
            foreach ($itemList as $item)
            {
                $itemListArr[] = [
                    "asset_id" => $item["asset_id"],
                    "organization_id" => $item["organization_id"],
                    "document_id" => $item["document_id"],
                    "asset_name" => $item["asset_name"],
                    "asset_target_file_name" => $item["asset_target_file_name"],
                    "asset_content_type" => $item["asset_content_type"],
                    "asset_size" => $item["asset_size"],
                    "title" => $item["title"],
                    "description" => $item["description"],
                    "item_linked_id" => $item["item_linked_id"],
                    "asset_linked_type" => $item["asset_linked_type"],
                    "status" =>$item["status"],
                    "asset_order" =>$item["asset_order"],
                    "uploaded_at" => $item["uploaded_at"]
                ];
            }

            $sorted_array = array();
            if($item["asset_linked_type"] != 5){
                foreach ($itemListArr as $key => $row)
                {
                    $sorted_array[$key] = $row['uploaded_at'];
                }
                array_multisort($sorted_array, SORT_DESC, $itemListArr);

            }else{
                foreach ($itemListArr as $key => $row)
                {
                    $sorted_array[$key] = $row['asset_order'];
                }
                array_multisort($sorted_array, SORT_ASC, $itemListArr);

            }
        }

        
        return $itemListArr;
    }
}
