<?php
namespace App\Domains\Asset\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;

class GetItemAssetListV2Job extends Job
{
    protected $assetRepository;

    private $itemLinkedIdToSearchWith;
    private $requestUserOrganizationId;
    private $domainName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $requestIdentifier, string $requestUserOrganizationId,$requestUrl='')
    {
        $this->itemLinkedIdToSearchWith =  $requestIdentifier;
        $this->requestUserOrganizationId = $requestUserOrganizationId;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        // set the db repositories
        $this->assetRepository = $assetRepository;
        $reqUserOrganizationId = $this->requestUserOrganizationId;
        $itemLinkedIdToSearch = $this->itemLinkedIdToSearchWith;
        $conditionalKeyValuePairs = [
            "organization_id" => $reqUserOrganizationId,
            "document_id" => $itemLinkedIdToSearch,
            "asset_linked_type" => 2
        ];
        $assetCollections = $this->assetRepository->findByAttributes($conditionalKeyValuePairs);
        $parsedAsset = [];
        foreach($assetCollections as $assets) {
            $itemAssetId = $assets->asset_id;
            $itemTitle = !empty($assets->title) ? $assets->title : "";
            $itemDescription = !empty($assets->description) ? $assets->description : "";
            $itemAssetFileName = !empty($assets->asset_name) ? $assets->asset_name : "";
            $itemAssetTargetFileName = !empty($assets->asset_target_file_name) ? $assets->asset_target_file_name : "";
            $itemPreviewUrl =  $this->createAndReturnAsset($itemAssetTargetFileName, 'preview',$this->domainName);
            $itemDownloadUrl = $this->createAndReturnAsset($itemAssetTargetFileName, 'download',$this->domainName);
            $itemAssetContentType = !empty($assets->asset_content_type) ? $assets->asset_content_type : "";
            $itemAssetSizeInBytes = isset($assets->asset_size) ? $assets->asset_size : 0;
            $itemAssetUploadedDateTime = !empty($assets->uploaded_at) ? $assets->uploaded_at : "";
          
            $parsedAsset[$assets->item_linked_id]['assets'][] = [
                "asset_id" => $itemAssetId,
                "title" => $itemTitle,
                "description" => $itemDescription,
                "preview_url" => $itemPreviewUrl,
                "download_url" => $itemDownloadUrl,
                "asset_file_name" => $itemAssetFileName,
                "asset_content_type" => $itemAssetContentType,
                "asset_size_in_bytes" => $itemAssetSizeInBytes,
                "asset_target_file_name" => $itemAssetTargetFileName,
                "asset_uploaded_date_time" => $itemAssetUploadedDateTime
            ];
        }
        return $parsedAsset;
        //
    }

    private function createAndReturnAsset(string $itemAssetPackageName, string $type,$domainName): string {
        if(strpos($domainName,'localhost')!==false)
        {
            $itemBaseUrl = env("APP_URL");
        }else{
            $itemBaseUrl = $domainName;
        }
        $resolvedUri = "";
        if(!empty($itemBaseUrl)) {
            $itemApiVersionPrefix = "api/v1";
            $itemEndPoint = "asset/$type";
            $itemBaseUrlWithEndPoint = "{$itemBaseUrl}/{$itemApiVersionPrefix}/{$itemEndPoint}";
            $resolvedUri = "{$itemBaseUrlWithEndPoint}/{$itemAssetPackageName}";
        }
        return $resolvedUri;
    }
}
