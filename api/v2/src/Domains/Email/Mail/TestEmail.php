<?php

namespace App\Domains\Email\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $userName;
    public $downloadLink;
    public $organizationName;
    public $orgCode;
    public $recepient;
    public $domainName;
    public $header;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];
        $this->recepient = $data['recepient'];
        $this->body = $data['body'];
         $this->userName = $data['username'];
         $this->downloadLink =  $data['downloadLink'];
         $this->organizationName = $data['orgname'];
         $this->orgCode = $data['orgCode'];
         $this->domainName = $data['domainName'];
         $this->header = $data['header'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->view('email.Export')
                    ->with([
                        "email"     => $this->recepient,
                        "body" => $this->body,
                        "userName" => $this->userName,
                        "downloadLink" => $this->downloadLink,
                        "organizationName" => $this->organizationName,
                        "domainName" => $this->domainName,
                        "linkDomainName" => $this->domainName,
                        "orgCode" => $this->orgCode,
                        "header" => $this->header

                    ])
                   ->subject($this->subject);


                  
    }
}
