<?php
namespace App\Domains\Email\Jobs;

use Lucid\Foundation\QueueableJob;

use Illuminate\Support\Facades\Mail;

use App\Domains\Email\Mail\TestEmail;

class SendEmailJob extends QueueableJob
{

    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         Mail::to($this->data['recepient'])->send(new TestEmail($this->data));
    }
}
