<?php
namespace App\Domains\Email\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Email\Validators\EmailInputValidator;

class ValidateEmailInputJob extends Job
{
    private $inputs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EmailInputValidator $validator)
    {
        $validation = $validator->validate($this->inputs);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
