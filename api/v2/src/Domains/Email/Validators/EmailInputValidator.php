<?php
namespace App\Domains\Email\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class EmailInputValidator extends BaseValidator {

    protected $rules = [
        'recepient' => 'required|email',
        'subject' => 'required|string',
        'body' => 'required|string'
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'string' => ':attribute should be a string.',
        'email' => ':attribute should be of type email.'
     ];
    
}