<?php
namespace App\Domains\Csv\Jobs;
use Illuminate\Http\Request;

use Lucid\Foundation\Job;
use Illuminate\Http\UploadedFile;

class ValidateCsvFileJob extends Job
{
    private $request;
    private $inputFileKey;
    private $inputFileExtension;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->inputFileKey = "csvFile";
        $this->inputFileExtension = "csv";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //dd($this->request->file($this->inputFileKey)->getClientOriginalExtension());
        if (
            $this->request->hasFile($this->inputFileKey) && 
            $this->request->file($this->inputFileKey)->isValid() && 
            $this->request->file($this->inputFileKey)->getClientSize() > 0 && 
            $this->request->file($this->inputFileKey)->getClientOriginalExtension()===$this->inputFileExtension
        ) {
            $status = true;
            $message = [];
            $file = $this->request->file($this->inputFileKey);
        }
        else {
            $status = false;
            $message = ["csv" => ["file" => "Please upload a valid CSV file."]];
            $file = '';
        }
        $response = ['status' => $status, "message" => $message, 'file' => $file];
        return $response;
    }
}
