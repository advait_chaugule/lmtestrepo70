<?php
namespace App\Domains\Csv\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Csv\Validators\ImportPass2CsvMultipleValidator;

class ValidateImportPass2MultipleCsv extends Job
{
    public $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImportPass2CsvMultipleValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->errors()->all();
    }
}
