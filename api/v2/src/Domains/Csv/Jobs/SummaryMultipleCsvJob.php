<?php
namespace App\Domains\Csv\Jobs;

ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

use App\Data\Models\Item;
use App\Data\Models\ItemAssociation;
use App\Services\Api\Traits\ArrayHelper;

class SummaryMultipleCsvJob extends Job
{
    use UuidHelperTrait, DateHelpersTrait, ArrayHelper;

    private $inputData;
    private $userId;
    private $organizationIdentifier;
    private $packageExists;

    private $errorData;
    private $parsedItemsFromCsv;
    private $parsedItemAssociationFromCsv;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $metadataRepository;

    private $count;
    private $blankCount;
    private $newDocCount;
    private $newNodeCount;
    private $deleteNodeCount;
    private $newAssociationsCount;

    private $updatedMetadataCount;
    private $blankMetadataCount;
    private $newAdditionalMetadataCount;

    private $columnFromHeaders;
    private $nodeTypeIdArray;

    private $createdDocumentIdentifier = '';
    private $changedDocument=[];
    private $changedItem=[];
    private $changedMetadata=[];

    private $nodeType;
    private $title;
    private $metadataList;
    private $caseMetadataSet;
    private $customMetadataSet;

    private $newItemArray;
    private $newItemIdArray;
    private $itemCollectionFromDb;

    private $updatedItemArray;
    private $createdItemArray;
    private $deletedItemArray;

    private $taxonomyStatus;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputData, string $organizationIdentifier, array $requestingUserDetail, string $importType, array $csvColumn, array $metadataListForATenant, string $packageExists, $request, $importJobId = '',$isBlocker = false)
    {
        $this->request = $request;
        $this->importJobId = $importJobId;
        $this->createdDocumentIdentifier = '';
        //Set the private atribute
        $this->count                    =   0;
        $this->blankCount               =   0;
        $this->newDocCount              =   0;
        $this->newNodeCount             =   0;
        $this->deleteNodeCount          =   0;
        $this->deleteAssociationsCount  =   0;
        $this->newAssociationsCount     =   0;

        $this->updatedMetadataCount         =   0;
        $this->blankMetadataCount           =   0;
        $this->newAdditionalMetadataCount   =   0;
        // Condition added to add case_association_type, action, case_node_type even if columns are not added
        foreach($inputData as $k1=>$v1)
        {
            $v1 = array_change_key_case($v1,CASE_LOWER);
            if(!array_key_exists('case_association_type',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['case_association_type' => ""];
                }, $inputData);
            }

            if(!array_key_exists('action',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['action' => ""];
                }, $inputData);
            }

            if(!array_key_exists('case_node_type',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['case_node_type' => ""];
                }, $inputData);
            }

            break;
        }
        $this->inputData        =   $this->trim_array_spaces($inputData);
        $this->userId           =   $requestingUserDetail['user_id'];
        $this->packageExists    =   $packageExists;
        $this->organizationIdentifier = $organizationIdentifier;
        $this->isBlocker = $isBlocker;
        // Condition added to add case_association_type, action, case_node_type even if columns are not added
        if (!in_array('case_association_type', $csvColumn))
        {
            array_push($csvColumn,'case_association_type');
        }
        if (!in_array('case_node_type', $csvColumn))
        {
            array_push($csvColumn,'case_node_type');
        }
        if (!in_array('action', $csvColumn))
        {
            array_push($csvColumn,'action');
        }

        $this->columnFromHeaders = array_slice($csvColumn,2);
        $this->importType = $importType;
        $this->metadataListForATenant   =   $metadataListForATenant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository, NodeTypeRepositoryInterface $nodeTypeRepository, ItemAssociationRepositoryInterface $itemAssociationRepository, MetadataRepositoryInterface $metadataRepository, ProjectRepositoryInterface $projectRepository, Request $request )
    {
        $emptyParent            =   0;
        $emptyNode              =   0;
        //Set the repo handler
        $this->documentRepository               =   $documentRepository;
        $this->itemRepository                   =   $itemRepository;
        $this->itemAssociationRepository        =   $itemAssociationRepository;
        $this->nodeTypeRepository               =   $nodeTypeRepository;
        $this->metadataRepository               =   $metadataRepository;
        $this->projectRepository                =   $projectRepository;
        $this->documentIdentifier = "";
        $inputData = $this->inputData;
        unset($inputData[0]);

        $this->fetchMetadataList($this->request);
        $listCaseAndCustomMetadata = $this->listCaseAndCustomMetadata();
        if($listCaseAndCustomMetadata == false) {
            return false;
        } else {
            if($listCaseAndCustomMetadata !==  true) {
                if($listCaseAndCustomMetadata == 3) {
                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                    return '3|mUy7V3e|';
                } else {
                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                    return '2|mUy7V3e|'.json_encode($listCaseAndCustomMetadata, true);
                }
            }else {
                $inputData  =   $this->inputData;

                if($inputData[0][$this->title] == '') {
                    return false;
                }else {
                    /**
                     * Validation to check if wrong parent id is present in csv start
                    */
                    $csvDocumentId = $inputData[0]['node_id'];
                    $getExistingParentNodeIdIfPresent = $this->getExistingParentNodeIdIfPresent($csvDocumentId);
                    $importType=$this->importType;

                    $arrayOfNodeParentIdFromCSV    =   array_column($inputData,'parent/destination_node_id');
                    $arrayOfNodeParentIdFromCSVUnique = array();
                    foreach($arrayOfNodeParentIdFromCSV as $arrayOfNodeParentIdFromCSVK=>$arrayOfNodeParentIdFromCSVV)
                    {
                        if(!empty($arrayOfNodeParentIdFromCSVV))
                        {
                            #uuid is of 36 length
                            // $arrayOfNodeParentIdFromCSVUnique[]=substr($arrayOfNodeParentIdFromCSVV,-36);
                            # altering this line since if uuid in csv is more than 36 characters long and upto 36 characters, uuid is correct, then this error is not thrown
                            $arrayOfNodeParentIdFromCSVUnique[]=trim($arrayOfNodeParentIdFromCSVV);
                        }
                    }
                    $arrayOfNodeIdFromCSV    =   array_column($inputData,'node_id');
                    $arrayOfNodeIdFromCSVUnique = array();
                    foreach($arrayOfNodeIdFromCSV as $arrayOfNodeIdFromCSVK=>$arrayOfNodeIdFromCSVV)
                    {
                        if(!empty($arrayOfNodeIdFromCSVV))
                        {
                            // $arrayOfNodeIdFromCSVUnique[]=substr($arrayOfNodeIdFromCSVV,-36);
                            # altering this line since if uuid in csv is more than 36 characters long and upto 36 characters, uuid is correct, then this error is not thrown
                            $arrayOfNodeIdFromCSVUnique[]=trim($arrayOfNodeIdFromCSVV);
                        }
                    }
                        if(!empty($getExistingParentNodeIdIfPresent))
                            $nodeParentIdDiff = array_diff($arrayOfNodeParentIdFromCSVUnique,$getExistingParentNodeIdIfPresent);
                        else
                            $nodeParentIdDiff = $arrayOfNodeParentIdFromCSVUnique;
                        $invalidParentNodeId = array_unique(array_diff($nodeParentIdDiff,$arrayOfNodeIdFromCSVUnique));
                        // invalidParentNodeId - all invalid destination id for nodes & association
                        if(!empty($invalidParentNodeId))
                        {
                            $destinationNodeIds = [];
                            foreach($inputData as $akey=>$assocList){
                                if(!empty($assocList['parent/destination_node_id']) && (empty($assocList['case_association_type']) || strtolower($assocList['case_association_type'])=='ischildof') && trim($assocList['case_full_statement'])!=''){
                                    array_push($destinationNodeIds,trim($assocList['parent/destination_node_id']));
                                }
                            }
                            // destinationNodeIds - all destination id only for nodes
                            $arrayInvalidNodeId = array_unique(array_intersect($invalidParentNodeId,$destinationNodeIds));
                            // arrayInvalidNodeId - get id if present in node ignore if association
                            # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                            if(!empty($arrayInvalidNodeId))
                                return '4|mUy7V3e|'.json_encode($arrayInvalidNodeId, true);

                        }
                    /**
                     * Validation to check in wrong parent id is present in csv end
                    */
                    unset($inputData[0]);
                    $arrayOfNodeParentId    =   array_column($inputData, 'parent/destination_node_id');
                    $arrayOfNodeId          =   array_column($inputData, 'node_id');    
                    foreach($arrayOfNodeParentId as $parentId) {
                        if(empty($parentId)) {
                            $emptyParent++;                        }
                    }
                    foreach($arrayOfNodeId as $nodeId) {
                        if(empty($nodeId)) {
                            $emptyNode++;
                        }
                    }
                    if($emptyParent > 0 || $emptyNode > 0) {
                        return false;
                    } else {
                        $columnsFromHeaders     =   $this->customMetadataSet;
                        # to check if custom metadata has type and if it is valid
                        $checkCustomNameAndType =   $this->checkCustomMetadataColumnNameAndType($columnsFromHeaders);

                        if($checkCustomNameAndType == true){
                            return false;
                        }else {
                            $data = $this->parseDataToFilterAssociation($inputData);

                            if(is_array($data))
                            {
                                if(end($data)=='duplicateNodeIds')
                                {
                                    array_pop($data);
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '5|mUy7V3e|'.json_encode($data, true);
                                }
                                else if(end($data)=='sameNodeIdAndParentId')
                                {
                                    array_pop($data);
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '6|mUy7V3e|'.json_encode($data, true);
                                }
                            }
                            else
                            {
                                if($this->isBlocker == false)
                                {
                                    $this->updateProgressPercentage(10);
                                    $this->validateInputDataWithActionTypes();
                                    $this->updateProgressPercentage(30);
                                    $this->updateDocumentData();
                                    $this->updateProgressPercentage(60);
                                    $this->updateItemData();
                                    $this->updateProgressPercentage(80);

                                    if($this->packageExists == 1  && $this->nodeType != '') {
                                        $updateMetadata =   $this->updateMetadataData();
                                    }

                                    // commented for not considering removed node count while csv update
                                    //$deleteItem     =   $this->deleteItemNotInCsvFromCollection();

                                    $responseData   =   $this->parseResponse();
                                    $this->updateProgressPercentage(100);
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '1|mUy7V3e|'.json_encode($responseData, true);
                                }
                                else
                                {
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '1|mUy7V3e|';
                                }   
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Public Setter and Getter methods
     */
    public function getParsedItemsFromCsv() {
        return !empty($this->parsedItemsFromCsv) ? $this->parsedItemsFromCsv : [];
    }

    public function getParsedItemAssociationFromCsv() {
        return !empty($this->parsedItemAssociationFromCsv) ? $this->parsedItemAssociationFromCsv : [];
    }

    private function fetchMetadataList($request) {
        $metadataList   =   $this->metadataRepository->getMetadataList($request);

        $this->metadataList = $metadataList;
    }

    public function getChangeInMetadata() {
        return $this->changedMetadata;
    }

    #update progress in database
    public function updateProgressPercentage($percentage)
    {
        if(!empty($this->importJobId)) {
            $taxonomy_settings = DB::table('import_job')->select('import_taxonomy_settings')->where('import_job_id', $this->importJobId)->get()->first();
            $settings_json = json_decode($taxonomy_settings->import_taxonomy_settings);
            $settings_json->summary_percentage = $percentage;
            DB::table('import_job')->where('import_job_id', $this->importJobId)->update(['import_taxonomy_settings' => json_encode($settings_json)]);
        }
    }

    /**
     * Method to filter Items from CSV
     * and Item Associations from CSV
     *
     * @param [array] $itemsFromCSV
     * @return void
     */
    private function parseDataToFilterAssociation($itemsFromCSV) {
        /**
         * Data filtered to get unique id based on node_id and action and case field start
         */

        $titleHeader        =   $this->title;
        $associationHeader  =   $this->associationType;
        if(!empty($associationHeader)){
        foreach($itemsFromCSV  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }

        $itemsFromCSVCopy = $itemsFromCSV;
        $getUniqueitemIdArray = array();
        $getUniqueitemIdArray = array_unique($itemIdArray);

        $getItemArrayDataToInsert = array();
        $totalItemIdsFromCsv = array();
        $sameNodeIdAndParentId = array();
        # check node_id and parent_id are same in one row
        foreach($getUniqueitemIdArray as $getUniqueitemIdArrayK=>$getUniqueitemIdArrayV)
        {
            foreach($itemsFromCSVCopy as $itemsFromCSVCopyK=>$itemsFromCSVCopyV)
            {
                // $associationType2 = strtolower(str_replace(' ','',$itemsFromCSVCopyV[$associationHeader]));
                if(($getUniqueitemIdArrayV==$itemsFromCSVCopyV['node_id']) && ((strtolower($itemsFromCSVCopyV[$associationHeader])==strtolower('isChildOf') || $itemsFromCSVCopyV[$associationHeader]=="") && !empty($itemsFromCSVCopyV['case_full_statement'])))
                {
                    $getItemArrayDataToInsert[]=$itemsFromCSVCopyV;
                    $totalItemIdsFromCsv[$itemsFromCSVCopyK] = $itemsFromCSVCopyV['node_id'];

                    if($itemsFromCSVCopyV['node_id'] == $itemsFromCSVCopyV['parent/destination_node_id'])
                    {
                        $sameNodeIdAndParentId[] = $itemsFromCSVCopyK+2;
                    }
                }
            }
        }

        if(!empty($sameNodeIdAndParentId))
        {
            $errorMessageToIdentify = ['sameNodeIdAndParentId'];
            $mergedArrayForSameNodeIdAndParentId = array_merge($sameNodeIdAndParentId,$errorMessageToIdentify);

            return $mergedArrayForSameNodeIdAndParentId;
        }

        $totalItemIdsFromCsvUnique = array_unique($totalItemIdsFromCsv);
        if( count($totalItemIdsFromCsvUnique) != count($totalItemIdsFromCsv) )
        {
            $duplicateItemIds = array();
            $duplicateItemIds = array_diff_assoc($totalItemIdsFromCsv, array_unique($totalItemIdsFromCsv));

            $getRowNos = array();
            foreach($totalItemIdsFromCsv as $totalItemIdsFromCsvKey=>$totalItemIdsFromCsvValue)
            {
                foreach($duplicateItemIds as $duplicateItemIdsKey=>$duplicateItemIdsValue)
                {
                    if($totalItemIdsFromCsvValue == $duplicateItemIdsValue)
                    {
                        $getRowNos[] = $totalItemIdsFromCsvKey+2;
                    }
                }
            }

            $errorMessageToIdentify = ['duplicateNodeIds'];
            $mergedArrayForDuplicateItemIds = array_merge($getRowNos,$errorMessageToIdentify);

            return $mergedArrayForDuplicateItemIds;
        }

        $itemArrayDataCopy = [];
        foreach($getItemArrayDataToInsert as $getItemArrayDataToInsertK=>$getItemArrayDataToInsert)
        {
            $itemArrayDataCopy[$getItemArrayDataToInsertK+1] = $getItemArrayDataToInsert;
        }

        /**
        * Data filtered to get all relations excluding isChildOf start
        */

        #TODO: merge multiple foreach where possible
        /*
        $getRawDataForMultipleAssociations = array();
        $exchildof=0;
        foreach($itemsFromCSVCopy as $itemsFromCSVCopyKey=>$itemsFromCSVCopyValue)
        {
            $itemsFromCSVCopyValue = array_change_key_case($itemsFromCSVCopyValue,CASE_LOWER);
            $associationType = strtolower(str_replace(' ','',$itemsFromCSVCopyValue['case_association_type']));
            $exludeChildOfArray = array('ischildof','');
            if(!in_array($associationType,$exludeChildOfArray))
            {
                $getRawDataForMultipleAssociations[$exchildof]['action'] = $itemsFromCSVCopyValue['action'];
                $getRawDataForMultipleAssociations[$exchildof]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                $getRawDataForMultipleAssociations[$exchildof]['parent/destination_node_id'] = $itemsFromCSVCopyValue['parent/destination_node_id'];
                $getRawDataForMultipleAssociations[$exchildof]['case_association_type'] = $itemsFromCSVCopyValue['case_association_type'];
                $getRawDataForMultipleAssociations[$exchildof]['document_id'] = $this->createdDocumentIdentifier;
                $exchildof = $exchildof +1;
            }
        }
        */

        $getRawDataForMultipleAssociations = array();
        $allasso=0;
        foreach($itemsFromCSVCopy as $itemsFromCSVCopyKey=>$itemsFromCSVCopyValue)
        {
            $itemsFromCSVCopyValue = array_change_key_case($itemsFromCSVCopyValue,CASE_LOWER);
            $associationType = strtolower(str_replace(' ','',$itemsFromCSVCopyValue['case_association_type']));
            $childOfArray = array('ischildof','');
            if(!in_array($associationType,$childOfArray))
            {
                $getRawDataForMultipleAssociations[$allasso]['action'] = $itemsFromCSVCopyValue['action'];
                $getRawDataForMultipleAssociations[$allasso]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                $getRawDataForMultipleAssociations[$allasso]['parent/destination_node_id'] = $itemsFromCSVCopyValue['parent/destination_node_id'];
                $getRawDataForMultipleAssociations[$allasso]['case_association_type'] = $itemsFromCSVCopyValue['case_association_type'];
                $getRawDataForMultipleAssociations[$allasso]['document_id'] = $this->createdDocumentIdentifier;
                $allasso = $allasso +1;
            } else if(in_array($associationType,$childOfArray) && trim($itemsFromCSVCopyValue['case_full_statement'])==''){
                $getRawDataForMultipleAssociations[$allasso]['action'] = $itemsFromCSVCopyValue['action'];
                $getRawDataForMultipleAssociations[$allasso]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                $getRawDataForMultipleAssociations[$allasso]['parent/destination_node_id'] = $itemsFromCSVCopyValue['parent/destination_node_id'];
                $getRawDataForMultipleAssociations[$allasso]['case_association_type'] = $itemsFromCSVCopyValue['case_association_type'];
                $getRawDataForMultipleAssociations[$allasso]['document_id'] = $this->createdDocumentIdentifier;
                $allasso = $allasso +1;
            }
        }


        /**
        * Data filtered to get all relations excluding isChildOf end
        */

        /**
        * Data filtered to get all isChildOf relations of current taxonomy or other taxonomy start
        */
        /*
        $getRawDataForisChildOfAssociations = array();
        $inchildof=0;
        foreach($itemsFromCSVCopy as $itemsFromCSVCopyIndex=>$itemsFromCSVCopyData)
        {
            $itemsFromCSVCopyData = array_change_key_case($itemsFromCSVCopyData,CASE_LOWER);
            $associationType2 = strtolower(str_replace(' ','',$itemsFromCSVCopyData['case_association_type']));
            $inludeChildOfArray = array('ischildof','');
            if(in_array($associationType2,$inludeChildOfArray) && ($itemsFromCSVCopyData['case_full_statement']==''))
            {
                    $getRawDataForisChildOfAssociations[$inchildof]['action'] = $itemsFromCSVCopyData['action'];
                    $getRawDataForisChildOfAssociations[$inchildof]['node_id'] = $itemsFromCSVCopyData['node_id'];
                    $getRawDataForisChildOfAssociations[$inchildof]['parent/destination_node_id'] = $itemsFromCSVCopyData['parent/destination_node_id'];
                    $getRawDataForisChildOfAssociations[$inchildof]['case_association_type'] = $itemsFromCSVCopyData['case_association_type'];
                    $getRawDataForisChildOfAssociations[$inchildof]['document_id'] = $this->createdDocumentIdentifier;
                    $inchildof = $inchildof +1;
            }
        }
        */


        /**
        * Data filtered to get all isChildOf relations of current taxonomy or other taxonomy end
        */

        /**
         * Merge getRawDataForMultipleAssociations and getRawDataForisChildOfAssociations start
         */

        // $multipleNewAssociationsRawData = array();
        // $multipleNewAssociationsRawData = array_merge($getRawDataForMultipleAssociations,$getRawDataForisChildOfAssociations);

        /**
         * Merge getRawDataForMultipleAssociations and getRawDataForisChildOfAssociations start
         */

        // $itemsFromCSV               =   $itemArrayDataCopy;
        // $itemArrayAssociationData   =   $multipleNewAssociationsRawData;

        $this->parsedItemsFromCsv = $itemArrayDataCopy;
        $this->parsedItemAssociationFromCsv = $getRawDataForMultipleAssociations;
        /**
         * Data filtered to get unique id based on node_id and action and case field end
         */
        }

        return true;
    }

    /**
     * Method to process the validation of data based on import type
     *
     * @param [integer] $importType
     * @param [array] $allItemsSourceIdFromTenant
     * @param [array] $itemIdNotInDb
     * @param [array] $itemsFromCSV
     * @param [array] $itemAssociationsFromCSV
     * @return void
     */
    private function processDataValidationForItemsAndAssociationsForImportType($importType, $allItemsSourceIdFromTenant, $itemIdNotInDb, $itemsFromCSV, $itemAssociationsFromCSV) {
        $errorUpdateRowForItems     =   [];
        $errorDeleteRowForItems     =   [];
        $errorRowForUpdateAssociations    =   [];
        $errorRowForDeleteAssociations    =   [];
        $errorRowForNotFoundDestinationId =   [];
        $errorInvalidStatusDateRowForItems = [];
        $errorGreaterStatusDateRowForItems = [];
        $errorSourceUrlRowForItems = [];
        $errorInvalidCustomDateRowForItems = [];

        $errorFlagForUpdateItem     =   0;
        $errorFlagForDeleteItem     =   0;
        $notFoundDestinationNode    =   0;
        $errorFlagForUpdateAssociation    =   0;
        $errorFlagForDeleteAssociation    =   0;
        $errorFlagForInvalidStatusDate = 0;
        $errorFlagForGreaterStatusDate = 0;
        $errorFlagForSourceUrl = 0;
        $errorFlagForInvalidCustomDate = 0;

        $arrayUpdate = ['u','update'];
        $arrayDelete = ['d','delete'];
        $inputData = $this->inputData;
        $deleteNodeCount = $this->deleteNodeCount;
        $deleteAssociationsCount = $this->deleteAssociationsCount;
        $newAssociationsCount = $this->newAssociationsCount;
        $customColumnsFromHeaders = $this->customMetadataSet;
        $fieldTypeValues = config("default_enum_setting.metadata_field_type");

        $nodeIdsFromCsv   = array_column($inputData, 'node_id');
        // Code changes for Start End date & Invalid Url
		if(isset($inputData[0]['case_status_start_date']) && isset($inputData[0]['case_status_end_date'])){
            if($inputData[0]['case_status_start_date']=="0" || $inputData[0]['case_status_end_date'] == "0" || (!empty($inputData[0]['case_status_start_date']) && !$this->validateDate($inputData[0]['case_status_start_date'])) || (!empty($inputData[0]['case_status_end_date']) && !$this->validateDate($inputData[0]['case_status_end_date'])))
            {
                array_push($errorInvalidStatusDateRowForItems, 2);
                $errorFlagForInvalidStatusDate  =   1;
            }
            else if(!empty($inputData[0]['case_status_start_date']) && !empty($inputData[0]['case_status_end_date']) && strtotime($inputData[0]['case_status_start_date']) > strtotime($inputData[0]['case_status_end_date'])){
                array_push($errorGreaterStatusDateRowForItems, 2);
                $errorFlagForGreaterStatusDate  =   1;
            }
        }
        # validate if valid url
        if(isset($inputData[0]['case_official_source_url']) && !empty($inputData[0]['case_official_source_url'])){
            if(!filter_var($inputData[0]['case_official_source_url'], FILTER_VALIDATE_URL)){
                array_push($errorSourceUrlRowForItems, 2);
                $errorFlagForSourceUrl  =   1;
            }
        }
        # validation for date values in custom metadata for document
        foreach($customColumnsFromHeaders as $customCsvHeaders) {
            $metadataPart       =   explode("(", $customCsvHeaders);
            $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
            if($metadataType == 4){
                if($inputData[0][$customCsvHeaders] == "0" || (!empty($inputData[0][$customCsvHeaders]) && !$this->validateDate($inputData[0][$customCsvHeaders])) || (!empty($inputData[0][$customCsvHeaders]) && !$this->validateDate($inputData[0][$customCsvHeaders])))
                {
                    array_push($errorInvalidCustomDateRowForItems, 2);
                    $errorFlagForInvalidCustomDate  =   1;
                }
            }
        }
        unset($inputData[0]);
        if($importType == 4) {
            foreach($itemsFromCSV as $itemKey => $itemsToAdd) {
                $itemsToAdd = array_change_key_case($itemsToAdd,CASE_LOWER);
                if(in_array($itemsToAdd['node_id'], $itemIdNotInDb)) {
                    if(in_array(strtolower($itemsToAdd['action']),$arrayUpdate)) {
                        $rowIndexFromCsv = $this->searchForId($itemsToAdd['node_id'],
                        $inputData, array('$'),$errorUpdateRowForItems, ['key'=>'action','value'=>$itemsToAdd['action']]);
                        array_push($errorUpdateRowForItems, ($rowIndexFromCsv + 2));
                        $errorFlagForUpdateItem  =   1;
                    } else if(in_array(strtolower($itemsToAdd['action']),$arrayDelete)) {
                        $rowIndexFromCsv = $this->searchForId(trim($itemsToAdd['node_id']),
                        $inputData, array('$'),$errorDeleteRowForItems, ['key'=>'action','value'=>$itemsToAdd['action']]);
                        array_push($errorDeleteRowForItems, ($rowIndexFromCsv + 2));
                        $errorFlagForDeleteItem  =   1;
                    }
                } else {
                    if(in_array(strtolower($itemsToAdd['action']),$arrayDelete)) {
                        $deleteNodeCount++;
                        $this->deleteNodeCount = $deleteNodeCount;
                    }
                }
				// Code changes for Start End date & Invalid Url
                if(isset($itemsToAdd['case_status_start_date']) && isset($itemsToAdd['case_status_end_date'])){
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                    if($itemsToAdd['case_status_start_date']=="0" || $itemsToAdd['case_status_end_date'] == "0" || (!empty($itemsToAdd['case_status_start_date']) && !$this->validateDate($itemsToAdd['case_status_start_date'])) || (!empty($itemsToAdd['case_status_end_date']) && !$this->validateDate($itemsToAdd['case_status_end_date'])))
                    {
                        array_push($errorInvalidStatusDateRowForItems, ($rowIndexFromCsv+2));
                        $errorFlagForInvalidStatusDate  =   1;
                    }
                    else if(!empty($itemsToAdd['case_status_start_date']) && !empty($itemsToAdd['case_status_end_date']) && strtotime($itemsToAdd['case_status_start_date']) > strtotime($itemsToAdd['case_status_end_date'])){
                        array_push($errorGreaterStatusDateRowForItems, ($rowIndexFromCsv+2));
                        $errorFlagForGreaterStatusDate  =   1;
                    }
                }
                if(isset($itemsToAdd['case_official_source_url']) && !empty($itemsToAdd['case_official_source_url'])){
                    if(!filter_var($itemsToAdd['case_official_source_url'], FILTER_VALIDATE_URL)){
                        $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                        array_push($errorSourceUrlRowForItems, ($rowIndexFromCsv + 2));
                        $errorFlagForSourceUrl  =   1;
                    }
                }

                foreach($customColumnsFromHeaders as $customCsvHeaders) {
                    $metadataPart       =   explode("(", $customCsvHeaders);
                    $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
                    if($metadataType == 4){
                        $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                        if($itemsToAdd[$customCsvHeaders] == "0" || (!empty($itemsToAdd[$customCsvHeaders]) && !$this->validateDate($itemsToAdd[$customCsvHeaders])) || (!empty($itemsToAdd[$customCsvHeaders]) && !$this->validateDate($itemsToAdd[$customCsvHeaders])))
                        {
                            array_push($errorInvalidCustomDateRowForItems, ($rowIndexFromCsv+2));
                            $errorFlagForInvalidCustomDate  =   1;
                        }
                    }
                }
            }

			// Check if destination id is in CSV, database & then association
            $sourceNodeIdsFromCsv = array_column($itemsFromCSV, 'node_id');
            array_push($sourceNodeIdsFromCsv, $nodeIdsFromCsv[0]);
            foreach($itemAssociationsFromCSV as $key=> $itemAssociationsToAdd) {
                $associationDestinationId = $itemAssociationsToAdd['parent/destination_node_id'];
                if(!in_array((string)$associationDestinationId, $sourceNodeIdsFromCsv)) {
                    if(!in_array((string)$associationDestinationId, $allItemsSourceIdFromTenant)) {
                        $checkAssocUpdateExists = $this->checkAssociationExists($itemAssociationsToAdd['node_id'],$itemAssociationsToAdd['parent/destination_node_id'],$itemAssociationsToAdd['case_association_type']);
                        if(empty($checkAssocUpdateExists)){
                            $notFoundDestinationNode = 1;
                            $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']));
                            array_push($errorRowForNotFoundDestinationId, ($rowIndexFromCsv + 2));
                        }
                    }
                }
            }
            $actionNodeIdsFromCsv = array_column($itemsFromCSV, 'action');
            foreach($itemAssociationsFromCSV as $key=> $itemAssociationsToAdd) {
                if(in_array($itemAssociationsToAdd['node_id'], $sourceNodeIdsFromCsv)) {
                    // Add association count if association does not exists and if it jas allowable actions
					$rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']));
                    if(in_array(strtolower($itemAssociationsToAdd['action']),['','a','add','u','update']) && !in_array(($rowIndexFromCsv + 2),$errorRowForNotFoundDestinationId)){
                        #check if assocition to be created does not have a node assocated with delete
                        $key1 = array_search($itemAssociationsToAdd['node_id'], $sourceNodeIdsFromCsv);
                        $actionOfNode = strtolower($actionNodeIdsFromCsv[$key1]);
                        if(!in_array($actionOfNode,$arrayDelete)) {
                            $checkAssocUpdateExists = $this->checkAssociationExists($itemAssociationsToAdd['node_id'],$itemAssociationsToAdd['parent/destination_node_id'],$itemAssociationsToAdd['case_association_type']);
                            if(empty($checkAssocUpdateExists))
                                $newAssociationsCount++;

                        }
                        unset($itemAssociationsFromCSV[$key]);
                    } else if(in_array(strtolower($itemAssociationsToAdd['action']),['d','delete']) && !in_array(($rowIndexFromCsv + 2),$errorRowForNotFoundDestinationId)) {
                        $deleteAssociationsCount ++;
                    }
                    #commenting this line because of this, in next foreach, these associations are not found to verify if associations exists, adding this unset in above if condition
                    // unset($itemAssociationsFromCSV[$key]);
                }
            }
            $this->newAssociationsCount = $newAssociationsCount;
            $this->deleteAssociationsCount = $deleteAssociationsCount;
            $itemAssociationsFromCSV = array_values($itemAssociationsFromCSV);
            foreach($itemAssociationsFromCSV as $itemAssociationsToAdd) {

                if(in_array(strtolower($itemAssociationsToAdd['action']),['','a','add','u','update'])) {

                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']),$errorRowForUpdateAssociations);
                    if($rowIndexFromCsv){
                        $checkAssocUpdateExists = $this->checkAssociationExists($itemAssociationsToAdd['node_id'],$itemAssociationsToAdd['parent/destination_node_id'],$itemAssociationsToAdd['case_association_type']);
                        if(empty($checkAssocUpdateExists)){
                            array_push($errorRowForUpdateAssociations, ($rowIndexFromCsv + 2));
                            $errorFlagForUpdateAssociation  =   1;
                        }
                    }
                } else if(in_array(strtolower($itemAssociationsToAdd['action']),$arrayDelete)) {
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']),$errorRowForDeleteAssociations);
                    if($rowIndexFromCsv){
                        $checkAssocDeleteExists = $this->checkAssociationExists($itemAssociationsToAdd['node_id'],$itemAssociationsToAdd['parent/destination_node_id'],$itemAssociationsToAdd['case_association_type']);
                        if(empty($checkAssocDeleteExists)){
                            array_push($errorRowForDeleteAssociations, ($rowIndexFromCsv + 2));
                            $errorFlagForDeleteAssociation  =   1;
                        }
                    }
                }
            }
        } else {
            foreach($itemsFromCSV as $itemKey => $itemsToAdd) {
                $itemsToAdd = array_change_key_case($itemsToAdd,CASE_LOWER);
                if(in_array(strtolower($itemsToAdd['action']),$arrayDelete)) {
                    $rowIndexFromCsv = $this->searchForId($itemsToAdd['node_id'],
                    $inputData, array('$'),$errorDeleteRowForItems, ['key'=>'action','value'=>$itemsToAdd['action']]);
                    array_push($errorDeleteRowForItems, ($rowIndexFromCsv + 2));
                    $errorFlagForDeleteItem  =   1;
                }
				// Code changes for Start End date & Invalid Url
                if(isset($itemsToAdd['case_status_start_date']) && isset($itemsToAdd['case_status_end_date'])){
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                    if($itemsToAdd['case_status_start_date']=="0" || $itemsToAdd['case_status_end_date'] == "0" || (!empty($itemsToAdd['case_status_start_date']) && !$this->validateDate($itemsToAdd['case_status_start_date'])) || (!empty($itemsToAdd['case_status_end_date']) && !$this->validateDate($itemsToAdd['case_status_end_date'])))
                    {
                        array_push($errorInvalidStatusDateRowForItems, ($rowIndexFromCsv + 2));
                        $errorFlagForInvalidStatusDate  =   1;
                    }
                    else if(!empty($itemsToAdd['case_status_start_date']) && !empty($itemsToAdd['case_status_end_date']) && strtotime($itemsToAdd['case_status_start_date']) > strtotime($itemsToAdd['case_status_end_date'])){
                        array_push($errorGreaterStatusDateRowForItems, ($rowIndexFromCsv + 2));
                        $errorFlagForGreaterStatusDate  =   1;
                    }
                }
                if(isset($itemsToAdd['case_official_source_url']) && !empty($itemsToAdd['case_official_source_url'])){
                    if(!filter_var($itemsToAdd['case_official_source_url'], FILTER_VALIDATE_URL)){
                        $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                        array_push($errorSourceUrlRowForItems, ($rowIndexFromCsv + 2));
                        $errorFlagForSourceUrl  =   1;
                    }
                }
            }

            $destinationNodes = array_column($itemAssociationsFromCSV, 'parent/destination_node_id');
            $sourceNodeIdsFromCsv = array_column($itemsFromCSV, 'node_id');
            array_push($sourceNodeIdsFromCsv, $nodeIdsFromCsv[0]);
            foreach($destinationNodes as $associationDestinationId) {
                if(!in_array((string)$associationDestinationId, $sourceNodeIdsFromCsv)) {
                    if(!in_array((string)$associationDestinationId, $allItemsSourceIdFromTenant)) {
                        $notFoundDestinationNode = 1;
                        $rowIndexFromCsv = $this->searchForId($associationDestinationId,
                        $inputData, array('$'), $errorRowForNotFoundDestinationId, ['key'=>'parent/destination_node_id','value'=>$associationDestinationId]);

                        array_push($errorRowForNotFoundDestinationId, ($rowIndexFromCsv + 2));
                    }
                }
            }
            $actionNodeIdsFromCsv = array_column($itemsFromCSV, 'action');
            foreach($itemAssociationsFromCSV as $key=> $itemAssociationsToAdd) {
                if(in_array($itemAssociationsToAdd['node_id'], $sourceNodeIdsFromCsv)) {
                    #check if assocition to be created does not have a node assocated with delete
                    $key1 = array_search($itemAssociationsToAdd['node_id'], $sourceNodeIdsFromCsv);
                    $actionOfNode = strtolower($actionNodeIdsFromCsv[$key1]);
                    if(!in_array($actionOfNode,$arrayDelete)) {
                        $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']));
                        if(in_array(strtolower($itemAssociationsToAdd['action']),['','a','add','u','update']) && !in_array(($rowIndexFromCsv + 2),$errorRowForNotFoundDestinationId)){
                            $newAssociationsCount++;
                        }
                        unset($itemAssociationsFromCSV[$key]);
                    }
                }
            }
            $this->newAssociationsCount = $newAssociationsCount;
            $itemAssociationsFromCSV = array_values($itemAssociationsFromCSV);
            foreach($itemAssociationsFromCSV as $itemAssociationsToAdd) {
                if(in_array(strtolower($itemAssociationsToAdd['action']),['','a','add','u','update'])) {
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']),$errorRowForUpdateAssociations);
                    array_push($errorRowForUpdateAssociations, ($rowIndexFromCsv + 2));
                    $errorFlagForUpdateAssociation  =   1;
                } else if(in_array(strtolower($itemAssociationsToAdd['action']),$arrayDelete)) {
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']),$errorRowForDeleteAssociations);
                    array_push($errorRowForDeleteAssociations, ($rowIndexFromCsv + 2));
                    $errorFlagForDeleteAssociation  =   1;
                }
            }
        }
        sort($errorUpdateRowForItems);
        sort($errorDeleteRowForItems);
        sort($errorRowForNotFoundDestinationId);
        sort($errorRowForUpdateAssociations);
        sort($errorRowForDeleteAssociations);
        sort($errorInvalidStatusDateRowForItems);
        sort($errorGreaterStatusDateRowForItems);
        sort($errorSourceUrlRowForItems);
        sort($errorInvalidCustomDateRowForItems);

        $this->errorUpdateRowForItems           =   $errorUpdateRowForItems;
        $this->errorDeleteRowForItems           =   $errorDeleteRowForItems;
        $this->errorRowForNotFoundDestinationId =   $errorRowForNotFoundDestinationId;
        $this->errorRowForUpdateAssociations    =   $errorRowForUpdateAssociations;
        $this->errorRowForDeleteAssociations    =   $errorRowForDeleteAssociations;
        $this->errorInvalidStatusDateRowForItems    =   $errorInvalidStatusDateRowForItems;
        $this->errorGreaterStatusDateRowForItems    =   $errorGreaterStatusDateRowForItems;
        $this->errorSourceUrlRowForItems    =   $errorSourceUrlRowForItems;
        $this->errorInvalidCustomDateRowForItems    =   $errorInvalidCustomDateRowForItems;
        unset($allItemsSourceIdFromTenant);
        $errorFlagArray = ['errorFlagForUpdateItem' => $errorFlagForUpdateItem, 'errorFlagForDeleteItem' => $errorFlagForDeleteItem, 'notFoundDestinationNode' => $notFoundDestinationNode, 'errorFlagForUpdateAssociation' => $errorFlagForUpdateAssociation, 'errorFlagForDeleteAssociation' => $errorFlagForDeleteAssociation, 'errorFlagForSourceUrl' => $errorFlagForSourceUrl, 'errorFlagForInvalidStatusDate' => $errorFlagForInvalidStatusDate, 'errorFlagForGreaterStatusDate' => $errorFlagForGreaterStatusDate, 'errorFlagForInvalidCustomDate' => $errorFlagForInvalidCustomDate];

        return $errorFlagArray;
    }

    /**
     * Method to validate the input data with action types
     *
     * @return void
     */
    private function validateInputDataWithActionTypes(){
        $sourceItemIdFromDb     =   [];

        $errorArray             =   [];
        $errorItemUpdateArray   =   [];
        $errorItemDeleteArray   =   [];
        $errorAssociationArray  =   [];

        $errorAssociationUpdateArray    =   [];
        $errorAssociationNotFoundArray  =   [];
        $errorItemArray        =   [];
        $errorAssociationDeleteArray = [];
        $errorItemStatusDateArray = [];
        $errorItemSourceUrlArray = [];
        $errorInvalidCustomDateArray = [];

        $organizationIdentifier     =   $this->organizationIdentifier;
        $inputData                  =   $this->inputData;
        $packageExists              =   $this->packageExists;

        $itemsFromCSV               = $this->getParsedItemsFromCsv();
        $itemAssociationsFromCSV    = $this->getParsedItemAssociationFromCsv();
        $importType                 = $this->importType;


        // $returnCollection   =   true;
        // $fieldsToReturn     =   ['source_item_id'];
        // $keyValuePairs      =   ['is_deleted'   =>  '0', 'organization_id'  =>  $organizationIdentifier];

        # TODO: Convert eloquent to DB query
        // $allItemsFromTenant =   $this->itemRepository->findByAttributesWithSpecifiedFields(
        //     $returnCollection,
        //     $fieldsToReturn,
        //     $keyValuePairs)->toArray();
        $allItemsFromTenant = DB::table('items')
                                ->select('source_item_id')
                                ->where('is_deleted', 0)
                                ->where('organization_id', $organizationIdentifier)
                                ->get()
                                ->toArray();
        $allItemsSourceIdFromTenant = array_column($allItemsFromTenant, 'source_item_id');

        unset($inputData[0]);
        /**
         * Prepare array of Items received from csv file
         */
        foreach($inputData  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }
        /**
         * Fetch item detail from db for the item_id from csv file
         */
        # TODO: Convert eloquent to DB query
        // $attributeIn        =   "source_item_id";
        // $containedInValues  =   $itemIdArray;
        // $returnCollection   =   true;
        // $fieldsToReturn     =  ["item_id", "source_item_id"];

        // $keyValuePairs = ['is_deleted'   =>  '0', 'organization_id' => $organizationIdentifier];

        // $itemCollectionFromCsv =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery($attributeIn, $containedInValues, $returnCollection, $fieldsToReturn, $keyValuePairs);

        $itemCollectionFromCsv = DB::table('items')
                                    ->select('item_id','source_item_id')
                                    ->whereIn('source_item_id', $itemIdArray)
                                    ->where('is_deleted', 0)
                                    ->where('organization_id', $organizationIdentifier)
                                    ->get();

        $itemCollectionArray =  $itemCollectionFromCsv->toArray();
        foreach($itemCollectionArray as $itemFromDb) {
            $sourceItemIdFromDb[]                           =   $itemFromDb->source_item_id;
        }

        $itemIdNotInDb = array_diff($itemIdArray, $sourceItemIdFromDb);

        # to process the validation of data based on import type
        $errorFlagArray = $this->processDataValidationForItemsAndAssociationsForImportType($importType, $allItemsSourceIdFromTenant, $itemIdNotInDb, $itemsFromCSV, $itemAssociationsFromCSV);
        unset($allItemsSourceIdFromTenant);
        foreach($errorFlagArray as $keyForError => $flag) {
            if($keyForError == 'errorFlagForUpdateItem' && $flag == 1) {
                $errorItemUpdateArray[]   =   ['Error' => 'Could not find nodes to be updated', 'Row' => implode(',', $this->errorUpdateRowForItems)];
            }

            if($keyForError == 'errorFlagForDeleteItem' && $flag == 1) {
                $errorItemDeleteArray[]   =   ['Error' => 'Could not find nodes to be deleted', 'Row' => implode(',', $this->errorDeleteRowForItems)];
            }

            if($keyForError == 'notFoundDestinationNode' && $flag == 1) {
                $errorAssociationNotFoundArray[]   =   ['Error' => 'Could not find the destination node to create association', 'Row' => implode(',', $this->errorRowForNotFoundDestinationId)];
            }

            if($keyForError == 'errorFlagForUpdateAssociation' && $flag == 1) {
                $errorAssociationUpdateArray[]   =   ['Error' => 'Could not find the source node/ association type to update association', 'Row' => implode(',', $this->errorRowForUpdateAssociations)];
            }

            if($keyForError == 'errorFlagForDeleteAssociation' && $flag == 1) {
                $errorAssociationDeleteArray[]   =   ['Error' => 'Could not find associations to be deleted', 'Row' => implode(',', $this->errorRowForDeleteAssociations)];
            }

            if($keyForError == 'errorFlagForInvalidStatusDate' && $flag == 1) {
                $errorItemStatusDateArray[]   =   ['Error' => 'Please enter valid date in format YYYY-MM-DD or YYYY/MM/DD', 'Row' => implode(',', $this->errorInvalidStatusDateRowForItems)];
            }

            if($keyForError == 'errorFlagForGreaterStatusDate' && $flag == 1) {
                $errorItemStatusDateArray[]   =   ['Error' => 'The start date is greater than the end date', 'Row' => implode(',', $this->errorGreaterStatusDateRowForItems)];
            }

            if($keyForError == 'errorFlagForSourceUrl' && $flag == 1) {
                $errorItemSourceUrlArray[]   =   ['Error' => 'The URL added is not valid', 'Row' => implode(',', $this->errorSourceUrlRowForItems)];
            }

            if($keyForError == 'errorFlagForInvalidCustomDate' && $flag == 1) {
                $errorInvalidCustomDateArray[]   =   ['Error' => 'Please enter valid date in format YYYY-MM-DD or YYYY/MM/DD', 'Row' => implode(',', $this->errorInvalidCustomDateRowForItems)];
            }

        }

        if(!empty($errorItemDeleteArray) || !empty($errorItemUpdateArray) || !empty($errorAssociationNotFoundArray) || !empty($errorItemStatusDateArray) || !empty($errorItemSourceUrlArray) || !empty($errorInvalidCustomDateArray)) {
            $errorItemArray         =   array_merge($errorAssociationNotFoundArray, array_merge($errorItemUpdateArray,$errorItemDeleteArray,$errorItemStatusDateArray,$errorItemSourceUrlArray, $errorInvalidCustomDateArray));
        }

        if(!empty($errorAssociationUpdateArray) || !empty($errorAssociationDeleteArray)) {
            $errorAssociationArray  =   array_merge($errorAssociationUpdateArray,$errorAssociationDeleteArray);
        }

        if(!empty($errorItemArray) || !empty($errorAssociationArray)){
            $errorArray     =   array_merge($errorItemArray, $errorAssociationArray);
        }

        $this->errorData    =   $errorArray;
        return !empty($errorArray) ? false : true;
    }

    /**
     * Method to search by single value
     * from associative array
     *
     * @param [string] $search_value
     * @param [array] $array
     * @param [reference] $id_path
     * @return void
     */
    private function searchForId($search_value, $array, $id_path, $rowCountArray=[], $compareValues=[]) {
        // Iterating over main array
        foreach ($array as $key1 => $val1) {

            $temp_path = $id_path;

            // Adding current key to search path
            array_push($temp_path, $key1);

            // Check if this value is an array
            // with atleast one element
            if(is_array($val1) and count($val1)) {

                // Iterating over the nested array
                foreach ($val1 as $key2 => $val2) {
                    if(trim($val2) == trim($search_value)) {

                        // Adding current key to search path
                        array_push($temp_path, $key2);
                        if(!empty($compareValues)){
                            if($val1[$compareValues['key']]!=$compareValues['value'])
                                continue;
                        }
                        if(!in_array(($temp_path[1]+2), $rowCountArray))
                            return $temp_path[1];
                    }
                }
            }

            elseif($val1 == $search_value) {
                if(!empty($compareValues)){
                    if($array[$compareValues['key']]!=$compareValues['value'])
                        continue;
                }
                if(!in_array(($temp_path[1]+2), $rowCountArray))
                    return $temp_path[1];
            }
        }

        return null;
    }

    /**
     * Method to search by conditional value
     * from associative array
     *
     * @param [type] $array
     * @param [type] $search
     * @return void
     */
    private function multiArraySearch($array, $search, $rowCount = [])
    {
        // Create the result array
        $result = 0;

        // Iterate over each array element
        foreach ($array as $key => $value)
        {

            // Iterate over each search condition
            foreach ($search as $k => $v)
            {

                // If the array element does not meet the search condition then continue to the next element
                if (!isset($value[$k]) || $value[$k] != $v)
                {
                    continue 2;
                }

            }

            // Add the array element's key to the result array
            $result = $key;

        }
        // Return the result array
        return $result;
    }

    /**
     * Method to check custom metadata by column name and type
     *
     * @param [array] $customMetadataColumns
     * @return void
     */
    private function checkCustomMetadataColumnNameAndType($customMetadataColumns) {
        $parsedCustomMetadataColumn     =   [];
        $fieldTypeValues                =   config("default_enum_setting.metadata_field_type");

        foreach($customMetadataColumns as $key => $customMetadata) {
            $metadataPart   =   explode('(', $customMetadata);
            $metadataType   =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;

            $parsedCustomMetadataColumn[$key]  =   $metadataPart[0].'('.$metadataType.')';
        }

        if(count(array_unique($parsedCustomMetadataColumn)) < count($parsedCustomMetadataColumn))
        {
            return true;
        }

        return false;
    }

    /**
     * List the different case metadata and the custom metadata
     */
    private function listCaseAndCustomMetadata() {

        $nodeType           =   '';
        $title              =   '';
        $associationType    =   '';

        $languageTitle          =   '';
        $subjectTitle           =   '';
        $subjectHierarchyCode   =   '';
        $subjectDescription     =   '';

        $conceptTitle           =   '';
        $conceptHierarchyCode   =   '';
        $conceptKeywords        =   '';
        $conceptDescription     =   '';

        $licenseTitle           =   '';
        $licenseText            =   '';
        $licenseDescription     =   '';

        $caseMetadataSet    =   [];
        $customMetadataSet  =   [];
        $arrayOfNonCaseMetadata = [];

        $metadataList           =   $this->metadataList->toArray();
        $caseMetadataListFromDb =   array_filter($metadataList, function ($var) {
            return ($var['is_custom'] == '0');
        });

        $caseMetadataListFromDb = array_column($caseMetadataListFromDb, 'internal_name');

        $columnsFromHeaders  = $this->columnFromHeaders;
        unset($columnsFromHeaders['parent/destination_node_id']);
        foreach($columnsFromHeaders as $headers) {
            if($headers == 'parent/destination_node_id')
                continue;
            if (stripos($headers, 'case') !== false) {
                $caseMetadataSet[] = $headers;
            } else if(trim(preg_replace("/[^A-Za-z\s]/", "", $headers))) {
                $customMetadataSet[] = $headers;
            }
        }

        if(sizeOf($caseMetadataSet) > 0) {
            foreach($caseMetadataSet as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(!in_array((string)$dbColumnFromCsvHeader, $caseMetadataListFromDb)) {
                    $arrayOfNonCaseMetadata[]   =   $metadata;
                } else {
                    $result = '';  //set default value

                    foreach ( $caseMetadataSet as $key2 => $value ) {
                        if (false !== stripos($value,'case_title'))   // hasstack comes before needle
                        {
                            $result .= $key2;  // concat results
                            //return $result;
                            $title = $value;
                        }

                        if(false !== stripos($value,'case_association_type')) {
                            $associationType = $value;
                        }

                        if(false !== stripos($value,'case_node_type')) {
                            $nodeType = $value;
                        }

                        if(false !== stripos($value,'case_language')) {
                            $languageTitle = $value;
                        }

                        if(false !== stripos($value,'case_subject_title')) {
                            $subjectTitle = $value;
                        }

                        if(false !== stripos($value,'case_subject_hierarchy_code')) {
                            $subjectHierarchyCode = $value;
                        }

                        if(false !== stripos($value,'case_subject_description')) {
                            $subjectDescription = $value;
                        }

                        if(false !== stripos($value,'case_concept_title')) {
                            $conceptTitle = $value;
                        }

                        if(false !== stripos($value,'case_concept_keywords')) {
                            $conceptKeywords = $value;
                        }

                        if(false !== stripos($value,'case_concept_hierarchy_code')) {
                            $conceptHierarchyCode = $value;
                        }

                        if(false !== stripos($value,'case_concept_description')) {
                            $conceptDescription = $value;
                        }

                        if(false !== stripos($value,'case_license_title')) {
                            $licenseTitle = $value;
                        }

                        if(false !== stripos($value,'case_license_text')) {
                            $licenseText = $value;
                        }

                        if(false !== stripos($value,'case_license_description')) {
                            $licenseDescription = $value;
                        }
                    }

                    if ($result == '') {
                        return false;
                    }else {
                        $this->title                    =   $title;
                        $this->nodeType                 =   $nodeType;
                        $this->associationType          =   $associationType;
                        $this->languageTitle            =   $languageTitle;

                        $this->subjectTitle             =   $subjectTitle;
                        $this->subjectHierarchyCode     =   $subjectHierarchyCode;
                        $this->subjectDescription       =   $subjectDescription;

                        $this->conceptTitle             =   $conceptTitle;
                        $this->conceptKeywords          =   $conceptKeywords;
                        $this->conceptHierarchyCode     =   $conceptHierarchyCode;
                        $this->conceptDescription       =   $conceptDescription;

                        $this->licenseTitle             =   $licenseTitle;
                        $this->licenseText              =   $licenseText;
                        $this->licenseDescription       =   $licenseDescription;

                        $this->caseMetadataSet = $caseMetadataSet;
                        if (($key = array_search('node_id', $customMetadataSet)) !== false) {
                            unset($customMetadataSet[$key]);
                        }
                        $this->customMetadataSet = $customMetadataSet;
                    }
                }
            }
        }
        else {
            return 3;
        }

        if (($key = array_search($nodeType, $arrayOfNonCaseMetadata)) !== false) {
            unset($arrayOfNonCaseMetadata[$key]);
        }

        if (($key = array_search($associationType, $arrayOfNonCaseMetadata)) !== false) {
            unset($arrayOfNonCaseMetadata[$key]);
        }
        
        if(sizeOf($arrayOfNonCaseMetadata) > 0){
            return $arrayOfNonCaseMetadata;
        } else {
            return true;
        }
    }

    /**
     * Method to update the document in db or
     * create a new document in db
     *
     * @return void
     */
    private function updateDocumentData() {
        $document               =   [];
        $metadataForDocument    =   [];

        $count                      =   $this->count;
        $blankCount                 =   $this->blankCount;
        $newDocCount                =   $this->newDocCount;
        $inputData                  =   $this->inputData;
        $nodeType                   =   $this->nodeType;
        $languageTitle            =   $this->languageTitle;
        $subjectTitle             =   $this->subjectTitle;
        $subjectHierarchyCode     =   $this->subjectHierarchyCode;
        $subjectDescription       =   $this->subjectDescription;

        $licenseTitle           =   $this->licenseTitle;
        $licenseText            =   $this->licenseText;
        $licenseDescription     =   $this->licenseDescription;

        $organizationIdentifier     =   $this->organizationIdentifier;
        $metadataList               =   $this->metadataList;
        $caseMetadata               =   $this->caseMetadataSet;

        foreach($metadataList as $metadataFromDb){
            $internalName          =  $metadataFromDb->toArray()['internal_name'];
            if(($metadataFromDb->toArray()['is_document'] == 1 || $metadataFromDb->toArray()['is_document'] == 2) && $metadataFromDb->toArray()['is_custom'] == 0 ){
                $metadataForDocument[] = $internalName;
            }
        }
        if (($key = array_search('subject_title', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('subject_hierarchy_code', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('subject_description', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }

        if (($key = array_search('license_title', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('license_text', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('license_description', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }

        if (($key = array_search('language', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }

        $this->documentIdentifier = trim($inputData[0]['node_id']);

        $returnCollection   =   false;
        $fieldsToReturn     =   array_merge(['document_id', 'source_document_id', 'node_type_id', 'language_id', 'license_id'], $metadataForDocument);
        $keyValuePairs      =   ['source_document_id'  => trim($inputData[0]['node_id']), 'is_deleted'   =>  '0', 'organization_id'  =>  $organizationIdentifier];
        $relations          =   ['nodeType', 'subjects', 'license', 'language'];

        $documentNode   =   $this->documentRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        $documentIdFromDb[trim($inputData[0]['node_id'])] = !empty($documentNode->document_id) ? $documentNode->document_id : '';

        $this->itemIdFromDb = $documentIdFromDb;
        if(!empty($subjectTitle)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $subjectTitle)[1]));
        }
        if(!empty($subjectHierarchyCode)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $subjectHierarchyCode)[1]));
        }
        if(!empty($subjectDescription)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $subjectDescription)[1]));
        }


        if(!empty($licenseTitle)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $licenseTitle)[1]));
        }
        if(!empty($licenseText)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $licenseText)[1]));
        }
        if(!empty($licenseDescription)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $licenseDescription)[1]));
        }

        if(!empty($languageTitle)) {
            array_push($metadataForDocument, strtolower(preg_split("/case_/i", $languageTitle)[1]));
        }
        array_push($metadataForDocument, 'node_type');
        if(!empty($documentNode->document_id)) {
            foreach($caseMetadata as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(in_array((string)$dbColumnFromCsvHeader, $metadataForDocument)) {
                    if(isset($documentNode->$dbColumnFromCsvHeader) && is_string($documentNode->$dbColumnFromCsvHeader))
                        $documentNode->$dbColumnFromCsvHeader = str_replace(array("\t", "\r"), '', $documentNode->$dbColumnFromCsvHeader);

                    // If start date & end date has value 0000, change it to blank
					if(isset($documentNode->$dbColumnFromCsvHeader) && $documentNode->$dbColumnFromCsvHeader == "0000-00-00 00:00:00" && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'))
                            $documentNode->$dbColumnFromCsvHeader = "";
                    if(!empty(trim($inputData[0][$metadata]))){
                        if(!empty($documentNode->$dbColumnFromCsvHeader) && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date')) {
                            $documentNode->$dbColumnFromCsvHeader = $this->formatDateTimeToDateOnly($documentNode->$dbColumnFromCsvHeader);
                        }
                        if($dbColumnFromCsvHeader == 'language' && isset($documentNode->language->name))
                            $documentNode->language = $documentNode->language->name;
                        if($dbColumnFromCsvHeader == 'node_type') {
                            if(isset($documentNode->nodeType->title))
                                $documentNode->nodeType->title = str_replace(array("\t", "\r"), '', trim($documentNode->nodeType->title));

                            if($documentNode->nodeType->title !== null && trim($documentNode->nodeType->title) != trim($inputData[0][$metadata])) {
                                $document[$documentNode->document_id]['item_name'] = $inputData[0][$metadata];
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => $documentNode->nodeType->title,
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $count++;
                                $this->count    =   $count;
                            }
                        } elseif($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'){
                            $inputData[0][$metadata] = trim($inputData[0][$metadata]);
                            if($inputData[0][$metadata]!="0" && (!empty($inputData[0][$metadata]) && $this->validateDate($inputData[0][$metadata])) && trim($documentNode->$dbColumnFromCsvHeader) != trim($inputData[0][$metadata])){
                                $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => ($documentNode->$dbColumnFromCsvHeader ? $documentNode->$dbColumnFromCsvHeader: ''),
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $count++;
                                $this->count    =   $count;
                            }
                        } elseif($dbColumnFromCsvHeader == 'subject_title' || $dbColumnFromCsvHeader == 'subject_hierarchy_code' || $dbColumnFromCsvHeader == 'subject_description') {
                            $header = str_replace('subject_','',$dbColumnFromCsvHeader);
                            if(isset($documentNode->subjects) && !empty($documentNode->subjects->toArray())) {
                                foreach ($documentNode->subjects as $subjects) {
                                    $subjects->$header = str_replace(array("\t", "\r"), '', $subjects->$header);
                                    if(trim($subjects->$header) != trim($inputData[0][$metadata])) {
                                        $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                        $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                        $document[$documentNode->document_id]['metadata_fields'][] = [
                                            'name' => $dbColumnFromCsvHeader,
                                            'old_value' => $subjects->$header,
                                            'new_value' => trim($inputData[0][$metadata])
                                        ];

                                        $count++;
                                        $this->count    =   $count;
                                    }
                                }
                            } else {
                                $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => '',
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $count++;
                                $this->count    =   $count;
                            }
                        } elseif($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                            $header = $dbColumnFromCsvHeader;
                            if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_description')
                                $header = str_replace('license_','',$dbColumnFromCsvHeader);
                            if(isset($documentNode->license->$header)){
                                $documentNode->license->$header = str_replace(array("\t", "\r"), '', $documentNode->license->$header);
                                if(trim($documentNode->license->$header) != trim($inputData[0][$metadata])) {
                                    $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                    $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                    $document[$documentNode->document_id]['metadata_fields'][] = [
                                        'name' => $dbColumnFromCsvHeader,
                                        'old_value' => $documentNode->license->$header,
                                        'new_value' => trim($inputData[0][$metadata])
                                    ];

                                    $count++;
                                    $this->count    =   $count;
                                }
                            } else {
                                $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => '',
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $count++;
                                $this->count    =   $count;
                            }
                        } else {
                            if(trim($documentNode->$dbColumnFromCsvHeader) != trim($inputData[0][$metadata])){
                                $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => ($documentNode->$dbColumnFromCsvHeader ? $documentNode->$dbColumnFromCsvHeader : ''),
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $count++;
                                $this->count    =   $count;
                            }
                        }
                    } else {
                        if(isset($documentNode->$dbColumnFromCsvHeader) && $documentNode->$dbColumnFromCsvHeader == "0000-00-00 00:00:00" && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'))
                            $documentNode->$dbColumnFromCsvHeader = "";
                        if(!empty($documentNode->$dbColumnFromCsvHeader) && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date')) {
                            $documentNode->$dbColumnFromCsvHeader = $this->formatDateTimeToDateOnly($documentNode->$dbColumnFromCsvHeader);
                        }

                        if($dbColumnFromCsvHeader == 'language' && isset($documentNode->language->name))
                            $documentNode->language = $documentNode->language->name;
                        if($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'){
                            $inputData[0][$metadata] = trim($inputData[0][$metadata]);
                            if($inputData[0][$metadata]!="0" && trim($documentNode->$dbColumnFromCsvHeader) != trim($inputData[0][$metadata])){
                                $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => ($documentNode->$dbColumnFromCsvHeader ? $documentNode->$dbColumnFromCsvHeader: ''),
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $blankCount++;
                                $this->blankCount    =   $blankCount;
                            }
                        } elseif($dbColumnFromCsvHeader == 'subject_title' || $dbColumnFromCsvHeader == 'subject_hierarchy_code' || $dbColumnFromCsvHeader == 'subject_description') {
                            $header = str_replace('subject_','',$dbColumnFromCsvHeader);
                            if(isset($documentNode->subjects) && !empty($documentNode->subjects->toArray())){
                                foreach ($documentNode->subjects as $subjects) {
                                    $subjects->$header = str_replace(array("\t", "\r"), '', $subjects->$header);
                                    if(trim($subjects->$header) != trim($inputData[0][$metadata])) {
                                        $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                        $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                        $document[$documentNode->document_id]['metadata_fields'][] = [
                                            'name' => $dbColumnFromCsvHeader,
                                            'old_value' => $subjects->$header,
                                            'new_value' => trim($inputData[0][$metadata])
                                        ];

                                        $blankCount++;
                                        $this->blankCount    =   $blankCount;
                                    }
                                }
                            }
                        }
                        elseif($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                            $header = $dbColumnFromCsvHeader;
                            if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_description')
                                $header = str_replace('license_','',$dbColumnFromCsvHeader);
                            if(isset($documentNode->license->$header)){
                                $documentNode->license->$header = str_replace(array("\t", "\r"), '', $documentNode->license->$header);
                                if(trim($documentNode->license->$header) != trim($inputData[0][$metadata])) {
                                    $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                    $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                    $document[$documentNode->document_id]['metadata_fields'][] = [
                                        'name' => $dbColumnFromCsvHeader,
                                        'old_value' => $documentNode->license->$header,
                                        'new_value' => trim($inputData[0][$metadata])
                                    ];

                                    $blankCount++;
                                    $this->blankCount    =   $blankCount;
                                }
                            }
                        } else {
                            if(trim($documentNode->$dbColumnFromCsvHeader) != trim($inputData[0][$metadata])) {
                                $document[$documentNode->document_id]['item_name'] = trim($inputData[0]['case_title']);
                                $document[$documentNode->document_id]['node_id'] = $documentNode->source_document_id;
                                $document[$documentNode->document_id]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => isset($documentNode->$dbColumnFromCsvHeader) ? $documentNode->$dbColumnFromCsvHeader : '',
                                    'new_value' => trim($inputData[0][$metadata])
                                ];

                                $blankCount++;
                                $this->blankCount    =   $blankCount;
                            }
                        }
                    }
                }
            }
            $this->changedDocument = $document;
        } else {
            $newDocCount++;
            $this->newDocCount    =   $newDocCount;
        }
    }

    /**
     * Method to update the item in db or
     * create new item in db
     *
     * @return void
     */
    private function createItemIdForNewItemsToBeAdded() {
        $arrayOfCreatedItemId   =   [];
        $getNewItemArray    =   $this->newItemArray;
        if(count($getNewItemArray) > 0) {
            foreach($getNewItemArray as $newItem) {
                $arrayOfCreatedItemId[]   =   [$newItem['node_id']    =>  $this->createUniversalUniqueIdentifier()];
            }
        }

        $this->newItemIdArray = $arrayOfCreatedItemId;
    }

    /**
     * Method to search the node_id of the selected item's parent_node_id
     * in recursive array
     *
     * @param array $array
     * @param string $key
     * @param string $value
     * @return void
     */
    private function searchForParentNode(array $array, string $key, string $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->searchForParentNode($subarray, $key, $value));
            }
        }

        return $results;
    }

    private function createNewNodeTypeAndMapWithMetadata($nodeTypeTitle, $organizationIdentifier) {
        $nodeTypeId = $this->createUniversalUniqueIdentifier();
        return $nodeTypeId;
    }

    /**
     * Method to update existing item in db or
     * to create new item
     *
     * @return void
     */
    private function updateItemData() {
        $key                    =   1;
        $nodeType               =   $this->nodeType;

        $conceptTitle           =   $this->conceptTitle;
        $conceptKeywords        =   $this->conceptKeywords;
        $conceptHierarchyCode   =   $this->conceptHierarchyCode;
        $conceptDescription     =   $this->conceptDescription;

        $licenseTitle           =   $this->licenseTitle;
        $licenseText            =   $this->licenseText;
        $licenseDescription     =   $this->licenseDescription;

        $languageTitle          =   $this->languageTitle;
        $blankCount             =   $this->blankCount;
        $newNodeCount           =   $this->newNodeCount;
        $inputData              =   $this->inputData;
        $packageExists          =   $this->packageExists;
        $organizationIdentifier =   $this->organizationIdentifier;
        $documentIdentifier     =   $this->createdDocumentIdentifier;

        $arrayOfDocumentNodeTypeId  =   $this->nodeTypeIdArray;
        $arrayOfItemNodeTypeId      =   [];

        $itemUpdated                =   0;
        $parentUpdated              =   0;
        $nodeTypeUpdated            =   0;
        $importType                 =   $this->importType;

        unset($inputData[0]);
        $newReset               =   [];
        $metadataIdArray        =   [];
        $newItemArray           =   [];
        $itemsToSave            =   [];
        $updatedItemArray       =   [];
        $itemChangedArray       =   [];
        $itemArray              =   [];
        $metadataForItem        =   [];
        $sourceItemIdFromDb     =   [];
        $itemIdFromDb           =   [];
        $itemsFromCsv           = $this->getParsedItemsFromCsv();
        $userId                 =   $this->userId;
        $currentDateTime        =   $this->createDateTime();

        $metadataList           =   $this->metadataList;
        $caseMetadata           =   $this->caseMetadataSet;
        $newAssociationsCount   =   $this->newAssociationsCount;

        foreach($metadataList as $metadataFromDb){
            $metadataFromDbArr = $metadataFromDb->toArray();
            $internalName          =  $metadataFromDbArr['internal_name'];
            if($metadataFromDbArr['is_document'] != 1 && $metadataFromDbArr['is_custom'] == 0){
                if(strpos($internalName, 'pacing') !== FALSE ) {

                } else {
                    $metadataForItem[] = $internalName;
                }
            }
        }

        if (($key = array_search('concept_title', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('concept_hierarchy_code', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('concept_keywords', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('concept_description', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }

        if (($key = array_search('license_title', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('license_text', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('license_description', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }

        if (($key = array_search('language', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }

        /**
         * Prepare array of Items received from csv file
         */
        foreach($inputData  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }

        /**
         * Fetch item detail from db for the item_id from csv file
         */
        $attributeIn        =   "source_item_id";
        $containedInValues  =   $itemIdArray;
        $returnCollection   =   true;
        $fieldsToReturn     =   array_merge(["item_id", "source_item_id", "parent_id", "node_type_id", "license_id", "concept_id", "language_id", "updated_at"], $metadataForItem);

        $relations =    ['nodeType','license','concept','language'];

        $keyValuePairs = ['is_deleted'   =>  '0', 'organization_id' => $organizationIdentifier];

        $returnPaginatedData    = false;
        $paginationFilters      = ['limit' => 10, 'offset' => 0];
        $returnSortedData       = false;
        $sortfilters            = ['orderBy' => 'list_enumeration', 'sorting' => 'desc'];
        $operator               = 'AND';

        $itemCollectionFromCsv =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery($attributeIn, $containedInValues, $returnCollection, $fieldsToReturn, $keyValuePairs, $returnPaginatedData, $paginationFilters, $returnSortedData, $sortfilters, $operator, $relations);

        $itemCollectionArray =  $itemCollectionFromCsv->toArray();

        foreach($itemCollectionArray as $itemFromDb) {
            $sourceItemIdFromDb[]                           =   $itemFromDb['source_item_id'];
            $itemIdFromDb[$itemFromDb['source_item_id']]    =   $itemFromDb['item_id'];
        }


        $idSetFromDb    =   $this->itemIdFromDb + $itemIdFromDb;
        $this->itemIdFromDb = $idSetFromDb;

        $itemIdNotInDb = ($importType == 4 ) ? array_diff($itemIdArray, $sourceItemIdFromDb) : $itemIdArray;
        array_push($metadataForItem, 'node_type');

        if(!empty($conceptTitle)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptTitle)[1]));
        }
        if(!empty($conceptKeywords)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptKeywords)[1]));
        }
        if(!empty($conceptHierarchyCode)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptHierarchyCode)[1]));
        }
        if(!empty($conceptDescription)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptDescription)[1]));
        }

        if(!empty($licenseTitle)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $licenseTitle)[1]));
        }
        if(!empty($licenseText)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $licenseText)[1]));
        }
        if(!empty($licenseDescription)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $licenseDescription)[1]));
        }

        if(!empty($languageTitle)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $languageTitle)[1]));
        }
       if($importType == 4 && count($sourceItemIdFromDb) > 0) {
            foreach($caseMetadata as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                    //Create newReset Array with value from csv
                    foreach($inputData as $item) {
                        if(in_array(trim($item['node_id']), $sourceItemIdFromDb) == true && empty($item['association_type']) && !empty($item['case_full_statement']) && in_array(strtolower($item['action']),['','a','add','u','update'])) {
                            $newReset[trim($item['node_id'])]['node_id'] = trim($item['node_id']);
                            $newReset[trim($item['node_id'])]['parent/destination_node_id'] = trim($item['parent/destination_node_id']);
                            $newReset[trim($item['node_id'])][$metadata]        = str_replace(array("\t", "\r"), '', trim($item[$metadata]));
                            //$newReset[trim($item['node_id'])]['node_type']      = trim($item['node_type']);
                        }
                    }

                    //Create itemArray with value from db
                    foreach($itemCollectionFromCsv as $key => $items) {
                        $itemArray[$key]['node_id']                 = !empty($items->source_item_id) ? $items->source_item_id : "";
                            $itemArray[$key]['parent/destination_node_id'] = !empty($items->parent_id) ? $items->parent_id  : "";


                            if($dbColumnFromCsvHeader == 'title') {
                                $itemArray[$key][$dbColumnFromCsvHeader]    = $items->full_statement;
                            } else if($dbColumnFromCsvHeader == 'language') {
                                $itemArray[$key]['language']               = !empty($items->language_id) ? $items->language->name : "";

                            } else if($dbColumnFromCsvHeader == 'concept_title' || $dbColumnFromCsvHeader == 'concept_hierarchy_code' || $dbColumnFromCsvHeader == 'concept_keywords' || $dbColumnFromCsvHeader == 'concept_description') {
                                $itemArray[$key]['concept_title']               = !empty($items->concept_id) ? $items->concept->title : "";
                                $itemArray[$key]['concept_hierarchy_code']               = !empty($items->concept_id) ? $items->concept->hierarchy_code : "";
                                $itemArray[$key]['concept_keywords']               = !empty($items->concept_id) ? $items->concept->keywords : "";
                                $itemArray[$key]['concept_description']               = !empty($items->concept_id) ? $items->concept->description : "";
                            } else if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                $itemArray[$key]['license_title']               = !empty($items->license_id) ? $items->license->title : "";
                                $itemArray[$key]['license_text']               = !empty($items->license_id) ? $items->license->license_text : "";
                                $itemArray[$key]['license_description']               = !empty($items->license_id) ? $items->license->description : "";
                            } else {
                                $itemArray[$key][$dbColumnFromCsvHeader]    = !empty($items->$dbColumnFromCsvHeader) ?  $items->$dbColumnFromCsvHeader : "";
                            }

                            $itemArray[$key]['node_type']               = !empty($items->nodeType->title) ? $items->nodeType->title : "";
                    }
                }
            }
            $this->itemCollectionFromDb = $itemArray;

            $oldReset = [];
            $toAdd    = [];
            foreach( $itemArray as $items ) {
                $oldReset[$items['node_id']] = $items;
            }
            foreach( $newReset as $key => $val ) {
                if( isset( $oldReset[$key] ) ) {
                    $toAdd[$key] = $val;
                }
            }
            $oldReset = $this->trim_array_spaces($oldReset);

            foreach($oldReset as $itemId => $data) {
                if(!empty($toAdd[$itemId]['parent/destination_node_id'])) {
                    $checkAssocUpdateExists = $this->checkAssociationExists($toAdd[$itemId]['node_id'],$toAdd[$itemId]['parent/destination_node_id'],'ischildof');
                    if(empty($checkAssocUpdateExists) && trim($toAdd[$itemId]['parent/destination_node_id']) !== $data['parent/destination_node_id']) {
                        $itemChangedArray[$itemId]['item_name'] = trim($toAdd[$itemId]['case_full_statement']).(isset($toAdd[$itemId]['case_human_coding_scheme']) ? ' '.trim($toAdd[$itemId]['case_human_coding_scheme']) : '');
                        $itemChangedArray[$itemId]['node_id'] = $itemId;
                        $itemChangedArray[$itemId]['metadata_fields'][] = [
                            'name' => 'parent_id',
                            'old_value' => $data['parent/destination_node_id'],
                            'new_value' => trim($toAdd[$itemId]['parent/destination_node_id'])
                        ];

                        $parentUpdated   =   1;

                    } else {
                        $parentUpdated   =   0;
                    }
                } else {
                    $blankCount++;
                    $this->blankCount    =   $blankCount;
                }
                if(isset($toAdd[$itemId][$nodeType])){
                    # TODO: Eloquent to db query
                // $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $toAdd[$itemId][$nodeType], 'is_deleted'   => '0'];
                // $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);

                $nodeTypeExist = DB::table('node_types')
                                    ->select('node_type_id')
                                    ->where('organization_id', $organizationIdentifier)
                                    ->where('title', $toAdd[$itemId][$nodeType])
                                    ->where('is_deleted', 0)
                                    ->count();

                // if(count($nodeTypeExist) == 0) {
                if($nodeTypeExist == 0) {

                    $nodeTypeId =    $this->createNewNodeTypeAndMapWithMetadata($toAdd[$itemId][$nodeType], $organizationIdentifier);

                    $arrayOfItemNodeTypeId[] = [$toAdd[$itemId][$nodeType] => $nodeTypeId];
                }

                if(!empty($toAdd[$itemId][$nodeType])) {
                    if($toAdd[$itemId][$nodeType] !== $data['node_type']) {
                        $nodeTypeUpdated++;
                    } else {
                        $nodeTypeUpdated    =   0;
                    }
                } else {
                    $nodeTypeUpdated    =   0;
                }
                }

                foreach($caseMetadata as $metadata) {
                    $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                    if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem)) {
                        if($data[$dbColumnFromCsvHeader] == "0000-00-00 00:00:00" && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'))
                            $data[$dbColumnFromCsvHeader] = "";
                        if(!empty($toAdd[$itemId][$metadata])){
                            if($data[$dbColumnFromCsvHeader]!=null && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date')) {
                                $data[$dbColumnFromCsvHeader] = $this->formatDateTimeToDateOnly($data[$dbColumnFromCsvHeader]);
                            }

                            if($toAdd[$itemId][$metadata] != $data[$dbColumnFromCsvHeader]) {

                                if($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'){
                                    $toAdd[$itemId][$metadata] = trim($toAdd[$itemId][$metadata]);
                                    if($toAdd[$itemId][$metadata]!="0" && (!empty($toAdd[$itemId][$metadata]) && $this->validateDate($toAdd[$itemId][$metadata]))){
                                        $itemChangedArray[$itemId]['item_name'] = trim($toAdd[$itemId]['case_full_statement']).(isset($toAdd[$itemId]['case_human_coding_scheme']) ? ' '.trim($toAdd[$itemId]['case_human_coding_scheme']) : '');
                                        $itemChangedArray[$itemId]['node_id'] = $itemId;
                                        $itemChangedArray[$itemId]['metadata_fields'][] = [
                                            'name' => $dbColumnFromCsvHeader,
                                            'old_value' => $data[$dbColumnFromCsvHeader],
                                            'new_value' => trim($toAdd[$itemId][$metadata])
                                        ];
                                        $itemUpdated++;
                                    }
                                } else {
                                    $itemChangedArray[$itemId]['item_name'] = trim($toAdd[$itemId]['case_full_statement']).(isset($toAdd[$itemId]['case_human_coding_scheme']) ? ' '.trim($toAdd[$itemId]['case_human_coding_scheme']) : '');
                                    $itemChangedArray[$itemId]['node_id'] = $itemId;
                                    $itemChangedArray[$itemId]['metadata_fields'][] = [
                                        'name' => $dbColumnFromCsvHeader,
                                        'old_value' => $data[$dbColumnFromCsvHeader],
                                        'new_value' => trim($toAdd[$itemId][$metadata])
                                    ];
                                    $itemUpdated++;
                                }
                            }
                        } else {
                            if(isset($toAdd[$itemId][$metadata]) && $toAdd[$itemId][$metadata] !== $data[$dbColumnFromCsvHeader]) {
                                $itemChangedArray[$itemId]['item_name'] = trim($toAdd[$itemId]['case_full_statement']).(isset($toAdd[$itemId]['case_human_coding_scheme']) ? ' '.trim($toAdd[$itemId]['case_human_coding_scheme']) : '');
                                $itemChangedArray[$itemId]['node_id'] = $itemId;
                                $itemChangedArray[$itemId]['metadata_fields'][] = [
                                    'name' => $dbColumnFromCsvHeader,
                                    'old_value' => $data[$dbColumnFromCsvHeader],
                                    'new_value' => trim($toAdd[$itemId][$metadata])
                                ];
                                $itemUpdated++;
                            }
                            if($data[$dbColumnFromCsvHeader] != null) {
                                $blankCount++;
                                $this->blankCount    =   $blankCount;
                            }
                        }
                    }
                }
                /* if(($parentUpdated == 1) || ($itemUpdated > 0) || ($nodeTypeUpdated > 0)){
                    $updatedItemArray[] =   ['item_id'  => $itemId];
                } */
            }
            $this->changedItem = $itemChangedArray;
            $this->updatedItemArray = array_keys($itemChangedArray);
        }

        if(count($itemIdNotInDb) > 0) {
            foreach($itemsFromCsv as $keyForNewItem => $item) {
                if(in_array(trim($item['node_id']), $itemIdNotInDb) && in_array(strtolower($item['action']),['','a','add','u','update'])) {
                    if($importType=="4" && in_array(strtolower($item['action']),['u','update']))
                        continue;
                    $arrayOfCreatedItemId[]   =   $this->createUniversalUniqueIdentifier();
                    $key = array_search (trim($item['parent/destination_node_id']), $itemIdArray);
                    $arrayForParentIdIndices[]  =   $key;

                    $newItemArray[$keyForNewItem]['node_id']  =  trim($item['node_id']);
                    $newItemArray[$keyForNewItem]['parent/destination_node_id'] = trim($item['parent/destination_node_id']);
                    //$newItemArray[$keyForNewItem]['node_type']   = trim($item['node_type']);

                    foreach($caseMetadata as $metadata){
                        $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                        if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem)) {
                            $newItemArray[$keyForNewItem][$dbColumnFromCsvHeader] = trim($item[$metadata]);
                        }
                    }
                }
            }

            $this->newItemArray = array_values($newItemArray);
            $this->createItemIdForNewItemsToBeAdded();

            $keyForItem         =   0;
            $nodeTypeIdForItem  =   '';
            $caseAssociationsToSave = []; //caseAssociationsToSave initialized
            foreach($newItemArray as $key => $itemToBeCreated) {
                $item_id            =   $arrayOfCreatedItemId[$keyForItem];
                $nodeType           =   isset($itemToBeCreated[$nodeType]) ? $itemToBeCreated[$nodeType] : 'Default';

                // $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $nodeType, 'is_deleted'   =>  '0' ];
                // $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);

                $nodeTypeExist = DB::table('node_types')
                                    ->select('node_type_id')
                                    ->where('organization_id', $organizationIdentifier)
                                    ->where('title', $nodeType)
                                    ->where('is_deleted', 0)
                                    ->count();

                // if(count($nodeTypeExist) == 0) {
                if($nodeTypeExist == 0) {
                    $nodeTypeId         =   $this->createNewNodeTypeAndMapWithMetadata($nodeType, $organizationIdentifier);
                    $arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];
                }


                $itemsToSave[$key]= [
                    "item_id"                   => $item_id,
                ];

                $itemAssociationID = $this->createUniversalUniqueIdentifier();
                $caseAssociationsToSave[] = [
                    "item_association_id" => $itemAssociationID,
                    "association_type" => 1,
                    "document_id" => $documentIdentifier,
                    "association_group_id" => '',
                ];

                $keyForItem++;
                $newNodeCount++;
                $this->newNodeCount             =   $newNodeCount;
            }
            $newAssociationsCount       =   $newAssociationsCount + sizeOf($caseAssociationsToSave);
            $this->newAssociationsCount =   $newAssociationsCount;
            $this->createdItemArray = $itemsToSave;
        }

        $this->nodeTypeIdArray = $arrayOfItemNodeTypeId;
    }

    private function getCustomMetadataDetailForDocumentAndItemFromCSV($inputData) {
        $organizationIdentifier =   $this->organizationIdentifier;
        
        $attributes     =   ['source_document_id' => trim($inputData[0]['node_id'])];
        $getDocumentIdentifier  =   $this->documentRepository->findByAttributes($attributes)->toArray();
        
        $documentDetail = $this->documentRepository->getDocumentDetails($getDocumentIdentifier[0]['document_id']);

        if(!empty($documentDetail)) {
            $documentDetail = $documentDetail->toArray();
        }
        
        $customMetadataOfItemsFromCsv[$documentDetail['source_document_id']]   =   [];
        if(isset($customMetadataOfItemsFromCsv[$documentDetail['source_document_id']]) && sizeOf($documentDetail['custom_metadata']) > 0){
            $customMetadataOfItemsFromCsv[$documentDetail['source_document_id']] = $documentDetail['custom_metadata'];//!empty($documentDetail['custom_metadata']) ? $documentDetail['custom_metadata'] : [];  
        }

        unset($inputData[0]);
        $idsFromCsv             =  array_column($inputData, 'node_id');

        $attributeIn            =   'source_item_id';
        $containedInValues      =   $idsFromCsv;
        $returnCollection       =   true;
        $fieldsToReturn         =   ['*'];
        $keyValuePairs          =   ['is_deleted' => 0, 'organization_id' => $organizationIdentifier];
        $returnPaginatedData    =   false;
        $paginationFilters      =   ['limit' => 10, 'offset' => 0];
        $returnSortedData       =   false;
        $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator               =   'AND';
        $relations              =   ['customMetadata'];

        $itemDetail = $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn,
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator,
            $relations );

        if(!empty($itemDetail)) {
            $itemDetail = $itemDetail->toArray();
            foreach($itemDetail as $item) {
                $customMetadataOfItemsFromCsv[$item['source_item_id']] = $item['custom_metadata'];
            }
        }

        return $customMetadataOfItemsFromCsv;
    }

    /**
     * Update metadata from csv
     */
    private function updateMetadataData() {
        $nodeTypeFromCsv                =   $this->nodeType;
        $title                          =   $this->title;
        $packageExists                  =   $this->packageExists;
        $newNodeCount                   =   $this->newNodeCount;
        $updatedMetadataCount           =   $this->updatedMetadataCount;
        $blankMetadataCount             =   $this->blankMetadataCount;
        $newAdditionalMetadataCount     =   $this->newAdditionalMetadataCount;
        $metadataListForATenant         =   $this->metadataListForATenant;

        $importType             =   $this->importType;

        $documentIdentifier     =   $this->createdDocumentIdentifier;
        $organizationIdentifier =   $this->organizationIdentifier;
        $columnsFromHeaders     =   $this->customMetadataSet;//$this->columnFromHeaders;

        $itemIdFromDBSet        =   $this->itemIdFromDb;
        $nodeTypeIdArray        =   $this->nodeTypeIdArray;
        $inputData              =   $this->inputData;

        $fieldTypeValues                =   config("default_enum_setting.metadata_field_type");
        $arrayOfCreatedItemId           =   [];
        $formattedDataFromCsv           =   [];
        $customMetadataOfItemsFromCsv   =   [];
        $metadataListOfNodeTypeArray    =   [];
        $customMetadataType             =   [];
        $nodeIdArray                    =   [];
        $formattedMetadataFromDb        =   [];
        $newCreatedItemArray            =   [];
        $arrayOfItemIdFromDb            =   [];
        $saveNodeType                   =   [];
        $itemIdArray                    =   [];

        $changedMetadata                =   [];

        $nodeIdArray    =   array_map('trim', array_column($inputData, 'node_id'));
        $nodeTypeArray  =   array_map('trim', array_column($inputData, $nodeTypeFromCsv));

        $itemCollectionArray        =   $this->itemCollectionFromDb;

        if(!empty($itemCollectionArray)){
            $arrayOfItemIdFromDb =   !empty($itemCollectionArray) ? array_column($itemCollectionArray, 'node_id') : [];
            array_unshift($arrayOfItemIdFromDb, trim($inputData[0]['node_id']));
        }

        if($importType == 4) {

            $customMetadataOfItemsFromCsv                       =   $this->getCustomMetadataDetailForDocumentAndItemFromCSV($inputData);
            $arrayOfCreatedItemId                               =   $this->newItemIdArray;
            $itemFromCsv                                        =   $this->newItemArray;
            if($arrayOfCreatedItemId !== null){
                foreach($arrayOfCreatedItemId as $key => $createdItem) {
                    $itemIdArray[]    =   $createdItem[$itemFromCsv[$key]['node_id']];
                }
            }

            foreach($inputData as $dataFromCsv) {
                if(!empty($dataFromCsv['case_full_statement']) || $dataFromCsv[$nodeTypeFromCsv] == 'Document')
                    $formattedDataFromCsv[trim($dataFromCsv['node_id'])] = $dataFromCsv;
            }

            if($arrayOfCreatedItemId !== null){
                foreach($arrayOfCreatedItemId as $newItem){
                    $newCreatedItemArray[array_keys($newItem)[0]]    =   $newItem[array_keys($newItem)[0]];
                }
            }
        } else {
            if($packageExists == 1) {
                $arrayOfCreatedItemId   =   $this->createdItemArray;
                foreach($arrayOfCreatedItemId as $createdItem) {
                    $itemIdArray[]    =   $createdItem['item_id'];
                }
            } else {
                $arrayOfCreatedItemId       =   $this->newItemIdArray;
                $itemFromCsv = $this->newItemArray;
                foreach($arrayOfCreatedItemId as $key => $createdItem) {
                    $itemIdArray[]    =   $createdItem[$itemFromCsv[$key]['node_id']];
                }
            }
            $formattedDataFromCsv[$documentIdentifier] = array_map('trim', $inputData[0]);
            $formattedDataFromCsv[$documentIdentifier]['node_id'] = $documentIdentifier;

            unset($inputData[0]);
            foreach($inputData as $inputDataKey => $data) {
                $formattedDataFromCsv[$itemIdArray[($inputDataKey-1)]] = array_map('trim', $data);
                $formattedDataFromCsv[$itemIdArray[($inputDataKey-1)]]['node_id'] = $itemIdArray[($inputDataKey-1)];
            }

            foreach($formattedDataFromCsv as $dataCreated){
                    $dataToInput[]    =   array_map('trim', $dataCreated);
            }

            $customMetadataOfItemsFromCsv                       =   $this->getCustomMetadataDetailForDocumentAndItemFromCSV($dataToInput);

            $prepend = array($documentIdentifier => $documentIdentifier);
            foreach($itemIdArray as $newItem){
                $newCreatedItemArray[$newItem]    =   $newItem;
            }

            $newCreatedItemArray = $prepend + $newCreatedItemArray;
        }

        if(sizeOf($customMetadataOfItemsFromCsv) > 0) {
            foreach($customMetadataOfItemsFromCsv as $itemIdFromDb => $customMetadataOfItem){
                foreach($customMetadataOfItem as $metadata){
                    $formattedMetadataFromDb[$itemIdFromDb][strtolower($metadata['name']).'('.strtolower($fieldTypeValues[$metadata['field_type']]).')'] = ['metadata_value' => $metadata['pivot']['metadata_value'], 'is_additional' => $metadata['pivot']['is_additional']];
                }
            }
        }
        # unwanted code commented
        /*
        $attributeIn            =   'title';
        $containedInValues      =   $nodeTypeArray;
        $returnCollection       =   true;
        $fieldsToReturn         =   ['*'];
        $keyValuePairs          =   ['organization_id'=> $organizationIdentifier, 'is_deleted' => 0];
        $returnPaginatedData    =   false;
        $paginationFilters      =   ['limit' => 10, 'offset' => 0];
        $returnSortedData       =   false;
        $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator               =   'AND';
        $relations              =   ["metadata"];

        $nodeTypeSetOfAllItem    =   $this->nodeTypeRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn,
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator, 
            $relations
        )->toArray();
        */
        /*
        foreach($nodeTypeSetOfAllItem as $itemNodeType) {
            $metadataListOfNodeTypeArray[$itemNodeType['title']] = [];
            if(sizeOf($itemNodeType['metadata']) > 0) {
                foreach($itemNodeType['metadata'] as $metadata) {
                    if($metadata['is_custom'] == 1 && $metadata['is_deleted'] == 0) {
                        $metadataNameAndType    =   strtolower($metadata['name']).'('.strtolower($fieldTypeValues[$metadata['field_type']]).')';
                        $metadataListOfNodeTypeArray[$itemNodeType['title']][] =   !empty($metadataNameAndType) ? $metadataNameAndType : '';
                    }
                }
            }
        }
        */

        /**
         * Create new metadata when not added in csv but not in db
         */
        /*
        foreach($columnsFromHeaders as $csvHeaders) {
            $metadataPart       =   explode("(", $csvHeaders);

            $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;


            $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];
            $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();

            if(sizeOf($metadataDetail) == 0) {
                $metadataId =   $this->createUniversalUniqueIdentifier();
                $metadataInputData[$metadataPart[0]]  = $metadataId;
            }else {
                if($metadataDetail[0]['is_deleted'] == 1){
                    $metadataId =   $this->createUniversalUniqueIdentifier();
                    $metadataInputData[$metadataPart[0]]  = $metadataId;
                }
            }
        }
        */
        /*
        $keyForMetadata = 0;
        foreach($metadataListForATenant['metadata'] as $metadata) {
            if($metadata['is_document'] != 1 && $metadata['is_custom']  ==  0){
                $metadataIdArray['metadata'][$keyForMetadata]  = [
                    'id' => $metadata['metadata_id'],
                    'order'  =>  $metadata['order'],
                    'name' => $metadata['name'],
                    'is_mandatory' => $metadata['is_mandatory']
                ];

                $keyForMetadata++;
            }
        }
        */
        /**
         * Associate custom metadata to MetadataSet not existing in the tenant
         */
        /*
        foreach($nodeTypeIdArray as $nodeType) {
            foreach($columnsFromHeaders as $headerIndex => $csvHeaders) {
                $metadataPart       =   explode("(", $csvHeaders);

                $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;

                $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];
                $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();

                $indexOfCustomMetadataForNodeType = array_search((sizeOf($metadataDetail) > 0 ? $metadataDetail[0]['metadata_id'] : $metadataInputData[$metadataPart[0]]), array_column($metadataIdArray['metadata'], 'id'));

                if($indexOfCustomMetadataForNodeType == ''){
                    array_push($metadataIdArray['metadata'],['id' => (sizeOf($metadataDetail) > 0 ? $metadataDetail[0]['metadata_id'] : $metadataInputData[$metadataPart[0]]), 'order'  =>  ++$headerIndex, 'name' => $metadataPart[0], 'is_mandatory' => 0, 'is_custom' => 1]);
                }

                if(isset($metadataListOfNodeTypeArray[array_keys($nodeType)[0]]) && !in_array(strtolower($csvHeaders), $metadataListOfNodeTypeArray[array_keys($nodeType)[0]])){
                    array_push($metadataListOfNodeTypeArray[array_keys($nodeType)[0]], strtolower($csvHeaders) );
                }
            }
        }
        */
        /**
         * Update and create the metadata for respective item in csv
         */
        //here
        $itemCount  =   0;
        $additionalMetadata = [];
        foreach($formattedDataFromCsv as $itemIdFromCsv => $dataFromCsv) {
            if(in_array((string) $itemIdFromCsv, $arrayOfItemIdFromDb)) {
                foreach($columnsFromHeaders as $headerIndex => $csvHeaders) {
                    $metadataPart       =   explode("(", $csvHeaders);
                    $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
                    $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];
                    $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();

                    if($metadataType == 4){
                        if(!empty(trim($dataFromCsv[$csvHeaders])) && $this->validateDate(trim($dataFromCsv[$csvHeaders]))) {
                            $dataFromCsv[$csvHeaders] = date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders])));
                        } else {
                            $dataFromCsv[$csvHeaders] = '';
                            continue;
                        }
                        // $dataFromCsv[$csvHeaders] = (!empty(trim($dataFromCsv[$csvHeaders])) && $this->validateDate(trim($dataFromCsv[$csvHeaders]))) ? date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders]))) : trim($dataFromCsv[$csvHeaders]);
                    }

                    if($metadataType == 6){
                        $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? number_format((float) $dataFromCsv[$csvHeaders],0,'','') : trim($dataFromCsv[$csvHeaders]);
                    }

                    // Check metadata changes only for nodes & document - Skip association
                    if(!empty($dataFromCsv['case_full_statement']) || $dataFromCsv[$nodeTypeFromCsv] == 'Document'){
                        if(isset($formattedMetadataFromDb[$itemIdFromCsv]) && (isset($formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders])) && trim($dataFromCsv[$csvHeaders]) != trim($formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders]['metadata_value'])) {
                            $changedMetadata[$itemCount]['item_name'] = ($dataFromCsv[$nodeTypeFromCsv] == 'Document') ? trim($dataFromCsv[$title]) : (trim($dataFromCsv['case_full_statement']).(isset($dataFromCsv['case_human_coding_scheme']) ? ' '.trim($dataFromCsv['case_human_coding_scheme']) : ''));
                            $changedMetadata[$itemCount]['node_id'] = trim($dataFromCsv['node_id']);
                            $changedMetadata[$itemCount]['metadata_fields'][] = [
                                'name' => $csvHeaders,
                                'old_value' => $formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders]['metadata_value'],
                                'new_value' => trim($dataFromCsv[$csvHeaders])
                            ];
                            $updatedMetadataCount++;
                        }else {
                            unset($oldValue);
                            $setOldValueBlank = true;
                            // Check if metadata exists in db for that tenant
                            if(isset($metadataDetail[0]['metadata_id'])){
                                // Check if metadata exists for specific document or item
                                if($dataFromCsv[$nodeTypeFromCsv] == 'Document') {
                                    $attributesForCustomAdditional = ['document_id' => $itemIdFromDBSet[$itemIdFromCsv], 'metadata_id' => $metadataDetail[0]['metadata_id']];
                                    $customAdditionalMetadataExists = DB::table('document_metadata')->where($attributesForCustomAdditional)->get();
                                }
                                else{
                                    $attributesForCustomAdditional = ['item_id' => $itemIdFromDBSet[$itemIdFromCsv], 'metadata_id' => $metadataDetail[0]['metadata_id']];
                                    $customAdditionalMetadataExists = DB::table('item_metadata')->where($attributesForCustomAdditional)->get();
                                }

                                // Check if metadata has value
                                if(!$customAdditionalMetadataExists->isEmpty()){
                                    $setOldValueBlank = false;
                                    if(trim($customAdditionalMetadataExists[0]->metadata_value) != trim($dataFromCsv[$csvHeaders])){
                                        $oldValue = str_replace(array("\t", "\r"), '', trim($customAdditionalMetadataExists[0]->metadata_value));
                                    }
                                }
                            }

                            // Set value only if metadata in csv has value
                            if($setOldValueBlank==true && !empty(trim($dataFromCsv[$csvHeaders]))){
                                if(!in_array((string)$metadataPart[0], $additionalMetadata)){
                                    $additionalMetadata[] = $metadataPart[0];
                                    $newAdditionalMetadataCount++;
                                }
                                $oldValue = '';
                            }
                            // Check if oldvalue is set with either db value or empty for new metadata
                            if(isset($oldValue)){
                                $changedMetadata[$itemCount]['item_name'] = ($dataFromCsv[$nodeTypeFromCsv] == 'Document') ? trim($dataFromCsv[$title]) : (trim($dataFromCsv['case_full_statement']).(isset($dataFromCsv['case_human_coding_scheme']) ? ' '.trim($dataFromCsv['case_human_coding_scheme']) : ''));
                                $changedMetadata[$itemCount]['node_id'] = trim($dataFromCsv['node_id']);
                                $changedMetadata[$itemCount]['metadata_fields'][] = [
                                    'name' => $csvHeaders,
                                    'old_value' => $oldValue,
                                    'new_value' => trim($dataFromCsv[$csvHeaders])
                                ];
                                $updatedMetadataCount++;
                            }
                        }
                    }
                }
            } else {
                $nodeIsDeleted      =   $this->itemRepository->find($itemIdFromCsv);

                if($nodeIsDeleted['is_deleted'] == 0) {
                    foreach($columnsFromHeaders as $csvHeaders){

                        $metadataPart       =   explode("(", $csvHeaders);
                        $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;

                        if($metadataType == 4){
                            $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders]))) : trim($dataFromCsv[$csvHeaders]);
                        }

                        if($metadataType == 6){
                            $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? number_format((float) $dataFromCsv[$csvHeaders],0,'','') : trim($dataFromCsv[$csvHeaders]);
                        }

                        $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];

                        $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();
                        if(isset($metadataPart[0]) && empty($metadataDetail) && !in_array((string)$metadataPart[0], $additionalMetadata)){
                            $additionalMetadata[] = $metadataPart[0];
                            $newAdditionalMetadataCount++;
                        }
                    }
                }
            }
            $itemCount++;
        }


        /* if(!array_key_exists(0, $changedMetadata)){
            array_unshift($changedMetadata,['item_name' => $inputData[0]['title'],'metadata_fields' => []]);
        } */
        $this->changedMetadata              = array_values($changedMetadata);
        $this->blankMetadataCount           = $blankMetadataCount;
        $this->newAdditionalMetadataCount   = $newAdditionalMetadataCount;
        $this->updatedMetadataCount         = $updatedMetadataCount;
    }

    /**
     * Method to delete the item not present in CSV
     */
    private function deleteItemNotInCsvFromCollection() {
        $itemArrayFromCsv           =   [];
        $itemArrayFromDb            =   [];
        $inputItemIds               =   [];
        $arrayOfProjectsAssociated  =   [];
        $createdItemIdArray         =   [];

        $documentIdentifier     =   $this->documentIdentifier;
        $organizationIdentifier =   $this->organizationIdentifier;
        $itemCollectionArray    =   $this->itemCollectionFromDb;
        $createdItemArray       =   $this->createdItemArray;

        if($createdItemArray !== null && count($createdItemArray) > 0) {
            foreach($createdItemArray as $createdItem) {
                $createdItemIdArray[] =   $createdItem['item_id'];
            }
        }

        if($itemCollectionArray != null && count($itemCollectionArray) > 0) {
            foreach($itemCollectionArray as $itemFromDb) {
                $itemArrayFromCsv[] =   $itemFromDb['node_id'];
            }
        }

        /**
         * Fetch item detail from db for the item_id from csv file
         */
        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_id'];
        $keyValuePairs      =   ['organization_id' => $organizationIdentifier, 'document_id'  =>  $documentIdentifier, 'is_deleted'   =>  '0' ];
        $relations          =   ['project'];

        $itemCollectionFromDb =   $this->itemRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        foreach($itemCollectionFromDb as $itemFromDb) {
            $itemArrayFromDb[]  =   $itemFromDb->item_id;
        }

        if(count($createdItemIdArray) > 0) {
            $itemArrayFromDb    =   array_diff($itemArrayFromDb, $createdItemIdArray);
        }

        $arrayOfItemsToBeDeletedFromDB  =   array_values(array_diff($itemArrayFromDb, $itemArrayFromCsv));
        foreach($arrayOfItemsToBeDeletedFromDB as $itemIdentifier) {

            $this->itemRepository->getAllChildItems($itemIdentifier, $inputItemIds);
            array_push($inputItemIds, $itemIdentifier);
        }

        $this->deletedItemArray = array_unique($inputItemIds);
    }

    /**
     * Method to parse the final response
     */
    private function parseResponse() {
        $caseMetadata     = '';
        $customMetadata   = '';
        $updatedItemArray =   [];
        $createdItemArray =   [];

        $updatedCount       =   $this->count;
        $blankUpdatedCount  =   $this->blankCount;
        $newDocCount        =   $this->newDocCount;
        $newNodeCount       =   $this->newNodeCount;
        $newAssociationsCount = $this->newAssociationsCount;
        $deletedAssociationsCount = $this->deleteAssociationsCount;

        $updatedMetadataCount       =   $this->updatedMetadataCount;
        $blankMetadataCount         =   $this->blankMetadataCount;
        $newAdditionalMetadataCount =   $this->newAdditionalMetadataCount;
        $updatedCount = 0;
        $caseMetadataList = $this->caseMetadataSet;

        foreach($caseMetadataList as $metadata) {
            $caseMetadata   .= substr($metadata, strlen('case_')).',';
        }

        $caseMetadata   = rtrim($caseMetadata, ',');
        $customMetadata = rtrim(implode(",", $this->customMetadataSet),',');

        // commented for not considering removed node count while csv update
        // $itemIdDeleted      =   $this->deletedItemArray;
        // $deleteNodeCount    =   $this->deleteNodeCount + count($itemIdDeleted);
        $itemIdDeleted = [];
        $deleteNodeCount    =   $this->deleteNodeCount;

        $createdItem    =   $this->createdItemArray;
        if($createdItem != null && count($createdItem) > 0) {
            $arrayOfCreatedItem   =   array_values($createdItem);

            foreach($arrayOfCreatedItem as $itemCreated) {
                $createdItemArray[]   = $itemCreated['item_id'];
            }

            if(count($itemIdDeleted) > 0) {
                $createdItemArray   =   array_diff($createdItemArray, $itemIdDeleted);
            }
        }

        $newNodeCount       =   $newDocCount + count($createdItemArray);

        $updatedItemArray    =   $this->updatedItemArray;

        if(count($itemIdDeleted) > 0) {
            $updatedItemArray   =   array_diff($updatedItemArray, $itemIdDeleted);
        }
		// If document changed, change updated coubt
        $changedDocument    =   $this->changedDocument;
        if(!empty($changedDocument))
            $updatedCount = 1;

        $updatedCount       =   $updatedCount + (($updatedItemArray !== null) ? count($updatedItemArray) : 0);

        $finalChangeInMetadata = [];
        if($newDocCount > 0) {
            $this->taxonomyStatus   =   'Taxonomy Created';
        } else {
            if($updatedCount   >   0 || $blankUpdatedCount > 0 || $deleteNodeCount > 0 || $newNodeCount > 0 || $updatedMetadataCount > 0 || $blankMetadataCount > 0 || $newAdditionalMetadataCount > 0) {
                $this->taxonomyStatus   =   'Taxonomy Updated';
            } else {
                $this->taxonomyStatus   =   'No change in taxonomy';
            }
        $changedItem        =   $this->changedItem;
        $changeInMetadata   =   $this->changedMetadata;
        if(!empty($changedDocument) && !empty(array_values($changedDocument)))
            $changeInMetadata = array_merge($changeInMetadata,array_values($changedDocument));
        if(!empty($changedItem) && !empty(array_values($changedItem)))
            $changeInMetadata = array_merge($changeInMetadata,array_values($changedItem));

        $checkMergeMetadata = [];
        if(!empty($changeInMetadata)){
            foreach($changeInMetadata as $key=>$value){
                if(!isset($checkMergeMetadata[$value['node_id']])){
                    $checkMergeMetadata[$value['node_id']] = $key;
                    $finalChangeInMetadata[$value['node_id']] = $value;
                }
                else{
                    if(in_array($value['node_id'],array_keys($checkMergeMetadata))){
                        $finalChangeInMetadata[$value['node_id']]['metadata_fields'] = array_merge($value['metadata_fields'],$finalChangeInMetadata[$value['node_id']]['metadata_fields']);
                        unset($changeInMetadata[$key]);
                    }
                }
            }
        }
        foreach(array_keys($finalChangeInMetadata) as $key) {
            unset($finalChangeInMetadata[$key]['node_id']);
        }
		if(!empty($finalChangeInMetadata))
        	$finalChangeInMetadata = array_values($finalChangeInMetadata);
        }
		$errorData          =   $this->errorData;
		return ["createdOrUpdated"  => $this->taxonomyStatus, "updated_node_count" => count($finalChangeInMetadata), "new_node_count" => $newNodeCount, 'delete_node_count' => $deleteNodeCount, 'new_associations_count' => $newAssociationsCount, 'deleted_associations_count' => $deletedAssociationsCount, 'case_metadata' => $caseMetadata, 'custom_metadata' => $customMetadata, 'document' =>  [], 'item' => [], 'new_metadata_count' => $newAdditionalMetadataCount, 'metadata' => $finalChangeInMetadata, 'errorData' => $errorData];
    }

    public function getExistingParentNodeIdIfPresent($csvDocumentId)
    {
        $csvDocumentId = substr($csvDocumentId,-36);
        $getExistingParentNodeIdIfPresent = DB::table('items')->select('parent_id')->where('document_id',$csvDocumentId)->where('is_deleted',0)->get()->toArray();
        $getExistingParentNodeIdIfPresentArray = array();
        if(!empty($getExistingParentNodeIdIfPresent))
        {
            foreach($getExistingParentNodeIdIfPresent as $getExistingParentNodeIdIfPresentK=>$getExistingParentNodeIdIfPresentV)
            {
                $getExistingParentNodeIdIfPresentArray[]=$getExistingParentNodeIdIfPresentV->parent_id;
            }
            return $getExistingParentNodeIdIfPresentArray;
        }
        else
        {
            return $getExistingParentNodeIdIfPresentArray;
        }
    }


    /**
     * function to check parent node id / destination node id exist in database
     */
    public function checkParentNodeIdExist($parent_node_id)
    {
        $organizationId = $this->organizationIdentifier;
        $count = DB::table('item_associations')
                    ->where('source_item_id',$parent_node_id)
                    ->where('organization_id',$organizationId)
                    ->where('is_deleted',0)
                    ->count();

        if($count>0)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

    public function getNodeIdIfPresent($nodeId)
    {
        $organizationId = $this->organizationIdentifier;

        $getNodeIdIfPresent = DB::table('item_associations')
        ->select('source_item_id')
        ->whereIn('source_item_id',$nodeId)
        ->where('organization_id',$organizationId)
        ->where('is_deleted',0)
        ->get()->toArray();

        return $getNodeIdIfPresent;
    }

    public function checkAssociationExists($originNodeId, $destinationNodeId, $associationType)
    {
        $organizationId = $this->organizationIdentifier;
        $associationType = strtolower($associationType);
        $assocTypeArray=array('ischildof'=>1,'ispeerof'=>2,'ispartof'=>3,'exactmatchof'=>4,'precedes'=>5,'isrelatedto'=>6,'replacedby'=>7,'hasskilllevel'=>9);

        if(!empty($organizationId) && !empty($originNodeId) && !empty($destinationNodeId) && !empty($associationType) && isset($assocTypeArray[$associationType])){
            if(!empty($originNodeId)){
                $originSourceIdItemIdMappingNode = $this->getSourceIdParentIdMapping([$originNodeId]);
                if(!empty($originSourceIdItemIdMappingNode)){
                    $originNodeId = $originSourceIdItemIdMappingNode[0]->item_id;
                }
            }
            if(!empty($destinationNodeId)){
                $destSourceIdItemIdMappingNode = $this->getSourceIdParentIdMapping([$destinationNodeId]);
                if(!empty($destSourceIdItemIdMappingNode)){
                    $destinationNodeId = $destSourceIdItemIdMappingNode[0]->item_id;
                }
            }
            $getAssociation = DB::table('item_associations')
                ->where('source_item_id',$originNodeId)
                ->where('target_item_id',$destinationNodeId)
                ->where('association_type',$assocTypeArray[$associationType])
                ->where('organization_id',$organizationId)
                ->where('is_deleted',0)
                ->get()->toArray();
            return $getAssociation;
        }

        return [];
    }

    function validateDate($date){
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date) || preg_match("/^[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return true;
        } else {
            return false;
        }
    }

    public function getSourceIdParentIdMapping($parentId)
    {

        $organizationId = $this->organizationIdentifier;
        $sourceItemIdMappedWithParentIdItem = DB::table('items')
        ->select('item_id','source_item_id')
        ->whereIn('source_item_id',$parentId)
        ->where('organization_id', $organizationId)
        ->where('is_deleted' , 0)
        ->get()
        ->toArray();

        $sourceItemIdMappedWithParentIdDocument = DB::table('documents')
        ->select('document_id as item_id','source_document_id as source_item_id')
        ->whereIn('source_document_id',$parentId)
        ->where('organization_id', $organizationId)
        ->where('is_deleted' , 0)
        ->get()
        ->toArray();

        $sourceItemIdMappedWithParentId = array_merge((array) $sourceItemIdMappedWithParentIdItem,(array) $sourceItemIdMappedWithParentIdDocument);

        return $sourceItemIdMappedWithParentId;

    }
}