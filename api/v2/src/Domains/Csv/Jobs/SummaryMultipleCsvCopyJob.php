<?php
namespace App\Domains\Csv\Jobs;

ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

use App\Data\Models\Item;
use App\Data\Models\ItemAssociation;

class SummaryMultipleCsvCopyJob extends Job
{
    use UuidHelperTrait, DateHelpersTrait;

    private $inputData;
    private $userId;
    private $organizationIdentifier;

    private $documentIdentifier;

    private $packageExists;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $metadataRepository;

    private $errorData;
    private $parsedItemsFromCsv;
    private $parsedItemAssociationFromCsv;

    private $count;
    private $blankCount;
    private $newDocCount;
    private $newNodeCount;
    private $deleteNodeCount;

    private $updatedMetadataCount;
    private $blankMetadataCount;
    private $newAdditionalMetadataCount;

    private $columnFromHeaders;
    private $createdDocumentIdentifier;

    private $nodeType;
    private $title;
    private $metadataList;
    private $caseMetadataSet;
    private $customMetadataSet;

    private $newItemArray;
    private $newItemIdArray;
    private $itemCollectionFromDb;

    private $updatedItemArray;
    private $createdItemArray;
    private $deletedItemArray;

    private $metadata;

    private $taxonomyStatus;

    /***
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputData, string $organizationIdentifier, array $requestingUserDetail, string $requestImport, array $csvColumn, array $metadataListForATenant, string $packageExists, $request,$isBlocker = false)
    {
        $this->request = $request;
        //Set the private atribute
        $this->count            =   0;
        $this->blankCount       =   0;
        $this->newDocCount      =   0;
        $this->newNodeCount     =   0;
        $this->deleteNodeCount  =   0;
        $this->newAssociationsCount = 0;

        $this->updatedMetadataCount         =   0;
        $this->blankMetadataCount           =   0;
        $this->newAdditionalMetadataCount   =   0;

        $this->packageExists    =   $packageExists;
        $this->isBlocker = $isBlocker;
        // Condition added to add case_association_type, action, case_node_type even if columns are not added
        foreach($inputData as $k1=>$v1)
        {
            $v1 = array_change_key_case($v1,CASE_LOWER);
            if(!array_key_exists('case_association_type',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['case_association_type' => ""];
                }, $inputData);
            }

            if(!array_key_exists('action',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['action' => ""];
                }, $inputData);
            }

            if(!array_key_exists('case_node_type',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['case_node_type' => ""];
                }, $inputData);
            }

            break;
        }
        $this->inputData        =   $inputData;
        $this->userId           =   $requestingUserDetail['user_id'];
        $this->organizationIdentifier = $organizationIdentifier;
        $this->importType = $requestImport;
        // Condition added to add case_association_type, action, case_node_type even if columns are not added
        if (!in_array('case_association_type', $csvColumn))
        {
            array_push($csvColumn,'case_association_type');
        }
        if (!in_array('case_node_type', $csvColumn))
        {
            array_push($csvColumn,'case_node_type');
        }
        if (!in_array('action', $csvColumn))
        {
            array_push($csvColumn,'action');
        }
        $this->columnFromHeaders = array_slice($csvColumn,2);
        $this->metadataListForATenant   =   $metadataListForATenant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository, NodeTypeRepositoryInterface $nodeTypeRepository, ItemAssociationRepositoryInterface $itemAssociationRepository, MetadataRepositoryInterface $metadataRepository, ProjectRepositoryInterface $projectRepository)
    {
        $emptyParent                =   0;
        $emptyNode                  =   0;
        //Set the repo handler
        $this->documentRepository               =   $documentRepository;
        $this->itemRepository                   =   $itemRepository;
        $this->itemAssociationRepository        =   $itemAssociationRepository;
        $this->nodeTypeRepository               =   $nodeTypeRepository;
        $this->metadataRepository               =   $metadataRepository;
        $this->projectRepository                =   $projectRepository;

        $inputData  =   $this->inputData;
        $this->fetchMetadataList($this->request);
        $listCaseAndCustomMetadata = $this->listCaseAndCustomMetadata();

        if($listCaseAndCustomMetadata == false) {
            return false;
        } else {
            if($listCaseAndCustomMetadata !==  true) {
                # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                return '2|mUy7V3e|'.json_encode($listCaseAndCustomMetadata, true);
            }else {
                $inputData  =   $this->inputData;
                if($inputData[0][$this->title] == '') {
                    return false;
                }else
                {
                    /**
                     * Validation to check in wrong parent id is present in csv start
                    */
                    $csvDocumentId = $inputData[0]['node_id'];
                    $getExistingParentNodeIdIfPresent = $this->getExistingParentNodeIdIfPresent($csvDocumentId);
                    $importType=$this->importType;

                    $arrayOfNodeParentIdFromCSV    =   array_column($inputData,'parent/destination_node_id');
                    $arrayOfNodeParentIdFromCSVUnique = array();
                    foreach($arrayOfNodeParentIdFromCSV as $arrayOfNodeParentIdFromCSVK=>$arrayOfNodeParentIdFromCSVV)
                    {
                        if(!empty($arrayOfNodeParentIdFromCSVV))
                        {
                            // $arrayOfNodeParentIdFromCSVUnique[]=substr($arrayOfNodeParentIdFromCSVV,-36);
                            # altering this line since if uuid in csv is more than 36 characters long and upto 36 characters, uuid is correct then this error is not thrown
                            $arrayOfNodeParentIdFromCSVUnique[]=trim($arrayOfNodeParentIdFromCSVV);
                        }
                    }
                    $arrayOfNodeIdFromCSV    =   array_column($inputData,'node_id');
                    $arrayOfNodeIdFromCSVUnique = array();
                    foreach($arrayOfNodeIdFromCSV as $arrayOfNodeIdFromCSVK=>$arrayOfNodeIdFromCSVV)
                    {
                        if(!empty($arrayOfNodeIdFromCSVV))
                        {
                            // $arrayOfNodeIdFromCSVUnique[]=substr($arrayOfNodeIdFromCSVV,-36);
                            # altering this line since if uuid in csv is more than 36 characters long and upto 36 characters, uuid is correct, then this error is not thrown
                            $arrayOfNodeIdFromCSVUnique[]=trim($arrayOfNodeIdFromCSVV);
                        }
                    }

                    if(!empty($getExistingParentNodeIdIfPresent))
                    {
                        $nodeParentIdDiff = array_diff($arrayOfNodeParentIdFromCSVUnique,$getExistingParentNodeIdIfPresent);
                        $invalidParentNodeId = array_diff($nodeParentIdDiff,$arrayOfNodeIdFromCSVUnique);
                        // invalidParentNodeId - all invalid destination id for nodes & association
                        if(!empty($invalidParentNodeId))
                        {
                            $destinationNodeIds = [];
                            foreach($inputData as $akey=>$assocList){
                                if(!empty($assocList['parent/destination_node_id']) && (empty($assocList['case_association_type']) || strtolower($assocList['case_association_type'])=='ischildof') && trim($assocList['case_full_statement'])!=''){
                                    array_push($destinationNodeIds,trim($assocList['parent/destination_node_id']));
                                }
                            }
                            // destinationNodeIds - all destination id only for nodes
                            $arrayInvalidNodeId = array_unique(array_intersect($invalidParentNodeId,$destinationNodeIds));
                            // arrayInvalidNodeId - get id if present in node ignore if association
                            # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                            if(!empty($arrayInvalidNodeId))
                                return '4|mUy7V3e|'.json_encode($arrayInvalidNodeId, true);

                        }
                    }
                    /**
                     * Validation to check in wrong parent id is present in csv end
                    */


                    unset($inputData[0]);
                    $arrayOfNodeParentId    =   array_column($inputData, 'parent/destination_node_id');
                    $arrayOfNodeId          =   array_column($inputData, 'node_id');
                    foreach($arrayOfNodeParentId as $parentId) {
                        if(empty($parentId)) {
                            $emptyParent++;
                        }
                    }
                    foreach($arrayOfNodeId as $nodeId) {
                        if(empty($nodeId)) {
                            $emptyNode++;
                        }
                    }
                    if($emptyParent > 0 || $emptyNode > 0) {        
                        return false;
                    } else {
                        $columnsFromHeaders     =   $this->customMetadataSet;
                        $checkCustomNameAndType =   $this->checkCustomMetadataColumnNameAndType($columnsFromHeaders);

                        if($checkCustomNameAndType == true){
                            return false;
                        }else {
                            $data = $this->parseDataToFilterAssociation($inputData);

                            if(is_array($data))
                            {
                                if(end($data)=='duplicateNodeIds')
                                {
                                    array_pop($data);
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '5|mUy7V3e|'.json_encode($data, true);
                                }
                                else if(end($data)=='sameNodeIdAndParentId')
                                {
                                    array_pop($data);
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '6|mUy7V3e|'.json_encode($data, true);
                                }
                            }
                            else
                            {
                                if($this->isBlocker == false)
                                {
                                    $this->validateInputDataWithActionTypes();
                                    $this->copyDocumentData();
                                    $this->copyItemData();
                                    // $deleteItem     =   $this->deleteItemNotInCsvFromCollection();
                                    $responseData   =   $this->parseResponse();

                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '1|mUy7V3e|'.json_encode($responseData, true);

                                }
                                else
                                {
                                    # replaced || with |mUy7V3e| to solve metadata where || is present, causing summary to be null
                                    return '1|mUy7V3e|';
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Public Setter and Getter methods
     */

    public function getParsedItemsFromCsv() {
        return !empty($this->parsedItemsFromCsv) ? $this->parsedItemsFromCsv : [];
    }

    public function getParsedItemAssociationFromCsv() {
        return !empty($this->parsedItemAssociationFromCsv) ? $this->parsedItemAssociationFromCsv : [];
    }

    private function fetchMetadataList($request) {
        $metadataList   =   $this->metadataRepository->getMetadataList($request);

        $this->metadataList = $metadataList;
    }

    /**
     * Method to filter Items from CSV
     * and Item Associations from CSV
     *
     * @param [array] $itemsFromCSV
     * @return void
     */
    private function parseDataToFilterAssociation($itemsFromCSV) {
        /**
         * Data filtered to get unique id based on node_id and action and case field start
         */

        $titleHeader        =   $this->title;
        $associationHeader  =   $this->associationType;
        if(!empty($associationHeader)){
        foreach($itemsFromCSV  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }

        $itemsFromCSVCopy = $itemsFromCSV;
        $getUniqueitemIdArray = array();
        $getUniqueitemIdArray = array_unique($itemIdArray);

        $getItemArrayDataToInsert = array();
        $totalItemIdsFromCsv = array();
        $sameNodeIdAndParentId = array();
        foreach($getUniqueitemIdArray as $getUniqueitemIdArrayK=>$getUniqueitemIdArrayV)
        {
            foreach($itemsFromCSVCopy as $itemsFromCSVCopyK => $itemsFromCSVCopyV)
            {
                if(($getUniqueitemIdArrayV==$itemsFromCSVCopyV['node_id']) && ((strtolower($itemsFromCSVCopyV[$associationHeader])==strtolower('isChildOf') || $itemsFromCSVCopyV[$associationHeader]=="") && !empty($itemsFromCSVCopyV['case_full_statement'])))
                {
                    $getItemArrayDataToInsert[]=$itemsFromCSVCopyV;
                    $totalItemIdsFromCsv[$itemsFromCSVCopyK] = $itemsFromCSVCopyV['node_id'];

                    if($itemsFromCSVCopyV['node_id'] == $itemsFromCSVCopyV['parent/destination_node_id'])
                    {
                        $sameNodeIdAndParentId[] = $itemsFromCSVCopyK+2;
                    }
                }
            }
        }
        
        if(!empty($sameNodeIdAndParentId))
        {
            $errorMessageToIdentify = ['sameNodeIdAndParentId'];
            $mergedArrayForSameNodeIdAndParentId = array_merge($sameNodeIdAndParentId,$errorMessageToIdentify);
            return $mergedArrayForSameNodeIdAndParentId;
             
        }
        
        $totalItemIdsFromCsvUnique = array_unique($totalItemIdsFromCsv);
        if( count($totalItemIdsFromCsvUnique) != count($totalItemIdsFromCsv) )
        {
            
            $duplicateItemIds = array();
            $duplicateItemIds = array_diff_assoc($totalItemIdsFromCsv, array_unique($totalItemIdsFromCsv));
            
            $getRowNos = array();
            foreach($totalItemIdsFromCsv as $totalItemIdsFromCsvKey=>$totalItemIdsFromCsvValue)
            {
                foreach($duplicateItemIds as $duplicateItemIdsKey=>$duplicateItemIdsValue)
                {
                    if($totalItemIdsFromCsvValue == $duplicateItemIdsValue)
                    {
                        $getRowNos[] = $totalItemIdsFromCsvKey+2;
                    }
                }
            }
            
            $errorMessageToIdentify = ['duplicateNodeIds'];
            $mergedArrayForDuplicateItemIds = array_merge($getRowNos,$errorMessageToIdentify);
            return $mergedArrayForDuplicateItemIds;
            
        }
        
        $itemArrayDataCopy = [];
        foreach($getItemArrayDataToInsert as $getItemArrayDataToInsertK=>$getItemArrayDataToInsert)
        {
            $itemArrayDataCopy[$getItemArrayDataToInsertK+1] = $getItemArrayDataToInsert;
        }
        /**
        * Data filtered to get all relations excluding isChildOf start
        */
        /*
        $getRawDataForMultipleAssociations = array();
        $exchildof=0;
        foreach($itemsFromCSVCopy as $itemsFromCSVCopyKey=>$itemsFromCSVCopyValue)
        {
            $itemsFromCSVCopyValue = array_change_key_case($itemsFromCSVCopyValue,CASE_LOWER);
            $associationType = strtolower(str_replace(' ','',$itemsFromCSVCopyValue[$associationHeader]));
            $exludeChildOfArray = array('ischildof','');
            if(!in_array($associationType,$exludeChildOfArray))
            {
                $getRawDataForMultipleAssociations[$exchildof]['action'] = $itemsFromCSVCopyValue['action'];
                $getRawDataForMultipleAssociations[$exchildof]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                $getRawDataForMultipleAssociations[$exchildof]['parent/destination_node_id'] = $itemsFromCSVCopyValue['parent/destination_node_id'];
                $getRawDataForMultipleAssociations[$exchildof]['case_association_type'] = $itemsFromCSVCopyValue[$associationHeader];
                $getRawDataForMultipleAssociations[$exchildof]['document_id'] = $this->createdDocumentIdentifier;
                $exchildof = $exchildof +1;
            }
        }
        */

        $getRawDataForMultipleAssociations = array();
        $allasso=0;
        foreach($itemsFromCSVCopy as $itemsFromCSVCopyKey=>$itemsFromCSVCopyValue)
        {
            $itemsFromCSVCopyValue = array_change_key_case($itemsFromCSVCopyValue,CASE_LOWER);
            $associationType = strtolower(str_replace(' ','',$itemsFromCSVCopyValue[$associationHeader]));
            $childOfArray = array('ischildof','');
            if(!in_array($associationType,$childOfArray))
            {
                $getRawDataForMultipleAssociations[$allasso]['action'] = $itemsFromCSVCopyValue['action'];
                $getRawDataForMultipleAssociations[$allasso]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                $getRawDataForMultipleAssociations[$allasso]['parent/destination_node_id'] = $itemsFromCSVCopyValue['parent/destination_node_id'];
                $getRawDataForMultipleAssociations[$allasso]['case_association_type'] = $itemsFromCSVCopyValue[$associationHeader];
                $getRawDataForMultipleAssociations[$allasso]['document_id'] = $this->createdDocumentIdentifier;
                $allasso = $allasso +1;
            }
            elseif(in_array($associationType,$childOfArray) && ($itemsFromCSVCopyValue['case_full_statement']==''))
            {
                $getRawDataForMultipleAssociations[$allasso]['action'] = $itemsFromCSVCopyValue['action'];
                $getRawDataForMultipleAssociations[$allasso]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                $getRawDataForMultipleAssociations[$allasso]['parent/destination_node_id'] = $itemsFromCSVCopyValue['parent/destination_node_id'];
                $getRawDataForMultipleAssociations[$allasso]['case_association_type'] = $itemsFromCSVCopyValue[$associationHeader];
                $getRawDataForMultipleAssociations[$allasso]['document_id'] = $this->createdDocumentIdentifier;
                $allasso = $allasso +1;
            }
        }

        
        /**
        * Data filtered to get all relations excluding isChildOf end
        */

        /**
        * Data filtered to get all isChildOf relations of current taxonomy or other taxonomy start
        */
        /*
        $getRawDataForisChildOfAssociations = array();
        $inchildof=0;
        foreach($itemsFromCSVCopy as $itemsFromCSVCopyIndex=>$itemsFromCSVCopyData)
        {
            $itemsFromCSVCopyData = array_change_key_case($itemsFromCSVCopyData,CASE_LOWER);
            $associationType2 = strtolower(str_replace(' ','',$itemsFromCSVCopyData[$associationHeader]));
            $inludeChildOfArray = array('ischildof','');
            if(in_array($associationType2,$inludeChildOfArray) && ($itemsFromCSVCopyData['case_full_statement']==''))
            {
                    $getRawDataForisChildOfAssociations[$inchildof]['action'] = $itemsFromCSVCopyData['action'];
                    $getRawDataForisChildOfAssociations[$inchildof]['node_id'] = $itemsFromCSVCopyData['node_id'];
                    $getRawDataForisChildOfAssociations[$inchildof]['parent/destination_node_id'] = $itemsFromCSVCopyData['parent/destination_node_id'];
                    $getRawDataForisChildOfAssociations[$inchildof]['case_association_type'] = $itemsFromCSVCopyData[$associationHeader];
                    $getRawDataForisChildOfAssociations[$inchildof]['document_id'] = $this->createdDocumentIdentifier;
                    $inchildof = $inchildof +1;
            }
        }
        */

        /**
        * Data filtered to get all isChildOf relations of current taxonomy or other taxonomy end 
        */

        /**
         * Merge getRawDataForMultipleAssociations and getRawDataForisChildOfAssociations start
         */

        // $multipleNewAssociationsRawData = array();
        // $multipleNewAssociationsRawData = array_merge($getRawDataForMultipleAssociations,$getRawDataForisChildOfAssociations);

        /**
         * Merge getRawDataForMultipleAssociations and getRawDataForisChildOfAssociations start
         */

        // $itemsFromCSV               =   $itemArrayDataCopy;
        // $itemArrayAssociationData   =   $getRawDataForMultipleAssociations;

        $this->parsedItemsFromCsv = $itemArrayDataCopy;
        $this->parsedItemAssociationFromCsv = $getRawDataForMultipleAssociations;
        }
        /**
         * Data filtered to get unique id based on node_id and action and case field end
         */

         return true;
    }

    private function listCaseAndCustomMetadata() {

        $nodeType           =   '';
        $title              =   '';
        $associationType    =   '';

        $languageTitle          =   '';
        $subjectTitle           =   '';
        $subjectHierarchyCode   =   '';
        $subjectDescription     =   '';

        $conceptTitle           =   '';
        $conceptHierarchyCode   =   '';
        $conceptKeywords        =   '';
        $conceptDescription     =   '';

        $licenseTitle           =   '';
        $licenseText            =   '';
        $licenseDescription     =   '';

        $caseMetadataSet    =   [];
        $customMetadataSet  =   []; 
        
        $metadataList           =   $this->metadataList->toArray();
        $caseMetadataListFromDb =   array_filter($metadataList, function ($var) {
            return ($var['is_custom'] == '0');
        });

        $caseMetadataListFromDb = array_column($caseMetadataListFromDb, 'internal_name');

        $columnsFromHeaders  = $this->columnFromHeaders;
        foreach($columnsFromHeaders as $headers) {
            if (stripos($headers, 'case') !== false) {
                $caseMetadataSet[] = $headers;
            } else {
                $customMetadataSet[] = $headers;
            }
        }

    
        if(sizeOf($caseMetadataSet) > 0) {
            foreach($caseMetadataSet as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(!in_array($dbColumnFromCsvHeader, $caseMetadataListFromDb)) {
                    $arrayOfNonCaseMetadata[]   =   $metadata;  
                } else {
                    $result = '';  //set default value

                    foreach ( $caseMetadataSet as $key2 => $value ) {
                        if (false !== stripos($value,'case_title'))   // hasstack comes before needle
                            {
                            $result .= $key2;  // concat results
                            //return $result;
                            $title = $value;
                        }
                    }
                    if ($result == '') {
                        return false;
                    }else {
                        foreach($caseMetadataSet as $key2 => $value) {
                            if (false !== stripos($value,'case_title'))   // hasstack comes before needle
                            {
                                $result .= $key2;  // concat results
                                //return $result;
                                $title = $value;
                            }

                            if(false !== stripos($value,'case_association_type')) {
                                $associationType = $value;
                            }

                            if(false !== stripos($value,'case_node_type')) {
                                $nodeType = $value;
                            }
                
                            if(false !== stripos($value,'case_language')) {
                                $languageTitle = $value;
                            }
                
                            if(false !== stripos($value,'case_subject_title')) {
                                $subjectTitle = $value;
                            }
                
                            if(false !== stripos($value,'case_subject_hierarchy_code')) {
                                $subjectHierarchyCode = $value;
                            }
                
                            if(false !== stripos($value,'case_subject_description')) {
                                $subjectDescription = $value;
                            }
                
                            if(false !== stripos($value,'case_concept_title')) {
                                $conceptTitle = $value;
                            }
                
                            if(false !== stripos($value,'case_concept_keywords')) {
                                $conceptKeywords = $value;
                            }
                
                            if(false !== stripos($value,'case_concept_hierarchy_code')) {
                                $conceptHierarchyCode = $value;
                            }
                
                            if(false !== stripos($value,'case_concept_description')) {
                                $conceptDescription = $value;
                            }
                
                            if(false !== stripos($value,'case_license_title')) {
                                $licenseTitle = $value;
                            }
                
                            if(false !== stripos($value,'case_license_text')) {
                                $licenseText = $value;
                            }
                
                            if(false !== stripos($value,'case_license_description')) {
                                $licenseDescription = $value;
                            }
                        }
                    }

                    $this->title                    =   $title;
                    $this->nodeType                 =   $nodeType;
                    $this->associationType          =   $associationType;
                    $this->languageTitle            =   $languageTitle;

                    $this->subjectTitle             =   $subjectTitle;
                    $this->subjectHierarchyCode     =   $subjectHierarchyCode;
                    $this->subjectDescription       =   $subjectDescription;

                    $this->conceptTitle             =   $conceptTitle;
                    $this->conceptKeywords          =   $conceptKeywords;
                    $this->conceptHierarchyCode     =   $conceptHierarchyCode;
                    $this->conceptDescription       =   $conceptDescription;

                    $this->licenseTitle             =   $licenseTitle;
                    $this->licenseText              =   $licenseText;
                    $this->licenseDescription       =   $licenseDescription;

                    $this->caseMetadataSet = $caseMetadataSet;
                    if (($key = array_search('node_id', $customMetadataSet)) !== false) {
                        unset($customMetadataSet[$key]);
                    }
                    $this->customMetadataSet = $customMetadataSet;
                }
            }
        }

        if (($key = array_search($nodeType, $arrayOfNonCaseMetadata)) !== false) {
            unset($arrayOfNonCaseMetadata[$key]);
        }

        if (($key = array_search($associationType, $arrayOfNonCaseMetadata)) !== false) {
            unset($arrayOfNonCaseMetadata[$key]);
        }

        if(sizeOf($arrayOfNonCaseMetadata) > 0){
            return $arrayOfNonCaseMetadata;
        } else {
            return true; 
        }
    }

    private function checkCustomMetadataColumnNameAndType($customMetadataColumns) {
        $parsedCustomMetadataColumn     =   [];
        $fieldTypeValues                =   config("default_enum_setting.metadata_field_type");

        foreach($customMetadataColumns as $key => $customMetadata) {
            $metadataPart   =   explode('(', $customMetadata);
            $metadataType   =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;

            $parsedCustomMetadataColumn[$key]  =   $metadataPart[0].'('.$metadataType.')';
        }

        if(count(array_unique($parsedCustomMetadataColumn)) < count($parsedCustomMetadataColumn))
        {
            return true;
        }

        return false;
    }

    /**
     * Method to process the validation of data based on import type
     *
     * @param [integer] $importType
     * @param [array] $allItemsSourceIdFromTenant
     * @param [array] $itemIdNotInDb
     * @param [array] $itemsFromCSV
     * @param [array] $itemAssociationsFromCSV
     * @return void
     */
    private function processDataValidationForItemsAndAssociationsForImportType($importType, $allItemsSourceIdFromTenant, $itemIdNotInDb, $itemsFromCSV, $itemAssociationsFromCSV) {
        $errorUpdateRowForItems     =   [];
        $errorDeleteRowForItems     =   [];
        $errorRowForUpdateAssociations    =   [];
        $errorRowForDeleteAssociations    =   [];
        $errorRowForNotFoundDestinationId =   [];
        $errorInvalidStatusDateRowForItems = [];
        $errorGreaterStatusDateRowForItems = [];
        $errorSourceUrlRowForItems = [];
        $errorInvalidCustomDateRowForItems = [];

        $errorFlagForDeleteItem     =   0;
        $notFoundDestinationNode    =   0;
        $errorFlagForUpdateAssociation    =   0;
        $errorFlagForDeleteAssociation    =   0;
        $errorFlagForInvalidStatusDate = 0;
        $errorFlagForGreaterStatusDate = 0;
        $errorFlagForSourceUrl = 0;
        $errorFlagForInvalidCustomDate = 0;

        $inputData = $this->inputData;
        $nodeIdsFromCsv   = array_column($inputData, 'node_id');
        $newAssociationsCount = $this->newAssociationsCount;        
        $customColumnsFromHeaders = $this->customMetadataSet;
        $fieldTypeValues = config("default_enum_setting.metadata_field_type");
        // Code changes for Start End date & Invalid Url   
        if(isset($inputData[0]['case_status_start_date']) && isset($inputData[0]['case_status_end_date'])){
            if($inputData[0]['case_status_start_date']=="0" || $inputData[0]['case_status_end_date'] == "0" || (!empty($inputData[0]['case_status_start_date']) && !$this->validateDate($inputData[0]['case_status_start_date'])) || (!empty($inputData[0]['case_status_end_date']) && !$this->validateDate($inputData[0]['case_status_end_date'])))
            {
                array_push($errorInvalidStatusDateRowForItems, 2);
                $errorFlagForInvalidStatusDate  =   1;
            }
            else if(!empty($inputData[0]['case_status_start_date']) && !empty($inputData[0]['case_status_end_date']) && strtotime($inputData[0]['case_status_start_date']) > strtotime($inputData[0]['case_status_end_date'])){
                array_push($errorGreaterStatusDateRowForItems, 2);
                $errorFlagForGreaterStatusDate  =   1;
            }
        }
        if(isset($inputData[0]['case_official_source_url']) && !empty($inputData[0]['case_official_source_url'])){
            if(!filter_var($inputData[0]['case_official_source_url'], FILTER_VALIDATE_URL)){
                array_push($errorSourceUrlRowForItems, 2);
                $errorFlagForSourceUrl  =   1;
            }
        }
        # validation for date values in custom metadata for document
        foreach($customColumnsFromHeaders as $customCsvHeaders) {
            $metadataPart       =   explode("(", $customCsvHeaders);
            $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
            if($metadataType == 4){
                if($inputData[0][$customCsvHeaders] == "0" || (!empty($inputData[0][$customCsvHeaders]) && !$this->validateDate($inputData[0][$customCsvHeaders])) || (!empty($inputData[0][$customCsvHeaders]) && !$this->validateDate($inputData[0][$customCsvHeaders])))
                {
                    array_push($errorInvalidCustomDateRowForItems, 2);
                    $errorFlagForInvalidCustomDate  =   1;
                }
            }
        }
        unset($inputData[0]);
        $arrayUpdate = ['u','update'];
        $arrayDelete = ['d','delete'];

        foreach($itemsFromCSV as $itemKey => $itemsToAdd) {
            $itemsToAdd = array_change_key_case($itemsToAdd,CASE_LOWER);
            if(in_array($itemsToAdd['node_id'], $itemIdNotInDb)) {
                if(in_array(strtolower($itemsToAdd['action']),$arrayDelete)) {
                    $rowIndexFromCsv = $this->searchForId($itemsToAdd['node_id'], 
                    $inputData, array('$'),$errorDeleteRowForItems, ['key'=>'action','value'=>$itemsToAdd['action']]); 
                    array_push($errorDeleteRowForItems, ($rowIndexFromCsv + 2));
                    $errorFlagForDeleteItem  =   1;
                }
            }
            // Code changes for Start End date & Invalid Url
            if(isset($itemsToAdd['case_status_start_date']) && isset($itemsToAdd['case_status_end_date'])){
                $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                        
                if($itemsToAdd['case_status_start_date']=="0" || $itemsToAdd['case_status_end_date'] == "0" || (!empty($itemsToAdd['case_status_start_date']) && !$this->validateDate($itemsToAdd['case_status_start_date'])) || (!empty($itemsToAdd['case_status_end_date']) && !$this->validateDate($itemsToAdd['case_status_end_date'])))
                {
                    array_push($errorInvalidStatusDateRowForItems, ($rowIndexFromCsv + 2));
                    $errorFlagForInvalidStatusDate  =   1;
                }
                else if(!empty($itemsToAdd['case_status_start_date']) && !empty($itemsToAdd['case_status_end_date']) && strtotime($itemsToAdd['case_status_start_date']) > strtotime($itemsToAdd['case_status_end_date'])){
                    array_push($errorGreaterStatusDateRowForItems, ($rowIndexFromCsv + 2));
                    $errorFlagForGreaterStatusDate  =   1;
                }
            }
            if(isset($itemsToAdd['case_official_source_url']) && !empty($itemsToAdd['case_official_source_url'])){
                if(!filter_var($itemsToAdd['case_official_source_url'], FILTER_VALIDATE_URL)){
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                    array_push($errorSourceUrlRowForItems, ($rowIndexFromCsv + 2));
                    $errorFlagForSourceUrl  =   1;
                }
            }

            foreach($customColumnsFromHeaders as $customCsvHeaders) {
                $metadataPart       =   explode("(", $customCsvHeaders);
                $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
                if($metadataType == 4){
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemsToAdd['action'],'node_id' => $itemsToAdd['node_id'], 'parent/destination_node_id' => $itemsToAdd['parent/destination_node_id'], 'case_association_type' => $itemsToAdd['case_association_type'], 'case_full_statement' => $itemsToAdd['case_full_statement']));
                    if($itemsToAdd[$customCsvHeaders] == "0" || (!empty($itemsToAdd[$customCsvHeaders]) && !$this->validateDate($itemsToAdd[$customCsvHeaders])) || (!empty($itemsToAdd[$customCsvHeaders]) && !$this->validateDate($itemsToAdd[$customCsvHeaders])))
                    {
                        array_push($errorInvalidCustomDateRowForItems, ($rowIndexFromCsv+2));
                        $errorFlagForInvalidCustomDate  =   1;
                    }
                }
            }
        }
        
        $destinationNodes = array_column($itemAssociationsFromCSV, 'parent/destination_node_id');
        $sourceNodeIdsFromCsv = array_column($itemsFromCSV, 'node_id');            
        array_push($sourceNodeIdsFromCsv, $nodeIdsFromCsv[0]);

        foreach($destinationNodes as $associationDestinationId) {
            if(!in_array((string)$associationDestinationId, $sourceNodeIdsFromCsv)) {
                if(!in_array((string)$associationDestinationId, $allItemsSourceIdFromTenant)) {
                    $notFoundDestinationNode = 1;

                    $rowIndexFromCsv = $this->searchForId($associationDestinationId, 
                    $inputData, array('$'),$errorRowForNotFoundDestinationId, ['key'=>'parent/destination_node_id','value'=>$associationDestinationId]); 

                    array_push($errorRowForNotFoundDestinationId, ($rowIndexFromCsv + 2));
                }
            }
        }
        $actionNodeIdsFromCsv = array_column($itemsFromCSV, 'action');
        foreach($itemAssociationsFromCSV as $key=> $itemAssociationsToAdd) {
            if(in_array($itemAssociationsToAdd['node_id'], $sourceNodeIdsFromCsv)) {
                #check if assocition to be created does not have a node assocated with delete
                $key1 = array_search($itemAssociationsToAdd['node_id'], $sourceNodeIdsFromCsv);
                $actionOfNode = strtolower($actionNodeIdsFromCsv[$key1]);
                if(!in_array($actionOfNode,$arrayDelete)) {
                    $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']));
                    if(in_array(strtolower($itemAssociationsToAdd['action']),['','a','add','u','update']) && !in_array(($rowIndexFromCsv + 2),$errorRowForNotFoundDestinationId)){
                        $newAssociationsCount++;
                    }
                    unset($itemAssociationsFromCSV[$key]);
                }
            }
        }
        $this->newAssociationsCount = $newAssociationsCount;  
        $itemAssociationsFromCSV = array_values($itemAssociationsFromCSV);
        // For create a copy, 
        foreach($itemAssociationsFromCSV as $itemAssociationsToAdd) {
            if(in_array(strtolower($itemAssociationsToAdd['action']),['','a','add','u','update'])) {                    
                $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']),$errorRowForUpdateAssociations);
                array_push($errorRowForUpdateAssociations, ($rowIndexFromCsv + 2));
                $errorFlagForUpdateAssociation  =   1;
            } else if(in_array(strtolower($itemAssociationsToAdd['action']),$arrayDelete)) {
                $rowIndexFromCsv = $this->multiArraySearch($inputData, array('action'=>$itemAssociationsToAdd['action'],'node_id' => $itemAssociationsToAdd['node_id'], 'parent/destination_node_id' => $itemAssociationsToAdd['parent/destination_node_id'], 'case_association_type' => $itemAssociationsToAdd['case_association_type']),$errorRowForDeleteAssociations); 
                array_push($errorRowForDeleteAssociations, ($rowIndexFromCsv + 2));
                $errorFlagForDeleteAssociation  =   1;
            }
        }
        sort($errorUpdateRowForItems);
        sort($errorDeleteRowForItems);
        sort($errorRowForNotFoundDestinationId);
        sort($errorRowForUpdateAssociations);
        sort($errorRowForDeleteAssociations);
        sort($errorInvalidStatusDateRowForItems);
        sort($errorGreaterStatusDateRowForItems);
        sort($errorSourceUrlRowForItems);
        sort($errorInvalidCustomDateRowForItems);

        $this->errorUpdateRowForItems           =   $errorUpdateRowForItems;
        $this->errorDeleteRowForItems           =   $errorDeleteRowForItems;
        $this->errorRowForNotFoundDestinationId =   $errorRowForNotFoundDestinationId;
        $this->errorRowForUpdateAssociations    =   $errorRowForUpdateAssociations;
        $this->errorRowForDeleteAssociations    =   $errorRowForDeleteAssociations;
        $this->errorInvalidStatusDateRowForItems    =   $errorInvalidStatusDateRowForItems;
        $this->errorGreaterStatusDateRowForItems    =   $errorGreaterStatusDateRowForItems;
        $this->errorSourceUrlRowForItems    =   $errorSourceUrlRowForItems;
        $this->errorInvalidCustomDateRowForItems    =   $errorInvalidCustomDateRowForItems;

        $errorFlagArray = ['errorFlagForDeleteItem' => $errorFlagForDeleteItem, 'notFoundDestinationNode' => $notFoundDestinationNode, 'errorFlagForUpdateAssociation' => $errorFlagForUpdateAssociation, 'errorFlagForDeleteAssociation' => $errorFlagForDeleteAssociation, 'errorFlagForSourceUrl' => $errorFlagForSourceUrl, 'errorFlagForInvalidStatusDate' => $errorFlagForInvalidStatusDate, 'errorFlagForGreaterStatusDate' => $errorFlagForGreaterStatusDate, 'errorFlagForInvalidCustomDate' => $errorFlagForInvalidCustomDate];

        return $errorFlagArray;
    }
    
    /**
     * Method to validate the input data with action types
     *
     * @return void
     */
    private function validateInputDataWithActionTypes(){
        $sourceItemIdFromDb     =   [];

        $errorArray             =   [];
        $errorItemUpdateArray   =   [];
        $errorItemDeleteArray   =   [];
        $errorAssociationArray  =   [];

        $errorAssociationUpdateArray    =   [];
        $errorAssociationNotFoundArray  =   [];
        $errorItemArray        =   [];
        $errorAssociationDeleteArray = [];
        $errorItemStatusDateArray = [];
        $errorItemSourceUrlArray = [];
        $errorInvalidCustomDateArray = [];

        $organizationIdentifier     =   $this->organizationIdentifier;
        $inputData                  =   $this->inputData;
        $packageExists              =   $this->packageExists;

        $itemsFromCSV               = $this->getParsedItemsFromCsv();
        $itemAssociationsFromCSV    = $this->getParsedItemAssociationFromCsv();

        $importType =  $this->importType;


        // $returnCollection   =   true;
        // $fieldsToReturn     =   ['source_item_id'];
        // $keyValuePairs      =   ['is_deleted'   =>  '0', 'organization_id'  =>  $organizationIdentifier];

        // $allItemsFromTenant =   $this->itemRepository->findByAttributesWithSpecifiedFields(
        //     $returnCollection,
        //     $fieldsToReturn,
        //     $keyValuePairs)->toArray();

        $allItemsFromTenant = DB::table('items')
                                ->select('source_item_id')
                                ->where('is_deleted', 0)
                                ->where('organization_id', $organizationIdentifier)
                                ->get()
                                ->toArray();

        $allItemsSourceIdFromTenant = array_column($allItemsFromTenant, 'source_item_id');

        unset($inputData[0]);
        /**
         * Prepare array of Items received from csv file
         */
        foreach($inputData  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }

        /**
         * Fetch item detail from db for the item_id from csv file
         */
        // $attributeIn        =   "source_item_id";
        // $containedInValues  =   $itemIdArray;
        // $returnCollection   =   true;
        // $fieldsToReturn     =  ["item_id", "source_item_id"];

        // $keyValuePairs = ['is_deleted'   =>  '0', 'organization_id' => $organizationIdentifier];

        // $itemCollectionFromCsv =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery($attributeIn, $containedInValues, $returnCollection, $fieldsToReturn, $keyValuePairs);

        $itemCollectionFromCsv = DB::table('items')
                                    ->select('item_id','source_item_id')
                                    ->whereIn('source_item_id', $itemIdArray)
                                    ->where('is_deleted', 0)
                                    ->where('organization_id', $organizationIdentifier)
                                    ->get();

        $itemCollectionArray =  $itemCollectionFromCsv->toArray();

        foreach($itemCollectionArray as $itemFromDb) {
            $sourceItemIdFromDb[]                           =   $itemFromDb->source_item_id;
        }

        $itemIdNotInDb = array_diff($itemIdArray, $sourceItemIdFromDb);

        $errorFlagArray = $this->processDataValidationForItemsAndAssociationsForImportType($importType, $allItemsSourceIdFromTenant, $itemIdNotInDb, $itemsFromCSV, $itemAssociationsFromCSV);

        foreach($errorFlagArray as $keyForError => $flag) {
            if($keyForError == 'errorFlagForDeleteItem' && $flag == 1) {
                $errorItemDeleteArray[]   =   ['Error' => 'Could not find nodes to be deleted', 'Row' => implode(',', $this->errorDeleteRowForItems)];
            }

            if($keyForError == 'notFoundDestinationNode' && $flag == 1) {
                $errorAssociationNotFoundArray[]   =   ['Error' => 'Could not find the destination node to create association', 'Row' => implode(',', $this->errorRowForNotFoundDestinationId)];
            }

            if($keyForError == 'errorFlagForUpdateAssociation' && $flag == 1) {
                $errorAssociationUpdateArray[]   =   ['Error' => 'Could not find the source node/ association type to update association', 'Row' => implode(',', $this->errorRowForUpdateAssociations)];
            }

            if($keyForError == 'errorFlagForDeleteAssociation' && $flag == 1) {
                $errorAssociationDeleteArray[]   =   ['Error' => 'Could not find associations to be deleted', 'Row' => implode(',', $this->errorRowForDeleteAssociations)];
            }

            if($keyForError == 'errorFlagForInvalidStatusDate' && $flag == 1) {
                $errorItemStatusDateArray[]   =   ['Error' => 'Please enter valid date in format YYYY-MM-DD or YYYY/MM/DD', 'Row' => implode(',', $this->errorInvalidStatusDateRowForItems)];
            }

            if($keyForError == 'errorFlagForGreaterStatusDate' && $flag == 1) {
                $errorItemStatusDateArray[]   =   ['Error' => 'The start date is greater than the end date', 'Row' => implode(',', $this->errorGreaterStatusDateRowForItems)];
            }

            if($keyForError == 'errorFlagForSourceUrl' && $flag == 1) {
                $errorItemSourceUrlArray[]   =   ['Error' => 'The URL added is not valid', 'Row' => implode(',', $this->errorSourceUrlRowForItems)];
            }

            if($keyForError == 'errorFlagForInvalidCustomDate' && $flag == 1) {
                $errorInvalidCustomDateArray[]   =   ['Error' => 'Please enter valid date in format YYYY-MM-DD or YYYY/MM/DD', 'Row' => implode(',', $this->errorInvalidCustomDateRowForItems)];
            }
        }

        if(!empty($errorItemDeleteArray) || !empty($errorItemUpdateArray) || !empty($errorAssociationNotFoundArray) || !empty($errorItemStatusDateArray) || !empty($errorItemSourceUrlArray) || !empty($errorInvalidCustomDateArray)) {
            $errorItemArray         =   array_merge($errorAssociationNotFoundArray, array_merge($errorItemUpdateArray,$errorItemDeleteArray,$errorItemStatusDateArray,$errorItemSourceUrlArray, $errorInvalidCustomDateArray));
        }

        if(!empty($errorAssociationUpdateArray) || !empty($errorAssociationDeleteArray)) {
            $errorAssociationArray  =   array_merge($errorAssociationUpdateArray,$errorAssociationDeleteArray);
        }

        if(!empty($errorItemArray) || !empty($errorAssociationArray)){
            $errorArray     =   array_merge($errorItemArray, $errorAssociationArray);
        }

        $this->errorData    =   $errorArray;
        return !empty($errorArray) ? false : true;
    }

    /**
     * Method to search by single value 
     * from associative array
     *
     * @param [string] $search_value
     * @param [array] $array
     * @param [reference] $id_path
     * @return void
     */
    private function searchForId($search_value, $array, $id_path, $rowCountArray=[], $compareValues=[]) { 
        // Iterating over main array
        foreach ($array as $key1 => $val1) {

            $temp_path = $id_path;

            // Adding current key to search path
            array_push($temp_path, $key1);

            // Check if this value is an array
            // with atleast one element
            if(is_array($val1) and count($val1)) {

                // Iterating over the nested array
                foreach ($val1 as $key2 => $val2) {
                    if(trim($val2) == trim($search_value)) {

                        // Adding current key to search path
                        array_push($temp_path, $key2);
                        if(!empty($compareValues)){
                            if($val1[$compareValues['key']]!=$compareValues['value'])
                                continue;
                        }
                        if(!in_array(($temp_path[1]+2), $rowCountArray))
                            return $temp_path[1];
                    }
                }
            }

            elseif($val1 == $search_value) {
                if(!empty($compareValues)){
                    if($array[$compareValues['key']]!=$compareValues['value'])
                        continue;
                }
                if(!in_array(($temp_path[1]+2), $rowCountArray))
                    return $temp_path[1];
            }
        }

        return null;
    }

    /**
     * Method to search by conditional value
     * from associative array
     *
     * @param [type] $array
     * @param [type] $search
     * @return void
     */
    private function multiArraySearch($array, $search, $rowCount = [])
    {
        // Create the result array
        $result = 0;

        // Iterate over each array element
        foreach ($array as $key => $value)
        {

            // Iterate over each search condition
            foreach ($search as $k => $v)
            {

                // If the array element does not meet the search condition then continue to the next element
                if (!isset($value[$k]) || $value[$k] != $v)
                {
                    continue 2;
                }

            }

            // Add the array element's key to the result array
            $result = $key;

        }
        // Return the result array
        return $result;
    }

    /**
     * Method to update the document in db or
     * create a new document in db
     *
     * @return void
     */
    private function copyDocumentData() {
        $documentIdentifier =   $this->createUniversalUniqueIdentifier();
        $this->createdDocumentIdentifier = $documentIdentifier;
        $newDocCount        =   $this->newDocCount;
        $newDocCount++;
        $this->newDocCount    =   $newDocCount;
    }

    /**
     * Method to update the item in db or
     * create new item in db
     *
     * @return void
     */
    private function createItemIdForNewItemsToBeAdded() {
        $arrayOfCreatedItemId   =   [];
        $getNewItemArray    =   $this->newItemArray;
        if(count($getNewItemArray) > 0) {
            foreach($getNewItemArray as $newItem) {
                $arrayOfCreatedItemId[]   =   [$newItem['node_id']    =>  $this->createUniversalUniqueIdentifier()];
            }
        }

        $this->newItemIdArray = $arrayOfCreatedItemId;
    }

    /**
     * Method to search the node_id of the selected item's parent_node_id
     * in recursive array
     *
     * @param array $array
     * @param string $key
     * @param string $value
     * @return void
     */
    private function searchForParentNode(array $array, string $key, string $value)
    {
        $results = array();

        foreach($array as $itemCreated) {
            if (is_array($itemCreated) == true) {
                if (isset($itemCreated[$key]) && $itemCreated[$key] == $value) {
                    $results[] = $itemCreated;
                }

                foreach ($itemCreated as $subarray) {
                    if(is_array($subarray) == true) {
                        $results = array_merge($results, $this->searchForParentNode($subarray, $key, $value));
                    }
                }
            }
        }
        return $results;
    }

    /**
     * Method to set the parent node id for newly created item
     *
     * @param string $nodeParentIdToSearchWith
     * @return void
     */
    private function setParentIdForNewItem(string $nodeParentIdToSearchWith) {
        $parentId               =   '';
        $getNewItemArray        =   $this->newItemArray;

        $tempNewItemArray       =   [];
        $arrayOfCreatedItemId   =   $this->newItemIdArray;

        $keyForItem =    0;
        foreach($getNewItemArray as $key => $newItem) {
            $newItem['node_id']     =   $arrayOfCreatedItemId[$keyForItem][$newItem['node_id']] ;
            $tempNewItemArray[]     =   $newItem;
            $keyForItem++;
        }

        $arrayOfParentId    =   $this->searchForParentNode($getNewItemArray, 'node_id', $nodeParentIdToSearchWith);
        foreach($arrayOfCreatedItemId as $item) {
            $keys   =   array_keys($item);

            if($keys[0] == $arrayOfParentId[0]['node_id']) {
                return $item[$keys[0]];
            }
        }
    }
    /*
    private function createNewNodeTypeAndMapWithMetadata($nodeTypeTitle, $organizationIdentifier) {
        $nodeTypeId = $this->createUniversalUniqueIdentifier();
        //$inputMetaData = ['title' => $nodeType, 'organization_id' => $organizationIdentifier, 'node_type_id' => $nodeTypeId, 'source_node_type_id'   => $nodeTypeId, 'created_by' => $this->userId, 'updated_by' =>  $this->userId];

        $nodeTypeCreated = DB::table('node_types')->insert(
            ['node_type_id' => $nodeTypeId, 'organization_id' => $organizationIdentifier, 'title' => $nodeTypeTitle,  'source_node_type_id'   => $nodeTypeId, 'created_by' => $this->userId, 'updated_by' =>  $this->userId]
        );

        if($nodeTypeCreated == true) {
            $nodeTypeCreatedDetails =   $this->nodeTypeRepository->find($nodeTypeId);

            $metadataList   =   $this->metadataList;

            foreach($metadataList as $metadata) {
                if($metadata->is_document != 1 && $metadata->is_custom  ==  0){
                    $metadataIdArray['metadata'][]  = ['id' => $metadata->metadata_id, 'order'  =>  $metadata->order, 'is_mandatory' => $metadata->is_mandatory];
                }
            }

            $this->nodeTypeRepository->saveNodeTypeMetadata($nodeTypeCreatedDetails->node_type_id, $metadataIdArray);

            return $nodeTypeCreatedDetails->node_type_id;
        } else {
        }
    }
    */
    /**
     * Method to update existing item in db or
     * to create new item
     *
     * @return void
     */
    private function copyItemData() {
        $key                    =   1;
        $newNodeCount           =   $this->newNodeCount;
        $blankCount             =   $this->blankCount;
        $inputData              =   $this->inputData;
        $nodeTypeHeader         =   $this->nodeType;
        $organizationIdentifier =   $this->organizationIdentifier;
        $documentIdentifier     =   $this->createdDocumentIdentifier;

        $metadataList           =   $this->metadataList;
        $caseMetadata           =   $this->caseMetadataSet;

        unset($inputData[0]);
        $metadataIdArray        =   [];
        $newItemArray           =   [];
        $itemsToSave            =   [];
        $updatedItemArray       =   [];
        $metadataForItem        =   [];
        $items                  =   $inputData;
        $itemsFromCsv           = $this->getParsedItemsFromCsv();
        $userId                 =   $this->userId;
        $currentDateTime        =   $this->createDateTime();
        $newAssociationsCount   =   $this->newAssociationsCount;
        foreach($metadataList as $metadataFromDb){
            $metadataFromDbArr = $metadataFromDb->toArray();
            $internalName          =  $metadataFromDbArr['internal_name'];
            if($metadataFromDbArr['is_document'] != 1 && $metadataFromDbArr['is_custom'] == 0){
                if(strpos($internalName, 'pacing') !== FALSE || strpos($internalName, 'concept') !== FALSE || strpos($internalName, 'language') !== FALSE || strpos($internalName, 'license') !== FALSE) {

                } else {
                    $metadataForItem[] = $internalName;
                }
            }
        }

        /**
         * Prepare array of Items received from csv file
         */
        foreach($items  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }
        foreach($items as $id => $item) {
            $arrayOfCreatedItemId[]   =   $this->createUniversalUniqueIdentifier();
            $key = array_search (trim($item['parent/destination_node_id']), $itemIdArray);
            $arrayForParentIdIndices[]  =   $key;               
            $arrayOfOldItemMappedToNewItem[] = [trim($item['node_id']) => $arrayOfCreatedItemId[--$id]];
        }
        
        foreach($itemsFromCsv as $id => $item) {
            if(in_array(strtolower($item['action']),['','a','add','u','update'])){                

                $newItemArray[$id]['node_id']  =  trim($item['node_id']);
                $newItemArray[$id]['parent/destination_node_id'] = trim($item['parent/destination_node_id']); 
                
                foreach($caseMetadata as $metadata){
                    $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                    if(in_array($dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                        $newItemArray[$id][$dbColumnFromCsvHeader] = trim($item[$metadata]);        
                    }
                }
            }
        }
        $this->newItemArray = $newItemArray;
        $this->createItemIdForNewItemsToBeAdded();

        $this->newItemIdArray = $arrayOfOldItemMappedToNewItem;

        $keyForItem         =   0;
        $nodeTypeIdForItem  =   '';
        $caseAssociationsToSave = []; //caseAssociationsToSave initialized
        foreach($newItemArray as $key => $itemToBeCreated) {
            $item_id            =   $arrayOfCreatedItemId[$keyForItem];
            $parentId           =   ($arrayForParentIdIndices[$keyForItem] !== false ) ? $arrayOfCreatedItemId[$arrayForParentIdIndices[$keyForItem]] : $documentIdentifier;

            // $nodeType =   isset($itemToBeCreated[$nodeTypeHeader]) ? trim($itemToBeCreated[$nodeTypeHeader]) : 'Default';

            // $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $nodeType, 'is_deleted'   =>  '0' ];
            // $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);

            $itemsToSave[$key]= [
                "item_id"                   => $item_id,
                "parent_id"                 => $parentId,
                "document_id"               => $documentIdentifier,
            ];

            $itemAssociationID = $this->createUniversalUniqueIdentifier();
            $caseAssociationsToSave[] = [
                "item_association_id" => $itemAssociationID,
                "association_type" => 1,
                "document_id" => $documentIdentifier,
                "association_group_id" => '',
            ];

            $keyForItem++;
            $newNodeCount++;
        }
        $this->newNodeCount         =   $newNodeCount;
        $this->newAssociationsCount =   $newAssociationsCount + sizeOf($caseAssociationsToSave);
        $this->createdItemArray = $itemsToSave;
    }

    private function deleteItemNotInCsvFromCollection() {
        $itemArrayFromCsv           =   [];
        $itemArrayFromDb            =   [];
        $inputItemIds               =   [];
        $arrayOfProjectsAssociated  =   [];
        $createdItemIdArray         =   [];

        $documentIdentifier     =   $this->documentIdentifier;
        $organizationIdentifier =   $this->organizationIdentifier;
        $itemCollectionArray    =   $this->itemCollectionFromDb;
        $createdItemArray       =   $this->createdItemArray;

        if($createdItemArray != null && count($createdItemArray) > 0) {
            foreach($createdItemArray as $createdItem) {
                $createdItemIdArray[] =   $createdItem['item_id'];
            }
        }

        if($itemCollectionArray != null && count($itemCollectionArray) > 0) {
            foreach($itemCollectionArray as $itemFromDb) {
                $itemArrayFromCsv[] =   $itemFromDb['node_id'];
            }
        }

        /**
         * Fetch item detail from db for the item_id from csv file
         */
        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_id'];
        $keyValuePairs      =   ['organization_id' => $organizationIdentifier, 'document_id'  =>  $documentIdentifier, 'is_deleted'   =>  '0' ];
        $relations          =   ['project'];

        $itemCollectionFromDb =   $this->itemRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        foreach($itemCollectionFromDb as $itemFromDb) {
            $itemArrayFromDb[]  =   $itemFromDb->item_id;           
        }

        $itemArrayFromDb    =   array_diff($itemArrayFromDb, $createdItemIdArray);

        $projectItemMapping = $this->projectRepository->getProjectItemMappingOfMultipleItems($itemArrayFromDb);

        foreach($projectItemMapping as $projects) {
            $arrayOfProjectsAssociated[]  = $projects->project_id;
        }

        $arrayOfProjectsAssociated  =   array_values(array_unique($arrayOfProjectsAssociated));

        $arrayOfItemsToBeDeletedFromDB  =   array_values(array_diff($itemArrayFromDb, $itemArrayFromCsv));
        
        foreach($arrayOfItemsToBeDeletedFromDB as $itemIdentifier) {
            
            $this->itemRepository->getAllChildItems($itemIdentifier, $inputItemIds);      
            array_push($inputItemIds, $itemIdentifier);
        }

        $this->deletedItemArray = $inputItemIds;
    }

    private function parseResponse() {
        $caseMetadata     = '';
        $customMetadata   = '';

        $updatedItemArray =   [];
        $createdItemArray =   [];

        $updatedCount       =   $this->count;

        $blankUpdatedCount  =   $this->blankCount;  
        $newDocCount        =   $this->newDocCount; 
        $newNodeCount       =   $this->newNodeCount;
        $newAssociationsCount   =   $this->newAssociationsCount;

        $caseMetadataList = $this->caseMetadataSet;

        foreach($caseMetadataList as $metadata) {
            $caseMetadata   .= substr($metadata, strlen('case_')).',';
        }

        $caseMetadata   = rtrim($caseMetadata, ',');
        $customMetadata = rtrim(implode(",", $this->customMetadataSet),',');

        $itemIdDeleted      =   $this->deletedItemArray;
        $deleteNodeCount    =   0;//count($itemIdDeleted);
        
        
        $createdItem    =   $this->createdItemArray;
    
        if($createdItem != null && count($createdItem) > 0) {
            $arrayOfCreatedItem   =   array_values($createdItem);
            foreach($arrayOfCreatedItem as $itemCreated) {
                $createdItemArray[]   = $itemCreated['item_id'];
            }
    
            if(isset($itemIdDeleted) && count($itemIdDeleted) > 0) {
                $createdItemArray   =   array_diff($createdItemArray, $itemIdDeleted);
            }
        }

        $newNodeCount       =   $newDocCount + count($createdItemArray);

        $updatedItem    =   $this->updatedItemArray;
        if($updatedItem != null && count($updatedItem) > 0) {
            $arrayOfUpdatedItem   =   array_values($updatedItem);
            foreach($arrayOfUpdatedItem as $itemUpdated) {
                $updatedItemArray[]   = $itemUpdated['item_id'];
            }
            
            if(isset($itemIdDeleted) && count($itemIdDeleted) > 0) {
                $updatedItemArray   =   array_diff($updatedItemArray, $itemIdDeleted);
            }    
        }
        
        $updatedCount       =   $updatedCount + count($updatedItemArray);

        if($newDocCount > 0) {
            $this->taxonomyStatus   =   'Taxonomy Created';
        } else {
            if($updatedCount    >   0 || $blankUpdatedCount > 0 || $deleteNodeCount > 0 || $newNodeCount > 0 ) {
                $this->taxonomyStatus   =   'Taxonomy Updated';
            } else {
                $this->taxonomyStatus   =   'No change in taxonomy';
            }
        }

        $errorData          =   $this->errorData;

        return ["createdOrUpdated"  => $this->taxonomyStatus, "updated_node_count" => $updatedCount, "blank_updated_count"    =>  $blankUpdatedCount, "new_node_count"    =>  $newNodeCount, 'delete_node_count'    => $deleteNodeCount, 'new_associations_count' => $newAssociationsCount, 'new_metadata_count' => 0, 'case_metadata' => $caseMetadata, 'custom_metadata' => $customMetadata, 'errorData' => $errorData];
    }

    public function getExistingParentNodeIdIfPresent($csvDocumentId)
    {
        $csvDocumentId = substr($csvDocumentId,-36);
        $getExistingParentNodeIdIfPresent = DB::table('items')->select('parent_id')->where('document_id',$csvDocumentId)->where('is_deleted',0)->get()->toArray();
        $getExistingParentNodeIdIfPresentArray = array();
        if(!empty($getExistingParentNodeIdIfPresent))
        {
            foreach($getExistingParentNodeIdIfPresent as $getExistingParentNodeIdIfPresentK=>$getExistingParentNodeIdIfPresentV)
            {
                $getExistingParentNodeIdIfPresentArray[]=$getExistingParentNodeIdIfPresentV->parent_id;
            }
            return $getExistingParentNodeIdIfPresentArray;
        }
        else
        {
            return $getExistingParentNodeIdIfPresentArray;
        }
        
    }

    /**
     * function to check parent node id / destination node id exist in database
     */
    public function checkParentNodeIdExist($parent_node_id)
    {
        $organizationId = $this->organizationIdentifier;
        $count = DB::table('item_associations')
                    ->where('source_item_id',$parent_node_id)
                    ->where('organization_id',$organizationId)
                    ->where('is_deleted',0)
                    ->count();

        if($count>0)
        {
            return 1;
        }           
        else
        {
            return 2;
        } 
    }    

    public function getNodeIdIfPresent($nodeId)
    {
        $organizationId = $this->organizationIdentifier;

        $getNodeIdIfPresent = DB::table('item_associations')
        ->select('source_item_id')
        ->whereIn('source_item_id',$nodeId)
        ->where('organization_id',$organizationId)
        ->where('is_deleted',0)
        ->get()->toArray();
        return $getNodeIdIfPresent;
        
    }

    public function checkAssociationExists($originNodeId, $destinationNodeId, $associationType)
    {
        $organizationId = $this->organizationIdentifier;
        $associationType = strtolower($associationType);
        $assocTypeArray=array('ischildof'=>1,'ispeerof'=>2,'ispartof'=>3,'exactmatchof'=>4,'precedes'=>5,'isrelatedto'=>6,'replacedby'=>7,'hasskilllevel'=>9);
            
        if(!empty($organizationId) && !empty($originNodeId) && !empty($destinationNodeId) && !empty($associationType) && isset($assocTypeArray[$associationType])){
            $getAssociation = DB::table('item_associations')
                ->where('source_item_id',$originNodeId)
                ->where('target_item_id',$destinationNodeId)
                ->where('association_type',$assocTypeArray[$associationType])
                ->where('organization_id',$organizationId)
                ->where('is_deleted',0)
                ->get()->toArray();
            return $getAssociation;
        }
        return [];
        
    }

    function validateDate($date){
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date) || preg_match("/^[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return true;
        } else {
            return false;
        }
        
    }

}