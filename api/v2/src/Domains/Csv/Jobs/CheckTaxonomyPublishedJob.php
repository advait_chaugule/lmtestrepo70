<?php
namespace App\Domains\Csv\Jobs;

use Lucid\Foundation\Job;
use DB;
use Illuminate\Support\Facades\Hash;

class CheckTaxonomyPublishedJob extends Job
{   
    
   
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sourceDocumentId)
    {
        //Set the private variable
        $this->sourceDocumentId             = $sourceDocumentId;   
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       
        //Set the db handler
        return $this->getTaxonomyDetails($this->sourceDocumentId); 
    }

    private function getTaxonomyDetails($sourceDocumentId)
    { 
       $getTaxonomyDetails= DB::table('documents')
                ->select ('status')
                ->where('source_document_id',$sourceDocumentId)
                ->get()
                ->toArray();  
            $status = "";
            if(!empty($getTaxonomyDetails)){            
                 $status = $getTaxonomyDetails[0]->status; 
            }
            
            if($status == 3)
            {
                return true;
            }
            else
            {
                return false;   
            }
    }
   
}


