<?php
namespace App\Domains\Csv\Jobs;

use Lucid\Foundation\Job;

class StoreExternalCsvFileJob extends Job
{
    private $externalCsvFile;
    private $baseFolderPathForStorage;
    private $externalCsvStorageFolder;
    private $externalFileName;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($externalCsvFile, $externalFileName)
    {
        $this->externalCsvFile = $externalCsvFile;
        $this->externalCsvStorageFolder = "temporary/csv";
        $this->baseFolderPathForStorage = storage_path('app');
        $this->externalFileName = $externalFileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->externalCsvFile->storeAs($this->externalCsvStorageFolder, $this->externalFileName);
        return $this->externalCsvStorageFolder."/".$this->externalFileName;
    }
}
