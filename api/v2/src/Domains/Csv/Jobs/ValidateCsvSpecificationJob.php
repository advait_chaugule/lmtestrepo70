<?php
namespace App\Domains\Csv\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ValidateCsvSpecificationJob extends Job
{
    private $inputData;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input, string $organizationIdentifier)
    {
        $this->inputData = $input;
        $this->organizationIdentifier = $organizationIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        //Set the repo handler
        $this->documentRepository       =   $documentRepository;
        //validate if content is proper in csv
        $csvContentVerificationStatus   =   $this->verifyContent();
        return $csvContentVerificationStatus;
    }
    
    public function verifyContent() {
        $inputData  =   $this->inputData;
        if(isset($inputData[0]['node_id']) && !empty(trim($inputData[0]['node_id']))) {
            $verifyTheFirstNodeAsDocument   =   $this->validateCFDocument($inputData)->toArray();
            //dd($verifyTheFirstNodeAsDocument);
            if(!empty($verifyTheFirstNodeAsDocument)) {
                if($verifyTheFirstNodeAsDocument['source_document_id'] == trim($inputData[0]['node_id'])){
                    $validationDetails['status'] = true;
                    $validationDetails['message'] = "Valid content.";
                }
                else{
                    $validationDetails = ["status" => false, "message" => "File content is not a CSV standard"];
                }
            } else {
                $validationDetails = ["status" => false, "message" => "File content is not a CSV standard"];
            }            
        } else {
            $validationDetails = ["status" => false, "message" => "File content is not a CSV standard"];
        }
        
        //dd($validationDetails);
        return $validationDetails;
    }

    private function validateCFDocument(array $inputData) {
        $attributes = ['source_document_id' => trim($inputData[0]['node_id']), 'organization_id' => $this->organizationIdentifier, 'is_deleted' => 0];
        $document = $this->documentRepository->findByAttributes($attributes)->first();
        return $document;
    }
}
