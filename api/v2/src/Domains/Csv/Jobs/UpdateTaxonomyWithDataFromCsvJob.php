<?php
namespace App\Domains\Csv\Jobs;

ini_set('max_execution_time', 0); 
ini_set('memory_limit', '-1');

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;    
use Illuminate\Support\Collection;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;

use App\Data\Models\Organization;
use App\Data\Models\Item;
use App\Data\Models\ItemAssociation;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;

class UpdateTaxonomyWithDataFromCsvJob extends Job
{
    use UuidHelperTrait, DateHelpersTrait, CaseFrameworkTrait;
    public $recordChunkCount = 100;

    private $inputData;
    private $userId;
    private $organizationIdentifier;

    private $packageExists;
    private $importType;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $metadataRepository;

    private $count;
    private $blankCount;
    private $newDocCount;
    private $newNodeCount;
    private $deleteNodeCount;
    private $newAssociationsCount;
    private $organizationCode;
    private $updatedMetadataCount;
    private $blankMetadataCount;
    private $newAdditionalMetadataCount;

    private $columnFromHeaders;
    private $createdDocumentIdentifier;
    private $documentFromDB;

    private $metadataList;
    private $nodeType;
    private $caseMetadataSet;
    private $customMetadataSet;

    private $nodeTypeIdArray;

    private $newItemArray;
    private $newItemIdArray;
    private $itemCollectionFromDb;

    private $updatedItemArray;
    private $createdItemArray;
    private $deletedItemArray;

    private $metadata;

    private $taxonomyStatus;
    private $domainName;

    private $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputData, string $organizationIdentifier, array $requestingUserDetail, string $importType, array $csvColumn, array $metadataListForATenant, string $packageExists,$requestUrl)
    {

        $inputData = $this->filterInputData($inputData);
        //Set the private atribute
        $this->count            =   0;
        $this->blankCount       =   0;
        $this->newDocCount      =   0;
        $this->newNodeCount     =   0;
        $this->deleteNodeCount  =   0;
        $this->newAssociationsCount = 0;

        $this->updatedMetadataCount         =   0;
        $this->blankMetadataCount           =   0;
        $this->newAdditionalMetadataCount   =   0;

        $this->packageExists    =   $packageExists;
        $this->domainName       =   $requestUrl;

        $this->allChildSeqArr = [];

        /** below loop added to change parent/destination_node_id from csv to node_parent_id start */
        foreach ($inputData as $k=>$v )
        {
            $inputData[$k] ['node_parent_id'] = $inputData[$k] ['parent/destination_node_id'];
            unset($inputData[$k]['parent/destination_node_id']);
        }
        /** below loop added to change parent/destination_node_id from csv to node_parent_id end */
        
        /**
         * adding case_association_type/action/case_node_type key action if not exists start
         */
        
        $actionExist = true;
        foreach($inputData as $k1=>$v1)
        {
            $v1 = array_change_key_case($v1,CASE_LOWER);
            if(!array_key_exists('case_association_type',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['case_association_type' => ""];
                }, $inputData);
            }

            if(!array_key_exists('action',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['action' => ""];
                }, $inputData);
            }

            if(!array_key_exists('case_node_type',$v1))
            {
                $inputData = array_map(function($inputData){
                    return $inputData + ['case_node_type' => ""];
                }, $inputData);
            }

            break;    
            
        }
        
        /**
         * adding case_association_type/action/case_node_type key action if not exists start
         */
        

        $this->inputData        =   $inputData;
        $this->userId           =   $requestingUserDetail['user_id'];
        $this->setOrganizationIdentifier($organizationIdentifier);   
        $this->setImportType($importType);   
        $this->setOrgCode($organizationIdentifier);
        /**
         * array slice 3 added to skip 1st 3 columns of csv
         */
        
        foreach($csvColumn as $csvColumnKey=>$csvColumnValue)
        {
            if('action'==strtolower($csvColumnValue))
            {
                unset($csvColumn[$csvColumnKey]);
            }
        }

        // to add case_node_type to csv header column start
        if (!in_array('case_node_type', $csvColumn))
            {
                array_push($csvColumn,'case_node_type');
            }
        // to add case_node_type to csv header column end    
        
        $this->setColumnsFromHeader(array_slice($csvColumn,2));  
        
        
        
        $this->metadataListForATenant   =   $metadataListForATenant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository, NodeTypeRepositoryInterface $nodeTypeRepository, ItemAssociationRepositoryInterface $itemAssociationRepository, MetadataRepositoryInterface $metadataRepository, ProjectRepositoryInterface $projectRepository, LicenseRepositoryInterface $licenseRepository,
    SubjectRepositoryInterface $subjectRepository,
    ConceptRepositoryInterface $conceptRepository, LanguageRepositoryInterface $languageRepository, Request $request )
    {
        //Set the repo handler
        $this->documentRepository               =   $documentRepository;
        $this->itemRepository                   =   $itemRepository;
        $this->itemAssociationRepository        =   $itemAssociationRepository;
        $this->licenseRepository                =   $licenseRepository;
        $this->languageRepository               =   $languageRepository;
        $this->subjectRepository                =   $subjectRepository;
        $this->conceptRepository                =   $conceptRepository;
        $this->nodeTypeRepository               =   $nodeTypeRepository;
        $this->metadataRepository               =   $metadataRepository;
        $this->projectRepository                =   $projectRepository;

        $importType          =   $this->getImportType();
        $columnsFromHeaders  =   $this->getColumnsFromHeader();
    
        $this->fetchMetadataList($request);
        $listCaseAndCustomMetadata = $this->listCaseAndCustomMetadata();
        
        if($listCaseAndCustomMetadata=="InValidCaseHeader")
        {
            return "InValidCaseHeader";
        }
        $this->extractAndSetCaseSubjectsToSave();
        $this->extractAndSetCaseConceptsToSave();
        $this->extractAndSetCaseLicensesToSave();

        $this->fetchAndSetSavedSubjects();
        $this->fetchAndSetSavedConcepts();
        $this->fetchAndSetSavedLicenses();
        $this->fetchAndSetSavedLanguages();

        $this->processAndSaveCaseSubjectsData();
        $this->processAndSaveCaseConceptsData();
        $this->processAndSaveCaseLicensesData();


        $updateDocument =   $this->updateDocumentData();
        $updateItem     =   $this->updateItemData();

        if(sizeOf($columnsFromHeaders) > 0 && $this->nodeType != ''){
            $updateMetadata =   $this->updateMetadataData();
        }

        if($importType == 4){
            // commented for not deleting nodes while csv update
            //$deleteItem     =   $this->deleteItemNotInCsvFromCollection();
             $this->deleteExistingNodeData();
        }

        $responseData   =   $this->parseResponse();
        $documentId = $this->getCreatedDocumentIdentifier();
        if(!empty($documentId)) {
            # make taxonomy visible in taxnonomy listing page
            DB::table('documents')->where('document_id', $documentId)->update(['import_status' => 0]);
        }
        // Code insert for cloud search in csv
        $this->raiseEventToUploadTaxonomySearchDataToSqs();
        return $responseData;
    }

    /**
     * Public Setter and Getter methods
     */
    public function setImportType($importType) {
        $this->importType   =   $importType;
    }

    public function getImportType() {
        return $this->importType;
    }

    public function setOrganizationIdentifier($organizationIdentifier) {
        $this->organizationIdentifier   =   $organizationIdentifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setDocumentIdentifier($inputData) {
        $this->documentIdentifier =   $inputData;
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setColumnsFromHeader($data) {
        $this->columnFromHeaders    =   $data;
    }

    public function getColumnsFromHeader() {
        return $this->columnFromHeaders;
    }

    public function setMetadata($list) {
        $this->metadataList =   $list;
    }

    public function getMetadata() {
        return $this->metadataList;
    }

    public function setCreatedDocumentIdentifier($identifier) {
        $this->createdDocumentIdentifier =   $identifier;
    }

    public function getCreatedDocumentIdentifier() {
        return $this->createdDocumentIdentifier;
    }

    private function fetchMetadataList($request) {
        $metadataList   =   $this->metadataRepository->getMetadataList($request);
        $this->setMetadata($metadataList);
    }

    public function setCaseMetadataSet($data){
        $this->caseMetadataSet = $data;
    }

    public function getCaseMetadataSet() {
        return $this->caseMetadataSet;
    }

    public function setCustomMetadataSet($data){
        $this->customMetadataSet = $data;
    }

    public function getCustomMetadataSet() {
        return $this->customMetadataSet;
    }

    public function setItemCollectionFromDb($itemCollection) {
        $this->itemCollectionFromDb =   $itemCollection;
    }

    public function getItemCollectionFromDb() {
        return $this->itemCollectionFromDb;
    }

    public function setNodeTypeIdArray($data) {
        $this->nodeTypeIdArray = $data;
    }

    public function getNodeTypeIdArray() {
        return $this->nodeTypeIdArray;
    }

    public function setNewItemArray($newItemArray) {
        $this->newItemArray  =   $newItemArray;
    }

    public function getNewItemArray() {
        return $this->newItemArray;
    }

    public function setArrayOfCreatedItemId($newItemIdArray) {
        $this->newItemIdArray  =   $newItemIdArray;
    }

    public function getArrayOfCreatedItemId() {
        return $this->newItemIdArray;
    }

    public function setCreatedItemArray($data) {
        $this->createdItemArray   =   $data;
    }

    public function getCreatedItemArray() {
        return $this->createdItemArray;
    }

    public function setUpdatedItemArray($data) {
        $this->updatedItemArray   =   $data;
    }

    public function getUpdatedItemArray() {
        return $this->updatedItemArray;
    }

    public function setItemIdsDeleted($data) {
        $this->deletedItemArray =   $data;
    }

    public function getItemIdsDeleted() {
        return $this->deletedItemArray;
    }

    public function setCaseConceptsToSave(array $data) {
        $this->caseConceptsToSave = $data;
    }

    public function getCaseConceptsToSave(): array {
        return $this->caseConceptsToSave;
    }

    public function setSavedConcepts(Collection $data) {
        $this->savedConcepts = $data;
    }

    public function getSavedConcepts(): Collection {
        return $this->savedConcepts;
    }

    public function setCaseLicensesToSave(array $data) {
        $this->caseLicencesToSave = $data;
    }

    public function getCaseLicensesToSave(): array {
        return $this->caseLicencesToSave;
    }

    public function setSavedLicenses(Collection $data) {
        $this->savedLicenses = $data;
    }

    public function getSavedLicenses(): Collection {
        return $this->savedLicenses;
    }

    public function setCaseSubjectsToSave(array $data) {
        $this->caseSubjectsToSave = $data;
    }

    public function getCaseSubjectsToSave(): array {
        return $this->caseSubjectsToSave;
    }

    public function setSavedSubjects(Collection $data) {
        $this->savedSubjects = $data;
    }

    public function getSavedSubjects(): Collection {
        return $this->savedSubjects;
    }

    public function setSavedLanguages(Collection $data) {
        $this->savedLanguages = $data;
    }

    public function getSavedLanguages(): Collection {
        return $this->savedLanguages;
    }

    public function setOrgCode(string $data) {
        
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        return $this->organizationCode;
    }

    public function setExistingDataToBeDeleted($arrayItemIdToDeleteNode)
    {
        $this->arrayItemIdToDeleteNode = $arrayItemIdToDeleteNode;
    }

    public function getExistingDataToBeDeleted()
    {
        return $this->arrayItemIdToDeleteNode;
    }

    private function listCaseAndCustomMetadata() {
        $nodeType           =   '';

        $languageTitle      =   '';
        
        $subjectTitle           =   '';
        $subjectHierarchyCode   =   '';
        $subjectDescription     =   '';

        $conceptTitle           =   '';
        $conceptHierarchyCode   =   '';
        $conceptKeywords        =   '';
        $conceptDescription     =   '';

        $licenseTitle           =   '';
        $licenseText            =   '';
        $licenseDescription     =   '';

        $caseMetadataSet    =   [];
        $customMetadataSet  =   []; 

        $metadataList           =   $this->getMetadata()->toArray();
        $caseMetadataListFromDb =   array_filter($metadataList, function ($var) {
            return ($var['is_custom'] == '0');
        });

        $caseMetadataListFromDb = array_column($caseMetadataListFromDb, 'internal_name');

        $columnsFromHeaders  = $this->getColumnsFromHeader();
        foreach($columnsFromHeaders as $headers) {
            if (stripos($headers, 'case') !== false) {
                $caseMetadataSet[] = $headers;
            } else if(trim(preg_replace("/[^A-Za-z\s]/", "", $headers))){
                $customMetadataSet[] = $headers;
            }
        }
        
        if(sizeOf($caseMetadataSet) > 0) {
            foreach($caseMetadataSet as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(!in_array((string)$dbColumnFromCsvHeader, $caseMetadataListFromDb)) {
                    $arrayOfNonCaseMetadata[]   =   $metadata;  
                } else {
                    $result = '';  //set default value

                    foreach ( $caseMetadataSet as $key2 => $value ) {
                        if (false !== stripos($value,'case_title'))   // hasstack comes before needle
                        {
                            $result .= $key2;  // concat results
                            //return $result;
                            $title = $value;
                        }

                        if(false !== stripos($value,'case_node_type')) {
                            $nodeType = $value;
                        }
            
                        if(false !== stripos($value,'case_language')) {
                            $languageTitle = $value;
                        }
            
                        if(false !== stripos($value,'case_subject_title')) {
                            $subjectTitle = $value;
                        }
            
                        if(false !== stripos($value,'case_subject_hierarchy_code')) {
                            $subjectHierarchyCode = $value;
                        }
            
                        if(false !== stripos($value,'case_subject_description')) {
                            $subjectDescription = $value;
                        }
            
                        if(false !== stripos($value,'case_concept_title')) {
                            $conceptTitle = $value;
                        }
            
                        if(false !== stripos($value,'case_concept_keywords')) {
                            $conceptKeywords = $value;
                        }
            
                        if(false !== stripos($value,'case_concept_hierarchy_code')) {
                            $conceptHierarchyCode = $value;
                        }
            
                        if(false !== stripos($value,'case_concept_description')) {
                            $conceptDescription = $value;
                        }
            
                        if(false !== stripos($value,'case_license_title')) {
                            $licenseTitle = $value;
                        }
            
                        if(false !== stripos($value,'case_license_text')) {
                            $licenseText = $value;
                        }
            
                        if(false !== stripos($value,'case_license_description')) {
                            $licenseDescription = $value;
                        }
                    }

                    if ($result == '') {
                        return false;
                    }else {
                        $this->title                    =   $title;
                        $this->nodeType                 =   $nodeType;
                        $this->languageTitle            =   $languageTitle;

                        $this->subjectTitle             =   $subjectTitle;
                        $this->subjectHierarchyCode     =   $subjectHierarchyCode;
                        $this->subjectDescription       =   $subjectDescription;

                        $this->conceptTitle             =   $conceptTitle;
                        $this->conceptKeywords          =   $conceptKeywords;
                        $this->conceptHierarchyCode     =   $conceptHierarchyCode;
                        $this->conceptDescription       =   $conceptDescription;

                        $this->licenseTitle             =   $licenseTitle;
                        $this->licenseText              =   $licenseText;
                        $this->licenseDescription       =   $licenseDescription;
                        $this->setCaseMetadataSet($caseMetadataSet);
                        $this->setCustomMetadataSet($customMetadataSet);
                    }
                }
            }
        }
        else
        {
            return "InValidCaseHeader";
        }
    }

    private function extractAndSetCaseSubjectsToSave() {
        $inputData      =   $this->inputData;
        $subjectData    =   [];
        $subjectTitle           =   $this->subjectTitle;
        $subjectHierarchyCode   =   $this->subjectHierarchyCode;
        $subjectDescription     =   $this->subjectDescription;
        

        if(!empty($subjectTitle)) {
            foreach($inputData as $dataFromCsv) {
                $subjectData[]   = ["title" =>  $dataFromCsv[$subjectTitle], "hierarchyCode" => (!empty($subjectHierarchyCode) ? $dataFromCsv[$subjectHierarchyCode]: ''), 'description' => (!empty($subjectDescription) ? $dataFromCsv[$subjectDescription] : '')];

            }
        }

        $this->setCaseSubjectsToSave($subjectData);
    }

    private function extractAndSetCaseConceptsToSave() {
        $inputData      =   $this->inputData;
        $conceptData    =   [];
        $conceptTitle           =   $this->conceptTitle;
        $conceptKeywords        =   $this->conceptKeywords;
        $conceptHierarchyCode   =   $this->conceptHierarchyCode;
        $conceptDescription     =   $this->conceptDescription;


        if(!empty($conceptTitle)) {
            foreach($inputData as $dataFromCsv) {
                $conceptData[]   = ["title" =>  $dataFromCsv[$conceptTitle], 'keywords' => (!empty($conceptKeywords) ? $dataFromCsv[$conceptKeywords] : ''), "hierarchyCode" => (!empty($conceptHierarchyCode) ? $dataFromCsv[$conceptHierarchyCode]: ''), 'description' => (!empty($conceptDescription) ? $dataFromCsv[$conceptDescription] : '')];

            }
        }
        //dd($conceptData);
        $this->setCaseConceptsToSave($conceptData);
    }

    private function extractAndSetCaseLicensesToSave() {
        $inputData      =   $this->inputData;
        $licenseData    =   [];
        $licenseTitle           =   $this->licenseTitle;
        $licenseDescription     =   $this->licenseDescription;
        $licenseText            =   $this->licenseText;

        if(!empty($licenseTitle)) {
            foreach($inputData as $dataFromCsv) {
                $licenseData[]   = ["title" =>  $dataFromCsv[$licenseTitle], 'description' => (!empty($licenseDescription) ? $dataFromCsv[$licenseDescription] : ''), 'licenseText' => (!empty($licenseText) ? $dataFromCsv[$licenseText] : '') ];

            }
        }

        $this->setCaseLicensesToSave($licenseData);
    }

    private function fetchAndSetSavedSubjects() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("subject");
        $this->setSavedSubjects($savedData);
    }

    private function fetchAndSetSavedConcepts() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("concept");
        $this->setSavedConcepts($savedData);
    }

    private function fetchAndSetSavedLicenses() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("license");
        $this->setSavedLicenses($savedData);
    }

    private function fetchAndSetSavedLanguages() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("language");
        $this->setSavedLanguages($savedData);
    }

    private function helperToFetchTenantSpecificDataOfProvidedType(string $type): Collection {
        $organizationId = $this->getOrganizationIdentifier();
        $dataFetchingConditionalClause = [ "organization_id" => $organizationId, 'is_deleted' => 0];
        $returnCollection = true;
        switch ($type) {
            case 'concept':
                $fieldsToReturn = ['concept_id', 'keywords', 'title', 'hierarchy_code', 'description', 'source_concept_id'];
                $repository = $this->conceptRepository;
                break;
            case 'subject':
                $fieldsToReturn = ['subject_id', 'title', 'hierarchy_code', 'description', 'source_subject_id'];
                $repository = $this->subjectRepository;
                break;
            case 'license':
                $fieldsToReturn = ['license_id', 'title', 'license_text', 'source_license_id', 'description'];
                $repository = $this->licenseRepository;
                break;
            case 'node':
                $fieldsToReturn = ['node_type_id', 'title', 'type_code', 'is_document', 'is_default', 'source_node_type_id'];
                $repository = $this->nodeTypeRepository;
                break;
            case 'associationGroup':
                $fieldsToReturn = ['association_group_id', 'title', 'source_association_group_id'];
                $repository = $this->associationGroupRepository;
                break;
            case 'language':
                $fieldsToReturn = ['language_id', 'short_code'];
                $repository = $this->languageRepository;
                break;
            default:
                $repository = false;
                break;
        }

        $dataToReturn = $repository!==false ? 
                        $repository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $dataFetchingConditionalClause) : 
                        collect([]);
        return $dataToReturn;
    }

    /* private function helperToFetchLanguageIdFromEitherExistingOrNew(string $languageToSearchFor): string {
        $savedLanguages = $this->getSavedLanguages();
        $searchResult = $savedLanguages->where("name", $languageToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $language = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId =  $this->getOrganizationIdentifier();
            $newLanguageIdentifier = $this->createUniversalUniqueIdentifier();
            $languageDataToSave = [
                "language_id" => $newLanguageIdentifier,
                "name" => ucwords($languageToSearchFor),
                "short_code" => $languageToSearchFor,
                "organization_id" => $organizationId
            ];
            // save record to db
            $language = $this->languageRepository->saveData($languageDataToSave);
            // update the exiting inmemory list
            $savedLanguages->push($language);
        }
        return $language->language_id;
    } */

    private function helperToFetchLanguageIdFromEitherExistingOrNew(string $languageToSearchFor): string {
        $savedLanguages = $this->getSavedLanguages();
        $languageName = $languageToSearchFor;
        // added for multiple language handling start
        $languageToSearchFor = strtolower(substr($languageToSearchFor, 0, 2));
        if($languageToSearchFor=='ge')
            {
                $languageToSearchFor = 'de';
            }
        $searchResult = $savedLanguages->where("short_code", $languageToSearchFor);
        // added for multiple language handling end
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            // added for multiple language handling start
            $languageData = $savedLanguages->toArray();
            foreach($languageData as $languageDataK=>$languageDataV)
            {
                if($languageToSearchFor==$languageDataV['short_code'])
                {
                    $language = $languageDataV['language_id'];
                }
            }
            return $language;
            // added for multiple language handling end
            // $language = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId =  $this->getOrganizationIdentifier();
            $newLanguageIdentifier = $this->createUniversalUniqueIdentifier();
            $languageDataToSave = [
                "language_id" => $newLanguageIdentifier,
                "name" => $languageName,
                "short_code" => $languageToSearchFor,
                "organization_id" => $organizationId
            ];
            // save record to db
            $language = $this->languageRepository->saveData($languageDataToSave);
            // update the exiting inmemory list
            $savedLanguages->push($language);
            return $language->language_id;
        }
        
    }

    private function helperToFetchSubjectIdFromEitherExistingOrNew(string $subjectToSearchFor): string {
        $subject                    =   [];
        $requestImportType          =   $this->getImportType();
        $savedSubjects              =   $this->getSavedSubjects();
        $orgCode                    =   $this->getOrgCode();
        
        if(!empty($subjectToSearchFor)){
            $searchResult = $savedSubjects->where("title", $subjectToSearchFor);
        }
        
        // if found in memory then return id
        if(!empty($searchResult)) {
            $subject = $searchResult->first();
        }
        
        return !empty($subject->subject_id) ? $subject->subject_id : "";
    }

    private function helperToFetchConceptIdFromEitherExistingOrNew(string $conceptToSearchFor): string {
        $concept            =   [];
        $requestImportType  =   $this->getImportType();
        $savedConcepts      =   $this->getSavedConcepts();
        $orgCode            =   $this->getOrgCode();
        $searchResult       =   '';
        $conceptTitleToSearchFor = $conceptToSearchFor;

        if(!empty($conceptTitleToSearchFor)){
            $searchResult = $savedConcepts->where("title", $conceptTitleToSearchFor);
        }

        // if found in memory then return id
        if(!empty($searchResult)) {
            $concept = $searchResult->first();
        }
        
        return !empty($concept->concept_id) ? $concept->concept_id : "";
    }

    private function helperToFetchLicenseIdFromEitherExistingOrNew(string $licenseToSearchFor): string {
        $license            =   [];
        $requestImportType  =   $this->getImportType();
        $savedLicenses      =   $this->getSavedLicenses();
        $orgCode            =   $this->getOrgCode();
        $searchResult       =   '';
        $licenseTitleToSearchFor = $licenseToSearchFor;

        if(!empty($licenseTitleToSearchFor)){
            $searchResult = $savedLicenses->where("title", $licenseTitleToSearchFor);
        }

        // if found in memory then return id
        if(!empty($searchResult)) {
            $license = $searchResult->first();
        }
        
        return !empty($license->license_id) ? $license->license_id : "";
    }

    /**
     * Method to update the document in db or
     * create a new document in db
     *
     * @return void
     */
    private function updateDocumentData() {
        
        $count          =   $this->count;
        $blankCount     =   $this->blankCount;
        $newDocCount    =   $this->newDocCount;
        $newNodeCount   =   $this->newNodeCount;

        $languageTitle            =   $this->languageTitle;
        $subjectTitle             =   $this->subjectTitle;
        $subjectHierarchyCode     =   $this->subjectHierarchyCode;
        $subjectDescription       =   $this->subjectDescription;

        $licenseTitle           =   $this->licenseTitle;
        $licenseText            =   $this->licenseText;
        $licenseDescription     =   $this->licenseDescription;


        $inputData      =   $this->inputData;
        $requestImportType      =   $this->getImportType();
        $orgCode                =   $this->getOrgCode();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $metadataList           =   $this->getMetadata();
        $caseMetadata           =   $this->getCaseMetadataSet();
        $nodeType               =   $this->nodeType;
        $request                =   $this->request;

        foreach($metadataList as $metadataFromDb){
            $internalName          =  $metadataFromDb->toArray()['internal_name'];
            if(($metadataFromDb->toArray()['is_document'] == 1 || $metadataFromDb->toArray()['is_document'] == 2) && $metadataFromDb->toArray()['is_custom'] == 0 ){
                $metadataForDocument[] = $internalName;
            }
        }

        if (($key = array_search('subject_title', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('subject_hierarchy_code', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('subject_description', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        
        if (($key = array_search('license_title', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('license_text', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        if (($key = array_search('license_description', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }

        if (($key = array_search('language', $metadataForDocument)) !== false) {
            unset($metadataForDocument[$key]);
        }
        
        
        $this->setDocumentIdentifier(trim($inputData[0]['node_id']));
     

        if($requestImportType == 4) {
            $returnCollection   =   false;
            $fieldsToReturn     =   array_merge(['document_id', 'source_document_id', 'node_type_id', 'language_id', 'license_id'], $metadataForDocument);
            $keyValuePairs      =   ['source_document_id'  => trim($inputData[0]['node_id']), 'is_deleted'   =>  '0', 'organization_id'  =>  $organizationIdentifier];
            $relations          =   ['nodeType', 'subjects', 'license', 'language'];
            
            $documentNode   =   $this->documentRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $relations );
            $this->documentFromDB = $documentNode;
            
            if(!empty($subjectTitle)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $subjectTitle)[1]));
            }
            if(!empty($subjectHierarchyCode)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $subjectHierarchyCode)[1]));
            }
            if(!empty($subjectDescription)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $subjectDescription)[1]));
            }


            if(!empty($licenseTitle)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $licenseTitle)[1]));
            }
            if(!empty($licenseText)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $licenseText)[1]));
            }
            if(!empty($licenseDescription)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $licenseDescription)[1]));
            }

            if(!empty($languageTitle)) {
                array_push($metadataForDocument, strtolower(preg_split("/case_/i", $languageTitle)[1]));
            }
            
            foreach($caseMetadata as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(in_array((string)$dbColumnFromCsvHeader, $metadataForDocument)) {
                    if(!empty(trim($inputData[0][$metadata]))){

                            if($dbColumnFromCsvHeader == 'subject_title' || $dbColumnFromCsvHeader == 'subject_hierarchy_code' || $dbColumnFromCsvHeader == 'subject_description') {
                                $subjectId              =   !empty(trim($inputData[0][$subjectTitle])) ? 
                                $this->helperToFetchSubjectIdFromEitherExistingOrNew(trim($inputData[0][$subjectTitle])) : ""; 
                                
                                $getSubjectDetail = DB::table('document_subject')
                                                    ->select('subject_id')
                                                    ->where('document_id',$documentNode->document_id)
                                                    ->get()->toArray();
                                                    
                                if(!empty($getSubjectDetail)) {
                                    DB::table('document_subject')
                                    ->where('document_id', $documentNode->document_id)
                                    ->update(['subject_id' => $subjectId]);
                                } else {
                                   DB::table('document_subject')->insert(['document_id' => $documentNode->document_id, 'subject_id' => $subjectId]);
                                }
                                if(isset($documentNode->subjects) && !empty($documentNode->subjects->toArray())){
                                    $header = str_replace('subject_','',$dbColumnFromCsvHeader);
                                    foreach ($documentNode->subjects as $subjects) {
                                        if($subjects->$header != trim($inputData[0][$metadata])) {
                                            $count++;
                                            $this->count    =   $count;
                                        }
                                    } 
                                }
                                else{
                                    $count++;
                                    $this->count    =   $count;
                                }
                            }  elseif($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                $licenseId              =   !empty(trim($inputData[0][$licenseTitle])) ? 
                                $this->helperToFetchLicenseIdFromEitherExistingOrNew(trim($inputData[0][$licenseTitle])) : ""; 

                                $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id'  =>  $organizationIdentifier];
                
                                $attributes             =   ['license_id' => trim($licenseId)];
                                        
                                $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                $header = $dbColumnFromCsvHeader;
                                if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_description')
                                    $header = str_replace('license_','',$dbColumnFromCsvHeader);
                                if(!isset($documentNode->license->$header) || (isset($documentNode->license->$header) && $documentNode->license->$header != trim($inputData[0][$metadata]))) {
                                    $count++;
                                    $this->count    =   $count;
                                } 
                            } elseif($dbColumnFromCsvHeader == 'language') {
                                $languageId              =   !empty(trim($inputData[0][$languageTitle])) ? 
                                $this->helperToFetchLanguageIdFromEitherExistingOrNew(trim($inputData[0][$languageTitle])) : ""; 

                                $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id'  =>  $organizationIdentifier];
                
                                $attributes             =   ['language_id' => trim($languageId)];
                                        
                                $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                if(!isset($documentNode->language->name) || (isset($documentNode->language->name) && $documentNode->language->name != trim($inputData[0][$metadata]))) {
                                    $count++;
                                    $this->count    =   $count;
                                } 
                                
                            } elseif($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'){
                                
                                $inputData[0][$metadata] = trim($inputData[0][$metadata]);
                                $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
                                $attributes             =   [$dbColumnFromCsvHeader    =>  trim($inputData[0][$metadata])];
                
                                $documentUpdate =   $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                if($inputData[0][$metadata]!="0" && (!empty($inputData[0][$metadata]) && $this->validateDate($inputData[0][$metadata])) && $documentNode->$dbColumnFromCsvHeader != trim($inputData[0][$metadata])){                                                            
                                    $count++;
                                    $this->count    =   $count;
                                }
                            } else {
                                $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
                                $attributes             =   [$dbColumnFromCsvHeader    =>  trim($inputData[0][$metadata])];
                
                                $documentUpdate =   $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                if($documentNode->$dbColumnFromCsvHeader != trim($inputData[0][$metadata])) {                                
                                    $count++;
                                    $this->count    =   $count;
                                }
                            }
                            
                    } else {
                        if(isset($documentNode->$dbColumnFromCsvHeader) && $documentNode->$dbColumnFromCsvHeader == "0000-00-00 00:00:00" && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'))
                            $documentNode->$dbColumnFromCsvHeader = "";    
                        if(!empty($documentNode->$dbColumnFromCsvHeader) && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date')) {
                            $documentNode->$dbColumnFromCsvHeader = $this->formatDateTimeToDateOnly($documentNode->$dbColumnFromCsvHeader);
                        }
                        if($dbColumnFromCsvHeader == 'subject_title' || $dbColumnFromCsvHeader == 'subject_hierarchy_code' || $dbColumnFromCsvHeader == 'subject_description') {
                            if(trim($inputData[0][$subjectTitle]) == '') {
                                $subjectId              =   !empty(trim($inputData[0][$subjectTitle])) ? 
                                $this->helperToFetchSubjectIdFromEitherExistingOrNew(trim($inputData[0][$subjectTitle])) : ""; 
                                
                                $getSubjectDetail = DB::table('document_subject')
                                                    ->select('subject_id')
                                                    ->where('document_id',$documentNode->document_id)
                                                    ->get()->toArray();
                                                    
                                if(!empty($getSubjectDetail)) {
                                    DB::table('document_subject')
                                    ->where('document_id', $documentNode->document_id)
                                    ->update(['subject_id' => $subjectId]);
                                } else {
                                DB::table('document_subject')->insert(['document_id' => $documentNode->document_id, 'subject_id' => $subjectId]);
                                }
                                if(isset($documentNode->subjects) && !empty($documentNode->subjects->toArray())){
                                    $header = str_replace('subject_','',$dbColumnFromCsvHeader);
                                    foreach ($documentNode->subjects as $subjects) {
                                        if($subjects->$header != trim($inputData[0][$metadata])) {
                                            $count++;
                                            $this->count    =   $count;
                                        }
                                    } 
                                }
                            }
                        }  elseif($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description'  ) {
                            if(trim($inputData[0][$licenseTitle]) == '') {
                                $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
            
                                $attributes             =   ['license_id' => ''];
                                    
                                $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                $header = $dbColumnFromCsvHeader;
                                if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_description')
                                    $header = str_replace('license_','',$dbColumnFromCsvHeader);
                                if(isset($documentNode->license->$header) && $documentNode->license->$header != trim($inputData[0][$metadata])) {
                                    $count++;
                                    $this->count    =   $count;
                                } 
                            }         
                            
                        } elseif($dbColumnFromCsvHeader == 'language') {
                            if(trim($inputData[0][$languageTitle]) == '') {
                                $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
            
                                $attributes             =   ['language_id' => ''];
                                    
                                $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                if(isset($documentNode->language->name) && $documentNode->language->name != trim($inputData[0][$metadata])) {
                                    $count++;
                                    $this->count    =   $count;
                                }
                            }
                            
                        } else {                                
                            $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
                            $attributes             =   [$dbColumnFromCsvHeader   =>   trim($inputData[0][$metadata])];
                    
                            $documentUpdate =   $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                            if($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'){
                                $inputData[0][$metadata] = trim($inputData[0][$metadata]);
                                if($inputData[0][$metadata]!="0" && (!empty($inputData[0][$metadata]) && $this->validateDate($inputData[0][$metadata]))){
                                    $count++;
                                    $this->count    =   $count;
                                }
                            }
                            else if($documentNode->$dbColumnFromCsvHeader != trim($inputData[0][$metadata])) {
                                $count++;
                                $this->count    =   $count;
                            }
                            
                        }                 
                    }
                }
            }

            $attributes =   ['organization_id' => $organizationIdentifier, 'is_document'    =>  '1'];
            $getDocumentNode =   $this->nodeTypeRepository->findByAttributes($attributes)->first();

            $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
            $nodeTypeAttributes =   ['node_type_id'   =>  $getDocumentNode->node_type_id];

            $documentUpdate =   $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $nodeTypeAttributes);

            if(empty(trim($inputData[0][$nodeType]))){
                $blankCount++;
                $this->blankCount    =   $blankCount;
            }

            $columnValueFilterPairs =   ['source_document_id'  =>  trim($inputData[0]['node_id']), 'organization_id' =>  $organizationIdentifier, 'is_deleted' => '0'];
            $attributesToUpdate     =   ['import_type' => $requestImportType];
            $documentUpdate =   $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs, $attributesToUpdate);
        } else {
            $subjectId          =   '';
            $returnCollection   =   false;
            $fieldsToReturn     =   array_merge(['document_id', 'source_document_id', 'node_type_id'], $metadataForDocument);
            $keyValuePairs      =   ['source_document_id'  => trim($inputData[0]['node_id']), 'is_deleted'   =>  '0', 'organization_id'  =>  $organizationIdentifier];
            $relations          =   ['nodeType'];

            $documentNode   =   $this->documentRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $relations );
            
            $userId             =   $this->userId;
            $currentDateTime    =   $this->createDateTime();
            $documentIdentifier =   $this->createUniversalUniqueIdentifier();
            $userOrganizationId =   $organizationIdentifier;

            $this->setCreatedDocumentIdentifier($documentIdentifier);
            
            $organizationName   = DB::table('organizations')->select('name')->where('organization_id', $userOrganizationId)->first()->name;

            // $languageId =  !empty($request['language_id']) ? 
            //                 $this->helperToFetchLanguageIdFromEitherExistingOrNew('en') : 
            //                 "";
            if(!array_key_exists('case_language',$inputData[0]))
            {
                // $inputData[0]['case_language'] = 'English';
                $languageId = "";

            }
            else
            {
                $languageId              =   !empty(trim($inputData[0]['case_language'])) ? 
                            $this->helperToFetchLanguageIdFromEitherExistingOrNew(trim($inputData[0]['case_language'])) : "";
            }                

            $attributes =   ['organization_id' => $organizationIdentifier, 'is_document'    =>  '1'];
            $getDocumentNode =   $this->nodeTypeRepository->findByAttributes($attributes)->first();
            $data = [
                "document_id"           =>  $documentIdentifier,
                //"title"                 =>  !empty($documentNode->document_id) ? trim($inputData[0]['title'])."-".$currentDateTime : trim($inputData[0]['title']),
                "creator"               =>  $organizationName,
                "language_id"           =>  $languageId,
                "organization_id"       =>  $userOrganizationId,
                "document_type"         =>  '1',
                "created_at"            =>  $currentDateTime,
                "updated_at"            =>  $currentDateTime,
                "source_document_id"    =>  $documentIdentifier,
                "node_type_id"          =>  $getDocumentNode->node_type_id,
                "node_template_id"      =>  !empty($request['node_template_id']) ? $request['node_template_id'] : '',
                "updated_by"            =>  $userId,
                "import_type"           =>  $requestImportType,
                "uri"                   =>  $this->getCaseApiUri("CFDocument",$documentIdentifier,true,$orgCode,$this->domainName),
                "created_by"            =>  $userId,
                "import_status"         =>  1 // to indicate import is in progress and to not show in taxnonomy listing until import is completed
            ];

            if(!empty($subjectTitle)) {
                array_push($metadataForDocument, preg_split("/case_/i", $subjectTitle)[1]);
            }
            if(!empty($subjectHierarchyCode)) {
                array_push($metadataForDocument, preg_split("/case_/i", $subjectHierarchyCode)[1]);
            }
            if(!empty($subjectDescription)) {
                array_push($metadataForDocument, preg_split("/case_/i", $subjectDescription)[1]);
            }


            if(!empty($licenseTitle)) {
                array_push($metadataForDocument, preg_split("/case_/i", $licenseTitle)[1]);
            }
            if(!empty($licenseText)) {
                array_push($metadataForDocument, preg_split("/case_/i", $licenseText)[1]);
            }
            if(!empty($licenseDescription)) {
                array_push($metadataForDocument, preg_split("/case_/i", $licenseDescription)[1]);
            }

            foreach($caseMetadata as $metadata){
                $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                if(in_array((string)$dbColumnFromCsvHeader, $metadataForDocument)) {
                    if($dbColumnFromCsvHeader == 'title') {
                        $data[$dbColumnFromCsvHeader] = !empty($documentNode->document_id) ? trim($inputData[0][$metadata])."-".$currentDateTime : trim($inputData[0][$metadata]);
                    } elseif($dbColumnFromCsvHeader == 'subject_title') {
                        $subjectId              =   !empty($inputData[0][$subjectTitle]) ? 
                        $this->helperToFetchSubjectIdFromEitherExistingOrNew($inputData[0][$subjectTitle]) : ""; 
                        
                    }  elseif($dbColumnFromCsvHeader == 'language') {
                        $languageId              =   !empty(trim($inputData[0][$languageTitle])) ? 
                        $this->helperToFetchLanguageIdFromEitherExistingOrNew(trim($inputData[0][$languageTitle])) : ""; 

                        $data['language_id'] =   $languageId;
                        
                    } elseif($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                        $licenseId              =   !empty(trim($inputData[0][$licenseTitle])) ? 
                        $this->helperToFetchLicenseIdFromEitherExistingOrNew(trim($inputData[0][$licenseTitle])) : ""; 
                        $data['license_id'] =   $licenseId;
                    } 
                    else {
                        $data[$dbColumnFromCsvHeader] = trim($inputData[0][$metadata]);
                    }
                }
            }
            $document = $this->documentRepository->saveData($data); 
            if(!empty($subjectId)) {
                DB::table('document_subject')->insert(['document_id' => $document->document_id, 'subject_id' => $subjectId]);
            }
            $count++;
            $this->count    =   $count;
            $newDocCount++;
            $this->newDocCount    =   $newDocCount; 
        } 
        
    }

    /**
     * Method to update the item in db or
     * create new item in db
     *
     * @return void
     */
    private function createItemIdForNewItemsToBeAdded() {
        $arrayOfCreatedItemId   =   [];
        $getNewItemArray    =   $this->getNewItemArray();
        if(count($getNewItemArray) > 0) {
            foreach($getNewItemArray as $newItem) {
                $arrayOfCreatedItemId[]   =   [trim($newItem['node_id'])    =>  $this->createUniversalUniqueIdentifier()];
            }
        }

        $this->setArrayOfCreatedItemId($arrayOfCreatedItemId);
    }

    /**
     * Method to search the node_id of the selected item's parent_node_id
     * in recursive array
     *
     * @param array $array
     * @param string $key
     * @param string $value
     * @return void
     */
    private function searchForParentNode(array $array, string $key, string $value)
    {
        $results = array();

        foreach($array as $itemCreated) {
            if (is_array($itemCreated) == true) {
                if (isset($itemCreated[$key]) && $itemCreated[$key] == $value) {
                    $results[] = $itemCreated;
                }

                foreach ($itemCreated as $subarray) {
                    if(is_array($subarray) == true) {
                        $results = array_merge($results, $this->searchForParentNode($subarray, $key, $value));
                    }                
                }
            }
        }
        return $results;
    }

    /**
     * Method to set the parent node id for newly created item
     *
     * @param string $nodeParentIdToSearchWith
     * @return void
     */
    private function setParentIdForNewItem(string $nodeParentIdToSearchWith) {
        $parentId               =   '';
        $getNewItemArray        =   $this->getNewItemArray();
        $arrayOfCreatedItemId   =   $this->getArrayOfCreatedItemId();
    
        $arrayOfParentId    =   $this->searchForParentNode($getNewItemArray, 'node_id', $nodeParentIdToSearchWith);
        foreach($arrayOfCreatedItemId as $item) {
            $keys   =   array_keys($item);
            if($keys[0] == $arrayOfParentId[0]['node_id']) {
                return $item[$keys[0]];
            }
        } 
    }

    private function createNewNodeTypeAndMapWithMetadata($nodeTypeTitle, $organizationIdentifier) {
        $nodeTypeId = $this->createUniversalUniqueIdentifier();
        //$inputMetaData = ['title' => $nodeType, 'organization_id' => $organizationIdentifier, 'node_type_id' => $nodeTypeId, 'source_node_type_id'   => $nodeTypeId, 'created_by' => $this->userId, 'updated_by' =>  $this->userId];
        $orgCode = $this->getOrgCode();
        $nodeTypeCreated = DB::table('node_types')->insert(
            ['node_type_id' => $nodeTypeId, 'organization_id' => $organizationIdentifier, 'title' => $nodeTypeTitle,  'source_node_type_id'   => $nodeTypeId, 'created_by' => $this->userId, 'updated_by' =>  $this->userId,'uri' =>$this->getCaseApiUri("CFItemTypes",$nodeTypeId,true,$orgCode,$this->domainName)]
        );

        if($nodeTypeCreated == true) {
            $nodeTypeCreatedDetails =   $this->nodeTypeRepository->find($nodeTypeId);
            //dd($nodeTypeCreatedDetails);

            $metadataList   =   $this->metadataListForATenant;

            foreach($metadataList['metadata'] as $metadata) {
                if($metadata['is_document'] != 1 && $metadata['is_custom']  ==  0){
                    $metadataIdArray['metadata'][]  = ['id' => $metadata['metadata_id'], 'order'  =>  $metadata['order'], 'is_mandatory' => $metadata['is_mandatory']];
                } 
            }

            $this->nodeTypeRepository->saveNodeTypeMetadata($nodeTypeCreatedDetails->node_type_id, $metadataIdArray);

            return $nodeTypeCreatedDetails->node_type_id;
        } else {
        }
    }

    /**
     * Method to update existing item in db or
     * to create new item
     *
     * @return void
     */
    private function updateItemData() {
        $key                    =   1;
        $nodeTypeFromCsv        =   $this->nodeType;

        $conceptTitle           =   $this->conceptTitle;
        $conceptKeywords        =   $this->conceptKeywords;
        $conceptHierarchyCode   =   $this->conceptHierarchyCode;
        $conceptDescription     =   $this->conceptDescription;

        $licenseTitle           =   $this->licenseTitle;
        $licenseText            =   $this->licenseText;
        $licenseDescription     =   $this->licenseDescription;

        $languageTitle           =   $this->languageTitle;

        $newNodeCount           =   $this->newNodeCount;
        $blankCount             =   $this->blankCount;
        $packageExists          =   $this->packageExists;
        $documentFromDb         =   $this->documentFromDB;

        $requestImportType      =   $this->getImportType();
        $inputData              =   $this->inputData;
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $documentIdentifier     =   $this->getCreatedDocumentIdentifier();
        $orgCode                =   $this->getOrgCode();
        
        $arrayOfDocumentNodeTypeId  =   $this->getNodeTypeIdArray();
        $arrayOfItemNodeTypeId      =   []; //$this->getNodeTypeIdArray();

        $parentUpdated              =   0;
        $itemUpdated                =   0;
        $nodeTypeUpdated            =   0;
        $educationLevelUpdated      =   0;
        $itemChangedArray           =   [];

        $nodeTypeUriObject          =   '';
        $oldParentId = $inputData[0]['node_id'];

        unset($inputData[0]);
        $metadataIdArray        =   [];
        $newItemArray           =   [];
        $itemsToSave            =   [];
        $updatedItemArray       =   [];
        $itemIdFromDb           =   [];
        $sourceItemIdFromDb     =   [];
        $itemConceptId          =   [];

        $itemsFromCSV           =   $inputData;
        $userId                 =   $this->userId;
        $currentDateTime        =   $this->createDateTime();

        $metadataList           =   $this->getMetadata();
        $caseMetadata           =   $this->getCaseMetadataSet();
        $newAssociationsCount = $this->newAssociationsCount;
        $deleteNodeCount = $this->deleteNodeCount;

        foreach($metadataList as $metadataFromDb){
            $internalName          =  $metadataFromDb->toArray()['internal_name'];
            if($metadataFromDb->toArray()['is_document'] != 1 && $metadataFromDb->toArray()['is_custom'] == 0){
                if(strpos($internalName, 'pacing') !== FALSE) {
                    
                } else {
                    $metadataForItem[] = $internalName;
                }
            }
        }
        /**
         * Prepare array of Items received from csv file
         */
        $csv_seq = 1;
        foreach($itemsFromCSV  as $key => $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
            $itemsFromCSV[$key]['csv_sequence'] = $csv_seq;
            $csv_seq++;
        }
        $inputData = $itemsFromCSV;
        /**
         * Fetch item detail from db for the item_id from csv file
         */
        $attributeIn = "source_item_id";
        $containedInValues = $itemIdArray;
        $returnCollection = true;
        
        if (($key = array_search('concept_title', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('concept_hierarchy_code', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('concept_keywords', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('concept_description', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        
        if (($key = array_search('license_title', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('license_text', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }
        if (($key = array_search('license_description', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }

        if (($key = array_search('language', $metadataForItem)) !== false) {
            unset($metadataForItem[$key]);
        }



        $fieldsToReturn     =   array_merge(["item_id", "source_item_id", "parent_id", "node_type_id", "concept_id", "license_id", "language_id", "updated_at"], $metadataForItem);
        $relations =    ['nodeType', 'concept', 'license', 'language'];

        $keyValuePairs = ['is_deleted'   =>  '0', 'organization_id' => $organizationIdentifier];

        $returnPaginatedData = false;
        $paginationFilters = ['limit' => 10, 'offset' => 0];
        $returnSortedData = false;
        $sortfilters = ['orderBy' => 'list_enumeration', 'sorting' => 'desc'];
        $operator = 'AND';
        
        $itemCollectionFromCsv =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery($attributeIn, $containedInValues, $returnCollection, $fieldsToReturn, $keyValuePairs, $returnPaginatedData, $paginationFilters, $returnSortedData, $sortfilters, $operator, $relations);

        $itemCollectionArray =  $itemCollectionFromCsv->toArray();
        foreach($itemCollectionArray as $itemFromDb) {
            $itemIdFromDb[$itemFromDb['source_item_id']]    =   $itemFromDb['item_id'];
            $sourceItemIdFromDb[]                           =   $itemFromDb['source_item_id'];
            $itemParentIdFromDb[]                           =   $itemFromDb['parent_id'];
            //$itemConceptId[$itemFromDb['source_item_id']]   =   $itemFromDb['concept_id'];

        }


        if(!empty($conceptTitle)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptTitle)[1]));
        }
        if(!empty($conceptKeywords)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptKeywords)[1]));
        }
        if(!empty($conceptHierarchyCode)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptHierarchyCode)[1]));
        }
        if(!empty($conceptDescription)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $conceptDescription)[1]));
        }

        if(!empty($licenseTitle)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $licenseTitle)[1]));
        }
        if(!empty($licenseText)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $licenseText)[1]));
        }
        if(!empty($licenseDescription)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $licenseDescription)[1]));
        }

        if(!empty($languageTitle)) {
            array_push($metadataForItem, strtolower(preg_split("/case_/i", $languageTitle)[1]));
        }

        $itemIdNotInDb = array_diff($itemIdArray, $sourceItemIdFromDb);
        
        array_push($metadataForItem, 'node_type');
        if($requestImportType == 4) {

            /**
             * Data filtered to get unique node id based on node_id and action and case field start
             */
            
            $itemsFromCSVCopy = $inputData;
            $getUniqueitemIdArray = array();
            $getUniqueitemIdArray = array_unique($itemIdArray);
            
            $getItemArrayDataToInsert = array();
            $actionMappedWithOldItemId= array();
            $getNodeIdMappedWithParentId = array();
            foreach($getUniqueitemIdArray as $getUniqueitemIdArrayK=>$getUniqueitemIdArrayV)
            {
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyK=>$itemsFromCSVCopyV)
                {
                    // added for converting array keys to lower case start
                    $itemsFromCSVCopyV = array_change_key_case($itemsFromCSVCopyV,CASE_LOWER);
                    // added for converting array keys to lower case end
                    $associationType2 = strtolower($itemsFromCSVCopyV['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyV['action']));
                    $inludeChildOfArray = array('ischildof','');
                    $actionsToConsider = array('a','u','d','add','update','delete','');

                    if( ($getUniqueitemIdArrayV==trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_id']))) && (in_array((string)$actionType,$actionsToConsider)) && (!empty($itemsFromCSVCopyV['case_full_statement'])) && (in_array((string)$associationType2,$inludeChildOfArray)) )
                    {
                            $getItemArrayDataToInsert[]=$itemsFromCSVCopyV;
                            $getNodeIdMappedWithParentId[$itemsFromCSVCopyV['node_id']] =$itemsFromCSVCopyV['node_parent_id'];
                            $actionMappedWithOldItemId[trim($itemsFromCSVCopyV['node_id'])]=$itemsFromCSVCopyV['action'];
                    }
                }
            }
            
            $itemArrayDataCopy = array();
            foreach($getItemArrayDataToInsert as $getItemArrayDataToInsertK=>$getItemArrayDataToInsert)
            {
                $itemArrayDataCopy[$getItemArrayDataToInsertK+1] = $getItemArrayDataToInsert;
            }
            
            $inputData = $itemArrayDataCopy;
            /**
             * Data filtered to get unique node id based on node_id and action and case field end
             */

            if(count($sourceItemIdFromDb) > 0) {
                foreach($inputData as $item) {
                    if(in_array(trim($item['node_id']), $sourceItemIdFromDb) == true && empty($item['association_type']) && !empty($item['case_full_statement']) && in_array(strtolower($item['action']),['d','delete'])) {
                            $deleteNodeCount++;                                    
                    }
                }
                $this->deleteNodeCount = $deleteNodeCount;
                foreach($caseMetadata as $metadata){
                    $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);

                    if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                        
                        //Create newReset Array with value from csv
                        foreach($inputData as $item) {
                            if(in_array(trim($item['node_id']), $sourceItemIdFromDb) == true) {
                                $newReset[trim($item['node_id'])]['node_id'] = trim($item['node_id']);
                                $newReset[trim($item['node_id'])]['node_parent_id'] = trim($item['node_parent_id']);
                                $newReset[trim($item['node_id'])][$metadata]        = trim($item[strtolower($metadata)]);
                                $newReset[trim($item['node_id'])]['action'] = isset($item['action']) && !empty($item['action']) ? strtolower(trim($item['action'])) : '';
                            }
                        }
    
                        //Create itemArray with value from db
                        foreach($itemCollectionFromCsv as $key => $items) {
                            
                            $itemArray[$key]['node_id']                 = !empty($items->source_item_id) ? trim($items->source_item_id) : "";
                            $itemArray[$key]['node_parent_id']          = !empty($items->parent_id) ? $items->parent_id : "";
    
    
                            if($dbColumnFromCsvHeader == 'title') {
                                $itemArray[$key][$dbColumnFromCsvHeader]    = $items->full_statement;
                            } else if($dbColumnFromCsvHeader == 'language') {
                                $itemArray[$key]['language']               = !empty($items->language_id) ? $items->language->name : "";
                                
                            } else if($dbColumnFromCsvHeader == 'concept_title' || $dbColumnFromCsvHeader == 'concept_hierarchy_code' || $dbColumnFromCsvHeader == 'concept_keywords' || $dbColumnFromCsvHeader == 'concept_description') {
                                $itemArray[$key]['concept_title']               = !empty($items->concept_id) ? $items->concept->title : "";
                                $itemArray[$key]['concept_hierarchy_code']               = !empty($items->concept_id) ? $items->concept->hierarchy_code : "";
                                $itemArray[$key]['concept_keywords']               = !empty($items->concept_id) ? $items->concept->keywords : "";
                                $itemArray[$key]['concept_description']               = !empty($items->concept_id) ? $items->concept->description : "";
                            } else if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                $itemArray[$key]['license_title']               = !empty($items->license_id) ? $items->license->title : "";
                                $itemArray[$key]['license_text']               = !empty($items->license_id) ? $items->license->license_text : "";
                                $itemArray[$key]['license_description']               = !empty($items->license_id) ? $items->license->description : "";
                            } else {
                                $itemArray[$key][$dbColumnFromCsvHeader]    = !empty($items->$dbColumnFromCsvHeader) ?  $items->$dbColumnFromCsvHeader : "";
                            }
    
                            $itemArray[$key]['node_type']               = !empty($items->nodeType->title) ? $items->nodeType->title : "";

                            
                        }

                        $this->setItemCollectionFromDb($itemArray);
                    }
                }
                $oldReset = [];
                $toAdd    = [];
                foreach( $itemArray as $items ) {
                    $oldReset[$items['node_id']] = $items;
                }
    
                foreach( $newReset as $key => $val ) {
                    if( isset( $oldReset[$key] ) ) {
                        $toAdd[$key] = $val;
                    }
                }
                foreach($oldReset as $itemId => $data) {                   
                    if(!empty($toAdd[$itemId]['node_parent_id'])) {
                        if(trim($toAdd[$itemId]['node_parent_id']) !== $data['node_parent_id']) {
                            if(!in_array($toAdd[$itemId]['action'],['d','delete'])){
                                $itemChangedArray[$itemId]['old_parent_id'] =   $data['node_parent_id'];
                                $itemChangedArray[$itemId]['new_parent_id'] =   $toAdd[$itemId]['node_parent_id'];
                            }                            

                            $columnValueFilterPairs =   ['item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier];
            
                            $attributes             =   ['parent_id' => trim($toAdd[$itemId]['node_parent_id'])];
                            $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);

                            $columnValueFilterPairsForAssociation =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier];
                            
                            $attributesForAssociation             =   ['target_item_id' => trim($toAdd[$itemId]['node_parent_id'])];
                            
                            $associationData = ['source_item_id'  =>  $itemId, 'association_type'  =>  1,'target_item_id' => trim($toAdd[$itemId]['node_parent_id'])];
                            
                            $count = $this->checkAssociationExist($associationData);
                            if($count==2)
                            {
                                $this->itemAssociationRepository->editWithCustomizedFields($columnValueFilterPairsForAssociation, $attributesForAssociation);
                            }                                    

                            $parentUpdated   =   1;
    
                        } else {
                            $parentUpdated   =   0;
                        }
                    } else {
                        $blankCount++;
                        $this->blankCount    =   $blankCount;
                    }
    
                    
                    foreach($caseMetadata as $metadata) {
                        $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                        if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem)) {
                            if(isset($toAdd[$itemId][$metadata]) && $data[$dbColumnFromCsvHeader] != trim($toAdd[$itemId][$metadata])) {
                            if($dbColumnFromCsvHeader == 'node_type') {
                                if($toAdd[$itemId][$nodeTypeFromCsv] !== $data['node_type']) {
                                    $nodeType       =   trim($toAdd[$itemId][$nodeTypeFromCsv]);
                                    $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $nodeType, 'is_deleted'   =>  '0' ];
                                    $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);

                                    if(!empty($nodeType)) {
                                        if(count($nodeTypeExist) == 0) {
                                            $nodeTypeId         =   $this->createNewNodeTypeAndMapWithMetadata($nodeType, $organizationIdentifier);
                                            $arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];

                                            $nodeTypeUriObject  =   json_encode(
                                                [
                                                    "identifier"    =>  $nodeTypeId,
                                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeId,  true, $orgCode,$this->domainName),
                                                    "title"         =>  $nodeType
                                                ]
                                            );
                                            if(!in_array($toAdd[$itemId]['action'],['d','delete'])){    
                                                $itemChangedArray[$itemId]['old_node_type'] =   $data['node_type'];
                                                $itemChangedArray[$itemId]['new_node_type'] = $nodeType;
                                            }

                                            $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                        
                                            $metadataAttributes             =   ['node_type_id' => $nodeTypeId, 'source_node_type_uri_object' => $nodeTypeUriObject];

                                            $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $metadataAttributes);

                                        } else {
                                            $nodeTypeUriObject  =   json_encode(
                                                [
                                                    "identifier"    =>  $nodeTypeExist[0]->node_type_id,
                                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeExist[0]->node_type_id,  true, $orgCode,$this->domainName),
                                                    "title"         =>  $nodeType
                                                ]
                                            );
                                            if(!in_array($toAdd[$itemId]['action'],['d','delete'])){
                                                $itemChangedArray[$itemId]['old_node_type'] =   $data['node_type'];
                                                $itemChangedArray[$itemId]['new_node_type'] = $nodeType;
                                            }

                                            $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                    
                                            $metadataAttributes             =   ['node_type_id' => $nodeTypeExist[0]->node_type_id, 'source_node_type_uri_object' => $nodeTypeUriObject];

                                            $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $metadataAttributes);
                                        }
                                    } else {
                                        $attributes     =   ['organization_id'  => $organizationIdentifier, 'is_default' => '1', 'is_deleted'   =>  '0' ];
                                        $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);
                    
                                        if(count($nodeTypeExist) > 0) {
                                            $nodeTypeUriObject  =   json_encode(
                                                [
                                                    "identifier"    =>  $nodeTypeExist[0]->node_type_id,
                                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeExist[0]->node_type_id,  true, $orgCode,$this->domainName),
                                                    "title"         =>  'Default'
                                                ]
                                            );

                                            $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                    
                                            $metadataAttributes             =   ['node_type_id' => $nodeTypeExist[0]->node_type_id, 'source_node_type_uri_object' => $nodeTypeUriObject];

                                            $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $metadataAttributes);
                                        
                                        } else {
                                            $nodeTypeId =    $this->createNewNodeTypeAndMapWithMetadata('Default', $organizationIdentifier);
                            
                                            $nodeTypeUriObject  =   json_encode(
                                                [
                                                    "identifier"    =>  $nodeTypeId,
                                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeId,  true, $orgCode,$this->domainName),
                                                    "title"         =>  'Default'
                                                ]
                                            );
                            
                                            $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                    
                                            $metadataAttributes             =   ['node_type_id' => $nodeTypeId, 'source_node_type_uri_object' => $nodeTypeUriObject];

                                            $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $metadataAttributes);
                                        }
                                    }
                                }
                            } elseif($dbColumnFromCsvHeader == 'language') {
                                $languageId              =   !empty($toAdd[$itemId][$languageTitle]) ? 
                                $this->helperToFetchLanguageIdFromEitherExistingOrNew(trim($toAdd[$itemId][$languageTitle])) : ""; 
                                if(!in_array($toAdd[$itemId]['action'],['d','delete'])){
                                    $itemChangedArray[$itemId]['old_'.$dbColumnFromCsvHeader] =   $data[$dbColumnFromCsvHeader];
                                    $itemChangedArray[$itemId]['new_'.$dbColumnFromCsvHeader] =   $toAdd[$itemId][$languageTitle];
                                }        
                                $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                
                                $attributes             =   ['language_id' => trim($languageId)];
                                        
                                $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                
                            }elseif($dbColumnFromCsvHeader == 'concept_title' || $dbColumnFromCsvHeader == 'concept_keywords' || $dbColumnFromCsvHeader == 'concept_hierarchy_code' || $dbColumnFromCsvHeader == 'concept_description') {
                                $conceptId              =   !empty($toAdd[$itemId][$conceptTitle]) ? 
                                $this->helperToFetchConceptIdFromEitherExistingOrNew($toAdd[$itemId][$conceptTitle]) : ""; 
                                if(!in_array($toAdd[$itemId]['action'],['d','delete'])){
                                    $itemChangedArray[$itemId]['old_'.$dbColumnFromCsvHeader] =   $data[$dbColumnFromCsvHeader];
                                    $itemChangedArray[$itemId]['new_'.$dbColumnFromCsvHeader] =   $toAdd[$itemId][$conceptTitle];
                                }      
                                $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                
                                $attributes             =   ['concept_id' => trim($conceptId)];
                                        
                                $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                
                            } elseif($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                $licenseId              =   !empty($toAdd[$itemId][$licenseTitle]) ? 
                                $this->helperToFetchLicenseIdFromEitherExistingOrNew($toAdd[$itemId][$licenseTitle]) : ""; 
                                if(!in_array($toAdd[$itemId]['action'],['d','delete'])){
                                    $itemChangedArray[$itemId]['old_'.$dbColumnFromCsvHeader] =   $data[$dbColumnFromCsvHeader];
                                    $itemChangedArray[$itemId]['new_'.$dbColumnFromCsvHeader] =   $toAdd[$itemId][$licenseTitle];
                                }
                                $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                
                                $attributes             =   ['license_id' => trim($licenseId)];
                                        
                                $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                
                            } else {
                                if($data[$dbColumnFromCsvHeader] == "0000-00-00 00:00:00" && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'))
                                    $data[$dbColumnFromCsvHeader] = "";
                                if(!empty($toAdd[$itemId][$metadata])){
                                    if(!empty($data[$dbColumnFromCsvHeader]) && ($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date')) {
                                        $data[$dbColumnFromCsvHeader] = $this->formatDateTimeToDateOnly($data[$dbColumnFromCsvHeader]);
                                    }
                                    if($toAdd[$itemId][$metadata] !== $data[$dbColumnFromCsvHeader]) {
                                        if(!in_array($toAdd[$itemId]['action'],['d','delete'])){
                                            if($dbColumnFromCsvHeader == 'status_start_date' || $dbColumnFromCsvHeader == 'status_end_date'){
                                                $toAdd[$itemId][$metadata] = trim($toAdd[$itemId][$metadata]);
                                                if($toAdd[$itemId][$metadata]!="0" && (!empty($toAdd[$itemId][$metadata]) && $this->validateDate($toAdd[$itemId][$metadata]))){
                                                    $itemChangedArray[$itemId]['old_'.$dbColumnFromCsvHeader] =   $data[$dbColumnFromCsvHeader];
                                                    $itemChangedArray[$itemId]['new_'.$dbColumnFromCsvHeader] =   trim($toAdd[$itemId][$metadata]);
                                                }
                                            }
                                            else{
                                                $itemChangedArray[$itemId]['old_'.$dbColumnFromCsvHeader] =   $data[$dbColumnFromCsvHeader];
                                                $itemChangedArray[$itemId]['new_'.$dbColumnFromCsvHeader] =   trim($toAdd[$itemId][$metadata]);
                                            }
                                        }
                                        
                                        $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
                
                                        $attributes             =   [$dbColumnFromCsvHeader => trim($toAdd[$itemId][$metadata])];

                                        $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);

                                        if($dbColumnFromCsvHeader=='full_statement')
                                        {
                                            $dbColumnFromCsvHeaderhtml = $dbColumnFromCsvHeader.'_html';

                                            $attributes             =   [$dbColumnFromCsvHeaderhtml => trim($toAdd[$itemId][$metadata])];
                                        
                                            $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);

                                        }
                                        
                                    }
                                } else {
                                    if($data[$dbColumnFromCsvHeader] != null) {
                                        $columnValueFilterPairs =   ['source_item_id'  =>  $itemId, 'organization_id'  =>  $organizationIdentifier, 'is_deleted' => '0'];
    
                                        $attributes             =   [$dbColumnFromCsvHeader => ""];
                                        
                                        $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
                                        $blankCount++;
                                        $this->blankCount    =   $blankCount;
                                    }
                                }
                            }
                        }

                        }
                    }

                }
                $this->setUpdatedItemArray(array_keys($itemChangedArray));
                /**
                 *   Creating Array for Associations start
                 */

                $mainAssociationArray = array(1=>'ischildof',2=>'ispeerof',3=>'ispartof',4=>'exactmatchof',5=>'precedes',6=>'isrelatedto',7=>'replacedby',9=>'hasskilllevel'); 

                 /**
                 *   Creating Array for Associations end
                 */


                 /**
                  * sorting associations to be deleted (exluding ischildof) relation start 
                  */
                  
                $arrayOfAssociationsToBeDeletedExIsChildOf = array();  
                $dl=0;
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyK=>$itemsFromCSVCopyV)
                {
                    // added for converting array keys to lower case start
                    $itemsFromCSVCopyV = array_change_key_case($itemsFromCSVCopyV,CASE_LOWER);
                    // added for converting array keys to lower case end
                    $associationType = strtolower($itemsFromCSVCopyV['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyV['action']));
                    $exludeChildOfArray = array('ispeerof','ispartof','exactmatchof','precedes','isrelatedto','replacedby','haspart','hasskilllevel');
                    $actionsToConsider = array('d','delete');

                    if( in_array((string)$actionType,$actionsToConsider) && in_array(trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_id'])),$sourceItemIdFromDb) && (in_array((string)$associationType,$exludeChildOfArray)) )
                    {
                        $associationTypeRelation = array_keys($mainAssociationArray,strtolower(trim(!empty($itemsFromCSVCopyV['case_association_type'])?$itemsFromCSVCopyV['case_association_type']:'ischildof')));
                        $arrayOfAssociationsToBeDeletedExIsChildOf[$dl]['source_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_id']));
                        $arrayOfAssociationsToBeDeletedExIsChildOf[$dl]['target_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_parent_id']));
                        $arrayOfAssociationsToBeDeletedExIsChildOf[$dl]['association_type'] = $associationTypeRelation[0];
                        $dl = $dl+1;
                    }
                }
                /**
                  * sorting associations to be deleted (exluding ischildof) relation end
                */


                /**
                  * sorting associations to be deleted (include ischildof) relation start 
                  */
                  
                  $arrayOfAssociationsToBeDeletedInIsChildOf = array();  
                  $dli=0;
                  foreach($itemsFromCSVCopy as $itemsFromCSVCopyKP=>$itemsFromCSVCopyVP)
                  {
                      // added for converting array keys to lower case start
                      $itemsFromCSVCopyVP = array_change_key_case($itemsFromCSVCopyVP,CASE_LOWER);
                      // added for converting array keys to lower case end
                      $associationType2 = strtolower($itemsFromCSVCopyVP['case_association_type']);
                      $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyVP['action']));
                      $inludeChildOfArray = array('ischildof','');
                      $actionsToConsider = array('d','delete');
  
                      if( in_array((string)$actionType,$actionsToConsider) && $itemsFromCSVCopyVP['case_full_statement']=='' && in_array(trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVP['node_id'])),$sourceItemIdFromDb) && (in_array((string)$associationType2,$inludeChildOfArray)) )
                      {
                          $associationTypeRelation2 = array_keys($mainAssociationArray,strtolower(trim(!empty($itemsFromCSVCopyVP['case_association_type'])?$itemsFromCSVCopyVP['case_association_type']:'ischildof')));
                          $arrayOfAssociationsToBeDeletedInIsChildOf[$dli]['source_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVP['node_id']));
                          $arrayOfAssociationsToBeDeletedInIsChildOf[$dli]['target_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVP['node_parent_id']));
                          $arrayOfAssociationsToBeDeletedInIsChildOf[$dli]['association_type'] = $associationTypeRelation2[0];
                          $dli = $dli+1;
                      }
                  }
                  /**
                    * sorting associations to be deleted (include ischildof) relation end
                  */



                /**
                  * sorting associations to be added (exlude ischildof) start
                  */
                  $arrayOfAssociationsToBeUpdatedExludeIsChildOf = array();  
                  $add=0;
                  foreach($itemsFromCSVCopy as $itemsFromCSVCopyKD=>$itemsFromCSVCopyVD)
                  {
                      // added for converting array keys to lower case start
                      $itemsFromCSVCopyVD = array_change_key_case($itemsFromCSVCopyVD,CASE_LOWER);
                      // added for converting array keys to lower case end
                      $associationType = strtolower($itemsFromCSVCopyVD['case_association_type']);
                      $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyVD['action']));
                      $actionsToConsider = array('a','add','');
                      $exludeChildOfArray = array('ispeerof','ispartof','exactmatchof','precedes','isrelatedto','replacedby','haspart','hasskilllevel');
                        
                      if( (in_array((string)$actionType,$actionsToConsider)) && in_array(trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_id'])),$sourceItemIdFromDb) && (in_array((string)$associationType,$exludeChildOfArray)) )
                      { 
                          // get target document id based on destination node id start
                          $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId(trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_parent_id'])),$organizationIdentifier);
                          $targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_parent_id']));
                          $itemAssociationID = $this->createUniversalUniqueIdentifier();
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['item_association_id'] = $itemAssociationID;
                          $associationTypeRelation3 = array_keys($mainAssociationArray,strtolower(trim(!empty($itemsFromCSVCopyVD['case_association_type'])?$itemsFromCSVCopyVD['case_association_type']:'ischildof')));
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['association_type'] = $associationTypeRelation3[0];
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['document_id'] = $documentFromDb->document_id;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['association_group_id'] = '';
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['origin_node_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_id']));
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['destination_node_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_parent_id']));
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['destination_document_id'] = $documentFromDb->document_id;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['sequence_number'] = $itemsFromCSVCopyVD['csv_sequence'];
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['external_node_title'] = '';
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['external_node_url'] = '';
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['created_at'] = $currentDateTime;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['source_item_association_id'] = $itemAssociationID;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['organization_id'] = $organizationIdentifier;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['uri'] = $this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode,$this->domainName);
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['source_document_id'] = $documentFromDb->document_id;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['source_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_id']));
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['target_document_id'] = $targetDocumentIdentifier;
                          $arrayOfAssociationsToBeUpdatedExludeIsChildOf[$add]['target_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVD['node_parent_id']));
                          $add = $add+1;
                      }
                  }
                  /**
                    * sorting associations to be (exlude ischildof) added end
                  */

                 /**
                  * sorting associations to be added (include ischildof) start
                  */
                  $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf = array();  
                  $ad=0;
                  foreach($itemsFromCSVCopy as $itemsFromCSVCopyKJ=>$itemsFromCSVCopyVJ)
                  {
                      // added for converting array keys to lower case start
                      $itemsFromCSVCopyVJ = array_change_key_case($itemsFromCSVCopyVJ,CASE_LOWER);
                      // added for converting array keys to lower case end
                      $associationType2 = strtolower($itemsFromCSVCopyVJ['case_association_type']);
                      $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyVJ['action']));
                      $inludeChildOfArrays = array('ischildof','');
                      $actionsToConsider = array('a','add','');
                        
                      if( (in_array((string)$actionType,$actionsToConsider)) && in_array(trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_id'])),$sourceItemIdFromDb) && (in_array((string)$associationType2,$inludeChildOfArrays)) )
                      { 
                          // get target document id based on destination node id start
                          $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId(trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_parent_id'])),$organizationIdentifier);
                          $targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_parent_id']));
                          $itemAssociationID = $this->createUniversalUniqueIdentifier();
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['item_association_id'] = $itemAssociationID;
                          $associationTypeRelation4 = array_keys($mainAssociationArray,strtolower(trim(!empty($itemsFromCSVCopyVJ['case_association_type'])?$itemsFromCSVCopyVJ['case_association_type']:'ischildof')));
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['association_type'] = $associationTypeRelation4[0];
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['document_id'] = $documentFromDb->document_id;      
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['association_group_id'] = '';  
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['origin_node_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_id']));
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['destination_node_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_parent_id']));
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['destination_document_id'] = $documentFromDb->document_id;
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['sequence_number'] = $itemsFromCSVCopyVJ['csv_sequence'];
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['external_node_title'] = '';
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['external_node_url'] = '';
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['created_at'] = $currentDateTime;
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['source_item_association_id'] = $itemAssociationID;
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['organization_id'] = $organizationIdentifier;
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['uri'] = $this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode,$this->domainName);
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['source_document_id'] = $documentFromDb->document_id;
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['source_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_id']));
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['target_document_id'] = $targetDocumentIdentifier;
                          $arrayOfAssociationsToBeUpdatedInlcudeIsChildOf[$ad]['target_item_id'] = trim(preg_replace('/\t+/', '', $itemsFromCSVCopyVJ['node_parent_id']));
                          $ad = $ad+1;
                      }
                  }
                  /**
                    * sorting associations to be added (include ischildof) end
                  */ 

                
                  /**
                   * queries to delete and add association start
                   */
                  
                   $existingAssociationsToBeDeleted = array();
                   $existingAssociationsToBeDeleted = array_merge($arrayOfAssociationsToBeDeletedExIsChildOf,$arrayOfAssociationsToBeDeletedInIsChildOf);

                   $existingAssociationsToBeUpdated = array();
                   #ischildof association should be first then other association in the merged array
                   $existingAssociationsToBeUpdated = array_merge($arrayOfAssociationsToBeUpdatedInlcudeIsChildOf, $arrayOfAssociationsToBeUpdatedExludeIsChildOf);
                    
                   if(!empty($existingAssociationsToBeDeleted))
                   {
                       $update = ['is_deleted' => '1'];
                       foreach($existingAssociationsToBeDeleted as $existingAssociationsToBeDeletedK=>$existingAssociationsToBeDeletedV)
                       {
                            $documentId = $this->getDocumentIdentifier();
                            
                            if($documentId!=$existingAssociationsToBeDeletedV['target_item_id'])
                            {
                                $dataReturned1 = $this->getItemIdAndSourceIdMappingArrayUpdate2($existingAssociationsToBeDeletedV['source_item_id']);
                                $dataReturned2 = $this->getItemIdAndSourceIdMappingArrayUpdate2($existingAssociationsToBeDeletedV['target_item_id']);
                                
                                if(!empty($dataReturned1) && !empty($dataReturned2))
                                {

                                DB::table('item_associations')
                                    ->where('source_item_id',$dataReturned1[0]->item_id)
                                    ->where('target_item_id',$dataReturned2[0]->item_id)
                                    ->where('association_type',$existingAssociationsToBeDeletedV['association_type'])
                                    ->update($update);
                                }
                                else if(!empty($dataReturned1) && empty($dataReturned2))
                                {
                                    DB::table('item_associations')
                                    ->where('source_item_id',$dataReturned1[0]->item_id)
                                    ->where('target_item_id',$existingAssociationsToBeDeletedV['target_item_id'])
                                    ->where('association_type',$existingAssociationsToBeDeletedV['association_type'])
                                    ->update($update);
                                }    
                                
                                
                            }
                            else
                            {
                                $dataReturned1 = $this->getItemIdAndSourceIdMappingArrayUpdate2($existingAssociationsToBeDeletedV['source_item_id']);
                                
                                if(!empty($dataReturned1) && !empty($documentId))
                                {
                                    DB::table('item_associations')
                                    ->where('source_item_id',$dataReturned1[0]->item_id)
                                    ->where('target_item_id',$documentId)
                                    ->where('association_type',$existingAssociationsToBeDeletedV['association_type'])
                                    ->update($update);
                                }    
                            }
                            
                       }
                       
                   }
                    $organizationId = $this->getOrganizationIdentifier();
                    #assign sequence no to all other associations other than child associations
                    // $allMaxSeqNo = $this->getMaxSeqNoChild();
                    // $existingAssociationsToBeUpdated = $this->assignSeqNumber($existingAssociationsToBeUpdated, $allMaxSeqNo);

                   if(!empty($existingAssociationsToBeUpdated))
                    {
                        foreach($existingAssociationsToBeUpdated as $existingAssociationsToBeUpdatedK=>$existingAssociationsToBeUpdatedV)
                        {
                            $count = $this->checkAssociationExist2($existingAssociationsToBeUpdatedV);
                            $dataReturnedValue3 = $this->getItemIdAndSourceIdMappingArrayUpdate2($existingAssociationsToBeUpdatedV['source_item_id']);
                            $dataReturnedValue4 = $this->getItemIdAndSourceIdMappingArrayUpdate2($existingAssociationsToBeUpdatedV['target_item_id']);
                            if(!isset($dataReturnedValue4[0]->item_id) && $existingAssociationsToBeUpdatedV['target_item_id'] == $existingAssociationsToBeUpdatedV['target_document_id']) {
                                $dataReturnedValue4 = $existingAssociationsToBeUpdatedV['target_item_id'];
                            } else {
                                $dataReturnedValue4 = $dataReturnedValue4[0]->item_id;
                            }
                            $existingAssociationsToBeUpdatedV['source_item_id'] = $dataReturnedValue3[0]->item_id;
                            $existingAssociationsToBeUpdatedV['target_item_id'] = $dataReturnedValue4;
                            if($count==2)
                            {   
                                if(!empty($dataReturnedValue3) && !empty($dataReturnedValue4))
                                {
                                    DB::table('item_associations')->insert($existingAssociationsToBeUpdatedV);
                                }    
                            } else {
                                $query = DB::table('item_associations')
                                            ->where('source_item_id', $existingAssociationsToBeUpdatedV['source_item_id'])
                                            ->where('target_item_id', $existingAssociationsToBeUpdatedV['target_item_id'])
                                            ->where('association_type', $existingAssociationsToBeUpdatedV['association_type'])
                                            ->where('is_deleted', 0)->where('organization_id',$organizationId);
                                if($query->count()) {
                                    $query->update(['sequence_number' => $existingAssociationsToBeUpdatedV['sequence_number']]);
                                } else {
                                    DB::table('item_associations')->insert($existingAssociationsToBeUpdatedV);
                                }
                            }
                        }

                        
                    }
                    
                  /**
                   * queries to delete and add association end
                   */


                  /**
                   * sorting array to delete node/item with action delete start
                   */
                    $arrayItemIdToDeleteNode = array();
                    $actionsToConsider = array('d','delete');
                    foreach($actionMappedWithOldItemId as $actionMappedWithOldItemIdK=>$actionMappedWithOldItemIdV)
                    {
                        $actionMappedWithOldItemIdV = strtolower($actionMappedWithOldItemIdV);
                        if(in_array((string)$actionMappedWithOldItemIdV,$actionsToConsider))
                        {
                            $arrayItemIdToDeleteNode[]=$actionMappedWithOldItemIdK;
                        }
                    }
                    
                   /**
                   * sorting array to delete node/item with action delete end
                   */

                   /**
                   * queries to delete node for delete action start
                   */
                  
                   $this->setExistingDataToBeDeleted($arrayItemIdToDeleteNode);
                
                  /**
                   * queries to delete node for delete action start
                   */

            }

            if(count($itemIdNotInDb) > 0) {

                /**
                * Data filtered to get unique node id based on node_id and action and case field start
                */
                
                $itemsFromCSVCopy = $itemsFromCSV;
                $getUniqueitemIdArray = array();
                $getUniqueitemIdArray = array_unique($itemIdArray);
                
                $getItemArrayDataToInsert = array();
                $actionMappedWithOldItemId= array();
                foreach($getUniqueitemIdArray as $getUniqueitemIdArrayK=>$getUniqueitemIdArrayV)
                {
                    foreach($itemsFromCSVCopy as $itemsFromCSVCopyK=>$itemsFromCSVCopyV)
                    {
                        // added for converting array keys to lower case start
                        $itemsFromCSVCopyV = array_change_key_case($itemsFromCSVCopyV,CASE_LOWER);
                        // added for converting array keys to lower case end
                        $associationType2 = strtolower($itemsFromCSVCopyV['case_association_type']);
                        $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyV['action']));
                        $actionsToConsider = array('a','u','d','add','update','delete','');
                        $inludeChildOfArray = array('ischildof','');

                        if( ($getUniqueitemIdArrayV==trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_id']))) && (in_array((string)$actionType,$actionsToConsider)) && (!empty($itemsFromCSVCopyV['case_full_statement'])) && ( in_array((string)$associationType2,$inludeChildOfArray)) )
                        {
                                $getItemArrayDataToInsert[]=$itemsFromCSVCopyV;
                                $actionMappedWithOldItemId[trim($itemsFromCSVCopyV['node_id'])]=$itemsFromCSVCopyV['action'];
                        }
                    }
                }
                
                $itemArrayDataCopy = array();
                foreach($getItemArrayDataToInsert as $getItemArrayDataToInsertK=>$getItemArrayDataToInsert)
                {
                    $itemArrayDataCopy[$getItemArrayDataToInsertK+1] = $getItemArrayDataToInsert;
                }
                
                $itemsFromCSV = $itemArrayDataCopy;
                /**
                 * Data filtered to get unique node id based on node_id and action and case field end
                 */


                /**
                *  sorting unique item id start
                */   
                $itemIdNotInDb = array_unique($itemIdNotInDb);          
                
                /**
                 *  sorting unique item id end
                */  
            
                $arrayOfCreatedItemId = [];
                $caseAssociationsToSave = [];
                array_push($sourceItemIdFromDb, trim($this->inputData[0]['node_id']));
                foreach($itemsFromCSV as $keyForNewItem => $item) {
                    if(in_array(trim($item['node_id']), $itemIdNotInDb)) {
                        //$arrayOfCreatedItemId[]   =   $this->createUniversalUniqueIdentifier();

                        $arrayOfCreatedItemId[]   =   [trim($item['node_id'])    =>  $this->createUniversalUniqueIdentifier()];

                        $key = array_search (trim($item['node_parent_id']), $itemIdArray);
                        $arrayForParentIdIndices[]  =   $key;
    
                        $newItemArray[$keyForNewItem]['node_id']  =  trim($item['node_id']);
                        $newItemArray[$keyForNewItem]['node_parent_id'] = trim($item['node_parent_id']);
                        $newItemArray[$keyForNewItem]['sequence_number'] = $item['csv_sequence'];
    
                        foreach($caseMetadata as $metadata){
                            $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                            if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                                if($dbColumnFromCsvHeader == 'concept_title' || $dbColumnFromCsvHeader == 'concept_hierarchy_code' || $dbColumnFromCsvHeader == 'concept_keywords' || $dbColumnFromCsvHeader == 'concept_description') {
                                    $newItemArray[$keyForNewItem]['concept_title']               = !empty($item['case_concept_title']) ? $item['case_concept_title'] : "";
                                    $newItemArray[$keyForNewItem]['concept_hierarchy_code']               = !empty($item['case_concept_hierarchy_code']) ? $item['case_concept_hierarchy_code'] : "";
                                    $newItemArray[$keyForNewItem]['concept_keywords']               = !empty($item['case_concept_keywords']) ? $item['case_concept_keywords'] : "";
                                    $newItemArray[$keyForNewItem]['concept_description']               = !empty($item['case_concept_description']) ? $item['case_concept_description'] : "";
                                } else if($dbColumnFromCsvHeader == 'language') {
                                    $newItemArray[$keyForNewItem]['language']               = !empty($item['case_language']) ? $item['case_language'] : "";
                                }else if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                    $newItemArray[$keyForNewItem]['license_title']               = !empty($item['case_license_title']) ? $item['case_license_title'] : "";
                                    $newItemArray[$keyForNewItem]['license_text']               = !empty($item['case_license_text']) ? $item['case_license_text'] : "";
                                    $newItemArray[$keyForNewItem]['license_description']               = !empty($item['case_license_description']) ? $item['case_license_description'] : "";
                                } else {
                                    $newItemArray[$keyForNewItem][$dbColumnFromCsvHeader] = trim($item[$metadata]);   
                                }   
                            }
                        }
                    }
                }
                
                $this->setNewItemArray(array_values($newItemArray));
                $this->setArrayOfCreatedItemId($arrayOfCreatedItemId);
                
                $keyForItem         =   0;
                $nodeTypeIdForItem  =   '';
                foreach($newItemArray as $key => $itemToBeCreated) {
                    $item_id            =   $arrayOfCreatedItemId[$keyForItem][trim($itemToBeCreated['node_id'])];

                    if($itemToBeCreated['node_parent_id'] == $this->getDocumentIdentifier())
                    {
                        $parentId = $documentFromDb->document_id;
                    } else {
                        if(in_array($itemToBeCreated['node_parent_id'], $sourceItemIdFromDb )) {
                            $keyId = $sourceItemIdFromDb[array_search($itemToBeCreated['node_parent_id'], $sourceItemIdFromDb)];
                            $parentId   =   $itemIdFromDb[$keyId];
                        } else {
                            $parentId     =   $this->setParentIdForNewItem($itemToBeCreated['node_parent_id']);
                        }
                    }

                    $itemsToSave[$key]= [
                        "item_id"                   => $item_id,
                        "parent_id"                 => $parentId,
                        "document_id"               => $documentFromDb->document_id,
                        "created_at"                => $currentDateTime,
                        "organization_id"           => $organizationIdentifier,
                        "source_item_id"            => $item_id,
                        "updated_by"                => $userId,
                        "uri"                       => $this->getCaseApiUri("CFItems",$item_id,true,$orgCode,$this->domainName)
                    ];


                    if(!empty($itemToBeCreated['node_type'])) {
                        $nodeType =   trim($itemToBeCreated['node_type']);

                        $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $nodeType, 'is_deleted'   =>  '0' ];
                        $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);
        
                        if(count($nodeTypeExist) > 0) {
                            $itemsToSave[$key]['node_type_id']  =   $nodeTypeExist[0]->node_type_id;
                            $nodeTypeUriObject  =   json_encode(
                                [
                                    "identifier"    =>  $nodeTypeExist[0]->node_type_id,
                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeExist[0]->node_type_id,  true, $orgCode,$this->domainName),
                                    "title"         =>  $nodeType
                                ]
                            );
            
                            $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
                        } else {
                            $nodeTypeId =    $this->createNewNodeTypeAndMapWithMetadata($nodeType, $organizationIdentifier);
            
                            $nodeTypeUriObject  =   json_encode(
                                [
                                    "identifier"    =>  $nodeTypeId,
                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeId,  true, $orgCode,$this->domainName),
                                    "title"         =>  $nodeType
                                ]
                            );
            
                            $itemsToSave[$key]['node_type_id']          =   $nodeTypeId;
                            $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
    
                            $arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];
                            
                        }
                    }  else {
                        $attributes     =   ['organization_id'  => $organizationIdentifier, 'is_default' => '1', 'is_deleted'   =>  '0' ];
                        $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);
    
                        if(count($nodeTypeExist) > 0) {
                            $nodeTypeUriObject  =   json_encode(
                                [
                                    "identifier"    =>  $nodeTypeExist[0]->node_type_id,
                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeExist[0]->node_type_id,  true, $orgCode,$this->domainName),
                                    "title"         =>  'Default'
                                ]
                            );

                            $itemsToSave[$key]['node_type_id']          =   $nodeTypeExist[0]->node_type_id;
                            $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
    
                            //$arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];
                        
                        } else {
                            $nodeTypeId =    $this->createNewNodeTypeAndMapWithMetadata('Default', $organizationIdentifier);
            
                            $nodeTypeUriObject  =   json_encode(
                                [
                                    "identifier"    =>  $nodeTypeId,
                                    "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeId,  true, $orgCode,$this->domainName),
                                    "title"         =>  'Default'
                                ]
                            );
            
                            $itemsToSave[$key]['node_type_id']          =   $nodeTypeId;
                            $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
                        }
                    }

                    foreach($caseMetadata as $metadata){
                        $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                        if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                            if($dbColumnFromCsvHeader == 'title') {
                                $itemsToSave[$key]['full_statement'] = $itemToBeCreated['title'];
                            } else if($dbColumnFromCsvHeader == 'language') {
                                $languageId  = !empty($itemToBeCreated['language']) ? 
                                $this->helperToFetchLanguageIdFromEitherExistingOrNew($itemToBeCreated['language']) : ""; 
                                $itemsToSave[$key]['language_id'] = $languageId;
                            }else if($dbColumnFromCsvHeader == 'concept_title' || $dbColumnFromCsvHeader == 'concept_hierarchy_code' || $dbColumnFromCsvHeader == 'concept_keywords' || $dbColumnFromCsvHeader == 'concept_description') {
                                $conceptId  = !empty($itemToBeCreated['concept_title']) ? 
                                $this->helperToFetchConceptIdFromEitherExistingOrNew($itemToBeCreated['concept_title']) : ""; 
                                $itemsToSave[$key]['concept_id'] = $conceptId;
                            } else if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                $licenseId  = !empty($itemToBeCreated['license_title']) ? 
                                $this->helperToFetchLicenseIdFromEitherExistingOrNew($itemToBeCreated['license_title']) : ""; 
                                $itemsToSave[$key]['license_id'] = $licenseId;
                            } else {
                                if($dbColumnFromCsvHeader != 'node_type') {
                                    $itemsToSave[$key][$dbColumnFromCsvHeader] = $itemToBeCreated[$dbColumnFromCsvHeader];
                                }
                            }     
                        }
                    }


                    // get target document id based on destination node id start
                    $documentIdentifierV = $this->itemRepository->find($parentId);
                    $targetDocumentData = $documentIdentifierV['document_id'];
                    $itemAssociationID = $this->createUniversalUniqueIdentifier();
                    $caseAssociationsToSave[] = [
                        "item_association_id" => $itemAssociationID,
                        "association_type" => 1,
                        "document_id" => $documentFromDb->document_id,
                        "association_group_id" => '',
                        "origin_node_id" => $item_id,
                        "destination_node_id" => $parentId,
                        "destination_document_id" => $documentFromDb->document_id,
                        "sequence_number" => $itemToBeCreated['sequence_number'],
                        "external_node_title" =>"",
                        "external_node_url" =>"",
                        "created_at" => $currentDateTime,
                        "source_item_association_id" => $itemAssociationID,
                        "organization_id" => $organizationIdentifier,
                        "uri"=>$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode,$this->domainName),
                        "source_document_id" => $documentFromDb->document_id,
                        "source_item_id" => $item_id,
                        "target_document_id" => $documentFromDb->document_id,
                        "target_item_id" => $parentId
                    ];

                    $keyForItem++;
                    $newNodeCount++;
                    $this->newNodeCount             =   $newNodeCount;
                }

                /**
                 * sorting new associations to be added for add action (exlude ischildof) start  
                 */
                
                $getRawDataForMultipleAssociations = array();
                $exchildof=0;
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyKey=>$itemsFromCSVCopyValue)
                {
                    $itemsFromCSVCopyValue = array_change_key_case($itemsFromCSVCopyValue,CASE_LOWER);
                    $associationType = strtolower($itemsFromCSVCopyValue['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyValue['action']));
                    $exludeChildOfArray = array('ispeerof','ispartof','exactmatchof','precedes','isrelatedto','replacedby','haspart','hasskilllevel');
                    $actionsToConsider = array('a','add','');
                    if( (in_array((string)$actionType,$actionsToConsider)) && (in_array((string)$associationType,$exludeChildOfArray)) )
                    {
                        $getRawDataForMultipleAssociations[$exchildof]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                        $getRawDataForMultipleAssociations[$exchildof]['node_parent_id'] = $itemsFromCSVCopyValue['node_parent_id'];
                        $getRawDataForMultipleAssociations[$exchildof]['case_association_type'] = $itemsFromCSVCopyValue['case_association_type'];
                        $getRawDataForMultipleAssociations[$exchildof]['document_id'] = $documentFromDb->document_id;
                        $getRawDataForMultipleAssociations[$exchildof]['sequence_number'] = $itemsFromCSVCopyValue['csv_sequence'];
                        $exchildof = $exchildof +1;
                    }
                }
                
                /**
                 * sorting new associations to be added for add action (exlude ischildof) end  
                 */
                
                /**
                 * sorting new associations to be added for add and update action (include ischildof) end  
                 */

                $getRawDataForisChildOfAssociations = array();
                $inchildof=0;
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyIndex=>$itemsFromCSVCopyData)
                {
                    $itemsFromCSVCopyData = array_change_key_case($itemsFromCSVCopyData,CASE_LOWER);
                    $associationType2 = strtolower($itemsFromCSVCopyData['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyData['action']));
                    $includeChildOfArray = array('ischildof','');
                    $actionsToConsider = array('a','add','');
                    if( (in_array((string)$actionType,$actionsToConsider)) && ($itemsFromCSVCopyData['case_full_statement']=='') && (in_array((string)$associationType2,$includeChildOfArray)) )
                    {
                            $getRawDataForisChildOfAssociations[$inchildof]['node_id'] = $itemsFromCSVCopyData['node_id'];
                            $getRawDataForisChildOfAssociations[$inchildof]['node_parent_id'] = $itemsFromCSVCopyData['node_parent_id'];
                            $getRawDataForisChildOfAssociations[$inchildof]['case_association_type'] = $itemsFromCSVCopyData['case_association_type'];
                            $getRawDataForisChildOfAssociations[$inchildof]['document_id'] = $documentFromDb->document_id;
                            $getRawDataForisChildOfAssociations[$inchildof]['sequence_number'] = $itemsFromCSVCopyData['csv_sequence'];
                            $inchildof = $inchildof +1;
                    }
                }
                
                /**
                 * sorting new associations to be added for add and update action (include ischildof) end  
                 */ 


                /**
                 * merging raw data of new node associations to add start
                 */
                
                $multipleNewAssociationsRawData = array();
                $multipleNewAssociationsRawData = array_merge($getRawDataForMultipleAssociations,$getRawDataForisChildOfAssociations);

                /**
                 * merging raw data of new node associations to add start
                 */


                /**
                 * creating key=>value relation between old and new node id start
                */ 
                
                $arrayOfOldItemMappedToNewItem = array();
                foreach($arrayOfCreatedItemId as $arrayOfCreatedItemIdKey=>$arrayOfCreatedItemIdValue)
                {
                    foreach($arrayOfCreatedItemIdValue as $arrayOfCreatedItemIdValueK=>$arrayOfCreatedItemIdValueV)
                    {
                        $arrayOfOldItemMappedToNewItem[$arrayOfCreatedItemIdValueK] = $arrayOfCreatedItemIdValueV;
                    }
                }
                
                $getArrayKeys = array();
                $getArrayValues = array();
                $getArrayValues = array();
                $getArrayKeys = array_keys($getNodeIdMappedWithParentId);
                $getArrayValues = array_values($getNodeIdMappedWithParentId);
                $getArrayKeysAndValues = array_merge($getArrayKeys,$getArrayValues);

                
                $getSameKeyValueMapped = array();
                foreach($getArrayKeysAndValues as $getArrayKeysAndValuesK=>$getArrayKeysAndValuesV)
                {
                    $getSameKeyValueMapped[$getArrayKeysAndValuesV] = $getArrayKeysAndValuesV;
                }
                
                 $treeHierarchyAssociation = array_diff_key($getSameKeyValueMapped,$arrayOfOldItemMappedToNewItem);
                 $arrayOfOldItemMappedToNewItem = $treeHierarchyAssociation+$arrayOfOldItemMappedToNewItem;
                 
                /**
                 * creating key=>value relation between old and new node id end
                */  

                /**
                *    Mapping origin node id start 
                */
                $getOriginNodeIdMapped = array();
                $dt=0;
                foreach($multipleNewAssociationsRawData as $multipleNewAssociationsRawDataK=>$multipleNewAssociationsRawDataV)
                {
                    foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemK=>$arrayOfOldItemMappedToNewItemV)
                    {
                            
                            if( trim(preg_replace('/\t+/', '', $multipleNewAssociationsRawDataV['node_id']))==$arrayOfOldItemMappedToNewItemK)
                            {
                                $getOriginNodeIdMapped[$dt]['source_item_id'] = $arrayOfOldItemMappedToNewItemV;
                                $getOriginNodeIdMapped[$dt]['node_parent_id'] = $multipleNewAssociationsRawDataV['node_parent_id'];
                                $getOriginNodeIdMapped[$dt]['case_association_type'] = $multipleNewAssociationsRawDataV['case_association_type'];
                                $getOriginNodeIdMapped[$dt]['document_id'] = $multipleNewAssociationsRawDataV['document_id'];
                                $getOriginNodeIdMapped[$dt]['sequence_number'] = $multipleNewAssociationsRawDataV['sequence_number'];
                                $dt = $dt+1;
                            }
                        
                    }
                }
                
                /**
                *    Mapping origin node id end 
                */
                

                /**
                * Mapping destination node id start
                */
                
                $getDestinationNodeIdMapped = array();
                $dni=0;
                foreach($getOriginNodeIdMapped as $getOriginNodeIdMappedK=>$getOriginNodeIdMappedV)
                {
                    foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemKey => $arrayOfOldItemMappedToNewItemValue)
                    {
                            
                            if(trim(preg_replace('/\t+/', '', $getOriginNodeIdMappedV['node_parent_id']))==$arrayOfOldItemMappedToNewItemKey)
                            {
                                $getDestinationNodeIdMapped[$dni]['source_item_id'] = $getOriginNodeIdMappedV['source_item_id'];
                                $getDestinationNodeIdMapped[$dni]['target_item_id'] = $arrayOfOldItemMappedToNewItemValue;
                                $getDestinationNodeIdMapped[$dni]['case_association_type'] = $getOriginNodeIdMappedV['case_association_type'];
                                $getDestinationNodeIdMapped[$dni]['document_id'] = $getOriginNodeIdMappedV['document_id'];
                                $getDestinationNodeIdMapped[$dni]['sequence_number'] = $getOriginNodeIdMappedV['sequence_number'];
                                $dni = $dni+1;
                            }
                        
                    }
                }
                
                /**
                * Mapping destination node id end
                */
                

                /**
                 * Map association of other taxonomy from same tenant start
                 */
                
                $getAssociationRelationOfOtherTenant = array();
                $extAsso=0;
                
                foreach($getOriginNodeIdMapped as $getOriginNodeIdMappedKey=>$getOriginNodeIdMappedValue)
                {
                    if( !in_array(trim(preg_replace('/\t+/', '', $getOriginNodeIdMappedValue['node_parent_id'])),$getUniqueitemIdArray) )
                    {
                        
                        $count = $this->checkParentNodeIdExist(trim(preg_replace('/\t+/', '', $getOriginNodeIdMappedValue['node_parent_id'])));
                        if($count==1)
                        {
                            $getAssociationRelationOfOtherTenant[$extAsso]['source_item_id'] = $getOriginNodeIdMappedValue['source_item_id'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['target_item_id'] = $getOriginNodeIdMappedValue['node_parent_id'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['case_association_type'] = $getOriginNodeIdMappedValue['case_association_type'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['document_id'] = $getOriginNodeIdMappedValue['document_id'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['sequence_number'] = $getOriginNodeIdMappedValue['sequence_number'];
                            $extAsso = $extAsso+1;
                        }
                    }
                }
                
                /**
                 * Map association of other taxonomy from same tenant end
                 */


                /**
                  * Map association on same taxonomy and other taxonomy/same tenant taxonomy start
                  */
                
                  $getAssociationData = array();
                  $getAssociationData = array_merge($getDestinationNodeIdMapped,$getAssociationRelationOfOtherTenant);  
                  
                /**
                * Map association on same taxonomy and other taxonomy/same tenant taxonomy end
                */
                
                
                // get difference of valid associations and invalid associations start

                $newAssociationsCreated = array();
                $allAssociationsPresent = array();
                $getItemIdAndSourceIdMappingArray = array();
                if(!empty($getAssociationData))
                {
                    $newAssociationsCreated = array_column($getAssociationData,'target_item_id');
                    $allAssociationsPresent = array_column($getOriginNodeIdMapped,'node_parent_id');
                }

                $getDiffAssociation = array_diff($allAssociationsPresent,$newAssociationsCreated);
                if(!empty($getDiffAssociation))
                {
                    $getItemIdAndSourceIdMappingArray = $this->getItemIdAndSourceIdMappingArray($getDiffAssociation);
                }
                
                $getIdsFromJsonImportType1And2 = array();
                $dtm = 0;
                if(!empty($getItemIdAndSourceIdMappingArray))
                {
                    foreach($getItemIdAndSourceIdMappingArray as $getItemIdAndSourceIdMappingArrayK=>$getItemIdAndSourceIdMappingArrayV)
                    {
                        if(!in_array((string)$getItemIdAndSourceIdMappingArrayV->source_item_id,$getUniqueitemIdArray))
                        {
                            $getIdsFromJsonImportType1And2[$dtm][$getItemIdAndSourceIdMappingArrayV->item_id] = $getItemIdAndSourceIdMappingArrayV->source_item_id;
                            $dtm = $dtm+1;
                        }
                    }

                }
                
                // get difference of valid associations and invalid associations end


                /**
                * Map association of another taxonomy which are imported as import type 1 and 3 for json start 
                */
                
                $createAssociationFromSourceItemId = array();
                $ktm=0;
                if(!empty($getIdsFromJsonImportType1And2))
                {
                    foreach($getOriginNodeIdMapped as $getOriginNodeIdMappedKeyData=>$getOriginNodeIdMappedValueData)
                    {
                        foreach($getIdsFromJsonImportType1And2 as $getIdsFromJsonImportType1And2K=>$getIdsFromJsonImportType1And2V)
                        {
                            foreach($getIdsFromJsonImportType1And2V as $getIdsFromJsonImportType1And2VK=>$getIdsFromJsonImportType1And2VV)
                            {
                                if($getOriginNodeIdMappedValueData['node_parent_id']==$getIdsFromJsonImportType1And2VV)
                                {
                                    $createAssociationFromSourceItemId[$ktm]['source_item_id'] = $getOriginNodeIdMappedValueData['source_item_id'];
                                    $createAssociationFromSourceItemId[$ktm]['target_item_id'] = $getIdsFromJsonImportType1And2VK;
                                    $createAssociationFromSourceItemId[$ktm]['case_association_type'] = $getOriginNodeIdMappedValueData['case_association_type'];
                                    $createAssociationFromSourceItemId[$ktm]['document_id'] = $getOriginNodeIdMappedValueData['document_id'];
                                    $createAssociationFromSourceItemId[$ktm]['sequence_number'] = $getOriginNodeIdMappedValueData['sequence_number'];
                                    $ktm = $ktm+1;
                                }
                            }
                        }
                    }
                }

                /**
                * Map association of another taxonomy which are imported as import type 1 and 3 for json end
                */
                 
                if(!empty($createAssociationFromSourceItemId))
                {
                    $getAssociationData = array_merge($getAssociationData,$createAssociationFromSourceItemId);
                }
                
                /**
                 *   Creating Array for Associations start
                 */

                $mainAssociationArray = array(1=>'ischildof',2=>'ispeerof',3=>'ispartof',4=>'exactmatchof',5=>'precedes',6=>'isrelatedto',7=>'replacedby',9=>'hasskilllevel'); 

                 /**
                 *   Creating Array for Associations end
                 */


                /**
                 * Map association of another taxonomy which are imported as import type 1 and 3 for json start (runs only if it has json 1 and 3 type association)
                 */
                $onlyJson1and3TypeAssociationToImport = array();
                $ad=0;
                if(!empty($getOriginNodeIdMapped) && !empty($newAssociationsCreated) &&  !empty($allAssociationsPresent))
                {
                    foreach($getOriginNodeIdMapped as $KeyData=>$KeyValue)
                    {
                        $dataFetched = $this->getItemIdAndSourceIdMappingArrayUpdate($KeyValue['node_parent_id']);
                        if(!empty($dataFetched))
                        {
                            // get target document id based on destination node id start
                            $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($dataFetched[0]->item_id,$organizationIdentifier);
                            $targetDocumentInfo = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $dataFetched[0]->item_id;
                            $associationTypeRelation = array_keys($mainAssociationArray,strtolower(trim(!empty($KeyValue['case_association_type'])?$KeyValue['case_association_type']:'ischildof')));
                            $itemAssociationID = $this->createUniversalUniqueIdentifier();
                            $onlyJson1and3TypeAssociationToImport[$ad]['item_association_id'] = $itemAssociationID;
                            $onlyJson1and3TypeAssociationToImport[$ad]["association_type"] = $associationTypeRelation[0];
                            $onlyJson1and3TypeAssociationToImport[$ad]["document_id"] = $KeyValue['document_id'];
                            $onlyJson1and3TypeAssociationToImport[$ad]["association_group_id"] = '';
                            $onlyJson1and3TypeAssociationToImport[$ad]["origin_node_id"] = $KeyValue['source_item_id'];
                            $onlyJson1and3TypeAssociationToImport[$ad]["destination_node_id"] = $dataFetched[0]->item_id;
                            $onlyJson1and3TypeAssociationToImport[$ad]["destination_document_id"] = $KeyValue['document_id'];
                            $onlyJson1and3TypeAssociationToImport[$ad]["sequence_number"] = $KeyValue['sequence_number'];
                            $onlyJson1and3TypeAssociationToImport[$ad]["external_node_title"] ="";
                            $onlyJson1and3TypeAssociationToImport[$ad]["external_node_url"] ="";
                            $onlyJson1and3TypeAssociationToImport[$ad]["created_at"] = $currentDateTime;
                            $onlyJson1and3TypeAssociationToImport[$ad]["source_item_association_id"] = $itemAssociationID;
                            $onlyJson1and3TypeAssociationToImport[$ad]["organization_id"] = $organizationIdentifier;
                            $onlyJson1and3TypeAssociationToImport[$ad]["uri"]=$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode);
                            $onlyJson1and3TypeAssociationToImport[$ad]["source_document_id"] = $KeyValue['document_id'];
                            $onlyJson1and3TypeAssociationToImport[$ad]["source_item_id"] = $KeyValue['source_item_id'];
                            $onlyJson1and3TypeAssociationToImport[$ad]["target_document_id"] = $targetDocumentInfo;
                            $onlyJson1and3TypeAssociationToImport[$ad]["target_item_id"] = $dataFetched[0]->item_id;
                            $ad = $ad +1;
                        }
                    }
                }
                 


                /**
                 * Map association of another taxonomy which are imported as import type 1 and 3 for json start (runs only if it has json 1 and 3 type association)
                 */
                
                $caseAssociationsToSave = array_merge($caseAssociationsToSave);

                #assign sequence no to child associations
                #assign sequence no to child associations
                // $caseAssociationsToSave = $this->itemSequenceNo($itemsToSave, $caseAssociationsToSave);
                // $allAssoMaxSeqNoItemResult = $this->getMaxSeqNoOfItems($caseAssociationsToSave);
                // $allParentMaxSeqNo = $this->getMaxSeqNoChild();
                // $caseAssociationsToSave = $this->itemSequenceNo($itemsToSave, $caseAssociationsToSave, $allParentMaxSeqNo);

                if(!empty($itemsToSave)) {
                    $recordsToSaveCollection = collect($itemsToSave);
                    foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
                        $this->itemRepository->saveMultiple($chunk->toArray());
                    }

                    $recordsAssociationToSaveCollection = collect($caseAssociationsToSave);
                    foreach ($recordsAssociationToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
                        $this->itemAssociationRepository->saveMultiple($chunk->toArray());
                    }
                }

                /** Inserting new added associations start */

                /**
                 * Creating final data of Associations start
                 */ 
                $finalAssociationArray = array();
                $as=0;
                foreach($getAssociationData as $getAssociationDataK=>$getAssociationDataV)
                {
                    // get target document id based on destination node id start
                    $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($getAssociationDataV['target_item_id'],$organizationIdentifier);
                    $targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $getAssociationDataV['target_item_id'];
                    $associationTypeRelation = array_keys($mainAssociationArray,strtolower(trim(!empty($getAssociationDataV['case_association_type'])?$getAssociationDataV['case_association_type']:'ischildof')));
                    # sequence no of association
                    // $sequenceNo = 0;
                    // if($associationTypeRelation[0] != 1) {
                    //     if(isset($allAssoMaxSeqNoItemResult[$getAssociationDataV['source_item_id']])) {
                    //         $sequenceNo = $allAssoMaxSeqNoItemResult[$getAssociationDataV['source_item_id']] + 1;
                    //     } else {
                    //         $sequenceNo = 1;
                    //     }
                    //     $allAssoMaxSeqNoItemResult[$getAssociationDataV['source_item_id']] = $sequenceNo;
                    // } else {
                    //     $sequenceNo = isset($this->allChildSeqArr[$getAssociationDataV['source_item_id']]) ? $this->allChildSeqArr[$getAssociationDataV['source_item_id']] + 1 : 0;
                    //     $this->allChildSeqArr[$getAssociationDataV['source_item_id']] = $sequenceNo;
                    // }
                    $itemAssociationID = $this->createUniversalUniqueIdentifier();
                    $finalAssociationArray[$as]["item_association_id"] = $itemAssociationID;
                    $finalAssociationArray[$as]["association_type"] = $associationTypeRelation[0];
                    $finalAssociationArray[$as]["document_id"] = $getAssociationDataV['document_id'];
                    $finalAssociationArray[$as]["association_group_id"] = '';
                    $finalAssociationArray[$as]["origin_node_id"] = $getAssociationDataV['source_item_id'];
                    $finalAssociationArray[$as]["destination_node_id"] = $getAssociationDataV['target_item_id'];
                    $finalAssociationArray[$as]["destination_document_id"] = $getAssociationDataV['document_id'];
                    $finalAssociationArray[$as]["sequence_number"] = $getAssociationDataV['sequence_number'];
                    $finalAssociationArray[$as]["external_node_title"] ="";
                    $finalAssociationArray[$as]["external_node_url"] ="";
                    $finalAssociationArray[$as]["created_at"] = $currentDateTime;
                    $finalAssociationArray[$as]["source_item_association_id"] = $itemAssociationID;
                    $finalAssociationArray[$as]["organization_id"] = $organizationIdentifier;
                    $finalAssociationArray[$as]["uri"]=$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode);
                    $finalAssociationArray[$as]["source_document_id"] = $getAssociationDataV['document_id'];
                    $finalAssociationArray[$as]["source_item_id"] = $getAssociationDataV['source_item_id'];
                    $finalAssociationArray[$as]["target_document_id"] = $targetDocumentIdentifier;
                    $finalAssociationArray[$as]["target_item_id"] = $getAssociationDataV['target_item_id'];
                    $as = $as+1;
                }
                /**
                 * Creating final data of Associations end
                 */

                /** Inserting new added associations end */
                
                /**
                 *  Insert only if associations doesn't exists start
                */ 
                #assign sequence no to all other associations other than child associations
                // $allMaxSeqNo = $this->getAllMaxSeqNo();
                // $onlyJson1and3TypeAssociationToImport = $this->assignSeqNumber2($onlyJson1and3TypeAssociationToImport, $allMaxSeqNo);
                if(!empty($onlyJson1and3TypeAssociationToImport))
                    {
                        foreach($onlyJson1and3TypeAssociationToImport as $onlyJson1and3TypeAssociationToImportK=>$onlyJson1and3TypeAssociationToImportV)
                        {
                            
                            $count = $this->checkAssociationExistInItemAssociationTable($onlyJson1and3TypeAssociationToImportV);
                            
                            if($count==2)
                            {   
                                DB::table('item_associations')->insert($onlyJson1and3TypeAssociationToImportV);
                            }
                        }

                        
                    }
                #assign sequence no to all other associations other than child associations
                // $allMaxSeqNo = $this->getAllMaxSeqNo();
                // $finalAssociationArray = $this->assignSeqNumber2($finalAssociationArray, $allMaxSeqNo);

                if(!empty($finalAssociationArray))
                    {
                        foreach($finalAssociationArray as $finalAssociationArrayK=>$finalAssociationArrayV)
                        {
                            $count = $this->checkAssociationExist($finalAssociationArrayV);

                            if($count==2)
                            {   
                                $dataReturnedValue = $this->getItemIdAndSourceIdMappingArrayUpdate($finalAssociationArrayV['target_item_id']);
                                $finalAssociationArrayV['origin_node_id'] = $dataReturnedValue[0]->item_id;
                                $finalAssociationArrayV['target_item_id'] = $dataReturnedValue[0]->item_id;
                                DB::table('item_associations')->insert($finalAssociationArrayV);
                            }
                        }

                        
                    }

                /**
                 *  Insert only if associations doesn't exists start
                */
                
                

                /**
                 * queries to insert new node associations end
                 */

                if(!empty($newNodeAssociationsToAdd))
                {
                    //DB::table('item_associations')->insert($newNodeAssociationsToAdd);
                }

                /**
                 * queries to insert new node associations start
                 */


                /**
                 * queries to delete new node for action update and delete start
                 */
                
                $actionsToConsider = array('u','update','d','delete');
                foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemKey=>$arrayOfOldItemMappedToNewItemValue)
                {
                    foreach($actionMappedWithOldItemId as $actionMappedWithOldItemIdKey=>$actionMappedWithOldItemIdValue)
                    {
                        $actionMappedWithOldItemIdValue = strtolower($actionMappedWithOldItemIdValue);
                        if( trim($arrayOfOldItemMappedToNewItemKey)==trim($actionMappedWithOldItemIdKey) && (in_array((string)$actionMappedWithOldItemIdValue,$actionsToConsider)) )
                        {   

                            $update = ['is_deleted' => '1'];

                            DB::table('items')->where('item_id', $arrayOfOldItemMappedToNewItemValue)
                                        ->where('organization_id',$organizationIdentifier)
                                        ->update($update);

                            DB::table('items')->where('parent_id', $arrayOfOldItemMappedToNewItemValue)
                                            ->where('organization_id',$organizationIdentifier)
                                            ->update($update);

                            DB::table('item_associations')->where('source_item_id', $arrayOfOldItemMappedToNewItemValue)
                                                        ->where('organization_id',$organizationIdentifier)
                                                        ->update($update);

                            DB::table('item_associations')->where('target_item_id', $arrayOfOldItemMappedToNewItemValue)
                                                        ->where('organization_id',$organizationIdentifier)
                                                        ->update($update);
                            $newNodeCount--;
                            $newAssociationsCount--;

                        }
                    }
                }

                 
                /**
                 * queries to insert new node action update and delete start
                 */ 

                $newAssociationsCount       =   $newAssociationsCount + sizeOf($caseAssociationsToSave);
                $this->newAssociationsCount =   $newAssociationsCount;
                $this->newNodeCount         =   $newNodeCount;
                $this->setCreatedItemArray($itemsToSave);
            }
        } else {
            if($packageExists == 1) {
                if(count($itemCollectionFromCsv) > 0){
                    /**
                     * convert data from db in sync to inputData
                    */
                    foreach($caseMetadata as $metadata){
                        $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                        if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                            //Create itemArray with value from db
                            foreach($itemCollectionFromCsv as $key => $items) {
                                $itemArray[$key]['node_id']                 = !empty($items->item_id) ? $items->item_id : "";
                                $itemArray[$key]['node_parent_id']          = !empty($items->parent_id) ? $items->parent_id : "";
        
        
                                if($dbColumnFromCsvHeader == 'title') {
                                    $itemArray[$key][$dbColumnFromCsvHeader]    = $items->full_statement;
                                } else {
                                    $itemArray[$key][$dbColumnFromCsvHeader]    = !empty($items->$dbColumnFromCsvHeader) ?  $items->$dbColumnFromCsvHeader : "";
                                }
        
                                $itemArray[$key]['node_type']               = !empty($items->nodeType->title) ? $items->nodeType->title : "";
                                
                            }
                        }
                    }
                    $this->setItemCollectionFromDb($itemArray);
                    
                }
            }

            /**
             * Data filtered to get unique id based on node_id and action and case field start
             */
            
            $itemsFromCSVCopy = $itemsFromCSV;
            $getUniqueitemIdArray = array();
            $getUniqueitemIdArray = array_unique($itemIdArray);
            
            $getItemArrayDataToInsert = array();
            $actionMappedWithOldItemId= array();
            $treeChildForOldItemId = array();
            $treeParentForOldItemId = array();
            $treeChildParentRelationForOldItem = array();
            foreach($getUniqueitemIdArray as $getUniqueitemIdArrayK=>$getUniqueitemIdArrayV)
            {
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyK=>$itemsFromCSVCopyV)
                {
                    // added for converting array keys to lower case start
                    $itemsFromCSVCopyV = array_change_key_case($itemsFromCSVCopyV,CASE_LOWER);
                    // added for converting array keys to lower case end
                    $associationType2 = strtolower($itemsFromCSVCopyV['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyV['action']));
                    $includeChildOfArray = array('ischildof','');
                    $actionsToConsider = array('a','u','d','add','update','delete','');

                    if( ($getUniqueitemIdArrayV==trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_id']))) && (in_array((string)$actionType,$actionsToConsider)) && (!empty($itemsFromCSVCopyV['case_full_statement'])) && (in_array((string)$associationType2,$includeChildOfArray)) )
                    {
                            $getItemArrayDataToInsert[]=$itemsFromCSVCopyV;
                            $actionMappedWithOldItemId[trim($itemsFromCSVCopyV['node_id'])]=$itemsFromCSVCopyV['action'];
                            $treeChildParentRelationForOldItem[trim($itemsFromCSVCopyV['node_id'])]=$itemsFromCSVCopyV['node_parent_id'];
                            $treeChildForOldItemId[]=$itemsFromCSVCopyV['node_id'];
                            $treeParentForOldItemId[]=$itemsFromCSVCopyV['node_parent_id'];
                    }
                }
            }
            
            $itemArrayDataCopy = array();
            foreach($getItemArrayDataToInsert as $getItemArrayDataToInsertK=>$getItemArrayDataToInsert)
            {
                $itemArrayDataCopy[$getItemArrayDataToInsertK+1] = $getItemArrayDataToInsert;
            }
            
            $itemsFromCSV = $itemArrayDataCopy;
            /**
             * Data filtered to get unique id based on node_id and action and case field end
             */

            /**
             * Unique Array Id sorted start 
            */
            
            $sortedUniqueitemIdArray = array();
            foreach($getUniqueitemIdArray as $getUniqueitemIdArrayKey=>$getUniqueitemIdArrayValue)
            {
                $sortedUniqueitemIdArray[]=$getUniqueitemIdArrayValue;
            }

            /**
             * Unique Array Id sorted end
            */


            $arrayOfOldItemMappedToNewItem = array();
            $caseAssociationsToSave = array();
            foreach($itemsFromCSV as $id => $item) {
                $createdItemId = $arrayOfCreatedItemId[trim($item['node_id'])] = $this->createUniversalUniqueIdentifier();
                $key = array_search (trim($item['node_parent_id']), $sortedUniqueitemIdArray);
                //$arrayForParentIdIndices[]  =   $key;

                $arrayOfOldItemMappedToNewItem[] = [trim($item['node_id']) => $createdItemId];
                --$id;
                $newItemArray[$id]['node_id']           =   trim($item['node_id']);
                $newItemArray[$id]['node_parent_id']    =   trim($item['node_parent_id']);
                $newItemArray[$id]['csv_sequence']      =   $item['csv_sequence'];

                //$newItemArray[$id]['node_type']         =   trim($item['node_type']);

                foreach($caseMetadata as $metadata){
                    $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                    if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                        $newItemArray[$id][$dbColumnFromCsvHeader] = trim($item[strtolower($metadata)]);       
                    }
                }
                
            }
            
            $this->setNewItemArray($newItemArray);
            $this->createItemIdForNewItemsToBeAdded();
            
            $this->setArrayOfCreatedItemId($arrayOfOldItemMappedToNewItem);

            $keyForItem         =   0;
                $nodeTypeIdForItem  =   '';
                foreach($newItemArray as $key => $itemToBeCreated) {
                    $item_id            =   $arrayOfCreatedItemId[$itemToBeCreated['node_id']];
                    $parentId           =   (isset($arrayOfCreatedItemId[$itemToBeCreated['node_parent_id']])) ? $arrayOfCreatedItemId[$itemToBeCreated['node_parent_id']] : $documentIdentifier;

                    $itemsToSave[$key]= [
                        "item_id"                   => $item_id,
                        "parent_id"                 => $parentId,
                        "document_id"               => $this->getCreatedDocumentIdentifier(),
                        "created_at"                => $currentDateTime,
                        "organization_id"           => $organizationIdentifier,
                        "source_item_id"            => $item_id,
                        "updated_by"                => $userId,
                        "uri"                       => $this->getCaseApiUri("CFItems",$item_id,true,$orgCode,$this->domainName)
                    ];

                    foreach($caseMetadata as $metadata){
                        $dbColumnFromCsvHeader = strtolower(preg_split("/case_/i", $metadata)[1]);
                        if(in_array((string)$dbColumnFromCsvHeader, $metadataForItem) || $dbColumnFromCsvHeader == 'title') {
                            if($dbColumnFromCsvHeader == 'title') {
                                $itemsToSave[$key]['full_statement'] = $itemToBeCreated['title'];
                            } else if($dbColumnFromCsvHeader == 'language') {
                                $languageId  = !empty($itemToBeCreated['language']) ? 
                                $this->helperToFetchLanguageIdFromEitherExistingOrNew($itemToBeCreated['language']) : ""; 
                                $itemsToSave[$key]['language_id'] = $languageId;
                            }else if($dbColumnFromCsvHeader == 'concept_title' || $dbColumnFromCsvHeader == 'concept_hierarchy_code' || $dbColumnFromCsvHeader == 'concept_keywords' || $dbColumnFromCsvHeader == 'concept_description') {
                                $conceptId  = !empty($itemToBeCreated['concept_title']) ? 
                                $this->helperToFetchConceptIdFromEitherExistingOrNew($itemToBeCreated['concept_title']) : ""; 
                                $itemsToSave[$key]['concept_id'] = $conceptId;
                            } else if($dbColumnFromCsvHeader == 'license_title' || $dbColumnFromCsvHeader == 'license_text' || $dbColumnFromCsvHeader == 'license_description') {
                                $licenseId  = !empty($itemToBeCreated['license_title']) ? 
                                $this->helperToFetchLicenseIdFromEitherExistingOrNew($itemToBeCreated['license_title']) : ""; 
                                $itemsToSave[$key]['license_id'] = $licenseId;
                            } else {
                                if($dbColumnFromCsvHeader != 'node_type') {
                                    $itemsToSave[$key][$dbColumnFromCsvHeader] = $itemToBeCreated[$dbColumnFromCsvHeader];
                                }
                            }     
                        }
                    }

                    
                    if(isset($itemToBeCreated['node_type'])){

                        $nodeType =   trim($itemToBeCreated['node_type']);

                        $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $nodeType, 'is_deleted'   =>  '0' ];
                        $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);
        
                        if(count($nodeTypeExist) == 0) {
                            $nodeTypeId         =   $this->createNewNodeTypeAndMapWithMetadata($nodeType, $organizationIdentifier);
                            $arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];
                        }  
                        
                        if(!empty($nodeType)) {
                            $attributes     =   ['organization_id'  => $organizationIdentifier, 'title' => $nodeType, 'is_deleted'   =>  '0' ];
                            $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);
        
                            if(count($nodeTypeExist) > 0) {
                                $itemsToSave[$key]['node_type_id']  =   $nodeTypeExist[0]->node_type_id;
                                $nodeTypeUriObject  =   json_encode(
                                    [
                                        "identifier"    =>  $nodeTypeExist[0]->node_type_id,
                                        "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeExist[0]->node_type_id,  true, $orgCode,$this->domainName),
                                        "title"         =>  $nodeType
                                    ]
                                );
                
                                $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
                            
                            } else {
                                $nodeTypeId =    $this->createNewNodeTypeAndMapWithMetadata($nodeType, $organizationIdentifier);
                
                                $nodeTypeUriObject  =   json_encode(
                                    [
                                        "identifier"    =>  $nodeTypeId,
                                        "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeId,  true, $orgCode,$this->domainName),
                                        "title"         =>  $nodeType
                                    ]
                                );
                
                                $itemsToSave[$key]['node_type_id']          =   $nodeTypeId;
                                $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
                                
                                $arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];
                            }
                        } else {
                            $attributes     =   ['organization_id'  => $organizationIdentifier, 'is_default' => '1', 'is_deleted'   =>  '0' ];
                            $nodeTypeExist  =   $this->nodeTypeRepository->findByAttributes($attributes);
        
                            if(count($nodeTypeExist) > 0) {
                                $itemsToSave[$key]['node_type_id']  =   $nodeTypeExist[0]->node_type_id;
                                $nodeTypeUriObject  =   json_encode(
                                    [
                                        "identifier"    =>  $nodeTypeExist[0]->node_type_id,
                                        "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeExist[0]->node_type_id,  true, $orgCode,$this->domainName),
                                        "title"         =>  'Default'
                                    ]
                                );
                
                                $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
                            
                            } else {
                                $nodeTypeId =    $this->createNewNodeTypeAndMapWithMetadata('Default', $organizationIdentifier);
                
                                $nodeTypeUriObject  =   json_encode(
                                    [
                                        "identifier"    =>  $nodeTypeId,
                                        "uri"           =>  $this->getCaseApiUri("CFItemTypes", $nodeTypeId,  true, $orgCode,$this->domainName),
                                        "title"         =>  'Default'
                                    ]
                                );
                
                                $itemsToSave[$key]['node_type_id']          =   $nodeTypeId;
                                $itemsToSave[$key]['source_node_type_uri_object']  =   $nodeTypeUriObject;
                                
                                $arrayOfItemNodeTypeId[] = [$nodeType => $nodeTypeId];
                            }
                        }
                    }
                    
                    // get target document id based on destination node id start
                    $documentIdentifierP = $this->itemRepository->find($parentId);
                    $targetDocumentIdentifierP = $documentIdentifierP['document_id'];
                    $itemAssociationID = $this->createUniversalUniqueIdentifier();
                    $caseAssociationsToSave[] = [
                        "item_association_id" => $itemAssociationID,
                        "association_type" => 1,
                        "document_id" => $this->getCreatedDocumentIdentifier(),
                        "association_group_id" => '',
                        "origin_node_id" => $item_id,
                        "destination_node_id" => $parentId,
                        "destination_document_id" => $this->getCreatedDocumentIdentifier(),
                        "sequence_number" => $itemToBeCreated['csv_sequence'],
                        "external_node_title" =>"",
                        "external_node_url" =>"",
                        "created_at" => $currentDateTime,
                        "source_item_association_id" => $itemAssociationID,
                        "organization_id" => $organizationIdentifier,
                        "uri"=>$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode,$this->domainName),
                        "source_document_id" => $this->getCreatedDocumentIdentifier(),
                        "source_item_id" => $item_id,
                        "target_document_id" => $this->getCreatedDocumentIdentifier(),
                        "target_item_id" => $parentId
                    ];

                    $keyForItem++;
                    $newNodeCount++;
                    $this->newNodeCount             =   $newNodeCount;
                }
                
                /**
                * Data filtered to get all relations excluding isChildOf start
                */
                
                $getRawDataForMultipleAssociations = array();
                $exchildof=0;
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyKey=>$itemsFromCSVCopyValue)
                {
                    $itemsFromCSVCopyValue = array_change_key_case($itemsFromCSVCopyValue,CASE_LOWER);
                    $associationType = strtolower($itemsFromCSVCopyValue['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyValue['action']));
                    $exludeChildOfArray = array('ispeerof','ispartof','exactmatchof','precedes','isrelatedto','replacedby','haspart','hasskilllevel');
                    $actionsToConsider = array('a','u','add','update','');
                    if( (in_array((string)$actionType,$actionsToConsider)) && (in_array((string)$associationType,$exludeChildOfArray)) )
                    {
                        $getRawDataForMultipleAssociations[$exchildof]['node_id'] = $itemsFromCSVCopyValue['node_id'];
                        $getRawDataForMultipleAssociations[$exchildof]['node_parent_id'] = $itemsFromCSVCopyValue['node_parent_id'];
                        $getRawDataForMultipleAssociations[$exchildof]['case_association_type'] = $itemsFromCSVCopyValue['case_association_type'];
                        $getRawDataForMultipleAssociations[$exchildof]['document_id'] = $this->getCreatedDocumentIdentifier();
                        $getRawDataForMultipleAssociations[$exchildof]['sequence_number'] = $itemsFromCSVCopyValue['csv_sequence'];
                        $exchildof = $exchildof +1;
                    }
                }

                
                /**
                * Data filtered to get all relations excluding isChildOf end
                */

                /**
                * Data filtered to get all isChildOf relations of current taxonomy or other taxonomy start
                */

                $getRawDataForisChildOfAssociations = array();
                $inchildof=0;
                foreach($itemsFromCSVCopy as $itemsFromCSVCopyIndex=>$itemsFromCSVCopyData)
                {
                    $itemsFromCSVCopyData = array_change_key_case($itemsFromCSVCopyData,CASE_LOWER);
                    $associationType2 = strtolower($itemsFromCSVCopyData['case_association_type']);
                    $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyData['action']));
                    $includeChildOfArray = array('ischildof','');
                    $actionsToConsider = array('a','u','add','update','');
                    if( (in_array((string)$actionType,$actionsToConsider)) && ($itemsFromCSVCopyData['case_full_statement']=='') && (in_array((string)$associationType2,$includeChildOfArray)) )
                    {
                            $getRawDataForisChildOfAssociations[$inchildof]['node_id'] = $itemsFromCSVCopyData['node_id'];
                            $getRawDataForisChildOfAssociations[$inchildof]['node_parent_id'] = $itemsFromCSVCopyData['node_parent_id'];
                            $getRawDataForisChildOfAssociations[$inchildof]['case_association_type'] = $itemsFromCSVCopyData['case_association_type'];
                            $getRawDataForisChildOfAssociations[$inchildof]['document_id'] = $this->getCreatedDocumentIdentifier();
                            $getRawDataForisChildOfAssociations[$inchildof]['sequence_number'] = $itemsFromCSVCopyData['csv_sequence'];
                            $inchildof = $inchildof +1;
                    }
                }
                
                /**
                * Data filtered to get all isChildOf relations of current taxonomy or other taxonomy end 
                */
                
                /**
                 * Merge getRawDataForMultipleAssociations and getRawDataForisChildOfAssociations start
                 */

                $multipleNewAssociationsRawData = array();
                $multipleNewAssociationsRawData = array_merge($getRawDataForMultipleAssociations,$getRawDataForisChildOfAssociations);

                /**
                 * Merge getRawDataForMultipleAssociations and getRawDataForisChildOfAssociations start
                 */


                /**
                *    Mapping origin node id start 
                */
                $getOriginNodeIdMapped = array();
                $dt=0;
                foreach($multipleNewAssociationsRawData as $multipleNewAssociationsRawDataK=>$multipleNewAssociationsRawDataV)
                {
                    foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemK=>$arrayOfOldItemMappedToNewItemV)
                    {
                        foreach($arrayOfOldItemMappedToNewItemV as $arrayOfOldItemMappedToNewItemVKey=>$arrayOfOldItemMappedToNewItemVValue)
                        {
                            if( trim(preg_replace('/\t+/', '', $multipleNewAssociationsRawDataV['node_id']))==$arrayOfOldItemMappedToNewItemVKey)
                            {
                                $getOriginNodeIdMapped[$dt]['source_item_id'] = $arrayOfOldItemMappedToNewItemVValue;
                                $getOriginNodeIdMapped[$dt]['node_parent_id'] = $multipleNewAssociationsRawDataV['node_parent_id'];
                                $getOriginNodeIdMapped[$dt]['case_association_type'] = $multipleNewAssociationsRawDataV['case_association_type'];
                                $getOriginNodeIdMapped[$dt]['document_id'] = $multipleNewAssociationsRawDataV['document_id'];
                                $getOriginNodeIdMapped[$dt]['sequence_number'] = $multipleNewAssociationsRawDataV['sequence_number'];
                                $dt = $dt+1;
                            }
                        }
                    }
                }
                
                
                /**
                *    Mapping origin node id end 
                */


                /**
                * Mapping destination node id start
                */
                
                $getDestinationNodeIdMapped = array();
                $dni=0;
                foreach($getOriginNodeIdMapped as $getOriginNodeIdMappedK=>$getOriginNodeIdMappedV)
                {
                    foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemKey => $arrayOfOldItemMappedToNewItemValue)
                    {
                        foreach($arrayOfOldItemMappedToNewItemValue as $arrayOfOldItemMappedToNewItemValueK=>$arrayOfOldItemMappedToNewItemValueV)
                        {
                            if(trim(preg_replace('/\t+/', '', $getOriginNodeIdMappedV['node_parent_id']))==$arrayOfOldItemMappedToNewItemValueK)
                            {
                                $getDestinationNodeIdMapped[$dni]['source_item_id'] = $getOriginNodeIdMappedV['source_item_id'];
                                $getDestinationNodeIdMapped[$dni]['target_item_id'] = $arrayOfOldItemMappedToNewItemValueV;
                                $getDestinationNodeIdMapped[$dni]['case_association_type'] = $getOriginNodeIdMappedV['case_association_type'];
                                $getDestinationNodeIdMapped[$dni]['document_id'] = $getOriginNodeIdMappedV['document_id'];
                                $getDestinationNodeIdMapped[$dni]['sequence_number'] = $getOriginNodeIdMappedV['sequence_number'];
                                $dni = $dni+1;
                            }
                            // else
                            // {

                            // }
                        }
                    }
                }
                
                /**
                * Mapping destination node id end
                */

                /**
                 * Map association of other taxonomy from same tenant start
                 */
                
                $getAssociationRelationOfOtherTenant = array();
                $extAsso=0;
                
                foreach($getOriginNodeIdMapped as $getOriginNodeIdMappedKey=>$getOriginNodeIdMappedValue)
                {
                    if( !in_array(trim(preg_replace('/\t+/', '', $getOriginNodeIdMappedValue['node_parent_id'])),$getUniqueitemIdArray) )
                    {
                        $count = $this->checkParentNodeIdExist(trim(preg_replace('/\t+/', '', $getOriginNodeIdMappedValue['node_parent_id'])));
                        if($count==1)
                        {
                            $getAssociationRelationOfOtherTenant[$extAsso]['source_item_id'] = $getOriginNodeIdMappedValue['source_item_id'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['target_item_id'] = $getOriginNodeIdMappedValue['node_parent_id'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['case_association_type'] = $getOriginNodeIdMappedValue['case_association_type'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['document_id'] = $getOriginNodeIdMappedValue['document_id'];
                            $getAssociationRelationOfOtherTenant[$extAsso]['sequence_number'] = $getOriginNodeIdMappedValue['sequence_number'];
                            $extAsso = $extAsso+1;
                        }
                    }
                }
                
                /**
                 * Map association of other taxonomy from same tenant end
                 */

                 /**
                  * Map association on same taxonomy and other taxonomy/same tenant taxonomy start
                  */
                
                $getAssociationData = array();
                $getAssociationData = array_merge($getDestinationNodeIdMapped,$getAssociationRelationOfOtherTenant);  
                
                /**
                  * Map association on same taxonomy and other taxonomy/same tenant taxonomy end
                  */


                // get difference of valid associations and invalid associations start

                $newAssociationsCreated = array();
                $allAssociationsPresent = array();
                $getItemIdAndSourceIdMappingArray = array();
                if(!empty($getAssociationData))
                {
                    $newAssociationsCreated = array_column($getAssociationData,'target_item_id');
                    $allAssociationsPresent = array_column($getOriginNodeIdMapped,'node_parent_id');
                }

                $getDiffAssociation = array_diff($allAssociationsPresent,$newAssociationsCreated);
                if(!empty($getDiffAssociation))
                {
                    $getItemIdAndSourceIdMappingArray = $this->getItemIdAndSourceIdMappingArray($getDiffAssociation);
                }
                
                $getIdsFromJsonImportType1And2 = array();
                $dtm = 0;
                if(!empty($getItemIdAndSourceIdMappingArray))
                {
                    foreach($getItemIdAndSourceIdMappingArray as $getItemIdAndSourceIdMappingArrayK=>$getItemIdAndSourceIdMappingArrayV)
                    {
                        if(!in_array((string)$getItemIdAndSourceIdMappingArrayV->source_item_id,$getUniqueitemIdArray))
                        {
                            $getIdsFromJsonImportType1And2[$dtm][$getItemIdAndSourceIdMappingArrayV->item_id] = $getItemIdAndSourceIdMappingArrayV->source_item_id;
                            $dtm = $dtm+1;
                        }
                    }

                }
                
                // get difference of valid associations and invalid associations end


                /**
                * Map association of another taxonomy which are imported as import type 1 and 3 for json start 
                */
                
                $createAssociationFromSourceItemId = array();
                $ktm=0;
                if(!empty($getIdsFromJsonImportType1And2))
                {
                    foreach($getOriginNodeIdMapped as $getOriginNodeIdMappedKeyData=>$getOriginNodeIdMappedValueData)
                    {
                        foreach($getIdsFromJsonImportType1And2 as $getIdsFromJsonImportType1And2K=>$getIdsFromJsonImportType1And2V)
                        {
                            foreach($getIdsFromJsonImportType1And2V as $getIdsFromJsonImportType1And2VK=>$getIdsFromJsonImportType1And2VV)
                            {
                                if($getOriginNodeIdMappedValueData['node_parent_id']==$getIdsFromJsonImportType1And2VV)
                                {
                                    $createAssociationFromSourceItemId[$ktm]['source_item_id'] = $getOriginNodeIdMappedValueData['source_item_id'];
                                    $createAssociationFromSourceItemId[$ktm]['target_item_id'] = $getIdsFromJsonImportType1And2VK;
                                    $createAssociationFromSourceItemId[$ktm]['case_association_type'] = $getOriginNodeIdMappedValueData['case_association_type'];
                                    $createAssociationFromSourceItemId[$ktm]['document_id'] = $getOriginNodeIdMappedValueData['document_id'];
                                    $createAssociationFromSourceItemId[$ktm]['sequence_number'] = $getOriginNodeIdMappedValueData['sequence_number'];
                                    $ktm = $ktm+1;
                                }
                            }
                        }
                    }
                }

                /**
                * Map association of another taxonomy which are imported as import type 1 and 3 for json end
                */

                if(!empty($createAssociationFromSourceItemId))
                {
                    $getAssociationData = array_merge($getAssociationData,$createAssociationFromSourceItemId);
                }
                
                /**
                 *   Creating Array for Associations start
                 */

                $mainAssociationArray = array(1=>'ischildof',2=>'ispeerof',3=>'ispartof',4=>'exactmatchof',5=>'precedes',6=>'isrelatedto',7=>'replacedby',9=>'hasskilllevel'); 

                 /**
                 *   Creating Array for Associations end
                 */


                /**
                 * Map association of another taxonomy which are imported as import type 1 and 3 for json start (runs only if it has json 1 and 3 type association)
                 */
                $onlyJson1and3TypeAssociationToImport = array();
                if(!empty($getOriginNodeIdMapped) && empty($newAssociationsCreated) &&  empty($allAssociationsPresent))
                {
                    foreach($getOriginNodeIdMapped as $KeyData=>$KeyValue)
                    {
                        $dataFetched = $this->getItemIdAndSourceIdMappingArrayUpdate($KeyValue['node_parent_id']);
                        if(!empty($dataFetched))
                        {
                            // get target document id based on destination node id start
                            $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($dataFetched[0]->item_id,$organizationIdentifier);
                            $targetDocumentIdentifierDM = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $dataFetched[0]->item_id;
                            $associationTypeRelation = array_keys($mainAssociationArray,strtolower(trim(!empty($KeyValue['case_association_type'])?$KeyValue['case_association_type']:'ischildof')));
                            $itemAssociationID = $this->createUniversalUniqueIdentifier();
                            $onlyJson1and3TypeAssociationToImport['item_association_id'] = $itemAssociationID;
                            $onlyJson1and3TypeAssociationToImport["association_type"] = $associationTypeRelation[0];
                            $onlyJson1and3TypeAssociationToImport["document_id"] = $KeyValue['document_id'];
                            $onlyJson1and3TypeAssociationToImport["association_group_id"] = '';
                            $onlyJson1and3TypeAssociationToImport["origin_node_id"] = $KeyValue['source_item_id'];
                            $onlyJson1and3TypeAssociationToImport["destination_node_id"] = $dataFetched[0]->item_id;
                            $onlyJson1and3TypeAssociationToImport["destination_document_id"] = $KeyValue['document_id'];
                            $onlyJson1and3TypeAssociationToImport["sequence_number"] = $KeyValue['sequence_number'];
                            $onlyJson1and3TypeAssociationToImport["external_node_title"] ="";
                            $onlyJson1and3TypeAssociationToImport["external_node_url"] ="";
                            $onlyJson1and3TypeAssociationToImport["created_at"] = $currentDateTime;
                            $onlyJson1and3TypeAssociationToImport["source_item_association_id"] = $itemAssociationID;
                            $onlyJson1and3TypeAssociationToImport["organization_id"] = $organizationIdentifier;
                            $onlyJson1and3TypeAssociationToImport["uri"]=$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode);
                            $onlyJson1and3TypeAssociationToImport["source_document_id"] = $KeyValue['document_id'];
                            $onlyJson1and3TypeAssociationToImport["source_item_id"] = $KeyValue['source_item_id'];
                            $onlyJson1and3TypeAssociationToImport["target_document_id"] = $targetDocumentIdentifierDM;
                            $onlyJson1and3TypeAssociationToImport["target_item_id"] = $dataFetched[0]->item_id;
                            DB::table('item_associations')->insert($onlyJson1and3TypeAssociationToImport);
                        }
                    }
                }
                 


                /**
                 * Map association of another taxonomy which are imported as import type 1 and 3 for json start (runs only if it has json 1 and 3 type association)
                 */ 
                 
                
                /**
                 * Filtering Item id to delete for action delete(D) start
                 */

                $itemIdsToBeDeleted = array();
                $actionsToConsider = array('d','delete');
                foreach($actionMappedWithOldItemId as $actionMappedWithOldItemIdK=>$actionMappedWithOldItemIdV)
                {
                    foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemData=>$arrayOfOldItemMappedToNewItemRecord)
                    {
                        foreach($arrayOfOldItemMappedToNewItemRecord as $arrayOfOldItemMappedToNewItemRecordK=>$arrayOfOldItemMappedToNewItemRecordV)
                        {   
                            $actionMappedWithOldItemIdV = strtolower($actionMappedWithOldItemIdV);
                            if( ($actionMappedWithOldItemIdK==$arrayOfOldItemMappedToNewItemRecordK) && (in_array((string)$actionMappedWithOldItemIdV,$actionsToConsider) ) )
                            {
                                $itemIdsToBeDeleted[]=$arrayOfOldItemMappedToNewItemRecordV;
                            }
                        }
                    }
                } 
                
                /**
                 * Filtering Item id to delete for action delete(D) end
                 */
                
                /**
                 * Filtering old item id without tree child parent relation start
                 */ 
                
                $treeChildForOldItemIdWithOldParentId = array_push($treeChildForOldItemId,$oldParentId);
                $getDiffOldParentId = array_diff($treeParentForOldItemId,$treeChildForOldItemId);
                
                $getOldChildIdWithoutTreeRelation = array();
                if(!empty($getDiffOldParentId))
                {
                    foreach($getDiffOldParentId as $getDiffOldParentIdK=>$getDiffOldParentIdV)
                    {
                        foreach($treeChildParentRelationForOldItem as $treeChildParentRelationForOldItemK=>$treeChildParentRelationForOldItemV)
                        {
                            if($getDiffOldParentIdV==$treeChildParentRelationForOldItemV)
                            {
                                foreach($arrayOfOldItemMappedToNewItem as $arrayOfOldItemMappedToNewItemK=>$arrayOfOldItemMappedToNewItemV)
                                {
                                    foreach($arrayOfOldItemMappedToNewItemV as $arrayOfOldItemMappedToNewItemVKey=>$arrayOfOldItemMappedToNewItemVValue)
                                    {
                                        if($treeChildParentRelationForOldItemK==$arrayOfOldItemMappedToNewItemVKey)
                                        {
                                            $getOldChildIdWithoutTreeRelation[] = $arrayOfOldItemMappedToNewItemVValue;
                                        }
                                    }
                                }
                            }
                        } 
                    }
                }

                #assign sequence no to child associations
                // $caseAssociationsToSave = $this->itemSequenceNo($itemsToSave, $caseAssociationsToSave);
                // $allAssoMaxSeqNoItemResult = $this->getMaxSeqNoOfItems($caseAssociationsToSave);
                /**
                 * Filtering old item id without tree child parent relation start
                 */ 

                if(!empty($itemsToSave)) {
                    $recordsToSaveCollection = collect($itemsToSave);
                    foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
                        $this->itemRepository->saveMultiple($chunk->toArray());
                    }

                    $recordsAssociationToSaveCollection = collect($caseAssociationsToSave);
                    foreach ($recordsAssociationToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
                        $this->itemAssociationRepository->saveMultiple($chunk->toArray());
                    }
                }

                /** Inserting new added associations start */


                /**
                 * Creating final data of Associations start
                 */ 
                $finalAssociationArray = array();
                $as=0;
                foreach($getAssociationData as $getAssociationDataK=>$getAssociationDataV)
                {
                    // get target document id based on destination node id start
                    $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($getAssociationDataV['target_item_id'],$organizationIdentifier);
                    $targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $getAssociationDataV['target_item_id'];
                    $associationTypeRelation = array_keys($mainAssociationArray,strtolower(trim(!empty($getAssociationDataV['case_association_type'])?$getAssociationDataV['case_association_type']:'ischildof')));
                    $itemAssociationID = $this->createUniversalUniqueIdentifier();
                    # sequence no of association
                    // $sequenceNo = 0;
                    // if($associationTypeRelation[0] != 1) {
                    //     if(isset($allAssoMaxSeqNoItemResult[$getAssociationDataV['source_item_id']])) {
                    //         $sequenceNo = $allAssoMaxSeqNoItemResult[$getAssociationDataV['source_item_id']] + 1;
                    //     } else {
                    //         $sequenceNo = 1;
                    //     }
                    //     $allAssoMaxSeqNoItemResult[$getAssociationDataV['source_item_id']] = $sequenceNo;
                    // } else {
                    //     $sequenceNo = isset($this->allChildSeqArr[$getAssociationDataV['source_item_id']]) ? $this->allChildSeqArr[$getAssociationDataV['source_item_id']] + 1 : 0;
                    //     $this->allChildSeqArr[$getAssociationDataV['source_item_id']] = $sequenceNo;
                    // }

                    $finalAssociationArray[$as]["item_association_id"] = $itemAssociationID;
                    $finalAssociationArray[$as]["association_type"] = $associationTypeRelation[0];
                    $finalAssociationArray[$as]["document_id"] = $getAssociationDataV['document_id'];
                    $finalAssociationArray[$as]["association_group_id"] = '';
                    $finalAssociationArray[$as]["origin_node_id"] = $getAssociationDataV['source_item_id'];
                    $finalAssociationArray[$as]["destination_node_id"] = $getAssociationDataV['target_item_id'];
                    $finalAssociationArray[$as]["destination_document_id"] = $getAssociationDataV['document_id'];
                    $finalAssociationArray[$as]["sequence_number"] = $getAssociationDataV['sequence_number'];
                    $finalAssociationArray[$as]["external_node_title"] ="";
                    $finalAssociationArray[$as]["external_node_url"] ="";
                    $finalAssociationArray[$as]["created_at"] = $currentDateTime;
                    $finalAssociationArray[$as]["source_item_association_id"] = $itemAssociationID;
                    $finalAssociationArray[$as]["organization_id"] = $organizationIdentifier;
                    $finalAssociationArray[$as]["uri"]=$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode,$this->domainName);
                    $finalAssociationArray[$as]["source_document_id"] = $getAssociationDataV['document_id'];
                    $finalAssociationArray[$as]["source_item_id"] = $getAssociationDataV['source_item_id'];
                    $finalAssociationArray[$as]["target_document_id"] = $targetDocumentIdentifier;
                    $finalAssociationArray[$as]["target_item_id"] = $getAssociationDataV['target_item_id'];
                    $as = $as+1;
                }
                /**
                 * Creating final data of Associations end
                 */
                
                if(!empty($finalAssociationArray)) {
                    
                    $recordsAssociationToSaveCollection1 = collect($finalAssociationArray);
                    foreach ($recordsAssociationToSaveCollection1->chunk($this->recordChunkCount) as $chunk) {
                        $this->itemAssociationRepository->saveMultiple($chunk->toArray());
                    }
                }
                /** Inserting new added associations end */

                /**
                 * soft deleting items with action delete(d) start
                */
                
                if(!empty($itemIdsToBeDeleted))
                {
                    $update = ['is_deleted' => '1'];

                    foreach($itemIdsToBeDeleted as $itemIdsToBeDeletedKey=>$itemIdsToBeDeletedValue)
                    {
                        DB::table('items')->where('item_id', $itemIdsToBeDeletedValue)
                                      ->where('organization_id',$organizationIdentifier)
                                      ->update($update);

                        DB::table('items')->where('parent_id', $itemIdsToBeDeletedValue)
                                        ->where('organization_id',$organizationIdentifier)
                                        ->update($update);

                        DB::table('item_associations')->where('source_item_id', $itemIdsToBeDeletedValue)
                                                    ->where('organization_id',$organizationIdentifier)
                                                    ->update($update);

                        DB::table('item_associations')->where('target_item_id', $itemIdsToBeDeletedValue)
                                                    ->where('organization_id',$organizationIdentifier)
                                                    ->update($update);
                        $newNodeCount--;
                        $newAssociationsCount--;
                    }
                                                  
                }

                /**
                 * soft deleting items with action delete(d) end
                */

                /**
                 * soft delete items that don't have parent child relation in csv start
                 */
                if(!empty($getOldChildIdWithoutTreeRelation))
                    {
                        $update = ['is_deleted' => '1'];

                        foreach($getOldChildIdWithoutTreeRelation as $getOldChildIdWithoutTreeRelationKey=>$getOldChildIdWithoutTreeRelationValue)
                            {
                                DB::table('items')->where('item_id', $getOldChildIdWithoutTreeRelationValue)
                                            ->where('organization_id',$organizationIdentifier)
                                            ->update($update);

                                DB::table('items')->where('parent_id', $getOldChildIdWithoutTreeRelationValue)
                                                ->where('organization_id',$organizationIdentifier)
                                                ->update($update);

                                DB::table('item_associations')->where('source_item_id', $getOldChildIdWithoutTreeRelationValue)
                                                            ->where('organization_id',$organizationIdentifier)
                                                            ->update($update);

                                DB::table('item_associations')->where('target_item_id', $getOldChildIdWithoutTreeRelationValue)
                                                            ->where('organization_id',$organizationIdentifier)
                                                            ->update($update);
                                $newNodeCount--;
                                $newAssociationsCount--;
                            }

                    }
                $caseAssociationsToSave    = array_merge($caseAssociationsToSave,$finalAssociationArray);
                $newAssociationsCount       =   $newAssociationsCount + sizeOf($caseAssociationsToSave);
                $this->newAssociationsCount =   $newAssociationsCount;
                $this->newNodeCount         =   $newNodeCount;
                $this->setCreatedItemArray($itemsToSave);
            
        }
        $this->setNodeTypeIdArray($arrayOfItemNodeTypeId);

        
    }

    private function getMaxSeqNoOfItems($allChildAssociation)
    {
        $allAssoMaxSeqNoItem = [];
        foreach($allChildAssociation as $asso) {
            if(!isset($allAssoMaxSeqNoItem[$asso['source_item_id']])) {
                $allAssoMaxSeqNoItem[$asso['source_item_id']] = (int)$asso['sequence_number'];
            } else {
                $thisSeqNo = (int)$asso['sequence_number'];
                if($allAssoMaxSeqNoItem[$asso['source_item_id']] < $thisSeqNo) {
                    $allAssoMaxSeqNoItem[$asso['source_item_id']] = $thisSeqNo;
                }
            }
        }

        return $allAssoMaxSeqNoItem;
    }

    private function processAndSaveCaseSubjectsData() {
        $requestImportType      =   $this->getImportType();
        $subjectsToSave         =   $this->getCaseSubjectsToSave();
        $subjectsAlreadySaved   =   $this->getSavedSubjects();
        if(!empty($subjectsToSave)) {
            $organizationId     =   $this->getOrganizationIdentifier();
            $newSubjectsToSave  =   [];
            foreach($subjectsToSave as $subjectToSave) {
                if(!empty($subjectToSave["title"])){
                    $subjectTitleToSearchWith = trim($subjectToSave["title"]);
                    $savedSubjectCollection = $subjectsAlreadySaved->where("title", $subjectTitleToSearchWith);

                    // if subject doesn't exists in the db then only save it and update the list of saved subjects
                    if($savedSubjectCollection->isEmpty()) {
                        $newSubjectIdentifier       =   $this->createUniversalUniqueIdentifier();
                        $subjectToSaveIdentifier    =   $newSubjectIdentifier;
                        $orgCode                    =   $this->getOrgCode();
                        $newSubjectTitle            =   $subjectTitleToSearchWith;
                        $newSubjectHierarchyCode    =   !empty($subjectToSave["hierarchyCode"]) ? trim($subjectToSave["hierarchyCode"]) : "";
                        $newSubjectDescription      =   !empty($subjectToSave["description"]) ? trim($subjectToSave["description"]) : "";
                        $lastChangeDateTime         =   !empty($subjectToSave["lastChangeDateTime"]) ? $this->formatDateTime($subjectToSave["lastChangeDateTime"]) : null;

                        $uriToSave                  =   $this->getCaseApiUri("CFSubjects", $newSubjectIdentifier, true, $orgCode,$this->domainName);
                        
                        $newSubject = [
                            "subject_id"        => $newSubjectIdentifier, 
                            "organization_id"   => $organizationId,
                            "title"             => $newSubjectTitle,
                            "hierarchy_code"    => $newSubjectHierarchyCode,
                            "description"       => $newSubjectDescription,
                            "updated_at"        => $lastChangeDateTime,
                            "source_subject_id" => $subjectToSaveIdentifier, 
                            "uri"               => $uriToSave
                        ];

                        $newSubjectsToSave[] = $newSubject;
                        $newSubjectSaved = $this->subjectRepository->createModelInstanceInMemory($newSubject);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $subjectsAlreadySaved->push($newSubjectSaved);
                    }
                }
            }
            $this->subjectRepository->saveMultiple($newSubjectsToSave);
        }
    }

    private function processAndSaveCaseConceptsData() {
        $requestImportType      =   $this->getImportType(); 
        $conceptsToSave         =   $this->getCaseConceptsToSave();
        $conceptsAlreadySaved   =   $this->getSavedConcepts();

        //print_r($conceptsAlreadySaved->toArray());

        if(!empty($conceptsToSave)) {
            $organizationId     =   $this->getOrganizationIdentifier();
            $newConceptsToSave  =   [];

            foreach($conceptsToSave as $conceptToSave) {
                if(!empty($conceptToSave["title"])){
                    $conceptTitleToSearchWith   =   trim($conceptToSave["title"]);
                    $savedConceptCollection     =   $conceptsAlreadySaved->where("title", $conceptTitleToSearchWith);
                    //print_r($savedConceptCollection->toArray());
                    // if concept doesn't exists in the db then only save it and update the list of saved concepts
                    if($savedConceptCollection->isEmpty()) {
                        $newConceptIdentifier       =   $this->createUniversalUniqueIdentifier() ;
                        $conceptToSaveIdentifier    =   $newConceptIdentifier;
                        $newConceptKeywords         =   !empty($conceptToSave["keywords"]) ? $conceptToSave["keywords"] : "";
                        $newConceptTitle            =   $conceptTitleToSearchWith;
                        $orgCode                    =   $this->getOrgCode();
                        $newConceptHierarchyCode    =   !empty($conceptToSave["hierarchyCode"]) ? trim($conceptToSave["hierarchyCode"]) : "";
                        $newConceptDescription      =   !empty($conceptToSave["description"]) ? trim($conceptToSave["description"]) : "";
                        $lastChangeDateTime         =   !empty($conceptToSave["lastChangeDateTime"]) ? $this->formatDateTime($conceptToSave["lastChangeDateTime"]) : null;

                        $uriToSave                  =   $this->getCaseApiUri("CFConcepts", $newConceptIdentifier, true, $orgCode,$this->domainName);

                        $newConcept = [
                            "concept_id"        => $newConceptIdentifier,
                            "organization_id"   => $organizationId,
                            "keywords"          => $newConceptKeywords,
                            "title"             => $newConceptTitle,
                            "description"       => $newConceptDescription,
                            "hierarchy_code"    => $newConceptHierarchyCode,
                            "updated_at"        => $lastChangeDateTime,
                            "source_concept_id" => $conceptToSaveIdentifier,
                            "uri"               => $uriToSave 
                        ];


                        $newConceptsToSave[] = $newConcept;
                        $newConceptSaved = $this->conceptRepository->createModelInstanceInMemory($newConcept);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $conceptsAlreadySaved->push($newConceptSaved);
                    }
                }
            }
            if(!empty($newConceptsToSave)) {
                $this->conceptRepository->saveMultiple($newConceptsToSave);
            }
        }
    }

    private function processAndSaveCaseLicensesData() {
        $requestImportType      =   $this->getImportType();
        $licensesToSave         =   $this->getCaseLicensesToSave();
        $licensesAlreadySaved   =   $this->getSavedLicenses();

        if(!empty($licensesToSave)) {
            $organizationId     =   $this->getOrganizationIdentifier();
            $newLicensesToSave  =   [];

            foreach($licensesToSave as $licenseToSave) {
                if(!empty($licenseToSave["title"])){
                    $licenseTitleToSearchWith   =   trim($licenseToSave["title"]);
                    $savedLicenseCollection     =   $licensesAlreadySaved->where("title", $licenseTitleToSearchWith);
                    // if license doesn't exists in the db then only save it and update the list of saved licenses
                    if($savedLicenseCollection->isEmpty()) {
                        $newLicenseIdentifier       =   $this->createUniversalUniqueIdentifier() ;
                        $licenseToSaveIdentifier    =   $newLicenseIdentifier;
                        $newLicenseTitle            =   $licenseTitleToSearchWith;
                        $orgCode                    =   $this->getOrgCode();
                        $newLicenseDescription      =   !empty($licenseToSave["description"]) ? trim($licenseToSave["description"]) : "";
                        $newLicenseText             =   !empty($licenseToSave["licenseText"]) ? trim($licenseToSave["licenseText"]) : "";
                        $lastChangeDateTime         =   !empty($licenseToSave["lastChangeDateTime"]) ? $this->formatDateTime($licenseToSave["lastChangeDateTime"]) : null;

                        
                        $uriToSave                  =   $this->getCaseApiUri("CFLicenses", $newLicenseIdentifier, true, $orgCode,$this->domainName);
                        
                        $newLicense = [
                            "license_id"        => $newLicenseIdentifier, 
                            "organization_id"   => $organizationId,
                            "title"             => $newLicenseTitle,
                            "description"       => $newLicenseDescription,
                            "license_text"      => $newLicenseText,
                            "updated_at"        => $lastChangeDateTime,
                            "source_license_id" => $licenseToSaveIdentifier,
                            "uri"               => $uriToSave
                        ];

                        $newLicensesToSave[] = $newLicense;
                        $newLicenseSaved = $this->licenseRepository->createModelInstanceInMemory($newLicense);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $licensesAlreadySaved->push($newLicenseSaved);
                    }
                }
            }
            if(!empty($newLicensesToSave)) {
                $this->licenseRepository->saveMultiple($newLicensesToSave);
            }
            
        }
    }

    private function getCustomMetadataDetailForDocumentAndItemFromCSV($inputData) {
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $documentDetail = $this->documentRepository->getDocumentDetails(trim($inputData[0]['node_id']));
        if(!empty($documentDetail)) {
            $documentDetail = $documentDetail->toArray();
        }
        $customMetadataOfItemsFromCsv[$documentDetail['document_id']]   =   [];
        if(isset($customMetadataOfItemsFromCsv[$documentDetail['document_id']]) && is_array($documentDetail['custom_metadata']) && sizeOf($documentDetail['custom_metadata']) > 0){
            $customMetadataOfItemsFromCsv[$documentDetail['document_id']] = $documentDetail['custom_metadata'];  
        }

        //$customMetadata[$itemId] = $documentDetail['custom_metadata'][] 
        unset($inputData[0]);
        $idsFromCsv             =  array_column($inputData, 'node_id');

        $attributeIn            =   'item_id'; 
        $containedInValues      =   $idsFromCsv;
        $returnCollection       =   true;
        $fieldsToReturn         =   ['*'];
        $keyValuePairs          =   ['is_deleted' => 0, 'organization_id' => $organizationIdentifier];
        $returnPaginatedData    =   false;
        $paginationFilters      =   ['limit' => 10, 'offset' => 0];
        $returnSortedData       =   false;
        $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator               =   'AND';
        $relations              =   ['customMetadata'];

        $itemDetail = $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator, 
            $relations );

        if(!empty($itemDetail)) {
            $itemDetail = $itemDetail->toArray();
            foreach($itemDetail as $item) {
                $customMetadataOfItemsFromCsv[$item['item_id']] = $item['custom_metadata'];
            }
        }
        

        return $customMetadataOfItemsFromCsv;
    }

    /**
     * Method to update existing custom metadata in db or
     * to create new additional metadata
     *
     * @return void
     */
    private function updateMetadataData() {
        $nodeTypeFromCsv                =   $this->nodeType;
        $packageExists                  =   $this->packageExists;  
        $newNodeCount                   =   $this->newNodeCount;
        $updatedMetadataCount           =   $this->updatedMetadataCount;
        $blankMetadataCount             =   $this->blankMetadataCount;
        $newAdditionalMetadataCount     =   $this->newAdditionalMetadataCount;
        $metadataListForATenant         =   $this->metadataListForATenant;

        $importType             =   $this->getImportType();
        
        $documentIdentifier     =   $this->getCreatedDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $columnsFromHeaders     =   $this->getCustomMetadataSet();
        $nodeTypeIdArray        =   $this->getNodeTypeIdArray();
        $inputData              =   $this->inputData;

        /**
         * Data filtered to get unique id based on node_id and action and case field start
         */
        $getDocumentNodeData = array();
        foreach($inputData  as $inputDataK=>$inputDataV) {
            if( trim(preg_replace('/\t+/', '', $inputDataV['node_parent_id']))=="" )
            {
                $getDocumentNodeData[]  =  $inputDataV;
            }
        }


        foreach($inputData  as $itemId) {
            $itemIdArray[]  =   trim($itemId['node_id']);
        }    
        
        $itemsFromCSVCopy = $inputData;
        $getUniqueitemIdArray = array();
        $getUniqueitemIdArray = array_unique($itemIdArray);
        
        $getItemArrayDataToInsert = array();
        foreach($getUniqueitemIdArray as $getUniqueitemIdArrayK=>$getUniqueitemIdArrayV)
        {
            foreach($itemsFromCSVCopy as $itemsFromCSVCopyK=>$itemsFromCSVCopyV)
            {
                $itemsFromCSVCopyVData = $itemsFromCSVCopyV;
                $itemsFromCSVCopyV = array_change_key_case($itemsFromCSVCopyV,CASE_LOWER);
                $associationType2 = strtolower(str_replace(' ','',$itemsFromCSVCopyV['case_association_type']));
                $actionType = strtolower(str_replace(' ','',$itemsFromCSVCopyV['action']));
                $actionsToConsider = array('a','u','d','add','update','delete','');
                $includeChildOfArray = array('ischildof','');

                if( ($getUniqueitemIdArrayV==trim(preg_replace('/\t+/', '', $itemsFromCSVCopyV['node_id'])) ) && (in_array((string)$actionType,$actionsToConsider)) && (!empty($itemsFromCSVCopyV['case_full_statement'])) && (in_array((string)$associationType2,$includeChildOfArray)) )
                {
                        $getItemArrayDataToInsert[]=$itemsFromCSVCopyVData;
                }
            }
        }
        
        $itemArrayDataCopy = array();
        foreach($getItemArrayDataToInsert as $getItemArrayDataToInsertK=>$getItemArrayDataToInsert)
        {
            $itemArrayDataCopy[] = $getItemArrayDataToInsert;
        }
        
        
        $inputData = array_merge($getDocumentNodeData,$itemArrayDataCopy);
        // $inputData = $itemArrayDataCopy;
        
        /**
         * Data filtered to get unique id based on node_id and action and case field end
         */

        
        $fieldTypeValues                =   config("default_enum_setting.metadata_field_type");
        $arrayOfCreatedItemId           =   [];
        $formattedDataFromCsv           =   [];
        $customMetadataOfItemsFromCsv   =   [];
        $metadataListOfNodeTypeArray    =   [];
        $customMetadataType             =   [];
        $nodeIdArray                    =   [];
        $formattedMetadataFromDb        =   [];
        $newCreatedItemArray            =   [];
        $arrayOfItemIdFromDb            =   [];
        $saveNodeType                   =   [];
        $itemIdArray                    =   [];

        $nodeIdArray    =   array_map('trim', array_column($inputData, 'node_id'));
        $nodeTypeArray  =   array_map('trim', array_column($inputData, $nodeTypeFromCsv)); 

        $itemCollectionArray        =   $this->getItemCollectionFromDb();
        //dd($itemCollectionArray);
        if(!empty($itemCollectionArray)){
            $arrayOfItemIdFromDb =   !empty($itemCollectionArray) ? array_column($itemCollectionArray, 'node_id') : [];
            array_unshift($arrayOfItemIdFromDb, trim($inputData[0]['node_id']));
            
        }
        $sourceDocumentId = $inputData[0]['node_id'];
        

        if($importType == 4) {
            
            $customMetadataOfItemsFromCsv                       =   $this->getCustomMetadataDetailForDocumentAndItemFromCSV($inputData);
            $arrayOfCreatedItemId                               =   $this->getArrayOfCreatedItemId();
            $itemFromCsv                                        =   $this->getNewItemArray();
            //dd($arrayOfCreatedItemId);
            if($arrayOfCreatedItemId !== null){
                foreach($arrayOfCreatedItemId as $key => $createdItem) {
                    $itemIdArray[]    =   $createdItem[$itemFromCsv[$key]['node_id']];
                }
            }
            
            foreach($inputData as $dataFromCsv) {
                $formattedDataFromCsv[trim($dataFromCsv['node_id'])] = $dataFromCsv;
            }

            if($arrayOfCreatedItemId !== null){
                foreach($arrayOfCreatedItemId as $newItem){
                    $newCreatedItemArray[array_keys($newItem)[0]]    =   $newItem[array_keys($newItem)[0]];
                }
            }

            
            
        } else {
            if($packageExists == 1) {
                $arrayOfCreatedItemId   =   $this->getCreatedItemArray();
                foreach($arrayOfCreatedItemId as $createdItem) {
                    $itemIdArray[]    =   $createdItem['item_id'];
                }
            } else {
                $arrayOfCreatedItemId       =   $this->getArrayOfCreatedItemId();
                $itemFromCsv = $this->getNewItemArray();
                //dd($itemFromCsv);
                foreach($arrayOfCreatedItemId as $key => $createdItem) {
                    $itemIdArray[]    =   $createdItem[trim($itemFromCsv[$key]['node_id'])];
                }
            }
            $formattedDataFromCsv[$documentIdentifier] = array_map('trim', $inputData[0]);
            $formattedDataFromCsv[$documentIdentifier]['node_id'] = $documentIdentifier;

            //dd($itemCollectionArray);
            unset($inputData[0]);
            foreach($inputData as $inputDataKey => $data) {
                $formattedDataFromCsv[$itemIdArray[($inputDataKey-1)]] = array_map('trim', $data);
                $formattedDataFromCsv[$itemIdArray[($inputDataKey-1)]]['node_id'] = $itemIdArray[($inputDataKey-1)];
            }

            foreach($formattedDataFromCsv as $dataCreated){
                $dataToInput[]    =   array_map('trim', $dataCreated);
            }

            $customMetadataOfItemsFromCsv                       =   $this->getCustomMetadataDetailForDocumentAndItemFromCSV($dataToInput);

            //dd($arrayOfCreatedItemId);
            $prepend = array($documentIdentifier => $documentIdentifier);
            foreach($itemIdArray as $newItem){
                $newCreatedItemArray[$newItem]    =   $newItem;
            }
            
            $newCreatedItemArray = $prepend + $newCreatedItemArray;
        }
        
        if(sizeOf($customMetadataOfItemsFromCsv) > 0) {
            foreach($customMetadataOfItemsFromCsv as $itemIdFromDb => $customMetadataOfItem){
                foreach($customMetadataOfItem as $metadata){
                    $formattedMetadataFromDb[$itemIdFromDb][strtolower($metadata['name']).'('.strtolower($fieldTypeValues[$metadata['field_type']]).')'] = ['metadata_value' => $metadata['pivot']['metadata_value'], 'is_additional' => $metadata['pivot']['is_additional']]; 
                }
            }
        }

        
        $attributeIn            =   'title';
        $containedInValues      =   $nodeTypeArray; 
        $returnCollection       =   true;
        $fieldsToReturn         =   ['*'];
        $keyValuePairs          =   ['organization_id'=> $organizationIdentifier, 'is_deleted' => 0];
        $returnPaginatedData    =   false;
        $paginationFilters      =   ['limit' => 10, 'offset' => 0];
        $returnSortedData       =   false;
        $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator               =   'AND';
        $relations              =   ["metadata"];

        $nodeTypeSetOfAllItem    =   $this->nodeTypeRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator, 
            $relations
        )->toArray();

        foreach($nodeTypeSetOfAllItem as $itemNodeType) {
            $metadataListOfNodeTypeArray[$itemNodeType['title']] = [];
            if(sizeOf($itemNodeType['metadata']) > 0) {
                foreach($itemNodeType['metadata'] as $metadata) {
                    if($metadata['is_custom'] == 1 && $metadata['is_deleted'] == 0) {
                        $metadataNameAndType    =   strtolower($metadata['name']).'('.strtolower($fieldTypeValues[$metadata['field_type']]).')';
                        $metadataListOfNodeTypeArray[$itemNodeType['title']][] =   !empty($metadataNameAndType) ? $metadataNameAndType : '';
                    }
                }
            }
        }

        /**
         * Create new metadata when not added in csv but not in db
         */
        foreach($columnsFromHeaders as $csvHeaders) {
            $metadataPart       =   explode("(", $csvHeaders);

            $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;


            $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];
            $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();
            
            if(sizeOf($metadataDetail) == 0) {
                $metadataId =   $this->createUniversalUniqueIdentifier();
                $fieldType  =   $metadataType;
                $listValues =   "";
                
                $metadataInputData  = ['name' => $metadataPart[0],   'field_type' => $fieldType,    'organization_id' => $organizationIdentifier,    'field_possible_values'   => $listValues, 'last_field_possible_values'   => $listValues];

                $metadataInputData['metadata_id']   =   $metadataId;
                $metadataInputData['order']         =   ++$metadataListForATenant['metadata'][0]['order'];
                $metadataInputData['is_custom']     =   1;
                $newAdditionalMetadataCount++;
                $this->metadataRepository->saveData($metadataInputData);
            }else {
                if($metadataDetail[0]['is_deleted'] == 1){
                    $metadataId =   $this->createUniversalUniqueIdentifier();
                    $fieldType  =   $metadataType;
                    $listValues =   "";
                    
                    $metadataInputData  = ['name' => $metadataPart[0],   'field_type' => $fieldType,    'organization_id' => $organizationIdentifier,    'field_possible_values'   => $listValues, 'last_field_possible_values'   => $listValues];

                    $metadataInputData['metadata_id']   =   $metadataId;
                    $metadataInputData['order']         =   ++$metadataListForATenant['metadata'][0]['order'];
                    $metadataInputData['is_custom']     =   1;
                    $newAdditionalMetadataCount++;
                    $this->metadataRepository->saveData($metadataInputData);
                }
            }
        }
        $this->newAdditionalMetadataCount = $newAdditionalMetadataCount;

        $keyForMetadata = 0;
        foreach($metadataListForATenant['metadata'] as $metadata) {
            if($metadata['is_document'] != 1 && $metadata['is_custom']  ==  0){
                $metadataIdArray['metadata'][$keyForMetadata]  = ['id' => $metadata['metadata_id'], 'order'  =>  $metadata['order'], 'name' => $metadata['name'], 'is_mandatory' => $metadata['is_mandatory']];

                $keyForMetadata++;
            }
        }

        /**
         * Associate custom metadata to MetadataSet not existing in the tenant
         */
        foreach($nodeTypeIdArray as $nodeType) {
            $nodeTypeCreatedDetails =   $this->nodeTypeRepository->find($nodeType[array_keys($nodeType)[0]]);

            foreach($columnsFromHeaders as $headerIndex => $csvHeaders) {

                $metadataPart       =   explode("(", $csvHeaders);

                $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;

                $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];
                $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();

                $indexOfCustomMetadataForNodeType = array_search($metadataDetail[0]['metadata_id'], array_column($metadataIdArray['metadata'], 'id'));


                if($indexOfCustomMetadataForNodeType == ''){
                    array_push($metadataIdArray['metadata'],['id' => $metadataDetail[0]['metadata_id'], 'order'  =>  ++$headerIndex, 'name' => $metadataDetail[0]['name'], 'is_mandatory' => 0, 'is_custom' => 1]);
                }

                if(!in_array(strtolower($csvHeaders), $metadataListOfNodeTypeArray[array_keys($nodeType)[0]])){
                    array_push($metadataListOfNodeTypeArray[array_keys($nodeType)[0]], strtolower($csvHeaders) );
                    
                }
            }
            $this->nodeTypeRepository->saveNodeTypeMetadata($nodeTypeCreatedDetails->node_type_id, $metadataIdArray);
                    
        }

        // map item id to source item id start
        $getImportType = $this->getImportType();
        if($getImportType==4)
        {
            $documentId = $this->documentFromDB->document_id;
        }
        else
        {
            $documentId = $this->getDocumentIdentifier();
        }

            $getItemId = array();
            
            $itemIDMappedWithSourceItemID = DB::table('items')
            ->select('item_id','source_item_id')
            ->whereIN('source_item_id',$arrayOfItemIdFromDb)
            ->where('organization_id', $organizationIdentifier)
            ->where('document_id', $documentId)
            ->where('is_deleted' , 0)
            ->get()
            ->toArray();

            $getItemId[$sourceDocumentId] = $documentId;
            foreach($itemIDMappedWithSourceItemID as $itemIDMappedWithSourceItemIDK => $itemIDMappedWithSourceItemIDV)
            {
                $getItemId[$itemIDMappedWithSourceItemIDV->source_item_id] = $itemIDMappedWithSourceItemIDV->item_id;
            }

        // map item id to source item id start

        /**
         * Update and create the metadata for respective item in csv
         */
        $additionalMetadata = [];
        foreach($formattedDataFromCsv as $itemIdFromCsv => $dataFromCsv) {
            if(in_array((string)$itemIdFromCsv, $arrayOfItemIdFromDb) == true) {
                foreach($columnsFromHeaders as $headerIndex => $csvHeaders) {
                    if(isset($getItemId[$itemIdFromCsv]))
                    {
                        $itemIdInsertOrUpdate = $getItemId[$itemIdFromCsv];
                    
                    $metadataPart       =   explode("(", $csvHeaders);
                    $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
                    $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];
                    $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();
                    
                    if($metadataDetail[0]['field_type'] == 3){
                        $values = explode(",",$metadataDetail[0]['field_possible_values']);

                        if(!empty(trim($dataFromCsv[$csvHeaders])) && !in_array(trim($dataFromCsv[$csvHeaders]), $values)){
                            $updateListValues = ['field_possible_values' => $metadataDetail[0]['field_possible_values'].','.trim($dataFromCsv[$csvHeaders]), 'last_field_possible_values' => $metadataDetail[0]['field_possible_values'].','.trim($dataFromCsv[$csvHeaders])];

                            $this->metadataRepository->edit($metadataDetail[0]['metadata_id'], $updateListValues);
                        }
                    }

                    if($metadataType == 4){
                        if(!empty(trim($dataFromCsv[$csvHeaders])) && $this->validateDate(trim($dataFromCsv[$csvHeaders]))) {
                            $dataFromCsv[$csvHeaders] = date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders])));
                        } else {
                            $dataFromCsv[$csvHeaders] = '';
                            continue;
                        }
                        // $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders]))) : trim($dataFromCsv[$csvHeaders]);
                    }

                    if($metadataType == 6){
                        $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? number_format((float) $dataFromCsv[$csvHeaders],0,'','') : trim($dataFromCsv[$csvHeaders]);
                    }
                    
                    if(isset($metadataListOfNodeTypeArray[trim($dataFromCsv[$nodeTypeFromCsv])]) && in_array(strtolower($csvHeaders), $metadataListOfNodeTypeArray[trim($dataFromCsv[$nodeTypeFromCsv])])){

                        if(!empty(trim($dataFromCsv[$csvHeaders]))){
                            if(isset($formattedMetadataFromDb[$itemIdFromCsv]) && (isset($formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders])) && trim($dataFromCsv[$csvHeaders]) != $formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders]['metadata_value']) {
                                $updatedMetadataCount++;
                                if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document') {
                                    $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), '0', 'edit');
                                } else {
                                    $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'],trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), '0', 'edit');
                                }                                
                            } else {
                                if($dataFromCsv[$nodeTypeFromCsv] == 'Document') {
                                    $attributesForCustomAdditional = ['document_id' => $itemIdInsertOrUpdate, 'metadata_id' => $metadataDetail[0]['metadata_id']];
                                    $customAdditionalMetadataExists = DB::table('document_metadata')->where($attributesForCustomAdditional)->get();

                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), '0', 'edit');
                                            $updatedMetadataCount++;
                                        }
                                    } else {
                                        $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), '0', 'insert');
                                    }
                                } else {

                                    $attributesForCustomAdditional = ['item_id' => $itemIdInsertOrUpdate, 'metadata_id' => $metadataDetail[0]['metadata_id']];
                                    $customAdditionalMetadataExists = DB::table('item_metadata')->where($attributesForCustomAdditional)->get();

                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), '0', 'edit');
                                            $updatedMetadataCount++;
                                        }
                                    } else {
                                        $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), '0', 'insert');
                                    }
                                    
                                }
                            }
                        } else {
                            if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document') {
                                $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], '','', '0', 'edit');
                            } else {
                                $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], '','', '0', 'edit');
                            }
                            $blankMetadataCount++;
                        }
                    } else {
                        if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document') {
                            $attributesForCustomAdditional = ['document_id' => $itemIdInsertOrUpdate, 'metadata_id' => $metadataDetail[0]['metadata_id']];
                            $customAdditionalMetadataExists = DB::table('document_metadata')->where($attributesForCustomAdditional)->get();
                            
                            if(!isset($formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders])){
                                if(!empty(trim($dataFromCsv[$csvHeaders]))){
                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'edit');
                                        
                                            $updatedMetadataCount++;
                                        }
                                    } else {
                                        $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'insert');                                        
                                    }                                        
                                } else {
                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], '','', 1, 'edit');
                                        }
                                    } else {
                                        $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], '','', 1, 'insert');
                                    }
                                    
                                    $blankMetadataCount++;
                                }
                            } else {
                                if(!empty(trim($dataFromCsv[$csvHeaders]))){
                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'edit');
                                            $updatedMetadataCount++;
                                        }
                                    } else {
                                        $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'insert');
                                    }
                                } else {
                                    $this->documentRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], '','', 1, 'edit');
                                    $blankMetadataCount++;
                                }
                            }                               
                        } else {
                            
                            if(!isset($formattedMetadataFromDb[$itemIdFromCsv][$csvHeaders])){
                                $attributesForCustomAdditional = ['item_id' => $itemIdInsertOrUpdate, 'metadata_id' => $metadataDetail[0]['metadata_id']];
                                $customAdditionalMetadataExists = DB::table('item_metadata')->where($attributesForCustomAdditional)->get();
                                if(!empty(trim($dataFromCsv[$csvHeaders]))){
                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'edit');
                                
                                            $updatedMetadataCount++;
                                        }
                                    } else {
                                        $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'insert');
                                    }
                                } else {
                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]), trim($dataFromCsv[$csvHeaders]), 1, 'edit');
                                        }
                                    }else{
                                        $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]), trim($dataFromCsv[$csvHeaders]), 1, 'insert');
                                    }
                                    
                                    $blankMetadataCount++;
                                }
                            } else {
                                if(!empty(trim($dataFromCsv[$csvHeaders]))){
                                    if(!$customAdditionalMetadataExists->isEmpty()) {
                                        if($customAdditionalMetadataExists[0]->metadata_value != $dataFromCsv[$csvHeaders]){
                                            $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'edit');
                                            $updatedMetadataCount++;
                                        }
                                    } else {
                                        $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]),trim($dataFromCsv[$csvHeaders]), 1, 'edit');
                                    }
                                } else {
                                    $this->itemRepository->addOrUpdateCustomMetadataValue($itemIdInsertOrUpdate, $metadataDetail[0]['metadata_id'], '','', 1, 'edit');
                                    $blankMetadataCount++;
                                }
                            } 
                        }
                    }
                }
                }       
            } else {
                foreach($columnsFromHeaders as $csvHeaders){
                    
                    $metadataPart       =   explode("(", $csvHeaders);
                    $metadataType       =   isset($metadataPart[1]) ? (!empty(array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues))? array_search(str_replace(')','',strtoupper($metadataPart[1])), $fieldTypeValues) : 1 ) : 1;
                    
                    $attributesToSearch =   ['name' => $metadataPart[0], 'organization_id' => $organizationIdentifier, 'is_deleted' => '0', 'field_type' => $metadataType];

                    $metadataDetail     =   $this->metadataRepository->findByAttributes($attributesToSearch)->toArray();
                    
                    if($metadataDetail[0]['field_type'] == 3){
                        $values = explode(",",$metadataDetail[0]['field_possible_values']);

                        if(!empty(trim($dataFromCsv[$csvHeaders])) && !in_array(trim($dataFromCsv[$csvHeaders]), $values)){
                            $updateListValues = ['field_possible_values' => $metadataDetail[0]['field_possible_values'].','.trim($dataFromCsv[$csvHeaders]), 'last_field_possible_values' => $metadataDetail[0]['field_possible_values'].','.trim($dataFromCsv[$csvHeaders])];

                            $this->metadataRepository->edit($metadataDetail[0]['metadata_id'], $updateListValues);
                        }
                    }

                    if($metadataType == 4){
                        if(!empty(trim($dataFromCsv[$csvHeaders])) && $this->validateDate(trim($dataFromCsv[$csvHeaders]))) {
                            $dataFromCsv[$csvHeaders] = date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders])));
                        } else {
                            $dataFromCsv[$csvHeaders] = '';
                            continue;
                        }
                        // $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? date("Y-m-d", strtotime(trim($dataFromCsv[$csvHeaders]))) : trim($dataFromCsv[$csvHeaders]);
                    }

                    if($metadataType == 6){
                        $dataFromCsv[$csvHeaders] = !empty(trim($dataFromCsv[$csvHeaders])) ? number_format((float) $dataFromCsv[$csvHeaders],0,'','') : trim($dataFromCsv[$csvHeaders]);
                    }
                    
                    if(!empty($metadataDetail[0]['metadata_id'])) {
                        if(isset($metadataListOfNodeTypeArray[trim($dataFromCsv[$nodeTypeFromCsv])]) && in_array(strtolower($csvHeaders), $metadataListOfNodeTypeArray[trim($dataFromCsv[$nodeTypeFromCsv])])){

                            if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document'){
                                if(isset($newCreatedItemArray[$itemIdFromCsv])) {
                                    $this->documentRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]), trim($dataFromCsv[$csvHeaders]), 0, 'insert');
                                }
                                
                            } else {
                                $this->itemRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]), trim($dataFromCsv[$csvHeaders]), 0, 'insert');
                                
                            }
                        }else {
                            if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document'){
                                if(isset($newCreatedItemArray[$itemIdFromCsv])){
                                    $this->documentRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]), trim($dataFromCsv[$csvHeaders]), 1, 'insert');
                                }
                                
                            } else {
                                $this->itemRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], trim($dataFromCsv[$csvHeaders]), trim($dataFromCsv[$csvHeaders]), 1, 'insert');
                            }
                        }
                    } else {
                        if(isset($metadataListOfNodeTypeArray[trim($dataFromCsv[$nodeTypeFromCsv])]) && in_array(strtolower($csvHeaders), $metadataListOfNodeTypeArray[trim($dataFromCsv[$nodeTypeFromCsv])])){
                            if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document') {
                                if(isset($newCreatedItemArray[$itemIdFromCsv])) {
                                    $this->documentRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], '','', 0, 'insert');
                                }
                            } else {
                                $this->itemRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], '','', 0, 'insert');
                            }
                        } else {
                            if(trim($dataFromCsv[$nodeTypeFromCsv]) == 'Document') {
                                if(isset($newCreatedItemArray[$itemIdFromCsv])) {
                                    $this->documentRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], '','', 1, 'insert');
                                }
                                
                            } else {
                                //echo "polo 123 : ".$itemIdFromCsv;
                                $this->itemRepository->addOrUpdateCustomMetadataValue($newCreatedItemArray[$itemIdFromCsv], $metadataDetail[0]['metadata_id'], '','', 1, 'insert');
                            }
                        }
                    }
                }
            }
        }
        /* echo "New Metadata : ".$newAdditionalMetadataCount;
        echo "<pre>";
        echo "Update Metadata : ".$updatedMetadataCount;
        echo "<pre>";
        echo "blank Metadata : ".$blankMetadataCount;
        dd(); */
        $this->updateEmptyNode();
        $this->blankMetadataCount           = $blankMetadataCount;
        //$this->newAdditionalMetadataCount   = $newAdditionalMetadataCount;
        $this->updatedMetadataCount         = $updatedMetadataCount;
    }

  
    private function deleteItemNotInCsvFromCollection() {
        $itemArrayFromCsv           =   [];
        $itemArrayFromDb            =   [];
        $inputItemIds               =   [];
        $arrayOfProjectsAssociated  =   [];
        $createdItemIdArray         =   [];
        
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $itemCollectionArray    =   $this->getItemCollectionFromDb();
        $createdItemArray       =   $this->getCreatedItemArray();

        

        if($createdItemArray != null && count($createdItemArray) > 0) {
            foreach($createdItemArray as $createdItem) {
                $createdItemIdArray[] =   $createdItem['item_id'];
            }
        }

        if($itemCollectionArray != null && count($itemCollectionArray) > 0) {
            foreach($itemCollectionArray as $itemFromDb) {
                $itemArrayFromCsv[] =   trim($itemFromDb['node_id']);
            }
        }

        //dd($itemArrayFromCsv);
        /**
         * Fetch item detail from db for the item_id from csv file
         */
        $returnCollection   =   true;
        $fieldsToReturn     =   ['item_id'];
        $keyValuePairs      =   ['organization_id' => $organizationIdentifier, 'document_id'  =>  $documentIdentifier, 'is_deleted'   =>  '0' ];
        $relations          =   ['project'];
        
        $itemCollectionFromDb =   $this->itemRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        foreach($itemCollectionFromDb as $itemFromDb) {
            $itemArrayFromDb[]  =   $itemFromDb->item_id;           
        }

        $itemArrayFromDb    =   array_diff($itemArrayFromDb, $createdItemIdArray);

        $projectItemMapping = $this->projectRepository->getProjectItemMappingOfMultipleItems($itemArrayFromDb);

        foreach($projectItemMapping as $projects) {
            $arrayOfProjectsAssociated[]  = $projects->project_id;
        }

        $arrayOfProjectsAssociated  =   array_values(array_unique($arrayOfProjectsAssociated));
        
        /* foreach($arrayOfProjectsAssociated as $projectId) {
            foreach($itemArrayFromDb as $items) {
                $this->projectRepository->deleteProjectItem($items);
            }

            $projectItems   =   $this->projectRepository->getProjectItems($projectId);
            if(count($projectItems) == 0) {
                $columnValueFilterPairs = [ "project_id" => $projectId ];
                $dataToUpdate = [ "is_deleted" => '1' ];
                $this->projectRepository->editWithCustomizedFields($columnValueFilterPairs, $dataToUpdate);
            }
        } */

        $arrayOfItemsToBeDeletedFromDB  =   array_values(array_diff($itemArrayFromDb, $itemArrayFromCsv));
        
        foreach($arrayOfItemsToBeDeletedFromDB as $itemIdentifier) {
            
            $this->itemRepository->getAllChildItems($itemIdentifier, $inputItemIds);      
            array_push($inputItemIds, $itemIdentifier);
        }

        $this->setItemIdsDeleted($inputItemIds);

        Item::whereIn('item_id',$inputItemIds)->update(['is_deleted'=>1]);
        ItemAssociation::whereIn('source_item_id',$inputItemIds)->update(['is_deleted'=>1]);
        ItemAssociation::whereIn('target_item_id',$inputItemIds)->update(['is_deleted'=>1]);
    }

    private function parseResponse() {
        $updatedItemArray =   [];
        $createdItemArray =   [];
        $deleteNodeCount  = $this->deleteNodeCount;
        $updatedCount       =   $this->count;

        $blankUpdatedCount  =   $this->blankCount;  
        $newDocCount        =   $this->newDocCount; 
        $newNodeCount       =   $this->newNodeCount;
        $newAssociationsCount   =   $this->newAssociationsCount;

        $updatedMetadataCount       =   $this->updatedMetadataCount;
        $blankMetadataCount         =   $this->blankMetadataCount;
        $newAdditionalMetadataCount =   $this->newAdditionalMetadataCount;
        $updatedCount = 0;
        // commented for not considering removed node count while csv update
        // $itemIdDeleted      =   $this->getItemIdsDeleted();
        $itemIdDeleted = [];
        if(!empty($itemIdDeleted && $itemIdDeleted != null)){
            $deleteNodeCount    =   $deleteNodeCount + count($itemIdDeleted);
        }
        
        
        
        $createdItem    =   $this->getCreatedItemArray();
    
        if($createdItem != null && count($createdItem) > 0) {
            $arrayOfCreatedItem   =   array_values($createdItem);
            foreach($arrayOfCreatedItem as $itemCreated) {
                $createdItemArray[]   = $itemCreated['item_id'];
            }
            
            if($itemIdDeleted != null && count($itemIdDeleted) > 0) {
                $createdItemArray   =   array_diff($createdItemArray, $itemIdDeleted);
            }
        }

        $newNodeCount       =   $newDocCount + $newNodeCount;
        $updatedItemArray    =   $this->getUpdatedItemArray();
        
        if($itemIdDeleted != null && count($itemIdDeleted) > 0) {
            $updatedItemArray   =   array_diff($updatedItemArray, $itemIdDeleted);
        } 
        if($this->count>0)
            $updatedCount = 1;

        $updatedCount       =   $updatedCount + (($updatedItemArray !== null) ? count($updatedItemArray) : 0);
        
        if($newDocCount > 0) {
            $this->taxonomyStatus   =   'Taxonomy Created';
        } else {
            if($updatedCount    >   0 || $deleteNodeCount > 0 || $newNodeCount > 0 || $newAdditionalMetadataCount > 0) {
                $this->taxonomyStatus   =   'Taxonomy Updated';
            } else {
                $this->taxonomyStatus   =   'No change in taxonomy';
            }
        }
        
        return ["createdOrUpdated"  => $this->taxonomyStatus, "updated_node_count" => $updatedCount, "new_node_count" =>  $newNodeCount, 'delete_node_count' => $deleteNodeCount, 'new_associations_count' => $newAssociationsCount, 'new_metadata_count' => $newAdditionalMetadataCount];
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs() {
        // get Document id base on import type for 2 = used getCreatedDocumentIdentifier and for 4 = used getDocumentIdentifier function
        $importType =  $this->getImportType();
        $documentId = "";
        if($importType = 2)
        {
            $documentId = $this->getCreatedDocumentIdentifier();
        }
        else
        {
            $documentId = $this->getDocumentIdentifier();
        }
        if($documentId != "")
        {
            $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
            $eventType = $sqsUploadEventConfigurations["taxonomy_created"];
            $eventData = [
                "type_id" => $documentId,
                "event_type" => $eventType
            ];
            // event for upload data in aws cloud 
            event(new UploadSearchDataToSqsEvent($eventData));
        }  
    }

    /**
     * function to check parent node id / destination node id exist in database
     */
    public function checkParentNodeIdExist($parent_node_id)
    {
        $organizationId = $this->getOrganizationIdentifier();

        $count = DB::table('item_associations')
                    ->where('source_item_id',$parent_node_id)
                    ->where('organization_id',$organizationId)
                    ->count();

        if($count>0)
        {
            return 1;
        }           
        else
        {
            return 2;
        } 
    }

    public function keychange($arr, $oldkey, $newkey) {
        $json = str_replace('"'.$oldkey.'":', '"'.$newkey.'":', json_encode($arr));
        return json_decode($json); 
       }

    public function checkAssociationExist($associationData)
    {
        $organizationId = $this->getOrganizationIdentifier();
        $dataReturned = $this->getItemIdAndSourceIdMappingArrayUpdate($associationData['target_item_id']);
        if(!empty($dataReturned))
        {
            $count = DB::table('item_associations')
                        ->where('source_item_id',$associationData['source_item_id'])
                        ->where('target_item_id',$dataReturned[0]->item_id)
                        ->where('association_type',$associationData['association_type'])
                        ->where('organization_id',$organizationId)
                        ->where('is_deleted',0)
                        ->count();

            if($count>0)
            {
                return 1;
            }           
            else
            {
                return 2;
            }
        }
        else
        {
            return 1;
        } 

    }
    
    public function updateEmptyNode()
    {
        $organizationId = $this->getOrganizationIdentifier();

        DB::table('node_types')
                    ->where('title', "")
                    ->where('organization_id', $organizationId)
                    ->where('used_for',0)
                    ->update(['is_deleted' => 1]);

    }

    public function filterInputData($inputData)
    {
        $filterInputData = array();
        foreach($inputData as $keyData => $keyValue)
        {
            if( !empty(trim($keyValue['node_id'])) )
            {
                $filterInputData[] = $keyValue;
            }
        }

        return $filterInputData;
        
    }

    public function getItemIdAndSourceIdMappingArray($sourceId)
    {
        $organizationId = $this->getOrganizationIdentifier();
        
        $itemIdMappedWithSourceId = DB::table('items')
        ->select('item_id','source_item_id')
        ->whereIn('source_item_id',$sourceId)
        ->where('organization_id', $organizationId)
        ->where('is_deleted' , 0)
        ->get()
        ->toArray();

        return $itemIdMappedWithSourceId;
    }

    public function getItemIdAndSourceIdMappingArrayUpdate($sourceId)
    {

        $organizationId = $this->getOrganizationIdentifier();
        $itemIdMappedWithSourceId = DB::table('items')
        ->select('item_id','source_item_id')
        ->where('source_item_id',$sourceId)
        ->where('organization_id', $organizationId)
        ->where('is_deleted' , 0)
        ->get()
        ->toArray();

        return $itemIdMappedWithSourceId;

    }

    public function deleteExistingNodeData()
    {   
        $organizationIdentifier = $this->getOrganizationIdentifier();
        $arrayItemIdToDeleteNode = $this->getExistingDataToBeDeleted();

        if(!empty($arrayItemIdToDeleteNode))
        {
        $update = ['is_deleted' => '1'];
        
            foreach($arrayItemIdToDeleteNode as $arrayItemIdToDeleteNodeK=>$arrayItemIdToDeleteNodeV)
            {
                
                $itemIdMappedWithSourceId = $this->getItemIdAndSourceIdMappingArrayUpdate($arrayItemIdToDeleteNodeV);
                
                if(!empty($itemIdMappedWithSourceId))
                {
                    $input_item_ids=array();
                    $this->itemRepository->getAllChildItemsV2($itemIdMappedWithSourceId[0]->item_id, $input_item_ids);        
                    array_push($input_item_ids,$itemIdMappedWithSourceId[0]->item_id);

                    foreach($input_item_ids as $input_item_idsK=>$input_item_idsV)
                    {

                        DB::table('items')->where('item_id', $input_item_idsV)
                        ->where('organization_id',$organizationIdentifier)
                        ->update($update);

                        DB::table('items')->where('parent_id', $input_item_idsV)
                                    ->where('organization_id',$organizationIdentifier)
                                    ->update($update);

                        DB::table('item_associations')->where('source_item_id', $input_item_idsV)
                                                ->where('organization_id',$organizationIdentifier)
                                                ->update($update);

                        DB::table('item_associations')->where('target_item_id', $input_item_idsV)
                                                ->where('organization_id',$organizationIdentifier)
                                                ->update($update);


                    }
                }
        
          
            }

        }

    }

    function validateDate($date){
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date) || preg_match("/^[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public function checkAssociationExistInItemAssociationTable($onlyJson1and3TypeAssociationToImportV)
    {
        $organizationId = $this->getOrganizationIdentifier();
        
        
            $count = DB::table('item_associations')
                        ->where('source_item_id',$onlyJson1and3TypeAssociationToImportV['source_item_id'])
                        ->where('target_item_id',$onlyJson1and3TypeAssociationToImportV['target_item_id'])
                        ->where('association_type',$onlyJson1and3TypeAssociationToImportV['association_type'])
                        ->where('organization_id',$organizationId)
                        ->where('is_deleted',0)
                        ->count();

            if($count>0)
            {
                return 1;
            }           
            else
            {
                return 2;
            }
        

    }

    public function getItemIdAndSourceIdMappingArrayUpdate2($sourceId)
    {

        $organizationId = $this->getOrganizationIdentifier();
        $getImportType = $this->getImportType();

        if($getImportType==4)
        {
            $documentId = $this->documentFromDB->document_id;
            $itemIdMappedWithSourceId = DB::table('items')
            ->select('item_id','source_item_id')
            ->where('source_item_id',$sourceId)
            // ->where('document_id',$documentId)
            ->where('organization_id', $organizationId)
            ->where('is_deleted' , 0)
            ->get()
            ->toArray();


            return $itemIdMappedWithSourceId;
        }
        else
        {
            $documentId = $this->getDocumentIdentifier();
            $itemIdMappedWithSourceId = DB::table('items')
            ->select('item_id','source_item_id')
            ->where('source_item_id',$sourceId)
            ->where('document_id',$documentId)
            ->where('organization_id', $organizationId)
            ->where('is_deleted' , 0)
            ->get()
            ->toArray();

            return $itemIdMappedWithSourceId;
        }

    }

    public function checkAssociationExist2($associationData)
    {
        $organizationId = $this->getOrganizationIdentifier();
        $dataReturnedOriginId = $this->getItemIdAndSourceIdMappingArrayUpdate2($associationData['source_item_id']);
        $dataReturned = $this->getItemIdAndSourceIdMappingArrayUpdate2($associationData['target_item_id']);
        if(!empty($dataReturned) && !empty($dataReturnedOriginId))
        {
            $count = DB::table('item_associations')
                        ->where('source_item_id',$dataReturnedOriginId[0]->item_id)
                        ->where('target_item_id',$dataReturned[0]->item_id)
                        ->where('association_type',$associationData['association_type'])
                        ->where('organization_id',$organizationId)
                        ->where('is_deleted',0)
                        ->count();

            if($count>0)
            {
                return 1;
            }           
            else
            {
                return 2;
            }
        }
        else
        {
            return 1;
        } 

    }
    public function checkAssociationExistAndReturnSeqNo($associationData)
    {
        $organizationId = $this->getOrganizationIdentifier();
        $dataReturnedOriginId = $this->getItemIdAndSourceIdMappingArrayUpdate2($associationData['source_item_id']);
        $dataReturned = $this->getItemIdAndSourceIdMappingArrayUpdate2($associationData['target_item_id']);
        if(!empty($dataReturned) && !empty($dataReturnedOriginId))
        {
            $itemAssoDetail = DB::table('item_associations')
                        ->where('source_item_id',$dataReturnedOriginId[0]->item_id)
                        ->where('target_item_id',$dataReturned[0]->item_id)
                        ->where('association_type',$associationData['association_type'])
                        ->where('organization_id',$organizationId)
                        ->where('is_deleted',0)
                        ->get();
            if($itemAssoDetail->count())
            {
                return $itemAssoDetail->first()->sequence_number;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        } 

    }

    public function checkAssociationExistAndReturnSeqNo3($associationData)
    {
        $organizationId = $this->getOrganizationIdentifier();
        // $dataReturnedOriginId = $this->getItemIdAndSourceIdMappingArrayUpdate2($associationData['source_item_id']);
        // $dataReturned = $this->getItemIdAndSourceIdMappingArrayUpdate2($associationData['target_item_id']);
        // if(!empty($dataReturned) && !empty($dataReturnedOriginId))
        // {
            $itemAssoDetail = DB::table('item_associations')
                        ->where('source_item_id',$associationData['source_item_id'])
                        ->where('target_item_id',$associationData['target_item_id'])
                        ->where('association_type',$associationData['association_type'])
                        ->where('organization_id',$organizationId)
                        ->where('is_deleted',0)
                        ->get();
            if($itemAssoDetail->count())
            {
                return $itemAssoDetail->first()->sequence_number;
            }
            else
            {
                return 0;
            }
        // }
        // else
        // {
        //     return 0;
        // } 

    }

    public function checkAssociationExistAndReturnSeqNo2($associationData)
    {
        $organizationId = $this->getOrganizationIdentifier();
        $dataReturned = $this->getItemIdAndSourceIdMappingArrayUpdate($associationData['target_item_id']);
        if(!empty($dataReturned))
        {
            $itemAssoDetail = DB::table('item_associations')
                        ->where('source_item_id',$associationData['source_item_id'])
                        ->where('target_item_id',$dataReturned[0]->item_id)
                        ->where('association_type',$associationData['association_type'])
                        ->where('organization_id',$organizationId)
                        ->where('is_deleted',0)
                        ->get();

            if($itemAssoDetail->count())
            {
                return $itemAssoDetail->first()->sequence_number;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }

    }

    #assign sequence no to child associations
    public function itemSequenceNo($itemArr, $caseAssoArr, $parentMaxSeqNo = [])
    {
        $parentChildArr = [];
        foreach($caseAssoArr as $asso) {
            $parentChildArr[$asso['target_item_id']][] = $asso['source_item_id'];
        }

        $allItemSeqArr = [];
        $counter = 1;
        # get sequence in which items will be inserted
        foreach($itemArr as $item) {
            $allItemSeqArr[$item['item_id']] = $counter;
            $counter++;
        }

        $newParentChildArr = [];
        # assign sequence according to order of insert in db
        foreach($parentChildArr as $k1 => $children) {
            foreach($children as $child) {
                $newParentChildArr[$k1][$child] = $allItemSeqArr[$child];
            }
        }

        # sort children according to seq
        foreach($newParentChildArr as $key => $children) {
            $temp = $children;
            asort($temp);
            $newParentChildArr[$key] = $temp;
        }
        $seqArr = [];
        # assign new seq no
        foreach($newParentChildArr as $key1 => $children) {
            if(!empty($parentMaxSeqNo)) {                                 #incase of import type 4
                $counter = isset($parentMaxSeqNo[$key1]) ? $parentMaxSeqNo[$key1] + 1 : 1;
            } else {                                                      #incase of import type 2
                $counter = 1;
            }
            foreach($children as $key2 => $value) {
                $seqArr[$key2] = $counter;
                $counter++;
            }
        }
        # assign seq number to child associations
        foreach($caseAssoArr as $key => $value) {
            $caseAssoArr[$key]['sequence_number'] = isset($seqArr[$value['source_item_id']]) ? $seqArr[$value['source_item_id']] : 0;
        }
        $this->allChildSeqArr = $seqArr;
        return $caseAssoArr;
    }

    public function getMaxSeqNoChild()
    {
        $documentId = $this->getDocumentIdentifier();
        $item_child_asso = DB::table('item_associations')
            ->select(DB::raw('max(sequence_number) as max_sequence_no' ), 'item_associations.target_item_id')
            ->where('association_type', 1)
            ->where('source_document_id', $documentId)
            ->where('item_associations.is_deleted', 0)
            ->groupBy('item_associations.target_item_id')
            ->get();
        $parentMaxSeqNo = [];
        //max sequence no of each parent
        foreach($item_child_asso as $asso) {
            $parentMaxSeqNo[$asso->target_item_id] = $asso->max_sequence_no;
        }

        return $parentMaxSeqNo;
    }

    public function getAllMaxSeqNo()
    {
        $documentId = $this->getDocumentIdentifier();
        $item_asso = DB::table('item_associations')
                        ->select(DB::raw('max(sequence_number) as max_sequence_no' ), 'item_associations.source_item_id')
                        ->where('source_document_id', $documentId)
                        ->where('item_associations.is_deleted', 0)
                        ->groupBy('item_associations.source_item_id')
                        ->get();
        $maxSeqNo = [];
        //max sequence no of each parent
        foreach($item_asso as $asso) {
            $maxSeqNo[$asso->source_item_id] = $asso->max_sequence_no;
        }

        return $maxSeqNo;
    }

    public function assignSeqNumber($item_asso, $item_max_seq_no) {
        foreach($item_asso as $key => $asso) {
            if($asso['association_type'] == 1 && $asso['source_document_id'] == $asso['target_document_id']) {
                $result = $this->checkAssociationExistAndReturnSeqNo3($asso);
                if($result) {
                    $item_asso[$key]['sequence_number'] = $result;
                    $item_max_seq_no[$asso['source_item_id']] = $result;
                } else {
                    $seq_no = isset($item_max_seq_no[$asso['source_item_id']]) ? $item_max_seq_no[$asso['source_item_id']] + 1 : 1;
                    $item_asso[$key]['sequence_number'] = $seq_no;
                    $item_max_seq_no[$asso['source_item_id']] = $seq_no;
                }
            } else {
                $seq_no = isset($item_max_seq_no[$asso['source_item_id']]) ? $item_max_seq_no[$asso['source_item_id']] + 1 : 1;
                $item_asso[$key]['sequence_number'] = $seq_no;
                $item_max_seq_no[$asso['source_item_id']] = $seq_no;
            }
        }

        return $item_asso;
    }
    public function assignSeqNumber2($item_asso, $item_max_seq_no) {
        foreach($item_asso as $key => $asso) {
            $result = $this->checkAssociationExistAndReturnSeqNo2($asso);
            if($result) {
                $asso[$key]['sequence_number'] = $result;
            } else {
                $seq_no = isset($item_max_seq_no[$asso['source_item_id']]) ? $item_max_seq_no[$asso['source_item_id']] + 1 : 1;
                $item_asso[$key]['sequence_number'] = $seq_no;
                $item_max_seq_no[$asso['source_item_id']] = $seq_no;
            }
        }

        return $item_asso;
    }
}
