<?php
namespace App\Domains\Csv\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ImportPass2CsvMultipleValidator extends BaseValidator {

    protected $rules = [
        // 'import_type' => 'required',
        'import_pass' => 'required',
        // 'source_identifier' => 'required',
        // 'batch_id' => 'required',
        // 'package_exists' => 'required',
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];

}