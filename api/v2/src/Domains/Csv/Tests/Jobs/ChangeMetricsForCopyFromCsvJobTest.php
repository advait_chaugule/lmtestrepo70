<?php
namespace App\Domains\Csv\Tests\Jobs;

use App\Domains\Csv\Jobs\ChangeMetricsForCopyFromCsvJob;
use Tests\TestCase;

class ChangeMetricsForCopyFromCsvJobTest extends TestCase
{
    public function test_ImportType()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setImportType($value);
        $this->assertEquals($value,$assets->getImportType());
    }

    public function test_OrganizationIdentifier()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_DocumentIdentifier()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_ColumnsFromHeader()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setColumnsFromHeader($value);
        $this->assertEquals($value,$assets->getColumnsFromHeader());
    }

    public function test_Metadata()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadata($value);
        $this->assertEquals($value,$assets->getMetadata());
    }

    public function test_ParsedItemsFromCsv()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setParsedItemsFromCsv($value);
        $this->assertEquals($value,$assets->getParsedItemsFromCsv());
    }

    public function test_ParsedItemAssociationFromCsv()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setParsedItemAssociationFromCsv($value);
        $this->assertEquals($value,$assets->getParsedItemAssociationFromCsv());
    }

    public function test_CreatedDocumentIdentifier()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCreatedDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getCreatedDocumentIdentifier());
    }

    public function test_ItemCollectionFromDb()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemCollectionFromDb($value);
        $this->assertEquals($value,$assets->getItemCollectionFromDb());
    }

    public function test_NewItemArray()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNewItemArray($value);
        $this->assertEquals($value,$assets->getNewItemArray());
    } 
    
    public function test_ArrayOfCreatedItemId()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setArrayOfCreatedItemId($value);
        $this->assertEquals($value,$assets->getArrayOfCreatedItemId());
    } 
    
    public function test_CreatedItemArray()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCreatedItemArray($value);
        $this->assertEquals($value,$assets->getCreatedItemArray());
    } 

    public function test_UpdatedItemArray()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setUpdatedItemArray($value);
        $this->assertEquals($value,$assets->getUpdatedItemArray());
    } 

    public function test_ItemIdsDeleted()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemIdsDeleted($value);
        $this->assertEquals($value,$assets->getItemIdsDeleted());
    } 

    public function test_CaseMetadataSet()
    {
        $inputData[][] = 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCaseMetadataSet($value);
        $this->assertEquals($value,$assets->getCaseMetadataSet());
    } 

    public function test_CustomMetadataSet()
    {
        $inputData[][]= 'bd2e5a34-8e0c-4af0-bce3-7e3f307f10f2';
        $requestingUserDetail['user_id'] = '0c243411-7be8-48a7-80f8-8057ab011a87';
        $csvColumn[] = 'bbfeb268-7b1c-4832-8d56-70d70b38c907';
        $metadataListForATenant[] = '8bc041fe-d5ab-4860-8c17-f7deb805d3d4';

        $assets = new ChangeMetricsForCopyFromCsvJob($inputData,'c4424f55-8900-446c-9c41-2f2c25d58c08',$requestingUserDetail,'d199f273-204e-4e8f-a6ab-c473e76f1b84',$csvColumn,$metadataListForATenant,'39c0191e-d5ae-4274-a1a1-2005fc52a3e3');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCustomMetadataSet($value);
        $this->assertEquals($value,$assets->getCustomMetadataSet());
    } 
}
