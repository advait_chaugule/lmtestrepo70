<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\SetOrderedTaxonomyTreeJob;
use Tests\TestCase;

class SetOrderedTaxonomyTreeJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new SetOrderedTaxonomyTreeJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_UserOrganizationId()
    {
        $assets = new SetOrderedTaxonomyTreeJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserOrganizationId($value);
        $this->assertEquals($value,$assets->getUserOrganizationId());
    }

    public function test_UserId()
    {
        $assets = new SetOrderedTaxonomyTreeJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserId($value);
        $this->assertEquals($value,$assets->getUserId());
    }
}
