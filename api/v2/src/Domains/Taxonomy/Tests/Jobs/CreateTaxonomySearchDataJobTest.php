<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\CreateTaxonomySearchDataJob;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Collection;

class CreateTaxonomySearchDataJobTest extends TestCase
{
    use RefreshDatabase;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $projectRepository;
    private $itemAssociationRepository;
    private $metadataRepository;
    private $languageRepository;
    private $licenseRepository;
    private $conceptRepository;

    private $testDocument;
    private $testItems;
    private $testNodeTypes;
    private $testProject;

    private $searchFields;

    public function setUp() {
        parent::setUp();

        // run seeder if required after environment has been setup
        Artisan::call('db:seed');

        // set database repositories
        $this->documentRepository = resolve('App\Data\Repositories\Contracts\DocumentRepositoryInterface');
        $this->itemRepository = resolve('App\Data\Repositories\Contracts\ItemRepositoryInterface');
        $this->nodeTypeRepository = resolve('App\Data\Repositories\Contracts\NodeTypeRepositoryInterface');
        $this->projectRepository = resolve('App\Data\Repositories\Contracts\ProjectRepositoryInterface');
        $this->itemAssociationRepository = resolve('App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface');
        $this->metadataRepository = resolve('App\Data\Repositories\Contracts\MetadataRepositoryInterface');
        $this->languageRepository = resolve('App\Data\Repositories\Contracts\LanguageRepositoryInterface');
        $this->licenseRepository = resolve('App\Data\Repositories\Contracts\LicenseRepositoryInterface');
        $this->conceptRepository = resolve('App\Data\Repositories\Contracts\ConceptRepositoryInterface');

        // execute defaults for this test class
        $this->prepareRelevantTestData();
        $this->saveRelevantTestData();
    }

    private function prepareRelevantTestData() {
        $taxonomyTestData = __('test_samples/test_taxonomy_package');
        $this->setTestDocument($taxonomyTestData['document']);
        $this->setTestItems($taxonomyTestData['items']);
        $this->setTestNodeTypes($taxonomyTestData['node_types']);

        $configuredSearchableFields = config('_search.taxonomy.search_fields');
        $extraMissingSearchFields = [
            'identifier', 'parent_identifier', 'organization_identifier', 'document_id', 'type'
        ];
        $mergedSearchFields = array_merge($configuredSearchableFields, $extraMissingSearchFields);
        $this->setSearchFields($mergedSearchFields);
    }

    private function saveRelevantTestData() {
        $testDocument = $this->getTestDocument();
        $testItems = $this->getTestItems();
        $testNodeTypes = $this->getTestNodeTypes();
        
        $this->documentRepository->saveData($testDocument);
        $this->itemRepository->saveMultiple($testItems);
        $this->nodeTypeRepository->saveMultiple($testNodeTypes);
    }

    public function setTestDocument(array $data) {
        $this->testDocument = $data;
    }

    public function getTestDocument(): array {
        return $this->testDocument;
    }

    public function setTestItems(array $data) {
        $this->testItems = $data;
    }

    public function getTestItems(): array {
        return $this->testItems;
    }

    public function setTestNodeTypes(array $data) {
        $this->testNodeTypes = $data;
    }

    public function getTestNodeTypes(): array {
        return $this->testNodeTypes;
    }

    public function setSearchFields(array $data) {
        $this->searchFields = $data;
    }

    public function getSearchFields(): array {
        return $this->searchFields;
    }
    
    public function test_taxonomy_data_to_test_is_saved()
    {
        $documentBeforeSave = $this->getTestDocument();
        $itemsBeforeSave = collect($this->getTestItems());
        $nodeTypesBeforeSave = collect($this->getTestNodeTypes());
        $beforSaveDocumentId = $documentBeforeSave['document_id'];
        $beforeSaveItemIds = $itemsBeforeSave->pluck('item_id')->toArray();
        $beforeSaveNodeTypeIds = $nodeTypesBeforeSave->pluck('node_type_id')->toArray();

        $afterSaveDocument = $this->documentRepository->find($beforSaveDocumentId);
        $afterSaveItems = $this->itemRepository->findByAttributeContainedIn('item_id', $beforeSaveItemIds, []);
        $afterSaveNodeTypes = $this->nodeTypeRepository->findByAttributeContainedIn('node_type_id', $beforeSaveNodeTypeIds, []);
        $afterSaveDocumentId = $afterSaveDocument->document_id;
        $afterSaveItemIds = $afterSaveItems->pluck('item_id')->toArray();
        $afterSaveNodeTypeIds = $afterSaveNodeTypes->pluck('node_type_id')->toArray();

        $this->assertEquals($beforSaveDocumentId, $afterSaveDocumentId);

        foreach($beforeSaveItemIds as $beforeSaveItemId) {
            $this->assertContains($beforeSaveItemId, $afterSaveItemIds);
        }

        foreach($beforeSaveNodeTypeIds as $beforeSaveNodeTypeId) {
            $this->assertContains($beforeSaveNodeTypeId, $afterSaveNodeTypeIds);
        }
        
    }

    public function test_create_taxonomy_search_data_job()
    {
        $document = $this->getTestDocument();
        $documentId = $document['document_id'];
        $nodeIds = collect($this->getTestItems())->pluck('item_id')->toArray();
        $job = $this->app->make('App\Domains\Taxonomy\Jobs\CreateTaxonomySearchDataJob', ['documentId' => $documentId]);
        $jobResponse = $job->handle(
                            $this->documentRepository, 
                            $this->itemRepository, 
                            $this->nodeTypeRepository, 
                            $this->projectRepository,
                            $this->itemAssociationRepository,
                            $this->metadataRepository,
                            $this->languageRepository,
                            $this->licenseRepository,
                            $this->conceptRepository
                        );

        $this->assertInstanceOf(Collection::class, $jobResponse);
        $this->assertTrue($jobResponse->isNotEmpty());
        $this->assertTrue($jobResponse->count()===3);

        $searchFields = $this->getSearchFields();
        array_push($nodeIds, $documentId);
        $type = ['add', 'delete'];
        foreach($jobResponse as $nodeDetail) {
            $this->assertArrayHasKey('id', $nodeDetail);
            $this->assertContains($nodeDetail['id'], $nodeIds);
            
            $this->assertArrayHasKey('type', $nodeDetail);
            $this->assertContains($nodeDetail['type'], $type);

            $this->assertArrayHasKey('fields', $nodeDetail); 
            $this->assertNotEmpty($nodeDetail['fields']);
            // foreach($nodeDetail['fields'] as $key=>$value){
            //     $this->assertContains($key, $searchFields);
            // }
        }
    }
}
