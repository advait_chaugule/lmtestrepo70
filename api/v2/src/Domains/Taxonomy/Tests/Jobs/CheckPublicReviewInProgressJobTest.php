<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\CheckPublicReviewInProgressJob;
use Tests\TestCase;

class CheckPublicReviewInProgressJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new CheckPublicReviewInProgressJob('f9153483-61b2-4cb1-9617-45fafd36d5aa','480f3836-77b7-4cd3-9a29-83f141c2238d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new CheckPublicReviewInProgressJob('f9153483-61b2-4cb1-9617-45fafd36d5aa','480f3836-77b7-4cd3-9a29-83f141c2238d');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
