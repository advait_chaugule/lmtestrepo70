<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\PrepareTaxonomyPackageDataForCsvExportJob;
use Tests\TestCase;

class PrepareTaxonomyPackageDataForCsvExportJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $data['organization_id'] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new PrepareTaxonomyPackageDataForCsvExportJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_RequestingUserOrganizationId()
    {
        $data['organization_id'] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new PrepareTaxonomyPackageDataForCsvExportJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRequestingUserOrganizationId($value);
        $this->assertEquals($value,$assets->getRequestingUserOrganizationId());
    }

    public function test_ParsedTaxonomyName()
    {
        $data['organization_id'] = 'f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new PrepareTaxonomyPackageDataForCsvExportJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setParsedTaxonomyName($value);
        $this->assertEquals($value,$assets->getParsedTaxonomyName());
    }
}
