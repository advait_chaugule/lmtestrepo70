<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\ChangeOfStatusForOldTaxonomySentToPRJob;
use Tests\TestCase;

class ChangeOfStatusForOldTaxonomySentToPRJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new ChangeOfStatusForOldTaxonomySentToPRJob('f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }
}
