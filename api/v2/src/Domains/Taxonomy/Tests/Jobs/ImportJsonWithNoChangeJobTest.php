<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\ImportJsonWithNoChangeJob;
use Tests\TestCase;

class ImportJsonWithNoChangeJobTest extends TestCase
{
    public function test_import_json_with_no_change_job()
    {
        $this->markTestIncomplete();
    }
}
