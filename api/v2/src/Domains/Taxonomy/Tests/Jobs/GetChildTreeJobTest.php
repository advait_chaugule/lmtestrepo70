<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\GetChildTreeJob;
use Tests\TestCase;

class GetChildTreeJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $data[] ='f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new GetChildTreeJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_ParentAndDocumentId()
    {
        $data[] ='f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new GetChildTreeJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a',$data);
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setParentAndDocumentId($value);
        $this->assertEquals($value,$assets->getParentAndDocumentId());
    }
}
