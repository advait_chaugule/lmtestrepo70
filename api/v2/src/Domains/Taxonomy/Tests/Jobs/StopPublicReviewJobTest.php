<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\StopPublicReviewJob;
use Tests\TestCase;

class StopPublicReviewJobTest extends TestCase
{
    public function test_stop_public_review_job()
    {
        $this->markTestIncomplete();
    }
}
