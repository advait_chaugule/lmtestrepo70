<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\SearchTaxonomyJob;
use Tests\TestCase;

class SearchTaxonomyJobTest extends TestCase
{
    public function test_SearchQuery()
    {
        $assets = new SearchTaxonomyJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setSearchQuery($value);
        $this->assertEquals($value,$assets->getSearchQuery());
    }

    public function test_RequestUserOrganizationId()
    {
        $assets = new SearchTaxonomyJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRequestUserOrganizationId($value);
        $this->assertEquals($value,$assets->getRequestUserOrganizationId());
    }
}
