<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\CreateTaxonomyJob;
use Tests\TestCase;

class CreateTaxonomyJobTest extends TestCase
{
    public function test_UserOrganizationId()
    {
        $data[] ='f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateTaxonomyJob($data,'https://api.acmt-dev.learningmate.com/server');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserOrganizationId($value);
        $this->assertEquals($value,$assets->getUserOrganizationId());
    }

    public function test_UserId()
    {
        $data[] ='f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateTaxonomyJob($data,'https://api.acmt-dev.learningmate.com/server');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserId($value);
        $this->assertEquals($value,$assets->getUserId());
    }
    
    public function test_SavedLanguages()
    {
        $data[] ='f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateTaxonomyJob($data,'https://api.acmt-dev.learningmate.com/server');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setSavedLanguages($value);
        $this->assertEquals($value,$assets->getSavedLanguages());
    }

    public function test_Document()
    {
        $data[] ='f9153483-61b2-4cb1-9617-45fafd36d5aa';
        $assets = new CreateTaxonomyJob($data,'https://api.acmt-dev.learningmate.com/server');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocument($value);
        $this->assertEquals($value,$assets->getDocument());
    }
}
