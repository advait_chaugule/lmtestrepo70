<?php
namespace App\Domains\Taxonomy\Tests\Jobs;

use App\Domains\Taxonomy\Jobs\GetProjectListAssociatedWithTaxonomyJob;
use Tests\TestCase;

class GetProjectListAssociatedWithTaxonomyJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new GetProjectListAssociatedWithTaxonomyJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new GetProjectListAssociatedWithTaxonomyJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ProjectsAssociatedWithDocument()
    {
        $assets = new GetProjectListAssociatedWithTaxonomyJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectsAssociatedWithDocument($value);
        $this->assertEquals($value,$assets->getProjectsAssociatedWithDocument());
    }
    
    public function test_OpenCommentCountPerProject()
    {
        $assets = new GetProjectListAssociatedWithTaxonomyJob('cb2c4be5-bb2f-4927-bd7a-8b9a0e163c1a','f9153483-61b2-4cb1-9617-45fafd36d5aa');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOpenCommentCountPerProject($value);
        $this->assertEquals($value,$assets->getOpenCommentCountPerProject());
    }
}
