<?php
namespace App\Domains\Taxonomy\Listeners;

use DB;
use Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Taxonomy\Events\ExportCSVBackgroundEvent as Event;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\GetEmailByTokenIdJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageWithActiveMetadataJob;
use App\Data\Repositories\Contracts\NotificationInterface;

class ExportCSVBackgroundEventListeners implements ShouldQueue
{
    use DispatchesJobs;

    public $queue ;
    public $connection = 'export_csv_sqs';

    public function __construct(NotificationInterface $notificationActivity)
    {
       $this->queue=config('queue.connections.export_csv_sqs.queue');
       $this->notificationRepository = $notificationActivity;
    }
    
    public function handle(Event $event)
    {
        ini_set('max_execution_time', 0);
        try
        {
            $data = $event->exportData;
            
            $user_email = $this->dispatch(new GetEmailByTokenIdJob($data['headers']));

            $packageData = $this->dispatch(new GetCasePackageWithActiveMetadataJob($data['documentIdentifier'], $data['requestingUserDetails'],$data['orgCode'],$data['is_html'], $data['exportKey'],$data['request_url']));
            $start_time1 = microtime(true);
            $result = $this->callExportApi($packageData, $user_email, $data['organization_id'], $data['trans_id']); // Transaction id added to identify different export request
            
            if($result['status'] == 1)
            {
                $successType = 'found';
                $message = 'Data found.';
                $_status = 'custom_status_here';
            }
            else{

                $successType = 'custom_not_found';
                $message = 'Data not found.';
                $_status = 'custom_status_here';

            }
            return $this->dispatch(new RespondWithJsonJob($successType, $result, $message, $_status));            
        }
        catch(\Exception $exception) {
            Log::error($exception);
        }
    }

    public function callExportApi($packageData, $email, $org_id, $trans_id)
    {
        $ch = curl_init();
        $final_array = array('email' => $email, 'data' => $packageData, 'orgid' => $org_id, 'trans_id' => $trans_id);  // Transaction id added to identify different export request
        $postData = array('trans_id' => $trans_id);  // Transaction id added to identify different export request
        

        $baseFolderPathForStorage = storage_path('app').'/downloaded_taxonomy/';
        
        if (!file_exists($baseFolderPathForStorage)) {
               
            mkdir($baseFolderPathForStorage, 0777);
        }

       
        $file = $baseFolderPathForStorage.$trans_id.".txt";
        $postData = array('trans_id' => $trans_id, 'file_loc' => $file);
        file_put_contents($file, json_encode($final_array)); 

        $url = "localhost:3000/exportcsv";
        curl_setopt($ch,CURLOPT_URL, $url);
        //curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($postData));

        //execute post
        $result = curl_exec($ch);
       // $res = curl_getinfo($ch); dd($res);
        //close connection
        curl_close($ch);
        
        return json_decode($result, 1);
    }
}
