<?php
namespace App\Domains\Taxonomy\Listeners;

use DB;
use Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Taxonomy\Jobs\CopyTaxonomyJob;
use App\Domains\Notifications\Mail\CopyTaxonomyEmail;
use App\Domains\Taxonomy\Events\CopyTaxonomyEvent as Event;


class CopyTaxonomyEventListeners implements ShouldQueue
{
    use DispatchesJobs;

    public function handle(Event $event)
    {
        ini_set('memory_limit','4098M');
        ini_set('max_execution_time', 0);
        try {
            $data = $event->data;
            $result = $this->dispatch(new CopyTaxonomyJob($data['document_id'], $data['taxonomy_name'], $data['is_copy'], $data['is_copy_asso'], $data['user'], $data['requestUrl']));
            $email_data = [
                'subject' => 'The copy of taxonomy "'.$result['source_taxonomy_name'].'" is created successfully',
                'body' => [
                    'email_body' => 'The copy of taxonomy "'.$result['source_taxonomy_name'].'" with title "'.$data['taxonomy_name'].'" has been successfully created.',
                    'document_id' => $result['new_document_id'],
                    'organization_name' => $result['organization_name'],
                    'organization_code' => $result['organization_code']
                ],
                'username' => $data['user']['first_name'],
                'domain_name' => $data['domain_name']
            ];
            Mail::to($data['user']['email'])->send(new CopyTaxonomyEmail($email_data));
        }
        catch(\Exception $exception) {
            Log::error($exception);
        }
    }
}
