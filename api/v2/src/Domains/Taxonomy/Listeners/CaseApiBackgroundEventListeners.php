<?php
namespace App\Domains\Taxonomy\Listeners;

use DB;
use Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Domains\CaseStandard\Jobs\ExtractCustomMetadataFromPackageJob;
use App\Domains\Taxonomy\Jobs\ImportJsonWithNoChangeJob;
use App\Domains\CaseStandard\Jobs\GetDataMetricsFromUploadedCaseJSonContentJob;
use App\Domains\Metadata\Jobs\SaveCustomMetadataJob;
use App\Domains\Import\Jobs\UpdateImportProcessJob;
use App\Domains\User\Jobs\GetUserEmailInfoByOrganizationIdJob;
use App\Domains\User\Jobs\GetOrgNameByOrgIdJob;
use App\Domains\User\Jobs\GetUserOrganizationIdWithUserJob;
use App\Data\Repositories\Contracts\NotificationInterface;
use App\Domains\Notifications\Jobs\ImportJobForEmailJob;
use App\Domains\CaseStandard\Jobs\ValidateCaseServerTaxonomyApiJob;
use App\Domains\Taxonomy\Events\CaseApiBackgroundEvent as Event;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

class CaseApiBackgroundEventListeners implements ShouldQueue
{
    use DispatchesJobs,ErrorMessageHelper, StringHelper, UuidHelperTrait, ArrayHelper;

    public $queue ;
    public $connection = 'import_api_sqs';

    public function __construct(NotificationInterface $notificationActivity)
    {
       $this->queue=config('queue.connections.import_api_sqs.queue');
       $this->notificationRepository = $notificationActivity;
    }
    
    public function handle(Event $event)
    {
        ini_set('max_execution_time', 0);
        try
        {
            $data = $event->caseData;

            $importData = DB::table('import_job')->select('import_job_id')->where('source_document_id',$data['identifier']['document_id'])->where('organization_id',$data['organization_id'])->where('status','2')->first();
            if($importData)
            {
                $errorType = 'validation_error';
                $message = "This taxononomy already in process,please select different taxonomy to import";
                $_status = ' Taxonomy Process already Started.';
                return $this->dispatch(new RespondWithJsonErrorJob($errorType, $message, $_status)); 
            }
            else
            {
                $validateCaseServerTaxonomyApi = $this->dispatch(new ValidateCaseServerTaxonomyApiJob($data['identifier']));
                $fileContent = [];
                if($validateCaseServerTaxonomyApi !== false && $validateCaseServerTaxonomyApi['error_no'] == 0)
                    $fileContent = $this->convertJsonStringToArray($validateCaseServerTaxonomyApi['result']);
                $packageDataArray = [ "packageData" => $fileContent, "oldNewItemMapping" => [] ];
                $getCustomData = $this->dispatch(new ExtractCustomMetadataFromPackageJob($data['identifier']));
    
                $uploadedPackageMetrics = $this->dispatch(new GetDataMetricsFromUploadedCaseJSonContentJob($fileContent)); // Get Item & Associations count
    
                // Update import process to "In Progress"
                $inputinupdateProcessInput['import_job_id']= $data['import_identifier'];
                $inputinupdateProcessInput['results']= '';
                $inputinupdateProcessInput['process_type']= 2;
                $inputinupdateProcessInput['status']= 2;
                $inputinupdateProcessInput['updated_at']= now()->toDateTimeString();
                $this->dispatch(new UpdateImportProcessJob($inputinupdateProcessInput));
    
                // If no associations found
                if($uploadedPackageMetrics['associations_count'] == 0 && $uploadedPackageMetrics['associations_group_count'] == 0)
                {
                    $uploadedPackageMetrics['document_id'] = "";
                    $uploadedPackageMetrics['document_title'] = $uploadedPackageMetrics['source_taxonomy_name'];
                    $uploadedPackageMetrics['import_status'] = "failed";
                    $taxonomyName = $uploadedPackageMetrics['source_taxonomy_name'];
                    // Create Notification for Taxonomy Imported
                    $email_body = "Could not upload the taxonomy {$taxonomyName} as the JSON did not include associtions for the taxnonomy";
                    $user_subject = "The import process for the Taxonomy {$taxonomyName} is failed.";
                    $inputupdateProcessInput['status']= 4;                
                }
                else
                {
                    $sourceType = 3;        // set source_type=3 for API
                    $returnDocumentDetail = $this->dispatch(new ImportJsonWithNoChangeJob($packageDataArray, $data['user_data'], $data['import_type'], $data['case_server'],$data['request_url'], $sourceType));
                    $returnImportType = isset($returnDocumentDetail["import_type"])?$returnDocumentDetail["import_type"]:1;
                    if($returnImportType!='2'){
                        // Code to update subscriber's document id against subscription record
                        DB::table('version_subscriber_detail as vsd')
                            ->join('version_subscription as vs', 'vs.subscriber_detail_id', '=', 'vsd.subscriber_detail_id')
                            ->where(['vsd.subscriber_id'=>$data['user_data']['user_id'],
                            'vsd.subscriber_org_id'=>$data['organization_id'],
                            'vs.is_active'=>'1',
                            'vs.pub_source_document_id'=>$data['identifier']['document_id']])
                            ->update(['sub_document_id' => $returnDocumentDetail["document_id"]]);
                    }

                    $uploadedPackageMetrics['document_id'] = $returnDocumentDetail["document_id"];
                    $uploadedPackageMetrics['document_title'] = $returnDocumentDetail["document_title"];
                    $uploadedPackageMetrics['import_type'] = $returnImportType;
                    $uploadedPackageMetrics['items']       = isset($returnDocumentDetail["items"])?$returnDocumentDetail["items"]:[];
                    $uploadedPackageMetrics['newlyCreatedNode'] = !empty($returnDocumentDetail['newlyCreatedNode'])?$returnDocumentDetail['newlyCreatedNode']:'';
                    $taxonomyName = $returnDocumentDetail["document_title"];
    
                    if($getCustomData['error_no']==0)
                    {
                        $getCustomDataDetails = $getCustomData['result'];
                        $customArr = $this->convertJsonStringToArray($getCustomDataDetails);
                        if(!empty($customArr) && array_key_exists('ACMTcustomfields',$customArr)){
                            $this->dispatch(new SaveCustomMetadataJob($customArr,$uploadedPackageMetrics['document_id'],$data['organization_id'],$uploadedPackageMetrics['items'],$uploadedPackageMetrics['newlyCreatedNode'],$data['copy']));
                        }
                    }
    
                    // Create Notification for Taxonomy Imported
                    $email_body = $user_subject = "The import process for the Taxonomy {$taxonomyName} is completed.";
                    $inputupdateProcessInput['status']= 3;
                    unset($uploadedPackageMetrics['items']);
                }
    
                // Update status of Import Process for In-App notification
                $inputupdateProcessInput['import_job_id']= $data['import_identifier'];
                $inputupdateProcessInput['results']= json_encode($uploadedPackageMetrics);
                $inputupdateProcessInput['updated_at']= now()->toDateTimeString();
                $this->dispatch(new UpdateImportProcessJob($inputupdateProcessInput));
    
                // Send Email notification
                $owner_id              = $data['user_data']['user_id'];
                $organizationId        = $data['organization_id'];
                $notificationId        = $this->createUniversalUniqueIdentifier();
                $notificationMsg       = $email_body;
                $targetContext         = ['document_id'=>$uploadedPackageMetrics['document_id'],'user_id'=>$owner_id];
                $targetContextJson     = json_encode($targetContext,JSON_UNESCAPED_SLASHES);
                $notificationCategory  = config('event_activity')["Notification_category"]["IMPORT_JSON"];
                $getUserDetails        = $this->getUserDetails($owner_id);
                $user_email            = $getUserDetails[0]->email;
                $firstName             = $getUserDetails[0]->first_name;
                $lastName              = $getUserDetails[0]->last_name;
                $currentOrgId          = $getUserDetails[0]->current_organization_id;
                $userName              = $firstName . ' ' . $lastName;
                $userOrganizationId    = $this->dispatch(new GetUserOrganizationIdWithUserJob($owner_id,$organizationId));
                $organizationData      = $this->dispatch(new GetOrgNameByOrgIdJob($userOrganizationId['organization_id']));
                $organizationName      = $organizationData['organization_name'];
                $organizationCode      = $organizationData['organization_code'];
                $emailData1            = [
                    'subject'=>$user_subject,
                    'email'    => $user_email,
                    'body'     => ['email_body'=>$email_body,'user_name'=>$userName,'organization_name'=>$organizationName,'organization_code'=>$organizationCode,'document_id'=>$uploadedPackageMetrics['document_id']],
                    'domain_name'=> $data['domain_name']
                ];    
                    
                $activityLog = [
                    "notification_id"       => $notificationId,
                    "description"           => $notificationMsg,
                    "target_type"           => "15",
                    "target_id"             => $uploadedPackageMetrics['document_id'],
                    "user_id"               => $owner_id,
                    "organization_id"       => $organizationId,
                    "is_deleted"            => "",
                    "created_by"            => $owner_id, // logged in user Id
                    "updated_by"            => $owner_id,
                    "target_context"        => $targetContextJson,
                    "read_status"           => "0", //0=unread 1= read
                    "notification_category" => $notificationCategory
                ];
                
                $this->notificationRepository->createNotification($activityLog);
                unset($activityLog);
                unset($targetContext);
                unset($targetContextJson);
                unset($notificationMsg); 
    
                $checkEmailStatus = $this->dispatch(new GetUserEmailInfoByOrganizationIdJob(['user_id' => $owner_id, 'organization_id' => $organizationId]));
                $emailSettingStatus = $checkEmailStatus[0]['email_setting'];
                if ($emailSettingStatus == 1) {
                    $this->dispatch(new ImportJobForEmailJob($emailData1));
                    unset($emailData1);
                }
    
                $successType = 'found';
                $message = 'Import Successfull.';
                $_status = 'custom_status_here';
                $result = $this->dispatch(new RespondWithJsonJob($successType, $uploadedPackageMetrics, $message, $_status));  
            }
                      
        }
        catch(\Exception $exception) {
            Log::error($exception);
        }
    }    
   
    public function getUserDetails($userId)
    {
        $query = $user = DB::table('users')->where('user_id',$userId)->get();
        if($query) {
            $data = $query->toArray();
            return $data;
        }
    }
}
