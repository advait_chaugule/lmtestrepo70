<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;
use App\Data\Models\ProjectItem;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;


class GetFlattenedTaxonomyTreeV2Job extends Job
{
    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $requestUserDetails;

    private $document;
    private $items;
    private $itemAssociationsUnderDocument;
    private $projectIdsAssociated;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;
    private $parentAndDocumentId;

    private $transformedDocument;
    private $transformedItems;
    private $transformedItemAssociationsUnderDocument;

    private $tree;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->requestUserDetails = $requestUserDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        //
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;


        
        $documentIdentifier =   $this->documentIdentifier;
        $input              =   $this->requestUserDetails;
        if($input['is_customview']){
            $isCustomview = $input['is_customview'];
        }else{
            $isCustomview = 0;
        }
        $organizationId     =   $input["organization_id"];
        
        //Get Document Details
        $returnCollection           =   false;
        $fieldsToReturn             =   ["document_id", "title", "node_type_id", "project_id", "node_type_id","status", "adoption_status","custom_view_visibility","title_html","import_type","is_deleted"];
        $relations                  =   ['nodeType'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];

        //dd($conditionalKeyValuePair);

        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $nodeType = !empty($document->nodeType->node_type_id) ? $document->nodeType->title : "";

        $projectType = $this->helperToReturnProjectType([$document->project_id]);

        $parsedDocument = [
            "id"                    =>  $document->document_id,
            "title"                 =>  !empty($document->title) ? $document->title : "",
            "title_html"            =>  !empty($document->title_html) ? $document->title_html : "",
            "human_coding_scheme"   =>  "",
            "list_enumeration"      =>  "",
            "sequence_number"       =>  "",
            "full_statement"        =>  "",
            "status"                =>  $document->status,
            "adoption_status"       =>  $document->adoption_status,
            "node_type"             =>  $nodeType,
            "metadataType"          =>  $nodeType,
            "node_type_id"          =>  $document->node_type_id,
            "project_enabled"       =>  !empty($projectType == 1) ? 1 : 0,
            "project_name"          =>  "",
            "is_document"           =>  1,
            "custom_view_visibility"=>  $document->custom_view_visibility,
        ];
        
        //Get Item Details
        $returnCollection           =   true;
      
        $relations                  =   ['nodeType', 'customMetadata'];
        $fieldsToReturn             =   ["item_id", "human_coding_scheme", "list_enumeration","full_statement", "node_type_id", "source_item_id", 'custom_view_data',"human_coding_scheme_html","full_statement_html"];
        if($isCustomview == 1)
        {
            $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0, 'custom_view_visibility' => 1];
        }
        else
        {
            $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];  
        }
        
        
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);

          //Relation for taxonomy
          $returnCollection           =   true;
          $fieldsToReturn             =   ["origin_node_id", "destination_node_id","sequence_number"];
          $conditionalKeyValuePair    =   [ 
              "document_id" =>  $documentIdentifier,
              "organization_id" => $organizationId,
              "is_deleted" => 0,
              "association_type" => 1 // 1 for isChildOf relation
          ];
  
          $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                              $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                           );
        $itemSequenceList = [];

        foreach($itemAssociationsUnderDocument as $itemAssociation) 
        {
            $itemSequenceList[$itemAssociation->origin_node_id] = $itemAssociation->sequence_number;
        }                                  
        $searchItemArray = [];
        $searchProjectItemArray = [];
        foreach($items as $item) {
            array_push($searchItemArray,$item->item_id);
        }
        
       
        $projectItems = ProjectItem::whereIn('item_id',$searchItemArray)->where('is_deleted','=',0)->get();
        if(count($projectItems) > 0)
        {
            foreach($projectItems as $projectItem) {
                $projectId[]    =   $projectItem->project_id;
            }

            $projectType = $this->helperToReturnProjectType($projectId);
            
            foreach($projectItems as $projectItem) {
                if($projectType[$projectItem->project_id] == 1) {
                    array_push($searchProjectItemArray,$projectItem->item_id);
                }
            }
        }
        $defaultCustomView = '{"report_path":"","version":"","allow_expand":"no","title":"","style":"font-size:1rem;background:white;font-style:none;color:black"}';
        foreach($items as $item) {
            $itemData = [
                "id" => $item->item_id,
                "title" => "",
                "human_coding_scheme"   => !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "",
                "human_coding_scheme_html"   => !empty($item->human_coding_scheme_html) ? $item->human_coding_scheme_html : $item->human_coding_scheme,
                "list_enumeration"      => !empty($item->list_enumeration) ? $item->list_enumeration : "",
                "sequence_number"       => !empty($itemSequenceList[$item->item_id]) ? $itemSequenceList[$item->item_id] : "",
                "full_statement"        => !empty($item->full_statement) ? $item->full_statement : "",
                "full_statement_html"   => !empty($item->full_statement_html) ? $item->full_statement_html : $item->full_statement,
                "node_type"             => !empty($item->nodeType) ? $item->nodeType->title : "",
                "metadataType"          => !empty($item->nodeType) ? $item->nodeType->title : "",
                "node_type_id"          => !empty($item->node_type_id) ? $item->node_type_id : "",
                "item_type"             => "",
                "project_enabled"       => in_array($item->item_id,$searchProjectItemArray) ? 1 : 0,
                "project_name"          => "",
                "is_document"           => 0,
                "parent_id"             =>  "",
                "document_id"           =>  "",
                "document_title"        =>  "",
                "custom_view_data"      => !empty($item->custom_view_data) ? $item->custom_view_data : $defaultCustomView,
            ];
            if(!$isCustomview == 1)
            {
                unset($itemData['custom_view_data']);
            }
                
            $parsedItems[] = $itemData;
            unset($itemData);
        }

        $parsedItemAssociations = [];
         foreach($itemAssociationsUnderDocument as $itemAssociation) {
        $originNodeId = $itemAssociation->origin_node_id;
        $destinationNodeId = $itemAssociation->destination_node_id;
        if(!empty($originNodeId) && !empty($destinationNodeId) && ($originNodeId != $destinationNodeId)) {
           if(in_array($destinationNodeId,$searchItemArray) || $destinationNodeId == $documentIdentifier)
            {
                $parsedItemAssociations[] = [
                    "child_id" => $itemAssociation->origin_node_id,
                    "parent_id" => $itemAssociation->destination_node_id,
                    "sequence_number" => $itemAssociation->sequence_number
                 ];
            }
       
                                    }
                                } 
        /* This code for handle the orphaned node condition ACMT-953*/                        
        $itemArr = [];
        if(!empty($parsedItems))
        {    
            foreach($parsedItems as $parsedItemsK => $parsedItemsV)
            {
                array_push($itemArr,$parsedItemsV['id']);
            }
        }
        if(!empty($document->document_id))
        {
            array_push($itemArr,$document->document_id);
        }
        if(!empty($parsedItemAssociations))
        {
            foreach($parsedItemAssociations as $parsedItemAssociationsK => $parsedItemAssociationsV)
            {
                if(in_array($parsedItemAssociationsV['parent_id'],$itemArr))
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = false;
                }
                else
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = true;
                }
            }
        } 
        /* End */                        
        if(!empty($parsedItems))
        {
                $nodes = array_prepend($parsedItems, $parsedDocument);
        }
        else
        {
                $nodes[] = $parsedDocument; 
        }                                                       
        
        $treeData = [
            "nodes" => $nodes,
            "relations" => $parsedItemAssociations,
            "import_type" => $document->import_type,
            "is_deleted"  => $document->is_deleted 
        ];
        return $treeData;
    }

    private function helperToReturnProjectType(array $projectId) {
        $projectType            =   [];
        $attributeIn            =   'project_id'; 
        $containedInValues      =   $projectId;
        $returnCollection       =   true;
        $fieldsToReturn         =   ['project_id', 'project_type'];
        $keyValuePairs          =   ['is_deleted' => 0];

        $projectTypeDetail = $this->projectRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs );

        foreach($projectTypeDetail as $project) {
            $projectType[$project->project_id] = $project->project_type;
        }

        return $projectType;
    }
}
