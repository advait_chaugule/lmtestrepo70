<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Services\Api\Traits\UuidHelperTrait;

class CreateGroupForTaxonomyJob extends Job
{
    use UuidHelperTrait;
    private $groupName;
    private $organizationId;
    private $userId;

    public function __construct($groupName,$organizationId,$userId)
    {
        $this->groupName = $groupName;
        $this->organizationId = $organizationId;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groupName = $this->groupName;
        $organizationId = $this->organizationId;
        $userId = $this->userId;

        $groupId = $this->createUniversalUniqueIdentifier();
        $date = date('Y-m-d H:i:s');
        $groupArray = [
                        "group_id"=>$groupId,
                        "name"=>$groupName,
                        "is_deleted"=>'0',
                        "organization_id"=>$organizationId,
                        "user_id"=>$userId,
                        "created_by"=>$userId,
                        "updated_by"=>$userId,
                        "is_user_default"=>"0",
                        "is_default"=>"0",
                        "created_at"=>$date,
                        "updated_at"=>$date
                    ];
        
        DB::table('group')->insert($groupArray);

        return DB::table('group')->select('group_id','name as group_name')->where('group_id','=',$groupId)->get();
        
    }
}
