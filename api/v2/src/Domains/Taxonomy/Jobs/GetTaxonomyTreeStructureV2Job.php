<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\NodeLevelTrait;
use Illuminate\Support\Facades\DB;

class GetTaxonomyTreeStructureV2Job extends Job
{

    use NodeLevelTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $documentId;
    private $organizationId;

    public function __construct($documentIdentifier, $organizationId, $userId,$publishedPermission)
    {
        $this->documentId = $documentIdentifier;
        $this->organizationId = $organizationId;
        $this->userId = $userId;
        $this->dummy_id_for_orphan_label = 'orphan_label_node_id';
        $this->publishedPermission = $publishedPermission; //To check view permission
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $documentIdentifier = $this->documentId;
        $organizationId = $this->organizationId;
        $userId = $this->userId;
        // Get Node level details for a particular document - This only has items and no document
        $orphanLabel = $this->dummy_id_for_orphan_label;
        $nodeLevel = $this->getNodesDepth($this->documentId,[],true);
        // Fetch document details of this document and make array containing this as main child
        $document = DB::table('documents')
            ->join('node_types', 'documents.node_type_id', '=', 'node_types.node_type_id')
            ->where('documents.document_id', $this->documentId)
            ->where('documents.organization_id', $this->organizationId)
            ->select('documents.document_id','documents.title','documents.node_type_id','documents.project_id','documents.node_type_id','documents.status',
            'documents.adoption_status','documents.custom_view_visibility','documents.title_html','documents.import_type','documents.is_deleted','node_types.title as node_title','documents.updated_at','documents.document_type','documents.language_id','documents.uri','documents.source_document_id','documents.source_type')
            ->get()->first();
            
        if(empty($document->document_id))
            return [];
        
        $nodeType = !empty($document->node_type_id) ? $document->node_title : ""; // Get nodetype of document        

        $projectType = $this->helperToReturnProjectType([$document->project_id]); //Get Project Type
        // Create array for document node as per values in db
        /********** Missing language for Document resolved : UF-1820 ********/
        $languageName = ''; 
        if(!empty($document->language_id)){
            $language = DB::table('languages')->select('name')->where(['language_id'=>$document->language_id,'is_deleted'=>0])->first();
            if(isset($language->name)){
                $languageName = $language->name; 
            }
        }
        /********** Checked for subscription : UF-346 ********/
        $isSubscribed = false; //If has view permission and is a publisher
        $subscription = DB::table('version_subscriber_detail as vsd')
                        ->join('version_subscription as vs', 'vs.subscriber_detail_id', '=', 'vsd.subscriber_detail_id')
                        ->where(['vs.is_active'=>'1',
                        'vs.pub_source_document_id'=>$document->source_document_id,
                        'sub_document_id' => $document->document_id])->first();
        if(isset($subscription->subscription_id)){ // If document is subscribed, then check for subscriber_id
            $isSubscribed = ($subscription->subscriber_id==$userId && $subscription->subscriber_org_id==$organizationId);
        }
        $isPublisher = ($this->publishedPermission && !isset($subscription->subscription_id)); // If has publisher permission and current taxonomy is not subscribed then show report to all users of that tenant
        /********** Checked for subscription : UF-346 ********/
            
        /********** Missing language for Document resolved : UF-1820 ********/
        $parsedDocument = [
            "id"                    =>  $document->document_id,
            "title"                 =>  !empty($document->title) ? $document->title : "",
            "title_html"            =>  !empty($document->title_html) ? $document->title_html : "",
            "human_coding_scheme"   =>  "",
            "list_enumeration"      =>  "",
            "sequence_number"       =>  "",
            "full_statement"        =>  !empty($document->title) ? $document->title : "",
            "status"                =>  $document->status,
            "document_type"         =>  $document->document_type,
            "adoption_status"       =>  $document->adoption_status,
            "node_type"             =>  $nodeType,
            "metadataType"          =>  $nodeType,
            "node_type_id"          =>  $document->node_type_id,
            "project_enabled"       =>  !empty($projectType == 1) ? 1 : 0,
            "project_name"          =>  "",
            "is_document"           =>  1,
            "custom_view_visibility"=>  $document->custom_view_visibility,
            "is_orphan"             =>  0,
            "full_statement_html"   =>  !empty($document->title) ? $document->title : "",
            "uri"                   =>  (!empty($document->uri) && !$isPublisher) ? $document->uri : "",
            "source_document_id"    =>  !empty($document->source_document_id) ? $document->source_document_id : "",
            "source_type"           =>  !empty($document->source_type) ? $document->source_type : "",
            "is_subscribed"         =>  ($isSubscribed || $isPublisher),
            "subscribed_version"    =>  !empty($subscription->version) ? $subscription->version : 1,
            "expand" => true,
            "cut" => 0,
            "paste" => 0,
            "reorder" => 0,
            "level" => 1,
            "isFirst" => true,
            "language_name"         => $languageName,
            "children" => []
        ];
        // Check item belongs to a project
        $searchItemArray = array_unique(array_column($nodeLevel,'item_id'));
        $searchProjectItemArray = [];

        $projectItems =DB::table('project_items')->whereIn('item_id',$searchItemArray)->where('is_deleted','0')->where('is_editable',1)->get();
        $projectId =[];
        if(count($projectItems) > 0)
        {
            foreach($projectItems as $projectItem) {
                $projectId[]    =   $projectItem->project_id;
            }
            $projectType = $this->helperToReturnProjectType($projectId);
            foreach($projectItems as $projectItem) {
                if($projectType[$projectItem->project_id] == 1) {
                    array_push($searchProjectItemArray,$projectItem->item_id);
                }
            }
        }

        // Get nodetype as per node_type_id of items
        $nodeTypesArray = DB::table('node_types')
            ->whereIn('node_types.node_type_id', array_unique(array_column($nodeLevel,'node_type_id')))
            ->where('node_types.is_deleted',0)
            ->where('node_types.used_for',0)
            ->select('node_types.node_type_id','node_types.title as node_titles')
            ->get()->toArray();
        $nodeTypes = array_column($nodeTypesArray,'node_titles','node_type_id');

        // Create child array for items as per level
        $temp = array(); // create temp array
        $temp[$document->document_id] = $parsedDocument['children']; // Assign root node to temp array                
        $tree = &$temp[$document->document_id.$document->document_id]['children']; // Assign reference of temp root node's children array
        $configItem = [];
        $ItemfinalList = $this->getUserSettingMetadata($documentIdentifier,$organizationId,$userId);
        $grandParent=[];
        foreach($nodeLevel as $key=>$item){
        }
        foreach($nodeLevel as $key=>$item){
            $child = $item->item_id;
            $parent = $item->target_item_id;
            $grandParent[$child] = $parent.$item->item_association_id.$item->lvl; 
			// Unset configItem if metadata does not exist
            if(!empty($ItemfinalList) && array_key_exists($item->item_id,$ItemfinalList)){
                $configItem = $ItemfinalList[$item->item_id];       
            }else{
                $configItem=[];
            }
            $childKey = $child.$parent.$item->item_association_id.$item->lvl;
            $temp[$childKey] = ["id" => $item->item_id,
            "title" => "",
            "human_coding_scheme" => !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "",	
            "human_coding_scheme_html" => !empty($item->human_coding_scheme_html) ? $item->human_coding_scheme_html : $item->human_coding_scheme,
            "list_enumeration" => (string) (!empty($item->list_enumeration) ? $item->list_enumeration : (!empty($item->seq) ? $item->seq : "")),
            "sequence_number" => !empty($item->seq) ? $item->seq : (!empty($item->list_enumeration) ? $item->list_enumeration : ""),
            "full_statement" => !empty($item->full_statement) ? $item->full_statement : "",
            "full_statement_html" => !empty($item->full_statement_html) ? $item->full_statement_html : $item->full_statement,
            "node_type" => !empty($item->node_type_id && isset($nodeTypes[$item->node_type_id])) ? $nodeTypes[$item->node_type_id] : "",
            "metadataType" => !empty($item->node_type_id && isset($nodeTypes[$item->node_type_id])) ? $nodeTypes[$item->node_type_id] : "",
            "node_type_id" => !empty($item->node_type_id) ? $item->node_type_id : "",
            "item_type" => "",
            "project_enabled" => in_array($item->item_id,$searchProjectItemArray) ? 1 : 0,
            "project_name" => "",
            "is_document" => 0,
            "parent_id" => ($item->association_type==1) ? $parent : $orphanLabel,
            "document_id" => "",
            "document_title" => "",
            "is_orphan" => ($item->association_type==1) ? 0 : 1,
            "children" => [],
            "expand" => false,
            "cut" => 0,
            "paste" => 0,
            "reorder" => 0,
            "level" => isset($item->lvl) ? ($item->lvl+1) : 1];
            
            if(!empty($configItem)){
                $temp[$childKey] = array_merge($temp[$childKey],$configItem);
            }
            if(!isset($grandParent[$parent])){
                $grandParent[$parent] = $document->document_id;
            }
            $parentKey = $parent.$grandParent[$parent];
            if (!empty($parent)) {
                if($item->association_type==1)
                    $temp[$parentKey]['children'][] = &$temp[$childKey];
                else
                    $temp[$parentKey]['children'][$orphanLabel][] = &$temp[$childKey];
            }             
        }
        // Orphan code changes
        if(isset($tree[$orphanLabel])){           
            $orphanLableNode = []; // 'orphanLableNode' is one dummy node
            $title = 'Nodes with no linked parent';
            $orphanLableNode['full_statement'] = $title;
            $orphanLableNode['full_statement_html'] = $title;
            $orphanLableNode['human_coding_scheme'] = '';
            $orphanLableNode['is_document'] = 0;
            $orphanLableNode['cut'] = 0;
            $orphanLableNode['paste'] = 0;
            $orphanLableNode['reorder'] = 0;
            $orphanLableNode['children'] = $tree[$orphanLabel];
            $orphanLableNode['sequence_number'] = '';
            $orphanLableNode['isOrphan'] = true;
            $orphanLableNode['isOrphanLabel'] = true;
            $orphanLableNode['parent_id'] = $document->document_id;
            $orphanLableNode['id'] = $orphanLabel; // assigning dummy id
            $orphanLableNode['list_enumeration'] = '';
            $orphanLableNode['level'] = 2;
            $orphanLableNode['expand'] = true;
            $tree[]=$orphanLableNode;
            unset($tree[$orphanLabel]);
        }
        $parsedDocument['children'] = $tree;
        // Pass data in children key
        $data = [
            'children' => [$parsedDocument],
            "import_type" => $document->import_type,
            "modified_at" => $document->updated_at,
            "is_deleted"  => $document->is_deleted
        ];
        
        return $data;
    }    

    /**
     * @param $projectId
     * @return mixed
     * @FunctionName helperToReturnProjectType
     */    

    private function helperToReturnProjectType(array $projectId) {
        $projectType            =   [];
        
        $projectTypeDetail =DB::table('projects')
                            ->select('project_id','project_type')
                            ->whereIn('project_id',$projectId)
                            ->where('is_deleted','0')
                            ->get();
            
        foreach($projectTypeDetail as $project) {
            $projectType[$project->project_id] = $project->project_type;
        }
        return $projectType;
    }    

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @FunctionName getTaxonomyCustomData
     */
    private function getTaxonomyCustomData($documentId,$organizationId,$userId)
    {
          $userSetting = DB::table('user_settings')->select('json_config_value')
                              ->where('user_id',$userId)
                              ->where('id',$documentId)
                              ->where('organization_id',$organizationId)
                              ->where('id_type',3)
                              ->where('is_deleted',0)
                              ->get()
                              ->first();
          return $userSetting;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @FunctionName getDocumentDefaultSetting
     */
    private function getDocumentDefaultSetting($documentId,$organizationId)
    {
          $getDocumentJson =  DB::table('documents')->select('display_options')
                               ->where('document_id',$documentId)
                               ->where('organization_id',$organizationId)
                               ->where('is_deleted',0)
                               ->get()
                               ->first();
          return $getDocumentJson;

    }
    

    /**
     * @param $internalNames
     * @param $organizationId
     * @return mixed
     * @Function Name getInternalMatching
     */
    private function getInternalMatching($internalNames,$documentIdentifier,$organizationId)
    {
        $queryData = DB::table('items')
            ->select($internalNames)
            ->leftJoin('concepts', function ($join) {
                $join->on('concepts.concept_id','=','items.concept_id')
                     ->where('concepts.is_deleted', '=', 0);
            })
            ->leftJoin('licenses', function ($join) {
                $join->on('licenses.license_id','=','items.license_id')
                     ->where('licenses.is_deleted', '=', 0);
            })
            ->leftJoin('languages', function ($join) {
                $join->on('languages.language_id','=','items.language_id')
                     ->where('languages.is_deleted', '=', 0);
            })
            ->where('items.document_id',$documentIdentifier)
            ->where('items.organization_id',$organizationId)
            ->where('items.is_deleted',0)
            ->get()
            ->toArray();
        return $queryData;
    }    

    private function getCustomMetadata($fieldName,$metadataIds)
    {
        $customMetadataArray = DB::table('item_metadata')
                          ->select('item_id as id', 'metadata_id', 'metadata_value')
                          ->where('is_deleted',0)
                          ->whereIn('metadata_id',$metadataIds)
                          ->get()
                          ->toArray();
        //Modified code to get as per mapped metadata_id & name - Start
        $customMetadata=[];
        foreach($customMetadataArray as $key => $custom){
            $customMetadata[$key]['id']=$custom->id;
            $customMetadata[$key][$fieldName[$custom->metadata_id]]=$custom->metadata_value;
        }
        //Modified code to get as per mapped metadata_id & name - End
        return $customMetadata;
    }

    private function getUserSettingMetadata($documentIdentifier,$organizationId,$userId){
        // User Setting for Taxonomy
        $documentArr =[];
        $view_type = "";
        $ItemfinalList =[];        
        //Check user setting is exist in user_setting table or not
        $getUserSettingData = $this->getTaxonomyCustomData($documentIdentifier,$organizationId,$userId);
        if($getUserSettingData) {
            if ($getUserSettingData->json_config_value) {
                $userJsonArr = json_decode($getUserSettingData->json_config_value, true);
                if (!empty($userJsonArr) && isset($userJsonArr['last_table_view'])) {
                    $tableName = $userJsonArr['last_table_view'];
                    foreach ($userJsonArr['taxonomy_table_views'] as $userJsonArrK => $userJsonArrV) {
                        $checkUserSettingStatus = $userJsonArrV['is_user_default'];
                        $savedTableName = $userJsonArrV['table_name'];
                        $view_type = $userJsonArrV['view_type'];
                        if ($checkUserSettingStatus == 1 && $savedTableName == $tableName) {
                            foreach ($userJsonArrV['table_config'] as $userJsonArrVK => $userMetadataList) {
                                $documentArr[] = $userMetadataList;
                            }
                        }
                    }
                }
            }
        }
        else{
            $getDocumentSetting = $this->getDocumentDefaultSetting($documentIdentifier, $organizationId);
                   
            if (!empty($getDocumentSetting->display_options)) {
                $documentJsonArr = json_decode($getDocumentSetting->display_options, true);
                $documentTable = $documentJsonArr['last_table_view'];
                $view_type = $documentJsonArr['taxonomy_table_views']['view_type'];
                if ($documentJsonArr['taxonomy_table_views']['default_for_all_user'] == 1 && $documentJsonArr['taxonomy_table_views']['table_name']==$documentTable) {
                    $tableConfig = $documentJsonArr['taxonomy_table_views']['table_config'];
                    $documentArr = $tableConfig;
                }
            }
        }
        // Check column name with existing or new
        $customInternalName=[];
        $metadataCustomIds=[];
        $internalNameOfNodeMetadata=[];
        if(!empty($documentArr)){
            if($view_type == "metadata"){
                foreach($documentArr as $internalNameK =>$internalNameV) {
                    if(isset($internalNameV['is_custom']) && $internalNameV['is_custom']==1) {
                        $customInternalName[$internalNameV['metadata_id']] = $internalNameV['internal_name']; //Modified code to get as per mapped metadata_id & name
                        $metadataCustomIds[] = $internalNameV['metadata_id'];
                    }else{
                        $internalNameOfNodeMetadata[] = $internalNameV['internal_name'];
                    }
                }

            }
            if($view_type == "node_type"){
                //print_r($documentArr);
                foreach($documentArr as $internalNameK =>$internalNameV) {
                    $internalMetaDataName = $internalNameV['metadata'];
                    foreach($internalMetaDataName as $internalNameKey=>$internalNameVal) {
                        if(isset($internalNameVal['is_custom']) && $internalNameVal['is_custom']==1) {
                            $customInternalName[$internalNameVal['metadata_id']] = $internalNameVal['internal_name']; //Modified code to get as per mapped metadata_id & name
                            $metadataCustomIds[] = $internalNameVal['metadata_id'];
                        }else{
                            $internalNameOfNodeMetadata[] = $internalNameVal['internal_name'];
                        }
                    }
                }
            }

            $defaultCol = ['full_statement','node_type','human_coding_scheme','list_enumeration'];
            foreach ($internalNameOfNodeMetadata as $keys=>$internalNameOfNodeMetadataV)
            {
                if(in_array($internalNameOfNodeMetadataV,$defaultCol)) {
                    unset($internalNameOfNodeMetadata[$keys]);
                }
            }

        $fieldNames =[];
        $fieldNames[] = 'items.item_id as id';
       
        if(!empty($internalNameOfNodeMetadata)) {
            foreach ($internalNameOfNodeMetadata as $key=>$value) {
                if(strpos($value,'_')!==false){
                    $posnum = strpos($value,'_');
                    $table_name  = substr($value,0,$posnum);
                    $column_name = substr($value,$posnum+1);
                }else{
                  $table_name    = "Default";
                }
                switch($table_name){
                    case "concept":
                        $concept = true;
                        if($column_name == "title"){
                            $alias = 'concept_title';
                        }else if($column_name == "description"){
                            $alias = 'concept_description';
                        }else if($column_name == "hierarchy_code"){
                            $alias = 'concept_hierarchy_code';
                        }else if($column_name == "keywords"){
                            $alias = 'concept_keywords';
                        }
                        
                        if(isset($alias)){
                            $fieldNames[] = 'concepts.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    case "license":
                        $license = true;
                        if($column_name == "title"){
                            $alias = 'license_title';
                        }else if($column_name == "description"){
                            $alias = 'license_description';
                        }else if($column_name == "hierarchy_code"){
                            $alias = 'license_hierarchy_code';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'licenses.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    
                    case "items":
                        $items = true;
                        if($column_name == "start_date"){
                            $alias = 'items_status_start_date';
                        }else if($column_name == "end_date"){
                            $alias = 'items_status_end_date';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'items.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    
                    case "language":
                        if($column_name == "name"){
                            $alias = 'language_name';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'languages.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                         }
                        unset($alias);
                        break;
                    default:
                        $fieldNames[] = $value;
                }
            }
            $getMetadataName = $this->getInternalMatching($fieldNames,$documentIdentifier,$organizationId);

            foreach ($getMetadataName as $getMetadataNameV) {
                $itemId = $getMetadataNameV->id;
                $ItemfinalList[$itemId]= (array)$getMetadataNameV;
            }

        }
        // Check Custom metadata
        if($customInternalName && $metadataCustomIds) {
            $customInternalName[] = 'item_id as id';
            $customData = $this->getCustomMetadata($customInternalName,$metadataCustomIds);
            if($customData) {
                foreach ($customData as $customDataK=>$customDataV) {
                    if(isset($ItemfinalList[$customDataV['id']])){
                        $ItemfinalList[$customDataV['id']] = array_merge($ItemfinalList[$customDataV['id']],$customDataV);
                    }
                    else{
                        $ItemfinalList[$customDataV['id']] = $customDataV;
                    }
                }
            }
        }
        }
        // User Setting for taxonomy ends here
        return $ItemfinalList;
    }
}
