<?php
namespace App\Domains\Taxonomy\Jobs;

ini_set('max_execution_time', 0);

use App\Data\Models\ItemAssociation;
use App\Data\Models\ProjectItem;
use Lucid\Foundation\Job;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class SetOrderedTaxonomyTreeJob extends Job
{
    use ErrorMessageHelper, StringHelper, DateHelpersTrait, UuidHelperTrait; 

    private $documentIdentifier;
    private $documentRepository;
    private $itemRepository;
    private $itemAssociationRepository;
    private $userOrganizationId;
    private $inputData;
    private $savedDocumentObject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($documentIdentifier, $inputData)
    {
        //Set the private variables
        $this->setDocumentIdentifier($documentIdentifier);
        $this->inputData    =   $inputData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        MetadataRepositoryInterface $metadataRepository,
        Request $request
    )
    {
        //Set the repo handlers
        $this->documentRepository           =   $documentRepository;
        $this->itemRepository               =   $itemRepository;
        $this->itemAssociationRepository    =   $itemAssociationRepository;
        $this->metadataRepository           =   $metadataRepository;

        $this->extractAndSetUserOrganizationIdFromRequestBody($request);
        $this->extractAndSetUserIdFromRequestBody($request);

        $this->extractSaveItems($request);
    }

    /**
     * Public Getter and Setter methods
     */
    public function setDocumentIdentifier($identifier) {
        $this->documentIdentifier   =   $identifier;
    }

    public function getDocumentIdentifier(){
        return $this->documentIdentifier;
    }

    public function setUserOrganizationId(string $data) {
        $this->userOrganizationId = $data;
    }

    public function getUserOrganizationId(): string {
        return $this->userOrganizationId;
    }

    public function setUserId(string $data) {
        $this->userId = $data;
    }

    public function getUserId(): string {
        return $this->userId;
    }

    private function extractAndSetUserOrganizationIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userOrganizationId = $userPerformingAction["organization_id"];
        
        $this->setUserOrganizationId($userOrganizationId);
    }

    private function extractAndSetUserIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userId = $userPerformingAction["user_id"];
        $this->setUserId($userId);
    }

    private function extractSaveItems($request) {
        $type      = $request->input('type');
        $projectId = $request->input('project_id');
        $type      = isset($type)?$type:'';
        $projectId = isset($projectId)?$projectId:'';
        $itemCustomMetadataArray        =   [];
        $parentItemCustomMetadataArray  =   [];

        $associationType    =   1;
        $items              =   $request->input('items');
        $old_items = [];
        if(null !== $request->input('old_items'))
        {
            $old_items          =   $request->input('old_items');
        }
        
        $documentId         =   $this->getDocumentIdentifier();
        $userId             =   $this->getUserId();
        $currentDateTime    =   $this->createDateTime();
        $userOrganizationId =   $this->getUserOrganizationId();
        foreach($items as $itemKey => $data) {
            $item_id                        =   $data['item_id'];
            $parentId                       =   !empty($data["parent_id"]) ? $data["parent_id"] : "";
            $listEnumeration                =   $data['list_enumeration'];
            $itemType                       =   !empty($data['item_type']) ? $data['item_type'] : '';
            $parentItemType                 =   !empty($data['parent_item_type']) ? $data['parent_item_type'] : '';
            
            if($parentId != $documentId) {
                if($itemType == 'standard' && $parentItemType == 'container'){
                    $associationType    =   3;
                }
            } else {
                if($itemType == 'standard'){
                    $associationType    =   3;
                }
            }
            
            $updatedAt = $currentDateTime;
            $attributes = [
                "list_enumeration"  =>  $listEnumeration,
                "updated_at"        =>  $updatedAt,
                "organization_id"   =>  $userOrganizationId,
                "updated_by"        =>  $userId,
            ];

            if($associationType != '3'){
                $attributes['parent_id']    =   $parentId;
            }


            $this->itemRepository->edit($item_id, $attributes);
            if($type=='project') {
                // In this case document Id treat as Project Id
                  $projectItemDetails = $this->getProjectItemDetails($projectId);
                  $itemIds = [];
                  foreach ($projectItemDetails as $arrItemIds)
                  {
                      $itemIds[] = $arrItemIds['item_id'];

                  }
                  $this->updateItemAssociationDetails($itemIds);

            }else{  // This is for taxonomy
                $deleteAttributes       =   ['is_deleted'   => '1'];
                $columnValueFilterPairs =   ['document_id'  =>  $documentId];
                $previousAssociations   =   $this->itemAssociationRepository->editWithCustomizedFields($columnValueFilterPairs, $deleteAttributes);
            }
            
            if(count($old_items) > 0) {
                $origin_node_id = $old_items[$itemKey]['item_id'];
                $destination_node_id = $old_items[$itemKey]['parent_id'];
                if($destination_node_id != "")
                {
                    $deleteDataForAssociationExistsForTaxonomy = [
                        'origin_node_id' => $origin_node_id,
                        'destination_node_id' => $destination_node_id,
                        'association_type' => 1,
                    ];
        
                    $checkAssociationExistsForTaxonomy =   $this->itemAssociationRepository->findByAttributes($deleteDataForAssociationExistsForTaxonomy);
                    
                    if($checkAssociationExistsForTaxonomy->count() > 0) {
                        $this->itemAssociationRepository->delete($deleteDataForAssociationExistsForTaxonomy);
                    }
                }
            }
            $itemAssociationID = $this->createUniversalUniqueIdentifier();
            $caseAssociationsToSave[] = [
                "item_association_id" => $itemAssociationID,
                "association_type" => $associationType,
                "document_id" => $documentId,
                "association_group_id" => '',
                "origin_node_id" => $item_id,
                "destination_node_id" => $parentId,
                "destination_document_id" => $documentId,
                "sequence_number" => $listEnumeration,
                "external_node_title" =>"",
                "external_node_url" =>"",
                "created_at" => $currentDateTime,
                "updated_at" => $updatedAt,
                "source_item_association_id" => $itemAssociationID,
                "organization_id" => $userOrganizationId 
            ];
        }

        //dd($caseAssociationsToSave);

        $this->itemAssociationRepository->saveMultiple($caseAssociationsToSave);

    }

    public function getProjectItemDetails($projectId)
    {
        $query = ProjectItem::select('item_id')
                 ->where('project_id',$projectId)
                 ->where('is_deleted','=',0)
                 ->get()
                 ->toArray();
        return $query;
    }

    public function updateItemAssociationDetails($itemIds)
    {
        ItemAssociation::whereIn('origin_node_id',$itemIds)
                        ->where('association_type',1)
                        ->update(['is_deleted'=> 0]);
    }

}