<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;

class GetFlattenedTaxonomyTreeJob extends Job
{

    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $requestUserDetails;

    private $document;
    private $items;
    private $itemAssociationsUnderDocument;
    private $projectIdsAssociated;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;
    private $parentAndDocumentId;

    private $transformedDocument;
    private $transformedItems;
    private $transformedItemAssociationsUnderDocument;

    private $tree;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setRequestUserDetails($requestUserDetails);
    }

    public function setDocumentIdentifier(string $data) {
        $this->documentIdentifier = $data;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setDocument(Document $data) {
        $this->document = $data;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setItems(Collection $data) {
        $this->items = $data;
    }

    public function getItems(): Collection {
        return $this->items;
    }

    public function setItemAssociationsUnderDocument(Collection $data) {
        $this->itemAssociationsUnderDocument = $data;
    }

    public function getItemAssociationsUnderDocument(): Collection {
        return $this->itemAssociationsUnderDocument;
    }

    public function setIdsOfProjectsAssociated(array $data) {
        $this->projectIdsAssociated = $data;
    }

    public function getIdsOfProjectsAssociated(): array {
        return $this->projectIdsAssociated;
    }

    public function setProjectItemMapping(Collection $data) {
        $this->projectItemMapping = $data;
    }

    public function getProjectItemMapping(): Collection {
        return $this->projectItemMapping;
    }

    public function setParentAndDocumentId($data) {
        $this->parentAndDocumentId = $data;
    }

    public function getParentAndDocumentId() {
        return $this->parentAndDocumentId;
    }

    public function setProjectsAssociated(Collection $data) {
        $this->projectsAssociated = $data;
    }

    public function getProjectsAssociated(): Collection {
        return $this->projectsAssociated;
    }

    public function setNodeTypes(Collection $data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes(): Collection {
        return $this->nodeTypes;
    }

    public function setTransformedDocument(array $data) {
        $this->transformedDocument = $data;
    }

    public function getTransformedDocument(): array {
        return $this->transformedDocument;
    }

    public function setTransformedItems(array $data) {
        $this->transformedItems = $data;
    }

    public function getTransformedItems(): array {
        return $this->transformedItems;
    }

    public function setTransformedItemAssociationsUnderDocument(array $data) {
        $this->transformedItemAssociationsUnderDocument = $data;
    }

    public function getTransformedItemAssociationsUnderDocument(): array {
        return $this->transformedItemAssociationsUnderDocument;
    }

    public function setTree(array $data) {
        $this->tree = $data;
    }

    public function getTree(): array {
        return $this->tree;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;

        // start fetching document, items and item associations etc
        $this->fetchAndSetAllTaxonomyRelatedData();

        // start transforming of data to ui compatible structure
        $this->transformTaxonomyRelatedData();

        // create the ui compatible tree response structure
        $this->buildTree();

        return $this->getTree();
        
    }

    /**
     * start fetching document, items and item associations from database
     */
    private function fetchAndSetAllTaxonomyRelatedData() {
        $this->fetchAndSetDocument();
        $this->fetchAndSetItems();
        $this->fetchAndSetItemAssociationsUnderDocument();
        $this->fetchAndSetProjectItemMapping();
        $this->fetchAndSetItemParentAndDocumentId();
        // $this->fetchAndSetProjectsAssociated();
        // $this->fetchAndSetNodeTypeDetailsForAllItemsAndDocument();
    }

    private function fetchAndSetDocument() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $input              =   $this->getRequestUserDetails();
        $organizationId     =   $input["organization_id"];
        
        $returnCollection           =   false;
        $fieldsToReturn             =   ["document_id", "title", "node_type_id", "project_id", "node_type_id", "adoption_status","status"];
        $relations                  =   ['nodeType'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];

        //dd($conditionalKeyValuePair);

        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        /**This line was causing laravel log*/
        if(!is_null($document)){
            $this->setDocument($document);
        }
        
    }

    private function fetchAndSetItems() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $organizationId     =   $this->getRequestUserDetails()["organization_id"];
        
        $returnCollection           =   true;
        $fieldsToReturn             =   ["item_id", "human_coding_scheme", "list_enumeration","full_statement", "node_type_id", "source_item_id"];
        $relations                  =   ['nodeType', 'customMetadata'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];
        
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $this->setItems($items);
    }

    private function fetchAndSetItemAssociationsUnderDocument() {
        $documentIdentifier =   $this->getDocumentIdentifier();
        $organizationId     =   $this->getRequestUserDetails()["organization_id"];
        $returnCollection           =   true;
        $fieldsToReturn             =   ["origin_node_id", "destination_node_id"];
        $conditionalKeyValuePair    =   [ 
            "document_id" =>  $documentIdentifier,
            "organization_id" => $organizationId,
            "is_deleted" => 0,
            "association_type" => 1 // 1 for isChildOf relation
        ];

        $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                         );
            // dd($itemAssociationsUnderDocument);
        $this->setItemAssociationsUnderDocument($itemAssociationsUnderDocument);
    }

    private function fetchAndSetItemParentAndDocumentId() {
        $hierarchyIdSet =   [];
        $items = $this->getItems();

        $itemSourceIdArray    =   [];
        foreach($items as $item) {
            $itemSourceIdArray[]  =   $item->source_item_id;
        }
        

        $attributeInAssociation         =   'origin_node_id';
        $containedInValuesAssociation   =   $itemSourceIdArray;
        $returnCollectionAssociation    =   true;
        $fieldsToReturnAssociation      =   ["destination_node_id"];

        $parentIdSetForItem = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeInAssociation, $containedInValuesAssociation, $returnCollectionAssociation, $fieldsToReturnAssociation)->toArray();

        $parentIdSetForItemArray    =   array_reverse($parentIdSetForItem);

        foreach($itemSourceIdArray as $key => $item) {
            $returnCollection   =   false;
            $fieldsToReturn     =   ['document_id', 'item_id'];
            $keyValuePairs      =   ['item_id'   => $item];

            $documentSetForItem =   $this->itemRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs );

            $hierarchyIdSet[$item] =   ['document_id' => !empty($documentSetForItem->document_id) ? $documentSetForItem->document_id : "", 'parent_id'    =>  !empty($parentIdSetForItemArray[$key]['destination_node_id'])? $parentIdSetForItemArray[$key]['destination_node_id']: ""];
        }

        $this->setParentAndDocumentId($hierarchyIdSet);
    }

    private function fetchAndSetProjectItemMapping() {
        $itemIds = $this->createArrayFromCommaeSeparatedString($this->getItems()->implode("item_id", ","));
        $projectItemMapping = $this->projectRepository->getProjectItemMappingOfMultipleItems($itemIds);
        $this->setProjectItemMapping($projectItemMapping);
    }

    /**
     * start transforming of data to ui compatible structure
     */
    private function transformTaxonomyRelatedData() {
        $this->transformDocument();
        $this->transformItems();
        $this->transformItemAssociations();
    }

    private function transformDocument() {
        $document = $this->getDocument();
        $nodeType = !empty($document->nodeType->node_type_id) ? $document->nodeType->title : "";
        //dd($document);
        if(!empty($document->project_id)) {

            $organizationId             =   $this->getRequestUserDetails()["organization_id"];
            $returnCollection           =   false;
            $fieldsToReturn             =   ["project_type"];
            $conditionalKeyValuePair    =   [ "project_id" =>  $document->project_id, "organization_id" => $organizationId ];
            
            $project = $this->projectRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
            if($project->project_type == 1)
            {
                $projectEnabled = 1;
            }
            else
            {
                $projectEnabled = 0;
            }  
        }
        else {
            $projectEnabled = 0;
        }

        $projectName            =   "";
        $documentNodeTypeId     =   $document->node_type_id;
        $documentNodeTypeTitle  =   $nodeType;
        $documentStatus         =   $document->adoption_status;
        $status                 =   $document->status;

        $parsedDocument = [
            "id"                    =>  $document->document_id,
            "title"                 =>  !empty($document->title) ? $document->title : "",
            "human_coding_scheme"   =>  "",
            "list_enumeration"      =>  "",
            "full_statement"        =>  "",
            "adoption_status"       =>  $documentStatus,
            "status"                =>  $status,
            "node_type"             =>  $documentNodeTypeTitle,
            "metadataType"          =>  $documentNodeTypeTitle,
            "node_type_id"          =>  $documentNodeTypeId,
            "project_enabled"       =>  (string) $projectEnabled,
            "project_name"          =>  $projectName,
            "is_document"           =>  1
        ];
        
        $this->setTransformedDocument($parsedDocument);
    }

    private function transformItems() {
        $items = $this->getItems();
        $projectItemMapping = $this->getProjectItemMapping()->toArray();

        $parentAndDocumentId    =   $this->getParentAndDocumentId();
        
        $parsedItems = [];
        foreach($items as $item) {
            $itemId = $item->item_id;

            $projectEnabled = $this->helperToReturnProjectEnabledStatus($itemId, $projectItemMapping);

            $listEnumeration = !empty($item->list_enumeration) ? $item->list_enumeration : "";
            $humanCodingScheme = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "";
            $fullStatememnt = !empty($item->full_statement) ? $item->full_statement : "";

            $nodeTypeId = !empty($item->node_type_id) ? $item->node_type_id : "";
            $nodeType = !empty($item->nodeType) ? $item->nodeType->title : "";

            $typeMetadataDet    =   $item->customMetadata->where('is_document', '3')->first();      
            $sourceDocumentDetail   =   $this->documentRepository->find($parentAndDocumentId[$item->source_item_id]['document_id']);

            $projectName = "";

            $parsedItems[] = [
                "id" => $itemId,
                "title" => "",
                "human_coding_scheme"   => $humanCodingScheme,
                "list_enumeration"      => $listEnumeration,
                "full_statement"        => $fullStatememnt,
                "node_type"             => $nodeType,
                "metadataType"          => $nodeType,
                "node_type_id"          => $nodeTypeId,
                "item_type"             => !empty($typeMetadataDet->metadata_id) ? $typeMetadataDet->pivot->metadata_value : "",
                "project_enabled"       => $projectEnabled,
                "project_name"          => $projectName,
                "is_document"           => 0,
                "parent_id"             =>  $parentAndDocumentId[$item->source_item_id]['parent_id'],
                "document_id"           =>  $parentAndDocumentId[$item->source_item_id]['document_id'],
                "document_title"        =>  !empty($sourceDocumentDetail->title) ? $sourceDocumentDetail->title : "",
                "source_item_id"        => $item->source_item_id
            ];

            
        }
        $this->setTransformedItems($parsedItems);
    }

    private function transformItemAssociations() {
        $itemAssociationsUnderDocument = $this->getItemAssociationsUnderDocument();
        
        $parsedItemAssociations = [];
        foreach($itemAssociationsUnderDocument as $itemAssociation) {
            $originNodeId = $itemAssociation->origin_node_id;
            $destinationNodeId = $itemAssociation->destination_node_id;
            if(!empty($originNodeId) && !empty($destinationNodeId)) {
                $parsedItemAssociations[] = [
                    "child_id" => $itemAssociation->origin_node_id,
                    "parent_id" => $itemAssociation->destination_node_id
                ];
            }
        }
        $this->setTransformedItemAssociationsUnderDocument($parsedItemAssociations);
    }

    /**
     * create the ui compatible tree response structure
     */
    private function buildTree () {
        $transformedDocument = $this->getTransformedDocument();
        $transformedItems = $this->getTransformedItems();
        $transformedItemAssociationsUnderDocument = $this->getTransformedItemAssociationsUnderDocument();
        $nodes = array_prepend($transformedItems, $transformedDocument);

        $treeData = [
            "nodes" => $nodes,
            "relations" => $transformedItemAssociationsUnderDocument,
        ];
        $this->setTree($treeData);
    }
    
    // private function helperToSearchAndReturnNodeTypeTitle(string $identifierToSearchFor): string {
    //     $nodeTypeTitleToReturn = "";
    //     $nodeTypes = $this->getNodeTypes();
    //     $searchResult = $nodeTypes->where("node_type_id", $identifierToSearchFor);
    //     if($searchResult->isNotEmpty()) {
    //         $nodeTypeTitleToReturn = $searchResult->first()->title;
    //     }
    //     return $nodeTypeTitleToReturn;
    // }

    // private function helperToReturnItemEditableFlagStatus(string $identifierToSearchFor, Collection $projectItemMapping): int {
    //     $statusToReturn = 2;
    //     if($projectItemMapping->isNotEmpty()) {
    //         $statusToReturn = 2;
    //         $searchResult = $projectItemMapping->where("item_id", $identifierToSearchFor);
    //         if($searchResult->isNotEmpty()) {
    //             $projectItem = $searchResult->first();
    //             if(isset($projectItem->is_editable)) {
    //                 $statusToReturn = $projectItem->is_editable;
    //             }
    //         }
    //     }
    //     return $statusToReturn;
    // }

    private function helperToReturnProjectEnabledStatus(string $identifierToSearchFor, array &$projectItemMapping): string {
        $statusToReturn = "0";
        if(!empty($projectItemMapping)) {
            foreach($projectItemMapping as $key=>$projectItemMap) {
                if($projectItemMap->item_id===$identifierToSearchFor && $projectItemMap->is_editable === 1) {
                    $statusToReturn = "1";
                    unset($projectItemMapping[$key]);
                    break;
                }
            }
        }
        return $statusToReturn;
    }

    // private function helperToReturnProjectNameForItemProvided(
    //     string $identifierToSearchFor, 
    //     Collection $projectItemMapping,
    //     Collection $projectsAssociated
    // ): string {
    //     $projectNameToReturn = "";
    //     if($projectItemMapping->isNotEmpty()) {
    //         $searchResult = $projectItemMapping->where("item_id", $identifierToSearchFor);
    //         if($searchResult->isNotEmpty()) {
    //             $projectIdentifier = $searchResult->project_id;
    //             $projectSearchResult = $projectsAssociated->where("project_id", $projectIdentifier);
    //             if($projectSearchResult->isNotEmpty()) {
    //                 $project = $projectSearchResult->first();
    //                 $projectNameToReturn = !empty($project->project_name) ? $project->project_name : "";
    //             }
    //         }
    //     }
    //     return $projectNameToReturn;
    // }
}
