<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Taxonomy\Validators\InputDataValidator;

class ValidateInputDataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(InputDataValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
