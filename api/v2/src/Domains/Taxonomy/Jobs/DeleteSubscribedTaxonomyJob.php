<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\VersionSubscription;

class DeleteSubscribedTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $documentDetails;
    private $requestData;

    public function __construct($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscriptionId = $this->subscriptionId;
        $subscriptionFetch = VersionSubscription::where([
            'subscription_id' => $subscriptionId
            ])->first();
        if(isset($subscriptionFetch->subscription_id)){
            VersionSubscription::where('subscription_id',$subscriptionId)
            ->delete();        
            return true;
        }
        else{
            return false;
        }
    }

}
