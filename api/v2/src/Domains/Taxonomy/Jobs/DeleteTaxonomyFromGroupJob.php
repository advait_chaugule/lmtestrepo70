<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class DeleteTaxonomyFromGroupJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $groupId;
    private $documentId;
    public function __construct($groupId,$documentIds)
    {
        $this->groupId = $groupId;
        $this->documentIds = $documentIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groupId = $this->groupId;
        $documentIds = $this->documentIds;
        
        return DB::table('group_document')
        ->select('document_id')
        ->where('group_id','=',$groupId)
        ->whereIn('document_id',$documentIds)->delete();
    }
}
