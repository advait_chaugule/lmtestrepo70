<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\TaxonomyRepositoryInterface;

class UpdateTaxonomyOrderJob extends Job
{
    private $data;
    private $targetId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($targetId, array $input)
    {
        //
        $this->targetId = $targetId;
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxonomyRepositoryInterface $taxonomyRepo)
    {
        //
        $i = 0;
        $countOrder = sizeOf($this->data); 
        while($countOrder > 0){
            $attributeValuePairs = ["order_id" => $i, "parent_id" => $this->targetId];$taxonomyRepo->editWithCustomizedFields(array('id' => $this->data[$i]), $attributeValuePairs);

            $countOrder--;
            $i++;
        }
        return 1;
    }
}
