<?php
namespace App\Domains\Taxonomy\Jobs;

ini_set('max_execution_time', 0); 

use Lucid\Foundation\Job;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class SetOrderedTaxonomyTreeForPacingGuideJob extends Job
{
    use ErrorMessageHelper, StringHelper, DateHelpersTrait, UuidHelperTrait; 

    private $documentIdentifier;
    private $documentRepository;
    private $itemRepository;
    private $itemAssociationRepository;
    private $userOrganizationId;
    private $inputData;
    private $savedDocumentObject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($documentIdentifier, $inputData)
    {
        //Set the private variables
        $this->setDocumentIdentifier($documentIdentifier);
        $this->inputData    =   $inputData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        MetadataRepositoryInterface $metadataRepository,
        Request $request
    )
    {
        //Set the repo handlers
        $this->documentRepository           =   $documentRepository;
        $this->itemRepository               =   $itemRepository;
        $this->itemAssociationRepository    =   $itemAssociationRepository;
        $this->metadataRepository           =   $metadataRepository;

        $this->extractAndSetUserOrganizationIdFromRequestBody($request);
        $this->extractAndSetUserIdFromRequestBody($request);

        $this->extractSaveItems($request);
    }

    /**
     * Public Getter and Setter methods
     */
    public function setDocumentIdentifier($identifier) {
        $this->documentIdentifier   =   $identifier;
    }

    public function getDocumentIdentifier(){
        return $this->documentIdentifier;
    }

    public function setUserOrganizationId(string $data) {
        $this->userOrganizationId = $data;
    }

    public function getUserOrganizationId(): string {
        return $this->userOrganizationId;
    }

    public function setUserId(string $data) {
        $this->userId = $data;
    }

    public function getUserId(): string {
        return $this->userId;
    }

    private function extractAndSetUserOrganizationIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userOrganizationId = $userPerformingAction["organization_id"];
        
        $this->setUserOrganizationId($userOrganizationId);
    }

    private function extractAndSetUserIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userId = $userPerformingAction["user_id"];
        $this->setUserId($userId);
    }

    private function extractSaveItems($request) {

        $itemCustomMetadataArray        =   [];
        $parentItemCustomMetadataArray  =   [];

        
        $items              =   $request->input('items');
        $old_items = [];
        if(null !== $request->input('old_items'))
        {
            $old_items          =   $request->input('old_items');
        }
        
        $documentId         =   $this->getDocumentIdentifier();
        $userId             =   $this->getUserId();
        $currentDateTime    =   $this->createDateTime();
        $userOrganizationId =   $this->getUserOrganizationId();

        foreach($items as $itemKey => $data) {
            $associationType                =   1;
            $item_id                        =   $data['item_id'];
            $parentId                       =   !empty($data["parent_id"]) ? $data["parent_id"] : $documentId;
            $listEnumeration                =   $data['list_enumeration'];
            $itemType                       =   !empty($data['item_type']) ? $data['item_type'] : '';
            $parentItemType                 =   !empty($data['parent_item_type']) ? $data['parent_item_type'] : '';
            $associationIdentifier          =   !empty($data['item_association_id']) ? $data['item_association_id']: '';
            
            if($parentId != $documentId) {
                if($itemType == 'standard' && $parentItemType == 'container'){
                    $associationType    =   3;
                }
            } else {
                if($itemType == 'standard'){
                    $associationType    =   3;
                }
            }

            $updatedAt = $currentDateTime;
            $attributes = [
                "list_enumeration"  =>  $listEnumeration,
                "updated_at"        =>  $updatedAt,
                "organization_id"   =>  $userOrganizationId,
                "updated_by"        =>  $userId,
            ];

            if($associationType != '3'){
                $attributes['parent_id']    =   $parentId;
            }

            $this->itemRepository->edit($item_id, $attributes);

            /** Get the destination taxonomy document id */
            $reverseOriginTaxonomy          =   $this->itemRepository->find($parentId);
            $reverseCfDocumentIdentifier    =   $reverseOriginTaxonomy['document_id'];

            /** Standard is created at root */
            if(empty($reverseCfDocumentIdentifier)) {
                $reverseCfDocumentIdentifier    =   $documentId;
            }

            if($associationType == 3) {
                $caseAssociationsToUpdate = [
                    "association_type"          => $associationType,
                    "document_id"               => $reverseCfDocumentIdentifier,
                    "destination_node_id"       => $parentId,
                    "destination_document_id"   => $documentId,
                    "sequence_number"           => $listEnumeration,
                    "updated_at"                => $updatedAt,
                    "is_reverse_association"    => 1,
                    "target_document_id"        =>  $documentId,
                    "target_item_id"            =>  $parentId,
                ];
            } else {
                $caseAssociationsToUpdate = [
                    "association_type"          => $associationType,
                    "document_id"               => $documentId,
                    "destination_node_id"       => $parentId,
                    "destination_document_id"   => $documentId,
                    "sequence_number"           => $listEnumeration,
                    "updated_at"                => $updatedAt,
                    "target_document_id"        =>  $documentId,
                    "target_item_id"            =>  $parentId,
                ];
            }

            $this->itemAssociationRepository->edit($associationIdentifier, $caseAssociationsToUpdate);
        }
    }
}
