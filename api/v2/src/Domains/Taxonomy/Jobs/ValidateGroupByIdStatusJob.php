<?php
namespace App\Domains\Taxonomy\Jobs;
use Lucid\Foundation\Job;
use App\Domains\Taxonomy\Validators\CheckGroupIdStatusValidator;

class ValidateGroupByIdStatusJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input){
        $this->input = $input;
    }

    public function handle(CheckGroupIdStatusValidator $validator){
       $validation = $validator->validate($this->input);
        return $validation === true ? $validation : $validation->errors()->all();
    }
}
