<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ValidateCreateDocumentJob extends Job
{
    public $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $validator)
    {
        $documentId = $this->input['documentId'];
        $dataToValidate = [ "document_id" => $documentId];
        $validation = $validator->findByAttributes($dataToValidate)->first();
       
        if(!empty($validation->document_id)){
            $validation = true;
        }
       
        return $validation===true ? $validation : "The selected source document id is invalid.";
    }
}
