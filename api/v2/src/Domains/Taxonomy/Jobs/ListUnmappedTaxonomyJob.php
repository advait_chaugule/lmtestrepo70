<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

class ListUnmappedTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $documentIdentifier)
    {   
        $this->documentIdentifier = $documentIdentifier;
        $this->setDocumentIdentifier($this->documentIdentifier);
    }

    //Public Getter and Setter methods
    public function setDocumentIdentifier(array $documentIdentifier){
        $this->documentIdentifier = $documentIdentifier;
    }

    public function getDocumentIdentifier(){
        return $this->documentIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeRepo)
    {
        $this->nodeRepo             = $nodeRepo;
        $documentIdentifier = $this->documentIdentifier;
        return $this->nodeRepo->getUnmappedTaxonomyList($documentIdentifier);
    }

}
