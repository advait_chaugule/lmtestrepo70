<?php
namespace App\Domains\Taxonomy\Jobs;
use Lucid\Foundation\Job;
use App\Domains\Taxonomy\Validators\CheckGroupNameValidator;

class ValidateGroupByNameJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    public function __construct(array $input){
        $this->input = $input;
    }

    public function handle(CheckGroupNameValidator $validator){
       $validation = $validator->validate($this->input);
        return $validation === true ? $validation : $validation->errors()->all();
        ;
    }
}
