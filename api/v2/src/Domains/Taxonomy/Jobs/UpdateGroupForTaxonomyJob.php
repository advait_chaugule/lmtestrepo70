<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Services\Api\Traits\UuidHelperTrait;

class UpdateGroupForTaxonomyJob extends Job
{
    use UuidHelperTrait;
    private $groupId;
    private $groupName;
    private $organizationId;
    private $userId;

    public function __construct($groupDetails=[])
    {
        $this->groupId = isset($groupDetails['group_id']) ? $groupDetails['group_id'] : '';
        $this->groupName = isset($groupDetails['name']) ? $groupDetails['name'] : '';
        $this->isUserDefault = isset($groupDetails['is_user_default']) ? $groupDetails['is_user_default'] : '';
        $this->isDefault = isset($groupDetails['is_default']) ? $groupDetails['is_default'] : '';
        $this->status = isset($groupDetails['status']) ? $groupDetails['status'] : '';        
        $this->userId = isset($groupDetails['user_id']) ? $groupDetails['user_id'] : '';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groupId = $this->groupId;
        if(empty($groupId))
            return [];
        
        $groupName = $this->groupName;
        $organizationId = $this->organizationId;
        $status = $this->status;
        $userId = $this->userId;
        $isUserDefault = $this->isUserDefault;
        $isDefault = $this->isDefault;
        $groupArray = [];
        if(!empty($this->groupName)) $groupArray["name"] = $this->groupName;
        if(!empty($this->status)) $groupArray["is_deleted"] = $this->status;
        if(!empty($this->isUserDefault)) $groupArray["is_user_default"] = $this->isUserDefault;
        if(isset($this->isDefault)) $groupArray["is_default"] = $this->isDefault;
        $date = date('Y-m-d H:i:s');
        if(!empty($groupArray)){
            $groupArray["updated_by"] = $userId;
            $groupArray["updated_at"] = $date;
            DB::table('group')->where('group_id','=',$groupId)->update($groupArray);
        }

        return DB::table('group')->select('group_id','name as group_name','is_deleted','is_user_default','is_default')->where('group_id','=',$groupId)->where('is_deleted','=','0')->get();
    }
}
