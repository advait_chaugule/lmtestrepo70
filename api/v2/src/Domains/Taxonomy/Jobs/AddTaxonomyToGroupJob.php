<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class AddTaxonomyToGroupJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $groupId;
    private $documentId;
    public function __construct($groupId,$documentIds)
    {
        $this->groupId = $groupId;
        $this->documentIds = $documentIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groupId = $this->groupId;
        $documentIds = $this->documentIds;
        
        $groupDocuments = DB::table('group_document')
        ->select('document_id')
        ->where('group_id','=',$groupId)
        ->whereIn('document_id',$documentIds)->get()->toArray();
        $groupDocuments = array_diff($documentIds,array_column($groupDocuments,'document_id'));
        $saveGrpDocuments = [];            
        if(!empty($groupDocuments)){
            foreach ($groupDocuments as $docKey => $document) {
                $saveGrpDocuments[$docKey]['group_id'] = $groupId;
                $saveGrpDocuments[$docKey]['document_id'] = $document;           
            }
            DB::table('group_document')->insert($saveGrpDocuments);
        }
        // If has records and group id passed, send specific records. Otherwise send all records or a blank array
        return $saveGrpDocuments;
    }
}
