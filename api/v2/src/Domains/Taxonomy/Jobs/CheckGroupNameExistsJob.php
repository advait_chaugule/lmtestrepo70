<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class CheckGroupNameExistsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $name;
    private $groupId;
    public function __construct($name,$groupId='',$organizationId)
    {
        $this->name = $name;
        $this->groupId = $groupId;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->name;
        $groupId = $this->groupId;
        $organizationId = $this->organizationId;
        $query = DB::table('group')
        ->select('name')
        ->where('group.name','=',$name)
        ->where('group.organization_id','=',$organizationId);       //to keep group name unique in tenant and not in application (Bug UF-3474).
        if(!empty($groupId))
            $query->where('group.group_id','!=',$groupId);
        $groupDetail = $query->where('group.is_deleted','=','0')->get();
        
        return $groupDetail->first() ? true : false;
    }
}