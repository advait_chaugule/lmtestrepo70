<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;

class GetTaxonomyNodesV2Job extends Job
{
    use ArrayHelper;

    private $itemRepository;
    private $items;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->requestUserDetails = $requestUserDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository)
    {
        //
        $this->itemRepository = $itemRepository;
        $documentIdentifier   =   $this->documentIdentifier;
        $input                =   $this->requestUserDetails;
       
        $organizationId             =   $input["organization_id"];
        $returnCollection           =   true;
        $fieldsToReturn             =   ["item_id"];
        $relations                  =   "";
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];
        
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $parsedItems = [];
        foreach($items as $item) {

            array_push($parsedItems,$item->item_id);
        }
        return $parsedItems;
    }
}
