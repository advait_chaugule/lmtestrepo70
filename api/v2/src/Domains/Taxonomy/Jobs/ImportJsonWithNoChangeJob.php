<?php
namespace App\Domains\Taxonomy\Jobs;
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Data\Models\Document;
use App\Data\Models\NodeType;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\StringHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;
use App\Data\Models\Organization;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use Illuminate\Support\Facades\Log;

class ImportJsonWithNoChangeJob extends Job
{
    use ArrayHelper, UuidHelperTrait, DateHelpersTrait, CaseFrameworkTrait, StringHelper;

    public $recordChunkCount = 1000;

    private $requestImportType;
    private $packageExists;
    private $packageData;
    private $oldNewItemMapping;
    private $requestingUser;
    private $requestingUserOrganizationId;

    private $documentRepository;
    private $itemRepository;
    private $languageRepository;
    private $licenseRepository;
    private $subjectRepository;
    private $nodeTypeRepository;
    private $conceptRepository;
    private $itemAssociationRepository;
    private $associationGroupRepository;

    private $caseDocumentToSave;
    private $caseItemsToSave;
    private $caseAssociationsToSave;
    private $childOfCaseAssociations;
    private $caseConceptsToSave;
    private $caseSubjectsToSave;
    private $caseLicensesToSave;
    private $caseItemTypesToSave;
    private $caseAssociationGroupsToSave;

    private $savedConcepts;
    private $savedSubjects;
    private $savedLicenses;
    private $savedNodeTypes;
    private $savedAssociationGroups;
    private $savedLanguages;
    private $destinationNodeItems;
    private $reverseDestinationNodeItems;
    private $originNodeItems;

    private $defaultItemNodeType;
    private $defaultDocumentNodeType;
    private $defaultItemNodeTypeMetadataIds;

    private $beforeSaveChildOfCaseAssociations;

    private $savedDocument;
    private $savedItems;
    private $itemSourceIdentifier;

    private $clonedAssociatonsToSave;
    private $organizationCode;

    private $conceptMappingData;

    private $newCreatedNodeType;
    private $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $packageDataArray, array $requestingUserDetails, int $requestImportType, int $packageExists,$requestUrl='',int $sourceType=2)
    {
        $this->packageExists = $packageExists;
        $this->requestImportType = $requestImportType;
        $this->packageData = $packageDataArray["packageData"];
        $this->requestingUser = $requestingUserDetails;
        $this->requestingUserOrganization = $requestingUserDetails["organization_id"];
        $this->organizationCode = Organization::select('org_code')->where('organization_id', $requestingUserDetails["organization_id"])->first()->org_code;
        $this->domainName = $requestUrl;
        $this->sourceType = $sourceType;        // source_type= 2 by default for JSON and Source_type =3 for API

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ConceptRepositoryInterface $conceptRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssociationGroupRepositoryInterface $associationGroupRepository
    )
    {
        // set the db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->subjectRepository = $subjectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->conceptRepository = $conceptRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->associationGroupRepository = $associationGroupRepository;

        // extract all relevant data to be saved from case package 
        $start_time1 = microtime(true);
        $this->extractAndSetCaseDocumentToSave();
        Log::error('i00001:Execution time of script:'.(microtime(true) - $start_time1));
        $start_time2 = microtime(true);
        $this->extractAndSetCaseItemsToSave();
        Log::error('i00002:Execution time of script:'.(microtime(true) - $start_time2));
        $start_time3 = microtime(true);
        $this->extractAndSetCaseAssociationsToSave();
        Log::error('i00003:Execution time of script:'.(microtime(true) - $start_time3));
        $start_time4 = microtime(true);
        $this->extractAndSetChildOfCaseAssociations();
        Log::error('i00004:Execution time of script:'.(microtime(true) - $start_time4));
        $start_time5 = microtime(true);
        $this->extractAndSetCaseConceptsToSave();
        Log::error('i00005:Execution time of script:'.(microtime(true) - $start_time5));
        $start_time6 = microtime(true);
        $this->extractAndSetCaseSubjectsToSave();
        Log::error('i00006:Execution time of script:'.(microtime(true) - $start_time6));
        $start_time7 = microtime(true);
        $this->extractAndSetCaseLicensesToSave();
        Log::error('i00007:Execution time of script:'.(microtime(true) - $start_time7));
        $start_time8 = microtime(true);
        $this->extractAndSetCaseItemTypesToSave();
        Log::error('i00008:Execution time of script:'.(microtime(true) - $start_time8));
        $start_time9 = microtime(true);
        $this->extractAndSetCaseAssociationGroupsToSave();
        Log::error('i00009:Execution time of script:'.(microtime(true) - $start_time9));
        
        // fetch from db and set saved data relevant to case
        $start_time10 = microtime(true);
        $this->fetchAndSetSavedConcepts();
        Log::error('i000010:Execution time of script:'.(microtime(true) - $start_time10));
        $start_time11 = microtime(true);
        $this->fetchAndSetSavedSubjects();
        Log::error('i000011:Execution time of script:'.(microtime(true) - $start_time11));
        $start_time12 = microtime(true);
        $this->fetchAndSetSavedLicenses();
        Log::error('i000012:Execution time of script:'.(microtime(true) - $start_time12));
        $start_time13 = microtime(true);
        $this->fetchAndSetSavedNodeTypes();
        Log::error('i000013:Execution time of script:'.(microtime(true) - $start_time13));
        $start_time14 = microtime(true);
        $this->fetchAndSetSavedAssociationGroups();
        Log::error('i000014:Execution time of script:'.(microtime(true) - $start_time14));
        $start_time15 = microtime(true);
        $this->fetchAndSetSavedLanguages();
        Log::error('i000015:Execution time of script:'.(microtime(true) - $start_time15));
        $start_time16 = microtime(true);
        // $this->fetchAndSetDestinationNodeItems();
        Log::error('i000016:Execution time of script:'.(microtime(true) - $start_time16));
        $start_time17 = microtime(true);
        // $this->fetchAndSetOriginNodeItems();
        Log::error('i000017:Execution time of script:'.(microtime(true) - $start_time17));
        $start_time18 = microtime(true);
        $this->fetchAndSetReverseDestinationNodeItems();
        Log::error('i000018:Execution time of script:'.(microtime(true) - $start_time18));
        
        // search for and set default item and document node types
        $start_time19 = microtime(true);
        $this->searchForAndSetDefaultItemNodeType();
        Log::error('i000019:Execution time of script:'.(microtime(true) - $start_time19));
        $start_time20 = microtime(true);
        $this->searchForAndSetDefaultDocumentNodeType();
        Log::error('i000020:Execution time of script:'.(microtime(true) - $start_time20));
        
        // fetch from db and set saved metadata Ids for item default node type
        $start_time21 = microtime(true);
        $this->fetchAndSetDefaultItemNodeTypeMetadataIds();
        Log::error('i000021:Execution time of script:'.(microtime(true) - $start_time21));
        
        // Open transaction block, process each case entities indivdually and save them to db
            $start_time22 = microtime(true);
            $this->processAndSaveCaseSubjectsData();
            Log::error('i000022:Execution time of script:'.(microtime(true) - $start_time22));
            $start_time23 = microtime(true);
            $this->processAndSaveCaseLicensesData();
            Log::error('i000023:Execution time of script:'.(microtime(true) - $start_time23));
            $start_time24 = microtime(true);
            $this->processAndSaveCaseConceptsData();
            Log::error('i000024:Execution time of script:'.(microtime(true) - $start_time24));
            $start_time25 = microtime(true);
            $this->processAndSaveCaseAssociationGroupingsData();
            Log::error('i000025:Execution time of script:'.(microtime(true) - $start_time25));
            $start_time26 = microtime(true);
            $this->processAndSaveCaseItemTypesData();
            Log::error('i000026:Execution time of script:'.(microtime(true) - $start_time26));
            $start_time27 = microtime(true);
            $this->processAndSaveCaseDocumentData();
            Log::error('i000027:Execution time of script:'.(microtime(true) - $start_time27));
            $start_time28 = microtime(true);
            $this->processAndSaveDocumentSubjects();
            Log::error('i000028:Execution time of script:'.(microtime(true) - $start_time28));
            $start_time29 = microtime(true);
            $this->processAndSaveCaseItemsData();
            Log::error('i000029:Execution time of script:'.(microtime(true) - $start_time29));
            $start_time30 = microtime(true);
            $this->processAndSaveCaseAssociationsData();
            Log::error('i000030:Execution time of script:'.(microtime(true) - $start_time30));

        $start_time31 = microtime(true);
        $this->raiseEventToUploadTaxonomySearchDataToSqs();
        Log::error('i000031:Execution time of script:'.(microtime(true) - $start_time31));

        $savedDocument = $this->savedDocument;
        $documentId = $savedDocument->document_id;
        $documentName = $savedDocument->title;
        $documentDetail = ["document_id" => $documentId , "document_title" => $documentName,'items'=>$this->savedItems,'newlyCreatedNode'=> !empty($this->newCreatedNodeType)?$this->newCreatedNodeType:'', 'import_type'=>$this->requestImportType];
        return $documentDetail;
    }

    /************** All extractors to extract and set case data to be saved **************/

    private function extractAndSetCaseDocumentToSave() {
        $packageData = $this->packageData;
        $caseDocumentToSave = $packageData["CFDocument"];
        $this->caseDocumentToSave = $caseDocumentToSave;
        unset($packageData);
    }

    private function extractAndSetCaseItemsToSave() {
        $packageData = $this->packageData;
        $caseItemsToSave = !empty($packageData["CFItems"]) ? $packageData["CFItems"] : [];
        $this->caseItemsToSave = $caseItemsToSave;
        unset($packageData);
    }

    private function extractAndSetCaseAssociationsToSave() {
        $packageData = $this->packageData;
        $caseAssociationsToSave = !empty($packageData["CFAssociations"]) ? $packageData["CFAssociations"] : [];
        $this->caseAssociationsToSave = $caseAssociationsToSave;
        unset($packageData);
    }

    private function extractAndSetChildOfCaseAssociations() {
        $associationsTosave = $this->caseAssociationsToSave;
        $data = [];
        foreach($associationsTosave as $associationToSave) {
            if($associationToSave["associationType"]==="isChildOf" && !empty($associationToSave["originNodeURI"]["identifier"])) {
                $data[] = $associationToSave;
            }
        }
        $this->childOfCaseAssociations = $data;
        unset($associationsTosave);
    }

    private function extractAndSetCaseConceptsToSave() {
        $packageData = $this->packageData;
        $caseConceptsToSave = !empty($packageData['CFDefinitions']["CFConcepts"]) ? $packageData['CFDefinitions']["CFConcepts"] : [];
        $this->caseConceptsToSave = $caseConceptsToSave;
        unset($packageData);
    }

    private function extractAndSetCaseSubjectsToSave() {
        $packageData = $this->packageData;
        $caseSubjectsToSave = !empty($packageData['CFDefinitions']["CFSubjects"]) ? $packageData['CFDefinitions']["CFSubjects"] : [];
        $this->caseSubjectsToSave = $caseSubjectsToSave;
        unset($packageData);
    }

    private function extractAndSetCaseLicensesToSave() {
        $packageData = $this->packageData;
        $caseLicensesToSave = !empty($packageData["CFDefinitions"]["CFLicenses"]) ? $packageData["CFDefinitions"]["CFLicenses"] : [];
        $this->caseLicensesToSave = $caseLicensesToSave;
        unset($packageData);
    }

    private function extractAndSetCaseItemTypesToSave() {
        $packageData = $this->packageData;
        $caseItemTypesToSave = !empty($packageData["CFDefinitions"]["CFItemTypes"]) ? $packageData["CFDefinitions"]["CFItemTypes"] : [];
        $this->caseItemTypesToSave = $caseItemTypesToSave;
        unset($packageData);
    }

    private function extractAndSetCaseAssociationGroupsToSave() {
        $packageData = $this->packageData;
        $caseAssociationGroupsToSave = !empty($packageData["CFDefinitions"]["CFAssociationGroupings"]) ? 
                                        $packageData["CFDefinitions"]["CFAssociationGroupings"] : 
                                        [];
        $this->caseAssociationGroupsToSave = $caseAssociationGroupsToSave;
        unset($packageData);
    }

    /************** All methods to fetch and set case data which is already saved inside our database **************/

    private function fetchAndSetSavedConcepts() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("concept");
        $this->savedConcepts = $savedData;
    }

    private function fetchAndSetSavedSubjects() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("subject");
        $this->savedSubjects = $savedData;
    }

    private function fetchAndSetSavedLicenses() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("license");
        $this->savedLicenses = $savedData;
    }

    private function fetchAndSetSavedNodeTypes() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("node");
        $this->savedNodeTypes = $savedData;
    }

    private function fetchAndSetSavedAssociationGroups() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("associationGroup");
        $this->savedAssociationGroups = $savedData;
    }

    private function fetchAndSetSavedLanguages() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("language");
        $this->savedLanguages = $savedData;
    }

    private function fetchAndSetDestinationNodeItems() {
        $destinationNodeIds = $this->helperToReturnDestinationNodeIds();
        $attributeIn = "source_item_id";
        $organizationId =   $this->requestingUserOrganization;
        $whereClauseAttributes = ['organization_id' => $organizationId];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $destinationNodeIds, $whereClauseAttributes);
        $dataToSet = $items->isNotEmpty() ? $items : collect([]);
        $this->destinationNodeItems = $dataToSet;
    }

    private function fetchAndSetOriginNodeItems() {
        $originNodeIds = $this->helperToReturnOriginNodeIds();
        $attributeIn = "source_item_id";
        $organizationId =   $this->requestingUserOrganization;
        $whereClauseAttributes = ['organization_id' => $organizationId];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $originNodeIds, $whereClauseAttributes);
        $dataToSet = $items->isNotEmpty() ? $items : collect([]);
        $this->originNodeItems = $dataToSet;
    }

    private function fetchAndSetReverseDestinationNodeItems() {
        $organizationId             = $this->requestingUserOrganization;
        $reverseDestinationNodeIds  = $this->helperToReturnReverseDestinationNodeIds();

        $attributeIn            = "source_item_id";
        $whereClauseAttributes  = ['organization_id' => $organizationId];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $reverseDestinationNodeIds, $whereClauseAttributes);
        $dataToSet = $items->isNotEmpty() ? $items : collect([]);

        $this->reverseDestinationNodeItems = $dataToSet;
    }

    /************** Methods to search and set default node types for item and document **************/

    private function searchForAndSetDefaultItemNodeType() {
        $savedNodeTypes = $this->savedNodeTypes;
        $defaultItemNodeType = $savedNodeTypes->where("is_default", 1)->first();
        $this->defaultItemNodeType = $defaultItemNodeType;
    }

    private function searchForAndSetDefaultDocumentNodeType() {
        $savedNodeTypes = $this->savedNodeTypes;
        $defaultDocumentNodeType = $savedNodeTypes->where("is_document", 1)->first();
        $this->defaultDocumentNodeType = $defaultDocumentNodeType;
    }

    /************** Methods to fetch and set default node type's metadata ids for item only**************/

    private function fetchAndSetDefaultItemNodeTypeMetadataIds() {
        $defaultItemNodeType = $this->defaultItemNodeType;
        // fetch active metadata only
        $defaultItemNodeTypeMetadataCsv = $defaultItemNodeType->metadata()
                                                              ->select(["metadata.metadata_id"])->where("is_active", 1)->get()
                                                              ->implode('metadata_id', ',');
        $defaultItemNodeTypeMetadataArray = $this->createArrayFromCommaeSeparatedString($defaultItemNodeTypeMetadataCsv);
        $this->defaultItemNodeTypeMetadataIds = $defaultItemNodeTypeMetadataArray;
    }

    /************** Methods to process and save each entities of case package **************/

    private function processAndSaveCaseSubjectsData() {
        $requestImportType      =   $this->requestImportType;
        $subjectsToSave         =   $this->caseSubjectsToSave;
        $subjectsAlreadySaved   =   $this->savedSubjects;
        if(!empty($subjectsToSave)) {
            $organizationId     =   $this->requestingUserOrganization;
            $newSubjectsToSave  =   [];
            foreach($subjectsToSave as $subjectToSave) {
                if(!empty($subjectToSave["title"])){
                    $subjectTitleToSearchWith = $subjectToSave["title"];
                    $savedSubjectCollection = $subjectsAlreadySaved->where("title", $subjectTitleToSearchWith);

                    // if subject doesn't exists in the db then only save it and update the list of saved subjects
                    if($savedSubjectCollection->isEmpty()) {
                        $newSubjectIdentifier       =   $this->createUniversalUniqueIdentifier();
                        $subjectToSaveIdentifier    =   ($requestImportType == 2)? $newSubjectIdentifier : $subjectToSave["identifier"];
                        $newSubjectTitle            =   $subjectTitleToSearchWith;
                        $newSubjectHierarchyCode    =   !empty($subjectToSave["hierarchyCode"]) ? $subjectToSave["hierarchyCode"] : "";
                        $newSubjectDescription      =   !empty($subjectToSave["description"]) ? $subjectToSave["description"] : "";
                        $lastChangeDateTime         =   !empty($subjectToSave["lastChangeDateTime"]) ? $this->formatDateTime($subjectToSave["lastChangeDateTime"]) : null;

                        /* if($requestImportType == 2) {
                            $subjectToSave['uri'] = str_replace($subjectToSave['identifier'],$newSubjectIdentifier, $subjectToSave['uri']);  
                        } */
                        
                        $uriToSave                  =   $subjectToSave['uri'] ;
                        
                        $newSubject = [
                            "subject_id"        => $newSubjectIdentifier, 
                            "organization_id"   => $organizationId,
                            "title"             => $newSubjectTitle,
                            "hierarchy_code"    => $newSubjectHierarchyCode,
                            "description"       => $newSubjectDescription,
                            "updated_at"        => $lastChangeDateTime,
                            "source_subject_id" => $subjectToSaveIdentifier, 
                            "uri"               => $uriToSave
                        ];

                        $newSubjectsToSave[] = $newSubject;
                        $newSubjectSaved = $this->subjectRepository->createModelInstanceInMemory($newSubject);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $subjectsAlreadySaved->push($newSubjectSaved);
                    }
                }
            }
            
            /** 1702 Subject DB query insert */
            $newSubjectsToSaveArry = array_chunk($newSubjectsToSave,10);
            foreach($newSubjectsToSaveArry as $newSubjectsToSaveArryK => $newSubjectsToSaveArryV)
            {
                DB::table('subjects')->insert($newSubjectsToSaveArryV);
            }
            //$this->subjectRepository->saveMultiple($newSubjectsToSave);
        }
    }

    private function processAndSaveCaseLicensesData() {
        $requestImportType      =   $this->requestImportType;
        $licensesToSave         =   $this->caseLicensesToSave;
        $licensesAlreadySaved   =   $this->savedLicenses;

        if(!empty($licensesToSave)) {
            $organizationId     =   $this->requestingUserOrganization;
            $newLicensesToSave  =   [];

            foreach($licensesToSave as $licenseToSave) {
                if(!empty($licenseToSave["title"])){
                    $licenseTitleToSearchWith   =   $licenseToSave["title"];
                    $savedLicenseCollection     =   $licensesAlreadySaved->where("title", $licenseTitleToSearchWith);
                    // if license doesn't exists in the db then only save it and update the list of saved licenses
                    if($savedLicenseCollection->isEmpty()) {
                        $newLicenseIdentifier       =   $this->createUniversalUniqueIdentifier() ;
                        $licenseToSaveIdentifier    =   ($requestImportType == 2)? $newLicenseIdentifier : $licenseToSave["identifier"];
                        $newLicenseTitle            =   $licenseTitleToSearchWith;
                        $newLicenseDescription      =   !empty($licenseToSave["description"]) ? $licenseToSave["description"] : "";
                        $newLicenseText             =   !empty($licenseToSave["licenseText"]) ? $licenseToSave["licenseText"] : "";
                        $lastChangeDateTime         =   !empty($licenseToSave["lastChangeDateTime"]) ? $this->formatDateTime($licenseToSave["lastChangeDateTime"]) : null;

                        /* if($requestImportType == 2) {
                            $licenseToSave['uri'] = str_replace($licenseToSave['identifier'],$newLicenseIdentifier, $licenseToSave['uri']);  
                        }*/
                        $uriToSave                  =   $licenseToSave['uri'];
                        
                        $newLicense = [
                            "license_id"        => $newLicenseIdentifier, 
                            "organization_id"   => $organizationId,
                            "title"             => $newLicenseTitle,
                            "description"       => $newLicenseDescription,
                            "license_text"      => $newLicenseText,
                            "updated_at"        => $lastChangeDateTime,
                            "source_license_id" => $licenseToSaveIdentifier,
                            "uri"               => $uriToSave
                        ];

                        $newLicensesToSave[] = $newLicense;
                        $newLicenseSaved = $this->licenseRepository->createModelInstanceInMemory($newLicense);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $licensesAlreadySaved->push($newLicenseSaved);
                    }
                }
            }
            if(!empty($newLicensesToSave)) {
                $newLicensesToSaveArry = array_chunk($newLicensesToSave,1000);
                foreach($newLicensesToSaveArry as $newLicensesToSaveK => $newLicensesToSaveV)
                {
                    DB::table('licenses')->insert($newLicensesToSaveV);
                }
                // $this->licenseRepository->saveMultiple($newLicensesToSave);
            }
            
        }
    }

    private function processAndSaveCaseConceptsData() {
        $requestImportType      =   $this->requestImportType;
        $conceptsToSave         =   $this->caseConceptsToSave;
        $conceptsAlreadySaved   =   $this->savedConcepts;

        if(!empty($conceptsToSave)) {
            $organizationId     =   $this->requestingUserOrganization;
            $newConceptsToSave  =   [];

            foreach($conceptsToSave as $conceptToSave) {
                if(!empty($conceptToSave["title"])){
                    $conceptTitleToSearchWith   =   $conceptToSave["title"];
                    $savedConceptCollection     =   $conceptsAlreadySaved->where("title", $conceptTitleToSearchWith);
                    // if concept doesn't exists in the db then only save it and update the list of saved concepts
                    if($savedConceptCollection->isEmpty()) {
                        $newConceptIdentifier       =   $this->createUniversalUniqueIdentifier() ;
                        $conceptToSaveIdentifier    =   ($requestImportType == 2) ? $newConceptIdentifier : $conceptToSave["identifier"];
                        $newConceptKeywords         =   !empty($conceptToSave["keywords"]) ? $conceptToSave["keywords"] : "";
                        $newConceptTitle            =   $conceptTitleToSearchWith;
                        $newConceptHierarchyCode    =   !empty($conceptToSave["hierarchyCode"]) ? $conceptToSave["hierarchyCode"] : "";
                        $newConceptDescription      =   !empty($conceptToSave["description"]) ? $conceptToSave["description"] : "";
                        $lastChangeDateTime         =   !empty($conceptToSave["lastChangeDateTime"]) ? $this->formatDateTime($conceptToSave["lastChangeDateTime"]) : null;

                        /* if($requestImportType == 2) {
                            $conceptToSave['uri'] = str_replace($conceptToSave['identifier'],$newConceptIdentifier, $conceptToSave['uri']);  
                        } */
                        $uriToSave                  =   $conceptToSave["uri"];

                        $newConcept = [
                            "concept_id"        => $newConceptIdentifier, 
                            "organization_id"   => $organizationId,
                            "keywords"          => $newConceptKeywords,
                            "title"             => $newConceptTitle,
                            "description"       => $newConceptDescription,
                            "hierarchy_code"    => $newConceptHierarchyCode,
                            "updated_at"        => $lastChangeDateTime,
                            "source_concept_id" => $conceptToSaveIdentifier,
                            "uri"               => $uriToSave 
                        ];

                        $newConceptsToSave[] = $newConcept;
                        $newConceptSaved = $this->conceptRepository->createModelInstanceInMemory($newConcept);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $conceptsAlreadySaved->push($newConceptSaved);
                    }
                }
            }
            
            if(!empty($newConceptsToSave)) {
                $newConceptsToSaveArry = array_chunk($newConceptsToSave,1000);
                foreach($newConceptsToSaveArry as $newConceptsToSaveK => $newConceptsToSaveV)
                {
                    DB::table('concepts')->insert($newConceptsToSaveV);
                }
                //$this->conceptRepository->saveMultiple($newConceptsToSave);
            }
            
        }
    }
    
    private function processAndSaveCaseAssociationGroupingsData() {
        $requestImportType                  =   $this->requestImportType;
        $associationGroupingsToSave         =   $this->caseAssociationGroupsToSave;
        $associationGroupingsAlreadySaved   =   $this->savedAssociationGroups;

        if(!empty($associationGroupingsToSave)) {
            $organizationId                 =   $this->requestingUserOrganization;
            $newAssociationGroupingsToSave  =   [];

            foreach($associationGroupingsToSave as $associationGroupingToSave) {
                if(!empty($associationGroupingToSave["title"])){
                    $associationGroupingTitleToSearchWith   =   $associationGroupingToSave["title"];
                    $savedAssociationGroupingCollection     =   $associationGroupingsAlreadySaved->where("title", $associationGroupingTitleToSearchWith);

                    // if association group doesn't exists in the db then only save it and update the list of saved association groups
                    if($savedAssociationGroupingCollection->isEmpty()) {
                        $newAssociationGroupingIdentifier       =   $this->createUniversalUniqueIdentifier() ;

                        $associationGroupingToSaveIdentifier    = ($requestImportType == 2)? $newAssociationGroupingIdentifier : $associationGroupingToSave["identifier"];
                        $newAssociationGroupingTitle            =   $associationGroupingTitleToSearchWith;
                        $newAssociationGroupingDescription      =   !empty($associationGroupingToSave["description"]) ?$associationGroupingToSave["description"] : "";
                        $lastChangeDateTime                     =   !empty($associationGroupingToSave["lastChangeDateTime"]) ? $this->formatDateTime($associationGroupingToSave["lastChangeDateTime"]) : null;

                        /* if($requestImportType == 2) {
                            $associationGroupingToSave['uri'] = str_replace($associationGroupingToSave['identifier'],$newAssociationGroupingIdentifier, $associationGroupingToSave['uri']);  
                        } */

                        $uriToSave                               =   $associationGroupingToSave['uri'];

                        $newAssociationGrouping = [
                            "association_group_id"          =>  $newAssociationGroupingIdentifier, 
                            "organization_id"               =>  $organizationId,
                            "title"                         =>  $newAssociationGroupingTitle,
                            "description"                   =>  $newAssociationGroupingDescription,
                            "updated_at"                    =>  $lastChangeDateTime,
                            "source_association_group_id"   =>  $associationGroupingToSaveIdentifier,
                            "uri"                           =>  $uriToSave
                        ];

                        $newAssociationGroupingsToSave[]    =   $newAssociationGrouping;
                        $newAssociationGroupingSaved        =   $this->associationGroupRepository->createModelInstanceInMemory($newAssociationGrouping);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $associationGroupingsAlreadySaved->push($newAssociationGroupingSaved);
                    }
                }
            }
            if(!empty($newAssociationGroupingsToSave)) {
                $this->associationGroupRepository->saveMultiple($newAssociationGroupingsToSave);
            }
            
        }
    }

    private function processAndSaveCaseItemTypesData() {
        $requestImportType      =   $this->requestImportType;
        $itemTypesToSave        =   $this->caseItemTypesToSave;
        $itemTypesAlreadySaved  =   $this->savedNodeTypes;
        $requestUserDetails     =   $this->requestingUser;
        $requestUserId          =   $requestUserDetails["user_id"];
        $currentDateTime        =   now()->ToDateTimeString();
        
        if(!empty($itemTypesAlreadySaved)) {
            $organizationId                     =   $this->requestingUserOrganization;
            $defaultItemNodeTypeMetadataIds     =   $this->defaultItemNodeTypeMetadataIds;
            $newItemTypesToSave                 =   [];
            $newItemTypeInstancesToMapMetadata  =   [];

            foreach($itemTypesToSave as $itemTypeToSave) {
                if(!empty($itemTypeToSave["title"])){
                    $itemTypeTitleToSearchWith  =   $itemTypeToSave["title"];
                    $itemTypeCodeToSearchWith   =   !empty($itemTypeToSave["typeCode"]) ? $itemTypeToSave["typeCode"] : "";
                    $savedItemTypeCollection    =   $itemTypesAlreadySaved->where("title", $itemTypeTitleToSearchWith);

                    if($savedItemTypeCollection->isEmpty()){
                        
                        $newItemTypeIdentifier      =   $this->createUniversalUniqueIdentifier() ;
                        $itemTypeToSaveIdentifier   =   ($requestImportType == 2) ? $newItemTypeIdentifier : $itemTypeToSave["identifier"];
                        $newItemTypeTitle           =   $itemTypeTitleToSearchWith;
                        $newItemTypeCode            =   $itemTypeCodeToSearchWith;
                        $newItemTypeDescription     =   !empty($itemTypeToSave["description"]) ? $itemTypeToSave["description"] : "";
                        $newItemTypeHierarchyCode   =   !empty($itemTypeToSave["hierarchyCode"]) ? $itemTypeToSave["hierarchyCode"] : "";
                        $lastChangeDateTime         =   !empty($itemTypeToSave["lastChangeDateTime"]) ? $this->formatDateTime($itemTypeToSave["lastChangeDateTime"]) : null;

                        $uriToSave                  =   $itemTypeToSave['uri'];

                        $newItemType = [
                            "node_type_id"          => $newItemTypeIdentifier, 
                            "title"                 => $newItemTypeTitle,
                            "organization_id"       => $organizationId,
                            "description"           => $newItemTypeDescription,
                            "hierarchy_code"        => $newItemTypeHierarchyCode,
                            "type_code"             => $newItemTypeCode,
                            "updated_at"            => $lastChangeDateTime,
                            "source_node_type_id"   => $itemTypeToSaveIdentifier,
                            "uri"                   => $uriToSave,
                            "created_by"            => $requestUserId,
                            "created_at"            => $currentDateTime,
                            "updated_at"            => $currentDateTime
                        ];

                        $newItemTypesToSave[]                   =   $newItemType;
                        $newItemTypeSaved                       =   $this->nodeTypeRepository->createModelInstanceInMemory($newItemType);
                        $newItemTypeInstancesToMapMetadata[]    =   $newItemTypeSaved;
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $itemTypesAlreadySaved->push($newItemTypeSaved);
                    }

                }
            }
            // convert to collection to insert records as chunk
            $recordsToSaveCollection = collect($newItemTypesToSave);
            foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
                $this->nodeTypeRepository->saveMultiple($chunk->toArray());
            }

            // save the new nodetype mapping with default item node metatdata ids
            foreach($newItemTypeInstancesToMapMetadata as $newItemType) {
                $newItemType->metadata()->attach($defaultItemNodeTypeMetadataIds);
            }

            $this->newCreatedNodeType = $newItemTypesToSave;
        }
    }

    private function processAndSaveCaseDocumentData() {
        $packageExists              =   $this->packageExists;
        $requestImportType          =   $this->requestImportType;
        $documentToSave             =   $this->caseDocumentToSave;
        $defaultDocumentNodeType    =   $this->defaultDocumentNodeType;
        $requestUser                =   $this->requestingUser;
        $currentDateTime            =   now()->toDateTimeString();
        $sourceType                 =   $this->sourceType;
        $sourceLicenseUriObject     = '';
        
        if($packageExists != 0) {
            $title = !empty($documentToSave["title"]) ? (($requestImportType == 2) ? ($documentToSave["title"]." ".$currentDateTime) : $documentToSave["title"]) : $currentDateTime;
        } else {
            $title = !empty($documentToSave["title"]) ? $documentToSave["title"] : $currentDateTime;
        }
        

        //$title                  =   $documentToSave['title'];
        $organizationId         =   $this->requestingUserOrganization;
        $organizationName       =   DB::table('organizations')->select('name')->where('organization_id', $organizationId)->first()->name;
        $newDocumentIdentifier  =   $this->createUniversalUniqueIdentifier();
        $sourceDocumentId       =   ($requestImportType != 2) ? $documentToSave["identifier"]: $newDocumentIdentifier;
        
        $officialSourceUrl      =   !empty($documentToSave["officialSourceURL"]) ? $documentToSave["officialSourceURL"] : "";
        // if no adoption status is set, then insert 1 (DRAFT)
        $adoptionStatus         =   !empty($documentToSave["adoptionStatus"]) ? $this->getSystemSpecifiedAdoptionStatusNumber($documentToSave["adoptionStatus"]) : 1;
        $notes                  =   !empty($documentToSave["notes"]) ? $documentToSave["notes"] : "";
        $publisher              =   !empty($documentToSave["publisher"]) ? $documentToSave["publisher"] : "";
        $description            =   !empty($documentToSave["description"]) ? $documentToSave["description"] : "";
        $version                =   !empty($documentToSave["version"]) ? $documentToSave["version"] : "";
        $statusStartDate        =   !empty($documentToSave["statusStartDate"]) ? $documentToSave["statusStartDate"] : null;
        $statusEndDate          =   !empty($documentToSave["statusEndDate"]) ? $documentToSave["statusEndDate"] : null;
        $creator                =   !empty($documentToSave["creator"]) ? $documentToSave["creator"] : $organizationName;
        $urlName                =   !empty($documentToSave["url_name"]) ? $documentToSave["url_name"] : "";
        $nodeTypeId             =   $defaultDocumentNodeType->node_type_id;
        $userId                 =   $requestUser["user_id"];
        // the above script enters the updated_at date from the json file and required date is of when the file was imported
        $updatedAt              =   $currentDateTime;                         
       
        $languageId             =  !empty($documentToSave["language"]) ? $this->helperToFetchLanguageIdFromEitherExistingOrNew($documentToSave["language"]) : ""; 
        $licenseId              =   !empty($documentToSave['licenseURI']['identifier']) ? $this->helperToFetchLicenseIdFromEitherExistingOrNew($documentToSave['licenseURI']) : ""; 

        /* if($requestImportType == 2) {
            $documentToSave['uri'] = str_replace($documentToSave['identifier'],$newDocumentIdentifier, $documentToSave['uri']);  
        } */
        
        $uriToSave              =  $documentToSave['uri'];  
        
        if($requestImportType == 2) {
            $sourceLicenseObject    = $this->helperToFetchSourceLicenseIdFromEitherExistingOrNew($licenseId);
            if(!empty($sourceLicenseObject)) {
                $sourceLicenseUriObject = json_encode(
                    [
                        "identifier"    =>  $sourceLicenseObject->source_license_id,
                        "uri"           =>  $sourceLicenseObject->uri,
                        "title"         =>  $sourceLicenseObject->title
                    ]
                );
            }
           
        }else {
            $sourceLicenseUriObject = !empty($documentToSave['licenseURI']) ? json_encode(
                $documentToSave['licenseURI']
            ) : "";
        }
        
                     
        $documentDataToSave = [
            "document_id"           =>  $newDocumentIdentifier,
            "title"                 =>  $title,
            "official_source_url"   =>  $officialSourceUrl,
            "adoption_status"       =>  $adoptionStatus,
            "notes"                 =>  $notes,
            "publisher"             =>  $publisher,
            "description"           =>  $description,
            "version"               =>  $version,
            "status_start_date"     =>  $statusStartDate,
            "status_end_date"       =>  $statusEndDate,
            "creator"               =>  $creator,
            "url_name"              =>  $urlName,
            "language_id"           =>  $languageId,
            "license_id"            =>  $licenseId,
            "source_license_uri_object"     =>  $sourceLicenseUriObject,
            "organization_id"       =>  $organizationId,
            "created_at"            =>  $currentDateTime,
            "updated_at"            =>  $updatedAt,
            "source_document_id"    =>  $sourceDocumentId,
            "node_type_id"          =>  $nodeTypeId,
            "import_type"           =>  $requestImportType,
            "actual_import_type"    =>  $requestImportType,     //for import type column view
            "updated_by"            =>  $userId,
            "uri"                   =>  $uriToSave,
            "created_by"            =>  $userId,
            "import_status"         => 1, // to indicate that import is in progress and to not show in taxonomy listing till import is completed
            "source_type"           =>  $sourceType,        //set sourceType = 2 for JSON and Source_type = 3 for API 
        ];

        $documentSaved = $this->documentRepository->saveData($documentDataToSave);
        $this->savedDocument = $documentSaved;

    }

    private function processAndSaveDocumentSubjects() {
        $documentToSave = $this->caseDocumentToSave;
        // save document subject mapping if provided
        if(!empty($documentToSave["subjectURI"])) {
            $savedDocument = $this->savedDocument;
            $subjectIdentifiersToMapDocumentWith = [];
            foreach($documentToSave["subjectURI"] as $subject) {
                $subjectIdentifiersToMapDocumentWith[] = $this->helperToFetchSubjectIdFromEitherExistingOrNew($subject);
                
            }
            $savedDocument->subjects()->attach($subjectIdentifiersToMapDocumentWith);
            //dd(json_encode($documentToSave["subjectURI"][0]));
            DB::table('document_subject')->where('document_id', $savedDocument['document_id'])->update(['source_subject_uri_object' => json_encode($documentToSave["subjectURI"][0])]);
        }
    }

    private function processAndSaveCaseItemsData() {
        $arrayOfSourceIdentifiers   =   [];
        $arrayOfExistingItemId      =   [];
        $orgCode                =   $this->organizationCode;
        $packageExists          =   $this->packageExists;
        $requestImportType      =   $this->requestImportType;
        $requestUser            =   $this->requestingUser;
        $userId                 =   $requestUser["user_id"];
        $organizationId         =   $this->requestingUserOrganization;
        $itemsToSave            =   $this->caseItemsToSave;
        $defaultItemNodeType    =   $this->defaultItemNodeType;
        $defaultItemNodeTypeUri =   json_encode(
                                    [
                                        "identifier"    =>  $defaultItemNodeType->source_node_type_id,
                                        "uri"           =>   $this->getCaseApiUri("CFItemTypes", $defaultItemNodeType->source_node_type_id, true, $orgCode,$this->domainName),
                                        "title"         =>  $defaultItemNodeType->title
                                    ] 
                                );
                                    
        $savedDocument          =   $this->savedDocument;
        $currentDateTime        =   now()->toDateTimeString();
        $documentId             =   $savedDocument->document_id;

        $sourceConceptUriObject     =   '';
        $sourceNodeTypeUriObject    =   '';
        $sourceLicenseUriObject     =   '';


        $dataToSave = [];
        $oldIdRelations = [];
        foreach($itemsToSave as $data) {
            
            $itemId                 =   $this->createUniversalUniqueIdentifier();
            $sourceItemId           =   ($requestImportType == 2) ? $itemId : $data["identifier"];
            $fullStatement          =   !empty($data["fullStatement"]) ? $data["fullStatement"] : "";
            $alternativeLabel       =   !empty($data["alternativeLabel"]) ? $data["alternativeLabel"] : "";
            $humanCodingScheme      =   !empty($data["humanCodingScheme"]) ? $data["humanCodingScheme"] : "";
            $listEnumeration        =   !empty($data["listEnumeration"]) ? $data["listEnumeration"] : "";
            $abbreviatedStatement   =   !empty($data["abbreviatedStatement"]) ? $data["abbreviatedStatement"] : "";
            $notes                  =   !empty($data["notes"]) ? $data["notes"] : "";
            
            $educationLevel         =   !empty($data["educationLevel"]) ? is_array($data["educationLevel"]) ? $this->arrayContentToCommaeSeparatedString($data["educationLevel"]) : $data["educationLevel"] : "";
            $statusStartDate        =   !empty($data["statusStartDate"]) ? $data["statusStartDate"] : null;
            $statusEndDate          =   !empty($data["statusEndDate"]) ? $data["statusEndDate"] : null;
            $updatedAt              =   !empty($data["lastChangeDateTime"]) ? $this->formatDateTime($data["lastChangeDateTime"]) : null;
            $languageId             =   !empty($data["language"]) ? $this->helperToFetchLanguageIdFromEitherExistingOrNew($data["language"]) : "";
            $licenseId              =   !empty($data['licenseURI']['identifier']) ? 
                         $this->helperToFetchLicenseIdFromEitherExistingOrNew($data['licenseURI']) : 
                         "";
            $conceptId              =   !empty($data["conceptKeywordsURI"]) ? 
                         $this->helperToFetchConceptIdFromEitherExistingOrNew($data["conceptKeywordsURI"]) : 
                         "";  

            $itemTypeId             =   !empty($data["CFItemTypeURI"]) ? 
                          $this->helperToFetchItemTypeIdFromEitherExistingOrNew($data["CFItemTypeURI"]) : 
                          $defaultItemNodeType->node_type_id;

            /* if($requestImportType == 2) {
                $data['uri'] = str_replace($data["identifier"], $itemId, $data['uri']);  
            } */

            $uriToSave              =   $data['uri'];

            $arrayOfSourceIdentifiers[$itemId] = $data['identifier'];

            
            if($requestImportType == 2) {
                $sourceLicenseObject    = $this->helperToFetchSourceLicenseIdFromEitherExistingOrNew($licenseId);
                
                if(!empty($sourceLicenseObject)){
                    $sourceLicenseUriObject = json_encode(
                        [
                            "identifier"    =>  $sourceLicenseObject->source_license_id,
                            "uri"           =>  $sourceLicenseObject->uri,
                            "title"         =>  $sourceLicenseObject->title
                        ]
                    );
                }
                

                $sourceNodeTypeObject        =  $this->helperToFetchSourceItemTypeIdFromEitherExistingOrNew($itemTypeId);
                if(!empty($sourceNodeTypeObject)) {
                    $sourceNodeTypeUriObject = json_encode(
                        [
                            "identifier"    =>  $sourceNodeTypeObject->source_node_type_id,
                            "uri"           =>  $sourceNodeTypeObject->uri,
                            "title"         =>  $sourceNodeTypeObject->title
                        ]
                    );
                }

                $sourceConceptObject        =  $this->helperToFetchSourceConceptIdFromEitherExistingOrNew($itemTypeId);
                if(!empty($sourceConceptObject)) {
                    $sourceCOnceptUriObject = json_encode(
                        [
                            "identifier"    =>  $sourceConceptObject->source_concept_id,
                            "uri"           =>  $sourceConceptObject->uri,
                            "title"         =>  $sourceConceptObject->keywords
                        ]
                    );
                }
                
            }else {
                $sourceLicenseUriObject = !empty($data['licenseURI']) ? json_encode(
                    $data['licenseURI']
                ) : "";

                $sourceNodeTypeUriObject        =  !empty($data['CFItemTypeURI']) ? json_encode($data['CFItemTypeURI']): $defaultItemNodeTypeUri;

                $sourceConceptUriObject        =  !empty($data['conceptKeywordsURI']) ? json_encode($data['conceptKeywordsURI']): "" ;
            }

            $itemToSave = [
                "item_id"               => $itemId,
                "parent_id"             => "",
                "document_id"           => $documentId,
                "full_statement"        => $fullStatement,
                "alternative_label"     => $alternativeLabel,
                "human_coding_scheme"   => $humanCodingScheme,
                "list_enumeration"      => $listEnumeration,
                "abbreviated_statement" => $abbreviatedStatement,
                "notes"                 => $notes,
                "node_type_id"          => $itemTypeId,
                "source_node_type_uri_object"   => $sourceNodeTypeUriObject,
                "concept_id"            => $conceptId,
                "source_concept_uri_object"     => $sourceConceptUriObject,
                "language_id"           => $languageId,
                "education_level"       => $educationLevel,
                "license_id"            => $licenseId,
                "source_license_uri_object"     => $sourceLicenseUriObject,
                "status_start_date"     => $statusStartDate,
                "status_end_date"       => $statusEndDate,
                "created_at"            => $currentDateTime,
                "updated_at"            => $updatedAt,
                "organization_id"       => $organizationId,
                "source_item_id"        => $sourceItemId,
                "updated_by"            => $userId,
                "uri"                   => $uriToSave 
            ];

            $dataToSave[$itemId] = $itemToSave;
            $dataToSave2[$sourceItemId] = $itemToSave;
            // $savedItems[] = $this->itemRepository->createModelInstanceInMemory($itemToSave);
            $oldIdRelations[$itemId]['id'] = $data["identifier"];
            $oldIdRelations[$itemId]['full_statement'] = $fullStatement;
            $oldIdRelations[$itemId]['uri'] = $data['uri'];
        }

        // set the saved items collection first
        $this->savedItems = $dataToSave;
        $this->itemSourceIdentifier = $arrayOfSourceIdentifiers;


        // search for parent id of each item to be saved and update the dataToSave array accordingly
        $documentSourceId = $this->caseDocumentToSave["identifier"];
        $updatedItemDataToSave = $this->helperToUpdateTheItemDataToSaveWithParentId($dataToSave, $documentSourceId, $documentId);
        $this->updatedItemDataToSave = $updatedItemDataToSave;
        $this->updatedItemDataToSave2 = $dataToSave2;

        // convert to collection to insert records as chunk
        /*
        $recordsToSaveCollection = collect($updatedItemDataToSave);
        foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
            $this->itemRepository->saveMultiple($chunk->toArray());
        }
        */
        $this->oldRelations =  $oldIdRelations;
    }


    private function processAndSaveCaseAssociationsData() {
        $updatedItemDataToSave = $this->updatedItemDataToSave;
        $updatedItemDataToSave2 = $this->updatedItemDataToSave2;
        $exactMatchOfAssociationDocumentId  =   '';
        $destinationNodeIdToSearchWith      =   '';
        $reverseAssociationTypeArray        =   ['3','5','7'];
        $oldRelationArr     = $this->oldRelations;
        $requestImportType  =   $this->requestImportType;
        $packageExists      =   $this->packageExists;
        $requestUser        =   $this->requestingUser;
        $userId             =   $requestUser["user_id"];
        $organizationId     =   $this->requestingUserOrganization;
        $currentDateTime    =   now()->toDateTimeString();
        $orgCode            =   $this->organizationCode;

        $caseAssociatonsFromJsonSave        =   $this->caseAssociationsToSave;
        $savedDocument                      =   $this->savedDocument;
        
        $documentId             =   $savedDocument->document_id;
        $caseAssociationsToSave =   [];
        // $itemIdSavedArray       =   [];
        $reverseDocumentId      =   '';

        $exactMatchOfAssociationDocumentId  =   $documentId;
        // $itemAttributes =   ['document_id' => $exactMatchOfAssociationDocumentId, "is_deleted" => '0'];
        // $itemExistData  =   $this->itemRepository->findByAttributes($itemAttributes);

        // foreach($itemExistData as $itemSaved) {
        //     $itemIdSavedArray[$itemSaved->item_id] = $itemSaved->uri;
        // }
        $exactMatchArr = [];
        /*
        # get all document_id from item id array
        $allDocumentIdentifierArr = [];
        $allSourceItemIdArr = [];
        $exactMatchSourceItemIdArr = [];
        $destinationNodeId = "";
        foreach($caseAssociatonsFromJsonSave as $association) {
            $originNodeId = !empty($association["originNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["originNodeURI"]['identifier']) : "";
            if($originNodeId!=="") {
                if(!in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                    if(!empty($association["destinationNodeURI"]['identifier'])) {
                        $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];

                        // search inside the imported taxonomy's saved items
                        $destinationNodeId = $this->helperToFetchIdFromAlreadySavedItems($destinationNodeIdToSearchWith);

                        // if not found then search inside all items that is stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);

                        }

                        // if not found for any item related search, then search for any document stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchForAndReturnAnyDocumentId($destinationNodeIdToSearchWith);
                        }
                    }

                    // by default set document id when destinationNodeId is not found and the relation type is childof
                    if($association["associationType"]==="isChildOf" && empty($destinationNodeId)) {
                        $destinationNodeId      =    $this->helperToSearchForAndReturnAnyDocumentId($destinationNodeIdToSearchWith);
                    }
                    if(empty($destinationNodeId))
                    {
                        $destinationNodeId = $destinationNodeIdToSearchWith;
                    }

                    $allSourceItemIdArr[] = $destinationNodeId;

                    if($requestImportType == 2 && $associationType == 1 && !in_array($originNodeId,$exactMatchArr)) {
                        $ExactMatchDestinationNodeId = (!empty($oldRelationArr[$originNodeId])) ? $oldRelationArr[$originNodeId]['id'] : "";
                        $exactMatchSourceItemIdArr[] = $ExactMatchDestinationNodeId;
                    }

                } else {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : "";
                    // if not found then search inside all items that is stored in the system
                    if($destinationNodeId!== "") {
                        $allSourceItemIdArr[] = $destinationNodeId;
                    } else {
                        $destinationId = "";
                        
                        if(!empty($association["destinationNodeURI"]['identifier'])) {
                            $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];

                            $destinationId   =   $this->helperToSearchAndReturnReverseDestinationNodeItem($destinationNodeIdToSearchWith);

                            if(empty($destinationId)) {
                                $destinationId = $destinationNodeIdToSearchWith;
                            }
                        }

                        $allSourceItemIdArr[] = $destinationId;
                    }
                }
            } else {
                if(in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $allSourceItemIdArr[] = $destinationNodeId;
                    }
                }
            }
        }

        $allSourceItemIdArr = array_merge($allSourceItemIdArr, $exactMatchSourceItemIdArr);
        $allSourceItemIdArr = array_unique($allSourceItemIdArr);

        $allDocumentIdentifierResult = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemIdArr($allSourceItemIdArr, $organizationId);

        foreach($allDocumentIdentifierResult as $docId) {
            $allDocumentIdentifierArr[$docId->source_item_id] = $docId->document_id;
        }
        */
        $updateItemArray = [];
        foreach($caseAssociatonsFromJsonSave as $association) {
            $destinationNodeId      =   "";
            $destinationDocumentId  =   "";
            
            $originNodeId = !empty($association["originNodeURI"]["identifier"]) ?
            $this->helperToFetchIdFromAlreadySavedItems($association["originNodeURI"]['identifier']) : 
            "";

            if($originNodeId!=="") {
                if(!in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    
                    $itemAssociationId = $this->createUniversalUniqueIdentifier();
                    $sourceItemAssociationId = ($requestImportType == 2) ? $itemAssociationId : $association["identifier"];
                    $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                    $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                        $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                        
                    
                    if(!empty($association["destinationNodeURI"]['identifier'])) {
                        $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];

                        // search inside the imported taxonomy's saved items
                        $destinationNodeId = $this->helperToFetchIdFromAlreadySavedItems($destinationNodeIdToSearchWith);

                        // if not found then search inside all items that is stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);

                        }

                        // if not found for any item related search, then search for any document stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchForAndReturnAnyDocumentId($destinationNodeIdToSearchWith);
                        }
                    }

                    // by default set document id when destinationNodeId is not found and the relation type is childof
                    if($association["associationType"]==="isChildOf" && empty($destinationNodeId)) {
                        $destinationNodeId      =    $this->helperToSearchForAndReturnAnyDocumentId($destinationNodeIdToSearchWith);
                    }
                    if(empty($destinationNodeId))
                    {
                        $destinationNodeId = $destinationNodeIdToSearchWith;
                    }
                    $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                    if(!empty($association["destinationNodeURI"]["uri"])) {
                        $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                    }
                    else {
                        $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"]['uri'] : "";
                    }
                    $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";

                    // set external details for exact match of relationship only
                    if($associationType===4) {
                        $externalNodeTitle = !empty($association["destinationNodeURI"]['title']) ? $association["destinationNodeURI"]['title'] : "";
                        $externalNodeUrl = !empty($association["destinationNodeURI"]['uri']) ? $association["destinationNodeURI"]['uri'] : "";
                        // ??confusion why empty?
                        // $destinationNodeId = "";
                    }
                    // set destination external details for exemplar relationship
                    else if($associationType===8 && empty($externalNodeUrl)) {
                        $externalNodeUrl = !empty($association["destinationNodeURI"]["uri"]) ? $association["destinationNodeURI"]["uri"] : "";
                    }
                    $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;

                    /*if(!empty($destinationNodeId)) {
                        $returnCollection = true;
                        $fieldsToReturn = ["*"];
                        $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                        $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, 
                                            $fieldsToReturn, 
                                            $dataFetchingConditionalClause
                                        )->first();

                        if(is_array($destinationNodeDetail) && sizeOf($destinationNodeDetail) > 0) {
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }
                    }*/

                    // get target document id based on destination node id start
                    if(isset($updatedItemDataToSave2[$destinationNodeId]) && !empty($updatedItemDataToSave2[$destinationNodeId]['document_id'])) {
                        $targetDocumentIdentifier = $updatedItemDataToSave2[$destinationNodeId]['document_id'];
                    } else {
                        $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($destinationNodeId,$organizationId);
                        $targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $documentId;
                    }

                    // $targetDocumentIdentifier = isset($allDocumentIdentifierArr[$destinationNodeId]) ? $allDocumentIdentifierArr[$destinationNodeId] : $documentId;

                    $caseAssociationsToSave[] = [
                        "item_association_id"           => $itemAssociationId,
                        "association_type"              => $associationType,
                        "document_id"                   => $documentId,
                        "association_group_id"          => $associationGroupId,
                        "origin_node_id"                => $originNodeId,
                        "destination_node_id"           => ($associationType!=8) ? $destinationNodeId : '',
                        "destination_document_id"       => ($associationType!=8) ? $documentId : '',
                        "sequence_number"               => $sequenceNumber,
                        "external_node_title"           => $externalNodeTitle,
                        "external_node_url"             => $externalNodeUrl,
                        "created_at"                    => $currentDateTime,
                        "updated_at"                    => $updatedAt,
                        "source_item_association_id"    => $sourceItemAssociationId,
                        "organization_id"               => $organizationId,
                        "is_reverse_association"        => 0,
                        "uri"                           => $association['uri'],
                        "source_document_id"            =>  $documentId,
                        "source_item_id"                =>  $originNodeId,
                        "target_document_id"            =>  ($associationType!=8) ? $targetDocumentIdentifier : '',
                        "target_item_id"                =>  ($associationType!=8) ? $destinationNodeId : ''
                    ];
                    if(isset($updatedItemDataToSave[$originNodeId])) {
                        $updatedItemDataToSave[$originNodeId]['parent_id'] = $destinationNodeId;
                    } else {
                        $updateItemArray[] = ['where' => $originNodeId, 'parent_id' => $destinationNodeId];
                    }
                    // $this->itemRepository->edit($originNodeId, ['parent_id' => $destinationNodeId]);
                    
                    // if package to be imported is a clone then create a new association with exact match of relationship
                    if($requestImportType == 2 && $associationType == 1 && !in_array($originNodeId,$exactMatchArr)) {

                        $newAssociationIdentifier = $this->createUniversalUniqueIdentifier();
                        $externalNodeTitle  = !empty($association["destinationNodeURI"]['title']) ? $association["destinationNodeURI"]['title'] : "";
                        $externalNodeUrl    = !empty($association["destinationNodeURI"]['uri']) ? 
                                        $association["destinationNodeURI"]['uri'] : "";
                        

                        //$originalSourceObject   = explode("CFItems/", $itemIdSavedArray[$originNodeId]);
                        // $originalSourceObject   = explode("/", $itemIdSavedArray[$originNodeId]);

                        $ExactMatchDestinationNodeId = (!empty($oldRelationArr[$originNodeId])) ? $oldRelationArr[$originNodeId]['id'] : "";
                        $ExactMatchExternalNodeTitle = (!empty($oldRelationArr[$originNodeId])) ? $oldRelationArr[$originNodeId]['full_statement'] : "";
                        $ExactMatchExternalNodeUrl = (!empty($oldRelationArr[$originNodeId])) ? $oldRelationArr[$originNodeId]['uri'] : "";

                        // get target document id based on destination node id start
                        if(isset($updatedItemDataToSave2[$ExactMatchDestinationNodeId]) && !empty($updatedItemDataToSave2[$ExactMatchDestinationNodeId]['document_id'])) {
                            $targetDocumentIdentifier = $updatedItemDataToSave2[$ExactMatchDestinationNodeId]['document_id'];
                        } else {
                            //$documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($ExactMatchDestinationNodeId,$organizationId);
                            //$targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $documentId;
                            $targetDocumentIdentifier = $documentId;
                        }

                        // $targetDocumentIdentifier = isset($allDocumentIdentifierArr[$ExactMatchDestinationNodeId]) ? $allDocumentIdentifierArr[$ExactMatchDestinationNodeId] : $documentId;

                        $caseAssociationsToSave[]  = [
                            "item_association_id"           => $newAssociationIdentifier,
                            "association_type"              => 4, // for exact match of relationship
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $ExactMatchDestinationNodeId,
                            "destination_document_id"       => $exactMatchOfAssociationDocumentId,
                            "sequence_number"               => $sequenceNumber++,
                            "external_node_title"           => $ExactMatchExternalNodeTitle,
                            "external_node_url"             => $ExactMatchExternalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $newAssociationIdentifier,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 0,
                            "uri"                           => $this->getCaseApiUri("CFAssociations", $newAssociationIdentifier,  true, $orgCode),
                            "source_document_id"            =>  $documentId,
                            "source_item_id"                =>  $originNodeId,
                            "target_document_id"            =>  $targetDocumentIdentifier,
                            "target_item_id"                =>  $ExactMatchDestinationNodeId
                            //"uri"                           => $this->getCaseApiUri("CFAssociations", $newAssociationIdentifier,  true, $orgCode,$this->domainName)
                        ];
                        array_push($exactMatchArr,$originNodeId);
                        
                    }
                } else {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $itemAssociationId          =   $this->createUniversalUniqueIdentifier();
                        $sourceItemAssociationId    =   ($requestImportType == 2) ? $itemAssociationId : $association["identifier"];

                        $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                            $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                        
                        // if not found then search inside all items that is stored in the system
                        if(empty($destinationNodeId)) {
                            $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);
                        }

                        //echo $destinationNodeId;
                        /*if(!empty($destinationNodeId)) {
                            $returnCollection = true;
                            $fieldsToReturn = ["*"];
                            $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                            $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                $returnCollection, 
                                                $fieldsToReturn, 
                                                $dataFetchingConditionalClause
                                            )->first();
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }
                        */
                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                        if(!empty($association["destinationNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]['uri']) ? $association["destinationNodeURI"]['uri'] : "";
                        }
                        $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";

                        // set external details for exact match of relationship only
                        if($associationType===4) {
                            $externalNodeTitle = !empty($association["originNodeURI"]['title']) ? $association["originNodeURI"]['title'] : "";
                            $externalNodeUrl = !empty($association["originNodeURI"]['uri']) ? $association["originNodeURI"]['uri'] : "";
                            // ??confusion why empty?
                            // $destinationNodeId = "";
                        }
                        // set destination external details for exemplar relationship
                        else if($associationType===8 && empty($externalNodeUrl)) {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]["uri"]) ? $association["destinationNodeURI"]["uri"] : "";
                        }
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
        
                        // get target document id based on destination node id start
                        if(isset($updatedItemDataToSave2[$destinationNodeId]) && !empty($updatedItemDataToSave2[$destinationNodeId]['document_id'])) {
                            $targetDocumentIdentifier1 = $updatedItemDataToSave2[$destinationNodeId]['document_id'];
                        } else {
                            $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($destinationNodeId,$organizationId);
                            $targetDocumentIdentifier1 = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $documentId;
                        }

                        // $targetDocumentIdentifier1 = isset($allDocumentIdentifierArr[$destinationNodeId]) ? $allDocumentIdentifierArr[$destinationNodeId] : $documentId;

                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => ($associationType!=8) ? $destinationNodeId : '',
                            "destination_document_id"       => ($associationType!=8) ? $documentId : '',
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 0,
                            "uri"                           => $association["uri"],
                            "source_document_id"            =>  $documentId,
                            "source_item_id"                =>  $originNodeId,
                            "target_document_id"            =>  ($associationType!=8) ? $targetDocumentIdentifier1 : '',
                            "target_item_id"                =>  ($associationType!=8) ? $destinationNodeId : ''
                        ];
                        if(isset($updatedItemDataToSave[$originNodeId])) {
                            $updatedItemDataToSave[$originNodeId]['parent_id'] = $destinationNodeId;
                        } else {
                            $updateItemArray[] = ['where' => $originNodeId, 'parent_id' => $destinationNodeId];
                        }

                        // $this->itemRepository->edit($originNodeId, ['parent_id' => $destinationNodeId]);
                    } else {
                        $itemAssociationId          =   $this->createUniversalUniqueIdentifier();
                        $sourceItemAssociationId = ($requestImportType == 2) ? $itemAssociationId : $association["identifier"];

                        $associationType            =   $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId         = !empty($association["CFAssociationGroupingURI"]) ?    $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        $destinationId = "";
                        
                        if(!empty($association["destinationNodeURI"]['identifier'])) {
                            //dd("in if");
                            $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];
                            
                            
                            $destinationId   =   $this->helperToSearchAndReturnReverseDestinationNodeItem($destinationNodeIdToSearchWith);

                            if(empty($destinationId)) {
                                $destinationId = $destinationNodeIdToSearchWith;
                            }
        
                        }  
                        //dd( $destinationNodeIdToSearchWith);
                        /*$destinationDocumentId     =   $this->helperToSearchAndReturnReverseDocumentNode($destinationNodeIdToSearchWith);
                        if(empty($destinationDocumentId)) {
                            $destinationDocumentId = $documentId;
                        }*/
        
                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;
        
                        if(!empty($association["destinationNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"]['uri'] : "";
                        }
                        $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";
        
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
        
                        // get target document id based on destination node id start
                        if(isset($updatedItemDataToSave2[$destinationId]) && !empty($updatedItemDataToSave2[$destinationId]['document_id'])) {
                            $targetDocumentIdentifier2 = $updatedItemDataToSave2[$destinationId]['document_id'];
                        } else {
                            $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($destinationId,$organizationId);
                            $targetDocumentIdentifier2 = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $documentId;
                        }
                        
                        // $targetDocumentIdentifier2 = isset($allDocumentIdentifierArr[$destinationId]) ? $allDocumentIdentifierArr[$destinationId] : $documentId;

                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => ($associationType!=8) ? $destinationId : '',
                            "destination_document_id"       => ($associationType!=8) ? $documentId : '',
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 0,
                            "uri"                           => $association['uri'],
                            "source_document_id"            =>  $documentId,
                            "source_item_id"                =>  $originNodeId,
                            "target_document_id"            =>  ($associationType!=8) ? $targetDocumentIdentifier2 : '',
                            "target_item_id"                =>  ($associationType!=8) ? $destinationId : ''
                        ];
                        if(isset($updatedItemDataToSave[$originNodeId])) {
                            $updatedItemDataToSave[$originNodeId]['parent_id'] = $destinationNodeId;
                        } else {
                            $updateItemArray[] = ['where' => $originNodeId, 'parent_id' => $destinationNodeId];
                        }
                        // $this->itemRepository->edit($originNodeId, ['parent_id' => $destinationId]);
                    }
                }
                
            }
            else {
                if(in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $itemAssociationId          = $this->createUniversalUniqueIdentifier();
                        $sourceItemAssociationId = ($requestImportType == 2) ? $itemAssociationId : $association["identifier"];
                        $associationType            = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId         = !empty($association["CFAssociationGroupingURI"]) ? 
                                            $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        
                        if(!empty($association["originNodeURI"]['identifier'])) {
                            $originNodeIdToSearchWith = $association["originNodeURI"]['identifier'];

                            
                            $originNodeId   =   $this->helperToSearchAndReturnOriginNodeItem($originNodeIdToSearchWith);

                            if(empty($originNodeId)) {
                                $originNodeId = $association["originNodeURI"]["identifier"];
                            } 
                        }

                        /*if(!empty($destinationNodeId)) {
                            $returnCollection = true;
                            $fieldsToReturn = ["*"];
                            $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                            $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                $returnCollection, 
                                                $fieldsToReturn, 
                                                $dataFetchingConditionalClause
                                            )->first();
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }*/ 
                        

                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                        if(!empty($association["originNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["originNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["originNodeURI"]) ? $association["originNodeURI"]['uri'] : "";
                        }
                        $externalNodeTitle = !empty($association["originNodeURI"]["title"]) ? $association["originNodeURI"]["title"] : "";

                        
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;

                        // get target document id based on destination node id start
                        if(isset($updatedItemDataToSave2[$destinationNodeId]) && !empty($updatedItemDataToSave2[$destinationNodeId]['document_id'])) {
                            $targetDocumentIdentifier3 = $updatedItemDataToSave2[$destinationNodeId]['document_id'];
                        } else {
                            $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($destinationNodeId,$organizationId);
                            $targetDocumentIdentifier3 = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $documentId;
                        }
                        
                        // $targetDocumentIdentifier3 = isset($allDocumentIdentifierArr[$destinationNodeId]) ? $allDocumentIdentifierArr[$destinationNodeId] : $documentId;


                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => ($associationType!=8) ? $destinationNodeId : '',
                            "destination_document_id"       => ($associationType!=8) ? $documentId : '',
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 1,
                            "uri"                           => $association['uri'],
                            "source_document_id"            =>  !empty($association["CFDocumentURI"]["identifier"]) ? $association["CFDocumentURI"]["identifier"] : "",
                            "source_item_id"                =>  $originNodeId,
                            "target_document_id"            =>  ($associationType!=8) ? $targetDocumentIdentifier3 : '',
                            "target_item_id"                =>  ($associationType!=8) ? $destinationNodeId : ''
                        ];
                    }
                }
            }            
        }

        #insert item nodes
        $updatedItemDataToSave = array_values($updatedItemDataToSave);
        $updatedItemDataToSaveArry = array_chunk($updatedItemDataToSave,1000);
        foreach($updatedItemDataToSaveArry as $updatedItemDataToSaveArryK => $updatedItemDataToSaveArryV)
        {
            DB::table('items')->insert($updatedItemDataToSaveArryV);
        }
        unset($updatedItemDataToSaveArry);
        
        /***
        14022020 : Eloquent multiple insert

        $recordsToSaveCollection = collect($updatedItemDataToSave);
        foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
            $this->itemRepository->saveMultiple($chunk->toArray());
        }
        ***/

        # update multiple Items
        if(count($updateItemArray)) {
            $updateItemArrayChunk = array_chunk($updateItemArray, 1000);
            $prefix = DB::getTablePrefix();
    
            foreach($updateItemArrayChunk as $chunk) {
                $statement = '';
                foreach($chunk as $value) {
                    $statement .= ' UPDATE '.$prefix.'items SET  parent_id = "'.$value['parent_id'].'" where item_id = "'.$value['where'].'"; ';
                }
                if(!empty($statement)) {
                    DB::unprepared($statement);
                }
                unset($statement);
            }
        }

        #insert item associations
        $caseAssociationsToSaveArry = array_chunk($caseAssociationsToSave,1000);
        foreach($caseAssociationsToSaveArry as $caseAssociationsToSaveArryK => $caseAssociationsToSaveArryV)
        {
            DB::table('item_associations')->insert($caseAssociationsToSaveArryV);
        }
        unset($caseAssociationsToSaveArry);
        /***
        14022020 : Eloquent multiple insert
        // convert to collection to insert records as chunk
        $recordsToSaveCollection = collect($caseAssociationsToSave);
        foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
            $this->itemAssociationRepository->saveMultiple($chunk->toArray());
        }
        ***/
        # make taxonomy visible in taxnonomy listing page
        DB::table('documents')->where('document_id', $savedDocument->document_id)->update(['import_status' => 0]);
    }

    /************** All helper methods specific to this import functionality only **************/

    private function helperToFetchTenantSpecificDataOfProvidedType(string $type): Collection {
        $organizationId = $this->requestingUserOrganization;
        $dataFetchingConditionalClause = [ "organization_id" => $organizationId, 'is_deleted' => 0];
        $returnCollection = true;
        switch ($type) {
            case 'concept':
                $fieldsToReturn = ['concept_id', 'keywords', 'title', 'source_concept_id'];
                $repository = $this->conceptRepository;
                break;
            case 'subject':
                $fieldsToReturn = ['subject_id', 'title', 'source_subject_id'];
                $repository = $this->subjectRepository;
                break;
            case 'license':
                $fieldsToReturn = ['license_id', 'title', 'license_text', 'source_license_id'];
                $repository = $this->licenseRepository;
                break;
            case 'node':
                $fieldsToReturn = ['node_type_id', 'title', 'type_code', 'is_document', 'is_default', 'source_node_type_id'];
                $repository = $this->nodeTypeRepository;
                break;
            case 'associationGroup':
                $fieldsToReturn = ['association_group_id', 'title', 'source_association_group_id'];
                $repository = $this->associationGroupRepository;
                break;
            case 'language':
                $fieldsToReturn = ['language_id', 'short_code'];
                $repository = $this->languageRepository;
                break;
            default:
                $repository = false;
                break;
        }

        $dataToReturn = $repository!==false ? 
                        $repository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $dataFetchingConditionalClause) : 
                        collect([]);
        
        return $dataToReturn;
    }

    private function helperToFetchLanguageIdFromEitherExistingOrNew(string $languageToSearchFor): string {
        $savedLanguages = $this->savedLanguages;
        $searchResult = $savedLanguages->where("short_code", $languageToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $language = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->requestingUserOrganization;
            $newLanguageIdentifier = $this->createUniversalUniqueIdentifier();
            $languageDataToSave = [
                "language_id" => $newLanguageIdentifier,
                "name" => ucwords($languageToSearchFor),
                "short_code" => $languageToSearchFor,
                "organization_id" => $organizationId
            ];
            // save record to db
            $language = $this->languageRepository->saveData($languageDataToSave);
            // update the exiting inmemory list
            $savedLanguages->push($language);
        }
        return $language->language_id;
    }

    private function helperToFetchLicenseIdFromEitherExistingOrNew(array $licenseToSearchFor): string {
        $license        =   [];
        $searchResult   =   '';
        $savedLicenses = $this->savedLicenses;
        $licenseTitleToSearchFor = $licenseToSearchFor["title"];
        
        if(!empty($licenseTitleToSearchFor)){
            $searchResult = $savedLicenses->where("title", $licenseTitleToSearchFor);
        }

        // if found in memory then return id
        if(!empty($searchResult)) {
            $license = $searchResult->first();
        }
        return !empty($license->license_id) ? $license->license_id : "";
    }

    private function helperToFetchSourceLicenseIdFromEitherExistingOrNew(string $licenseIdToSearchFor){
        $license            =   [];
        $savedLicenses      =   $this->savedLicenses;
        $searchResult       =   $savedLicenses->where("license_id", $licenseIdToSearchFor);

        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $license = $searchResult->first();
        }

        return $license;
    }

    private function helperToFetchSubjectIdFromEitherExistingOrNew(array $subjectToSearchFor): string {
        $subject                    =   [];
        $savedSubjects              =   $this->savedSubjects;
        $subjectTitleToSearchFor    =   $subjectToSearchFor["title"];
        
        if(!empty($subjectTitleToSearchFor)){
            $searchResult = $savedSubjects->where("title", $subjectTitleToSearchFor);
        }
        
        // if found in memory then return id
        if(!empty($searchResult)) {
            $subject = $searchResult->first();
        }
        
        return !empty($subject->subject_id) ? $subject->subject_id : "";
    }

    private function helperToFetchConceptIdFromEitherExistingOrNew(array $conceptToSearchFor): string {
        $concept            =   [];
        $savedConcepts      =   $this->savedConcepts;
        $searchResult       =   '';
        $conceptTitleToSearchFor = $conceptToSearchFor["title"];

        if(!empty($conceptTitleToSearchFor)){
            $searchResult = $savedConcepts->where("title", $conceptTitleToSearchFor);
        }

        // if found in memory then return id
        if(!empty($searchResult)) {
            $concept = $searchResult->first();
        }
        
        return !empty($concept->concept_id) ? $concept->concept_id : "";
    }

    private function helperToFetchSourceConceptIdFromEitherExistingOrNew(string $conceptIdToSearchFor){
        $concept            =   [];
        $savedConcepts      =   $this->savedConcepts;
        $searchResult = $savedConcepts->where("concept_id", $conceptIdToSearchFor);

        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $concept = $searchResult->first();
        }
        return $concept;
    }

    private function helperToFetchItemTypeIdFromEitherExistingOrNew(array $itemTypeToSearchFor): string {
        $itemType           =   [];
        $searchResult       =   '';
        $savedNodeTypes     =   $this->savedNodeTypes;
        $itemTypeTitleToSearchFor = $itemTypeToSearchFor["title"];
        if(!empty($itemTypeTitleToSearchFor)){
            $searchResult = $savedNodeTypes->where("title", $itemTypeTitleToSearchFor);
        }

        // if found in memory then return id
        if(!empty($searchResult)) {
            $itemType = $searchResult->first();
        }
        
        
        return $itemType->node_type_id;
    }

    private function helperToFetchSourceItemTypeIdFromEitherExistingOrNew(string $itemTypeIdToSearchFor) {
        $itemType           =   [];
        $savedNodeTypes     =   $this->savedNodeTypes;
        $searchResult       =   $savedNodeTypes->where("node_type_id", $itemTypeIdToSearchFor);

        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $itemType = $searchResult->first();
        }
        
        return $itemType;
    }

    private function helperToUpdateTheItemDataToSaveWithParentId(array $itemsToSave, string $documentSourceId, string $savedDocumentId): array {
        if(!empty($itemsToSave)) {
            $caseChildOfAssociatons = $this->childOfCaseAssociations;
            if(!empty($caseChildOfAssociatons)){
                // for each item iterate through the isChildOf associations
                foreach($itemsToSave as $itemKey=>$savedItem) {
                    // set empty parent id by default
                    $parentId="";
                    $itemIdentifierToSearchFor = $savedItem["source_item_id"];
                    // for each item search the case associations for parent id
                    foreach($caseChildOfAssociatons as $associationKey=>$associationEntity){
                        $originNodeIdentifier = $associationEntity['originNodeURI']['identifier'];
                        $destinationNodeIdentifier = $associationEntity["destinationNodeURI"]["identifier"];

                        if($itemIdentifierToSearchFor===$originNodeIdentifier){
                            // search the item list for parent
                            $newItemIdentifier = $this->helperToReturnNewItemIdentifier($destinationNodeIdentifier, $itemsToSave);
                            
                            if($newItemIdentifier!=="") {
                                $parentId = $newItemIdentifier;
                            }
                            else if($documentSourceId===$destinationNodeIdentifier){
                                $parentId = $savedDocumentId;
                            }
                            else {
                                $parentId = "";
                            }
                            // reduce the association data set with each successfull iteration to reduce the array size
                            unset($caseChildOfAssociatons[$associationKey]);
                            break;
                        }
                    }
                    // update the item's parent id
                    $itemsToSave[$itemKey]["parent_id"] = $parentId;
                }
            }
            unset($caseChildOfAssociatons);
        }
        return $itemsToSave;
    }

    private function helperToReturnNewItemIdentifier(string $identifier, array $items): string {
        $newItemIdentifier = "";
        foreach($items as $item) {
            if($identifier===$item["source_item_id"]){
                $newItemIdentifier = $item["item_id"];
                break;
            }
        }
        return $newItemIdentifier;
    }

    private function helperToFetchAssociationGroupIdFromEitherExistingOrNew(array $associationGroupToSearchFor): string {
        $searchResult           =   [];
        $associationGroup       =   [];
        $savedAssociationGroups = $this->savedAssociationGroups;
        $associationGroupTitleToSearchFor = $associationGroupToSearchFor["title"];

        if(!empty($associationGroupTitleToSearchFor)) {
            $searchResult = $savedAssociationGroups->where("title", $associationGroupTitleToSearchFor);
        }
        
        // if found in memory then return id
        if(!empty($searchResult)) {
            $associationGroup = $searchResult->first();
            $associationGrouopId = $associationGroup->association_group_id;
        }
        else
        {
            $associationGrouopId = "";
        }
        return $associationGrouopId;
    }

    private function helperToFetchIdFromAlreadySavedItems(string $sourceItemId){
        $itemIdentifierToReturn =   "";
        $savedItems             =   $this->savedItems;
        $itemSourceIdentifier   =   $this->itemSourceIdentifier;

        // get before save document
        $originalDocument = $this->caseDocumentToSave;

        $originalDocumentIdentifier = $originalDocument["identifier"];
        foreach($savedItems as $savedItem) {
            if($sourceItemId===$savedItem["source_item_id"]) {
                $itemIdentifierToReturn = $savedItem['item_id'];
                break;
            }
        }

        // if no match found for items, then match with document
        if($itemIdentifierToReturn==="" && $sourceItemId===$originalDocumentIdentifier){
            $savedDocument = $this->savedDocument;
            $itemIdentifierToReturn = $savedDocument->document_id;
        }

        if(empty($itemIdentifierToReturn)) {
            foreach($itemSourceIdentifier as $key=>$sourceId) {
                if($sourceItemId == $sourceId) {
                    $itemIdentifierToReturn =   $key;
                }
            }
        }
        return $itemIdentifierToReturn;
    }

    private function helperToFetchIdFromAllSavedItems(string $sourceItemId){
        $itemIdentifierToReturn =  '';
        $organizationId =   $this->requestingUserOrganization;
        $attributes =   ['source_item_id' => $sourceItemId, 'organization_id' => $organizationId, 'is_deleted' => 0];
        $itemInDatabase = $this->itemRepository->findByAttributes($attributes)->first();
        if($itemInDatabase != null) {
            $itemIdentifierToReturn = $itemInDatabase->item_id;
        } else {
            $attributes =   ['source_document_id' => $sourceItemId, 'organization_id' => $organizationId, 'is_deleted' => 0];
            $documentInDatabase = $this->documentRepository->findByAttributes($attributes)->first();

            if($documentInDatabase != null) {
                $itemIdentifierToReturn = $documentInDatabase->document_id;
            }
        }

        return $itemIdentifierToReturn;
    }
    

    private function helperToReturnDestinationNodeIds(): array {
        $destinationNodeIds = [];
        $caseAssociationsToSave = $this->caseAssociationsToSave;
        foreach($caseAssociationsToSave as $association) {
            if(!empty($association["destinationNodeURI"]["identifier"])) {
                $destinationNodeIds[] = $association["destinationNodeURI"]["identifier"];
            }
        }
        unset($caseAssociationsToSave);
        return $destinationNodeIds;
    }

    private function helperToReturnOriginNodeIds(): array {
        $originNodeIds = [];
        $caseAssociationsToSave = $this->caseAssociationsToSave;
        foreach($caseAssociationsToSave as $association) {
            if(!empty($association["originNodeURI"]["identifier"])) {
                $originNodeIds[] = $association["originNodeURI"]["identifier"];
            }
        }
        unset($caseAssociationsToSave);
        return $originNodeIds;
    }

    private function helperToReturnReverseDestinationNodeIds(): array {
        $reverseDestinationNodeIds = [];
        $caseAssociationsToSave = $this->caseAssociationsToSave;
        foreach($caseAssociationsToSave as $association) {
            
            if($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']) == 3 || $this->getSystemSpecifiedAssociationTypeNumber($association['associationType']) == 5 || $this->getSystemSpecifiedAssociationTypeNumber($association['associationType']) == 7) {
                if(!empty($association["originNodeURI"]["identifier"])) {
                    $reverseDestinationNodeIds[] = $association["originNodeURI"]["identifier"];
                }
            }
        }
        unset($caseAssociationsToSave);
        return $reverseDestinationNodeIds;
    }

    private function helperToSearchAndReturnDestinationNodeItem(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $organizationId =   $this->requestingUserOrganization;
        // $destinationNodeItems = $this->destinationNodeItems;
        // if($destinationNodeItems->isNotEmpty()) {
        //     $searchResult = $destinationNodeItems->where('source_item_id', $searchIdentifier);
        //     $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->item_id : "";
        // }
        $searchResult = DB::table('items')->select('item_id')->where('source_item_id', $searchIdentifier)->where('organization_id', $organizationId)->first();
        if(isset($searchResult->item_id)) {
            $nodeIdToReturn = $searchResult->item_id;
        }
        return $nodeIdToReturn;
    }

    private function helperToSearchAndReturnOriginNodeItem(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $organizationId =   $this->requestingUserOrganization;
        // $originNodeItems = $this->originNodeItems;
        // if($originNodeItems->isNotEmpty()) {
        //     $searchResult = $originNodeItems->where('source_item_id', $searchIdentifier);
        //     $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->item_id : "";
        // }
        $searchResult = DB::table('items')->select('item_id')->where('source_item_id', $searchIdentifier)->where('organization_id', $organizationId)->first();
        if(isset($searchResult->item_id)) {
            $nodeIdToReturn = $searchResult->item_id;
        }
        return $nodeIdToReturn;
    }

    private function helperToSearchAndReturnReverseDestinationNodeItem(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $reverseDestinationNodeItems = $this->reverseDestinationNodeItems;
        if($reverseDestinationNodeItems->isNotEmpty()) {
            $searchResult = $reverseDestinationNodeItems->where('source_item_id', $searchIdentifier);
            $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->item_id : "";
        }
        return $nodeIdToReturn;
    }

    private function helperToSearchAndReturnReverseDocumentNode(string $searchIdentifier){
        $documentIdToReturn = "";
        $destinationNodeItems = $this->reverseDestinationNodeItems;
        //print_r($originNodeItems);
        if($destinationNodeItems->isNotEmpty()) {
            $searchResult = $destinationNodeItems->where('source_item_id', $searchIdentifier);
            $documentIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->document_id : "";
        }

        //echo $documentIdToReturn;
        return $documentIdToReturn;
    }

    private function helperToSearchForAndReturnAnyDocumentId(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $conditionalAttributes = [ "source_document_id" => $searchIdentifier ];
        $searchResult = $this->documentRepository->findByAttributes($conditionalAttributes);
        $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->document_id : "";
        return $nodeIdToReturn;
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs() {
        $documentId = $this->savedDocument->document_id;
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["taxonomy_created"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }

}
