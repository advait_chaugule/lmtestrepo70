<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use DB;
use App\Services\Api\Traits\ComparisonSummaryHelperTrait;
use App\Services\Api\Traits\ErrorMessageHelper;

class ComparisonSummaryDetailsJob extends Job
{
    use ErrorMessageHelper, ComparisonSummaryHelperTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $comparisonIdentifier;
    private $cfType;
    private $metadataCountReport;

    public function __construct($comparisonId,$type,$detailsType='displayDetails',$internalName='',$nodeTypeId='')
    {
        $this->comparisonIdentifier = $comparisonId;
        $this->cfType = $type;
        $this->detailsType = $detailsType;
        $this->internalName = $internalName;
        $this->nodeTypeId = $nodeTypeId;
        $this->metadataCountReport = [];
        $this->setComparisonVariables();
        $this->allowAllSubjectFlag = true;
        $this->allowAllLicenseFlag = true;
        $this->allowAllConceptFlag = true;
        $this->documentMetaCount = 0;
            
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $comparisonId = $this->comparisonIdentifier;
            $internalName = $this->internalName;
            $nodeTypeId = $this->nodeTypeId; 
            $detailsType = $this->detailsType;
            $action = $this->action;       
            $type = $this->cfType;
            switch($type){
                case 'document':
                    $vntType = ['CFDocument'];
                break;
                case 'associations':
                    $vntType = ['CFAssociations'];
                break;
                case 'nodetype':
                    $vntType = ['CFItems'];
                break;
                case 'metadata':
                case 'allMetadata':
                default:
                    $vntType = ['CFItems','CFDocument'];
                break;
            }
            $summaryCount = $this->getNodeBasedDetails($comparisonId,$type,$vntType,$nodeTypeId);
            $this->summaryReport = ['type'=>$type];
            $this->licenses = [];
            $this->subjects = [];
            $this->concepts = [];
            $this->itemDetails = [];            
            
            // To save CFType, NodeType, Action. Eg: CFAssociations - isChildOf - D / CFItems - Course - E
            foreach($summaryCount as $summary){   
                if(!empty($summary['value'])){
                    $summaryJsonValue = $summary['value'];               
                    $summary['value'] = json_decode($this->stripSpecialCharacters($summary['value']),true);                
                }
                if($summary['value']==null && (json_last_error() != JSON_ERROR_NONE)){
                    $comparisonError = ['type'=>$type,'node_id'=>$summary['node_id'],'summary_value'=>$summaryJsonValue,'error_type'=>json_last_error(),'error_description'=>json_last_error_msg()];
                    $this->logComparisonError($comparisonError);
                }
                $summCfType = $summary['type'];
                $nodeTypeActionValue = ['action'=>$summary['action'], 'value'=> $summary['value']];
                switch($summCfType){
                    case 'CFLicenses':
                        $this->licenses[$summary['node_id']] = $nodeTypeActionValue;
                    break;
                    case 'CFSubjects':
                        $this->subjects[$summary['node_id']] = $nodeTypeActionValue;
                    break;
                    case 'CFConcepts':
                        $this->concepts[$summary['node_id']] = $nodeTypeActionValue;
                    break;
                    case 'CFItems':
                    case 'CFDocument':
                        if(!isset($this->itemDetails[$summary['node_id']])){
                            $this->itemDetails[$summary['node_id']] = $summary['value'];
                        }
                    break;
                    default:
                    break;
                }
                
                $mappedAction = $action[$summary['action']];            
                if($type=='associations' && $summCfType=='CFAssociations'){
                    if($mappedAction!='edit'){
                        $this->summaryReport[$mappedAction][$summary['node_id']] = $summary['value'];
                    }
                    $originNodeTitle = explode(':',$summary['value']['originNodeURI']['title']);
                    $destinationNodeTitle = explode(':',$summary['value']['destinationNodeURI']['title']);
                    if(!empty($originNodeTitle)){
                        $originNodeFullStatement = (count($originNodeTitle)=='4') ? trim($originNodeTitle[2].' '.$originNodeTitle[3]) : $originNodeTitle[0];
                        if($mappedAction=='edit'){                    
                            $this->assocDetails[$summary['node_id']]['fullStatement'] = $originNodeFullStatement;
                        }    
                        else{
                            $this->summaryReport[$mappedAction][$summary['node_id']]['fullStatement'] = $originNodeFullStatement;
                        }
                    }
                    if(!empty($destinationNodeTitle)){
                        $destinationNode = (count($destinationNodeTitle)=='4') ? trim($destinationNodeTitle[2].' '.$destinationNodeTitle[3]) : $destinationNodeTitle[0];
                        $destinationTaxonomy = $destinationNodeTitle[0];
                        
                        if($mappedAction=='edit'){
                            $this->assocDetails[$summary['node_id']]['destinationNode'] = $destinationNode;
                            $this->assocDetails[$summary['node_id']]['destinationTaxonomy'] = $destinationTaxonomy;
                            $this->assocDetails[$summary['node_id']]['associationType'] = $summary['value']['associationType'];
                        }
                        else{
                            $this->summaryReport[$mappedAction][$summary['node_id']]['destinationNode']  = $destinationNode;
                            $this->summaryReport[$mappedAction][$summary['node_id']]['destinationTaxonomy']  = $destinationTaxonomy;
                        }
                    }
                }
            }
            $fetchInternalName = $internalName;
            if(strpos($fetchInternalName, 'subject') !== false){
                $fetchInternalName = 'subjectURI';
                $this->allowAllSubjectFlag = false;
            }
            if(strpos($fetchInternalName, 'license') !== false){
                $fetchInternalName = 'licenseURI';
                $this->allowAllLicenseFlag = false;
            }
            if(strpos($fetchInternalName, 'concept') !== false){
                $fetchInternalName = 'conceptKeywordsURI';
                $this->allowAllConceptFlag = false;
            }
            $metadataSummaryCount = $this->getNodeBasedMetadataDetails($comparisonId, $vntType, $fetchInternalName);
            $this->generateMetadataReport($metadataSummaryCount);
            if($detailsType=='showCount'){
                return ['items'=>array_values($this->metadataCountReport),'document'=>$this->documentMetaCount];
            }
            else{
                return $this->summaryReport;
            }
        }
        catch(\Exception $exception){
            $comparisonError = ['error_type'=>'EXCEPTION', 'error_description'=>$this->createErrorMessageFromException($exception)];
            $this->logComparisonError($comparisonError);
            return [];
        }
    }

    public function getNodeBasedDetails($comparisonId, $type, $vntType='',$nodeTypeId=''){
        $conceptSubjectLicenseDetails = [];
        if($type!='associations'){
            $conceptSubjectLicenseDetails = $this->getConceptSubjectLicenseDetails($comparisonId);
        }
        
        $query = DB::table('version_node_type')
                        ->select('type','node_type','node_type_id','node_id','action','value')
                        ->where('comparison_id',$comparisonId)
                        ->whereIn('type',$vntType);
                        if(!empty($nodeTypeId)){
                            $query->where('node_type_id',$nodeTypeId);
                        }
        $nodeSummary = $query->get()->toArray();
        $nodeSummary = json_decode(json_encode($nodeSummary),true);
        if(!empty($nodeSummary) && !empty($conceptSubjectLicenseDetails)){
            $nodeSummary = array_merge($nodeSummary,$conceptSubjectLicenseDetails);
        }            
        return $nodeSummary;
    }

    public function getNodeBasedMetadataDetails($comparisonId, $vntType, $internalName=''){
        $query = DB::table('version_metadata as vm')
                ->select('vnt.node_id','vm.action','vm.metadata','vm.value','vm.node_id as metadata_node_id','vnt.value as itemdetails','vm.metadta_type')
                ->join('version_node_type as vnt', function($join)
                {
                    $join->on('vm.comparison_id','=','vnt.comparison_id');
                    $join->on('vm.node_type_id','=','vnt.node_type_id');
                    $join->on('vm.node_id','=','vnt.node_id');               
                })
                ->where('vm.comparison_id',$comparisonId)
                ->where('vm.metadata','!=','subject');
        if(!empty($internalName)){
                $query->where('vm.metadata','like', "$internalName%");
        }
                $query->whereIn('vnt.type',$vntType);
                $query->groupby('vnt.node_id','vm.action','vm.metadata');
                
        $metadataSummaryCount = $query->get()->toArray();
        return json_decode(json_encode($metadataSummaryCount),true);
    }

    public function getConceptSubjectLicenseDetails($comparisonId){
        $conceptSubjectLicenseDetails = DB::table('version_node_type')
                        ->select('type','node_type','node_type_id','node_id','action','value')
                        ->where('comparison_id',$comparisonId)
                        ->whereIn('type',['CFConcepts','CFLicenses','CFSubjects'])
                        ->get()->toArray();
        return json_decode(json_encode($conceptSubjectLicenseDetails),true);
    }

    public function generateMetadataReport($metadataSummaryCount){
        $type = $this->cfType;
        $action = $this->action;
        $internalName = $this->internalName;
        $documentDetails = DB::table('version_comparison')->where('comparison_id',$this->comparisonIdentifier)->first();            
        if(!empty($metadataSummaryCount)){
            foreach($metadataSummaryCount as $metasummary){
                $mappedAction = ($metasummary['action']=='N' & $type=='document') ? $action['E'] : $action[$metasummary['action']];
                $summaryMetadata = $metasummary['metadata'];                
                $isDocument = ($metasummary['node_id']==$documentDetails->source_document_id);
                if($metasummary['node_id']!=$documentDetails->source_document_id && $type=='document'){
                    continue;
                }
                if(isset($this->itemDetails[$metasummary['node_id']]['fullStatement'])){
                    $humanCodingScheme = isset($this->itemDetails[$metasummary['node_id']]['humanCodingScheme']) ? $this->itemDetails[$metasummary['node_id']]['humanCodingScheme'] : '';
                    $itemName = $humanCodingScheme.' '.$this->itemDetails[$metasummary['node_id']]['fullStatement'];
                }
                else{
                    $itemName = isset($this->itemDetails[$metasummary['node_id']]['title']) ? $this->itemDetails[$metasummary['node_id']]['title'] : '';
                }

                $updateSummaryMetadata = $this->updateSummaryMetadata($metasummary['metadata'],$summaryMetadata);
                $metasummary['metadata']    = $updateSummaryMetadata['tableMetaData'];
                $summaryMetadata            = $updateSummaryMetadata['summaryMetadata'];

                $summaryMetaValue = json_decode($this->stripSpecialCharacters($metasummary['value']),true);
                if($summaryMetaValue==null && (json_last_error() != JSON_ERROR_NONE)){
                    $comparisonError = ['type'=>$type,'node_id'=>$metasummary['node_id'],'metadata'=>$metasummary['metadata'],'summary_value'=>$metasummary['value'],'error_type'=>json_last_error(),'error_description'=>json_last_error_msg()];
                    $this->logComparisonError($comparisonError);
                }                
                if($type=='metadata'){
                    $this->summaryReport["internal_name"] = $metasummary['metadata'];
                    $this->summaryReport["metadata_name"] = $summaryMetadata;
                    $summaryMetaValue["fullStatement"] = $itemName;
                }
                
                if($summaryMetadata=='Education Level'){
                    $oldValue = is_array($summaryMetaValue['old']) ? implode(',',$summaryMetaValue['old']) : $summaryMetaValue['old'];
                    $newValue = is_array($summaryMetaValue['new']) ? implode(',',$summaryMetaValue['new']) : $summaryMetaValue['new'];
                    $summaryMetaValue['old'] = $oldValue;
                    $summaryMetaValue['new'] = $newValue;
                }
                
                if(($type=='nodetype' || $type=='allMetadata') && !empty($summaryMetaValue))
                {                
                    $summaryMetaValue["internal_name"] = $metasummary['metadata'];
                    $summaryMetaValue["metadata_name"] = $summaryMetadata;
                    $summaryMetaValue["fullStatement"] = $itemName;                                       
                }

                $summaryMetadata = $this->setLicenseMetadataInReport($summaryMetadata, $summaryMetaValue, $mappedAction, $metasummary['node_id'], $itemName, $isDocument);
                $summaryMetadata = $this->setConceptMetadataInReport($summaryMetadata, $summaryMetaValue, $mappedAction, $metasummary['node_id'], $itemName, $isDocument);
                $summaryMetadata = $this->setSubjectMetadataInReport($summaryMetadata, $summaryMetaValue, $mappedAction, $metasummary['node_id'], $itemName, $isDocument);
                
                if($mappedAction=='new'){
                    if(in_array($summaryMetadata,['CFItemTypeURI','CFDocumentURI']) && isset($summaryMetaValue['new']['uri'])){
                        $summaryMetaValue['new'] = $summaryMetaValue['new']['uri'];
                    }
                    if($metasummary['metadta_type']=='exemplar_and_association'){
                        $summaryMetaValue['new'] = $summaryMetaValue['new']['external_node_url'];
                    }                    
                }
                if(!empty($summaryMetadata)){
                    if(isset($summaryMetaValue['internal_name'])){
                        $this->setMetadataCount($summaryMetadata, $summaryMetaValue['internal_name'], $mappedAction);
                    }
                    $summaryMetaValue['old'] = isset($summaryMetaValue['old']['metadata_value']) ? $summaryMetaValue['old']['metadata_value'] : $summaryMetaValue['old'];
                    $summaryMetaValue['new'] = isset($summaryMetaValue['new']['metadata_value']) ? $summaryMetaValue['new']['metadata_value'] : $summaryMetaValue['new'];
                    $this->setDocumentMetadataCount($isDocument);            
                    if($type=='document'){
                        $this->summaryReport[$mappedAction][$summaryMetadata] = $summaryMetaValue;                    
                    }
                    else{
                        if($type=='associations'){
                            $summaryMetaValue['fullStatement'] = isset($this->assocDetails[$metasummary['node_id']]['fullStatement']) ? $this->assocDetails[$metasummary['node_id']]['fullStatement'] : '';
                            $summaryMetaValue['destinationNode'] = isset($this->assocDetails[$metasummary['node_id']]['destinationNode']) ? $this->assocDetails[$metasummary['node_id']]['destinationNode'] : '';
                            $summaryMetaValue['destinationTaxonomy'] = isset($this->assocDetails[$metasummary['node_id']]['destinationTaxonomy']) ? $this->assocDetails[$metasummary['node_id']]['destinationTaxonomy'] : '';
                            $summaryMetaValue['associationType'] = isset($this->assocDetails[$metasummary['node_id']]['associationType']) ? $this->assocDetails[$metasummary['node_id']]['associationType'] : '';
                            $this->summaryReport[$mappedAction][$metasummary['node_id']][$summaryMetadata] = $summaryMetaValue;
                            unset($summaryMetaValue);
                        }
                        else{
                            $this->summaryReport[$mappedAction][$metasummary['node_id']][] = $summaryMetaValue;
                        }                    
                    }
                }
            }
        }
    }    

    public function setMetadataCount($metadataName, $internalName, $action, $count=1)
    {
        if(!isset($this->metadataCountReport[$internalName])){
            $this->metadataCountReport[$internalName] = ['type' => $metadataName,'internal_name' => $internalName,'new' => 0,'edit' => 0,'delete' => 0];
        }
        $this->metadataCountReport[$internalName][$action] = $this->metadataCountReport[$internalName][$action] + $count;
    }

    public function logComparisonError($comparisonError){
        $comparisonError['comparison_id'] = $this->comparisonIdentifier;
        $currentTime = now()->toDateTimeString();
        $comparisonError['created_at'] = $currentTime;
        $comparisonError['updated_at'] = $currentTime;
        DB::table('version_compairison_logs')->insert($comparisonError);
    }

    public function setLicenseMetadataInReport($summaryMetadata, $summaryMetaValue, $mappedAction, $nodeId, $itemName, $isDocument){
        if(strpos($summaryMetadata, 'license') !== false){
            $licensesMaster = [['case_key' => 'uri', 'internal_name' => 'license_uri', "metadata_name" => 'License URI'],
                            ['case_key' => 'title', 'internal_name' => 'license_title', "metadata_name" => 'License Title'],
                            ['case_key' => 'licenseText', 'internal_name' => 'license_licenseText', "metadata_name" => 'License Text'],
                            ['case_key' => 'description', 'internal_name' => 'license_description', "metadata_name" => 'License Description']];
            $licenses = $this->licenses; // License Master
            $summaryLicenseOld = $summaryLicenseNew = ['uri'=>"",'title'=>"",'licenseText'=>"",'description'=>""];
            if(isset($summaryMetaValue['new']['identifier']) && isset($licenses[$summaryMetaValue['new']['identifier']])){
                $summaryLicenseNew = $licenses[$summaryMetaValue['new']['identifier']]['value'];
            }
            if(isset($summaryMetaValue['old']['identifier']) && isset($licenses[$summaryMetaValue['old']['identifier']])){
                $summaryLicenseOld = $licenses[$summaryMetaValue['old']['identifier']]['value'];
            }
                
            foreach($licensesMaster as $licenseMeta){
                if(!$this->allowAllLicenseFlag && $this->internalName!=$licenseMeta['internal_name']){
                    continue;
                }
                if(!isset($summaryLicenseOld[$licenseMeta['case_key']])){
                    $summaryLicenseOld[$licenseMeta['case_key']]="";
                }
                if(!isset($summaryLicenseNew[$licenseMeta['case_key']])){
                    $summaryLicenseNew[$licenseMeta['case_key']]="";
                }
                if($summaryLicenseOld[$licenseMeta['case_key']]!=$summaryLicenseNew[$licenseMeta['case_key']]){
                    $this->setDocumentMetadataCount($isDocument);
                    $this->setMetadataCount($licenseMeta['metadata_name'], $licenseMeta['internal_name'], $mappedAction);                      
                    if($this->cfType=="document"){
                        $this->summaryReport[$mappedAction][$licenseMeta['metadata_name']] = ['old' => $summaryLicenseOld[$licenseMeta['case_key']], "new" => $summaryLicenseNew[$licenseMeta['case_key']], "internal_name" => $licenseMeta['internal_name'], "metadata_name" => $licenseMeta['metadata_name']];                                
                    }
                    else{
                        $this->summaryReport[$mappedAction][$nodeId][] = ['old' => $summaryLicenseOld[$licenseMeta['case_key']], "new" => $summaryLicenseNew[$licenseMeta['case_key']], "internal_name" => $licenseMeta['internal_name'], "metadata_name" => $licenseMeta['metadata_name'],'fullStatement' => $itemName];                            
                    }
                }                
            }
            $summaryMetadata='';
        }
        return $summaryMetadata;
    }

    public function setConceptMetadataInReport($summaryMetadata, $summaryMetaValue, $mappedAction, $nodeId, $itemName, $isDocument){
        if(strpos($summaryMetadata, 'concept') !== false){
            $concepts = $this->concepts;
            $conceptsMaster = 	[['case_key' => 'uri', 'internal_name' => 'concept_uri', "metadata_name" => 'Concept URI'],
                            ['case_key' => 'title', 'internal_name' => 'concept_title', "metadata_name" => 'Concept Title'],
                            ['case_key' => 'keywords', 'internal_name' => 'concept_keywords', "metadata_name" => 'Concept Keywords'],
                            ['case_key' => 'hierarchyCode', 'internal_name' => 'concept_hierarchyCode', "metadata_name" => 'Concept Hierarchy Code'],
                            ['case_key' => 'description', 'internal_name' => 'concept_description', "metadata_name" => 'Concept Description']];
            $summaryConceptNew = $summaryConceptOld = ['uri'=>"",'title'=>"",'keywords'=>"",'hierarchyCode'=>"",'description'=>""];
                        
            if(isset($summaryMetaValue['new']['identifier']) && isset($concepts[$summaryMetaValue['new']['identifier']])){
                $summaryConceptNew = $concepts[$summaryMetaValue['new']['identifier']]['value'];
            }
            if(isset($summaryMetaValue['old']['identifier']) && isset($concepts[$summaryMetaValue['old']['identifier']])){
                $summaryConceptOld = $concepts[$summaryMetaValue['old']['identifier']]['value'];
            }
            
            foreach($conceptsMaster as $conceptMeta){
                if(!$this->allowAllConceptFlag && $this->internalName!=$conceptMeta['internal_name']){
                    continue;
                }
                if(isset($summaryConceptOld[$conceptMeta['case_key']]) && isset($summaryConceptNew[$conceptMeta['case_key']]) && $summaryConceptOld[$conceptMeta['case_key']]!=$summaryConceptNew[$conceptMeta['case_key']]){                    
                    $this->summaryReport[$mappedAction][$nodeId][] = ['old' => $summaryConceptOld[$conceptMeta['case_key']], "new" => $summaryConceptNew[$conceptMeta['case_key']], "internal_name" => $conceptMeta['internal_name'], "metadata_name" => $conceptMeta['metadata_name'],'fullStatement' => $itemName];
                    $this->setMetadataCount($conceptMeta['metadata_name'], $conceptMeta['internal_name'], $mappedAction);
                }
            }
            $summaryMetadata='';
        }
        return $summaryMetadata;
    }

    public function setSubjectMetadataInReport($summaryMetadata, $summaryMetaValue, $mappedAction, $nodeId, $itemName, $isDocument){
        if($summaryMetadata == 'subjectURI'){
            $subjects = $this->subjects;
            $subjectsMaster = 	[['case_key' => 'uri', 'internal_name' => 'subject_uri', "metadata_name" => 'Subject URI'],
                            ['case_key' => 'title', 'internal_name' => 'subject_title', "metadata_name" => 'Subject Title'],
                            ['case_key' => 'hierarchyCode', 'internal_name' => 'subject_hierarchyCode', "metadata_name" => 'Subject Hierarchy Code'],
                            ['case_key' => 'description', 'internal_name' => 'subject_description', "metadata_name" => 'Subject Description']];
            $summarySubjectNew = $summarySubjectOld = ['uri'=>"",'title'=>"",'hierarchyCode'=>"",'description'=>""];
            $subjectNodeidentifierList = [];
            if(!empty($summaryMetaValue['new'])){
                foreach($summaryMetaValue['new'] as $subjectNodeNewValue){
                    $subjectNodeidentifier = $subjectNodeNewValue['identifier'];
                    if(isset($subjects[$subjectNodeidentifier])){
                        $summarySubjectNew[$subjectNodeidentifier] = $subjects[$subjectNodeidentifier]['value'];
                        $subjectNodeidentifierList[] = $subjectNodeidentifier;
                    }
                }
            }
            
            if(!empty($summaryMetaValue['old'])){
                foreach($summaryMetaValue['old'] as $subjectNodeOldValue){                    
                    $subjectNodeidentifier = $subjectNodeOldValue['identifier'];
                    if(isset($subjects[$subjectNodeidentifier])){
                        $summarySubjectOld[$subjectNodeidentifier] = $subjects[$subjectNodeidentifier]['value'];
                        $subjectNodeidentifierList[] = $subjectNodeidentifier;
                    }
                }
            }
            foreach($subjectsMaster as $subjectMeta){     
                if(!$this->allowAllSubjectFlag && $this->internalName!=$subjectMeta['internal_name']){
                    continue;
                }
                foreach($subjectNodeidentifierList as $subjectList){
                    $summarySubjectOldValue = isset($summarySubjectOld[$subjectList][$subjectMeta['case_key']]) ? $summarySubjectOld[$subjectList][$subjectMeta['case_key']] : '';
                    $summarySubjectNewValue = isset($summarySubjectNew[$subjectList][$subjectMeta['case_key']]) ? $summarySubjectNew[$subjectList][$subjectMeta['case_key']] : '';
                    
                    if($summarySubjectOldValue!=$summarySubjectNewValue){
                        $this->setDocumentMetadataCount($isDocument);
                        $this->summaryReport[$mappedAction][$subjectMeta['metadata_name']]['subjectList'][$subjectList] = ['old' => $summarySubjectOldValue, "new" => $summarySubjectNewValue];
                        $this->summaryReport[$mappedAction][$subjectMeta['metadata_name']]["internal_name"] = $subjectMeta['internal_name'];
                        $this->summaryReport[$mappedAction][$subjectMeta['metadata_name']]["metadata_name"] = $subjectMeta['metadata_name'];
                        if($this->internalName == $subjectMeta['internal_name']){
                            
                            $this->summaryReport["internal_name"] = $subjectMeta['internal_name'];
                            $this->summaryReport["metadata_name"] = $subjectMeta['metadata_name'];
                        }
                        $this->setMetadataCount($subjectMeta['metadata_name'], $subjectMeta['internal_name'], $mappedAction);
                    }
                }
            }
            $summaryMetadata='';
        }
        return $summaryMetadata;
    }

    public function updateSummaryMetadata($tableMetaData,$summaryMetadata){
        if(strpos($tableMetaData, 'educationLevel') !== false){
            $tableMetaData = $summaryMetadata = 'educationLevel';
        }
        if(strpos($tableMetaData, 'subjectURI,0,uri') !== false){
            $tableMetaData = $summaryMetadata = 'SubjectURI';
        }
        $summaryMetadata = str_replace(',',' - ',$summaryMetadata);
        if(isset($this->metadataMapping[$summaryMetadata])){
            $summaryMetadata = $this->metadataMapping[$summaryMetadata];
        }
        return ['tableMetaData' => $tableMetaData, 'summaryMetadata' => $summaryMetadata];
    }
    public function setDocumentMetadataCount($isDocument){
        if($isDocument){
            $this->documentMetaCount = $this->documentMetaCount+1;
        }
    }

}
