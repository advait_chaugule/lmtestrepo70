<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\TaxonomyPubStateHistory;

class GetSubscribedTaxonomyVersionJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $documentId;

    public function __construct($documentId)
    {
        $this->documentId = $documentId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $documentId = $this->documentId;

        $publishedVersion = TaxonomyPubStateHistory::select('publish_number','tag','publish_date')->where([
            'document_id' => $documentId,
            'is_deleted' => 0
            ])->get()->toArray();
        $publishedVersionArray = [];
        if(!empty($publishedVersion)){
			// Made changes as per UF-3344 Concating version - tag -publishdate
            foreach($publishedVersion as $key =>$pubHistory){
                $displayPublishNum = $pubHistory['publish_number'].' - '.(!empty($pubHistory['tag']) ? $pubHistory['tag'].' - ': '').$pubHistory['publish_date'];
                $publishedVersionArray[] = ['publish_number' => $pubHistory['publish_number'], 'displayPublishNum' => $displayPublishNum];
            }
			// Made changes as per UF-3344 Concating version - tag -publishdate          
        }
        return $publishedVersionArray;
    }

}
