<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;
use App\Data\Models\ProjectItem;
use App\Data\Models\Thread;
use App\Data\Models\Item;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;
use DB;

class GetProjectTreeV2Job extends Job
{
    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $requestUserDetails;

    private $document;
    private $items;
    private $itemAssociationsUnderDocument;
    private $projectIdsAssociated;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;
    private $parentAndDocumentId;

    private $transformedDocument;
    private $transformedItems;
    private $transformedItemAssociationsUnderDocument;

    private $tree;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestUserDetails,string $projectId)
    {
        $this->requestUserDetails = $requestUserDetails;
        $this->projectIdentifier = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;


        
        //$documentIdentifier =   $this->documentIdentifier;
        $input              =   $this->requestUserDetails;
       
        $organizationId     =   $input["organization_id"];
        $userId     =   $input["user_id"];
        $searchProjectItemArray = [];
        $finalList = [];
        
        $projectItems = ProjectItem::where('project_id',$this->projectIdentifier)->where('is_deleted','=',0)->get();
        if(count($projectItems) > 0)
        {
            foreach($projectItems as $projectItem) {
                array_push($searchProjectItemArray,$projectItem->item_id);
                $projectItemEditable[$projectItem->item_id] = $projectItem->is_editable;

            }
        }
        if(!empty($searchProjectItemArray))
        {
            $getDocument = Item::select('document_id')->whereIn('item_id',$searchProjectItemArray)->first()->toArray();
        }
        else
        {
            $getDocument = DB::table('projects')->select('document_id')->where('project_id',$this->projectIdentifier)->get()->toArray();
            $getDocument['document_id'] = $getDocument[0]->document_id;
        }
       
        $documentIdentifier = $getDocument['document_id'];
       
        //Get Document Details
        $returnCollection           =   false;
        $fieldsToReturn             =   ["document_id", "title", "node_type_id", "project_id", "node_type_id", "adoption_status","title_html", "updated_at", "language_id"];
        $relations                  =   ['nodeType'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];

        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $nodeType = !empty($document->nodeType->node_type_id) ? $document->nodeType->title : "";
        $parsedDocument = [];
        $isDocumentEditable = 0;
        $isDocumentEnabled = 0;
        /**
         * Don't remove the below  if statement else 
         * the document node will be editable in multiple project.
         */
        if(!empty($document->project_id) && $document->project_id == $this->projectIdentifier)
        {
            $isDocumentEditable = 1;
        }
        if(!empty($document->project_id))
        {
            $isDocumentEnabled = 1;
        }
        $searchItemArray = [];
        array_push($searchProjectItemArray,$document->document_id);
        $openCommentCountList = DB::table('threads')->select(array('thread_source_id',DB::raw('COUNT(thread_id) as comment_count')))->whereIn('thread_source_id',$searchProjectItemArray)->where('status','=',1)->where('is_deleted','=',0)->groupBy('thread_source_id')->get();
       
        $projectItemCommentCount = [];
        if(count($openCommentCountList) > 0)
        {
            foreach($openCommentCountList as $openCommentCount) {
               
                $projectItemCommentCount[$openCommentCount->thread_source_id] = $openCommentCount->comment_count;

            }
        }
        
        /********** Missing language for Document resolved : UF-1820 ********/
        $languageName = ''; 
        if(!empty($document->language_id)){
            $language = DB::table('languages')->select('name')->where(['language_id'=>$document->language_id,'is_deleted'=>0])->first();
            if(isset($language->name)){
                $languageName = $language->name; 
            }
        }
        /********** Missing language for Document resolved : UF-1820 ********/
  
            $parsedDocument = [
                "id"                    =>  $document->document_id,
                "title"                 =>  !empty($document->title) ? $document->title : "",
                "title_html"            =>  !empty($document->title_html) ? $document->title_html : "",
                "human_coding_scheme"   =>  "",
                "list_enumeration"      =>  "",
                "sequence_number"      =>  "",
                "full_statement"        =>  "",
                "status"                =>  $document->adoption_status,    
                "node_type"             =>  $nodeType,
                "metadataType"          =>  $nodeType,
                "node_type_id"          =>  $document->node_type_id,
                "is_editable"           =>  $isDocumentEditable,
                "project_enabled"       =>  $isDocumentEnabled,
                "is_document"           =>  1,
                "open_comment_count"    =>  !empty($projectItemCommentCount[$document->document_id]) ? $projectItemCommentCount[$document->document_id] : 0,
                "is_orphan"             =>  0,
                "language_name"         => $languageName
            ];
       
        
        
        //Get Item Details
        $returnCollection           =   true;
        $fieldsToReturn             =   ["item_id", "human_coding_scheme", "list_enumeration","full_statement", "node_type_id", "source_item_id","human_coding_scheme_html","full_statement_html"];
        $relations                  =   ['nodeType', 'customMetadata'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];
        
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
                //Relation for taxonomy
                $returnCollection           =   true;
                 //$fieldsToReturn             =   ["origin_node_id", "destination_node_id","sequence_number"];
               $fieldsToReturn             =   ["source_item_id", "target_item_id","sequence_number"];
                $conditionalKeyValuePair    =   [ 
                    "source_document_id" =>  $documentIdentifier,
                    "organization_id" => $organizationId,
                    "is_deleted" => 0,
                    "association_type" => 1 // 1 for isChildOf relation
                ];
        
                $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                                    $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                                 );
             $itemSequenceList = [];
             $allItemList = [];                                       
        foreach($itemAssociationsUnderDocument as $itemAssociation) 
        {
                 $itemSequenceList[$itemAssociation->source_item_id] = $itemAssociation->sequence_number;
        }

         // get User Setting for Project
         $projectArr =[];
         //Check users project setting is exist in user_setting table or not
         $getUserSetting = $this->getProjectSetting($this->projectIdentifier,$organizationId,$userId);
       
         if($getUserSetting) {
            if ($getUserSetting->json_config_value) {
                $userJsonArr = json_decode($getUserSetting->json_config_value, true);
                if (!empty($userJsonArr) && isset($userJsonArr['last_table_view'])) {
                    $lastTableName = $userJsonArr['last_table_view'];
                    foreach ($userJsonArr['project_table_views'] as $userJsonArrK => $userJsonArrV) {
                        $checkUserSettingStatus = $userJsonArrV['is_user_default'];
                        $savedTableName = $userJsonArrV['table_name'];
                        $view_type = $userJsonArrV['view_type'];
                        if ($checkUserSettingStatus == 1 && $savedTableName == $lastTableName) {
                            foreach ($userJsonArrV['table_config'] as $userJsonArrVK => $userMetadataList) {
                                $projectArr[] = $userMetadataList;
                            }
                        }
                    }
                }
            }
        }
        else{
			// Get project settings marked as default for all users accessing this project
            $getProjectSetting = $this->getProjectDefaultSetting($this->projectIdentifier, $organizationId);
            if (!empty($getProjectSetting->display_options)) {
                $projectJsonArr = json_decode($getProjectSetting->display_options, true);
                $projectTable = $projectJsonArr['last_table_view'];
                $view_type = $projectJsonArr['project_table_views']['view_type'];
                if ($projectJsonArr['project_table_views']['default_for_all_user'] == 1 && $projectJsonArr['project_table_views']['table_name']==$projectTable) {
                        $tableConfig = $projectJsonArr['project_table_views']['table_config'];
                        $projectArr = $tableConfig;
                }
            }
        }
        // Check for new colums except default
        
        $metadataIds=[];
        $InternalNameCustom=[];
        $internalNames=[];

        if(!empty($projectArr)){
            if($view_type == "metadata"){
                foreach($projectArr as $internalNameK =>$internalNameV) {
                    if(isset($internalNameV['is_custom']) && $internalNameV['is_custom']==1) {
                        $InternalNameCustom[$internalNameV['metadata_id']] = $internalNameV['internal_name']; //Modified code to get as per mapped metadata_id & name
                        $metadataIds[] = $internalNameV['metadata_id'];
                    }else{
                        $internalNames[] = $internalNameV['internal_name'];
                    }
                }

            }
            if($view_type == "node_type"){
                foreach($projectArr as $projectArrk =>$projectArrV) {
                    $internalMetaData = $projectArrV['metadata'];
                    foreach($internalMetaData as $internalMetaDatak=>$internalMetaDataV) {
                        if(isset($internalMetaDataV['is_custom']) && $internalMetaDataV['is_custom']==1) {
                            $InternalNameCustom[$internalMetaDataV['metadata_id']] = $internalMetaDataV['internal_name']; //Modified code to get as per mapped metadata_id & name
                            $metadataIds[] = $internalMetaDataV['metadata_id'];
                        }else{
                            $internalNames[] = $internalMetaDataV['internal_name'];
                        }
                }
                }
           }

            $defaultarr = ['full_statement','node_type','human_coding_scheme'];
            foreach ($internalNames as $internalNamesk=>$internalNamesV)
            {
                if(in_array($internalNamesV,$defaultarr)) {
                    unset($internalNames[$internalNamesk]);
                }
            }

        }

        $fieldNames =[];
        $fieldNames[] = 'project_items.item_id as id';
        $finalitems = [];
		$itemlist = [];
        if(!empty($internalNames)) {
            foreach ($internalNames as $key=>$value) {
                if(strpos($value,'_')!==false){
                    $posnum = strpos($value,'_');
                    $table_name  = substr($value,0,$posnum);
                    $column_name = substr($value,$posnum+1);
                }else{
                  $table_name    = "Default";
                }
                switch($table_name){
                    case "concept":
                        if($column_name == "title"){
                            $alias = 'concept_title';
                        }else if($column_name == "description"){
                            $alias = 'concept_description';
                        }else if($column_name == "hierarchy_code"){
                            $alias = 'concept_hierarchy_code';
                        }else if($column_name == "keywords"){
                            $alias = 'concept_keywords';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'concepts.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    case "license":
                        if($column_name == "title"){
                            $alias = 'license_title';
                        }else if($column_name == "description"){
                            $alias = 'license_description';
                        }else if($column_name == "hierarchy_code"){
                            $alias = 'license_hierarchy_code';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'licenses.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;

                    case "items":
                        if($column_name == "start_date"){
                            $alias = 'items_status_start_date';
                        }else if($column_name == "end_date"){
                            $alias = 'items_status_end_date';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'items.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;

                    case "language":
                        if($column_name == "name"){
                            $alias = 'language_name';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'languages.'.$column_name.' as '.$alias;
                        }else{
                           $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                        
                    default:
                        $fieldNames[] = $value;
                }
            }

           
            $getFieldValues = $this->getFieldData($fieldNames,$organizationId,$this->projectIdentifier);
            

            foreach($getFieldValues as $getFieldValuesK =>$getFieldValuesV)
            {
                $finalList[$getFieldValuesV->id] = (array)$getFieldValuesV;

            }  
           
        }

        if(!empty($metadataIds)){
            $fieldnamesCustom[] =  'item_metadata.item_id';

            foreach($InternalNameCustom as $InternalNameCustomk => $InternalNameCustomV) {
                    $fieldnamesCustom [$InternalNameCustomk] = $InternalNameCustomV; //Modified code to get as per mapped metadata_id & name
            } 
            $getcustomitems = DB::table('item_metadata')
                    ->select('item_id as id', 'metadata_id', 'metadata_value')
                    ->whereIn('metadata_id', $metadataIds)
                    ->get()
                    ->toarray(); 
            $getcustomitems = json_decode(json_encode($getcustomitems),true); // To convert all child nodes to array
            foreach($getcustomitems as $getcustomitemsK =>$getcustomitemsV)
            {
                $getcustomitemsV[$fieldnamesCustom[$getcustomitemsV['metadata_id']]]=$getcustomitemsV['metadata_value'];
                unset($getcustomitemsV['metadata_id']);
                unset($getcustomitemsV['metadata_value']);
                if(isset($finalList[$getcustomitemsV['id']])){

                    $finalList[$getcustomitemsV['id']] = array_merge($finalList[$getcustomitemsV['id']],$getcustomitemsV);
                }
                else{
                    $finalList[$getcustomitemsV['id']] = $getcustomitemsV;
                }
            }              
        }

        foreach($items as $item) {
            $itemlist = []; // Reinitialize to resolve the issue of duplocate custom data
            if(in_array($item->item_id,$searchProjectItemArray))
            {
                $searchItemArray =
                [
                    "id" => $item->item_id,
                    "title" => "",
                    "human_coding_scheme"   => !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "",
                    "human_coding_scheme_html"   => !empty($item->human_coding_scheme_html) ? $item->human_coding_scheme_html : $item->human_coding_scheme,
                    "list_enumeration"      => !empty($item->list_enumeration) ? $item->list_enumeration : "",
                    "sequence_number"       => !empty($itemSequenceList[$item->item_id]) ? $itemSequenceList[$item->item_id] : "",
                    "full_statement"        => !empty($item->full_statement) ? $item->full_statement : "",
                    "full_statement_html"   => !empty($item->full_statement_html) ? $item->full_statement_html : $item->full_statement,
                    "node_type"             => !empty($item->nodeType) ? $item->nodeType->title : "",
                    "metadataType"          => !empty($item->nodeType) ? $item->nodeType->title : "",
                    "node_type_id"          => !empty($item->node_type_id) ? $item->node_type_id : "",
                    "item_type"             => "",
                    "is_document"           => 0,
                    "is_editable"           => $projectItemEditable[$item->item_id],
                    "item_usage_count"      => 0,
                    "open_comment_count"    => !empty($projectItemCommentCount[$item->item_id]) ? $projectItemCommentCount[$item->item_id] : 0,
                ];


            }
            if(!empty($finalList)){
                if(array_key_exists($item->item_id,$finalList))
                {
                    $itemlist = $finalList[$item->item_id];
                }
            }
           
           array_push($allItemList,$item->item_id);   
        
            if(!empty($itemlist)){
                $finalitems[] =  array_merge($searchItemArray,$itemlist);
            }else{
                $finalitems[] = $searchItemArray;
            }
        }
        array_push($allItemList,$documentIdentifier);

        $parsedItemAssociations = [];
        $parsedOrginNodeItemArr = []; 
           /* This code for handle the orphaned node condition ACMT-953*/
           $itemArr = [];
           if(!empty($finalitems))
           {    
               foreach($finalitems as $finalitemsK => $finalitemsV)
               {
                   if(isset($finalitemsV['id'])){
                     array_push($itemArr,$finalitemsV['id']);
                   }
               }
           }
           if(!empty($document->project_id))
           {
               array_push($itemArr,$document->document_id);
           }

        foreach($itemAssociationsUnderDocument as $itemAssociation)
        {
            $originNodeId = $itemAssociation->source_item_id;
            $destinationNodeId = $itemAssociation->target_item_id;

                if(!empty($originNodeId) && !empty($destinationNodeId) && ($originNodeId != $destinationNodeId)) 
                {
                    if(in_array($originNodeId,$searchProjectItemArray) && (in_array($destinationNodeId,$searchProjectItemArray) || $destinationNodeId == $document->document_id))
                    {
                        $parsedItemAssociations[] = [
                            "child_id" => $itemAssociation->source_item_id,
                            "parent_id" => $itemAssociation->target_item_id,
                            "sequence_number" => $itemAssociation->sequence_number
                        ]; 

                        array_push($parsedOrginNodeItemArr,$itemAssociation->source_item_id);
                        array_push($parsedOrginNodeItemArr,$itemAssociation->target_item_id);  
                    }
            
                }
        }
        $orphanedNodeArr=array_diff($searchProjectItemArray,$parsedOrginNodeItemArr);
        
        if(!empty($finalitems))
        {    
            foreach($finalitems as $finalitemsK => $finalitemsV)
            {
               if(isset($finalitemsV['id'])){
                if(in_array($finalitemsV['id'],$orphanedNodeArr))
                   {
                       $finalitems[$finalitemsK]['is_orphan'] = 1;
                   }
                   else {
                       $finalitems[$finalitemsK]['is_orphan'] = 0;
                   }
                }
            }
        }
       
        if(!empty($parsedItemAssociations))
        {
            foreach($parsedItemAssociations as $parsedItemAssociationsK => $parsedItemAssociationsV)
            {
                if(in_array($parsedItemAssociationsV['parent_id'],$allItemList))
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = false;
                }
                else
                {
                    unset($parsedItemAssociations[$parsedItemAssociationsK]);
                }
            }
        } 
        /* End */                       
        if(!empty($finalitems))
        {
            if(!empty($document->project_id))
            {
                $nodes = array_prepend($finalitems, $parsedDocument);
            }
            else
            {
                $nodes = $finalitems;
            }
            
        }
        else
        {
            if(!empty($document->project_id))
            {
                $nodes[] = $parsedDocument;
            }
            else
            {
                $nodes = [];
            }
            
        }                                                       
        
        $treeData = [
            "nodes" => $nodes,
            "relations" => $parsedItemAssociations,
            "modified_at" =>  $document->updated_at,
        ];
        return $treeData;
        
    }
    
     /**
     * @param $projectId
     * @param $organizationId
     * @return mixed
     * @FunctionName getProjectSetting
     */
    private function getProjectSetting($projectId,$organizationId,$userId)
    {
          $projectSetting = DB::table('user_settings')->select('json_config_value')
                              ->where('id',$projectId)
                              ->where('user_id',$userId)
                              ->where('organization_id',$organizationId)
                              ->where('id_type',4)
                              ->where('is_deleted',0)
                              ->get()
                              ->first();
          return $projectSetting;
    }

    /**
     * @param $projectId
     * @param $organizationId
     * @return mixed
     * @FunctionName getProjectDefaultSetting
     */
    private function getProjectDefaultSetting($projectId,$organizationId)
    {
          $getProjectJson =  DB::table('projects')->select('display_options')
                               ->where('project_id',$projectId)
                               ->where('organization_id',$organizationId)
                               ->where('is_deleted',0)
                               ->get()
                               ->first();
          return $getProjectJson;

    }

     /**
     * @param $internalNames
     * @param $projectId
     * @return mixed
     */

    private function getFieldData($internalNames,$organizationId,$projectId)
    {
        $queryData = DB::table('project_items')
            ->select($internalNames)
            ->leftjoin('items','items.item_id','=','project_items.item_id')
            ->leftJoin('concepts', function ($join) {
                $join->on('concepts.concept_id','=','items.concept_id')
                     ->where('concepts.is_deleted', '=', 0);
            })
            ->leftJoin('licenses', function ($join) {
                $join->on('licenses.license_id','=','items.license_id')
                     ->where('licenses.is_deleted', '=', 0);
            })
            ->leftJoin('languages', function ($join) {
                $join->on('languages.language_id','=','items.language_id')
                     ->where('languages.is_deleted', '=', 0);
            })
            ->where('project_items.project_id',$projectId)
            ->where('items.organization_id',$organizationId)
            ->where('items.is_deleted',0)
            ->get()
            ->toArray();
            
        return $queryData;
    }

  
}
