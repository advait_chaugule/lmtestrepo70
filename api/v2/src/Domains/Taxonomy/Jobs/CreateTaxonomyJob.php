<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

use App\Data\Models\Document;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Domains\Search\Events\UploadSearchDataToSqsEvent;

use App\Data\Models\Organization;

class CreateTaxonomyJob extends Job
{
    use ErrorMessageHelper, StringHelper, DateHelpersTrait, UuidHelperTrait,CaseFrameworkTrait;
    private $documentRepository;
    private $itemRepository;
    private $itemAssociationRepository;
    private $userOrganizationId;
    private $languageRepository;

    private $savedLanguages;
    private $organizationCode;
    private $savedDocumentObject;
    private $inputData;
    private $domainName;

    /**
     * CreateTaxonomyJob constructor.
     * @param array $input
     * @param $requestUrl
     */
    public function __construct(array $input,$requestUrl) {
        $this->inputData    =   $input;
        $this->domainName   =   $requestUrl;
    }

    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        LanguageRepositoryInterface $languageRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        Request $request
    )
    {
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->languageRepository = $languageRepository;
        $this->nodeTypeRepository   =   $nodeTypeRepository;
        
        $this->extractAndSetUserOrganizationIdFromRequestBody($request);
        $this->extractAndSetUserIdFromRequestBody($request);

        $this->fetchAndSetSavedLanguages();

        $this->extractRequestData($request);

        $this->raiseEventToUploadTaxonomySearchDataToSqs();
               
        return $this->getSavedDocumentObject()->document_id;
    }
    public function setUserOrganizationId(string $data) {
        $this->userOrganizationId = $data;
    }

    public function getUserOrganizationId(): string {
        return $this->userOrganizationId;
    }
    public function setUserId(string $data) {
        $this->userId = $data;
    }

    public function getUserId(): string {
        return $this->userId;
    }

    public function setSavedLanguages($data) {
        $this->savedLanguages = $data;
    }

    public function getSavedLanguages(){
        return $this->savedLanguages;
    }

    public function setSavedDocumentObject(Document $document) {
        $this->savedDocumentObject = $document;
    }

    public function getSavedDocumentObject() {
        return $this->savedDocumentObject;
    }

    public function setDocument($data) {
        $this->document = $data;
    }

    public function getDocument() {
        return $this->document;
    }
    public function setOrgCode(string $data) {
        
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        return $this->organizationCode;
    }


    private function extractAndSetUserOrganizationIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userOrganizationId = $userPerformingAction["organization_id"];
        $this->setOrgCode($userOrganizationId);
        $this->setUserOrganizationId($userOrganizationId);
    }

    private function extractAndSetUserIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userId = $userPerformingAction["user_id"];
        $this->setUserId($userId);
    }


    private function fetchAndSetSavedLanguages() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("language");
        $this->setSavedLanguages($savedData);
    }

    /************** All helper methods specific to this import functionality only **************/

    private function helperToFetchTenantSpecificDataOfProvidedType(string $type) {
        $organizationId = $this->getUserOrganizationId();
        $dataFetchingConditionalClause = [ "organization_id" => $organizationId ];
        $returnCollection = true;
        switch ($type) {
            case 'language':
                $fieldsToReturn = ['language_id', 'short_code'];
                $repository = $this->languageRepository;
                break;
            default:
                $repository = false;
                break;
        }
        $dataToReturn = $repository!==false ? 
                        $repository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $dataFetchingConditionalClause) : 
                        collect([]);
        return $dataToReturn;
    }

    private function helperToFetchLanguageIdFromEitherExistingOrNew(string $languageToSearchFor): string {
        $savedLanguages = $this->getSavedLanguages();
        $searchResult = $savedLanguages->where("short_code", $languageToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $language = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getUserOrganizationId();
            $newLanguageIdentifier = $this->createUniversalUniqueIdentifier();
            $languageDataToSave = [
                "language_id" => $newLanguageIdentifier,
                "name" => ucwords($languageToSearchFor),
                "short_code" => $languageToSearchFor,
                "organization_id" => $organizationId
            ];
            // save record to db
            $language = $this->languageRepository->saveData($languageDataToSave);
            // update the exiting inmemory list
            $savedLanguages->push($language);
        }
        return $language->language_id;
    }
    
    private function extractRequestData($request) {
        $this->extractSaveDocument($request);
        if(isset($request['document_type']) && $request['document_type'] == '1') {
            $this->extractSaveItems($request);
        }
    }


    private function extractSaveDocument($request){
        $userId = $this->getUserId();
        $currentDateTime = $this->createDateTime();
        $userOrganizationId = $this->getUserOrganizationId();
        $organizationName = DB::table('organizations')->select('name')->where('organization_id', $userOrganizationId)->first()->name;
        $orgCode          =   $this->getOrgCode();
        $languageId =  !empty($request['language_id']) ? 
                        $this->helperToFetchLanguageIdFromEitherExistingOrNew('en') : 
                        "";

        $data = [
            "document_id" => $this->inputData['document_id'],
            "title" => $request['document_title'],
            "official_source_url" => '',
            "adoption_status" => 2,// Adoption Status changed to 2 for Draft as per UF-3498
            "notes" => '',
            "publisher" =>  '',
            "description" => '',
            "version" => '',
            "status_start_date" => '',
            "status_end_date" => '',
            "creator" => $organizationName,
            "url_name" => '',
            "language_id" =>  $languageId,
            "license_id" => '',
            "organization_id" => $userOrganizationId,
            "project_id"       => !empty($this->inputData['project_id']) ? $this->inputData['project_id'] : "",
            "document_type" => $request['document_type'],
            "created_at" => $currentDateTime,
            "updated_at" => $currentDateTime,
            "source_document_id"=>$this->inputData['document_id'],
            "node_type_id"=>$request['document_node_type_id'],
            "node_template_id"=>!empty($request['node_template_id']) ? $request['node_template_id'] : '',
            "updated_by" => $userId,
            "import_type"=> 2,
            "actual_import_type" =>2,     //for import type column view
            "uri" => $this->getCaseApiUri("CFDocuments",$request['document_id'],true,$orgCode,$this->domainName),
            "created_by" => $userId,
            "source_type"=> 4,         //source_type = 4 for Builder
        ];

        $document = $this->documentRepository->saveData($data); 
        $this->setSavedDocumentObject($document);
        $this->setDocument($document);
        $this->fetchAndSetCustomMetadataAssociatedToDocument();
    }

    private function extractSaveItems($request) {
        $items = $request->input('items');
        $documentId = $request['document_id'];
        //$languageId = $request['language_id'];
        $orgCode =   $this->getOrgCode();
        $userId = $this->getUserId();
        $currentDateTime = $this->createDateTime();
        $userOrganizationId = $this->getUserOrganizationId();
        foreach($items as $data) {
            $languageId =  !empty($request['language_id']) ? 
                        $this->helperToFetchLanguageIdFromEitherExistingOrNew('en') : 
                        "";
            $item_id = $data['item_id'];
            $parentId = !empty($data["parent_id"]) ? $data["parent_id"] : $documentId;
            $fullStatement = !empty($data["full_statement"]) ? $data["full_statement"] : "";
            $alternativeLabel = "";
            $itemTypeId = !empty($data["node_type_id"]) ? $data["node_type_id"] : "";
            $humanCodingScheme = !empty($data["human_coding_scheme"]) ? $data["human_coding_scheme"] : "";
            $listEnumeration = !empty($data["list_enumeration"]) ? $data["list_enumeration"] : "";
            $abbreviatedStatement = "";
            $conceptId = "";
            $notes = "";
            $educationLevel = "";
            $licenseId = "";
            $statusStartDate = null;
            $statusEndDate = null;
            $updatedAt = null;
            $itemsToSave[]= [
                "item_id" => $item_id,
                "parent_id" => $parentId,
                "document_id" => $documentId,
                "full_statement" => $fullStatement,
                "alternative_label" => $alternativeLabel,
                "human_coding_scheme" => $humanCodingScheme,
                "list_enumeration" => $listEnumeration,
                "abbreviated_statement" => $abbreviatedStatement,
                "notes" => $notes,
                "node_type_id" => $itemTypeId,
                "concept_id" => $conceptId,
                "language_id" => $languageId,
                "education_level" => $educationLevel,
                "license_id" => $licenseId,
                "status_start_date" => $statusStartDate,
                "status_end_date" => $statusEndDate,
                "created_at" => $currentDateTime,
                "updated_at" => $updatedAt,
                "organization_id" => $userOrganizationId,
                "source_item_id" => $item_id,
                "updated_by" => $userId,
                "uri" =>$this->getCaseApiUri("CFItems",$item_id,true,$orgCode,$this->domainName),
                "import_type" =>2,
            ];
            $itemAssociationID = $this->createUniversalUniqueIdentifier();
            $caseAssociationsToSave[] = [
                "item_association_id" => $itemAssociationID,
                "association_type" => 1,
                "document_id" => $documentId,
                "association_group_id" => '',
                "origin_node_id" => $item_id,
                "destination_node_id" => $parentId,
                "sequence_number" => $listEnumeration,
                "external_node_title" =>"",
                "external_node_url" =>"",
                "created_at" => $currentDateTime,
                "updated_at" => $updatedAt,
                "source_item_association_id" => $itemAssociationID,
                "organization_id" => $userOrganizationId,
                "uri"=>$this->getCaseApiUri("CFAssociationGroupings",$itemAssociationID,true,$orgCode,$this->domainName)
            ];
        }
        
        if(!empty($itemsToSave)){
            $this->itemRepository->saveMultiple($itemsToSave);
            $this->itemAssociationRepository->saveMultiple($caseAssociationsToSave);
        }

    }

    private function raiseEventToUploadTaxonomySearchDataToSqs() {
        $documentId = $this->getSavedDocumentObject()->document_id;
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["taxonomy_created"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }

    private function fetchAndSetCustomMetadataAssociatedToDocument() {
        $customMetadataAssociatedToDocumentArray    =   [];
        $savedDocument                      =   $this->getDocument();
        $nodeTypeMetadata                   =   $this->nodeTypeRepository->getNodeTypeMetadata($savedDocument->node_type_id);
        $customMetadataAssociatedToDocument =   $savedDocument->customMetadata()->where(['is_active'  =>  '1', 'is_deleted'    => '0'])->get();

        foreach($customMetadataAssociatedToDocument as $customMetadata) {
            $customMetadataAssociatedToDocumentArray[] = $customMetadata->metadata_id;
        }

        foreach($nodeTypeMetadata->metadata as $metadata) {
            if(sizeOf($customMetadataAssociatedToDocumentArray) > 0) {
                if($metadata->is_custom == 1) {
                    if(in_array($metadata->metadata_id, $customMetadataAssociatedToDocumentArray) === false) {
                        $isAdditional = 0;
                        $this->documentRepository->addOrUpdateCustomMetadataValue($savedDocument->document_id, $metadata->metadata_id, '','', $isAdditional, 'insert');
                    }
                }
            } else {
                if($metadata->is_custom == 1) {
                    $isAdditional = 0;
                    $this->documentRepository->addOrUpdateCustomMetadataValue($savedDocument->document_id, $metadata->metadata_id, '','', $isAdditional, 'insert');
                }
            }
        }

        // $customMetadataAssociatedToDocument =   $savedDocument->customMetadata()->where(['is_active'  =>  '1', 'is_deleted'    => '0'])->get();
        
        // if($customMetadataAssociatedToDocument->isNotEmpty()) {
        //     $this->setCustomMetadata($customMetadataAssociatedToDocument);
        // }        
    }

}
