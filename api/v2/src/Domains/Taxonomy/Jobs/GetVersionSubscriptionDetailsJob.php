<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class GetVersionSubscriptionDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $publisherDocumentId,string $publisherOrgId,string $userId,string $organizationId,string $subscriberDocId='')
    {
        $this->publisherDocumentId = $publisherDocumentId;
        $this->publisherOrgId = $publisherOrgId;
        $this->userId = $userId;
        $this->organizationId = $organizationId;
        $this->subscriberDocId = $subscriberDocId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('version_subscriber_detail as vsd')
                ->join('version_subscription as vs', 'vs.subscriber_detail_id', '=', 'vsd.subscriber_detail_id')
                ->select('vs.subscription_id')
                ->where(['pub_document_id'=>$this->publisherDocumentId,
                        'vsd.publisher_org_id'=>$this->publisherOrgId,
                        'vsd.subscriber_id'=>$this->userId,
                        'vsd.subscriber_org_id'=>$this->organizationId,
                        'vs.is_active'=>1])
                ->whereNull('deleted_at');  
        if(!empty($this->subscriberDocId)){
            $query->where('vs.sub_document_id',$this->subscriberDocId);
        }
        $versionSubscription = $query->first();
        return (isset($versionSubscription->subscription_id)) ? $versionSubscription->subscription_id : '';
    }
}
