<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;
use App\Data\Models\ProjectItem;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;
use Illuminate\Support\Facades\DB;

class GetFlattenedTaxonomyTreeV3Job extends Job
{

    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $requestUserDetails;

    private $document;
    private $items;
    private $itemAssociationsUnderDocument;
    private $projectIdsAssociated;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;
    private $parentAndDocumentId;

    private $transformedDocument;
    private $transformedItems;
    private $transformedItemAssociationsUnderDocument;

    private $tree;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->requestUserDetails = $requestUserDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        //
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $documentIdentifier =   $this->documentIdentifier;
        $input              =   $this->requestUserDetails;
        if(array_key_exists('is_customview',$input)){
            $isCustomview = $input['is_customview'];
        }else{
            $isCustomview = 0;
        }
        $organizationId     =   $input["organization_id"];

        // User Setting for Taxonomy
        $documentArr =[];
        //Check user setting is exist in user_setting table or not
        $getUserSettingData = $this->getTaxonomyCustomData($documentIdentifier,$organizationId);
      
        if($getUserSettingData) {
            if ($getUserSettingData->json_config_value) {
                $userJsonArr = json_decode($getUserSettingData->json_config_value, true);
    
                if (!empty($userJsonArr) && isset($userJsonArr['last_table_view'])) {
                    $tableName = $userJsonArr['last_table_view'];
                    foreach ($userJsonArr['taxonomy_table_views'] as $userJsonArrK => $userJsonArrV) {
                        $checkUserSettingStatus = $userJsonArrV['is_user_default'];
                        $savedTableName = $userJsonArrV['table_name'];
                        $view_type = $userJsonArrV['view_type'];
                        if ($checkUserSettingStatus == 1 && $savedTableName == $tableName) {
                            foreach ($userJsonArrV['table_config'] as $userJsonArrVK => $userMetadataList) {
                                $documentArr[] = $userMetadataList;
                            }
                        }
                    }

                    $getDocumentSetting = $this->getDocumentDefaultSetting($documentIdentifier, $organizationId);
                   
                    if (!empty($getDocumentSetting->display_options)) {
                        $documentJsonArr = json_decode($getDocumentSetting->display_options, true);
                        $documentTable = $documentJsonArr['last_table_view'];
                        $view_type = $documentJsonArr['taxonomy_table_views']['view_type'];
                        if ($documentJsonArr['taxonomy_table_views']['default_for_all_user'] == 1 && $documentJsonArr['taxonomy_table_views']['table_name']==$documentTable) {
                                $tableConfig = $documentJsonArr['taxonomy_table_views']['table_config'];
                                $documentArr[] = $tableConfig;


                        }
                    }
                }
            }
        }
        // Check column name with existing or new
        $customInternalName=[];
        $metadataCustomIds=[];
        $internalNameOfNodeMetadata=[];
        $view_type = "";
        if(!empty($documentArr)){
            if($view_type == "metadata"){
                foreach($documentArr as $internalNameK =>$internalNameV) {
                    if($internalNameV['is_custom']==1) {
                        $customInternalName[] = $internalNameV['internal_name'];
                        $metadataCustomIds[] = $internalNameV['metadata_id'];
                    }else{
                        $internalNameOfNodeMetadata[] = $internalNameV['internal_name'];
                    }
                }

            }
            if($view_type == "node_type"){
                foreach($documentArr as $internalNameK =>$internalNameV) {
                    $internalMetaDataName = $internalNameV['metadata'];
                    foreach($internalMetaDataName as $internalNameKey=>$internalNameVal) {
                        if($internalNameVal['is_custom']==1) {
                            $customInternalName[] = $internalNameVal['internal_name'];
                            $metadataCustomIds[] = $internalNameVal['metadata_id'];
                        }else{
                            $internalNameOfNodeMetadata[] = $internalNameVal['internal_name'];
                        }
                    }
                }
            }

            $defaultCol = ['full_statement','node_type','human_coding_scheme'];
            foreach ($internalNameOfNodeMetadata as $keys=>$internalNameOfNodeMetadataV)
            {
                if(in_array($internalNameOfNodeMetadataV,$defaultCol)) {
                    unset($internalNameOfNodeMetadata[$keys]);
                }
            }
        }
        
        $fieldNames =[];
        $fieldNames[] = 'items.item_id as id';
        $arrValue =[];
       
        if(!empty($internalNameOfNodeMetadata)) {
            foreach ($internalNameOfNodeMetadata as $key=>$value) {
                if(strpos($value,'_')!==false){
                    $posnum = strpos($value,'_');
                    $table_name  = substr($value,0,$posnum);
                    $column_name = substr($value,$posnum+1);
                }else{
                  $table_name    = "Default";
                }
                switch($table_name){
                    case "concept":
                        $concept = true;
                        if($column_name == "title"){
                            $alias = 'concept_title';
                        }else if($column_name == "description"){
                            $alias = 'concept_description';
                        }else if($column_name == "hierarchy_code"){
                            $alias = 'concept_hierarchy_code';
                        }
                        
                        if(isset($alias)){
                            $fieldNames[] = 'concepts.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    case "license":
                        $license = true;
                        if($column_name == "title"){
                            $alias = 'license_title';
                        }else if($column_name == "description"){
                            $alias = 'license_description';
                        }else if($column_name == "hierarchy_code"){
                            $alias = 'license_hierarchy_code';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'licenses.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    
                    case "items":
                        $items = true;
                        if($column_name == "start_date"){
                            $alias = 'items_status_start_date';
                        }else if($column_name == "end_date"){
                            $alias = 'items_status_end_date';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'items.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                        }
                        unset($alias);
                        break;
                    
                    case "language":
                        if($column_name == "name"){
                            $alias = 'language_name';
                        }
                        if(isset($alias)){
                            $fieldNames[] = 'languages.'.$column_name.' as '.$alias;
                        }else{
                            $fieldNames[] = $value;
                         }
                        unset($alias);
                        break;
                    default:
                        $fieldNames[] = $value;
                }
            }
            $getMetadataName = $this->getInternalMatching($fieldNames,$documentIdentifier,$organizationId);

            foreach ($getMetadataName as $getMetadataNameV) {
                $itemId = $getMetadataNameV->id;
                $arrValue[$itemId]= (array)$getMetadataNameV;
            }

        }

        // Check Custom metadata
        $customList =[];
        if($customInternalName && $metadataCustomIds) {
            $customData = $this->getCustomMetadata($customInternalName,$metadataCustomIds);
            if($customData) {
                foreach ($customData as $customDataK=>$customDataV) {
                    $customList[$customDataV->id] = (array)$customDataV;
                }
            }
        }
        // User Setting for taxonomy ends here
        $document = DB::table('documents')
            ->join('node_types', 'documents.node_type_id', '=', 'node_types.node_type_id')
            ->where('documents.document_id', $documentIdentifier)
            ->where('documents.organization_id', $organizationId)
            ->select('documents.document_id','documents.title','documents.node_type_id','documents.project_id','documents.node_type_id','documents.status',
            'documents.adoption_status','documents.custom_view_visibility','documents.title_html','documents.import_type','documents.is_deleted','node_types.title as node_title','documents.updated_at','documents.document_type','documents.source_document_id','documents.language_id')
            ->get()->first();

        $nodeType = !empty($document->node_type_id) ? $document->node_title : "";
        $projectEnabled = 0;
        if(!empty($document->project_id))
        {
            $projectType = $this->helperToReturnProjectType([$document->project_id]);
            if(!empty($projectType) && $projectType[$document->project_id] == 1)
            {
                $projectEnabled = 1;
            }
        }
        /********** Missing language for Document resolved : UF-1820 ********/
        $languageName = ''; 
        if(!empty($document->language_id)){
            $language = DB::table('languages')->select('name')->where(['language_id'=>$document->language_id,'is_deleted'=>0])->first();
            if(isset($language->name)){
                $languageName = $language->name; 
            }
        }
        /********** Missing language for Document resolved : UF-1820 ********/
        $parsedDocument = [
            "id"                    =>  $document->document_id,
            "title"                 =>  !empty($document->title) ? $document->title : "",
            "title_html"            =>  !empty($document->title_html) ? $document->title_html : "",
            "human_coding_scheme"   =>  "",
            "list_enumeration"      =>  "",
            "sequence_number"       =>  "",
            "full_statement"        =>  "",
            "status"                =>  $document->status,
            "document_type"         =>  $document->document_type,
            "adoption_status"       =>  $document->adoption_status,
            "node_type"             =>  $nodeType,
            "metadataType"          =>  $nodeType,
            "node_type_id"          =>  $document->node_type_id,
            "project_enabled"       =>  $projectEnabled,
            "project_name"          =>  "",
            "is_document"           =>  1,
            "custom_view_visibility"=>  $document->custom_view_visibility,
            "is_orphan"             =>  0,
            "source_document_id"    => $document->source_document_id,
            "language_name"         => $languageName
        ];

        //Get Item Details
        
        if($isCustomview == 1)
        {
            $items = DB::table('items')
            ->join('node_types', 'items.node_type_id', '=', 'node_types.node_type_id')
            ->where('items.document_id', $documentIdentifier)
            ->where('items.organization_id',$organizationId)
            ->where('items.is_deleted',0)
            ->where('items.custom_view_visibility',1)
            ->select('items.item_id','items.human_coding_scheme','items.list_enumeration','items.full_statement',
            'items.node_type_id','items.source_item_id', 'items.custom_view_data','items.human_coding_scheme_html','items.full_statement_html','node_types.title as node_titles')
            ->get();
        }
        else
        {
            $items = DB::table('items')
            ->join('node_types', 'items.node_type_id', '=', 'node_types.node_type_id')
            ->where('items.document_id', $documentIdentifier)
            ->where('items.organization_id', $organizationId)
            ->where('items.is_deleted',0)
            ->select('items.item_id','items.human_coding_scheme','items.list_enumeration','items.full_statement',
            'items.node_type_id','items.source_item_id', 'items.custom_view_data','items.human_coding_scheme_html','items.full_statement_html','node_types.title as node_titles')
            ->get();
        }    
        $itemAssociationsUnderDocument =DB::table('item_associations')
        // ->select('origin_node_id','destination_node_id','sequence_number')                
        ->select('source_item_id','target_item_id','sequence_number')
        ->where('source_document_id',$documentIdentifier)
        ->where('organization_id',$organizationId)
        ->where('association_type',1)
        ->where('is_deleted','0')
        ->get();


        $itemSequenceList = [];

        foreach($itemAssociationsUnderDocument as $itemAssociation) 
        {
            $itemSequenceList[$itemAssociation->source_item_id] = $itemAssociation->sequence_number;
        }                                  
        $searchItemArray = [];
        $searchProjectItemArray = [];
        foreach($items as $item) {
            array_push($searchItemArray,$item->item_id);
        }

        $projectItems =DB::table('project_items')->whereIn('item_id',$searchItemArray)->where('is_deleted','0')->where('is_editable',1)->get();
        $projectId =[];
        if(count($projectItems) > 0)
        {
            foreach($projectItems as $projectItem) {
                $projectId[]    =   $projectItem->project_id;
            }
            $projectType = $this->helperToReturnProjectType($projectId);
            foreach($projectItems as $projectItem) {
                if($projectType[$projectItem->project_id] == 1) {
                    array_push($searchProjectItemArray,$projectItem->item_id);
                }
            }
        }
        $parsedItems =[];
        $parseResult =[];
        $ItemfinalList = [];
        $configItem = [];

        $defaultCustomView = '{"report_path":"","version":"","allow_expand":"no","title":"","style":"font-size:1rem;background:white;font-style:none;color:black"}';
        foreach($items as $item) {
            $itemData = [
                "id" => $item->item_id,
                "title" => "",
                "human_coding_scheme"   => !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "",
                "human_coding_scheme_html"   => !empty($item->human_coding_scheme_html) ? $item->human_coding_scheme_html : $item->human_coding_scheme,
                "list_enumeration"      => !empty($item->list_enumeration) ? $item->list_enumeration : "",
                "sequence_number"       => !empty($itemSequenceList[$item->item_id]) ? $itemSequenceList[$item->item_id] : "",
                "full_statement"        => !empty($item->full_statement) ? $item->full_statement : "",
                "full_statement_html"   => !empty($item->full_statement_html) ? $item->full_statement_html : $item->full_statement,
                "node_type"             => !empty($item->node_titles) ? $item->node_titles : "",
                "metadataType"          => !empty($item->node_titles) ? $item->node_titles : "",
                "node_type_id"          => !empty($item->node_type_id) ? $item->node_type_id : "",
                "item_type"             => "",
                "project_enabled"       => in_array($item->item_id,$searchProjectItemArray) ? 1 : 0,
                "project_name"          => "",
                "is_document"           => 0,
                "parent_id"             =>  "",
                "document_id"           =>  "",
                "document_title"        =>  "",
                "custom_view_data"      => !empty($item->custom_view_data) ? $item->custom_view_data : $defaultCustomView,
                "source_item_id" 		=> $item->source_item_id
            ];

            if(!$isCustomview == 1)
            {
                unset($itemData['custom_view_data']);
            }
                
            $parsedItems = $itemData;

            unset($itemData);

            
            if(!empty($arrValue) && !empty($customList)){
                $ItemfinalList = array_merge($arrValue,$customList);
            }else if(!empty($arrValue) && empty($customList)){
                $ItemfinalList = $arrValue;
            }else if(empty($arrValue) && !empty($customList)){
                $ItemfinalList = $customList;
            }

            if(!empty($ItemfinalList)){
                if(array_key_exists($item->item_id,$ItemfinalList))
                {
                    $configItem = $ItemfinalList[$item->item_id];
                }
            }

            if($configItem){
                $finalParsedItem[] = array_merge($parsedItems,$configItem);
            }else {
                $finalParsedItem[]  = $parsedItems;
            }

        }
        $parsedItemAssociations = [];
        $parsedOrginNodeItemArr = [];
        if(!empty($document->document_id))
        {
            array_push($searchItemArray,$document->document_id);
        }
         foreach($itemAssociationsUnderDocument as $itemAssociation) {
        $originNodeId = $itemAssociation->source_item_id;
        $destinationNodeId = $itemAssociation->target_item_id;
        if(!empty($originNodeId) && !empty($destinationNodeId) && ($originNodeId != $destinationNodeId) && in_array($destinationNodeId,$searchItemArray)) {

        $parsedItemAssociations[] = [
                                        "child_id" => $itemAssociation->source_item_id,
                                        "parent_id" => $itemAssociation->target_item_id,
                                        "sequence_number" => $itemAssociation->sequence_number
                                     ];
                 array_push($parsedOrginNodeItemArr,$itemAssociation->source_item_id);                             
                                    }
                                }
        /* This code for handle the orphaned node condition ACMT-953*/                          
        $itemArr = [];
        $orphanedNodeArr = [];
        if(!empty($finalParsedItem))
        {    
            foreach($finalParsedItem as $finalParsedItemK => $finalParsedItemV)
            {
                array_push($itemArr,$finalParsedItemV['id']);
            }
            $orphanedNodeArr=array_diff($itemArr,$parsedOrginNodeItemArr);
            foreach($finalParsedItem as $finalParsedItemK => $finalParsedItemV)
            {
                if(in_array($finalParsedItemV['id'],$orphanedNodeArr))
                {
                    $finalParsedItem[$finalParsedItemK]['is_orphan'] = 1;
                }
                else {
                    $finalParsedItem[$finalParsedItemK]['is_orphan'] = 0;
                }
            }
        }
        
        
        if(!empty($document->document_id))
        {
            array_push($itemArr,$document->document_id);
        }
        if(!empty($parsedItemAssociations))
        {
            foreach($parsedItemAssociations as $parsedItemAssociationsK => $parsedItemAssociationsV)
            {
                if(in_array($parsedItemAssociationsV['parent_id'],$itemArr))
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = false;
                }
                else
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = true;
                }
            }
        }

        /* End */
        

        if(!empty($finalParsedItem)) {
                $nodes = array_prepend($finalParsedItem, $parsedDocument);
        }
        else {
                $nodes = array_prepend([], $parsedDocument);              // changes to give node response in array format 
        }
        $treeData = [
            "nodes" => $nodes,
            "relations" => $parsedItemAssociations,
            "import_type" => $document->import_type,
            "modified_at" => $document->updated_at,
            "is_deleted"  => $document->is_deleted 
        ];
      
        return $treeData;

    }

    private function helperToReturnProjectType(array $projectId) {
        $projectType            =   [];
        
        $projectTypeDetail =DB::table('projects')
                            ->select('project_id','project_type')
                            ->whereIn('project_id',$projectId)
                            ->where('is_deleted','0')
                            ->get();
            
        foreach($projectTypeDetail as $project) {
            $projectType[$project->project_id] = $project->project_type;
        }
        return $projectType;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @FunctionName getTaxonomyCustomData
     */
    private function getTaxonomyCustomData($documentId,$organizationId)
    {
          $userSetting = DB::table('user_settings')->select('json_config_value')
                              ->where('id',$documentId)
                              ->where('organization_id',$organizationId)
                              ->where('id_type',3)
                              ->where('is_deleted',0)
                              ->get()
                              ->first();
          return $userSetting;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @FunctionName getDocumentDefaultSetting
     */
    private function getDocumentDefaultSetting($documentId,$organizationId)
    {
          $getDocumentJson =  DB::table('documents')->select('display_options')
                               ->where('document_id',$documentId)
                               ->where('organization_id',$organizationId)
                               ->where('is_deleted',0)
                               ->get()
                               ->first();
          return $getDocumentJson;

    }

    /**
     * @param $internalNames
     * @param $organizationId
     * @return mixed
     * @Function Name getInternalMatching
     */
    private function getInternalMatching($internalNames,$documentIdentifier,$organizationId)
    {
        $queryData = DB::table('items')
            ->select($internalNames)
            ->leftJoin('concepts', function ($join) {
                $join->on('concepts.concept_id','=','items.concept_id')
                     ->where('concepts.is_deleted', '=', 0);
            })
            ->leftJoin('licenses', function ($join) {
                $join->on('licenses.license_id','=','items.license_id')
                     ->where('licenses.is_deleted', '=', 0);
            })
            ->leftJoin('languages', function ($join) {
                $join->on('languages.language_id','=','items.language_id')
                     ->where('languages.is_deleted', '=', 0);
            })
            ->where('items.document_id',$documentIdentifier)
            ->where('items.organization_id',$organizationId)
            ->where('items.is_deleted',0)
            ->get()
            ->toArray();
        return $queryData;
    }

    private function getCustomMetadata($fieldName,$metadataIds)
    {
        $customMetadata = DB::table('item_metadata')
                          ->select($fieldName)
                          ->where('is_deleted',0)
                          ->whereIn('metadata_id',$metadataIds)
                          ->get()
                          ->toArray();
        return $customMetadata;
    }

}
