<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\DB;

use App\Data\Models\Document;
use App\Data\Models\Item;
use App\Data\Models\NodeType;
use App\Data\Models\Project;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class CreateTaxonomySearchDataJob extends Job
{

    use StringHelper, DateHelpersTrait;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $projectRepository;
    private $itemAssociationRepository;
    private $metadataRepository;
    private $languageRepository;
    private $licenseRepository;
    private $conceptRepository;

    private $documentId;

    private $document;
    private $items;
    private $itemIds;
    private $itemAssociations;
    private $itemConceptIds;
    private $itemLicenseIds;
    private $itemLanguageIds;
    private $itemConcepts;
    private $itemLicenses;
    private $itemLanguages;

    private $organizationId;
    private $metadata;
    private $documentMetadata;
    private $itemMetadata;
    private $itemSystemMetadata;
    private $documentSystemMetadata;
    private $itemCustomMetadata;
    private $documentCustomMetadata;

    private $nodeTypeIds;
    private $itemProjectIds;
    private $projectIds;
    private $nodeTypes;
    private $projects;

    private $searchData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentId)
    {
        $this->setDocumentId($documentId);
    }

    /*******************************Setters and Getters for Document, Item and Associations***********************************/

    public function setDocumentId(string $data) {
        $this->documentId = $data;
    }

    public function getDocumentId(): string {
        return $this->documentId;
    }

    public function setDocument(Document $data) {
        $this->document = $data;
    }

    public function getDocument(): Document {
        return $this->document;
    }

    public function setItems(Collection $data) {
        $this->items = $data;
    }

    public function getItems(): Collection {
        return $this->items;
    }

    public function setItemIds(array $data) {
        $this->itemIds = $data;
    }

    public function getItemIds(): array {
        return $this->itemIds;
    }

    public function setItemAssociations(Collection $data) {
        $this->itemAssociations = $data;
    }

    public function getItemAssociations(): Collection {
        return $this->itemAssociations;
    }

    public function setItemConceptIds(array $data) {
        $this->itemConceptIds = $data;
    }

    public function getItemConceptIds(): array {
        return $this->itemConceptIds;
    }

    public function setItemLicenseIds(array $data) {
        $this->itemLicenseIds = $data;
    }

    public function getItemLicenseIds(): array {
        return $this->itemLicenseIds;
    }

    public function setItemLanguageIds(array $data) {
        $this->itemLanguageIds = $data;
    }

    public function getItemLanguageIds(): array {
        return $this->itemLanguageIds;
    }

    public function setItemConcepts(Collection $data) {
        $this->itemConcepts = $data;
    }

    public function getItemConcepts(): Collection {
        return $this->itemConcepts;
    }

    public function setItemLicenses(Collection $data) {
        $this->itemLicenses = $data;
    }

    public function getItemLicenses(): Collection {
        return $this->itemLicenses;
    }

    public function setItemLanguages(Collection $data) {
        $this->itemLanguages = $data;
    }

    public function getItemLanguages(): Collection {
        return $this->itemLanguages;
    }

    /*******************************Setters and Getters for Organization, Metadata and Custom Metadata***********************************/

    public function setOrganizationId(string $data) {
        $this->organizationId = $data;
    }

    public function getOrganizationId(): string {
        return $this->organizationId;
    }

    public function setMetadata(Collection $data) {
        $this->metadata = $data;
    }

    public function getMetadata(): Collection {
        return $this->metadata;
    }

    public function setDocumentMetadata(Collection $data) {
        $this->documentMetadata = $data;
    }

    public function getDocumentMetadata(): Collection {
        return $this->documentMetadata;
    }

    public function setItemMetadata(Collection $data) {
        $this->itemMetadata = $data;
    }

    public function getItemMetadata(): Collection {
        return $this->itemMetadata;
    }

    public function setItemSystemMetadata(Collection $data) {
        $this->itemSystemMetadata = $data;
    }

    public function getItemSystemMetadata(): Collection {
        return $this->itemSystemMetadata;
    }

    public function setDocumentSystemMetadata(Collection $data) {
        $this->documentSystemMetadata = $data;
    }

    public function getDocumentSystemMetadata(): Collection {
        return $this->documentSystemMetadata;
    }

    public function setItemCustomMetadata(Collection $data) {
        $this->itemCustomMetadata = $data;
    }

    public function getItemCustomMetadata(): Collection {
        return $this->itemCustomMetadata;
    }

    public function setDocumentCustomMetadata(Collection $data) {
        $this->documentCustomMetadata = $data;
    }

    public function getDocumentCustomMetadata(): Collection {
        return $this->documentCustomMetadata;
    }

    /*******************************Setters and Getters for Node Types and Projects***********************************/

    public function setNodeTypeIds(array $data) {
        $this->nodeTypeIds = $data;
    }

    public function getNodeTypeIds(): array {
        return $this->nodeTypeIds;
    }

    public function setItemProjectIds(Collection $data) {
        $this->itemProjectIds = $data;
    }

    public function getItemProjectIds(): Collection {
        return $this->itemProjectIds;
    }

    public function setProjectIds(array $data) {
        $this->projectIds = $data;
    }

    public function getProjectIds(): array {
        return $this->projectIds;
    }

    public function setNodeTypes(Collection $data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes(): Collection {
        return $this->nodeTypes;
    }

    public function setProjects(Collection $data) {
        $this->projects = $data;
    }

    public function getProjects(): Collection {
        return $this->projects;
    }

    /*******************************Setters and Getters for Search Data***********************************/

    public function setSearchData(Collection $data) {
        $this->searchData = $data;
    }

    public function getSearchData(): Collection {
        return $this->searchData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ProjectRepositoryInterface $projectRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        MetadataRepositoryInterface $metadataRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        ConceptRepositoryInterface $conceptRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->projectRepository = $projectRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->metadataRepository = $metadataRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->conceptRepository = $conceptRepository;

        $this->fetchAndSetDocument();
        $this->fetchAndSetItems();
        $this->extractAndSetItemIds();
        $this->fetchAndSetItemAssociations();
        $this->extractAndSetItemConceptIdsCsv();
        $this->extractAndSetItemLicenseIdsCsv();
        $this->extractAndSetItemLanguageIdsCsv();
        $this->fetchAndSetItemConcepts();
        $this->fetchAndSetItemLicenses();
        $this->fetchAndSetItemLanguages();

        $this->extractAndSetTaxonomyOrganizationId();
        $this->fetchAndSetMetadata();
        $this->fetchAndSetDocumentMetadata();
        $this->fetchAndSetItemMetadata();
        $this->parseAndSetItemSystemMetadata();
        $this->parseAndSetDocumentSystemMetadata();
        $this->parseAndSetItemCustomMetadata();
        $this->parseAndSetDocumentCustomMetadata();

        $this->extractAndSetNodeTypeIds();
        $this->extractAndSetProjectIds();
        $this->fetchAndSetNodeTypes();
        $this->fetchAndSetProjects();
        
        $this->prepareAndSetSearchTaxonomyData();

        return $this->getSearchData();
    }

    private function fetchAndSetDocument() {
        $documentId = $this->getDocumentId();
        $document = $this->documentRepository->find($documentId);
        $this->setDocument($document);
    }

    private function fetchAndSetItems() {
        $document = $this->getDocument();
        $documentId = $document->document_id;
        $conditionalClause = [ "document_id" => $documentId ];
        $items = $this->itemRepository->findByAttributes($conditionalClause);

        // for testing
        // $items = Item::where('document_id', $documentId)->offset(51)->limit(1)->get();
        // dd($items->toArray());

        $this->setItems($items);
    }

    private function extractAndSetItemIds() {
        $itemCollection = $this->getItems();
        $itemIds = $itemCollection->pluck('item_id')->values()->toArray();
        $this->setItemIds($itemIds);
    }

    private function fetchAndSetItemAssociations() {
        $document = $this->getDocument();
        $documentId = $document->document_id;
        $returnCollection = true;
        $fieldsToReturn = [ 'item_association_id', 'origin_node_id', 'association_type' ];
        $conditionalClause = [ 'document_id' => $documentId ];
        $itemAssociations = $this->itemAssociationRepository->findByAttributesWithSpecifiedFields(
                                $returnCollection, 
                                $fieldsToReturn, 
                                $conditionalClause
                            );
        $this->setItemAssociations($itemAssociations);
    }
    
    private function extractAndSetItemConceptIdsCsv() {
        $itemCollection = $this->getItems();
        $itemConceptIds = $itemCollection->unique('concept_id')->pluck('concept_id')->values()->toArray();
        $this->setItemConceptIds($itemConceptIds);
    }

    private function extractAndSetItemLicenseIdsCsv() {
        $itemCollection = $this->getItems();
        $itemLicenseIds = $itemCollection->unique('license_id')->pluck('license_id')->values()->toArray();
        $this->setItemLicenseIds($itemLicenseIds);
    }

    private function extractAndSetItemLanguageIdsCsv() {
        $itemCollection = $this->getItems();
        $itemLanguageIds = $itemCollection->unique('language_id')->pluck('language_id')->values()->toArray();
        $this->setItemLanguageIds($itemLanguageIds);
    }

    private function fetchAndSetItemConcepts() {
        $attributeIn = 'concept_id';
        $itemConceptIds = $this->getItemConceptIds();
        $whereClauseAttributes = [];
        $itemConcepts = $this->conceptRepository->findByAttributeContainedIn($attributeIn, $itemConceptIds, $whereClauseAttributes);
        $this->setItemConcepts($itemConcepts);
    }

    private function fetchAndSetItemLicenses() {
        $attributeIn = 'license_id';
        $itemLicenseIds = $this->getItemLicenseIds();
        $whereClauseAttributes = [];
        $itemLicenses = $this->licenseRepository->findByAttributeContainedIn($attributeIn, $itemLicenseIds, $whereClauseAttributes);
        $this->setItemLicenses($itemLicenses);
    }
    
    private function fetchAndSetItemLanguages() {
        $attributeIn = 'language_id';
        $itemLanguageIds = $this->getItemLanguageIds();
        $whereClauseAttributes = [];
        $itemLicenses = $this->languageRepository->findByAttributeContainedIn($attributeIn, $itemLanguageIds, $whereClauseAttributes);
        $this->setItemLanguages($itemLicenses);
    }

    private function extractAndSetTaxonomyOrganizationId() {
        $document = $this->getDocument();
        $organizationId = $document->organization_id;
        $this->setOrganizationId($organizationId);
    }

    private function fetchAndSetMetadata() {
        $organizationId = $this->getOrganizationId();
        $returnCollection = true;
        $fieldsToReturn = [ 'metadata_id', 'name', 'internal_name', 'is_custom', 'is_document' ];
        $conditionalClause = [ 'organization_id' => $organizationId, 'is_deleted' => 0, 'is_active' => 1 ];
        $metadataCollection =   $this->metadataRepository->findByAttributesWithSpecifiedFields(
                                    $returnCollection, 
                                    $fieldsToReturn, 
                                    $conditionalClause
                                );
        $this->setMetadata($metadataCollection);
    }

    private function fetchAndSetDocumentMetadata() {
        $document = $this->getDocument();
        $documentId = $document->document_id;
        $documentData = DB::table('document_metadata')->where('document_id', $documentId)->get();
        $this->setDocumentMetadata($documentData);
    }

    private function fetchAndSetItemMetadata() {
        $itemIds = $this->getItemIds();
        $itemData = DB::table('item_metadata')->whereIn('item_id', $itemIds)->get();
        $this->setItemMetadata($itemData);
    }

    private function parseAndSetItemSystemMetadata() {
        $metadataCollection = $this->getMetadata();
        $itemMetadata = [];
        $itemSystemMetadataCollection = $metadataCollection->whereIn('is_document', [0,2])->where('is_custom', 0);
        foreach($itemSystemMetadataCollection as $itemSystemMetadataCollectionK => $itemSystemMetadataCollectionV)
        {
            array_push($itemMetadata,$itemSystemMetadataCollectionV->metadata_id);
        }
        $this->systemMetadataItemInTenant = array_unique($itemMetadata);
        $this->setItemSystemMetadata($itemSystemMetadataCollection);
    }

    private function parseAndSetDocumentSystemMetadata() {
        $metadataCollection = $this->getMetadata();
        $documentSystemMetadataCollection = $metadataCollection->whereIn('is_document', [1,2])->where('is_custom', 0);
        $this->setDocumentSystemMetadata($documentSystemMetadataCollection);
    }

    private function parseAndSetItemCustomMetadata() {
        $metadataCollection = $this->getMetadata();
        $itemCustomMetadadataCollection = $metadataCollection->whereIn('is_document', [0,2])->where('is_custom', 1);
        $this->setItemCustomMetadata($itemCustomMetadadataCollection);
    }

    private function parseAndSetDocumentCustomMetadata() {
        $metadataCollection = $this->getMetadata();
        $documentCustomMetadadataCollection = $metadataCollection->whereIn('is_document', [1,2])->where('is_custom', 1);
        $this->setDocumentCustomMetadata($documentCustomMetadadataCollection);
    }

    private function extractAndSetNodeTypeIds() {
        $document = $this->getDocument();
        $documentNodeTypeId = $document->node_type_id;
        $items = $this->getItems();
        $itemNodeTypeIdsCollection = $items->unique('node_type_id')->pluck('node_type_id');
        $nodeTypeIds = $itemNodeTypeIdsCollection->push($documentNodeTypeId)->toArray();
        $this->setNodeTypeIds($nodeTypeIds);
    }

    private function extractAndSetProjectIds() {
        $document = $this->getDocument();
        $documentProjectId = $document->project_id;
        $items = $this->getItems();
        $itemProjectIds = $items->pluck('item_id')->toArray();
        $projectIdsCollection = DB::table('project_items')->select('project_id', 'item_id')->whereIn('item_id', $itemProjectIds)->get();
        // will require this when searching for item project name later
        $this->setItemProjectIds($projectIdsCollection);

        $itemProjectIds = $projectIdsCollection->pluck('project_id');
        $mergedProjectIds = (!empty($documentProjectId) ? $itemProjectIds->push($documentProjectId) : $itemProjectIds)->toArray();
        $this->setProjectIds($mergedProjectIds);
    }

    private function fetchAndSetNodeTypes() {
        $attributeIn = 'node_type_id';
        $nodeTypeIds = $this->getNodeTypeIds();
        $whereClauseAttributes = [];
        $nodeTypesCollection = $this->nodeTypeRepository->findByAttributeContainedIn($attributeIn, $nodeTypeIds, $whereClauseAttributes);
        $this->setNodeTypes($nodeTypesCollection);
    }

    private function fetchAndSetProjects() {
        $attributeIn = 'project_id';
        $projectIds = $this->getProjectIds();
        $whereClauseAttributes = [];
        $projectCollection = $this->projectRepository->findByAttributeContainedIn($attributeIn, $projectIds, $whereClauseAttributes);
        $this->setProjects($projectCollection);
    }

    private function prepareAndSetSearchTaxonomyData() {
        $document = $this->getDocument();
        $items = $this->getItems();
        $documentSearchNode = $this->createAndReturnDocumentNodeSearchData($document);
        $itemSearchNodes = $this->createAndReturnItemNodesSearchData($items, $document);
        $searchDataBatch = collect(array_prepend($itemSearchNodes, $documentSearchNode));
        $this->setSearchData($searchDataBatch);
    }

    private function createAndReturnDocumentNodeSearchData(Document $document): array {
        $identifier = $document->document_id;
        $parentIdentifier = "";
        $organizationIdentifier = $document->organization_id;
        $documentId = $identifier;
        $taxonomyName = $document->title ?: "";
        $documentProjectId = $document->project_id ?: "";
        $projectDetails = $this->helperToSearchAndReturnProjectDetails($documentProjectId);
        $projectName = $projectDetails['project_name'];
        $projectWorkflowId = $projectDetails['workflow_id'];
        $title = $taxonomyName;
        $officialSourceUrl = $document->official_source_url ?: "";
        $notes = $document->notes ?: "";
        $publisher = $document->publisher ?: "";
        $description = $document->description ?: "";
        $nodeTypeId = !empty($document->node_type_id) ? $document->node_type_id : "";
        $nodeType = $nodeTypeId!=="" ? $this->helperToSearchAndReturnNodeTypeTitle($nodeTypeId) : "";
        $type = 'document';
        $deleteOrAddStatusTextForCloudSearch = $document->is_deleted===1 ? 'delete' : 'add';
        $adoptionStatus = $document->adoption_status;
        $updatedBy = $document->updated_by ? $document->updated_by : "";
        // $updateOn = (!empty($document->created_at) && $document->created_at!=="0000-00-00 00:00:00") ? 
        //             $this->formatDateTimeToCloudSarchCompatibleFormat($document->created_at->toDateTimeString()) : 
        //             "1970-01-01T00:00:00Z";

        // modify updateOn cloud search index logic
        if(!empty($document->updated_at) && $document->updated_at!=="0000-00-00 00:00:00") {
            $updateOn = $this->formatDateTimeToCloudSarchCompatibleFormat($document->updated_at->toDateTimeString());
        }
        else if(!empty($document->created_at) && $document->created_at!=="0000-00-00 00:00:00") {
            $updateOn = $this->formatDateTimeToCloudSarchCompatibleFormat($document->created_at->toDateTimeString());
        }
        else {
            $updateOn = "1970-01-01T00:00:00Z";
        }

        $documentSystemMetadataKeyValuePairsCsv = $this->helperToReturnDocumentSystemMetadataKeyValuePairsCsv($document);
        $customMetadataKeyValuePairsCsv = $this->helperToReturnDocumentCustomMetadataKeyValuePairsCsv($document);
        $documentNodeSearchData = [
            'id' => $identifier,
            'type' => $deleteOrAddStatusTextForCloudSearch,
            'fields' => [
                'identifier' => $identifier,
                'parent_identifier' => $parentIdentifier,
                'organization_identifier' => $organizationIdentifier,
                'document_id' => $documentId,
                'taxonomy_name' => $taxonomyName,
                'project_name' => $projectName,
                'title' => $title,
                'official_source_url' => $officialSourceUrl,
                'notes' => $notes,
                'publisher' => $publisher,
                'description' => $description,
                'human_coding_scheme' => '',
                'list_enumeration' => '',
                'full_statement' => '',
                'alternative_label' => '',
                'abbreviated_statement' => '',
                'node_type' => $nodeType,
                'type' => $type,
                'node_type_id' => $nodeTypeId,
                'project_id' => $documentProjectId,
                'project_workflow_id' => $projectWorkflowId,
                'adoption_status' => $adoptionStatus,
                'item_association_type' => [],
                'updated_by' => $updatedBy,
                'update_on' => $updateOn,
                'item_metadata' => '',
                'document_metadata' => $documentSystemMetadataKeyValuePairsCsv,
                'item_custom_metadata' => $customMetadataKeyValuePairsCsv // custom metadata key:value csv
            ]
        ];
        return $documentNodeSearchData;
    }

    private function createAndReturnItemNodesSearchData(Collection $items, Document $document): array {
        $itemNodesSearchData = [];
        foreach($items as $item) {
            $identifier = $item->item_id;
            $parentIdentifier = $item->parent_id;
            $organizationIdentifier = $item->organization_id;
            $documentId = $document->document_id;
            $taxonomyName = $document->title ?: "";
            $itemProjectId = $this->helperToReturnItemProjectId($identifier);
            $projectDetails = $this->helperToSearchAndReturnProjectDetails($itemProjectId);
            $projectName = $projectDetails['project_name'];
            $projectWorkflowId = $projectDetails['workflow_id'];
            $title = $taxonomyName;
            $humanCodingScheme = "";
            // clean special characters in humen coding scheme
            /*if(!empty($item->human_coding_scheme)) {
                $replacedStringHCS = $this->replaceSpecialCharactersFromString($item->human_coding_scheme, "#");
                $humanCodingScheme = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedStringHCS ));
            }*/
            $humanCodingScheme = $item->human_coding_scheme ?: "";
            $listEnumeration = $item->list_enumeration ?: "";
            $fullStatement = "";
            // clean special characters
            if(!empty($item->full_statement)) {
                $replacedString = $this->replaceSpecialCharactersFromString($item->full_statement, "#");
                $fullStatement = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
            }
            $alternativeLabel = $item->alternative_label ?: "";
            $abbreviatedStatement = $item->abbreviated_statement ?: "";
            $nodeTypeId = !empty($item->node_type_id) ? $item->node_type_id : "";
            $nodeType = $nodeTypeId!=="" ? $this->helperToSearchAndReturnNodeTypeTitle($nodeTypeId) : "";
            $type = 'item';
            $deleteOrAddStatusTextForCloudSearch = $item->is_deleted===1 ? 'delete' : 'add';
            $itemAssociationTypes = $this->helperToSearchAndReturnItemAssociationTypes($identifier);
            $updatedBy = $item->updated_by ? $item->updated_by : "";
            // $updateOn = (!empty($item->created_at) && $item->created_at!=="0000-00-00 00:00:00") ? 
            //             $this->formatDateTimeToCloudSarchCompatibleFormat($item->created_at->toDateTimeString()) : 
            //             "1970-01-01T00:00:00Z";

            // modify updateOn cloud search index logic
            if(!empty($item->updated_at) && $item->updated_at!=="0000-00-00 00:00:00") {
                $updateOn = $this->formatDateTimeToCloudSarchCompatibleFormat($item->updated_at->toDateTimeString());
            }
            else if(!empty($item->created_at) && $item->created_at!=="0000-00-00 00:00:00") {
                $updateOn = $this->formatDateTimeToCloudSarchCompatibleFormat($item->created_at->toDateTimeString());
            }
            else {
                $updateOn = "1970-01-01T00:00:00Z";
            }            

            $itemSystemMetadataKeyValuePairsCsv = $this->helperToReturnItemSystemMetadataKeyValuePairsCsv($item);
            $customMetadataKeyValuePairsCsv = $this->helperToReturnItemCustomMetadataKeyValuePairsCsv($item);
            $itemNodesSearchData[] = [
                'id' => $identifier,
                'type' => $deleteOrAddStatusTextForCloudSearch,
                'fields' => [
                    'identifier' => $identifier,
                    'parent_identifier' => $parentIdentifier,
                    'organization_identifier' => $organizationIdentifier,
                    'document_id' => $documentId,
                    'taxonomy_name' => $taxonomyName,
                    'project_name' => $projectName,
                    'title' => $title,
                    'official_source_url' => '',
                    'notes' => '',
                    'publisher' => '',
                    'description' => '',
                    'human_coding_scheme' => $humanCodingScheme,
                    'list_enumeration' => $listEnumeration,
                    'full_statement' => $fullStatement,
                    'alternative_label' => $alternativeLabel,
                    'abbreviated_statement' => $abbreviatedStatement,
                    'node_type' => $nodeType,
                    'type' => $type,
                    'node_type_id' => $nodeTypeId,
                    'project_id' => $itemProjectId,
                    'project_workflow_id' => $projectWorkflowId,
                    'adoption_status' => 0, // set default value for item
                    'item_association_type' => $itemAssociationTypes,
                    'updated_by' => $updatedBy,
                    'update_on' => $updateOn,
                    'item_metadata' => $itemSystemMetadataKeyValuePairsCsv,
                    'document_metadata' => '',
                    'item_custom_metadata' => $customMetadataKeyValuePairsCsv // custom metadata key:value csv
                ]
            ];
        }
        return $itemNodesSearchData;
    }

    private function helperToSearchAndReturnProjectDetails(string $projectIdToSearchFor): array {
        $dataToReturn = [
            "project_name" => '',
            "workflow_id" => ''
        ];
        $projectCollection = $this->getProjects();
        $searchResult = $projectCollection->where("project_id", $projectIdToSearchFor);
        if($searchResult->isNotEmpty()) {
            $project = $searchResult->first();
            $projectName = $project->project_name ?: "";
            $projectWorkflowId = $project->workflow_id ?: "";
            $dataToReturn['project_name'] = $projectName;
            $dataToReturn['workflow_id'] = $projectWorkflowId;
        }
        return $dataToReturn;
    }

    private function helperToReturnItemProjectId(string $itemId): string {
        $itemProjectIds = $this->getItemProjectIds();
        $projectIdToReturn = "";
        foreach($itemProjectIds as $key=>$itemProjectId) {
            if($itemProjectId->item_id===$itemId) {
                $projectIdToReturn = $itemProjectId->project_id;
                // remove the info found above from the original collection for performance optimization
                $itemProjectIds->forget($key);
                break;
            }
        }
        return $projectIdToReturn;
    }

    private function helperToSearchAndReturnNodeTypeTitle(string $nodeTypeIdToSearchFor): string {
        $nodeTypesCollection = $this->getNodeTypes();
        $searchResult = $nodeTypesCollection->where("node_type_id", $nodeTypeIdToSearchFor);
        return $searchResult->isNotEmpty() ? ($searchResult->first()->title ?: "") : "";
    }

    private function helperToSearchAndReturnItemAssociationTypes(string $originNodeIdentifier): array {
        $associationTypesToReturn = [];
        $itemAssociations = $this->getItemAssociations();
        $searchData = $itemAssociations->where('origin_node_id', $originNodeIdentifier)->unique('association_type');
        if($searchData->isNotEmpty()) {
            $associationTypesToReturn = $searchData->values()->pluck('association_type')->toArray();
        }
        return $associationTypesToReturn;
    }

    private function helperToReturnDocumentSystemMetadataKeyValuePairsCsv(Document $document): string {
        $documentSystemMetadata = $this->getDocumentSystemMetadata();
        $documentArray = $document->toArray();
        $documentSystemMetadataKeyValuePairs = [];
        foreach($documentArray as $indexKey => $value) {
            if(!empty($value) ) {
                // clean special characters
                $replacedString = $this->replaceSpecialCharactersFromString($value, "#");
                $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
            
                $searchForMetadata = $documentSystemMetadata->where('internal_name', $indexKey);
                if($searchForMetadata->isNotEmpty()) {
                    $metadataId = $searchForMetadata->first()->metadata_id;
                    $keyValueString = "$metadataId:$value";
                    array_push($documentSystemMetadataKeyValuePairs, $keyValueString);
                }
            }
        }
        // create key value for 'license', 'language', 'subjects' system attributes 
        $documentLicense = $document->license;
        $documentLanguage = $document->language;
        $documentSubjects = $document->subjects;
        if(!empty($documentLicense)){
            if(!empty($documentLicense->license_text)){
                $licenseTextSearchMetadata = $documentSystemMetadata->where('internal_name', 'license_text');
                if($licenseTextSearchMetadata->isNotEmpty()) {
                    $licenseTextMetadataId = $licenseTextSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($documentLicense->license_text, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($documentSystemMetadataKeyValuePairs, "$licenseTextMetadataId:$value");
                }
            }
            if(!empty($documentLicense->description)){
                $licenseDescriptionSearchMetadata = $documentSystemMetadata->where('internal_name', 'license_description');
                if($licenseDescriptionSearchMetadata->isNotEmpty()) {
                    $licenseDescriptionMetadataId = $licenseDescriptionSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($documentLicense->description, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($documentSystemMetadataKeyValuePairs, "$licenseDescriptionMetadataId:$value");
                }
            }
            if(!empty($documentLicense->title)){
                $licenseTitleSearchMetadata = $documentSystemMetadata->where('internal_name', 'license_title');
                if($licenseTitleSearchMetadata->isNotEmpty()) {
                    $licenseTitleMetadataId = $licenseTitleSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($documentLicense->title, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($documentSystemMetadataKeyValuePairs, "$licenseTitleMetadataId:$value");
                }
            }
        }

        if(!empty($documentLanguage->short_code)) {
            $languageSearchMetadata = $documentSystemMetadata->where('internal_name', 'language');
            if($languageSearchMetadata->isNotEmpty()) {
                $languageMetadataId = $languageSearchMetadata->first()->metadata_id;
                // clean special characters
                $replacedString = $this->replaceSpecialCharactersFromString($documentLanguage->short_code, "#");
                $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                array_push($documentSystemMetadataKeyValuePairs, "$languageMetadataId:$value");
            }
        }

        if(!empty($documentSubjects)) {
            $documentSubject = $documentSubjects->first();

            if(!empty($documentSubject->title)){
                $subjectTitleSearchMetadata = $documentSystemMetadata->where('internal_name', 'subject_title');
                if($subjectTitleSearchMetadata->isNotEmpty()) {
                    $subjectTitleMetadataId = $subjectTitleSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($documentSubject->title, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($documentSystemMetadataKeyValuePairs, "$subjectTitleMetadataId:$value");
                }
            }

            if(!empty($documentSubject->description)){
                $subjectDescriptionSearchMetadata = $documentSystemMetadata->where('internal_name', 'subject_description');
                if($subjectDescriptionSearchMetadata->isNotEmpty()) {
                    $subjectDescriptionMetadataId = $subjectDescriptionSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($documentSubject->description, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($documentSystemMetadataKeyValuePairs, "$subjectDescriptionMetadataId:$value");
                }
            } 
        }

        // return $this->arrayContentToCommaeSeparatedString($documentSystemMetadataKeyValuePairs);
        return implode(", ",$documentSystemMetadataKeyValuePairs);
    }

    private function helperToReturnDocumentCustomMetadataKeyValuePairsCsv(Document $document): string {
        $documentAllMetadata = $this->getDocumentMetadata();
        $documentSystemMetadata = $this->getDocumentSystemMetadata();
        $documentCustomMetadataKeyValuePairs = [];
        foreach($documentAllMetadata as $metadata) {
            $metadataValue = $metadata->metadata_value;
            $metadataId = $metadata->metadata_id;
            $systemMetadataSearchResult = $documentSystemMetadata->where('metadata_id', $metadataId);
            if($systemMetadataSearchResult->isEmpty() && !empty($metadataValue)) {
                // clean special characters
                $replacedString = $this->replaceSpecialCharactersFromString($metadataValue, "#");
                $metadataValue = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                $keyValueString = "$metadataId:$metadataValue";
                array_push($documentCustomMetadataKeyValuePairs, $keyValueString);
            }
        }
        // return $this->arrayContentToCommaeSeparatedString($documentCustomMetadataKeyValuePairs);
        return implode(", ",$documentCustomMetadataKeyValuePairs);
    }

    private function helperToReturnItemSystemMetadataKeyValuePairsCsv(Item $item): string {
        $itemSystemMetadata = $this->getItemSystemMetadata();
        $itemArray = $item->toArray();
        $itemSystemMetadataKeyValuePairs = [];
        foreach($itemArray as $indexKey => $value) {
            if(!empty($value)) {
                $searchForMetadata = $itemSystemMetadata->where('internal_name', $indexKey);
                if($searchForMetadata->isNotEmpty()) {
                    $metadataId = $searchForMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($value, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    $keyValueString = "$metadataId:$value";
                    array_push($itemSystemMetadataKeyValuePairs, $keyValueString);
                }
            }
        }

        // create key value for 'license', 'language', 'concepts' system attributes 
        $licenseId = $item->license_id ?: "";
        $itemLicense = $this->helperToSearchAndReturnItemLicense($licenseId);
        if(!empty($itemLicense->license_id)){
            if(!empty($itemLicense->license_text)){
                $licenseTextSearchMetadata = $itemSystemMetadata->where('internal_name', 'license_text');
                if($licenseTextSearchMetadata->isNotEmpty()) {
                    $licenseTextMetadataId = $licenseTextSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemLicense->license_text, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$licenseTextMetadataId:$value");
                }
            }
            if(!empty($itemLicense->description)){
                $licenseDescriptionSearchMetadata = $itemSystemMetadata->where('internal_name', 'license_description');
                if($licenseDescriptionSearchMetadata->isNotEmpty()) {
                    $licenseDescriptionMetadataId = $licenseDescriptionSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemLicense->description, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$licenseDescriptionMetadataId:$value");
                }
            }
            if(!empty($itemLicense->title)){
                $licenseTitleSearchMetadata = $itemSystemMetadata->where('internal_name', 'license_title');
                if($licenseTitleSearchMetadata->isNotEmpty()) {
                    $licenseTitleMetadataId = $licenseTitleSearchMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemLicense->title, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$licenseTitleMetadataId:$value");
                }
            }
        }

        $languageId = $item->language_id ?: "";
        $itemLanguage = $this->helperToSearchAndReturnItemLanguage($languageId);
        if(!empty($itemLanguage->short_code)) {
            $languageSearchMetadata = $itemSystemMetadata->where('internal_name', 'language');
            if($languageSearchMetadata->isNotEmpty()) {
                $languageMetadataId = $itemSystemMetadata->first()->metadata_id;
                // clean special characters
                $replacedString = $this->replaceSpecialCharactersFromString($itemLanguage->short_code, "#");
                $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                array_push($itemSystemMetadataKeyValuePairs, "$languageMetadataId:$value");
            }
        }

        $conceptId = $item->concept_id ?: "";
        $itemConcept = $this->helperToSearchAndReturnItemConcept($conceptId);
        if(!empty($itemConcept->concept_id)) {
            if(!empty($itemConcept->title)) {
                $conceptSearchTitleMetadata = $itemSystemMetadata->where('internal_name', 'concept_title');
                if($conceptSearchTitleMetadata->isNotEmpty()) {
                    $conceptTitleMetadataId = $conceptSearchTitleMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemConcept->title, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptTitleMetadataId:$value");
                }
            }

            if(!empty($itemConcept->keywords)) {
                $conceptSearchKeywordsMetadata = $itemSystemMetadata->where('internal_name', 'concept_keywords');
                if($conceptSearchKeywordsMetadata->isNotEmpty()) {
                    $conceptKeywordsMetadataId = $conceptSearchKeywordsMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemConcept->keywords, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptKeywordsMetadataId:$value");
                }
            }
            
            if(!empty($itemConcept->hierarchy_code)) {
                $conceptSearchHierarchyCodeMetadata = $itemSystemMetadata->where('internal_name', 'concept_hierarchy_code');
                if($conceptSearchHierarchyCodeMetadata->isNotEmpty()) {
                    $conceptHierarchyCodeMetadataId = $conceptSearchHierarchyCodeMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemConcept->hierarchy_code, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptHierarchyCodeMetadataId:$value");
                }
            }
            
            if(!empty($itemConcept->description)) {
                $conceptSearchDescriptionMetadata = $itemSystemMetadata->where('internal_name', 'concept_description');
                if($conceptSearchDescriptionMetadata->isNotEmpty()) {
                    $conceptDescriptionCodeMetadataId = $conceptSearchDescriptionMetadata->first()->metadata_id;
                    // clean special characters
                    $replacedString = $this->replaceSpecialCharactersFromString($itemConcept->description, "#");
                    $value = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptDescriptionCodeMetadataId:$value");
                }
            }
        }

        // return $this->arrayContentToCommaeSeparatedString($itemSystemMetadataKeyValuePairs);
        return implode(", ",$itemSystemMetadataKeyValuePairs);
    }

    private function helperToReturnItemCustomMetadataKeyValuePairsCsv(Item $item): string {
        $itemArray = $item->toArray();        
        $itemAllMetadata = $this->getItemMetadata();
        $itemSystemMetadata = $this->getItemSystemMetadata();
        $itemIntenentMetadata = $this->systemMetadataItemInTenant;
        $itemCustomMetadataKeyValuePairs = [];
        foreach($itemAllMetadata as $metadata) {
            $metadataValue = $metadata->metadata_value;
            $metadataId = $metadata->metadata_id;
                if(!in_array($metadataId,$itemIntenentMetadata) && !empty($metadataValue) && $itemArray['item_id']==$metadata->item_id ) {
                // clean special characters
                $replacedString = $this->replaceSpecialCharactersFromString($metadataValue, "#");
                $metadataValue = $this->replaceMultipleSpacesWithSingleSpace(str_replace( '#', " ", $replacedString ));
                $keyValueString = "$metadataId:$metadataValue";
                array_push($itemCustomMetadataKeyValuePairs, $keyValueString);
            }
        }
        // return $this->arrayContentToCommaeSeparatedString($itemCustomMetadataKeyValuePairs);
        return implode(", ",$itemCustomMetadataKeyValuePairs);
    }

    private function helperToSearchAndReturnItemLicense(string $licenseId) {
        $itemLicenses = $this->getItemLicenses();
        $searchResult = $itemLicenses->where('license_id', $licenseId);
        $dataToReturn = [];
        if($searchResult->isNotEmpty()) {
            $dataToReturn = $searchResult->first();
        }
        return $dataToReturn;
    }

    private function helperToSearchAndReturnItemLanguage(string $languageId) {
        $itemLanguages = $this->getItemLanguages();
        $searchResult = $itemLanguages->where('language_id', $languageId);
        $dataToReturn = [];
        if($searchResult->isNotEmpty()) {
            $dataToReturn = $searchResult->first();
        }
        return $dataToReturn;
    }

    private function helperToSearchAndReturnItemConcept(string $conceptId) {
        $itemConcepts = $this->getItemConcepts();
        $searchResult = $itemConcepts->where('concept_id', $conceptId);
        $dataToReturn = [];
        if($searchResult->isNotEmpty()) {
            $dataToReturn = $searchResult->first();
        }
        return $dataToReturn;
    }
}
