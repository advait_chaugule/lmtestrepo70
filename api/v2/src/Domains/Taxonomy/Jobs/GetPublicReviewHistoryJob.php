<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class GetPublicReviewHistoryJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $documentIdentifier)
    {
        $this->setRequestData($documentIdentifier);
    }

    public function setRequestData(array $data) {
        $this->documentIdentifier = $data;
    }

    public function getRequestData(): array {
        return $this->documentIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo)
    {
        $this->documentRepo = $documentRepo;
        $documentIdentifier=$this->documentIdentifier;
        return $this->documentRepo->getPublicReviewHistory($documentIdentifier);
    }
}
