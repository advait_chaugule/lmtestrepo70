<?php
namespace App\Domains\Taxonomy\Jobs;
ini_set('max_execution_time', 0);

use App\Data\Models\ItemAssociation;
use App\Data\Models\ProjectItem;
use App\Data\Models\Item;
use Lucid\Foundation\Job;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Domains\Http\Jobs\RespondWithJsonJob;
use App\Domains\Http\Jobs\RespondWithJsonErrorJob;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
class SetOrderedTaxonomyTreeV2Job extends Job
{
    use ErrorMessageHelper, StringHelper, DateHelpersTrait, UuidHelperTrait, CaseFrameworkTrait;

    private $documentIdentifier;
    private $documentRepository;
    private $itemRepository;
    private $itemAssociationRepository;
    private $userOrganizationId;
    private $inputData;
    private $savedDocumentObject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($documentIdentifier, $inputData)
    {
        //Set the private variables
        $this->documentIdentifier = $documentIdentifier;
        $this->inputData    =   $inputData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository,
    ItemRepositoryInterface $itemRepository,
    ItemAssociationRepositoryInterface $itemAssociationRepository,
    MetadataRepositoryInterface $metadataRepository)
    {
        $this->documentRepository           =   $documentRepository;
        $this->itemRepository               =   $itemRepository;
        $this->itemAssociationRepository    =   $itemAssociationRepository;
        $this->metadataRepository           =   $metadataRepository;

        $userPerformingAction = $this->inputData["auth_user"];
        $userOrganizationId = $userPerformingAction["organization_id"];
        $organizationCode       =   DB::table('organizations')->select('org_code')->where('organization_id', $userOrganizationId)->first()->org_code;
        $this->organizationCode = $organizationCode;
        $userId = $userPerformingAction["user_id"];
        $type      = $this->inputData['type'];
        $projectId = $this->inputData['project_id'];
        $oldItems = $this->inputData['old_items'];
        $newItems = $this->inputData['items'];
        $oldOriginNodeIdList = [];
        $newNodeIdList = [];
        $allNodesIds = [];
        $allNodesDetails = [];
        $oldOriginNodeIdList1 = [];
        foreach ($oldItems as $oldItemsK => $oldItemsV) {
            array_push($oldOriginNodeIdList,$oldItemsV['item_id']);
            array_push($oldOriginNodeIdList1,$oldItemsV['parent_id']);
        }
        foreach ($newItems as $newItemsK => $newItemsV) {
            array_push($allNodesIds,$newItemsV['item_id']);
        }
        $nodeDetails = DB::table('items')
                                ->select('items.*')
                                ->where('is_deleted',0)
                                ->whereIn('item_id',$allNodesIds)
                                ->get()->toArray();
        foreach ($nodeDetails as $nodeDetailsK => $nodeDetailsV) {
            $allNodesDetails[$nodeDetailsV->item_id] = $nodeDetailsV->full_statement;                
        }
        $currentDateTime    =   $this->createDateTime();
        $associationType    =   1;
        $newItemParentIdMapping = []; // Array to add only unique associations - Initialize array
        foreach ($newItems as $newItemsK => $newItemsV) {
            $checkItemParentId = $newItemsV['item_id']." ".$newItemsV['parent_id']; // Array to add only unique associations - Check for duplicates
            if(!empty($newItemsV['parent_id']) && !in_array($checkItemParentId, $newItemParentIdMapping))
            {
                $itemAssociationID = $this->createUniversalUniqueIdentifier();
                $uri = $this->getCaseApiUri("CFAssociations", $itemAssociationID, true, $this->organizationCode);
                $externalNodeuri = $this->getCaseApiUri("CFItems", $newItemsV['parent_id'], true, $this->organizationCode);
                $externalNodeTitle = (isset($allNodesDetails[$newItemsV['item_id']])) ? $allNodesDetails[$newItemsV['item_id']] : "";
                $newNodeIdList[] = [
                    "item_association_id" => $itemAssociationID,
                    "association_type" => $associationType,
                    "document_id" => $this->documentIdentifier,
                    "destination_document_id" => $this->documentIdentifier,
                    "is_reverse_association" => 0,
                    "association_group_id" => '',
                    "origin_node_id" => $newItemsV['item_id'],
                    "destination_node_id" => $newItemsV['parent_id'],
                    "sequence_number" => $newItemsV['list_enumeration'],
                    "external_node_title" => $externalNodeTitle,
                    "external_node_url" =>$externalNodeuri,
                    "created_at" => $currentDateTime,
                    "updated_at" => $currentDateTime,
                    "source_item_association_id" => $itemAssociationID,
                    "organization_id" => $userOrganizationId,
                    "uri"                           => $uri,
                    "description"                   => '', /**added blank to resolve column count issue ofr sql**/
                    "has_asset"                     => 0, /**added 0 to resolve column count issue ofr sql **/
                    "source_document_id" => $this->documentIdentifier,
                    "source_item_id" => $newItemsV['item_id'],
                    "target_document_id" => $this->documentIdentifier,
                    "target_item_id"    => $newItemsV['parent_id']
                ];
                $newItemParentIdMapping[] = $newItemsV['item_id']." ".$newItemsV['parent_id'];// Array to add only unique associations - Save values
                $updatedAt = $currentDateTime;
                $attributes = [
                    "updated_at"        =>  $updatedAt,
                    "organization_id"   =>  $userOrganizationId,
                    "updated_by"        =>  $userId,
                    "parent_id"         =>  $newItemsV['parent_id']
                ];
                DB::table('items')
                ->where('item_id',$newItemsV['item_id'])
                ->update($attributes);
            }

            
        }

       $ItemAssociationArry = array_chunk($newNodeIdList,4000);
       if(count($oldOriginNodeIdList1) > 0 && in_array($this->documentIdentifier, $oldOriginNodeIdList1))
       {
            array_push($oldOriginNodeIdList,$this->documentIdentifier);
       }
       ItemAssociation::whereIn('source_item_id', $oldOriginNodeIdList)->whereIn('target_item_id', $oldOriginNodeIdList)->where('document_id','=',$this->documentIdentifier)->where('association_type','=',1)->delete();
      
      
        foreach($ItemAssociationArry as $ItemAssociationArryK => $ItemAssociationArryV)
        {
            $this->itemAssociationRepository->saveMultiple($ItemAssociationArryV);
        }

        // raise delta document update to cloud search each time document date is updated
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_document"];
        $eventData = [
            "type_id" => $this->documentIdentifier,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
}
