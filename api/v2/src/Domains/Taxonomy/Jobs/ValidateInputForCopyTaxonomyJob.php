<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Taxonomy\Validators\CopyTaxonomyValidator;

class ValidateInputForCopyTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CopyTaxonomyValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->errors()->all();
    }
}
