<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;

use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;

class PrepareTaxonomyPackageDataForCsvExportJob extends Job
{

    use CaseFrameworkTrait;

    private $metadataRepository;
    private $documentRepository;
    private $itemRepository;
    private $itemAssociationRepository;

    private $documentIdentifier;
    private $requestingUser;
    private $requestingUserOrganization;
    private $csvHeader;

    private $savedDocument;
    private $tenantMetadata;
    private $savedItems;
    private $savedItemsToSearchWith;
    private $savedIsChildOfAssociations;

    private $parsedTaxonomyName;
    private $parsedPackageCollection;
    private $jobResponseData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestingUserDetails)
    {
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setRequestingUserOrganizationId($requestingUserDetails["organization_id"]);
        $this->setCsvHeader();
    }

    public function setDocumentIdentifier(string $data) {
        $this->documentIdentifier = $data;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setRequestingUserOrganizationId(string $data) {
        $this->requestingUserOrganization = $data;
    }

    public function getRequestingUserOrganizationId() {
        return $this->requestingUserOrganization;
    }

    public function setCsvHeader() {
        $this->csvHeader =  config('excel')['exports']['csv']['taxonomy']['column_headers'];
    }

    private function getCsvHeader(): array {
        return $this->csvHeader;
    }

    /************************************Setters and getters for saved CASE related Entities***********************************************/
    
    public function setSavedDocument(Document $data) {
        $this->savedDocument = $data;
    }

    public function getSavedDocument(): Document {
        return $this->savedDocument;
    }
    
    public function setTenantMetadata(Collection $data) {
        $this->tenantMetadata = $data;
    }

    public function getTenantMetadata(): Collection {
        return $this->tenantMetadata;
    }

    public function setSavedItems(Collection $data) {
        $this->savedItems = $data;
    }

    public function getSavedItems(): Collection {
        return $this->savedItems;
    }

    public function setSavedItemsToSearchWith(Collection $data) {
        $this->savedItemsToSearchWith = $data;
    }

    public function getSavedItemsToSearchWith(): Collection {
        return $this->savedItemsToSearchWith;
    }

    public function setSavedIsChildOfAssociations(Collection $data) {
        $this->savedIsChildOfAssociations = $data;
    }

    public function getSavedIsChildOfAssociations(): Collection {
        return $this->savedIsChildOfAssociations;
    }

    /**********************setters and getters for all relevant parsed data for taxonomy csv***********************/

    public function setParsedPackage(Collection $data) {
        $this->parsedPackageCollection = $data;
    }

    public function getParsedPackage(): Collection {
        return $this->parsedPackageCollection;
    }

    public function setParsedTaxonomyName(string $data) {
        $this->parsedTaxonomyName = $data;
    }

    public function getParsedTaxonomyName(): string {
        return $this->parsedTaxonomyName;
    }
    
    public function setJobResponseData(array $data) {
        $this->jobResponseData = $data;
    }

    public function getJobResponseData(): array {
        return $this->jobResponseData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        MetadataRepositoryInterface $metadataRepository,
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository
    )
    {
        // set the db repositories
        $this->metadataRepository = $metadataRepository;
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;

        $this->fetchAndSetTenantMetadata();
        $this->fetchAndSetSavedDocument();
        $this->fetchAndSetSavedItems();
        $this->fetchAndSetSavedIsChildOfItemAssociations();

        $this->parseAndSetTaxonomyName();
        $this->parsePackageDataAndSetIt();
        $this->prepareJobResponseDataAndSetIt();
        return $this->getJobResponseData();
    }

    /*******************************************Fetch and set case related data********************************************/

    private function fetchAndSetTenantMetadata() {
        $organizationId = $this->getRequestingUserOrganizationId();
        $returnCollection = true;
        $fieldsToReturn = ["internal_name"];
        $dataFetchingConditionalClause = [ "organization_id" => $organizationId, "is_active" => 1 ,"is_deleted" => 0 ];
        $tenantMetadata =    $this->metadataRepository->findByAttributesWithSpecifiedFields(
                                $returnCollection, 
                                $fieldsToReturn, 
                                $dataFetchingConditionalClause
                            );
        $this->setTenantMetadata($tenantMetadata);
    }

    private function fetchAndSetSavedDocument() {
        $documentId = $this->getDocumentIdentifier();
        $organizationId = $this->getRequestingUserOrganizationId();
        $returnCollection = false;
        $fieldsToReturn = ["document_id","title","source_document_id","created_at","updated_at"];
        $dataFetchingConditionalClause = [ 
            "is_deleted" => 0,
            "document_id" => $documentId,
            "organization_id" => $organizationId
        ];
        $savedDocument =    $this->documentRepository->findByAttributesWithSpecifiedFields(
                                $returnCollection, 
                                $fieldsToReturn, 
                                $dataFetchingConditionalClause
                            );
        $this->setSavedDocument($savedDocument);
    }

    private function fetchAndSetSavedItems() {
        $savedDocument = $this->getSavedDocument();
        $documentId = $savedDocument->document_id;
        $organizationId = $this->getRequestingUserOrganizationId();
        $returnCollection = true;
        $fieldsToReturn = ["item_id","source_item_id","full_statement","human_coding_scheme","list_enumeration","created_at","updated_at"];
        $dataFetchingConditionalClause = [ "document_id" => $documentId, "organization_id" => $organizationId, "is_deleted" => 0 ];
        $savedItems = $this->itemRepository->findByAttributesWithSpecifiedFields(
                            $returnCollection, 
                            $fieldsToReturn, 
                            $dataFetchingConditionalClause
                        );
        $this->setSavedItems($savedItems);
        $this->setSavedItemsToSearchWith($savedItems);
    }

    private function fetchAndSetSavedIsChildOfItemAssociations() {
        $savedDocument = $this->getSavedDocument();
        $documentId = $savedDocument->document_id;
        $organizationId = $this->getRequestingUserOrganizationId();
        $associationTypeNumber = $this->getSystemSpecifiedAssociationTypeNumber('isChildOf');
        $returnCollection = true;
        $fieldsToReturn = ["origin_node_id","destination_node_id","source_item_association_id"];
        $dataFetchingConditionalClause = [
            "document_id" => $documentId, 
            "organization_id" => $organizationId,
            "association_type" => $associationTypeNumber
        ];
        $savedAssociationsCollection = $this->itemAssociationRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, 
                                            $fieldsToReturn, 
                                            $dataFetchingConditionalClause
                                        );
        $savedAssociations = $savedAssociationsCollection->filter(function ($item, $key) {
                                return !empty($item->origin_node_id) && 
                                       !empty($item->destination_node_id) && 
                                       !empty($item->source_item_association_id);
                            });
        $this->setSavedIsChildOfAssociations($savedAssociations);
    }

    private function parseAndSetTaxonomyName() {
        $savedDocument = $this->getSavedDocument();
        $title = !empty($savedDocument->title) ? $savedDocument->title : $savedDocument->source_document_id;
        $this->setParsedTaxonomyName($title);
    }

    private function parsePackageDataAndSetIt() {
        $csvHeaderDataToExport = $this->getCsvHeader();
        $savedDocument = $this->getSavedDocument();
        $parsedItemExportData = $this->parseAndReturnItemsToExport($savedDocument);
        $parsedDocumentExportData = $this->parseAndReturnDocumentToExport($savedDocument);
        $packageData = array_prepend($parsedItemExportData, $parsedDocumentExportData);
        $packageData = array_prepend($packageData, $csvHeaderDataToExport);
        $packageCollection = collect($packageData);
        $this->setParsedPackage($packageCollection);
    }

    private function parseAndReturnItemsToExport(Document $savedDocument): array {
        $rootNodeIdentifier = $savedDocument->source_document_id;
        $itemsToParse = $this->getSavedItems();
        $itemsDataToExport = [];
        $nodeDeleteStatus = '0';
        foreach($itemsToParse as $item) {
            $primaryItemIdentifier = $item->item_id;
            $nodeIdentifier = $item->source_item_id;
            $parentNodeIdentifier = $this->helperToSearchAndReturnForParentNodeIdentifier($primaryItemIdentifier, $rootNodeIdentifier);
            $nodeType = "item";
            $title = "";
            $humanCodingScheme = $this->isMetadataActive('human_coding_scheme') ? 
                                (!empty($item->human_coding_scheme) ? $item->human_coding_scheme : "") : 
                                "";
            $listEnumeration = $this->isMetadataActive('list_enumeration') ? 
                                (!empty($item->list_enumeration) ? $item->list_enumeration : "") : 
                                "";
           $fullStatement = $this->isMetadataActive('full_statement') ? (!empty($item->full_statement) ? $item->full_statement : "") : "";
           $createdAt = (!empty($item->created_at) && $item->created_at!=="0000-00-00 00:00:00") ? $item->created_at->toDateTimeString() : "";

            $itemsDataToExport[] = [
                $nodeIdentifier, 
                $parentNodeIdentifier, 
                $rootNodeIdentifier, 
                $nodeType, 
                $title, 
                $humanCodingScheme, 
                $listEnumeration, 
                $fullStatement,
                $createdAt,
                $nodeDeleteStatus
            ];
        }
        return $itemsDataToExport;
    }

    private function parseAndReturnDocumentToExport(Document $savedDocument): array {
        $rootNodeIdentifier = $savedDocument->source_document_id;
        $nodeIdentifier = $rootNodeIdentifier;
        $parentNodeIdentifier = "";
        $nodeType = "document";
        $humanCodingScheme = "";
        $listEnumeration = "";
        $fullStatement = "";
        $title = $this->isMetadataActive("title") ? 
                 (!empty($savedDocument->title) ? $savedDocument->title : "") : 
                 "";
        $createdAt = (!empty($savedDocument->created_at) && $savedDocument->created_at!=="0000-00-00 00:00:00") ? 
                    $savedDocument->created_at->toDateTimeString() : 
                    "";
        $nodeDeleteStatus = '0';
        $documentDataToExport = [
                                    $nodeIdentifier, 
                                    $parentNodeIdentifier, 
                                    $rootNodeIdentifier, 
                                    $nodeType, 
                                    $title, 
                                    $humanCodingScheme, 
                                    $listEnumeration, 
                                    $fullStatement,
                                    $createdAt,
                                    $nodeDeleteStatus
                                ];
        return $documentDataToExport;
    }

    private function prepareJobResponseDataAndSetIt() {
        $parsedPackageData = $this->getParsedPackage();
        $jobResponseData =  [
                                "taxonomyData" => $this->getParsedPackage(),
                                "taxonomyName" => $this->getParsedTaxonomyName()
                            ];
        $this->setJobResponseData($jobResponseData);
    }

    private function helperToSearchAndReturnForParentNodeIdentifier(string $nodeIdToSearchFor, string $rootNodeIdentifier): string {
        $isChildOfNodeAssociations = $this->getSavedIsChildOfAssociations();
        // by default set the parent node id to root node id so that no node is abondoned in the exported taxonomy
        $parentNodeIdentifier = $rootNodeIdentifier;
        foreach($isChildOfNodeAssociations as $key => $association) {
            $originNodeId = $association->origin_node_id;
            if($nodeIdToSearchFor===$originNodeId) {
                $destinationSourceItemIdentifier = $this->helperToSearchForItemSourceIdentifier($association->destination_node_id);
                // if no source item identifier is found, set root document source identifer so that the node is not abandoned
                $parentNodeIdentifier =  !empty($destinationSourceItemIdentifier) ? $destinationSourceItemIdentifier : $rootNodeIdentifier;
                // remove the association from the original collection 
                $isChildOfNodeAssociations->forget($key);
                break;
            }
        }
        return $parentNodeIdentifier;
    }

    private function helperToSearchForItemSourceIdentifier(string $nodeIdToSearchFor): string {
        $savedItems = $this->getSavedItemsToSearchWith();
        $itemSourceId = "";
        foreach($savedItems as $key=>$item) {
            if($item->item_id===$nodeIdToSearchFor) {
                $itemSourceId = $item->source_item_id;
                // remove the item from the original collection 
                $savedItems->forget($key);
                break;
            }
        }
        return $itemSourceId;
    }

    private function isMetadataActive(string $internalNameToSearchFor): bool {
        $metadataCollection = $this->getTenantMetadata();
        return $metadataCollection->where("internal_name", $internalNameToSearchFor)->isNotEmpty();
    }
}
