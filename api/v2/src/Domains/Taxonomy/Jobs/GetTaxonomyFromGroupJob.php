<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class GetTaxonomyFromGroupJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $groupId;
    public function __construct($groupId,$organizationId,$userId)
    {
        $this->groupId = $groupId;
        $this->organizationId = $organizationId;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groupId = $this->groupId;
        $organizationId = $this->organizationId;
        $userId = $this->userId;
        
        $query = DB::table('group_document')
        ->select('group_document.document_id','documents.title as document_name')
        ->join('documents','documents.document_id','=','group_document.document_id');

        if(!empty($groupId))
            $query->where('group_id','=',$groupId);
        return $groupDetail = $query->where('is_deleted','=','0')->get()->toArray();
    }
}
