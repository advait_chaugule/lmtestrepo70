<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class GetPublishedTaxonomyJsonJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $publisherDocumentId,string $publisherOrgId,string $version='',string $tag='')
    {
        $this->publisherDocumentId = $publisherDocumentId;
        $this->publisherOrgId = $publisherOrgId;
        $this->version = $version;
        $this->tag = $tag;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('taxonomy_pubstate_history')
                                ->select('tag','publish_number','taxonomy_json_file_s3','custom_json_file_s3','publish_date')
                                ->where(['document_id'=>$this->publisherDocumentId,'organization_id'=>$this->publisherOrgId]);
        if(!empty($this->version)){
            $query->where('publish_number',$this->version);
        }
        if(!empty($this->tag)){
            $query->where('tag',$this->tag);
        }
        $publishedVersion = $query->where('is_deleted',0)->orderBy('publish_number','desc')->first();
        return (!isset($publishedVersion->publish_number)) ? false : $publishedVersion;
    }
}
