<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\VersionSubscription;

class UpdateSubscribedTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $documentDetails;
    private $requestData;

    public function __construct($subscriptionId,$isActive)
    {
        $this->subscriptionId = $subscriptionId;
        $this->isActive = $isActive;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscriptionId = $this->subscriptionId;
        $isActive       = $this->isActive==='true' ? 1 : 0;
        $subscriptionFetch = VersionSubscription::where([
            'subscription_id' => $subscriptionId
            ])->first();
        if(isset($subscriptionFetch->subscription_id)){
            VersionSubscription::where('subscription_id',$subscriptionId)
            ->update(['is_active'=>$isActive]);        
            return true;
        }
        else{
            return false;
        }
    }

}
