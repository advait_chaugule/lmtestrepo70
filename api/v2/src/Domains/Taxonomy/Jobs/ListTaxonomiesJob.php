<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\TaxonomyRepositoryInterface;

class ListTaxonomiesJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxonomyRepositoryInterface $taxonomyRepo)
    {
        return $taxonomyRepo->all();
    }
}
