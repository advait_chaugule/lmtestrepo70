<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class GetGroupForTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $groupId;
    public function __construct($groupId,$organizationId,$userId)
    {
        $this->groupId = $groupId;
        $this->organizationId = $organizationId;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $groupId = $this->groupId;
        $organizationId = $this->organizationId;
        $userId = $this->userId;
        $query = DB::table('group')->select('name as group_name','group_id','is_user_default','is_default');

        if(!empty($groupId))
            $query->where('group_id','=',$groupId);
        $query->where('organization_id','=',$organizationId);
        $query->where(function ($query) use ($userId) {
            $query->where('user_id','=',$userId)->orWhere('is_default', '=', '1');
        });
        
        $groupDetail = $query->where('is_deleted','=','0')->get()->toArray();
        // If has records and group id passed, send specific records. Otherwise send all records or a blank array
        return (!empty($groupId) && !empty($groupDetail)) ? $groupDetail[0] : $groupDetail;
    }
}
