<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

class GetProjectListAssociatedWithTaxonomyJob extends Job
{
    private $documentIdentifier;
    private $organizationIdentifier;

    private $documentRepository;
    private $projectRepository;

    private $associatedProjects;
    private $openCommentCountPerProject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, string $organizationIdentifier)
    {
        //Set the private variable
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setOrganizationidentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ProjectRepositoryInterface $projectRepository, ThreadRepositoryInterface $threadRepository)
    {
        //Set the db handlers
        $this->documentRepository   =   $documentRepository;
        $this->projectRepository    =   $projectRepository;
        $this->threadRepository     =   $threadRepository;

        // fetch and set projects associated to documents
        $this->fetchAndSetProjectsAssociatedToDocument();

        // fetch and set projects open comments
        $this->fetchAndSetProjectOpenComments();

        //Parse the project list with Project Workflow status
        return $this->parseProjectList();
    }

    // Public Getter and Setter methods
    public function setDocumentIdentifier($identifier) {
        $this->documentIdentifier = $identifier;
    }

    public function getDocumentIdentifier() : string {
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() : string {
        return $this->organizationIdentifier;
    }

    public function setProjectsAssociatedWithDocument($data) {
        $this->associatedProjects   =   $data;
    }

    public function getProjectsAssociatedWithDocument() {
        return $this->associatedProjects;
    }

    public function setOpenCommentCountPerProject($data) {
        $this->openCommentCountPerProject   =   $data;
    }

    public function getOpenCommentCountPerProject() {
        return $this->openCommentCountPerProject;
    }

    private function fetchAndSetProjectsAssociatedToDocument() {
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['project_id', 'project_name', 'workflow_id', 'document_id', 'is_deleted', 'updated_by', 'current_workflow_stage_id'];
        $keyValuePairs      =   ['document_id'  => $documentIdentifier, 'is_deleted' => 0, 'organization_id'    => $organizationIdentifier];
        $relations          =   ['project_status', 'user', 'items'];
        
        $data = $this->projectRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        $this->setProjectsAssociatedWithDocument($data);
    }

    private function fetchAndSetProjectOpenComments() {
        $openDocumentCommentList    =   [];
        $openItemCommentList        =   [];
        $projectItems               =   [];
        
        $projectAssociated  =   $this->getProjectsAssociatedWithDocument();
        $documentIdentifier =   $this->getDocumentIdentifier();

        $documentDetail     =   $this->documentRepository->find($documentIdentifier);
        if(!empty($documentDetail->project_id)) {
            $attributeIn        =   'thread_source_id';
            $containedInValues  =   [$documentIdentifier];
            $returnCollection   =   true;
            $fieldsToReturn     =   ['thread_id'];
            $keyValuePairs      =   ['is_deleted'    => '0'];
            $notInKeyValuePairs =   ['status'   =>  '4'];

            $openDocumentComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                    $attributeIn, 
                    $containedInValues,
                    $returnCollection,
                    $fieldsToReturn,
                    $keyValuePairs,
                    $notInKeyValuePairs)->toArray();

            //dd(count($openDocumentComments));

            $openDocumentCommentList[$documentDetail->project_id][0]   =   count($openDocumentComments);
        }

        foreach($projectAssociated as $project) {
            foreach($project->items as $itemAssociated) {
                $projectItems[$project->project_id]['items'][]   =   $itemAssociated->item_id;  
            }
        }
        
        foreach($projectItems as $key => $itemIds) {
            $attributeIn        =   'thread_source_id';
            $containedInValues  =   $itemIds['items'];
            $returnCollection   =   true;
            $fieldsToReturn     =   ['thread_id'];
            $keyValuePairs      =   ['is_deleted'    => '0'];
            $notInKeyValuePairs =   ['status'   =>  '4'];

            /* $openComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                $attributeIn, 
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs )->toArray(); */

            $openComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                    $attributeIn, 
                    $containedInValues,
                    $returnCollection,
                    $fieldsToReturn,
                    $keyValuePairs,
                    $notInKeyValuePairs)->toArray();

            $openItemCommentList[$key][]    =   count($openComments);

            if(isset($openDocumentCommentList[$key])) {
                $openItemCommentList[$key][0]    =  $openItemCommentList[$key][0] +  $openDocumentCommentList[$key][0];
            }
            
        }
        
        
        $this->setOpenCommentCountPerProject($openItemCommentList);
    }

    private function parseProjectList() {
        $parsedProjects     =   [];
        $projectAssociated          =   $this->getProjectsAssociatedWithDocument();
        $openCommentCountPerProject =   $this->getOpenCommentCountPerProject();
        
        foreach($projectAssociated  as $project) {
            $parsedProjects[] = [
                'project_id'            =>  $project->project_id,
                'project_name'          =>  !empty($project->project_name) ? $project->project_name : '',
                'workflow_status'       =>  !empty($project->project_status->stage_name) ? $project->project_status->stage_name : "",
                'open_comment_count'    =>  $openCommentCountPerProject[$project->project_id][0],
                // only project level last_changed by added, will be dynamic after workflow and LRS implementation
                'last_changed_by'       =>  !empty($project->user->first_name) ? $project->user->first_name : "",
            ];
        }

        return $parsedProjects;
    }
}

