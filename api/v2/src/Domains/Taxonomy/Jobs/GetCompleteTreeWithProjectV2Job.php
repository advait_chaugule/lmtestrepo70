<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Models\Document;
use App\Data\Models\ProjectItem;
use App\Data\Models\Thread;
use App\Data\Models\Item;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;

use App\Services\Api\Traits\ArrayHelper;
use DB;

class GetCompleteTreeWithProjectV2Job extends Job
{
    use ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $itemAssociationsRepository;
    private $projectRepository;
    private $nodeTypeRepository;

    private $documentIdentifier;
    private $requestUserDetails;

    private $document;
    private $items;
    private $itemAssociationsUnderDocument;
    private $projectIdsAssociated;
    private $projectItemMapping;
    private $projectsAssociated;
    private $nodeTypes;
    private $parentAndDocumentId;

    private $transformedDocument;
    private $transformedItems;
    private $transformedItemAssociationsUnderDocument;

    private $tree;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public function __construct(array $requestUserDetails,string $projectId)
    {
        $this->requestUserDetails = $requestUserDetails;
        $this->projectIdentifier = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ItemAssociationRepositoryInterface $itemAssociationsRepository,
        ProjectRepositoryInterface $projectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $this->projectRepository = $projectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;


        
        //$documentIdentifier =   $this->documentIdentifier;
        $input              =   $this->requestUserDetails;
       
        $organizationId     =   $input["organization_id"];
        $searchProjectItemArray = [];
        $projectItems = ProjectItem::where('project_id',$this->projectIdentifier)->where('is_deleted','=',0)->where('is_editable','=',1)->get();
       
        if(count($projectItems) > 0)
        {
            foreach($projectItems as $projectItem) {
                array_push($searchProjectItemArray,$projectItem->item_id);
                $projectItemEditable[$projectItem->item_id] = $projectItem->is_editable;

            }
        }
        $getDocument = Item::select('document_id')->whereIn('item_id',$searchProjectItemArray)->first()->toArray();
       
        $documentIdentifier = $getDocument['document_id'];
       
        //Get Document Details
        $returnCollection           =   false;
        $fieldsToReturn             =   ["document_id", "title", "node_type_id", "project_id", "node_type_id", "adoption_status","title_html"];
        $relations                  =   ['nodeType'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];

        //dd($conditionalKeyValuePair);

        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $nodeType = !empty($document->nodeType->node_type_id) ? $document->nodeType->title : "";
        $parsedDocument = [];
        $isDocumentEditable = 0;
        if(!empty($document->project_id) && $document->project_id == $this->projectIdentifier)         //added for ACMT-3545
        {
            $isDocumentEditable = 1;
        }
        $searchItemArray = [];
        
        array_push($searchProjectItemArray,$document->document_id);
        $openCommentCountList = DB::table('threads')->select(array('thread_source_id',DB::raw('COUNT(thread_id) as comment_count')))->whereIn('thread_source_id',$searchProjectItemArray)->where('status','=',1)->where('is_deleted','=',0)->groupBy('thread_source_id')->get();
       
        $projectItemCommentCount = [];
        if(count($openCommentCountList) > 0)
        {
            foreach($openCommentCountList as $openCommentCount) {
               
                $projectItemCommentCount[$openCommentCount->thread_source_id] = $openCommentCount->comment_count;

            }
        }
  
            $parsedDocument = [
                "id"                    =>  $document->document_id,
                "title"                 =>  !empty($document->title) ? $document->title : "",
                "title_html"            =>  !empty($document->title_html) ? $document->title_html : "",
                "human_coding_scheme"   =>  "",
                "list_enumeration"      =>  "",
                "sequence_number"      =>  "",
                "full_statement"        =>  "",
                "status"                =>  $document->adoption_status,    
                "node_type"             =>  $nodeType,
                "metadataType"          =>  $nodeType,
                "node_type_id"          =>  $document->node_type_id,
                "is_editable"           =>  $isDocumentEditable,
                "is_document"           =>  1,
                "project_enabled"       =>  !empty($document->project_id) ? 1 : 0,
                "open_comment_count"    =>  !empty($projectItemCommentCount[$document->document_id]) ? $projectItemCommentCount[$document->document_id] : 0,
                "is_orphan"             =>  0,
            ];
       
       
        
        //Get Item Details
        $returnCollection           =   true;
        $fieldsToReturn             =   ["item_id", "human_coding_scheme", "list_enumeration","full_statement", "node_type_id", "source_item_id","human_coding_scheme_html","full_statement_html",];
        $relations                  =   ['nodeType', 'customMetadata'];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId, "is_deleted" => 0 ];
        
        $items = $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        
                //Relation for taxonomy
                $returnCollection           =   true;
                $fieldsToReturn             =   ["origin_node_id", "destination_node_id","sequence_number"];
                $conditionalKeyValuePair    =   [ 
                    "document_id" =>  $documentIdentifier,
                    "organization_id" => $organizationId,
                    "is_deleted" => 0,
                    "association_type" => 1 // 1 for isChildOf relation
                ];
        
                $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                                    $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                                 );
             $itemSequenceList = [];

        foreach($itemAssociationsUnderDocument as $itemAssociation) 
        {
                 $itemSequenceList[$itemAssociation->origin_node_id] = $itemAssociation->sequence_number;
        }          
                                        
        foreach($items as $item) {
                $searchItemArray[] =
                [
                    "id" => $item->item_id,
                    "title" => "",
                    "human_coding_scheme"   => !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "",
                    "human_coding_scheme_html"   => !empty($item->human_coding_scheme_html) ? $item->human_coding_scheme_html : $item->human_coding_scheme,
                    "list_enumeration"      => !empty($item->list_enumeration) ? $item->list_enumeration : "",
                    "sequence_number"       => !empty($itemSequenceList[$item->item_id]) ? $itemSequenceList[$item->item_id] : "",
                    "full_statement"        => !empty($item->full_statement) ? $item->full_statement : "",
                    "full_statement_html"   => !empty($item->full_statement_html) ? $item->full_statement_html : $item->full_statement,
                    "node_type"             => !empty($item->nodeType) ? $item->nodeType->title : "",
                    "metadataType"          => !empty($item->nodeType) ? $item->nodeType->title : "",
                    "node_type_id"          => !empty($item->node_type_id) ? $item->node_type_id : "",
                    "item_type"             => "",
                    "is_document"           => 0,
                    "project_enabled"       => in_array($item->item_id,$searchProjectItemArray) ? 1 : 0,
                    "is_editable"           => !empty($projectItemEditable[$item->item_id]) ?  $projectItemEditable[$item->item_id] : 0,
                    "item_usage_count"      => 0,
                    "open_comment_count"    => !empty($projectItemCommentCount[$item->item_id]) ? $projectItemCommentCount[$item->item_id] : 0,
                ];   
        }

            /* This code for handle the orphaned node condition ACMT-953*/
            $itemArr = [];
            $itemArr1 = [];
            if(!empty($searchItemArray))
            {    
                foreach($searchItemArray as $searchItemArrayK => $searchItemArrayV)
                {
                    array_push($itemArr,$searchItemArrayV['id']);
                    array_push($itemArr1,$searchItemArrayV['id']);
                }
            }
            if(!empty($document->document_id))
            {
                array_push($itemArr,$document->document_id);
            }
        $parsedItemAssociations = [];
        $parsedOrginNodeItemArr = []; 
        foreach($itemAssociationsUnderDocument as $itemAssociation)
        {
            $originNodeId = $itemAssociation->origin_node_id;
            $destinationNodeId = $itemAssociation->destination_node_id;

                if(!empty($originNodeId) && !empty($destinationNodeId) && ($originNodeId != $destinationNodeId) && in_array($destinationNodeId,$itemArr)) 
                {
                        $parsedItemAssociations[] = [
                            "child_id" => $itemAssociation->origin_node_id,
                            "parent_id" => $itemAssociation->destination_node_id,
                            "sequence_number" => $itemAssociation->sequence_number
                        ]; 
                        array_push($parsedOrginNodeItemArr,$itemAssociation->origin_node_id); 
                }
        }
        $orphanedNodeArr=array_diff($itemArr1,$parsedOrginNodeItemArr);
        if(!empty($searchItemArray))
            {    
                foreach($searchItemArray as $searchItemArrayK => $searchItemArrayV)
                {
                    if(in_array($searchItemArrayV['id'],$orphanedNodeArr))
                   {
                       $searchItemArray[$searchItemArrayK]['is_orphan'] = 1;
                   }
                   else {
                       $searchItemArray[$searchItemArrayK]['is_orphan'] = 0;
                   }
                }
            }

        if(!empty($parsedItemAssociations))
        {
            foreach($parsedItemAssociations as $parsedItemAssociationsK => $parsedItemAssociationsV)
            {
                if(in_array($parsedItemAssociationsV['parent_id'],$itemArr))
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = false;
                }
                else
                {
                    $parsedItemAssociations[$parsedItemAssociationsK]['external_link'] = true;
                }
            }
        }    
        /* End */                              
        if(!empty($searchItemArray))
        {
                $nodes = array_prepend($searchItemArray, $parsedDocument); 
        }
        else
        {
                $nodes = $parsedDocument;  
        }                                                       
        
        $treeData = [
            "nodes" => $nodes,
            "relations" => $parsedItemAssociations,
        ];
        return $treeData;
        
    }
}

