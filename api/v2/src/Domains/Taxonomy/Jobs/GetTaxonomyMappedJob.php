<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\TaxonomyRepositoryInterface;

class GetTaxonomyMappedJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxonomyRepositoryInterface $taxonomyRepo)
    {
        $taxonomyId = $this->input['id'];
        return $taxonomyRepo->getMappedNodes($taxonomyId);
    }
}
