<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\StringHelper;

use App\Domains\Taxonomy\Validators\CreateAndUploadTaxonomyDeltaUpdatesSearchDataValidator as Validator;

class ValidateInputsForTaxonomyDeltaUpdatesSearchDataUploadJob extends Job
{

    use StringHelper;

    private $requestInputToValidate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $requestInputToValidate)
    {
        $this->requestInputToValidate = $requestInputToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->requestInputToValidate);
        return $validation===true ? $validation : $this->validatorMessageParser($validation->messages()->getMessages());
    }
}
