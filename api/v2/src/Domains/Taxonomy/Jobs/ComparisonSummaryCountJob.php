<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use DB;
use App\Services\Api\Traits\ComparisonSummaryHelperTrait;

class ComparisonSummaryCountJob extends Job
{
    use ComparisonSummaryHelperTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $documentDetails;
    private $requestData;

    public function __construct($comparisonId)
    {
        $this->comparisonId = $comparisonId;
        $this->setComparisonVariables();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $comparisonId = $this->comparisonId;
        
        $CFDefinitions= ["CFConcepts","CFSubjects","CFLicenses","CFItemTypes"];
        
        // To get count of document, associations, nodetype
        $summaryCount = DB::table('version_node_type')
                        ->select('type','node_type','node_type_id','node_id','action',DB::raw("sum(`count`) as totalCount"))
                        ->where('comparison_id',$comparisonId)
                        ->whereNotIn('type',$CFDefinitions)
                        ->groupBy('type','node_type_id','node_id','action')
                        ->get()->toArray();
        $summaryCount = json_decode(json_encode($summaryCount),true);

        $action = ['E' => 'edit','N' => 'new','D' => 'delete']; // Map action to full forms
        $nodeChangesGroup = [];
        // To save CFType, NodeType, Action. Eg: CFAssociations - isChildOf - D / CFItems - Course - E
        foreach($summaryCount as $summkey => $summary){            
            $summCfType = $summary['type'];
            $summNodeType = ($summCfType=='CFAssociations') ? $summary['node_type'] : $summary['node_type_id']; //For associations we want to section it as association_type, for other as nodetype
            $mappedAction = $action[$summary['action']];
            if($summCfType=='CFItems'){
                if(!isset($nodeChangesGroup[$summary['node_id']])){
                    $nodeChangesGroup[$summary['node_id']] = $summNodeType;
                }
                else{
                    continue;
                }
            }
            if(!isset($summaryReport[$summCfType][$summNodeType])){
                $summaryReport[$summCfType][$summNodeType] = ['type'=>$summary['node_type'],'new'=>0,'edit'=>0,'delete'=>0];
                if(!in_array($summCfType,['CFAssociations','CFDocument'])){
                    $summaryReport[$summCfType][$summNodeType]['node_type_id'] = $summary['node_type_id'];
                }                
            }
            $totalCount = ($mappedAction == 'edit' && $summCfType!='CFDocument') ? 1 : ((int) $summary['totalCount']);            
            $summaryReport[$summCfType][$summNodeType][$mappedAction] = $summaryReport[$summCfType][$summNodeType][$mappedAction] + $totalCount;
        }

        $finalSummaryReport = [];

        /*********** CFDocument *****************/
        if(isset($summaryReport['CFDocument'])){
            $finalSummaryReport["document"] = array_values($summaryReport['CFDocument'])[0];
            $finalSummaryReport["document"]["displayName"] = "Document";
        }
        /*********** CFDocument *****************/
        /*********** CFAssociations *****************/
        if(isset($summaryReport['CFAssociations'])){
            $finalSummaryReport["associations"]["displayName"] = "Associations";
            $finalSummaryReport["associations"]['items'] = array_values($summaryReport['CFAssociations']);
            $nodetypeId[] = array_keys($summaryReport['CFAssociations']);                    
        }
        /*********** CFAssociations *****************/

        /*********** CFItems *****************/
        if(isset($summaryReport['CFItems'])){
            $finalSummaryReport["nodetype"]["displayName"] = "NodeType";
            $finalSummaryReport["nodetype"]['items'] = array_values($summaryReport['CFItems']);
        }
        /*********** CFItems *****************/

        /*********** CFItemTypes *****************/
        if(isset($summaryReport['CFItemTypes'])){
            $nodetypeId[] = array_keys($summaryReport['CFItemTypes']);
        }
        /*********** CFItemTypes *****************/

        return $finalSummaryReport;
    }

}
