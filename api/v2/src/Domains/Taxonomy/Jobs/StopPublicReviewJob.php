<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class StopPublicReviewJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input , array $userId)
    {
        $this->setRequestData($input);

        $this->setUserRequestData($userId);
    }

    public function setRequestData(array $data) {
        $this->input = $data;
    }

    public function getRequestData(): array {
        return $this->input;
    }

    public function setUserRequestData(array $userId) {
        $this->userData = $userId;
    }

    public function getUserRequestData(): array {
        return $this->userData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo)
    {
        $this->documentRepo = $documentRepo;
        $input=$this->input;
        $userData=$this->userData;
        return $this->documentRepo->stopPublicReview($input,$userData);
    }

}
