<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class ExportTaxonomyJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->setDocumentIdentifier($this->documentIdentifier);

        $this->requestUserDetails = $requestUserDetails;
    }

    //Public Getter and Setter methods
    public function setDocumentIdentifier(string $documentIdentifier){
        $this->documentIdentifier = $documentIdentifier;
    }

    public function getDocumentIdentifier(){
        return $this->documentIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $docRepo, ItemAssociationRepositoryInterface $itemAssociationsRepository)
    {
        $this->docRepo             = $docRepo;
        $this->itemAssociationsRepository = $itemAssociationsRepository;
        $documentIdentifier = $this->documentIdentifier;
        $nodesData = $this->docRepo->exportTaxonomy($documentIdentifier);

        $input              =   $this->requestUserDetails;
        $organizationId     =   $input["organization_id"];
        // To get parent child relation for Taxonomy
        $returnCollection           =   true;
        $fieldsToReturn             =   ["origin_node_id", "destination_node_id","sequence_number"];
        $conditionalKeyValuePair    =   [ 
            "document_id" =>  $documentIdentifier,
            "organization_id" => $organizationId,
            "is_deleted" => 0,
            "association_type" => 1 // 1 for isChildOf relation
        ];

        $itemAssociationsUnderDocument = $this->itemAssociationsRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, $fieldsToReturn, $conditionalKeyValuePair
                                         );
        $parsedItemAssociations = [];
        $itemSequenceList = [];
        foreach($itemAssociationsUnderDocument as $itemAssociation)
        {
            $originNodeId = $itemAssociation->origin_node_id;
            $destinationNodeId = $itemAssociation->destination_node_id;
            $itemSequenceList[$itemAssociation->origin_node_id] = $itemAssociation->sequence_number;
                if(!empty($originNodeId) && !empty($destinationNodeId) && ($originNodeId != $destinationNodeId))
                {

                    $parsedItemAssociations[] = [
                                                    "child_id" => $itemAssociation->origin_node_id,
                                                    "parent_id" => $itemAssociation->destination_node_id,
                                                    "sequence_number" => $itemAssociation->sequence_number,
                                                ];
                }
        }
        foreach($nodesData as $nodesDataK => $nodesDataV)
        {
            $itemK = $nodesDataV['id'];
            $nodesData[$nodesDataK]['sequence_number'] = !empty($itemSequenceList[$itemK]) ? $itemSequenceList[$itemK] : "";
        }
        $taxonomyData = [
            "nodes" => $nodesData,
            "relations" => $parsedItemAssociations,
        ];

        return $taxonomyData;
    }
}
