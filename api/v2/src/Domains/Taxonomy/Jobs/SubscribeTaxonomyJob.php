<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Data\Models\TaxonomyPubStateHistory;

class SubscribeTaxonomyJob extends Job
{
    use UuidHelperTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $documentDetails;
    private $requestData;

    public function __construct($documentDetails, $requestData)
    {
        $this->documentDetails = $documentDetails;
        $this->requestData = $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $requestData = $this->requestData;
        extract($requestData);
        /***** Get source_document_id, document_id, organization_id based on all publisher's document id's passed *****/
        $documentDetails = $this->documentDetails;
        $documentDetails = json_decode(json_encode($documentDetails),TRUE);
        /***** Get source_document_id, document_id, organization_id based on all publisher's document id's passed *****/
        
        $subDocumentId = !empty($subDocumentId) ? array_column($subDocumentId,'sub_document_id','document_id') :[];
        $subscription = [];
        $subscriberDetails = [];
        foreach($documentDetails as $docKey=>$documents){
                $eachDocumentId = $documents['document_id'];
                /***** Prepare array for subscriber detail to be inserted in version_subscriber_detail table *****/
                $publisherOrgId = $documents['organization_id'];                
                // Check if details already exists of a subscriber for publisher's organization
                $subscriberDetailsFetch = DB::table('version_subscriber_detail')->where(
                    ['subscriber_id' => $subscriberUserId,
                    'subscriber_org_id' => $subscriberOrgId,
                    'publisher_org_id' => $publisherOrgId])->first();
                if(isset($subscriberDetailsFetch->subscriber_detail_id)){
                    $subscriberDetailId = $subscriberDetailsFetch->subscriber_detail_id;
                }
                else{
                    // Else create a record subscriber for publisher's organization
                    $subscriberDetailId = $this->createUniversalUniqueIdentifier();
                    $subscriberDetails[]  = [
                        'subscriber_detail_id' => $subscriberDetailId,
                        'subscriber_id' => $subscriberUserId,
                        'subscriber_org_id' => $subscriberOrgId,                            
                        'publisher_org_id' => $publisherOrgId,
                        'is_internal' => $isInternal,
                        'subscriber_end_point' => $subscriberEndPoint
                    ];
                }
                /***** Prepare array for subscriber detail to be inserted in version_subscriber_detail table *****/
                
                /***** Prepare array for subscription to be inserted in version_subscription table *****/
                // Check if subscription exists of a subscriber for publisher's organization with same taxonomy and is active
                $subscriptionFetch = DB::table('version_subscription')->where([
                    'subscriber_detail_id' => $subscriberDetailId,
                    'pub_document_id' => $eachDocumentId,
                    'is_active' => 1,
                    ])->whereNull('deleted_at')->first();
                if(!isset($subscriptionFetch->subscription_id)){                    
                    $subscriptionId = $this->createUniversalUniqueIdentifier();
                    if(empty($version)){ // Saving version for ACMT while subscribing
                        $version = TaxonomyPubStateHistory::where('document_id',$eachDocumentId)->where('organization_id',$publisherOrgId)->where('is_deleted',0)->max('publish_number');
                    }
                    $subscription[] = [
                        'subscription_id' => $subscriptionId,
                        'sub_document_id' => isset($subDocumentId[$eachDocumentId]) ? $subDocumentId[$eachDocumentId] : '',
                        'subscriber_detail_id' => $subscriberDetailId,
                        'pub_source_document_id' => $documents['source_document_id'],
                        'pub_document_id' => $eachDocumentId,
                        'publisher_org_id' => $publisherOrgId,
                        'version' => $version,
                        'subscription_datetime' => date('Y-m-d H:i:s'),
                        'is_active' => 1
                    ];
                    $subscriptionResponse[] = [
                        'subscription_id' => $subscriptionId
                    ];
                }
                /***** Prepare array for subscription to be inserted in version_subscription table *****/
        }
        /***** Bulk insert records for version_subscriber_detail, version_subscription table *****/
        $response = false;
        if(!empty($subscriberDetails)){
            DB::table('version_subscriber_detail')->insert($subscriberDetails);
        }
        if(!empty($subscription)){
            DB::table('version_subscription')->insert($subscription);
            $response = $subscriptionResponse;
        }
        /***** Bulk insert records for version_subscriber_detail, version_subscription table *****/
        return $response;
    }

}
