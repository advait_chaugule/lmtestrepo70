<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use DateTime;
use DateTimeZone;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;
use App\Data\Repositories\Contracts\WorkflowRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class CopyTaxonomyAndRelatedDataJob extends Job
{
    use DateHelpersTrait, UuidHelperTrait;

    private $documentIdentifier;
    private $organizationIdentifier;
    private $userIdentifier;
    private $workflowIdentifier;
    private $inputData;
    private $requestingUserDetail;

    private $taxonomyTitle;

    private $documentRepository;
    private $itemRepository;
    private $metadataRepository;
    private $projectRepository;
    private $workflowRepository;
    private $userRepository;

    private $document;
    private $documentCustomMetadata;
    private $copiedDocument;

    private $item;
    private $copiedItem;

    private $destinationNodeIdsOfCopiedItem;

    private $projectData;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestingUserDetail, array $requestData)
    {
        //Set the private attributes
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setOrganizationIdentifier($requestingUserDetail['organization_id']);
        $this->setUserIdentifier($requestingUserDetail['user_id']);
        $this->setDocumentTitle($requestData['title']);
        $this->inputData    =   $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository, ItemAssociationRepositoryInterface $itemAssociationRepository, MetadataRepositoryInterface $metadataRepository, ProjectRepositoryInterface $projectRepository, NodeTypeRepositoryInterface $nodeTypeRepository, AssetRepositoryInterface $assetRepository, WorkflowRepositoryInterface $workflowRepository, UserRepositoryInterface $userRepository)
    {
        //Set the repo handlers
        $this->documentRepository           =   $documentRepository;
        $this->itemRepository               =   $itemRepository;
        $this->itemAssociationRepository    =   $itemAssociationRepository;
        $this->metadataRepository           =   $metadataRepository;
        $this->projectRepository            =   $projectRepository;
        $this->nodeTypeRepository           =   $nodeTypeRepository;
        $this->assetRepository              =   $assetRepository;
        $this->workflowRepository           =   $workflowRepository;
        $this->userRepository               =   $userRepository;

        $this->fetchAndSetDocumentAndItsRelatedData();
        $this->fetchAndSetItemAndRelatedData();

        if($this->checkIfWorkflowExistsForPR() != false) {
            $this->copyDocumentNode();

            $this->copyItemNode();
            $this->copyItemAssociations();

            $this->copyCustomMetadata();

            $this->copyDocumentAsset(); 
            $this->copyItemAsset();           

            $this->createProjectForPublicReviewTaxonomy();

            $this->setPublicReviewHistory();

            return $this->parseResponse();
        } else {
            return  0;
        }
    }

    /**
     * Public setter and getter methods
     */
    public function setDocumentIdentifier($identifier) {
        $this->documentIdentifier   =   $identifier;
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setUserIdentifier($identifier) {
        $this->userIdentifier   =   $identifier;
    }

    public function getUserIdentifier() {
        return $this->userIdentifier;
    }

    public function setWorkflowIdentifier($identifier) {
        $this->workflowIdentifier   =   $identifier;
    }

    public function getWorkflowIdentifier() {
        return $this->workflowIdentifier;
    }

    public function setDocumentTitle($taxonomyTitle) {
        $this->taxonomyTitle   =   $taxonomyTitle;
    }

    public function getDocumentTitle() {
        return $this->taxonomyTitle;
    }

    public function setDocument($document) {
        $this->document   =   $document;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setItem($item) {
        $this->item   =   $item;
    }

    public function getItem() {
        return $this->item;
    }

    public function setDocumentCustomMetadata($customMetadata) {
        $this->documentCustomMetadata   =   $customMetadata;
    }

    public function getDocumentCustomMetadata() {
        return $this->documentCustomMetadata;
    }

    public function setDocumentAsset($asset) {
        $this->documentAsset   =   $asset;
    }

    public function getDocumentAsset() {
        return $this->documentAsset;
    }

    public function setSavedDocumentObject($document) {
        $this->copiedDocument   =   $document;
    }

    public function getSavedDocumentObject() {
        return $this->copiedDocument;
    }

    public function setSavedItemCollection($items) {
        $this->copiedItems   =   $items;
    }

    public function getSavedItemCollection() {
        return $this->copiedItems;
    }

    public function setProject($projectData) {
        $this->projectData   =   $projectData;
    }

    public function getProject() {
        return $this->projectData;
    }

    /**
     * Fetch the selected document from database
     */
    private function fetchAndSetDocumentAndItsRelatedData() {
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection   =   false;
        $fieldsToReturn     =   ['*'];
        $keyValuePairs      =   ['document_id' => $documentIdentifier, 'organization_id'    => $organizationIdentifier];
        $relations          =   ['customMetadata', 'asset'];


        $documentDetail =   $this->documentRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        $this->setDocument($documentDetail);
        $this->setDocumentCustomMetadata($documentDetail->customMetadata);
        $this->setDocumentAsset($documentDetail->asset);

    }

    /**
     * Fetch the items of selected document
     */
    private function fetchAndSetItemAndRelatedData() {
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['*'];
        $keyValuePairs      =   ['document_id' => $documentIdentifier, 'organization_id'    => $organizationIdentifier, 'is_deleted'    =>  '0'];
        $relations          =   ['itemAssociations', 'customMetadata', 'asset'];

        $itemDetail =   $this->itemRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations );

        $this->setItem($itemDetail);
    }

    public function checkIfWorkflowExistsForPR() {
        $workflowIdentifier     =   '';
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $userIdentifier         =   $this->getUserIdentifier();

        $workflowCode       =   trim(config("selfRegistration")["WORKFLOW"]["DEFAULT_WORKFLOW_PROJECT_PUBLIC_REVIEW"]); 
        $workflowAttributes =   ['organization_id'    =>  $organizationIdentifier, 'workflow_code'    => $workflowCode];

        $workflowDetail   =   $this->workflowRepository->findByAttributes($workflowAttributes)->first();
        
        if(!empty($workflowDetail->workflow_id)) {
            $this->setWorkflowIdentifier($workflowDetail->workflow_id);
            return 1;
        } else {
            return 0;
        }

        
    }

    /**
     * Copy selected document
     */
    private function copyDocumentNode() {
        $currentDateTime            =   $this->createDateTime();
        $documentNodeToBeCopied     =   $this->getDocument();
        $createdDocumentIdentifier  =   $this->createUniversalUniqueIdentifier();
        $organizationName           =   DB::table('organizations')->select('name')->where('organization_id', $this->getOrganizationIdentifier())->first()->name;

        $data = [
            "document_id"           => $createdDocumentIdentifier,
            "title"                 => !empty($this->getDocumentTitle()) ? $this->getDocumentTitle(): $documentNodeToBeCopied->title,
            "official_source_url"   => !empty($documentNodeToBeCopied->official_source_url) ? $documentNodeToBeCopied->official_source_url : "",
            "adoption_status"       => 6,
            "notes"                 => !empty($documentNodeToBeCopied->notes) ? $documentNodeToBeCopied->notes : "",
            "publisher"             =>  !empty($documentNodeToBeCopied->publisher) ? $documentNodeToBeCopied->publisher : "",
            "description"           => !empty($documentNodeToBeCopied->description) ? $documentNodeToBeCopied->description : "",
            "version"               => !empty($documentNodeToBeCopied->description) ? $documentNodeToBeCopied->description : "",
            "status_start_date"     =>  !empty($documentNodeToBeCopied->status_start_date) ? $documentNodeToBeCopied->status_start_date : "",
            "status_end_date"       =>  !empty($documentNodeToBeCopied->status_end_date) ? $documentNodeToBeCopied->status_end_date : "",
            "creator"               =>  !empty($documentNodeToBeCopied->creator) ? $documentNodeToBeCopied->creator : $organizationName,
            "url_name"              =>  !empty($documentNodeToBeCopied->url_name) ? $documentNodeToBeCopied->url_name : "",
            "language_id"           =>  !empty($documentNodeToBeCopied->language_id) ? $documentNodeToBeCopied->language_id : "",
            "license_id"            =>  !empty($documentNodeToBeCopied->license_id) ? $documentNodeToBeCopied->license_id : "",
            "organization_id"       =>  $this->getOrganizationIdentifier(),
            "document_type"         =>  !empty($documentNodeToBeCopied->document_type) ? $documentNodeToBeCopied->document_type : "1",
            "created_at"            =>  $currentDateTime,
            "updated_at"            =>  $currentDateTime,
            "source_document_id"    =>  $documentNodeToBeCopied->document_id,
            "node_type_id"          =>  !empty($documentNodeToBeCopied->node_type_id) ? $documentNodeToBeCopied->node_type_id : "",
            "node_template_id"      =>  !empty($documentNodeToBeCopied->node_template_id) ? $documentNodeToBeCopied->node_template_id : "",
            "updated_by"            =>  $this->getUserIdentifier(),
            "source_type"           =>  !empty($documentNodeToBeCopied->source_type) ? $documentNodeToBeCopied->source_type : "",           //set source_type for Public Review
        ];

        $documentCopied = $this->documentRepository->saveData($data); 
        $this->setSavedDocumentObject($documentCopied);

        $attributesToSaveMapping    =   ['original_document_id' =>  $documentNodeToBeCopied->document_id, 'copied_document_id'  =>  $createdDocumentIdentifier, 'created_by'    =>  $this->getUserIdentifier(), 'timestamp' => $currentDateTime];

        $this->documentRepository->saveDocumentMapping($attributesToSaveMapping);

        $updateDocumentStatusAttribute  = ['adoption_status'    =>  '5'];
        $this->documentRepository->edit($documentNodeToBeCopied->document_id, $updateDocumentStatusAttribute);
    }

    /**
     * Copy Custom metadata of selected document and respective items
     */
    private function copyCustomMetadata() {
        $itemsToBeCopied                        =   $this->getItem();
        $documentCopied                         =   $this->getSavedDocumentObject();
        $documentCustomMetadataToBeCopied       =   $this->getDocumentCustomMetadata();

        if(count($documentCustomMetadataToBeCopied) > 0) {
            foreach($documentCustomMetadataToBeCopied as $customMetadata) {
                $this->documentRepository->addOrUpdateCustomMetadataValue($documentCopied->document_id, $customMetadata->metadata_id, $customMetadata->pivot->metadata_value,$customMetadata->pivot->metadata_value_html,$customMetadata->pivot->is_additional,'insert');
            }
        } 
        
        foreach($itemsToBeCopied as $items) {

            $itemCustomMetadata =   $items->customMetadata;
            if(count($itemCustomMetadata) > 0) {
                foreach($itemCustomMetadata as $customMetadata) {

                    $copiedItemId       =   $this->helperToSearchAndReturnDestinationNodeItem($customMetadata->pivot->item_id);

                    $this->itemRepository->addOrUpdateCustomMetadataValue($copiedItemId, $customMetadata->metadata_id, $customMetadata->pivot->metadata_value,$customMetadata->pivot->metadata_value_html,$customMetadata->pivot->is_additional,'insert');
                }
            }
        }
    }

    /**
     * Copy Assets of selected document
     */
    private function copyDocumentAsset() {
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $documentAsset          =   $this->getDocumentAsset();
        $documentCopied         =   $this->getSavedDocumentObject();

        if(count($documentAsset) > 0) {
            foreach($documentAsset as $asset) {
                $assetDataToSave = [
                    "asset_id"                  => $this->createUniversalUniqueIdentifier(), 
                    "organization_id"           => $organizationIdentifier,
                    "document_id"               => !empty($documentCopied->document_id)? $documentCopied->document_id : $asset->document_id,
                    'asset_name'                => !empty($asset->asset_name)? $asset->asset_name : "", 
                    'asset_target_file_name'    => !empty($asset->asset_target_file_name)? $asset->asset_target_file_name : "", 
                    'asset_content_type'        => !empty($asset->asset_content_type)? $asset->asset_content_type : "", 
                    'asset_size'                => !empty($asset->asset_size)? $asset->asset_size : "",
                    'title'                     => !empty($asset->title)? $asset->title : "",
                    'description'               => !empty($asset->description)? $asset->description : "",
                    'item_linked_id'            => !empty($documentCopied->document_id)? $documentCopied->document_id : $asset->item_linked_id,
                    'asset_linked_type'         => !empty($asset->asset_linked_type) ? $asset->asset_linked_type : '1',
                    'uploaded_at'               => $this->createDateTime()
                ];

                $this->assetRepository->saveData($assetDataToSave);
            }
        }
    }

    /**
     * Copy items of selected document
     */
    public function copyItemNode() {
        $itemIdArray                =   [];
        $arrayForParentIdIndices    =   [];
        $arrayOfCreatedItemId       =   [];

        $itemsToBeCopied        =   $this->getItem();
        $documentCopied         =   $this->getSavedDocumentObject();
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $currentDateTime        =   $this->createDateTime();


        if(count($itemsToBeCopied) > 0) {

            foreach($itemsToBeCopied  as $itemId) {
                $itemIdArray[]  =   $itemId['item_id'];
            }

            foreach($itemsToBeCopied as $newItem) {
                $arrayOfCreatedItemId[]   =   $this->createUniversalUniqueIdentifier();
                $key = array_search ($newItem['parent_id'], $itemIdArray);
                $arrayForParentIdIndices[]  =   $key;
            }

            $keyForItem         =   0;
            foreach($itemsToBeCopied as $items) {
                $itemIdentifier =   $this->createUniversalUniqueIdentifier();
                $itemsToSave[]= [
                    "item_id"               => $itemIdentifier,
                    "parent_id"             => ($arrayForParentIdIndices[$keyForItem] !== false ) ? $arrayOfCreatedItemId[$arrayForParentIdIndices[$keyForItem]] : $documentCopied->document_id,
                    "document_id"           => $documentCopied->document_id,
                    "full_statement"        => !empty($items->full_statement) ? $items->full_statement : "",
                    "alternative_label"     => !empty($items->alternative_label) ? $items->alternative_label : "",
                    "human_coding_scheme"   => !empty($items->human_coding_scheme) ? $items->human_coding_scheme : "",
                    "list_enumeration"      => !empty($items->list_enumeration) ? $items->list_enumeration : "",
                    "abbreviated_statement" => !empty($items->abbreviated_statement) ? $items->abbreviated_statement : "",
                    "notes"                 => !empty($items->notes) ? $items->notes : "",
                    "node_type_id"          => !empty($items->node_type_id) ? $items->node_type_id : "",
                    "concept_id"            => !empty($items->concept_id) ? $items->concept_id : "",
                    "language_id"           => !empty($items->language_id) ? $items->language_id : "",
                    "education_level"       => !empty($items->education_level) ? $items->education_level : "",
                    "license_id"            => !empty($items->license_id) ? $items->license_id : "",
                    "status_start_date"     => !empty($items->status_start_date) ? $items->status_start_date : null,
                    "status_end_date"       => !empty($items->status_end_date) ? $items->status_end_date : null,
                    "created_at"            => $currentDateTime,
                    "updated_at"            => $currentDateTime,
                    "organization_id"       => $organizationIdentifier,
                    "source_item_id"        => $items->item_id,
                    "updated_by"            => $this->getUserIdentifier(),
                ];

                $keyForItem++;
            }

            if(!empty($itemsToSave)) {
                $this->setSavedItemCollection($itemsToSave);
                //$this->itemRepository->saveMultiple($itemsToSave);
                // data saved in chunks of 1000 for items
                $itemsToSave = array_chunk($itemsToSave,1000);
                foreach($itemsToSave as $itemsToSaveK => $itemsToSaveV)
                {
                    DB::table('items')->insert($itemsToSaveV);
                }
            }
        }
    }

    /**
     * Helper to Search Destination node id for association
     */
    private function helperToSearchAndReturnDestinationNodeItem(string $searchIdentifier){
        $documentToCopy     =   $this->getDocument();
        $copiedDocument     =   $this->getSavedDocumentObject();
        $nodeIdToReturn     =   "";
        $nodeIdReturnArray  =   [];
        //$destinationNodeItems = $this->getDestinationNodeOfCopiedItems();
        $attributes     =   ['source_item_id'   => $searchIdentifier, 'is_deleted'  =>  '0' ];
        
        $searchResult   =   $this->itemRepository->findByAttributes($attributes);
        
        $parseSearchResult = (count($searchResult) > 0) ? $searchResult->toArray() : [];

        if(count($parseSearchResult) > 0) {
            foreach($parseSearchResult as $nodesToReturn) {
                if(!empty($nodesToReturn['document_id'])) {
                    if($nodesToReturn['document_id'] != $documentToCopy->document_id) {
                        if($nodesToReturn['document_id']    ==  $copiedDocument->document_id) {
                            $nodeIdToReturn    =   $nodesToReturn['item_id'];
                        }
                    }
                }
            }
        } else {
            $attributes     =   ['item_id'   => $searchIdentifier, 'is_deleted'  =>  '0' ];
        
            $searchResult   =   $this->itemRepository->findByAttributes($attributes);
            
            $parseSearchResult = (count($searchResult) > 0) ? $searchResult->toArray() : [];
            
            if(count($parseSearchResult) > 0) {
                $nodeIdToReturn  =   $parseSearchResult[0]['item_id'];
            } else {
                $nodeIdToReturn =   '';
            }
        }
        

        return $nodeIdToReturn;
    }

    /**
     * Copy Item Asociations of copied Items
     */
    public function copyItemAssociations() {
        $destinationNodeIdToBeSaved =   '';
        $itemsIdArray       =   [];
        $document           =   $this->getDocument(); //old
        $itemsToBeCopied    =   $this->getItem();
        $copiedDocument     =   $this->getSavedDocumentObject(); //new
        $copiedItems        =   $this->getSavedItemCollection();
        $currentDateTime    =   $this->createDateTime();

        //dd($copiedItems);

        foreach($copiedItems as $items) {
            $itemsIdArray[] =   $items['item_id'];
        }

        if(count($itemsToBeCopied) > 0) {
            foreach($itemsToBeCopied as $key => $items){
                $itemAssociations   =   $items->itemAssociations;

                foreach($itemAssociations as $associationToBeCopied) {
                    $itemAssociationIdentifier =   $this->createUniversalUniqueIdentifier();

                    $destinationNodeIdToSearchWith = $associationToBeCopied->destination_node_id;

                    /* echo "SOURCE";
                    echo  $destinationNodeIdToSearchWith;
                    echo "<pre>"; */

                    $destinationNodeId  =   $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);

                    /* echo "COPY";
                    echo $destinationNodeId;
                    echo "<pre>"; */
                    
                    if($destinationNodeId   ==  '' ){
                        $destinationNodeIdToBeSaved =   $copiedDocument->document_id;
                    } else {
                        if(in_array($destinationNodeId, $itemsIdArray)) {
                            $destinationNodeIdToBeSaved =   $destinationNodeId;
                        } else {
                            $destinationNodeIdToBeSaved =   $associationToBeCopied->destination_node_id;
                        }
                    }

                    $caseAssociationsToSave[] = [
                        "item_association_id"           =>  $itemAssociationIdentifier,
                        "association_type"              =>  $associationToBeCopied->association_type,
                        "document_id"                   =>  $copiedDocument->document_id,
                        "association_group_id"          =>  $associationToBeCopied->association_group_id,
                        "origin_node_id"                =>  $copiedItems[$key]['item_id'],
                        "destination_node_id"           =>  $destinationNodeIdToBeSaved,
                        "sequence_number"               =>  $associationToBeCopied->sequence_number,
                        "external_node_title"           =>  $associationToBeCopied->external_node_title,
                        "external_node_url"             =>  $associationToBeCopied->external_node_url,
                        "created_at"                    =>  $currentDateTime,
                        "updated_at"                    =>  $currentDateTime,
                        "source_item_association_id"    =>  $associationToBeCopied->item_association_id,
                        "organization_id"               =>  $this->getOrganizationIdentifier(),
                        "source_document_id"            =>  $copiedDocument->document_id,
                        "source_item_id"                =>  $copiedItems[$key]['item_id'],
                        "target_document_id"            =>  $destinationNodeIdToBeSaved, // target_document_id and target_item_id will be same when target_item_id is document other wise it will be reassigned later in the function
                        "target_item_id"                =>  $destinationNodeIdToBeSaved,
                    ];
                    $target_item_id_arr[] = $destinationNodeIdToBeSaved;
                }
            }

            # reassigning target_document_id incase when target_item_id is not document_id
            if(count($target_item_id_arr)) {
                $items = DB::table('items')
                            ->select('document_id','item_id')
                            ->whereIn('item_id', $target_item_id_arr)
                            ->get();
                foreach($items as $item) {
                    foreach($caseAssociationsToSave as $key => $row) {
                        if($row['target_item_id'] == $item->item_id) {
                            $caseAssociationsToSave[$key]['target_document_id'] = $item->document_id;
                        }
                    }
                }
            }

            if(!empty($caseAssociationsToSave)){
                //$this->itemAssociationRepository->saveMultiple($caseAssociationsToSave);
                // data is been saved in chunks of 1000 for item associations
                $caseAssociationsToSave = array_chunk($caseAssociationsToSave,1000);
                foreach($caseAssociationsToSave as $caseAssociationsToSaveK => $caseAssociationsToSaveV)
                {
                    DB::table('item_associations')->insert($caseAssociationsToSaveV);
                }
            }
        }
    }

    /**
     * Copy assets of the items copied
     */
    public function copyItemAsset() {

        $itemsIdArray           =   [];
        $assetDataToSave        =   [];
        $documentCopied         =   $this->getSavedDocumentObject();
        $itemsToBeCopied        =   $this->getItem();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        foreach($itemsToBeCopied as $items) {
            $itemAsset      =   $items->asset;

            if(count($itemAsset) > 0) {
                foreach($itemAsset as $assetToBeCopied) {

                    $itemLinkedId   =   $this->helperToSearchAndReturnDestinationNodeItem($assetToBeCopied->item_linked_id);

                    $assetDataToSave = [
                        "asset_id"                  => $this->createUniversalUniqueIdentifier(), 
                        "organization_id"           => $organizationIdentifier,
                        "document_id"               => !empty($documentCopied->document_id)? $documentCopied->document_id : $assetToBeCopied->document_id,
                        'asset_name'                => !empty($assetToBeCopied->asset_name)? $assetToBeCopied->asset_name : "", 
                        'asset_target_file_name'    => !empty($assetToBeCopied->asset_target_file_name)? $assetToBeCopied->asset_target_file_name : "", 
                        'asset_content_type'        => !empty($assetToBeCopied->asset_content_type)? $assetToBeCopied->asset_content_type : "", 
                        'asset_size'                => !empty($assetToBeCopied->asset_size)? $assetToBeCopied->asset_size : "",
                        'title'                     => !empty($assetToBeCopied->title)? $assetToBeCopied->title : "",
                        'description'               => !empty($assetToBeCopied->description)? $assetToBeCopied->description : "",
                        'item_linked_id'            => $itemLinkedId,
                        'asset_linked_type'         => !empty($assetToBeCopied->asset_linked_type) ? $assetToBeCopied->asset_linked_type : '',
                        'uploaded_at'               => $this->createDateTime()
                    ];

                    $savedAsset = $this->assetRepository->saveData($assetDataToSave);
                }
            }
        }

        /* //dd($assetDataToSave);
        if(!$assetDataToSave) {
            $this->assetRepository->saveMultiple($assetDataToSave);
        } */
    }

    private function createProjectForPublicReviewTaxonomy() {
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        $userIdentifier         =   $this->getUserIdentifier();
        $workflowIdentifier     =   $this->getWorkflowIdentifier();
        $documentNodeToBeCopied =   $this->getDocument();
        

        $projectData        =   [
            'project_id'        => $this->createUniversalUniqueIdentifier(), 
            'organization_id'   => $organizationIdentifier,
            'project_name'      => !empty($this->getDocumentTitle()) ? $this->getDocumentTitle(): $documentNodeToBeCopied->title, 
            'description'       => 'This project is created for Documents to be set to public review', 
            'workflow_id'       => $workflowIdentifier, 
            'project_type'      => 3, 
            'is_deleted'        => 0, 
            'updated_by'        => $userIdentifier,
        ];

        $workflowStage = $this->projectRepository->findWokflowStageID($workflowIdentifier)->first();
        $projectData['current_workflow_stage_id'] = !empty($workflowStage->workflow_stage_id) ? $workflowStage->workflow_stage_id : "" ;
       
        $projectCreated =   $this->projectRepository->fillAndSave($projectData);  
        
        $this->setProject($projectCreated);        
    }

    private function setPublicReviewHistory() {
        $status                     =   2;
        $latestReviewNumber         =   1;
        $currentDateTime            =   $this->createDateTime();
        $getProjectCreated          =   $this->getProject();
        $originalDocumentIdentifier =   $this->getDocumentIdentifier();
        $organizationIdentifier     =   $this->getOrganizationIdentifier();

        $getPublicReviewPreviousHistory    =   $this->documentRepository->fetchPublicReviewHistory(
            $originalDocumentIdentifier, $organizationIdentifier)->toArray(); 
            
        if(count($getPublicReviewPreviousHistory) > 0) {
            $latestReviewNumber =   $getPublicReviewPreviousHistory[count($getPublicReviewPreviousHistory)-1]->review_number + 1;
        }

        $timezone   =   $this->inputData['timezone'];
        $startDate  =   $this->inputData['formatted_start_date'];
        $endDate    =   $this->inputData['formatted_end_date'];

        $createdBy  =   $this->getUserIdentifier();
        $createdAt  =   $currentDateTime;
        /** Get Current Date Time Of South Carolina Start */
        $date = new DateTime($createdAt);
        $date->setTimezone(new DateTimeZone('America/New_York'));
        $currentDateTime=$date->format('Y-m-d H:i:s');
        /** Get Current Date Time Of South Carolina Start */

        if ($startDate > $currentDateTime) {
            $status =   1;
        }

        $dataToInsert   =   [
            'project_id'        =>  $getProjectCreated->project_id,
            'document_id'       =>  $originalDocumentIdentifier,
            'organization_id'   =>  $organizationIdentifier,
            'review_number'     =>  $latestReviewNumber,
            'timezone'          =>  $timezone,
            'start_date'        =>  $startDate,
            'end_date'          =>  $endDate,
            'status'            =>  $status,
            'created_by'        =>  $createdBy,
            'updated_by'        =>  $createdBy,
            'created_at'        =>  $createdAt,
            'updated_at'        =>  $createdAt,
        ];

        $saveHistory    =   $this->documentRepository->savePublicReviewHistory($dataToInsert);
    }

    /**
     * Fetch the self registered users
     */
    private function getSelfRegisteredUserList() {
        $selfRegisteredUsers    =   [];
        $organizationIdentifier =   $this->getOrganizationIdentifier();
        
        $attributes =   ['organization_id'  =>  $organizationIdentifier, 'self_reg_status'   =>  '1'];
        
        $userList   =   $this->userRepository->findByAttributes($attributes);

        foreach($userList as $user) {
            $selfRegisteredUsers[]  =   $user->user_id;
        }

        return $selfRegisteredUsers;

    }


    private function parseResponse() {
        $copiedItemsId          =   [];
        $copiedParentItemsId    =   [];
        $projectCreated         =   $this->getProject();
        $documentCopied         =   $this->getSavedDocumentObject();
        $itemsCopied            =   $this->getSavedItemCollection();

        foreach($itemsCopied as $items) {
            $copiedItemsId[]    =   $items['item_id'];
        }
        
        $response   =   [
            'project_id'    =>  $projectCreated->project_id,
            'document_id'   =>  $documentCopied->document_id,
            'item_id'       =>  implode(',', $copiedItemsId),
            'user_id'       =>  $this->getSelfRegisteredUserList(),
        ];

        return $response;
    }

    

    
}
