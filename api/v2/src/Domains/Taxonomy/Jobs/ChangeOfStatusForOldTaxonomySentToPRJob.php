<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ChangeOfStatusForOldTaxonomySentToPRJob extends Job
{
    private $documentIdentifier;
    private $documentRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {   
        $this->setDocumentIdentifier($documentIdentifier);   
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        //Set the repository handler
        $this->documentRepository   =   $documentRepository;

        $changeDocumentStatus   =   $this->changeDocumentStatus();

        return $changeDocumentStatus;
    }

    public function setDocumentIdentifier($documentIdentifier) {
        $this->documentIdentifier   =   $documentIdentifier;
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    private function changeDocumentStatus() {
        $documentIdentifier =   $this->getDocumentIdentifier();

        $documentDetail =   $this->documentRepository->find($documentIdentifier);

        if($documentDetail->adoption_status != 5) {
            $attributes =   ['adoption_status'  =>  '5'];
            return $this->documentRepository->edit($documentIdentifier, $attributes);
        } else {
            return false;
        }
    }
}
