<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

class GetTaxonomyTreeStructureJob extends Job
{


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($taxonomyHierarchy, $orphan)
    {
        $this->taxonomyHierarchy = $taxonomyHierarchy;
        $this->nodes = $taxonomyHierarchy['nodes'];
        $this->relations = $taxonomyHierarchy['relations'];
        $this->import_type = $taxonomyHierarchy['import_type'];
        $this->is_deleted = $taxonomyHierarchy['is_deleted'];
        $this->addWrapperNode = true;
        $this->expandLevel = 1;
        $this->isPacingGuide = false;
        $this->dummy_id_for_orphan_label = 'orphan_label_node_id';
        $this->want_orphan = $orphan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->want_orphan) {
            $this->findTopmostOrphan();
        }

        return $this->parseTreeData();
    }

    public function findTopmostOrphan() {
        $nodes = $this->nodes;
        $this->topMostOrphanNodes = [];
        foreach ($nodes as $node) {
            if ($node['is_orphan'] === 1) { // top most orphan node
                $this->topMostOrphanNodes[] = $node;
            }
        }
    }

    public function parseTreeData()
    {
        $treeNodes = $this->nodes;
        $documentNode = $treeNodes[0] ? $treeNodes[0] : $treeNodes;
        $this->rootNodeId = $documentNode['id'];
        $parsedTree = $documentNode;
        if ($parsedTree && $documentNode && $this->addWrapperNode) {
            $parsedTree['full_statement'] = $documentNode['title'];
            $parsedTree['full_statement_html'] = $documentNode['title_html'] ? $documentNode['title_html'] : $documentNode['title'];
            $parsedTree['is_document'] = 1;
            $parsedTree['title'] = $documentNode['title'];
            $parsedTree['expand'] = true;
            $parsedTree['cut'] = 0;
            $parsedTree['paste'] = 0;
            $parsedTree['reorder'] = 0;
            $parsedTree['level'] = 1;
            $parsedTree['isFirst'] = true;
            $parsedTree['list_enumeration'] = $parsedTree['sequence_number'] ? '' . $parsedTree['sequence_number'] : $parsedTree['list_enumeration'];
            $this->isPacingGuide = (isset($documentNode['document_type']) && $documentNode['document_type'] == 2) ? true : false;
        }

        $parsedTree['children'] = $this->createTreeStructure($documentNode['id'], 1);

        $parsedTree['children'] = $this->sortData($parsedTree['children']);

        if($this->want_orphan) {
            $parsedTree = $this->buildOrphanLableNode($parsedTree);
        }

        $wrapperNode = [
            'children' => [
                $parsedTree
            ]
        ];

        if ($this->addWrapperNode) {
            return $wrapperNode;
        } else {
            return $parsedTree;
        }
    }

    public function createTreeStructure($nodeId, $level)
    {
        $data1 = [];
        $childrenNodes = $this->extractChildren($nodeId);
        if (count($childrenNodes) > 0) {
            $level = $level + 1;
            foreach($childrenNodes as $childNode) {
                if (!empty($childNode)) {
                    $nodeId = $childNode['id'];
                    $childNode['children'] = [];
                    $childNode['expand'] = $level <= $this->expandLevel ? true : false;
                    $childNode['cut'] = 0;
                    $childNode['paste'] = 0;
                    $childNode['reorder'] = 0;
                    $childNode['level'] = $level;
                    $childNode['list_enumeration'] = $childNode['sequence_number'] ? '' . $childNode['sequence_number'] : $childNode['list_enumeration'];
                    $nodeChildren = $this->createTreeStructure($nodeId, $level);
                    if (count($nodeChildren)) {
                        $childNode['children'] = $nodeChildren;
                    }
                    $data1[] = $childNode;
                }
            }
        }
        return $data1;
    }

    public function extractChildren($nodeId)
    {
        $treeNodes = $this->nodes;
        $nodeRelations = $this->relations;
        $data2 = [];
        foreach($nodeRelations as $relation) {
            if (!empty($relation) && $relation['child_id'] !== $relation['parent_id']) {
                $parentId = $relation['parent_id'];
                if ($nodeId === $parentId) {
                    $childId = $relation['child_id'];
                    $childNode = $this->extractNode($childId, $parentId);
                    if($childNode) {
                        $childNode['parent_id'] = $parentId;
                        $data2[] = $childNode;
                    }
                }
            }
        }
        return $data2;
    }

    public function extractNode($nodeIdToSearchWith, $parentId)
    {
        $treeNodes = $this->nodes;
        $nodeFound = '';
        foreach ($treeNodes as $node) {
            if (!empty($node)) {
                if ($this->isPacingGuide) {
                    // This to handle if hierarchy_id is null
                    if (!isset($node['hierarchy_id'])) {
                        $node['hierarchy_id'] = $treeNodes[0]['id'];
                    }
                    if ($nodeIdToSearchWith === $node['id'] && $parentId === $node['hierarchy_id']) {
                        $nodeFound = $node;
                        break;
                    }
                } else {
                    if ($nodeIdToSearchWith === $node['id']) {
                        $nodeFound = $node;
                        break;
                    }
                }
            }
        }
        return $nodeFound;
    }

    public function buildOrphanLableNode($parentNode)
    {
        $orphanFound = false;
        $nodes = $this->nodes;
        $orphanLableNode = []; // 'orphanLableNode' is one dummy node
        $title = 'Nodes with no linked parent';
        $orphanLableNode['full_statement'] = $title;
        $orphanLableNode['full_statement_html'] = $title;
        $orphanLableNode['human_coding_scheme'] = '';
        $orphanLableNode['is_document'] = 0;
        $orphanLableNode['cut'] = 0;
        $orphanLableNode['paste'] = 0;
        $orphanLableNode['reorder'] = 0;
        $orphanLableNode['children'] = [];
        $orphanLableNode['sequence_number'] = '';
        $orphanLableNode['isOrphan'] = true;
        $orphanLableNode['isOrphanLabel'] = true;
        $orphanLableNode['parent_id'] = $parentNode['id'];
        $orphanLableNode['id'] = $this->dummy_id_for_orphan_label; // assigning dummy id
        $orphanLableNode['list_enumeration'] = '' . ($this->getMaxLE($parentNode['children']));
        $orphanLableNode['level'] = $parentNode['level'] + 1;
        $orphanLableNode['expand'] = true;

        foreach($this->topMostOrphanNodes as $node) {
            $orphanFound = true;
            $node['cut'] = 0;
            $node['paste'] = 0;
            $node['reorder'] = 0;
            $node['parent_id'] = $this->dummy_id_for_orphan_label;
            $node['level'] = $orphanLableNode['level'] + 1;
            $node['expand'] = $node['level'] <= $this->expandLevel ? true : false;
            $node['children'] = $this->createTreeStructure($node['id'], $node['level']);
            $orphanLableNode['children'][] = $node;
        }
        if ($orphanFound) {
            $parentNode['children'][] = $orphanLableNode;
        }

        return $parentNode;
    }

    public function getMaxLE($nodes)
    {
        $listEnumerations = [];
        foreach($nodes as $node) {
            if(trim($node['list_enumeration']) != '' && is_numeric($node['list_enumeration'])) {
                $listEnumerations[] = $node['list_enumeration'];
            }
        }
        if(count($listEnumerations)) {
            return (max($listEnumerations) + 1);
        } else {
            return 1;
        }
    }

    public function sortData($data, $sortBy = 'human_coding_scheme')
    {
        $data = array_values($this->arraySort($data, $sortBy));
        foreach ($data as $key => $value) {
            if (isset($value['children']) && count($value['children'])) {
                $sortBy = $this->checkSortType($value['children']);
                $data[$key]['children'] = array_values($this->sortData($value['children'], $sortBy));
            }
        }
        return $data;
    }

    public function checkSortType($children)
    {
        if (isset($children[0]['list_enumeration']) && trim($children[0]['list_enumeration']) !== '') {
            return 'list_enumeration';
        } else {
            if (isset($children[0]['human_coding_scheme']) && trim($children[0]['human_coding_scheme']) !== '') {
                return 'human_coding_scheme';
            } else {
                return 'full_statement';
            }
        }
    }

    public function arraySort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }
        return $new_array;
    }
}
