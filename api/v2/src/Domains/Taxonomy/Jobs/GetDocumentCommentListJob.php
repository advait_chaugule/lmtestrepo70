<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ThreadCommentRepositoryInterface;

class GetDocumentCommentListJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemsDetails,array $itemsCommentMadeFor)
    {
        $this->setitemsDetailsData($itemsDetails);

        $this->setRequestData($itemsCommentMadeFor);
    }

    public function setitemsDetailsData(array $info) {
        $this->requestInfo = $info;
    }

    public function getitemsDetailsData(): array {
        return $this->requestInfo;
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThreadCommentRepositoryInterface $threadComment)
    {
        $this->threadComment = $threadComment;
        $itemsCommentMadeFor=$this->requestData;
        $itemsDetails=$this->requestInfo;
        return $this->threadComment->getDocumentCommentList($itemsDetails,$itemsCommentMadeFor);
    }
}
