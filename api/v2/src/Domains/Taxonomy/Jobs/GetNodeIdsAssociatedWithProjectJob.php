<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class GetNodeIdsAssociatedWithProjectJob extends Job
{

    private $documentRepository;
    private $itemRepository;
    private $projectRepository;

    private $projectIdentifier;
    private $documentIdAssignedToProject;
    private $itemIdsAssignedToProject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $projectIdentifier)
    {
        $this->setProjectIdentifier($projectIdentifier);
    }

    public function setProjectIdentifier(string $identifier) {
        $this->projectIdentifier = $identifier;
    }

    public function getProjectIdentifier(): string {
        return $this->projectIdentifier;
    }

    public function setDocumentIdAssignedToProject(array $data) {
        $this->documentIdAssignedToProject = $data;
    }

    public function getDocumentIdAssignedToProject(): array {
        return $this->documentIdAssignedToProject;
    }

    public function setItemIdsAssignedToProject(array $data) {
        $this->itemIdsAssignedToProject = $data;
    }

    public function getItemIdsAssignedToProject(): array {
        return $this->itemIdsAssignedToProject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        ProjectRepositoryInterface $projectRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->projectRepository = $projectRepository;

        $this->fetchAndSetDocumentIdAssignedToProject();
        $this->fetchAndSetItemIdsAssignedToProject();

        return $this->parseAndReturnJobResponse();
    }

    private function fetchAndSetDocumentIdAssignedToProject() {
        $projectId = $this->getProjectIdentifier();
        $returnCollection = false;
        $fieldsToReturn = [ "document_id" ];
        $conditionalKeyValuePair = [ "project_id" => $projectId ];
        $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
        $documentIdAssignedToProject = !empty($document->document_id) ? [ $document->document_id ] : [];
        $this->setDocumentIdAssignedToProject($documentIdAssignedToProject);
    }

    private function fetchAndSetItemIdsAssignedToProject() {
        $projectId = $this->getProjectIdentifier();
        $itemIdsAssignedToProject = $this->projectRepository->getItemIdsAssignedToProject($projectId);
        $this->setItemIdsAssignedToProject($itemIdsAssignedToProject);
    }

    private function parseAndReturnJobResponse(): array {
        $documentIdAssignedToProject = $this->getDocumentIdAssignedToProject();
        $itemIdsAssignedToProject = $this->getItemIdsAssignedToProject();
        $parsedData = [ 'document_id' => $documentIdAssignedToProject, 'item_ids' => $itemIdsAssignedToProject ];
        return $parsedData;
    }
}
