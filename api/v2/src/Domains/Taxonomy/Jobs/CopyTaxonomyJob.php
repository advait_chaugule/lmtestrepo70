<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class CopyTaxonomyJob extends Job
{
    use DateHelpersTrait, UuidHelperTrait, CaseFrameworkTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($document_id, $taxonomy_name, $is_copy, $is_copy_asso, $user, $requestUrl='')
    {
        $this->document_id = $document_id;
        $this->taxonomy_name = $taxonomy_name;
        $this->source_taxonomy_name = '';
        $this->iscopy = $is_copy;
        $this->isCopyAsso = $is_copy_asso;
        $this->user = $user;
        $this->new_document_id = '';
        $this->all_child_nodes = [];
        $this->all_associations = [];
        $this->all_child_associations = [];
        $this->all_other_associations = []; # all other associations than child associations
        $this->all_exact_match_associations = [];
        $organization = DB::table('organizations')->select('org_code','name')->where('organization_id',  $this->user['organization_id'])->first();
        $this->organization_code = $organization->org_code;
        $this->organization_name = $organization->name;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository)
    {
        ini_set('memory_limit','8098M');
        ini_set('max_execution_time', 0);

        $this->itemRepository = $itemRepository;
        $this->copyDocumentNode();

        $this->all_child_nodes = DB::table('items')->where('document_id', $this->document_id)->where('is_deleted', 0)->get()->toArray();
        $this->all_child_nodes_item_id = array_column($this->all_child_nodes, 'item_id');

        $this->copyItemNode();
        $this->getAllItemCustomData();
        $this->getAllItemAssetData();
        $this->getAllItemExemplarData();
        $this->getAllItemChildAssociation();
        $this->getAllRevereseAssociation();
        if($this->iscopy) {
            $this->getExactMatchAssociation();
        }
        $this->InsertItemAndRelatedData();
        if(!empty($this->new_document_id)) {
            # make taxonomy visible in taxnonomy listing page
            DB::table('documents')->where('document_id', $this->new_document_id)->update(['import_status' => 0]);
        }
        return [
            'new_document_id' => $this->new_document_id,
            'organization_name' => $this->organization_name,
            'organization_code' => $this->organization_code,
            'source_taxonomy_name' => $this->source_taxonomy_name
        ];
    }

    public function copyItemNode()
    {
        $conceptArr = [];
        $licenseArr = [];
        $nodTypeArr = [];
        foreach ($this->all_child_nodes as $child_node) {
            $itemIdentifier =   $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFItems", $itemIdentifier, true, $this->organization_code,$this->domainName);
            $current_date_time = $this->createDateTime();
            $create_item_arr[$child_node->item_id] = [
                "item_id" => $itemIdentifier,
                "parent_id" => $child_node->parent_id,
                "document_id" => $this->new_document_id,
                "full_statement" => $child_node->full_statement,
                "alternative_label" => $child_node->alternative_label,
                "item_type_id" => $child_node->item_type_id,
                "human_coding_scheme" => $child_node->human_coding_scheme,
                "list_enumeration" => $child_node->list_enumeration,
                "abbreviated_statement" => $child_node->abbreviated_statement,
                "concept_id" => $child_node->concept_id,
                "notes" => $child_node->notes,
                "language_id" => $child_node->language_id,
                "education_level" => $child_node->education_level,
                "license_id" => $child_node->license_id,
                "source_license_uri_object" => '',
                "status_start_date" => $child_node->status_start_date,
                "status_end_date" => $child_node->status_end_date,
                "is_deleted" => 0,
                "updated_by" => '',
                "organization_id" => $this->user['organization_id'],
                "source_item_id" => $itemIdentifier,
                "node_type_id" =>  $child_node->node_type_id,
                "created_at" =>  $current_date_time,
                "updated_at" =>  $current_date_time,
                "uri" => $uri,
                "source_node_type_uri_object" => '',
                "source_concept_uri_object" => '',
                "full_statement_html" => $child_node->full_statement_html,
                "alternative_label_html" => $child_node->alternative_label_html,
                "human_coding_scheme_html" =>  $child_node->human_coding_scheme_html,
                "abbreviated_statement_html" =>  $child_node->abbreviated_statement_html,
                "notes_html" =>  $child_node->notes_html,
                "custom_view_visibility" => $child_node->custom_view_visibility,
                "custom_view_data" =>  $child_node->custom_view_data,
                "import_type" => 2
            ];

            if($child_node->concept_id != "")
            {
                array_push($conceptArr,$child_node->concept_id);
            }
            if($child_node->license_id != "")
            {
                array_push($licenseArr,$child_node->license_id);
            }
            if($child_node->node_type_id != "")
            {
                array_push($nodTypeArr,$child_node->node_type_id);
            }
        }

        foreach ($create_item_arr as $item_k => $item_v) {
            if($item_v['parent_id'] != '' && !empty($create_item_arr[$item_v['parent_id']])) {
                $create_item_arr[$item_k]['parent_id'] = $create_item_arr[$item_v['parent_id']]['item_id'];
            }
            else if($item_v['parent_id'] != '' && empty($create_item_arr[$item_v['parent_id']])) {
                $create_item_arr[$item_k]['parent_id'] = $this->new_document_id;
            }
        }

        #For concepts
        $getItemConcepts = DB::table('concepts')
                                ->select('concepts.*')
                                ->whereIn('concept_id',$conceptArr)
                                ->get()->toArray();
        $createConceptArr = [];
        $createLincenseArr = [];
        foreach($getItemConcepts as  $getItemConceptsK=> $getItemConceptsV)
        {
            $newConceptId = $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFConcepts", $newConceptId, true, $this->organization_code,$this->domainName);
            $createConceptArr[$getItemConceptsV->concept_id] = [
                                                                "concept_id" => $newConceptId,
                                                                "title" => $getItemConceptsV->title,
                                                                "keywords" => $getItemConceptsV->keywords,
                                                                "hierarchy_code" => $getItemConceptsV->hierarchy_code,
                                                                "description" => $getItemConceptsV->description,
                                                                "is_deleted" => $getItemConceptsV->is_deleted,
                                                                "source_concept_id" => $getItemConceptsV->concept_id,
                                                                "organization_id" => $this->user['organization_id'],
                                                                "created_at" => now()->toDateTimeString(),
                                                                "updated_at" => now()->toDateTimeString(),
                                                                "uri" => $uri,
                                                                "title_html" => $getItemConceptsV->title_html,
                                                                "description_html" => $getItemConceptsV->description_html,
                                                               ];
            $createConceptURI[$getItemConceptsV->concept_id] = [
                                                                    "identifier" => $newConceptId,
                                                                    "uri" => $uri,
                                                                    "title" => $getItemConceptsV->keywords
                                                                ];
        }

        $getItemLicenses = DB::table('licenses')
                                ->select('licenses.*')
                                ->whereIn('license_id',$licenseArr)
                                ->get()->toArray();

        foreach($getItemLicenses as  $getItemLicensesK=> $getItemLicensesV)
        {
            $newLicenseId = $this->createUniversalUniqueIdentifier();
            $uri = $this->getCaseApiUri("CFLicenses", $newLicenseId, true, $this->organization_code,$this->domainName);
            $createLincenseArr[$getItemLicensesV->license_id] = [
                                                                    "license_id" => $newLicenseId,
                                                                    "title" => $getItemLicensesV->title,
                                                                    "description" => $getItemLicensesV->description,
                                                                    "license_text" => $getItemLicensesV->license_text,
                                                                    "is_deleted" => $getItemLicensesV->is_deleted,
                                                                    "source_license_id" => $getItemLicensesV->license_id,
                                                                    "organization_id" => $this->user['organization_id'],
                                                                    "created_at" => now()->toDateTimeString(),
                                                                    "updated_at" => now()->toDateTimeString(),
                                                                    "uri" => $uri,
                                                                    "title_html" => $getItemLicensesV->title_html,
                                                                    "description_html" => $getItemLicensesV->description_html,
                                                                    "license_text_html" => $getItemLicensesV->license_text_html,
                                                                ];
            $createLicenseURI[$getItemLicensesV->license_id] = [
                                                                    "identifier" => $newLicenseId,
                                                                    "uri" => $uri,
                                                                    "title" => $getItemLicensesV->title
                                                                ];

        }

        $getItemNodeType = DB::table('node_types')
                                ->select('node_types.*')
                                ->whereIn('node_type_id',$nodTypeArr)
                                ->where('used_for',0)
                                ->get()->toArray();
        
        foreach($getItemNodeType as  $getItemNodeTypeK=> $getItemNodeTypeV)
        {
            $uri = $this->getCaseApiUri("CFItemTypes", $getItemNodeTypeV->node_type_id, true, $this->organization_code,$this->domainName);
            $createNodeTypeArr[$getItemNodeTypeV->node_type_id] = [
                                                                "node_type_id" => $getItemNodeTypeV->node_type_id,
                                                                "organization_id" => $getItemNodeTypeV->organization_id,
                                                                "title" => $getItemNodeTypeV->title,
                                                                "type_code" => $getItemNodeTypeV->type_code,
                                                                "hierarchy_code" => $getItemNodeTypeV->hierarchy_code,
                                                                "description" => $getItemNodeTypeV->description,
                                                                "source_node_type_id" => $getItemNodeTypeV->source_node_type_id,
                                                                "is_document" => $getItemNodeTypeV->is_document,
                                                                "is_custom" => $getItemNodeTypeV->is_custom,
                                                                "is_deleted" => $getItemNodeTypeV->is_deleted,
                                                                "is_default" => $getItemNodeTypeV->is_default,
                                                                "created_by" => $getItemNodeTypeV->created_by,
                                                                "updated_by" => $getItemNodeTypeV->updated_by,
                                                                "created_at" => $getItemNodeTypeV->created_at,
                                                                "updated_at" => $getItemNodeTypeV->updated_at,
                                                                "uri" => $getItemNodeTypeV->uri,
                                                               ];
            $createNodeTypeURI[$getItemNodeTypeV->node_type_id] = [
                                                                "identifier" => $getItemNodeTypeV->node_type_id,
                                                                "uri" => $uri,
                                                                "title" => $getItemNodeTypeV->title
                                                            ];
        }

        foreach($create_item_arr as $createItemArrK => $createItemArrV)
        {
            if($createItemArrV['concept_id'] != "")
            {
                $searchConceptID = $createItemArrV['concept_id'];
                $create_item_arr[$createItemArrK]['concept_id'] = $createConceptArr[$searchConceptID]['concept_id'];
                $create_item_arr[$createItemArrK]['source_concept_uri_object'] = json_encode($createConceptURI[$searchConceptID]);
            }
            if($createItemArrV['license_id'] != "")
            {
                $searchLicenseID = $createItemArrV['license_id'];
                $create_item_arr[$createItemArrK]['license_id'] = $createLincenseArr[$searchLicenseID]['license_id'];
                $create_item_arr[$createItemArrK]['source_license_uri_object'] = json_encode($createLicenseURI[$searchLicenseID]);
            }
            if($createItemArrV['node_type_id'] != "")
            {
                $searchNodeTypeID = $createItemArrV['node_type_id'];
                $create_item_arr[$createItemArrK]['source_node_type_uri_object'] = json_encode($createNodeTypeURI[$searchNodeTypeID]);
            }
        }
        $this->createItemArr = $create_item_arr;
        $this->createConceptArr = $createConceptArr;
        $this->createLicenseArr = $createLincenseArr;
    }

    /**
     * Get all custom data from item id
     */
    public function getAllItemCustomData()
    {
        $getItemCustom = DB::table('item_metadata')
                            ->select('item_metadata.*')
                            ->where('is_deleted',0)
                            ->whereIn('item_id', $this->all_child_nodes_item_id)
                            ->get()->toArray();

        $createCustomItemArr = [];
        $AllItemArr = $this->createItemArr;
        foreach($getItemCustom as $getItemCustomK=>$getItemCustomV)
        {

            $createCustomItemArr[$getItemCustomV->item_id.'||'.$getItemCustomV->metadata_id] = [
                "item_id" => $AllItemArr[$getItemCustomV->item_id]['item_id'],
                "metadata_id" => $getItemCustomV->metadata_id,
                "metadata_value" => $getItemCustomV->metadata_value,
                "is_deleted" => 0,
                "is_additional" =>$getItemCustomV->is_additional,
                "metadata_value_html" => $getItemCustomV->metadata_value_html,
            ];
        }
        $this->createCustomArr = $createCustomItemArr;
    }

    /**
     * Get all asset data with the item_linked_id id
     */
    public function getAllItemAssetData()
    {
        $getItemAsset = DB::table('assets')
                            ->select('assets.*')
                            ->where('asset_linked_type','=',2)
                            ->whereIn('item_linked_id', $this->all_child_nodes_item_id)
                            ->get()->toArray();
        $createItemAsset = [];
        $AllItemArr = $this->createItemArr;
        foreach($getItemAsset as $getItemAssetK => $getItemAssetV)
        {
            $newAssetId = $this->createUniversalUniqueIdentifier();
            $name = $getItemAssetV->asset_target_file_name;
            $ext=$this->get_file_extension($name);
            $targetFile = $newAssetId.'.'.$ext;
            $bucketName = config("filesystems")["disks"]["s3"]["bucket"];
            $path = "ASSET/ITEM/";
            $exists = Storage::disk('s3')->exists($path.$name);
            if($exists)
            {
                if($name!=="")
                {
                    Storage::disk('s3')->copy($path.$name,$path.$targetFile);
                }
                $itemLinkedId = $getItemAssetV->item_linked_id;
                $createItemAsset[$newAssetId] = [
                    "asset_id" =>  $newAssetId,
                    "organization_id" =>  $this->user['organization_id'],
                    "document_id" =>  $this->new_document_id,
                    "asset_name" =>  $getItemAssetV->asset_name,
                    "asset_target_file_name" =>  $targetFile,
                    "asset_content_type" =>  $getItemAssetV->asset_content_type,
                    "asset_size" =>  $getItemAssetV->asset_size,
                    "title" =>  $getItemAssetV->title,
                    "description" =>  $getItemAssetV->description,
                    "asset_linked_type" =>  $getItemAssetV->asset_linked_type,
                    "item_linked_id" =>  $AllItemArr[$itemLinkedId]['item_id'],
                    "uploaded_at" =>  now()->toDateTimeString(),
                    "asset_order" =>  $getItemAssetV->asset_order,
                    "status" =>  $getItemAssetV->status,
                ];
            }
        }
        $this->createItemAsset = $createItemAsset;
    }

    function getAllItemExemplarData()
    {
        $AllItemArr = $this->createItemArr;

        $getItemExemplar = DB::table('item_associations')
                                ->select('item_associations.*')
                                ->where('association_type',8)
                                ->where('is_deleted', 0)
                                ->whereIn('source_item_id', $this->all_child_nodes_item_id)
                                ->get()->toArray();

        $createItemExemplar = [];
        $exemplarAssetId = [];
        foreach($getItemExemplar as $getItemExemplarK => $getItemExemplarV)
        {
            $newExemplarId = $this->createUniversalUniqueIdentifier();

            $createItemExemplar[$getItemExemplarV->item_association_id] = [
                "item_association_id"           => $newExemplarId,
                "association_type"              => $getItemExemplarV->association_type,
                "document_id"                   => $this->new_document_id,
                "association_group_id"          => '',
                "origin_node_id"                => $AllItemArr[$getItemExemplarV->source_item_id]['item_id'],
                "destination_node_id"           => '',
                "destination_document_id"       => '',
                "sequence_number"               => $getItemExemplarV->sequence_number,
                "external_node_title"           => $getItemExemplarV->external_node_title,
                "external_node_url"             => $getItemExemplarV->external_node_url,
                "description"                   => $getItemExemplarV->description,
                "created_at"                    => now()->toDateTimeString(),
                "updated_at"                    => now()->toDateTimeString(),
                "source_item_association_id"    => $newExemplarId,
                "organization_id"               => $this->user['organization_id'],
                "is_reverse_association"        => $getItemExemplarV->is_reverse_association,
                "uri"                           => !empty($getItemExemplarV->uri) ? $getItemExemplarV->uri : '',
                "has_asset"                     => $getItemExemplarV->description,
                'is_deleted' => 0,
                "source_document_id"            => $this->new_document_id,
                "source_item_id"                => $AllItemArr[$getItemExemplarV->source_item_id]['item_id'],
                "target_document_id"            => '',
                "target_item_id"                => '',
            ];

            if($getItemExemplarV->has_asset == 1)
            {
                array_push($exemplarAssetId,$getItemExemplarV->item_association_id);
            }
        }

        $getExamplarAsset = DB::table('assets')
                                ->select('assets.*')
                                ->where('asset_linked_type','=',3)
                                ->whereIn('item_linked_id',$exemplarAssetId)
                                ->get()->toArray();

        $createExemplarAsset = [];
        foreach($getExamplarAsset as $getExamplarAssetK => $getExamplarAssetV)
        {
            $newExemplarAssetId = $this->createUniversalUniqueIdentifier();
            $exemplarname       = $getExamplarAssetV->asset_target_file_name;
            $exemplarext=$this->get_file_extension($exemplarname);
            $exemplartargetFile = $newExemplarAssetId.'.'.$exemplarext;
            $exemplarbucketName = config("filesystems")["disks"]["s3"]["bucket"];
            $exemplarpath = "ASSET/EXEMPLAR/";
            $exemplarexists = Storage::disk('s3')->exists($exemplarpath.$exemplarname);
            if($exemplarexists)
            {
                if($exemplarname!=="")
                {
                    Storage::disk('s3')->copy($exemplarpath.$exemplarname,$exemplarpath.$exemplartargetFile);
                }

                $createExemplarAsset[$newExemplarAssetId] = [ 
                "asset_id" =>  $newExemplarAssetId, 
                "organization_id" =>  $this->user['organization_id'],
                "document_id" =>  $this->new_document_id,
                "asset_name" =>  $getExamplarAssetV->asset_name,
                "asset_target_file_name" =>  $exemplartargetFile,
                "asset_content_type" =>  $getExamplarAssetV->asset_content_type,
                "asset_size" =>  $getExamplarAssetV->asset_size,
                "title" =>  $getExamplarAssetV->title,
                "description" =>  $getExamplarAssetV->description,
                "asset_linked_type" =>  $getExamplarAssetV->asset_linked_type,
                "item_linked_id" =>  $createItemExemplar[$getExamplarAssetV->item_linked_id]['item_association_id'],
                "uploaded_at" =>  '',
                "asset_order" =>  $getExamplarAssetV->asset_order,
                "status" =>  $getExamplarAssetV->status,
                ];
                $baseUrl = env("APP_URL");
                $acmtApiVersionPrefix = "api/v1";
                $endPoint = "asset/preview/exemplar";
                $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
                $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$exemplartargetFile}";
                $external_node_url = $fullyResolvedUri;
                $createItemExemplar[$getExamplarAssetV->item_linked_id]['has_asset'] = 1;
                $createItemExemplar[$getExamplarAssetV->item_linked_id]['external_node_url'] = $external_node_url;
            }
        }
        $this->createItemExemplar = $createItemExemplar;
        $this->createExamplarAsset = $createExemplarAsset;
    }

    public function getAllItemChildAssociation()
    {
        $all_child_nodes_asso = DB::table('item_associations')->where('document_id', $this->document_id)->whereNotIn('association_type', [8])->where('is_deleted', 0)->get();

        foreach ($all_child_nodes_asso as $child_nodes_asso) {
            $association_identifier =   $this->createUniversalUniqueIdentifier();
            $current_date_time = $this->createDateTime();
            if((in_array($child_nodes_asso->target_item_id, $this->all_child_nodes_item_id) || $child_nodes_asso->target_item_id == $this->document_id) && $child_nodes_asso->association_type == 1) {
                if($this->document_id == $child_nodes_asso->target_item_id) {
                    $target_item_id = $this->new_document_id;
                } else if(isset($this->createItemArr[$child_nodes_asso->target_item_id])){
                    $target_item_id = $this->createItemArr[$child_nodes_asso->target_item_id]['item_id'];
                } else {
                    $target_item_id = '';
                }
                $uri = $this->getCaseApiUri("CFAssociations", $association_identifier, true, $this->organization_code,$this->domainName);
                if($target_item_id) {
                    $externalNodeuri = $this->getCaseApiUri("CFItems", $target_item_id, true, $this->organization_code,$this->domainName);
                } else {
                    $externalNodeuri = '';
                }
                // get target document id based on destination node id start
                $documentIdentifier = $this->itemRepository->find($target_item_id);
                $targetDocumentIdentifier = $documentIdentifier['document_id'];
                $this->all_child_associations[] = [
                    'item_association_id' => $association_identifier,
                    'association_type' => $child_nodes_asso->association_type,
                    'document_id' => $this->new_document_id,
                    'association_group_id' => $child_nodes_asso->association_group_id,
                    'origin_node_id' => $this->createItemArr[$child_nodes_asso->source_item_id]['item_id'],
                    'destination_node_id' => $target_item_id,
                    'destination_document_id' => $this->new_document_id,
                    'sequence_number' => $child_nodes_asso->sequence_number,
                    'is_deleted' => 0,
                    'source_item_association_id' => $association_identifier,
                    'external_node_title' => $child_nodes_asso->external_node_title,
                    'description' => $child_nodes_asso->description,
                    'external_node_url' => $externalNodeuri,
                    'organization_id' => $this->user['organization_id'],
                    'has_asset' => $child_nodes_asso->has_asset,
                    'created_at' => $current_date_time,
                    'updated_at' => $current_date_time,
                    'uri' => $uri,
                    'is_reverse_association' => $child_nodes_asso->is_reverse_association,
                    "source_document_id"            => $this->new_document_id,
                    "source_item_id"                => $this->createItemArr[$child_nodes_asso->source_item_id]['item_id'],
                    "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$this->new_document_id,
                    "target_item_id"                => $target_item_id,
                ];
            } else {
                # for when copy_association = true
                if($this->document_id == $child_nodes_asso->target_item_id) {
                    $target_item_id = $this->new_document_id;
                } else if(isset($this->createItemArr[$child_nodes_asso->target_item_id])){
                    $target_item_id = $this->createItemArr[$child_nodes_asso->target_item_id]['item_id'];
                } else {
                    $target_item_id = $child_nodes_asso->target_item_id;
                }
                $uri = $this->getCaseApiUri("CFAssociations", $association_identifier, true, $this->organization_code,$this->domainName);
                $externalNodeuri = $this->getCaseApiUri("CFItems", $target_item_id, true, $this->organization_code,$this->domainName);
                // get target document id based on destination node id start
                $documentIdentifier = $this->itemRepository->find($target_item_id);
                $targetDocumentIdentifier = $documentIdentifier['document_id'];
                $this->all_other_associations[] = [
                    'item_association_id' => $association_identifier,
                    'association_type' => $child_nodes_asso->association_type,
                    'document_id' => $this->new_document_id,
                    'association_group_id' => $child_nodes_asso->association_group_id,
                    'origin_node_id' => $this->createItemArr[$child_nodes_asso->source_item_id]['item_id'],
                    'destination_node_id' => $target_item_id,
                    'destination_document_id' => ($child_nodes_asso->destination_document_id!=$this->document_id) ? $child_nodes_asso->destination_document_id : $this->new_document_id,
                    'sequence_number' => $child_nodes_asso->sequence_number,
                    'is_deleted' => 0,
                    'source_item_association_id' => $association_identifier,
                    'external_node_title' => $child_nodes_asso->external_node_title,
                    'description' => $child_nodes_asso->description,
                    'external_node_url' => $externalNodeuri,
                    'organization_id' => $this->user['organization_id'],
                    'has_asset' => $child_nodes_asso->has_asset,
                    'created_at' => $current_date_time,
                    'updated_at' => $current_date_time,
                    'uri' => $uri,
                    'is_reverse_association' => $child_nodes_asso->is_reverse_association,
                    "source_document_id"            => $this->new_document_id,
                    "source_item_id"                => $this->createItemArr[$child_nodes_asso->source_item_id]['item_id'],
                    "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$this->new_document_id,
                    "target_item_id"                => $target_item_id,
                ];
            }
        }
    }

    public function getAllRevereseAssociation()
    {
        $AllItemArr = $this->createItemArr;
        $getItemReverseAssociation = DB::table('item_associations')
                                        ->select('item_associations.*')
                                        ->whereIn('target_item_id', $this->all_child_nodes_item_id)
                                        // ->where('is_reverse_association', 1)
                                        ->where('is_deleted', 0)
                                        ->get();

        foreach($getItemReverseAssociation as $reverseAsso)
        {
            if(!($reverseAsso->association_type == 1))
            {
                $newAssociationId = $this->createUniversalUniqueIdentifier();
                $originId = (isset($AllItemArr[$reverseAsso->source_item_id]['item_id'])) ? $AllItemArr[$reverseAsso->source_item_id]['item_id'] : $reverseAsso->source_item_id;
                $destinationId = (isset($AllItemArr[$reverseAsso->target_item_id]['item_id'])) ? $AllItemArr[$reverseAsso->target_item_id]['item_id'] : $reverseAsso->target_item_id;
                $uri = $this->getCaseApiUri("CFAssociations", $newAssociationId, true, $this->organization_code,$this->domainName);
                $externalNodeuri = $this->getCaseApiUri("CFItems", $destinationId, true, $this->organization_code,$this->domainName);
                // get target document id based on destination node id start
                $documentIdentifier1 = $this->itemRepository->find($destinationId);
                $targetDocumentIdentifier1 = $documentIdentifier1['document_id'];
                if($reverseAsso->document_id!=$this->document_id){
                    $this->all_other_associations[] = [
                        "item_association_id"           => $newAssociationId,
                        "association_type"              => $reverseAsso->association_type,
                        "document_id"                   => $reverseAsso->document_id,
                        "association_group_id"          => '',
                        "origin_node_id"                => $originId,
                        "destination_node_id"           => $destinationId,
                        "destination_document_id"       => $this->new_document_id,
                        "sequence_number"               => $reverseAsso->sequence_number,
                        "external_node_title"           => $reverseAsso->external_node_title,
                        "external_node_url"             => $externalNodeuri,
                        "description"                   => $reverseAsso->description,
                        "created_at"                    => now()->toDateTimeString(),
                        "updated_at"                    => now()->toDateTimeString(),
                        "source_item_association_id"    => $newAssociationId,
                        "organization_id"               => $this->user['organization_id'],
                        "is_reverse_association"        => $reverseAsso->is_reverse_association,
                        "uri"                           => $uri,
                        "has_asset"                     => 0,
                        'is_deleted'                    => 0,
                        "source_document_id"            => $reverseAsso->document_id,
                        "source_item_id"                => $originId,
                        "target_document_id"            => $this->new_document_id,
                        "target_item_id"                => $destinationId
                    ];
                }
            }
        }
    }

    public function getLastSequenceNoForNodes()
    {
        $allAssociation = array_merge($this->all_child_associations, $this->createItemExemplar);
        if($this->isCopyAsso)
        {
            $allAssociation = array_merge($allAssociation, $this->all_other_associations);
        }

        $allAssoMaxSeqNoItem = [];
        foreach($allAssociation as $asso) {
            if(!isset($allAssoMaxSeqNoItem[$asso['source_item_id']])) {
                $allAssoMaxSeqNoItem[$asso['source_item_id']] = (int)$asso['sequence_number'];
            } else {
                $thisSeqNo = (int)$asso['sequence_number'];
                if($allAssoMaxSeqNoItem[$asso['source_item_id']] < $thisSeqNo) {
                    $allAssoMaxSeqNoItem[$asso['source_item_id']] = $thisSeqNo;
                }
            }
        }

        return $allAssoMaxSeqNoItem;
    }

    public function getExactMatchAssociation()
    {
        $allAssoMaxSeqNoItem = $this->getLastSequenceNoForNodes();
        $item_arr = $this->createItemArr;
        foreach ($item_arr as $item_arr_key => $item_arr_value) {
            $association_identifier =   $this->createUniversalUniqueIdentifier();
            $current_date_time = $this->createDateTime();
            $uri = $this->getCaseApiUri("CFAssociations", $association_identifier, true, $this->organization_code,$this->domainName);
            $externalNodeuri = $this->getCaseApiUri("CFItems", $item_arr_key, true, $this->organization_code,$this->domainName);
            // get target document id based on destination node id start
            $documentIdentifier = $this->itemRepository->find($item_arr_key);
            $targetDocumentIdentifier = $documentIdentifier['document_id'];
            if(isset($allAssoMaxSeqNoItem[$item_arr_value['item_id']])) {
                $sequenceNo = $allAssoMaxSeqNoItem[$item_arr_value['item_id']] + 1;
                $allAssoMaxSeqNoItem[$item_arr_value['item_id']] = $allAssoMaxSeqNoItem[$item_arr_value['item_id']] + 1;
            } else {
                $sequenceNo = 1;
                $allAssoMaxSeqNoItem[$item_arr_value['item_id']] = 1;
            }
            $this->all_exact_match_associations[] = [
                'item_association_id' => $association_identifier,
                'association_type' => 4, //exact match association_type
                'document_id' => $this->new_document_id,
                'association_group_id' => '',
                'origin_node_id' => $item_arr_value['item_id'],
                'destination_node_id' => $item_arr_key,
                'destination_document_id' => $this->new_document_id,
                'sequence_number' => $sequenceNo,
                'is_deleted' => 0,
                'source_item_association_id' => $association_identifier,
                'external_node_title' => $item_arr_value['full_statement'],
                'description' => '',
                'external_node_url' => $externalNodeuri,
                'organization_id' => $this->user['organization_id'],
                'has_asset' => 0,
                'created_at' => $current_date_time,
                'updated_at' => $current_date_time,
                'uri' => $uri,
                'is_reverse_association' => '', // to do
                "source_document_id"            => $this->new_document_id,
                "source_item_id"                => $item_arr_value['item_id'],
                "target_document_id"            => !empty($targetDocumentIdentifier)?$targetDocumentIdentifier:$this->new_document_id,
                "target_item_id"                => $item_arr_key
            ];
        }
    }

    function InsertItemAndRelatedData()
    {
        #array_values is used to change the index of array from item identifiers to same index as parent array
        $allAssociation = array_values(array_merge($this->all_child_associations, $this->createItemExemplar));
        if($this->iscopy)
        {
            $allAssociation = array_values(array_merge($allAssociation, $this->all_exact_match_associations));
        }
        if($this->isCopyAsso)
        {
            $allAssociation = array_values(array_merge($allAssociation, $this->all_other_associations));
        }
        $allAsset = array_merge($this->createItemAsset, $this->createExamplarAsset);

        $itemInsertArry = array_chunk($this->createItemArr, 1000);
        foreach($itemInsertArry as $itemInsertArryK => $itemInsertArryV)
        {
            DB::table('items')->insert($itemInsertArryV);
        }

        $itemConceptArry = array_chunk($this->createConceptArr, 1000);
        foreach($itemConceptArry as $itemConceptArryK => $itemConceptArryV)
        {
            DB::table('concepts')->insert($itemConceptArryV);
        }

        $itemLicenseArry = array_chunk($this->createLicenseArr, 1000);
        foreach($itemLicenseArry as $itemLicenseArryK => $itemLicenseArryV)
        {
            DB::table('licenses')->insert($itemLicenseArryV);
        }

        $itemCustomArry = array_chunk($this->createCustomArr, 1000);
        foreach($itemCustomArry as $itemCustomArryK => $itemCustomArryV)
        {
            DB::table('item_metadata')->insert($itemCustomArryV);
        }

        if(!empty($allAsset))
        {
            $allAssetArry = array_chunk($allAsset, 1000);
            foreach($allAssetArry as $allAssetArryK => $allAssetArryV)
            {
                DB::table('assets')->insert($allAssetArryV);
            }
        }

        $allAssociationArry = array_chunk($allAssociation,1000);
        foreach($allAssociationArry as $allAssociationArryK => $allAssociationArryV)
        {
            DB::table('item_associations')->insert($allAssociationArryV);
        }

        // raise delta document update to cloud search each time document data is created
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["taxonomy_created"];
        $eventData = [
            "type_id" => $this->new_document_id,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }

    public function copyDocumentNode()
    {
        $current_date_time = $this->createDateTime();
        $source_document_node = $this->getDocumentDetails();
        $this->source_taxonomy_name = $source_document_node->title;
        $this->new_document_id = $this->createUniversalUniqueIdentifier();
        $document_license = [];
        $source_license_uri_object = '';
        if(isset($source_document_node->license_id) && $source_document_node->license_id != '') {
            $document_license = $this->createDocumentLicenseArr($source_document_node->license_id);
            $source_license_uri_object = $document_license['source_license_uri_object'];
            unset($document_license['source_license_uri_object']);
        }

        $document_subject_arr = $this->createDocumentSubjectArr();
        $document_subject = $document_subject_arr['document_subject_arr'];
        $subject = $document_subject_arr['subject_arr'];
        $metadata = $this->createMetadataArr();
        $asset = $this->createDocumentAssetArr();
        $uri = $this->getCaseApiUri("CFDocuments", $this->new_document_id, true, $this->organization_code,$this->domainName);
        $taxonomy_data = [
            "document_id"               =>  $this->new_document_id,
            "title"                     =>  $this->taxonomy_name,
            "official_source_url"       =>  !empty($source_document_node->official_source_url) ? $source_document_node->official_source_url : "",
            "language_id"               =>  !empty($source_document_node->language_id) ? $source_document_node->language_id : "",
            "adoption_status"           =>  2,// Adoption Status changed to 2 for Draft as per UF-3498
            'status'                    =>  1,
            "notes"                     =>  !empty($source_document_node->notes) ? $source_document_node->notes : "",
            "publisher"                 =>  !empty($source_document_node->publisher) ? $source_document_node->publisher : "",
            "description"               =>  !empty($source_document_node->description) ? $source_document_node->description : "",
            "version"                   =>  !empty($source_document_node->version) ? $source_document_node->version : "",
            "status_start_date"         =>  !empty($source_document_node->status_start_date) ? $source_document_node->status_start_date : "",
            "status_end_date"           =>  !empty($source_document_node->status_end_date) ? $source_document_node->status_end_date : "",
            "license_id"                =>  isset($document_license['license_id']) ? $document_license['license_id'] : "",
            "source_license_uri_object" =>  $source_license_uri_object,
            "organization_id"           =>  $this->user['organization_id'],
            "document_type"             =>  !empty($source_document_node->document_type) ? $source_document_node->document_type : 1,
            "creator"                   =>  !empty($source_document_node->creator) ? $source_document_node->creator : $this->organization_name,
            'is_deleted'                =>  0,
            "created_by"                =>  $this->user['user_id'],
            "updated_by"                =>  $this->user['user_id'],
            "source_document_id"        =>  $this->new_document_id,
            "pulished_taxonomy_snapshot_s3_zip_file_id"        =>  '',
            "node_type_id"              =>  !empty($source_document_node->node_type_id) ? $source_document_node->node_type_id : "",
            "node_template_id"          =>  !empty($source_document_node->node_template_id) ? $source_document_node->node_template_id : "",
            "created_at"                =>  $current_date_time,
            "updated_at"                =>  $current_date_time,
            "import_type"               =>  2,
            "actual_import_type"        =>  2,      // for import type column view 
            "uri"                       =>  $uri,
            "title_html"                =>  $this->taxonomy_name,
            "notes_html"                =>  !empty($source_document_node->notes_html) ? $source_document_node->notes_html : "",
            "publisher_html"            =>  $this->user['user_id'],
            "description_html"          =>  !empty($source_document_node->description_html) ? $source_document_node->description_html : "",
            "custom_view_visibility"    =>  !empty($source_document_node->custom_view_visibility) ? $source_document_node->custom_view_visibility : 0,
            "import_status"             =>  1 ,// to hide taxonomy while its getting copied
            "source_type"               =>  !empty($source_document_node->source_type) ? $source_document_node->source_type : "",           //set source_type for Copy Taxonomy
        ];

        $this->insertAllDocumentRelated($document_license, $document_subject, $subject, $metadata, $asset, $taxonomy_data);

        return $this->new_document_id;
    }

    public function getDocumentDetails()
    {
        return DB::table('documents')
                    ->where('document_id', $this->document_id)
                    ->where('is_deleted', 0)
                    ->first();
    }

    public function createDocumentLicenseArr($license_id)
    {
        $license = DB::table('licenses')->where('license_id', $license_id)->first();
        $new_generated_license_id = $this->createUniversalUniqueIdentifier();
        $current_date_time = $this->createDateTime();
        $uri = $this->getCaseApiUri("CFLicenses", $new_generated_license_id, true, $this->organization_code,$this->domainName);
        $source_license_uri_object = json_encode([
            "identifier" =>  $new_generated_license_id,
            "uri" =>  $uri,
            "title" => !empty($license->title) ? $license->title : ''
        ]);
        return [
            'license_id' => $new_generated_license_id,
            'title' => !empty($license->title) ? $license->title : '',
            'description' => !empty($license->description) ? $license->description : '',
            'license_text' => !empty($license->license_text) ? $license->license_text : '',
            'is_deleted' => 0,
            'source_license_id' => $new_generated_license_id,
            'organization_id' => $this->user['organization_id'],
            'created_at' => $current_date_time,
            'updated_at' => $current_date_time,
            'uri' => $uri,
            'title_html' => !empty($license->title_html) ? $license->title_html : '',
            'description_html' => !empty($license->description_html) ? $license->description_html : '',
            'license_text_html' => !empty($license->license_text_html) ? $license->license_text_html : '',
            'source_license_uri_object' => $source_license_uri_object
        ];
    }

    public function createDocumentSubjectArr()
    {
        $subject = DB::table('document_subject')
                        ->select('document_subject.*','subjects.*')
                        ->leftJoin('subjects', 'subjects.subject_id', '=', 'document_subject.subject_id')
                        ->where('document_id', $this->document_id)
                        ->first();
        $document_subject_arr = [];
        $subject_arr = [];
        if(!empty($subject)) {
            $new_generated_subject_id = $this->createUniversalUniqueIdentifier();
            $current_date_time = $this->createDateTime();
            $uri = $this->getCaseApiUri("CFSubjects", $new_generated_subject_id, $this->organization_code,$this->domainName);
            $document_subject_arr = [
                'document_id' => $this->new_document_id,
                'subject_id' => $new_generated_subject_id,
                'source_subject_uri_object' => json_encode([
                    "identifier"    =>  $new_generated_subject_id,
                    "uri"           =>  $uri,
                    "title"         =>  $subject->title
                ])
            ];
            $subject_arr = [
                'subject_id' => $new_generated_subject_id,
                'title' => $subject->title,
                'hierarchy_code' => $subject->hierarchy_code,
                'description' => $subject->description,
                'is_deleted' => 0,
                'source_subject_id' => $new_generated_subject_id,
                'organization_id' => $this->user['organization_id'],
                'created_at' => $current_date_time,
                'updated_at' => $current_date_time,
                'uri' => $uri,
                'title_html' => $subject->title_html,
                'description_html' => $subject->description_html,
            ];
        }

        return [
            'document_subject_arr' => $document_subject_arr,
            'subject_arr' => $subject_arr
        ];
    }

    public function createMetadataArr()
    {
        $document_metadatas = DB::table('document_metadata')->where('document_id', $this->document_id)->get();

        $document_metadata_arr = [];
        foreach ($document_metadatas as $metadata) {
            array_push($document_metadata_arr, [
                'document_id' => $this->new_document_id,
                'metadata_id' => $metadata->metadata_id,
                'metadata_value' => $metadata->metadata_value,
                'is_additional' => $metadata->is_additional,
                'metadata_value_html' => $metadata->metadata_value_html
            ]);
        }
        return $document_metadata_arr;
    }

    public function get_file_extension($file_name) {
        return substr(strrchr($file_name,'.'),1);
    }

    public function createDocumentAssetArr()
    {
        $document_assets = DB::table('assets')
                            ->where('asset_linked_type', '=', 1)
                            ->where('document_id', $this->document_id)
                            ->get();
        $document_assets_arr = [];
        foreach($document_assets as $asset)
        {
            $new_asset_id = $this->createUniversalUniqueIdentifier();
            $name = $asset->asset_target_file_name;
            $ext=$this->get_file_extension($name);
            $target_file = $new_asset_id.'.'.$ext;
            $path = "ASSET/DOCUMENT/";
            $exists = Storage::disk('s3')->exists($path.$name);
            if($exists)
            {
                if($name !== "")
                {
                    Storage::disk('s3')->copy($path.$name, $path.$target_file);
                }
                $document_assets_arr[] = [
                    "asset_id" =>  $new_asset_id,
                    "organization_id" =>  $this->user['organization_id'],
                    "document_id" =>  $this->new_document_id,
                    "asset_name" =>  $asset->asset_name,
                    "asset_target_file_name" =>  $target_file,
                    "asset_content_type" =>  $asset->asset_content_type,
                    "asset_size" =>  $asset->asset_size,
                    "title" =>  $asset->title,
                    "description" =>  $asset->description,
                    "asset_linked_type" =>  1,
                    "item_linked_id" =>  $this->new_document_id,
                    "uploaded_at" =>  now()->toDateTimeString(),
                    "asset_order" =>  $asset->asset_order,
                    "status" =>  $asset->status,
                ];
            }
        }

        return $document_assets_arr;
    }

    public function insertAllDocumentRelated($document_license, $document_subject, $subject, $metadata, $asset, $taxonomy_data)
    {
        if(!empty($document_license)){
            DB::table('licenses')->insert($document_license);
        }
        if(!empty($document_subject)){
            DB::table('document_subject')->insert($document_subject);
        }
        if(!empty($subject)){
            DB::table('subjects')->insert($subject);
        }
        if(!empty($metadata)){
            DB::table('document_metadata')->insert($metadata);
        }
        if(!empty($asset)){
            DB::table('assets')->insert($asset);
        }
        if(!empty($taxonomy_data)){
            DB::table('documents')->insert($taxonomy_data);
        }
    }
}
