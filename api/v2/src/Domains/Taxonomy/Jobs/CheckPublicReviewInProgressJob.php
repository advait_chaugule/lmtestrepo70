<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class CheckPublicReviewInProgressJob extends Job
{
    private $documentIdentifier;
    private $organizationIdentifier;
    
    private $documentRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, string $organizationIdentifier)
    {
        //Set the private attributes
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        //Set the repository handler
        $this->documentRepository   =   $documentRepository;

        return $this->fetchPublicReviewHistoryStatus();
    }

    public function setDocumentIdentifier($identifier) {
        $this->documentIdentifier    =   $identifier;
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =    $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    private function fetchPublicReviewHistoryStatus() {
        $openFlag               =   0;
        $reviewStatus           =   ['1','2'];
        $documentIdentifier     =   $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();


        $publicReviewHistory   =   $this->documentRepository->checkPublicReviewIsOpen($documentIdentifier,  $organizationIdentifier, $reviewStatus); 

        if($publicReviewHistory->count() > 0) {
            $openFlag   =   1;            
        }
        
        return $openFlag;
    }
}
