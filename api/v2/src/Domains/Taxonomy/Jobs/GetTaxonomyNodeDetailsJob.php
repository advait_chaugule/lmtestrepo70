<?php
namespace App\Domains\Taxonomy\Jobs;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\NodeLevelTrait;
use App\Services\Api\Traits\MetadataHelperTrait;
use DB;
class GetTaxonomyNodeDetailsJob extends Job
{
    use NodeLevelTrait;
    use MetadataHelperTrait;
    private $organizationId;
    private $documentId;
    private $viewType;
    private $returnOrder;
    public function __construct($documentId,$organizationId,$viewType,$returnOrder)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
        $this->viewType       = $viewType;
        $this->returnOrder    = $returnOrder;
    }

    public function handle()
    {
        if($this->returnOrder == '0'){
            $getSetting = $this->getTaxonomySetting($this->documentId,$this->organizationId);
            if(!empty($getSetting)){
                $nodeDetails   = $this->prepareDataForNodeDetails($this->documentId,$this->organizationId,$this->viewType); 
            }else{
                $nodeDetails   = $this->prepareDefaultJson();
            }
        }else{
            $nodeDetails   = $this->prepareDataForNodeDetails($this->documentId,$this->organizationId,$this->viewType);
        }
        return $nodeDetails;
    }

    public function prepareDataForNodeDetails($documentId,$organizationId,$viewType)
    {
        /*Get Node type Ids from Item table*/
        $getNodeIds  =  $this->getNodesTypeIds($documentId,$organizationId);
        $nodeIds     =  array_column($getNodeIds,'node_type_id');
        /*Get Node details*/
        $nodeDetails = $this->getNodeTypeDetails($nodeIds);
        // Get node level of document with order and compatibility
        $nodeDepth = $this->getNodesDepth($documentId);
        $default = [];

		if(isset($nodeDepth['sequence'])){
          
			$default = array_values($nodeDepth['sequence']);
			$nodeDepth['sequence'] = array_unique($nodeDepth['sequence']);
		}     
        $compatibility = $nodeDepth['compatibility'];
		// Add missing nodetypes in sequence for sorting
        foreach ($nodeDetails as $nodeDetailsK=>$nodeDetailsV) {
            $nodeTypeId =  $nodeDetailsV->node_type_id;
            if(!isset($nodeDepth['sequence']) || !in_array($nodeTypeId,$nodeDepth['sequence']))
                $nodeDepth['sequence'][] = $nodeTypeId;
        }
       
        $metadataNodeMapping=[];
		$nodeTypeArr =[];
        /*Get metadata ids from node ids*/
        $metadataIdsList  = $this->getMetadataIdsList($nodeIds);
        if($metadataIdsList) {
			// To sort nodetype and metadata mapping array in a same sequence			
            foreach ($nodeDepth['sequence'] as $value) {
                foreach ($nodeDetails as $nodeDetailsK=>$nodeDetailsV) {
                $nodeTypeId =  $nodeDetailsV->node_type_id;
                
                    if($nodeTypeId == $value){
                        $nodeTypeArr[$nodeTypeId] = ['display_name' =>$nodeDetailsV->title];
                    }
                }
                foreach ($metadataIdsList as $metadataIdsK => $metadataIdsV) {
                    $metadataNodeTypeId = $metadataIdsV->node_type_id;
                    if($metadataNodeTypeId == $value){
                        $metadataNodeMapping[$metadataNodeTypeId][] = ['metadata_id' => $metadataIdsV->metadata_id];
                    }
                }
            }
            $metadataIds = array_column($metadataIdsList,'metadata_id');
            $getMetadataDetails = $this->getMetadataDetails($metadataIds,$organizationId);
            $metadataArr = [];
            if($getMetadataDetails) {
                foreach ($getMetadataDetails as $getMetadataDetailsK => $getMetadataDetailsV) {
                    $metadataId = $getMetadataDetailsV->metadata_id;
                    $metadataArr[$metadataId] = ['display_name' => $getMetadataDetailsV->name,
                        'internal_name' => $getMetadataDetailsV->internal_name,
                        'is_custom' => $getMetadataDetailsV->is_custom
                    ];
                }
            }
            
            /*Prepare metadata details*/
            $nodeMetadataVal =[];
            foreach ($metadataArr as $metadataArrK=>$metadataArrV){
                foreach ($metadataNodeMapping as $metadataNodeMappingK=>$metadataNodeMappingV)
                {
                    foreach ($metadataNodeMappingV as $key=>$metadataNodeMappingData)
                    if($metadataArrK==$metadataNodeMappingData['metadata_id']) {
                        if($metadataArrV['internal_name']=='language')
                        {
                            $metadataArrV['internal_name']= 'language_name';
                        }
                        $nodeMetadataVal[$metadataNodeMappingK]['metadata'][]=
                            [
                                'display_name'  => isset($metadataArrV['display_name'])?$metadataArrV['display_name']:"",
                                'internal_name' => isset($metadataArrV['internal_name'])?$metadataArrV['internal_name']:'',
                                'metadata_id'   => $metadataArrK,
                                'is_custom'     => isset($metadataArrV['is_custom'])?$metadataArrV['is_custom']:"",
                            ];
                    }
                }
            }
            //Mapping Node Id with their node name
            $nodeNameMappingWithNodeIds=[];
            foreach ($nodeTypeArr as $nodeTypeArrK=>$nodeTypeArrV) {
                $nodeNameMappingWithNodeIds[$nodeTypeArrK] = [
                     'node_type_id' => isset($nodeTypeArrK)?$nodeTypeArrK:"",
                     'internal_name'=> isset($nodeTypeArrV['display_name'])?$nodeTypeArrV['display_name']:"",
                    'display_name'  => isset($nodeTypeArrV['display_name'])?$nodeTypeArrV['display_name']:"",
                ];
            }
            //Finally merge data of node and metadata
            $NodeWithMetaData=[];
            foreach ($nodeMetadataVal as $nodeMetadataValK => $nodeMetadataValV) {
                foreach ($nodeNameMappingWithNodeIds as $nodeNameMappingWithNodeIdK=>$nodeNameMappingWithNodeIdV) {
                    if($nodeNameMappingWithNodeIdK==$nodeMetadataValK) {
                        $NodeWithMetaData['nodeTypes'][] = array_merge($nodeNameMappingWithNodeIdV,$nodeMetadataValV);
                    }
                }
            }
            $NodeWithMetaData['defaultNodeTypesOrder'] = $default;
            $NodeWithMetaData['compatibility'] = $compatibility;
            $prepareArr=[];
            if($viewType=='node_type') {
                return $NodeWithMetaData;
            }else if($viewType=='metadata'){
                foreach ($nodeMetadataVal as $metadataKey => $metadataValue) {
                    $metadataList = $metadataValue['metadata'];
                    foreach ($metadataList as $metadataListV) {
                        array_push($prepareArr,$metadataListV);
                    }
                }
             $metadataId            = array_column($prepareArr,'metadata_id');
             $uniqueMetadataId[]    = array_unique($metadataId);
             $uniqueMetadataList    = array_map("unserialize", array_unique(array_map("serialize", $prepareArr)));
                $uniqueMetadata=[];
                foreach ($uniqueMetadataList as $uniqueMetadataListV) {
                    $uniqueMetadata['metadata'][] = $uniqueMetadataListV;
                }
                return $uniqueMetadata;
              }
            
        }

    }




    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @FunctionName getNodesTypeIds
     * @Purpose This function is used to return distinct node_type_id from item table
     */
    private function getNodesTypeIds($documentId,$organizationId)
    {
        $nodeIds = DB::table('items')->select('node_type_id')
            ->where('document_id',$documentId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->distinct('node_type_id')
            ->get()
            ->toArray();
        return $nodeIds;
    }

    /**
     * @param $nodeIds
     * @return mixed
     * @FunctionName getNodeTypeDetails
     * @purpose This function is used to return node details based on node type Ids
     */
    public function getNodeTypeDetails($nodeIds)
    {
        $nodeDetails = DB::table('node_types')
            ->select('node_type_id','title')
            ->whereIn('node_type_id',$nodeIds)
            ->where('is_deleted',0)
            ->where('used_for', 0)
            ->get()
            ->toArray();
        return $nodeDetails;
    }

    /**
     * @param $uniqueNodeIds
     * @return mixed
     * @FunctionName getMetadataIdsList
     * @Purpose This function is used to return metadata Id and Node type Id based on node type Ids
     */
    private function getMetadataIdsList($uniqueNodeIds)
    {
        $metadataIds = DB::table('node_type_metadata')
            ->select('metadata_id','node_type_id')
            ->whereIn('node_type_id',$uniqueNodeIds)
            ->distinct('metadata_id')
            ->get()
            ->toArray();
        return $metadataIds;
    }

    /**
     * @param $metadataIds
     * @param $organizationId
     * @return mixed
     * @FunctionName getMetadataDetails
     * @Purpose This function is used to return metadata details based on metadata Ids
     */
    private function getMetadataDetails($metadataIds,$organizationId)
    {
        $metadata = DB::table('metadata')
            ->select('metadata_id','name','internal_name','is_custom')
            ->whereIn('metadata_id',$metadataIds)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->toArray();
        return $metadata;
    }

    private function getTaxonomySetting($documentId,$organizationId)
    {
          $userSetting = DB::table('user_settings')->select('json_config_value')
                              ->where('id',$documentId)
                              ->where('organization_id',$organizationId)
                              ->where('id_type',3)
                              ->where('is_deleted',0)
                              ->get()
                              ->first();
          return $userSetting;
    }

    public function prepareDefaultJson()
    {
        // Removed hard coded metadata and added code to fetch from db - Start
        $defaultInternalNames = array("full_statement","node_type","human_coding_scheme");
        $metadataName = $this->getmetadataName($this->organizationId,$defaultInternalNames);
        
        $default_taxonomy_list = array(array($metadataName["full_statement"],"full_statement","120"),array($metadataName["node_type"],"node_type","120"),array($metadataName["human_coding_scheme"],"human_coding_scheme","120"));
        // Removed hard coded metadata and added code to fetch from db - Start
        $num = 1;
        $valuesArray =[];
        foreach ($default_taxonomy_list as $default_taxonomy_listK=>$default_taxonomy_listV)
        {
            $valuesArray[] = 
               ['metadata_id'=> $this->getmetadataid($this->organizationId,$default_taxonomy_listV[1]),
                    'display_name' => $default_taxonomy_listV[0],
                    'internal_name'=> $default_taxonomy_listV[1],
                    'order' => $num,
                    'width' => $default_taxonomy_listV[2],
                    'is_custom' => 0,
                ];
                
            $num++;
            // $finalArr  = array_merge($valueArr,$valuesArray);
        }
        //$tableConfig = ['table_config'=>$valuesArray];
        $valueArr = ['node_type_id'=>'',
        'display_name' => '',
        'internal_name'=> '',
           ];
      $finalArr  = array_merge($valueArr,['metadata'=>$valuesArray]);
        return $finalArr;
    }

    private function getmetadataid($orgnizationIdentifier,$metadataname){
        $queryData = DB::table('metadata')->select('metadata_id')
       ->where('organization_id','=', $orgnizationIdentifier)
       ->where('internal_name','=',$metadataname)
       ->get()
       ->toArray();

       if(!empty($queryData)){
           $metadata_id = $queryData[0]->metadata_id;
       }else{
           $metadata_id = "";
       }
       return $metadata_id;
    }

}