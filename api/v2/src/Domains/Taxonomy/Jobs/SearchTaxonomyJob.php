<?php
namespace App\Domains\Taxonomy\Jobs;

use Lucid\Foundation\Job;

use App\Foundation\BaseCloudSearchHandler;

use Aws\CloudSearchDomain\CloudSearchDomainClient;

class SearchTaxonomyJob extends Job
{

    private $cloudSearchHandler;

    private $searchQuery;
    private $requestUserOrganizationId;

    private $searchClient;

    private $searchResult;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $searchQuery, string $requestUserOrganizationId)
    {
        $this->setSearchQuery($searchQuery);
        $this->setRequestUserOrganizationId($requestUserOrganizationId);
    }

    public function setSearchQuery(string $data) {
        $this->searchQuery = $data;
    }

    public function getSearchQuery(): string {
        return $this->searchQuery;
    }

    public function setRequestUserOrganizationId(string $data) {
        $this->requestUserOrganizationId = $data;
    }

    public function getRequestUserOrganizationId(): string {
        return $this->requestUserOrganizationId;
    }

    public function setSearchClient() {
        $this->searchClient = $this->cloudSearchHandler->getSearchClient();
    }

    public function getSearchClient(): CloudSearchDomainClient {
        return $this->searchClient;
    }

    public function setSearchResult(array $data) {
        $this->searchResult = $data;
    }

    public function getSearchResult(): array {
        return $this->searchResult;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BaseCloudSearchHandler $cloudSearchHandler)
    {
        // set the cloud search handler
        $this->cloudSearchHandler = $cloudSearchHandler;

        // set cloud search client 
        $this->setSearchClient();

        $this->searchAndSetSearchResult();

        return $this->getsearchResult();
    }

    private function searchAndSetSearchResult() {
        $searchClient = $this->getSearchClient();
        $searchQuery = $this->getSearchQuery();
        $organizationIdentifier = $this->getRequestUserOrganizationId();
        $staticSearchFilter = "organization_identifier:'$organizationIdentifier'";
        $fieldsToSearch = config('_search.taxonomy.search_fields');
        $searchFieldsJsonString = json_encode(['fields' => $fieldsToSearch]);
        $searchRequestParams = [
            'query' => $searchQuery,
            'filterQuery' => $staticSearchFilter,
            'queryOptions' => $searchFieldsJsonString

            // 'cursor' => '<string>',
            // 'expr' => '<string>',
            // 'facet' => '<string>',
            // 'filterQuery' => '<string>',
            // 'highlight' => '<string>',
            // 'partial' => true || false,
            // 'query' => '<string>', // REQUIRED
            // 'queryOptions' => '<string>',
            // 'queryParser' => 'simple|structured|lucene|dismax',
            // 'return' => '<string>',
            // 'size' => <integer>,
            // 'sort' => '<string>',
            // 'start' => <integer>,
            // 'stats' => '<string>',
        ];
        $searchAction = $searchClient->search($searchRequestParams);
        $searchResult = $searchAction->get('hits');
        $this->setSearchResult($searchResult);
    }

}
