<?php
namespace App\Domains\Taxonomy\UpdateTaxonomyTime\Listeners;

use App\Domains\Taxonomy\UpdateTaxonomyTime\Events\UpdateTaxonomyTimeEvent as Event;

use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class UpdateTaxonomyTimeListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct(){
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $itemId = $event->itemId;
            $this->dispatch(new UpdateCFItemMultipleTablesJob($itemId));
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
            // cleanup folder and archive from local storage
            $this->cleanLocalStorage();
        }
    }

    
}