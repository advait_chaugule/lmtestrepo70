<?php


namespace App\Domains\Taxonomy\UpdateTaxonomyTime\Events;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateTaxonomyTimeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $itemId;

    public function __construct($itemId){
        $this->itemId = $itemId;
        
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }
}