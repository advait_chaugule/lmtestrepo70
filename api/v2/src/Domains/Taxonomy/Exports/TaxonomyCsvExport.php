<?php

namespace App\Domains\Taxonomy\Exports;

use Illuminate\Support\Collection;

use Maatwebsite\Excel\Concerns\FromCollection;

class TaxonomyCsvExport implements FromCollection
{
    private $dataToExport;

    public function __construct(Collection $dataToExport) {
        $this->dataToExport = $dataToExport;
    }

    public function collection(): Collection {
        return $this->dataToExport;
    }
}