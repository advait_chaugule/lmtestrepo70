<?php
namespace App\Domains\Taxonomy\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class TaxonomyExistsValidator extends BaseValidator {

    protected $rules = [
        'id' => 'required|exists:taxonomies'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];
    
}