<?php
namespace App\Domains\Taxonomy\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class InputDataValidator extends BaseValidator {

    protected $rules = [
        'document_id' => 'required|exists:documents,document_id',
        'project_id' => 'required|exists:projects,project_id',
        'review_number' => 'required'
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        "exists"  =>  ":attribute is invalid."
     ];
    
}