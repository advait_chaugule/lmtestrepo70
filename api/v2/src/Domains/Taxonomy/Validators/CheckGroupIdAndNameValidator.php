<?php
namespace App\Domains\Taxonomy\Validators;

use App\Foundation\BaseValidator;

class CheckGroupIdAndNameValidator extends BaseValidator
{
    public $rules = [
        "group_id" => "required|exists:group,group_id,is_deleted,0",
        'name' => 'required_without:is_default',
        'is_default' => 'required_without:name'
   ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"   => ":attribute does not exist."
    ];
}