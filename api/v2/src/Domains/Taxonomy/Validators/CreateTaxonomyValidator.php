<?php
namespace App\Domains\Taxonomy\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateTaxonomyValidator extends BaseValidator {

    protected $rules = [
        'documentTitle' => 'required',
        'documentId' => 'required|exists:documents,document_id'
    ];

     protected $messages = [
        'required'  =>  ":attribute is required."
     ];
    
}