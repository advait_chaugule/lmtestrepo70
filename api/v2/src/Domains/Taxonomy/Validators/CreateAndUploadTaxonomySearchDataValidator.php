<?php
namespace App\Domains\Taxonomy\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateAndUploadTaxonomySearchDataValidator extends BaseValidator {

    protected $rules = [
        'document_ids' => 'required',
        // 'called_from_http_client' => 'required|in:1,0'
    ];

     protected $messages = [];
    
}