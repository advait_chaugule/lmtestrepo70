<?php
namespace App\Domains\Taxonomy\Validators;

use App\Foundation\BaseValidator;

class CheckGroupIdValidator extends BaseValidator
{
    protected $rules = [
        "group_id" => "required|exists:group,group_id,is_deleted,0"
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"   => ":attribute does not exist."
    ];
}