<?php
namespace App\Domains\Taxonomy\Validators;
use Illuminate\Validation\ValidationException;
use App\Foundation\BaseValidator;

class CopyTaxonomyValidator extends BaseValidator
{
    protected $rules = [
        'documentId'      => 'required|exists:documents,document_id,is_deleted,0',
        'taxonomyName'    => 'required'
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists" => ":attribute is invalid."
    ];
}