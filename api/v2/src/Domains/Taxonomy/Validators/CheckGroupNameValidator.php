<?php
namespace App\Domains\Taxonomy\Validators;

use App\Foundation\BaseValidator;

class CheckGroupNameValidator extends BaseValidator
{
    protected $rules = [
        "name" => "required",
    ];

    protected $messages = [
        "required" => "Group Name is required."
    ];
}