<?php
namespace App\Domains\Taxonomy\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateAndUploadTaxonomyDeltaUpdatesSearchDataValidator extends BaseValidator {

    protected $rules = [
        'document_ids' => 'required_without:item_ids',
        'item_ids' => 'required_without:document_ids'
    ];

    protected $messages = [];
    
}