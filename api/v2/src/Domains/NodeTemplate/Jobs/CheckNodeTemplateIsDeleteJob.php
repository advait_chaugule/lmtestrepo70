<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class CheckNodeTemplateIsDeleteJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepo)
    {
        //Check whether Template deleted
        $attributes = ['node_template_id' => $this->identifier, 'is_deleted' => '1'];
        return $nodeTemplateRepo->findByAttributes($attributes)->isNotEmpty();
    }
}
