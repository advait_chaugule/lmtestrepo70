<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class EditNodeTemplateJob extends Job
{
    use DateHelpersTrait;

    private $nodeTemplateIdentifier;
    private $nodeTemplateRepository;

    private $requestData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, array $inputData)
    {
        //Set the private identifier
        $this->setNodeTemplateIdentifier($identifier);
        $this->requestData  =   $inputData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set the node template repository handler
        $this->nodeTemplateRepository   =   $nodeTemplateRepository;
        //Get the identifier
        $nodeTemplateIdentifier         =   $this->getNodeTemplateIdentifier();
        $dataToUpdate                   =   $this->requestData;
        $updateRequest                  =   $this->nodeTemplateRepository->edit($nodeTemplateIdentifier, $dataToUpdate);
        
        $responseData = $this->parsedResponseData($updateRequest);
        return $responseData;

    }

    //Public Getter and Setter methods
    public function setNodeTemplateIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getNodeTemplateIdentifier() {
        return $this->identifier;
    }

    private function parsedResponseData($data){
        $templateEntity = [
            'node_template_id'  => $data['node_template_id'],
            'template_name'     => $data['name'],
            'is_active'         => $data['is_active'],
            'last_updated_at'   => $this->formatDateTimeToISO8601($data['updated_at'])
        ];

        return $templateEntity;
    }
}
