<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class CreateNodeTemplateJob extends Job
{
    use UuidHelperTrait, DateHelpersTrait;

    private $nodeTemplateRepository;

    private $organizationIdentifier;
    private $nodeTemplateName;
    private $createdBy;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the private attribute
        $this->organizationIdentifier   =   $input['organization_id'];
        $this->nodeTemplateName         =   $input['name'];
        $this->createdBy                =   $input['created_by'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set the Node Template Repository handler
        $this->nodeTemplateRepository   =   $nodeTemplateRepository;
        $attributes =   ['node_template_id' => $this->createUniversalUniqueIdentifier(), 'organization_id'  =>  $this->organizationIdentifier,  'name' =>  $this->nodeTemplateName, 'updated_by' => $this->createdBy, 'created_at' =>  $this->createDateTime(), 'updated_at' =>  $this->createDateTime()];
        $savedTemplateObject    =   $this->nodeTemplateRepository->fillAndSave($attributes);

        return $this->parseResponse($savedTemplateObject);
    }

    private function parseResponse($templateObject){
        $nodeTemplateEntity = [
            'node_template_id'  =>  $templateObject->node_template_id,
        ];

        return $nodeTemplateEntity;
    }
}

