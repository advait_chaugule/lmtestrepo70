<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class AssociateNodeTypeWithNodeTemplateJob extends Job
{
    private $nodeTemplateRepository;

    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set the private attribute
        $this->input    =   $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set the NodeTemplate repo handler
        $this->nodeTemplateRepository = $nodeTemplateRepository;
        $this->nodeTemplateRepository->associateNodeTypeWithTemplate( $this->input );
    }
}
