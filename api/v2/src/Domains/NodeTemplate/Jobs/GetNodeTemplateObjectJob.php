<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class GetNodeTemplateObjectJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the Identifier
        $this->identifier   =   $identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set the node template repo handler
        $this->nodeTemplateRepository   =   $nodeTemplateRepository;
        $identifier                     =   $this->identifier;

        $nodetemplate                   =   $this->nodeTemplateRepository->find($identifier);
        return $nodetemplate;

    }
}
