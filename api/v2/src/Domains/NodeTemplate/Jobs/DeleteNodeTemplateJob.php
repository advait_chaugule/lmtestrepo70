<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class DeleteNodeTemplateJob extends Job
{
    private $identifier;
    private $nodeTemplateRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set Role Identifier
        $this->setIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set NodeTemplate Repo handler
        $this->nodeTemplateRepository = $nodeTemplateRepository;
        //Get the identifier
        $nodeTemplateIdentifier = $this->getIdentifier();
        //set the attribute to update
        $attribute = ['is_deleted' => '1'];
        
        return $this->nodeTemplateRepository->edit($nodeTemplateIdentifier, $attribute);
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }
}
