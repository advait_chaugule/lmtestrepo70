<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class CheckIsNodeTemplateDeletedJob extends Job
{
    private $nodeTemplateRepository;
    private $nodeTemplateIdentifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier)
    {
        //Set the private identifier
        $this->setNodeTemplateIdentifier($identifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set the nodeTemplate Repository handler
        $this->nodeTemplateRepository   =   $nodeTemplateRepository;

        $nodeTemplateIdentifier =   $this->getNodeTemplateIdentifier();

        //Check whether NodeTemplate deleted
        $attributes = ['node_template_id' => $nodeTemplateIdentifier, 'is_deleted' => '1'];
        return $nodeTemplateRepository->findByAttributes($attributes)->isNotEmpty();
    }

    //Public Setter and Getter methods
    public function setNodeTemplateIdentifier($identifier) {
        $this->nodeTemplateIdentifier   =   $identifier;
    }
    
    public function getNodeTemplateIdentifier() {
        return $this->nodeTemplateIdentifier;
    }
}
