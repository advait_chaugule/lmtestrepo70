<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Http\Request;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class ListNodeTemplateJob extends Job
{
    private $nodeTemplateRepository;
    private $templateData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository, Request $request)
    {
        // set NodeTemplate repository handler
        $this->nodeTemplateRepository = $nodeTemplateRepository;

        // set pagination and sort filters here when required

        // get all templates
        $templates = $this->nodeTemplateRepository->getNodeTemplateList($request);

        $this->setTemplates($templates);
        
        return $this->parseResponse();
        
    }

    public function setTemplates($templates) {
        $this->templateData =   $templates;
    }

    public function getTemplates() {
        return $this->templateData;
    }

    private function parseResponse() {
        $templateEntity =   [];
        $templates      =   $this->getTemplates();

        foreach($templates as $template) {
            $usageCount =   $this->nodeTemplateRepository->findUsageCount($template->node_template_id);

            $templateEntity[] =   [
                'node_template' =>  $template->node_template_id,
                'name'          =>  $template->name,
                'usage'         =>  $usageCount
            ];
        }

        return $templateEntity;
        
    }
}
