<?php
namespace App\Domains\NodeTemplate\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTemplateRepositoryInterface;

class GetNodeTemplateDetailJob extends Job
{
    use DateHelpersTrait;

    private $nodeTemplateIdentifier;
    private $nodeTemplateRepository;
    private $nodeTemplateDetail;
    private $nodeTemplateEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $input)
    {
        //Set the identifier
        $this->setNodeTemplateIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTemplateRepositoryInterface $nodeTemplateRepository)
    {
        //Set the Node Template Repository handler
        $this->nodeTemplateRepository   =   $nodeTemplateRepository;
        $nodeTemplateIdentifier         =   $this->getNodeTemplateIdentifier();

        $returnCollection   =   false;
        $fieldsToReturn     =   ['node_template_id', 'name', 'is_active', 'updated_at', 'updated_by'];
        $keyValuePairs      =   ['node_template_id' =>  $nodeTemplateIdentifier];
        $relations          =   ['updatedBy', 'nodeTypes'];

        $nodeTemplateDetail             =   $this->nodeTemplateRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations);
        $this->setNodeTemplateDetail($nodeTemplateDetail);

        $this->parseResponse();

        $nodeTemplateEntity =   $this->getNodeTemplateEntity();

        return $nodeTemplateEntity;
    }

    //Public Getter and Setter Method
    public function setNodeTemplateIdentifier($identifier) {
        $this->nodeTemplateIdentifier = $identifier;
    }

    public function getNodeTemplateIdentifier() {
        return $this->nodeTemplateIdentifier;
    }

    public function setNodeTemplateDetail( $data ) {
        $this->nodeTemplateDetail    =   $data;
    }

    public function getNodeTemplateDetail() {
        return $this->nodeTemplateDetail;
    }

    public function setNodeTemplateEntity( $data ) {
        $this->nodeTemplateEntity    =   $data;
    }

    public function getNodeTemplateEntity() {
        return $this->nodeTemplateEntity;
    }

    private function parsedNodeTypeResponse() {
        $nodeTypeEntity     =   [];

        $nodeTemplateDetail =   $this->getNodeTemplateDetail();
        $nodeTypes          =   $nodeTemplateDetail->nodeTypes;

        //dd($nodeTypes);


        foreach($nodeTypes  as $nodeType) {
            $nodeTypeEntity[]   =   [
                "node_type_id"          =>  $nodeType->node_type_id,
                "title"                 =>  $nodeType->title,
                "parent_node_type_id"   =>  $nodeType->pivot->parent_node_type_id,//$role->pivot->created_at;
                "sort_order"            =>  $nodeType->pivot->sort_order
            ];
        }

        $sortedNodeTypes = array();
        foreach ($nodeTypeEntity as $key => $row)
        {
            $sortedNodeTypes[$key] = $row['sort_order'];
        }
        
        array_multisort($sortedNodeTypes, SORT_ASC, $nodeTypeEntity);
        return $nodeTypeEntity;
    }

    private function parseResponse() {
        $nodeTemplateDetail =   $this->getNodeTemplateDetail();
        
        $nodeTemplateEntity =   [
            'node_template_id'  =>  $nodeTemplateDetail->node_template_id,
            'name'              =>  $nodeTemplateDetail->name,
            'is_active'         =>  $nodeTemplateDetail->is_active,
            'updated_at'        =>  $this->formatDateTimeToISO8601($nodeTemplateDetail->updated_at),
            'updated_by'        =>  $nodeTemplateDetail->updatedBy->first_name.' '.$nodeTemplateDetail->updatedBy->last_name,
            'node_types'        =>  $this->parsedNodeTypeResponse()
        ];

        $this->setNodeTemplateEntity($nodeTemplateEntity);
    }

}
