<?php
namespace App\Domains\NodeTemplate\Tests\Jobs;

use App\Domains\NodeTemplate\Jobs\GetNodeTemplateDetailJob;
use Tests\TestCase;

class GetNodeTemplateDetailJobTest extends TestCase
{
    public function test_NodeTemplateIdentifier()
    {
        $assets = new GetNodeTemplateDetailJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTemplateIdentifier($value);
        $this->assertEquals($value,$assets->getNodeTemplateIdentifier());
    }

    public function test_NodeTemplateDetail()
    {
        $assets = new GetNodeTemplateDetailJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTemplateDetail($value);
        $this->assertEquals($value,$assets->getNodeTemplateDetail());
    }

    public function test_NodeTemplateEntity()
    {
        $assets = new GetNodeTemplateDetailJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTemplateEntity($value);
        $this->assertEquals($value,$assets->getNodeTemplateEntity());
    }
}
