<?php
namespace App\Domains\NodeTemplate\Tests\Jobs;

use App\Domains\NodeTemplate\Jobs\ListNodeTemplateJob;
use Tests\TestCase;

class ListNodeTemplateJobTest extends TestCase
{
    public function test_Templates()
    {
        $assets = new ListNodeTemplateJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setTemplates($value);
        $this->assertEquals($value,$assets->getTemplates());
    }
}
