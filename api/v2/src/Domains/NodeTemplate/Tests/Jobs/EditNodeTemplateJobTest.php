<?php
namespace App\Domains\NodeTemplate\Tests\Jobs;

use App\Domains\NodeTemplate\Jobs\EditNodeTemplateJob;
use Tests\TestCase;

class EditNodeTemplateJobTest extends TestCase
{
    public function test_NodeTemplateIdentifier()
    {
        $data[] = ['989d3047-c369-4b09-b4f1-dee410aa84be'];
        $assets = new EditNodeTemplateJob('c4424f55-8900-446c-9c41-2f2c25d58c08',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTemplateIdentifier($value);
        $this->assertEquals($value,$assets->getNodeTemplateIdentifier());
    }
}
