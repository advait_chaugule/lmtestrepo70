<?php
namespace App\Domains\NodeTemplate\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class NodeTemplateExistsValidator extends BaseValidator {

    protected $rules = [
        'node_template_id' => 'required|exists:node_templates,node_template_id'
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
    ];
    
}