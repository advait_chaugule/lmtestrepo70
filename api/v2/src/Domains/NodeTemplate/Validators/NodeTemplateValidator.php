<?php
namespace App\Domains\NodeTemplate\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class NodeTemplateValidator extends BaseValidator {

    protected $rules = [
        'name' => 'required|string|max:50',
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'string' => ':attribute should be of type string.',
        'max:50' => ':attribute should be of maximum 50 characters.',
     ];
    
}