<?php
namespace App\Domains\Helper\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\File;

class GetFileContentHelperJob extends Job
{
    private $fullFilePath;
    private $baseFolderPathForStorage;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $filePath)
    {
       $this->fullFilePath = storage_path('app')."/".$filePath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileContent = File::get($this->fullFilePath);
        return $fileContent;
    }
}
