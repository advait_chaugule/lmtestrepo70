<?php
namespace App\Domains\Helper\Jobs;

use Lucid\Foundation\Job;

class RemoveSpecifiedElementsFromArrayJob extends Job
{
    private $data;
    private $dataKeys;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data = [], $dataKeys = [])
    {
        $this->data = $data;
        $this->dataKeys = $dataKeys;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->dataKeys as $key){
            if(array_key_exists($key, $this->data)) {
                unset($this->data[$key]);
            }
        }
        return $this->data;
    }
}
