<?php
namespace App\Domains\Helper\Jobs;

use Lucid\Foundation\Job;
use \Ramsey\Uuid\Uuid;

class CreateUniversalUniqueIdentifierHelperJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return Uuid::uuid4();
    }
}
