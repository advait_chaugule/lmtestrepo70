<?php
namespace App\Domains\Helper\Jobs;

use Lucid\Foundation\Job;

class CreateErrorMessageFromExceptionHelperJob extends Job
{
    private $exceptionObject;

    private $systemEnvironment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\Exception $exception)
    {
        $this->exceptionObject = $exception;
        $this->systemEnvironment = strtolower(env("APP_ENV", "dev"));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
             return ($this->systemEnvironment=="dev" || $this->systemEnvironment=="local") ?
                    "Line Number -> ".$this->exceptionObject->getLine()."\n".
                    "File Path-> ".$this->exceptionObject->getFile()."\n".
                    "Exception Message -> ".$this->exceptionObject->getMessage() : "Internal Error occurred.";
    }
}
