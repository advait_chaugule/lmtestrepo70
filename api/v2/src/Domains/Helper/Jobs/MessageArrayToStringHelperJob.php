<?php
namespace App\Domains\Helper\Jobs;

use Lucid\Foundation\Job;

class MessageArrayToStringHelperJob extends Job
{
    private $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->messages = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $messageString = "";
        foreach ($this->messages as $attrName=>$messages) {
            $messageString.="$attrName: ";
            foreach($messages as $key=>$message ) {
                $messageString.="#$key -> $message ";
            }
        }
        return trim($messageString);
    }
}
