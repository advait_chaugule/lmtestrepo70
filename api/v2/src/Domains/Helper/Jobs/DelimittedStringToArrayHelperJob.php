<?php
namespace App\Domains\Helper\Jobs;

use Lucid\Foundation\Job;

class DelimittedStringToArrayHelperJob extends Job
{
    private $delimittedString;
    private $separator;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($delimittedString = "", $separator = ",")
    {
        $this->delimittedString = $delimittedString;
        $this->separator = $separator;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return explode($this->separator, $this->delimittedString);
    }
}
