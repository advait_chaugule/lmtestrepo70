<?php
namespace App\Domains\Cftree\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetCFTreeJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->data = $inputs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $documentId = $this->data['document_id'];
        $cfTree = $itemRepo->getItemTreeofDocument($documentId);
        return $cfTree;
    }
}
