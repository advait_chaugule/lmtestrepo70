<?php
namespace App\Domains\Protip\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class GetProtipJsonInputValidator extends BaseValidator {

    protected $rules = [
        's3_bucket_name' => 'required|in:acmthost,acmtqa,acmtstag,scdprod'
    ];

     protected $messages = [

     ];
    
}