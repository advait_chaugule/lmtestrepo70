<?php
namespace App\Domains\Protip\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Protip\Validators\GetProtipJsonInputValidator as Validator;

class ValidateGetProtipJsonInputJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
