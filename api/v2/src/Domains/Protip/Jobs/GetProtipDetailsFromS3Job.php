<?php
namespace App\Domains\Protip\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\FileHelper;
use Illuminate\Support\Facades\Storage;

use App\Services\Api\Traits\ArrayHelper;

class GetProtipDetailsFromS3Job extends Job
{

    use ArrayHelper;

    private $S3Storage;
    private $bucketName;
    private $protipFolderName;
    private $protipFileName;
    private $protipDetails;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $bucketName)
    {
        $this->setBucketName($bucketName);
        $this->setS3BucketConfiguration();
        $this->setS3FileStorage();
        $this->setProtipFolderName();
        $this->setProtipFileName();
    }

    public function setBucketName(string $data) {
        $this->bucketName = $data;
    }

    public function getBucketName(): string {
        return $this->bucketName;
    }

    public function setS3BucketConfiguration() {
        config(['filesystems.disks.s3.bucket' => $this->getBucketName()]);
    }

    public function setS3FileStorage() {
        $this->S3Storage = Storage::disk('s3');
    }

    public function getS3FileStorage() {
        return $this->S3Storage;
    }

    public function setProtipFolderName() {
        $this->protipFolderName = config("protip.S3_FOLDER_NAME");
    }

    public function getProtipFolderName(): string {
        return $this->protipFolderName;
    }

    public function setProtipFileName() {
        $this->protipFileName = config("protip.FILE_NAME");
    }

    public function getProtipFileName(): string {
        return $this->protipFileName;
    }

    public function setProtipDetails(array $data) {
        $this->protipDetails = $data;
    }

    public function getProtipDetails(): array {
        return $this->protipDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->readAndSetProtipDetails();
        
        return $this->getProtipDetails();
    }

    private function readAndSetProtipDetails() {
        $protipDetails = [];
        $s3Disk = $this->getS3FileStorage();
        $protipFolderName = $this->getProtipFolderName();
        $protipFileName = $this->getProtipFileName();
        $pathToProtipJson = $protipFolderName . "/" . $protipFileName;
        if($s3Disk->exists($pathToProtipJson)===true) {
            $protipDetails = $this->convertJsonStringToArray($s3Disk->get($pathToProtipJson));
        }
        $this->setProtipDetails($protipDetails);
    }
}
