<?php
namespace App\Domains\Protip\Tests\Jobs;

use App\Domains\Protip\Jobs\GetProtipDetailsFromS3Job;
use Tests\TestCase;

class GetProtipDetailsFromS3JobTest extends TestCase
{
    public function test_BucketName()
    {
        $assets = new GetProtipDetailsFromS3Job('f30b9d7a-edf9-4f07-b510-eca225d6e9d8');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setBucketName($value);
        $this->assertEquals($value,$assets->getBucketName());
    }
}
