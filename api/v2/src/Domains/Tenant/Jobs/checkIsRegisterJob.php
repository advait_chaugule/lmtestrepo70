<?php
namespace App\Domains\Tenant\Jobs;

use Lucid\Foundation\Job;
use DB;

class checkIsRegisterJob extends Job
{
    private $emailId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailId)
    {
        
        $this->emailId = $emailId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->checkIsRegisterStatus($this->emailId);
    }

    private function checkIsRegisterStatus($emailId)
    { 
        
       $checkQuery = [];
       $checkQuery= DB::table('users')
                ->select('users.*')
                ->where('email',$emailId)
                ->where('is_register',0)
                ->where('is_active',0)
                ->where('is_deleted',0)
                ->get()
                ->toArray();  
      
         return  $checkQuery;      

    }
}
