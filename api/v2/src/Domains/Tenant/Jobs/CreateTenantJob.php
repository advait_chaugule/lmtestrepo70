<?php
namespace App\Domains\Tenant\Jobs;

use Lucid\Foundation\Job;

class CreateTenantJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        return $this->view('tenant.createtenant');

    }
}
