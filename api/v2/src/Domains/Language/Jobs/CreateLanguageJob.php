<?php
namespace App\Domains\Language\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;

class CreateLanguageJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LanguageRepositoryInterface $languageRepo)
    {
        //
        //dd($this->input);
        return $languageRepo->fillAndSave($this->input);
    }
}
