<?php
namespace App\Domains\Language\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LanguageRepositoryInterface;

class GetListOfLanguagesJob extends Job
{
    private $languageRepository;

    private $organizationIdentifier;
    private $languages;
    private $parsedLanguages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $organizationIdentifier)
    {
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    public function setOrganizationIdentifier(string $data) {
        $this->organizationIdentifier = $data;
    }

    public function getOrganizationIdentifier(): string {
        return $this->organizationIdentifier;
    }

    public function setLanguages($data) {
        $this->languages = $data;
    }

    public function getLanguages() {
        return $this->languages;
    }

    public function setParsedLanguages($data) {
        $this->parsedLanguages = $data;
    }

    public function getParsedLanguages() {
        return $this->parsedLanguages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        LanguageRepositoryInterface $languageRepository
    )
    {
        // set db repositories
        $this->languageRepository = $languageRepository;

        $this->fetchAndSetTenantSpecificLanguages();
        $this->parseAndSetLanguageList();
        return $this->getParsedLanguages();
    }

    private function fetchAndSetTenantSpecificLanguages() {
        $organizationIdentifier = $this->getOrganizationIdentifier();
        $conditionalClauseKeyValuePairs = [
            "organization_id" => $organizationIdentifier,
            "is_deleted" => 0
        ];
        $languages = $this->languageRepository->findByAttributes($conditionalClauseKeyValuePairs);
        $this->setLanguages($languages);
    }

    private function parseAndSetLanguageList() {
        $languages = $this->getLanguages();
        $parsedLanguages = [];
        foreach($languages as $language) {
            $languageName = $language->name ?: "";
            $languageShortcode = $language->short_code ?: "";
            if($languageShortcode!=="") {
                if($languageName!=="") {
                    $displayText = ucwords($languageName) . " " . "(" . $languageShortcode . ")";
                }
                else {
                    $displayText = $languageShortcode;
                }
                $parsedLanguages[] = [
                    "language_id"   =>  $language->language_id,
                    "short_code" => $languageShortcode,
                    "display_text" => $displayText
                ];
            }
        }
        $this->setParsedLanguages($parsedLanguages);
    }
}
