<?php
namespace App\Domains\Language\Tests\Jobs;

use App\Domains\Language\Jobs\GetListOfLanguagesJob;
use Tests\TestCase;

class GetListOfLanguagesJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $assets = new GetListOfLanguagesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_Languages()
    {
        $assets = new GetListOfLanguagesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setLanguages($value);
        $this->assertEquals($value,$assets->getLanguages());
    }

    public function test_ParsedLanguages()
    {
        $assets = new GetListOfLanguagesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setParsedLanguages($value);
        $this->assertEquals($value,$assets->getParsedLanguages());
    }
}
