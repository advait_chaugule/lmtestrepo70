<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\NodeType;
use Lucid\Foundation\Job;

class GetNodeTypeSourceIdByNodeTypeId extends Job
{
    private $nodeTypeId;
    private $organizationId;

    public function __construct($nodeTypeId,$organizationId){
        $this->nodeTypeId     = $nodeTypeId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $nodeSourceId = NodeType::select('source_node_type_id')
                  ->whereIn('node_type_id',$this->nodeTypeId)
                  ->where('organization_id',$this->organizationId)
                  ->where('is_deleted',0)
                  ->get();
        if($nodeSourceId){
            $nodeSourceId = $nodeSourceId->toArray();
            return $nodeSourceId;
        }
    }
}