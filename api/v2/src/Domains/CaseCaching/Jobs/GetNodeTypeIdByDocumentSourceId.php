<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\Item;
use Lucid\Foundation\Job;


class GetNodeTypeIdByDocumentSourceId extends Job
{
    private $documentId;
    private $organizationId;

    public function __construct($documentId,$organizationId)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $nodeIds = Item::select('node_type_id')
              ->where('document_id',$this->documentId)
              ->where('organization_id',$this->organizationId)
              ->where('is_deleted',0)
              ->get();
        if($nodeIds){
            $nodeIds = $nodeIds->toArray();
            return $nodeIds;
        }

    }
}