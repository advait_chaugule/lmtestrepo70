<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\Item;
use Lucid\Foundation\Job;

class GetConceptIdByItemId extends Job
{
    private $organizationId;
    private $itemId;
    public function __construct($itemId,$organizationId){
        $this->itemId    = $itemId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $conceptIds = Item::select('concept_id')
              ->whereIn('item_id',$this->itemId)
              ->where('organization_id',$this->organizationId)
              ->where('is_deleted',0)
              ->get();
        if($conceptIds) {
            $conceptIds = $conceptIds->toArray();
            return $conceptIds;
        }
    }
}