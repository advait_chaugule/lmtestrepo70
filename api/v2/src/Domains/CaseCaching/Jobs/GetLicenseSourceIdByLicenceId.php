<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\License;
use Lucid\Foundation\Job;

class GetLicenseSourceIdByLicenceId extends Job
{
    private $licenseId;
    private $organizationId;

    public function __construct($licenseId,$organizationId)
    {
        $this->licenseId      = $licenseId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $getLicenseSourceId = License::select('source_license_id')
                              ->whereIn('license_id',$this->licenseId )
                              ->where('organization_id',$this->organizationId)
                              ->where('is_deleted',0)
                              ->get();
        if($getLicenseSourceId) {
            $getLicenseSourceId = $getLicenseSourceId->toArray();
            return $getLicenseSourceId;
        }
    }
}