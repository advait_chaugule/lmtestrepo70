<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\Subject;
use Lucid\Foundation\Job;
use DB;
class GetSubjectIdByDocumentId extends Job
{
    private $documentId;
    private $organizationId;

    public function __construct($documentId,$organizationId)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $getSubjectId = $this->getSubjectId($this->documentId);
        if($getSubjectId && !empty($getSubjectId)) {
            $subjectId = $getSubjectId->subject_id;
            $getSubjectSource = Subject::select('source_subject_id')
                ->where('subject_id', $subjectId)
                ->where('organization_id', $this->organizationId)
                ->where('is_deleted', 0)
                ->get();
            if ($getSubjectSource) {
                $getSubjectSource = $getSubjectSource->toArray();
                return $getSubjectSource;
            }
        }
    }

    public function getSubjectId($documentId)
    {
        $subjectId = DB::table('document_subject')
                      ->select('subject_id')
                      ->where('document_id',$documentId)
                      ->first();
        return $subjectId;
    }

}