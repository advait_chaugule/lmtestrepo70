<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\TaxonomyPubStateHistory;
use Lucid\Foundation\Job;

class GetJsonFileNameJob extends Job
{
    private $documentId;
    private $organizationId;

    public function __construct($documentId,$organizationId)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $getFileName = TaxonomyPubStateHistory::select('taxonomy_json_file_s3')
            ->orderBy('created_at', 'desc')
            ->where('document_id', $this->documentId)
            ->where('organization_id', $this->organizationId)
            ->where('is_deleted', 0)
            ->first();
        return $getFileName;
    }
}
