<?php
namespace App\Domains\CaseCaching\Jobs;

use App\Data\Models\Item;
use Lucid\Foundation\Job;

class GetItemIdFromDocumentId extends Job
{
    private $documentId;
    private $organizationId;

    public function __construct($documentId,$organizationId)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $itemId = Item::select('item_id')
                 ->where('document_id',$this->documentId)
                 ->where('organization_id',$this->organizationId)
                 ->where('is_deleted',0)
                 ->get()
                 ->toArray();
        return $itemId;

    }

}