<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\Item;
use Lucid\Foundation\Job;

class GetLicenseIdByItemId extends Job
{
    private $itemId;
    private $organizationId;

    public function __construct($itemId,$organizationId)
    {
        $this->itemId     = $itemId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $licenceIds = Item::select('license_id')
            ->whereIn('item_id',$this->itemId)
            ->where('organization_id',$this->organizationId)
            ->where('is_deleted',0)
            ->get();
        if($licenceIds){
            $licenceIds= $licenceIds->toArray();
            return $licenceIds;
        }


    }
}