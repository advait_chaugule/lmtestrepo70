<?php


namespace App\Domains\CaseCaching\Jobs;


use App\Data\Models\ItemAssociation;
use Lucid\Foundation\Job;

class GetAssociationSourceId extends Job
{
    private $documentId;
    private $organizationId;

    public function __construct($documentId,$organizationId){
        $this->documentId    = $documentId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $getItemAssociation = ItemAssociation::select('source_item_association_id')
                              ->where('document_id',$this->documentId)
                              ->where('organization_id',$this->organizationId)
                              ->where('is_deleted',0)
                              ->get();
        if($getItemAssociation)
        {
            $getItemAssociation = $getItemAssociation->toArray();
            return $getItemAssociation;
        }
    }
}