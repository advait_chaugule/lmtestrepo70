<?php
namespace App\Domains\CaseCaching\Jobs;
use App\Data\Models\Concept;
use Lucid\Foundation\Job;

class GetConceptSourceIdByConceptId extends Job
{
    private $organizationId;
    private $conceptId;

    public function __construct($conceptId,$organizationId){
        $this->conceptId    = $conceptId;
        $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $conceptIds = Concept::select('source_concept_id')
                     ->whereIn('concept_id',$this->conceptId)
                     ->where('organization_id',$this->organizationId)
                     ->where('is_deleted',0)
                     ->get();
        if($conceptIds) {
            $conceptIds = $conceptIds->toArray();
            return $conceptIds;
        }

    }
}