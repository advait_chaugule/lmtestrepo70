<?php
namespace App\Domains\CaseCaching\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CFItemEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $itemEventData;

    public function __construct($itemEventData){
        $this->itemEventData = $itemEventData;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }
}