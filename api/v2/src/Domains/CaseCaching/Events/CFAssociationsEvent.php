<?php
namespace App\Domains\CaseCaching\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CFAssociationsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $associationEventData;

    public function __construct($associationEventData){
        $this->associationEventData = $associationEventData;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }
}