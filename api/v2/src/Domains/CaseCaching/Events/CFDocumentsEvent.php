<?php

namespace App\Domains\CaseCaching\Events;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CFDocumentsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $docEventData;

    public function __construct($docEventData){
        $this->docEventData = $docEventData;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }
}