<?php
namespace App\Domains\CaseCaching\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CustomMetadataEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $customEventData;

    public function __construct($customEventData){
        $this->customEventData = $customEventData;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn(){
        return new PrivateChannel('channel-name');
    }
}