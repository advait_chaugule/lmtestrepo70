<?php
namespace App\Domains\CaseCaching\Listeners;


use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\CustomMetadataEvent as Event;
use App\Domains\CaseStandard\Jobs\CustomMetadataJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class CustomMetadataEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    //customEventData
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $eventData1 = $event->customEventData;
            $this->cacheCustomData($eventData1);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheCustomData($data)
    {
        $eventType = isset($data['event_type']) ? $data['event_type'] : '';
        $domainUrl = $data['domain_name'];
        switch ($eventType) {
            case "CUSTOMDATA":

                $documentId        = $data['document_id'];
                $organizationId    = $data['organizationId'];
                $getDocumentSource = $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                $documentSourceId  = $getDocumentSource->source_document_id;
                $getOrgCode        = $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode           = $getOrgCode->org_code;
                $metadata          = $this->dispatch(new CustomMetadataJob($documentSourceId,$orgCode,$documentId,$domainUrl));
                $uuidName          =  $data['uuid'];
                $s3FileName        =  $uuidName.'_custom.json';
                $s3Bucket          =  config("event_activity")["Taxonomy_Folder"];
                $destinationS3     = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/$s3FileName";
                Storage::disk('s3')->put($destinationS3,json_encode($metadata));
                $customKey         = ['identifier' =>$documentSourceId,'prefix' => 'customData'];
                $this->dispatch(new DeleteFromCacheJob($customKey));
                $setStatus = $this->dispatch(new SetToCacheJob($metadata,$customKey));

                if(is_array($setStatus) && $setStatus['status'] == "error") {
                    // handle failure, data already exists , delete and re-add all
                    $this->dispatch(new DeleteFromCacheJob($customKey));
                    $this->dispatch(new SetToCacheJob($metadata,$customKey));
                }
    }

}
}