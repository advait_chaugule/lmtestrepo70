<?php


namespace App\Domains\CaseCaching\Listeners;


use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\CFItemEvent as Event;
use App\Domains\CaseCaching\Jobs\GetItemIdFromDocumentId;
use App\Domains\CaseStandard\Jobs\GetCaseFrameworkItemJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class CFItemEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
    }

    public function handle(Event $event)
    {
        try {
            $eventData2 = $event->itemEventData;
            $this->cacheCFItem($eventData2);
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheCFItem($data)
    {
        $itemEventType = isset($data['event_type']) ? $data['event_type'] : '';
        $domainUrl     = $data['domain_name'];
        switch($itemEventType) {
            case "CFITEM":

                $documentId        = $data['document_id'];
                $organizationId    = $data['organizationId'];
                $getOrgCode        = $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode           = $getOrgCode->org_code;
                $getDocumentSource  = $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                $documentSourceId   = $getDocumentSource->source_document_id;
                $getItemIds = $this->dispatch(new GetItemIdFromDocumentId($documentSourceId,$organizationId));
                foreach ($getItemIds as $getItemIdsK => $getItemIdsV) {
                    $itemId   = $getItemIdsV['item_id'];
                    $itemData = $this->dispatch(new GetCaseFrameworkItemJob($itemId,$orgCode,$domainUrl));
                    $itemKey  = ['identifier' =>$itemId,'prefix' => 'cfItem'];
                    $this->dispatch(new DeleteFromCacheJob($itemKey));
                    $setStatus = $this->dispatch(new SetToCacheJob($itemData,$itemKey));
                    if (is_array($setStatus) && $setStatus['status'] == "error") {
                        // handle failure, data already exists , delete and re-add all
                        $this->dispatch(new DeleteFromCacheJob($itemKey));
                        $this->dispatch(new SetToCacheJob($itemData,$itemKey));
                    }
                }

    }
}
}