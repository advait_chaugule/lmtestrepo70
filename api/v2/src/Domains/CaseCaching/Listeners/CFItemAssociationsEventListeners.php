<?php
namespace App\Domains\CaseCaching\Listeners;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\CFItemAssociationsEvent as Event;
use App\Domains\CaseCaching\Jobs\GetItemSourceIdFromDocumentId;
use App\Domains\Casestandard\Jobs\GetCaseFrameworkItemAssociationJob;
use App\Domains\CaseStandard\Jobs\GetCaseFrameworkItemJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class CFItemAssociationsEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $itemAssociationEventData = $event->itemAssociationEventData;
            $this->cacheItemAssociationEvent($itemAssociationEventData);
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheItemAssociationEvent($data)
    {
        $licenceEventType = isset($data['event_type']) ? $data['event_type'] : '';
        $domainUrl = $data['domain_name'];
        switch ($licenceEventType) {
            case "ITEMASSOCIATION":
                $documentId = $data['document_id'];
                $organizationId = $data['organizationId'];

                $getOrgCode = $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode = $getOrgCode->org_code;
                $getItemSource = $this->dispatch(new GetItemSourceIdFromDocumentId($documentId, $organizationId));
                foreach ($getItemSource as $getItemSourceK => $getItemSourceV) {
                    $itemSourceId = $getItemSourceV['source_item_id'];
                    $caseFrameworkItem = ['CFItem' => $this->dispatch(new GetCaseFrameworkItemJob($itemSourceId, $orgCode,$domainUrl))];
                    $response = $this->dispatch(new GetCaseFrameworkItemAssociationJob($itemSourceId, $orgCode,$domainUrl));
                    if (!empty($caseFrameworkItem) && !empty($response)) {
                        $response = $caseFrameworkItem + $response;
                    }
                    $cfItemAssociationKey = ['identifier' => $itemSourceId, 'prefix' => 'cfItemAssociation'];
                    $this->dispatch(new DeleteFromCacheJob($cfItemAssociationKey));
                    $setItemAssoc = $this->dispatch(new SetToCacheJob($response, $cfItemAssociationKey));
                    if (is_array($setItemAssoc) && $setItemAssoc['status'] == "error") {
                        // handle failure, data already exists , delete and re-add all
                        $this->dispatch(new DeleteFromCacheJob($cfItemAssociationKey));
                        $this->dispatch(new SetToCacheJob($response, $cfItemAssociationKey));
                    }
                }
    }

}
}