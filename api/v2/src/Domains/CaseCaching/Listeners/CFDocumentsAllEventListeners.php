<?php
namespace App\Domains\CaseCaching\Listeners;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\CaseCaching\Events\CFDocumentsAllEvent as Event;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseStandard\Jobs\GetAllCaseFrameworkDocumentsJob;

class CFDocumentsAllEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $eventData1 = $event->eventData;
            $this->cacheCFDocAll($eventData1);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheCFDocAll($data)
    {
        $eventType = isset($data['event_type']) ? $data['event_type'] : '';
                $organizationId = $data['organizationId'];
                $domainUrl      = $data['domain_name'];
                // Get CFDocuments All data
                $getOrgCode     =  $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode        =  $getOrgCode->org_code;
                $cfDocumentsAll = $this->dispatch(new GetAllCaseFrameworkDocumentsJob($orgCode,$domainUrl));
                if(!empty($cfDocumentsAll) && count($cfDocumentsAll) > 0) {
                    // Prepare key for caching
                    $keyInfoData = ['identifier' => $organizationId, 'prefix' => 'cfdocs_'];
                    $this->dispatch(new DeleteFromCacheJob($keyInfoData));
                    $setStatus = $this->dispatch(new SetToCacheJob($cfDocumentsAll,$keyInfoData));

                    if (is_array($setStatus) && $setStatus['status'] == "error") {
                        // handle failure, data already exists , delete and re-add all
                        $this->dispatch(new DeleteFromCacheJob($keyInfoData));
                        $this->dispatch(new SetToCacheJob($cfDocumentsAll, $keyInfoData));
                    }
                }
    }
}