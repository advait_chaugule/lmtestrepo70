<?php


namespace App\Domains\CaseCaching\Listeners;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Foundation\ReportListenerHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\CaseCaching\Events\CFDocumentsEvent as Event;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseStandard\Jobs\GetCaseFrameworkDocumentJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
class CFDocumentsEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
    }

    public function handle(Event $event)
    {
        try {
            $eventData1 = $event->docEventData;
            $this->cacheCFDocuments($eventData1);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheCFDocuments($data)
    {
        $eventType = isset($data['event_type']) ? $data['event_type'] : '';
        $domainUrl = $data['domain_name'];
                $documentId        = $data['document_id'];
                $organizationId    = $data['organizationId'];
                $getOrgCode        = $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode           = $getOrgCode->org_code;
                $getDocumentSource = $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                $documentSourceId  = $getDocumentSource->source_document_id;
                $cfDocuments       = $this->dispatch(new GetCaseFrameworkDocumentJob($documentSourceId,$orgCode,$domainUrl));
                $keyInfoData       = ['identifier' =>$documentSourceId,'prefix' =>'cfdocuments'];

                $this->dispatch(new DeleteFromCacheJob($keyInfoData));
                $setStatus = $this->dispatch(new SetToCacheJob($cfDocuments,$keyInfoData));

                if (is_array($setStatus) && $setStatus['status'] == "error") {
                    // handle failure, data already exists , delete and re-add all
                    $status = $this->dispatch(new DeleteFromCacheJob($keyInfoData));
                    $status = $this->dispatch(new SetToCacheJob($cfDocuments,$keyInfoData));
                }
    }
}