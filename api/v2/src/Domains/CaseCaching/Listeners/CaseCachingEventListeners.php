<?php
namespace App\Domains\CaseCaching\Listeners;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\CaseCaching\Events\CaseCachingEvent as Event;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\CaseStandard\Jobs\GetCasePackageWithActiveMetadataJob;
use Illuminate\Support\Facades\Storage;
use App\Domains\CaseStandard\Jobs\GetLastUpdatedTimeJob;
class CaseCachingEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $eventData = $event->eventData;
            $this->cacheActionsEvent($eventData);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheActionsEvent($data)
    {
        $eventType = isset($data['event_type'])?$data['event_type']:'';
        $domainUrl = $data['domain_name'];
        switch ($eventType) {
            case "CFPACKAGE":

                $documentIdentifier     =  $data['document_id'];
                $organizationId         =  $data['organizationId'];
                $requestingUserDetail   =  $data['requestUserDetails'];
                $uuidName               =  $data['uuid'];
                $versionTag             =  $data['versionTag'];

                $getDocumentSource      =  $this->dispatch(new GetDocumentSourceIdJob($documentIdentifier,$organizationId));
                $documentSourceId       =  $getDocumentSource->source_document_id;
                $getOrgCode             =  $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode                =  $getOrgCode->org_code;
                if(is_null($documentSourceId))
                    break;
                $additionalParams       = ['versioning'=>true, 'versionTag'=>$versionTag, 'publishedBy'=>$requestingUserDetail['user_id']];
                $getPackageData  =  $this->dispatch(new GetCasePackageWithActiveMetadataJob($documentSourceId,[],$orgCode,0,'',$domainUrl,$additionalParams)); // $requestingUserDetail removed since incorrect setSavedDocument was returning null
                $s3FileName      =  $uuidName.'.json';
                $s3Bucket        =  config("event_activity")["Taxonomy_Folder"];
                $destinationS3   = "{$s3Bucket["MAIN_ARCHIVE_FOLDER"]}/$s3FileName";
                Storage::disk('s3')->put($destinationS3,json_encode($getPackageData));
                $getLastUpdatedTime =  $this->dispatch(new GetLastUpdatedTimeJob($documentSourceId,$organizationId));
                $updatedAt = $getLastUpdatedTime->updated_at;
                $document_id = $getLastUpdatedTime->document_id;
                $keyInfo   = ['identifier' => $documentSourceId.'_'.$orgCode.'_'.$document_id, 'prefix' =>'cfpackages'];
                $delStatus = $this->dispatch(new DeleteFromCacheJob($keyInfo));
                $status    = $this->dispatch(new SetToCacheJob($getPackageData,$keyInfo));

                if(is_array($status) && $status['status']=="error"){
                    // handle failure, data already exists , delete and re-add all
                    $status = $this->dispatch(new DeleteFromCacheJob($keyInfo));
                    $status = $this->dispatch(new SetToCacheJob($getPackageData,$keyInfo));
                }
    }
}
}