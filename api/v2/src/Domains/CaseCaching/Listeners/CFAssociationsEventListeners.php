<?php
namespace App\Domains\CaseCaching\Listeners;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\CFAssociationsEvent as Event;
use App\Domains\CaseCaching\Jobs\GetAssociationSourceId;
use App\Domains\Casestandard\Jobs\GetCaseFrameworkAssociationJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class CFAssociationsEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
{
    try {
        $associationData = $event->associationEventData;
        $this->cacheAssociationEvent($associationData);
    }
    catch(\Exception $exception) {
        $this->logErrors($exception);
        // cleanup folder and archive from local storage
        //$this->cleanLocalStorage();
    }
}

    public function cacheAssociationEvent($data)
    {
        $eventType = isset($data['event_type'])?$data['event_type']:'';
        $domainUrl = $data['domain_name'];
        switch ($eventType) {
            case "ASSOCIATION":
                $documentId = $data['document_id'];
                $organizationId = $data['organizationId'];
                $getOrgCode = $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode = $getOrgCode->org_code;
                $getAssociation = $this->dispatch(new GetAssociationSourceId($documentId, $organizationId));
                foreach ($getAssociation as $getAssociationK => $getAssociationV) {
                    $sourceAssociationId = $getAssociationV['source_item_association_id'];
                    $caseFrameworkDocument = $this->dispatch(new GetCaseFrameworkAssociationJob($sourceAssociationId, $orgCode,$domainUrl));

                    $keyData = ['identifier' => $sourceAssociationId, 'prefix' => 'cfAssociation'];
                    $this->dispatch(new DeleteFromCacheJob($keyData));
                    $status = $this->dispatch(new SetToCacheJob($caseFrameworkDocument, $keyData));

                    if (is_array($status) && $status['status'] == "error") {
                        // handle failure, data already exists , delete and re-add all
                        $this->dispatch(new DeleteFromCacheJob($keyData));
                        $this->dispatch(new SetToCacheJob($caseFrameworkDocument, $keyData));
                    }
                }
    }
}
}