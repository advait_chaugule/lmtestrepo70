<?php
namespace App\Domains\CaseCaching\Listeners;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\CFSubjectEvent as Event;
use App\Domains\CaseCaching\Jobs\GetSubjectIdByDocumentId;
use App\Domains\Casestandard\Jobs\GetCaseFrameworkSubjectJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class CFSubjectEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
    }

    public function handle(Event $event)
    {
        try {
            $subjectEventData = $event->subjectEventData;
            $this->cacheSubjectData($subjectEventData);
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
        }
    }

    public function cacheSubjectData($data)
    {
        $subjectEventType = isset($data['event_type']) ? $data['event_type'] : '';
                $documentId           = $data['document_id'];
                $organizationId       = $data['organizationId'];
                $domainUrl            = $data['domain_name'];
                $getOrgCode           =  $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode              =  $getOrgCode->org_code;
                $getSubjectSourceId   = $this->dispatch(new GetSubjectIdByDocumentId($documentId,$organizationId));
                if(!empty($getSubjectSourceId)) {
                    foreach ($getSubjectSourceId as $getSubjectSourceIdK => $getSubjectSourceIdV) {
                        $subjectSourceId = $getSubjectSourceIdV['source_subject_id'];
                        $caseFrameworkSubject = $this->dispatch(new GetCaseFrameworkSubjectJob($subjectSourceId, $orgCode,$domainUrl));
                        $cfSubjectKey = ['identifier' => $getSubjectSourceId, 'prefix' => 'cfSubject'];
                        $this->dispatch(new DeleteFromCacheJob($cfSubjectKey));
                        $setLicense = $this->dispatch(new SetToCacheJob($caseFrameworkSubject, $cfSubjectKey));
                        if (is_array($setLicense) && $setLicense['status'] == "error") {
                            // handle failure, data already exists , delete and re-add all
                            $this->dispatch(new DeleteFromCacheJob($cfSubjectKey));
                            $this->dispatch(new SetToCacheJob($caseFrameworkSubject, $cfSubjectKey));
                        }
                    }
                }
    }
}