<?php
namespace App\Domains\CaseCaching\Listeners;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\ItemTypeEvent as Event;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\CaseCaching\Jobs\GetNodeTypeIdByDocumentSourceId;
use App\Domains\CaseCaching\Jobs\GetNodeTypeSourceIdByNodeTypeId;
use App\Domains\Casestandard\Jobs\GetCaseFrameworkItemTypeJob;

class ItemTypeEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct(){
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $itemTypeEvent = $event->itemTypeEventData;
            $this->cacheItemType($itemTypeEvent);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheItemType($data)
    {
        $itemTypeEventType = isset($data['event_type']) ? $data['event_type'] : '';
        $domainUrl         = $data['domain_name'];
        switch ($itemTypeEventType) {
            case "ITEMTYPE":
                $documentId        = $data['document_id'];
                $organizationId    = $data['organizationId'];
                $getDocumentSource = $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                $documentSourceId  = $getDocumentSource->source_document_id;
                // Get Node type ID
                $getNodeTypeId     = $this->dispatch(new GetNodeTypeIdByDocumentSourceId($documentSourceId,$organizationId));
                $nodeTypeId        = array_column($getNodeTypeId,'node_type_id');
                $getNodeSourceId   = $this->dispatch(new GetNodeTypeSourceIdByNodeTypeId($nodeTypeId,$organizationId));
                // Get Org Code by Organization Id
                $getOrgCode        = $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode           = $getOrgCode->org_code;
                //GetCaseFrameworkItemTypeJob
                foreach($getNodeSourceId as $getNodeSourceIdK=>$getNodeSourceIdV)
                {
                    $nodeTypeSourceId  = $getNodeSourceIdV['source_node_type_id'];
                    $ItemTypeData      = $this->dispatch(new GetCaseFrameworkItemTypeJob($nodeTypeSourceId,$orgCode,$domainUrl));
                    $cfItemTypeKey     = ['identifier' =>$nodeTypeSourceId,'prefix' => 'cfItemType'];
                    $this->dispatch(new DeleteFromCacheJob($cfItemTypeKey));
                    $setStatusItemType = $this->dispatch(new SetToCacheJob($ItemTypeData,$cfItemTypeKey));
                    if(is_array($setStatusItemType) && $setStatusItemType['status'] == "error") {
                        // handle failure, data already exists , delete and re-add all
                        $this->dispatch(new DeleteFromCacheJob($cfItemTypeKey));
                        $this->dispatch(new SetToCacheJob($ItemTypeData,$cfItemTypeKey));
                    }
                }
        }
    }
}