<?php
namespace App\Domains\CaseCaching\Listeners;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\GetFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\UnPublishTaxonomyEvent as Event;
use App\Domains\CaseStandard\Jobs\GetAllCaseFrameworkDocumentsJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use App\Domains\CaseStandard\Jobs\GetLastUpdatedTimeJob;
use Illuminate\Support\Facades\Storage;
use DB;

class UnPublishTaxonomyEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct(){
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $unPublishTypeEvent = $event->unPublishData;
            $this->upPublishTaxonomy($unPublishTypeEvent);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function upPublishTaxonomy($data)
    {
        $itemTypeEventType = isset($data['event_type']) ? $data['event_type'] : '';
       
                $organizationId     = $data['organizationId'];
                $documentId         = $data['document_id'];
                $requestUrl         = $data['domain_name'];
                $getOrgCode         =  $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode            =  $getOrgCode->org_code;
                $getDocumentSource  =  $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                // CF Document All
                $docKeyInfo       = ['identifier' => $organizationId, 'prefix' =>'cfdocs_'];
                $this->dispatch(new DeleteFromCacheJob($docKeyInfo));
                $data = $this->dispatch(new GetAllCaseFrameworkDocumentsJob($orgCode,$requestUrl));
                if (!empty($data) && count($data) > 0) {
                    $this->dispatch(new SetToCacheJob($data,$docKeyInfo));
                }
               /*CF Document All Ends Here */
                $this->dispatch(new DeleteFromCacheJob($docKeyInfo));
                $documentSourceId       =  $getDocumentSource->source_document_id;
                $getLastUpdatedTime =  $this->dispatch(new GetLastUpdatedTimeJob($documentSourceId,$organizationId));
                $document_id = $getLastUpdatedTime->document_id;
                
                $cfKeyInfo        = ['identifier' => $documentSourceId.'_'.$orgCode.'_'.$document_id, 'prefix' =>'cfpackages'];
                $this->dispatch(new DeleteFromCacheJob($cfKeyInfo));
                $delKeyInfo       = ['identifier'=>$organizationId, 'prefix'=>'docs'];
                $this->dispatch(new DeleteFromCacheJob($delKeyInfo));
                $cacheKey         = ['identifier' =>$documentSourceId,'prefix' =>'cfdocuments'];
                $this->dispatch(new DeleteFromCacheJob($cacheKey));

                $taxonomyHierarchy = ['identifier' =>$documentSourceId,'prefix' =>'doc_hierarchy'];
               
                $s3FileNameHierarchy      =  $documentId.'-hierarchy'.'.json';                                          // set filename e.g. ba6f3ef5-0ca8-4e74-877d-0348358adc78-Hierarchy.json
                $s3BucketHierarchy        =  config("event_activity")["HIERARCHY_FOLDER"];                              // event activity
                $destinationS3Hierarchy   = "{$s3BucketHierarchy["MAIN_ARCHIVE_FOLDER"]}/$s3FileNameHierarchy";         // s3 destination
                $s3Hierarchy              = Storage::disk('s3')->delete($destinationS3Hierarchy,$s3FileNameHierarchy);  // Deleting file from s3
                              
                $taxonomyDetails = ['identifier' =>$documentSourceId,'prefix' =>'doc_details'];
                
                $s3FileNameDetails      =  $documentId.'-details'.'.json';                                              // set filename e.g. ba6f3ef5-0ca8-4e74-877d-0348358adc78-details.json
                $s3BucketDetails        =  config("event_activity")["HIERARCHY_FOLDER"];                                // event activity
                $destinationS3Details   = "{$s3BucketDetails["MAIN_ARCHIVE_FOLDER"]}/$s3FileNameDetails";               // s3 destination
                $s3Details              = Storage::disk('s3')->delete($destinationS3Details,$s3FileNameDetails);        // Deleting file from s3

                $existsHeirarchy = Storage::disk('s3')->exists($destinationS3Hierarchy);               // check if the taxonomyHeirarchy file exists in s3
                $existsDetails = Storage::disk('s3')->exists($destinationS3Details);                   // check if the taxonomyDetail file exists in s3
                       
                //if file is dose not exists in s3(deleted from s3), then delete entry from DB
                if(!$existsHeirarchy && !$existsDetails){
                                
                    $updateDetail = [
                                    'treehierarchy_json_file_s3' => null,
                                    'treedetails_json_file_s3'   => null
                                ];

                    $query = DB::table('taxonomy_pubstate_history')                   // DB query
                                ->where('document_id',$documentId)
                                ->where('organization_id',$organizationId)
                                ->update($updateDetail);
                }

                $customMetadata = ['identifier' =>$documentSourceId,'prefix' =>'customData'];
                $this->dispatch(new DeleteFromCacheJob($customMetadata));
            }
    
}