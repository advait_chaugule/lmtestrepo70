<?php
namespace App\Domains\CaseCaching\Listeners;
use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\ConceptEvent as Event;
use App\Domains\CaseCaching\Jobs\GetConceptIdByItemId;
use App\Domains\CaseCaching\Jobs\GetConceptSourceIdByConceptId;
use App\Domains\CaseCaching\Jobs\GetItemIdFromDocumentId;
use App\Domains\Casestandard\Jobs\GetCaseFrameworkConceptsJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class ConceptEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();
    }

    public function handle(Event $event)
    {
        try {
            $conceptData = $event->conceptEventData;
            $this->cacheCFConcept($conceptData);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception); // Commented due to log space full
            // cleanup folder and archive from local storage
            //$this->cleanLocalStorage();
        }
    }

    public function cacheCFConcept($data)
    {
        $conceptEventType = isset($data['event_type']) ? $data['event_type'] : '';
        $domainUrl        = $data['domain_name'];
        switch($conceptEventType) {
            case "CONCEPT":
                $documentId         = $data['document_id'];
                $organizationId     = $data['organizationId'];
                $getDocumentSource  = $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                $documentSourceId   = $getDocumentSource->source_document_id;
                $getItemIds         = $this->dispatch(new GetItemIdFromDocumentId($documentSourceId,$organizationId));
                $itemIds            = array_column($getItemIds,'item_id');
                $getConceptIds      = $this->dispatch(new GetConceptIdByItemId($itemIds,$organizationId));
                $conceptIds         = array_column($getConceptIds,'concept_id');
                $getSourceConceptId = $this->dispatch(new GetConceptSourceIdByConceptId($conceptIds,$organizationId));
                $getOrgCode         =  $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode            =  $getOrgCode->org_code;
                foreach ($getSourceConceptId as $getSourceConceptIdK => $getSourceConceptIdV) {
                    $sourceConceptId   = $getSourceConceptIdV['source_concept_id'];
                    $cfConceptData     = $this->dispatch(new GetCaseFrameworkConceptsJob($sourceConceptId,$orgCode,$domainUrl));
                    $cfConceptKey      = ['identifier' =>$sourceConceptId,'prefix' => 'cfConcept'];
                    $this->dispatch(new DeleteFromCacheJob($cfConceptKey));
                    $setConcept        = $this->dispatch(new SetToCacheJob($cfConceptData,$cfConceptKey));
                    if (is_array($setConcept) && $setConcept['status'] == "error") {
                        // handle failure, data already exists , delete and re-add all
                        $status = $this->dispatch(new DeleteFromCacheJob($cfConceptKey));
                        $status = $this->dispatch(new SetToCacheJob($cfConceptData,$cfConceptKey));
                    }
                }
    }
}
}