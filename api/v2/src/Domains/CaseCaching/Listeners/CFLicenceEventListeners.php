<?php
namespace App\Domains\CaseCaching\Listeners;

use App\Domains\Caching\Jobs\DeleteFromCacheJob;
use App\Domains\Caching\Jobs\SetToCacheJob;
use App\Domains\CaseCaching\Events\CFLicenceEvent as Event;
use App\Domains\CaseCaching\Jobs\GetItemIdFromDocumentId;
use App\Domains\CaseCaching\Jobs\GetLicenseIdByItemId;
use App\Domains\CaseCaching\Jobs\GetLicenseSourceIdByLicenceId;
use App\Domains\Casestandard\Jobs\GetCaseFrameworkLicenseJob;
use App\Domains\Document\Jobs\GetDocumentSourceIdJob;
use App\Domains\Organization\Jobs\GetOrgCodeByOrgId;
use App\Foundation\ReportListenerHandler;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class CFLicenceEventListeners extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue,UuidHelperTrait,DispatchesJobs;
    public function __construct()
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

    }

    public function handle(Event $event)
    {
        try {
            $licenceEventData = $event->licenceEventData;
            $this->cacheLicenceData($licenceEventData);
        }
        catch(\Exception $exception) {
            //$this->logErrors($exception);
        }
    }

    public function cacheLicenceData($data)
    {
        $licenceEventType = isset($data['event_type']) ? $data['event_type'] : '';
                $documentId        = $data['document_id'];
                $organizationId    = $data['organizationId'];
                $domainUrl         = $data['domain_name'];

                $getOrgCode        =  $this->dispatch(new GetOrgCodeByOrgId($organizationId));
                $orgCode           =  $getOrgCode->org_code;

                $getDocumentSource = $this->dispatch(new GetDocumentSourceIdJob($documentId,$organizationId));
                $documentSourceId  = $getDocumentSource->source_document_id;

                $getItemIds        = $this->dispatch(new GetItemIdFromDocumentId($documentSourceId,$organizationId));
                $itemIds           = array_column($getItemIds,'item_id');

                $getLicenceIds     = $this->dispatch(new GetLicenseIdByItemId($itemIds,$organizationId));
                $licenseIds        = array_column($getLicenceIds,'license_id');

                $getSourceLicenseIds = $this->dispatch(new GetLicenseSourceIdByLicenceId($licenseIds,$organizationId));
                if($getSourceLicenseIds) {
                    foreach ($getSourceLicenseIds as $getSourceLicenseIdsK => $getSourceLicenseIdsV) {
                        $licenseSourceId = $getSourceLicenseIdsV['source_license_id'];
                        $caseFrameworkLicense = $this->dispatch(new GetCaseFrameworkLicenseJob($licenseSourceId, $orgCode, $domainUrl));

                        $cfLicenceKey = ['identifier' => $licenseSourceId, 'prefix' => 'cfLicence'];
                        $this->dispatch(new DeleteFromCacheJob($cfLicenceKey));
                        $setLicense = $this->dispatch(new SetToCacheJob($caseFrameworkLicense, $cfLicenceKey));
                        if (is_array($setLicense) && $setLicense['status'] == "error") {
                            // handle failure, data already exists , delete and re-add all
                            $this->dispatch(new DeleteFromCacheJob($cfLicenceKey));
                            $this->dispatch(new SetToCacheJob($caseFrameworkLicense, $cfLicenceKey));
                        }
                    }
                }
    }
}