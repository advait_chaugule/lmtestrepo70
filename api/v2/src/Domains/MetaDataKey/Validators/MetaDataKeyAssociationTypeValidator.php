<?php
namespace App\Domains\MetaDataKey\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class MetaDataKeyAssociationTypeValidator extends BaseValidator {

    protected $rules = [
        'type' => 'required|in:ls_document,child_item'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "in" => ":attribute must be of type ls_document, child_item."
     ];
    
}