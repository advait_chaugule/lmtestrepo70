<?php
namespace App\Domains\MetaDataKey\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\MetaDataKeyRepositoryInterface;

class ListMetaDataKeysJob extends Job
{

    private $key_association_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param)
    {
        $this->key_association_type = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetaDataKeyRepositoryInterface $metaDataKeyRepo)
    {
        $data = $metaDataKeyRepo->findByAttributes([ 'key_association_type' => $this->key_association_type ]);
        return $data->count() > 0 ? $data : [];
    }
}
