<?php
namespace App\Domains\MetaDataKey\Jobs;

use Lucid\Foundation\Job;

use App\Domains\MetaDataKey\Validators\MetaDataKeyAssociationTypeValidator;

class ValidateMetaDataKeyAssociationTypeJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetaDataKeyAssociationTypeValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
