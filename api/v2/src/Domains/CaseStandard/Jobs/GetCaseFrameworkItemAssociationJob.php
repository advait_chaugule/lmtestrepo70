<?php
namespace App\Domains\Casestandard\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\Document; 
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Data\Models\Organization;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use Illuminate\Support\Facades\DB;

class GetCaseFrameworkItemAssociationJob extends Job
{
    use CaseFrameworkTrait, DateHelpersTrait;

    /*** declare private attributes ****/
    private $identifier;
    private $itemDetail;
    private $itemAssociationDetail;

    private $itemRepository;
    private $itemAssociationRepository;

    private $documentUri;
    private $originNodeURIEntity;
    private $destinationNodeURIEntity;
    private $associationGroupingURIEntity;

    private $nodeTypeRepository;
    private $metadataList;
    private $organizationCode;
    private $orgCode;
    private $domainName;

    /**
     * GetCaseFrameworkItemAssociationJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl)
    {
        //Set identifier
        $this->setIdentifier($identifier);
        $this->orgCode = $orgCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo, ItemAssociationRepositoryInterface $itemAssociationRepo, NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        // set the item and itemAssociation and nodetype repository
        $this->itemRepository               =   $itemRepo;        
        $this->itemAssociationRepository    =   $itemAssociationRepo;
        $this->nodeTypeRepository           =   $nodeTypeRepository;
        // get all relevant associaton data for the ITEM
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId = $this->itemRepository->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }
        $itemAssociationDetails = $this->fetchItemAssociationDetailsFromRepository($organizationId);
        if(!empty($itemAssociationDetails))
        {
            // set item association details globally
            $this->setItemAssociationDetails($itemAssociationDetails);
            // transform to and set the CFItemAssociation Standard Structure
            return $this->transFormToCaseStandardStructure();
        }
        return false;
    }

    /**
     * Declaratin of Getters and Setters Method 
     */
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }

     public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setItemAssociationDetails($data){
        $this->itemAssociationDetail = $data;
    }

    public function getItemAssociationDetails(){
        return $this->itemAssociationDetail;
    }

    public function setDocumenturi($documentUri){
        $this->documentUri = $documentUri;
    }

    public function getDocumentUri(){
        return $this->documentUri;
    }

    public function setOriginNodeUri($originNodeURIEntity){
        $this->originNodeURIEntity = $originNodeURIEntity;
    }

    public function getOriginNodeUri(){
        return $this->originNodeURIEntity;
    }

    public function setDestinationNodeUri($destinationNodeURIEntity){
        $this->destinationNodeURIEntity = $destinationNodeURIEntity;
    }

    public function getDestinationNodeUri(){
        return $this->destinationNodeURIEntity;
    }

    public function setAssociationGroupingEntity($CFAssociationGroupingURIEntity){
        $this->associationGroupingURIEntity = $CFAssociationGroupingURIEntity;
    }

    public function getAssociationGroupingEntity(){
        return $this->associationGroupingURIEntity;
    }

    public function setMetadataList($data){
        $this->metadataList = $data;
    }

    public function getMetadataList(){
        return $this->metadataList;
    }

    private function parseMetadata() {
        $metadataList   =   $this->getMetadataList();
        $arrayOfInternalName=[];
        foreach($metadataList->metadata as $metadata) {
            $arrayOfInternalName[]  =   $metadata->internal_name;
        }

        return  $arrayOfInternalName;
    }

    private function transFormToCaseStandardStructure() {
        // transform and set CFItemAssociation
        return $this->transformAndSetItemAssociation();
    }

    /***
     * @param $organizationId
     * @return mixed
     * Fetch Association details for the selected item
     */
    private function fetchItemAssociationDetailsFromRepository($organizationId){
        $itemIdentifier = $this->getIdentifier();

        $returnCollection = false;
        $fieldsToReturn = ["*"];
        if($organizationId){
            $dataFetchingConditionalClause = ["source_item_id" => $itemIdentifier,"organization_id"=>$organizationId,"is_deleted" => 0];
        }else{
            $dataFetchingConditionalClause = ["source_item_id" => $itemIdentifier, "is_deleted" => 0];
        }

        
        $itemDetails =    $this->itemRepository->findByAttributesWithSpecifiedFields(
                                $returnCollection, 
                                $fieldsToReturn, 
                                $dataFetchingConditionalClause
                            );

        if(!empty($itemDetails)) {
            $associationDetail = $itemDetails->itemAssociations;
            return $associationDetail;
        }

    }

    /**
     * Set DocumentURI for an association model
     */
    private function transformAndSetDocumentUri($itemAssociationIdentifier){    
        $itemAssociationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModel($itemAssociationIdentifier);
        $orgCode                =   $this->getOrgCode();
        $associationDocument = !empty($itemAssociationDetails->document) ? $itemAssociationDetails->document : [];
        $getImportType = $this->getdocumentImportType( $itemAssociationDetails->document_id,$itemAssociationDetails->organization_id);
        $documentUri = ($getImportType->import_type==1)?$getImportType->uri:$this->getCaseApiUri("CFDocuments", $associationDocument->source_document_id,true,$orgCode,$this->domainName);
        
        $this->setDocumenturi($documentUri);
        return $this->getDocumentUri();
    }

    /**
     * Set originNodeURIEntity for an association model
     */
    private function transformAndSetOriginNodeEntity($itemAssociationIdentifier) {
        $itemAssociationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModel($itemAssociationIdentifier);
        $orgCode                =   $this->getOrgCode();
        $associationOriginNode = !empty($itemAssociationDetails->sourceItemId) ? $itemAssociationDetails->sourceItemId : [];

        $getImportType = $this->getdocumentImportType( $itemAssociationDetails->document_id,$itemAssociationDetails->organization_id);
        //Fetch the Metadata list for the selected nodeType of the Item
        $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationOriginNode->node_type_id);
        $this->setMetadataList($metadataList);

        $arrayOfInternalName    =   $this->parseMetadata();
       
        if(in_array('full_statement', $arrayOfInternalName) === true ){
            if($getImportType->import_type==1)
            {
                $getItemUri = $this->getUriFromItem($itemAssociationDetails->document_id,$itemAssociationDetails->organization_id,$itemAssociationDetails->source_item_id);

                $originNodeURIEntity = [
                    "title"         =>  !empty($associationOriginNode->full_statement) ? $associationOriginNode->full_statement : "",
                    "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                    "uri"           =>  !empty($getItemUri->uri) ? $getItemUri->uri : "",
                ];
            }else if($getImportType->import_type==2)
            {
                $originNodeURIEntity = [
                    "title"         =>  !empty($associationOriginNode->full_statement) ? $associationOriginNode->full_statement : "",
                    "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                    "uri"           =>  !empty($associationOriginNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationOriginNode->source_item_id,true,$orgCode,$this->domainName) : "",
                ];
            }else{
                $originNodeURIEntity = [
                    "title"         =>  !empty($associationOriginNode->full_statement) ? $associationOriginNode->full_statement : "",
                    "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                    "uri"           =>  !empty($associationOriginNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationOriginNode->source_item_id,true,$orgCode,$this->domainName) : "",
                ];
            }
          
        }   else    {
            $originNodeURIEntity = [
                "title"         =>  "",
                "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                "uri"           =>  !empty($associationOriginNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationOriginNode->source_item_id,true,$orgCode,$this->domainName) : "",
            ];
        }
        $this->setOriginNodeUri($originNodeURIEntity);
        return $this->getOriginNodeUri();
    }

    /**
     * Set destinationNodeURIEntity for an association model
     */
    private function transformAndSetDestinationNodeEntity($itemAssociationIdentifier) {
        $destinationNodeURIEntity = [];

        $itemAssociationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModel($itemAssociationIdentifier);
        $orgCode                = $this->getOrgCode();
        $getImportType = $this->getdocumentImportType( $itemAssociationDetails->document_id,$itemAssociationDetails->organization_id);
        //Check whether destination_node and document_node are similar
        if($itemAssociationDetails->document_id == $itemAssociationDetails->target_item_id) {
            $associationDestinationNode = !empty($itemAssociationDetails->document) ? $itemAssociationDetails->document : [];
            //Fetch the Metadata list for the selected nodeType of the Item
            $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationDestinationNode->node_type_id);
            $this->setMetadataList($metadataList);

            $arrayOfInternalName    =   $this->parseMetadata();

            if(in_array('title', $arrayOfInternalName) === true ){
                if($getImportType->import_type==1){
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->title) ? $associationDestinationNode->title : "",
                        "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                        "uri"           =>  !empty($getImportType->uri) ? $getImportType->uri   : ""
                    ];
                }else if ($getImportType->import_type==2){
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->title) ? $associationDestinationNode->title : "",
                        "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                        "uri"           =>  !empty($associationDestinationNode->source_document_id) ?  $this->getCaseApiUri("CFDocuments", $associationDestinationNode->source_document_id,true,$orgCode,$this->domainName) : ""
                    ];  
                }else{
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->title) ? $associationDestinationNode->title : "",
                        "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                        "uri"           =>  !empty($associationDestinationNode->source_document_id) ?  $this->getCaseApiUri("CFDocuments", $associationDestinationNode->source_document_id,true,$orgCode,$this->domainName) : ""
                    ];  
                }
                
            }   else    {
                $destinationNodeURIEntity = [
                    "title"         =>  "",
                    "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                    "uri"           =>  !empty($associationDestinationNode->source_document_id) ?  $this->getCaseApiUri("CFDocuments", $associationDestinationNode->source_document_id,true,$orgCode,$this->domainName) : ""
                ];  
            }     
        }
        else{
            $associationDestinationNode = !empty($itemAssociationDetails->targetItemId) ? $itemAssociationDetails->targetItemId : [];
            
            //Fetch the Metadata list for the selected nodeType of the Item
            $arrayOfInternalName =[];
            if(!empty($associationDestinationNode)) {
                $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationDestinationNode->node_type_id);
                $this->setMetadataList($metadataList);

                $arrayOfInternalName    =   $this->parseMetadata();
            }
            if(in_array('full_statement', $arrayOfInternalName) === true ){
                $destinationNodeURIEntity = [
                    "title"         =>  !empty($associationDestinationNode->full_statement) ? $associationDestinationNode->full_statement : "",
                    "identifier"    =>  !empty($associationDestinationNode->source_item_id) ? $associationDestinationNode->source_item_id : "",
                    "uri"           =>  !empty($associationDestinationNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationDestinationNode->source_item_id,true,$orgCode,$this->domainName) : "",
                ];  
            }   else    {
                $destinationNodeURIEntity = [
                    "title"         =>  "",
                    "identifier"    =>  !empty($associationDestinationNode->source_item_id) ? $associationDestinationNode->source_item_id : "",
                    "uri"           =>  !empty($associationDestinationNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationDestinationNode->source_item_id,true,$orgCode,$this->domainName) : "",
                ];  
            }              
        }

        $this->setDestinationNodeUri($destinationNodeURIEntity);
        return $this->getDestinationNodeUri();
    }

    /**
     * Set AssociationGroupEntity for an association model
     */
    private function transformAndSetAssociationGroupingEntity($associationIdentifier) {
        $associationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModel($associationIdentifier);
        $associationGroupDetail = !empty($associationDetails->associationGroup) ? $associationDetails->associationGroup : [];
        $getImportType = $this->getdocumentImportType( $associationDetails->document_id,$associationDetails->organization_id);
        $orgCode                = $this->getOrgCode();
        // parse out the SubjectURI related info
        if($getImportType->import_type==1)
        {
            $getGroupUri = $this->getUriFromGroups($associationDetails->association_group_id,$associationDetails->organization_id);
            $CFAssociationGroupingURIEntity = [
                "title"         => !empty($associationGroupDetail->title) ? $associationGroupDetail->title : "",
                "identifier"    => !empty($associationGroupDetail->source_association_group_id) ? $associationGroupDetail->source_association_group_id : "",
                "uri"           => !empty($getGroupUri->uri) ? $getGroupUri->uri  : "",
            ];
        }else if($getImportType->import_type==2)
        {
            $CFAssociationGroupingURIEntity = [
                "title"         => !empty($associationGroupDetail->title) ? $associationGroupDetail->title : "",
                "identifier"    => !empty($associationGroupDetail->source_association_group_id) ? $associationGroupDetail->source_association_group_id : "",
                "uri"           => !empty($associationGroupDetail->source_association_group_id) ? $this->getCaseApiUri("CFAssociationGroupings", $associationGroupDetail->source_association_group_id,true,$orgCode,$this->domainName) : ""
            ];
        }else{
            $CFAssociationGroupingURIEntity = [
                "title"         => !empty($associationGroupDetail->title) ? $associationGroupDetail->title : "",
                "identifier"    => !empty($associationGroupDetail->source_association_group_id) ? $associationGroupDetail->source_association_group_id : "",
                "uri"           => !empty($associationGroupDetail->source_association_group_id) ? $this->getCaseApiUri("CFAssociationGroupings", $associationGroupDetail->source_association_group_id,true,$orgCode,$this->domainName) : ""
            ];
            
        }
        
        $this->setAssociationGroupingEntity($CFAssociationGroupingURIEntity);
        return $this->getAssociationGroupingEntity();
    }

    /**
     * Set ItemAssociation list for the selected ITEM
     */
    private function transformAndSetItemAssociation(){
        $CFItemAssociations = [];

        $itemAssociations = $this->getItemAssociationDetails();
      
        foreach($itemAssociations as $associations){
            $associationType = $this->returnAssociationType($associations->association_type);
            $orgCode         = $this->getOrgCode();

            $sourceitemAssociationId = $associations->source_item_association_id;
           
            $getimportType = DB::table('item_associations')
            ->leftJoin('documents', 'item_associations.source_document_id', '=', 'documents.document_id')
            ->where('item_associations.source_item_association_id','=',$sourceitemAssociationId)
            ->where('item_associations.organization_id','=',$associations->organization_id)
            ->get()
            ->toarray();

            if(!empty($getimportType)){
                $import_type = $getimportType[0]->import_type;
            }
        
            if($import_type == 1) {
                $item_association_uri = $getimportType[0]->uri;
            } 
    

            // create the CFItemAssociations
            $CFItemAssociations["CFAssociations"][] = [
                "identifier"                => $associations->source_item_association_id,
                "associationType"           => $associationType,
                "CFDocumentURI"             => $this->transformAndSetDocumentUri($associations->item_association_id),
                "sequenceNumber"            => $associations->sequence_number,
                "uri"                       => isset($item_association_uri) ? $item_association_uri : $this->getCaseApiUri("CFAssociations", $associations->source_item_association_id,true,$orgCode,$this->domainName),
                "originNodeURI"             => $this->transformAndSetOriginNodeEntity($associations->item_association_id),
                "destinationNodeURI"        => $this->transformAndSetDestinationNodeEntity($associations->item_association_id),
                "CFAssociationGroupingURI"  => $this->transformAndSetAssociationGroupingEntity($associations->item_association_id),
                "lastChangeDateTime"        => $this->formatDateTimeToISO8601($associations->updated_at),
                "description"               => !empty($associations->description) ? $associations->description :'',
            ];
        }

        return $CFItemAssociations;
    }

    public function getdocumentImportType($documentId,$organizationId)
    {
    $query = DB::table('documents')
    ->select('import_type','uri')
    ->where('document_id',$documentId)
    ->where('organization_id',$organizationId)
    ->where('is_deleted',0)
    ->get()
    ->first();
    return $query;
    }

    public function getUriFromGroups($groupId,$organizationId){
        $groupUrl = DB::table('association_groups')->select('uri')
        ->where('association_group_id',$groupId)
        ->where('organization_id',$organizationId)
        ->where('is_deleted',0)
        ->get()
        ->first();
        return $groupUrl;
        }

    public function getUriFromItem($documentId,$organizationId,$itemId){
            $itemUrl = DB::table('items')->select('uri')
            ->where('item_id',$itemId)
            ->where('organization_id',$organizationId)
            ->where('document_id',$documentId)
            ->where('is_deleted',0)
            ->get()
            ->first();
            return $itemUrl;
    }
}

