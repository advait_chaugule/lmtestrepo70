<?php
namespace App\Domains\Casestandard\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\Document; 
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Data\Models\Organization;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use DB;
class GetCaseFrameworkLicenseJob extends Job
{
    use DateHelpersTrait, CaseFrameworkTrait;

    private $licenseRepository;
    private $metadataRepository;
    private $metadataList;
    private $identifier;
    private $licenseDetail;
    private $organizationCode;
    private $orgCode;
    private $repositoryOrg;
    private $domainName;

    /**
     * GetCaseFrameworkLicenseJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl='')
    {
        //set the identifier
        $this->setIdentifier($identifier);
        $this->orgCode = $orgCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LicenseRepositoryInterface $licenseRepo, MetadataRepositoryInterface $metadataRepository,ItemRepositoryInterface $repository)
    {
        //set the repo variable
        $this->licenseRepository    =   $licenseRepo;
        $this->repositoryOrg           =   $repository;
        $this->metadataRepository   =   $metadataRepository;
        //get the license data from repo
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId = $this->repositoryOrg->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }
        $licenseData = $this->fetchLicenseDataFromRepo($organizationId);
        //print_r($licenseData);die;
        if(!empty($licenseData['source_license_id'])) {
            //Fetch and set active metadata on the basis of organization
            $attributes =   ['organization_id'  =>  $licenseData['organization_id'], 'is_active'    =>  '1', 'is_deleted'   => '0'];
            $metadataList   =   $this->metadataRepository->findByAttributes($attributes);

            $this->setMetadataList($metadataList);

            //set the license data 
            $this->setLicenseDetail($licenseData);
            //transform the license data from repo to Case Standard
            $licenseEntity = $this->transformAndSetLicense();
            //finally return response as per CASE
            return $licenseEntity; 
        }

        return false;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setMetadataList($data)
    {
        $this->metadataList = $data;
    }

    public function getMetadataList()
    {
        return $this->metadataList;
    }
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }


    private function fetchLicenseDataFromRepo($organizationId)
    {
        $identifier     =   $this->getIdentifier();
        $result = DB::table('licenses')->select('license_id')->where('source_license_id', $identifier)->count();
       if($result > 0)
       {
            if($organizationId) {
                $attributes     =   ['source_license_id'=>$identifier,'organization_id'=>$organizationId,'is_deleted' => '0'];
            }else{
                $attributes     =   ['source_license_id'=>$identifier,'is_deleted' => '0'];
            }
       }
       else {

            if($organizationId) {
                $attributes     =   ['license_id'=>$identifier,'organization_id'=>$organizationId,'is_deleted' => '0'];
            }else{
                $attributes     =   ['license_id'=>$identifier,'is_deleted' => '0'];
            }
        
       }
        

        $licenseDetail  =   $this->licenseRepository->findByAttributes($attributes)->first();
        $this->setOrgCode($licenseDetail->organization_id);
        $licenseEntity  =   [
            "license_id"                =>  $licenseDetail->license_id,
            "license_title"             =>  !empty($licenseDetail->title) ? $licenseDetail->title : "",
            "license_description"       =>  !empty($licenseDetail->description) ? $licenseDetail->description : "",
            "license_text"              =>  !empty($licenseDetail->license_text) ? $licenseDetail->license_text : "",
            "is_deleted"                =>  $licenseDetail->is_deleted,
            "source_license_id"         =>  $licenseDetail->source_license_id,
            "organization_id"           =>  $licenseDetail->organization_id, 
            "created_at"                =>  $licenseDetail->created_at,
            "updated_at"                =>  $licenseDetail->updated_at
        ];

        return  $licenseEntity;
    }

    public function setLicenseDetail($data)
    {
        $this->licenseDetail = $data;
    }

    public function getLicenseDetail()
    {
        return $this->licenseDetail;
    }

    private function parseMetadata() {
        $metadataList   =   $this->getMetadataList() ;

        foreach ($metadataList as $metadata) {
            $arrayOfMetadataInternalName[]    =   $metadata->internal_name;
        }

        return $arrayOfMetadataInternalName;
    }

    private function transformAndSetLicense()
    {
        $licenseData                    =   $this->getLicenseDetail();
        $arrayOfMetadataInternalName    =   $this->parseMetadata();
        $orgCode                        =   $this->getOrgCode();
        foreach($licenseData as $key => $license) {
            if(in_array($key, $arrayOfMetadataInternalName) === true) {
                $licenseMatchedMetadata[$key]   =   $license;
            } else {
                $licenseMatchedMetadata[$key]   =   "";
            }
        }

        $getImportType = $this->getDocumentType($licenseData['license_id'],$licenseData['organization_id']);
        if(empty($getImportType)) {
            $getItemImportType = $this->getDocumentTypeViaItem($licenseData['license_id'],$licenseData['organization_id']);
            $importType    = $getItemImportType->import_type;
        }else{
            $importType    = $getImportType->import_type;
        }


        $identifier = !empty($licenseData['source_license_id']) ? $licenseData['source_license_id'] : "";
        $uri ='';
        if($identifier) {
            $getLicenseUri = $this->getLicenseUri($identifier);
            $uri           =$getLicenseUri->uri;
        }

        $licenseEntity = [
            "identifier"            =>  $identifier,
            "uri"                   =>  ($importType==1)?$uri:$this->getCaseApiUri('CFLicenses', $identifier,true,$orgCode,$this->domainName),
            "title"                 =>  (!empty($licenseMatchedMetadata['license_title']) ? $licenseMatchedMetadata['license_title'] : ""),
            "description"           =>  (!empty($licenseMatchedMetadata['license_description']) ? $licenseMatchedMetadata['license_description'] : ""),
            "licenseText"           =>  (!empty($licenseMatchedMetadata['license_text'])? $licenseMatchedMetadata['license_text'] : ""),
            "lastChangeDateTime"    =>  $this->formatDateTimeToISO8601($licenseData['updated_at']),
        ];

        return $licenseEntity;
    }

    public function getDocumentType($licenseId,$organizationId)
    {
        $importType = DB::table('documents')
                      ->select('import_type')
                      ->where('license_id',$licenseId)
                      ->where('organization_id',$organizationId)
                      ->where('is_deleted',0)
                      ->get()
                      ->first();
        return $importType;


    }

    public function getDocumentTypeViaItem($licenseId,$organizationId)
    {
        $importTypes = DB::table('items')
             ->join('documents','documents.document_id','=','items.document_id')
             ->select('documents.import_type')
            ->where('items.license_id',$licenseId)
            ->where('items.organization_id',$organizationId)
            ->where('items.is_deleted',0)
            ->get()
            ->first();
        return $importTypes;


    }

    public function getLicenseUri($sourceLicenseId)
    {
        $importType = DB::table('licenses')
            ->select('uri')
            ->where('source_license_id',$sourceLicenseId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $importType;


    }
}
