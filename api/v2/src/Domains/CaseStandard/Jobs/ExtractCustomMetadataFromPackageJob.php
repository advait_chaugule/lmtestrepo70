<?php
namespace App\Domains\CaseStandard\Jobs;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\CurlHelper;
use Illuminate\Support\Facades\DB;

class ExtractCustomMetadataFromPackageJob extends Job
{
    use CurlHelper;
    private $input;
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    public function handle()
    {
        $linkedId   = $this->input['linked_server_id'];
        $documentId = $this->input['document_id'];

        $linkedServerDetails = DB::table('linked_servers')
        ->select('server_url')
        ->where('linked_server_id', $linkedId)        
        ->where('is_deleted' , 0)
        ->first();
        
        $api_url = $linkedServerDetails->server_url;
        if($documentId != "")
        {
            $endPoint = $api_url."/CustomMetadata/".$documentId;
            $response = $this->checkUrl($endPoint,"GET");
            return $response;
        }else{
            return false;
        }
    }

}