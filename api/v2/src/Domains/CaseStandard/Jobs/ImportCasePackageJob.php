<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Data\Models\Document;
use App\Data\Models\NodeType;
use App\Data\Models\Organization;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\StringHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;

use App\Domains\Search\Events\UploadSearchDataToSqsEvent;

class ImportCasePackageJob extends Job
{
    use ArrayHelper, UuidHelperTrait, DateHelpersTrait, CaseFrameworkTrait, StringHelper;

    public $recordChunkCount = 500;

    private $requestImportType;
    private $packageExists;
    private $packageData;

    private $oldNewItemMapping;
    private $requestingUser;
    private $requestingUserOrganizationId;
    private $importAsClone;

    private $documentRepository;
    private $itemRepository;
    private $languageRepository;
    private $licenseRepository;
    private $subjectRepository;
    private $nodeTypeRepository;
    private $conceptRepository;
    private $itemAssociationRepository;
    private $associationGroupRepository;

    private $caseDocumentToSave;
    private $caseItemsToSave;
    private $caseAssociationsToSave;
    private $childOfCaseAssociations;
    private $caseConceptsToSave;
    private $caseSubjectsToSave;
    private $caseLicensesToSave;
    private $caseItemTypesToSave;
    private $caseAssociationGroupsToSave;

    private $savedConcepts;
    private $savedSubjects;
    private $savedLicenses;
    private $savedNodeTypes;
    private $savedAssociationGroups;
    private $savedLanguages;
    private $destinationNodeItems;
    private $reverseDestinationNodeItems;
    private $originNodeItems;

    private $defaultItemNodeType;
    private $defaultDocumentNodeType;
    private $defaultItemNodeTypeMetadataIds;

    private $beforeSaveChildOfCaseAssociations;

    private $savedDocument;
    private $savedItems;
    private $importType;
    private $clonedAssociatonsToSave;
    private $customDataArr;
    private $newCreatedNodeType;

    private $organizationCode;
    private $domainName;
    /**
     * ImportCasePackageJob constructor.
     * @param array $packageDataArray
     * @param array $requestingUserDetails
     * @param bool $importAsClone
     * @param $customData
     */
    public function __construct(array $packageDataArray, array $requestingUserDetails, bool $importAsClone,$customData,$requestUrl)
    {
        $this->setPackageData($packageDataArray["packageData"]);
        $this->setOldNewItemMapping($packageDataArray["oldNewItemMapping"]);
        $this->setRequestingUser($requestingUserDetails);
        $this->setRequestingUserOrganizationId($requestingUserDetails["organization_id"]);
        $this->setImportAsCloneStatus($importAsClone);
        if($importAsClone)
        {
            $requestingUserDetails["import_type"]=2;
       
        }else{
            $requestingUserDetails["import_type"]=1;
        }
        $this->setImportType($requestingUserDetails["import_type"]);
        $this->domainName = $requestUrl;

        //$this->customDataArr = json_decode( $customData);

    }

    /************** All setters and getters for data passed to the job **************/
    public function setPackageExists(int $data) {
        $this->packageExists = $data;
    }

    public function getPackageExists() {
        return $this->packageExists;
    }

    public function setImportType(int $data) {
        $this->requestImportType = $data;
    }

    public function getImportType() {
        return $this->requestImportType;
    }

    public function setPackageData(array $data) {
        $this->packageData = $data;
    }

    public function getPackageData(): array {
        return $this->packageData;
    }

    public function setOldNewItemMapping(array $data) {
        $this->oldNewItemMapping = $data;
    }

    public function getOldNewItemMapping(): array {
        return $this->oldNewItemMapping;
    }

    public function setRequestingUser(array $data) {
        $this->requestingUser = $data;
    }

    public function getRequestingUser(): array {
        return $this->requestingUser;
    }

    public function setRequestingUserOrganizationId(string $data) {
        $this->requestingUserOrganization = $data;
    }

    public function getRequestingUserOrganizationId(): string {
        return $this->requestingUserOrganization;
    }

    public function setImportAsCloneStatus(bool $data) {
        $this->importAsClone = $data;
    }

    public function getImportAsCloneStatus(): bool {
        return $this->importAsClone;
    }

    /************** All setters and getters for case package relevant entities to be saved **************/

    public function setCaseDocumentToSave(array $data) {
        $this->caseDocumentToSave = $data;
    }

    public function getCaseDocumentToSave(): array {
        return $this->caseDocumentToSave;
    }

    public function setCaseItemsToSave(array $data) {
        $this->caseItemsToSave = $data;
    }

    public function getCaseItemsToSave(): array {
        return $this->caseItemsToSave;
    }

    public function setCaseAssociationsToSave(array $data) {
        $this->caseAssociationsToSave = $data;
    }

    public function getCaseAssociationsToSave(): array {
        return $this->caseAssociationsToSave;
    }

    public function setChildOfCaseAssociations(array $data) {
        $this->childOfCaseAssociations = $data;
    }

    public function getChildOfCaseAssociations(): array {
        return $this->childOfCaseAssociations;
    }

    public function setCaseConceptsToSave(array $data) {
        $this->caseConceptsToSave = $data;
    }

    public function getCaseConceptsToSave(): array {
        return $this->caseConceptsToSave;
    }

    public function setCaseSubjectsToSave(array $data) {
        $this->caseSubjectsToSave = $data;
    }

    public function getCaseSubjectsToSave(): array {
        return $this->caseSubjectsToSave;
    }

    public function setCaseLicensesToSave(array $data) {
        $this->caseLicensesToSave = $data;
    }

    public function getCaseLicensesToSave(): array {
        return $this->caseLicensesToSave;
    }

    public function setCaseItemTypesToSave(array $data) {
        $this->caseItemTypesToSave = $data;
    }

    public function getCaseItemTypesToSave(): array {
        return $this->caseItemTypesToSave;
    }

    public function setCaseAssociationGroupsToSave(array $data) {
        $this->caseAssociationGroupsToSave = $data;
    }

    public function getCaseAssociationGroupsToSave(): array {
        return $this->caseAssociationGroupsToSave;
    }

    /************** All setters and getters for case relavant data that is already saved in our database **************/

    public function setSavedConcepts(Collection $data) {
        $this->savedConcepts = $data;
    }

    public function getSavedConcepts(): Collection {
        return $this->savedConcepts;
    }

    public function setSavedSubjects(Collection $data) {
        $this->savedSubjects = $data;
    }

    public function getSavedSubjects(): Collection {
        return $this->savedSubjects;
    }

    public function setSavedLicenses(Collection $data) {
        $this->savedLicenses = $data;
    }

    public function getSavedLicenses(): Collection {
        return $this->savedLicenses;
    }

    public function setSavedNodeTypes(Collection $data) {
        $this->savedNodeTypes = $data;
    }

    public function getSavedNodeTypes(): Collection {
        return $this->savedNodeTypes;
    }

    public function setSavedAssociationGroups(Collection $data) {
        $this->savedAssociationGroups = $data;
    }

    public function getSavedAssociationGroups(): Collection {
        return $this->savedAssociationGroups;
    }

    public function setSavedLanguages(Collection $data) {
        $this->savedLanguages = $data;
    }

    public function getSavedLanguages(): Collection {
        return $this->savedLanguages;
    }

    public function setDestinationNodeItems(Collection $data) {
        $this->destinationNodeItems = $data;
    }

    public function getDestinationNodeItems(): Collection {
        return $this->destinationNodeItems;
    }

    public function setOriginNodeItems(Collection $data) {
        $this->originNodeItems = $data;
    }

    public function getOriginNodeItems(): Collection {
        return $this->originNodeItems;
    }

    public function setReverseDestinationNodeItems(Collection $data) {
        $this->reverseDestinationNodeItems = $data;
    }

    public function getReverseDestinationNodeItems(): Collection {
        return $this->reverseDestinationNodeItems;
    }

    

    /************** All setters and getters for default node types for document and item and their respective metadata Ids **************/

    public function setDefaultItemNodeType(NodeType $data) {
        $this->defaultItemNodeType = $data;
    }

    public function getDefaultItemNodeType(): NodeType {
        return $this->defaultItemNodeType;
    }

    public function setDefaultDocumentNodeType(NodeType $data) {
        $this->defaultDocumentNodeType = $data;
    }

    public function getDefaultDocumentNodeType(): NodeType {
        return $this->defaultDocumentNodeType;
    }

    public function setDefaultItemNodeTypeMetadataIds(array $data) {
        $this->defaultItemNodeTypeMetadataIds = $data;
    }

    public function getDefaultItemNodeTypeMetadataIds(): array {
        return $this->defaultItemNodeTypeMetadataIds;
    }

    /************** All setters and getters for document and items saved **************/

    public function setSavedDocument(Document $data) {
        $this->savedDocument = $data;
    }

    public function getSavedDocument(): Document {
        return $this->savedDocument;
    }

    public function setSavedItems(array $data) {
        $this->savedItems = $data;
    }

    public function getSavedItems(): array {
        return $this->savedItems;
    }

    public function setClonedAssociatonsToSave(array $data) {
        $this->clonedAssociatonsToSave = $data;
    }

    public function getClonedAssociatonsToSave(): array {
        return $this->clonedAssociatonsToSave;
    }

    public function setOrgCode(string $data) {
        
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        return $this->organizationCode;
    }

    public function setNewCreatedNodeType(array $data) {
        $this->newCreatedNodeType = $data;
    }

    public function getNewCreatedNodeType(): array {
        return $this->newCreatedNodeType;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ConceptRepositoryInterface $conceptRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssociationGroupRepositoryInterface $associationGroupRepository
    )
    {
        // set the db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->subjectRepository = $subjectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->conceptRepository = $conceptRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->associationGroupRepository = $associationGroupRepository;

        // extract all relevant data to be saved from case package 
        $this->extractAndSetCaseDocumentToSave();
        $this->extractAndSetCaseItemsToSave();
        $this->extractAndSetCaseAssociationsToSave();
        $this->extractAndSetChildOfCaseAssociations();
        $this->extractAndSetCaseConceptsToSave();
        $this->extractAndSetCaseSubjectsToSave();
        $this->extractAndSetCaseLicensesToSave();
        $this->extractAndSetCaseItemTypesToSave();
        $this->extractAndSetCaseAssociationGroupsToSave();

        // fetch from db and set saved data relevant to case
        $this->fetchAndSetSavedConcepts();
        $this->fetchAndSetSavedSubjects();
        $this->fetchAndSetSavedLicenses();
        $this->fetchAndSetSavedNodeTypes();
        $this->fetchAndSetSavedAssociationGroups();
        $this->fetchAndSetSavedLanguages();
        $this->fetchAndSetDestinationNodeItems();
        $this->fetchAndSetOriginNodeItems();
        $this->fetchAndSetReverseDestinationNodeItems();

        // search for and set default item and document node types
        $this->searchForAndSetDefaultItemNodeType();
        $this->searchForAndSetDefaultDocumentNodeType();

        // fetch from db and set saved metadata Ids for item default node type
        $this->fetchAndSetDefaultItemNodeTypeMetadataIds();
        // Open transaction block, process each case entities indivdually and save them to db
        $this->processAndSaveCaseSubjectsData();
        $this->processAndSaveCaseLicensesData();
        $this->processAndSaveCaseConceptsData();
        $this->processAndSaveCaseAssociationGroupingsData();
        $this->processAndSaveCaseItemTypesData();
        $this->processAndSaveCaseDocumentData();
        $this->processAndSaveDocumentSubjects();
        $this->processAndSaveCaseItemsData();
        $this->processAndSaveCaseAssociationsData();

        $this->raiseEventToUploadTaxonomySearchDataToSqs();
        $savedDocument = $this->getSavedDocument();
        $documentId = $savedDocument->document_id;
        $documentName = $savedDocument->title;
        $documentDetail = ["document_id" => $documentId , "document_title" => $documentName,'items'=>$this->getSavedItems(),'newlyCreatedNode'=> !empty($this->getNewCreatedNodeType())?$this->getNewCreatedNodeType():''];
        return $documentDetail;
    }

    /************** All extractors to extract and set case data to be saved **************/

    private function extractAndSetCaseDocumentToSave() {
        $packageData = $this->getPackageData();
        $caseDocumentToSave = $packageData["CFDocument"];
        $this->setCaseDocumentToSave($caseDocumentToSave);
    }

    private function extractAndSetCaseItemsToSave() {
        $packageData = $this->getPackageData();
        $caseItemsToSave = !empty($packageData["CFItems"]) ? $packageData["CFItems"] : [];

        $this->setCaseItemsToSave($caseItemsToSave);
    }

    private function extractAndSetCaseAssociationsToSave() {
        $packageData = $this->getPackageData();
        $caseAssociationsToSave = !empty($packageData["CFAssociations"]) ? $packageData["CFAssociations"] : [];

        //dd($caseAssociationsToSave);

        $this->setCaseAssociationsToSave($caseAssociationsToSave);
    }

    private function extractAndSetChildOfCaseAssociations() {
        $associationsTosave = $this->getCaseAssociationsToSave();
        $data = [];
        foreach($associationsTosave as $associationToSave) {
            if($associationToSave["associationType"]==="isChildOf" && !empty($associationToSave["originNodeURI"]["identifier"])) {
                $data[] = $associationToSave;
            }
        }
        $this->setChildOfCaseAssociations($data);
    }

    private function extractAndSetCaseConceptsToSave() {
        $packageData = $this->getPackageData();
        $caseConceptsToSave = !empty($packageData['CFDefinitions']["CFConcepts"]) ? $packageData['CFDefinitions']["CFConcepts"] : [];
        $this->setCaseConceptsToSave($caseConceptsToSave);
    }

    private function extractAndSetCaseSubjectsToSave() {
        $packageData = $this->getPackageData();
        $caseSubjectsToSave = !empty($packageData['CFDefinitions']["CFSubjects"]) ? $packageData['CFDefinitions']["CFSubjects"] : [];
        $this->setCaseSubjectsToSave($caseSubjectsToSave);
    }

    private function extractAndSetCaseLicensesToSave() {
        $packageData = $this->getPackageData();
        $caseLicensesToSave = !empty($packageData["CFDefinitions"]["CFLicenses"]) ? $packageData["CFDefinitions"]["CFLicenses"] : [];
        $this->setCaseLicensesToSave($caseLicensesToSave);
    }

    private function extractAndSetCaseItemTypesToSave() {
        $packageData = $this->getPackageData();
        $caseItemTypesToSave = !empty($packageData["CFDefinitions"]["CFItemTypes"]) ? $packageData["CFDefinitions"]["CFItemTypes"] : [];
        $this->setCaseItemTypesToSave($caseItemTypesToSave);
    }

    private function extractAndSetCaseAssociationGroupsToSave() {
        $packageData = $this->getPackageData();
        $caseAssociationGroupsToSave = !empty($packageData["CFDefinitions"]["CFAssociationGroupings"]) ? 
                                        $packageData["CFDefinitions"]["CFAssociationGroupings"] : 
                                        [];
        $this->setCaseAssociationGroupsToSave($caseAssociationGroupsToSave);
    }

    /************** All methods to fetch and set case data which is already saved inside our database **************/

    private function fetchAndSetSavedConcepts() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("concept");
        $this->setSavedConcepts($savedData);
    }

    private function fetchAndSetSavedSubjects() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("subject");
        $this->setSavedSubjects($savedData);
    }

    private function fetchAndSetSavedLicenses() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("license");
        $this->setSavedLicenses($savedData);
    }

    private function fetchAndSetSavedNodeTypes() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("node");
        $this->setSavedNodeTypes($savedData);
    }

    private function fetchAndSetSavedAssociationGroups() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("associationGroup");
        $this->setSavedAssociationGroups($savedData);
    }

    private function fetchAndSetSavedLanguages() {
        $savedData = $this->helperToFetchTenantSpecificDataOfProvidedType("language");
        $this->setSavedLanguages($savedData);
    }

    private function fetchAndSetDestinationNodeItems() {
        $destinationNodeIds = $this->helperToReturnDestinationNodeIds();
        $attributeIn = "source_item_id";
        $organizationId =   $this->getRequestingUserOrganizationId();
        $whereClauseAttributes = ['organization_id' => $organizationId];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $destinationNodeIds, $whereClauseAttributes);
        $dataToSet = $items->isNotEmpty() ? $items : collect([]);
        $this->setDestinationNodeItems($dataToSet);
    }

    private function fetchAndSetOriginNodeItems() {
        $originNodeIds = $this->helperToReturnOriginNodeIds();
        $attributeIn = "source_item_id";
        $organizationId =   $this->getRequestingUserOrganizationId();
        $whereClauseAttributes = ['organization_id' => $organizationId];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $originNodeIds, $whereClauseAttributes);
        $dataToSet = $items->isNotEmpty() ? $items : collect([]);
        $this->setOriginNodeItems($dataToSet);
    }

    private function fetchAndSetReverseDestinationNodeItems() {
        $organizationId             = $this->getRequestingUserOrganizationId();
        $reverseDestinationNodeIds  = $this->helperToReturnReverseDestinationNodeIds();

        $attributeIn            = "source_item_id";
        $whereClauseAttributes  = ['organization_id' => $organizationId];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $reverseDestinationNodeIds, $whereClauseAttributes);
        $dataToSet = $items->isNotEmpty() ? $items : collect([]);

        $this->setReverseDestinationNodeItems($dataToSet);
    }

    /************** Methods to search and set default node types for item and document **************/

    private function searchForAndSetDefaultItemNodeType() {
        $savedNodeTypes = $this->getSavedNodeTypes();
        $organizationId             = $this->getRequestingUserOrganizationId();
        $defaultItemNodeType = $savedNodeTypes->where("organization_id",$organizationId)->where("is_default",1)->first();
        $this->setDefaultItemNodeType($defaultItemNodeType);
    }

    private function searchForAndSetDefaultDocumentNodeType() {
        $savedNodeTypes = $this->getSavedNodeTypes();
        $organizationId             = $this->getRequestingUserOrganizationId();
        $defaultDocumentNodeType = $savedNodeTypes->where("organization_id",$organizationId)->where("is_document", 1)->first();
        $this->setDefaultDocumentNodeType($defaultDocumentNodeType);
    }

    /************** Methods to fetch and set default node type's metadata ids for item only**************/

    private function fetchAndSetDefaultItemNodeTypeMetadataIds() {
        $defaultItemNodeType = $this->getDefaultItemNodeType();
        // fetch active metadata only
        $defaultItemNodeTypeMetadataCsv = $defaultItemNodeType->metadata()
                                                              ->select(["metadata.metadata_id"])->where("is_active", 1)->get()
                                                              ->implode('metadata_id', ',');
        $defaultItemNodeTypeMetadataArray = $this->createArrayFromCommaeSeparatedString($defaultItemNodeTypeMetadataCsv);
        $this->setDefaultItemNodeTypeMetadataIds($defaultItemNodeTypeMetadataArray);
    }

    /************** Methods to process and save each entities of case package **************/

    private function processAndSaveCaseSubjectsData() {
        $subjectsToSave = $this->getCaseSubjectsToSave();
        $subjectsAlreadySaved = $this->getSavedSubjects();
        if(!empty($subjectsToSave)) {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newSubjectsToSave = [];
            foreach($subjectsToSave as $subjectToSave) {
                if(!empty($subjectToSave["title"])){
                    $subjectTitleToSearchWith = $subjectToSave["title"];
                    $this->setOrgCode($organizationId);
                    $savedSubjectCollection = $subjectsAlreadySaved->where("title", $subjectTitleToSearchWith);
                    // if subject doesn't exists in the db then only save it and update the list of saved subjects
                    if($savedSubjectCollection->isEmpty()) {
                        $subjectToSaveIdentifier = $subjectToSave["identifier"];
                        $newSubjectIdentifier = $this->createUniversalUniqueIdentifier();
                        $newSubjectTitle = $subjectTitleToSearchWith;
                        $orgCode = $this->getOrgCode();
                        $newSubjectHierarchyCode = !empty($subjectToSave["hierarchyCode"]) ? $subjectToSave["hierarchyCode"] : "";
                        $newSubjectDescription = !empty($subjectToSave["description"]) ? $subjectToSave["description"] : "";
                        $lastChangeDateTime = !empty($subjectToSave["lastChangeDateTime"]) ? 
                                              $this->formatDateTime($subjectToSave["lastChangeDateTime"]) : null;
                        $newSubject = [
                            "subject_id" => $newSubjectIdentifier, 
                            "organization_id" => $organizationId,
                            "title" => $newSubjectTitle,
                            "hierarchy_code" => $newSubjectHierarchyCode,
                            "description" => $newSubjectDescription,
                            "updated_at" => $lastChangeDateTime,
                            "source_subject_id" => $subjectToSaveIdentifier, 
                            "uri" => $this->getCaseApiUri("CFSubjects", $newSubjectIdentifier,true,$orgCode,$this->domainName)
                        ];
                        $newSubjectsToSave[] = $newSubject;
                        $newSubjectSaved = $this->subjectRepository->createModelInstanceInMemory($newSubject);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $subjectsAlreadySaved->push($newSubjectSaved);
                    }
                }
            }
            $this->subjectRepository->saveMultiple($newSubjectsToSave);
        }
    }

    private function processAndSaveCaseLicensesData() {
        $licensesToSave = $this->getCaseLicensesToSave();
        $licensesAlreadySaved = $this->getSavedLicenses();
        $orgCode = $this->getOrgCode();
        if(!empty($licensesToSave)) {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newLicensesToSave = [];
            foreach($licensesToSave as $licenseToSave) {
                if(!empty($licenseToSave["title"])){
                    $licenseTitleToSearchWith = $licenseToSave["title"];
                    $savedLicenseCollection = $licensesAlreadySaved->where("title", $licenseTitleToSearchWith);
                    $this->setOrgCode($organizationId);
                    // if license doesn't exists in the db then only save it and update the list of saved licenses
                    if($savedLicenseCollection->isEmpty()) {
                        $licenseToSaveIdentifier = $licenseToSave["identifier"];
                        $newLicenseIdentifier = $this->createUniversalUniqueIdentifier();
                        $newLicenseTitle = $licenseTitleToSearchWith;
                        $newLicenseDescription = !empty($licenseToSave["description"]) ? $licenseToSave["description"] : "";
                        $newLicenseText = !empty($licenseToSave["licenseText"]) ? $licenseToSave["licenseText"] : "";
                        $lastChangeDateTime = !empty($licenseToSave["lastChangeDateTime"]) ? 
                                              $this->formatDateTime($licenseToSave["lastChangeDateTime"]) : null;
                        $newLicense = [
                            "license_id" => $newLicenseIdentifier, 
                            "organization_id" => $organizationId,
                            "title" => $newLicenseTitle,
                            "description" => $newLicenseDescription,
                            "license_text" => $newLicenseText,
                            "updated_at" => $lastChangeDateTime,
                            "source_license_id" => $licenseToSaveIdentifier,
                            "uri" => $this->getCaseApiUri("CFLicenses", $newLicenseIdentifier,true,$orgCode,$this->domainName)
                        ];
                        $newLicensesToSave[] = $newLicense;
                        $newLicenseSaved = $this->licenseRepository->createModelInstanceInMemory($newLicense);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $licensesAlreadySaved->push($newLicenseSaved);
                    }
                }
            }
            $this->licenseRepository->saveMultiple($newLicensesToSave);
        }
    }

    private function processAndSaveCaseConceptsData() {
        $conceptsToSave = $this->getCaseConceptsToSave();
        $conceptsAlreadySaved = $this->getSavedConcepts();
        if(!empty($conceptsToSave)) {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newConceptsToSave = [];
            foreach($conceptsToSave as $conceptToSave) {
                if(!empty($conceptToSave["title"])){
                    $conceptTitleToSearchWith = $conceptToSave["title"];
                    $savedConceptCollection = $conceptsAlreadySaved->where("title", $conceptTitleToSearchWith);
                    // if concept doesn't exists in the db then only save it and update the list of saved concepts
                    $this->setOrgCode($organizationId);
                    if($savedConceptCollection->isEmpty()) {
                        $conceptToSaveIdentifier = $conceptToSave["identifier"];
                        $orgCode =   $this->getOrgCode();
                        $newConceptIdentifier = $this->createUniversalUniqueIdentifier();
                        $newConceptKeywords = !empty($conceptToSave["keywords"]) ? $conceptToSave["keywords"] : "";
                        $newConceptTitle = $conceptTitleToSearchWith;
                        $newConceptHierarchyCode = !empty($conceptToSave["hierarchyCode"]) ? $conceptToSave["hierarchyCode"] : "";
                        $newConceptDescription = !empty($conceptToSave["description"]) ? $conceptToSave["description"] : "";
                        $lastChangeDateTime = !empty($conceptToSave["lastChangeDateTime"]) ? 
                                              $this->formatDateTime($conceptToSave["lastChangeDateTime"]) : null;
                        $newConcept = [
                            "concept_id" => $newConceptIdentifier, 
                            "organization_id" => $organizationId,
                            "keywords" => $newConceptKeywords,
                            "title" => $newConceptTitle,
                            "description" => $newConceptDescription,
                            "hierarchy_code" => $newConceptHierarchyCode,
                            "updated_at" => $lastChangeDateTime,
                            "source_concept_id" => $conceptToSaveIdentifier, 
                            "uri" => $this->getCaseApiUri("CFConcepts", $newConceptIdentifier,true,$orgCode,$this->domainName)
                        ];
                        $newConceptsToSave[] = $newConcept;
                        $newConceptSaved = $this->conceptRepository->createModelInstanceInMemory($newConcept);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $conceptsAlreadySaved->push($newConceptSaved);
                    }
                }
            }
            $this->conceptRepository->saveMultiple($newConceptsToSave);
        }
    }

    private function processAndSaveCaseAssociationGroupingsData() {
        $associationGroupingsToSave = $this->getCaseAssociationGroupsToSave();
        $associationGroupingsAlreadySaved = $this->getSavedAssociationGroups();
        $orgCode =   $this->getOrgCode();
        if(!empty($associationGroupingsToSave)) {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newAssociationGroupingsToSave = [];
            foreach($associationGroupingsToSave as $associationGroupingToSave) {
                if(!empty($associationGroupingToSave["title"])){
                    $this->setOrgCode($organizationId);
                    $associationGroupingTitleToSearchWith = $associationGroupingToSave["title"];
                    $savedAssociationGroupingCollection = $associationGroupingsAlreadySaved->where("title", $associationGroupingTitleToSearchWith);
                    // if association group doesn't exists in the db then only save it and update the list of saved association groups
                    if($savedAssociationGroupingCollection->isEmpty()) {
                        $associationGroupingToSaveIdentifier = $associationGroupingToSave["identifier"];
                        $newAssociationGroupingIdentifier = $this->createUniversalUniqueIdentifier();
                        $newAssociationGroupingTitle = $associationGroupingTitleToSearchWith;
                        $newAssociationGroupingDescription = !empty($associationGroupingToSave["description"]) ? $associationGroupingToSave["description"] : "";
                        $lastChangeDateTime = !empty($associationGroupingToSave["lastChangeDateTime"]) ? 
                                              $this->formatDateTime($associationGroupingToSave["lastChangeDateTime"]) : null;
                        $newAssociationGrouping = [
                            "association_group_id" => $newAssociationGroupingIdentifier, 
                            "organization_id" => $organizationId,
                            "title" => $newAssociationGroupingTitle,
                            "description" => $newAssociationGroupingDescription,
                            "updated_at" => $lastChangeDateTime,
                            "source_association_group_id" => $associationGroupingToSaveIdentifier, 
                            "uri" => $this->getCaseApiUri("CFAssociationGroupings", $newAssociationGroupingIdentifier,true,$orgCode,$this->domainName)
                        ];
                        $newAssociationGroupingsToSave[] = $newAssociationGrouping;
                        $newAssociationGroupingSaved = $this->associationGroupRepository->createModelInstanceInMemory($newAssociationGrouping);
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $associationGroupingsAlreadySaved->push($newAssociationGroupingSaved);
                    }
                }
            }
            $this->associationGroupRepository->saveMultiple($newAssociationGroupingsToSave);
        }
    }

    private function processAndSaveCaseItemTypesData() {
        $itemTypesToSave = $this->getCaseItemTypesToSave();
        $itemTypesAlreadySaved = $this->getSavedNodeTypes();
        $requestUserDetails = $this->getRequestingUser();
        $requestUserId = $requestUserDetails["user_id"];
        $currentDateTime = now()->ToDateTimeString();
        if(!empty($itemTypesAlreadySaved)) {
            $organizationId = $this->getRequestingUserOrganizationId();
            $defaultItemNodeTypeMetadataIds = $this->getDefaultItemNodeTypeMetadataIds();
            //dd($defaultItemNodeTypeMetadataIds);
            $newItemTypesToSave = [];
            $newItemTypeInstancesToMapMetadata = [];

            foreach($itemTypesToSave as $itemTypeToSave) {
                if(!empty($itemTypeToSave["title"])){
                    $itemTypeTitleToSearchWith = $itemTypeToSave["title"];
                    $itemTypeCodeToSearchWith = !empty($itemTypeToSave["typeCode"]) ? $itemTypeToSave["typeCode"] : "";
                    $orgCode =   $this->getOrgCode();
                    // $savedItemTypeCollection = $itemTypesAlreadySaved->
                    //                            filter(function ($item, $key) use ($itemTypeTitleToSearchWith, $itemTypeCodeToSearchWith) {
                    //                                 return $item->title===$itemTypeTitleToSearchWith || $item->type_code===$itemTypeCodeToSearchWith;
                    //                            });
                    $this->setOrgCode($organizationId);
                    $savedItemTypeCollection = $itemTypesAlreadySaved->where("title", $itemTypeTitleToSearchWith);

                    if($savedItemTypeCollection->isEmpty()){
                        $itemTypeToSaveIdentifier = $itemTypeToSave["identifier"];
                        $newItemTypeIdentifier = $this->createUniversalUniqueIdentifier();
                        $newItemTypeTitle = $itemTypeTitleToSearchWith;
                        $newItemTypeCode = $itemTypeCodeToSearchWith;
                        $newItemTypeDescription = !empty($itemTypeToSave["description"]) ? $itemTypeToSave["description"] : "";
                        $newItemTypeHierarchyCode = !empty($itemTypeToSave["hierarchyCode"]) ? $itemTypeToSave["hierarchyCode"] : "";
                        $lastChangeDateTime = !empty($itemTypeToSave["lastChangeDateTime"]) ? 
                                              $this->formatDateTime($itemTypeToSave["lastChangeDateTime"]) : null;
                        $newItemType = [
                            "node_type_id" => $newItemTypeIdentifier, 
                            "title" => $newItemTypeTitle,
                            "organization_id" => $organizationId,
                            "description" => $newItemTypeDescription,
                            "hierarchy_code" => $newItemTypeHierarchyCode,
                            "type_code" => $newItemTypeCode,
                            "updated_at" => $lastChangeDateTime,
                            "source_node_type_id" => $itemTypeToSaveIdentifier,
                            "created_by" => $requestUserId,
                            "created_at" => $currentDateTime,
                            "updated_at" => $currentDateTime,
                            "uri" => $this->getCaseApiUri("CFNodeType",$newItemTypeIdentifier,true,$orgCode,$this->domainName)
                        ];
                        $newItemTypesToSave[] = $newItemType;
                        $newItemTypeSaved = $this->nodeTypeRepository->createModelInstanceInMemory($newItemType);
                        $newItemTypeInstancesToMapMetadata[] = $newItemTypeSaved;
                        // update the existing list with the new one
                        //##note (update will occur at original collection reference)
                        $itemTypesAlreadySaved->push($newItemTypeSaved);
                    }                    

                }
            }

            // $this->nodeTypeRepository->saveMultiple($newItemTypesToSave);

            // convert to collection to insert records as chunk
            $recordsToSaveCollection = collect($newItemTypesToSave);
            
            foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
                $this->nodeTypeRepository->saveMultiple($chunk->toArray());
            }

            // save the new nodetype mapping with default item node metatdata ids
            foreach($newItemTypeInstancesToMapMetadata as $newItemType) {
                $newItemType->metadata()->attach($defaultItemNodeTypeMetadataIds);
            }

            $this->setNewCreatedNodeType($newItemTypesToSave);
        }

    }

    private function processAndSaveCaseDocumentData() {
        $documentToSave = $this->getCaseDocumentToSave();
        $defaultDocumentNodeType = $this->getDefaultDocumentNodeType();
        $requestUser = $this->getRequestingUser();
        $currentDateTime = now()->toDateTimeString();
        $isCloneStatus = $this->getImportAsCloneStatus();
        $importType = $this->getImportType();
        $title = !empty($documentToSave["title"]) ? 
                 ($isCloneStatus ? ($documentToSave["title"]." ".$currentDateTime) : $documentToSave["title"]) : 
                 "";
        $orgCode =   $this->getOrgCode();
        $organizationId = $this->getRequestingUserOrganizationId();
        $organizationName = DB::table('organizations')->select('name')->where('organization_id', $organizationId)->first()->name;
        $sourceDocumentId = $documentToSave["identifier"];
        $newDocumentIdentifier = $this->createUniversalUniqueIdentifier();
        $officialSourceUrl = !empty($documentToSave["officialSourceURL"]) ? $documentToSave["officialSourceURL"] : "";
        // if no adoption status is set, then insert 1 (DRAFT)
        $adoptionStatus = !empty($documentToSave["adoptionStatus"]) ? 
                            $this->getSystemSpecifiedAdoptionStatusNumber($documentToSave["adoptionStatus"]) : 
                            1;
        $notes = !empty($documentToSave["notes"]) ? $documentToSave["notes"] : "";
        $publisher =  !empty($documentToSave["publisher"]) ? $documentToSave["publisher"] : "";
        $description = !empty($documentToSave["description"]) ? $documentToSave["description"] : "";
        $version = !empty($documentToSave["version"]) ? $documentToSave["version"] : "";
        $statusStartDate = !empty($documentToSave["statusStartDate"]) ? $documentToSave["statusStartDate"] : null;
        $statusEndDate = !empty($documentToSave["statusEndDate"]) ? $documentToSave["statusEndDate"] : null;
        $creator = !empty($documentToSave["creator"]) ? $documentToSave["creator"] : $organizationName;
        $urlName = !empty($documentToSave["url_name"]) ? $documentToSave["url_name"] : "";
        $nodeTypeId = $defaultDocumentNodeType->node_type_id;
        $userId = $requestUser["user_id"];
        // $updatedAt = !empty($documentToSave["lastChangeDateTime"]) ? $this->formatDateTime($documentToSave["lastChangeDateTime"]) : null;
        // the above script enters the updated_at date from the json file and required date is of when the file was imported
        $updatedAt = date('Y-m-d H:i:s');                         
        $languageId =  !empty($documentToSave["language"]) ? 
                        $this->helperToFetchLanguageIdFromEitherExistingOrNew($documentToSave["language"]) : 
                        "";
        
        $licenseId = !empty($documentToSave['licenseURI']['identifier']) ? 
                     $this->helperToFetchLicenseIdFromEitherExistingOrNew($documentToSave['licenseURI']) : 
                     "";            
        $this->setOrgCode($organizationId);             
        $documentDataToSave = [
            "document_id" => $newDocumentIdentifier,
            "title" => $title,
            "official_source_url" => $officialSourceUrl,
            "adoption_status" => $adoptionStatus,
            "notes" => $notes,
            "publisher" =>  $publisher,
            "description" => $description,
            "version" => $version,
            "status_start_date" => $statusStartDate,
            "status_end_date" => $statusEndDate,
            "creator" => $creator,
            "url_name" => $urlName,
            "language_id" =>  $languageId,
            "license_id" => $licenseId,
            "organization_id" => $organizationId,
            "created_at" => $currentDateTime,
            "updated_at" => $updatedAt,
            "source_document_id"=>$sourceDocumentId,
            "node_type_id"=>$nodeTypeId,
            "updated_by" => $userId,
            "import_type" => $importType,
            "uri" => $documentToSave['uri']  
        ];

        $documentSaved = $this->documentRepository->saveData($documentDataToSave);
        $this->setSavedDocument($documentSaved);

    }

    private function processAndSaveDocumentSubjects() {
        $documentToSave = $this->getCaseDocumentToSave();
        // save document subject mapping if provided
        if(!empty($documentToSave["subjectURI"])) {
            $savedDocument = $this->getSavedDocument();
            $subjectIdentifiersToMapDocumentWith = [];
            foreach($documentToSave["subjectURI"] as $subject) {
                $subjectIdentifiersToMapDocumentWith[] = $this->helperToFetchSubjectIdFromEitherExistingOrNew($subject);
            }
            $savedDocument->subjects()->attach($subjectIdentifiersToMapDocumentWith);
        }
    }

    private function processAndSaveCaseItemsData() {
        $importType = $this->getImportType();
        $requestUser = $this->getRequestingUser();
        $userId = $requestUser["user_id"];
        $organizationId = $this->getRequestingUserOrganizationId();
        $currentDateTime = now()->toDateTimeString();
        $this->setOrgCode($organizationId);  
        $itemsToSave = $this->getCaseItemsToSave();
        $defaultItemNodeType = $this->getDefaultItemNodeType();
        $savedDocument = $this->getSavedDocument();
        $documentId = $savedDocument->document_id;
        
        $sourceConceptUriObject     =   '';
        $sourceNodeTypeUriObject    =   '';
        $sourceLicenseUriObject     =   '';

        $dataToSave = [];
        foreach($itemsToSave as $data) {
            $itemId = $this->createUniversalUniqueIdentifier();
            $sourceItemId = $data["identifier"];
            $fullStatement = !empty($data["fullStatement"]) ? $data["fullStatement"] : "";
            $alternativeLabel = !empty($data["alternativeLabel"]) ? $data["alternativeLabel"] : "";
            $humanCodingScheme = !empty($data["humanCodingScheme"]) ? $data["humanCodingScheme"] : "";
            $listEnumeration = !empty($data["listEnumeration"]) ? $data["listEnumeration"] : "";
            $abbreviatedStatement = !empty($data["abbreviatedStatement"]) ? $data["abbreviatedStatement"] : "";
            $notes = !empty($data["notes"]) ? $data["notes"] : "";
            $orgCode =   $this->getOrgCode();
            $educationLevel = !empty($data["educationLevel"]) ? is_array($data["educationLevel"]) ? $this->arrayContentToCommaeSeparatedString($data["educationLevel"]) : $data["educationLevel"] : "";
            $statusStartDate = !empty($data["statusStartDate"]) ? $data["statusStartDate"] : null;
            $statusEndDate = !empty($data["statusEndDate"]) ? $data["statusEndDate"] : null;
            $updatedAt = !empty($data["lastChangeDateTime"]) ? $this->formatDateTime($data["lastChangeDateTime"]) : null;
            
            $languageId = !empty($data["language"]) ? $this->helperToFetchLanguageIdFromEitherExistingOrNew($data["language"]) : "";
           
            $licenseId = !empty($data['licenseURI']['identifier']) ? 
                         $this->helperToFetchLicenseIdFromEitherExistingOrNew($data['licenseURI']) : 
                         "";
            
            $conceptId = !empty($data["conceptKeywordsURI"]) ? 
                         $this->helperToFetchConceptIdFromEitherExistingOrNew($data["conceptKeywordsURI"]) : 
                         "";

            $itemTypeId = !empty($data["CFItemTypeURI"]) ? 
                          $this->helperToFetchItemTypeIdFromEitherExistingOrNew($data["CFItemTypeURI"]) : 
                          $defaultItemNodeType->node_type_id;

            //
            if($importType == 2) {
                $sourceLicenseObject    = $this->helperToFetchSourceLicenseIdFromEitherExistingOrNew($licenseId);
                
                if(!empty($sourceLicenseObject)){
                    $sourceLicenseUriObject = json_encode(
                        [
                            "identifier"    =>  $sourceLicenseObject->source_license_id,
                            "uri"           =>  $sourceLicenseObject->uri,
                            "title"         =>  $sourceLicenseObject->title
                        ]
                    );
                }
                

                $sourceNodeTypeObject        =  $this->helperToFetchSourceItemTypeIdFromEitherExistingOrNew($itemTypeId);
                if(!empty($sourceNodeTypeObject)) {
                    $sourceNodeTypeUriObject = json_encode(
                        [
                            "identifier"    =>  $sourceNodeTypeObject->source_node_type_id,
                            "uri"           =>  $sourceNodeTypeObject->uri,
                            "title"         =>  $sourceNodeTypeObject->title
                        ]
                    );
                }

                $sourceConceptObject        =  $this->helperToFetchSourceConceptIdFromEitherExistingOrNew($itemTypeId);
                if(!empty($sourceConceptObject)) {
                    $sourceConceptUriObject = json_encode(
                        [
                            "identifier"    =>  $sourceConceptObject->source_concept_id,
                            "uri"           =>  $sourceConceptObject->uri,
                            "title"         =>  $sourceConceptObject->keywords
                        ]
                    );
                }
                
            }else {
                $sourceLicenseUriObject = !empty($data['licenseURI']) ? json_encode(
                    $data['licenseURI']
                ) : "";

                $sourceNodeTypeUriObject        =  !empty($data['CFItemTypeURI']) ? json_encode($data['CFItemTypeURI']): "" ;

                $sourceConceptUriObject        =  !empty($data['conceptKeywordsURI']) ? json_encode($data['conceptKeywordsURI']): "" ;
            }
            //              

            $itemToSave = [
                "item_id" => $itemId,
                "parent_id" => "",
                "document_id" => $documentId,
                "full_statement" => $fullStatement,
                "alternative_label" => $alternativeLabel,
                "human_coding_scheme" => $humanCodingScheme,
                "list_enumeration" => $listEnumeration,
                "abbreviated_statement" => $abbreviatedStatement,
                "notes" => $notes,
                "node_type_id" => $itemTypeId,
                "source_node_type_uri_object"   => $sourceNodeTypeUriObject,
                "concept_id" => $conceptId,
                "source_concept_uri_object"     => $sourceConceptUriObject,
                "language_id" => $languageId,
                "education_level" => $educationLevel,
                "license_id" => $licenseId,
                "source_license_uri_object"     => $sourceLicenseUriObject,
                "status_start_date" => $statusStartDate,
                "status_end_date" => $statusEndDate,
                "created_at" => $currentDateTime,
                "updated_at" => $updatedAt,
                "organization_id" => $organizationId,
                "source_item_id" => $sourceItemId,
                "updated_by" => $userId,
                "uri" => $data['uri']
            ];

            $dataToSave[] = $itemToSave;
            // $savedItems[] = $this->itemRepository->createModelInstanceInMemory($itemToSave);
        }

        // set the saved items collection first
        $this->setSavedItems($dataToSave);

        // search for parent id of each item to be saved and update the dataToSave array accordingly
        $documentSourceId = $this->getCaseDocumentToSave()["identifier"];
        $updatedItemDataToSave = $this->helperToUpdateTheItemDataToSaveWithParentId($dataToSave, $documentSourceId, $documentId);

        // convert to collection to insert records as chunk
        $recordsToSaveCollection = collect($updatedItemDataToSave);
        foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
            $this->itemRepository->saveMultiple($chunk->toArray());
        }
    }

    /* private function processAndSaveCaseAssociationsData() {
        $reverseAssociationTypeArray        =   ['3','5','7'];
        $requestUser                        =   $this->getRequestingUser();
        $userId                             =   $requestUser["user_id"];
        $organizationId                     =   $this->getRequestingUserOrganizationId();
        $currentDateTime                    =   now()->toDateTimeString();
        $caseAssociatonsFromJson            =   $this->getCaseAssociationsToSave();
        $originalPackageOldNewItemMapping   =   $this->getOldNewItemMapping();
        $isCloneStatus                      =   $this->getImportAsCloneStatus();
        $savedDocument                      =   $this->getSavedDocument();
        $documentId                         =   $savedDocument->document_id;
        $reverseDocumentId                  =   '';

        $caseAssociationsToSave = [];

        // only applicable during cloning
        // $originalSourceItemIds = [];
        // $clonedAssociatonsToSave = [];

        //dd($caseAssociatonsFromJson);
        foreach($caseAssociatonsFromJson as $association) {
            
            $destinationNodeId      =   "";
            $destinationDocumentId  =   "";

            $originNodeId           =   !empty($association["originNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["originNodeURI"]['identifier']) : 
                "";
            
            if($originNodeId!=="") {
                if(!in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $sourceItemAssociationId = $association["identifier"];
                    $itemAssociationId = $this->createUniversalUniqueIdentifier();
                    $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                    $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                        $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                        
                    
                    if(!empty($association["destinationNodeURI"]['identifier'])) {
                        $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];

                        // search inside the imported taxonomy's saved items
                        $destinationNodeId = $this->helperToFetchIdFromAlreadySavedItems($destinationNodeIdToSearchWith);

                        // if not found then search inside all items that is stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);

                        }

                        // if not found for any item related search, then search for any document stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchForAndReturnAnyDocumentId($destinationNodeIdToSearchWith);
                        }
                    }

                    // by default set document id when destinationNodeId is not found and the relation type is childof
                    if($association["associationType"]==="isChildOf" && empty($destinationNodeId)) {
                        $destinationNodeId      =   $documentId;
                    }
                    
                    $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                    if(!empty($association["destinationNodeURI"]["uri"])) {
                        $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                    }
                    else {
                        $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"] : "";
                    }
                    $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";

                    // set external details for exact match of relationship only
                    if($associationType===4) {
                        $externalNodeTitle = !empty($association["originNodeURI"]['title']) ? $association["originNodeURI"]['title'] : "";
                        $externalNodeUrl = !empty($association["originNodeURI"]['uri']) ? $association["originNodeURI"]['uri'] : "";
                        // ??confusion why empty?
                        // $destinationNodeId = "";
                    }
                    // set destination external details for exemplar relationship
                    else if($associationType===8 && empty($externalNodeUrl)) {
                        $externalNodeUrl = !empty($association["destinationNodeURI"]["uri"]) ? $association["destinationNodeURI"]["uri"] : "";
                    }
                    $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;

                    if(!empty($destinationNodeId)) {
                        $returnCollection = true;
                        $fieldsToReturn = ["*"];
                        $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                        $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, 
                                            $fieldsToReturn, 
                                            $dataFetchingConditionalClause
                                        )->first();

                        if(is_array($destinationNodeDetail) && sizeOf($destinationNodeDetail) > 0) {
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }
                    }
                    
                    $caseAssociationsToSave[] = [
                        "item_association_id"           => $itemAssociationId,
                        "association_type"              => $associationType,
                        "document_id"                   => $documentId,
                        "association_group_id"          => $associationGroupId,
                        "origin_node_id"                => $originNodeId,
                        "destination_node_id"           => $destinationNodeId,
                        "destination_document_id"       => $documentId,
                        "sequence_number"               => $sequenceNumber,
                        "external_node_title"           => $externalNodeTitle,
                        "external_node_url"             => $externalNodeUrl,
                        "created_at"                    => $currentDateTime,
                        "updated_at"                    => $updatedAt,
                        "source_item_association_id"    => $sourceItemAssociationId,
                        "organization_id"               => $organizationId
                    ];

                    // if package to be imported is a clone then create a new association with exact match of relationship
                    if($isCloneStatus) {
                        $newAssociationIdentifier = $this->createUniversalUniqueIdentifier();
                        foreach($originalPackageOldNewItemMapping as $originalSourceItemId => $originalOldNewMap) {
                            $newlyCreatedItemIdentifierDuringCloning = $originalOldNewMap["identifier"];
                            $sourceItemIdentifier = $association["originNodeURI"]['identifier'];
                            if($sourceItemIdentifier===$newlyCreatedItemIdentifierDuringCloning) {
                                $externalNodeTitle = !empty($originalOldNewMap["title"]) ? $originalOldNewMap["title"] : "";
                                $externalNodeUrl= !empty($originalOldNewMap["uri"]) ? 
                                                str_replace($sourceItemIdentifier, $originalSourceItemId, $originalOldNewMap["uri"])
                                                : "";

                                $returnCollection = true;
                                $fieldsToReturn = ["*"];
                                $dataFetchingConditionalClause = [ "item_id" =>  $originalSourceItemId];
                                $relations      =   ['nodeType'];
                                $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                    $returnCollection, 
                                                    $fieldsToReturn, 
                                                    $dataFetchingConditionalClause,$relations
                                                )->first();
                                // $originalSourceItemIds[] = $originalSourceItemId;
                                // $clonedAssociatonsToSave[$originalSourceItemId]
                                $caseAssociationsToSave[]  = [
                                    "item_association_id" => $newAssociationIdentifier,
                                    "association_type" => 4, // for exact match of relationship
                                    "document_id" => $documentId,
                                    "association_group_id" => $associationGroupId,
                                    "origin_node_id" => $originNodeId,
                                    "destination_node_id" => $originalSourceItemId,
                                    "destination_document_id" => $documentId,
                                    "sequence_number" => $sequenceNumber++,
                                    "external_node_title" => $externalNodeTitle,
                                    "external_node_url" => $externalNodeUrl,
                                    "created_at" => $currentDateTime,
                                    "updated_at" => $updatedAt,
                                    "source_item_association_id" => $newAssociationIdentifier,
                                    "organization_id" => $organizationId
                                ];
                                unset($originalPackageOldNewItemMapping[$originalSourceItemId]);
                                break;
                            }
                        }
                    }
                } else {
                    
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $sourceItemAssociationId = $association["identifier"];
                        $itemAssociationId = $this->createUniversalUniqueIdentifier();
                        $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                            $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        
                        if(!empty($association["destinationNodeURI"]['identifier'])) {
                            $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];

                            // search inside the imported taxonomy's saved items
                            $destinationNodeId = $this->helperToFetchIdFromAlreadySavedItems($destinationNodeIdToSearchWith);

                            // if not found then search inside all items that is stored in the system
                            if(empty($destinationNodeId)) {
                                $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);
                            }
                        }    

                        //echo $destinationNodeId;
                        if(!empty($destinationNodeId)) {
                            $returnCollection = true;
                            $fieldsToReturn = ["*"];
                            $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                            $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                $returnCollection, 
                                                $fieldsToReturn, 
                                                $dataFetchingConditionalClause
                                            )->first();
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }

                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                        if(!empty($association["destinationNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"] : "";
                        }
                        $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";

                        // set external details for exact match of relationship only
                        if($associationType===4) {
                            $externalNodeTitle = !empty($association["originNodeURI"]['title']) ? $association["originNodeURI"]['title'] : "";
                            $externalNodeUrl = !empty($association["originNodeURI"]['uri']) ? $association["originNodeURI"]['uri'] : "";
                            // ??confusion why empty?
                            // $destinationNodeId = "";
                        }
                        // set destination external details for exemplar relationship
                        else if($associationType===8 && empty($externalNodeUrl)) {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]["uri"]) ? $association["destinationNodeURI"]["uri"] : "";
                        }
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
        

                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $destinationNodeId,
                            "destination_document_id"       => $documentId,
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId
                        ];
                    } else {
                        $sourceItemAssociationId    =   $association["identifier"];
                        $itemAssociationId          =   $this->createUniversalUniqueIdentifier();
                        $associationType            =   $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId         = !empty($association["CFAssociationGroupingURI"]) ?    $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        $destinationId = "";
                        
                        if(!empty($association["destinationNodeURI"]['identifier'])) {
                           
                            $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];
                            
        
                            $destinationId   =   $this->helperToSearchAndReturnReverseDestinationNodeItem($destinationNodeIdToSearchWith);

                            if(empty($destinationId)) {
                                $destinationId = $destinationNodeIdToSearchWith;
                            }
        
                        }  
                        
                        $destinationDocumentId     =   $this->helperToSearchAndReturnReverseDocumentNode($destinationNodeIdToSearchWith);
                        if(empty($destinationDocumentId)) {
                            $destinationDocumentId = $documentId;
                        }
        
                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;
        
                        if(!empty($association["destinationNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"] : "";
                        }
                        $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";
        
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
        
                        //dd($destinationNodeDetail);
                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $destinationId,
                            "destination_document_id"       => $documentId,
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId
                        ];
                    }
                }
            }
            else {
                if(in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $sourceItemAssociationId    = $association["identifier"];
                        $itemAssociationId          = $this->createUniversalUniqueIdentifier();
                        $associationType            = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId         = !empty($association["CFAssociationGroupingURI"]) ? 
                                            $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        
                        if(!empty($association["originNodeURI"]['identifier'])) {
                            $originNodeIdToSearchWith = $association["originNodeURI"]['identifier'];

                            
                            $originNodeId   =   $this->helperToSearchAndReturnOriginNodeItem($originNodeIdToSearchWith);

                            if(empty($originNodeId)) {
                                $originNodeId = $association["originNodeURI"]["identifier"];
                            } 
                        }

                        if(!empty($destinationNodeId)) {
                            $returnCollection = true;
                            $fieldsToReturn = ["*"];
                            $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                            $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                $returnCollection, 
                                                $fieldsToReturn, 
                                                $dataFetchingConditionalClause
                                            )->first();
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        } 
                        

                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                        if(!empty($association["originNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["originNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["originNodeURI"]) ? $association["originNodeURI"] : "";
                        }
                        $externalNodeTitle = !empty($association["originNodeURI"]["title"]) ? $association["originNodeURI"]["title"] : "";

                        
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
                        
                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $association["CFDocumentURI"]["identifier"],
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $destinationNodeId,
                            "destination_document_id"       => $documentId,
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId
                        ];
                    }
                }
            }  
        }

        //print_r($caseAssociationsToSave);
        //dd();
        // convert to collection to insert records as chunk
        $recordsToSaveCollection = collect($caseAssociationsToSave);
        foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
            $this->itemAssociationRepository->saveMultiple($chunk->toArray());
        }
    } */

    private function processAndSaveCaseAssociationsData() {
        $exactMatchOfAssociationDocumentId  =   '';
        $reverseAssociationTypeArray        =   ['3','5','7'];
        $requestImportType  =   $this->getImportType();
        $packageExists      =   $this->getPackageExists();
        $requestUser        =   $this->getRequestingUser();
        $userId             =   $requestUser["user_id"];
        $organizationId     =   $this->getRequestingUserOrganizationId();
        $currentDateTime    =   now()->toDateTimeString();

        $caseAssociatonsFromJsonSave        =   $this->getCaseAssociationsToSave();
        $originalPackageOldNewItemMapping   =   $this->getOldNewItemMapping();
        $savedDocument                      =   $this->getSavedDocument();
        $savedItems                         =   $this->getSavedItems();
        
        $documentId             =   $savedDocument->document_id;
        $caseAssociationsToSave =   [];
        $itemIdSavedArray       =   [];
        $reverseDocumentId      =   '';

        $exactMatchOfAssociationDocumentId  =   $documentId;
        $itemAttributes =   ['document_id' => $exactMatchOfAssociationDocumentId, "is_deleted" => '0'];
        $itemExistData  =   $this->itemRepository->findByAttributes($itemAttributes);

        foreach($itemExistData as $itemSaved) {
            $itemIdSavedArray[$itemSaved->source_item_id] = $itemSaved->item_id;
        }

        foreach($caseAssociatonsFromJsonSave as $association) {
            $destinationNodeId      =   "";
            $destinationDocumentId  =   "";
            
            $originNodeId = !empty($association["originNodeURI"]["identifier"]) ?
            $this->helperToFetchIdFromAlreadySavedItems($association["originNodeURI"]['identifier']) : 
            "";
            if($originNodeId!=="") {
                if(!in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $sourceItemAssociationId = $association["identifier"];
                    $itemAssociationId = $this->createUniversalUniqueIdentifier();
                    $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                    $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                        $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                        
                    
                    if(!empty($association["destinationNodeURI"]['identifier'])) {
                        $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];

                        // search inside the imported taxonomy's saved items
                        $destinationNodeId = $this->helperToFetchIdFromAlreadySavedItems($destinationNodeIdToSearchWith);

                        // if not found then search inside all items that is stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);

                        }

                        // if not found for any item related search, then search for any document stored in the system
                        if(empty($destinationNodeId) && $association["associationType"]!=="isChildOf") {
                            $destinationNodeId = $this->helperToSearchForAndReturnAnyDocumentId($destinationNodeIdToSearchWith);
                        }
                    }

                    // by default set document id when destinationNodeId is not found and the relation type is childof
                    if($association["associationType"]==="isChildOf" && empty($destinationNodeId)) {
                        $destinationNodeId      =   $documentId;
                    }
                    
                    $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                    if(!empty($association["destinationNodeURI"]["uri"])) {
                        $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                    }
                    else {
                        $externalNodeUrl = !empty($association["destinationNodeURI"]['uri']) ? $association["destinationNodeURI"] : "";
                    }
                    $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";

                    // set external details for exact match of relationship only
                    if($associationType===4) {
                        $externalNodeTitle = !empty($association["destinationNodeURI"]['title']) ? $association["destinationNodeURI"]['title'] : "";
                        $externalNodeUrl = !empty($association["destinationNodeURI"]['uri']) ? $association["destinationNodeURI"]['uri'] : "";
                        // ??confusion why empty?
                        // $destinationNodeId = "";
                    }
                    // set destination external details for exemplar relationship
                    else if($associationType===8 && empty($externalNodeUrl)) {
                        $externalNodeUrl = !empty($association["destinationNodeURI"]["uri"]) ? $association["destinationNodeURI"]["uri"] : "";
                    }
                    $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;

                    if(!empty($destinationNodeId)) {
                        $returnCollection = true;
                        $fieldsToReturn = ["*"];
                        $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                        $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                            $returnCollection, 
                                            $fieldsToReturn, 
                                            $dataFetchingConditionalClause
                                        )->first();

                        if(is_array($destinationNodeDetail) && sizeOf($destinationNodeDetail) > 0) {
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }
                    }
                    
                    $caseAssociationsToSave[] = [
                        "item_association_id"           => $itemAssociationId,
                        "association_type"              => $associationType,
                        "document_id"                   => $documentId,
                        "association_group_id"          => $associationGroupId,
                        "origin_node_id"                => $originNodeId,
                        "destination_node_id"           => $destinationNodeId,
                        "destination_document_id"       => $documentId,
                        "sequence_number"               => $sequenceNumber,
                        "external_node_title"           => $externalNodeTitle,
                        "external_node_url"             => $externalNodeUrl,
                        "created_at"                    => $currentDateTime,
                        "updated_at"                    => $updatedAt,
                        "source_item_association_id"    => $sourceItemAssociationId,
                        "organization_id"               => $organizationId,
                        "is_reverse_association"        => 0,
//                        "description"                   => $this->getDescription($originNodeId,$sourceItemAssociationId)
                    ];

                    // if package to be imported is a clone then create a new association with exact match of relationship
                    if($requestImportType == 2 && $associationType == 1) {
                        $newAssociationIdentifier = $this->createUniversalUniqueIdentifier();
                        $sourceItemIdentifier = $association["originNodeURI"]['identifier'];
                        $externalNodeTitle  = !empty($association["destinationNodeURI"]['title']) ? $association["originNodeURI"]['title'] : "";
                        $externalNodeUrl    = !empty($association["destinationNodeURI"]['uri']) ? 
                                        $association["destinationNodeURI"]['uri'] : "";
                    
                        $caseAssociationsToSave[]  = [
                            "item_association_id"           => $newAssociationIdentifier,
                            "association_type"              => 4, // for exact match of relationship
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $itemIdSavedArray[$association["originNodeURI"]['identifier']],
                            "destination_document_id"       => $exactMatchOfAssociationDocumentId,
                            "sequence_number"               => $sequenceNumber++,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $newAssociationIdentifier,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 0
                        ];
                    }
                } else {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $sourceItemAssociationId = $association["identifier"];
                        $itemAssociationId = $this->createUniversalUniqueIdentifier();
                        $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                            $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                        
                        // if not found then search inside all items that is stored in the system
                        if(empty($destinationNodeId)) {
                            $destinationNodeId = $this->helperToSearchAndReturnDestinationNodeItem($destinationNodeIdToSearchWith);
                        }

                        //echo $destinationNodeId;
                        if(!empty($destinationNodeId)) {
                            $returnCollection = true;
                            $fieldsToReturn = ["*"];
                            $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                            $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                $returnCollection, 
                                                $fieldsToReturn, 
                                                $dataFetchingConditionalClause
                                            )->first();
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        }

                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                        if(!empty($association["destinationNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"] : "";
                        }
                        $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";

                        // set external details for exact match of relationship only
                        if($associationType===4) {
                            $externalNodeTitle = !empty($association["originNodeURI"]['title']) ? $association["originNodeURI"]['title'] : "";
                            $externalNodeUrl = !empty($association["originNodeURI"]['uri']) ? $association["originNodeURI"]['uri'] : "";
                            // ??confusion why empty?
                            // $destinationNodeId = "";
                        }
                        // set destination external details for exemplar relationship
                        else if($associationType===8 && empty($externalNodeUrl)) {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]["uri"]) ? $association["destinationNodeURI"]["uri"] : "";
                        }
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
        

                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $destinationNodeId,
                            "destination_document_id"       => $documentId,
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 0
                        ];
                    } else {
                        $sourceItemAssociationId    =   $association["identifier"];
                        $itemAssociationId          =   $this->createUniversalUniqueIdentifier();
                        $associationType            =   $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId         = !empty($association["CFAssociationGroupingURI"]) ?    $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        $destinationId = "";
                        
                        if(!empty($association["destinationNodeURI"]['identifier'])) {
                            
                            $destinationNodeIdToSearchWith = $association["destinationNodeURI"]['identifier'];
                            
        
                            $destinationId   =   $this->helperToSearchAndReturnReverseDestinationNodeItem($destinationNodeIdToSearchWith);

                            if(empty($destinationId)) {
                                $destinationId = $destinationNodeIdToSearchWith;
                            }
        
                        }  
                        
                        $destinationDocumentId     =   $this->helperToSearchAndReturnReverseDocumentNode($destinationNodeIdToSearchWith);
                        if(empty($destinationDocumentId)) {
                            $destinationDocumentId = $documentId;
                        }
        
                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;
        
                        if(!empty($association["destinationNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["destinationNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["destinationNodeURI"]) ? $association["destinationNodeURI"] : "";
                        }
                        $externalNodeTitle = !empty($association["destinationNodeURI"]["title"]) ? $association["destinationNodeURI"]["title"] : "";
        
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
        
                        //dd($destinationNodeDetail);
                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => $documentId,
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $destinationId,
                            "destination_document_id"       => $documentId,
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 0
                        ];
                    }
                }
                
            }
            else {
                if(in_array($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']), $reverseAssociationTypeArray)) {
                    $destinationNodeId    =   !empty($association["destinationNodeURI"]["identifier"]) ? $this->helperToFetchIdFromAlreadySavedItems($association["destinationNodeURI"]['identifier']) : 
                        "";

                    if($destinationNodeId!== "") {
                        $sourceItemAssociationId    = $association["identifier"];
                        $itemAssociationId          = $this->createUniversalUniqueIdentifier();
                        $associationType            = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
                        $associationGroupId         = !empty($association["CFAssociationGroupingURI"]) ? 
                                            $this->helperToFetchAssociationGroupIdFromEitherExistingOrNew($association["CFAssociationGroupingURI"]) : "";
                            
                        
                        if(!empty($association["originNodeURI"]['identifier'])) {
                            $originNodeIdToSearchWith = $association["originNodeURI"]['identifier'];

                            
                            $originNodeId   =   $this->helperToSearchAndReturnOriginNodeItem($originNodeIdToSearchWith);

                            if(empty($originNodeId)) {
                                $originNodeId = $association["originNodeURI"]["identifier"];
                            } 
                        }

                        if(!empty($destinationNodeId)) {
                            $returnCollection = true;
                            $fieldsToReturn = ["*"];
                            $dataFetchingConditionalClause = [ "item_id" =>  $destinationNodeId];
                            $destinationNodeDetail = $this->itemRepository->findByAttributesWithSpecifiedFields(
                                                $returnCollection, 
                                                $fieldsToReturn, 
                                                $dataFetchingConditionalClause
                                            )->first();
                            $destinationDocumentId  =   $destinationNodeDetail['document_id'];
                        } 
                        

                        $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;

                        if(!empty($association["originNodeURI"]["uri"])) {
                            $externalNodeUrl = $association["originNodeURI"]["uri"];
                        }
                        else {
                            $externalNodeUrl = !empty($association["originNodeURI"]) ? $association["originNodeURI"] : "";
                        }
                        $externalNodeTitle = !empty($association["originNodeURI"]["title"]) ? $association["originNodeURI"]["title"] : "";

                        
                        $updatedAt = !empty($association["lastChangeDateTime"]) ? $this->formatDateTime($association["lastChangeDateTime"]) : null;
                        
                        $caseAssociationsToSave[] = [
                            "item_association_id"           => $itemAssociationId,
                            "association_type"              => $associationType,
                            "document_id"                   => isset($association["CFDocumentURI"]["identifier"])?$association["CFDocumentURI"]["identifier"]:'',
                            "association_group_id"          => $associationGroupId,
                            "origin_node_id"                => $originNodeId,
                            "destination_node_id"           => $destinationNodeId,
                            "destination_document_id"       => $documentId,
                            "sequence_number"               => $sequenceNumber,
                            "external_node_title"           => $externalNodeTitle,
                            "external_node_url"             => $externalNodeUrl,
                            "created_at"                    => $currentDateTime,
                            "updated_at"                    => $updatedAt,
                            "source_item_association_id"    => $sourceItemAssociationId,
                            "organization_id"               => $organizationId,
                            "is_reverse_association"        => 1
                        ];
                    }
                }
            }            
        }
        // convert to collection to insert records as chunk
        $recordsToSaveCollection = collect($caseAssociationsToSave);
        foreach ($recordsToSaveCollection->chunk($this->recordChunkCount) as $chunk) {
            $this->itemAssociationRepository->saveMultiple($chunk->toArray());
        }
    }

    /************** All helper methods specific to this import functionality only **************/

    private function helperToFetchTenantSpecificDataOfProvidedType(string $type): Collection {
        $organizationId = $this->getRequestingUserOrganizationId();
        $dataFetchingConditionalClause = [ "organization_id" => $organizationId, "is_deleted" => 0];
        $returnCollection = true;
        switch ($type) {
            case 'concept':
                $fieldsToReturn = ['concept_id', 'title'];
                $repository = $this->conceptRepository;
                break;
            case 'subject':
                $fieldsToReturn = ['subject_id', 'title', 'source_subject_id'];
                $repository = $this->subjectRepository;
                break;
            case 'license':
                $fieldsToReturn = ['license_id', 'title', 'license_text'];
                $repository = $this->licenseRepository;
                break;
            case 'node':
                $fieldsToReturn = ['node_type_id', 'title', 'type_code', 'is_document', 'is_default','organization_id', 'is_deleted'];
                $repository = $this->nodeTypeRepository;
                break;
            case 'associationGroup':
                $fieldsToReturn = ['association_group_id', 'title'];
                $repository = $this->associationGroupRepository;
                break;
            case 'language':
                $fieldsToReturn = ['language_id', 'short_code'];
                $repository = $this->languageRepository;
                break;
            default:
                $repository = false;
                break;
        }
        $dataToReturn = $repository!==false ? 
                        $repository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $dataFetchingConditionalClause) : 
                        collect([]);
        return $dataToReturn;
    }

    private function helperToFetchLanguageIdFromEitherExistingOrNew(string $languageToSearchFor): string {
        $savedLanguages = $this->getSavedLanguages();
        $searchResult = $savedLanguages->where("short_code", $languageToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $language = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newLanguageIdentifier = $this->createUniversalUniqueIdentifier();
            $languageDataToSave = [
                "language_id" => $newLanguageIdentifier,
                "name" => ucwords($languageToSearchFor),
                "short_code" => $languageToSearchFor,
                "organization_id" => $organizationId
            ];
            // save record to db
            $language = $this->languageRepository->saveData($languageDataToSave);
            // update the exiting inmemory list
            $savedLanguages->push($language);
        }
        return $language->language_id;
    }

    private function helperToFetchLicenseIdFromEitherExistingOrNew(array $licenseToSearchFor): string {
        $savedLicenses = $this->getSavedLicenses();
        $licenseTitleToSearchFor = $licenseToSearchFor["title"];
        $searchResult = $savedLicenses->where("title", $licenseTitleToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $license = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newLicenseIdentifier = $this->createUniversalUniqueIdentifier();
            $licenseDataToSave = [
                "license_id" => $newLicenseIdentifier,
                "title" => $licenseTitleToSearchFor,
                "organization_id" => $organizationId,
                "source_license_id" => $newLicenseIdentifier
            ];
            // save record to db
            $license = $this->licenseRepository->saveData($licenseDataToSave);
            // update the exiting inmemory list
            $savedLicenses->push($license);
        }
        return $license->license_id;
    }

    private function helperToFetchSubjectIdFromEitherExistingOrNew(array $subjectToSearchFor): string {
        $savedSubjects= $this->getSavedSubjects();
        $subjectTitleToSearchFor = $subjectToSearchFor["title"];
        $searchResult = $savedSubjects->where("title", $subjectTitleToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $subject = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newSubjectIdentifier = $this->createUniversalUniqueIdentifier();
            $subjectDataToSave = [
                "subject_id" => $newSubjectIdentifier,
                "title" => $subjectTitleToSearchFor,
                "organization_id" => $organizationId,
                "source_subject_id" => $newSubjectIdentifier
            ];
            // save record to db
            $subject = $this->subjectRepository->saveData($subjectDataToSave);
            // update the exiting inmemory list
            $savedSubjects->push($subject);
        }
        return $subject->subject_id;
    }

    private function helperToFetchConceptIdFromEitherExistingOrNew(array $conceptToSearchFor): string {
        $savedConcepts = $this->getSavedConcepts();
        $conceptTitleToSearchFor = $conceptToSearchFor["title"];
        $searchResult = $savedConcepts->where("title", $conceptTitleToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $concept = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newConceptIdentifier = $this->createUniversalUniqueIdentifier();
            $conceptDataToSave = [
                "concept_id" => $newConceptIdentifier,
                "title" => $conceptTitleToSearchFor,
                "organization_id" => $organizationId,
                "source_concept_id" => $newConceptIdentifier
            ];
            // save record to db
            $concept = $this->conceptRepository->saveData($conceptDataToSave);
            // update the exiting inmemory list
            $savedConcepts->push($concept);
        }
        return $concept->concept_id;
    }

    private function helperToFetchItemTypeIdFromEitherExistingOrNew(array $itemTypeToSearchFor): string {
        $savedNodeTypes = $this->getSavedNodeTypes();
        $itemTypeTitleToSearchFor = $itemTypeToSearchFor["title"];
        $searchResult = $savedNodeTypes->where("title", $itemTypeTitleToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $itemType = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newItemTypeIdentifier = $this->createUniversalUniqueIdentifier();
            $itemTypeDataToSave = [
                "node_type_id" => $newItemTypeIdentifier,
                "title" => $itemTypeTitleToSearchFor,
                "organization_id" => $organizationId,
                "source_node_type_id" => $newItemTypeIdentifier
            ];
            // save record to db
            $itemType = $this->nodeTypeRepository->saveData($itemTypeDataToSave);
            // update the exiting inmemory list
            $savedNodeTypes->push($itemType);
        }
        return $itemType->node_type_id;
    }

    private function helperToUpdateTheItemDataToSaveWithParentId(array $itemsToSave, string $documentSourceId, string $savedDocumentId): array {
        if(!empty($itemsToSave)) {
            $caseChildOfAssociatons = $this->getChildOfCaseAssociations();
            //print_r($caseChildOfAssociatons);
            if(!empty($caseChildOfAssociatons)){
                // for each item iterate through the isChildOf associations
                foreach($itemsToSave as $itemKey=>$savedItem) {
                    // set empty parent id by default
                    $parentId="";
                    $itemIdentifierToSearchFor = $savedItem["source_item_id"];
                    // for each item search the case associations for parent id
                    foreach($caseChildOfAssociatons as $associationKey=>$associationEntity){
                        $originNodeIdentifier = $associationEntity['originNodeURI']['identifier'];
                        $destinationNodeIdentifier = $associationEntity["destinationNodeURI"]["identifier"];

                        if($itemIdentifierToSearchFor===$originNodeIdentifier){
                            // search the item list for parent
                            $newItemIdentifier = $this->helperToReturnNewItemIdentifier($destinationNodeIdentifier, $itemsToSave);
                            
                            if($newItemIdentifier!=="") {
                                $parentId = $newItemIdentifier;
                            }
                            else if($documentSourceId===$destinationNodeIdentifier){
                                $parentId = $savedDocumentId;
                            }
                            else {
                                $parentId = "";
                            }
                            // reduce the association data set with each successfull iteration to reduce the array size
                            unset($caseChildOfAssociatons[$associationKey]);
                            break;
                        }
                    }
                    // update the item's parent id
                    $itemsToSave[$itemKey]["parent_id"] = $parentId;
                }
            }
        }
        return $itemsToSave;
    }

    private function helperToReturnNewItemIdentifier(string $identifier, array $items): string {
        $newItemIdentifier = "";
        foreach($items as $item) {
            if($identifier===$item["source_item_id"]){
                $newItemIdentifier = $item["item_id"];
                break;
            }
        }
        return $newItemIdentifier;
    }

    private function helperToFetchAssociationGroupIdFromEitherExistingOrNew(array $associationGroupToSearchFor): string {
        $savedAssociationGroups = $this->getSavedAssociationGroups();
        $associationGroupTitleToSearchFor = $associationGroupToSearchFor["title"];
        $searchResult = $savedAssociationGroups->where("title", $associationGroupTitleToSearchFor);
        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $associationGroup = $searchResult->first();
        }
        // if not found in existing records, then create, save, merge with existing
        else {
            $organizationId = $this->getRequestingUserOrganizationId();
            $newAssociationGroupIdentifier = $this->createUniversalUniqueIdentifier();
            $associationGroupDataToSave = [
                "association_group_id" => $newAssociationGroupIdentifier,
                "title" => $associationGroupTitleToSearchFor,
                "organization_id" => $organizationId,
                "source_association_group_id" => $newAssociationGroupIdentifier
            ];
            // save record to db
            $associationGroup = $this->associationGroupRepository->saveData($associationGroupDataToSave);
            // update the exiting inmemory list
            $savedAssociationGroups->push($associationGroup);
        }
        return $associationGroup->association_group_id;
    }

    private function helperToFetchIdFromAlreadySavedItems(string $sourceItemId): string {
        $itemIdentifierToReturn = "";
        $savedItems = $this->getSavedItems();
        // get before save document
        $originalDocument = $this->getCaseDocumentToSave(); 
        $originalDocumentIdentifier = $originalDocument["identifier"];
        foreach($savedItems as $savedItem) {
            if($sourceItemId===$savedItem["source_item_id"]) {
                $itemIdentifierToReturn = $savedItem['item_id'];
                break;
            }
        }
        // if no match found for items, then match with document
        if($itemIdentifierToReturn==="" && $sourceItemId===$originalDocumentIdentifier){
            $savedDocument = $this->getSavedDocument();
            $itemIdentifierToReturn = $savedDocument->document_id;
        }
        return $itemIdentifierToReturn;
    }

    private function helperToReturnDestinationNodeIds(): array {
        $destinationNodeIds = [];
        $caseAssociationsToSave = $this->getCaseAssociationsToSave();
        foreach($caseAssociationsToSave as $association) {
            if(!empty($association["destinationNodeURI"]["identifier"])) {
                $destinationNodeIds[] = $association["destinationNodeURI"]["identifier"];
            }
        }
        return $destinationNodeIds;
    }

    private function helperToReturnOriginNodeIds(): array {
        $originNodeIds = [];
        $caseAssociationsToSave = $this->getCaseAssociationsToSave();
        foreach($caseAssociationsToSave as $association) {
            if(!empty($association["originNodeURI"]["identifier"])) {
                $originNodeIds[] = $association["originNodeURI"]["identifier"];
            }
        }
        return $originNodeIds;
    }

    private function helperToReturnReverseDestinationNodeIds(): array {
        $reverseDestinationNodeIds = [];
        $caseAssociationsToSave = $this->getCaseAssociationsToSave();
        foreach($caseAssociationsToSave as $association) {
            
            if($this->getSystemSpecifiedAssociationTypeNumber($association['associationType']) == 3 || $this->getSystemSpecifiedAssociationTypeNumber($association['associationType']) == 5 || $this->getSystemSpecifiedAssociationTypeNumber($association['associationType']) == 7) {
                if(!empty($association["originNodeURI"]["identifier"])) {
                    $reverseDestinationNodeIds[] = $association["originNodeURI"]["identifier"];
                }
            }
        }
        return $reverseDestinationNodeIds;
    }

    private function helperToSearchAndReturnDestinationNodeItem(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $destinationNodeItems = $this->getDestinationNodeItems();
        if($destinationNodeItems->isNotEmpty()) {
            $searchResult = $destinationNodeItems->where('source_item_id', $searchIdentifier);
            $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->item_id : "";
        }
        return $nodeIdToReturn;
    }

    private function helperToSearchAndReturnOriginNodeItem(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $originNodeItems = $this->getOriginNodeItems();
        if($originNodeItems->isNotEmpty()) {
            $searchResult = $originNodeItems->where('source_item_id', $searchIdentifier);
            $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->item_id : "";
        }
        return $nodeIdToReturn;
    }

    private function helperToSearchAndReturnReverseDestinationNodeItem(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $reverseDestinationNodeItems = $this->getReverseDestinationNodeItems();
        if($reverseDestinationNodeItems->isNotEmpty()) {
            $searchResult = $reverseDestinationNodeItems->where('source_item_id', $searchIdentifier);
            $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->item_id : "";
        }
        return $nodeIdToReturn;
    }

    private function helperToSearchAndReturnReverseDocumentNode(string $searchIdentifier){
        $documentIdToReturn = "";
        $destinationNodeItems = $this->getReverseDestinationNodeItems();
        //print_r($originNodeItems);
        if($destinationNodeItems->isNotEmpty()) {
            $searchResult = $destinationNodeItems->where('source_item_id', $searchIdentifier);
            $documentIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->document_id : "";
        }

        //echo $documentIdToReturn;
        return $documentIdToReturn;
    }

    private function helperToSearchForAndReturnAnyDocumentId(string $searchIdentifier): string {
        $nodeIdToReturn = "";
        $conditionalAttributes = [ "source_document_id" => $searchIdentifier ];
        $searchResult = $this->documentRepository->findByAttributes($conditionalAttributes);
        $nodeIdToReturn = $searchResult->isNotEmpty() ? $searchResult->first()->document_id : "";
        return $nodeIdToReturn;
    }

    private function helperToFetchSourceLicenseIdFromEitherExistingOrNew(string $licenseIdToSearchFor){
        $license            =   [];
        $requestImportType  =   $this->getImportType();
        $savedLicenses      =   $this->getSavedLicenses(); 
        $searchResult       =   $savedLicenses->where("license_id", $licenseIdToSearchFor);

        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $license = $searchResult->first();
        }

        return $license;
    }

    private function helperToFetchSourceItemTypeIdFromEitherExistingOrNew(string $itemTypeIdToSearchFor) {
        $itemType           =   [];
        $requestImportType  =   $this->getImportType();
        $savedNodeTypes     =   $this->getSavedNodeTypes();
        $searchResult       =   $savedNodeTypes->where("node_type_id", $itemTypeIdToSearchFor);

        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $itemType = $searchResult->first();
        }
        
        return $itemType;
    }

    private function helperToFetchSourceConceptIdFromEitherExistingOrNew(string $conceptIdToSearchFor){
        $concept            =   [];
        $requestImportType  =   $this->getImportType();
        $savedConcepts      =   $this->getSavedConcepts();
        $searchResult = $savedConcepts->where("concept_id", $conceptIdToSearchFor);

        // if found in memory then return id
        if($searchResult->isNotEmpty()) {
            $concept = $searchResult->first();
        }
        return $concept;
    }

    // private function helperToProcessAndCreateCaseAssociationsDuringCloningOnly(array $sourceItemIds): array {
    //     $updatedClonedAssociationsToReturn = [];
    //     $fieldsToReturn = ['item_id', 'source_item_id'];
    //     $returnCollection = true;
    //     $whereAttribute = "source_item_id";
    //     $itemCollection =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
    //                             $whereAttribute, $sourceItemIds, $returnCollection, $fieldsToReturn 
    //                         );
    //     if($itemCollection->isNotEmpty()) {
    //         $itemsToSearch = $itemCollection->toArray();
    //         $clonedAssociationsToSave = $this->getClonedAssociatonsToSave();
    //         foreach($clonedAssociationsToSave as $sourceIdToSearchWith=>$clonedAssociation) {
    //             // for each cloned association search the items fetched
    //             foreach($itemsToSearch as $key=>$item) {
    //                 $itemSourceId = $item["source_item_id"];
    //                 if($sourceIdToSearchWith===$itemSourceId){
    //                     $itemId = $item["item_id"];
    //                     // ??confusion why not item id instead of source id
    //                     $clonedAssociation["external_node_url"] = !empty($clonedAssociation["external_node_url"]) ? 
    //                                                 str_replace($sourceIdToSearchWith, $itemId, $clonedAssociation["external_node_url"])
    //                                                 : "";
    //                     $updatedClonedAssociationsToReturn[] = $clonedAssociation;
    //                     unset($itemsToSearch[$key]);
    //                     break;
    //                 }
    //             }
    //         }
    //     }
    //     return $updatedClonedAssociationsToReturn;
    // }

    private function raiseEventToUploadTaxonomySearchDataToSqs() {
        $documentId = $this->getSavedDocument()->document_id;
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["taxonomy_created"];
        $eventData = [
            "type_id" => $documentId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }

    public function getDescription($originNodeId,$sourceItemAssociationId)
    {
        $dataArr = $this->customDataArr->ACMTcustomfields->CFItem;


        $description ='';
        foreach($dataArr as $dataArrK=>$dataArrV)
        {
            foreach ($dataArrV->exemplar_and_association as $dataExemplarK=>$dataExemplarV)
            {
                if($originNodeId==$dataExemplarV->destination_node_id && $sourceItemAssociationId ==$dataExemplarV->source_item_association_id) {
                    $description = $dataExemplarV->description;
                }
            }
        }
        return $description;
    }

}