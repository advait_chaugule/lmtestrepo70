<?php
namespace App\Domains\CaseStandard\Jobs;

//use Illuminate\Http\Request;
use App\Data\Models\Document; 
use Lucid\Foundation\Job;
use App\Data\Models\Organization;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use DB;
class GetCaseFrameworkItemJob extends Job
{

    use CaseFrameworkTrait, DateHelpersTrait;
    
    /**
     * Declare private attributes
     */
    private $metadataRepository;
    private $itemRepository;
    private $documentRepository;
    private $entityType = "cf_item";
    private $identifier;
    private $itemDetails;
    private $item;
    private $cfRootURIEntity;
    private $cfItemTypeURIEntity;
    private $licenseURI;
    private $conceptKeywordsURI;
    private $organizationCode;
    private $metadataList;
    private $orgCode;
    private $organizationId;
    private $nodeTypeRepository;
    private $itemEntity;
    private $domainName;

    /**
     * GetCaseFrameworkItemJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param string $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl='')
    {
        $this->setIdentifier($identifier);
        $this->orgCode    = $orgCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NodeTypeRepositoryInterface $nodeTypeRepository, ItemRepositoryInterface $itemRepo, DocumentRepositoryInterface $documentRepo)
    {
        // set the item and document repository
        $this->itemRepository       =   $itemRepo;
        $this->documentRepository   =   $documentRepo;
        //set the metadata repository
        $this->nodeTypeRepository   =   $nodeTypeRepository;
        if($this->orgCode)
        {
            $getOrganizationId = $this->itemRepository->getOrganizationDetails($this->orgCode);
            $this->organizationId    = $getOrganizationId['organization_id'];
        }
        // get all relevant item data
        $itemDetails = $this->fetchItemDetailsFromRepository();
        if(!empty($itemDetails))
        {
            $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($itemDetails->node_type_id);
            $this->setMetadataList($metadataList);
        }
        //Fetch the Metadata list for the selected nodeType of the Item



        if(!empty($itemDetails->source_item_id)){
            // set item details globally
            $this->setItemDetails($itemDetails);
            //Parse the item detail response
            $this->parseItemDetailResponse();
           
            // transform to and set the Item Standard Structure
            $this->transFormToCaseStandardStructure();
            // finally return the CASE standard Item structure
            return $this->getItem();
        }
        else{
            return false; 
        }
    }

    /**
     * Public Setters and Getters Method
     */
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }

     public function setIdentifier(string $identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() :string {
        return $this->identifier;
    }

    public function getEntityType(): string {
        return $this->entityType;
    }

    public function setMetadataList($data) {
        $this->metadataList = $data;
    }

    public function getMetadataList() {
        return $this->metadataList;
    }

    public function setItemDetails($data) {
        $this->itemDetails = $data;
    }

    public function getItemDetails() {
        return $this->itemDetails;
    }

    public function setItemEntity($data) {
        $this->itemEntity = $data;
    }

    public function getItemEntity() {
        return $this->itemEntity;
    }

    public function setItem($data) {
        $this->item = $data;
    }

    public function getItem() {
        return $this->item;
    }

    public function setCFRootURIEntity(array $data){
        $this->cfRootURIEntity = $data;
    }

    public function getCFRootURIEntity(): array{
        return $this->cfRootURIEntity;
    }

    public function setCFItemTypeURIEntity($data) {
        $this->cfItemTypeURIEntity = $data;
    }

    public function getCFItemTypeURIEntity() {
        return $this->cfItemTypeURIEntity;
    }

    public function setLicenseURIEntity($data) {
        $this->licenseURI = $data;
    }

    public function getLicenseURIEntity() {
        return $this->licenseURI;
    }

    public function setConceptKeywordsURIEntity($data) {
        $this->conceptKeywordsURI = $data;
    }

    public function getConceptKeywordsURIEntity() {
        return $this->conceptKeywordsURI;
    }

    /**
     * @param $organizationId
     * @return mixed
     * Get Item Detail from Repository
     */
    private function fetchItemDetailsFromRepository() {
        $identifier = $this->getIdentifier();

        $returnCollection = false;
        $fieldsToReturn = ["*"];
        if($this->organizationId) {
            $dataFetchingConditionalClause = [ "source_item_id" => $identifier, "is_deleted" => 0,'organization_id'=>$this->organizationId];
        }else{
            $dataFetchingConditionalClause = [ "source_item_id" => $identifier, "is_deleted" => 0 ];
        }

        
        $itemDetails =    $this->itemRepository->findByAttributesWithSpecifiedFields(
                                $returnCollection, 
                                $fieldsToReturn, 
                                $dataFetchingConditionalClause
                            );
        // call the repository method to fetch data from database
        return $itemDetails;
    }

    private function parseItemDetailResponse() {
        $item = $this->getItemDetails();
        $concept    =   $item->concept;
        $license    =   $item->license;

        $itemEntity = [
            "item_id"                   => $item->item_id,
            "source_item_id"            => $item->source_item_id,
            "document_id"               => $item->document_id,
            "node_type"                 => $item->nodeType->title,
            "full_statement"            => !empty($item->full_statement) ? $item->full_statement : "",
            "alternative_label"         => !empty($item->alternative_label) ? $item->alternative_label : "",
            "human_coding_scheme"       => !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "",
            "list_enumeration"          => !empty($item->list_enumeration) ? $item->list_enumeration : "",
            "abbreviated_statement"     => !empty($item->abbreviated_statement) ? $item->abbreviated_statement : "",
            "source_concept_id"         => !empty($concept->source_concept_id) ? $concept->source_concept_id : "",
            "concept_title"             => !empty($item->concept->title) ? $item->concept->title : "",
            "concept_keywords"          => !empty($item->concept->keywords) ? $item->concept->keywords : "",
            "concept_hierarchy_code"    => !empty($item->concept->hierarchy_code) ? $item->concept->hierarchy_code : "",
            "notes"                     => !empty($item->notes) ? $item->notes : "",
            "language"                  => !empty($item->language->short_code) ? $item->language->short_code : "",
            "education_level"           => !empty($item->education_level) ? $item->education_level : "",
            "source_license_id"         => !empty($license->source_license_id) ? $license->source_license_id : "",
            "license_title"             => !empty($item->license->title) ? $item->license->title : "",
            "license_description"       => !empty($item->license->description) ? $item->license->description : "",
            "license_text"              => !empty($item->license->license_text) ? $item->license->license_text : "",
            "status_start_date"         => !empty($item->status_start_date) ? $item->status_start_date : "",
            "status_end_date"           => !empty($item->status_end_date) ? $item->status_end_date : "",
            "updated_at"                => $item->updated_at ,
            "uri"                       => $item->uri,
            "source_concept_uri_object" => !empty($item->source_concept_uri_object)?$item->source_concept_uri_object:""
        ];

        $this->setItemEntity($itemEntity);
    }

    private function parseMetadataForItem() {
        $metadataList   =   $this->getMetadataList();
        $arrayOfInternalNameForMetadata =[];
        foreach($metadataList->metadata as $metadata){
            $arrayOfInternalNameForMetadata[] = $metadata->internal_name;
        }
        return $arrayOfInternalNameForMetadata;
    }

    private function transFormToCaseStandardStructure() {
        // transform and set CFRootURI
        $this->transformAndSetCFRootURI();
        // transform and set CFItemTypeURI
        $this->transformAndSetCFItemTypeURI();
        // transform and set LicenseURI
        $this->transformAndSetLicenseURIEntity();
        // transform and set ConceptKeywordsURI
        $this->transformAndSetConceptKeywordsURIEntity();
        // finally transform and set CFItem
        $this->transformAndSetItem();
    }

    /**
     * Set Document Root URI Entity
     */
    private function transformAndSetCFRootURI() {
        $item = $this->getItemEntity();
        $orgCode = $this->getOrgCode();
        $documentNode = $this->documentRepository->getDocumentDataAndAssociatedModels($item['document_id']);
        // parse out the CFRootURI related info
        $uriEntity = [
            "title"         => !empty($documentNode->title) ? $documentNode->title : "",
            "identifier"    => $documentNode->source_document_id,
            "uri"           => ($documentNode->import_type==1)?$documentNode->uri:$this->getCaseApiUri("CFDocuments", $documentNode->source_document_id,true,$orgCode,$this->domainName)
            //!empty($rootNodeitem->uri) ? $rootNodeitem->uri : ""
        ];
        // set the CFRootURI for later use
        $this->setCFRootURIEntity($uriEntity);
    }

    /**
     * Set ItemType URI Entity
     */
    private function transformAndSetCFItemTypeURI() {
        $item = $this->getItemDetails();
        $orgCode = $this->getOrgCode();
        $nodeType   =   $item->nodeType;
        
        $cfItemTypeURIEntity = [
            "title"         => !empty($nodeType->title) ? $nodeType->title : "",
            "identifier"    => !empty($nodeType->source_node_type_id) ? $nodeType->source_node_type_id : "",
            "uri"           => !empty($nodeType->source_node_type_id) ? $this->getCaseApiUri("CFItemTypes", $nodeType->source_node_type_id,true,$orgCode,$this->domainName) : ""
        ];
        $this->setCFItemTypeURIEntity($cfItemTypeURIEntity);
    }

    /**
     * Set License URI Entity
     */
    private function transformAndSetLicenseURIEntity() {
        $item = $this->getItemEntity();
        $orgCode = $this->getOrgCode();
        $documentNode = $this->documentRepository->getDocumentDataAndAssociatedModels($item['document_id']);
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForItem();
        if(in_array('license_title', $arrayOfInternalNameForMetadata) === true) {
            // parse out the LicenseURI related info
            if($documentNode->import_type==1)
            {
                $licenceUri = $documentNode['license'];
                $uri              = $licenceUri['uri'];
                $sourceLicenceId  = $licenceUri['source_license_id'];
                $uriEntity = [
                    "title"         => !empty($item['license_title']) ? $item['license_title'] : "",
                    "identifier"    => $sourceLicenceId,
                    "uri"           => $uri,
                ];
            }else if($documentNode->import_type==2){
                $uriEntity = [
                    "title"         => !empty($item['license_title']) ? $item['license_title'] : "",
                    "identifier"    => !empty($item['source_license_id']) ? $item['source_license_id'] : "",
                    "uri"           => !empty($item['source_license_id']) ? $this->getCaseApiUri("CFLicenses", $item['source_license_id'],true,$orgCode,$this->domainName) : "",
                ];
            }else{
                $uriEntity = [
                    "title"         => !empty($item['license_title']) ? $item['license_title'] : "",
                    "identifier"    => !empty($item['source_license_id']) ? $item['source_license_id'] : "",
                    "uri"           => !empty($item['source_license_id']) ? $this->getCaseApiUri("CFLicenses", $item['source_license_id'],true,$orgCode,$this->domainName) : "",
                ];
            }

        } else {
            // parse out the LicenseURI related info
            $uriEntity = [
                "title"         => "",
                "identifier"    => "",
                "uri"           => "",
            ];
        }
        // set the LicenseURI for later use
        $this->setLicenseURIEntity($uriEntity);
    }

    /**
     * Set Concept Keywords URI Entity
     */
    private function transformAndSetConceptKeywordsURIEntity() {
        $item = $this->getItemEntity();
        $orgCode = $this->getOrgCode();
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForItem();

        if(in_array('concept_keywords', $arrayOfInternalNameForMetadata) === true) {
            $documentId  =  $item['document_id'];
            $getImportType = $this->getDocumentImportType($documentId);
            $importType    = isset($getImportType->import_type)?$getImportType->import_type:'';
            if($importType==1)
            {
                $conceptUriObj = $item['source_concept_uri_object'];
                if($conceptUriObj)
                {
                    $conceptArr = json_decode($conceptUriObj,true);
                    $uri        =  $conceptArr['uri'];
                    $identifier =  $conceptArr['identifier'];
                    $uriEntity = [
                        "title"         => !empty($item['concept_keywords']) ? $item['concept_keywords'] : "",
                        "identifier"    => $identifier,
                        "uri"           => $uri
                    ];
                }
            }else if($importType==2){
                $uriEntity = [
                    "title"         => !empty($item['concept_keywords']) ? $item['concept_keywords'] : "",
                    "identifier"    => !empty($item['source_concept_id']) ? $item['source_concept_id'] : "",
                    "uri"           => !empty($item['source_concept_id']) ? $this->getCaseApiUri("CFConcepts", $item['source_concept_id'],true,$orgCode,$this->domainName) : ""
                ];
            }else{
                $uriEntity = [
                    "title"         => !empty($item['concept_keywords']) ? $item['concept_keywords'] : "",
                    "identifier"    => !empty($item['source_concept_id']) ? $item['source_concept_id'] : "",
                    "uri"           => !empty($item['source_concept_id']) ? $this->getCaseApiUri("CFConcepts", $item['source_concept_id'],true,$orgCode,$this->domainName) : ""
                ];
            }

        } else {
            // parse out the ConceptURI related info
            $uriEntity = [
                "title" => "",
                "identifier" => "",
                "uri" => ""
            ];
        }
        // set the ConceptKeywordsURI for later use
        $this->setConceptKeywordsURIEntity($uriEntity);
    }

    /**
     * Set Item Entity
     */
    private function transformAndSetItem() {
        $itemMatchedMetadataValues  =   [];
        //parse the active metadata list
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForItem();
        $orgCode = $this->getOrgCode();
        $itemDetails    =   $this->getItemEntity();
        foreach($itemDetails as $key => $itemMetadata) {
            
            if(in_array($key, $arrayOfInternalNameForMetadata) === true){
                $itemMatchedMetadataValues[$key]      =  $itemMetadata; 
            }
            else {
                $itemMatchedMetadataValues[$key]      =   "";
            }
        }

        $conceptKeywords = [
            $itemMatchedMetadataValues['concept_keywords']
        ];
        $item = $this->getItemEntity();
        $documentNode = $this->documentRepository->getDocumentDataAndAssociatedModels($item['document_id']);
        $cfRootNodeUri = $this->getCFRootURIEntity();
        $parsedItemEntity = [
            "identifier"            => $itemDetails['source_item_id'],
            "CFDocumentURI"         => $cfRootNodeUri,
            "fullStatement"         => $itemMatchedMetadataValues['full_statement'],
            "alternativeLabel"      => $itemMatchedMetadataValues['alternative_label'],
            "CFItemType"            => $itemDetails['node_type'],
            "uri"                   => ($documentNode->import_type==1)?$item['uri'] :$this->getCaseApiUri("CFItems", $itemDetails['source_item_id'],true,$orgCode,$this->domainName),
            "humanCodingScheme"     => $itemMatchedMetadataValues['human_coding_scheme'],
            "listEnumeration"       => $itemMatchedMetadataValues['list_enumeration'],
            "abbreviatedStatement"  => $itemMatchedMetadataValues['abbreviated_statement'],
            "conceptKeywords"       => $conceptKeywords,
            "conceptKeywordsURI"    => $this->getConceptKeywordsURIEntity(),
            "notes"                 => $itemMatchedMetadataValues['notes'],
            "language"              => $itemMatchedMetadataValues['language'],
            "educationLevel"        => [$itemMatchedMetadataValues['education_level']],
            "CFItemTypeURI"         => $this->getCFItemTypeURIEntity(),
            "licenseURI"            => $this->getLicenseURIEntity(),
            "statusStartDate"       => $itemMatchedMetadataValues['status_start_date'],
            "statusEndDate"         => $itemMatchedMetadataValues['status_end_date'],
            "lastChangeDateTime"    => $this->formatDateTimeToISO8601($itemDetails['updated_at']),
        ];

        $this->setItem($parsedItemEntity);
    }

    public function getDocumentImportType($documentId)
    {
        $query =  DB::table('documents')
             ->select('import_type')
             ->where('document_id',$documentId)
             ->where('is_deleted',0)
             ->get()
             ->first();
        return $query;

    }

}
