<?php
namespace App\Domains\CaseStandard\Jobs;
use App\Data\Models\Document;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Data\Models\Organization;
use DB;
class GetCaseFrameworkAssociationGroupJob extends Job
{
    use DateHelpersTrait, CaseFrameworkTrait;

    private $identifier;
    private $organizationCode;
    private $orgCode;
    private $repository;
    private $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl)
    {
        $this->identifier = $identifier;
        $this->orgCode    = $orgCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssociationGroupRepositoryInterface $associationGroupRepository,ItemRepositoryInterface $repo)
    {
        $this->repository = $repo;
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId = $this->repository->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }
        if($organizationId){
            $attributes = ['source_association_group_id'=>$this->identifier,'organization_id'=>$organizationId];
        }else{
            $attributes =['source_association_group_id' =>$this->identifier];
        }
        $data = $associationGroupRepository->findByAttributes($attributes)->first();
        if(!empty($data)) {
            return $this->parseAndReturnCaseStandardAssociationGroupingData($data,$this->domainName);
        }else{
            return false;
        }

    }
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }


    private function parseAndReturnCaseStandardAssociationGroupingData($associationGrouping,$domainName) {
        $orgCode                =   $this->getOrgCode();
        //echo "<pre>";print_r($associationGrouping);die;
        $itemAssociation = $this->getImportType($associationGrouping->association_group_id);
        $getImportType=[];
        if($itemAssociation) {
            $getImportType = $this->documentImportType($itemAssociation->document_id,$itemAssociation->organization_id);
        }
        $data = [
            "identifier"            =>  $associationGrouping->source_association_group_id,
            "uri"                   =>  ($getImportType->import_type==1)?$associationGrouping->uri:$this->getCaseApiUri('CFAssociationGroupings',$associationGrouping->source_association_group_id,true,$orgCode,$domainName),
            "title"                 =>  !empty($associationGrouping->title) ? $associationGrouping->title : "",
            "description"           =>  !empty($associationGrouping->description) ? $associationGrouping->description : "",
            "lastChangeDateTime"    =>  !empty($associationGrouping->updated_at) ? $this->formatDateTimeToISO8601($associationGrouping->updated_at) : ""
        ];
        return $data;
    }

    public function getImportType($associationGrpId)
    {
        $itemAssociation= DB::table('item_associations')
            ->select('document_id','organization_id')
            ->where('association_group_id',$associationGrpId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $itemAssociation;
    }

    public function documentImportType($documentId,$organizationId)
    {
        $query = DB::table('documents')
            ->select('import_type','uri')
            ->where('document_id',$documentId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $query;
    }


}
