<?php
namespace App\Domains\CaseStandard\Jobs;
use Lucid\Foundation\Job;
use DB;
class GetLastUpdatedTimeJob extends Job
{
    private $sourceIdentifier;
    private $organizationId;
    public function __construct($sourceDocumentId,$organizationId)
    {
            $this->sourceIdentifier =  $sourceDocumentId;
            $this->organizationId   =  $organizationId;
    }

    public function handle()
    {
        $updatedTime=DB::table('documents')
                    ->select('updated_at','document_id','status')
                    ->where('source_document_id',$this->sourceIdentifier)
                    ->where('organization_id',$this->organizationId)
                    ->where('is_deleted',0)
                    ->latest('updated_at')
                    ->get()
                    ->first();
        return $updatedTime;
    }
}