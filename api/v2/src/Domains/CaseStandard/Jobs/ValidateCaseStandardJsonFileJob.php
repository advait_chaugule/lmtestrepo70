<?php
namespace App\Domains\CaseStandard\Jobs;
use Illuminate\Http\Request;

use Lucid\Foundation\Job;
use Illuminate\Http\UploadedFile;

class ValidateCaseStandardJsonFileJob extends Job
{
    private $request;
    private $inputFileKey;
    private $inputFileExtension;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->inputFileKey = "case_json";
        $this->inputFileExtension = "json";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (
            $this->request->hasFile($this->inputFileKey) && 
            $this->request->file($this->inputFileKey)->isValid() && 
            $this->request->file($this->inputFileKey)->getClientSize() > 0 && 
            $this->request->file($this->inputFileKey)->getClientOriginalExtension()===$this->inputFileExtension
        ) {
            $status = true;
            $message = [];
            $file = $this->request->file($this->inputFileKey);
        }
        else {
            $status = false;
            $message = ["case_json" => ["file" => "Please upload a valid JSON file."]];
            $file = '';
        }
        $response = ['status' => $status, "message" => $message, 'file' => $file];
        return $response;
    }
}
