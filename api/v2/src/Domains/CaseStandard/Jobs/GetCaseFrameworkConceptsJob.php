<?php
namespace App\Domains\Casestandard\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\Document;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Data\Models\Organization;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use DB;
class GetCaseFrameworkConceptsJob extends Job
{
    use DateHelpersTrait, CaseFrameworkTrait; 

    private $conceptRepository;
    private $identifier;
    private $conceptDetails;
    private $metadataList;
    private $organizationCode;
    private $orgCode;
    private $itemRepository;
    private $domainName;
    /**
     * GetCaseFrameworkConceptsJob constructor.
     * @param string $identifier
     * @param string $ordCode
     * @param string $requestUrl
     */
    public function __construct(string $identifier,$ordCode='',$requestUrl)
    {
        $this->setIdentifier($identifier);
        $this->orgCode = $ordCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ConceptRepositoryInterface $conceptRepo, MetadataRepositoryInterface $metadataRepository,ItemRepositoryInterface $repository)
    {
        $this->conceptRepository    =   $conceptRepo;
        $this->metadataRepository   =   $metadataRepository;
        $this->itemRepository       =   $repository;
        $organizationId ='';
        if($this->orgCode){
            $getOrganizationId = $this->itemRepository->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }
        $concept = $this->fetchConceptData($organizationId);
        if(!empty($concept->source_concept_id)){
            $conceptDetail  =   $this->parseConceptData($concept,$this->orgCode);
            $this->setConceptDetails($conceptDetail);

            $attributes =   ['organization_id'  =>  $conceptDetail['organization_id'], 'is_active'    =>  '1', 'is_deleted'   =>  '0'];
            $metadataList   =   $this->metadataRepository->findByAttributes($attributes);
            //$this->setOrgCode($conceptDetail['organization_id']);
            $this->setMetadataList($metadataList);
            // transform to and set the Concept Standard Structure
            $conceptData = $this->transformAndSetConceptData($this->orgCode);
            // finally return CASE formatted Concept Data
            return $conceptData;
        }
        else{
            return $concept->source_concept_id;
        }
    }

    public function setIdentifier(string $identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }

    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }

    private function fetchConceptData($organizationId){
        $identifier =   $this->getIdentifier();
        if($organizationId)
        {
            $attributes =   ['source_concept_id'=>$identifier,'organization_id'=>$organizationId];
        }else{
            $attributes =   ['source_concept_id'=>$identifier];
        }

        return $this->conceptRepository->findByAttributes($attributes)->first();
    }


    private function parseConceptData($concept,$orgCode) {
        $getDocumentId  = $this->getConceptItemId($concept['concept_id']);
        $documentId     =  $getDocumentId->document_id;
        $docImportType  = $this->getDocumentImportType($documentId);
        $importType     = $docImportType->import_type;

        $conceptDetail = [
            "concept_id"                =>  $concept->concept_id,
            "concept_title"             =>  !empty($concept->title) ? $concept->title : "",
            "concept_keywords"          =>  !empty($concept->keywords) ? $concept->keywords : "",
            "concept_hierarchy_code"    =>  !empty($concept->hierarchy_code) ? $concept->hierarchy_code : "",
            "concept_description"       =>  !empty($concept->description) ? $concept->description : "",
            "is_deleted"                =>  $concept->is_deleted,
            "source_concept_id"         =>  !empty($concept->source_concept_id) ? $concept->source_concept_id : "",
            "organization_id"           =>  $concept->organization_id,
            "created_at"                =>  $concept->created_at, 
            "updated_at"                =>  $concept->updated_at,
            "uri"                       =>  ($importType==1)?$concept->uri:$this->getCaseApiUri('CFConcepts', $concept->concept_id,true,$orgCode,$this->domainName)
        ];
        return $conceptDetail;
    }

    private function parseMetadata() {
        $metadataList   =   $this->getMetadataList();

        foreach($metadataList as $metadata) {
            $arrayofMetadataInternalName[] =   $metadata->internal_name;
        }

        return $arrayofMetadataInternalName;
    }

    private function transformAndSetConceptData($orgCode) {
        $arrayofMetadataInternalName    =   $this->parseMetadata();
        $concept                        =   $this->getConceptDetails();
        $orgCode                        =   $this->getOrgCode();
        $conceptMetadataArray =[];
        foreach($concept as $key => $conceptMetadata) {
            if(in_array($key, $arrayofMetadataInternalName) === true) {
                $conceptMetadataArray[$key]   = $conceptMetadata; 
            }
            else {
                $conceptMetadataArray[$key]   = ""; 
            }
        }
        $identifier = !empty($concept['source_concept_id']) ? $concept['source_concept_id'] : "";

        // parse and set Concept data as in CASE
        $uriEntity = [
            "identifier"            => $identifier,
            "uri"                   => ($concept['uri'])?$concept['uri']:$this->getCaseApiUri('CFConcepts', $identifier,true,$orgCode,$this->domainName),
            "title"                 => $conceptMetadataArray['concept_title'],
            "keywords"              => $conceptMetadataArray['concept_keywords'],
            "hierarchyCode"         => $conceptMetadataArray['concept_hierarchy_code'],
            "description"           => $conceptMetadataArray['concept_description'],
            "lastChangeDateTime"    => $this->formatDateTimeToISO8601($concept['updated_at']),
        ];
        
        return $uriEntity;
    }

    public function setConceptDetails($data) {
        $this->conceptDetails = $data;
    }

    public function getConceptDetails() {
       return $this->conceptDetails;
    }

    public function setMetadataList($data) {
        $this->metadataList = $data;
    }

    public function getMetadataList() {
       return $this->metadataList;
    }

    public function getConceptItemId($conceptId)
    {
        $query = DB::table('items')
                ->select('document_id')
                ->where('concept_id',$conceptId)
                ->where('is_deleted',0)
                ->get()
                ->first();
        return $query;
    }

    public function getDocumentImportType($documentId)
    {
        $importType =  DB::table('documents')
                  ->select('import_type')
                  ->where('document_id',$documentId)
                  ->where('is_deleted',0)
                  ->get()
                  ->first();
        return $importType;
    }

}
