<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

class StoreExternalCaseJsonFileJob extends Job
{
    private $externalCaseFile;
    private $baseFolderPathForStorage;
    private $externalCaseStorageFolder;
    private $externalFileName;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($externalCaseFile, $externalFileName)
    {
        $this->externalCaseFile = $externalCaseFile;
        $this->externalCaseStorageFolder = env("ACMT_CASE_FOLDER", "CASE_PACKAGES")."/external";
        $this->baseFolderPathForStorage = storage_path('app');
        $this->externalFileName = $externalFileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->externalCaseFile->storeAs($this->externalCaseStorageFolder, $this->externalFileName);
        return $this->externalCaseStorageFolder."/".$this->externalFileName;
    }
}

