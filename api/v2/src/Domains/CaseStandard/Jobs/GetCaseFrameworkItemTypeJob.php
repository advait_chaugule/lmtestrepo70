<?php
namespace App\Domains\Casestandard\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\Document; 
use App\Data\Models\Organization;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use DB;
class GetCaseFrameworkItemTypeJob extends Job
{
    use DateHelpersTrait, CaseFrameworkTrait;

    private $nodeTypeRepository;
    private $identifier;
    private $nodeTypeDetails;
    private $organizationCode;
    private $orgCode;
    private $itemRepository;
    private $domainName;
    private $url;

    /**
     * GetCaseFrameworkItemTypeJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl,$url='')
    {
        //
        $this->setIdentifier($identifier);
        $this->orgCode = $orgCode;
        $this->domainName = $requestUrl;
        $this->url = $url;
    }


    public function handle(NodeTypeRepositoryInterface $nodeTypeRepo,ItemRepositoryInterface $itemRepo )
    {
        // set the nodeType repository
        $this->nodeTypeRepository = $nodeTypeRepo;
        // get all relevant itemtype alias nodeType data
        $this->itemRepository = $itemRepo;
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId =  $this->itemRepository->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }

        $nodeTypeDetails = $this->fetchNodeTypeDetails($organizationId);
        if(!empty($nodeTypeDetails[0]->source_node_type_id)){
            $this->setNodeTypeDetails($nodeTypeDetails[0]);
            $nodeType = $this->transformAndSetCFItemType();
            return $nodeType;
        }else{
            $nodeType = $this->transformAndSetCFItemTypeFromItem($organizationId);
            return $nodeType;
        }
        
        //return $nodeTypeDetails=false;
    }

    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }

    public function setIdentifier(string $identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setNodeTypeDetails($data) {
        $this->nodeTypeDetails = $data;
    }

    public function getNodeTypeDetails() {
        return $this->nodeTypeDetails;
    }

    private function fetchNodeTypeDetails($organizationId){
        $identifier = $this->getIdentifier();
        if($organizationId)
        {
            $attributes = ['source_node_type_id'   =>  $identifier,'organization_id'=>$organizationId,'is_deleted' => '0'];
        }else{
            $attributes = ['source_node_type_id'   =>  $identifier,'is_deleted' => '0'];
        }

        return $this->nodeTypeRepository->findByAttributes($attributes);
    }

    private function transformAndSetCFItemType() {
        $itemType = $this->getNodeTypeDetails();
        $orgCode  = $this->getOrgCode();
        $identifier = !empty($itemType->source_node_type_id) ? $itemType->source_node_type_id : "";
        //$getImportType = $this->getDocumentType($itemType->node_type_id,$itemType->organization_id);
//        if($getImportType) {
//            $importType= $getImportType->import_type;
//        }else{
//            $getItemType = $this->getItemNodeType($itemType->node_type_id,$itemType->organization_id);
//            $importType= $getItemType->import_type;
//        }

        $cfItemTypeEntity = [
            "identifier"            => $identifier,
            "uri"                   =>  $this->getCaseApiUri('CFItemTypes', $identifier,true,$orgCode,$this->domainName),
            "title"                 => !empty($itemType->title) ? $itemType->title : "",
            "description"           => !empty($itemType->description) ? $itemType->description : "",
            "hierarchyCode"         => !empty($itemType->hierarchy_code) ? $itemType->hierarchy_code : "",
            "typeCode"              => !empty($itemType->type_code) ? $itemType->type_code : "",
            "lastChangeDateTime"    => $this->formatDateTimeToISO8601($itemType->updated_at),
        ];
        return $cfItemTypeEntity;
    }

    private function transformAndSetCFItemTypeFromItem($organizationId) {
        $identifier = $this->getIdentifier();
        $orgCode  = $this->getOrgCode();
        $jsonUri ='';
        $getImportType = $this->getDocumentType($identifier,$organizationId);
        if($getImportType) {
            $importType= $getImportType->import_type;
        }else{
            $getNodeDetails = $this->getItemNodeObjUri($identifier,$organizationId);

            $nodeTitle      = $getNodeDetails['node_details']->title;
            $hierarchyCode  = $getNodeDetails['node_details']->hierarchy_code;
            $description    = $getNodeDetails['node_details']->description;
            $typeCode       = $getNodeDetails['node_details']->type_code;
            $updatedAt      = $getNodeDetails['node_details']->updated_at;
            $cfItemTypeEntity = [
                "identifier"            => $identifier,
                "uri"                   => $this->url,
                "title"                 => !empty($nodeTitle) ? $nodeTitle : "",
                "description"           => !empty($description)?$description:"",
                "hierarchyCode"         => !empty($hierarchyCode) ? $hierarchyCode : "",
                "typeCode"              => !empty($typeCode) ?$typeCode : "",
                "lastChangeDateTime"    => $this->formatDateTimeToISO8601($updatedAt),
            ];
            return $cfItemTypeEntity;

        }
    }
    public function getItemNodeType($nodeTypeId,$organizationId)
    {
        $importTypes = DB::table('items')
            ->join('documents','documents.document_id','=','items.document_id')
            ->select('documents.import_type')
            ->where('items.node_type_id',$nodeTypeId)
            ->where('items.organization_id',$organizationId)
            ->where('items.is_deleted',0)
            ->get()
            ->first();
        return $importTypes;
    }

    public function getDocumentType($nodeTypeId,$organizationId)
    {
        $query = DB::table('documents')
            ->select('import_type')
            ->where('node_type_id',$nodeTypeId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $query;

    }

    public function getItemNodeObjUri($nodeTypeId,$organizationId)
    {
        $nodeDetails = DB::table('items')
           // ->join('documents','documents.document_id','=','items.document_id')
            ->select('source_node_type_uri_object','node_type_id')
            ->where('source_node_type_uri_object','like','%'.$nodeTypeId.'%')
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->first();

        $nodeTypeId      = !empty($nodeDetails->node_type_id)?$nodeDetails->node_type_id:$nodeTypeId;
        $nodeDetailsNode = DB::table('node_types')
            ->where('node_type_id',$nodeTypeId)
            ->where('is_deleted',0)
            ->where('used_for',0)
            ->get()
            ->first();
        $nodeData = ['node_details'=>$nodeDetailsNode];
        return $nodeData;
    }
}
