<?php
namespace App\Domains\CaseStandard\Jobs;
ini_set('max_execution_time', 0);
use App\Data\Models\Organization;
use App\Data\Models\Document;
use Illuminate\Http\Request;
use Lucid\Foundation\Job;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use DB;
class GetAllCaseFrameworkDocumentsJob extends Job
{
    use CaseFrameworkTrait, DateHelpersTrait;

    private $documentRepository;
    private $requestDataFilters;
    private $allowedDataFilters;
    private $searchFilters;

    private $nodeTypes  =   [];

    private $documents;
    private $documentsDetail;
    private $cfDocumentList;
    private $setSubjectEntity;
    private $cfRootURIEntity;
    private $subjectURI;
    private $licenseURI;
    private $orgCode;
    private $nodeTypeRepository;
    private $organizationCode;
    private $domainName;

    public function __construct($orgCode,$serverName)
    {
        $this->orgCode = $orgCode;
        //Hostname
        $this->domainName = $serverName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, NodeTypeRepositoryInterface $nodeTypeRepository, Request $request)
    {
        // set filters if any
        $this->requestDataFilters = $request->all();
        $this->orgCode  = isset($this->orgCode)?$this->orgCode:'';

        // set allowed filters
        $this->allowedDataFilters = ["limit", "offset"];

        // set the case repository
        $this->documentRepository = $documentRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        // process and set filters
        $this->processAndSetFilters();

        // fetch CFDocuments from DB and set it
        $this->fetchAndSetDocuments();

        //list the metadata for respective document type node type id;
        $this->fetchMetadataList();

        // parse each CFDocument and create List of CFDocuments
        $this->createAndSetCFDocumentList();

        // return CFDocument list formatted according to CASE standard
        return $this->getFinalCFDocumentListStructure();
    }

    public function setOrgCode(string $data) {
        if(!empty($data)){
            $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        }else{
            $organizationCode = '';
        }
        
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty( $this->organizationCode)) {
            return $this->orgCode;
        }else{
            return $this->organizationCode;
        }
    }

    public function setDocuments($data) {
        $this->documents = $data;
    }

    public function getDocuments() {
        return $this->documents;
    }

    public function setDocumentDetails($data) {
        $this->documentsDetail = $data;
    }

    public function getDocumentDetails() {
        return $this->documentsDetail;
    }

    public function setNodeTypes($data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes() {
        return $this->nodeTypes;
    }

    public function setMetadataList($data) {
        $this->metadataList = $data;
    }

    public function getMetadataList() {
        return $this->metadataList;
    }

    public function setCFDocumentList(array $data) {
        $this->cfDocumentList = $data;
    }

    public function getCFDocumentList(): array {
        return $this->cfDocumentList;
    }

    public function getRequestFilters(): array {
        return $this->requestDataFilters;
    }

    public function getAllowedDataFilters(): array {
        return $this->allowedDataFilters;
    }

    public function setSearchFilters(array $data) {
        $this->searchFilters = $data;
    }

    public function getSearchFilters(): array {
        return $this->searchFilters;
    }

    public function setCFRootURIEntity(array $data){
        $this->cfRootURIEntity = $data;
    }

    public function getCFRootURIEntity(): array{
        return $this->cfRootURIEntity;
    }

    public function setSubjectEntity($data) {
        $this->setSubjectEntity = $data;
    }

    public function getSubjectEntity() {
        return $this->setSubjectEntity;
    }

    public function setSubjectURIEntity($data) {
        $this->subjectURI = $data;
    }

    public function getSubjectURIEntity() {
        return $this->subjectURI;
    }

    public function setLicenseURIEntity($data) {
        $this->licenseURI = $data;
    }

    public function getLicenseURIEntity() {
        return $this->licenseURI;
    }

    private function processAndSetFilters() {
        $requestFilters = $this->getRequestFilters();
        $allowedFilters = $this->getAllowedDataFilters();
        // set default filter attribute and values
        $filters = [
            "limit" => 100,
            "offset" => 0
        ];
        foreach($allowedFilters as $allowedFilter){
            // if filter allowed set it
            if(array_key_exists($allowedFilter, $requestFilters)){
                // override with requested filter values if allowed
                $filters[$allowedFilter] = (integer)$requestFilters[$allowedFilter];
            }
        }
        $this->setSearchFilters($filters);
    }

    private function fetchAndSetDocuments() {
        $arrayOfDocumentId  =   [];
        $searchFilterAttributeWithValues = $this->getSearchFilters();
        // fetch data from db and set
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId = $this->getOrganizationDetails($this->orgCode); // organization specific
             $organizationId = $getOrganizationId['organization_id'];
               $isCheck = 1;
             $data = $this->documentRepository->getAllCFDocuments($searchFilterAttributeWithValues,$organizationId,$isCheck);
        }else{
            $data = $this->documentRepository->getAllCFDocuments($searchFilterAttributeWithValues);
        }

        foreach($data as $documentDetails){
            $arrayOfDocumentId[]    =   $documentDetails->source_document_id;
        }


            $attributeIn            =   'source_document_id';
            $containedInValues      =   $arrayOfDocumentId;
            $returnCollection       =   true;
            $fieldsToReturn         =   ['*'];
            $keyValuePairs          =   [];
            $returnPaginatedData    =   false;
            $paginationFilters      =   ['limit' => 10, 'offset' => 0];
            $returnSortedData       =   false;
            $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
            $operator               =   'AND';
            $relations              =   ['subjects', 'license'];
            $conditionalKeyValuePair = ["is_deleted" => 0, "status" => 3];
            if($organizationId!='') {
                $conditionalKeyValuePair = [ "organization_id" => $organizationId, "is_deleted" => 0, "status" => 3];
            }
            $this->setOrgCode($organizationId);
        //ACMT-2085
        $documents =   $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair, $relations);
        $arrayOfNodeType = [];
        $documentEntity  = [];
        foreach($documents as $documentDetails){
            $arrayOfNodeType[]  =   $documentDetails->node_type_id;

            $subjects           =   $documentDetails->subjects;
            $license            =   $documentDetails->license;

            $documentEntity[]    =   [
                "document_id"               =>  $documentDetails->document_id,
                "source_document_id"        =>  $documentDetails->source_document_id,
                "node_type_id"              =>  $documentDetails->node_type_id,
                "title"                     =>  !empty($documentDetails->title) ? $documentDetails->title  : "",
                "official_source_url"       =>  !empty($documentDetails->official_source_url) ? $documentDetails->official_source_url  : "",
                "language"                  =>  !empty($documentDetails->language->short_code) ? $documentDetails->language->short_code : "",
                "adoption_status"           =>  !empty($documentDetails->adoption_status) ? $documentDetails->adoption_status : "",
                "status"                    =>  !empty($documentDetails->status) ? $documentDetails->status : "",
                "notes"                     =>  !empty($documentDetails->notes) ? $documentDetails->notes : "",
                "publisher"                 =>  !empty($documentDetails->publisher) ? $documentDetails->publisher : "",
                "description"               =>  !empty($documentDetails->description) ? $documentDetails->description : "",
                "version"                   =>  !empty($documentDetails->version) ? $documentDetails->version : "",
                "status_start_date"         =>  !empty($documentDetails->status_start_date) ? $documentDetails->status_start_date : "",
                "status_end_date"           =>  !empty($documentDetails->status_end_date) ? $documentDetails->status_end_date : "",
                "license_id"                =>  !empty($license->source_license_id) ? $license->source_license_id : "",
                "license_title"             =>  !empty($license->title) ? $license->title : "",
                "license_description"       =>  !empty($license->description) ? $license->description : "",
                "license_text"              =>  !empty($license->license_text) ? $license->license_text : "",
                "creator"                   =>  !empty($documentDetails->creator) ? $documentDetails->creator : "",
                "subject_id"                =>  !empty($subjects[0]->source_subject_id) ? $subjects[0]->source_subject_id : "",
                "subject_title"             =>  !empty($subjects[0]->title) ? $subjects[0]->title : "",
                "subject_hierarchy_code"    =>  !empty($subjects[0]->hierarchy_code) ? $subjects[0]->hierarchy_code : "",
                "subject_description"       =>  !empty($subjects[0]->description) ? $subjects[0]->description : "",
                "updated_at"                =>  $documentDetails->updated_at,
                "import_type"               =>  $documentDetails->import_type,
                "uri"                       =>  $documentDetails->uri,
            ];
        }

        $this->setNodeTypes($arrayOfNodeType);
        $this->setDocuments($documentEntity);
    }

    private function fetchMetadataList() {
        $nodeTypes  =   $this->getNodeTypes();
        //$nodeTypes  =   array_unique($nodeTypes);

        $attributeIn        =   'node_type_id';
        $containedInValues  =   $nodeTypes;
        $returnCollection   =   true;
        $fieldsToReturn     =   ['node_type_id'];
        $keyValuePairs      =   [];

        $returnPaginatedData    =   false;
        $paginationFilters      =   ['limit' => 10, 'offset' => 0];
        $returnSortedData       =   false;
        $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator               =   'AND';
        $relations              =   ['metadata'];

        $metadataList  =    $this->nodeTypeRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, $containedInValues, $returnCollection, $fieldsToReturn, $keyValuePairs, $returnPaginatedData, $paginationFilters, $returnSortedData, $sortfilters, $operator, $relations);

        $this->setMetadataList($metadataList);
    }

    private function parseMetadataInternalName() {
        $metadataList   =   $this->getMetadataList();
        $arrayOfInternalName = [];
        foreach($metadataList as $metadata) {
            foreach($metadata->metadata as $metadataSet){
                $arrayOfInternalName[$metadata['node_type_id']][]   = $metadataSet->internal_name;
            }
        }

        return  $arrayOfInternalName;
    }

    /**
     * Set Document URI Entity
     */
    private function transformAndSetCFRootURI($documentDetail) {
        $arrayOfInternalName    =   $this->parseMetadataInternalName();
        $orgCode                =   $this->getOrgCode();

        // parse out the CFRootURI related info
        $uriEntity = [
            "title"         => (in_array('title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['title'] : "",
            "identifier"    => !empty($documentDetail['source_document_id']) ? $documentDetail['source_document_id'] : "",
            "uri"           => ($documentDetail['import_type']==1)?str_replace('CFDocuments',  'CFPackages',$documentDetail['uri']):$this->getCaseApiUri("CFPackages", $documentDetail['source_document_id'],true, $orgCode, $this->domainName)
        ];
        // set the CFRootURI for later use
        $this->setCFRootURIEntity($uriEntity);
    }

    /**
     * Set Subject Entity
     */
    private function transformAndSetSubjectEntity($documentDetail) {
        $arrayOfInternalName    =   $this->parseMetadataInternalName();

        $uriEntity[] = (in_array('subject_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['subject_title'] : "";

        // set the SubjectEntity for later use
        $this->setSubjectEntity($uriEntity);
    }

    /**
     * Set Subject URI Entity
     */
    private function transformAndSetSubjectURIEntity($documentDetail) {
        $arrayOfInternalName    =   $this->parseMetadataInternalName();
        $orgCode                =   $this->getOrgCode();
        $uriEntity=[];
          if($documentDetail['import_type']==2)
          {
              $uriEntity = [
                  "title"         => (in_array('subject_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['subject_title'] : "",
                  "identifier"    => !empty($documentDetail['subject_id']) ? $documentDetail['subject_id'] : "",
                  "uri"           => !empty($documentDetail['subject_id']) ? $this->getCaseApiUri("CFSubjects", $documentDetail['subject_id'],true,$orgCode,$this->domainName) : "",
              ];
          }else{
              if($documentDetail['subject_id']) {
                  $subjectUri = $this->getSubjectUri($documentDetail['document_id']);
                  $subjectUriJson = $subjectUri->source_subject_uri_object;
                  $subArrUri = json_decode($subjectUriJson);
                 $uri = isset($subArrUri->uri)?$subArrUri->uri:'';
                  $identifier = isset($subArrUri->identifier)?$subArrUri->identifier:'';
              if($documentDetail['import_type']==1) {
                  $uriEntity = [
                      "title"         => (in_array('subject_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['subject_title'] : "",
                      "identifier"    => $identifier,
                      "uri"           => $uri,
                  ];
              }else{
                  $uriEntity = [
                      "title"         => (in_array('subject_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['subject_title'] : "",
                      "identifier"    => !empty($documentDetail['subject_id']) ? $documentDetail['subject_id'] : "",
                      "uri"           => !empty($documentDetail['subject_id']) ? $this->getCaseApiUri("CFSubjects", $documentDetail['subject_id'],true,$orgCode,$this->domainName) : "",
                  ];
              }
          }
        }

        // parse out the SubjectURI related info

        // set the CFRootURI for later use
        $this->setSubjectURIEntity($uriEntity);
    }

    /**
     * Set License URI Entity
     */
    private function transformAndSetLicenseURIEntity($documentDetail) {
        $arrayOfInternalName    =   $this->parseMetadataInternalName();
        $orgCode                =   $this->getOrgCode();
        $uriEntity=[];
        if($documentDetail['import_type']==2)
        {
            $uriEntity = [
                "title"         => (in_array('license_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['license_title'] : "",
                "identifier"    => !empty($documentDetail['license_id']) ? $documentDetail['license_id'] : "",
                "uri"           => !empty($documentDetail['license_id']) ? $this->getCaseApiUri("CFLicenses", $documentDetail['license_id'],true,$orgCode,$this->domainName) : ""
            ];
        }else {
            if ($documentDetail['license_id']) {
                if ($documentDetail['import_type'] == 1) {
                    $licenceDetails = $this->getLicenceUri($documentDetail['license_id']);
                    $url = $licenceDetails->uri;
                    $uriEntity = [
                        "title"         => (in_array('license_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['license_title'] : "",
                        "identifier"    => !empty($documentDetail['license_id']) ? $documentDetail['license_id'] : "",
                        "uri"           => $url
                    ];

            } else {
                    $uriEntity = [
                        "title" => (in_array('license_title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['license_title'] : "",
                        "identifier" => !empty($documentDetail['license_id']) ? $documentDetail['license_id'] : "",
                        "uri" => !empty($documentDetail['license_id']) ? $this->getCaseApiUri("CFLicenses", $documentDetail['license_id'], true, $orgCode, $this->domainName) : ""
                    ];
                }
            }
        }
        // set the CFRootURI for later use
        $this->setLicenseURIEntity($uriEntity);
    }

    private function transFormToCaseStandardStructure($documentDetail) {
        // transform and set CFRootURI
        $this->transformAndSetCFRootURI($documentDetail);
        // transform and set Subject
        $this->transformAndSetSubjectEntity($documentDetail);
        // transform and set SubjectURI
        $this->transformAndSetSubjectURIEntity($documentDetail);
        // transform and set LicenseURI
        $this->transformAndSetLicenseURIEntity($documentDetail);
    }

    private function createAndSetCFDocumentList() {
        $documentMatchedMetadataValues  =   [];
        $cfDocuments            =   $this->getDocuments();
        $arrayOfInternalName    =   $this->parseMetadataInternalName();
        $orgCode                =   $this->getOrgCode();
        $cfDocumentsToSet = [];
        foreach($cfDocuments as $documentDetail) {
            $this->transFormToCaseStandardStructure($documentDetail);

            // create the CFDocument
            $cfDocumentsToSet[] = [
                "identifier"            => $documentDetail['source_document_id'],
                "uri"                   => ($documentDetail['import_type']==1)?$documentDetail['uri']:$this->getCaseApiUri("CFDocuments", $documentDetail['source_document_id'],true,$orgCode, $this->domainName),
                "creator"               => (in_array('creator', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['creator'] : "",
                "title"                 => (in_array('title', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['title'] : "",
                "lastChangeDateTime"    => $this->formatDateTimeToISO8601($documentDetail['updated_at']),
                "officialSourceURL"     => (in_array('official_source_url', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['official_source_url'] : "",
                "publisher"             => (in_array('publisher', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['publisher'] : "",
                "description"           => (in_array('description', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['description'] : "",
                "subject"               => $this->getSubjectEntity(),
                "subjectURI"            => $this->getSubjectURIEntity(),
                "language"              => (in_array('language', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['language'] : "",
                "version"               => (in_array('version', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['version'] : "",
                "adoptionStatus"        => !empty($documentDetail['adoption_status']) ? $this->getSystemSpecifiedAdoptionStatusText($documentDetail['adoption_status']) : "",
                "statusStartDate"       => (in_array('status_start_date', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['status_start_date'] : "",
                "statusEndDate"         => (in_array('status_end_date', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['status_end_date'] : "",
                "licenseURI"            => $this->getLicenseURIEntity(),
                "notes"                 => (in_array('notes', $arrayOfInternalName[$documentDetail['node_type_id']]) === true) ? $documentDetail['notes'] : "",
                // it might be uri object not sure $this->getCFRootURIEntity(),
                "CFPackageURI"          => $this->getCFRootURIEntity()
            ];
        }
        $this->setCFDocumentList($cfDocumentsToSet);
    }

    private function getFinalCFDocumentListStructure(): array {
        $listData = $this->getCFDocumentList();
        return [ "CFDocuments" => $listData ];
    }

    /**
     * @param $orgCode
     * @return mixed
     * @Funcrion getOrganizationDetails
     * @Purpose Based on org code get org id for tenant specific
     */
    public function getOrganizationDetails($orgCode)
    {
        $orgId = Organization::select('organization_id')
                ->where('org_code',$orgCode)
                ->first();
        if($orgId) {
            $orgId = $orgId->toArray();
        }
        return $orgId ;
    }

    public function getSubjectUri($documentId)
    {
         $subject = DB::table('document_subject')
                   ->select('source_subject_uri_object')
                   ->where('document_id',$documentId)
                   ->get()
                  ->first();
          return $subject;
    }

    public function getLicenceUri($licenceId)
    {
        $licence = DB::table('licenses')
                     ->select('uri')
                     ->where('source_license_id',$licenceId)
                     ->where('is_deleted',0)
                     ->get()
                     ->first();
        return $licence;
    }
}
