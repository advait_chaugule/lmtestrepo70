<?php
namespace App\Domains\CaseStandard\Jobs;
ini_set('max_execution_time', '0');
ini_set('memory_limit', '-1');
use App\Data\Models\Organization;
use Lucid\Foundation\Job;
use Illuminate\Support\Collection;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;
use Illuminate\Support\Facades\DB;
use App\Data\Models\TaxonomyPubStateHistory;

class GetCasePackageWithActiveMetadataJob extends Job
{

    use ArrayHelper, CaseFrameworkTrait, DateHelpersTrait, UuidHelperTrait;

    private $metadataRepository;
    private $documentRepository;
    private $itemRepository;
    private $languageRepository;
    private $licenseRepository;
    private $subjectRepository;
    private $nodeTypeRepository;
    private $conceptRepository;
    private $itemAssociationRepository;
    private $associationGroupRepository;

    private $documentIdentifier;
    private $requestingUser;
    private $requestingUserOrganization;
    private $requestingUserFullName;
    private $tenantMetadata;

    private $savedDocument;
    private $savedItems;
    private $savedItemTypes;
    private $savedAssociations;
    private $savedReverseAssociations;
    private $savedAssociationGroups;
    private $savedConcepts;
    private $savedSubjects;
    private $savedLicenses;
    private $savedLanguages;

    private $sourceConceptUriObjectOfSavedItems;
    private $sourceLicenseUriObjectOfSavedItems;
    private $sourceItemTypeUriObjectOfSavedItems;

    private $conceptIdsCsv;
    private $licenseIdsCsv;
    private $languageIdsCsv;
    private $itemTypeIdsCsv;
    private $associationGroupIdsCsv;

    private $parsedCaseSubjects;
    private $parsedCaseSubjectsUri;
    private $parsedCaseSubjectsTitle;
    private $parsedCaseLicenses;
    private $parsedCaseLicensesUri;
    private $parsedCaseConcepts;
    private $parsedCaseConceptsUri;
    private $parsedCaseConceptsKeyword;
    private $parsedCaseItemTypes;
    private $parsedCaseItemTypesUri;
    private $parsedCaseAssociationGroupings;
    private $parsedCaseAssociationGroupingsUri;
    private $parsedCaseDocument;
    private $parsedCaseDocumentUri;
    private $parsedCaseItems;
    private $parsedCaseItemsUri;
    private $parsedCaseAssociations;
    private $parsedImportTypeItems;

    private $casePackage;
    private $organizationCode;
    private $_tempStorageForSavedDocument;
    private $_tempStorageForSavedItem;
    private $status;
    private $orgCode;
    private $exportKey;
    private $domainName;
    private $versioning;
    private $versionTag;
    private $publishedBy;
    private $versionInfo;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestingUserDetails = [],$orgCode='', $is_html=0, $export_key = 0,$requestUrl='',$additionalParams=[])
    {
        $this->setDocumentIdentifier($documentIdentifier);
        $this->exportKey = $export_key;

        // set organization id when the job is executed internally
        if(!empty($requestingUserDetails["organization_id"])) {
            $this->setRequestingUserOrganizationId($requestingUserDetails["organization_id"]);
        }

        $this->setRequestingUserFullName($requestingUserDetails);
        //$this->status=$status;
        $this->orgCode = $orgCode;
        $this->is_html = $is_html;
        $this->domainName = $requestUrl;
        $this->versioning = isset($additionalParams['versioning']) ? $additionalParams['versioning'] : false; //Added to check request is for versioning - UF-1871
        $this->versionTag = isset($additionalParams['versionTag']) ? $additionalParams['versionTag'] : '';
        $this->publishedBy = isset($additionalParams['publishedBy']) ? $additionalParams['publishedBy'] : '';
    }

    public function setDocumentIdentifier(string $data) {
        $this->documentIdentifier = $data;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setRequestingUserOrganizationId(string $data) {
        $this->requestingUserOrganization = $data;
    }

    public function getRequestingUserOrganizationId() {
        return $this->requestingUserOrganization;
    }

    public function setRequestingUserFullName(array $data) {
        if(empty($data)) {
            $data = [ "first_name" => "INTERNAL", "last_name" => "" ];
        }
        $firstName = $data["first_name"] ?: "";
        $lastName = $data["last_name"] ?: "";
        $this->requestingUserFullName = trim($firstName." ".$lastName);
    }

    public function getRequestingUserFullName(): string {
        return $this->requestingUserFullName;
    }

    public function setTenantMetadata($data) {
        $this->tenantMetadata = $data ? $data->toArray() : [];
    }

    public function getTenantMetadata(): array {
        return $this->tenantMetadata;
    }

    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)){
            return $this->orgCode;
        }else{
            return $this->organizationCode;
        }
    }

    /************************************Setters and getters for saved CASE related Entities***********************************************/

    public function setSavedDocument($data) {
        
        $this->savedDocument = $data ? $data->toArray() : [];
    }

    public function getSavedDocument() {
        return $this->savedDocument;
    }

    public function setSavedItems($data) {
        $this->savedItems = $data ? $data->toArray() : [];
    }

    public function getSavedItems(): array {
        return $this->savedItems;
    }

    public function setSavedItemTypes( $data) {
        $this->savedItemTypes = $data ? $data->toArray() : [];
    }

    public function getSavedItemTypes(): array {
        return $this->savedItemTypes;
    }

    public function setSavedAssociations( $data) {
        $this->savedAssociations = $data;
    }

    public function getSavedAssociations(): Collection {
        return $this->savedAssociations;
    }

    public function setReverseAssociations( $data) {
       
        $this->savedReverseAssociations =   $data;
    }

    public function getReverseAssociations() : Collection {
        return $this->savedReverseAssociations;
    }

    public function setSavedAssociationGroups( $data) {
        $this->savedAssociationGroups = $data ? $data->toArray() : [];
    }

    public function getSavedAssociationGroups(): array {
        return $this->savedAssociationGroups;
    }

    public function setSavedConcepts( $data) {
        $this->savedConcepts = $data ? $data->toArray() : [];
    }

    public function getSavedConcepts(): array {
        return $this->savedConcepts;
    }

    public function setSavedSubjects( $data) {
       
        $this->savedSubjects = $data;
    }

    public function getSavedSubjects(): array {
       
        return $this->savedSubjects;
    }

    public function setSavedLicenses( $data) {
        $this->savedLicenses = $data ? $data->toArray() : [];
    }

    public function getSavedLicenses(): array {
        return $this->savedLicenses;
    }

    public function setSavedLanguages( $data) {
        $this->savedLanguages = $data ? $data->toArray() : [];
    }

    public function getSavedLanguages(): array {
        return $this->savedLanguages;
    }

    /*****************************Setters and getters for all master tables csv of ids related to case**********************************/

    public function setConceptIdsCsv(string $data) {
        $this->conceptIdsCsv = $data;
    }

    public function getConceptIdsCsv(): string {
        return $this->conceptIdsCsv;
    }

    public function setLicenseIdsCsv(string $data) {
        $this->licenseIdsCsv = $data;
    }

    public function getLicenseIdsCsv(): string {
        return $this->licenseIdsCsv;
    }

    public function setLanguageIdsCsv(string $data) {
        $this->languageIdsCsv = $data;
    }

    public function getLanguageIdsCsv(): string {
        return $this->languageIdsCsv;
    }

    public function setItemTypeIdsCsv(string $data) {
        $this->itemTypeIdsCsv = $data;
    }

    public function getItemTypeIdsCsv(): string {
        return $this->itemTypeIdsCsv;
    }

    public function setAssociationGroupIdsCsv(string $data) {
        $this->associationGroupIdsCsv = $data;
    }

    public function getAssociationGroupIdsCsv(): string {
        return $this->associationGroupIdsCsv;
    }

    /*********************************Setters and getters for parsed Case related entities***************************************/

    public function setParsedCaseSubjects(array $data) {
        
        $this->parsedCaseSubjects = $data;
    }

    public function getParsedCaseSubjects(): array {
        return $this->parsedCaseSubjects;
    }

    public function setParsedCaseSubjectsUri(array $data) {
        $this->parsedCaseSubjectsUri = $data;
    }

    public function getParsedCaseSubjectsUri(): array {
        return $this->parsedCaseSubjectsUri;
    }

    public function setParsedCaseSubjectsTitle(array $data) {
        $this->parsedCaseSubjectsTitle = $data;
    }

    public function getParsedCaseSubjectsTitle(): array {
        return $this->parsedCaseSubjectsTitle;
    }

    public function setParsedCaseLicenses(array $data) {
        $this->parsedCaseLicenses = $data;
    }

    public function getParsedCaseLicenses(): array {
        return $this->parsedCaseLicenses;
    }

    public function setParsedCaseLicensesUri(array $data) {
        $this->parsedCaseLicensesUri = $data;
    }

    public function getParsedCaseLicensesUri(): array {
        return $this->parsedCaseLicensesUri;
    }

    public function setParsedCaseConcepts(array $data) {
        $this->parsedCaseConcepts = $data;
    }

    public function getParsedCaseConcepts(): array {
        return $this->parsedCaseConcepts;
    }

    public function setParsedCaseConceptsUri(array $data) {
        $this->parsedCaseConceptsUri = $data;
    }

    public function getParsedCaseConceptsUri(): array {
        return $this->parsedCaseConceptsUri;
    }

    public function setParsedCaseConceptsKeyword(array $data) {
        $this->parsedCaseConceptsKeyword = $data;
    }

    public function getParsedCaseConceptsKeyword(): array {
        return $this->parsedCaseConceptsKeyword;
    }

    public function setParsedCaseItemTypes(array $data) {
        $this->parsedCaseItemTypes = $data;
    }

    public function getParsedCaseItemTypes(): array {
        return $this->parsedCaseItemTypes;
    }

    public function setParsedCaseItemTypesUri(array $data) {
        $this->parsedCaseItemTypesUri = $data;
    }

    public function getParsedCaseItemTypesUri(): array {
        return $this->parsedCaseItemTypesUri;
    }

    public function setParsedCaseAssociationGroupings(array $data) {
        $this->parsedCaseAssociationGroupings = $data;
    }

    public function getParsedCaseAssociationGroupings(): array {
        return $this->parsedCaseAssociationGroupings;
    }

    public function setParsedCaseAssociationGroupingsUri(array $data) {
        $this->parsedCaseAssociationGroupingsUri = $data;
    }

    public function getParsedCaseAssociationGroupingsUri(): array {
        return $this->parsedCaseAssociationGroupingsUri;
    }

    public function setParsedCaseDocument(array $data) {
        $this->parsedCaseDocument = $data;
    }

    public function getParsedCaseDocument(): array {
        return $this->parsedCaseDocument;
    }

    public function setParsedCaseDocumentUri(array $data) {
        $this->parsedCaseDocumentUri = $data;
    }

    public function getParsedCaseDocumentUri(): array {
        return $this->parsedCaseDocumentUri;
    }

    public function setParsedCaseDocumentUriForReverseAssociation(array $data) {
        $this->parsedCaseDocumentUriForReversedAssociation = $data;
    }

    public function getParsedCaseDocumentUriForReverseAssociation(): array {
        return $this->parsedCaseDocumentUriForReversedAssociation;
    }

    public function setSourceConceptUriObjectOfSavedItems($data) {
        $this->sourceConceptUriObjectOfSavedItems = $data;
    }

    public function getSourceConceptUriObjectOfSavedItems() {
        return $this->sourceConceptUriObjectOfSavedItems;
    }

    public function setSourceLicenseUriObjectOfSavedItems($data) {
        $this->sourceLicenseUriObjectOfSavedItems = $data;
    }

    public function getSourceLicenseUriObjectOfSavedItems() {
        return $this->sourceLicenseUriObjectOfSavedItems;
    }

    public function setSourceItemTypeUriObjectOfSavedItems($data) {
        $this->sourceItemTypeUriObjectOfSavedItems = $data;
    }

    public function getSourceItemTypeUriObjectOfSavedItems() {
        return $this->sourceItemTypeUriObjectOfSavedItems;
    }

    public function setParsedCaseItems(array $data) {
        $this->parsedCaseItems = $data;
    }

    public function getParsedCaseItems(): array {
        return $this->parsedCaseItems;
    }

    public function setParsedCaseItemsUri(array $data) {
        $this->parsedCaseItemsUri = $data;
    }

    public function getParsedCaseItemsUri(): array {
        return $this->parsedCaseItemsUri;
    }

    public function setParsedCaseItemsUriForReverseAssociation($data) {
        $this->parsedCaseItemsUriForReverseAssociation   =   $data;
    }

    public function getParsedCaseItemsUriForReverseAssociation() {
        return $this->parsedCaseItemsUriForReverseAssociation;
    }

    public function setParsedCaseAssociations(array $data) {
        $this->parsedCaseAssociations = $data;
    }

    public function getParsedCaseAssociations(): array {
        return $this->parsedCaseAssociations;
    }

    public function setParsedAssociationsReverseCase(array $data) {
        $this->parsedAssociationsReverseCase = $data;
    }

    public function getParsedAssociationsReverseCase(): array {
        return $this->parsedAssociationsReverseCase;
    }

    public function setParsedImportTypeItems(array $data) {
        $this->parsedImportTypeItems = $data;
    }

    public function getParsedImportTypeItems(): array {
        return $this->parsedImportTypeItems;
    }
    /**********************The final getter and setter for case package*************************/

    public function setCasePackage(array $data) {
        $this->casePackage = $data;
    }

    public function getCasePackage(): array {
        return $this->casePackage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        MetadataRepositoryInterface $metadataRepository,
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ConceptRepositoryInterface $conceptRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssociationGroupRepositoryInterface $associationGroupRepository
    )
    {
        // set the db repositories
        $this->metadataRepository = $metadataRepository;
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->subjectRepository = $subjectRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->conceptRepository = $conceptRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->associationGroupRepository = $associationGroupRepository;
        // fetch from db and set saved data relevant to case document first
        $start_time = microtime(true);
        $this->fetchAndSetSavedDocument();
     //   echo ('G1:'.(microtime(true) - $start_time));
        // fetch tenant specific metadata list
     //   $start_time = microtime(true);
        $this->fetchAndSetTenantMetadata();
      //  echo "<br>";
      //  echo ('G2:'.(microtime(true) - $start_time));
      //  $start_time = microtime(true);
        $this->fetchAndSetSavedItems();
     
        $this->fetchAndSetSavedItemAssociations();

        // extract and set all ids for case related master tables from document, items and associations fetched above
        $this->extractAndSetConceptIdsCsv();
      
        $this->extractAndSetLicenseIdsCsv();
     
        $this->extractAndSetLanguageIdsCsv();
      
        $this->extractAndSetItemTypeIdsCsv();
       
        $this->extractAndSetAssociationGroupIdsCsv();
      
        // start fetching all master table data based on ids fetched above
        $this->fetchAndSetSavedConcepts();
      
        $this->fetchAndSetSavedSubjects();
      
        $this->fetchAndSetSavedLicenses();
     
        $this->fetchAndSetSavedItemTypes();
       
        $this->fetchAndSetSavedLanguages();
    
        $this->fetchAndSetSavedAssociationGroups();
      
        // parse and set case package entities
        $this->parseAndGetCFDefinitionsDataOfItemsSaved();
      
        $this->parseAndSetCaseSubjects();
     
        $this->parseAndSetCaseLicenses();
      
        $this->parseAndSetCaseConcepts();
      
        $this->parseAndSetCaseItemTypes();
       
        $this->parseAndSetCaseAssociationGroupings();
        
        $this->parseAndSetCaseDocument();
      
        $this->parseAndSetCaseItems();
      
        $this->parseAndSetCaseDocumentInReverseAssociations();
      
        $this->parseAndSetCaseItemsInReverseAssociations();
       
        $csvExport = $this->exportKey;
        if($csvExport == 1){ //$this->exportKey == 1
          
            $this->parseAndSetCSVAssociations();
            
            $this->prepareAndSetCSVPackageData();
          
        } else {
            // prepare and set the case package data
            $this->parseAndSetCaseAssociations();
            $this->prepareAndSetCasePackageData();
        }

        // return the case package data
        return $this->getCasePackage();
    }

    /*******************************************Fetch and set case related data********************************************/

    private function fetchAndSetSavedDocument() {
        $documentId = $this->getDocumentIdentifier();
        $organizationId = $this->getRequestingUserOrganizationId();
       
        if(!is_null($organizationId)){
            $this->setOrgCode($organizationId);
        }
        $this->orgCode = $this->getOrgCode();
        $returnCollection = false;
        $fieldsToReturn = ["*"];
        $relations  =   ['nodeType', 'subjects', 'license'];
        $dataFetchingConditionalClause = [ "is_deleted" => 0];
        // set organization id when called internally
        if(!empty($organizationId)) {
            $dataFetchingConditionalClause["document_id"] = $documentId;
            $dataFetchingConditionalClause["organization_id"] = $organizationId;
        }
        else{
            $dataFetchingConditionalClause["source_document_id"] = $documentId;
        }
        
        if($this->orgCode)
        {
            $getOrganizationId = $this->getOrganizationDetails($this->orgCode); 
            $dataFetchingConditionalClause["organization_id"] =  $getOrganizationId['organization_id'];
            $savedDocument =    $this->documentRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $dataFetchingConditionalClause,
                $relations
            );
            $this->setSavedDocument($savedDocument);
        }else{
            $savedDocument =    $this->documentRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $dataFetchingConditionalClause,
                $relations
            );
            $this->setSavedDocument($savedDocument);
        }

        // set organization id from document when called externally (case api - getCFPackage)
        if(empty($organizationId)) { 
            $this->setRequestingUserOrganizationId($savedDocument["organization_id"]);
            // reset the source document identifier to primary identifier
            //$this->setDocumentIdentifier($savedDocument->document_id);
        }
    }

    private function fetchAndSetTenantMetadata() {
        $organizationId = $this->getRequestingUserOrganizationId();
        
        $returnCollection = true;
        $fieldsToReturn = ["*"];
        $dataFetchingConditionalClause = [ "organization_id" => $organizationId, "is_active" => 1 ,"is_deleted" => 0 ];
        $tenantMetadata =    $this->metadataRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $dataFetchingConditionalClause
        );
        
        $this->setTenantMetadata($tenantMetadata);
    }

    private function fetchAndSetSavedItems() {
        $savedDocument = $this->getSavedDocument();
        $documentId = $savedDocument["document_id"];
        $organizationId = $this->getRequestingUserOrganizationId();
        $returnCollection = true;
        $fieldsToReturn = ["*"];
        
        $dataFetchingConditionalClause = [ "document_id" => $documentId, "organization_id" => $organizationId, "is_deleted" => 0 ];
       
      
        $relations      =   ['nodeType'];
        $savedItems = $this->itemRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $dataFetchingConditionalClause,$relations
        );
        
        $this->setSavedItems($savedItems);
    }

    private function fetchAndSetSavedItemAssociations() {
        $itemIdArray    =   [];
        $savedDocument  =   $this->getSavedDocument();
        $savedItems     =   $this->getSavedItems();
        $documentId     =   $savedDocument["document_id"];
        $organizationId =   $this->getRequestingUserOrganizationId();

        $returnCollection = true;
        $fieldsToReturn = ["*"];
        // $dataFetchingConditionalClause = [ "document_id" => $documentId, "organization_id" => $organizationId, 'is_deleted' => 0, 'destination_document_id' => $documentId ];
        $dataFetchingConditionalClause = [ "source_document_id" => $documentId, "destination_document_id" => $documentId, "organization_id" => $organizationId, 'is_deleted' => 0];
        $savedAssociationsCollection = $this->itemAssociationRepository->findByAttributesWithSpecifiedFieldsUpdated(
            $returnCollection,
            $fieldsToReturn,
            $dataFetchingConditionalClause
        );
        $savedAssociationsWithinTaxonomy = $savedAssociationsCollection->filter(function ($item, $key) {
            return !empty($item->source_item_id) && !empty($item->source_item_association_id);
        });
        foreach($savedItems as $itemsFromTaxonomy) {
            $itemIdArray[]  =   $itemsFromTaxonomy["item_id"];
        }

        $attributeIn                =   'target_item_id';
        $containedInValues          =   $itemIdArray;
        $keyValuePairs              =   ['target_document_id' => $documentId, 'destination_document_id' => $documentId,'organization_id' => $organizationId, 'is_deleted' => 0];
        $notInKeyValuePairs         =   ['source_document_id'  =>  $documentId];
        $returnPaginatedData        =   false;
        $paginationFilters          =   ['limit' => 10, 'offset' => 0];
        $returnSortedData           =   false;
        $sortfilters                =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator                   =   'AND';
        $relations                  =   ['document', 'sourceItemId', 'sourceDocumentNode', 'targetItemId', 'targetDocumentNode'];

        $savedAssociationsCollectionForReverseAssociation = $this->itemAssociationRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
            $attributeIn,
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $notInKeyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator,
            $relations);


        $savedReverseAssociations = $savedAssociationsCollectionForReverseAssociation->filter(function ($item, $key) {
            return !empty($item->source_item_id) && !empty($item->source_item_association_id);
        });

        $this->setReverseAssociations($savedReverseAssociations);

        $savedAssociations  =   $savedReverseAssociations->merge($savedAssociationsWithinTaxonomy);

        $this->setSavedAssociations($savedAssociations);
    }

    private function fetchAndSetSavedConcepts() {
        $savedConcepts = $this->helperToFetchDataBasedOnProvidedType("concept");
        $this->setSavedConcepts($savedConcepts);
    }

    private function fetchAndSetSavedSubjects() {
        $savedDocument = $this->getSavedDocument();
        
        $this->setSavedSubjects($savedDocument["subjects"]);
    }

    private function fetchAndSetSavedLicenses() {
        $savedLicenses = $this->helperToFetchDataBasedOnProvidedType("license");
        $this->setSavedLicenses($savedLicenses);
    }

    private function fetchAndSetSavedItemTypes() {
        $savedItemTypes = $this->helperToFetchDataBasedOnProvidedType("itemType");
        $this->setSavedItemTypes($savedItemTypes);
    }

    private function fetchAndSetSavedLanguages() {
        $savedLanguages = $this->helperToFetchDataBasedOnProvidedType("language");
        $this->setSavedLanguages($savedLanguages);
    }

    private function fetchAndSetSavedAssociationGroups() {
        $savedAssociationGroups = $this->helperToFetchDataBasedOnProvidedType("associationGroup"); 
        $this->setSavedAssociationGroups($savedAssociationGroups);
    }

    /**************************Extract and set csv of ids for master tables related to CASE package*******************************/

    private function extractAndSetConceptIdsCsv() {
        $savedItems = $this->getSavedItems();
        
        $conceptIdsCsv = !empty($savedItems) ? implode("," , array_column($savedItems, "concept_id")) : "";
        $this->setConceptIdsCsv($conceptIdsCsv);
    }

    private function extractAndSetLicenseIdsCsv() {
        $savedItems = $this->getSavedItems();
        $savedDocument = $this->getSavedDocument();
        $itemLicenseIdsCsv = !empty($savedItems) ? implode("," , array_column($savedItems, "license_id")) : "";

        $documentLicenseId = !empty($savedDocument) ? implode("," , array_column($savedDocument, "license_id")) : "";

        $licenseIdsCsv = $documentLicenseId;
        if($itemLicenseIdsCsv!=="") {
            $licenseIdsCsv = $itemLicenseIdsCsv.",".$documentLicenseId;
        }

        $this->setLicenseIdsCsv($licenseIdsCsv);
    }

    private function extractAndSetLanguageIdsCsv() {
        $savedItems = $this->getSavedItems();
        $savedDocument = $this->getSavedDocument();
        $itemLanguageIdsCsv = !empty($savedItems) ? implode("," , array_column($savedItems, "language_id")) : "";
        $documentLanguageId = !empty($savedDocument) ? implode("," , array_column($savedDocument, "language_id")) : "";
        $languageIdsCsv = $documentLanguageId;
        if($itemLanguageIdsCsv!=="" || !empty($documentLanguageId)) {
            $languageIdsCsv = $itemLanguageIdsCsv.",".$documentLanguageId;
        }
        $this->setLanguageIdsCsv($languageIdsCsv);
    }

    private function extractAndSetItemTypeIdsCsv() {
        $savedItems = $this->getSavedItems();
        $itemTypeIdsCsv = !empty($savedItems) ? implode("," , array_column($savedItems, "node_type_id")) : "";
        $this->setItemTypeIdsCsv($itemTypeIdsCsv);
    }

    private function extractAndSetAssociationGroupIdsCsv() {
        $savedAssociations = $this->getSavedAssociations();
        $associationGroupIdsCsv = $savedAssociations->isNotEmpty() ? $savedAssociations->implode("association_group_id", ",") : "";
        $this->setAssociationGroupIdsCsv($associationGroupIdsCsv);
    }

    /*******************************Following methods will be used to parse and set case related entities**********************************/
    private function parseAndGetCFDefinitionsDataOfItemsSaved() {
        $savedDocument      =   $this->getSavedDocument();
        $savedItems         =   $this->getSavedItems();
        $orgCode            =   $this->getOrgCode();

        $sourceLicenseUriObject     =   [];
        $sourceConceptUriObject     =   [];
        $sourceItemTypeUriObject    =   [];

      foreach($savedItems as $item) {
            $sourceLicenseUriObject[] = [
                json_decode($item["source_license_uri_object"], true)
            ];

            $sourceConceptUriObject[] = [
                json_decode($item["source_concept_uri_object"], true)
            ];

            $sourceItemTypeUriObject[] = [
                json_decode($item["source_node_type_uri_object"], true)
            ];
        }
       

        //For License in Document put in SourceuriObject
        $sourceLicenseUriObject[] = [
            json_decode($savedDocument["source_license_uri_object"], true)
        ];

        $this->setSourceConceptUriObjectOfSavedItems($sourceConceptUriObject);
        $this->setSourceLicenseUriObjectOfSavedItems($sourceLicenseUriObject);
        $this->setSourceItemTypeUriObjectOfSavedItems($sourceItemTypeUriObject);

    }

    private function parseAndSetCaseSubjects() {
        $savedDocument      =   $this->getSavedDocument();
        $savedSubjects = $this->getSavedSubjects();
        $orgCode       = $this->getOrgCode();
        $is_html       = $this->is_html;
        $parsedSubjects = [];
        $parsedSubjectsUri = [];
        $parsedSubjectsTitle = [];
        
        foreach($savedSubjects as $subject) {
            $identifier = $subject['source_subject_id'];
            $primaryIdentifier = $subject["subject_id"];
            $uri = '';

            $title = $this->isMetadataActive('subject_title') ? (!empty($subject["title"]) ? $subject["title"] : "") : "";
            if($is_html == 1)
            {
                $title = $subject["title_html"];
            }
            $hierarchyCode = $this->isMetadataActive('subject_hierarchy_code') ?
                (!empty($subject["hierarchy_code"]) ? $subject["hierarchy_code"] : "") :
                "";
            $description = $this->isMetadataActive('subject_description') ? (!empty($subject["description"]) ? $subject["description"] : "") : "";
            if($is_html == 1)
            {
                $description = $subject["description_html"];
            }
            $lastChangeDateTime = !empty($subject["updated_at"]) ? $this->formatDateTimeToISO8601($subject["updated_at"]) : $this->formatDateTimeToISO8601($subject["created_at"]);

            if($savedDocument["import_type"] == 2) {
                $identifier         =   $primaryIdentifier;
                $uri                =   $this->getCaseApiUri("CFSubjects", $identifier, true, $orgCode,$this->domainName);
            } else {
                if($savedDocument["import_type"] == 1) {
                    $json_array  = json_decode($savedDocument["subjects"][0]['pivot']['source_subject_uri_object'], true); //object params converted to array to resolve error
                    $uri = $json_array['uri'];
                } else {
                    $uri                =   $this->getCaseApiUri("CFSubjects", $identifier, true, $orgCode,$this->domainName);
                }
            }
            if($uri == null || $uri == "")
            {
                $uri = $this->getCaseApiUri("CFSubjects", $uri, true, $orgCode,$this->domainName);
            }
            $subDetailArr = [
                "identifier" => $identifier,
                "uri" => $uri,
                "title" => $title,
                "hierarchyCode" => $hierarchyCode,
                "description" => $description,
                "lastChangeDateTime" => $lastChangeDateTime
            ];

                if($description == "")
                {
                    unset($subDetailArr['description']);
                }

            $parsedSubjects[] = $subDetailArr;

            $parsedSubjectsUri[] = [
                "identifier" => $identifier,
                "uri" => $uri,
                "title" => $title
            ];

            $parsedSubjectsTitle[] = $title;
        }

        $this->setParsedCaseSubjects($parsedSubjects);
        $this->setParsedCaseSubjectsUri($parsedSubjectsUri);
        $this->setParsedCaseSubjectsTitle($parsedSubjectsTitle);
    }

    private function parseAndSetCaseLicenses() {
        $savedDocument      =   $this->getSavedDocument();
        $savedLicenses      =   $this->getSavedLicenses(); 
        $orgCode            =   $this->getOrgCode();
        $parsedCaseLicenses     = [];
        $parsedCaseLicensesUri  = [];

        $sourceLicenseUriObjectForSavedItems = $this->getSourceLicenseUriObjectOfSavedItems();
        $sourceLicenseIdentifier = [];
       
        foreach($sourceLicenseUriObjectForSavedItems as $key => $sourceLicenseObject) {
            $sourceLicenseIdentifier[$key]  =   $sourceLicenseObject[0]['identifier'];
        }

        foreach($sourceLicenseUriObjectForSavedItems as $key => $sourceLicenseObject) {
            $sourceLicenseIdentifier[$sourceLicenseObject[0]['title']]  = ['title' => $sourceLicenseObject[0]['title'],'identifier' => $sourceLicenseObject[0]['identifier'], 'uri' => $sourceLicenseObject[0]['uri']] ;
        }

        foreach($savedLicenses as $license) {
            if(!empty($license["title"])){
                $uri = '';
                $identifier = $license["source_license_id"];
                $primaryIdentifier = $license["license_id"];
                $is_html       = $this->is_html;
                //$uri = ($savedDocument->import_type != 0 && $savedDocument->import_type == 3) ? $this->getCaseApiUri("CFLicenses", $identifier,$orgCode) : $license->uri;
                $title = $this->isMetadataActive('license_title') ? (!empty($license["title"]) ? $license["title"] : "") : "";
                if ($is_html == 1) {
                    //code change for UF-2069 to handle $license is coverted to array 
                    //$license->title_html this code is not used becasue object coveretd to array now

                    $title = $license['title_html'];
                }
                $description = $this->isMetadataActive('license_description') ? (!empty($license["description"]) ? $license["description"] : "") : "";
                if($is_html == 1)
                {
                    $description = $license["description_html"];
                }
                $licenseText =   $this->isMetadataActive('license_text') ?
                    (!empty($license["license_text"]) ? $license["license_text"] : "") :
                    "";
                if($is_html == 1)
                {
                    $licenseText = $license["license_text_html"];
                }
                $lastChangeDateTime = !empty($license["updated_at"]) ? $this->formatDateTimeToISO8601($license["updated_at"]) : $this->formatDateTimeToISO8601($license["created_at"]);

                if($savedDocument["import_type"] == 2) {
                    $identifier         =   $primaryIdentifier;
                    $uri                =   $this->getCaseApiUri("CFLicenses", $identifier, true, $orgCode,$this->domainName);
                } else {
                    if($savedDocument["import_type"] == 1) {
                        if($key = array_search($identifier, $sourceLicenseIdentifier)) {
                            $uri = $license["uri"];
                        } else {
                            $identifier = isset($sourceLicenseIdentifier[$title])? $sourceLicenseIdentifier[$title]['identifier'] : $identifier;
                            $uri = isset($sourceLicenseIdentifier[$title])?  $sourceLicenseIdentifier[$title]['uri'] : $license["uri"] ;
                        }
                    } else {
                        if($key = array_search($identifier, $sourceLicenseIdentifier)) {
                            $uri = $this->getCaseApiUri("CFLicenses", $identifier, true, $orgCode,$this->domainName);
                        } else {
                            $identifier = isset($sourceLicenseIdentifier[$title])? $sourceLicenseIdentifier[$title]['identifier'] : $identifier;
                            $uri = isset($sourceLicenseIdentifier[$title])? $this->getCaseApiUri("CFLicenses", $sourceLicenseIdentifier[$title]['identifier'], true, $orgCode,$this->domainName) : $license["uri"] ; //$sourceItemTypeUriObjectForSavedItems[$key][0]['uri'];
                        }
                    }
                }

                $licenseDetailArr =  [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $title,
                    "licenseText" => $licenseText,
                    "description" => $description,
                    "lastChangeDateTime" => $lastChangeDateTime
                ];
                if($description == "")
                {
                    unset($licenseDetailArr['description']);
                }
                $parsedCaseLicenses[] = $licenseDetailArr;
                $csvExport = $this->exportKey;
               if($csvExport == 1)
               {
                    $licenseCsvDataArr = [
                        "identifier" => $identifier,
                        "uri" => $uri,
                        "title" => $title,
                        "licenseText" => $licenseText,
                        "description" => $description,
                        "lastChangeDateTime" => $lastChangeDateTime
                    ];
                    $parsedCaseLicensesUri[$primaryIdentifier] = $licenseCsvDataArr;
               }
               else
               {
                   $licenseDataArr = [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $title,
                ];
                $parsedCaseLicensesUri[$primaryIdentifier] = $licenseDataArr;
               }
         
            }

        }
        $this->setParsedCaseLicenses($parsedCaseLicenses);
        $this->setParsedCaseLicensesUri($parsedCaseLicensesUri);
    }

    private function parseAndSetCaseConcepts() {
        $savedDocument      =   $this->getSavedDocument();
        $savedConcepts      =   $this->getSavedConcepts();
        $orgCode            =   $this->getOrgCode();
        $is_html            =   $this->is_html;

        $sourceConceptUriObjectForSavedItems = $this->getSourceConceptUriObjectOfSavedItems();

        foreach($sourceConceptUriObjectForSavedItems as $key => $sourceConceptObject) {
            $sourceConceptIdentifier[$sourceConceptObject[0]['title']]  = ['tile' => $sourceConceptObject[0]['title'],'identifier' => $sourceConceptObject[0]['identifier'], 'uri' => $sourceConceptObject[0]['uri']] ;
        }

        $parsedCaseConcepts = [];
        $parsedCaseConceptsUri = [];
        $parsedCaseConceptsKeyword = [];
        
        foreach($savedConcepts as $concept) {
            if(!empty($concept["keywords"])){
                $uri                =   '';
                $identifier         = $concept["source_concept_id"];
                $primaryIdentifier  = $concept["concept_id"];
                $keywords           = $this->isMetadataActive('concept_keywords') ? (!empty($concept["keywords"]) ? $concept["keywords"] : "") : "";

                //$uri = ($savedDocument->import_type == 3) ? $this->getCaseApiUri("CFConcepts", $identifier,$orgCode) :$concept->uri;
                $title = $this->isMetadataActive('concept_title') ? (!empty($concept["title"]) ? $concept["title"] : "") : "";
                if($is_html == 1)
                {
                    $title = $concept["title_html"];
                }
                $description = $this->isMetadataActive('concept_description') ? (!empty($concept["description"]) ? $concept["description"] : "") : "";
                if($is_html == 1)
                {
                    $description = $concept["description_html"];
                }
                $hierarchyCode =   $this->isMetadataActive('concept_hierarchy_code') ?
                    (!empty($concept["hierarchy_code"]) ? $concept["hierarchy_code"] : "") :
                    "";
                $lastChangeDateTime = !empty($concept["updated_at"]) ? $this->formatDateTimeToISO8601($concept["updated_at"]) : $this->formatDateTimeToISO8601($concept["created_at"]);

                if($savedDocument["import_type"] == 2) {
                    $identifier         =   $primaryIdentifier;
                    $uri                =   $this->getCaseApiUri("CFConcepts", $identifier, true, $orgCode,$this->domainName);
                } else {
                    if($savedDocument["import_type"] == 1) {
                        if($key = array_search($identifier, $sourceConceptIdentifier)) {
                            $uri = $concept["uri"];
                        } else {
                            $identifier = isset($sourceConceptIdentifier[$title])? $sourceConceptIdentifier[$title]['identifier'] : $identifier;
                            $uri = isset($sourceConceptIdentifier[$title])?  $sourceConceptIdentifier[$title]['uri'] : $concept["uri"] ;
                        }
                    } else {
                        if($key = array_search($identifier, $sourceConceptIdentifier)) {
                            $uri = $this->getCaseApiUri("CFConcepts", $identifier, true, $orgCode,$this->domainName);
                        } else {
                            $identifier = isset($sourceConceptIdentifier[$title])? $sourceConceptIdentifier[$title]['identifier'] : $identifier;
                            $uri = isset($sourceConceptIdentifier[$title])? $this->getCaseApiUri("CFConcepts", $sourceConceptIdentifier[$title]['identifier'], true, $orgCode,$this->domainName) : $concept["uri"]; //$sourceItemTypeUriObjectForSavedItems[$key][0]['uri'];
                        }
                    }
                }

                $conceptDetailsArr = [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $title,
                    "keywords" => $keywords,
                    "hierarchyCode" => $hierarchyCode,
                    "description" => $description,
                    "lastChangeDateTime" => $lastChangeDateTime
                ];
                if($keywords == "")
                {
                    unset($conceptDetailsArr['keywords']);
                }
                if($description == "")
                {
                    unset($conceptDetailsArr['description']);
                }
                $parsedCaseConcepts[] = $conceptDetailsArr;


               // comment code json export not for csv csv need new function for get those data
               $csvExport = $this->exportKey;
               if($csvExport == 1)
               {
                    $conceptDataArrCSV = [
                        "identifier" => $identifier,
                        "uri" => $uri,
                        "title" => $title,
                        "keywords" => $keywords,
                        "hierarchyCode" => $hierarchyCode,
                        "description" => $description,
                    ];
                    $parsedCaseConceptsUri[$primaryIdentifier] = $conceptDataArrCSV;
               }
               else
               {
                    $conceptDataArr = [
                        "identifier" => $identifier,
                        "uri" => $uri,
                        "title" => $title,
                    ];
                    $parsedCaseConceptsUri[$primaryIdentifier] = $conceptDataArr;
               }
               
               

                $parsedCaseConceptsKeyword[$primaryIdentifier][] = $keywords;
            }
        }
        
        $this->setParsedCaseConcepts($parsedCaseConcepts);
        $this->setParsedCaseConceptsUri($parsedCaseConceptsUri);
        $this->setParsedCaseConceptsKeyword($parsedCaseConceptsKeyword);
    }

    private function parseAndSetCaseItemTypes() {
        $savedDocument      =   $this->getSavedDocument();
        $savedItemTypes = $this->getSavedItemTypes();
        $orgCode        = $this->getOrgCode();
        $is_html        = $this->is_html;

        $sourceItemTypeUriObjectForSavedItems = $this->getSourceItemTypeUriObjectOfSavedItems();

        foreach($sourceItemTypeUriObjectForSavedItems as $key => $sourceItemTypeObject) {
            $sourceItemTypeIdentifier[$sourceItemTypeObject[0]['title']]  = ['tile' => $sourceItemTypeObject[0]['title'],'identifier' => $sourceItemTypeObject[0]['identifier'], 'uri' => $sourceItemTypeObject[0]['uri']] ;
        }

        $parsedCaseItemTypes = [];
        $parsedCaseItemTypesUri = [];
        
        foreach($savedItemTypes as $itemType) {
            if(!empty($itemType["title"])) {
                $uri                =   '';
                $identifier         = $itemType["source_node_type_id"];
                $primaryIdentifier  = $itemType["node_type_id"];
                //$uri                = ($savedDocument->import_type != 1) ?  $this->getCaseApiUri("CFItemTypes", $identifier, true, $orgCode) : $itemType->uri;
                $title = !empty($itemType["title"]) ? $itemType["title"] : "";
                $description = !empty($itemType["description"]) ? $itemType["description"] : "";
                $hierarchyCode = !empty($itemType["hierarchy_code"]) ? $itemType["hierarchy_code"] : "";
                $typeCode = !empty($itemType["type_code"]) ? $itemType["type_code"] : "";
                $lastChangeDateTime = !empty($itemType["updated_at"]) ? $this->formatDateTimeToISO8601($itemType["updated_at"]) : $this->formatDateTimeToISO8601($itemType["created_at"]);

                if($savedDocument["import_type"] == 2) {
                    $identifier         =   $primaryIdentifier;
                    $uri                =   $this->getCaseApiUri("CFItemTypes", $identifier, true, $orgCode,$this->domainName);
                } else {
                    if($savedDocument["import_type"] == 1) {
                        if($key = array_search($identifier, $sourceItemTypeIdentifier)) {
                            $uri = $itemType["uri"];
                        } else {
                            $identifier = isset($sourceItemTypeIdentifier[$title])? $sourceItemTypeIdentifier[$title]['identifier'] : $identifier;
                            $uri = isset($sourceItemTypeIdentifier[$title])?  $sourceItemTypeIdentifier[$title]['uri'] : $itemType["uri"] ;
                        }
                    } else {
                        if($key = array_search($identifier, $sourceItemTypeIdentifier)) {
                            $uri = $this->getCaseApiUri("CFItemTypes", $identifier, true, $orgCode,$this->domainName);
                        } else {
                            $identifier = isset($sourceItemTypeIdentifier[$title])? $sourceItemTypeIdentifier[$title]['identifier'] : $identifier;
                            $uri = isset($sourceItemTypeIdentifier[$title])? $this->getCaseApiUri("CFItemTypes", $sourceItemTypeIdentifier[$title]['identifier'], true, $orgCode,$this->domainName) : $itemType["uri"] ;; //$sourceItemTypeUriObjectForSavedItems[$key][0]['uri'];
                        }
                    }
                }
                $ItemTypesArr = [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $title,
                    "hierarchyCode" => $hierarchyCode,
                    "description" => $description,
                    "typeCode" => $typeCode,
                    "lastChangeDateTime" => $lastChangeDateTime
                ];

                if($typeCode == "")
                {
                    unset($ItemTypesArr['typeCode']);
                }
                $parsedCaseItemTypes[] = $ItemTypesArr;

                $parsedCaseItemTypesUri[$primaryIdentifier] = [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $title
                ];
            }


        }

        $this->setParsedCaseItemTypes($parsedCaseItemTypes);
        $this->setParsedCaseItemTypesUri($parsedCaseItemTypesUri);
    }

    private function parseAndSetCaseAssociationGroupings() {
        $savedDocument      =   $this->getSavedDocument();
        $savedAssociationGroups = $this->getSavedAssociationGroups();
        $orgCode                = $this->getOrgCode();
        $is_html                = $this->is_html;
        $parsedCaseAssociationGroups = [];
        $parsedCaseAssociationGroupsUri = [];
        
        foreach($savedAssociationGroups as $associationGroup) {
            $identifier = $associationGroup["source_association_group_id"];
            $primaryIdentifier = $associationGroup["association_group_id"];
            $uri = ($savedDocument["import_type"] == 1) ? $associationGroup["uri"] : $this->getCaseApiUri("CFAssociationGroupings", $identifier, true, $orgCode,$this->domainName);
            $title = !empty($associationGroup["title"]) ? $associationGroup["title"] : "";
            $description = !empty($associationGroup["description"]) ? $associationGroup["description"] : "";
            $lastChangeDateTime = !empty($associationGroup["updated_at"]) ? $this->formatDateTimeToISO8601($associationGroup["updated_at"]) : $this->formatDateTimeToISO8601($associationGroup["created_at"]);



            $parsedCaseAssociationGroups[] = [
                "identifier" => $identifier,
                "uri" => $uri,
                "title" => $title,
                "description" => $description,
                "lastChangeDateTime" => $lastChangeDateTime
            ];

            $parsedCaseAssociationGroupsUri[$primaryIdentifier] = [
                "identifier" => $identifier,
                "uri" => $uri,
                "title" => $title
            ];
        }
        $this->setParsedCaseAssociationGroupings($parsedCaseAssociationGroups);
        $this->setParsedCaseAssociationGroupingsUri($parsedCaseAssociationGroupsUri);
    }

    /**
     * Parse Document from database and format into CASE json
     *
     * @return void
     */
    private function parseAndSetCaseDocument() {
        $savedDocument  =   $this->getSavedDocument();
        $identifier     =   $savedDocument["source_document_id"];
        
        $nodeType       =   $savedDocument["node_type"]["title"];
        $orgCode        =   $this->getOrgCode();
        $is_html        =   $this->is_html;
        $uri            =   ($savedDocument["import_type"] == 1) ? $savedDocument["uri"] : $this->getCaseApiUri("CFDocuments", $identifier, true, $orgCode,$this->domainName);
        $creator        =   !empty($savedDocument["creator"]) ? $savedDocument["creator"] : "";
        $title          =   $this->isMetadataActive("title") ? (!empty($savedDocument["title"]) ? $savedDocument["title"] : "") : "";
        if($is_html == 1)
        {
            $title = $savedDocument["title_html"];
        }
       
        $lastChangeDateTime =   !empty($savedDocument["updated_at"]) ? $this->formatDateTimeToISO8601($savedDocument["updated_at"]) : $this->formatDateTimeToISO8601($savedDocument["created_at"]);
        $officialSourceURL  =   $this->isMetadataActive("official_source_url") ?
            (!empty($savedDocument["official_source_url"]) ? $savedDocument["official_source_url"] : "") :
            "";
        $publisher = $this->isMetadataActive("publisher") ?
            (!empty($savedDocument["publisher"]) ? $savedDocument["publisher"] : "") :
            "";
        if($is_html == 1)
        {
            $publisher = $savedDocument["publisher_html"];
        }

        $description = $this->isMetadataActive("description") ?
            (!empty($savedDocument["description"]) ? $savedDocument["description"] : "") :
            "";
        if($is_html == 1)
        {
            $description = $savedDocument["description_html"];
        }
        $subject = $this->getParsedCaseSubjectsTitle();
        $subjectsURI = $this->getParsedCaseSubjectsUri();
        $language = $this->isMetadataActive("language") ?
            (!empty($savedDocument["language_id"]) ? $this->searchForAndReturnLanguageShortCode($savedDocument["language_id"]) : "") :
            "";
        $version = $this->isMetadataActive("version") ? (!empty($savedDocument["version"]) ? $savedDocument["version"] : "") : "";
        $adoptionStatus = !empty($savedDocument["adoption_status"]) ?
            $this->getSystemSpecifiedAdoptionStatusText($savedDocument["adoption_status"]) :
            "Draft";
        $statusStartDate=$this->isMetadataActive("status_start_date") ?
            (!empty($savedDocument["status_start_date"]) ? $this->formatDateTimeToDateOnly($savedDocument["status_start_date"]) : "") :
            "";
        $statusEndDate = $this->isMetadataActive("status_end_date") ?
            (!empty($savedDocument["status_end_date"]) ? $this->formatDateTimeToDateOnly($savedDocument["status_end_date"]) : "") :
            "";
        $licenseUri     =   ($savedDocument["import_type"] == 1) ? json_decode($savedDocument["source_license_uri_object"], true): (!empty($savedDocument["license_id"]) ? $this->helperToSearchAndReturnLicenseUri($savedDocument["license_id"]) : []); //true since it was considered it as object instead of an array
        if($savedDocument["import_type"] == 2)
        {
            if(!empty($licenseUri))
            {
                $licenseUri = $this->helperToSearchAndReturnLicenseUri($savedDocument["license_id"]);
                if($is_html ==1){
                    $licenseUri = $this->helperToSearchAndReturnLicenseUri($savedDocument["license_id"]);
                }
            }
            else
            {
                $licenseUri = json_decode($savedDocument["source_license_uri_object"], true);
            }
        }
		// To resolve object to array error
        if(!empty($licenseUri) && !is_array($licenseUri)){
            $licenseUri = json_decode(json_encode($licenseUri), true);
        }
                if(isset($licenseUri['description']))
                {
                    unset($licenseUri['description']);
                }
                if(isset($licenseUri['licenseText']))
                {
                    unset($licenseUri['licenseText']);
                }
                    $licensestr = isset($licenseUri['uri'])?$licenseUri['uri']:"";
                    if(substr($licensestr,0,8) == "/api/v1/")
                    {
                        $licenseUri['uri'] = substr($licensestr,8);
                    }


        $notes = $this->isMetadataActive("notes") ? (!empty($savedDocument["notes"]) ? $savedDocument["notes"] : "") : "";
        if($is_html == 1)
        {
            $notes = $savedDocument["notes_html"];
        }
        $casePackageUri = [
            "identifier" => $identifier,
            "uri" => ($savedDocument["import_type"] == 1) ? str_replace('CFDocuments',  'CFPackages', $savedDocument["uri"]) : $this->getCaseApiUri("CFPackages", $identifier, true, $orgCode,$this->domainName),
            "title" => $title
        ];

        $caseDocumentData = [
            "identifier" => isset($identifier)?$identifier:"",
            "uri"        => isset($uri)?$uri:'',
            "creator"    => $creator,
            "title" => $title,
            "lastChangeDateTime" => $lastChangeDateTime,
            "officialSourceURL" => isset($officialSourceURL)?$officialSourceURL:"",
            "publisher" => isset($publisher)?$publisher:'',
            "description" => $description,
            "subject" => $subject,
            "subjectURI" => isset($subjectsURI)?$subjectsURI:"",
            "language" => $language,
            "version" => $version,
            "adoptionStatus" => $adoptionStatus,
            "statusStartDate" => $statusStartDate,
            "statusEndDate" => $statusEndDate,
            "licenseURI" => isset($licenseUri)?$licenseUri:"",
            "notes" => $notes,
            "CFPackageURI" => $casePackageUri,
            "nodeTypeId" => $savedDocument["node_type_id"]
        ];
        if($savedDocument["updated_at"] == "0000-00-00 00:00:00" || $lastChangeDateTime == "")
        {
            unset($caseDocumentData['lastChangeDateTime']);
        }
        if($savedDocument["official_source_url"] == "")
        {
            unset($caseDocumentData['officialSourceURL']);
        }
       if($publisher == "")
        {
            unset($caseDocumentData['publisher']);
        }
        if($description == "")
        {
            unset($caseDocumentData['description']);
        }
        if(empty($subject))
        {
            unset($caseDocumentData['subject']);
        }
        if(empty($subjectsURI))
        {
            unset($caseDocumentData['subjectURI']);
        }
        if($language == "")
        {
            unset($caseDocumentData['language']);
        }
        if($version == "")
        {
            unset($caseDocumentData['version']);
        }
        if($adoptionStatus == "")
        {
            unset($caseDocumentData['adoptionStatus']);
        }
        if($savedDocument["status_start_date"] == "0000-00-00 00:00:00" || $statusStartDate == "")
        {
            unset($caseDocumentData['statusStartDate']);
        }
        if($savedDocument["status_end_date"] == "0000-00-00 00:00:00" || $statusEndDate == "")
        {
            unset($caseDocumentData['statusEndDate']);
        }
        if(empty($licenseUri))
        {
            unset($caseDocumentData['licenseURI']);
        }
        if($notes == "")
        {
            unset($caseDocumentData['notes']);
        }
        $caseDocumentUri = [
            "identifier" => $identifier,
            "uri" => $uri,
            "title" => $title
        ];
		/****************** If Versioning is true, get version info **********************/
        if($this->versioning==true){
            $publishNum = 1;
            $versionTag = $this->versionTag;
            $created_by = $this->publishedBy;
            $publisherName = '';
			// If publishedBy is sent that means it is being published, get max of version.. rest parameters are already sent
            if(!empty($this->publishedBy)){
                $publishNum = TaxonomyPubStateHistory::
                        where('document_id',$savedDocument['document_id'])
                        ->where('organization_id',$savedDocument['organization_id'])
                        ->where('is_deleted',0)
                        ->max('publish_number');
            }
            else{
			// This means already published, get latest version details
                $publishTaxonomy = TaxonomyPubStateHistory::
                        where('document_id',$savedDocument['document_id'])
                        ->where('organization_id',$savedDocument['organization_id'])
                        ->where('is_deleted',0)
                        ->orderBy('publish_number','desc')
                        ->first();
                if(isset($publishTaxonomy->document_id)){
                    $publishNum = $publishTaxonomy->publish_number;
                    $versionTag = $publishTaxonomy->tag;
                    $created_by = $publishTaxonomy->created_by;                    
                }
            }
			// To get name of publisher of current logged in user or last published by
            if(!empty($created_by)){
                $getCreatedUser = DB::table('users')
                    ->select('user_id','first_name','last_name')
                    ->where('user_id', $created_by)
                    ->where('is_deleted',0)
                    ->first();
                $publisherName = isset($getCreatedUser->user_id) ? $getCreatedUser->first_name.' '.$getCreatedUser->last_name : '';
            }            
			// Save the version info
            $this->versionInfo = [
                'version'         => $publishNum,
                'tag'             => $versionTag,
                'caseServerUrl'   => ($savedDocument["import_type"] == 1) ? str_replace('CFDocuments',  'CFPackages', $savedDocument["uri"]) : $this->getCaseApiUri("CFPackages", $identifier, true, $orgCode,$this->domainName),
                'publisherName'   => $publisherName,
                'orgCode'         => $orgCode
            ];
			
        }

        $this->setParsedCaseDocument($caseDocumentData);
        $this->setParsedCaseDocumentUri($caseDocumentUri);

    }

    private function parseAndSetCaseItems() {
        $savedDocument      =   $this->getSavedDocument();
        $savedItems         =   $this->getSavedItems();
        $CFDocumentURI      =   $this->getParsedCaseDocumentUri();
        $parsedCaseItems    =   [];
        $parsedCaseItemsUri =   [];
        $orgCode            =   $this->getOrgCode();

        $parsedImportTypeItem = [];

        $is_html            =  $this->is_html;

        foreach($savedItems as $item) {
            $identifier         =   $item["source_item_id"];
            $primaryIdentifier  =   $item["item_id"];
            $nodeType           =   $item["node_type"]["title"];
            $fullStatement      =   $this->isMetadataActive('full_statement') ? (!empty($item["full_statement"]) ? $item["full_statement"] : "") : "";
            if($is_html == 1)
            {
                $fullStatement = $item["full_statement_html"];
            }
            $alternativeLabel   =   $this->isMetadataActive('alternative_label') ?
                (!empty($item["alternative_label"]) ? $item["alternative_label"] : "") :
                "";
            if($is_html == 1)
            {
                $alternativeLabel = $item["alternative_label_html"];
            }

            $uri                    =   ($savedDocument["import_type"] == 1) ? $item["uri"] : $this->getCaseApiUri("CFItems", $identifier, true, $orgCode,$this->domainName) ;
            if($item["import_type"] == 2)
            {
                $uri = $this->getCaseApiUri("CFItems", $identifier, true, $orgCode,$this->domainName);
            }
            
            $humanCodingScheme      =   $this->isMetadataActive('human_coding_scheme') ?
                (!empty($item["human_coding_scheme"]) ? $item["human_coding_scheme"] : "") :
                "";
               
            if($is_html == 1)
            {
                $humanCodingScheme = $item["human_coding_scheme_html"];
            }
            $listEnumeration        =   $this->isMetadataActive('list_enumeration') ?
                (!empty($item["list_enumeration"]) ? $item["list_enumeration"] : "") :
                "";
            $abbreviatedStatement   =    $this->isMetadataActive('abbreviated_statement') ?
                (!empty($item["abbreviated_statement"]) ? $item["abbreviated_statement"] : "") :
                "";
            if($is_html == 1)
            {
                $abbreviatedStatement = $item["abbreviated_statement_html"];
            }
            $conceptKeywordsURI     =   ($savedDocument["import_type"] == 1) ? json_decode($item["source_concept_uri_object"]): (!empty($item["concept_id"]) ? $this->helperToSearchAndReturnConceptUri($item["concept_id"]) : []);
            if($item["import_type"] == 2)
            {
                if(!empty($conceptKeywordsURI))
                {
                    $conceptKeywordsURI = json_decode($item["source_concept_uri_object"], true);
                    if($is_html == 1)
                    {
                        if(!empty($item["concept_id"]))
                        {
                            $conceptKeywordsURI = $this->helperToSearchAndReturnConceptUri($item["concept_id"]);
                        }

                    }
                    else
                    {
                        $conceptKeywordsURI = $this->helperToSearchAndReturnConceptUri($item["concept_id"]);
                    }
                }
            }
            // remove extra value
			// To resolve object to array error
                if(!empty($conceptKeywordsURI) && !is_array($conceptKeywordsURI)){
                    $conceptKeywordsURI = json_decode(json_encode($conceptKeywordsURI), true);
                }              
                    
                if(isset($conceptKeywordsURI['keywords']))
                {
                    unset($conceptKeywordsURI['keywords']);
                }
                if(isset($conceptKeywordsURI['description']))
                {
                    unset($conceptKeywordsURI['description']);
                }
                if(isset($conceptKeywordsURI['hierarchyCode']))
                {
                    unset($conceptKeywordsURI['hierarchyCode']);
                }

            $conceptKeywords        =   !empty($item["concept"]["concept_id"]) ? $this->helperToSearchAndReturnConceptKeyword($item["concept"]["concept_id"]) :  "";
            $notes                  =   $this->isMetadataActive("notes") ? (!empty($item["notes"]) ? $item["notes"] : "") : "";
            if($is_html == 1)
            {
                $notes = $item["notes_html"];
            }
            $language               =   $this->isMetadataActive("language") ?
                (!empty($item["language_id"]) ? $this->searchForAndReturnLanguageShortCode($item["language_id"]) : "") :
                "";
            $educationLevel         =   $this->isMetadataActive("education_level") ?
                (!empty($item["education_level"]) ? $this->createArrayFromCommaeSeparatedString($item["education_level"]) : "") :
                "";

            $CFItemTypeUri = ($savedDocument["import_type"] == 1) ? json_decode($item["source_node_type_uri_object"], true): (!empty($item["node_type_id"]) ? $this->helperToSearchAndReturnItemTypeUri($item["node_type_id"]) : "");
            if($item["import_type"] == 2)
            {
                if(!empty($CFItemTypeUri))
                {
                    $CFItemTypeUri = json_decode($item["source_node_type_uri_object"], true);
                }
            }
			// To resolve object to array error
			if(!empty($CFItemTypeUri) && !is_array($CFItemTypeUri)){
                $CFItemTypeUri = json_decode(json_encode($CFItemTypeUri), true);
            }
            $CFItemType = !empty($CFItemTypeUri["title"]) ? $CFItemTypeUri["title"] : "";


            $licenseUri     =   ($savedDocument["import_type"] == 1) ? json_decode($item["source_license_uri_object"]): (!empty($item["license_id"]) ? $this->helperToSearchAndReturnLicenseUri($item["license_id"]) : []);
            if($item["import_type"] == 2)
            {
                if(!empty($licenseUri))
                {
                    $licenseUri = json_decode($item["source_license_uri_object"], true);
                    if($is_html == 1)
                    {
                        if(!empty($item["license_id"]))
                        {
                            $licenseUri = $this->helperToSearchAndReturnLicenseUri($item["license_id"]);
                        }
                    }
                    else
                    {
                        $licenseUri = $this->helperToSearchAndReturnLicenseUri($item["license_id"]);
                    }

                }
            }
				// To resolve object to array error
                if(!empty($licenseUri) && !is_array($licenseUri)){
                    $licenseUri = json_decode(json_encode($licenseUri), true);
                }
                if(isset($licenseUri['description']))
                {
                    unset($licenseUri['description']);
                }
                if(isset($licenseUri['licenseText']))
                {
                    unset($licenseUri['licenseText']);
                }
               
                $licensestr = isset($licenseUri['uri'])?$licenseUri['uri']:"";
                if(substr($licensestr,0,8) == "/api/v1/")
                {
                    $licenseUri['uri'] = substr($licensestr,8);
                }
            $statusStartDate    =   $this->isMetadataActive("status_start_date") ?
                (!empty($item["status_start_date"]) ? $this->formatDateTimeToDateOnly($item["status_start_date"]) : "") :
                "";
            $statusEndDate      =   $this->isMetadataActive("status_end_date") ?
                (!empty($item["status_end_date"]) ? $this->formatDateTimeToDateOnly($item["status_end_date"]) : "") :
                "";
            $lastChangeDateTime =   !empty($item["updated_at"]) ? $this->formatDateTimeToISO8601($item["updated_at"]) : $this->formatDateTimeToISO8601($item["created_at"]);     
            $caseItemsDetails = [
                "identifier" => $identifier,
                "CFDocumentURI" => $CFDocumentURI,
                "fullStatement" => $fullStatement,
                "alternativeLabel" => $alternativeLabel,
                "uri" => $uri,
                "humanCodingScheme" => $humanCodingScheme,
                "listEnumeration" => $listEnumeration,
                "abbreviatedStatement" => $abbreviatedStatement,
                "conceptKeywords" => $conceptKeywords,
                "conceptKeywordsURI" => !empty($conceptKeywordsURI) ? $conceptKeywordsURI : [],
                "notes" => $notes,
                "language" => $language,
                "CFItemType" => $CFItemType,
                "educationLevel" => $educationLevel,
                "CFItemTypeURI" => !empty($CFItemTypeUri) ? $CFItemTypeUri : [],
                "licenseURI" => !empty($licenseUri) ? $licenseUri : [],
                "statusStartDate" => $statusStartDate,
                "statusEndDate" => $statusEndDate,
                "lastChangeDateTime" => $lastChangeDateTime
            ];
        if($abbreviatedStatement == "")
        {
            unset($caseItemsDetails['abbreviatedStatement']);
        }
        if($alternativeLabel == "")
        {
            unset($caseItemsDetails['alternativeLabel']);
        }
       if($CFItemType == "")
        {
            unset($caseItemsDetails['CFItemType']);
        }
        if(empty($CFItemTypeUri))
        {
            unset($caseItemsDetails['CFItemTypeURI']);
        }
        if($conceptKeywords == "")
        {
            unset($caseItemsDetails['conceptKeywords']);
        }
        if(empty($conceptKeywordsURI))
        {
            unset($caseItemsDetails['conceptKeywordsURI']);
        }
        if($educationLevel == "")
        {
            unset($caseItemsDetails['educationLevel']);
        }
        if($humanCodingScheme == "")
        {
            unset($caseItemsDetails['humanCodingScheme']);
        }
        if($item["status_start_date"] == "0000-00-00 00:00:00" || $statusStartDate == "")
        {
            unset($caseItemsDetails['statusStartDate']);
        }
        if($item["status_end_date"] == "0000-00-00 00:00:00" || $statusEndDate == "")
        {
            unset($caseItemsDetails['statusEndDate']);
        }
        if(empty($licenseUri))
        {
            unset($caseItemsDetails['licenseURI']);
        }
        if($notes == "")
        {
            unset($caseItemsDetails['notes']);
        }
        if($language == "")
        {
            unset($caseItemsDetails['language']);
        }
        if($uri == "")
        {
            unset($caseItemsDetails['uri']);
        }
        if($listEnumeration == "")
        {
            unset($caseItemsDetails['listEnumeration']);
        }
            $parsedCaseItems[] = $caseItemsDetails;
            $parsedCaseItemsUri[$primaryIdentifier] = [
                "identifier" => $identifier,
                "uri" => $uri,
                "title" => $CFDocumentURI['title'].":".$nodeType.":".$humanCodingScheme.":".$fullStatement,
            ];
            $parsedImportTypeItem[$primaryIdentifier]=["import_type" => $item["import_type"]];
      }
    
        $this->setParsedCaseItems($parsedCaseItems);
        $this->setParsedCaseItemsUri($parsedCaseItemsUri);
        $this->setParsedImportTypeItems($parsedImportTypeItem);
        
    }

    private function parseAndSetCaseDocumentInReverseAssociations() {
        $documentNodeIdArray          =     [];
        $caseDocumentUri              =     [];
        $savedReverseAssociations     =     $this->getReverseAssociations();
        $orgCode                      =     $this->getOrgCode();
        $is_html                      = $this->is_html;
        if(count($savedReverseAssociations) > 0) {
            foreach($savedReverseAssociations as $reverseAssociation) {
                $documentNodeIdArray[]    =   $reverseAssociation->document_id;
            }

            $attributeIn            =   'document_id';
            $containedInValues      =   $documentNodeIdArray;
            $returnCollection       =   true;
            $fieldsToReturn         =   ['*'];
            $keyValuePairs          =   ['is_deleted' => 0];

            $documentNodeList =   $this->documentRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                $attributeIn,
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs
            );
            $documentNodeListarr = $documentNodeList ? $documentNodeList->toArray() : [];
            foreach($documentNodeListarr as $document) {
                $identifier         =   $document["source_document_id"];
                $primaryIdentifier  =   $document["document_id"];
                $uri                =   ($document["import_type"] == 3) ? $document["uri"] : $this->getCaseApiUri("CFDocuments", $identifier, true, $orgCode,$this->domainName);
                $title              =   $this->isMetadataActive("title") ?
                    (!empty($document["title"]) ? $document["title"] : "") :
                    "";

                $caseDocumentUri[$primaryIdentifier] = [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $title
                ];
            }
        }


        $this->setParsedCaseDocumentUriForReverseAssociation($caseDocumentUri);

    }

    private function parseAndSetCaseItemsInReverseAssociations() {
        $savedDocument              =   $this->getSavedDocument();
        $originNodeIdArray          =   [];
        $parsedCaseItemsUri         =   [];
        $savedReverseAssociations   =   $this->getReverseAssociations();
        $orgCode                    =   $this->getOrgCode();
        $is_html                    =   $this->is_html;

        if(count($savedReverseAssociations) > 0) {
            foreach($savedReverseAssociations as $reverseAssociation) {
                $originNodeIdArray[]    =   $reverseAssociation->source_item_id;
            }


            $attributeIn            =   'item_id';
            $containedInValues      =   $originNodeIdArray;
            $returnCollection       =   true;
            $fieldsToReturn         =   ['*'];
            $keyValuePairs          =   ['is_deleted' => 0];
            $returnPaginatedData    =   false;
            $paginationFilters      =   ['limit' => 10, 'offset' => 0];
            $returnSortedData       =   false;
            $sortfilters            =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
            $operator               =   'AND';
            $relations              =   ['nodeType', 'document'];

            $originNodeList =   $this->itemRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                $attributeIn,
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $returnPaginatedData,
                $paginationFilters,
                $returnSortedData,
                $sortfilters,
                $operator,
                $relations
            );
            $originNodeListarr = $originNodeList ? $originNodeList->toArray() : [];
            foreach($originNodeList as $item) {
                $identifier         =   $item["source_item_id"];
                $primaryIdentifier  =   $item["item_id"];
                $nodeType           =   $item["nodeType"]["title"];
                $taxonomyName       =   $item["document"]["title"];
                $fullStatement      =   $this->isMetadataActive('full_statement') ? (!empty($item["full_statement"]) ? $item["full_statement"] : "") : "";
                $uri                =   ($item["import_type"] == 3) ?  $this->getCaseApiUri("CFItems", $identifier, true, $orgCode,$this->domainName) : $item["uri"];
                $humanCodingScheme  =   $this->isMetadataActive('human_coding_scheme') ?
                    (!empty($item["human_coding_scheme"]) ? $item["human_coding_scheme"] : "") :
                    "";
                $listEnumeration    =   $this->isMetadataActive('list_enumeration') ?
                    (!empty($item["list_enumeration"]) ? $item["list_enumeration"] : "") :
                    "";

                $parsedCaseItemsUri[$primaryIdentifier] = [
                    "identifier" => $identifier,
                    "uri" => $uri,
                    "title" => $taxonomyName.":".$nodeType.":".$humanCodingScheme.":".$fullStatement,
                ];
            }
        }
        $this->setParsedCaseItemsUriForReverseAssociation($parsedCaseItemsUri);
    }

    private function parseAndSetCaseAssociations() {
        $savedAssociations  =   $this->getSavedAssociations();
        $savedDocument      =   $this->getSavedDocument();
        $orgCode            =   $this->getOrgCode();
        $savedImportType    =   $this->getParsedImportTypeItems();
        $is_html            =   $this->is_html;
        $parsedCaseAssociations = [];
        foreach($savedAssociations as $association) {
            $identifier         =   $association->source_item_association_id;
            $sequenceNumber     =   $association->sequence_number;
            $associationType    =   $this->returnAssociationType($association->association_type);
            $itemImportType     =   !empty($savedImportType[$association->source_item_id]) ? $savedImportType[$association->source_item_id] : $savedDocument["import_type"];
            $uri                =   ($itemImportType == 1) ? $association->uri : $this->getCaseApiUri("CFAssociations", $identifier, true, $orgCode,$this->domainName);
            $originNodeURI      =   $this->helperToSearchAndReturnItemUri($association->source_item_id);

            if(!empty($originNodeURI)) {
                $CFDocumentURI = $this->getParsedCaseDocumentUri();
                $destinationNodeURI = [];
                if($associationType==="exemplar") {
                    $uri        =   !empty($association->external_node_url) ? $association->external_node_url : "";
                    $title      =   !empty($association->external_node_title) ? $association->external_node_title : "";
                    // as discussed with Avalon, since exemplar association destination can be any resource,
                    // not specific to CASE standard item or document, so we create a dummy destination identifier for this
                    // which doesn't exist in our system
                    $dummyDestinationIdentifier = $this->createUniversalUniqueIdentifier();
                    $destinationNodeURI = [
                        "identifier" => $dummyDestinationIdentifier,
                        "uri" => $uri,
                        "title" => $title
                    ];
                }
                else if(!empty($association->target_item_id)) {
                    if(($association->target_item_id===$savedDocument["document_id"]))
                    {
                        $destinationNodeURI = $CFDocumentURI;
                    }
                    else
                    {
                        $destinationNodeURI = $this->fetchAnyAssociatedItemOrDocumentUri($association->target_item_id);
                    }
                    if(empty($destinationNodeURI))
                    {
                        $destinationNodeURI = [
                            "identifier" => $association->target_item_id,
                            "uri" => $association->external_node_url,
                            "title" => $association->external_node_title
                        ];
                    }
                }
                else if(empty($association->target_item_id))
                {
                    $destinationNodeURI = [
                        "identifier" => $association->target_item_id,
                        "uri" => $association->external_node_url,
                        "title" => $association->external_node_title
                    ];
                }
                $CFAssociationGroupingURI = !empty($association->association_group_id) ?
                    $this->helperToSearchAndReturnAssociationGroupUri($association->association_group_id) :
                    [];
                $lastChangeDateTime = !empty($association->updated_at) ? $this->formatDateTimeToISO8601($association->updated_at) : $this->formatDateTimeToISO8601($association->created_at);
                if($destinationNodeURI['uri'] == "")
                {
                    $destinationNodeURI['uri'] = $this->getCaseApiUri("CFDocuments", $destinationNodeURI['identifier'], true, $orgCode,$this->domainName);
                }
                $associateArr = [
                    "identifier" => $identifier,
                    "associationType" => $associationType,
                    "CFDocumentURI" => $CFDocumentURI,
                    "sequenceNumber" => $sequenceNumber,
                    "uri" => $uri,
                    "originNodeURI" => $originNodeURI,
                    "destinationNodeURI" => $destinationNodeURI,
                    "CFAssociationGroupingURI" => $CFAssociationGroupingURI,
                    "lastChangeDateTime" => $lastChangeDateTime
                ];
                if(empty($CFAssociationGroupingURI))
                {
                    unset($associateArr['CFAssociationGroupingURI']);
                }
				//Added associationPersistentId to send unique identifier for Assoications for versioning request - UF-1871
                if($this->versioning){
                    $associateArr["associationPersistentId"] = $originNodeURI["identifier"].'_'.$associationType.'_'.$destinationNodeURI["identifier"];
                }
                $parsedCaseAssociations[] = $associateArr;

            } else {
                //$CFDocumentURI = $this->getParsedCaseDocumentUriForReverseAssociation();
                $originNodeURI      =   $this->helperToSearchAndReturnItemUriForReverseAssociation($association->source_item_id);

                $CFDocumentURI      =   $this->helperToSearchAndReturnDocumentUriForReverseAssociation($association->source_document_id);
                $destinationNodeURI =   $this->fetchAnyAssociatedItemOrDocumentUri($association->target_item_id);

                $CFAssociationGroupingURI = !empty($association->association_group_id) ?
                    $this->helperToSearchAndReturnAssociationGroupUri($association->association_group_id) :
                    [];
                $lastChangeDateTime = !empty($association->updated_at) ? $this->formatDateTimeToISO8601($association->updated_at) : $this->formatDateTimeToISO8601($association->created_at);

                if(!empty($destinationNodeURI) && (!isset($destinationNodeURI['uri']) || (isset($destinationNodeURI['uri']) && $destinationNodeURI['uri'] == "")))
                {
                    $destinationNodeURI['uri'] = $this->getCaseApiUri("CFDocuments", $destinationNodeURI['identifier'], true, $orgCode,$this->domainName);
                }
                $associateArr = [
                    "identifier" => $identifier,
                    "associationType" => $associationType,
                    "CFDocumentURI" => $CFDocumentURI,
                    "sequenceNumber" => $sequenceNumber,
                    "uri" => $uri,
                    "originNodeURI" => $originNodeURI,
                    "destinationNodeURI" => $destinationNodeURI,
                    "CFAssociationGroupingURI" => $CFAssociationGroupingURI,
                    "lastChangeDateTime" => $lastChangeDateTime
                ];

                if(empty($CFAssociationGroupingURI))
                {
                    unset($associateArr['CFAssociationGroupingURI']);
                }
				//Added associationPersistentId to send unique identifier for Assoications for versioning request - UF-1871
                if($this->versioning){
                    $associateArr["associationPersistentId"] = $originNodeURI["identifier"].'_'.$associationType.'_'.$destinationNodeURI["identifier"];
                }
                $parsedCaseAssociations[] = $associateArr;
            }
        
        }
        
        $this->setParsedCaseAssociations($parsedCaseAssociations);
    }

    /*******************************Final method to prepare case package************************************/

    private function prepareAndSetCasePackageData() {
        $casePackageData = [
            "CFDocument" => $this->getParsedCaseDocument(),
            "CFItems" =>  $this->getParsedCaseItems(),
            "CFAssociations" => $this->getParsedCaseAssociations(),
            "CFDefinitions" => [
                "CFConcepts" => $this->getParsedCaseConcepts(),
                "CFSubjects" => $this->getParsedCaseSubjects(),
                "CFLicenses" => $this->getParsedCaseLicenses(),
                "CFItemTypes" => $this->getParsedCaseItemTypes(),
                "CFAssociationGroupings" => $this->getParsedCaseAssociationGroupings()
            ],            
            // "CFRubrics" => $CFRubrics
        ];
        if($this->versioning==true){
            $this->versionInfo['dateTime'] = date('Y-m-d H:i:s');
            $casePackageData["ACMTVersionInfo"] = $this->versionInfo;
        }
        $this->setCasePackage($casePackageData);
    }

    /**Method to parse the data for csv export with custom metadata */
    private function prepareAndSetCSVPackageData(){
        $itemIdArray        =   [];

        $parsedCaseItems    =   [];
        $parsedCaseDocument =   [];

        $getDocDetailToExport   =   [];
        $getItemDetailToExport  =   [];

        $parsedItemRelations    =   [];
        $parsedItemAssociations =   [];

        $destinationNodeIdSet   =   [];

        $association_type   =   ['3','5','7'];
        $is_html            =   $this->is_html;

        $organizationId     =   $this->getRequestingUserOrganizationId();
        $documentIdentifier =   $this->getDocumentIdentifier();
        $savedDocument      =   $this->getSavedDocument();
        $savedItems         =   $this->getSavedItems();
        //$CFDocumentURI      =   $this->getParsedCaseDocumentUri();
        $orgCode            =   $this->getOrgCode();


        $identifier     =   $savedDocument["source_document_id"];
        $nodeType       =   $savedDocument["node_type"]["title"];
        $creator        =   !empty($savedDocument["creator"]) ? $savedDocument["creator"] : "";
        $title          =   $this->isMetadataActive("title") ? (!empty($savedDocument["title"]) ? $savedDocument["title"] : "") : "";
        if($is_html == 1)
        {
            $title = $savedDocument["title_html"];
        }

        $officialSourceURL  =   $this->isMetadataActive("official_source_url") ?
            (!empty($savedDocument["official_source_url"]) ? $savedDocument["official_source_url"] : "") :
            "";
        $publisher          =   $this->isMetadataActive("publisher") ?
            (!empty($savedDocument["publisher"]) ? $savedDocument["publisher"] : "") :
            "";
        if($is_html == 1)
        {
            $publisher = $savedDocument["publisher_html"];
        }

        $description        =   $this->isMetadataActive("description") ?
            (!empty($savedDocument["description"]) ? $savedDocument["description"] : "") :
            "";
        if($is_html == 1)
        {
            $description = $savedDocument["description_html"];
        }

        $subject        =   $this->getParsedCaseSubjects();
        $subjectsURI    =   $this->getParsedCaseSubjectsUri();
        $language       =   $this->isMetadataActive("language") ?
            (!empty($savedDocument["language_id"]) ? $this->searchForAndReturnLanguageShortCode($savedDocument["language_id"]) : "") :
            "";
        $version            =   $this->isMetadataActive("version") ? (!empty($savedDocument["version"]) ? $savedDocument["version"] : "") : "";

        $adoptionStatus     =   !empty($savedDocument["adoption_status"]) ?
            $this->getSystemSpecifiedAdoptionStatusText($savedDocument["adoption_status"]) :
            "Draft";
        $statusStartDate    =   $this->isMetadataActive("status_end_date") ?
            (!empty($savedDocument["status_start_date"]) ? (($savedDocument["status_start_date"] == '0000-00-00 00:00:00') ? '0000-00-00 00:00:00' : $this->formatDateTimeToDateOnly($savedDocument["status_start_date"])) : "") :
            "";
        $statusEndDate      =   $this->isMetadataActive("status_end_date") ?
            (!empty($savedDocument["status_end_date"]) ? (($savedDocument["status_end_date"] == '0000-00-00 00:00:00') ? '0000-00-00 00:00:00' : $this->formatDateTimeToDateOnly($savedDocument["status_end_date"])) : "") :
            "";
        $licenseUri         =   ($savedDocument["import_type"] == 1) ? json_decode($savedDocument["source_license_uri_object"]): (!empty($savedDocument["license_id"]) ? $this->helperToSearchAndReturnLicenseUri($savedDocument["license_id"]) : []);
        if($savedDocument["import_type"] == 2)
        {
            if(!empty($licenseUri))
            {
                $licenseUri = $this->helperToSearchAndReturnLicenseUri($savedDocument["license_id"]);
                if($is_html ==1){
                    $licenseUri = $this->helperToSearchAndReturnLicenseUri($savedDocument["license_id"]);
                }
            }
            else
            {
                $licenseUri = json_decode($savedDocument["source_license_uri_object"], true);
            }
        }
		// To resolve object to array error
        if(!empty($licenseUri) && !is_array($licenseUri)){
            $licenseUri = json_decode(json_encode($licenseUri), true);
        }
        $notes = $this->isMetadataActive("notes") ? (!empty($savedDocument["notes"]) ? $savedDocument["notes"] : "") : "";
        if($is_html == 1)
        {
            $notes = $savedDocument["notes_html"];
        }
        
        $parsedCaseDocument[] = [
            "identifier" => $identifier,
            "id"        =>  $savedDocument["document_id"],
            "creator" => $creator,
            "title" => $title,
            "officialSourceURL" => $officialSourceURL,
            "publisher" => $publisher,
            "description" => $description,
            "subject_title" => isset($subject[0]['title']) ? $subject[0]['title'] : '',
            "subject_hierarchy_code" => isset($subject[0]['hierarchyCode']) ? $subject[0]['hierarchyCode'] : '',
            "subject_description"   => isset($subject[0]['description']) ? $subject[0]['description'] : '',
            "licenseTitle"          =>  isset($licenseUri['title']) ? $licenseUri['title'] : '',
            "licenseText"           => isset($licenseUri['licenseText']) ? $licenseUri['licenseText'] : '',
            "licenseDescription"    => isset($licenseUri['description']) ? $licenseUri['description'] : '',
            "language" => !empty($language) ? $language : "",
            "statusStartDate" => ($statusStartDate!=='0000-00-00 00:00:00')?$statusStartDate:'',
            "statusEndDate" => ($statusEndDate!=='0000-00-00 00:00:00')?$statusEndDate:'',

            "notes" => $notes,
        ];
        $doc=0;
        foreach($parsedCaseDocument as $getDocDetailK=>$getDocDetailV)
        {
            $getDocDetailToExport[$doc]['id'] = $getDocDetailV['identifier'];
            $getDocDetailToExport[$doc]['document_id'] = $getDocDetailV['id'];
            $getDocDetailToExport[$doc]['parent_id'] = "";
            $getDocDetailToExport[$doc]['is_document'] = 1;

            $getDocDetailToExport[$doc]['case_metadata'] = [['title' => $getDocDetailV['title'], 'full_statement' => "", 'human_coding_scheme' => "", 'list_enumeration' => "",'node_type' => "Document", 'education_level' => "", 'creator' => $getDocDetailV['creator'], 'official_source_url' => $getDocDetailV['officialSourceURL'], 'publisher' => $getDocDetailV['publisher'], 'description' => $getDocDetailV['description'], 'subject_title' => $getDocDetailV['subject_title'], 'subject_hierarchy_code' => $getDocDetailV['subject_hierarchy_code'], 'subject_description' => $getDocDetailV['subject_description'], 'license_title' => $getDocDetailV['licenseTitle'], 'license_text' => $getDocDetailV['licenseText'], 'license_description' => $getDocDetailV['licenseDescription'], 'language' => $getDocDetailV['language'], 'status_start_date' => $getDocDetailV['statusStartDate'], 'status_end_date' => $getDocDetailV['statusEndDate'], 'alternative_label' => "", 'abbreviated_statement' => "", 'concept_title' => "", 'concept_keywords' => "", 'concept_hierarchy_code' => "", 'concept_description' => "", 'notes' => !empty($getDocDetailV['notes']) ? $getDocDetailV['notes']:"",]];


            $getDocDetailToExport[$doc]['metadata_detail'] = array();

            $doc = $doc+1;
        }

        $parsedAssociations             = $this->getParsedCaseAssociations();
        $parsedAssociationsReverseCase  = $this->getParsedAssociationsReverseCase();

        foreach($parsedAssociations as $associations) {
            if($associations['associationType'] == 'isChildOf'){
                if(isset($associations['originNodeURI']['identifier']))
                {
                    $destinationNodeIdSet[$associations['originNodeURI']['identifier']] = $associations['destinationNodeURI']['identifier'];
                }    
            }
        }

        foreach($savedItems as $item) {

            $identifier         =   $item["source_item_id"];
            $primaryIdentifier  =   $item["item_id"];
            $itemIdArray[]      =   $identifier;
            $parentIdentifier   =   $destinationNodeIdSet[$identifier];
            $nodeType           =   $item["node_type"]["title"];
            $fullStatement      =   $this->isMetadataActive('full_statement') ? (!empty($item["full_statement"]) ? $item["full_statement"] : "") : "";
            if($is_html == 1)
            {
                $fullStatement = $item["full_statement_html"];
            }
            $alternativeLabel   =   $this->isMetadataActive('alternative_label') ?
                (!empty($item["alternative_label"]) ? $item["alternative_label"] : "") :
                "";
            if($is_html == 1)
            {
                $alternativeLabel = $item["alternative_label_html"];
            }

            $humanCodingScheme      =   $this->isMetadataActive('human_coding_scheme') ?
                (!empty($item["human_coding_scheme"]) ? $item["human_coding_scheme"] : "") :
                "";
            if($is_html == 1)
            {
                $humanCodingScheme = $item["human_coding_scheme_html"];
            }
           
            $listEnumeration        =   $this->isMetadataActive('list_enumeration') ?
                (!empty($item["list_enumeration"]) ? $item["list_enumeration"] : "") :
                "";
            $abbreviatedStatement   =    $this->isMetadataActive('abbreviated_statement') ?
                (!empty($item["abbreviated_statement"]) ? $item["abbreviated_statement"] : "") :
                "";
            if($is_html == 1)
            {
                $abbreviatedStatement = $item["abbreviated_statement_html"];
            }

            $conceptKeywords        =   !empty($item["concept"]["concept_id"]) ? $this->helperToSearchAndReturnConceptKeyword($item["concept"]["concept_id"]) : [ "" ];

            $conceptURI     =   ($savedDocument["import_type"] == 1) ? json_decode($item["source_concept_uri_object"]): (!empty($item["concept_id"]) ? $this->helperToSearchAndReturnConceptUri($item["concept_id"]) : []);
            if($item["import_type"] == 2)
            {
                if(!empty($conceptURI))
                {
                    $conceptURI = json_decode($item["source_concept_uri_object"], true);
                    if($is_html == 1)
                    {
                        if(!empty($item["concept_id"]))
                        {
                            $conceptURI = $this->helperToSearchAndReturnConceptUri($item["concept_id"]);
                        }

                    }
                    else
                    {
                        $conceptURI = $this->helperToSearchAndReturnConceptUri($item["concept_id"]);
                    }
                }
            }
			// To resolve object to array error
            if(!empty($conceptURI) && !is_array($conceptURI)){
                $conceptURI = json_decode(json_encode($conceptURI), true);
            }

            $licenseUri     =   ($savedDocument["import_type"] == 1) ? json_decode($item["source_license_uri_object"]): (!empty($item["license_id"]) ? $this->helperToSearchAndReturnLicenseUri($item["license_id"]) : []);
            if($item["import_type"] == 2)
            {
                if(!empty($licenseUri))
                {
                    $licenseUri = json_decode($item["source_license_uri_object"], true);
                    if($is_html == 1)
                    {
                        if(!empty($item["license_id"]))
                        {
                            $licenseUri = $this->helperToSearchAndReturnLicenseUri($item["license_id"]);
                        }
                    }
                    else
                    {
                        $licenseUri = $this->helperToSearchAndReturnLicenseUri($item["license_id"]);
                    }

                }
            }
            // To resolve object to array error
            if(!empty($licenseUri) && !is_array($licenseUri)){
                $licenseUri = json_decode(json_encode($licenseUri), true);
            }

            $notes                  =   $this->isMetadataActive("notes") ? (!empty($item["notes"]) ? $item["notes"] : "") : "";
            if($is_html == 1)
            {
                $notes = $item["notes_html"];
            }
            $language               =   $this->isMetadataActive("language") ?
                (!empty($item["language_id"]) ? $this->searchForAndReturnLanguageShortCode($item["language_id"]) : "") :
                "";
            $educationLevel         =   $this->isMetadataActive("education_level") ?
                (!empty($item["education_level"]) ? $item["education_level"] : "") :
                "";

            $statusStartDate    =   $this->isMetadataActive("status_start_date") ?
                (!empty($item["status_start_date"]) ? $item["status_start_date"] : "") :
                "";
            $statusEndDate      =   $this->isMetadataActive("status_end_date") ?
                (!empty($item["status_end_date"]) ? $item["status_end_date"] : "") :
                "";
            $parsedCaseItems[] = [
                "source_item_id" => $identifier,
                "item_id"        => $primaryIdentifier,
                "parent_id"  => $parentIdentifier,
                "full_statement" => $fullStatement,
                "alternative_label" => $alternativeLabel,
                "human_coding_scheme" => $humanCodingScheme,
                "list_enumeration" => $listEnumeration,
                "abbreviated_statement" => $abbreviatedStatement,
                "conceptTitle"      =>  isset($conceptURI['title']) ? $conceptURI['title'] : '',
                "conceptKeywords" => isset($conceptURI['keywords']) ? $conceptURI['keywords'] : '',
                "conceptHierarchyCode" => isset($conceptURI['hierarchyCode']) ? $conceptURI['hierarchyCode'] : '',
                "conceptDescription"    => isset($conceptURI['description']) ? $conceptURI['description'] : '',
                "licenseTitle"      =>  isset($licenseUri['title']) ? $licenseUri['title'] : '',
                "licenseText" => isset($licenseUri['licenseText']) ? $licenseUri['licenseText'] : '',
                "licenseDescription"    => isset($licenseUri['description']) ? $licenseUri['description'] : '',
                "notes" => $notes,
                "language" => $language,
                "nodeType" => $nodeType,
                "education_level" => $educationLevel,
                "statusStartDate" => ($statusStartDate!=='0000-00-00 00:00:00' && $statusStartDate!=='')?date('Y-m-d',strtotime($statusStartDate)):'',
                "statusEndDate" => ($statusEndDate!=='0000-00-00 00:00:00' && $statusEndDate!=='')?date('Y-m-d',strtotime($statusEndDate)):'',

            ];
        }
        $i=0;
        
        foreach($parsedCaseItems as $getItemDetailK=>$getItemDetailV)
        {
            //$getItemDetailToExport[$i]['id'] = $getItemDetailV['source_item_id'];
            $getItemDetailToExport[$i]['id'] = $getItemDetailV['source_item_id'];
            $getItemDetailToExport[$i]['item_id'] = $getItemDetailV['item_id'];
            //$getItemDetailToExport[$i]['node_id'] = $getItemDetailV['source_item_id'];
            $getItemDetailToExport[$i]['parent_id'] = $getItemDetailV['parent_id'];
            $getItemDetailToExport[$i]['is_document'] = 0;

            $getItemDetailToExport[$i]['case_metadata'] =   [['title' => "", 'full_statement' => $getItemDetailV['full_statement'], 'human_coding_scheme' => $getItemDetailV['human_coding_scheme'], 'list_enumeration' => $getItemDetailV['list_enumeration'], 'node_type' =>  $getItemDetailV['nodeType'], 'education_level' => !empty($getItemDetailV['education_level']) ? $getItemDetailV['education_level']:"", 'alternative_label' => !empty($getItemDetailV['alternative_label']) ? $getItemDetailV['alternative_label']:"", 'abbreviated_statement'  => !empty($getItemDetailV['abbreviated_statement']) ? $getItemDetailV['abbreviated_statement']:"",'concept_title'  => !empty($getItemDetailV['conceptTitle']) ? $getItemDetailV['conceptTitle']:"", 'concept_keywords' => !empty($getItemDetailV['conceptKeywords']) ? $getItemDetailV['conceptKeywords']:"", 'concept_hierarchy_code' => !empty($getItemDetailV['conceptHierarchyCode']) ? $getItemDetailV['conceptHierarchyCode']:"", 'concept_description' => !empty($getItemDetailV['conceptDescription']) ? $getItemDetailV['conceptDescription']:"", 'license_title' => !empty($getItemDetailV['licenseTitle']) ? $getItemDetailV['licenseTitle']:"", 'license_text' => !empty($getItemDetailV['licenseText']) ? $getItemDetailV['licenseText']:"", 'license_description' => !empty($getItemDetailV['licenseDescription']) ? $getItemDetailV['licenseDescription']:"", 'notes' => !empty($getItemDetailV['notes']) ? $getItemDetailV['notes']:"", 'status_start_date' => !empty($getItemDetailV['statusStartDate']) ? $getItemDetailV['statusStartDate']:"", 'status_end_date' => !empty($getItemDetailV['statusEndDate']) ? $getItemDetailV['statusEndDate']:"", 'creator' => "", 'official_source_url' => "", 'publisher' => "", 'description' => "", 'subject_title' => '', 'subject_hierarchy_code' => '', 'subject_description' => '', 'language' => !empty($getItemDetailV['language']) ? $getItemDetailV['language']: ""]];

            //$getItemDetailToExport[$i] = ['full_statement' => $getItemDetailV['full_statement'], 'human_coding_scheme' => $getItemDetailV['human_coding_scheme'], 'list_enumeration' => $getItemDetailV['list_enumeration']];

            $getItemDetailToExport[$i]['metadata_detail'] = array();
            $i = $i+1;
        }

        $exportTaxonomyData = $this->documentRepository->exportTaxonomy($documentIdentifier, $getItemDetailToExport, $getDocDetailToExport);

        // get tree hierarchy child parent relation start

        $getTreeHierarchyChildRelationData = DB::table('items')
            ->select('source_item_id','parent_id')
            ->where('document_id',$documentIdentifier)
            ->get()
            ->toArray();

        $mappingItemIdToParentId = array();
        foreach($getTreeHierarchyChildRelationData as $getTreeHierarchyChildRelationDataK=>$getTreeHierarchyChildRelationDataV)
        {
            array_push($mappingItemIdToParentId,$getTreeHierarchyChildRelationDataV->source_item_id);
        }
        array_push($mappingItemIdToParentId,$savedDocument["source_document_id"]);
        // get tree hierarchy child parent relation end

        // To get parent child relation for Taxonomy
        $arrayToStoreOnlyOneChildRelation = array();
       // dump($parsedAssociations);
        foreach($parsedAssociations as $itemAssociation)
        {
            //If the association_type = isChildOf (the basic association)

            if($itemAssociation['associationType'] == 'isChildOf') {
                $originNodeId = isset($itemAssociation['originNodeURI']['identifier'])?$itemAssociation['originNodeURI']['identifier']:'';
                $destinationNodeId = isset($itemAssociation['destinationNodeURI']['identifier']) ? $itemAssociation['destinationNodeURI']['identifier'] : '';

                if(!empty($originNodeId) && !empty($destinationNodeId) && ($originNodeId != $destinationNodeId) && !in_array($originNodeId,$arrayToStoreOnlyOneChildRelation) && in_array($destinationNodeId,$mappingItemIdToParentId) )
                {

                    $parsedItemRelations[] = [
                        "child_id"          =>  $originNodeId,
                        "parent_id"         =>  $destinationNodeId,
                        "association_type"  =>  $itemAssociation['associationType'],
                        "tree_association" => 1,
                        "external_association" => 0,
                        "sequence_number" => $itemAssociation['sequenceNumber']
                    ];
                    array_push($arrayToStoreOnlyOneChildRelation,$originNodeId);
                }
                else
                {
                    if(in_array($destinationNodeId,$mappingItemIdToParentId))
                    {
                        $parsedItemAssociations[] = [
                            "child_id"          =>  $originNodeId,
                            "parent_id"         =>  $destinationNodeId,
                            "association_type"  =>  $itemAssociation['associationType'],
                            "tree_association" => 0,
                            "external_association" => 0,
                            "sequence_number" => $itemAssociation['sequenceNumber']
                        ];
                    }
                    else
                    {
                        $parsedItemAssociations[] = [
                            "child_id"          =>  $originNodeId,
                            "parent_id"         =>  $destinationNodeId,
                            "association_type"  =>  $itemAssociation['associationType'],
                            "tree_association" => 0,
                            "external_association" => 1,
                            "sequence_number" => $itemAssociation['sequenceNumber']
                        ];
                    }

                }
            }
            else
            {
                //To include associations other than isChildOf
                if(isset($itemAssociation['originNodeURI']['identifier']) && in_array($itemAssociation['originNodeURI']['identifier'], $itemIdArray)) { // Checking if the origin node belongs to the taxonomy to be exported
                    if(isset($itemAssociation['destinationNodeURI']['identifier']) && in_array($itemAssociation['destinationNodeURI']['identifier'], $itemIdArray)) { // Checking if any association with same taxonomy
                        if($parsedAssociationsReverseCase[$itemAssociation['identifier']] != 1) { // Excluding the reverse association of same taxonomy
                            $originNodeId = $itemAssociation['originNodeURI']['identifier'];
                            $destinationNodeId = isset($itemAssociation['destinationNodeURI']['identifier']) ? $itemAssociation['destinationNodeURI']['identifier'] : '';

                            if(!empty($originNodeId) && !empty($destinationNodeId))
                            { // Validating empty origin node and destination node
                                $parsedItemAssociations[] = [
                                    "child_id"          =>  $originNodeId,
                                    "parent_id"         =>  $destinationNodeId,
                                    "association_type"  =>  $itemAssociation['associationType'],
                                    "tree_association" => 0,
                                    "external_association" => 0,
                                    "sequence_number" => $itemAssociation['sequenceNumber']
                                ];
                            }
                        }
                    } else { // Checking associations with different taxonomy
                        $originNodeId = $itemAssociation['originNodeURI']['identifier'];
                        $destinationNodeId = isset($itemAssociation['destinationNodeURI']['identifier']) ? $itemAssociation['destinationNodeURI']['identifier'] : '';

                        if(!empty($originNodeId) && !empty($destinationNodeId))
                        { // Validating empty origin node and destination node
                            $parsedItemAssociations[] = [
                                "child_id"          =>  $originNodeId,
                                "parent_id"         =>  $destinationNodeId,
                                "association_type"  =>  $itemAssociation['associationType'],
                                "tree_association" => 0,
                                "external_association" => 1,
                                "sequence_number" => $itemAssociation['sequenceNumber']
                            ];
                        }
                    }
                }
            }
        }
       
        $parsedItemRelationsAssociations = array();
        $parsedItemRelationsAssociations = array_merge($parsedItemRelations,$parsedItemAssociations);
        //dump($parsedItemRelations,$parsedItemAssociations);

        #items in current taxonomy
        $itemsInTree[] = $savedDocument["source_document_id"];
        foreach($savedItems as $item) {
            $itemsInTree[] = $item["source_item_id"];
        }

        $new_array = [];
        foreach($parsedItemRelationsAssociations as $relations) {
            if($relations['association_type'] == 'isChildOf' && $relations['external_association'] == 0 && $relations['tree_association'] == 1) {
                $new_array[] = $relations;
            }
        }

        foreach($new_array as $value) {
            $chnode = $value['child_id'];
            $association_type = $value['association_type'];
            $external_association = $value['external_association'];
            $match_array = array_filter(array_map(function($n) use(&$chnode, &$association_type, &$external_association, &$itemsInTree) {
                # in_array condition to ensure that tree_association (main association) is with node in current taxonomy
                if($n['child_id'] == $chnode && $n['association_type'] == $association_type && $external_association == 0 && in_array($n['parent_id'],$itemsInTree)) {
                    return $n;
                }
            }, $parsedItemRelationsAssociations));

            if(count($match_array) > 1)
            {
                $minval = min(array_column($match_array, 'sequence_number'));
                foreach($match_array as $key => $val)
                {
                    if($val['sequence_number'] == $minval) {
                        $parsedItemRelationsAssociations[$key]['tree_association'] = 1;
                    } else {
                        $parsedItemRelationsAssociations[$key]['tree_association'] = 0;
                    }
                }
            }
        }
        $treeAssociationArr = [];
        $otherAssociationArr = [];
        foreach($parsedItemRelationsAssociations as $relations) {
            if($relations['tree_association'] == 1) {
                $treeAssociationArr[] = $relations;
            } else {
                $otherAssociationArr[] = $relations;
            }
        }
        $parsedItemRelationsAssociations = array_merge($treeAssociationArr, $otherAssociationArr);

        $itemSequenceList=[];
        
        foreach($exportTaxonomyData as $nodesDataK => $nodesDataV)
        {
            $itemK = $nodesDataV['id'];
            $exportTaxonomyData[$nodesDataK]['full_statement'] = !empty($nodesDataV['case_metadata'][0]['full_statement']) ? $nodesDataV['case_metadata'][0]['full_statement'] : "";
            $exportTaxonomyData[$nodesDataK]['human_coding_scheme'] = !empty($nodesDataV['case_metadata'][0]['human_coding_scheme']) ? $nodesDataV['case_metadata'][0]['human_coding_scheme'] : "";
            $exportTaxonomyData[$nodesDataK]['list_enumeration'] = !empty($nodesDataV['case_metadata'][0]['list_enumeration']) ? $nodesDataV['case_metadata'][0]['list_enumeration'] : "";
            $exportTaxonomyData[$nodesDataK]['sequence_number'] = !empty($itemSequenceList[$itemK]) ? $itemSequenceList[$itemK] : "";
        }

       

        $taxonomyData = [
            "nodes" => $exportTaxonomyData,
            "relations" => $parsedItemRelationsAssociations
        ];

        $this->setCasePackage($taxonomyData);
    }

    /*************************************Helper methods*******************************************/

    private function helperToFetchDataBasedOnProvidedType(string $type): Collection {
        $fieldsToReturn = ['*'];
        $dataToReturn = collect([]);
        switch ($type) {
            case 'concept':
                $idsCsv = $this->getConceptIdsCsv();
                $attributeIn = "concept_id";
                $repository = $this->conceptRepository;
                break;
            case 'license':
                $idsCsv = $this->getLicenseIdsCsv();
                $attributeIn = "license_id";
                $repository = $this->licenseRepository;
                break;
            case 'itemType':
                $idsCsv = $this->getItemTypeIdsCsv();
                $attributeIn = "node_type_id";
                $repository = $this->nodeTypeRepository;
                break;
            case 'associationGroup':
                $idsCsv = $this->getAssociationGroupIdsCsv();
                $attributeIn = "association_group_id";
                $repository = $this->associationGroupRepository;
                break;
            case 'language':
                $idsCsv = $this->getLanguageIdsCsv();
                $attributeIn = "language_id";
                $repository = $this->languageRepository;
                break;
            default:
                $repository = false;
                break;
        }
        if($repository!==false) {
            $ids = $idsCsv!=="" ? array_unique($this->createArrayFromCommaeSeparatedString($idsCsv)) : [];
            $organizationId = $this->getRequestingUserOrganizationId();
            $returnCollection = true;
            $fieldsToReturn = ['*'];
            $dataFetchingConditionalClause = [ "organization_id" => $organizationId, 'is_deleted' => 0 ];
            $dataToReturn =   $repository->findByAttributesWithSpecifiedFieldsWithInQuery(
                $attributeIn,
                $ids,
                $returnCollection,
                $fieldsToReturn,
                $dataFetchingConditionalClause
            );
        }
        return $dataToReturn;
    }

    private function searchForAndReturnLanguageShortCode(string $identifierToSearchFor): string {
        $languageColl = $this->getSavedLanguages();

        $languageCollection = json_decode(json_encode($languageColl), true);
          
        $searchResultkey =   array_search($identifierToSearchFor, array_column($languageCollection, 'language_id'));  
         
        return ($searchResultkey !== false ) ? $languageCollection[$searchResultkey]["name"] : "";
    }

    private function isMetadataActive(string $internalNameToSearchFor): bool {
        $metadataCollection = $this->getTenantMetadata();
       
        return in_array( $internalNameToSearchFor, array_filter(array_column($metadataCollection, "internal_name")));
       // return $metadataCollection->where("internal_name", $internalNameToSearchFor)->isNotEmpty();
    }

    private function helperToSearchAndReturnLicenseUri(string $licenseIdentifier): array {
        $parsedCaseLicensesUri = $this->getParsedCaseLicensesUri();
        return !empty($parsedCaseLicensesUri[$licenseIdentifier]) ? $parsedCaseLicensesUri[$licenseIdentifier] : [];
    }

    private function helperToSearchAndReturnConceptUri(string $conceptIdentifier): array {
        $parsedCaseConceptsUri = $this->getParsedCaseConceptsUri();
        return !empty($parsedCaseConceptsUri[$conceptIdentifier]) ? $parsedCaseConceptsUri[$conceptIdentifier] : [];
    }

    private function helperToSearchAndReturnConceptKeyword(string $conceptIdentifier){
        $parsedCaseConceptsKeywords = $this->getParsedCaseConceptsKeyword();
        return !empty($parsedCaseConceptsKeywords[$conceptIdentifier]) ? $parsedCaseConceptsKeywords[$conceptIdentifier] : "";
    }

    private function helperToSearchAndReturnItemTypeUri(string $itemTypeIdentifier): array {
        $parsedCaseItemTypesUri = $this->getParsedCaseItemTypesUri();
        return !empty($parsedCaseItemTypesUri[$itemTypeIdentifier]) ? $parsedCaseItemTypesUri[$itemTypeIdentifier] : [];
    }

    private function helperToSearchAndReturnAssociationGroupUri(string $associationGroupIdentifier): array {
        $parsedCaseAssociationGroupsUri = $this->getParsedCaseAssociationGroupingsUri();
        return !empty($parsedCaseAssociationGroupsUri[$associationGroupIdentifier]) ?
            $parsedCaseAssociationGroupsUri[$associationGroupIdentifier] :
            [];
    }

    private function helperToSearchAndReturnItemUri(string $itemIdentifier): array {
        $parsedCaseItems = $this->getParsedCaseItemsUri();
        return !empty($parsedCaseItems[$itemIdentifier]) ? $parsedCaseItems[$itemIdentifier] : [];
    }

    private function helperToSearchAndReturnDocumentUriForReverseAssociation(string $documentIdentifier): array {
        $parsedCaseDocumentsForReverseAssociation = $this->getParsedCaseDocumentUriForReverseAssociation();
        return !empty($parsedCaseDocumentsForReverseAssociation[$documentIdentifier]) ? $parsedCaseDocumentsForReverseAssociation[$documentIdentifier] : [];
    }

    private function helperToSearchAndReturnItemUriForReverseAssociation(string $itemIdentifier): array {
        $parsedCaseItems = $this->getParsedCaseItemsUriForReverseAssociation();
        return !empty($parsedCaseItems[$itemIdentifier]) ? $parsedCaseItems[$itemIdentifier] : [];
    }

    private function helperToCreateAndReturnUriOfProvidedType($data, string $type): array {
        $identifier = $type==="document" ? $data["source_document_id"] : $data["source_item_id"];
        $orgCode    = $this->getOrgCode();
        $nodeType       =   isset($data["nodeType"]["title"]) ? $data["nodeType"]["title"] : '';

        $fullStatement  =   !empty($data["full_statement"]) ? $data["full_statement"] : '';
        $caseUriTypeText = $type==="document" ? "CFDocuments" : "CFItems";
        $uri = $this->getCaseApiUri($caseUriTypeText, $identifier, true, $orgCode,$this->domainName);

        if( $type==="document") {
            $taxonomyName   =   $data["title"];
            $title = $this->isMetadataActive("title") ? (!empty($data["title"]) ? $data["title"] : "") : "";
        }
        else {
            $taxonomyName   =   isset($data["document"]["title"]) ? $data["document"]["title"] : '';
            $humanCodingScheme = $this->isMetadataActive('human_coding_scheme') ?
                (!empty($data["human_coding_scheme"]) ? $data["human_coding_scheme"] : "") :
                "";
            $listEnumeration = $this->isMetadataActive('list_enumeration') ?
                (!empty($data["list_enumeration"]) ? $data["list_enumeration"] : "") :
                "";
            $title = $humanCodingScheme;
        }

        $parsedUri = [
            "identifier" => $identifier,
            "uri" => $uri,
            "title" => $taxonomyName.":".$nodeType.":".$title.":".$fullStatement
        ];

        return $parsedUri;
    }

    private function fetchAnyAssociatedItemOrDocumentUri(string $identifier): array {
        $uriToReturn = [];
        $itemUriAssociatedToRequestedTaxonomy = $this->helperToSearchAndReturnItemUri($identifier);
        if(empty($itemUriAssociatedToRequestedTaxonomy)) {
            // search the temporary storage array for document i.e. already queried
            $documentAlreadyQueried = $this->helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb($identifier, "document");
            $anyDocument = !empty($documentAlreadyQueried) ? $documentAlreadyQueried : $this->documentRepository->find($identifier);
            $this->helperToSetDocumentAndItemAlreadyQueriedFromDb($anyDocument, $identifier, "document");

            if(empty($anyDocument)) {
                // $anyItem = $this->itemRepository->find($identifier);

                // search the temporary storage array for item i.e. already queried
                $itemAlreadyQueried = $this->helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb($identifier, "item");

                if(!empty($itemAlreadyQueried)) {
                    $anyItem = $itemAlreadyQueried;
                }
                else {
                    $conditionsToReturn = [
                        "item_id" => $identifier,
                        "source_item_id" => $identifier
                    ];
                    $returnPaginatedData = false;
                    $paginationFilters = [];
                    $returnSortedData = false;
                    $sortfilters = [];
                    $operator = 'OR';
                    $relations  =   ['nodeType', 'document'];

                    $anyItemCollection =  $this->itemRepository->findByAttributes(
                        $conditionsToReturn,
                        $returnPaginatedData,
                        $paginationFilters,
                        $returnSortedData ,
                        $sortfilters,
                        $operator,
                        $relations
                    );
                    $anyItem = $anyItemCollection->first();
                }

                $this->helperToSetDocumentAndItemAlreadyQueriedFromDb($anyItem, $identifier, "item");

                $uriToReturn = !empty($anyItem) ? $this->helperToCreateAndReturnUriOfProvidedType($anyItem, "item") : [];
            }
            else {
                $uriToReturn = $this->helperToCreateAndReturnUriOfProvidedType($anyDocument, "document");
            }
        }
        else {
            $uriToReturn = $itemUriAssociatedToRequestedTaxonomy;
        }
        return $uriToReturn;
    }

    private function helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb(string $searchIdentifier, string $type) {
        if($type==="document") {
            return !empty($this->_tempStorageForSavedDocument[$searchIdentifier]) ?
                $this->_tempStorageForSavedDocument[$searchIdentifier] :
                [];
        }
        else {
            return !empty($this->_tempStorageForSavedItem[$searchIdentifier]) ?
                $this->_tempStorageForSavedItem[$searchIdentifier] :
                [];
        }
    }

    private function helperToSetDocumentAndItemAlreadyQueriedFromDb($dataToSet, string $identifier, string $type) {
        if($type==="document") {
            $this->_tempStorageForSavedDocument[$identifier] = $dataToSet;
        }
        else {
            $this->_tempStorageForSavedItem[$identifier] = $dataToSet;
        }
    }

    /**
     * @param $orgCode
     * @return mixed
     * @Funcrion getOrganizationDetails
     * @Purpose Based on org code get org id for tenant specific
     */
    public function getOrganizationDetails($orgCode)
    {
        $orgId = Organization:: select('organization_id')
            ->where('org_code',$orgCode)
            ->first();
        if($orgId) {
            $orgId = $orgId->toArray();
        }
        return $orgId ;
    }

    private function parseAndSetCSVAssociations() {
        $savedAssociations  =   $this->getSavedAssociations();
        $savedDocument      =   $this->getSavedDocument();
        $orgCode            =   $this->getOrgCode();
        $savedImportType    =   $this->getParsedImportTypeItems();
        $is_html            =   $this->is_html;

        $parsedAssociationsReverseCase  = [];
        $parsedCaseAssociations         = [];
      
        foreach($savedAssociations as $association) {
            if(!($association["target_item_id"] == "" && $association["source_document_id"] == $association["source_item_id"]))
            {
                $identifier         =   $association["source_item_association_id"];
                $sequenceNumber     =   $association["sequence_number"];
                $associationType    =   $this->returnAssociationType($association["association_type"]);
               
                $itemImportType     =   !empty($savedImportType[$association["source_item_id"]]) ? $savedImportType[$association["source_item_id"]] : $savedDocument["import_type"];
                $uri                =   ($itemImportType == 1) ? $association["uri"] : $this->getCaseApiUri("CFAssociations", $identifier, true, $orgCode,$this->domainName);
                $originNodeURI      =   $this->helperToSearchAndReturnItemUri($association["source_item_id"]);

                if(!empty($originNodeURI)) {
                    $CFDocumentURI = $this->getParsedCaseDocumentUri();
                    $destinationNodeURI = [];
                    if($associationType==="exemplar") {
                        $uri        =   !empty($association["external_node_url"]) ? $association["external_node_url"] : "";
                        $title      =   !empty($association["external_node_title"]) ? $association["external_node_title"] : "";
                        // as discussed with Avalon, since exemplar association destination can be any resource,
                        // not specific to CASE standard item or document, so we create a dummy destination identifier for this
                        // which doesn't exist in our system
                        $dummyDestinationIdentifier = $this->createUniversalUniqueIdentifier();
                        $destinationNodeURI = [
                            "identifier" => $dummyDestinationIdentifier,
                            "uri" => $uri,
                            "title" => $title
                        ];
                    }
                    else if(!empty($association["target_item_id"])) {
                        $destinationNodeURI = ($association["target_item_id"]===$savedDocument["document_id"]) ?
                            $CFDocumentURI :
                            $this->fetchAnyAssociatedItemOrDocumentUri($association["target_item_id"]);
                        if(empty($destinationNodeURI))
                        {
                            $destinationNodeURI = [
                                "identifier" => $association["target_item_id"],
                                "uri" => $association["external_node_url"],
                                "title" => $association["external_node_title"]
                            ];
                        }
                    }
                    else if(empty($association["target_item_id"]))
                    {
                        $destinationNodeURI = [
                            "identifier" => $association["target_item_id"],
                            "uri" => $association["external_node_url"],
                            "title" => $association["external_node_title"]
                        ];
                    }

                    $CFAssociationGroupingURI = !empty($association["association_group_id"]) ?
                        $this->helperToSearchAndReturnAssociationGroupUri($association["association_group_id"]) :
                        [];
                    $lastChangeDateTime = !empty($association["updated_at"]) ? $this->formatDateTimeToISO8601($association["updated_at"]) : $this->formatDateTimeToISO8601($association["created_at"]);
                    $parsedAssociationsReverseCase[$identifier] = 0;
                    $parsedCaseAssociations[] = [
                        "identifier" => $identifier,
                        "associationType" => $associationType,
                        "CFDocumentURI" => $CFDocumentURI,
                        "sequenceNumber" => $sequenceNumber,
                        "uri" => $uri,
                        "originNodeURI" => $originNodeURI,
                        "destinationNodeURI" => $destinationNodeURI,
                        "CFAssociationGroupingURI" => $CFAssociationGroupingURI,
                        "lastChangeDateTime" => $lastChangeDateTime
                    ];
					//Added associationPersistentId to send unique identifier for Assoications for versioning request - UF-1871
                    if($this->versioning){
                        $parsedCaseAssociations["associationPersistentId"] = $originNodeURI["identifier"].'_'.$associationType.'_'.$destinationNodeURI["identifier"];
                    }

                } else {
                    //$CFDocumentURI = $this->getParsedCaseDocumentUriForReverseAssociation();
                    $originNodeURI      =   $this->helperToSearchAndReturnItemUriForReverseAssociation($association["source_item_id"]);

                    $CFDocumentURI      =   $this->helperToSearchAndReturnDocumentUriForReverseAssociation($association["source_document_id"]);
                    $destinationNodeURI =   $this->fetchAnyAssociatedItemOrDocumentUri($association["target_item_id"]);

                    $CFAssociationGroupingURI = !empty($association["association_group_id"]) ?
                        $this->helperToSearchAndReturnAssociationGroupUri($association["association_group_id"]) :
                        [];
                    $lastChangeDateTime = !empty($association["updated_at"]) ? $this->formatDateTimeToISO8601($association["updated_at"]) : $this->formatDateTimeToISO8601($association["created_at"]);
                    $parsedAssociationsReverseCase[$identifier] = 0;
                    $parsedCaseAssociations[] = [
                        "identifier" => $identifier,
                        "associationType" => $associationType,
                        "CFDocumentURI" => $CFDocumentURI,
                        "sequenceNumber" => $sequenceNumber,
                        "uri" => $uri,
                        "originNodeURI" => $originNodeURI,
                        "destinationNodeURI" => $destinationNodeURI,
                        "CFAssociationGroupingURI" => $CFAssociationGroupingURI,
                        "lastChangeDateTime" => $lastChangeDateTime
                    ];
					//Added associationPersistentId to send unique identifier for Assoications for versioning request - UF-1871
                    if($this->versioning){
                        $parsedCaseAssociations["associationPersistentId"] = $originNodeURI["identifier"].'_'.$associationType.'_'.$destinationNodeURI["identifier"];
                    }
                }
            }
        }
       
        $this->setParsedAssociationsReverseCase($parsedAssociationsReverseCase);
        $this->setParsedCaseAssociations($parsedCaseAssociations);
    }

    
}