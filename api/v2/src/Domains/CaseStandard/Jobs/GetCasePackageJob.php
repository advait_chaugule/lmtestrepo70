<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Database\Eloquent\Collection;

use App\Data\Models\Document;
use App\Data\Models\Organization;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
//use App\Data\Repositories\Contracts\ItemTypeRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;

use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;

class GetCasePackageJob extends Job
{

    use DateHelpersTrait, CaseFrameworkTrait, ArrayHelper;

    private $documentRepository;
    private $itemRepository;
    private $licenseRepository;
    private $subjectRepository;
    //private $itemTypeRepository;
    private $nodeTypeRepository;
    private $conceptRepository;
    private $itemAssociationRepository;
    private $associationGroupRepository;
    private $languageRepository;

    private $unprocessedDocument;
    private $unprocessedDocumentSubjects;

    private $unprocessedItems;
    private $unprocessedItemTypes;
    private $unprocessedConcepts;
    private $unprocessedItemAssociations;
    private $unprocessedAssociationGroups;
    private $unprocessedLicenses;
    private $unprocessedLanguages;

    private $documentIdentifier;
    private $itemTypeIdentifiersToSearchFor;
    private $licenseIdentifiersToSearchFor = [];
    private $conceptIdentifiersToSearchFor;
    private $associationGroupIdentifiersToSearchFor;
    private $languageIdentifiersToSearchFor;

    private $parsedCaseDocumentSubjects = [];
    private $parsedCaseDocumentSubjectUris = [];
    private $parsedCaseDocumentLicenseUri;
    private $parsedCaseConcepts;
    private $parsedCaseItemTypes;
    
    private $parsedCaseDocument;
    private $parsedCaseDocumentUri;
    private $parsedCaseItems;

    private $parsedCaseAssociationGroups;
    private $parsedCaseAssociations;
    private $organizationCode;
    private $casePackageData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {
        $this->setDocumentIdentifier($documentIdentifier);
    }

    public function setDocumentIdentifier(string $data) {
        $this->documentIdentifier = $data;
    }

    public function getDocumentIdentifier(): string {
        return $this->documentIdentifier;
    }

    public function setUnprocessedDocument(Document $data) {
        $this->unprocessedDocument = $data;
    }

    public function getUnprocessedDocument() {
        return $this->unprocessedDocument;
    }

    public function setUnprocessedItems(Collection $data) {
        $this->unprocessedItems = $data;
    }

    public function getUnprocessedItems() {
        return $this->unprocessedItems;
    }

    public function setUnprocessedDocumentSubjects(Collection $data) {
        $this->unprocessedDocumentSubjects = $data;
    }

    public function getUnprocessedDocumentSubjects() {
        return $this->unprocessedDocumentSubjects;
    }

    public function setUnprocessedItemTypes(Collection $data) {
        $this->unprocessedItemTypes = $data;
    }

    public function getUnprocessedItemTypes() {
        return $this->unprocessedItemTypes;
    }

    public function setUnprocessedConcepts(Collection $data) {
        $this->unprocessedConcepts = $data;
    }

    public function getUnprocessedConcepts() {
        return $this->unprocessedConcepts;
    }

    public function setUnprocessedItemAssociations(Collection $data) {
        $this->unprocessedItemAssociations = $data;
    }

    public function getUnprocessedItemAssociations() {
        return $this->unprocessedItemAssociations;
    }

    public function setUnprocessedAssociationGroups(Collection $data) {
        $this->unprocessedAssociationGroups = $data;
    }

    public function getUnprocessedAssociationGroups() {
        return $this->unprocessedAssociationGroups;
    }

    public function setUnprocessedLicenses(Collection $data) {
        $this->unprocessedLicenses = $data;
    }

    public function getUnprocessedLicenses() {
        return $this->unprocessedLicenses;
    }

    public function setUnprocessedLanguages(Collection $data) {
        $this->unprocessedLanguages = $data;
    }

    public function getUnprocessedLanguages() {
        return $this->unprocessedLanguages;
    }

    public function setItemTypeIdentifiersToSearchFor(array $data) {
        $this->itemTypeIdentifiersToSearchFor = $data;
    }

    public function getItemTypeIdentifiersToSearchFor(): array {
        return $this->itemTypeIdentifiersToSearchFor;
    }

    public function setLicenseIdentifiersToSearchFor(array $data) {
        $this->licenseIdentifiersToSearchFor = $data;
    }

    public function getLicenseIdentifiersToSearchFor(): array {
        return $this->licenseIdentifiersToSearchFor;
    }

    public function setConceptIdentifiersToSearchFor(array $data) {
        $this->conceptIdentifiersToSearchFor = $data;
    }

    public function getConceptIdentifiersToSearchFor(): array {
        return $this->conceptIdentifiersToSearchFor;
    }

    public function setAssociationGroupIdentifiersToSearchFor(array $data) {
        $this->associationGroupIdentifiersToSearchFor = $data;
    }

    public function getAssociationGroupIdentifiersToSearchFor(): array {
        return $this->associationGroupIdentifiersToSearchFor;
    }

    public function setLanguageIdentifiersToSearchFor(array $data) {
        $this->languageIdentifiersToSearchFor = $data;
    }

    public function getLanguageIdentifiersToSearchFor(): array {
        return $this->languageIdentifiersToSearchFor;
    }

    public function setParsedCaseDocumentSubjects(array $data) {
        $this->parsedCaseDocumentSubjects = $data;
    }

    public function getParsedCaseDocumentSubjects(): array {
        return $this->parsedCaseDocumentSubjects;
    }

    public function setParsedCaseDocumentSubjectUris(array $data) {
        $this->parsedCaseDocumentSubjectUris = $data;
    }

    public function getParsedCaseDocumentSubjectUris(): array {
        return $this->parsedCaseDocumentSubjectUris;
    }

    public function setParsedCaseLanguages(array $data) {
        $this->parsedCaseLanguages = $data;
    }

    public function getParsedCaseLanguages() {
        return $this->parsedCaseLanguages;
    }

    public function setParsedCaseLicenses(array $data) {
        $this->parsedCaseLicenses = $data;
    }

    public function getParsedCaseLicenses(): array {
        return $this->parsedCaseLicenses;
    }

    public function setParsedCaseDocumentLicenseUri(array $data) {
        $this->parsedCaseDocumentLicenseUri = $data;
    }

    public function getParsedCaseDocumentLicenseUri(): array {
        return $this->parsedCaseDocumentLicenseUri;
    }

    public function setParsedCaseConcepts(array $data) {
        $this->parsedCaseConcepts = $data;
    }

    public function getParsedCaseConcepts(): array {
        return $this->parsedCaseConcepts;
    }

    public function setParsedCaseItemTypes(array $data) {
        $this->parsedCaseItemTypes = $data;
    }

    public function getParsedCaseItemTypes(): array {
        return $this->parsedCaseItemTypes;
    }

    public function setParsedCaseDocument(array $data) {
        $this->parsedCaseDocument = $data;
    }

    public function getParsedCaseDocument(): array {
        return $this->parsedCaseDocument;
    }

    public function setParsedCaseDocumentUri(array $data) {
        $this->parsedCaseDocumentUri = $data;
    }

    public function getParsedCaseDocumentUri(): array {
        return $this->parsedCaseDocumentUri;
    }

    public function setParsedCaseItems(array $data) {
        $this->parsedCaseItems = $data;
    }

    public function getParsedCaseItems(): array {
        return $this->parsedCaseItems;
    }
    
    public function setParsedCaseAssociationGroups(array $data) {
        $this->parsedCaseAssociationGroups = $data;
    }

    public function getParsedCaseAssociationGroups(): array {
        return $this->parsedCaseAssociationGroups;
    }
    
    public function setParsedCaseAssociations(array $data) {
        $this->parsedCaseAssociations = $data;
    }

    public function getParsedCaseAssociations(): array {
        return $this->parsedCaseAssociations;
    }

    public function setCasePackageData(array $data) {
        $this->casePackageData = $data;
    }

    public function getCasePackageData(): array {
        return $this->casePackageData;
    }

    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        return $this->organizationCode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        //ItemTypeRepositoryInterface $itemTypeRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ConceptRepositoryInterface $conceptRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssociationGroupRepositoryInterface $associationGroupRepository
    )
    {
        // set the database repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->subjectRepository = $subjectRepository;
        //$this->itemTypeRepository = $itemTypeRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->conceptRepository = $conceptRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->associationGroupRepository = $associationGroupRepository;

        // fetch and set Document and it's related models
        $this->processDocumentAndRelatedEntities();

        // fetch and set Item and it's related models
        $this->processItems();

        // extract all identifiers for all item related models and set them
        $this->extractAllItemRelatedEntitiesIdentifier();

        // merge document license with item licenses only after all identifiers related to item has been processed
        $this->pushDocumentLicenseIdentifierIntoLicenseIdentifiersToSearchFor();

        // merge document language with item languages only after all identifiers related to item has been processed
        $this->pushDocumentLanguageIdentifierIntoLanguageIdentifiersToSearchFor();

        // fetch and set all items and their related entities
        $this->processItemRelatedEntities();

        // fetch and set association
        $this->processItemAssociationAndRelatedEntiy();

        // extract association group identifiers and set them
        $this->extractAssociationGroupIdentifiers();

        // fetch and set association groups
        $this->processAssociationGroups();

        // parse all data fetched from database to CASE compatible data structures
        $this->parseCaseRelevantData();

        // prepare the main CASE Package Container
        $this->prepareCasePackage();

        return $this->getCasePackageData();
    }

    /**
     * fetch and set Document and it's related models
     */
    private function processDocumentAndRelatedEntities() {
        $unprocessedDocument = $this->fetchDocument();
        $this->setUnprocessedDocument($unprocessedDocument);
        $unprocessedDocumentSubjects = $this->fetchDocumentSubjects();
        $this->setUnprocessedDocumentSubjects($unprocessedDocumentSubjects);
    }

    /**
     * fetch and set Item and it's related models
     */
    private function processItems() {
        $unprocessedItems = $this->fetchItems();
        $this->setUnprocessedItems($unprocessedItems);
    }

    /**
     * extract all identifiers for all item related models and set them
     */
    private function extractAllItemRelatedEntitiesIdentifier() {
        $itemTypeIdentifiersToSearchFor = $this->helperToExtractSpecifiedTypeIdentifiersFromItems('node_type_id');
        $this->setItemTypeIdentifiersToSearchFor($itemTypeIdentifiersToSearchFor);
        // extract the concept ids and set them
        $conceptIdentifiersToSearchFor = $this->helperToExtractSpecifiedTypeIdentifiersFromItems('concept_id');
        $this->setConceptIdentifiersToSearchFor($conceptIdentifiersToSearchFor);
        // extract the license ids and set them
        $licenseIdentifiersToSearchFor = $this->helperToExtractSpecifiedTypeIdentifiersFromItems('license_id');
        $this->setLicenseIdentifiersToSearchFor($licenseIdentifiersToSearchFor);
        // extract the language ids and set them
        $languageIdentifiersToSearchFor = $this->helperToExtractSpecifiedTypeIdentifiersFromItems('language_id');
        $this->setLanguageIdentifiersToSearchFor($languageIdentifiersToSearchFor);
    }

    /**
     * fetch and set all items and their related entities
     */
    private function processItemRelatedEntities() {
        // fetch and set item types
        $unprocessedItemTypes = $this->fetchItemTypes();
        $this->setUnprocessedItemTypes($unprocessedItemTypes);
        // fetch and set items concepts
        $unprocessedConcepts = $this->fetchConcepts();
        $this->setUnprocessedConcepts($unprocessedConcepts);
        // fetch and set licenses
        $unprocessedLicenses = $this->fetchLicenses();
        $this->setUnprocessedLicenses($unprocessedLicenses);
        // fetch and set languages
        $unprocessedLanguages = $this->fetchLanguages();
        $this->setUnprocessedLanguages($unprocessedLanguages);
    }

    /**
     * fetch and set association
     */
    private function processItemAssociationAndRelatedEntiy() {
        $unprocessedItemAssociations = $this->fetchItemAssociations();
        $this->setUnprocessedItemAssociations($unprocessedItemAssociations);
    }

    /**
     *  extract association group identifiers and set them
     */
    private function extractAssociationGroupIdentifiers() {
        $itemAssociations = $this->getUnprocessedItemAssociations();
        $filteredIdentifiers = $itemAssociations->pluck("association_group_id")->filter(function ($item, $key) {
            if(!empty($item)){
                return $item;
            }
        });
        $this->setAssociationGroupIdentifiersToSearchFor($filteredIdentifiers->toArray());
    }

    /**
     * fetch and set association groups
     */
    private function processAssociationGroups() {
        $unprocessedAssociationGroups = $this->fetchAssociationGroups();
        $this->setUnprocessedAssociationGroups($unprocessedAssociationGroups);
    }

    private function fetchDocument() {
        $documentIdentifier = $this->getDocumentIdentifier();
        return $this->documentRepository->find($documentIdentifier);
    }

    private function fetchDocumentSubjects() {
        $document = $this->getUnprocessedDocument();
        return $document->subjects()->where('is_deleted', 0)->get()->unique('subject_id');
    }

    private function fetchItems() {
        $documentIdentifier = $this->getDocumentIdentifier();
        $searchAttributes = ["document_id" => $documentIdentifier, "is_deleted"=> 0];
        return $this->itemRepository->findByAttributes($searchAttributes);
    }

    private function fetchItemTypes() {
        $itemTypeIdentifiers = $this->getItemTypeIdentifiersToSearchFor();
        return $this->nodeTypeRepository->findByAttributeContainedIn("node_type_id", $itemTypeIdentifiers, ["is_deleted" => 0]);
    }

    private function fetchLicenses() {
        $licenseIdentifiers = $this->getLicenseIdentifiersToSearchFor();
        return $this->licenseRepository->findByAttributeContainedIn("license_id", $licenseIdentifiers, ["is_deleted" => 0]);
    }

    private function fetchConcepts() {
        $conceptIdentifiers = $this->getConceptIdentifiersToSearchFor();
        return $this->conceptRepository->findByAttributeContainedIn("concept_id", $conceptIdentifiers, ["is_deleted" => 0]);
    }

    private function fetchItemAssociations() {
        $documentIdentifier = $this->getDocumentIdentifier();
        $attributeValueToSearchFor = ["document_id" => $documentIdentifier, "is_deleted" => 0];
        return $this->itemAssociationRepository->findByAttributes($attributeValueToSearchFor);
    }
    
    private function fetchAssociationGroups() {
        $associationGroupIdentifiers = $this->getAssociationGroupIdentifiersToSearchFor();
        return $this->associationGroupRepository->findByAttributeContainedIn("association_group_id", $associationGroupIdentifiers, ["is_deleted" => 0]);
    }

    private function fetchLanguages() {
        $languageIdentifiers = $this->getLanguageIdentifiersToSearchFor();
        return $this->languageRepository->findByAttributeContainedIn("language_id", $languageIdentifiers, ["is_deleted" => 0]);
    }

    /**
     * parse all data fetched from database to CASE compatible data structures
     */
    private function parseCaseRelevantData() {
        $this->parseCaseDocumentSubjects();
        $this->parseCaseDocumentSubjectUris();
        $this->parseCaseLanguages();
        $this->parseCaseLicenses();
        $this->parseCaseDocumentLicenseUri();
        $this->parseCaseConcepts();
        $this->parseItemTypes();

        $this->parseCaseDocument();
        $this->parseCaseItems();

        $this->parseCaseAssociationGroup();
        $this->parseCaseAssociations();
    }

    private function parseCaseDocumentSubjects() {
        $unprocessedDocumentSubjects = $this->getUnprocessedDocumentSubjects();
        $orgCode                     = $this->getOrgCode();
        if(!empty($unprocessedDocumentSubjects) && $unprocessedDocumentSubjects->count() > 0){
            $parsedDocumentSubjects = [];
            foreach($unprocessedDocumentSubjects as $documentSubject) {
                $identifier = $documentSubject->subject_id;
                $title = !empty($documentSubject->title) ? $documentSubject->title : "";
                $hierarchyCode = !empty($documentSubject->hierarchy_code) ? $documentSubject->hierarchy_code : "";
                $description = !empty($documentSubject->description) ? $documentSubject->description : "";
                $uri = $this->getCaseApiUri("CFSubjects", $identifier,'',$orgCode,'');
                $lastChangeDateTime = !empty($documentSubject->updated_at) ? $this->formatDateTimeToISO8601($documentSubject->updated_at) : "";
                $parsedDocumentSubjects[] = [
                    "identifier" => $identifier,
                    "source_identifier" => $documentSubject->source_subject_id,
                    "uri" => $uri,
                    "title" => $title,
                    "hierarchyCode" => $hierarchyCode,
                    "description" => $description,
                    "lastChangeDateTime" => $lastChangeDateTime
                ];
            }
            $this->setParsedCaseDocumentSubjects($parsedDocumentSubjects);
        }
    }

    private function parseCaseDocumentSubjectUris() {
        $parsedSubjects = $this->getParsedCaseDocumentSubjects();
        $subjectUris = [];
        foreach($parsedSubjects as $parsedSubject) {
            $subjectUris[] = [
                "title" => $parsedSubject['title'],
                "identifier" => $parsedSubject['identifier'],
                "source_identifier" =>$parsedSubject['source_identifier'],
                "uri" => $parsedSubject['uri']
            ];
        }
        $this->setParsedCaseDocumentSubjectUris($subjectUris);
    }

    private function parseCaseLanguages() {
        $unprocessedLanguages = $this->getUnprocessedLanguages();
        $parsedLanguages = [];
        foreach($unprocessedLanguages as $unprocessedLanguage) {
            $identifier = $unprocessedLanguage->language_id;
            $name = !empty($unprocessedLanguage->name) ? $unprocessedLanguage->name : "";
            $shortCode = !empty($unprocessedLanguage->short_code) ? $unprocessedLanguage->short_code : "";
            $parsedLanguages[] = [
                "identifier" => $identifier,
                "name" => $name,
                "short_code" => $shortCode
            ];
        }
        $this->setParsedCaseLanguages($parsedLanguages);
    }

    private function parseCaseLicenses() {
        $unprocessedLicenses = $this->getUnprocessedLicenses();
        $orgCode             = $this->getOrgCode();
        $parsedLicenses = [];
        foreach($unprocessedLicenses as $license) {
            $identifier = $license["license_id"];
            $uri = $this->getCaseApiUri("CFLicenses", $identifier,'',$orgCode,'');
            $title = !empty($license->title) ? $license->title : "";
            $description = !empty($license->description) ? $license->description : "";
            $licenseText = !empty($license->license_text) ? $license->license_text : "";
            $lastChangeDateTime = !empty($license->updated_at) ? $this->formatDateTimeToISO8601($license->updated_at) : "";
            $parsedLicenses[] = [
                "identifier" => $identifier,
                "source_identifier" => $license["source_license_id"],
                "uri" => $uri,
                "title" => $title,
                "description" => $description,
                "licenseText" => $licenseText,
                "lastChangeDateTime" => $lastChangeDateTime
            ];
        }
        $this->setParsedCaseLicenses($parsedLicenses);
    }

    private function parseCaseDocumentLicenseUri() {
        $unprocessedLicenses = $this->getUnprocessedDocument();
        $documentLicenseIdentifierTosearchFor = $unprocessedLicenses['license_id'];
        $parsedLicenses = $this->getParsedCaseLicenses();
        
        $documentLicense = [];
        if(!empty($parsedLicenses)){
            foreach($parsedLicenses as $license) {
                if($license['identifier']===$documentLicenseIdentifierTosearchFor) {
                    $documentLicense = [
                        "identifier" => $license['identifier'],
                        "source_identifier" => $license['source_identifier'],
                        "uri" => $license['uri'],
                        "title" => $license['title']
                    ];
                    break;
                }
            }
        }
        $this->setParsedCaseDocumentLicenseUri($documentLicense);
    }

    private function parseCaseConcepts() {
        $unprocessedConcepts = $this->getUnprocessedConcepts();
        $orgCode             = $this->getOrgCode();
        $parsedCaseConcepts = [];
        foreach($unprocessedConcepts as $concept) {
            $identifier = $concept->concept_id;
            $uri = $this->getCaseApiUri("CFConcepts", $identifier,'',$orgCode,'');
            $title = !empty($concept->title) ? $concept->title : "";
            $keywords = !empty($concept->keywords) ? $concept->keywords : "";
            $hierarchyCode = !empty($concept->hierarchy_code) ? $concept->hierarchy_code : "";
            $description = !empty($concept->description) ? $concept->description : "";
            $lastChangeDateTime = !empty($concept->updated_at) ? $this->formatDateTimeToISO8601($concept->updated_at) : "";
            $parsedCaseConcepts[] = [
                "identifier" => $identifier,
                "source_identifier" => $concept->source_concept_id,
                "uri" => $uri,
                "title" => $title,
                "keywords" => $keywords,
                "hierarchyCode" => $hierarchyCode,
                "description" => $description,
                "lastChangeDateTime" => $lastChangeDateTime,
            ];
        }
        $this->setParsedCaseConcepts($parsedCaseConcepts);
    }

    private function parseItemTypes() {
        $unprocessedItemTypes = $this->getUnprocessedItemTypes();
        $orgCode              = $this->getOrgCode();
        $parsedCaseItemTypes = [];
        foreach($unprocessedItemTypes as $itemType) {
            $identifier = $itemType->node_type_id;
            $uri = $this->getCaseApiUri("CFItemTypes", $identifier,'',$orgCode,'');
            $title = !empty($itemType->title) ? $itemType->title : "";
            $description = !empty($itemType->description) ? $itemType->description : "";
            $hierarchyCode = !empty($itemType->hierarchy_code) ? $itemType->hierarchy_code : "";
            $typeCode = !empty($itemType->type_code) ? $itemType->type_code : "";
            $lastChangeDateTime = !empty($itemType->updated_at) ? $this->formatDateTimeToISO8601($itemType->updated_at) : "";
            $parsedCaseItemTypes[] = [
                "identifier" => $identifier,
                "source_identifier" => $itemType->source_node_type_id,
                "uri" => $uri,
                "title" => $title,
                "description" => $description,
                "hierarchyCode" => $hierarchyCode,
                "typeCode" => $typeCode,
                "lastChangeDateTime" => $lastChangeDateTime,
            ];
        }
        $this->setParsedCaseItemTypes($parsedCaseItemTypes);
    }

    private function parseCaseDocument() {
        $unprocessedCaseDocument = $this->getUnprocessedDocument();
        $orgCode                 = $this->getOrgCode();
        $identifier = $unprocessedCaseDocument->source_document_id;
        $uri = $this->getCaseApiUri("CFDocuments", $identifier,'',$orgCode,'');
        $creator = !empty($unprocessedCaseDocument->creator) ? $unprocessedCaseDocument->creator : "";
        $title = !empty($unprocessedCaseDocument->title) ? $unprocessedCaseDocument->title : "";
        $lastChangeDateTime = !empty($unprocessedCaseDocument->updated_at) ? $this->formatDateTimeToISO8601($unprocessedCaseDocument->updated_at) : "";
        $officialSourceURL = !empty($unprocessedCaseDocument->official_source_url) ? $unprocessedCaseDocument->official_source_url : "";
        $publisher = !empty($unprocessedCaseDocument->publisher) ? $unprocessedCaseDocument->publisher : "";
        $description = !empty($unprocessedCaseDocument->description) ? $unprocessedCaseDocument->description : "";
        $subjectsTitle = $this->helperToCreateSubjectsTitle();
        $subjectUriTemp = $this->getParsedCaseDocumentSubjectUris();
        for($i=0; $i < count($subjectUriTemp); $i++){
            if($subjectUriTemp[$i]["source_identifier"] != ""){
                $subjectUriTemp[$i]["identifier"] = $subjectUriTemp[$i]["source_identifier"];
                unset($subjectUriTemp[$i]["source_identifier"]);
            }
        }
        $language = !empty($unprocessedCaseDocument->language_id) ? $this->searchForLanguage($unprocessedCaseDocument->language_id) : "";
        $version = !empty($unprocessedCaseDocument->version) ? $unprocessedCaseDocument->version : "";
        $adoptionStatus = !empty($unprocessedCaseDocument->adoption_status) ? 
                            $this->getSystemSpecifiedAdoptionStatusText($unprocessedCaseDocument->adoption_status) : 
                            "Draft";
        $statusStartDate = !empty($unprocessedCaseDocument->status_start_date) ? $this->formatDateTimeToDateOnly($unprocessedCaseDocument->status_start_date) : "";
        $statusEndDate = !empty($unprocessedCaseDocument->status_end_date) ? $this->formatDateTimeToDateOnly($unprocessedCaseDocument->status_end_date) : "";
        $licenseUriTemp = $this->getParsedCaseDocumentLicenseUri();
       
       
        if(!empty($licenseUriTemp)){
            $licenseUriTemp["identifier"] = $licenseUriTemp["source_identifier"];
            unset($licenseUriTemp["source_identifier"]);
        }
       
        $notes = !empty($unprocessedCaseDocument->notes) ? $unprocessedCaseDocument->notes : "";
        
        $casePackageUri['title'] = $title;
        $casePackageUri['identifier'] =  $identifier;
        $casePackageUri ['uri']= $this->getCaseApiUri("CFPackages", $identifier,'',$orgCode,'');
       
        $caseDocumentData = [
            "identifier" => $identifier,
            "uri" => $uri,
            "creator" => $creator,
            "title" => $title,
            "lastChangeDateTime" => $lastChangeDateTime,
            "officialSourceURL" => $officialSourceURL,
            "publisher" => $publisher,
            "description" => $description,
            "subject" => $subjectsTitle,
            "subjectURI" => $subjectUriTemp,
            "language" => $language,
            "version" => $version,
            "adoptionStatus" => $adoptionStatus,
            "statusStartDate" => $statusStartDate,
            "statusEndDate" => $statusEndDate,
            "licenseURI" => $licenseUriTemp,
            "notes" => $notes,
            "CFPackageURI" => $casePackageUri
        ];
        $this->setParsedCaseDocument($caseDocumentData);

        $this->setParsedCaseDocumentUri([
            "title" => $title,
            "identifier" => $unprocessedCaseDocument->document_id,
            "source_identifier" => $identifier,
            "uri" => $uri
        ]);
    }

    private function parseCaseItems() {
        $unprocessedItems = $this->getUnprocessedItems();
        $tempCFDocumentURI = $this->getParsedCaseDocumentUri();
        $orgCode             = $this->getOrgCode();
        $CFDocumentURI['title'] = $tempCFDocumentURI['title'];
        $CFDocumentURI['identifier'] = $tempCFDocumentURI['source_identifier'];
        $CFDocumentURI['uri'] = $tempCFDocumentURI['uri'];
        
        $parsedCaseItems = [];
        foreach($unprocessedItems as $item) {
            $identifier = $item->item_id;
            $fullStatement = !empty($item->full_statement) ? $item->full_statement : "";
            $alternativeLabel = !empty($item->alternative_label) ? $item->alternative_label : "";
            $uri = !empty($identifier) ? $this->getCaseApiUri("CFItems", $identifier,'',$orgCode,'') : "";
            $humanCodingScheme = !empty($item->human_coding_scheme) ? $item->human_coding_scheme : "";
            $listEnumeration = !empty($item->list_enumeration) ? $item->list_enumeration : "";
            $abbreviatedStatement = !empty($item->abbreviated_statement) ? $item->abbreviated_statement : "";

            $conceptIdentifier = $item->concept_id;
            if($conceptIdentifier != ""){
                $conceptDetails = $this->searchForItemConceptKeywordsUri($conceptIdentifier);
                $conceptKeywords = $conceptDetails['conceptKeywords'];
                $conceptKeywordsURI = $conceptDetails['conceptKeywordsURI'];
            }else{
                $conceptDetails = "";
                $conceptKeywords = "";
                $conceptKeywordsURI = "";
            }
            

            $notes = !empty($item->notes) ? $item->notes : "";
            $language = !empty($item->language_id) ? $this->searchForLanguage($item->language_id) : "";
            $educationLevel = !empty($item->education_level) ? $this->createArrayFromCommaeSeparatedString($item->education_level) : "";

            $CFItemTypeIdentifier = !empty($item->node_type_id) ? $item->node_type_id : "";
            
            $caseItemTypeDetails = $this->searchForItemTypeUri($CFItemTypeIdentifier);
            $CFItemTypeTitle = $caseItemTypeDetails["CFItemTypeTitle"];
            $CFItemTypeUri = $caseItemTypeDetails["CFItemTypeUri"];

            $licenseUri = !empty($item->license_id) ? $this->searchForLicenseUri($item->license_id) : [];

            $statusStartDate = !empty($item->status_start_date) ? $this->formatDateTimeToDateOnly($item->status_start_date) : "";
            $statusEndDate = !empty($item->status_end_date) ? $this->formatDateTimeToDateOnly($item->status_end_date) : "";
            $lastChangeDateTime = !empty($item->updated_at) ? $this->formatDateTimeToISO8601($item->updated_at) : "";
            $parsedCaseItems[] = [
                        "identifier" => $identifier,
                        "source_identifier" => $item->source_item_id,
                        "CFDocumentURI" => $CFDocumentURI,
                        "fullStatement" => $fullStatement,
                        "alternativeLabel" => $alternativeLabel,
                        "CFItemType" => $CFItemTypeTitle,
                        "uri" => $uri,
                        "humanCodingScheme" => $humanCodingScheme,
                        "listEnumeration" => $listEnumeration,
                        "abbreviatedStatement" => $abbreviatedStatement,
                        "conceptKeywords" => $conceptKeywords,
                        "conceptKeywordsURI" => $conceptKeywordsURI,
                        "notes" => $notes,
                        "language" => $language,
                        "CFItemType" => $CFItemTypeTitle,
                        "educationLevel" => $educationLevel,
                        "CFItemTypeURI" => $CFItemTypeUri,
                        "licenseURI" => $licenseUri,
                        "statusStartDate" => $statusStartDate,
                        "statusEndDate" => $statusEndDate,
                        "lastChangeDateTime" => $lastChangeDateTime
                    ];
        }
        $this->setParsedCaseItems($parsedCaseItems);
    }

    private function parseCaseAssociationGroup() {
        $unprocessedAssociationGroups = $this->getUnprocessedAssociationGroups();
        $orgCode                      = $this->getOrgCode();
        $parsedAssociationGroups = [];
        foreach($unprocessedAssociationGroups as $associationGroup) {
            $identifier = $associationGroup->association_group_id;
            $uri = $this->getCaseApiUri("CFAssociationGroupings", $identifier,'',$orgCode,'');
            $title = !empty($associationGroup->title) ? $associationGroup->title : "";
            $description = !empty($associationGroup->description) ? $associationGroup->description : "";
            $lastChangeDateTime = !empty($associationGroup->updated_at) ? $this->formatDateTimeToISO8601($associationGroup->updated_at) : "";
            $parsedAssociationGroups[] = [
                "identifier" => $associationGroup->association_group_id,
                "source_identifier" => $associationGroup->source_association_group_id,
                "uri" => $uri,
                "title" => $title,
                "description" => $description,
                "lastChangeDateTime" => $lastChangeDateTime,
            ];
        }
        $this->setParsedCaseAssociationGroups($parsedAssociationGroups);
    }

    private function parseCaseAssociations() {
        $unprocessedCaseAssociations = $this->getUnprocessedItemAssociations();
        $orgCode                     = $this->getOrgCode();
        $parsedCaseAssociations = [];
        foreach($unprocessedCaseAssociations as $association) {
            $identifier = $association->item_association_id;
            $associationType = $this->returnAssociationType($association->association_type);
            $tempCFDocumentURI = $this->getParsedCaseDocumentUri();
            $CFDocumentURI['title'] = $tempCFDocumentURI['title'];
            $CFDocumentURI['identifier'] = $tempCFDocumentURI['source_identifier'];
            $CFDocumentURI['uri'] = $tempCFDocumentURI['uri'];
            $sequenceNumber = $association->sequence_number;
            $uri = $this->getCaseApiUri("CFAssociations", $identifier,'',$orgCode,'');
            $originNodeURI = $this->searchForItemUriWhileParsingAssociations($association->origin_node_id);
            $destinationNodeURI = $this->searchForItemUriWhileParsingAssociations($association->destination_node_id);
            if(!empty($association->association_group_id)){
                $CFAssociationGroupingURI = $this->searchForAssociationGroupUri($association->association_group_id);
            }else{
                $CFAssociationGroupingURI = "";
            }
            $lastChangeDateTime = !empty($association->updated_at) ? $this->formatDateTimeToISO8601($association->updated_at) : "";

            $parsedCaseAssociations[] = [
                "identifier" => $identifier,
                "source_identifier" => $association->source_item_association_id,
                "associationType" => $associationType,
                "CFDocumentURI" => $CFDocumentURI,
                "sequenceNumber" => $sequenceNumber,
                "uri" => $uri,
                "originNodeURI" => $originNodeURI,
                "destinationNodeURI" => $destinationNodeURI,
                "CFAssociationGroupingURI" => $CFAssociationGroupingURI,
                "lastChangeDateTime" => $lastChangeDateTime
            ];
        }
        $this->setParsedCaseAssociations($parsedCaseAssociations);
    }

    private function prepareCasePackage() {
        $CFDocument = $this->getParsedCaseDocument();  
        $CFItems = $this->getParsedCaseItems();
        for($i=0; $i < count($CFItems); $i++){
            if($CFItems[$i]["source_identifier"] != ""){
                $CFItems[$i]["identifier"] = $CFItems[$i]["source_identifier"];
                unset($CFItems[$i]["source_identifier"]);
            }
        }
        $CFAssociations = $this->getParsedCaseAssociations();
       
        for($i=0; $i < count($CFAssociations); $i++){
            if($CFAssociations[$i]["source_identifier"] != ""){
                $CFAssociations[$i]["identifier"] = $CFAssociations[$i]["source_identifier"];
                unset($CFAssociations[$i]["source_identifier"]);
            }
        }
        
        // for CFDefinitions
        $CFConcepts = $this->getParsedCaseConcepts();
        for($i=0; $i < count($CFConcepts); $i++){
            if($CFConcepts[$i]["source_identifier"] != ""){
                $CFConcepts[$i]["identifier"] = $CFConcepts[$i]["source_identifier"];
                unset($CFConcepts[$i]["source_identifier"]);
            }
        }
        $CFSubjects = $this->getParsedCaseDocumentSubjects();
        for($i=0; $i < count($CFSubjects); $i++){
            if($CFSubjects[$i]["source_identifier"] != ""){
                $CFSubjects[$i]["identifier"] = $CFSubjects[$i]["source_identifier"];
                unset($CFSubjects[$i]["source_identifier"]);
            }
        }
        $CFLicenses = $this->getParsedCaseLicenses();
        for($i=0; $i < count($CFLicenses); $i++){
            if($CFLicenses[$i]["source_identifier"] != ""){
                $CFLicenses[$i]["identifier"] = $CFLicenses[$i]["source_identifier"];
                unset($CFLicenses[$i]["source_identifier"]);
            }
        }
        $CFItemTypes = $this->getParsedCaseItemTypes();
        for($i=0; $i < count($CFItemTypes); $i++){
            if($CFItemTypes[$i]["source_identifier"] != ""){
                $CFItemTypes[$i]["identifier"] = $CFItemTypes[$i]["source_identifier"];
                unset($CFItemTypes[$i]["source_identifier"]);
            }
        }
        $CFAssociationGroupings = $this->getParsedCaseAssociationGroups();
        for($i=0; $i < count($CFAssociationGroupings); $i++){
            if($CFAssociationGroupings[$i]["source_identifier"] != ""){
                $CFAssociationGroupings[$i]["identifier"] = $CFAssociationGroupings[$i]["source_identifier"];
                unset($CFAssociationGroupings[$i]["source_identifier"]);
            }
        }
        // Don't have rubrics yet (will do later)
        $CFRubrics = [];

        $casePackageData = [
            "CFDocument" => $CFDocument,
            "CFItems" => $CFItems,
            "CFAssociations" => $CFAssociations,
            "CFDefinitions" => [
                "CFConcepts" =>$CFConcepts,
                "CFSubjects" => $CFSubjects,
                "CFLicenses" => $CFLicenses,
                "CFItemTypes" => $CFItemTypes,
                "CFAssociationGroupings" => $CFAssociationGroupings
            ],
            // "CFRubrics" => $CFRubrics
        ];
        
        $this->setCasePackageData($casePackageData);
    }

    private function searchForItemConceptKeywordsUri(string $identifierToSearchFor): array {
        $parsedCaseConcepts = $this->getParsedCaseConcepts();
       
        $conceptDetails['conceptKeywords'] = [];
        $conceptDetails['conceptKeywordsURI'] = [];
        foreach($parsedCaseConcepts as $concept) {
            if($concept['identifier']===$identifierToSearchFor) {
                $conceptKeywords = !empty($concept['keywords']) ? [ $concept['keywords'] ] : [];
                $conceptDetails["conceptKeywords"] = $conceptKeywords;
                $conceptDetails["conceptKeywordsURI"] = [
                    "title" => $concept['title'],
                    "uri" => $concept['uri'],
                    "identifier" => $concept['source_identifier']
                ];
                break;
            }
        }
        return $conceptDetails;
    }

    private function searchForLanguage(string $identifierToSearchFor): string {
        $parsedLanguages = $this->getParsedCaseLanguages();
        $languageFound = "";
        foreach($parsedLanguages as $language) {
            if($language['identifier']===$identifierToSearchFor) {
                $languageFound = !empty($language['short_code']) ? $language['short_code'] : "";
                break;
            }
        }
        return $languageFound;
    }

    private function searchForLicenseUri(string $identifierToSearchFor): array {
        $parsedLLicenses = $this->getParsedCaseLicenses();
        $licenseFound = [];
        foreach($parsedLLicenses as $license) {
            if($license['identifier']===$identifierToSearchFor) {
                $licenseFound = [
                    "title" => $license['title'],
                    "uri" => $license['uri'],
                    "identifier" => $license['source_identifier']
                ];
                break;
            }
        }
        return $licenseFound;
    }

    private function searchForItemTypeUri($identifierToSearchFor): array {
        $parsedItemTypes = $this->getParsedCaseItemTypes();
        $caseItemTypeDetails["CFItemTypeTitle"] = "";
        $caseItemTypeDetails["CFItemTypeUri"] = [];
        foreach($parsedItemTypes as $itemType) {
            if($itemType['identifier']===$identifierToSearchFor) {
                $itemTypeTitle = !empty($itemType['title']) ? $itemType['title'] : "";
                $caseItemTypeDetails["CFItemTypeTitle"] = $itemTypeTitle;
                $caseItemTypeDetails["CFItemTypeUri"] = [
                    "title" => $itemTypeTitle,
                    "uri" => $itemType['uri'],
                    "identifier" => $itemType['source_identifier']
                ];
                break;
            }
        }
        return $caseItemTypeDetails;
    }

    /**
     * For Association, origin node is an item, but destination can be a document or an item
     * Consider document as well as item data for searching
     */
    private function searchForItemUriWhileParsingAssociations(string $identifierToSearchFor): array {
        $parsedItems = $this->getParsedCaseItems();
        
        $tempCFDocumentURI = $this->getParsedCaseDocumentUri();
        $CFDocumentURI['title'] = $tempCFDocumentURI['title'];
        $CFDocumentURI['identifier'] = $tempCFDocumentURI['source_identifier'];
        $CFDocumentURI['uri'] = $tempCFDocumentURI['uri'];
        // if identifier belongs to Case Document then return DocumentUri
        if($CFDocumentURI["identifier"]=== $identifierToSearchFor){
            return $CFDocumentURI;
        }

        // if identifier doesn't belong to Case Document then search case items
        $foundItemUri = [];
       
        foreach($parsedItems as $item) {
            if($item['identifier']===$identifierToSearchFor) {
                $foundItemUri = [
                    "title" => $item['humanCodingScheme'],
                    "uri" => $item['uri'],
                    "identifier" => $item['source_identifier']
                ];
                break;
            }
        }
        return $foundItemUri;
    }

    private function searchForAssociationGroupUri(string $identifierToSearchFor): array {
        $parsedCaseAssociationGroups = $this->getParsedCaseAssociationGroups();
        $associationGroupUriFound = [];
        foreach($parsedCaseAssociationGroups as $associationGroup) {
            if($associationGroup["identifier"]===$identifierToSearchFor){
                $associationGroupUriFound = [
                    "title" => $associationGroup['title'],
                    "identifier" => $associationGroup['source_identifier'],
                    "uri" => $associationGroup['uri']
                ];
                break;
            }
        }
        return $associationGroupUriFound;
    }

    private function pushDocumentLicenseIdentifierIntoLicenseIdentifiersToSearchFor() {
        $document = $this->getUnprocessedDocument();
        if(!empty($document->license_id)){
            $documentLicenseIdentifier = $document->license_id;
            $this->licenseIdentifiersToSearchFor[] = $documentLicenseIdentifier;
        }
    }

    private function pushDocumentLanguageIdentifierIntoLanguageIdentifiersToSearchFor() {
        $document = $this->getUnprocessedDocument();
        if(!empty($document->language_id)){
            $documentLanguageIdentifier = $document->language_id;
            $this->languageIdentifiersToSearchFor[] = $documentLanguageIdentifier;
        }
    }

    private function helperToExtractSpecifiedTypeIdentifiersFromItems(string $type): array {
        $unprocessedItems = $this->getUnprocessedItems();
        $filteredIdentifiers = $unprocessedItems->pluck($type)->filter(function ($item, $key) {
            if(!empty($item)){
                return $item;
            }
        });
        return $filteredIdentifiers->unique()->toArray();
    }

    private function helperToCreateSubjectsTitle(): array {
        $unprocessedDocumentSubjects = $this->getUnprocessedDocumentSubjects();
        return $commaeSeaparatedSubjectTitle = $unprocessedDocumentSubjects->pluck('title')->filter(function ($item, $key) {
            if(!empty($item)){
                return $item;
            }
        })->toArray();
    }

}