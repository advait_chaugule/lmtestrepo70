<?php
namespace App\Domains\CaseStandard\Jobs;

use App\Services\Api\Traits\UuidHelperTrait;
use Lucid\Foundation\Job;
use App\Data\Models\Report\FactActivityLog;
use DB;
class InsertAccessActivityLogJob extends Job
{
    use UuidHelperTrait;
    private $organizationId;
    private $ipAddress;
    private $activityEventType;
    private $activityLogTime;
    private $activityLogId;
    private $identifier;
    public function __construct($organizationId,$ipAddress,$activityEventType,$identifier){
        $this->organizationId    =  $organizationId;
        $this->identifier        =  $identifier;
        $this->ipAddress         =  $ipAddress;
        $this->activityEventType =  $activityEventType;
        $this->activityLogTime   =  date("Y-m-d H:i:s");
        $this->activityLogId     =  $this->createUniversalUniqueIdentifier();
    }

    public function handle()
    {

        $insertData = ['activity_log_id'                     => $this->activityLogId,
                       'user_id'                             => '',
                       'organization_id'                     => $this->organizationId,
                       'activity_event_type'                 => $this->activityEventType,
                       'activity_id'                         => $this->identifier,
                       'activity_event_sub_type'             => '',
                       'activity_event_sub_type_activity_id' => '',
                       'activity_package_s3_file_id'         => '',
                       'cache_activity_name'                 => '',
                       'cache_activity_subtype_activity_name'=> '',
                       'cache_user_display_name'             => '',
                       'activity_log_timestamp'              => $this->activityLogTime,
                       'ip_address'                          => $this->ipAddress,
                       ];
        FactActivityLog::create($insertData);
    }
}