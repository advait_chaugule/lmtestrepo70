<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;
use Storage;

class StoreExternalCaseJsonToFileJob extends Job
{
    private $externalCaseFile;
    private $baseFolderPathForStorage;
    private $externalCaseStorageFolder;
    private $externalFileName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($externalJsonData, $externalFileName)
    {
        $this->externalJsonData = $externalJsonData;
        $this->externalCaseStorageFolder = env("ACMT_CASE_FOLDER", "CASE_PACKAGES")."/external";
        $this->baseFolderPathForStorage = storage_path('app');
        $this->externalFileName = $externalFileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
      Storage::put($this->externalCaseStorageFolder.'/'.$this->externalFileName,$this->externalJsonData);
       
        return $this->externalCaseStorageFolder."/".$this->externalFileName;
    }
}
