<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\ArrayHelper;

class GetDataMetricsFromUploadedCaseJSonContentJob extends Job
{

    use ArrayHelper;

    private $content;
    private $parsedDataReadyToProcess;
    private $metricsData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->setContentToProcess($content);
    }

    public function setContentToProcess($content) {
        $this->content = $content;
    }

    public function getContentToProcess() {
        return $this->content;
    }

    public function setParsedDataReadyToProcess(array $data) {
        $this->parsedDataReadyToProcess = $data;
    }

    public function getParsedDataReadyToProcess(): array {
        return $this->parsedDataReadyToProcess;
    }

    public function setMetricsData(array $data) {
        $this->metricsData = $data;
    }

    public function getMetricsData(): array {
        return $this->metricsData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // convert to case json string to array
        $this->convertCaseContentJsonToArray();
        // process the data and extract metrics
        $this->processDataToExtractMetrics();
        unset($this->parsedDataReadyToProcess);
        // finally return the metrics data
        return $this->getMetricsData();
    }

    private function convertCaseContentJsonToArray() {
        $parsedData = $this->getContentToProcess();
        $this->setParsedDataReadyToProcess($parsedData);
        unset($parsedData);
    }

    private function processDataToExtractMetrics() {
        $dataToProcess = $this->getParsedDataReadyToProcess();
        $cfItemCount = count($dataToProcess['CFItems']);
        $cfAssociationsCount = array_key_exists("CFAssociations",$dataToProcess) ? count($dataToProcess['CFAssociations']) : 0;
        $cfAssociationsGroupCount = array_key_exists("CFAssociationGroupings",$dataToProcess['CFDefinitions']) ? count($dataToProcess['CFDefinitions']['CFAssociationGroupings']) : 0;
        $cfDocumentTitle = $dataToProcess['CFDocument']['title'];
        
        $metricsData = [
            "children_taxonomies_count" => $cfItemCount,
            "associations_count" => $cfAssociationsCount,
            "source_taxonomy_name" => $cfDocumentTitle,
            "associations_group_count" => $cfAssociationsGroupCount,
        ];
        $this->setMetricsData($metricsData);
    }
}
