<?php
namespace App\Domains\CaseStandard\Jobs;
use App\Data\Models\Document;
use App\Data\Models\Organization;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use DB;
class GetCaseFrameworkDocumentJob extends Job
{
    use CaseFrameworkTrait, DateHelpersTrait;

    /**
     * Declare private attributes
     */
    private $documentRepository;
    private $entityType = "cf_doc";

    private $identifier;
    private $documentDetails;

    private $metadataList;

    private $document;
    private $documentEntity;
    private $setSubjectEntity;
    private $cfRootURIEntity;
    private $subjectURI;
    private $licenseURI;
    private $orgCode;
    private $organizationCode;
    private $domainName;

    /**
     * GetCaseFrameworkDocumentJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param string $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl='')
    {
        $this->setIdentifier($identifier);
        $this->orgCode      = $orgCode;
        $this->domainName   = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo, NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        // set the document repository
        $this->documentRepository   =   $documentRepo;
        $this->nodeTypeRepository   =   $nodeTypeRepository;
        // get all relevant document data
        $this->fetchDocumentDetailsFromRepository();
        $documentDetails    =   $this->getDocumentEntity();
        if(!empty($documentDetails['document_id'])){
            //GET THE METADATA LIST FOR THE DOCUMENT
            $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($documentDetails['node_type_id']);
            $this->setMetadataList($metadataList);
            // set document details globally
            $this->setDocumentDetails($documentDetails);
            // transform to and set the Document Standard Structure
            $this->transFormToCaseStandardStructure();
            // finally return the CASE standard Document structure
            return $this->getDocument();
        }
        else{
            return false;
        }
    }

    /**
     * Public Setters and Getters method definition
     */
    public function setIdentifier(string $identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() :string {
        return $this->identifier;
    }

    public function setMetadataList($data) {
        $this->metadataList = $data;
    }

    public function getMetadataList() {
        return $this->metadataList;
    }

    public function setDocumentDetails($data) {
        $this->documentDetails = $data;
    }

    public function getDocumentDetails() {
        return $this->documentDetails;
    }

    public function setDocumentEntity(array $data) {
        $this->documentEntity = $data;
    }

    public function getDocumentEntity(): array {
        return $this->documentEntity;
    }

    public function setDocument(array $data) {
        $this->document = $data;
    }

    public function getDocument(): array {
        return $this->document;
    }

    public function getEntityType(): string {
        return $this->entityType;
    }

    public function setCFRootURIEntity(array $data){
        $this->cfRootURIEntity = $data;
    }

    public function getCFRootURIEntity(): array{
        return $this->cfRootURIEntity;
    }

    public function setSubjectEntity($data) {
        $this->setSubjectEntity = $data;
    }

    public function getSubjectEntity() {
        return $this->setSubjectEntity;
    }

    public function setSubjectURIEntity($data) {
        $this->setSubjectURIEntity = $data;
    }

    public function getSubjectURIEntity() {
        return $this->setSubjectURIEntity;
    }

    public function setLicenseURIEntity($data) {
        $this->licenseURI = $data;
    }

    public function getLicenseURIEntity() {
        return $this->licenseURI;
    }
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty( $this->organizationCode)) {
            return $this->orgCode;
        }else{
            return $this->organizationCode;
        }
    }


    //Get Document detail from Repository
    private function fetchDocumentDetailsFromRepository() {
        $identifier = $this->getIdentifier();

        $returnCollection = false;
        $fieldsToReturn = ["*"];
        $dataFetchingConditionalClause = [ "source_document_id" => $identifier, "is_deleted" => 0,'status'=>3 ];

        if($this->orgCode) {
            $getOrganizationId = $this->getOrganizationDetails($this->orgCode);
            $dataFetchingConditionalClause["organization_id"] =  $getOrganizationId['organization_id'];
            $documentDetails =    $this->documentRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $dataFetchingConditionalClause
            );
            $subjects   =   $documentDetails->subjects;
            $license    =   $documentDetails->license;
        }else{
            $documentDetails =    $this->documentRepository->findByAttributesWithSpecifiedFields(
                $returnCollection,
                $fieldsToReturn,
                $dataFetchingConditionalClause
            );
            $subjects   =   $documentDetails->subjects;
            $license    =   $documentDetails->license;
        }

        $documentEntity =   [
            "document_id"           => $documentDetails->document_id,
            "source_document_id"    => $documentDetails->source_document_id,
            "node_type_id"          => $documentDetails->node_type_id,
            "title"                 => !empty($documentDetails->title) ? $documentDetails->title  : "",
            "official_source_url"   => !empty($documentDetails->official_source_url) ? $documentDetails->official_source_url  : "",
            "language"              => !empty($documentDetails->language->short_code) ? $documentDetails->language->short_code : "",
            "adoption_status"       => !empty($documentDetails->adoption_status) ? $documentDetails->adoption_status : "",
            "notes"                 => !empty($documentDetails->notes) ? $documentDetails->notes : "",
            "publisher"             => !empty($documentDetails->publisher) ? $documentDetails->publisher : "",
            "description"           => !empty($documentDetails->description) ? $documentDetails->description : "",
            "version"               => !empty($documentDetails->version) ? $documentDetails->version : "",
            "status_start_date"     => !empty($documentDetails->status_start_date) ? $documentDetails->status_start_date : "",
            "status_end_date"       => !empty($documentDetails->status_end_date) ? $documentDetails->status_end_date : "",
            "source_license_id"     => !empty($license->source_license_id) ? $license->source_license_id : "",
            "license_title"         => !empty($license->title) ? $license->title : "",
            "license_description"   => !empty($license->description) ? $license->description : "",
            "license_text"          => !empty($license->license_text) ? $license->license_text : "",
            "creator"               => !empty($documentDetails->creator) ? $documentDetails->creator : "",
            "source_subject_id"     => !empty($subjects[0]->source_subject_id) ? $subjects[0]->source_subject_id : "",
            "subject_title"         => !empty($subjects[0]->title) ? $subjects[0]->title : "",
            "updated_at"            => $documentDetails->updated_at,
            "uri"                   => !empty($documentDetails->uri)?$documentDetails->uri:"",
            "import_type"           => !empty($documentDetails->import_type)?$documentDetails->import_type:"",
        ];

        $this->setDocumentEntity($documentEntity);
    }

    private function parseMetadataForDocument() {
        $metadataList   =   $this->getMetadataList();
        foreach($metadataList->metadata as $metadata) {
            $arrayOfInternalNameForMetadata[]   =   $metadata->internal_name;
        }

        return $arrayOfInternalNameForMetadata;
    }

    private function transFormToCaseStandardStructure() {
        // transform and set CFRootURI
        $this->transformAndSetCFRootURI();
        // transform and set Subject
        $this->transformAndSetSubjectEntity();
        // transform and set SubjectURI
        $this->transformAndSetSubjectURIEntity();
        // transform and set LicenseURI
        $this->transformAndSetLicenseURIEntity();
        // transform and set cfdocument
        $this->transformAndSetDocument();
    }

    /**
     * Set Document URI Entity
     */
    private function transformAndSetCFRootURI() {
        $document = $this->getDocumentEntity();
        $orgCode  = $this->getOrgCode();
        // parse out the CFRootURI related info
        $uriEntity = [
            "title"         =>  !empty($document['title']) ? $document['title'] : "",
            "identifier"    =>  !empty($document['source_document_id']) ? $document['source_document_id'] : "",
            "uri"           =>  ($document['import_type']==1)?str_replace('CFDocuments',  'CFPackages', $document['uri']):$this->getCaseApiUri("CFPackages", $document['source_document_id'],true,$orgCode,$this->domainName)
        ];

        // set the CFRootURI for later use
        $this->setCFRootURIEntity($uriEntity);
    }

    /**
     * Set Subject Entity
     */
    private function transformAndSetSubjectEntity() {
        //parse the active metadata list
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForDocument();
        $documentDetails                =   $this->getDocumentEntity();
        if(in_array('subject_title', $arrayOfInternalNameForMetadata) === true){
            $uriEntity = [!empty($documentDetails['subject_title']) ? $documentDetails['subject_title'] : ""];     
        }
        else {
            $uriEntity = "";
        }
        // set the SubjectEntity for later use
        $this->setSubjectEntity($uriEntity);      
    }

    /**
     * Set Subject URI Entity
     */
    private function transformAndSetSubjectURIEntity() {
        $uriEntity  =   [];

        //parse the active metadata list
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForDocument();
        $documentDetails                =   $this->getDocumentEntity();
//        echo "<pre>";
//        print_r($documentDetails);die;
        $orgCode                        =   $this->getOrgCode();
        if(in_array('subject_title', $arrayOfInternalNameForMetadata) === true){
            if($documentDetails['import_type']==2)
            {
                $uriEntity[] = [
                    "title"         => !empty($documentDetails['subject_title']) ? $documentDetails['subject_title'] : "",
                    "identifier"    => !empty($documentDetails['source_subject_id']) ? $documentDetails['source_subject_id'] : "",
                    "uri"           => !empty($documentDetails['source_subject_id']) ? $this->getCaseApiUri("CFSubjects", $documentDetails['source_subject_id'],true,$orgCode,$this->domainName) : ""
                ];
            }else if($documentDetails['import_type']==1)
            {
                $getSubject     =  $this->getSubjectDetails($documentDetails['document_id']);
                $subjectUriJson = $getSubject->source_subject_uri_object;
                $subArrUri      = json_decode($subjectUriJson);
                $uri            = $subArrUri->uri;
                $identifier     = $subArrUri->identifier;
                $uriEntity[] = [
                    "title"         => !empty($documentDetails['subject_title']) ? $documentDetails['subject_title'] : "",
                    "identifier"    => $identifier,
                    "uri"           => $uri
                ];
            }else{
                // parse out the SubjectURI related info
                $uriEntity[] = [
                    "title"         => !empty($documentDetails['subject_title']) ? $documentDetails['subject_title'] : "",
                    "identifier"    => !empty($documentDetails['source_subject_id']) ? $documentDetails['source_subject_id'] : "",
                    "uri"           => !empty($documentDetails['source_subject_id']) ? $this->getCaseApiUri("CFSubjects", $documentDetails['source_subject_id'],true,$orgCode,$this->domainName) : ""
                ];
            }


        }
        else {
            $uriEntity[] = [
                "title"         =>  "",
                "identifier"    =>  "",
                "uri"           =>  ""
            ];
        }
        // set the SubjectURI for later use
        $this->setSubjectURIEntity($uriEntity);
    }
    
    /**
     * Set License URI Entity
     */
    private function transformAndSetLicenseURIEntity() {
        
        //parse the active metadata list
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForDocument();
        $documentDetails                =   $this->getDocumentEntity();
        $orgCode                        =   $this->getOrgCode();
        if(in_array('license_title', $arrayOfInternalNameForMetadata) === true){
            if($documentDetails['import_type']==2) {
                // parse out the LicenseURI related info
                $uriEntity = [
                    "title" => !empty($documentDetails['license_title']) ? $documentDetails['license_title'] : "",
                    "identifier" => !empty($documentDetails['source_license_id']) ? $documentDetails['source_license_id'] : "",
                    "uri" => !empty($documentDetails['source_license_id']) ? $this->getCaseApiUri("CFLicenses", $documentDetails['source_license_id'], true, $orgCode, $this->domainName) : ""
                ];
            }else{
                if($documentDetails['import_type']==1) {
                    $license = $this->getLicenseDetails($documentDetails['source_license_id']);
                    $uri     = $license->uri;
                    $uriEntity = [
                        "title" => !empty($documentDetails['license_title']) ? $documentDetails['license_title'] : "",
                        "identifier" => !empty($documentDetails['source_license_id']) ? $documentDetails['source_license_id'] : "",
                        "uri"  => $uri
                    ];
                }else{
                    $uriEntity = [
                        "title" => !empty($documentDetails['license_title']) ? $documentDetails['license_title'] : "",
                        "identifier" => !empty($documentDetails['source_license_id']) ? $documentDetails['source_license_id'] : "",
                        "uri" => !empty($documentDetails['source_license_id']) ? $this->getCaseApiUri("CFLicenses", $documentDetails['source_license_id'], true, $orgCode, $this->domainName) : ""
                    ];
                }
            }
        } else {
            $uriEntity  =   [
                "title"         => "",
                "identifier"    => "",
                "uri"           => ""
            ];
        }
        // set the LicenseURI for later use
        $this->setLicenseURIEntity($uriEntity);
    }

    /**
     * Set Document Entity
     */
    private function transformAndSetDocument() {
        $itemMatchedMetadataValues  =   [];
        
        //parse the active metadata list
        $arrayOfInternalNameForMetadata =   $this->parseMetadataForDocument();
        $documentDetails                =   $this->getDocumentEntity();
        $orgCode                        =   $this->getOrgCode();
        foreach($documentDetails as $key => $documentMetadata) {
            
            if(in_array($key, $arrayOfInternalNameForMetadata) === true){
                $itemMatchedMetadataValues[$key]      =  $documentMetadata; 
            }
            else {
               $itemMatchedMetadataValues[$key]      =   "";
            }
        }

        $documentEntity = [
            "identifier"            =>  $documentDetails['source_document_id'],
            "uri"                   =>  ($documentDetails['import_type']==1)?$documentDetails['uri']:$this->getCaseApiUri("CFDocuments", $documentDetails['source_document_id'],true,$orgCode,$this->domainName),
            "creator"               =>  !empty($itemMatchedMetadataValues['creator']) ? $itemMatchedMetadataValues['creator'] : "",
            "title"                 =>  !empty($itemMatchedMetadataValues['title']) ? $itemMatchedMetadataValues['title'] : "",
            "lastChangeDateTime"    =>  $this->formatDateTimeToISO8601($documentDetails['updated_at']),
            "officialSourceURL"     =>  !empty($itemMatchedMetadataValues['official_source_url']) ? $itemMatchedMetadataValues['official_source_url'] : "",
            "publisher"             =>  !empty($itemMatchedMetadataValues['publisher']) ? $itemMatchedMetadataValues['publisher'] : "",
            "description"           =>  !empty($itemMatchedMetadataValues['description']) ? $itemMatchedMetadataValues['description'] : "",
            "subject"               =>  $this->getSubjectEntity(),
            "subjectURI"            =>  $this->getSubjectURIEntity(),
            "language"              =>  !empty($itemMatchedMetadataValues['language']) ? $itemMatchedMetadataValues['language'] : "",
            "version"               =>  !empty($itemMatchedMetadataValues['version']) ? $itemMatchedMetadataValues['version'] : "",
            "adoptionStatus"        =>  !empty($documentDetails['adoption_status']) ? $this->getSystemSpecifiedAdoptionStatusText($documentDetails['adoption_status']) : "",
            
            "licenseURI"            =>  $this->getLicenseURIEntity(),
            "notes"                 =>  !empty($itemMatchedMetadataValues['notes']) ? $itemMatchedMetadataValues['notes'] : "",
            "statusStartDate"       =>  !empty($itemMatchedMetadataValues['status_start_date']) ? $itemMatchedMetadataValues['status_start_date'] : "",
            "statusEndDate"         =>  !empty($itemMatchedMetadataValues['status_end_date']) ? $itemMatchedMetadataValues['status_end_date'] : "",
            // it might be uri object not sure $this->getCFRootURIEntity(),
            "CFPackageURI"          => $this->getCFRootURIEntity()
        ];
        $this->setDocument($documentEntity);
    }

    /**
     * @param $orgCode
     * @return mixed
     * @Funcrion getOrganizationDetails
     * @Purpose Based on org code get org id for tenant specific
     */
    public function getOrganizationDetails($orgCode)
    {
        $orgId = Organization:: select('organization_id')
            ->where('org_code',$orgCode)
            ->first();
        if($orgId) {
            $orgId = $orgId->toArray();
        }

        return $orgId ;
    }

   public function getSubjectDetails($documentId)
   {
       $subject = DB::table('document_subject')
           ->select('source_subject_uri_object')
           ->where('document_id',$documentId)
           ->get()
           ->first();
       return $subject;
   }

   public function getLicenseDetails($licenceId)
   {
       $licence = DB::table('licenses')
           ->select('uri')
           ->where('source_license_id',$licenceId)
           ->where('is_deleted',0)
           ->get()
           ->first();
       return $licence;
   }
}
