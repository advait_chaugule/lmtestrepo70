<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

class CreateCaseStandardDataJob extends Job
{

    private $hierarchialTaxonomyData;
    private $cfRootURIEntity;
    private $cfItemEntitiesFlattened = [];
    private $childrenTaxonomy = [];

    private $cfDocumentEntity;
    private $cfAssociations;
    private $cfItemTypeEntities;

    private $cfPackadgeData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->setTaxonomyHierarchy($data);
    }

    public function setTaxonomyHierarchy($data){
        $this->hierarchialTaxonomyData = $data;
    }

    public function getTaxonomyHierarchy() {
        return $this->hierarchialTaxonomyData;
    }

    public function setCFRootURIEntity(array $data){
        $this->cfRootURIEntity = $data;
    }

    public function getCFRootURIEntity(): array{
        return $this->cfRootURIEntity;
    }

    public function setCFDocumentEntity(array $data){
        $this->cfDocumentEntity = $data;
    }

    public function getCFDocumentEntity(): array{
        return $this->cfDocumentEntity;
    }

    public function getCFItemEntities(): array{
        return $this->cfItemEntitiesFlattened;
    }

    public function setCFAssociations(array $data){
        $this->cfAssociations = $data;
    }

    public function getCFAssociationEntities(): array{
        return $this->cfAssociations;
    }

    public function getChildrenTaxonomy(): array{
        return $this->childrenTaxonomy;
    }

    public function setCFItemTypeEntities(array $data){
        $this->cfItemTypeEntities = $data;
    }

    public function getCFItemTypeEntities(): array{
        return $this->cfItemTypeEntities;
    }

    public function setCFPackadgeData(array $data) {
        $this->cfPackadgeData = $data;
    }

    public function getCFPackadgeData(): array {
        return $this->cfPackadgeData;
    }

    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // parse/extract and transform according to CASE standard
        $this->runTransformers();
        // arrange data according to CASE standard 
        $this->arrangeCASEStandardData();
        // return the CASE standard data
        return $this->getCFPackadgeData();
    }

    /**
     * This method will run transformers for various entity types of CASE standard
     * After transformation, it will set relevant data
     * @return void
     */
    private function runTransformers() {
        $this->transformToCFRootURIEntity();
        $this->transformToCFDocumentEntity();
        $this->transformToCFItemEntities();
        $this->transformToCFAssociationEntities();
        $this->transformToCFItemTypeEntities();
    }

    /**
     * This method's role is to arrange data according to the CASE standard and set it
     * @return void
     */
    private function arrangeCASEStandardData() {
        $cfDocumentEntity = $this->getCFDocumentEntity();
        $cfItemEntities = $this->getCFItemEntities();
        $cfAssociationentities = $this->getCFAssociationEntities();
        $cfItemTypeEntities = $this->getCFItemTypeEntities();
        $cfPackadgeData = [
            "CFDocument" => $cfDocumentEntity,
            "CFItems" => $cfItemEntities,
            "CFAssociations" => $cfAssociationentities,
            "CFDefinitions" => [
                "CFConcepts" => [],
                "CFSubjects" => [],
                "CFLicenses" => [],
                "CFItemTypes" => $cfItemTypeEntities,
                "CFAssociationGroupings" => [],
            ]
        ];
        $this->setCFPackadgeData($cfPackadgeData);
    }

    /**
     * This method will help transforming to CFDocument entity
     *
     * @return void
     */
    private function transformToCFDocumentEntity() {
        $taxonomy = $this->getTaxonomyHierarchy();
        // parse out the cfdocument related info
        $cfDocumentEntity = [
            "uri" => $taxonomy->uri,
            "identifier" => $taxonomy->id,
            "lastChangeDateTime" => $this->formatToCaseSpecificationDateTime($taxonomy->updated_at),
            "CFPackageURI" => $this->getCFRootURIEntity(),
            "officialSourceURL" => !empty($taxonomy->official_source_url) ? $taxonomy->official_source_url : "",
            "creator" => $taxonomy->creator,
            "publisher" => !empty($taxonomy->publisher_name) ? $taxonomy->publisher_name : "",
            "title" => $taxonomy->title,
            "url_name" => !empty($taxonomy->url_name) ? $taxonomy->url_name : "",
            "version" => !empty($taxonomy->version) ? $taxonomy->version : "",
            "description" => !empty($taxonomy->description) ? $taxonomy->description : "",
            "language" => !empty($taxonomy->language_short_code) ? $taxonomy->language_short_code : "",
            "adoptionStatus" => $this->getCaseSpecificationAdoptionStatus($taxonomy->adoption_status),
            "statusStartDate" => $this->formatToCaseSpecificationDateTime($taxonomy->status_start_date, true),
            "statusEndDate" => $this->formatToCaseSpecificationDateTime($taxonomy->status_end_date, true),
            "notes" => !empty($taxonomy->note) ? $taxonomy->note : "",
        ];
        // call the method to set data
        $this->setCFDocumentEntity($cfDocumentEntity);
    }

    /**
     * This method will transform and set CFrooturientitytype
     * @return void
     */
    private function transformToCFRootURIEntity() {
        $taxonomy = $this->getTaxonomyHierarchy();
        // parse out the CFRootURI related info
        $uriEntity = [
            "title" => $taxonomy->title,
            "identifier" => $taxonomy->id,
            "uri" => $taxonomy->uri
        ];
        // set the CFRootURI for later use
        $this->setCFRootURIEntity($uriEntity);
    }

    /**
     * This is a recursive method which will be used to get all cfitem enities in a linear fashion
     * @param array $taxonomies
     * @return void
     */
    private function transformToCFItemEntities(array $taxonomies = []) {
        $taxonomies = $taxonomies===[] ? $this->getTaxonomyHierarchy()->children : $taxonomies;
        $cfItemEntities = [];
        foreach($taxonomies as $taxonomy) {
            // parse out the cfitem related info
            $cfItemEntity = [
                "uri" => $taxonomy->uri,
                "identifier" => $taxonomy->id,
                "lastChangeDateTime" => $this->formatToCaseSpecificationDateTime($taxonomy->updated_at),
                "CFDocumentURI" => $this->getCFRootURIEntity(),
                "CFItemType" => !empty($taxonomy->item_type_title) ? $taxonomy->item_type_title : "",
                "CFItemTypeURI" => $this->getCFItemTypeURIEntity($taxonomy),
                "conceptKeywords" => $this->getConceptKeywordEntity($taxonomy),
                "educationLevel" => $this->getEducationLevelEntity($taxonomy),
                "humanCodingScheme" => !empty($taxonomy->human_coding_scheme) ? $taxonomy->human_coding_scheme : "",
                "listEnumeration" => !empty($taxonomy->list_enumeration) ? $taxonomy->list_enumeration : "",
                "fullStatement" => !empty($taxonomy->full_statement) ? $taxonomy->full_statement : "",
                "abbreviatedStatement" => !empty($taxonomy->abbreviated_statement) ? $taxonomy->abbreviated_statement : "",
                "notes" => !empty($taxonomy->note) ? $taxonomy->note : "",
                "language" => !empty($taxonomy->language_short_code) ? $taxonomy->language_short_code : "",
                "statusStartDate" => $this->formatToCaseSpecificationDateTime($taxonomy->status_start_date, true),
                "statusEndDate" => $this->formatToCaseSpecificationDateTime($taxonomy->status_end_date, true),
            ];

            // push each item recursively inside a global array
            $this->cfItemEntitiesFlattened[] = $cfItemEntity;

            // also push the some other info for further logical referencing
            $cfItemEntity["parent_id"] = $taxonomy->parent_id;
            $cfItemEntity["human_coding_scheme"] = $taxonomy->human_coding_scheme;
            $cfItemEntity["updated_at"] = $taxonomy->updated_at;
            $cfItemEntity["item_type_id"]  = $taxonomy->item_type_id;
            $cfItemEntity["item_type_title"]  = $taxonomy->item_type_title;
            $cfItemEntity["item_type_uri"]  = $taxonomy->item_type_uri;
            $cfItemEntity["item_type_type_code"]  = $taxonomy->item_type_type_code;
            $cfItemEntity["item_type_hierarchy_code"]  = $taxonomy->item_type_hierarchy_code;
            $cfItemEntity["item_type_updated_at"]  = $taxonomy->item_type_updated_at;
            // create a new flattened cfitem taxonomy array with other info mentioned above
            $this->childrenTaxonomy[] = $cfItemEntity;

            if(!empty($taxonomy->children)){
                $this->transformToCFItemEntities($taxonomy->children);
            }
        }
    }

    /**
     * This method will transform CfAssociations entity and set data
     * @return void
     */
    private function transformToCFAssociationEntities() {
        $cfRootURIEntity = $this->getCFRootURIEntity();
        $childrenTaxonomy = $this->getChildrenTaxonomy();
        $cfAssociations = [];
        // loop through each taxonomy and find it's parent
        foreach($childrenTaxonomy as $taxonomy) {
            $isParentRoot = false;
            $parentTaxonomy = $this->searchForParentTaxonomy($taxonomy['parent_id']);
            // if no parent found, then compare with root
            if(empty($parentTaxonomy) && $taxonomy['parent_id']===$cfRootURIEntity["identifier"]){
                $isParentRoot = true;
                $parentTaxonomy = $cfRootURIEntity;
            }

            $originNodeURI = [
                "title" => $taxonomy['human_coding_scheme'],
                "identifier" => $taxonomy['identifier'],
                "uri" => $taxonomy['uri']
            ];

            $destinationNodeURI = [
                "title" => $isParentRoot ? $parentTaxonomy['title'] : $taxonomy['human_coding_scheme'],
                "identifier" => $parentTaxonomy['identifier'],
                "uri" => $parentTaxonomy['uri']
            ];

            $cfAssociations[] = [
                "uri" => $taxonomy['uri'],
                "identifier" => $taxonomy['identifier'],
                "lastChangeDateTime" => $this->formatToCaseSpecificationDateTime($taxonomy['updated_at']),
                "CFDocumentURI" => $cfRootURIEntity,
                "originNodeURI" => $originNodeURI,
                "associationType" => "isChildOf",
                "destinationNodeURI" => $destinationNodeURI
            ];

        }

        $this->setCFAssociations($cfAssociations);
    }

    /**
     * This method will traverse through all child taxonomies (cfitems) and 
     * extract itemtype entities if found any and finally set it
     * @return void
     */
    private function transformToCFItemTypeEntities() {
        $childrenTaxonomy = $this->getChildrenTaxonomy();
        $cfItemTypeEnities = [];
        $trackItemTypesAdded = [];
        // loop through each cfitem taxonomy parse out item type entity info 
        foreach($childrenTaxonomy as $taxonomy) {
           if(!empty($taxonomy["item_type_id"])){
                $identifier = $taxonomy["item_type_id"];
                // skip to the next item type if it has already been added
                if(in_array($identifier, $trackItemTypesAdded)){
                    continue;
                }
                if(!empty($identifier)){
                    $itemTypeEntity = [
                        "uri" => $taxonomy["item_type_uri"],
                        "identifier" => $identifier,
                        "lastChangeDateTime" => $this->formatToCaseSpecificationDateTime($taxonomy["item_type_updated_at"]),
                        "title" => !empty($taxonomy["item_type_title"]) ? $taxonomy["item_type_title"] : "",
                        "typeCode" => !empty($taxonomy["item_type_type_code"]) ? $taxonomy["item_type_type_code"] : "",
                        "hierarchyCode" => !empty($taxonomy["item_type_hierarchy_code"]) ? $taxonomy["item_type_hierarchy_code"] : ""
                    ];
                    $cfItemTypeEnities[] = $itemTypeEntity;
                    $trackItemTypesAdded[] = $identifier;
                }
           }
        }
        $this->setCFItemTypeEntities($cfItemTypeEnities);
    }

    /**
     * This method will traverse through all children taxonomy (cfitems)
     * and search for parent the parent taxonomy
     * @param string $searchIdentifier
     * @return void
     */
    private function searchForParentTaxonomy(string $searchIdentifier) {
        $parent = null;
        $searchInside = $this->getChildrenTaxonomy();
        foreach($searchInside as $taxonomy){
            if($taxonomy['identifier']===$searchIdentifier){
                $parent = $taxonomy;
                break;
            }
        }
        return $parent;
    }

    /**
     * This method will parse out the cfitemtype related info
     * @param object $taxonomy
     * @return array
     */
    private function getCFItemTypeURIEntity($taxonomy): array {
        $cfItemTypeURIEntity = [
            "title" => !empty($taxonomy->item_type_title) ? $taxonomy->item_type_title : "",
            "identifier" => !empty($taxonomy->item_type_id) ? $taxonomy->item_type_id : "",
            "uri" => !empty($taxonomy->item_type_uri) ? $taxonomy->item_type_uri : ""
        ];
        return $cfItemTypeURIEntity;
    }

    /**
     * This method will parse out the concept keywords
     * @param object $taxonomy
     * @return array
     */
    private function getConceptKeywordEntity($taxonomy): array {
        // (for now keyword only inside an array)
        $conceptKeywords = [!empty($taxonomy->concept_keyword) ? $taxonomy->concept_keyword : ""];
        return $conceptKeywords;
    }


    /**
     * This method will parse out the education level string
     * @param object $taxonomy
     * @return array
     */
    private function getEducationLevelEntity($taxonomy): array {
        // (for now string only inside an array)
        $educationLevel = [!empty($taxonomy->education_level) ? $taxonomy->education_level : ""];
        return $educationLevel;
    }

    private function formatToCaseSpecificationDateTime($dateTimeString, $dateOnly=false): string {
        $formattedString = "";
        if(!empty($dateTimeString)){
            $formattedString = $dateTimeString;
            if($dateOnly===false) {
                $dateTimeString = new \DateTime($dateTimeString);
                $formattedString = $dateTimeString->format(\DateTime::ATOM); // Updated ISO8601
            }
        }
        return $formattedString;
    }

    private function getCaseSpecificationAdoptionStatus($adoptionStatus): string {
        $systemPresetCaseAdoptionStatusMapping = [
            "private_draft" => "Private Draft",
            "draft" => "Draft", 
            "adopted" => "Adopted", 
            "deprecated" => "Deprecated"
        ];
        return !empty($systemPresetCaseAdoptionStatusMapping[$adoptionStatus]) ? 
               $systemPresetCaseAdoptionStatusMapping[$adoptionStatus] : 
                "Draft";
    }

    

    
}
