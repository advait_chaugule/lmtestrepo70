<?php
namespace App\Domains\CaseStandard\Jobs;
use Lucid\Foundation\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Data\Models\Document;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
//use App\Data\Repositories\Contracts\ItemTypeRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;



class ImportCaseStandardDataFromJsonJob extends Job
{
    use ArrayHelper, UuidHelperTrait, StringHelper, DateHelpersTrait, CaseFrameworkTrait;

    private $documentRepository;
    private $itemRepository;
    private $languageRepository;
    private $licenseRepository;
    private $subjectRepository;
    //private $itemTypeRepository;
    private $NodeTypeRepository;
    private $conceptRepository;
    private $itemAssociationRepository;
    private $associationGroupRepository;
    private $userOrganizationId;

    private $uploadedCaseJson;
    private $uploadedCaseData;
    private $uploadedCaseDocument;
    private $uploadedCaseItems;
    private $uploadedCaseAssociations;
    private $uploadedCaseConcepts;
    private $uploadedCaseSubjects;
    private $uploadedCaseLicenses;
    private $uploadedCaseItemTypes;
    private $uploadedCaseAssociationGroups;

    private $savedLicenseIdentifiers = [];
    private $savedConceptIdentifiers = [];
    //private $savedItemTypeIdentifiers = [];
    private $savedNodeTypeIdentifiers = [];
    private $savedAssociationGroupIdentifiers = [];
    private $savedSubjectIdentifiers = [];

    private $savedDocumentObject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $uploadedCaseJson)
    {
        $this->setUploadedCaseJson($uploadedCaseJson);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        //ItemTypeRepositoryInterface $itemTypeRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ConceptRepositoryInterface $conceptRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        AssociationGroupRepositoryInterface $associationGroupRepository,
        Request $request
    )
    {
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->subjectRepository = $subjectRepository;
        //$this->itemTypeRepository = $itemTypeRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->conceptRepository = $conceptRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->associationGroupRepository = $associationGroupRepository;

        $this->extractAndSetUserOrganizationIdFromRequestBody($request);

        $this->extractAndSetUserIdFromRequestBody($request);

        $this->extractAndSetCopyFromRequestBody($request);

        // convert the uploaded content to array
        $caseDataArray = $this->convertJsonStringToArray($this->getUploadedCaseJson());
        // set the case data array
        $this->setUploadedCaseData($caseDataArray);

        // start extraction of CASE relevant data viz. CFDocument, CFItems, CFAssociations, CFDefinitions
        $this->extractCaseStandardEntityData();

        // // process the extracted CASE data and start saving them to database
        $this->processAndSaveExtractedCaseStandardData();
    }

    public function setCopy(string $data) {
        $this->copy = $data;
    }

    public function getCopy(): string {
        return $this->copy;
    }

    public function setUserId(string $data) {
        $this->userId = $data;
    }

    public function getUserId(): string {
        return $this->userId;
    }

    public function setUserOrganizationId(string $data) {
        $this->userOrganizationId = $data;
    }

    public function getUserOrganizationId(): string {
        return $this->userOrganizationId;
    }

    public function setUploadedCaseJson(string $uploadedCaseJson) {
        $this->uploadedCaseJson = $uploadedCaseJson;
    }

    public function getUploadedCaseJson(): string {
        return $this->uploadedCaseJson;
    }

    public function setUploadedCaseData(array $data) {
        $this->uploadedCaseData = $data;
    }

    public function getUploadedCaseData(): array {
        return $this->uploadedCaseData;
    }

    public function setUploadedCaseDocument(array $data) {
        $this->uploadedCaseDocument = $data;
    }

    public function getUploadedCaseDocument(): array {
        return $this->uploadedCaseDocument;
    }

    public function setUploadedCaseItems(array $data) {
        $this->uploadedCaseItems = $data;
    }

    public function getUploadedCaseItems(): array {
        return $this->uploadedCaseItems;
    }

    public function setUploadedCaseAssociations(array $data) {
        $this->uploadedCaseAssociations = $data;
    }

    public function getUploadedCaseAssociations(): array {
        return $this->uploadedCaseAssociations;
    }

    public function setUploadedCaseConcepts(array $data) {
        $this->uploadedCaseConcepts = $data;
    }

    public function getUploadedCaseConcepts(): array {
        return $this->uploadedCaseConcepts;
    }

    public function setUploadedCaseSubjects(array $data) {
        $this->uploadedCaseSubjects = $data;
    }

    public function getUploadedCaseSubjects(): array {
        return $this->uploadedCaseSubjects;
    }

    public function setUploadedCaseLicenses(array $data) {
        $this->uploadedCaseLicenses = $data;
    }

    public function getUploadedCaseLicenses(): array {
        return $this->uploadedCaseLicenses;
    }

    public function setUploadedCaseItemTypes(array $data) {
        $this->uploadedCaseItemTypes = $data;
    }

    public function getUploadedCaseItemTypes(): array {
        return $this->uploadedCaseItemTypes;
    }

    public function setUploadedCaseAssociationGroups(array $data) {
        $this->uploadedCaseAssociationGroups = $data;
    }

    public function getUploadedCaseAssociationGroups(): array {
        return $this->uploadedCaseAssociationGroups;
    }

    public function setSavedDocumentObject(Document $document) {
        $this->savedDocumentObject = $document;
    }

    public function getSavedDocumentObject() {
        return $this->savedDocumentObject;
    }

    /**
     * Store each saved identifier of any of the type
     * viz. license, concept, itemType, associationGroup, subject
     */
    public function setSavedIdentifiers(string $identifier, string $type) {
        switch ($type) {
            case 'license':
                $this->savedLicenseIdentifiers[$identifier] = $identifier;
                break;
            case 'concept':
                $this->savedConceptIdentifiers[$identifier] = $identifier;
                break;
            case 'itemType':
                $this->savedNodeTypeIdentifiers[$identifier] = $identifier; //savedItemTypeIdentifiers
                break;
            case 'associationGroup':
                $this->savedAssociationGroupIdentifiers[$identifier] = $identifier;
                break;
            case 'subject':
            
                $this->savedSubjectIdentifiers[$identifier] = $identifier;
                break;
            default:
                break;
        }
    }

    /**
     * Get set identifiers of any of the type
     * viz. license, concept, itemType, associationGroup
     */
    public function getSavedIdentifiers(string $type): array {
        switch ($type) {
            case 'license':
                return $this->savedLicenseIdentifiers;
                break;
            case 'concept':
                return $this->savedConceptIdentifiers;
                break;
            case 'itemType':
                return $this->savedNodeTypeIdentifiers; //savedItemTypeIdentifiers
                break;
            case 'associationGroup':
                return  $this->savedAssociationGroupIdentifiers;
                break;
            case 'subject':
                return $this->savedSubjectIdentifiers;;
                break;
            default:
                return [];
                break;
        }
    }

    /**
     * This method will traverse through case data and extract all relevant entity data
     * @return void
     */
    private function extractCaseStandardEntityData() {
            $this->extractCaseDocument();
            $this->extractCaseItems();
            $this->extractCaseAssociations();
            $this->extractCaseConcepts();
            $this->extractCaseSubjects();
            $this->extractCaseLicenses();
            $this->extractCaseItemTypes();
            $this->extractCaseAssociationGroups();
    }

    private function processAndSaveExtractedCaseStandardData(){
        $this->processAndSaveCaseDocumentData();
        $this->processAndSaveCaseItemData();
        $this->processAndSaveCaseAssociations();
        $this->processAndSaveCaseDefinitions();
    }

    private function extractCaseDocument() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCFDocument = $caseData["CFDocument"];
        $this->setUploadedCaseDocument($uploadedCFDocument);
    }

    private function extractCaseItems() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseItems = !empty($caseData["CFItems"]) ? $caseData["CFItems"] : [];
        $this->setUploadedCaseItems($uploadedCaseItems);
    }

    private function extractCaseAssociations() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseAssociations = !empty($caseData["CFAssociations"]) ? $caseData["CFAssociations"] : [];
        $this->setUploadedCaseAssociations($uploadedCaseAssociations);
    }

    private function extractCaseConcepts() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseConcepts = !empty($caseData['CFDefinitions']["CFConcepts"]) ? $caseData['CFDefinitions']["CFConcepts"] : [];
        $this->setUploadedCaseConcepts($uploadedCaseConcepts);
    }

    private function extractCaseSubjects() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseSubjects = !empty($caseData['CFDefinitions']["CFSubjects"]) ? $caseData['CFDefinitions']["CFSubjects"] : [];
        $this->setUploadedCaseSubjects($uploadedCaseSubjects);
    }

    private function extractCaseLicenses() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseLicenses = !empty($caseData["CFDefinitions"]["CFLicenses"]) ? $caseData["CFDefinitions"]["CFLicenses"] : [];
        $this->setUploadedCaseLicenses($uploadedCaseLicenses);
    }

    private function extractCaseItemTypes() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseItemTypes = !empty($caseData["CFDefinitions"]["CFItemTypes"]) ? $caseData["CFDefinitions"]["CFItemTypes"] : [];
        $this->setUploadedCaseItemTypes($uploadedCaseItemTypes);
    }

    private function extractCaseAssociationGroups() {
        $caseData = $this->getUploadedCaseData();
        $uploadedCaseAssociationGroups = !empty($caseData["CFDefinitions"]["CFAssociationGroupings"]) ? 
                                $caseData["CFDefinitions"]["CFAssociationGroupings"] : 
                                [];
        $this->setUploadedCaseAssociationGroups($uploadedCaseAssociationGroups);
    }

    private function processAndSaveCaseDocumentData() {
        $uploadedCFDocument = $this->getUploadedCaseDocument();
        $currentDateTime = $this->createDateTime();
        $documentId = $this->createUniversalUniqueIdentifier();
        if($this->getCopy() == 0){
            $sourceDocumentId = $uploadedCFDocument["identifier"];
            $title = !empty($uploadedCFDocument["title"]) ? $uploadedCFDocument["title"] : "";
        }else{
            $sourceDocumentId = $documentId;
            $title = !empty($uploadedCFDocument["title"]) ? $uploadedCFDocument["title"] : "";
            $title = $title." ".date("Y-m-d H:i:s");
        }       
        
        $officialSourceUrl = !empty($uploadedCFDocument["officialSourceURL"]) ? $uploadedCFDocument["officialSourceURL"] : "";

        $adoptionStatus = !empty($uploadedCFDocument["adoptionStatus"]) ? 
                            $this->getSystemSpecifiedAdoptionStatusNumber($uploadedCFDocument["adoptionStatus"]) : 
                            0;

        $notes = !empty($uploadedCFDocument["notes"]) ? $uploadedCFDocument["notes"] : "";
        $publisher =  !empty($uploadedCFDocument["publisher"]) ? $uploadedCFDocument["publisher"] : "";
        $description = !empty($uploadedCFDocument["description"]) ? $uploadedCFDocument["description"] : "";
        $version = !empty($uploadedCFDocument["version"]) ? $uploadedCFDocument["version"] : "";
        $statusStartDate = !empty($uploadedCFDocument["statusStartDate"]) ? $uploadedCFDocument["statusStartDate"] : null;
        $statusEndDate = !empty($uploadedCFDocument["statusEndDate"]) ? $uploadedCFDocument["statusEndDate"] : null;
        $creator = !empty($uploadedCFDocument["creator"]) ? $uploadedCFDocument["creator"] : "";
        $urlName = !empty($uploadedCFDocument["url_name"]) ? $uploadedCFDocument["url_name"] : "";

        $languageId =  !empty($uploadedCFDocument["language"]) ? 
                          $this->createOrUpdateLanguage($uploadedCFDocument["language"]) : 
                          "";

        $licenseId = !empty($uploadedCFDocument['licenseURI']['identifier']) ? 
                        $this->createOrUpdateLicense($uploadedCFDocument['licenseURI']) : 
                        "";
        
        $userOrganizationId = $this->getUserOrganizationId();
        $nodeTpyeId = $this->findNodetypeID();
        $userId = $this->getUserId();

        $createdAt = $currentDateTime;

        $updatedAt = !empty($uploadedCFDocument["lastChangeDateTime"]) ? 
                        $this->formatDateTime($uploadedCFDocument["lastChangeDateTime"]) : 
                        null;
        //dd($this->getCopy());
        $data = [
            "document_id" => $documentId,
            "title" => $title,
            "official_source_url" => $officialSourceUrl,
            "adoption_status" => $adoptionStatus,
            "notes" => $notes,
            "publisher" =>  $publisher,
            "description" => $description,
            "version" => $version,
            "status_start_date" => $statusStartDate,
            "status_end_date" => $statusEndDate,
            "creator" => $creator,
            "url_name" => $urlName,
            "language_id" =>  $languageId,
            "license_id" => $licenseId,
            "organization_id" => $userOrganizationId,
            "created_at" => $createdAt,
            "updated_at" => $updatedAt,
            "source_document_id"=>$sourceDocumentId,
            "node_type_id"=>$nodeTpyeId,
            "updated_by" => $userId,
            "created_by" => $userId
        ];
        

        $document = $this->documentRepository->saveData($data);

        $this->setSavedDocumentObject($document);
    }

    private function processAndSaveCaseItemData() {
        $uploadedCaseItems = $this->getUploadedCaseItems();
        
        $uploadedCaseDocument = $this->getUploadedCaseDocument();
        $currentDateTime = $this->createDateTime();
        $userOrganizationId = $this->getUserOrganizationId();
        $documentId = $this->getSavedDocumentObject()->document_id;
        
        $userId = $this->getUserId();
        $itemsToSave = [];
        foreach($uploadedCaseItems as $data) {
            
            $item_id = $this->createUniversalUniqueIdentifier();
            if($this->getCopy() == 0){
                $identifier = $data['identifier'];
            }else{
                $identifier = $item_id."||".$data['identifier'];
            }
            $parentId = $this->getItemParentId($identifier);
            
            if(empty($parentId)){
                $parentId = $documentId;
            }

            $fullStatement = !empty($data["fullStatement"]) ? $data["fullStatement"] : "";
            $alternativeLabel = !empty($data["alternativeLabel"]) ? $data["alternativeLabel"] : "";
            $itemTypeId = !empty($data["CFItemTypeURI"]) ? 
                            $this->createOrUpdateItemType($data["CFItemTypeURI"]) : 
                            $this->getDefaultNodeTypeId();
            $humanCodingScheme = !empty($data["humanCodingScheme"]) ? $data["humanCodingScheme"] : "";
            $listEnumeration = !empty($data["listEnumeration"]) ? $data["listEnumeration"] : "";
            $abbreviatedStatement = !empty($data["abbreviatedStatement"]) ? $data["abbreviatedStatement"] : "";
            $conceptId = !empty($data["conceptKeywordsURI"]) ? 
                         $this->createOrUpdateConcept($data["conceptKeywordsURI"]) : 
                         "";
            $notes = !empty($data["notes"]) ? $data["notes"] : "";
            $languageId =  !empty($data["language"]) ? 
                              $this->createOrUpdateLanguage($data["language"]) : 
                              "";
            $educationLevel = !empty($data["educationLevel"]) ? 
                              $this->arrayContentToCommaeSeparatedString($data["educationLevel"]) :
                              "";
            $licenseId = !empty($data['licenseURI']['identifier']) ? 
                         $this->createOrUpdateLicense($data['licenseURI']) : 
                         "";
            $statusStartDate = !empty($data["statusStartDate"]) ? $data["statusStartDate"] : null;
            $statusEndDate = !empty($data["statusEndDate"]) ? $data["statusEndDate"] : null;
            $updatedAt = !empty($data["lastChangeDateTime"]) ? 
                            $this->formatDateTime($data["lastChangeDateTime"]) : 
                            null;
            $itemsToSave[]= [
                "item_id" => $item_id,
                "parent_id" => $parentId,
                "document_id" => $documentId,
                "full_statement" => $fullStatement,
                "alternative_label" => $alternativeLabel,
                "human_coding_scheme" => $humanCodingScheme,
                "list_enumeration" => $listEnumeration,
                "abbreviated_statement" => $abbreviatedStatement,
                "notes" => $notes,
                "node_type_id" => $itemTypeId,
                "concept_id" => $conceptId,
                "language_id" => $languageId,
                "education_level" => $educationLevel,
                "license_id" => $licenseId,
                "status_start_date" => $statusStartDate,
                "status_end_date" => $statusEndDate,
                "created_at" => $currentDateTime,
                "updated_at" => $updatedAt,
                "organization_id" => $userOrganizationId,
                "source_item_id" => $identifier,
                "updated_by" => $userId,
            ];
           
        }
        
        if(!empty($itemsToSave)){
            $this->itemRepository->saveMultiple($itemsToSave);
        }
        foreach($uploadedCaseItems as $data) {
            if($this->getCopy() == 0){
                $identifier = $data['identifier'];
            }else{
                $identifier = "||".$data['identifier'];
            }
            $parentId = $this->getItemParentId($identifier);
            if(empty($parentId)){
                $parentId = $documentId; 
            }
            $this->itemRepository->updateItemParentId($identifier, $parentId);
        }
        
    }

    

    private function processAndSaveCaseAssociations() {
        $uploadedCaseAssociations = $this->getUploadedCaseAssociations();
        $uploadedCaseDocument = $this->getUploadedCaseDocument();
        $currentDateTime = $this->createDateTime();
        $caseAssociationsToSave = [];
        $userOrganizationId = $this->getUserOrganizationId();
        $this->itemAssociationRepository->saveMultiple($caseAssociationsToSave);
        $documentId = $this->getSavedDocumentObject()->document_id;
        foreach($uploadedCaseAssociations as $association) {
            $itemAssociationId = $this->createUniversalUniqueIdentifier();
            if($this->getCopy() == 0){
                $sourceItemAssociationId = $association['identifier'];
                $originNodeId = $this->getItemId($association["originNodeURI"]['identifier']);
                $destinationNodeId = isset($association["destinationNodeURI"]['identifier']) ? $this->getItemId($association["destinationNodeURI"]['identifier']) : ""; 
                if($destinationNodeId == ""){
                    $destinationNodeId = $documentId;
                }
            }else{
                $sourceItemAssociationId = $itemAssociationId;
                $originNodeId = $this->getItemId("||".$association["originNodeURI"]['identifier']);
                $destinationNodeId = isset($association["destinationNodeURI"]['identifier']) ? $this->getItemId("||".$association["destinationNodeURI"]['identifier']) : ""; 
                if($destinationNodeId == ""){
                    $destinationNodeId = $documentId;
                }
            }
            $associationGroupId = !empty($association["CFAssociationGroupingURI"]) ? 
                                    $this->createOrUpdateAssociationGroup($association["CFAssociationGroupingURI"]) : 
                                    "";
            $associationType = $this->getSystemSpecifiedAssociationTypeNumber($association["associationType"]);
            
            $sequenceNumber = isset($association["sequenceNumber"]) ? $association["sequenceNumber"] : 0;
            
            $updatedAt = !empty($association["lastChangeDateTime"]) ?
                         $this->formatDateTime($association["lastChangeDateTime"]) : 
                         null;
            if($associationType == 4){
                $caseAssociationsToSave[] = [
                    "item_association_id" => $itemAssociationId,
                    "association_type" => $associationType,
                    "document_id" => $documentId,
                    "association_group_id" => $associationGroupId,
                    "origin_node_id" => $originNodeId,
                    "destination_node_id" => "",
                    "sequence_number" => $sequenceNumber,
                    "external_node_title" =>$association["originNodeURI"]['title'],
                    "external_node_url" =>$association["originNodeURI"]['uri'],
                    "created_at" => $currentDateTime,
                    "updated_at" => $updatedAt,
                    "source_item_association_id" => $sourceItemAssociationId,
                ];
            }else{          
                $caseAssociationsToSave[] = [
                    "item_association_id" => $itemAssociationId,
                    "association_type" => $associationType,
                    "document_id" => $documentId,
                    "association_group_id" => $associationGroupId,
                    "origin_node_id" => $originNodeId,
                    "destination_node_id" => $destinationNodeId,
                    "sequence_number" => $sequenceNumber,
                    "external_node_title" =>"",
                    "external_node_url" =>"",
                    "created_at" => $currentDateTime,
                    "updated_at" => $updatedAt,
                    "source_item_association_id" => $sourceItemAssociationId,
                ];
            }
            if($this->getCopy() == 1){
                $itemAssociationId_extra = $this->createUniversalUniqueIdentifier();
                $caseAssociationsToSave[] = [
                    "item_association_id" => $itemAssociationId_extra,
                    "association_type" => 4,
                    "document_id" => $documentId,
                    "association_group_id" => $associationGroupId,
                    "origin_node_id" => $originNodeId,
                    "destination_node_id" => "",
                    "sequence_number" => $sequenceNumber+1,
                    "external_node_title" =>$association["originNodeURI"]['title'],
                    "external_node_url" =>$association["originNodeURI"]['uri'],
                    "created_at" => $currentDateTime,
                    "updated_at" => $updatedAt,
                    "source_item_association_id" => $itemAssociationId_extra,
                ];                
                //$this->itemAssociationRepository->saveData($caseAssociationsToSave_extra);
            }
        }
        if(!empty($caseAssociationsToSave)){
            $this->itemAssociationRepository->saveMultiple($caseAssociationsToSave);
        }
        if($this->getCopy() == 1){
            $this->itemRepository->updateSourceId($userOrganizationId, $documentId); 
        }    
    }
    
    private function processAndSaveCaseDefinitions() {
        $uploadedConcepts = $this->getUploadedCaseConcepts();
        $uploadedSubjects = $this->getUploadedCaseSubjects();
        $uploadedLicenses = $this->getUploadedCaseLicenses();
        $uploadedItemTypes = $this->getUploadedCaseItemTypes();
        $uploadedAssociationGroups = $this->getUploadedCaseAssociationGroups();

        foreach($uploadedConcepts as $conceptToSave) {
            $this->createOrUpdateConcept($conceptToSave);
        }

        foreach($uploadedSubjects as $subjectToSave) {
            $this->createOrUpdateSubject($subjectToSave);
        }

        foreach($uploadedLicenses as $licenseToSave) {
            $this->createOrUpdateLicense($licenseToSave);
        }

        foreach($uploadedItemTypes as $itemTypeToSave) {
            $this->createOrUpdateItemType($itemTypeToSave);
        }

        foreach($uploadedAssociationGroups as $associationGroupToSave) {
            $this->createOrUpdateAssociationGroup($associationGroupToSave);
        }
    }

    private function createOrUpdateLanguage(string $shortCode): string {
        $searchValue = trim(strtolower($shortCode));
        $language = $this->languageRepository->findBy("short_code", $searchValue);
        if(empty($language)){
            $data = [
                "language_id" => $this->createUniversalUniqueIdentifier(), 
                "name" => $searchValue, 
                "short_code" => $searchValue
            ];
            $language = $this->languageRepository->saveData($data);
        }
        return $language->language_id;
    }

    private function createOrUpdateLicense(array $licenseToSave): string {
        $licenseIdentifier = $licenseToSave["identifier"];
        $organization_id = $this->getUserOrganizationId();
        if($this->identifierNotSaved($licenseIdentifier, "license")){
            $attributes = ['source_license_id' => $licenseIdentifier, 'organization_id' => $organization_id];
            $itemType = $this->licenseRepository->findByAttributes($attributes)->first();
            //$license = $this->licenseRepository->findBy("source_license_id", $licenseIdentifier);
            if(empty($license)){
                $licenseDataFromUploadedContent = $this->searchForUploadedLicense($licenseIdentifier);
                $licenseDataFromUploadedContent = !empty($licenseDataFromUploadedContent) ? $licenseDataFromUploadedContent : $licenseToSave;
                $licenseId = $this->createUniversalUniqueIdentifier();
                if($this->getCopy() == 0){
                    $sourceLicenseId = $licenseDataFromUploadedContent['identifier'];
                }else{
                    $sourceLicenseId = $licenseId;
                }
                
                $data = [
                    "license_id" => $licenseId, 
                    "organization_id" => $organization_id,
                    "title" => !empty($licenseDataFromUploadedContent['title']) ? $licenseDataFromUploadedContent['title'] : "",
                    "description" => !empty($licenseDataFromUploadedContent['description']) ? $licenseDataFromUploadedContent['description'] : "",
                    "text" => !empty($licenseDataFromUploadedContent['licenseText']) ? $licenseDataFromUploadedContent['licenseText'] : "",
                    "updated_at" => !empty($licenseDataFromUploadedContent['lastChangeDateTime']) ? 
                                    $this->formatDateTime($licenseDataFromUploadedContent['lastChangeDateTime'] ) : 
                                    null,
                    "source_license_id" => $sourceLicenseId,

                ];
                $license = $this->licenseRepository->saveData($data);
    
                // store the saved object's identifier in memory
                $this->setSavedIdentifiers($license->license_id, "license");
            }
        }
        return $license->license_id;
    }

    private function createOrUpdateItemType(array $itemTypeToSave): string {
        
        $itemTypeTypeCode = $itemTypeToSave["title"];
        $organization_id = $this->getUserOrganizationId();
        if($this->identifierNotSaved($itemTypeTypeCode, "nodeType")){
            $attributes = ['type_code' => $itemTypeTypeCode, 'organization_id' => $organization_id];
            $itemType = $this->nodeTypeRepository->findByAttributes($attributes)->first();
            
            if(empty($itemType)){
                if(!empty($itemType )){
                    $itemTypeDataFromUploadedContent = $this->searchForUploadedItemType($itemType);
                }
                $itemTypeDataFromUploadedContent = !empty($itemTypeDataFromUploadedContent) ? $itemTypeDataFromUploadedContent : $itemTypeToSave;
                $itemTypeId = $this->createUniversalUniqueIdentifier();
                if($this->getCopy() == 0){
                    $sourceItemTypeId = $itemTypeDataFromUploadedContent['identifier'];
                }else{
                    $sourceItemTypeId = $itemTypeId;
                }
                
                $data = [
                    "node_type_id" => $itemTypeId, 
                    "title" => !empty($itemTypeDataFromUploadedContent['title']) ? $itemTypeDataFromUploadedContent['title'] : "",
                    "organization_id" => $organization_id,
                    "description" => !empty($itemTypeDataFromUploadedContent['description']) ? $itemTypeDataFromUploadedContent['description'] : "",
                    "hierarchy_code" => !empty($itemTypeDataFromUploadedContent['hierarchyCode']) ? $itemTypeDataFromUploadedContent['hierarchyCode'] : "",
                    "type_code" => !empty($itemTypeDataFromUploadedContent['typeCode']) ? $itemTypeDataFromUploadedContent['typeCode'] : $itemTypeDataFromUploadedContent['title'],
                    "updated_at" => !empty($itemTypeDataFromUploadedContent['lastChangeDateTime']) ? 
                                    $this->formatDateTime($itemTypeDataFromUploadedContent['lastChangeDateTime'] ) : 
                                    null,
                    "source_node_type_id" => $itemTypeToSave["identifier"],                 
                ];
                
                $itemType = $this->nodeTypeRepository->saveData($data);

                // store the saved object's identifier in memory
                $this->setSavedIdentifiers($itemType->node_type_id, "itemType");
            }
        }
       
        return $itemType->node_type_id;
    }

    private function createOrUpdateConcept(array $conceptToSave): string {
        $conceptIdentifier = $conceptToSave["identifier"];
        $organization_id = $this->getUserOrganizationId();
        if($this->identifierNotSaved($conceptIdentifier, "concept")){
            $attributes = ['source_concept_id' => $conceptIdentifier, 'organization_id' => $organization_id];
            $concept = $this->conceptRepository->findByAttributes($attributes)->first();
            if(empty($concept)){
                $conceptId = $this->createUniversalUniqueIdentifier();
                $conceptDataFromUploadedContent = $this->searchForUploadedConcept($conceptIdentifier);
                $conceptDataFromUploadedContent = !empty($conceptDataFromUploadedContent) ? $conceptDataFromUploadedContent : $conceptToSave;
                
                if($this->getCopy() == 0){
                    $sourceConceptId = $conceptDataFromUploadedContent['identifier'];
                }else{
                    $sourceConceptId = $conceptId;
                }
                
                $data = [
                    "concept_id" => $conceptId,
                    "organization_id" => $organization_id,
                    "keywords" => !empty($conceptDataFromUploadedContent['keywords']) ? $conceptDataFromUploadedContent['keywords'] : "",
                    "title" => !empty($conceptDataFromUploadedContent['title']) ? $conceptDataFromUploadedContent['title'] : "",
                    "description" => !empty($conceptDataFromUploadedContent['description']) ? $conceptDataFromUploadedContent['description'] : "",
                    "hierarchy_code" => !empty($conceptDataFromUploadedContent['hierarchyCode']) ? $conceptDataFromUploadedContent['hierarchyCode'] : "",
                    "updated_at" => !empty($conceptDataFromUploadedContent['lastChangeDateTime']) ? 
                                    $this->formatDateTime($conceptDataFromUploadedContent['lastChangeDateTime'] ) : 
                                    null,
                    "source_concept_id" => $sourceConceptId,     
                ];
                $concept = $this->conceptRepository->saveData($data);
                
                // store the saved object's identifier in memory
                $this->setSavedIdentifiers($concept->concept_id, "concept");
            }
        }
        return $concept->concept_id;
    }

    private function createOrUpdateAssociationGroup(array $associationGroupToSave): string {
        $associationGroupIdentifier = $associationGroupToSave["identifier"];
        $organization_id = $this->getUserOrganizationId();
        if($this->identifierNotSaved($associationGroupIdentifier, "associationGroup")){
            $attributes = ['source_association_group_id' => $associationGroupIdentifier, 'organization_id' => $organization_id];
            $associationGroup = $this->associationGroupRepository->findByAttributes($attributes)->first();
            //$associationGroup = $this->associationGroupRepository->findBy("source_association_group_id", $associationGroupIdentifier);
            if(empty($associationGroup)){
                $associationGroupDataFromUploadedContent = $this->searchForUploadedAssociationGroup($associationGroupIdentifier);
                $associationGroupDataFromUploadedContent = !empty($associationGroupDataFromUploadedContent) ? $associationGroupDataFromUploadedContent : $associationGroupToSave;
                $associationGroupId = $this->createUniversalUniqueIdentifier();
                if($this->getCopy() == 0){
                    $sourceAssociationGroupId = $associationGroupDataFromUploadedContent['identifier'];
                }else{
                    $sourceAssociationGroupId = $associationGroupId;
                }
                
                $data = [
                    "association_group_id" => $associationGroupId,
                    "organization_id" => $organization_id,
                    "title" => !empty($associationGroupDataFromUploadedContent['title']) ? $associationGroupDataFromUploadedContent['title'] : "",
                    "description" => !empty($associationGroupDataFromUploadedContent['description']) ? $associationGroupDataFromUploadedContent['description'] : "",
                    "updated_at" => !empty($associationGroupDataFromUploadedContent['lastChangeDateTime']) ? 
                                    $this->formatDateTime($associationGroupDataFromUploadedContent['lastChangeDateTime'] ) : 
                                    null,
                    "source_association_group_id" => $sourceAssociationGroupId
                ];
                $associationGroup = $this->associationGroupRepository->saveData($data);

                // store the saved object's identifier in memory
                $this->setSavedIdentifiers($associationGroup->association_group_id, "associationGroup");
            }
        }
        return $associationGroup->association_group_id;
    }

    private function createOrUpdateSubject(array $subjectToSave): string {
        $subjectIdentifier = $subjectToSave["identifier"];
        $organization_id = $this->getUserOrganizationId();
        if($this->identifierNotSaved($subjectIdentifier, "subject")){
            //$subject = $this->subjectRepository->findBy("source_subject_id", $subjectIdentifier);
            $attributes = ['source_subject_id' => $subjectIdentifier, 'organization_id' => $organization_id];
            $subject = $this->subjectRepository->findByAttributes($attributes)->first();
            if(empty($subject)){
                $subjectDataFromUploadedContent = $this->searchForUploadedSubject($subjectIdentifier);
                $subjectDataFromUploadedContent = !empty($subjectDataFromUploadedContent) ? $subjectDataFromUploadedContent : $subjectToSave;
                $subjectId = $this->createUniversalUniqueIdentifier();
                if($this->getCopy() == 0){
                    $sourceAssociationGroupId = $subjectIdentifier;
                }else{
                    $sourceAssociationGroupId = $subjectId;
                }
                
                $data = [
                    "subject_id" => $subjectId, 
                    "organization_id" => $organization_id,
                    "title" => !empty($subjectDataFromUploadedContent['title']) ? $subjectDataFromUploadedContent['title'] : "",
                    "hierarchy_code" => !empty($subjectDataFromUploadedContent['hierarchyCode']) ? $subjectDataFromUploadedContent['hierarchyCode'] : "",
                    "description" => !empty($subjectDataFromUploadedContent['description']) ? $subjectDataFromUploadedContent['description'] : "",
                    "updated_at" => !empty($subjectDataFromUploadedContent['lastChangeDateTime']) ? 
                                    $this->formatDateTime($subjectDataFromUploadedContent['lastChangeDateTime'] ) : 
                                    null,
                    "source_subject_id" => $sourceAssociationGroupId, 
                ];
                
                $subject = $this->subjectRepository->saveData($data);
               
                // store the saved object's identifier in memory
                $this->setSavedIdentifiers($subjectId, "subject");
                
                $this->saveAndMapSubjectsWithDocument($subjectId);
                
            }
        }
        return $subjectIdentifier;
    }

    private function getDefaultNodeTypeId() {
        $organizationId =   $this->getUserOrganizationId();

        $attributes     =   ['title'    =>  'Default', 'organization_id'    =>  $organizationId];

        $defaultNodeType   =   $this->nodeTypeRepository->findByAttributes($attributes);

        return $defaultNodeType[0]->node_type_id;
    }
    
    private function searchForUploadedLicense(string $searchForLicenseIdentifier): array {
        $uploadedLicenses = $this->getUploadedCaseLicenses();
        $licenseFound = [];
        foreach($uploadedLicenses as $license){
            if($license['identifier']===$searchForLicenseIdentifier){
                $licenseFound = $license;
                break;
            }
        }
        return $licenseFound;
    }
    
    private function searchForUploadedItemType(string $searchForItemTypeIdentifier): array {
        $uploadedItemTypes = $this->getUploadedCaseItemTypes();
        $itemTypeFound = [];
        foreach($uploadedItemTypes as $itemType){
            if($itemType['identifier']===$searchForItemTypeIdentifier){
                $itemTypeFound = $itemType;
                break;
            }
        }
        return $itemTypeFound;
    }

    private function searchForUploadedConcept(string $searchForConceptIdentifier): array {
        $uploadedConcepts = $this->getUploadedCaseConcepts();
        $conceptFound = [];
        foreach($uploadedConcepts as $concept){
            if($concept['identifier']===$searchForConceptIdentifier){
                $conceptFound = $concept;
                break;
            }
        }
        return $conceptFound;
    }

    private function searchForUploadedAssociationGroup(string $searchForAssociationGroupIdentifier): array {
        $uploadedAssociationGroups = $this->getUploadedCaseAssociationGroups();
        $associationGroupFound = [];
        foreach($uploadedAssociationGroups as $associationGroup){
            if($associationGroup['identifier']===$searchForAssociationGroupIdentifier){
                $associationGroupFound = $associationGroup;
                break;
            }
        }
        return $associationGroupFound;
    }

    private function searchForUploadedSubject(string $searchForSubjectIdentifier): array {
        $uploadedSubjects = $this->getUploadedCaseSubjects();
        $subjectFound = [];
        foreach($uploadedSubjects as $subject){
            if($subject['identifier']===$searchForSubjectIdentifier){
                $subjectFound = $subject;
                break;
            }
        }
        return $subjectFound;
    }

    private function getItemParentId(string $identifier) {
        $caseAssociations = $this->getUploadedCaseAssociations();
        $parentId = "";
        $sourceParentId = "";
        foreach($caseAssociations as $associationEntity){
            if($associationEntity['originNodeURI']['identifier']==$identifier && $associationEntity["associationType"]=="isChildOf"){
                $sourceParentId = !empty($associationEntity["destinationNodeURI"]["identifier"]) ? $associationEntity["destinationNodeURI"]["identifier"] : "";
                break;
            }
        }
       
        if($sourceParentId != "") {
            //$parentId = $this->itemRepository->findItemID($sourceParentId);
            $organizationId = $this->getUserOrganizationId();
            $itemIds = $this->itemRepository->findItemId($sourceParentId,$organizationId);
            $itemId = !empty($itemIds) ? $itemIds[0]['item_id'] : "";
            if($itemId == ""){
                $identifier = str_replace("||","",$identifier);
                $itemIds = $this->documentRepository->findBy("source_document_id", $sourceParentId);
                $itemId = !empty($itemIds) ? $itemIds->document_id : "";
            }    
            return $itemId;        
        }
       
    }

    private function identifierNotSaved(string $identifier, string $type): bool {
        $savedIdentifiers = $this->getSavedIdentifiers($type);
        return !in_array($identifier, $savedIdentifiers);
    }

    private function saveAndMapSubjectsWithDocument($subjects) {
        $document = $this->getSavedDocumentObject();
       
            //$savedSubjectIdentifier = $this->createOrUpdateSubject($subjectToSave);
            $this->documentRepository->mapDocumentWithSubject($document, $subjects);
        
    }

    private function extractAndSetUserOrganizationIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userOrganizationId = $userPerformingAction["organization_id"];
        $this->setUserOrganizationId($userOrganizationId);
    }

    private function extractAndSetUserIdFromRequestBody($request){
        $userPerformingAction = $request->input("auth_user");
        $userId = $userPerformingAction["user_id"];
        $this->setUserId($userId);
    }

    private function extractAndSetCopyFromRequestBody($request){
        if($request->input('clone')){
            $copy = $request->input('clone');
        }else{
            $copy = 0;
        }   
        $this->setCopy($copy);
    }

    private function getItemId($identifier) {
        $organizationId = $this->getUserOrganizationId();
        
        $itemIds = $this->itemRepository->findItemId($identifier,$organizationId);
        $itemId = !empty($itemIds) ? $itemIds[0]['item_id'] : "";
        return $itemId;
    }

    private function findNodetypeID(){
        $organizationId = $this->getUserOrganizationId();
        $findBy = ['organization_id'=>$organizationId, 'type_code'=>'Document'];
        $nodeTypeIds = $this->nodeTypeRepository->findByAttributes($findBy)->first();
        
        return $nodeTypeIds->node_type_id;
    }
}
