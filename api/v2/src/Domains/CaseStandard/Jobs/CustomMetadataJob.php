<?php
namespace App\Domains\CaseStandard\Jobs;
use App\Data\Models\Document;
use App\Data\Models\DocumentMetadata;
use App\Data\Models\ItemAssociation;
use App\Data\Models\ItemMetadata;
use App\Data\Models\Metadata;
use App\Data\Models\NodeType;
use App\Data\Models\Organization;
use Lucid\Foundation\Job;
use App\Data\Models\Item;
use App\Data\Models\Asset;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\AssetRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\AssetHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use Illuminate\Support\Facades\DB;

class CustomMetadataJob extends Job
{
	 use CaseFrameworkTrait, ArrayHelper, AssetHelper, DateHelpersTrait;

    private $identifier;
    private $orgCode;
    private $domainName;
    private $documentId;

    private $itemRepository;
    private $itemAssociationRepository;

    public function __construct($identifier, $orgCode='', $documentIdentifier = '',$requestUrl ){
        $this->identifier   =   $identifier;
        $this->orgCode      =   $orgCode;
        $this->documentId   =   $documentIdentifier;
        $this->domainName   =   $requestUrl;
    }

    public function handle(ItemRepositoryInterface $itemRepository,ItemAssociationRepositoryInterface $itemAssociationRepository,AssetRepositoryInterface  $assetRepository)
    {
        $this->itemRepository               =   $itemRepository;
        $this->itemAssociationRepository    =   $itemAssociationRepository;
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId = $this->getOrganizationDetails($this->orgCode);
            $organizationId = $getOrganizationId['organization_id'];
        }
        // echo $organizationId;
        // echo "--";
        // echo $this->identifier;
        $documentDetails   = $this->getDocumentDetails($this->identifier,$organizationId);
        if(!empty($this->documentId)){
            $documentId = $this->documentId;
        }
        else {
            $documentId        = $documentDetails[0]['document_id']; 
        }

        //5685e553-952a-4ec7-9161-836c1397cbba
        $resultData        = $this->prepareResponseForItem($documentId,$organizationId);
        $documentData      = $this->prepareDocumentResponseForItem($documentId);

       // For Item Document
		$parsedAsset = [];
        foreach($documentData as $asset) {
            $assetId       = $asset['asset_id'];
            $title         = !empty($asset['title']) ? $asset['title'] : "";
            $assetType     = !empty($asset['asset_linked_type']) ? $asset['asset_linked_type'] : 0;
            $description   = !empty($asset['description']) ? $asset['description'] : "";
            $assetFileName = !empty($asset['asset_name']) ? $asset['asset_name'] : "";
            $assetLinkedType = !empty($asset['item_linked_id']) ? $asset['item_linked_id'] : "";
            $assetTargetFileName = !empty($asset['asset_target_file_name']) ? $asset['asset_target_file_name'] : "";
            $previewUrl =  $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
            $downloadUrl = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
            $assetContentType = !empty($asset['asset_content_type']) ? $asset['asset_content_type'] : "";
            $assetSizeInBytes = isset($asset['asset_size']) ? $asset['asset_size'] : 0;
            $assetUploadedDateTime = !empty($asset['uploaded_at']) ? $asset['uploaded_at'] : "";
          
            $parsedAsset[$asset['item_linked_id']][] = [
                "identifier"         => $assetId,
                "title"              => $title,
                "asset_linked_type"  => $assetType,
                "document_id"        => $documentId,
                "description"        => $description,
                "uri"                => $previewUrl,
                "item_linked_id"     => $assetLinkedType,
                "name"               => $assetTargetFileName,
                "uploaded_date_time" => $assetUploadedDateTime,
                'organization_id'    => isset($organizationId)?$organizationId:'',
                'asset_content_type' => $assetContentType,
                'asset_name'         => $assetFileName,
                'asset_size'         => $assetSizeInBytes
            ];

        }
        // For document Data insert
        
        $documentDetail = $this->prepareDocumentData($documentId);
       
        $parsedDocument =[];
        foreach ($documentDetail as $documentDetailK => $documentDetailV)
        {
            $assetId               = $documentDetailV['asset_id'];
            $title                 = !empty($documentDetailV['title']) ? $documentDetailV['title'] : "";
            $assetType             = !empty($documentDetailV['asset_linked_type']) ? $documentDetailV['asset_linked_type'] : 0;
            $description           = !empty($documentDetailV['description']) ? $documentDetailV['description'] : "";
            $assetFileName         = !empty($documentDetailV['asset_name']) ? $documentDetailV['asset_name'] : "";
            $assetLinkedType       = !empty($documentDetailV['item_linked_id']) ? $documentDetailV['item_linked_id'] : "";
            $assetTargetFileName   = !empty($documentDetailV['asset_target_file_name']) ? $documentDetailV['asset_target_file_name'] : "";
            $previewUrl            =  $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
            $downloadUrl           = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
            $assetContentType      = !empty($documentDetailV['asset_content_type']) ? $documentDetailV['asset_content_type'] : "";
            $assetSizeInBytes      = isset($documentDetailV['asset_size']) ? $documentDetailV['asset_size'] : 0;
            $assetUploadedDateTime = !empty($documentDetailV['uploaded_at']) ? $documentDetailV['uploaded_at'] : "";

            $parsedDocument[$documentDetailV['item_linked_id']]['document'][] = [
                "identifier"         => $assetId,
                "title"              => $title,
                "asset_linked_type"  => $assetType,
                "document_id"        =>  $documentId,
                "description"        => $description,
                "uri"                => $previewUrl,
                "item_linked_id"     => $assetLinkedType,
                "name"               => $assetTargetFileName,
                "uploaded_date_time" => $assetUploadedDateTime,
                'organization_id'    => isset($organizationId)?$organizationId:'',
                'asset_content_type' => $assetContentType,
                'asset_name'         => $assetFileName,
                'asset_size'         => $assetSizeInBytes
            ];
        }

		$exemplarAssociationTypeEnumValue = $this->getSystemSpecifiedAssociationTypeNumber("exemplar");
        $conditionalAttributes = [
            "source_document_id" => $documentId,
            "association_type" => $exemplarAssociationTypeEnumValue,
        ];
        $exemplarAssociations = $itemAssociationRepository->findByAttributes($conditionalAttributes);
        $associationIds = [];
        foreach($exemplarAssociations as $examplar)
        {
            array_push($associationIds,$examplar->item_association_id);
        }
        $conditionalKeyValuePairs = [
            "document_id" => $documentId,
            "asset_linked_type" => 3
        ];
        $assetCollection = $assetRepository->findByAttributes($conditionalKeyValuePairs);
        $parsedAsset1 = [];
        $assetTargetFileName ='';
        foreach($assetCollection as $asset) {
            $assetId = $asset->asset_id;
            $title = !empty($asset->title) ? $asset->title : "";
            $description = !empty($asset->description) ? $asset->description : "";
            $assetFileName = !empty($asset->asset_name) ? $asset->asset_name : "";
            $assetTargetFileName = !empty($asset->asset_target_file_name) ? $asset->asset_target_file_name : "";
            $previewUrl =  $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
            $downloadUrl = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'download',$this->domainName);
            $assetContentType = !empty($asset->asset_content_type) ? $asset->asset_content_type : "";
            $assetSizeInBytes = isset($asset->asset_size) ? $asset->asset_size : 0;
            $assetUploadedDateTime = !empty($asset->uploaded_at) ? $asset->uploaded_at : "";
          
            $parsedAsset1[$asset->item_linked_id] = [
                "asset_id" => $assetId,
                "asset_target_file_name"=>$assetTargetFileName,
                "asset_content_type"=>$assetContentType,
                "asset_size_in_bytes"=>$assetSizeInBytes,
                "title" => $title,
                "description" => $description,
                "uri" => $previewUrl,
                "name" => $assetFileName,
                "uploaded_at" => $assetUploadedDateTime
            ];   
        }
        $parsedExemplarAssociations = [];
        foreach($exemplarAssociations as $association)
        {
            $associationId = $association->item_association_id;
            $hasAsset      = $association->has_asset;
            if (!empty($associationId))
            {
                // get target document id based on destination node id start
                $documentIdentifier = $this->itemRepository->getTargetDocumentIdBasedOnSourceItemId($association->destination_node_id,$organizationId);
                $targetDocumentIdentifier = !empty($documentIdentifier[0]->document_id) ? $documentIdentifier[0]->document_id : $association->destination_node_id;
                $associationType         = isset($association->association_type)?$association->association_type:'';
                $documentId              = isset($association->document_id)?$association->document_id:'';
                $associationGroupId      = isset($association->association_group_id)?$association->association_group_id:'';
                $originNodeId            = isset($association->origin_node_id)?$association->origin_node_id:'';
                $destinationNodeId       = isset($association->destination_node_id)?$association->destination_node_id:'';
                $destinationDocumentId   = isset($association->destination_document_id)?$association->destination_document_id:'';
                $sequenceNumber          = isset($association->sequence_number)?$association->sequence_number:'';
                $sourceItemAssociationId = isset($association->source_item_association_id)?$association->source_item_association_id:'';
                $externalNodeTitle       = isset($association->external_node_title)?$association->external_node_title:'';
                $description             = isset($association->description)?$association->description:'';
                $organizationId          = isset($organizationId)?$organizationId:'';
                $hasAsset                = isset($association->has_asset)?$association->has_asset:'';
                $externalNodeUrl         = isset($association->external_node_url)?$association->external_node_url:'';
                $uri                     = isset($association->uri)?$association->uri:'';
                $isReverseAssociation    = isset($association->is_reverse_association)?$association->is_reverse_association:'';
                $source_document_id      = isset($association->source_document_id)?$association->source_document_id:'';
                $source_item_id          = isset($association->source_item_id)?$association->source_item_id:'';
                $target_document_id      = $targetDocumentIdentifier;
                $target_item_id          = isset($association->target_item_id)?$association->target_item_id:'';

                $externalPreviewUrl  = $this->createAndReturnAssetLinkForType($assetTargetFileName, 'preview',$this->domainName);
                $parsedExemplarAssociations[$association->source_item_id][] =
                [
                    'association_type'           => $associationType,
                    'association_group_id'       => $associationGroupId,
                    'origin_node_id'             => $originNodeId,
                    'destination_node_id'        => $destinationNodeId,
                    'destination_document_id'    => $destinationDocumentId,
                    'sequence_number'            => $sequenceNumber,
                    'source_item_association_id' => $sourceItemAssociationId,
                    'external_node_title'        => $externalNodeTitle,
                    'description'                => $description,
                    'external_node_url'          => $externalNodeUrl,
                    'organization_id'            => $organizationId,
                    'has_asset'                  => $hasAsset,
                    //'created_at'                 => $assetUploadedDateTime ,
                    //'updated_at'                 => $assetUploadedDateTime,
                    'uri'                        => $externalPreviewUrl,
                    'is_reverse_association'     => $isReverseAssociation,
                    'assets'                     => !empty($parsedAsset1[$sourceItemAssociationId])?($parsedAsset1[$sourceItemAssociationId]):'',
                    'source_document_id'         => $source_document_id,
                    'source_item_id'             => $source_item_id,
                    'target_document_id'         => $target_document_id,
                    'target_item_id'             => $target_item_id
                ];
            }   else   {
                $assetFileName = "";
                $assetTargetFileName = "";
                $externalPreviewUrl = $association->external_node_url;
                $externalDownloadUrl = $association->external_node_url;
                $assetId = "";
                $assetContentType = "";
                $assetSizeInBytes = 0;
            }

        }



        //862019    
        $itemData             = $this->getItemDetails($documentId,$organizationId);
        $sourceList =[];
        foreach($itemData as $itemDataK => $itemDataV)
        {
            $sourceList[$itemDataV['item_id']] = $itemDataV['source_item_id'];
        }
        $this->sourceList     = $sourceList;
        $itemIdsArr           = array_column($itemData, 'item_id');
        //862019
            
        $getUniqueItemId   = $this->getUniqueItemId($resultData);
        $getItemData       = $this->getItemLevelData($itemIdsArr);
        $nodeTypeId = [];
        foreach ($getItemData as $getItemDataK=>$getItemDataV) {
           $nodeTypeId[] = $getItemDataV['node_type_id'];
        }


        $getUniqueNodeId    = array_unique($nodeTypeId);

        $getNodeDetails     = $this->getNameNodeType($getUniqueNodeId);
        $arrDataNode        = [];
        
        $itemIdAndNodeIdData = array();
        foreach ($getItemData as $itemDataK=>$itemDataV)
        {
            foreach ($getNodeDetails as $getNodeDetailsK=>$getNodeDetailsV)
            {
                if($itemDataV['node_type_id']==$getNodeDetailsV['node_type_id'])
                    {
                        $itemIdAndNodeIdData[] = [
                                                    'identifier'                =>  $itemDataV['item_id'],
                                                    'node_type_id'              =>  $getNodeDetailsV['node_type_id'],
                                                    'title'                     =>  $getNodeDetailsV['title'],
                                                    'source_node_type_id'       =>  $getNodeDetailsV['source_node_type_id'],
                                                    'custom'                    =>  array(),
                                                    'asset'                     =>  array(),
                                                    'exemplar_and_association'  =>  array(),
                                                    'association_description'   =>  array()
                                                ];
                    }
                }
        }
        
        $formattedData = [];
        foreach($getUniqueItemId as $getUniqueItemIdV)
        {
            foreach ($resultData as $resultDataK => $resultDataV)
            {
                if($getUniqueItemIdV==$resultDataV['item_id']) // Checking item_id with unique item Id
                {
                    unset($resultDataV['item_id']);
                    //unset($resultDataV['metadata_id']); // Commented to show metadata id for the custom metadata - UF-1871
                    $formattedData[$getUniqueItemIdV][]=$resultDataV;
                }
            }
        }
        $info=[];
        $j=0;
        $ra=0;
        $sourceListArr = $this->sourceList;
        
        // Custom Start
        if(!empty($formattedData))
        {
            $customData=array();
            foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK=>$itemIdAndNodeIdDataV)
            {
                foreach($formattedData as $formattedDataK=>$formattedDataV)
                {
                    if($itemIdAndNodeIdDataV['identifier']==$formattedDataK)
                    {
                        $itemIdAndNodeIdDataV['custom'] = !empty($formattedDataV)?$formattedDataV:array();
                        $customData[]=$itemIdAndNodeIdDataV;

                    }
                }   
            }
            
            $getCustomIdItemId = array();
            foreach($customData as $customDataK=>$customDataV)
            {
                $getCustomIdItemId[]=$customDataV['identifier'];
            }

            $getNonCustomItemId = array();
            $getNonCustomItemId =array_diff($itemIdsArr,$getCustomIdItemId);
            
            if(!empty($getNonCustomItemId))
            {
                $getNonCustomData = array();
                foreach($getNonCustomItemId as $getNonCustomItemIdK=>$getNonCustomItemIdV)
                {
                    foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK=>$itemIdAndNodeIdDataV)
                    {
                        if($getNonCustomItemIdV==$itemIdAndNodeIdDataV['identifier'])
                        {
                            $getNonCustomData[]=$itemIdAndNodeIdDataV;
                        }
                    }
                }

                $customMetadataData = array_merge($customData,$getNonCustomData);

                $itemIdAndNodeIdData=$customMetadataData;
            }
            else
            {
                $itemIdAndNodeIdData=$customData;
            }

        }

        # Code to solve UF-762 start
        foreach($itemIdAndNodeIdData as $key => $itemData) {
            $customMetadataAlreadInArr = [];
            foreach($itemData['custom'] as $custom) {
                $customMetadataAlreadInArr[] = $custom['metadata_id'];
            }
            $customNodeTypeMetadata = DB::table('node_type_metadata')
                                        ->leftJoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
                                        ->where('node_type_id', $itemData['node_type_id'])
                                        ->where('metadata.is_custom', 1)
                                        ->whereNotIn('metadata.metadata_id', $customMetadataAlreadInArr)
                                        ->get();

            foreach($customNodeTypeMetadata as $data) {
                $itemIdAndNodeIdData[$key]['custom'][] = [
                    'metadata_value' => '',
                    'metadata_id' => $data->metadata_id,
                    'organization_id' => $data->organization_id,
                    'parent_metadata_id' => $data->parent_metadata_id,
                    'name' => $data->name,
                    'internal_name' => $data->internal_name,
                    'field_type' => $data->field_type,
                    'field_possible_values' => $data->field_possible_values,
                    'last_field_possible_values' => $data->last_field_possible_values,
                    'order' => $data->order,
                    'is_custom' => $data->is_custom,
                    'is_document' => $data->is_document,
                    'is_mandatory' => $data->is_mandatory,
                    'is_active' => $data->is_active,
                    'is_deleted' => $data->is_deleted,
                    'updated_by' => $data->updated_by,
                    'created_at' => $data->created_at,
                    'updated_at' => $data->updated_at,
                ];
            }
        }
        # Code to solve UF-762 end
        //Custom End
        
        //Exemplar Start
        if(!empty($parsedExemplarAssociations))
        {
            $parsedExemplarAssociationsData = array();
            foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK=>$itemIdAndNodeIdDataV)
            {
                foreach($parsedExemplarAssociations as $parsedExemplarAssociationsK=>$parsedExemplarAssociationsV)
                {
                    if($itemIdAndNodeIdDataV['identifier']==$parsedExemplarAssociationsK)
                    {
                        $itemIdAndNodeIdDataV['exemplar_and_association'] = !empty($parsedExemplarAssociationsV)?$parsedExemplarAssociationsV:array();
                        $parsedExemplarAssociationsData[]=$itemIdAndNodeIdDataV;

                    }
                }   
            }
            $geExemplarAssociationsId = array();
            foreach($parsedExemplarAssociationsData as $parsedExemplarAssociationsDataK=>$parsedExemplarAssociationsDataV)
            {
                $geExemplarAssociationsId[]=$parsedExemplarAssociationsDataV['identifier'];
            }

            $getExemplarAssociationsId = array();
            $getExemplarAssociationsId =array_diff($itemIdsArr,$geExemplarAssociationsId);

            if(!empty($getExemplarAssociationsId))
            {
                $ExemplarAssociationsNonData = array();
                foreach($getExemplarAssociationsId as $getExemplarAssociationsIdK=>$getExemplarAssociationsIdV)
                {
                    foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK=>$itemIdAndNodeIdDataV)
                    {
                        if($getExemplarAssociationsIdV==$itemIdAndNodeIdDataV['identifier'])
                        {
                            $ExemplarAssociationsNonData[]=$itemIdAndNodeIdDataV;
                        }
                    }
                }

                $ExemplarAssociationsData = array_merge($parsedExemplarAssociationsData,$ExemplarAssociationsNonData);

                $itemIdAndNodeIdData=$ExemplarAssociationsData;
            }
            else
            {
                $itemIdAndNodeIdData=$parsedExemplarAssociationsData;
            }

        }
        //Exemplar End
        //Association Start
        $parsedAssociations = $this->fetchItemAssociations($documentId, $organizationId);
        if(!empty($parsedAssociations))
        {
            $parsedAssociationsData = array();
            foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK => $itemIdAndNodeIdDataV)
            {
                foreach($parsedAssociations as $parsedAssociationsK =>$parsedAssociationsV)
                {
                    if($itemIdAndNodeIdDataV['identifier'] == $parsedAssociationsK)
                    {
                        $itemIdAndNodeIdDataV['association_description']    =   !empty($parsedAssociationsV)?$parsedAssociationsV:array();
                        $parsedAssociationsData[]                           =   $itemIdAndNodeIdDataV;

                    }
                }
            }

            $geAssociationsId = array();
            foreach($parsedAssociationsData as $parsedAssociationsDataK => $parsedAssociationsDataV)
            {
                $geAssociationsId[]    =   $parsedAssociationsDataV['identifier'];
            }

            $getAssociationsId =    array();
            $getAssociationsId =    array_diff($itemIdsArr,$geAssociationsId);

            if(!empty($getAssociationsId))
            {
                $AssociationsNonData = array();
                foreach($getAssociationsId as $getAssociationsIdK => $getAssociationsIdV)
                {
                    foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK => $itemIdAndNodeIdDataV)
                    {
                        if($getAssociationsIdV == $itemIdAndNodeIdDataV['identifier'])
                        {
                            $AssociationsNonData[] = $itemIdAndNodeIdDataV;
                        }
                    }
                }

                $AssociationsNonData = array_merge($parsedAssociationsData,$AssociationsNonData);

                $itemIdAndNodeIdData = $AssociationsNonData;
            }
            else
            {
                $itemIdAndNodeIdData = $parsedAssociationsData;
            }
        }
        //Association End

        //Asset Start
        if(!empty($parsedAsset))
        {
            $parsedAssetData = array();
            foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK=>$itemIdAndNodeIdDataV)
            {
                foreach($parsedAsset as $parsedAssetK=>$parsedAssetV)
                {
                    if($itemIdAndNodeIdDataV['identifier']==$parsedAssetK)
                    {
                        $itemIdAndNodeIdDataV['asset'] = !empty($parsedAssetV)?$parsedAssetV:array();
                        $parsedAssetData[]=$itemIdAndNodeIdDataV;

                    }
                }   
            }

            $geAssetId = array();
            foreach($parsedAssetData as $parsedAssetDataK=>$parsedAssetDataV)
            {
                $geAssetId[]=$parsedAssetDataV['identifier'];
            }

            $getAssetId = array();
            $getAssetId =array_diff($itemIdsArr,$geAssetId);

            if(!empty($getAssetId))
            {
                $assetNonData = array();
                foreach($getAssetId as $getAssetIdK=>$getAssetIdV)
                {
                    foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataK=>$itemIdAndNodeIdDataV)
                    {
                        if($getAssetIdV==$itemIdAndNodeIdDataV['identifier'])
                        {
                            $assetNonData[]=$itemIdAndNodeIdDataV;
                        }
                    }
                }

                $assetData = array_merge($parsedAssetData,$assetNonData);

                $itemIdAndNodeIdData=$assetData;
            }
            else
            {
                $itemIdAndNodeIdData=$parsedAssetData;
            }

        }
        //Asset End
        
        //FINAL DATA PREPARE START
        $itemIdAndNodeIdFinalData = array();
        foreach($itemIdAndNodeIdData as $itemIdAndNodeIdDataKey=>$itemIdAndNodeIdDataValue)
        {
            foreach($this->sourceList as $itemIdData=>$itemSourceIdData)
            {
                if($itemIdAndNodeIdDataValue['identifier']==$itemIdData)
                {
                    $itemIdAndNodeIdDataValue['identifier'] = $itemSourceIdData;
                    $itemIdAndNodeIdFinalData[] = $itemIdAndNodeIdDataValue;
                }
            }
        }
        //FINAL DATA PREPARE END


        $documentResultData = $this->prepareResponseForDocument($documentId,$organizationId);
        $formattedDataDocument[$this->identifier]=$documentResultData;
        $doc  =[];
        $arr3 =[];
        $DocumentListArr = $this->documentSourceList;
        $getNodeDetails  = $this->getDocumentNodeDetails($this->identifier);
        $nodeTypeId      = $getNodeDetails[0]['node_type_id'];
        $nodeDetails     = $this->getNameNodeDocument($nodeTypeId);
        foreach($formattedDataDocument as $formattedDataDocumentK=>$formattedDataDocumentV) {
            foreach ($parsedDocument as $parsedDocumentK =>$parsedDocumentV) {

                if ($documentId == $parsedDocumentK) {
                    $arr3 = $parsedDocumentV['document'];
                }
            }
            $doc['identifier']          = !empty($DocumentListArr[$formattedDataDocumentK]) ? $DocumentListArr[$formattedDataDocumentK] : $formattedDataDocumentK;
            $doc['node_type_id']        = $nodeDetails[0]['node_type_id'];
            $doc['title']               = $nodeDetails[0]['title'];
            $doc['source_node_type_id'] = $nodeDetails[0]['source_node_type_id'];
            $doc['custom']              = $formattedDataDocumentV;
            $doc['document_asset']      = $arr3;
        }
        if(!empty($doc))
        {
            $doc2[] = $doc;
        }else{
            $doc2 = $doc;
        }


        $data =['ACMTcustomfields'=>['CFItem'=> $itemIdAndNodeIdFinalData,'CFDocument'=>$doc2]];
        return $data ;
    }

    private function fetchItemAssociations($documentId, $organizationId){
        $parsedCaseAssociations =   [];
        $returnCollection = true;
        $fieldsToReturn = ["*"];
        $dataFetchingConditionalClause = [ "document_id" => $documentId, "organization_id" => $organizationId, "is_deleted" => 0 ];
        $relations      =   ['nodeType'];
        $savedItems = $this->itemRepository->findByAttributesWithSpecifiedFields(
                            $returnCollection, 
                            $fieldsToReturn, 
                            $dataFetchingConditionalClause,$relations
                        );

        $returnCollection = true;
        $fieldsToReturn = ["*"];
        // $dataFetchingConditionalClause = [ "document_id" => $documentId, "organization_id" => $organizationId, 'is_deleted' => 0, 'destination_document_id' => $documentId ];
        $dataFetchingConditionalClause = [ "source_document_id" => $documentId, "organization_id" => $organizationId, 'is_deleted' => 0 ];
        $savedAssociationsCollection = $this->itemAssociationRepository->findByAttributesWithSpecifiedFields(
                                $returnCollection, 
                                $fieldsToReturn, 
                                $dataFetchingConditionalClause
                            );
        $savedAssociationsWithinTaxonomy = $savedAssociationsCollection->filter(function ($item, $key) {
                                return !empty($item->source_item_id) && !empty($item->source_item_association_id);
                            });

        foreach($savedItems as $itemsFromTaxonomy) {
            $itemIdArray[]  =   $itemsFromTaxonomy->item_id;
        }

        $attributeIn                =   'target_item_id'; 
        $containedInValues          =   $itemIdArray;
        //$keyValuePairs              =   ['organization_id' => $organizationId, 'is_deleted' => 0, 'destination_document_id' => $documentId, 'is_reverse_association' => 1];
        $keyValuePairs              =   ['organization_id' => $organizationId, 'is_deleted' => 0, 'target_document_id' => $documentId ];
        $notInKeyValuePairs         =   ['source_document_id'  =>  $documentId];
        $returnPaginatedData        =   false;
        $paginationFilters          =   ['limit' => 10, 'offset' => 0];
        $returnSortedData           =   false;
        $sortfilters                =   ['orderBy' => 'updated_at', 'sorting' => 'desc'];
        $operator                   =   'AND';
        $relations                  =   ['document', 'sourceItemId', 'sourceDocumentNode', 'targetItemId', 'targetItemDocumentNode'];

        $savedAssociationsCollectionForReverseAssociation = $this->itemAssociationRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
            $attributeIn, 
            $containedInValues,
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $notInKeyValuePairs,
            $returnPaginatedData,
            $paginationFilters,
            $returnSortedData,
            $sortfilters,
            $operator, 
            $relations);


        $savedReverseAssociations = $savedAssociationsCollectionForReverseAssociation->filter(function ($item, $key) {
                                return !empty($item->source_item_id) && !empty($item->source_item_association_id);
                            });

        $savedAssociations  =   $savedReverseAssociations->merge($savedAssociationsWithinTaxonomy);

        foreach($savedAssociations as $association) {
            $identifier         =   $association->source_item_association_id;
            $sequenceNumber     =   $association->sequence_number;
            $associationType    =   $this->returnAssociationType($association->association_type);

            if($associationType != "exemplar") {
                $parsedCaseAssociations[$association->source_item_id][] = [
                    "identifier"                =>  $identifier,
                    "associationType"           =>  $associationType,
                    "description"               =>  $association->description
                ];
            }
        }
        return $parsedCaseAssociations;
    }

    public function getItemLevelData($itemId)
    {
        $itemNode= Item::select('source_item_id','node_type_id','item_id')
             ->whereIn('item_id',$itemId)
             ->where('is_deleted',0)
             ->get()
             ->toArray();
       return $itemNode;
    }

    public function getDocumentNodeDetails($documentId)
    {
        $getNodeId = Document::select('node_type_id')
                     ->where('source_document_id',$documentId)
                     ->where('is_deleted',0)
                     ->get()
                     ->toArray();
        return $getNodeId;
    }

    public function getNameNodeDocument($nodeTypeId){
        $getNodeName = NodeType::select('node_type_id','title','source_node_type_id')
            ->where('source_node_type_id',$nodeTypeId)
            ->where('is_deleted',0)
            ->get()
            ->toArray();

        return $getNodeName;
    }

    public function getNameNodeType($nodeTypeId){
        $getNodeName = NodeType::select('node_type_id','title','source_node_type_id')
                       ->whereIn('node_type_id',$nodeTypeId)
                       ->where('is_deleted',0)
                       ->get()
                       ->toArray();     

        return $getNodeName;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return array
     * @Function Name prepareResponse
     * @Purpose This function is used to prepare all function and make it a single array of data
     */
    public function prepareResponseForItem($documentId,$organizationId)
    {
        $itemData             = $this->getItemDetails($documentId,$organizationId);
        $sourceList =[];
        foreach($itemData as $itemDataK => $itemDataV)
        {
            $sourceList[$itemDataV['item_id']] = $itemDataV['source_item_id'];
        }
        $this->sourceList     = $sourceList;
        $itemIdsArr           = array_column($itemData, 'item_id');
        $metadata             = $this->getItemMetadataDetails($itemIdsArr);
        $metadataId           = array_column($metadata, 'metadata_id');
        $metadataList         = $this->getMetadataName($metadataId,$organizationId);
        $customMetadataDetail = $this->customMetadataDetail($metadata,$metadataList);
        // $assetData            = $this->assetData($documentId,$organizationId);
        // $exemplar             = $this->getExemplarDetails($documentId,$organizationId);
        // $arrData              = array_merge($assetData,$customMetadataDetail);
        // $finalArr             = array_merge($arrData,$exemplar);
        return $customMetadataDetail;


    }

    public function prepareDocumentResponseForItem($documentId)
    {
        $assetCollection = Asset::where('document_id','=',$documentId)
            ->where('asset_linked_type',2)
            ->get();
        if($assetCollection) {
            $assetCollection = $assetCollection->toArray();
        }
        return $assetCollection;
    }

    public function assetData($documentId,$organizationId)
    {
        $assetItemId = Asset::select('item_linked_id as item_id')->where('document_id','=',$documentId)
            ->where('asset_linked_type',2)
            ->where('organization_id',$organizationId)
            ->get();
        if($assetItemId) {
            $assetItemId = $assetItemId->toArray();
        }
        return $assetItemId;
    }


    public function prepareDocumentData($documentId)
    {
        $documentCollection = Asset::where('document_id','=',$documentId)
            ->where('asset_linked_type',1)
            ->get();
        if($documentCollection) {
            $documentCollection = $documentCollection->toArray();
        }
        return $documentCollection;
    }

    public function getExemplarDetails($documentId,$organizationId)
    {
        $exemplar = ItemAssociation::select('source_item_id as item_id')->where('source_document_id',$documentId)
                         ->where('association_type',8)
                         ->where('organization_id',$organizationId)
                         ->get()
                         ->toArray();
        return $exemplar;
    }

    public function prepareResponseForDocument($identifier,$organizationId)
    {
        $documentData             = $this->getDocumentSourceDetails($identifier,$organizationId);
        $documentSourceList =[];
        foreach($documentData as $documentDataK => $documentDataV)
        {
            $documentSourceList[$documentDataV['document_id']] = $documentDataV['source_document_id'];
        }
        $this->documentSourceList = $documentSourceList;
        $getDocMetadataDetails        = $this->getDocumentMetadataDetails($identifier);
        $metadataId                   = array_column($getDocMetadataDetails,'metadata_id');
        $metadataDetails              = $this->getMetadataName($metadataId,$organizationId);
        $customDocumentMetadataDetail = $this->customDocumentMetadataDetail($getDocMetadataDetails,$metadataDetails);
        return $customDocumentMetadataDetail;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @Function Name getItemDetails
     * @Purpose This function is used to get item details from Item table
     */
    public function getItemDetails($documentId,$organizationId){
       if($organizationId)
       {
           $itemData = Item::select('item_id','source_item_id')
               ->where('document_id','=',$documentId)
               ->where('organization_id',$organizationId)
               ->where('is_deleted','=',0)
               ->get()
               ->toArray();
           return $itemData;
       }else{
           $item = Item::select('item_id','source_item_id')
               ->where('document_id','=',$documentId)
               ->where('is_deleted','=',0)
               ->get()
               ->toArray();
           return $item;
       }

    }

    public function getDocumentSourceDetails($documentId,$organizationId){
        $document = Document::select('document_id','source_document_id')
            ->where('source_document_id','=',$documentId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted','=',0)
            ->get();
        if($document) {
            $document =$document->toArray();
        }

        return $document;
    }

    /**
     * @param $itemId
     * @return mixed
     * @Function Name: getItemMetadataDetails
     * @purpose This function is used to get item metadata list
     */
    public function getItemMetadataDetails($itemId){
        $itemMetadata = ItemMetadata::select('item_id', 'metadata_value', 'metadata_id')
            ->whereIn('item_id',$itemId)
            ->where('is_deleted',0)
            ->get();
        if($itemMetadata) {
            $itemMetadata= $itemMetadata->toArray();
        }
        return $itemMetadata;
    }


    /**
     * @param $itemId
     * @param string $organizationId
     * @return mixed
     * @Function Name This function is used to get metadata details from Metadata table
     */
    public function getMetadataName($itemId,$organizationId='')
    {
        if($organizationId!='')
        {
            $metadataName = Metadata::
            whereIn('metadata_id',$itemId)
                ->where('is_deleted',0)
                ->where('organization_id',$organizationId)
                ->get()
                ->toArray();
            return $metadataName;
        }else{
            $metadataName = Metadata::
            whereIn('metadata_id',$itemId)
                ->where('is_deleted',0)
                ->get()
                ->toArray();
            return $metadataName;
        }
    }


    /**
     * @param $metadata
     * @param $metadataList
     * @return array
     * @FunctionName customMetadataDetail
     * @purpose This function is used to merge two array
     */
    public function customMetadataDetail($metadata,$metadataList)
    {
        $customMetadataDetail = [];
        foreach($metadata as $metadataK=>$metadataV)
        {
            foreach ($metadataList as $metadataListK=>$metadataListV)
            {
                if($metadataV['metadata_id']==$metadataListV['metadata_id'])
                {
                    $customMetadataDetail[]= array_merge($metadataV,$metadataListV);
                }
            }
        }
        return $customMetadataDetail;
    }

    /**
     * @param $getDocMetadataDetails
     * @param $metadataDetails
     * @return array
     * @FunctionName customDocumentMetadataDetail
     * @Purpose This function is used to merge document metadata and metadata
     */
    public function customDocumentMetadataDetail($getDocMetadataDetails,$metadataDetails)
    {
        $customDocument = [];
        foreach ($getDocMetadataDetails as $getDocMetadataDetailsK=> $getDocMetadataDetailsV)
        {
            foreach ($metadataDetails as $metadataDetailsK=>$metadataDetailsV)
            {
                if($getDocMetadataDetailsV['metadata_id']==$metadataDetailsV['metadata_id'])
                {
                    $customDocument[]= array_merge($getDocMetadataDetailsV,$metadataDetailsV);
                }
            }
        }
        return $customDocument;
    }

    /**
     * @param $resultData
     * @return array
     * @FunctionName getUniqueItemId
     * @Purpose This function is used to filter unique item Id
     */
    public function getUniqueItemId($resultData)
    {
        $data = [];
        foreach ($resultData as $itemIds)
        {
            $data[] = $itemIds['item_id'];
        }
        $getUniqueItemId = array_unique($data);
        return $getUniqueItemId;
    }
    /**
     * @param $documentId
     * @param string $organizationId
     * @return mixed
     * @FunctionName getDocumentDetails
     * @Purpose This function is used to get Document details based on document Id.
     */
    Public function getDocumentDetails($documentId,$organizationId='')
    {
        if($organizationId!='') {
            $document = Document::select('document_id')
                ->where('source_document_id','=',$documentId)
                ->where('is_deleted','=',0)
                ->where('organization_id','=',$organizationId)
                ->get()
                ->toArray();

            return $document;
        }else{
            $document = Document::select('document_id')
                ->where('source_document_id','=',$documentId)
                ->where('is_deleted','=',0)
                ->get()
                ->toArray();
            return $document;
        }
    }

    /**
     * @param $documentId
     * @return mixed
     * @FunctionName getDocumentMetadataDetails
     * @Purpose This function is used to get document metadata details
     */
    public function getDocumentMetadataDetails($documentId)
    {
        $docMetadata = DocumentMetadata::select('metadata_id', 'metadata_value')
            ->where('document_id','=',$documentId)
            ->get();
        if($docMetadata){
            $docMetadata = $docMetadata->toArray();
        }
        return $docMetadata;
    }

    /**
     * @param $orgCode
     * @return mixed
     * @Function getOrganizationDetails
     * @Purpose This function is based on orgCode and return organization id for tenant specific
     */
    public function getOrganizationDetails($orgCode)
    {
        $orgId = Organization::select('organization_id')
            ->where('org_code',$orgCode)
            ->first();
        if($orgId) {
            $orgId = $orgId->toArray();
        }
        return $orgId ;
    }
	
	 private function createAndReturnAssetLinkForType(string $assetPackageName, string $type,$domainName=''): string {
         if(strpos($domainName,'localhost')!==false)
         {
             $baseUrl = env("APP_URL");
         }else{
            $baseUrl = $domainName;

         }

        $fullyResolvedUri = "";
        if(!empty($baseUrl)) {
            $acmtApiVersionPrefix = "api/v1";
            $endPoint = "asset/$type";
            $baseUrlWithEndPoint = "{$baseUrl}/{$acmtApiVersionPrefix}/{$endPoint}";
            $fullyResolvedUri = "{$baseUrlWithEndPoint}/{$assetPackageName}";
        }
        return $fullyResolvedUri;
    }
}
