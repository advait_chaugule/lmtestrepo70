<?php
namespace App\Domains\Casestandard\Jobs;

use Lucid\Foundation\Job;
use App\Data\Models\Document; 
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Data\Models\Organization;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use Illuminate\Support\Facades\DB;
class GetCaseFrameworkSubjectJob extends Job
{
    use DateHelpersTrait, CaseFrameworkTrait;

    private $subjectRepository;
    private $identifier;
    private $subjectDetails;
    private $organizationCode;
    private $orgCode;
    private $repository;
    private $domainName;

    /**
     * GetCaseFrameworkSubjectJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl)
    {
        //
        $this->setIdentifier($identifier);
        $this->orgCode = $orgCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SubjectRepositoryInterface $subjectRepo, MetadataRepositoryInterface $metadataRepository,ItemRepositoryInterface $repo)
    {
        // set the case repository
        $this->subjectRepository    =   $subjectRepo;
        $this->metadataRepository   =   $metadataRepository;
        $this->repository           =   $repo;
        // get the subject detail
        $organizationId ='';
        if($this->orgCode) {
            $getOrganizationId = $this->repository->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }
        $subject = $this->fetchSubjectDetailsFromRepository($organizationId);

        if(!empty($subject['subject_id'])){
            $attributes     =   ['organization_id'  =>  $subject['organization_id'], 'is_active'    =>  '1', 'is_deleted'   =>  '0'];
            $metadataList   =   $this->metadataRepository->findByAttributes($attributes);

            $this->setMetadataList($metadataList);

            // set taxonomy details globally
            $this->setSubjectDetails($subject);
            $cfSubject = $this->transformAndSetCFSubject();
            // finally return the CASE standard CFSubject structure
            return $cfSubject;
        }

        return false;
    }

    public function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;
    }

    public function getIdentifier() :string
    {
        return $this->identifier;
    }

    public function setMetadataList($data)
    {
        $this->metadataList = $data;
    }

    public function getMetadataList()
    {
        return $this->metadataList;
    }

    public function setSubjectDetails($data)
    {
        $this->subjectDetails = $data;
    }

    public function getSubjectDetails()
    {
        return $this->subjectDetails;
    }
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }


    private function fetchSubjectDetailsFromRepository($organizationId)
    {
        $identifier     =   $this->getIdentifier();
        if($organizationId){
            $attributes     =   ['source_subject_id'=>$identifier,'organization_id'=>$organizationId,'is_deleted'=>'0'];
        }else{
            $attributes     =   ['source_subject_id'=>$identifier,'is_deleted' => '0'];
        }

        $subjectDetail  =   $this->subjectRepository->findByAttributes($attributes)->first();
        $this->setOrgCode($subjectDetail->organization_id);
        $subjectEntity  =   [
            "subject_id"                =>  $subjectDetail->subject_id,
            "source_subject_id"         =>  $subjectDetail->source_subject_id,
            "subject_title"             =>  $subjectDetail->title,
            "subject_hierarchy_code"    =>  $subjectDetail->hierarchy_code,
            "subject_description"       =>  $subjectDetail->description,
            "organization_id"           =>  $subjectDetail->organization_id,
            "is_deleted"                =>  $subjectDetail->is_deleted,
            "updated_at"                =>  $subjectDetail->updated_at
        ];
        return $subjectEntity;
    }

    private function parseMetadata() {
        $metadataList   =   $this->getMetadataList();

        foreach($metadataList as $metadata) {
            $arrayOfMetadataInternal[]    =   $metadata->internal_name;
        }

        return $arrayOfMetadataInternal;
    }

    private function transformAndSetCFSubject()
    {
        $subjectData                    =   $this->getSubjectDetails();
        $orgCode                        =   $this->getOrgCode();
        $arrayOfMetadataInternalName    =   $this->parseMetadata();
        foreach($subjectData as $key => $subject) {
            if(in_array($key, $arrayOfMetadataInternalName) === true) {
                $subjectMatchedMetadata[$key]   =   $subject;
            } else {
                $subjectMatchedMetadata[$key]   =   "";
            }
        }

        $identifier = !empty($subjectData['source_subject_id']) ? $subjectData['source_subject_id'] : "";

         $subject_id = $subjectData['subject_id'];

        $getimportType = DB::table('document_subject')
        ->leftJoin('documents', 'document_subject.document_id', '=', 'documents.document_id')
        ->where('document_subject.subject_id','=',$subject_id)
        ->get()
        ->toarray();

        if(!empty($getimportType)){
            $import_type = $getimportType[0]->import_type;
        }
    
        if($import_type == 1) {
            $json_array  = json_decode($getimportType[0]->source_subject_uri_object, true);
            $subject_uri = $json_array['uri'];
        } 

        return [
            "identifier"            =>  $identifier,
            "uri"                   =>  isset($subject_uri) ? $subject_uri : $this->getCaseApiUri('CFSubjects', $identifier,true,$orgCode,$this->domainName),
            "title"                 =>  !empty($subjectMatchedMetadata['subject_title']) ? $subjectMatchedMetadata['subject_title'] : "",
            "hierarchyCode"         =>  !empty($subjectMatchedMetadata['subject_hierarchy_code']) ? $subjectMatchedMetadata['subject_hierarchy_code'] : "",
            "description"           =>  !empty($subjectMatchedMetadata['subject_description']) ? $subjectMatchedMetadata['subject_description'] : "",
            "lastChangeDateTime"    =>  $this->formatDateTimeToISO8601($subjectData['updated_at']),            
        ];
    }
}
