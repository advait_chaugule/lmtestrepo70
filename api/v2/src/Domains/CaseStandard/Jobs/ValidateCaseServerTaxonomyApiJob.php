<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\CurlHelper;
use Illuminate\Support\Facades\DB;

class ValidateCaseServerTaxonomyApiJob extends Job
{
    use CurlHelper;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $linkedId = $this->input['linked_server_id'];
        $documentId = $this->input['document_id'];

        // Repository code changed to query builder code
        $linkedServerDetails = DB::table('linked_servers')
        ->select('server_url')
        ->where('linked_server_id', $linkedId)        
        ->where('is_deleted' , 0)
        ->first();

        $api_url = $linkedServerDetails->server_url;
        if($documentId != "")
        {
             $endPoint = $api_url."/CFPackages/".$documentId;
          
             $serverReponse = $this->checkUrl($endPoint,"GET");  
             return $serverReponse;
             
        }
        else
        {
            return false;
        }
        //
    }
}
