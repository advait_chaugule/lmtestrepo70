<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Storage;

class StoreCaseStandardJsonJob extends Job
{
    private $baseFolderPathForStorage;
    private $caseStandardData;
    private $casePackageStorageFolder;
    private $casePackageFilePrefix;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->caseStandardData = $data;
        $this->caseStorageFolder = env("ACMT_CASE_FOLDER", "CASE_PACKAGES");
        $this->casePackageFilePrefix = $data["CFDocument"]["identifier"];
        $this->baseFolderPathForStorage = storage_path('app');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $casePackageFileName = $this->casePackageFilePrefix.".json";
        $filePathToStore = $this->caseStorageFolder."/".$casePackageFileName;
        $contents = json_encode($this->caseStandardData);
        Storage::put($filePathToStore, $contents);
        $fileExists = Storage::exists($filePathToStore);
        return $fileExists ? $this->baseFolderPathForStorage."/".$filePathToStore : false;
    }
}
