<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

class ExtractCaseFrameworkDocumentFromUploadedJsonContentJob extends Job
{
    use ArrayHelper, StringHelper;

    private $content;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // create array from json content
        $data = $this->convertJsonStringToArray($this->content);
        return (object)$data["CFDocument"];
    }
}
