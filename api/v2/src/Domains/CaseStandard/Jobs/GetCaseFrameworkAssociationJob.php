<?php
namespace App\Domains\Casestandard\Jobs;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use Lucid\Foundation\Job;
use App\Data\Models\Document;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Data\Models\Organization;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use DB;
class GetCaseFrameworkAssociationJob extends Job
{
    use CaseFrameworkTrait, DateHelpersTrait;

    private $identifier;
    private $itemAssociationDetail;
    private $itemAssociationRepository;

    private $nodeTypeRepository;
    private $metadataList;
    private $organizationCode;
    private $orgCode;
    private $repository;
    private $domainName;

    /**
     * GetCaseFrameworkAssociationJob constructor.
     * @param string $identifier
     * @param string $orgCode
     * @param $requestUrl
     */
    public function __construct(string $identifier,$orgCode='',$requestUrl='')
    {
        $this->setIdentifier($identifier);
        $this->orgCode = $orgCode;
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $itemAssociationRepo, NodeTypeRepositoryInterface $nodeTypeRepository,ItemRepositoryInterface $repo)
    {
        // set the association and nodetype repository
        $this->itemAssociationRepository    =   $itemAssociationRepo;
        $this->nodeTypeRepository           =   $nodeTypeRepository;
        $this->repository                   =   $repo;
        $organizationId = '';
        if($this->orgCode) {
            $getOrganizationId = $this->repository->getOrganizationDetails($this->orgCode);
            $organizationId    = $getOrganizationId['organization_id'];
        }
        // get all relevant association data to create CFAssociation
        $itemAssociationDetails = $this->fetchAssociationDetailsFromRepository($organizationId);
        
        if(!empty($itemAssociationDetails->source_item_association_id)){
            // set association details globally
            $this->setAssociationDetails($itemAssociationDetails);
            // transform to and set the CFAssociation Standard Structure
            $association = $this->transFormToCaseStandardStructure($this->domainName);
            // finally return the CASE standard CFAssociation structure
            return $association;
        }
        else{
            return false;
        }
    }

    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    private function fetchAssociationDetailsFromRepository($organizationId){
        $identifier = $this->getIdentifier();
        if($organizationId){
            $attributes =   ['source_item_association_id'=>$identifier,'organization_id'=>$organizationId];
        }else{
            $attributes =  ['source_item_association_id'=>$identifier];
        }
        // call the repository method to fetch data from database
        return $this->itemAssociationRepository->findByAttributes($attributes)->first();
    }

    public function setAssociationDetails($data){
        $this->associationDetail = $data;
    }

    public function getAssociationDetails(){
        return $this->associationDetail;
    }

    public function setMetadataList($data){
        $this->metadataList = $data;
    }

    public function getMetadataList(){
        return $this->metadataList;
    }
    public function setOrgCode(string $data) {
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        if(empty($this->organizationCode)) {
            return $this->orgCode ;
        }else{
            return $this->organizationCode;
        }
    }


    private function parseMetadata() {
        $metadataList   =   $this->getMetadataList();

        foreach($metadataList->metadata   as  $metadata){
            $arrayOfInternalName[]  =   $metadata->internal_name;
        }

        return  $arrayOfInternalName;
    }

    private function getAssociationType() {
        $identifier = $this->getIdentifier();
        $associationDetail = $this->associationRepository->find($identifier);
        $associationType = $associationDetail->caseAssociationType;
        
        return $associationType;
    }

    /**
     * Set DocumentURI for an association model
     */
    private function transformAndSetDocumentUri($itemAssociationIdentifier){
        $itemAssociationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModelBySourceId($itemAssociationIdentifier);
        $getImportType = $this->documentImportType($itemAssociationDetails->document_id,$itemAssociationDetails->organization_id);
        $associationDocument = !empty($itemAssociationDetails->document_id) ? $itemAssociationDetails->document_id : [];
        //GET THE METADATA LIST FOR THE DOCUMENT
        $arrayOfInternalName =[];
        $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationDocument->node_type_id);
        if(!empty($metadataList))
        {
            $this->setMetadataList($metadataList);
            $arrayOfInternalName    =   $this->parseMetadata();
        }

        $orgCode                =   $this->getOrgCode();

        if(in_array('title', $arrayOfInternalName) === true) {
            if($getImportType->import_type==1)
            {
                //$getUrl = $this->getUriFromAssociation($associationDocument->document_id,$itemAssociationDetails->organization_id,$itemAssociationDetails->source_item_association_id);
                $documentUri = [
                    "title"         =>  !empty($associationDocument->title) ? $associationDocument->title : "NA", // IMS Certification
                    "identifier"    =>  !empty($associationDocument->source_document_id) ? $associationDocument->source_document_id : "",
                    "uri"           =>  !empty($getImportType->uri)?$getImportType->uri:"",
                ];
            }else if($getImportType->import_type==2){
                $documentUri = [
                    "title"         =>  !empty($associationDocument->title) ? $associationDocument->title : "NA", // IMS Certification
                    "identifier"    =>  !empty($associationDocument->source_document_id) ? $associationDocument->source_document_id : "",
                    "uri"           =>  !empty($associationDocument->source_document_id) ? $this->getCaseApiUri("CFDocuments", $associationDocument->source_document_id,true,$orgCode,$this->domainName) : "",
                ];
            }else{
                $documentUri = [
                    "title"         =>  !empty($associationDocument->title) ? $associationDocument->title : "NA", // IMS Certification
                    "identifier"    =>  !empty($associationDocument->source_document_id) ? $associationDocument->source_document_id : "",
                    "uri"           =>  !empty($associationDocument->source_document_id) ? $this->getCaseApiUri("CFDocuments", $associationDocument->source_document_id,true,$orgCode,$this->domainName) : "",
                ];
            }

        }   else    {
            $documentUri = [
                "title"         =>  "NA", // IMS Certification
                "identifier"    =>  !empty($associationDocument->source_document_id) ? $associationDocument->source_document_id : "",
                "uri"           =>  !empty($associationDocument->source_document_id) ? $this->getCaseApiUri("CFDocuments", $associationDocument->source_document_id,true,$orgCode,$this->domainName) : "",
            ];
        }
        
        return $documentUri;
    }

    /**
     * Set originNodeURIEntity for an association model
     */
    private function transformAndSetOriginNodeEntity($itemAssociationIdentifier) {
        $itemAssociationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModelBySourceId($itemAssociationIdentifier);
        $associationOriginNode = !empty($itemAssociationDetails->sourceItemId) ? $itemAssociationDetails->sourceItemId : $itemAssociationDetails->targetItemId;
        $getImportType = $this->documentImportType($associationOriginNode->document_id,$itemAssociationDetails->organization_id);
        //GET THE METADATA LIST FOR THE ORIGIN NODE
        $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationOriginNode->node_type_id);
        $this->setMetadataList($metadataList);
        $orgCode                =   $this->getOrgCode();
        $arrayOfInternalName    =   $this->parseMetadata();
        if(in_array('full_statement', $arrayOfInternalName) === true) {
            if($getImportType->import_type==1) {
                $getItemUrl = $this->getUriFromItem($associationOriginNode->document_id,$itemAssociationDetails->organization_id,$associationOriginNode->item_id);
                $originNodeURIEntity = [
                    "title"         =>  !empty($associationOriginNode->full_statement) ? $associationOriginNode->full_statement : "NA", // IMS Certification
                    "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                    "uri"           =>  !empty($getItemUrl->uri)?$getItemUrl->uri : "",
                ];
            }else if($getImportType->import_type==2){
                $originNodeURIEntity = [
                    "title"         =>  !empty($associationOriginNode->full_statement) ? $associationOriginNode->full_statement : "NA", // IMS Certification
                    "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                    "uri"           =>  !empty($associationOriginNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationOriginNode->source_item_id,true,$orgCode,$this->domainName) : "",
                ];
            }else{
                $originNodeURIEntity = [
                    "title"         =>  !empty($associationOriginNode->full_statement) ? $associationOriginNode->full_statement : "NA", // IMS Certification
                    "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                    "uri"           =>  !empty($associationOriginNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationOriginNode->source_item_id,true,$orgCode,$this->domainName) : "",
                ];
            }

        }   else    {
            $originNodeURIEntity = [
                "title"         =>  "NA", // IMS Certification
                "identifier"    =>  !empty($associationOriginNode->source_item_id) ? $associationOriginNode->source_item_id : "",
                "uri"           =>  !empty($associationOriginNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationOriginNode->source_item_id,true,$orgCode,$this->domainName) : "",
            ];
        }
        return $originNodeURIEntity;
    }

    /**
     * Set destinationNodeURIEntity for an association model
     */
    private function transformAndSetDestinationNodeEntity($itemAssociationIdentifier) {
        $destinationNodeURIEntity = [];
        $itemAssociationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModelBySourceId($itemAssociationIdentifier);
        //Check whether destination_node and document_node are similar
        if($itemAssociationDetails->document_id == $itemAssociationDetails->target_item_id) {
            $associationDestinationNode = !empty($itemAssociationDetails->document_id) ? $itemAssociationDetails->document_id : [];
            //GET THE METADATA LIST FOR THE ORIGIN NODE
            $arrayOfInternalName=[];
            if(!empty($associationDestinationNode))
            {
                $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationDestinationNode->node_type_id);
                $this->setMetadataList($metadataList);
                $arrayOfInternalName    =   $this->parseMetadata();
            }

            $getImportType = $this->documentImportType($itemAssociationDetails->document_id,$itemAssociationDetails->organization_id);
            $orgCode                =   $this->getOrgCode();
            if(in_array('title', $arrayOfInternalName) === true) {
                if($getImportType->import_type==1)
                {
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->title) ? $associationDestinationNode->title : "NA",
                        "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                        "uri"           =>  !empty($getImportType->uri) ?$getImportType->uri:""
                    ];
                }else if($getImportType->import_type==2)
                {
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->title) ? $associationDestinationNode->title : "NA",
                        "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                        "uri"           =>  !empty($associationDestinationNode->source_document_id) ?  $this->getCaseApiUri("CFDocuments", $associationDestinationNode->source_document_id,true,$orgCode,$this->domainName) : ""
                    ];
                }else{
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->title) ? $associationDestinationNode->title : "NA",
                        "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                        "uri"           =>  !empty($associationDestinationNode->source_document_id) ?  $this->getCaseApiUri("CFDocuments", $associationDestinationNode->source_document_id,true,$orgCode,$this->domainName) : ""
                    ];
                }

            }   else    {
                $destinationNodeURIEntity = [
                    "title"         =>  "NA",
                    "identifier"    =>  !empty($associationDestinationNode->source_document_id) ?  $associationDestinationNode->source_document_id : "",
                    "uri"           =>  !empty($associationDestinationNode->source_document_id) ?  $this->getCaseApiUri("CFDocuments", $associationDestinationNode->source_document_id,true,$orgCode,$this->domainName) : ""
                ];
            }
                    
        }
        else{
            $orgCode                =   $this->getOrgCode();
            $associationDestinationNode = !empty($itemAssociationDetails->targetItemId) ? $itemAssociationDetails->targetItemId : [];
            //dd($associationDestinationNode);
            //GET THE METADATA LIST FOR THE ORIGIN NODE
            //$metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($associationDestinationNode->node_type_id);
            //$this->setMetadataList($metadataList);
            $getImportType = $this->documentImportType($itemAssociationDetails->document_id,$itemAssociationDetails->organization_id);
            $arrayOfInternalName    =   $this->parseMetadata();
            if(in_array('full_statement', $arrayOfInternalName) === true) {
                if($getImportType->import_type==1){
                    $getItemUrl = $this->getUriFromItem($associationDestinationNode->document_id,$itemAssociationDetails->organization_id,$associationDestinationNode->item_id);
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->full_statement) ? $associationDestinationNode->full_statement : "",
                        "identifier"    =>  !empty($associationDestinationNode->source_item_id) ? $associationDestinationNode->source_item_id : "",
                        "uri"           =>  !empty($getItemUrl->uri) ? $getItemUrl->uri : "",
                    ];
                }else if($getImportType->uri==2)
                {
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->full_statement) ? $associationDestinationNode->full_statement : "",
                        "identifier"    =>  !empty($associationDestinationNode->source_item_id) ? $associationDestinationNode->source_item_id : "",
                        "uri"           =>  !empty($associationDestinationNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationDestinationNode->source_item_id,true,$orgCode,$this->domainName) : "",
                    ];
                }else{
                    $destinationNodeURIEntity = [
                        "title"         =>  !empty($associationDestinationNode->full_statement) ? $associationDestinationNode->full_statement : "",
                        "identifier"    =>  !empty($associationDestinationNode->source_item_id) ? $associationDestinationNode->source_item_id : "",
                        "uri"           =>  !empty($associationDestinationNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationDestinationNode->source_item_id,true,$orgCode,$this->domainName) : "",
                    ];
                }

            }   else    {
                $destinationNodeURIEntity = [
                    "title"         =>  "",
                    "identifier"    =>  !empty($associationDestinationNode->source_item_id) ? $associationDestinationNode->source_item_id : "",
                    "uri"           =>  !empty($associationDestinationNode->source_item_id) ? $this->getCaseApiUri("CFItems", $associationDestinationNode->source_item_id,true, $orgCode,$this->domainName) : "",
                ];
            }
               
        }

        return $destinationNodeURIEntity;
    }

    /**
     * Set AssociationGroupEntity for an association model
     */
    private function transformAndSetAssociationGroupingEntity($associationIdentifier) {
        $associationDetails = $this->itemAssociationRepository->getItemAssociationAndRelatedModelBySourceId($associationIdentifier);
        $associationGroupDetail = !empty($associationDetails->associationGroup) ? $associationDetails->associationGroup : [];
        $getImportType = $this->documentImportType($associationDetails->document_id,$associationDetails->organization_id);
        $orgCode                =   $this->getOrgCode();
        // parse out the SubjectURI related info
        if($getImportType->import_type==1){
            $groupUri = $this->getUriFromGroup($associationDetails->association_group_id,$associationDetails->organization_id);
            $CFAssociationGroupingURIEntity = [
                "title" => !empty($associationGroupDetail->title) ? $associationGroupDetail->title : "",
                "identifier" => !empty($associationGroupDetail->source_association_group_id) ? $associationGroupDetail->source_association_group_id : "",
                "uri" => !empty($groupUri->uri)?$groupUri->uri:"",
            ];
        }else if($getImportType->import_type==2){
            $CFAssociationGroupingURIEntity = [
                "title" => !empty($associationGroupDetail->title) ? $associationGroupDetail->title : "",
                "identifier" => !empty($associationGroupDetail->source_association_group_id) ? $associationGroupDetail->source_association_group_id : "",
                "uri" => !empty($associationGroupDetail->source_association_group_id) ? $this->getCaseApiUri("CFAssociationGroupings", $associationGroupDetail->source_association_group_id,true,$orgCode,$this->domainName) : ""
            ];

        }else{
            $CFAssociationGroupingURIEntity = [
                "title" => !empty($associationGroupDetail->title) ? $associationGroupDetail->title : "",
                "identifier" => !empty($associationGroupDetail->source_association_group_id) ? $associationGroupDetail->source_association_group_id : "",
                "uri" => !empty($associationGroupDetail->source_association_group_id) ? $this->getCaseApiUri("CFAssociationGroupings", $associationGroupDetail->source_association_group_id,true,$orgCode,$this->domainName) : ""
            ];

        }

        return $CFAssociationGroupingURIEntity;
    }

    private function transFormToCaseStandardStructure($domainName){
        $associations = $this->getAssociationDetails();
        $associationType = $this->returnAssociationType($associations->association_type);
        $orgCode         =   $this->getOrgCode();
        $getImportType = $this->documentImportType($associations->document_id,$associations->organization_id);
        // create the CFItemAssociations
        $CFAssociation = [
            "identifier" => $associations->source_item_association_id,
            "associationType" => $associationType,
            "CFDocumentURI" => $this->transformAndSetDocumentUri($associations->source_item_association_id),
            "sequenceNumber" => $associations->sequence_number,
            "uri" => ($getImportType->import_type==1)?$associations->uri:$this->getCaseApiUri("CFAssociations", $associations->source_item_association_id,true,$orgCode,$domainName),
            "originNodeURI" => $this->transformAndSetOriginNodeEntity($associations->source_item_association_id),
            "destinationNodeURI" => $this->transformAndSetDestinationNodeEntity($associations->source_item_association_id),
            "CFAssociationGroupingURI" => $this->transformAndSetAssociationGroupingEntity($associations->source_item_association_id),
            "lastChangeDateTime" => $this->formatDateTimeToISO8601($associations->updated_at),
        ];

        return $CFAssociation;
        
    }

    public function documentImportType($documentId,$organizationId)
    {
        $query = DB::table('documents')
             ->select('import_type','uri')
             ->where('document_id',$documentId)
             ->where('organization_id',$organizationId)
             ->where('is_deleted',0)
             ->get()
             ->first();
        return $query;
    }


    public function getUriFromAssociation($documentId,$organizationId,$sourceAssociationId)
    {
        $associationUrl =  DB::table('item_associations')->select('uri')
                  ->where('source_item_association_id',$sourceAssociationId)
                  ->where('organization_id',$organizationId)
                  ->where('source_document_id',$documentId)
                  ->where('is_deleted',0)
                  ->get()
                  ->first();
        return $associationUrl;
    }

    public function getUriFromItem($documentId,$organizationId,$itemId){
        $itemUrl =  DB::table('items')->select('uri')
            ->where('item_id',$itemId)
            ->where('organization_id',$organizationId)
            ->where('document_id',$documentId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $itemUrl;
    }

    public function getUriFromGroup($groupId,$organizationId){
        $groupUrl =  DB::table('association_groups')->select('uri')
            ->where('association_group_id',$groupId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->get()
            ->first();
        return $groupUrl;
    }
}
