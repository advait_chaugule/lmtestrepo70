<?php


namespace App\Domains\CaseStandard\Jobs;


use Lucid\Foundation\Job;
use DB;
class ValidateSourceNodeIdFromItemTableJob extends Job
{
    private $sourceIdentifier;
    public function __construct(string $sourceIdentifier)
    {
        $this->sourceIdentifier = $sourceIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('items')->select('source_node_type_uri_object')
            ->where('source_node_type_uri_object','like','%'.$this->sourceIdentifier.'%')
            ->where('is_deleted',0)
            ->get()
            ->first();
        $nodeTypeId      = !empty($query->node_type_id)?$query->node_type_id:$this->sourceIdentifier;
       if($nodeTypeId != "")
       {
           return true;
       }else{
           return false;
       }
    }

}