<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\AssociationGroupRepositoryInterface;

class ValidateCFAssociationGroupBySourceIdJob extends Job
{
    private $sourceIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $sourceIdentifier)
    {
        $this->sourceIdentifier = $sourceIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssociationGroupRepositoryInterface $repository)
    {
        $dataToValidate = [ "source_association_group_id" =>  $this->sourceIdentifier, "is_deleted" => '0' ];
        $data = $repository->findByAttributes($dataToValidate);
        return $data->isNotEmpty();
    }
}
