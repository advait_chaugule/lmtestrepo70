<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Collection;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\UuidHelperTrait;

class CloneCasePackageDataJob extends Job
{
    use ArrayHelper, UuidHelperTrait;

    private $packageJson;
    private $packageArray;

    private $newSubjectUris;
    private $newLicenseUris;
    private $newItemTypeUris;
    private $newConceptUris;
    private $newAssociationGroupings;
    private $newDocumentUri;
    private $newItemUris;
    private $newAssociations;

    private $oldDocumentMappingWithNew;

    private $clonedCasePackage;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $packageJson)
    {
        $this->setPackageJson($packageJson);
        $this->helperToCreateAndSetPackageArray();
    }

    public function setPackageJson(string $data) {
        $this->packageJson = $data;
    }

    public function getPackageJson(): string {
        return $this->packageJson;
    }

    public function setPackageArray(array $data) {
        $this->packageArray = $data;
    }

    public function getPackageArray(): array {
        return $this->packageArray;
    }

    public function setNewSubjectUris(array $data) {
        $this->newSubjectUris = $data;
    }

    public function getNewSubjectUris(): array {
        return $this->newSubjectUris;
    }

    public function setNewLicenseUris(array $data) {
        $this->newLicenseUris = $data;
    }

    public function getNewLicenseUris(): array {
        return $this->newLicenseUris;
    }

    public function setNewItemTypeUris(array $data) {
        $this->newItemTypeUris = $data;
    }

    public function getNewItemTypeUris(): array {
        return $this->newItemTypeUris;
    }

    public function setNewConceptUris(array $data) {
        $this->newConceptUris = $data;
    }

    public function getNewConceptUris(): array {
        return $this->newConceptUris;
    }

    public function setNewAssociationGroupings(array $data) {
        $this->newAssociationGroupings = $data;
    }

    public function getNewAssociationGroupings(): array {
        return $this->newAssociationGroupings;
    }

    public function setNewDocumentUri(array $data) {
        $this->newDocumentUri = $data;
    }

    public function getNewDocumentUri(): array {
        return $this->newDocumentUri;
    }

    public function setNewItemUris(array $data) {
        $this->newItemUris = $data;
    }

    public function getNewItemUris(): array {
        return $this->newItemUris;
    }
    

    public function setNewAssociations(array $data) {
        $this->newAssociations = $data;
    }

    public function getNewAssociations(): array {
        return $this->newAssociations;
    }

    public function setOldDocumentMappingWithNew(array $data) {
        $this->oldDocumentMappingWithNew = $data;
    }

    public function getOldDocumentMappingWithNew(): array {
        return $this->oldDocumentMappingWithNew;
    }

    public function setClonedCasePackage(array $data){
        $this->clonedCasePackage = $data;
    }

    public function getClonedCasePackage(): array{
        return $this->clonedCasePackage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->createSubjectIdentifierForEachSubjectAndUpdateThePackageArray();
        $this->createLicenseIdentifierForEachLicenseAndUpdateThePackageArray();
        $this->createItemTypeIdentifierForEachItemTypeAndUpdateThePackageArray();
        $this->createConceptIdentifierForEachConceptAndUpdateThePackageArray();
        $this->createAssociationGroupIdentifierForEachAssociationGroupAndUpdateThePackageArray();
        $this->createDocumentIdentifierAndUpdateThePackageArray();
        $this->createItemIdentifierForEachItemAndUpdateThePackageArray();
        $this->createAssociationIdentifierForEachAssociationAndUpdateThePackageArray();
        $this->prepareAndSetClonedPackage();
        return $this->getClonedCasePackage();
    }

    private function createSubjectIdentifierForEachSubjectAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageSubjects = $packageData["CFDefinitions"]["CFSubjects"];
        $newlyCreatedSubjectUris = [];
        if(!empty($packageSubjects)) {
            foreach($packageSubjects as $key=>$oldSubject) {
                $newSubjectIdentifier = $this->createUniversalUniqueIdentifier();
                $oldSubjectIdentifier = $oldSubject["identifier"];
                $newSubjectUri = str_replace($oldSubjectIdentifier, $newSubjectIdentifier, $oldSubject["uri"]);
                $title = !empty($oldSubject["title"]) ? $oldSubject["title"] : "";
                // override the relevant subject identifier, uri and title
                $packageData["CFDefinitions"]["CFSubjects"][$key]["title"] = $title;
                $packageData["CFDefinitions"]["CFSubjects"][$key]["identifier"] = $newSubjectIdentifier;
                $packageData["CFDefinitions"]["CFSubjects"][$key]["uri"] = $newSubjectUri;
                
                // create set of subject uri objects and map them with old subject identifier
                $newlyCreatedSubjectUris[$oldSubjectIdentifier] = [
                    "identifier" => $newSubjectIdentifier,
                    "title" => $title,
                    "uri" => $newSubjectUri
                ];
            }
        }
        // set the newly created subject uris
        $this->setNewSubjectUris($newlyCreatedSubjectUris);
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createLicenseIdentifierForEachLicenseAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageLicenses = $packageData["CFDefinitions"]["CFLicenses"];
        $newlyCreatedLicenseUris = [];
        if(!empty($packageLicenses)) {
            foreach($packageLicenses as $key=>$oldLicense) {
                $newLicenseIdentifier = $this->createUniversalUniqueIdentifier();
                $oldLicenseIdentifier = $oldLicense["identifier"];
                $newLicenseUri = str_replace($oldLicenseIdentifier, $newLicenseIdentifier, $oldLicense["uri"]);
                $title = !empty($oldLicense["title"]) ? $oldLicense["title"] : "";
                // override the relevant license identifier, uri and title
                $packageData["CFDefinitions"]["CFLicenses"][$key]["title"] = $title;
                $packageData["CFDefinitions"]["CFLicenses"][$key]["identifier"] = $newLicenseIdentifier;
                $packageData["CFDefinitions"]["CFLicenses"][$key]["uri"] = $newLicenseUri;
                
                // create set of license uri objects and map them with old license identifier
                $newlyCreatedLicenseUris[$oldLicenseIdentifier] = [
                    "identifier" => $newLicenseIdentifier,
                    "title" => $title,
                    "uri" => $newLicenseUri
                ];
            }
        }
        // set the newly created license uris
        $this->setNewLicenseUris($newlyCreatedLicenseUris);
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createItemTypeIdentifierForEachItemTypeAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageItemTypes = $packageData["CFDefinitions"]["CFItemTypes"];
        $newlyCreatedItemTypeUris = [];
        if(!empty($packageItemTypes)) {
            foreach($packageItemTypes as $key=>$oldItemType) {
                $newItemTypeIdentifier = $this->createUniversalUniqueIdentifier();
                $oldItemTypeIdentifier = $oldItemType["identifier"];
                $newItemTypeUri = str_replace($oldItemTypeIdentifier, $newItemTypeIdentifier, $oldItemType["uri"]);
                $title = !empty($oldItemType["title"]) ? $oldItemType["title"] : "";
                // override the relevant item type identifier, uri and title
                $packageData["CFDefinitions"]["CFItemTypes"][$key]["title"] = $title;
                $packageData["CFDefinitions"]["CFItemTypes"][$key]["identifier"] = $newItemTypeIdentifier;
                $packageData["CFDefinitions"]["CFItemTypes"][$key]["uri"] = $newItemTypeUri;
                
                // create set of item type uri objects and map them with old item type identifier
                $newlyCreatedItemTypeUris[$oldItemTypeIdentifier] = [
                    "identifier" => $newItemTypeIdentifier,
                    "title" => $title,
                    "uri" => $newItemTypeUri
                ];
            }
        }
        // set the newly created item type uris
        $this->setNewItemTypeUris($newlyCreatedItemTypeUris);
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createConceptIdentifierForEachConceptAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageConcepts = $packageData["CFDefinitions"]["CFConcepts"];
        $newlyCreatedConceptUris = [];
        if(!empty($packageConcepts)) {
            foreach($packageConcepts as $key=>$oldConcept) {
                $newConceptIdentifier = $this->createUniversalUniqueIdentifier();
                $oldConceptIdentifier = $oldConcept["identifier"];
                $newConceptUri = str_replace($oldConceptIdentifier, $newConceptIdentifier, $oldConcept["uri"]);
                $title = !empty($oldConcept["title"]) ? $oldConcept["title"] : "";
                // override the relevant concept identifier, uri and title
                $packageData["CFDefinitions"]["CFConcepts"][$key]["title"] = $title;
                $packageData["CFDefinitions"]["CFConcepts"][$key]["identifier"] = $newConceptIdentifier;
                $packageData["CFDefinitions"]["CFConcepts"][$key]["uri"] = $newConceptUri;
                // create set of concept uri objects and map them with old concept identifier
                $newlyCreatedConceptUris[$oldConceptIdentifier] = [
                    "identifier" => $newConceptIdentifier,
                    "title" => $title,
                    "uri" => $newConceptUri
                ];
            }
        }
        // set the newly created concept uris
        $this->setNewConceptUris($newlyCreatedConceptUris);
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createAssociationGroupIdentifierForEachAssociationGroupAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageAssociationGroupings = $packageData["CFDefinitions"]["CFAssociationGroupings"];
        $newlyCreatedAssociationGroupings = [];
        if(!empty($packageAssociationGroupings)) {
            foreach($packageAssociationGroupings as $key=>$oldAssociationGroup) {
                $newAssociationGroupingIdentifier = $this->createUniversalUniqueIdentifier();
                $oldAssociationGroupingIdentifier = $oldAssociationGroup["identifier"];
                $newAssociationGroupUri = str_replace($oldAssociationGroupingIdentifier, $newAssociationGroupingIdentifier, $oldAssociationGroup["uri"]);
                $title = !empty($oldAssociationGroup["title"]) ? $oldAssociationGroup["title"] : "";
                // override the relevant concept identifier, uri and title
                $packageData["CFDefinitions"]["CFAssociationGroupings"][$key]["title"] = $title;
                $packageData["CFDefinitions"]["CFAssociationGroupings"][$key]["identifier"] = $newAssociationGroupingIdentifier;
                $packageData["CFDefinitions"]["CFAssociationGroupings"][$key]["uri"] = $newAssociationGroupUri;
                
                // create set of concept uri objects and map them with old concept identifier
                $newlyCreatedAssociationGroupings[$oldAssociationGroupingIdentifier] = [
                    "identifier" => $newAssociationGroupingIdentifier,
                    "title" => $title,
                    "uri" => $newAssociationGroupUri
                ];
            }
        }
        // set the newly created concept uris
        $this->setNewAssociationGroupings($newlyCreatedAssociationGroupings);
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createDocumentIdentifierAndUpdateThePackageArray() {
        $newDocumentIdentifier = $this->createUniversalUniqueIdentifier();
        $packageData = $this->getPackageArray();
        $packageDocument = $packageData["CFDocument"];
        $oldDocumentIdentifier = $packageDocument["identifier"];
        $documentUri = str_replace($oldDocumentIdentifier, $newDocumentIdentifier, $packageData["CFDocument"]["uri"]);
        // override the identifier and uri
        $packageData["CFDocument"]["identifier"] = $newDocumentIdentifier;
       
        // override document's subject uris
        if(!empty($packageData["CFDocument"]["subjectURI"])) {
            $newSubjectUris = [];
            foreach($packageData["CFDocument"]["subjectURI"] as $subject) {
                $newSubjectUris[] = $this->helperToSearchForTransformedEntityOfProvidedType($subject["identifier"], "subject");
            }
            $packageData["CFDocument"]["subjectURI"] = $newSubjectUris;
        }

        // override document's license uri
        if(!empty($packageData["CFDocument"]["licenseURI"])) {
            $licenseIdentifierToSearchFor = $packageData["CFDocument"]["licenseURI"]["identifier"];
            $packageData["CFDocument"]["licenseURI"] = 
            $this->helperToSearchForTransformedEntityOfProvidedType($licenseIdentifierToSearchFor, "license");
        }
        
        // override CFPackageURI
        $packageData["CFDocument"]["CFPackageURI"]["identifier"] = $newDocumentIdentifier;
        $packageData["CFDocument"]["CFPackageURI"]["uri"] = str_replace($oldDocumentIdentifier, $newDocumentIdentifier, $packageData["CFDocument"]["CFPackageURI"]["uri"]);
        
        // set newly created document as DocumentUri object
        $newDocumentUri = [
            "identifier" => $newDocumentIdentifier,
            "title" => $packageData["CFDocument"]["title"],
            "uri" => $packageData["CFDocument"]["uri"]
        ];
        $this->setNewDocumentUri($newDocumentUri);

        // this has been done for a quick fix (keep a map of the transformed document with old one)
        $this->setOldDocumentMappingWithNew([$oldDocumentIdentifier => $newDocumentUri]);

        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createItemIdentifierForEachItemAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageItems = $packageData["CFItems"];
        $newlyCreatedDocumentUri = $this->getNewDocumentUri();
        $newlyCreatedItemUris = [];
//print_r($packageItems); die;
        if(!empty($packageItems)) {
            foreach($packageItems as $key=>$oldItem) {
                $newItemIdentifier = $this->createUniversalUniqueIdentifier();
                $oldItemIdentifier = $oldItem["identifier"];
                $newItemUri = str_replace($oldItemIdentifier, $newItemIdentifier, $oldItem["uri"]);
                $humanCodingScheme = !empty($oldItem["humanCodingScheme"]) ? $oldItem["humanCodingScheme"] : "";
                $listEnumeration = !empty($oldItem["listEnumeration"]) ? $oldItem["listEnumeration"] : "";
                // override the relevant item identifier and uri
                $packageData["CFItems"][$key]["identifier"] = $newItemIdentifier;
                $packageData["CFItems"][$key]["uri"] = $oldItem["uri"];
                $packageData["CFItems"][$key]["CFDocumentURI"] = $newlyCreatedDocumentUri;
    
                // override the relevant item concept uri
                if(!empty($packageData["CFItems"][$key]["conceptKeywordsURI"])) {
                    $conceptIdentifierToSearchFor = $packageData["CFItems"][$key]["conceptKeywordsURI"]["identifier"];
                    $packageData["CFItems"][$key]["conceptKeywordsURI"] = 
                    $this->helperToSearchForTransformedEntityOfProvidedType($conceptIdentifierToSearchFor, "concept");
                }
    
                // override the relevant item type uri
                if(!empty($packageData["CFItems"][$key]["CFItemTypeURI"])) {
                    $itemTypeIdentifierToSearchFor = $packageData["CFItems"][$key]["CFItemTypeURI"]["identifier"];
                    $packageData["CFItems"][$key]["CFItemTypeURI"] = 
                    $this->helperToSearchForTransformedEntityOfProvidedType($itemTypeIdentifierToSearchFor, "itemType");
                }
    
                // override the relevant licenset uri
                if(!empty($packageData["CFItems"][$key]["licenseURI"])) {
                    $licenseIdentifierToSearchFor = $packageData["CFItems"][$key]["licenseURI"]["identifier"];
                    $packageData["CFItems"][$key]["licenseURI"] = 
                    $this->helperToSearchForTransformedEntityOfProvidedType($licenseIdentifierToSearchFor, "license");
                }
                
                // create set of item uri objects and map them with old item identifier
                $newlyCreatedItemUris[$oldItemIdentifier] = [
                    "identifier" => $newItemIdentifier,
                    "title" => $humanCodingScheme ?: $listEnumeration,
                    "uri" => $oldItem["uri"],
                ];
            }
        }
        // set the newly created item uris
        $this->setNewItemUris($newlyCreatedItemUris);
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function createAssociationIdentifierForEachAssociationAndUpdateThePackageArray() {
        $packageData = $this->getPackageArray();
        $packageAssociations = $packageData["CFAssociations"];
        $newlyCreatedDocumentUri = $this->getNewDocumentUri();
        if(!empty($packageAssociations)) {
            foreach($packageAssociations as $key=>$oldAssociation) {
                $newAssociationIdentifier = $this->createUniversalUniqueIdentifier();
                $oldAssociationIdentifier = $oldAssociation["identifier"];
                $newAssociationUri = str_replace($oldAssociationIdentifier, $newAssociationIdentifier, $oldAssociation["uri"]);
                
                // override the relevant association identifier and uri
                $packageData["CFAssociations"][$key]["identifier"] = $newAssociationIdentifier;
                $packageData["CFAssociations"][$key]["uri"] = $newAssociationUri;
                $packageData["CFAssociations"][$key]["CFDocumentURI"] = $newlyCreatedDocumentUri;
    
                // override the relevant association grouping 
                if(!empty($packageData["CFAssociations"][$key]["CFAssociationGroupingURI"])) {
                    $oldAssociationGroupingUri = $oldAssociation["CFAssociationGroupingURI"]["identifier"];
                    $packageData["CFAssociations"][$key]["CFAssociationGroupingURI"] = 
                    $this->helperToSearchForTransformedEntityOfProvidedType($oldAssociationGroupingUri, "associationGroup");
                }
                
                // override the origin node uri with the updated ones (either document or item)
                if(!empty($packageData["CFAssociations"][$key]["originNodeURI"])) {
                    $searchOriginIdentifier = $packageData["CFAssociations"][$key]["originNodeURI"]["identifier"];
                    $documentUri = $this->helperToSearchForTransformedEntityOfProvidedType($searchOriginIdentifier, "document");
                    if(!empty($documentUri)) {
                        $originNodeUri = $documentUri;
                    }
                    else {
                        $originNodeUri = $this->helperToSearchForTransformedEntityOfProvidedType($searchOriginIdentifier, "item");

                        if(empty($originNodeUri)){
                            $originNodeUri =  $packageData["CFAssociations"][$key]["originNodeURI"];
                        }
                    }
                    $packageData["CFAssociations"][$key]["originNodeURI"] = $originNodeUri;
                }
                          
                // override the destination node uri with the updated ones (either document or item)
                if(!empty($packageData["CFAssociations"][$key]["destinationNodeURI"])) {
                    $searchDestinationIdentifier = !empty($packageData["CFAssociations"][$key]["destinationNodeURI"]["identifier"]) ? 
                                                    $packageData["CFAssociations"][$key]["destinationNodeURI"]["identifier"] : 
                                                    "";
                    $documentUri = $this->helperToSearchForTransformedEntityOfProvidedType($searchDestinationIdentifier, "document");
                    if(!empty($documentUri)) {
                        $destinationNodeUri = $documentUri;
                    }
                    else {
                        $destinationNodeUri = $this->helperToSearchForTransformedEntityOfProvidedType($searchDestinationIdentifier, "item");
                    }

                    // hook here to set destination node uri for association type exemplar
                    if($packageData["CFAssociations"][$key]["associationType"]==="exemplar") {
                        $destinationNodeUri = !empty($packageData["CFAssociations"][$key]["destinationNodeURI"]) ? 
                                              $packageData["CFAssociations"][$key]["destinationNodeURI"] : 
                                              "";
                    }

                    // if the associated entity is not a part of the uploaded taxonomy then keep as it is
                    // $destinationNodeUri will be blank when this scenario will occur
                    if(!empty($destinationNodeUri)) {
                        $packageData["CFAssociations"][$key]["destinationNodeURI"] = $destinationNodeUri;
                    }
                }
            }
        }
        // override the package with updated data
        $this->setPackageArray($packageData);
    }

    private function prepareAndSetClonedPackage() {
        $clonedPackageData = [
            "packageData" => $this->getPackageArray(),
            "oldNewItemMapping" => $this->getNewItemUris()
        ];
        $this->setClonedCasePackage($clonedPackageData);
    }

    private function helperToCreateAndSetPackageArray() {
        $packageJson = $this->getPackageJson();
        $convertPackageJsonToArray = $this->convertJsonStringToArray($packageJson);
        $this->setPackageArray($convertPackageJsonToArray);
    }

    private function helperToSearchForTransformedEntityOfProvidedType(string $identifier, string $type): array {
        switch ($type) {
            case 'subject':
                $data = $this->getNewSubjectUris();
                break;
            case 'license':
                $data = $this->getNewLicenseUris();
                break;
            case 'itemType':
                $data = $this->getNewItemTypeUris();
                break;
            case 'concept':
                $data = $this->getNewConceptUris();
                break;
            case 'associationGroup':
                $data = $this->getNewAssociationGroupings();
                break;
            case 'item':
                $data = $this->getNewItemUris();
                break;
            case 'document':
                $data = $this->getOldDocumentMappingWithNew();
                break;
            default:
                $data = [];
                break;
        }
        return !empty($data[$identifier]) ? $data[$identifier] : [];
    }
}
