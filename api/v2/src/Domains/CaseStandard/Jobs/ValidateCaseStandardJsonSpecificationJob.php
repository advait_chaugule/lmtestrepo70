<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\StringHelper;

class ValidateCaseStandardJsonSpecificationJob extends Job
{
    use ArrayHelper, StringHelper;

    private $inputData;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->inputData = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // verify if case specifications has been made
        $caseSpecifiedVerificationStatus = $this->verifyContentCompliancyWithCASESpecifications($this->inputData);
        $validationDetails['status'] = $caseSpecifiedVerificationStatus;
        $validationDetails['message'] = $caseSpecifiedVerificationStatus ? "Valid content." : "Content is not compliant with CASE specifications.";
        unset($this->inputData);

        return $validationDetails;
    }

    /**
     * This method will only check outer level atributes specified in CASE standard
     */
    private function verifyContentCompliancyWithCASESpecifications(array $content): bool {
        $cfDocumentStatus = false;
        if(isset($content['CFDocument'])) {
            //$cfDocumentStatus = $this->validateCFDocument($content['CFDocument']);
            $cfDocumentStatus = true;
        }

        $cfItemsStatusStatus = false;
        if(isset($content['CFItems'])) {
            // set true always since CFItems key exists
            $cfItemsStatusStatus = true;
           /* $cfItems = $content['CFItems'];
            foreach($cfItems as $item) {
                $cfItemsStatusStatus = $this->validateEachCFItem($item);
                if(!$cfItemsStatusStatus) {
                    break;
                }
            }*/
        }
        
      //  $cfAssociationsStatus = isset($content['CFAssociations']) ? true : false;
      // $cfAssociationsStatus = isset($content['CFAssociations']) ? true : true;
          $cfAssociationsStatus = true;
          $cfDefinitionsStatus = isset($content['CFDefinitions']) ? true : false;

        return $cfDocumentStatus && $cfItemsStatusStatus && $cfAssociationsStatus && $cfDefinitionsStatus;
    }

    private function validateCFDocument(array $cfDocument): bool {
        $cfDocumentIdentifierStatus = !empty($cfDocument['identifier']) ? true : false;
        $cfDocumentUriStatus = !empty($cfDocument['uri']) ? true : false;
        //$cfDocumentCreatorStatus = !empty($cfDocument['creator']) ? true : false;
        $cfDocumentTitleStatus = !empty($cfDocument['title']) ? true : false;

        $cfDocumentCfPackageUriTitleStatus = !empty($cfDocument['CFPackageURI']['title']) ? true : false;
        $cfDocumentCfPackageUriIdentifierStatus = !empty($cfDocument['CFPackageURI']['identifier']) ? true : false;
        $cfDocumentCfPackageUriStatus = !empty($cfDocument['CFPackageURI']['uri']) ? true : false;
        
        return $cfDocumentIdentifierStatus && $cfDocumentUriStatus && $cfDocumentTitleStatus && 
                $cfDocumentCfPackageUriTitleStatus && $cfDocumentCfPackageUriIdentifierStatus && 
                $cfDocumentCfPackageUriStatus;
    }

    private function validateEachCFItem(array $cfItem): bool {
        $cfItemIdentifierStatus = !empty($cfItem['identifier']) ? true : false;
        $cfItemUriStatus = !empty($cfItem['uri']) ? true : false;
        $cfItemFullStatementStatus = !empty($cfItem['fullStatement']) ? true : false;

        $cfDocumentUriIdentifierStatus = !empty($cfItem['CFDocumentURI']['identifier']) ? true : false;
        $cfDocumentUriTitleStatus = !empty($cfItem['CFDocumentURI']['title']) ? true : false;
        $cfDocumentUriStatus = !empty($cfItem['CFDocumentURI']['uri']) ? true : false;

        return $cfItemIdentifierStatus && $cfItemUriStatus && $cfItemFullStatementStatus && 
               $cfDocumentUriIdentifierStatus && $cfDocumentUriTitleStatus && $cfDocumentUriStatus;
    }
}
