<?php
namespace App\Domains\CaseStandard\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use DB;

class ValidateLicenseBySourceIdJob extends Job
{
    private $sourceIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $sourceIdentifier)
    {
        $this->sourceIdentifier = $sourceIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LicenseRepositoryInterface $repository)
    {
        $result = DB::table('licenses')->select('license_id')->where('source_license_id', $this->sourceIdentifier)->orWhere('license_id', $this->sourceIdentifier)->count();
        if($result > 0)
        {
            $flag = true;
        }
        else {
            $flag = false;
        }
        return $flag;
    }
}
