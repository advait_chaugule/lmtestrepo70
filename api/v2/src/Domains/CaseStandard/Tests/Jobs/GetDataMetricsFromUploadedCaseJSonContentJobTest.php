<?php
namespace App\Domains\CaseStandard\Tests\Jobs;

use App\Domains\CaseStandard\Jobs\GetDataMetricsFromUploadedCaseJSonContentJob;
use Tests\TestCase;

class GetDataMetricsFromUploadedCaseJSonContentJobTest extends TestCase
{
    public function test_ContentToProcess()
    {
        $assets = new GetDataMetricsFromUploadedCaseJSonContentJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setContentToProcess($value);
        $this->assertEquals($value,$assets->getContentToProcess());
    }
}
