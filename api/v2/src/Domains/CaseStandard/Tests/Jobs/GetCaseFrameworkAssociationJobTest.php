<?php
namespace App\Domains\Casestandard\Tests\Jobs;

use App\Domains\Casestandard\Jobs\GetCaseFrameworkAssociationJob;
use Tests\TestCase;

class GetCaseFrameworkAssociationJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetCaseFrameworkAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_AssociationDetails()
    {
        $assets = new GetCaseFrameworkAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssociationDetails($value);
        $this->assertEquals($value,$assets->getAssociationDetails());
    }

    public function test_MetadataList()
    {
        $assets = new GetCaseFrameworkAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataList($value);
        $this->assertEquals($value,$assets->getMetadataList());
    }
}
