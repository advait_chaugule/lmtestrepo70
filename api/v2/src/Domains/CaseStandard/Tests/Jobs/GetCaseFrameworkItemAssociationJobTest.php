<?php
namespace App\Domains\Casestandard\Tests\Jobs;

use App\Domains\Casestandard\Jobs\GetCaseFrameworkItemAssociationJob;
use Tests\TestCase;

class GetCaseFrameworkItemAssociationJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_ItemAssociationDetails()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemAssociationDetails($value);
        $this->assertEquals($value,$assets->getItemAssociationDetails());
    }

    public function test_Documenturi()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumenturi($value);
        $this->assertEquals($value,$assets->getDocumenturi());
    }

    public function test_OriginNodeUri()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOriginNodeUri($value);
        $this->assertEquals($value,$assets->getOriginNodeUri());
    }

    public function test_DestinationNodeUri()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDestinationNodeUri($value);
        $this->assertEquals($value,$assets->getDestinationNodeUri());
    }

    public function test_AssociationGroupingEntity()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setAssociationGroupingEntity($value);
        $this->assertEquals($value,$assets->getAssociationGroupingEntity());
    }

    public function test_MetadataList()
    {
        $assets = new GetCaseFrameworkItemAssociationJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataList($value);
        $this->assertEquals($value,$assets->getMetadataList());
    }
}
