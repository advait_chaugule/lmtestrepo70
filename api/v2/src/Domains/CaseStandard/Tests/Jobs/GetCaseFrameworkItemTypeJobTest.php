<?php
namespace App\Domains\Casestandard\Tests\Jobs;

use App\Domains\Casestandard\Jobs\GetCaseFrameworkItemTypeJob;
use Tests\TestCase;

class GetCaseFrameworkItemTypeJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetCaseFrameworkItemTypeJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_NodeTypeDetails()
    {
        $assets = new GetCaseFrameworkItemTypeJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypeDetails($value);
        $this->assertEquals($value,$assets->getNodeTypeDetails());
    }
}
