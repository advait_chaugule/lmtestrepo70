<?php
namespace App\Domains\CaseStandard\Tests\Jobs;

use App\Domains\CaseStandard\Jobs\GetCasePackageJob;
use Tests\TestCase;

class GetCasePackageJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new GetCasePackageJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }
}
