<?php
namespace App\Domains\CaseStandard\Tests\Jobs;
use App\Domains\CaseStandard\Jobs\GetAllCaseFrameworkDocumentsJob;
use Tests\TestCase;

class GetAllCaseFrameworkDocumentsJobTest extends TestCase
{
    public function test_Documents()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocuments($value);
        $this->assertEquals($value,$assets->getDocuments());
    }

    public function test_DocumentDetails()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentDetails($value);
        $this->assertEquals($value,$assets->getDocumentDetails());
    }

    public function test_NodeTypes()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNodeTypes($value);
        $this->assertEquals($value,$assets->getNodeTypes());
    }

    public function test_MetadataList()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataList($value);
        $this->assertEquals($value,$assets->getMetadataList());
    }

    public function test_SubjectEntity()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setSubjectEntity($value);
        $this->assertEquals($value,$assets->getSubjectEntity());
    }

    public function test_SubjectURIEntity()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setSubjectURIEntity($value);
        $this->assertEquals($value,$assets->getSubjectURIEntity());
    }

    public function test_LicenseURIEntity()
    {
        $assets = new GetAllCaseFrameworkDocumentsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setLicenseURIEntity($value);
        $this->assertEquals($value,$assets->getLicenseURIEntity());
    }
}
