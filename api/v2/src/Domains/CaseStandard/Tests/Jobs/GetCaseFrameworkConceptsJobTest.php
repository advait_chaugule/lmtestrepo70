<?php
namespace App\Domains\Casestandard\Tests\Jobs;

use App\Domains\Casestandard\Jobs\GetCaseFrameworkConceptsJob;
use Tests\TestCase;

class GetCaseFrameworkConceptsJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetCaseFrameworkConceptsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_ConceptDetails()
    {
        $assets = new GetCaseFrameworkConceptsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setConceptDetails($value);
        $this->assertEquals($value,$assets->getConceptDetails());
    }

    public function test_MetadataList()
    {
        $assets = new GetCaseFrameworkConceptsJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','https://api.acmt-dev.learningmate.com/server');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataList($value);
        $this->assertEquals($value,$assets->getMetadataList());
    }
}
