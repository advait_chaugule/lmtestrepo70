<?php
namespace App\Domains\CaseStandard\Tests\Jobs;

use App\Domains\CaseStandard\Jobs\GetCaseFrameworkItemJob;
use Tests\TestCase;

class GetCaseFrameworkItemJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_MetadataList()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataList($value);
        $this->assertEquals($value,$assets->getMetadataList());
    }

    public function test_ItemDetails()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemDetails($value);
        $this->assertEquals($value,$assets->getItemDetails());
    }

    public function test_ItemEntity()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemEntity($value);
        $this->assertEquals($value,$assets->getItemEntity());
    }

    public function test_Item()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItem($value);
        $this->assertEquals($value,$assets->getItem());
    }

    public function test_CFItemTypeURIEntity()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCFItemTypeURIEntity($value);
        $this->assertEquals($value,$assets->getCFItemTypeURIEntity());
    }

    public function test_LicenseURIEntity()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setLicenseURIEntity($value);
        $this->assertEquals($value,$assets->getLicenseURIEntity());
    }

    public function test_ConceptKeywordsURIEntity()
    {
        $assets = new GetCaseFrameworkItemJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','5e5a4b22-67bc-40ad-824e-d83cc166fcea','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setConceptKeywordsURIEntity($value);
        $this->assertEquals($value,$assets->getConceptKeywordsURIEntity());
    }
}
