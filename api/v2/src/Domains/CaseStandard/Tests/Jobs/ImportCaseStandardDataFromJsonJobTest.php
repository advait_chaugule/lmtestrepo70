<?php
namespace App\Domains\CaseStandard\Tests\Jobs;

use App\Domains\CaseStandard\Jobs\ImportCaseStandardDataFromJsonJob;
use Tests\TestCase;

class ImportCaseStandardDataFromJsonJobTest extends TestCase
{
    public function test_Copy()
    {
        $assets = new ImportCaseStandardDataFromJsonJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCopy($value);
        $this->assertEquals($value,$assets->getCopy());
    }

    public function test_UserId()
    {
        $assets = new ImportCaseStandardDataFromJsonJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setUserId($value);
        $this->assertEquals($value,$assets->getUserId());
    }

    public function test_UserOrganizationId()
    {
        $assets = new ImportCaseStandardDataFromJsonJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setUserOrganizationId($value);
        $this->assertEquals($value,$assets->getUserOrganizationId());
    }

    public function test_UploadedCaseJson()
    {
        $assets = new ImportCaseStandardDataFromJsonJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setUploadedCaseJson($value);
        $this->assertEquals($value,$assets->getUploadedCaseJson());
    }
}
