<?php
namespace App\Domains\Casestandard\Tests\Jobs;

use App\Domains\Casestandard\Jobs\GetCaseFrameworkLicenseJob;
use Tests\TestCase;

class GetCaseFrameworkLicenseJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new GetCaseFrameworkLicenseJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_LicenseDetail()
    {
        $assets = new GetCaseFrameworkLicenseJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setLicenseDetail($value);
        $this->assertEquals($value,$assets->getLicenseDetail());
    }

    public function test_MetadataList()
    {
        $assets = new GetCaseFrameworkLicenseJob('15e11d90-8d2d-4a68-9b8d-f68f14a98d89','ee96ccc7-b6fd-4c71-a5e5-66fd20530f78');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setMetadataList($value);
        $this->assertEquals($value,$assets->getMetadataList());
    }
}
