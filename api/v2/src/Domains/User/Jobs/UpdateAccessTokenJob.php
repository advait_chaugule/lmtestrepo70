<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

use App\Services\Api\Traits\UuidHelperTrait;

class UpdateAccessTokenJob extends Job
{
    use UuidHelperTrait;

    private $userRepository;
    private $userId;
    private $accessToken;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $userId)
    {
        $this->setUserId($userId);
    }

    public function setUserId(string $data) {
        $this->userId = $data;
    }

    public function getUserId(): string {
        return $this->userId;
    }

    public function setAccessToken(string $data) {
        $this->accessToken = $data;
    }

    public function getAccessToken(): string {
        return $this->accessToken;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        // set db repository(s)
        $this->userRepository = $userRepository;
        // generate and set user-accesstoken
        $this->generateAndSetUserAccessToken();
        // update the user access token along with access_token_updated_at timestamp
        $this->updateUserAccessToken();
        // finally return the access token that is updated
        return $this->getAccessToken();
    }

    private function generateAndSetUserAccessToken() {
        $userAccessToken = $this->createUniversalUniqueIdentifier();
        $this->setAccessToken($userAccessToken);
    }

    private function updateUserAccessToken() {
        $userId = $this->getUserId();
        $userAccessToken = $this->getAccessToken();
        $columnValuePairToUpdate = [
            // disable updating the access-token (temporary)
            //"active_access_token" => $userAccessToken,
            "access_token_updated_at" => now()->toDateTimeString(),
        ];
        $conditionalClausePair = [ "user_id" => $userId ];
        $this->userRepository->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);
    }
}
