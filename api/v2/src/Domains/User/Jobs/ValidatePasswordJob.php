<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Domains\User\Validators\ChangePasswordValidator;

class ValidatePasswordJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ChangePasswordValidator $validator)
    {
        $validation = $validator->validate($this->input);
        $response = $validation===true ? $validation : $validation->messages()->getMessages();
        
               if($response!='true'){
                   foreach($response as $value)
                   {
                        foreach($value as $singlemsg){
                        $errorarr[] = $singlemsg;
                        }
                    }
                    return $errorarr;
                }
                else{
                    return $response;
                }
           
    }
}
