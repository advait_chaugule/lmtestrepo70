<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;

class CheckUserExistByEmailAndUsernameJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $check_user_exist = DB::table('users')
                                ->where('username',$this->data['username'])
                                ->Orwhere('email',$this->data['email'])
                                ->Orwhere('user_id',$this->data['user_id'])
                                ->where('organization_id',$this->data['organization_id'])
                                ->count();

        return $check_user_exist;                        
    }
}
