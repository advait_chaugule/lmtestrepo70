<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class GetDefaultUserRoleIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
        return $this->fetchRole();
    }

    private function fetchRole() {

        $requestData = $this->getRequestData();
        $requestOrganizationId = $requestData['organization_id'];
        $requestRoleCode = $requestData['role_code'];
        $userLoginConditionAttributeData = [ "organization_id" => $requestOrganizationId,"role_code"=>$requestRoleCode];
        $returnCollection = false;
        $fieldsToReturn = [ 
            "role_id", "organization_id", "name", "role_code", 
            "updated_at", "is_deleted", "is_active"
        ];

        $userRole = $this->roleRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $userLoginConditionAttributeData);
        
        if($userRole == null)
        {
            return null;
        }
        else
        {   
            $userRoleInfo = $userRole->toArray();
            return $userRoleInfo['role_id'];  
        }   
    }
}
