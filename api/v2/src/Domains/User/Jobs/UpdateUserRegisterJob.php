<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use DB;

class UpdateUserRegisterJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $id = $this->input['new_user_id'];
        $userDetails = $userRepo->find($id);
        $organizationId = $userDetails['organization_id'];
        $input['password']= bcrypt($this->input['new_password']);
        $input['first_name']= $this->input['new_first_name'];
        $input['last_name']= $this->input['new_last_name'];
        $input['current_organization_id']= $organizationId;
        $input['is_active']= 1;
        $input['is_register']=1;
        $userResult = $userRepo->edit($id, $input);
        $userOrg  = ['user_id'         => $userDetails['user_id'],
        'organization_id' => $userDetails['organization_id'],
        'role_id'         => $userDetails['role_id'],
        'is_active'       => 1,
       ];
        DB::table('users_organization')->where('user_id',$userDetails['user_id'])->where('organization_id',$userDetails['organization_id'])->update($userOrg);
        return $userResult;
    }
}
