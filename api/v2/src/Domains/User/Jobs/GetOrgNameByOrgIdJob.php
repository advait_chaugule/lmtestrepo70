<?php
namespace App\Domains\User\Jobs;
use App\Data\Models\Organization;
use Lucid\Foundation\Job;

class GetOrgNameByOrgIdJob extends Job
{
    private $organizationId;

    public function __construct(string $organizationId){
        $this->organizationId =$organizationId;
    }
    public function handle()
    {
        $orgName = Organization::select('name as organization_name','org_code as organization_code')
                   ->where('organization_id','=',$this->organizationId)
                   ->first()
                   ->toArray();
        return $orgName;
    }
}