<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 11-04-2019
 * Time: 20:11
 */

namespace App\Domains\User\Jobs;
use DB;
use Lucid\Foundation\Job;

class GetUserActiveStatusJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $user_id;
    private $organizationId;

    public function __construct($input)
    {
        $this->user_id       = $input['user_id'];
        $this->organizationId = $input['organization_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = DB::table('users_organization')
            ->where('user_id',$this->user_id)
            ->where('organization_id', $this->organizationId)
            ->where('is_deleted','=',0)
            ->get()
            ->first();
        return $user;


    }
}