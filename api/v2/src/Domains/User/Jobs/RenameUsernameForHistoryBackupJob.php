<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class RenameUsernameForHistoryBackupJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
       
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        //
        $this->userRepository = $userRepository;
        return $this->updateUser();
    }

    private function updateUser() {

        $requestData = $this->getRequestData();
        $requestUsername = $requestData['email'];
        
        $userList = $this->userRepository->getUserListForRename($requestUsername);
         foreach($userList As $user)
         {
            
             $user_id = $user->user_id;
             $username = $user->username;
             $newUsername = '*'.$username;
             $columnValuePairToUpdate = [
                "username" => $newUsername,
                "email" => $newUsername,
            ];
            $conditionalClausePair = ['user_id' => $user_id];
            $this->userRepository->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);   
         }
       



    }


}
