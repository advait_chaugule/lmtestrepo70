<?php


namespace App\Domains\User\Jobs;


use Lucid\Foundation\Job;
use DB;
class GetOrgCodeFromEmailIdJob extends Job
{
    private $email;
    public function __construct($emailData)
    {
        $this->email = $emailData['email'];
    }

    public function handle()
    {
        $orgId = DB::table('users')->select('current_organization_id')
                          ->where('email',$this->email)
                          ->where('is_deleted',0)
                          ->where('is_active',1)
                          ->get()
                          ->first();

            return $orgId->current_organization_id;
        }

}