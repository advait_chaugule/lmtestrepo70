<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;
use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;
use DB;
class SwitchUserOrganizationJob extends  Job
{
    private $userRepository;
    private $roleRepository;
    private $orgRepository;
    private $userId;
    private $requestData;
    private $userSystemRole;

    private $userProject;
    private $projectRolePermissions;
    private $groupedProjectRolePermissions;

    private $groupedSystemRolePermissions;

    private $userOrganization;

    private $userDeletedStatus;
    private $userBlockedStatus;
    private $userSystemRoleInactiveStatus;
    private $userSystemRoleDeletedStatus;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($input){
        $this->userId  = $input['user_id'];
        $this->organization_id  = $input['organization_id'];
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    public function setUserSystemRole($data) {
        $this->userSystemRole = $data;
    }

    public function getUserSystemRole() {
        return $this->userSystemRole;
    }

    public function setUserProject($data) {
        $this->userProject = $data;
    }

    public function getUserProject() {
        return $this->userProject;
    }

    public function setProjectRolePermissions($data) {
        $this->projectRolePermissions = $data;
    }

    public function getProjectRolePermissions() {
        return $this->projectRolePermissions;
    }

    public function setGroupedProjectRolePermissions(array $data) {
        $this->groupedProjectRolePermissions = $data;
    }

    public function getGroupedProjectRolePermissions(): array {
        return $this->groupedProjectRolePermissions;
    }

    public function setGroupedSystemRolePermissions(array $data) {
        $this->groupedSystemRolePermissions = $data;
    }

    public function getGroupedSystemRolePermissions(): array {
        return $this->groupedSystemRolePermissions;
    }

    public function setuserOrganization($data) {
        $this->userOrganization = $data;
    }

    public function getuserOrganization() {
        return $this->userOrganization;
    }

    public function setUserDeletedStatus(bool $status) {
        $this->userDeletedStatus = $status;
    }

    public function getUserDeletedStatus() {
        return $this->userDeletedStatus;
    }

    public function setUserBlockedStatus(bool $status) {
        $this->userBlockedStatus = $status;
    }

    public function getUserBlockedStatus() {
        return $this->userBlockedStatus;
    }

    public function setUserSystemRoleInactiveStatus(bool $status) {
        $this->userSystemRoleInactiveStatus = $status;
    }

    public function getUserSystemRoleInactiveStatus() {
        return $this->userSystemRoleInactiveStatus;
    }

    public function setUserSystemRoleDeletedStatus(bool $status) {
        $this->userSystemRoleDeletedStatus = $status;
    }

    public function getUserSystemRoleDeletedStatus() {
        return $this->userSystemRoleDeletedStatus;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        UserRepositoryInterface $userRepository,
        RoleRepositoryInterface $roleRepository,
        OrganizationRepositoryInterface $orgRepository
    )
    {
        // set the db repositories
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->orgRepository  = $orgRepository;
        // fetch and set user system level role
        $userSystemRole = $this->fetchUserSystemRole();

        $this->setUserSystemRole($userSystemRole);
        $this->getUsersRole($this->userId);
        // fetch user and set it
        $this->fetchUser($this->userId);
        $userProject = $this->fetchUserProject();
        $this->setUserProject($userProject);
        $this->groupProjectRolePermissions();
        $this->groupSystemRolePermissions();


        $userOrganization = $this->fetchUserOrganization();
        $this->setuserOrganization($userOrganization);
        // prepare the final login response data and return it
        return $this->prepareLoginResponseData();
}

    private function fetchUser($userId)
    {
        $userLogin =  DB::table('users')->select('user_id','active_access_token','email',
            'first_name','last_name',
            'updated_at','current_organization_id')
            ->where('user_id','=',$userId)
            ->first();
        return $userLogin;

    }
    private function fetchUserOrgData($userId)
    {
        $userLogin =  DB::table('users_organization')->select('user_id','role_id','organization_id')
            ->where('user_id','=',$userId)
            ->where('organization_id','=',$this->organization_id)
            ->where('is_deleted','=',0)
            ->where('is_active','=',1)
            ->first();
        return $userLogin;

    }
    private function getUsersRole($userId)
    {
        $user   = $this->fetchUserOrgData($userId);
        $roleId = $user->role_id;
        $role = DB::table('roles')->select('role_id','name','role_code')
            ->where('is_deleted','=',0)
            ->where('role_id','=',$roleId)
            ->first();
        return $role;
    }

    private function fetchUserProject() {
        $user = $this->fetchUser($this->userId);
        $organization_id = $user->current_organization_id;
        $conditionalKeyValuePairs = ["user_id" => $user->user_id, "is_deleted" => '0',"organization_id" => $organization_id];
        $fieldsToReturn = ["project_id", "workflow_stage_role_id"];
        $userProject = $this->userRepository->fetchProjectsList($fieldsToReturn, $conditionalKeyValuePairs);
        return json_decode(json_encode($userProject));
    }
    
    private function checkAndSetUserBlockedStatus() {
        $user = $this->getUser();
        $status = $user->is_active===0 ? true : false;
        $this->setUserBlockedStatus($status);
    }

    private function checkAndSetUserDeletedStatus() {
        $user = $this->getUser();
        $status = $user->is_deleted===1 ? true : false;
        $this->setUserDeletedStatus($status);
    }

    private function fetchUserSystemRole() {
        $user = $this->fetchUser($this->userId);
        $roleId = $this->getRole($user->current_organization_id,$user->user_id);

        $organization_id = $user->current_organization_id;
        $conditionalKeyValuePairs = ["role_id" => $roleId];
        $returnCollection = true;
        $fieldsToReturn = ["role_id", "name", "role_code", "is_active", "is_deleted"];

        return $this->roleRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePairs);

    }

    private function fetchUserProjectRolePermissions() {
        $projectRolePermission = [];
        $userProject = $this->getUserProject();

        return $projectRolePermission;

    }
    private function fetchUserOrganization() {
        $user = $this->fetchUser($this->userId);
        $organization_id = $user->current_organization_id;
        $conditionalKeyValuePairs = ["organization_id" => $organization_id];
        $returnCollection = false;
        $fieldsToReturn = ["organization_id", "name", "org_code"];
        return $this->orgRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePairs);
    }

    private function prepareLoginResponseData(): array {
        //$isUserVerified = $this->getUserVerificationStatus();
//        $userDeletedStatus = $this->getUserDeletedStatus();
//        $userBlockedStatus = $this->getUserBlockedStatus();
//        $userSystemRoleInactiveStatus = $this->getUserSystemRoleInactiveStatus();
//        $userSystemRoleDeletedStatus = $this->getUserSystemRoleDeletedStatus();
        // set error response messages based on statuses
        $responseMessageList = "";
        $responseBody = json_decode(json_encode($this->fetchUser($this->userId)),true);
            // make array from object data
            $userSystemRole             = json_decode(json_encode($this->getUsersRole($this->userId)),true);
            $userProjectRolePermissions = json_decode(json_encode($this->getGroupedProjectRolePermissions()),true);
            $userSystemRolePermissions  = json_decode(json_encode($this->getGroupedSystemRolePermissions()),true);
            $userOrganization           = json_decode(json_encode($this->getuserOrganization()),true);

            // create the response array and set it
            $responseBody["system_role"] = $userSystemRole;
            $responseBody["project_role_permissions"] = $userProjectRolePermissions;
            $responseBody["system_role_permissions"] = $userSystemRolePermissions;
            $responseBody["organization"] = $userOrganization;

        $loginResponse = [
            //"loginStatus" => $loginStatus,
            "responseMessageList" => $responseMessageList,
            "responseBody" => $responseBody
        ];

        return $loginResponse;
    }


    private function groupProjectRolePermissions(){
        $projectPermissionList = [];
        $project = $this->getUserProject();
        foreach($project as $projects){
            $roleId = explode("||",$projects->workflow_stage_role_id);
            $roleDetail = $this->roleRepository->getRelatedModels($roleId[2]);
            $permissionList = $roleDetail->permissions;
            $stagesData = ['stage_id'=>$roleId[1],'workflow_stage_role_id'=>$projects->workflow_stage_role_id,'role_id'=>$roleId[2]];
            $projectPermissionList[] = $this->groupRolePermissions($projects->project_id,$stagesData,$permissionList);
        }
        $loginProjectPermission=[];
        foreach ($projectPermissionList as $projectPermissionListV)
        {
            $loginProjectPermission[$projectPermissionListV['project_id']]['project_id'] = $projectPermissionListV['project_id'];
            $loginProjectPermission[$projectPermissionListV['project_id']]['stages'][$projectPermissionListV['stages'][0]['stage_id']][] = $projectPermissionListV['stages'][0];


        }
        $finalArray =[];
        foreach($loginProjectPermission as $loginProjectPermissionK => $loginProjectPermissionV)
        {
            $stageArr = [];
            foreach($loginProjectPermission[$loginProjectPermissionK]['stages'] as $key => $val)
            {
                $finalPermissionArry = [];
                $project_permissions = [];
                $taxonomy_permissions  = [];
                $comment_permissions  = [];
                $metadata_permissions  = [];
                $node_type_permissions  = [];
                $node_template_permissions  = [];
                $workflow_permissions  = [];
                $role_user_permissions  = [];
                $notification_permissions  = [];
                $public_review_permissions  = [];
                $note_permissions  = [];
                $pacing_guide_permissions  = [];
               foreach($val as $valK => $valV)
               {
                   foreach($valV['project_permissions'] as $ppk => $ppv)
                   {
                    $project_permissions[] = [
                        "permission_id" => $ppv['permission_id'],
                        "parent_permission_id" => $ppv['parent_permission_id'],
                        "display_name" => $ppv['display_name'],
                        "display_order" => $ppv['display_order']];

                   }

                   foreach($valV['taxonomy_permissions'] as $tk => $tv)
                   {
                    $taxonomy_permissions[] = [
                        "permission_id" => $tv['permission_id'],
                        "parent_permission_id" => $tv['parent_permission_id'],
                        "display_name" => $tv['display_name'],
                        "display_order" => $tv['display_order']];

                   }

                   foreach($valV['comment_permissions'] as $ck => $cv)
                   {
                    $comment_permissions[] = [
                        "permission_id" => $cv['permission_id'],
                        "parent_permission_id" => $cv['parent_permission_id'],
                        "display_name" => $cv['display_name'],
                        "display_order" => $cv['display_order']];

                   }

                   foreach($valV['metadata_permissions'] as $mk => $mv)
                   {
                    $metadata_permissions[] = [
                        "permission_id" => $mv['permission_id'],
                        "parent_permission_id" => $mv['parent_permission_id'],
                        "display_name" => $mv['display_name'],
                        "display_order" => $mv['display_order']];

                   }

                   foreach($valV['node_type_permissions'] as $ntk => $ntv)
                   {
                    $node_type_permissions[] = [
                        "permission_id" => $ntv['permission_id'],
                        "parent_permission_id" => $ntv['parent_permission_id'],
                        "display_name" => $ntv['display_name'],
                        "display_order" => $ntv['display_order']];

                   }

                   foreach($valV['node_template_permissions'] as $ntek => $ntev)
                   {
                    $node_template_permissions[] = [
                        "permission_id" => $ntev['permission_id'],
                        "parent_permission_id" => $ntev['parent_permission_id'],
                        "display_name" => $ntev['display_name'],
                        "display_order" => $ntev['display_order']];

                   }

                   foreach($valV['workflow_permissions'] as $wpk => $wpv)
                   {
                    $workflow_permissions[] = [
                        "permission_id" => $wpv['permission_id'],
                        "parent_permission_id" => $wpv['parent_permission_id'],
                        "display_name" => $wpv['display_name'],
                        "display_order" => $wpv['display_order']];

                   }

                   foreach($valV['role_user_permissions'] as $rupk => $rupv)
                   {
                    $role_user_permissions[] = [
                        "permission_id" => $rupv['permission_id'],
                        "parent_permission_id" => $rupv['parent_permission_id'],
                        "display_name" => $rupv['display_name'],
                        "display_order" => $rupv['display_order']];

                   }

                   foreach($valV['notification_permissions'] as $npk => $npv)
                   {
                    $notification_permissions[] = [
                        "permission_id" => $npv['permission_id'],
                        "parent_permission_id" => $npv['parent_permission_id'],
                        "display_name" => $npv['display_name'],
                        "display_order" => $npv['display_order']];

                   }

                   foreach($valV['public_review_permissions'] as $prpk => $prpv)
                   {
                    $public_review_permissions[] = [
                        "permission_id" => $prpv['permission_id'],
                        "parent_permission_id" => $prpv['parent_permission_id'],
                        "display_name" => $prpv['display_name'],
                        "display_order" => $prpv['display_order']];

                   }

                   foreach($valV['note_permissions'] as $npk => $npv)
                   {
                    $note_permissions[] = [
                        "permission_id" => $npv['permission_id'],
                        "parent_permission_id" => $npv['parent_permission_id'],
                        "display_name" => $npv['display_name'],
                        "display_order" => $npv['display_order']];

                   }

                   foreach($valV['pacing_guide_permissions'] as $pgk => $pgv)
                   {
                    $pacing_guide_permissions[] = [
                        "permission_id" => $pgv['permission_id'],
                        "parent_permission_id" => $pgv['parent_permission_id'],
                        "display_name" => $pgv['display_name'],
                        "display_order" => $pgv['display_order']];

                   }
               }
               $finalPermissionArry = [
                "stage_id" =>  $valV['stage_id'],  
                "project_permissions"       => array_values($this->sortArray($project_permissions,'display_order')),
                "taxonomy_permissions"      => array_values($this->sortArray($taxonomy_permissions,'display_order')),
                "comment_permissions"       => array_values($this->sortArray($comment_permissions,'display_order')),
                "metadata_permissions"      => array_values($this->sortArray($metadata_permissions,'display_order')),
                "node_type_permissions"     => array_values($this->sortArray($node_type_permissions,'display_order')),
                "node_template_permissions" => array_values($this->sortArray($node_template_permissions,'display_order')),
                "workflow_permissions"      => array_values($this->sortArray($workflow_permissions,'display_order')),
                "role_user_permissions"     => array_values($this->sortArray($role_user_permissions,'display_order')),
                "notification_permissions"  => array_values($this->sortArray($notification_permissions,'display_order')),
                "public_review_permissions" => array_values($this->sortArray($public_review_permissions,'display_order')),
                "note_permissions"          => array_values($this->sortArray($note_permissions,'display_order')),
                "pacing_guide_permissions"  => array_values($this->sortArray($pacing_guide_permissions,'display_order')),
               ];
                 array_push($stageArr,$finalPermissionArry);
            }
            
            $loginProjectPermission[$loginProjectPermissionK]['stages'] = $stageArr;
            $finalArray[] =  $loginProjectPermission[$loginProjectPermissionK];
        }
        $this->setGroupedProjectRolePermissions($finalArray);
    }

    private function groupRolePermissions($projectId, $stagesData,$permissionList) {
        $permission = [
            "project_permissions"       => $this->helperToReturnParsedOutPermissions(1, $permissionList),
            "taxonomy_permissions"      => $this->helperToReturnParsedOutPermissions(2, $permissionList),
            "comment_permissions"       => $this->helperToReturnParsedOutPermissions(7, $permissionList),
            "metadata_permissions"      => $this->helperToReturnParsedOutPermissions(5, $permissionList),
            "node_type_permissions"     => $this->helperToReturnParsedOutPermissions(6, $permissionList),
            "node_template_permissions" => $this->helperToReturnParsedOutPermissions(8, $permissionList),
            "workflow_permissions"      => $this->helperToReturnParsedOutPermissions(3, $permissionList),
            "role_user_permissions"     => $this->helperToReturnParsedOutPermissions(4, $permissionList),
            "notification_permissions"  => $this->helperToReturnParsedOutPermissions(9, $permissionList),
            "public_review_permissions" => $this->helperToReturnParsedOutPermissions(10, $permissionList),
            "note_permissions"          => $this->helperToReturnParsedOutPermissions(11, $permissionList),
            "pacing_guide_permissions"  => $this->helperToReturnParsedOutPermissions(12, $permissionList)
        ];

        $groupByPermissionsName = [
            "project_id" => $projectId,
            "stages"     => [array_merge($stagesData,$permission)]
        ];
       return $groupByPermissionsName;
    }
    
    private function groupRoleSystemPermissions($projectId, $systemPermissionList) {
        $sysPermissions = [
            "project_id"                => $projectId,
            "project_permissions"       => $this->helperToReturnParsedOutPermissions(1, $systemPermissionList),
            "taxonomy_permissions"      => $this->helperToReturnParsedOutPermissions(2, $systemPermissionList),
            "comment_permissions"       => $this->helperToReturnParsedOutPermissions(7, $systemPermissionList),
            "metadata_permissions"      => $this->helperToReturnParsedOutPermissions(5, $systemPermissionList),
            "node_type_permissions"     => $this->helperToReturnParsedOutPermissions(6, $systemPermissionList),
            "node_template_permissions" => $this->helperToReturnParsedOutPermissions(8, $systemPermissionList),
            "workflow_permissions"      => $this->helperToReturnParsedOutPermissions(3, $systemPermissionList),
            "role_user_permissions"     => $this->helperToReturnParsedOutPermissions(4, $systemPermissionList),
            "notification_permissions"  => $this->helperToReturnParsedOutPermissions(9, $systemPermissionList),
            "public_review_permissions" => $this->helperToReturnParsedOutPermissions(10,$systemPermissionList),
            "note_permissions"          => $this->helperToReturnParsedOutPermissions(11,$systemPermissionList),
            "pacing_guide_permissions"  => $this->helperToReturnParsedOutPermissions(12,$systemPermissionList)
        ];

        return $sysPermissions;
    }
    private function groupSystemRolePermissions(){
        $systemRole = $this->getUserSystemRole()->toArray();
        $roleDetail = $this->roleRepository->getRelatedModels($systemRole[0]['role_id']);
        $permissionList = $roleDetail->permissions;
        $systemPermissionList = $this->groupRoleSystemPermissions('', $permissionList);

        $this->setGroupedSystemRolePermissions($systemPermissionList);
    }

    private function helperToReturnParsedOutPermissions($permissionGroupType, $permissions): array {

        $groupedPermissions = $permissions->filter(function ($item, $key) use($permissionGroupType) {
            if($item->permission_group===$permissionGroupType) {
                return $item;
            }
        });
        $parsedPermissions = [];

        foreach($groupedPermissions as $permission) {
            if($permission->permission_id != 'acee9a40-f0c0-4576-9f5c-0becfc177907'){
                $parsedPermissions[] = [
                    "permission_id" => $permission->permission_id,
                    "parent_permission_id" => $permission->parent_permission_id,
                    "display_name" => $permission->display_name,
                    "display_order" => $permission->order
                ];
            }
        }

        array_multisort(array_column($parsedPermissions, 'display_order'), SORT_ASC, $parsedPermissions);
        return $parsedPermissions;
    }

    private function getRole($organization_id,$user_id)
    {
        $roleInfo = DB::table('users_organization')
            ->select('role_id')
            ->where('organization_id',$organization_id)
            ->where('user_id',$user_id)
            ->where('is_deleted','0')
            ->get()
            ->toArray();
        if(count($roleInfo) > 0) {
            return $roleInfo[0]->role_id;
        }
        else {
            return "";
        }
    }    

    private function sortArray($data, $field)
    {
      if(!is_array($field)) $field = array($field);
      usort($data, function($a, $b) use($field) {
        $retval = 0;
        foreach($field as $fieldname) {
          if($retval == 0) $retval = strnatcmp($a[$fieldname],$b[$fieldname]);
        }
        return $retval;
      });
      return $data;
    }
}