<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use Lucid\Foundation\QueueableJob;

use Illuminate\Support\Facades\Mail;
use App\Domains\User\Mail\SendResetEmailToUser;

class SendResetPasswordEmailNotificationJob
{
    private $input;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->input['subject'] = 'Reset your password';        
        Mail::to($this->input['email'])->send(new SendResetEmailToUser($this->input));
    }
}
