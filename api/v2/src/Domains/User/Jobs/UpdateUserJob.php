<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class UpdateUserJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $user_id = $this->input['user_id'];

        // receive role_id from input if set
        if(isset($this->input['role_id'])){
            $input['role_id'] = $this->input['role_id'];
        }

        // receive is_active from input if set
        if(isset($this->input['is_active'])){
            $input['is_active'] = $this->input['is_active'];
        }

        if(isset($this->input['organization_id'])){
            $input['organization_id'] = $this->input['organization_id'];
        }
        return $userRepo->updateUserOrganization($user_id, $input);
    }
}
