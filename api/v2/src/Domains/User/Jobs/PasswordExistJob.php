<?php
namespace App\Domains\User\Jobs;
use Illuminate\Support\Facades\Hash;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Lucid\Foundation\Job;

class PasswordExistJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $resetRequestToken = $this->input['reset_request_token'];
        $requestPassword = $this->input['password'];
        $collectionType = false;
        $userDetails = $userRepo->findByAttributesWithSpecifiedFields(
            $collectionType, ['password'], ['forgot_pasword_request_token' => $resetRequestToken]
        );
        if(Hash::check($requestPassword, $userDetails->password))
        {
            return 0;
        }
        else
        {
            return 1;
        } 
    }
}