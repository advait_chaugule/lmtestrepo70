<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class GetUserDetailByEmailIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $emailId)
    {
        //Set the private variable
        $this->emailId   =   $emailId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
        $emailId = $this->emailId;

        $userDetail =   $this->userRepo->getUserDetailBasedOnEmailId($emailId);

        return $userDetail;
    }
}
