<?php

namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use DB;
class GetUserDerailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $token_id;
    private $organizationId;

    public function __construct($input)
    {
        $this->token_id       = $input['token_id'];
        $this->organizationId = $input['organization_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = DB::table('users')
                    ->where('active_access_token',$this->token_id)
                    ->where('is_deleted','=',0)
                    ->get()
                   ->first();
        return $user;


    }
}