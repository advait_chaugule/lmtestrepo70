<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;
class UpdateUserActiveStatusJob extends Job
{
    private $userId;
    private $organizationId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->userId         = $input['user_id'];
        $this->organizationId = $input['organization_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = DB::table('users_organization')
                ->where('user_id','=', $this->userId )
                ->where('organization_id','=', $this->organizationId)
                ->where('is_deleted','=',0)
                ->update(['is_active'=> 1]);
        
        // Query added to update isactive status to 1 if in all other organizations user was inactive and in users table, record was made inactive
        DB::table('users')
        ->where('user_id','=', $this->userId )
        ->where('is_active','=',0)
        ->update(['is_active'=> 1]);
        return $query;
    }
}