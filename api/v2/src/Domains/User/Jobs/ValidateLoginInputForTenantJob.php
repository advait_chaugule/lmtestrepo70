<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;

class ValidateLoginInputForTenantJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $email = $this->input['email'];
        $organization_id = $this->input['organization_id'];
        $collectionType = false;
        $userDetails = $userRepo->findByAttributesWithSpecifiedFields(
            $collectionType, ['user_id'], ['email' => $email , 'organization_id' => $organization_id]
        );
        
        if($userDetails == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
