<?php
namespace App\Domains\User\Jobs;
use Illuminate\Support\Facades\Hash;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;
use App\Data\Repositories\Contracts\OrganizationRepositoryInterface;
use DB;

class LoginJob extends Job
{
    private $userRepository;
    private $roleRepository;
    private $orgRepository;

    private $requestData;
    private $user;
    private $userVerificationStatus;
    private $userSystemRole;

    private $userProject;
    private $projectRolePermissions;
    private $groupedProjectRolePermissions;

    private $groupedSystemRolePermissions;

    private $userOrganization;

    private $userDeletedStatus;
    private $userBlockedStatus;
    private $userSystemRoleInactiveStatus;
    private $userSystemRoleDeletedStatus;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    public function setUser($data) {
        $this->user = $data;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUserVerificationStatus(bool $data) {
        $this->userVerificationStatus = $data;
    }

    public function getUserVerificationStatus(): bool {
        return $this->userVerificationStatus;
    }

    public function setUserSystemRole($data) {
        $this->userSystemRole = $data;
    }

    public function getUserSystemRole() {
        return $this->userSystemRole;
    }

    public function setUserProject($data) {
        $this->userProject = $data;
    }

    public function getUserProject() {
        return $this->userProject;
    }

    public function setProjectRolePermissions($data) {
        $this->projectRolePermissions = $data;
    }

    public function getProjectRolePermissions() {
        return $this->projectRolePermissions;
    }

    public function setGroupedProjectRolePermissions(array $data) {
        $this->groupedProjectRolePermissions = $data;
    }

    public function getGroupedProjectRolePermissions(): array {
        return $this->groupedProjectRolePermissions;
    }

    public function setGroupedSystemRolePermissions(array $data) {
        $this->groupedSystemRolePermissions = $data;
    }

    public function getGroupedSystemRolePermissions(): array {
        return $this->groupedSystemRolePermissions;
    }

    public function setuserOrganization($data) {
        $this->userOrganization = $data;
    }

    public function getuserOrganization() {
        return $this->userOrganization;
    }

    public function setUserDeletedStatus(bool $status) {
        $this->userDeletedStatus = $status;
    }

    public function getUserDeletedStatus() {
        return $this->userDeletedStatus;
    }

    public function setUserBlockedStatus(bool $status) {
        $this->userBlockedStatus = $status;
    }

    public function getUserBlockedStatus() {
        return $this->userBlockedStatus;
    }

    public function setUserSystemRoleInactiveStatus(bool $status) {
        $this->userSystemRoleInactiveStatus = $status;
    }

    public function getUserSystemRoleInactiveStatus() {
        return $this->userSystemRoleInactiveStatus;
    }

    public function setUserSystemRoleDeletedStatus(bool $status) {
        $this->userSystemRoleDeletedStatus = $status;
    }

    public function getUserSystemRoleDeletedStatus() {
        return $this->userSystemRoleDeletedStatus;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        UserRepositoryInterface $userRepository,
        RoleRepositoryInterface $roleRepository,
        OrganizationRepositoryInterface $orgRepository
    )
    {
        // set the db repositories
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->orgRepository  = $orgRepository;

        // fetch user and set it
        $user = $this->fetchUser();
        $this->setUser($user);

        // verify whether username and password matches
        $this->verifyUser();

        // if user is verified fetch other dependent data and prepare the login response data
        $isUserVerified = $this->getUserVerificationStatus();
        if($isUserVerified===true){
            $this->checkAndSetUserBlockedStatus();
            $this->checkAndSetUserDeletedStatus();

            // fetch and set user system level role
            $userSystemRole = $this->fetchUserSystemRole();
            $this->setUserSystemRole($userSystemRole);

            $this->checkAndSetUserRoleActiveStatus();
            $this->checkAndSetUserRoleDeletedStatus();

            $systemRoleDeletedStatus = $this->getUserSystemRoleDeletedStatus();
            $systemRoleInactiveStatus = $this->getUserSystemRoleInactiveStatus();

            $userProject = $this->fetchUserProject();
            $this->setUserProject($userProject); 
            
            $this->groupProjectRolePermissions();

            $this->groupSystemRolePermissions();

            // fetch and set user organization
            $userOrganization = $this->fetchUserOrganization();
            $this->setuserOrganization($userOrganization);
        }
            
        // prepare the final login response data and return it
        return $this->prepareLoginResponseData();
    }

    private function fetchUser() {
        $requestData = $this->getRequestData();
        $requestUsername = $requestData['username'];
        $userLoginConditionAttributeData = [ "username" => $requestUsername ,"is_active" => '1'];
        $returnCollection = false;
        $fieldsToReturn = [ 
            "user_id", "password", "active_access_token", "email", 
            "first_name", "last_name", "role_id", "organization_id", 
            "updated_at", "is_deleted", "is_active","current_organization_id","is_super_admin"
        ];
        $user = $this->userRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $userLoginConditionAttributeData);
        return $user;
    }

    private function verifyUser() {
        $user = $this->getUser();
        $requestData = $this->getRequestData();
        $requestPassword = $requestData['password'];
        $verificationStatus = false;
        if(!empty($user->user_id) && Hash::check($requestPassword, $user->password)){
            $verificationStatus = true;
        }
        $this->setUserVerificationStatus($verificationStatus);
    }

    private function checkAndSetUserBlockedStatus() {
        $user = $this->getUser();
        $status = $user->is_active===0 ? true : false;
        $this->setUserBlockedStatus($status);
    }

    private function checkAndSetUserDeletedStatus() {
        $user = $this->getUser();
        $status = $user->is_deleted===1 ? true : false;
        $this->setUserDeletedStatus($status);
    }

    private function fetchUserSystemRole() {
        $user = $this->getUser();
        $roleId = $this->getRole($user->current_organization_id,$user->user_id);
        $organization_id = $user->current_organization_id;
        $conditionalKeyValuePairs = ["role_id" => $roleId, "organization_id" => $organization_id];
        $returnCollection = false;
        $fieldsToReturn = ["role_id", "name", "role_code", "is_active", "is_deleted"];

        return $this->roleRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePairs);
    } 

    private function fetchUserProject() {
        $user = $this->getUser();
        $organization_id = $user->current_organization_id;
        $conditionalKeyValuePairs = ["user_id" => $user->user_id, "is_deleted" => '0',"organization_id" => $organization_id];
        $fieldsToReturn = ["project_id", "workflow_stage_role_id"];
        $userProject = $this->userRepository->fetchProjectUser($fieldsToReturn, $conditionalKeyValuePairs);
        return $userProject;
    }

    private function checkAndSetUserRoleActiveStatus() {
        $systemRole = $this->getUserSystemRole();
        $status = (!isset($systemRole) || $systemRole->is_active==0) ?  true : false;
        $this->setUserSystemRoleInactiveStatus($status);
    }

    private function checkAndSetUserRoleDeletedStatus() {
        $systemRole = $this->getUserSystemRole();
        $status = (!isset($systemRole) || $systemRole->is_deleted==1) ?  true : false;
        $this->setUserSystemRoleDeletedStatus($status);
    }

    private function fetchUserProjectRolePermissions() {
        $projectRolePermission = [];
        $userProject = $this->getUserProject();
        
        return $projectRolePermission;
        
    }

    private function fetchUserOrganization() {
        $user = $this->getUser();
        $organization_id = $user->current_organization_id;
        $conditionalKeyValuePairs = ["organization_id" => $organization_id,"is_active" => '1'];
        $returnCollection = false;
        $fieldsToReturn = ["organization_id", "name", "org_code","domain_name"];
        return $this->orgRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePairs);
    }

    private function prepareLoginResponseData(): array {
        $isUserVerified = $this->getUserVerificationStatus();
        $userDeletedStatus = $this->getUserDeletedStatus();
        $userBlockedStatus = $this->getUserBlockedStatus();
        $userSystemRoleInactiveStatus = $this->getUserSystemRoleInactiveStatus();
        $userSystemRoleDeletedStatus = $this->getUserSystemRoleDeletedStatus();

        // set error response messages based on statuses
        $responseMessageList = "";
        $userSystemRoleStatus = true;

        if($isUserVerified !==null && !$isUserVerified)
            $responseMessageList = "Invalid username or password.";
        if($userDeletedStatus !==null && $userDeletedStatus===true)
            $responseMessageList = "Your access has been revoked. Kindly contact your administrator";
        if($userBlockedStatus !==null && $userBlockedStatus===true)
            $responseMessageList = "Your account is deactivated. Please contact your administrator";
        if($userSystemRoleInactiveStatus !==null && $userSystemRoleInactiveStatus===true){
            $responseMessageList = "You are yet to receive permissions to access COMET. Kindly contact your administrator";
            $userSystemRoleStatus = false;
        }             
        if($userSystemRoleDeletedStatus !==null && $userSystemRoleDeletedStatus === true){
            $responseMessageList = "Access has been revoked. Kindly contact the admin";
            $userSystemRoleStatus = false;
        }
        
		// Give priority to messages of validation
        if($responseMessageList == "" || $userSystemRoleStatus == false){
            // Check if user's current organization is active.
            $userOrganization = $this->getuserOrganization();
            if($userOrganization && $userSystemRoleStatus == true)
                $userOrganization = $userOrganization->toArray();
            else{
                // Check if user's is associated to any other active organization.
                $currOrganization = $this->getActiveOrganizationByUserId();
                if($currOrganization)
                {
                    // Update current organization of user.
                    $this->updateCurrentOrganization($currOrganization);
                    $responseMessageList = "";
                    $userSystemRoleStatus = true;
                }
                else{
                    // If user is not associated to any other tenant, then show this message.
                    $responseMessageList = "This Tenant is deactivated and please contact admin";
                    $userOrganization = [];
                }
            }
        }
        
        // Check if user is associated to any organization. If empty, show deactivated message
        $loginStatus = $isUserVerified===true ? 
                       $userDeletedStatus===false && $userBlockedStatus===false &&
                       $userSystemRoleStatus===true && !empty($userOrganization) : 
                       false;

        $responseBody = [];
        if($loginStatus===true){
            $responseBody = $this->getUser()->toArray();
            // remove data from final response
            array_forget($responseBody, 'organization_id');
            array_forget($responseBody, 'role_id');
            array_forget($responseBody, 'is_deleted');
            array_forget($responseBody, 'is_active');

            $userSystemRole = $this->getUserSystemRole()->toArray();
            array_forget($userSystemRole, 'is_deleted');
            array_forget($userSystemRole, 'is_active');
            $userProjectRolePermissions = $this->getGroupedProjectRolePermissions();
            $userSystemRolePermissions = $this->getGroupedSystemRolePermissions();
            
            // create the response array and set it
            $responseBody["system_role"] = $userSystemRole;
            $responseBody["project_role_permissions"] = $userProjectRolePermissions;
            $responseBody["system_role_permissions"] = $userSystemRolePermissions;
            $responseBody["organization"] = $userOrganization;
        }

        $loginResponse = [
            "loginStatus" => $loginStatus,
            "responseMessageList" => $responseMessageList,
            "responseBody" => $responseBody
        ];

        return $loginResponse;
    }


    private function groupProjectRolePermissions(){
        $projectPermissionList = [];
        $project = $this->getUserProject();
        foreach($project as $projects){
            $roleId = explode("||",$projects->workflow_stage_role_id);
            $roleDetail = $this->roleRepository->getRelatedModels($roleId[2]);
            $permissionList = $roleDetail->permissions;
            $stagesData = ['stage_id'=>$roleId[1],'workflow_stage_role_id'=>$projects->workflow_stage_role_id,'role_id'=>$roleId[2]];
            $projectPermissionList[] = $this->groupRolePermissions($projects->project_id,$stagesData,$permissionList);
           
        }
        $loginProjectPermission=[];
        foreach ($projectPermissionList as $projectPermissionListV)
        {
            $loginProjectPermission[$projectPermissionListV['project_id']]['project_id'] = $projectPermissionListV['project_id'];
            $loginProjectPermission[$projectPermissionListV['project_id']]['stages'][$projectPermissionListV['stages'][0]['stage_id']][] = $projectPermissionListV['stages'][0];


        }
        $finalArray =[];
        foreach($loginProjectPermission as $loginProjectPermissionK => $loginProjectPermissionV)
        {
            $stageArr = [];
            foreach($loginProjectPermission[$loginProjectPermissionK]['stages'] as $key => $val)
            {
                $finalPermissionArry = [];
                $project_permissions = [];
                $taxonomy_permissions  = [];
                $comment_permissions  = [];
                $metadata_permissions  = [];
                $node_type_permissions  = [];
                $node_template_permissions  = [];
                $workflow_permissions  = [];
                $role_user_permissions  = [];
                $notification_permissions  = [];
                $public_review_permissions  = [];
                $note_permissions  = [];
                $pacing_guide_permissions  = [];
               foreach($val as $valK => $valV)
               {
                   foreach($valV['project_permissions'] as $ppk => $ppv)
                   {
                    $project_permissions[] = [
                        "permission_id" => $ppv['permission_id'],
                        "parent_permission_id" => $ppv['parent_permission_id'],
                        "display_name" => $ppv['display_name'],
                        "display_order" => $ppv['display_order']];

                   }

                   foreach($valV['taxonomy_permissions'] as $tk => $tv)
                   {
                    $taxonomy_permissions[] = [
                        "permission_id" => $tv['permission_id'],
                        "parent_permission_id" => $tv['parent_permission_id'],
                        "display_name" => $tv['display_name'],
                        "display_order" => $tv['display_order']];

                   }

                   foreach($valV['comment_permissions'] as $ck => $cv)
                   {
                    $comment_permissions[] = [
                        "permission_id" => $cv['permission_id'],
                        "parent_permission_id" => $cv['parent_permission_id'],
                        "display_name" => $cv['display_name'],
                        "display_order" => $cv['display_order']];

                   }

                   foreach($valV['metadata_permissions'] as $mk => $mv)
                   {
                    $metadata_permissions[] = [
                        "permission_id" => $mv['permission_id'],
                        "parent_permission_id" => $mv['parent_permission_id'],
                        "display_name" => $mv['display_name'],
                        "display_order" => $mv['display_order']];

                   }

                   foreach($valV['node_type_permissions'] as $ntk => $ntv)
                   {
                    $node_type_permissions[] = [
                        "permission_id" => $ntv['permission_id'],
                        "parent_permission_id" => $ntv['parent_permission_id'],
                        "display_name" => $ntv['display_name'],
                        "display_order" => $ntv['display_order']];

                   }

                   foreach($valV['node_template_permissions'] as $ntek => $ntev)
                   {
                    $node_template_permissions[] = [
                        "permission_id" => $ntev['permission_id'],
                        "parent_permission_id" => $ntev['parent_permission_id'],
                        "display_name" => $ntev['display_name'],
                        "display_order" => $ntev['display_order']];

                   }

                   foreach($valV['workflow_permissions'] as $wpk => $wpv)
                   {
                    $workflow_permissions[] = [
                        "permission_id" => $wpv['permission_id'],
                        "parent_permission_id" => $wpv['parent_permission_id'],
                        "display_name" => $wpv['display_name'],
                        "display_order" => $wpv['display_order']];

                   }

                   foreach($valV['role_user_permissions'] as $rupk => $rupv)
                   {
                    $role_user_permissions[] = [
                        "permission_id" => $rupv['permission_id'],
                        "parent_permission_id" => $rupv['parent_permission_id'],
                        "display_name" => $rupv['display_name'],
                        "display_order" => $rupv['display_order']];

                   }

                   foreach($valV['notification_permissions'] as $npk => $npv)
                   {
                    $notification_permissions[] = [
                        "permission_id" => $npv['permission_id'],
                        "parent_permission_id" => $npv['parent_permission_id'],
                        "display_name" => $npv['display_name'],
                        "display_order" => $npv['display_order']];

                   }

                   foreach($valV['public_review_permissions'] as $prpk => $prpv)
                   {
                    $public_review_permissions[] = [
                        "permission_id" => $prpv['permission_id'],
                        "parent_permission_id" => $prpv['parent_permission_id'],
                        "display_name" => $prpv['display_name'],
                        "display_order" => $prpv['display_order']];

                   }

                   foreach($valV['note_permissions'] as $npk => $npv)
                   {
                    $note_permissions[] = [
                        "permission_id" => $npv['permission_id'],
                        "parent_permission_id" => $npv['parent_permission_id'],
                        "display_name" => $npv['display_name'],
                        "display_order" => $npv['display_order']];

                   }

                   foreach($valV['pacing_guide_permissions'] as $pgk => $pgv)
                   {
                    $pacing_guide_permissions[] = [
                        "permission_id" => $pgv['permission_id'],
                        "parent_permission_id" => $pgv['parent_permission_id'],
                        "display_name" => $pgv['display_name'],
                        "display_order" => $pgv['display_order']];

                   }
               }
               $finalPermissionArry = [
                "stage_id" =>  $valV['stage_id'],  
                "project_permissions"       => array_values($this->sortArray($project_permissions,'display_order')),
                "taxonomy_permissions"      => array_values($this->sortArray($taxonomy_permissions,'display_order')),
                "comment_permissions"       => array_values($this->sortArray($comment_permissions,'display_order')),
                "metadata_permissions"      => array_values($this->sortArray($metadata_permissions,'display_order')),
                "node_type_permissions"     => array_values($this->sortArray($node_type_permissions,'display_order')),
                "node_template_permissions" => array_values($this->sortArray($node_template_permissions,'display_order')),
                "workflow_permissions"      => array_values($this->sortArray($workflow_permissions,'display_order')),
                "role_user_permissions"     => array_values($this->sortArray($role_user_permissions,'display_order')),
                "notification_permissions"  => array_values($this->sortArray($notification_permissions,'display_order')),
                "public_review_permissions" => array_values($this->sortArray($public_review_permissions,'display_order')),
                "note_permissions"          => array_values($this->sortArray($note_permissions,'display_order')),
                "pacing_guide_permissions"  => array_values($this->sortArray($pacing_guide_permissions,'display_order')),
               ];
                 array_push($stageArr,$finalPermissionArry);
            }
            
            $loginProjectPermission[$loginProjectPermissionK]['stages'] = $stageArr;
            $finalArray[] =  $loginProjectPermission[$loginProjectPermissionK];
        }
        //die();
        $this->setGroupedProjectRolePermissions($finalArray);
    }

    private function groupRolePermissions($projectId, $stagesData,$permissionList) {
        $permission = [
            "project_permissions"       => $this->helperToReturnParsedOutPermissions(1, $permissionList),
            "taxonomy_permissions"      => $this->helperToReturnParsedOutPermissions(2, $permissionList),
            "comment_permissions"       => $this->helperToReturnParsedOutPermissions(7, $permissionList),
            "metadata_permissions"      => $this->helperToReturnParsedOutPermissions(5, $permissionList),
            "node_type_permissions"     => $this->helperToReturnParsedOutPermissions(6, $permissionList),
            "node_template_permissions" => $this->helperToReturnParsedOutPermissions(8, $permissionList),
            "workflow_permissions"      => $this->helperToReturnParsedOutPermissions(3, $permissionList),
            "role_user_permissions"     => $this->helperToReturnParsedOutPermissions(4, $permissionList),
            "notification_permissions"  => $this->helperToReturnParsedOutPermissions(9, $permissionList),
            "public_review_permissions" => $this->helperToReturnParsedOutPermissions(10, $permissionList),
            "note_permissions"          => $this->helperToReturnParsedOutPermissions(11, $permissionList),
            "pacing_guide_permissions"  => $this->helperToReturnParsedOutPermissions(12, $permissionList)
        ];

        $groupByPermissionsName = [
            "project_id" => $projectId,
            "stages"     => [array_merge($stagesData,$permission)]
        ];
       return $groupByPermissionsName;
    }

    private function groupRoleSystemPermissions($projectId, $systemPermissionList) {
        $sysPermissions = [
            "project_id"                => $projectId,
            "project_permissions"       => $this->helperToReturnParsedOutPermissions(1, $systemPermissionList),
            "taxonomy_permissions"      => $this->helperToReturnParsedOutPermissions(2, $systemPermissionList),
            "comment_permissions"       => $this->helperToReturnParsedOutPermissions(7, $systemPermissionList),
            "metadata_permissions"      => $this->helperToReturnParsedOutPermissions(5, $systemPermissionList),
            "node_type_permissions"     => $this->helperToReturnParsedOutPermissions(6, $systemPermissionList),
            "node_template_permissions" => $this->helperToReturnParsedOutPermissions(8, $systemPermissionList),
            "workflow_permissions"      => $this->helperToReturnParsedOutPermissions(3, $systemPermissionList),
            "role_user_permissions"     => $this->helperToReturnParsedOutPermissions(4, $systemPermissionList),
            "notification_permissions"  => $this->helperToReturnParsedOutPermissions(9, $systemPermissionList),
            "public_review_permissions" => $this->helperToReturnParsedOutPermissions(10,$systemPermissionList),
            "note_permissions"          => $this->helperToReturnParsedOutPermissions(11,$systemPermissionList),
            "pacing_guide_permissions"  => $this->helperToReturnParsedOutPermissions(12,$systemPermissionList)
        ];

        return $sysPermissions;
    }

    private function groupSystemRolePermissions(){
        $systemRole = $this->getUserSystemRole();
        if(isset($systemRole)){
        $roleDetail = $this->roleRepository->getRelatedModels($systemRole->role_id);
        $permissionList = $roleDetail->permissions;   
        $systemPermissionList = $this->groupRoleSystemPermissions('', $permissionList);

        $this->setGroupedSystemRolePermissions($systemPermissionList);
        }
    }

    private function helperToReturnParsedOutPermissions($permissionGroupType, $permissions): array {  

        $groupedPermissions = $permissions->filter(function ($item, $key) use($permissionGroupType) { 
            if($item->permission_group===$permissionGroupType) { 
                return $item; 
            } 
        });
        $parsedPermissions = [];

        foreach($groupedPermissions as $permission) {
            if($permission->permission_id != 'acee9a40-f0c0-4576-9f5c-0becfc177907'){
                $parsedPermissions[] = [
                    "permission_id" => $permission->permission_id,
                    "parent_permission_id" => $permission->parent_permission_id,
                    "display_name" => $permission->display_name,
                    "display_order" => $permission->order
                ];
            }            
        }

        array_multisort(array_column($parsedPermissions, 'display_order'), SORT_ASC, $parsedPermissions);
        return $parsedPermissions;
    }

    private function getRole($organization_id,$user_id)
    {
        $roleInfo = DB::table('users_organization')
        ->select('role_id')
        ->where('organization_id',$organization_id)
        ->where('user_id',$user_id)
        ->where('is_active','1')
        ->where('is_deleted','0')
        ->get()
        ->toArray();
        if(count($roleInfo) > 0)
        {
            return $roleInfo[0]->role_id;
        }
        else
        {
            return "";
        }
    }    

    private function getActiveOrganizationByUserId()
    {
        $user = $this->getUser();
		// If user exists, then get active org
        $organization = null;
        if($user){
            $organization = DB::table('users_organization')
            ->select('users_organization.user_id','users_organization.organization_id')
            ->join('organizations','users_organization.organization_id', '=', 'organizations.organization_id')
            ->where('users_organization.user_id',$user->user_id)
            ->where('users_organization.is_deleted',0)
            ->where('users_organization.is_active',1)
            ->where('organizations.is_active',1)
            ->get()
            ->first();
        }
        
        return $organization;
    }

    // Function to update current organization id and update user details in current object
    private function updateCurrentOrganization($currOrganization){
        if(!empty($currOrganization)){
            DB::table('users')
            ->where('user_id','=',$currOrganization->user_id)
            ->update(['current_organization_id'=>$currOrganization->organization_id]);
            $user = $this->fetchUser();
            $this->setUser($user);
        }
    }

    private function sortArray($data, $field)
    {
      if(!is_array($field)) $field = array($field);
      usort($data, function($a, $b) use($field) {
        $retval = 0;
        foreach($field as $fieldname) {
          if($retval == 0) $retval = strnatcmp($a[$fieldname],$b[$fieldname]);
        }
        return $retval;
      });
      return $data;
    }
}
