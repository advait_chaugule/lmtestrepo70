<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;
use Illuminate\Support\Facades\Hash;

class ChangePasswordJob extends Job
{   
    
    private $userId;
    private $currentPassword;
    private $newPassword;
    private $confirmNewPassword;
   
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$currentPassword,$newPassword,$confirmNewPassword)
    {
        //Set the private variable
        $this->userId             = $userId;
        $this->currentPassword    = $currentPassword;
        $this->newPassword        = $newPassword;
        $this->confirmNewPassword = $confirmNewPassword;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Set the db handler
        return $this->checkPassword($this->userId,$this->currentPassword,$this->newPassword, $this->confirmNewPassword );
    }

    private function checkPassword($userId, $currentPassword,$newPassword,$confirmNewPassword)
    { 
       $checkQuery= DB::table('users')
                ->where('user_id',$userId)
                ->where('is_deleted',0)
                ->where('is_active',1)
                ->get()
                ->toArray();  
      
            if(!empty($checkQuery)){
                 $getPassword = $checkQuery[0]->password; 
            }
              
            if(Hash::check($currentPassword,$getPassword)){ 

                if(Hash::check($newPassword,$getPassword)){
                    return 3;
                }
                if($this->newPassword!=$this->confirmNewPassword){
                    return 2;         
                }else{
                        $this->newPassword=bcrypt($newPassword);
                        $this->confirmNewPassword=bcrypt($confirmNewPassword);
                        $this->changePassword($this->userId,$this->newPassword);
                }
            }else{
                return 1;           
            } 
    }
    private function changePassword($userId,$newPassword)
    {
            $updateQuery= ['password'=> $newPassword];
            $changeQuery = DB::table('users')
                        ->where('user_id',$userId)
                        ->where('is_deleted',0)
                        ->where('is_active',1)
                        ->update($updateQuery);
    }
}


