<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;
class SetUserOrganizationListJob extends Job
{
    private $userId;
    private $organizationId;

    public function __construct($input){
        $this->userId         = $input['user_id'];
        $this->organizationId = $input['organization_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {
        $query =  DB::table('users')
                  ->where('user_id','=',$this->userId)
                  ->update(['current_organization_id'=> $this->organizationId]);
        return $query;
    }
}