<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class ProjectCountforUsersJob extends Job
{
    private $projectRepository;

    private $userIdentifierSet;
    private $organizationIdentifier;
    private $projectCount;
    private $countArray;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $userIdentifierArray, string $organizationIdentifier)
    {
        //Set identifier from input
        $this->setUserIdentifierSet($userIdentifierArray);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set the project repo handler
        $this->projectRepository = $projectRepository;

        $projectCountDetail = $this->fetchProjectCount();
        $this->setProjectCount($projectCountDetail);

        $this->parseProjectCountResponse();

        $countArray = $this->getParsedProjectCount();
        return $countArray;
    }

    // Public Getter and Setter methods
    public function setUserIdentifierSet(array $identifier) {
        $this->userIdentifierSet = $identifier;
    }

    public function getUserIdentifierSet() {
        return $this->userIdentifierSet;
    }

    public function setOrganizationIdentifier(string $identifier) {
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setProjectCount($projectCountDetail) {
        $this->projectCount = $projectCountDetail;
    }

    public function getProjectCount() {
        return $this->projectCount;
    }

    public function setParsedProjectCount($countArray) {
        $this->countArray = $countArray;
    }

    public function getParsedProjectCount() {
        return $this->countArray;
    }

    private function fetchProjectCount(){
        $userIdentifierSet = $this->getUserIdentifierSet();
        $organizationIdentifier = $this->getOrganizationIdentifier();

        $model = 'project_users';
        $mapColumn = 'user_id';

        $queryParam = ['is_deleted' => '0','organization_id'=>$organizationIdentifier];
        $fieldsToCount = 'project_id';
        
        $usageCount = $this->projectRepository->projectCount($model, $mapColumn, $userIdentifierSet, $fieldsToCount, $queryParam);

        return $usageCount;
    }

    private function parseProjectCountResponse(){
        $count = [];
        $projectCount = $this->getProjectCount();
        
        foreach($projectCount as $key => $countArray){
            $count[$key] = !empty($countArray) ? $countArray : 0 ; 
        }

        $this->setParsedProjectCount($count);
    }
}
