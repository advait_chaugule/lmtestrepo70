<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;

class EditUserJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data ;

        DB::table('users')
              ->where('username', $data['username'])
              ->orWhere('email', $data['email'])
              ->orWhere('user_id', $data['user_id'])
              ->where('organization_id', $data['organization_id'])
              ->update([ 'first_name' => $data['first_name'], 'last_name'=> $data['last_name']]);

             

    }
}
