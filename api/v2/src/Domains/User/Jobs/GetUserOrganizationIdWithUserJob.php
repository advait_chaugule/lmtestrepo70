<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use App\Data\Models\UserOrganization;
class GetUserOrganizationIdWithUserJob extends Job
{
    private $userId;
    private $organizationId;

    public function __construct($userId,$organizationId){
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
    }

    public function handle(){
        $query = UserOrganization:: select('organization_id')
            ->where('user_id','=',$this->userId )
            ->where('organization_id','=',$this->organizationId)
            ->first()
            ->toArray();
        return $query;
    }
}