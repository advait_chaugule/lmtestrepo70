<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;
class GetUserOrganizationListJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $userId;

    public function __construct($input){
        $this->userId         = $input['user_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $queryData = DB::table('users_organization')->select('users_organization.organization_id','organizations.name as organization_name')
                     ->join('organizations','organizations.organization_id','=','users_organization.organization_id')
                     ->where('users_organization.user_id','=',$this->userId)
                     ->where('users_organization.is_active','=',1)
                     ->where('organizations.is_active','=',1)
                     ->where('users_organization.is_deleted','=',0)
                     ->get();
        return $queryData;
    }
}