<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;
use Illuminate\Support\Facades\Hash;

class GetEditedProfileJob extends Job
{   
    private $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        //Set the private variable
       $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Set the db handler
       return $this->getEditedProfile($this->request);
    }

    public function getEditedProfile($request)
    {   
        $user = $request['auth_user']['user_id'];
    
        $userList = DB::table('users')
                ->select('user_id','email','first_name','last_name','reg_user_type','reg_teacher_grades','reg_teacher_subjects','reg_admin_languages','reg_admin_other_languages')
                ->where('user_id',$user)
                ->where('is_deleted',0)
                ->where('is_active',1)
                ->get()
                ->first();
        foreach ($userList as $key => $value) {
            if (is_null($value) || empty($value)) {
                $userList->$key = "";
            }
        }
        return $userList;    
    }
}


