<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class ConfirmUserSelfRegisterationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        return $this->fetchUser();
    }

    private function fetchUser() {
        $requestData = $this->getRequestData();
        $activeAccessToken = $requestData['active_access_token'];
        $userLoginConditionAttributeData = [ "active_access_token" => $activeAccessToken];
        $returnCollection = false;
        $fieldsToReturn = [ 
            "user_id", "password", "active_access_token", "email", 
            "first_name", "last_name", "role_id", "organization_id", 
            "updated_at", "is_deleted", "is_active","self_reg_expiry"
        ];
        $user = $this->userRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $userLoginConditionAttributeData);
        if($user == null)
        {
            return 0;
        }
        else
        {
            $userInfo = $user->toArray();
            $selfRegExpiry = strtotime($userInfo['self_reg_expiry']);
            $currentTime = strtotime(date("Y-m-d H:i:s"));
            
            if($selfRegExpiry>$currentTime && $userInfo['is_active'] == 0 && $userInfo['is_deleted'] == 0)
            {   
                //Self Register Successfully
                $this->updateUsers($activeAccessToken);
                return 1;
            }
            else if($selfRegExpiry>$currentTime && $userInfo['is_active'] == 1 && $userInfo['is_deleted'] == 0)
            {
                //Account Already Activated!!Login
                return 2;
            }
            else
            {
                //Link Expired.Self Register Again
                return 3;
            }
        }
        
    }

    public function updateUsers($activeAccessToken){

       DB::table('users')
            ->where('active_access_token',$activeAccessToken )
            ->update(['is_active' => 1,'self_reg_status'=>1]);
            
            $userDetails = $this->userRepository->findBy('active_access_token',$activeAccessToken);
            $organizationId = $userDetails['organization_id'];
            $input['current_organization_id']= $organizationId;
            $input['is_active']= 1;
            $input['is_register']= 1;
            $userResult = $this->userRepository->edit($userDetails['user_id'], $input);
            $userOrg  = ['user_id'         => $userDetails['user_id'],
            'organization_id' => $userDetails['organization_id'],
            'role_id'         => $userDetails['role_id'],
            'is_active'       => 1,
           ];
            DB::table('users_organization')->where('user_id',$userDetails['user_id'])->where('organization_id',$userDetails['organization_id'])->update($userOrg);    

        return true;    
    }

}
