<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use DB;
class CreateUserJob extends Job
{
    private $input;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $arrData = ['organization_id'=> $this->input['organization_id'],
                    'user_id'        =>$this->input['user_id'],
                    'role_id'        => $this->input['role_id'],
                    'is_active'      => 0
            ];
        DB::table('users_organization')->insert($arrData);
        return $userRepo->fillAndSave($this->input);
    }
}
