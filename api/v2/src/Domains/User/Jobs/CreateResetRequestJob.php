<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class CreateResetRequestJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        //
         $forgot_pasword_request_token_expiry = $this->input['forgot_pasword_request_token_expiry'];
        $forgot_pasword_request_token = $this->input['forgot_pasword_request_token'];
        $email = $this->input['email'];

           
       

        // get user details
        $userDetails =  $userRepo->findByAttributesWithSpecifiedFields(
            false,['forgot_pasword_request_token', 'forgot_pasword_request_token_expiry','first_name','last_name'], ['email'=>$email, 'is_active'=>1, 'is_deleted'=>0]
        );

       
        $user_name = $userDetails['first_name'].' '.$userDetails['last_name'];
         $old_forgot_pasword_request_token_expiry = $userDetails['forgot_pasword_request_token_expiry'];
         $old_forgot_pasword_request_token = $userDetails['forgot_pasword_request_token'];
      
        
        if($old_forgot_pasword_request_token_expiry == '0000-00-00 00:00:00')
        {
            $TokenExpiry = $forgot_pasword_request_token_expiry;
            $request_token  = $forgot_pasword_request_token;
        }
        else
        {
               
            
            $oldTokenExpiry = strtotime($old_forgot_pasword_request_token_expiry);
            
             $newTokenExpiry = strtotime($forgot_pasword_request_token_expiry);
         
            if($oldTokenExpiry > $newTokenExpiry)
                {
                    $TokenExpiry    = $old_forgot_pasword_request_token_expiry;
                    $request_token  = $old_forgot_pasword_request_token;
                }   
            else
                {
                    $TokenExpiry    = $forgot_pasword_request_token_expiry;
                    $request_token  = $forgot_pasword_request_token;
                }   
                
        }
        
        $columnValuePairToUpdate = [
            // disable updating the access-token (temporary)
            //"active_access_token" => $userAccessToken,
            "forgot_pasword_request_token_expiry" => $TokenExpiry,
            "forgot_pasword_request_token" => $request_token,
        ];
        $conditionalClausePair = [ 'email'=>$email, 'is_active'=>1, 'is_deleted'=>0 ];
        $userRepo->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);

        $resultPair =  ['email' =>$email, 'reset_token' =>$request_token, 'user_name' =>$user_name]; 
        //print_r($resultPair);
         return $resultPair;
          
    }
}
