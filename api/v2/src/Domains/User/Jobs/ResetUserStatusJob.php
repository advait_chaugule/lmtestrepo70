<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;


class ResetUserStatusJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $columnValuePairToUpdate = [
            "is_deleted" => "0",
            "is_active" => "1",
        ];
        $conditionalClausePair = ['username' => $this->input['email']];
        return $userRepo->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);
    }
}
