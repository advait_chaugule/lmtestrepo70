<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Models\UserOrganization;

class UpdateUserInfoByOrganizationJob extends Job
{
    private $userIdentifier;
    private $inputData;
    private $organizationIdentifier;
    private $userRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputData)
    {
        //Set the private variables
        $this->userIdentifier           =   $inputData['user_id'];
        $this->organizationIdentifier   =   $inputData['organization_id'];
        $this->inputData                =   $inputData['user_info'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Set the User Repo handler
        $update = UserOrganization::where('user_id','=',$this->userIdentifier)
                                    ->where('organization_id','=',$this->organizationIdentifier)
                                    ->where('is_deleted','=',0)
                                    ->update(['enable_email_notification'=>$this->inputData]);
        if($update) {
            $query = UserOrganization::select('user_id','organization_id','enable_email_notification')
                ->where('user_id','=',$this->userIdentifier)
                ->where('organization_id','=',$this->organizationIdentifier)
                ->where('is_deleted','=',0)
                ->get()
                ->toArray();
            return $query;
        }

    }
}
