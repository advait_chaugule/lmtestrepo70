<?php
namespace App\Domains\User\Jobs;
use App\Services\Api\Traits\ProjectSettingTrait;
use Lucid\Foundation\Job;
class SaveProjectSettingsJob extends Job
{
    use ProjectSettingTrait;
    private $userId;
    private $organizationId;
    private $projectId;
    private $jsonData;
    private $userSetting;
    private $tableName;

    public function __construct($userId,$organizationId,$projectId,$jsonData,$tableName='')
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->projectId     = $projectId;
        $this->jsonData       = $jsonData;
        $this->tableName       = $tableName;
    }

    public function handle()
    {
        $this->mappedProjectSettingData($this->userId,$this->projectId,$this->organizationId,$this->jsonData,$this->tableName);
    }
}