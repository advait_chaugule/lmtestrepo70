<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use App\Data\Models\UserOrganization;
class GetUserEmailInfoByOrganizationIdJob extends Job
{
    private $userId;
    private $organizationId;

    public function __construct(array $input){
        $this->userId         = $input['user_id'];
        $this->organizationId = $input['organization_id'];
    }

    public function handle(){
        $query = UserOrganization:: select('organization_id','user_id','enable_email_notification as email_setting')
                                    ->where('user_id','=',$this->userId )
                                    ->where('organization_id','=',$this->organizationId)
                                    ->where('is_deleted','=',0)
                                    ->get()
                                    ->toArray();
        return $query;
    }
}