<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Domains\User\Validators\CheckBulkUserCSVValidator;
use DB;
use Illuminate\Support\Facades\Hash;

class ValidateBulkUserViaCSVJob extends Job
{
    private $firstName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CheckBulkUserCSVValidator $validator)
    {
        $input = $this->input;
        $orgID = $input['orgID'];
        $role = $input['role'];
        $row = $input['row'];
        $errors = [];
        $validation = $validator->validate($input);
        $csv_errors = $validation===true ? $validation : $validation->errors()->all();
       
        if($validation===true){

            return $validation;
        }else{
           
           $errors['errors'] = $csv_errors; 
           $errors['row'] = $row;
           
            $keys =  $errors['errors'];
            $ErrorAndRow = array_fill_keys($keys,$errors['row']);
            return $ErrorAndRow;
        }
      
    }

}