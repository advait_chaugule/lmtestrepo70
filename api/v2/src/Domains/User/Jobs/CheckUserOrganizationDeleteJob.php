<?php
/**
 * Created by PhpStorm.
 * User: shailesh.pandey
 * Date: 29-03-2019
 * Time: 16:06
 */

namespace App\Domains\User\Jobs;


use Lucid\Foundation\Job;
use DB;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;

class CheckUserOrganizationDeleteJob extends Job
{
    private $userId;
    private $organizationId;

    public function __construct($userId,$organizationId,$email)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->email = $email;
    }

    /**
     * This function is used to check organization id deleted from user
     * organization table then in user table it will change current organization Id
     */

    public function handle(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        // Added is_active condition to get only org in which user is active
        $query = DB::table('users_organization')
            ->where('user_id','=', $this->userId)
            ->where('is_active','=',1)
            ->where('is_deleted','=',0)
            ->first();

// remove count condition and just check query result using query objet in if condition
        if(!$query){
           $queryData = DB::table('users')
                        ->where('user_id','=',$this->userId)
                        ->update(['is_deleted'=>1,'is_active'=>0]);

                        $userList = $this->userRepository->getUserListForRename($this->email);
                        foreach($userList As $user)
                        {
                           
                            $user_id = $user->user_id;
                            $username = $user->username;
                            $newUsername = '*'.$username;
                            $columnValuePairToUpdate = [
                               "username" => $newUsername,
                               "email" => $newUsername,
                           ];
                           $conditionalClausePair = ['user_id' => $user_id];
                           $this->userRepository->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);   
                        }             
           return $queryData;
        }else{
            $userQuery = DB::table('users')
                        ->where('user_id','=',$this->userId)
                        ->update(['current_organization_id'=>$query->organization_id]);
            // below code is to update the access_token_updated column to preveios date to expire session of self delete user  
            $columnValuePairToUpdate = [
                 "access_token_updated_at" =>  Carbon::yesterday()->toDateTimeString(),
                ];
                $conditionalClausePair = [ "user_id" => $this->userId ];
                $this->userRepository->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);
            return $userQuery;
            
        }
       
    }
}