<?php
namespace App\Domains\User\Jobs;

use App\Data\Models\User;
use Lucid\Foundation\Job;

class GetUserCurrentOrganizationIdJob extends Job
{
    private $userId;

    public function __construct(array $input){
        $this->userId         = $input['user_id'];
    }
    // This function will return user's current organization by userId
    public function handle()
    {
        $query = User:: select('current_organization_id')
            ->where('user_id','=',$this->userId )
            ->first()
            ->toArray();
        return $query;
    }
}
