<?php
namespace App\Domains\User\Jobs;
use Illuminate\Support\Facades\Hash;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use DB;

class AddUserInOrganizationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
       
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        //
        $this->userRepository = $userRepository;
        return $this->insertUserInTenant();
    }

    private function insertUserInTenant() {
        $requestData = $this->getRequestData();
        $requestUsername = $requestData['email'];
        $requestOrganizationId = $requestData['organization_id'];
        $requestRoleId = $requestData['role_id'];
        $userLoginConditionAttributeData = [ "username" => $requestUsername];
        $returnCollection = false;
        $fieldsToReturn = [ 
            "user_id", "password", "active_access_token", "email", 
            "first_name", "last_name", "role_id", "organization_id", 
            "updated_at", "is_deleted", "is_active"
        ];
        $user = $this->userRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $userLoginConditionAttributeData);
        $userInfo = $user->toArray();
            $user_id  = $userInfo['user_id'];
            $insertUserOrg  = ['user_id'         => $user_id,
                         'organization_id' => $requestOrganizationId,
                         'role_id'         => $requestRoleId,
                         'is_active'         => 0,
                        ];
            DB::table('users_organization')->insert($insertUserOrg);
            
            $detailInfo   = ['user_id'         => $user_id,
            'organization_id' => $requestOrganizationId,
            'role_id'         => $requestRoleId,
            'active_access_token' => $userInfo['active_access_token'],
            'first_name' => $userInfo['first_name'],
            'last_name' => $userInfo['last_name'],
           ];
           return $detailInfo;
    }
}
