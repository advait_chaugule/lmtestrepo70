<?php
namespace App\Domains\User\Jobs;
use Illuminate\Support\Facades\Hash;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class IsCheckUserCurrentStatusJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
       
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        //
        $this->userRepository = $userRepository;
        return $this->fetchUser();
    }

    private function fetchUser() {
        $requestData = $this->getRequestData();
        $requestUsername = $requestData['email'];
        $userLoginConditionAttributeData = [ "username" => $requestUsername];
        $returnCollection = false;
        $fieldsToReturn = [ 
            "user_id", "password", "active_access_token", "email", 
            "first_name", "last_name", "role_id", "organization_id", 
            "updated_at", "is_deleted", "is_active"
        ];
        $user = $this->userRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $userLoginConditionAttributeData);
        if($user == null)
        {
            return 0;
        }
        else
        {
            $userInfo = $user->toArray();
            if($userInfo['is_active'] == 0 && $userInfo['is_deleted'] == 0)
            {
                return 1;
            }
            else if($userInfo['is_active'] == 0 && $userInfo['is_deleted'] == 1)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
        
    }
}
