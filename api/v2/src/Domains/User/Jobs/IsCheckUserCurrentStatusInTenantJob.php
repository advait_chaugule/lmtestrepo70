<?php
namespace App\Domains\User\Jobs;
use Illuminate\Support\Facades\Hash;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use DB;

class IsCheckUserCurrentStatusInTenantJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->setRequestData($input);
       
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        //
        $this->userRepository = $userRepository;
        return $this->fetchUser();
    }

    private function fetchUser() {
        $requestData = $this->getRequestData();
        $requestUsername = $requestData['email'];
        $requestOrganizationId = $requestData['organization_id'];
        $userLoginConditionAttributeData = [ "username" => $requestUsername];
        $returnCollection = false;
        $fieldsToReturn = [ 
            "user_id", "password", "active_access_token", "email", 
            "first_name", "last_name", "role_id", "organization_id", 
            "updated_at", "is_deleted", "is_active"
        ];
        $user = $this->userRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $userLoginConditionAttributeData);
        if($user == null)
        {
            return 0;
        }
        else
        {
            $userInfo = $user->toArray();
            $user_id  = $userInfo['user_id'];
            $userOrg = DB::table('users_organization')
            ->where('user_id',$user_id)
            ->where('organization_id',$requestOrganizationId)
            ->get()
            ->toArray();
            if($userOrg == null)
            {
                return 4;
            }
            else
            {
                if($userOrg[0]->is_active == 0 && $userOrg[0]->is_deleted == 0)
                {
                    return 1;
                }
                if($userOrg[0]->is_active == 1 && $userOrg[0]->is_deleted == 1)
                {
                    return 4;
                }
                else if($userOrg[0]->is_active == 0 && $userOrg[0]->is_deleted == 1)
                {
                    return 4;
                }
                else
                {
                    return 3;
                }
            }
           
        }  
    }
}
