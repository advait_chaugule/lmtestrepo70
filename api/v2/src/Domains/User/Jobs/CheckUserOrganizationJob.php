<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use DB;
class CheckUserOrganizationJob extends Job
{
    private $userId;
    private $organizationId;

    public function __construct($input){
        $this->userId         = $input['user_id'];
        $this->organizationId = $input['organization_id'];
    }

    public function handle()
    {
        $query = DB::table('users_organization')
                ->where('user_id','=',$this->userId)
                ->where('organization_id','=',$this->organizationId)
                ->where('is_active','=',1)
                ->where('is_deleted','=',0)
                ->get()
                ->toArray();
        return $query;
    }
}