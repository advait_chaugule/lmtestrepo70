<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use DB;
use App\Services\Api\Traits\AddDefaultSettings;
use App\Services\Api\Traits\MetadataHelperTrait;

class GetTaxonomyConfigSettingsJob extends Job
{
    private $userId;
    private $organizationId;
    use AddDefaultSettings;
    use MetadataHelperTrait;

    public function __construct($userId,$organizationId,$settingId,$feature_id)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->settingId      = $settingId;
        $this->feature_id     = $feature_id; 
    }

    public function handle()
    {
        $result = array();
        $getUserSetting = DB::table('user_settings')
                          ->where('user_id',$this->userId)
                          ->where('organization_id',$this->organizationId)
                          ->where('is_deleted',0)
                          ->where('id_type',1)
                          ->get()
                          ->toArray();
                          
        if(!empty($getUserSetting)){
            $setting_json = $getUserSetting[0]->json_config_value; 
        }else{
            $getUserSetting = DB::table('organizations')
                    ->where('organization_id',$this->organizationId)
                    ->where('is_deleted',0)
                    ->get()
                    ->toArray();
            if(!empty($getUserSetting)){
              $setting_json = $getUserSetting[0]->json_config_value;
            }
        }
        /***** Get updated metadata for Taxonomy Listing screen settings of Organization/User  *************/
        if(!empty($setting_json)){
            $decodedSettingJson = json_decode($setting_json);
            if(isset($decodedSettingJson->taxonomy_list_setting) && !empty($decodedSettingJson->taxonomy_list_setting)){
                $taxonomy_list_setting = $decodedSettingJson->taxonomy_list_setting;
                foreach($taxonomy_list_setting as $setKey => $setting){
                    if(!empty($setting->metadata_id) && isset($setting->customCol) && $setting->customCol==true){
                        $metadataId = $taxonomy_list_setting[$setKey]->metadata_id;
                        $metadataName = $this->getmetadataName($this->organizationId,array($metadataId),'metadata_id');
                        $taxonomy_list_setting[$setKey]->display_name = $taxonomy_list_setting[$setKey]->name = $metadataName[$metadataId];
                    }
                }
                $decodedSettingJson->taxonomy_list_setting = $taxonomy_list_setting;
                $setting_json = json_encode($decodedSettingJson);
            }
        }
        /***** Get updated metadata for Taxonomy Listing screen settings of Organization/User  *************/

        $nodetypeid = DB::table('node_types')
                    ->where('organization_id',$this->organizationId)
                    ->where('is_deleted',0)
                    ->where('used_for', 0)
                    ->where('is_document',1)
                    ->get()
                    ->toArray();
        
        $setting_arr = array("version"=>"1","is_user_default"=>0,"default_for_all_user"=>0,"feature_id"=>1,"setting_id"=>1,"taxonomy_list_setting"=>[]);
        $result['feature_id'] =  $this->feature_id;
        $result['setting_id'] =  $this->settingId;
        $result['user_id'] =  $this->userId; 
        $result['document_node_type_id'] = $nodetypeid[0]->node_type_id;
        $result['json_config_value'] = !empty($setting_json) ? json_decode($setting_json):$setting_arr; 
        $action = "Get";
        $super_setting = $this->updatedefaultconfigsettings($this->organizationId,0,0,$action);
        $result['super_default_json'] =  json_decode($super_setting);
        return $result;


    }
}