<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Domains\User\Validators\RegistrationInputValidator;

class ValidateuserRegistrationInputJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RegistrationInputValidator $validator)
    {
        $input['user_id']= $this->input['new_user_id'];
        $input['password']= $this->input['new_password'];
        $input['first_name']= $this->input['new_first_name'];
        $input['last_name']= $this->input['new_last_name'];
        $input['organization_id']= $this->input['new_organization_id'];
        $validation = $validator->validate($input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
