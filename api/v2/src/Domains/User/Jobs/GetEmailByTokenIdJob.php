<?php
namespace App\Domains\User\Jobs;


use Lucid\Foundation\Job;
use DB;
class GetEmailByTokenIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $input;
    private $flag = true;
    public function __construct(array $input)
    {
        if(isset($input['reset_request_token']))
        {
            $this->input = $input['reset_request_token'];
            $this->flag = true;
        }
        else
        {
            $this->input = $input['Authorization'];
            $this->flag = false;
        }
    }

    public function handle()
    {
        if( $this->flag == true)
        {
            $query = DB::table('users')->select('email')
                ->where('forgot_pasword_request_token','=',$this->input)
                ->first();
        }
        else
        {
            $query = DB::table('users')->select('email')
                ->where('active_access_token','=',$this->input)
                ->first();
        }
        return $query;
    }

    
}