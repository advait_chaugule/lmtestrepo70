<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\ProjectSettingTrait;
use App\Services\Api\Traits\MetadataHelperTrait;
use DB;
class GetProjectSettingDetailsJob extends Job
{
    use ProjectSettingTrait;
    use MetadataHelperTrait;
    private $userId;
    private $organizationId;
    private $projectId;
    private $featureId;
    private $settingId;

    public function __construct($userId,$organizationId,$projectId,$featureId,$settingId)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->projectId      =  $projectId;
        $this->featureId      =  $featureId;
        $this->settingId      =  $settingId;
    }

    public function handle()
    {
        $ProjectData = $this->getJsonDataFromProjectSettings();
        return $ProjectData;
    }

    public function ProjectJsonForSuperDefault()
    {
        // Removed hard coded metadata and added code to fetch from db - Start
        $defaultInternalNames = array("full_statement","node_type","human_coding_scheme");
        $metadataName = $this->getmetadataName($this->organizationId,$defaultInternalNames);
        $default_project_list = array(array($metadataName["full_statement"],"full_statement","300"),array($metadataName["node_type"],"node_type","300"),array($metadataName["human_coding_scheme"],"human_coding_scheme","300"));
        // Removed hard coded metadata and added code to fetch from db - End
        $num = 1;
        $valueArr =[ 'table_name' => 'supper_default_table',
            'is_user_default' => 0,
            'default_for_all_user' => 0,
            'view_type'=> 'default_table',
        ];
        $valuesArray =[];
        foreach ($default_project_list as $default_project_listK=>$default_project_listV)
        {
            $valuesArray[] = [
                'node_type_id'=>'',
                'display_name' => '',
                'internal_name'=> '',
                'order' => $num,
                'width' => $default_project_listV[2],
                "metadata"=>[['metadata_id'=> $this->getmetadataid($this->organizationId,$default_project_listV[1]),
                    'display_name' => $default_project_listV[0],
                    'internal_name'=> $default_project_listV[1],
                    'order' => $num,
                    'width' => $default_project_listV[2],
                    'is_custom' => 0,
                    ]
                ]];
            $num++;
        }
        $tableConfig = ['table_config'=>$valuesArray];
        $finalArr    = array_merge($valueArr,$tableConfig);
        return $finalArr;
    }

      /**
     * @return mixed
     * @Function getJsonDataFromUserSettings
     * @Purpose This function is used to get data from user settings
     * table based on userId and OrganizationId
     */
    public function getJsonDataFromProjectSettings()
    {
        $getProjectSetting = DB::table('user_settings')
            ->where('user_id',$this->userId)
            ->where('id', $this->projectId)
            ->where('organization_id',$this->organizationId)
            ->where('is_deleted',0)
            ->where('id_type',4)
            ->get()
            ->toArray();

            $getProjectData=[];
            $data     = $this->ProjectJsonForSuperDefault();
            $lastTableView = "";
            if($getProjectSetting) {
                foreach ($getProjectSetting as $getProjectSettingK => $getProjectSettingV) {
                    $jsonProjectData = $getProjectSettingV->json_config_value;
                    $arrData   = json_decode($jsonProjectData);
                    $version   = $arrData->version;
                    $featureId = isset($arrData->feature_id)?$arrData->feature_id:$this->featureId;
                    $settingId = isset($arrData->setting_id)?$arrData->setting_id:$this->settingId;
                    $lastTableView   = isset($arrData->last_table_view)?$arrData->last_table_view:'';
                    $projectSetting = !empty($arrData->project_table_views)?$arrData->project_table_views:[];
                    /***** Get updated metadata for Project settings of User  *************/
                    if(isset($projectSetting[0]->table_config) && !empty($projectSetting[0]->table_config)){
                        $tableConfig = $projectSetting[0]->table_config;
                        $view_type = $projectSetting[0]->view_type;
                        $documentId  = $this->getProjectDocument();
                        $projectSetting[0]->table_config = $this->getUpdatedConfigMetadata($tableConfig, $this->organizationId, $view_type, $documentId);
                    }
                    /***** Get updated metadata for Project settings of User  *************/          
                    $getProjectData = [ 'version'        => $version,
                                     'feature_id'        => $featureId,
                                     'setting_id'        => $settingId,
                                     'last_table_view'   => $lastTableView,
                                     'user_id'           => $this->userId,
                                     'organization_id'   => $getProjectSettingV->organization_id,
                                     'project_id'       =>  $this->projectId,
                                     'id_type'           => '4',
                                     'updated_by'        => $getProjectSettingV->updated_by,
                                     'project_table_views' => array_prepend($projectSetting,$data)
                                ];
                }
            }else{
                $getProjectData = [ 'version'  => 1,
                    'feature_id'        => $this->featureId,
                    'setting_id'        => $this->settingId,
                    'user_id'           => $this->userId,
                    'organization_id'   => $this->organizationId,
                    'project_id'        => $this->projectId,
                    'id_type'           => '4',
                    'updated_by'           => $this->userId  ,
                    'project_table_views' => [$data]
                ];

                // Get default project settings applicable to all users of tenant 
                $getProjectSetting = $this->getProjectDefaultSetting($this->projectId, $this->organizationId);
                if (!empty($getProjectSetting) && !empty($getProjectSetting->display_options)) {
                    $projectJsonArr = json_decode($getProjectSetting->display_options, true);
                    if($projectJsonArr['project_table_views']['default_for_all_user'] == 1 && $lastTableView!=$projectJsonArr['last_table_view']){
                        if(!isset($getProjectData['last_table_view'])){
                            $getProjectData['last_table_view'] = $projectJsonArr['last_table_view'];                
                        }
                        /***** Get updated metadata for Project settings of Organization  *************/
                        $projectSetting = $projectJsonArr['project_table_views'];
                        if(isset($projectSetting['table_config']) && !empty($projectSetting['table_config'])){
                            $tableConfig = $projectSetting['table_config'];
                            $view_type = $projectSetting['view_type'];
                            $documentId  = $this->getProjectDocument();
                            $projectJsonArr['project_table_views']['table_config'] = $this->getUpdatedConfigMetadata($tableConfig,$this->organizationId, $view_type,$documentId);
                        }
                        /***** Get updated metadata for Project settings of Organization  *************/
                        $getProjectData['project_table_views'] = array_merge($getProjectData['project_table_views'],[$projectJsonArr['project_table_views']]);
                    }            
                }
            }
            return $getProjectData;
    }

    private function getmetadataid($orgnizationIdentifier,$metadataname){
        $queryData = DB::table('metadata')->select('metadata_id')
       ->where('organization_id','=', $orgnizationIdentifier)
       ->where('internal_name','=',$metadataname)
       ->get()
       ->toArray();

       if(!empty($queryData)){
           $metadata_id = $queryData[0]->metadata_id;
       }else{
           $metadata_id = "";
       }
       return $metadata_id;
    }

    /**
     * @param $projectId
     * @param $organizationId
     * @return mixed
     * @FunctionName getProjectDefaultSetting
     */
    private function getProjectDefaultSetting($projectId,$organizationId)
    {
          $getProjectJson =  DB::table('projects')->select('display_options')
                               ->where('project_id',$projectId)
                               ->where('organization_id',$organizationId)
                               ->where('is_deleted',0)
                               ->get()
                               ->first();
          return $getProjectJson;

    }
    

    private function getProjectItemId($projectId)
    {
        $itemIds = DB::table('project_items')
                  ->select('item_id')
                  ->where('project_id',$projectId)
                  ->where('is_deleted',0)
                  ->distinct('item_id')
                  ->get();
        if($itemIds) {
            $itemIds = $itemIds->toArray();
            return $itemIds;
        }
    }

    /**
     * @param $itemIds
     * @param $organizationId
     * @return mixed
     * @FunctionName getNodeTypeIds
     * @Purpose This function is used to return node_type_id from Item table based on Item Ids
     */
    private function getNodeTypeIds($itemIds,$organizationId)
    {
        $nodeTypeId = DB::table('items')
                      ->select('node_type_id','document_id')
                      ->whereIn('item_id',$itemIds)
                      ->where('organization_id',$organizationId)
                      ->where('is_deleted',0)
                      ->distinct('node_type_id')
                      ->get();
        if($nodeTypeId) {
            $nodeTypeId = $nodeTypeId->toArray();
            return $nodeTypeId;
        }
    }
    
    private function getProjectDocument(){
        $projectItemIds = $this->getProjectItemId($this->projectId);
        $itemIds        = array_column($projectItemIds,'item_id');
        $getNodeTypeIds = $this->getNodeTypeIds($itemIds,$this->organizationId);
        $documentId  = array_column($getNodeTypeIds,'document_id');
        return !empty($documentId) ? $documentId[0] : "";
    }
}