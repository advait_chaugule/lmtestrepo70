<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\UserSettingTrait;
use App\Services\Api\Traits\MetadataHelperTrait;
use DB;
class GetUserSettingDetailsJob extends Job
{
    use UserSettingTrait;
    use MetadataHelperTrait;
    private $userId;
    private $organizationId;
    private $documentId;
    private $settingId;
    private $featureId;

    public function __construct($userId,$organizationId,$documentId,$settingId,$featureId)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->documentId     = $documentId;
        $this->settingId     = $settingId;
        $this->featureId     = $featureId;
    }

    public function handle()
    {
        $jsonData = $this->getJsonDataFromUserSettings();
        return $jsonData;
    }
    /**
     * @return mixed
     * @Function getJsonDataFromUserSettings
     * @Purpose This function is used to get data from user settings
     * table based on userId and OrganizationId
     */
    public function getJsonDataFromUserSettings()
    {
        $getUserSetting = DB::table('user_settings')
            ->where('user_id',$this->userId)
            ->where('id', $this->documentId)
            ->where('organization_id',$this->organizationId)
            ->where('is_deleted',0)
            ->where('id_type',3)
            ->get()
            ->toArray();

        $getJsonData=[];
        $data     = $this->prepareJsonForSuperDefault();
        $lastTableView = "";
        if($getUserSetting) {
            foreach ($getUserSetting as $getUserSettingK => $getUserSettingV) {
                $jsonData = $getUserSettingV->json_config_value;
                $arrData   = json_decode($jsonData);
                $version   = $arrData->version;
                $featureId = isset($arrData->feature_id)?$arrData->feature_id:$this->featureId;
                $settingId = isset($arrData->setting_id)?$arrData->setting_id:$this->settingId;
                $lastTableView   = isset($arrData->last_table_view)?$arrData->last_table_view:'';
                $taxonomySetting = !empty($arrData->taxonomy_table_views)?$arrData->taxonomy_table_views:[];
                /***** Get updated metadata for Taxonomy settings of User  *************/
                if(isset($taxonomySetting[0]->table_config) && !empty($taxonomySetting[0]->table_config)){
                    $tableConfig = $taxonomySetting[0]->table_config;
                    $view_type = $taxonomySetting[0]->view_type;
                    $taxonomySetting[0]->table_config = $this->getUpdatedConfigMetadata($tableConfig, $this->organizationId, $view_type, $this->documentId);
                }
                /***** Get updated metadata for Taxonomy settings of User  *************/
                $getJsonData = [ 'version'           => $version,
                                 'feature_id'        => $featureId,
                                 'setting_id'        => $settingId,
                                 'last_table_view'   => $lastTableView,
                                 'user_id'           => $this->userId,
                                 'organization_id'   => $getUserSettingV->organization_id,
                                 'document_id'       => $this->documentId,
                                 'id_type'           => '3',
                                 'updated_by'        => $getUserSettingV->updated_by,
                                 'taxonomy_table_views' =>array_prepend($taxonomySetting,$data)
                            ];
            }
        }else{
            $getJsonData = [ 'version'  => 1,
                'feature_id'        => $this->featureId,
                'setting_id'        => $this->settingId,
                'user_id'           => $this->userId,
                'organization_id'   => $this->organizationId,
                'document_id'       => $this->documentId,
                'id_type'           => '3',
                'updated_by'           => $this->userId  ,
                'taxonomy_table_views' => [$data]
            ];
            // Get default document settings applicable to all users of tenant 
            $getDocumentSetting = $this->getDocumentDefaultSetting($this->documentId, $this->organizationId);
            if (!empty($getDocumentSetting) && !empty($getDocumentSetting->display_options)) {
                $documentJsonArr = json_decode($getDocumentSetting->display_options, true);
                if($documentJsonArr['taxonomy_table_views']['default_for_all_user'] == 1 && $lastTableView!=$documentJsonArr['last_table_view']){
                    if(!isset($getJsonData['last_table_view'])){
                        $getJsonData['last_table_view'] = $documentJsonArr['last_table_view'];                
                    }
                    /***** Get updated metadata for Taxonomy settings of Organization  *************/
                    $taxonomySetting = $documentJsonArr['taxonomy_table_views'];
                    if(isset($taxonomySetting['table_config']) && !empty($taxonomySetting['table_config'])){
                        $tableConfig = $taxonomySetting['table_config'];
                        $view_type = $taxonomySetting['view_type'];
                        $documentJsonArr['taxonomy_table_views']['table_config'] = $this->getUpdatedConfigMetadata($tableConfig,$this->organizationId, $view_type, $this->documentId);
                    }
                    /***** Get updated metadata for Taxonomy settings of Organization  *************/                    
                    $getJsonData['taxonomy_table_views'] = array_merge($getJsonData['taxonomy_table_views'],[$documentJsonArr['taxonomy_table_views']]);
                }            
            }
        }		

        return $getJsonData;
    }

    public function prepareJsonForSuperDefault()
    {
        // Removed hard coded metadata and added code to fetch from db - Start
        $defaultInternalNames = array("full_statement","node_type","human_coding_scheme");
        $metadataName = $this->getmetadataName($this->organizationId,$defaultInternalNames);
        
        $default_taxonomy_list = array(array($metadataName["full_statement"],"full_statement","300"),array($metadataName["node_type"],"node_type","300"),array($metadataName["human_coding_scheme"],"human_coding_scheme","300"));
        // Removed hard coded metadata and added code to fetch from db - End
        $num = 1;
        $valueArr =[ 'table_name' => 'supper_default_table',
            'is_user_default' => 0,
            'default_for_all_user' => 0,
            'view_type'=> 'default_table',
        ];
        $valuesArray =[];
        foreach ($default_taxonomy_list as $default_taxonomy_listK=>$default_taxonomy_listV)
        {
            $valuesArray[] = [
                'node_type_id'=>'',
                'display_name' => '',
                'internal_name'=> '',
                'order' => $num,
                'width' => $default_taxonomy_listV[2],
                "metadata"=>[['metadata_id'=> $this->getmetadataid($this->organizationId,$default_taxonomy_listV[1]),
                    'display_name' => $default_taxonomy_listV[0],
                    'internal_name'=> $default_taxonomy_listV[1],
                    'order' => $num,
                    'width' => $default_taxonomy_listV[2],
                    'is_custom' => 0,
                    ]
                ]];
            $num++;
        }
        $tableConfig = ['table_config'=>$valuesArray];
        $finalArr    = array_merge($valueArr,$tableConfig);
        return $finalArr;
    }

    private function getmetadataid($orgnizationIdentifier,$metadataname){
        $queryData = DB::table('metadata')->select('metadata_id')
       ->where('organization_id','=', $orgnizationIdentifier)
       ->where('internal_name','=',$metadataname)
       ->get()
       ->toArray();

       if(!empty($queryData)){
           $metadata_id = $queryData[0]->metadata_id;
       }else{
           $metadata_id = "";
       }
       return $metadata_id;
    }

    /**
     * @param $documentId
     * @param $organizationId
     * @return mixed
     * @FunctionName getDocumentDefaultSetting
     */
    private function getDocumentDefaultSetting($documentId,$organizationId)
    {
          $getDocumentJson =  DB::table('documents')->select('display_options')
                               ->where('document_id',$documentId)
                               ->where('organization_id',$organizationId)
                               ->where('is_deleted',0)
                               ->get()
                               ->first();
          return $getDocumentJson;

    }

}