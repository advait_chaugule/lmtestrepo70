<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;
use DB;
class GetUserByPermissionJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $input = $this->data;
        //$permissionName="View Published Taxonomies";
        $permissionName= $input['permission_name'];
        $getPermissionId = $this->getPermissionId($permissionName);
        $organization_id = $input['organization_id'];
		if(isset($input['user_id'])){
            return $this->checkUserHasPermissionInRoles($organization_id,$input['user_id'],$getPermissionId);
        }
          $getOrgRoleList = $this->getOrgRoleList($organization_id);
          $getFilterRoleList = $this->getRoleList($getPermissionId,$getOrgRoleList);
          if($getFilterRoleList !=0)
          {
            $getUserList = $this->getUserList($getFilterRoleList);
            return $getUserList;
          }
          else
          {
              return 0;
          }
      //  
    }

    private function getPermissionId($permissionName){

    $permissionDetails =DB::table('permissions')
    ->select('permission_id')
    ->where('display_name', '=',$permissionName)
    ->limit(1)->get();
    $permissionDetails = $permissionDetails->toArray();
    return $permissionDetails[0]->permission_id;
    }

    private function getOrgRoleList($organization_id)
    {
        $orgRoleDetails =DB::table('roles')
        ->select('role_id')
        ->where('organization_id', '=',$organization_id)
        ->get();
        if(count($orgRoleDetails) > 0)
        {
            $orgRoleDetails = $orgRoleDetails->toArray();
        
             $roleArr = array();
        
            foreach($orgRoleDetails as $orgRole)
            {
                $roleArr[] =  $orgRole->role_id;
            }
            return $roleArr;
        }
        else
        {
            return 0;
        }
        
    }

    private function getRoleList($getPermissionId,$getOrgRoleList){

        $roleDetails =DB::table('role_permission')
        ->select('role_id')
        ->where('permission_id', '=',$getPermissionId)
        ->whereIn('role_id',$getOrgRoleList)
        ->get();
        if(count($roleDetails) > 0)
        {
            $roleDetails = $roleDetails->toArray();
        
             $roleArr1 = array();
        
            foreach($roleDetails as $role)
            {
                $roleArr1[] =  $role->role_id;
            }
            return $roleArr1;
        }
        else
        {
            return 0;
        }
       
    }

    private function getUserList($getFilterRoleList){

        $userDetails =DB::table('users')
        ->join('users_organization','users_organization.user_id','=','users.user_id')
        ->where('users_organization.is_active', '=',"1")
        ->where('users_organization.is_deleted', '=', "0")
        ->whereIn('users_organization.role_id',$getFilterRoleList)
        ->get();
        if(count($userDetails) > 0)
        {
            $userDetails = $userDetails->toArray();
            return $userDetails;
        }
        else
        {
            return 0;
        }
        

    }

    private function checkUserHasPermissionInRoles($organization_id,$userId,$getPermissionId){

        $userDetails =DB::table('users')        
        ->join('users_organization','users_organization.user_id','=','users.user_id')
        ->select('users.role_id')
        ->where('users_organization.is_active', '=',"1")
        ->where('users_organization.is_deleted', '=', "0")
        ->where('users_organization.organization_id', '=', $organization_id)
        ->where('users.user_id',$userId)
        ->get()->toArray();
        if(!empty($userDetails))
        {
            $getRoleList = array_column($userDetails,'role_id');
            $getFilterRoleList = $this->getRoleList($getPermissionId,$getRoleList);
            return !empty($getFilterRoleList);
        }        

        return false; //If isset it means use has permission in that role
    }
}
