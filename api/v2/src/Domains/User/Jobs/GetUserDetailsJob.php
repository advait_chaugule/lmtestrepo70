<?php
namespace App\Domains\User\Jobs;
use Illuminate\Http\Request;
use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class GetUserDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository, Request $request)
    {
        $active_access_token = $request->route('active_access_token');
       
        // set user data repository
        $this->userRepository = $userRepository;

        // get user details
        $userDetails =  $userRepository->findByAttributesWithSpecifiedFields(
            false,['user_id', 'username', 'organization_id'], ['active_access_token'=>$active_access_token, 'is_active'=>0]
        );

        // prepare the response
        return $userDetails;
    }
}
