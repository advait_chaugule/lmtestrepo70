<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;

class UpdateProfileJob extends Job
{
   private $userId;
   private $firstName;
   private $lastName;
   private $reg_user_type;
   private $teacher_grade_level_taught;
   private $teacher_subject_area_taught;
   private $admin_district_language_taught;
   private $admin_district_other_language_taught;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$firstName,$lastName,$reg_user_type,$teacher_grade_level_taught,$teacher_subject_area_taught,$admin_district_language_taught,$admin_district_other_language_taught)
    {
        //Set the private variable
        $this->userId = $userId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->reg_user_type = $reg_user_type;
        $this->teacher_grade_level_taught = $teacher_grade_level_taught;
        $this->teacher_subject_area_taught = $teacher_subject_area_taught;
        $this->admin_district_language_taught = $admin_district_language_taught;
        $this->admin_district_other_language_taught = $admin_district_other_language_taught;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Set the db handler
        $this->updateUser($this->userId,$this->firstName,$this->lastName,$this->reg_user_type,$this->teacher_grade_level_taught,$this->teacher_subject_area_taught, $this->admin_district_language_taught,$this->admin_district_other_language_taught);
      
    }

    private function updateUser($userId,$firstName,$lastName,$reg_user_type,$teacher_grade_level_taught,$teacher_subject_area_taught,$admin_district_language_taught,$admin_district_other_language_taught)
    {
      $updateQuery = [
                        'first_name'                =>  $firstName,
                        'last_name'                 =>  $lastName,
                        'reg_user_type'             =>  $reg_user_type,
                        'reg_teacher_grades'        =>  $teacher_grade_level_taught,
                        'reg_teacher_subjects'      =>  $teacher_subject_area_taught,
                        'reg_admin_languages'       =>  $admin_district_language_taught,
                        'reg_admin_other_languages' =>  $admin_district_other_language_taught
                    ];
      $query=  DB::table('users')
            ->where('user_id',$userId)
            ->where('is_deleted',0)
            ->where('is_active',1)
            ->update($updateQuery);
            return   $query;
    }
}
