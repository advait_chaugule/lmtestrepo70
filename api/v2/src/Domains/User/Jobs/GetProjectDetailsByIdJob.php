<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class GetProjectDetailsByIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $user_id = $this->input['user_id'];
        $returnCollection   =   true;
        $fieldsToReturn     =   ['user_id'];
        $keyValuePairs      =   ['user_id' => $user_id];
        $relations          =   ['projects'];

        $ProjectDetail   =   $userRepo->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs, $relations)->toArray();
        return $ProjectDetail;
      
       
    }
}
