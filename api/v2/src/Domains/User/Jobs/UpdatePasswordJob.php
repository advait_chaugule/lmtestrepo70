<?php
namespace App\Domains\User\Jobs;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use Lucid\Foundation\Job;

class UpdatePasswordJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $columnValuePairToUpdate = [
            "password" => bcrypt($this->input['password']),
            "forgot_pasword_request_token_expiry" => "0000-00-00 00:00:00",
            "forgot_pasword_request_token" => null,
        ];
        $resetToken = $this->input['reset_request_token'];
        $conditionalClausePair = ['forgot_pasword_request_token' => $resetToken];
        return $userRepo->editWithCustomizedFields($conditionalClausePair, $columnValuePairToUpdate);
    }
}
