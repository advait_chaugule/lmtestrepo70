<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

class ValidateMandatoryFieldsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $validation_result = [];
        $mandatory_fields_to_check = array('username','email','role_id','organization_id');
        
        foreach($data as $key=>$value)
        {
            if(in_array($key,$mandatory_fields_to_check) && empty($value))
            {
                $validation_result[] = $key;
            }
        }

        return $validation_result;
    }
}
