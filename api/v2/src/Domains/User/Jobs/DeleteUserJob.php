<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class DeleteUserJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $user_id        = $this->input['user_id'];
        $organizationId = $this->input['organization_id'];
        // remove id from the input array since it has been assigned to $id variable
        $input['is_deleted']= 1;
        $input['is_active']= 0;

        return $userRepo->updateOrganizationUserDeleteStatus($user_id,$organizationId,$input);
    }
}
