<?php
namespace App\Domains\User\Jobs;
use Lucid\Foundation\Job;
use DB;
use App\Services\Api\Traits\AddDefaultSettings;
class SetTaxonomyConfigSettingsJob extends Job
{
    private $userId;
    private $organizationId;

    use AddDefaultSettings;

    public function __construct($userId,$organizationId,$settingId,$jsonconfig_value,$feature_id)
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->settingId = $settingId;
        $this->jsonconfig_value = $jsonconfig_value;
        $this->feature_id = $feature_id;
    }

    public function handle()
    {
        $result = array();
        $json_arr = $this->jsonconfig_value;
        //$json_value = json_decode($json_arr);
        $is_user_default = $json_arr['is_user_default'];
        $default_for_all_user = $json_arr['default_for_all_user'];
         
       
        $getUserSetting = DB::table('user_settings')
                            ->where('user_id',$this->userId)
                            ->where('organization_id',$this->organizationId)
                            ->where('is_deleted',0)
                            ->where('id_type',1)
                            ->get()
                            ->toArray();
        if(!empty($getUserSetting) && $is_user_default == 1){
            DB::table('user_settings')
                ->where('user_id',$this->userId)
                ->where('organization_id',$this->organizationId)
                ->update(['json_config_value' => json_encode($json_arr)]); 
        } 
        else if(!empty($getUserSetting) &&  $is_user_default == 0)
        {
            DB::table('user_settings')
            ->where('user_id',$this->userId)
            ->where('organization_id',$this->organizationId)
            ->delete(); 
        
        }
        else{
            $insert_confsetting = ["user_id"=>$this->userId,"organization_id"=>$this->organizationId,"id"=>$this->organizationId,"id_type"=>1,"updated_by"=>"","is_deleted"=>"0","json_config_value"=>json_encode($json_arr)];
            DB::table('user_settings')->insert($insert_confsetting); 
        }
        
        if($default_for_all_user == 1){
            DB::table('organizations')
            ->where('organization_id', $this->organizationId)
            ->update(['json_config_value' => json_encode($json_arr)]);   
        }else 
        {
            $action = "Get";
            $setting_json = $this->updatedefaultconfigsettings($this->organizationId,$is_user_default,$default_for_all_user,$action);

            DB::table('organizations')
            ->where('organization_id', $this->organizationId)
            ->update(['json_config_value' => $setting_json]);
        }

        $result['feature_id'] =  $this->feature_id;
        $result['setting_id'] =  $this->settingId;
        $result['user_id'] =  $this->userId; 

        if($is_user_default == 1 && $default_for_all_user == 1){
            $result['json_config_value'] = $this->jsonconfig_value; 
        }else if($is_user_default == 1 && $default_for_all_user == 0){
            $result['json_config_value'] = $this->jsonconfig_value; 
        }else if($is_user_default == 0 && $default_for_all_user == 1){
            $result['json_config_value'] = $this->jsonconfig_value; 
        }else if($is_user_default == 0 && $default_for_all_user == 0){
            $action = "Get";
            $setting_json = $this->updatedefaultconfigsettings($this->organizationId,$is_user_default,$default_for_all_user,$action);
            $result['json_config_value'] =  json_decode($setting_json);
        }
 
        return $result;

    }
}