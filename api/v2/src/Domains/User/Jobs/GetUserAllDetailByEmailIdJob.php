<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

namespace App\Domains\User\Jobs;
use Illuminate\Http\Request;
use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

class GetUserAllDetailByEmailIdJob extends Job
{
    private $userRepository;
    private $emailId;
    public function __construct(string $emailId)
    {
        //Set the private variable
        $this->emailId   =   $emailId;
    }

    public function handle(UserRepositoryInterface $userRepo)
    {
       
       
        // set user data repository
        $userRepository = $userRepo;

        // get user details
        $userDetails =  $userRepository->findByAttributesWithSpecifiedFields(
            false,['user_id', 'username', 'first_name', 'last_name' , 'organization_id','current_organization_id'], ['email'=>$this->emailId, 'is_active'=>1]
        );

        // prepare the response
        return $userDetails;
    }

}