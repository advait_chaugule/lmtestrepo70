<?php
namespace App\Domains\User\Jobs;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Lucid\Foundation\Job;

class ValidateResetTokenJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $resetRequestToken = $this->input['reset_request_token'];
        $collectionType = false;
        $userDetails = $userRepo->findByAttributesWithSpecifiedFields(
            $collectionType, ['forgot_pasword_request_token_expiry'], ['forgot_pasword_request_token' => $resetRequestToken]
        );
        if ($userDetails == null) {
            return 0;
        } else {
            $forgotPaswordRequestTokenExpiry = strtotime($userDetails['forgot_pasword_request_token_expiry']);
            $currentTime = strtotime(date("Y-m-d H:i:s"));

            if ($forgotPaswordRequestTokenExpiry > $currentTime) {
                return 1;
            } else {
                return 2;
            }
        }
    }
}
