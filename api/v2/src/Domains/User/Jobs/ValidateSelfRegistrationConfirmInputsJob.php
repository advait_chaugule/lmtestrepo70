<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Domains\User\Validators\CheckSelfRegistrationInputValidator as Validator;

class ValidateSelfRegistrationConfirmInputsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
