<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;
use App\Data\Repositories\Contracts\RoleRepositoryInterface;

class UserRoleListJob extends Job
{
    private $userIds;
    private $projectId;
    private $userId;
    private $roleId;
    private $userProjectRoleList;
    private $userRepository;
    private $roleRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $userIds, string $projectId)
    {
        //
        $this->userIds = $userIds;
        $this->projectId = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository)
    {
        //Set user repository handler
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        $userIds = $this->userIds;
        $userProjectRoleList = $this->userRepository->getUserProjectRoleList($userIds, $this->projectId);
        $this->setUserProjectRoleList($userProjectRoleList);
        $projectRoleEntity = $this->parseUserProjectRoleData();

        return $projectRoleEntity;
    }

    public function setUserProjectRoleList($userProjectRoleList) {
        $this->userProjectRoleList = $userProjectRoleList;
    }

    public function getUserProjectRoleList() {
        return $this->userProjectRoleList;
    }

    public function setUserIdentifier($userId) {
        $this->userId = $userId;
    }

    public function getUserIdentifier(){
        return $this->userId;
    }

    public function setRoleIdentifier($roleId) {
        $this->roleId = $roleId;
    }

    public function getRoleIdentifier(){
        return $this->roleId;
    }

    private function getUserDetail() {
        $userIdentifier = $this->getUserIdentifier();

        $userDetail = $this->userRepository->find($userIdentifier)->toArray();

        $userEntity = [
            'user_name' => (!empty($userDetail['first_name']) ? $userDetail['first_name'] : " ").' '.(!empty($userDetail['last_name']) ? $userDetail['last_name'] : " "),
            'user_email' => !empty($userDetail['email']) ? $userDetail['email'] : " ",

        ];
        return $userEntity;
    }

    private function getRoleName() {
        $roleIdentifier = $this->getRoleIdentifier();

        $roleDetail = $this->roleRepository->find($roleIdentifier)->toArray();
        return !empty($roleDetail['name']) ? $roleDetail['name'] : " ";
    }

    private function parseUserProjectRoleData() {
        $projectRoleEntity = [];
        $userProjectRoleList = $this->getUserProjectRoleList();
        foreach($userProjectRoleList as $projectRoleList){

            $this->setUserIdentifier($projectRoleList->user_id);
            $this->setRoleIdentifier(explode('||', $projectRoleList->workflow_stage_role_id)[2]);

            $userEntity = $this->getUserDetail();

            $projectRoleEntity[] = [
                'user_id' => $projectRoleList->user_id,
                'user_name' => $userEntity['user_name'],
                'user_email' => $userEntity['user_email'],
                'workflow_stage_role_id' => $projectRoleList->workflow_stage_role_id,
                'role_name' => $this->getRoleName()
            ];
        }
        $combineUserRole=[];
        foreach ($projectRoleEntity as $projectRoleEntityK=>$projectRoleEntityV)
        {
            $combineUserRole[] =  $projectRoleEntityV['user_id'].$projectRoleEntityV['role_name'];
        }
        $uniqueRole      = array_unique($combineUserRole);
        $projectRoleData = array_intersect_key($projectRoleEntity,$uniqueRole);
        $projectData=[];
        foreach ($projectRoleData as $key=>$value) {
            $projectData[] =$value;
        }
        return $projectData;
    }
}
