<?php
namespace App\Domains\User\Jobs;
use App\Services\Api\Traits\UserSettingTrait;
use Lucid\Foundation\Job;
class SaveUserSettings extends Job
{
    use UserSettingTrait;
    private $userId;
    private $organizationId;
    private $documentId;
    private $jsonData;
    private $userSetting;
    private $adminSetting;
    private $tableName;

    public function __construct($userId,$organizationId,$documentId,$jsonData,$tableName='')
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->documentId     = $documentId;
        $this->jsonData       = $jsonData;
        $this->tableName      = $tableName;
    }

    public function handle()
    {
        $this->mappedUserSettingData($this->userId,$this->documentId,$this->organizationId,$this->jsonData,$this->tableName);
    }
}