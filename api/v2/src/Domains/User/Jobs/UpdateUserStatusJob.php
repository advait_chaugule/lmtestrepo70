<?php
namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use DB;

class UpdateUserStatusJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        // is_active 0 changed to 1 and vice versa to make ACMT Application compatible with CIS

        if($data['is_active'] == 0 )
        {
            $data['is_active'] = 1;
        }
        else
        {
            $data['is_active'] = 0;
        }
        
        DB::table('users')
        ->where('username', $data['username'])
        ->orWhere('email', $data['email'])
        ->orWhere('user_id', $data['user_id'])
        ->where('organization_id', $data['organization_id'])
        ->update([ 'is_active' => $data['is_active'] ]);

    }
}
