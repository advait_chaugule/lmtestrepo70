<?php
namespace App\Domains\User\Jobs;

use Illuminate\Http\Request;
use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\UserRepositoryInterface;

class ListUserJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepositoryInterface $userRepository, Request $request)
    {
        
        $input = $request->input();
       
        $organization_id = $input['auth_user']['organization_id']; 
       
        // set user data repository
        $this->userRepository = $userRepository;

        // get user details
        $users = $this->userRepository->getUsersList($organization_id);

        // prepare the response
        return ['users' => $users, 'count' => count($users), 'totalUsers' => count($users)];
    }    
}
