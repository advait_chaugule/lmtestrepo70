<?php
namespace App\Domains\User\Tests\Jobs;

use App\Domains\User\Jobs\UpdateUserInfoByOrganizationJob;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Collection;

use App\Services\Api\Traits\UuidHelperTrait;

class UpdateUserInfoByOrganizationJobTest extends TestCase
{
    use RefreshDatabase, UuidHelperTrait;

    public function setUp() {
        parent::setUp();
        Artisan::call('db:seed');

        $this->userRepo = resolve('App\Data\Repositories\Contracts\UserRepositoryInterface');
        //$this->itemRepository = resolve('App\Data\Repositories\Contracts\ItemRepositoryInterface');
       // $this->nodeTypeRepository = resolve('App\Data\Repositories\Contracts\NodeTypeRepositoryInterface');

        
    }

    public function test_update_user_info_by_organization_job()
    {
        
        $this->markTestIncomplete();
       /* $userIdentifier     =   $this->createUniversalUniqueIdentifier();

        $input['organization_id']       =  'a83db6c0-1a5e-428e-8384-c8d58d2a83ff';
        $input['user_id']               =  $userIdentifier;
        $input['active_access_token']   =  $userIdentifier;
        $input['email']                 =  'testuser@gmail.com';
        $input['username']              =  'testuser@gmail.com';
        $input['role_id']               =  'a987a611-15d7-425f-81a1-8b0f72a0f6fa';


        $arrData = ['organization_id'=> 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
                    'user_id'        => $userIdentifier,
                    'role_id'        => 'a987a611-15d7-425f-81a1-8b0f72a0f6fa',
                    'is_active'      => 0
            ];

        $userInfo['enable_email']   =   '1';

        DB::table('users_organization')->insert($arrData);
        $this->userRepo->fillAndSave($input);

        $inputData  =   ['user_id' => $userIdentifier, 'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff','user_info' => $userInfo];

        $job         = $this->app->make('App\Domains\User\Jobs\UpdateUserInfoByOrganizationJob', ['inputData' => $inputData]);

        $jobResponse = $job->handle($this->userRepo);

        
        $this->assertTrue($jobResponse->isNotEmpty());
        */
    }
}
