<?php
namespace App\Domains\User\Tests\Jobs;

use App\Domains\User\Jobs\ProjectCountforUsersJob;
use Tests\TestCase;

class ProjectCountforUsersJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new ProjectCountforUsersJob($data,'a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ProjectCount()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new ProjectCountforUsersJob($data,'a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setProjectCount($value);
        $this->assertEquals($value,$assets->getProjectCount());
    }

    public function test_ParsedProjectCount()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new ProjectCountforUsersJob($data,'a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setParsedProjectCount($value);
        $this->assertEquals($value,$assets->getParsedProjectCount());
    }
}
