<?php
namespace App\Domains\User\Tests\Jobs;

use App\Domains\User\Jobs\UpdateAccessTokenJob;
use Tests\TestCase;

class UpdateAccessTokenJobTest extends TestCase
{
    public function test_UserId()
    {
        $assets = new UpdateAccessTokenJob('a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserId($value);
        $this->assertEquals($value,$assets->getUserId());
    }

    public function test_AccessToken()
    {
        $assets = new UpdateAccessTokenJob('a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setAccessToken($value);
        $this->assertEquals($value,$assets->getAccessToken());
    }
}
