<?php
namespace App\Domains\User\Tests\Jobs;

use App\Domains\User\Jobs\UserRoleListJob;
use Tests\TestCase;

class UserRoleListJobTest extends TestCase
{
    public function test_UserProjectRoleList()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UserRoleListJob($data,'a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserProjectRoleList($value);
        $this->assertEquals($value,$assets->getUserProjectRoleList());
    }

    public function test_UserIdentifier()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UserRoleListJob($data,'a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setUserIdentifier($value);
        $this->assertEquals($value,$assets->getUserIdentifier());
    }

    public function test_RoleIdentifier()
    {
        $data[] = '8465fd5a-78e0-4301-a8d1-4de079fa0fc5';
        $assets = new UserRoleListJob($data,'a42c92f9-52b9-4bd9-9241-ae1f54e10472');
        $value = '5c46395c-8bce-48f7-b626-871dd7079fce';
        $assets->setRoleIdentifier($value);
        $this->assertEquals($value,$assets->getRoleIdentifier());
    }
}
