<?php

namespace App\Domains\User\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInviteEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $email;
    public $orgName;
    public $domainName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];        
        $this->domainName = $data['domain_name'];
        $domainName = str_replace('api.','',$data['domain_name']);
        if(strpos($domainName,'localhost')!==false)
        {
            $this->body = env('BASE_URL') . '#/org/'.$data['org_code'].'/acceptInvite/' . $data['active_access_token'].'/'.$data['organization_id'];
        }else{
            $this->body = $domainName . '#/org/'.$data['org_code'].'/acceptInvite/' . $data['active_access_token'].'/'.$data['organization_id'];
        }
        $this->email = $data['email'];
        $this->orgName = $data['org_name'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        // return $this->view('view.name');
        return $this->view('email.emailinvitation')
                    ->with([
                        "body" => $this->body,
                        "email" => $this->email,
                        "orgName" => $this->orgName,
                        "domainName"=>$this->domainName
                    ])
                    ->subject($this->subject);
    }
}
