<?php

namespace App\Domains\User\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $domainName;
    public $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];
        $this->domainName = $data['domain_name'];
        $domainName = str_replace('api.','',$data['domain_name']);
        if(strpos($domainName,'localhost')!==false)
        {
            $this->body = env('BASE_URL') . '#/org/'.$data['org_code'].'/register/' . $data['active_access_token'];
        }else{
            $this->body = $domainName . '#/org/'.$data['org_code'].'/register/' . $data['active_access_token'];
        }

        $this->email = $data['email'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        // return $this->view('view.name');
        return $this->view('email.emailverification')
                    ->with([
                        "body" => $this->body,
                        "email" => $this->email,
                        "domainName"=>$this->domainName
                    ])
                    ->subject($this->subject);
    }
}
