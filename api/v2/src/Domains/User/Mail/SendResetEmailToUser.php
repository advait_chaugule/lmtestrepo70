<?php

namespace App\Domains\User\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResetEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $email;
    public $username;
    public $domainName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];        
        $this->domainName = $data['domain_name'];
        $domainName = str_replace('api.','',$data['domain_name']);
        if(strpos($domainName,'localhost')!==false) {
            $this->body = env('BASE_URL') . '#/org/'.$data['org_code'].'/reset/' . $data['forgot_reset_request_token'];
        }else{
            $this->body = $domainName . '#/org/'.$data['org_code'].'/reset/' . $data['forgot_reset_request_token'];
        }

        $this->email = $data['email'];        
        $this->username = $data['user_name'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.passwordreset')
                    ->with([
                        "body" => $this->body,
                        "email" => $this->email,
                        "user_name" => $this->username,
                        "domainName"=>$this->domainName
                    ])
                    ->subject($this->subject);
    }
}
