<?php

namespace App\Domains\User\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSelfRegisterEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $email;
    public $domainName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->subject = $data['subject'];//strpos($domainName,'localhost')!==false
        $this->domainName = $data['domain_name'];
        $domainName = str_replace('api.','',$data['domain_name']);
        if(strpos($domainName,'localhost')!==false) {
            $this->body = env('BASE_URL') . '#/org/'.$data['org_code'].'/confirm/' . $data['active_access_token'];
        }else{
            $this->body = $domainName . '#/org/'.$data['org_code'].'/confirm/' . $data['active_access_token'];
        }
        $this->email = $data['email'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        // return $this->view('view.name');
        return $this->view('email.emailconfirm')
                    ->with([
                        "body" => $this->body,
                        "domainName" => $this->domainName,
                        "email" => $this->email
                    ])
                    ->subject($this->subject);
    }
}
