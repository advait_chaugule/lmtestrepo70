<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class RegistrationInputValidator extends BaseValidator {

    protected $rules = [
        'user_id'=>'required|exists:users',
        'password' => 'required',
        'first_name' => 'required',
        'organization_id' => 'required'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}