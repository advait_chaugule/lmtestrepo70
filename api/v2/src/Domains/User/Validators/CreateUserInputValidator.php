<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateUserInputValidator extends BaseValidator {

    protected $rules = [
        'email' => 'required|email|max:255|unique:users',
    ];
    protected $messages = [
        'required' => ':attribute is required.',
        'email' => ':attribute is invalid.'
    ];   
     
}