<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CheckSelfRegistrationInputValidator extends BaseValidator {

    protected $rules = [
        'email'=>'required|exists:users'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}