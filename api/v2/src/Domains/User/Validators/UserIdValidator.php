<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class UserIDValidator extends BaseValidator {

    protected $rules = [
        'user_id'=>'required|exists:users'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}