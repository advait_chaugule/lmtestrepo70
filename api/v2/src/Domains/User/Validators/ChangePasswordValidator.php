<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ChangePasswordValidator extends BaseValidator {

    protected $rules = [
        'currentPassword' => 'required| min:8',
        'newPassword' => 'required|min:8',
        'confirmNewPassword' => 'required|min:8'
    ];

     protected $messages = [
        'required' => ':attribute is required'
     ];
    
}