<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CheckUserInputValidator extends BaseValidator {

    protected $rules = [
        'first_name'=>'required',
        'last_name'=>'required',
        'email'=>'required',
        'password'=>'required',
        'organization_id'=>'required'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}