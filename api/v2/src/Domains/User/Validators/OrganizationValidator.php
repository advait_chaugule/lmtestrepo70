<?php

namespace App\Domains\User\Validators;
use Illuminate\Validation\ValidationException;
use App\Foundation\BaseValidator;

class OrganizationValidator extends BaseValidator
{
    protected $rules = [
        'organization_id' => 'required',
    ];

    protected $messages = [
        'required' => ':attribute is required.'
    ];
}