<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CheckBulkUserCSVValidator extends BaseValidator {

    protected $rules = [
        'email_id' => 'required|email|unique:users,email',
        'role'  =>  'required|exists:roles,name'
    ];

     protected $messages = [
        'email_id.required' => '1',
        'role.required'     => '2',
        'email_id.unique'   => '3',
        'email_id.email'    => '4',
        'role.exists'       => '5'
     ];
    
}