<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class LoginInputValidator extends BaseValidator {

    protected $rules = [
        'username' => 'required',
        'password' => 'required',
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}