<?php
namespace App\Domains\User\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class EmailIdValidator extends BaseValidator {

    protected $rules = [
        'email' => "required|email|exists:users,email,is_deleted,0,is_active,1",
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];   
    
}