<?php
namespace App\Domains\CaseAssociationGroup\Jobs;

use App\Domains\CaseAssociationGroup\Validators\CFAssociationGroupValidator as Validator;

use Lucid\Foundation\Job;

class ValidateCFAssociationGroupJob extends Job
{

    private $dataToValidate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataToValidate)
    {
        $this->dataToValidate = $dataToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->dataToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
