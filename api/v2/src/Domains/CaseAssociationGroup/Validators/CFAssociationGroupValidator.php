<?php
namespace App\Domains\CaseAssociationGroup\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CFAssociationGroupValidator extends BaseValidator {

    protected $rules = [
        'association_group_id' => 'required|exists:association_groups,association_group_id'
    ];

     protected $messages = [
        'required' => ':attribute is mandatory.',
     ];
    
}