<?php
namespace App\Domains\Note\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Data\Repositories\Contracts\NoteRepositoryInterface;

class UpdateNoteJob extends Job
{
    use DateHelpersTrait;

    private $noteIdentifier;
    private $requestData;
    private $noteRepository;

 /*   private $fieldTypeValues;*/
    private $noteEntity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, array $input)
    {
        $this->setNoteIdentifier($identifier);
        $this->setInputData($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {
        $this->noteRepository = $noteRepository;

        $noteIdentifier           = $this->getNoteIdentifier();
        $getAllFileDate =$this->getInputData();
        if(isset($getAllFileDate['title']))
            $attribute['title'] = $getAllFileDate['title'];
        if(isset($getAllFileDate['description']))
            $attribute['description'] = $getAllFileDate['description'];
        if(isset($getAllFileDate['note_status']))
            $attribute['note_status'] = $getAllFileDate['note_status'];
        if(isset($getAllFileDate['note_order']))
            $attribute['note_order'] = $getAllFileDate['note_order'];
        $attribute['updated_by']  = $this->getInputData()['auth_user']['user_id'];
        //dump($getAllFileDate);
        $updatedNote = $this->noteRepository->edit($noteIdentifier, $attribute);

        $this->parseResponse();

      /*  $noteEntity = $this->getNoteEntity();
        return $noteEntity;*/
        return $this->getNoteEntity();
    }

    public function setNoteIdentifier($identifier){
        $this->noteIdentifier = $identifier;
    }

    public function getNoteIdentifier(){
        return $this->noteIdentifier;
    }

    public function setInputData($input){
        $this->requestData = $input;
    }

    public function getInputData(){
        return $this->requestData;
    }

    public function setNoteEntity($noteEntity){
        $this->noteEntity = $noteEntity;
    }

    public function getNoteEntity(){
        return $this->noteEntity;
    }

    private function parseResponse(){
        $noteIdentifier = $this->getNoteIdentifier();
        $noteDetail = $this->noteRepository->find($noteIdentifier);

        $noteEntity = [
            "note_id"     =>  $noteIdentifier,
            "title"       =>  !empty($noteDetail->title) ? $noteDetail->title : "",
            "description" =>  !empty($noteDetail->description) ? $noteDetail->description : "",
            "note_order"  =>  !empty($noteDetail->note_order) ? $noteDetail->note_order: "",
            "note_status" =>  $noteDetail->note_status,
            "updated_at"  =>  $this->formatDateTimeToISO8601($noteDetail->updated_at)
        ];
        $this->setNoteEntity($noteEntity);
    }
}
