<?php
namespace App\Domains\Note\Jobs;

use App\Data\Repositories\Contracts\NoteRepositoryInterface;
use Lucid\Foundation\Job;

class ReorderNoteJob extends Job
{
    private $noteRepository;
    private $organizationIdentifier;
    private $noteIdentifier;
    private $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( string $organizationIdentifier, array $requestData)
    {
        //$this->setNoteIdentifier($identifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->requestData  =   $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {

        //set the note repository handler
        $this->noteRepository       =   $noteRepository;
        $editNoteOrder  =   $this->editNoteOrder();
        return $editNoteOrder;

        //return $this->parseResponse();
    }

    public function setNoteIdentifier($identifier) {
        $this->noteIdentifier   =   $identifier;
    }

    public function getNoteIdentifier() {
        return $this->noteIdentifier;
    }

    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    private function editNoteOrder()   {
        $requestData        =   $this->requestData;
        foreach($requestData['note'] as $noteReorder) {


            $columnValueFilterPairs =   ['note_id'     =>  $noteReorder['note_id']];
            $attributes             =   ['note_order'  => $noteReorder['note_order']];

            $noteDetails    =   $this->noteRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        }

        return $requestData['note'];
    }

    private function parseResponse() {
        $noteIdentifier =   $this->getNoteIdentifier();
        $organizationId     =   $this->getOrganizationIdentifier();

        $noteDetails    =   $this->noteRepository->geNoteDetails($organizationId, $noteIdentifier);

        // prepare the response
        return ['note_order' => $noteDetails];

    }
}
