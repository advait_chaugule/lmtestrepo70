<?php
namespace App\Domains\Note\Jobs;
use App\Domains\Note\Validators\NoteExistsValidator;
use Lucid\Foundation\Job;

class ValidateNoteIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
       // dd($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
