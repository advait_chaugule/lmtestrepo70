<?php
namespace App\Domains\Note\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Data\Repositories\Contracts\NoteRepositoryInterface;

class GetNoteDetailsJob extends Job
{
    use DateHelpersTrait;

    private $noteIdentifier;
    private $organizationIdentifier;
    private $noteRepository;
    private $noteDetail;
    private $noteEntity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $identifier, string $organizationIdentifier)
    {
        $this->setNoteIdentifier($identifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {
        //Set the note repo handler
        $this->noteRepository = $noteRepository;
        $noteIdentifier = $this->getNoteIdentifier();

        $noteDetail = $this->getNote();
        $this->setNoteDetail($noteDetail);

        $this->parseNoteResponse();

        $noteEntity = $this->getNoteEntity();
        return $noteEntity;
    }

    public function setNoteIdentifier($noteIdentifier){
        $this->noteIdentifier = $noteIdentifier;
    }

    public function getNoteIdentifier() {
        return $this->noteIdentifier;
    }

    public function setOrganizationIdentifier($organizationIdentifier){
        $this->organizationIdentifier = $organizationIdentifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setNoteDetail($noteDetail) {
        $this->noteDetail = $noteDetail;
    }

    public function getNoteDetail() {
        return $this->noteDetail;
    }

    public function setNoteEntity($noteEntity) {
        $this->noteEntity = $noteEntity;
    }

    public function getNoteEntity() {
        return $this->noteEntity;
    }

    private function getNote() {
        $identifier              =   $this->getNoteIdentifier();
        $organizationIdentifier  =   $this->getOrganizationIdentifier();

        $noteDetail = $this->noteRepository->getNote($identifier, $organizationIdentifier);
        return $noteDetail;
    }

    private function parseNoteResponse() {
        $noteDetail = $this->getNoteDetail();
        $this->setNoteEntity($noteDetail);
    }
}
