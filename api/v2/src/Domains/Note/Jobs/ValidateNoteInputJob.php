<?php
namespace App\Domains\Note\Jobs;

use App\Domains\Note\Validators\CreateUpdateNoteValidator;
use Lucid\Foundation\Job;

class ValidateNoteInputJob extends Job
{
    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateUpdateNoteValidator $validator)
    {
        $validation = $validator->validate($this->data);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
