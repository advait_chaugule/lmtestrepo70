<?php
namespace App\Domains\Note\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\NoteRepositoryInterface;

class DeleteNoteJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($noteIdentifier,$organizationIdentifier )
    {
        $this->note_Id          =   $noteIdentifier;
        $this->organizationId   =   $organizationIdentifier;
        $this->deleteStatusData =   ["is_deleted" => 1];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepo)
    {
       //
        return $noteRepo->edit($this->note_Id, $this->deleteStatusData);
    }
}
