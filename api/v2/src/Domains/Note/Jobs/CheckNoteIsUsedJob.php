<?php
namespace App\Domains\Note\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\NoteRepositoryInterface;

class CheckNoteIsUsedJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $noteIdIdentifier)
    {
        $this->identifier = $noteIdIdentifier;
        $this->setNoteIdentifier($this->identifier);
    }

    public function setNoteIdentifier(array $identifier){
        $this->identifier = $identifier;
    }

    public function getNoteIdentifier() {
        return $this->identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {
        $noteIdentifier = $this->identifier;
        $this->noteRepository=$noteRepository;
        return $this->noteRepository->getNoteIsUsedData($noteIdentifier);
    }
}
