<?php
namespace App\Domains\Note\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\NoteRepositoryInterface;

class CheckNoteIsDeleteJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->identifier = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {
        $attributes = ['note_id' => $this->identifier, 'is_deleted' => '1'];
        //dd($attributes);
      // dump($noteRepository->findByAttributes($attributes));
        return $noteRepository->findByAttributes($attributes)->isNotEmpty();
    }
}
