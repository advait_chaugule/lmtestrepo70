<?php
namespace App\Domains\Note\Jobs;

use Lucid\Foundation\Job;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Data\Repositories\Contracts\NoteRepositoryInterface;

class CreateNoteJob extends Job
{
    use DateHelpersTrait;
    private $noteDetail;
    private $noteData;
    private $noteRepository;
    private $noteEntity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->noteDetail = $input;
    }

    public function setSavedNoteData($data){
        $this->noteData = $data ;
    }

    public function getSavedNoteData(){
        return $this->noteData;
    }
    public function setNoteEntity($noteEntity){
        $this->noteEntity = $noteEntity;
    }

    public function getNoteEntity(){
        return $this->noteEntity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {
        $this->noteRepository = $noteRepository;
        $savedNoteData = $this->noteRepository->fillAndSave($this->noteDetail);
        $this->setSavedNoteData($savedNoteData);
        $this->parseResponse();
        return $this->getNoteEntity();
    }

    private function parseResponse(){
        $savedNoteData = $this->getSavedNoteData();
        $noteEntity = [
            "note_id"     => $savedNoteData->note_id,
            "title"       => $savedNoteData->title,
            "description" => $savedNoteData->description,
            "note_status" => $savedNoteData->note_status,
            "note_order" => $savedNoteData->note_order,
            "created_at"  => $this->formatDateTimeToISO8601($savedNoteData->created_at)
        ];
        //dd($noteEntity);
        $this->setNoteEntity($noteEntity);
    }
}
