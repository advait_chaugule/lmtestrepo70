<?php
namespace App\Domains\Note\Jobs;

use App\Data\Repositories\Contracts\NoteRepositoryInterface;
use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class GetAllNoteJob extends Job
{
    private $organizationIdentifier;
    private $noteId;
    private $noteRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
        $organizationIdentifier = $this->input['organization_id'];
        $noteId = $this->input['note_id'];
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->setNoteId($noteId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NoteRepositoryInterface $noteRepository)
    {
        $this->noteRepository = $noteRepository;
        $noteDetails = $this->getNoteDetailFromModel();
        $dataNoteDetails=$noteDetails->toArray();
        $getAllNoteDetail = array();
      // print_r($dataNoteDetails); die;
        foreach($dataNoteDetails as $noteDetailV)
        {
                $getAllNoteDetail[] = [
                    "note_id"    =>  $noteDetailV['note_id'],
                    'title'  =>$noteDetailV['title'],
                    "description"    =>  $noteDetailV['description'],
                    'created_at' => $noteDetailV['created_at'],
                    'note_status' => $noteDetailV['note_status'],
                    'note_order' =>$noteDetailV['note_order']
                ];

        }
        return $getAllNoteDetail;
            //dump($getAllNoteDetail);
    }
    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }
    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }
    public function setNoteId($identifier){
        $this->noteId = $identifier;
    }
    public function getNotetId(){
        return $this->noteId;
    }
    public function getNoteDetailFromModel(){
        $organizationIdentifier =  $this->getOrganizationIdentifier();
        $noteId =   $this->getNotetId();

        if($noteId == "")
        {

            $returnCollection = true;
            $keyValuePairs = ['organization_id' => $organizationIdentifier ,'is_deleted'=>0];
        }
        else
        {

            $returnCollection = true;
            $keyValuePairs = ['organization_id' => $organizationIdentifier, 'note_id' => $noteId];
        }
        $fieldsToReturn = ['note_id',
            'title',
            'description',
            'note_status',
            'note_order',
            'created_at',
         ];

        $noteDetails = $this->noteRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $keyValuePairs,null,false,['limit' => 10, 'offset' => 0],true,['orderBy' => 'note_order', 'sorting' => 'asc']);

        return $noteDetails;
    }
}
