<?php
namespace App\Domains\Note\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateUpdateNoteValidator extends BaseValidator {

    protected $rules = [
        'organization_id' => 'required|exists:organizations,organization_id',
       /* 'title' => 'required|string',
        'description' =>'required|string'*/
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'string' => ':attribute should be of type string.',
        'max:50' => ':attribute should be of maximum 50 characters.',
     ];
    
}