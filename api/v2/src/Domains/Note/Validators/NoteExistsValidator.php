<?php
namespace App\Domains\Note\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class NoteExistsValidator extends BaseValidator {

    protected $rules = [
        'note_id' => "required|exists:notes,note_id,is_deleted,0",
    ];

    protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
    ];
}