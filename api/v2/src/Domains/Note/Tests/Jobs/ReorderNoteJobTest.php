<?php
namespace App\Domains\Note\Tests\Jobs;

use App\Domains\Note\Jobs\ReorderNoteJob;
use Tests\TestCase;

class ReorderNoteJobTest extends TestCase
{
    public function test_NoteIdentifier()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new ReorderNoteJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNoteIdentifier($value);
        $this->assertEquals($value,$assets->getNoteIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $data[] = '7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b'; 
        $assets = new ReorderNoteJob('ad323249-1bf7-4321-ab84-d345aa9c06d1',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }
}
