<?php
namespace App\Domains\Note\Tests\Jobs;

use App\Domains\Note\Jobs\CreateNoteJob;
use Tests\TestCase;

class CreateNoteJobTest extends TestCase
{
    public function test_SavedNoteData()
    {
        $assets = new CreateNoteJob('ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setSavedNoteData($value);
        $this->assertEquals($value,$assets->getSavedNoteData());
    }

    public function test_NoteEntity()
    {
        $assets = new CreateNoteJob('ad323249-1bf7-4321-ab84-d345aa9c06d1');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNoteEntity($value);
        $this->assertEquals($value,$assets->getNoteEntity());
    }
}
