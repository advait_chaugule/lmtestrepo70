<?php
namespace App\Domains\Note\Tests\Jobs;

use App\Domains\Note\Jobs\CheckNoteIsUsedJob;
use Tests\TestCase;

class CheckNoteIsUsedJobTest extends TestCase
{
    public function test_NoteIdentifier()
    {
        $data[] = 'ad323249-1bf7-4321-ab84-d345aa9c06d1';
        $assets = new CheckNoteIsUsedJob($data);
        $value[] = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNoteIdentifier($value);
        $this->assertEquals($value,$assets->getNoteIdentifier());
    }
}
