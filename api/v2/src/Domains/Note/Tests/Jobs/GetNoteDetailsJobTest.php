<?php
namespace App\Domains\Note\Tests\Jobs;

use App\Domains\Note\Jobs\GetNoteDetailsJob;
use Tests\TestCase;

class GetNoteDetailsJobTest extends TestCase
{
    public function test_NoteIdentifier()
    {
        $assets = new GetNoteDetailsJob('ad323249-1bf7-4321-ab84-d345aa9c06d1','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNoteIdentifier($value);
        $this->assertEquals($value,$assets->getNoteIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new GetNoteDetailsJob('ad323249-1bf7-4321-ab84-d345aa9c06d1','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_NoteDetail()
    {
        $assets = new GetNoteDetailsJob('ad323249-1bf7-4321-ab84-d345aa9c06d1','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNoteDetail($value);
        $this->assertEquals($value,$assets->getNoteDetail());
    }

    public function test_NoteEntity()
    {
        $assets = new GetNoteDetailsJob('ad323249-1bf7-4321-ab84-d345aa9c06d1','7cdf4bd4-be5b-49bb-a9ca-4e073e39e60b');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setNoteEntity($value);
        $this->assertEquals($value,$assets->getNoteEntity());
    }
}
