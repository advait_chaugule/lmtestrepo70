<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\ListItemIdForDocumentJob;
use Tests\TestCase;

class ListItemIdForDocumentJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new ListItemIdForDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_ItemListWithAssociation()
    {
        $assets = new ListItemIdForDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemListWithAssociation($value);
        $this->assertEquals($value,$assets->getItemListWithAssociation());
    }
}
