<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\ReorderAdminDocumentJob;
use Tests\TestCase;

class ReorderAdminDocumentJobTest extends TestCase
{
    public function test_OrganizationIdentifier()
    {
        $data = ['c4424f55-8900-446c-9c41-2f2c25d58c08'];
        $assets = new ReorderAdminDocumentJob('ac5349f7-3771-4501-b7ea-28292526d7a0',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_FileIdentifier()
    {
        $data = ['c4424f55-8900-446c-9c41-2f2c25d58c08'];
        $assets = new ReorderAdminDocumentJob('ac5349f7-3771-4501-b7ea-28292526d7a0',$data);
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setFileIdentifier($value);
        $this->assertEquals($value,$assets->getFileIdentifier());
    }
}
