<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\CheckProjectAssociatedToDocumentJob;
use Tests\TestCase;

class CheckProjectAssociatedToDocumentJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new CheckProjectAssociatedToDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08','d199f273-204e-4e8f-a6ab-c473e76f1b84');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }

    public function test_OrganizationIdentifier()
    {
        $assets = new CheckProjectAssociatedToDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08','d199f273-204e-4e8f-a6ab-c473e76f1b84');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setOrganizationIdentifier($value);
        $this->assertEquals($value,$assets->getOrganizationIdentifier());
    }

    public function test_ProjectList()
    {
        $assets = new CheckProjectAssociatedToDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08','d199f273-204e-4e8f-a6ab-c473e76f1b84');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setProjectList($value);
        $this->assertEquals($value,$assets->getProjectList());
    }

    public function test_ProjectEntity()
    {
        $assets = new CheckProjectAssociatedToDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08','d199f273-204e-4e8f-a6ab-c473e76f1b84');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setProjectEntity($value);
        $this->assertEquals($value,$assets->getProjectEntity());
    }
}
