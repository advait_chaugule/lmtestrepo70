<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\PublishDocumentJob;
use Tests\TestCase;

class PublishDocumentJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new PublishDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08','989d3047-c369-4b09-b4f1-dee410aa84be','56b543bf-2839-4b97-a83e-e291c89f2374','ac5349f7-3771-4501-b7ea-28292526d7a0');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }

    public function test_setUser()
    {
        $assets = new PublishDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08','989d3047-c369-4b09-b4f1-dee410aa84be','56b543bf-2839-4b97-a83e-e291c89f2374','ac5349f7-3771-4501-b7ea-28292526d7a0');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setUser($value);
        $this->assertEquals($value,$assets->getUser());
    }
}
