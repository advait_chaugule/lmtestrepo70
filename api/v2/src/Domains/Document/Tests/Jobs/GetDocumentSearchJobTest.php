<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\GetDocumentSearchJob;
use Tests\TestCase;

class GetDocumentSearchJobTest extends TestCase
{
    public function test_DocumentsFetched()
    {
        $assets = new GetDocumentSearchJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentsFetched($value);
        $this->assertEquals($value,$assets->getDocumentsFetched());
    }

    public function test_ProjectsAssociatedWithDocuments()
    {
        $assets = new GetDocumentSearchJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setProjectsAssociatedWithDocuments($value);
        $this->assertEquals($value,$assets->getProjectsAssociatedWithDocuments());
    }
}
