<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\ValidateDocumentForATenantJob;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;
use App\Services\Api\Traits\UuidHelperTrait;

class ValidateDocumentForATenantJobTest extends TestCase
{
    use DatabaseMigrations, ErrorMessageHelper, StringHelper, DateHelpersTrait, UuidHelperTrait;

    public function setUp() {
        parent::setUp();
        //Artisan::call('db:seed');
    }

    public function test_validate_document_for_a_tenant_job()
    {
        //$this->markTestIncomplete();
        $organizationId     =   "a83db6c0-1a5e-428e-8384-c8d58d2a83ff";
        $organizationName   =   'Organization 1';
        
        $documentRepository         =   resolve('App\Data\Repositories\Contracts\DocumentRepositoryInterface');
        $itemRepository             =   resolve('App\Data\Repositories\Contracts\ItemRepositoryInterface');
        $itemAssociationRepository  =   resolve('App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface');

        /**
         * Prepare the data for document
         */
        $documentIdentifier     =   $this->createUniversalUniqueIdentifier();

        $documentData = [
            "document_id"       =>  $documentIdentifier,
            "title"             =>  'Document Title',
            "organization_id"   =>  $organizationId,
        ];

        //Save Document
        $documentRepository->saveData($documentData); 

        /**
         * Prepare the data for Item
         */
        $itemIdentifier1    =   $this->createUniversalUniqueIdentifier();
        $itemsToSave[0]= [
            "item_id"               =>  $itemIdentifier1,
            "parent_id"             =>  $documentIdentifier,
            "document_id"           =>  $documentIdentifier,
            "full_statement"        =>  'Full Statement1',
            "human_coding_scheme"   =>  'HS 1.1',
            "organization_id"       =>  $organizationId,
        ];

        $itemIdentifier2    =   $this->createUniversalUniqueIdentifier();
        $itemsToSave[1]= [
            "item_id"               =>  $itemIdentifier2,
            "parent_id"             =>  $documentIdentifier,
            "document_id"           =>  $documentIdentifier,
            "full_statement"        =>  'Full Statement2',
            "human_coding_scheme"   =>  'HS 1.2',
            "organization_id"       =>  $organizationId,
        ];

        //Save Items
        $itemRepository->saveMultiple($itemsToSave);

        /**
         * Prepare the data for Item Association
         */
        $itemAssociationID1 = $this->createUniversalUniqueIdentifier();
        $caseAssociationsToSave[0] = [
            "item_association_id"           =>  $itemAssociationID1,
            "association_type"              =>  1,
            "document_id"                   =>  $documentIdentifier,
            "association_group_id"          =>  '',
            "origin_node_id"                =>  $itemIdentifier1,
            "destination_node_id"           =>  $documentIdentifier,
            "sequence_number"               =>  '1',
            "external_node_title"           =>  "",
            "external_node_url"             =>  "",
            "created_at"                    =>  $this->createDateTime(),
            "updated_at"                    =>  null,
            "source_item_association_id"    =>  $itemAssociationID1,
            "organization_id"               =>  $organizationId,
        ];

        $itemAssociationID2 = $this->createUniversalUniqueIdentifier();
        $caseAssociationsToSave[1] = [
            "item_association_id"           =>  $itemAssociationID2,
            "association_type"              =>  1,
            "document_id"                   =>  $documentIdentifier,
            "association_group_id"          =>  '',
            "origin_node_id"                =>  $itemIdentifier2,
            "destination_node_id"           =>  $documentIdentifier,
            "sequence_number"               =>  '1',
            "external_node_title"           =>  "",
            "external_node_url"             =>  "",
            "created_at"                    =>  $this->createDateTime(),
            "updated_at"                    =>  null,
            "source_item_association_id"    =>  $itemAssociationID2,
            "organization_id"               =>  $organizationId,
        ];

        //Save Item Association
        $itemAssociationRepository->saveMultiple($caseAssociationsToSave);

        $documentRepository         =   resolve('App\Data\Repositories\Contracts\DocumentRepositoryInterface');

        $job         = $this->app->make('App\Domains\Document\Jobs\ValidateDocumentForATenantJob', ['documentIdentifier' =>  $documentIdentifier, 'organizationId'    =>  $organizationId]);

        $jobResponse    =   $job->handle($documentRepository);
        
        $this->assertTrue($jobResponse);
    }
}
