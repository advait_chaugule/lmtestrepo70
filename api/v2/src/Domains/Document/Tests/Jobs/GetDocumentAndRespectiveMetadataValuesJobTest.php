<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\GetDocumentAndRespectiveMetadataValuesJob;
use Tests\TestCase;

class GetDocumentAndRespectiveMetadataValuesJobTest extends TestCase
{
    public function test_DocumentId()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentId($value);
        $this->assertEquals($value,$assets->getDocumentId());
    }

    public function test_Document()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocument($value);
        $this->assertEquals($value,$assets->getDocument());
    }

    public function test_Language()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setLanguage($value);
        $this->assertEquals($value,$assets->getLanguage());
    }

    public function test_License()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setLicense($value);
        $this->assertEquals($value,$assets->getLicense());
    }

    public function test_Subject()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setSubject($value);
        $this->assertEquals($value,$assets->getSubject());
    }

    public function test_CustomMetadata()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCustomMetadata($value);
        $this->assertEquals($value,$assets->getCustomMetadata());
    }

    public function test_DocumentMetadataValues()
    {
        $assets = new GetDocumentAndRespectiveMetadataValuesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentMetadataValues($value);
        $this->assertEquals($value,$assets->getDocumentMetadataValues());
    }
}
