<?php
namespace App\Domains\Document\Tests\Jobs;

use App\Domains\Document\Jobs\DeleteDocumentJob;
use Tests\TestCase;

class DeleteDocumentJobTest extends TestCase
{
    public function test_Identifier()
    {
        $assets = new DeleteDocumentJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setIdentifier($value);
        $this->assertEquals($value,$assets->getIdentifier());
    }
}
