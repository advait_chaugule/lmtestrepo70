<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\DB;

use App\Data\Models\Document;
use App\Data\Models\NodeType;
use App\Data\Models\Project;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class CreateDeltaDocumentUpdatesSearchDataJob extends Job
{

    use StringHelper, DateHelpersTrait;

    private $documentRepository;
    private $languageRepository;
    private $licenseRepository;
    private $nodeTypeRepository;
    private $projectRepository;
    private $metadataRepository;

    private $documentIds;
    private $documents;
    private $languages;
    private $licenses;
    private $nodeTypes;
    private $projects;

    private $metadata;
    private $documentsMetadata;
    private $documentsSystemMetadata;
    private $documentsCustomMetadata;

    private $parsedDocumentDeltaUpdatesSearchData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $documentIds)
    {
        $this->setDocumentIds($documentIds);
    }

    /*******************************Getters and setters related to documents*********************************/

    public function setDocumentIds(array $data) {
        $this->documentIds = $data;
    }

    public function getDocumentIds(): array {
        return $this->documentIds;
    }

    public function setDocuments(Collection $data) {
        $this->documents = $data;
    }

    public function getDocuments(): Collection {
        return $this->documents;
    }

    public function setLanguages(Collection $data) {
        $this->languages = $data;
    }

    public function getLanguages(): Collection {
        return $this->languages;
    }

    public function setLicenses(Collection $data) {
        $this->licenses = $data;
    }

    public function getLicenses(): Collection {
        return $this->licenses;
    }

    public function setNodeTypes(Collection $data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes(): Collection {
        return $this->nodeTypes;
    }

    public function setProjects(Collection $data) {
        $this->projects = $data;
    }

    public function getProjects(): Collection {
        return $this->projects;
    }

    /******************************Setters and getters for metadata related data*******************************/

    public function setMetadata(Collection $data) {
        $this->metadata = $data;
    }

    public function getMetadata(): Collection {
        return $this->metadata;
    }

    public function setDocumentsMetadata(Collection $data) {
        $this->documentsMetadata = $data;
    }

    public function getDocumentsMetadata(): Collection {
        return $this->documentsMetadata;
    }

    public function setDocumentsSystemMetadata(Collection $data) {
        $this->documentsSystemMetadata = $data;
    }

    public function getDocumentsSystemMetadata(): Collection {
        return $this->documentsSystemMetadata;
    }

    public function setDocumentsCustomMetadata(Collection $data) {
        $this->documentsCustomMetadata = $data;
    }

    public function getDocumentsCustomMetadata(): Collection {
        return $this->documentsCustomMetadata;
    }

    public function setParsedDocumentDeltaUpdatesSearchData(Collection $data) {
        $this->parsedDocumentDeltaUpdatesSearchData = $data;
    }

    public function getParsedDocumentDeltaUpdatesSearchData(): Collection {
        return $this->parsedDocumentDeltaUpdatesSearchData;
    }
    

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ProjectRepositoryInterface $projectRepository,
        MetadataRepositoryInterface $metadataRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->projectRepository = $projectRepository;
        $this->metadataRepository = $metadataRepository;

        $this->fetchAndSetDocuments();
        $this->fetchAndSetLanguages();
        $this->fetchAndSetLicenses();
        $this->fetchAndSetNodeTypes();
        $this->fetchAndSetProjects();

        $this->fetchAndSetMetadata();
        $this->fetchAndSetDocumentsMetadata();
        $this->parseAndSetDocumentSystemMetadata();
        $this->parseAndSetDocumentsCustomMetadata();
        $this->prepareAndSetSearchTaxonomyData();

        return $this->getParsedDocumentDeltaUpdatesSearchData();
        
    }

    private function fetchAndSetDocuments() {
        $attributeIn = 'document_id';
        $documentIds = $this->getDocumentIds();
        $whereClauseAttributes = [];
        $documents = $this->documentRepository->findByAttributeContainedIn($attributeIn, $documentIds, $whereClauseAttributes);
        $this->setDocuments($documents);
    }

    private function fetchAndSetLanguages() {
        $documentCollection = $this->getDocuments();
        $languageIds = $documentCollection->unique('language_id')->pluck('language_id')->toArray();
        $attributeIn = 'language_id';
        $whereClauseAttributes = [];
        $languages = $this->languageRepository->findByAttributeContainedIn($attributeIn, $languageIds, $whereClauseAttributes);
        $this->setLanguages($languages);
    }

    private function fetchAndSetLicenses() {
        $documentCollection = $this->getDocuments();
        $licenseIds = $documentCollection->unique('license_id')->pluck('license_id')->toArray();
        $attributeIn = 'license_id';
        $whereClauseAttributes = [];
        $licenses = $this->licenseRepository->findByAttributeContainedIn($attributeIn, $licenseIds, $whereClauseAttributes);
        $this->setLicenses($licenses);
    }

    private function fetchAndSetNodeTypes() {
        $documentCollection = $this->getDocuments();
        $nodeTypeIds = $documentCollection->unique('node_type_id')->pluck('node_type_id')->toArray();
        $attributeIn = 'node_type_id';
        $whereClauseAttributes = [];
        $nodeTypes = $this->nodeTypeRepository->findByAttributeContainedIn($attributeIn, $nodeTypeIds, $whereClauseAttributes);
        $this->setNodeTypes($nodeTypes);
    }

    private function fetchAndSetProjects() {
        $documentCollection = $this->getDocuments();
        $projectIds = $documentCollection->unique('project_id')->pluck('project_id')->toArray();
        $attributeIn = 'project_id';
        $whereClauseAttributes = [];
        $projects = $this->projectRepository->findByAttributeContainedIn($attributeIn, $projectIds, $whereClauseAttributes);
        $this->setProjects($projects);
    }

    private function fetchAndSetMetadata() {
        $documentCollection = $this->getDocuments();
        $organizationIds = $documentCollection->unique('organization_id')->pluck('organization_id')->toArray();
        $returnCollection = true;
        $fieldsToReturn = [ 'metadata_id', 'name', 'internal_name', 'is_custom', 'is_document', 'organization_id' ];
        $conditionalClause = [ 'is_deleted' => 0, 'is_active' => 1 ];

        $attributeIn = 'organization_id';
        $returnCollection = true;
        $metadataCollection = $this->metadataRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $organizationIds,
            $returnCollection,
            $fieldsToReturn,
            $conditionalClause
        );
        $this->setMetadata($metadataCollection);
    }

    private function fetchAndSetDocumentsMetadata() {
        $documentIds = $this->getDocumentIds();
        $documentsMetaData = DB::table('document_metadata')->whereIn('document_id', $documentIds)->get();
        $this->setDocumentsMetadata($documentsMetaData);
    }

    private function parseAndSetDocumentSystemMetadata() {
        $metadataCollection = $this->getMetadata();
        $documentsSystemMetadataCollection = $metadataCollection->whereIn('is_document', [1,2])->where('is_custom', 0);
        $this->setDocumentsSystemMetadata($documentsSystemMetadataCollection);
    }

    private function parseAndSetDocumentsCustomMetadata() {
        $metadataCollection = $this->getMetadata();
        $documentsCustomMetadadataCollection = $metadataCollection->whereIn('is_document', [1,2])->where('is_custom', 1);
        $this->setDocumentsCustomMetadata($documentsCustomMetadadataCollection);
    }

    private function prepareAndSetSearchTaxonomyData() {
        $documentCollection = $this->getDocuments();
        $parsedDocumentDeltaUpdatesSearchData = [];
        foreach($documentCollection as $document) {
            $parsedDocumentNodeSearchData = $this->createAndReturnDocumentNodeSearchData($document);
            $parsedDocumentDeltaUpdatesSearchData[] = $parsedDocumentNodeSearchData;
        }
        $searchDataCollection = collect($parsedDocumentDeltaUpdatesSearchData);
        $this->setParsedDocumentDeltaUpdatesSearchData($searchDataCollection);
    }

    private function createAndReturnDocumentNodeSearchData(Document $document): array {
        $identifier = $document->document_id;
        $parentIdentifier = "";
        $organizationIdentifier = $document->organization_id;
        $documentId = $identifier;
        $taxonomyName = $document->title ?: "";
        $documentProjectId = $document->project_id ?: "";
        $projectDetails = $this->helperToSearchAndReturnProjectDetails($documentProjectId);
        $projectName = $projectDetails['project_name'];
        $projectWorkflowId = $projectDetails['workflow_id'];
        $title = $taxonomyName;
        $officialSourceUrl = $document->official_source_url ?: "";
        $notes = $document->notes ?: "";
        $publisher = $document->publisher ?: "";
        $description = $document->description ?: "";
        $nodeTypeId = !empty($document->node_type_id) ? $document->node_type_id : "";
        $nodeType = $nodeTypeId!=="" ? $this->helperToSearchAndReturnNodeTypeTitle($nodeTypeId) : "";
        $type = 'document';
        $deleteOrAddStatusTextForCloudSearch = $document->is_deleted===1 ? 'delete' : 'add';
        $adoptionStatus = $document->adoption_status;
        $updatedBy = $document->updated_by ?: "";
        $updateOn = (!empty($document->updated_at) && $document->updated_at!=="0000-00-00 00:00:00") ? 
                    $this->formatDateTimeToCloudSarchCompatibleFormat($document->updated_at->toDateTimeString()) : 
                    "1970-01-01T00:00:00Z";
        $documentSystemMetadataKeyValuePairsCsv = $this->helperToReturnDocumentSystemMetadataKeyValuePairsCsv($document);
        $customMetadataKeyValuePairsCsv = $this->helperToReturnDocumentCustomMetadataKeyValuePairsCsv($document);
        $documentNodeSearchData = [
            'id' => $identifier,
            'type' => $deleteOrAddStatusTextForCloudSearch,
            'fields' => [
                'identifier' => $identifier,
                'parent_identifier' => $parentIdentifier,
                'organization_identifier' => $organizationIdentifier,
                'document_id' => $documentId,
                'taxonomy_name' => $taxonomyName,
                'project_name' => $projectName,
                'title' => $title,
                'official_source_url' => $officialSourceUrl,
                'notes' => $notes,
                'publisher' => $publisher,
                'description' => $description,
                'human_coding_scheme' => '',
                'list_enumeration' => '',
                'full_statement' => '',
                'alternative_label' => '',
                'abbreviated_statement' => '',
                'node_type' => $nodeType,
                'type' => $type,
                'node_type_id' => $nodeTypeId,
                'project_id' => $documentProjectId,
                'project_workflow_id' => $projectWorkflowId,
                'adoption_status' => $adoptionStatus,
                'item_association_type' => [],
                'updated_by' => $updatedBy,
                'update_on' => $updateOn,
                'item_metadata' => '',
                'document_metadata' => $documentSystemMetadataKeyValuePairsCsv,
                'item_custom_metadata' => $customMetadataKeyValuePairsCsv // custom metadata key:value csv
            ]
        ];
        return $documentNodeSearchData;
    }

    private function helperToSearchAndReturnProjectDetails(string $projectIdToSearchFor): array {
        $dataToReturn = [
            "project_name" => '',
            "workflow_id" => ''
        ];
        $projectCollection = $this->getProjects();
        $searchResult = $projectCollection->where("project_id", $projectIdToSearchFor);
        if($searchResult->isNotEmpty()) {
            $project = $searchResult->first();
            $projectName = $project->project_name ?: "";
            $projectWorkflowId = $project->workflow_id ?: "";
            $dataToReturn['project_name'] = $projectName;
            $dataToReturn['workflow_id'] = $projectWorkflowId;
        }
        return $dataToReturn;
    }

    private function helperToSearchAndReturnNodeTypeTitle(string $nodeTypeIdToSearchFor): string {
        $nodeTypesCollection = $this->getNodeTypes();
        $searchResult = $nodeTypesCollection->where("node_type_id", $nodeTypeIdToSearchFor);
        return $searchResult->isNotEmpty() ? ($searchResult->first()->title ?: "") : "";
    }

    private function helperToReturnDocumentSystemMetadataKeyValuePairsCsv(Document $document): string {
        $documentSystemMetadata = $this->getDocumentsSystemMetadata();
        $documentArray = $document->toArray();
        $documentSystemMetadataKeyValuePairs = [];
        foreach($documentArray as $indexKey => $value) {
            if(!empty($value) ) {
                $searchForMetadata = $documentSystemMetadata->where('internal_name', $indexKey);
                if($searchForMetadata->isNotEmpty()) {
                    $metadataId = $searchForMetadata->first()->metadata_id;
                    $keyValueString = "$metadataId:$value";
                    array_push($documentSystemMetadataKeyValuePairs, $keyValueString);
                }
            }
        }
        // create key value for 'license', 'language', 'subjects' system attributes 
        $documentLicense = [];
        $documentLanguage = [];
        $documentSubjects = [];
        if(!is_null($document->license_id)){
            $documentLicense = $this->helperToReturnDocumentLicense($document->license_id);
        }

        if(!is_null($document->language_id)){
            $documentLanguage = $this->helperToReturnDocumentLanguage($document->language_id);
        }
        if(!is_null($document->subjects)){
            $documentSubjects = $document->subjects;
        }
        if(!empty($documentLicense)){
            if(!empty($documentLicense->license_text)){
                $licenseTextSearchMetadata = $documentSystemMetadata->where('internal_name', 'license_text');
                if($licenseTextSearchMetadata->isNotEmpty()) {
                    $licenseTextMetadataId = $licenseTextSearchMetadata->first()->metadata_id;
                    array_push($documentSystemMetadataKeyValuePairs, "$licenseTextMetadataId:$documentLicense->license_text");
                }
            }
            if(!empty($documentLicense->description)){
                $licenseDescriptionSearchMetadata = $documentSystemMetadata->where('internal_name', 'license_description');
                if($licenseDescriptionSearchMetadata->isNotEmpty()) {
                    $licenseDescriptionMetadataId = $licenseDescriptionSearchMetadata->first()->metadata_id;
                    array_push($documentSystemMetadataKeyValuePairs, "$licenseDescriptionMetadataId:$documentLicense->description");
                }
            }
            if(!empty($documentLicense->title)){
                $licenseTitleSearchMetadata = $documentSystemMetadata->where('internal_name', 'license_title');
                if($licenseTitleSearchMetadata->isNotEmpty()) {
                    $licenseTitleMetadataId = $licenseTitleSearchMetadata->first()->metadata_id;
                    array_push($documentSystemMetadataKeyValuePairs, "$licenseTitleMetadataId:$documentLicense->title");
                }
            }
        }

        if(!empty($documentLanguage->short_code)) {
            $languageSearchMetadata = $documentSystemMetadata->where('internal_name', 'language');
            if($languageSearchMetadata->isNotEmpty()) {
                $languageMetadataId = $languageSearchMetadata->first()->metadata_id;
                array_push($documentSystemMetadataKeyValuePairs, "$languageMetadataId:$documentLanguage->short_code");
            }
        }

        if(!empty($documentSubjects)) {
            $documentSubject = $documentSubjects->first();

            if(!empty($documentSubject->title)){
                $subjectTitleSearchMetadata = $documentSystemMetadata->where('internal_name', 'subject_title');
                if($subjectTitleSearchMetadata->isNotEmpty()) {
                    $subjectTitleMetadataId = $subjectTitleSearchMetadata->first()->metadata_id;
                    array_push($documentSystemMetadataKeyValuePairs, "$subjectTitleMetadataId:$documentSubject->title");
                }
            }

            if(!empty($documentSubject->description)){
                $subjectDescriptionSearchMetadata = $documentSystemMetadata->where('internal_name', 'subject_description');
                if($subjectDescriptionSearchMetadata->isNotEmpty()) {
                    $subjectDescriptionMetadataId = $subjectDescriptionSearchMetadata->first()->metadata_id;
                    array_push($documentSystemMetadataKeyValuePairs, "$subjectDescriptionMetadataId:$documentSubject->description");
                }
            } 
        }

        // return $this->arrayContentToCommaeSeparatedString($documentSystemMetadataKeyValuePairs);
        return implode(", ",$documentSystemMetadataKeyValuePairs);
    }

    private function helperToReturnDocumentCustomMetadataKeyValuePairsCsv(Document $document): string {
        $documentAllMetadata = $this->getDocumentsMetadata();
        $documentSystemMetadata = $this->getDocumentsSystemMetadata();
        $documentCustomMetadataKeyValuePairs = [];
        foreach($documentAllMetadata as $metadata) {
            $metadataValue = $metadata->metadata_value;
            $metadataId = $metadata->metadata_id;
            $systemMetadataSearchResult = $documentSystemMetadata->where('metadata_id', $metadataId);
            if($systemMetadataSearchResult->isEmpty() && !empty($metadataValue)) {
                $keyValueString = "$metadataId:$metadataValue";
                array_push($documentCustomMetadataKeyValuePairs, $keyValueString);
            }
        }
        // return $this->arrayContentToCommaeSeparatedString($documentCustomMetadataKeyValuePairs);
        return implode(", ",$documentCustomMetadataKeyValuePairs);
    }

    private function helperToReturnDocumentLicense(string $licenseId) {
        $licenseCollection = $this->getLicenses();
        $searchResult = $licenseCollection->where('license_id', $licenseId);
        return $searchResult->isNotEmpty() ? $searchResult->first() : [];
    }

    private function helperToReturnDocumentLanguage(string $languageId) {
        $languageCollection = $this->getLanguages();
        $searchResult = $languageCollection->where('language_id', $languageId);
        return $searchResult->isNotEmpty() ? $searchResult->first() : [];
    }

    
}
