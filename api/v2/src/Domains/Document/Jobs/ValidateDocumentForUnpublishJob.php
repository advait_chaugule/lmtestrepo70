<?php
namespace App\Domains\Document\Jobs;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use Lucid\Foundation\Job;

class ValidateDocumentForUnpublishJob extends Job
{
    private $documentId;
    private $organizationId;
    public function __construct(string $documentId, string $organizationId)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        $dataToValidates = [ "document_id" => $this->documentId ,  "organization_id" => $this->organizationId, "is_deleted" => '0', "status" => '1'];
        $documents       = $documentRepository->findByAttributes($dataToValidates);
        return $documents->isNotEmpty();
    }
}