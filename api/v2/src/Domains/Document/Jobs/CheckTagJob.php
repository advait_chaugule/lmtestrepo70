<?php
namespace App\Domains\Document\Jobs;
use Lucid\Foundation\Job;
use DB;

class CheckTagJob extends Job
{
    private $documentIdentifier;
    private $organizationId;
    private $versionTag;

    public function __construct($documentIdentifier,$organizationId,$versionTag)
    {
       $this->documentIdentifier     = $documentIdentifier;
       $this->organizationId         = $organizationId;
       $this->versionTag             = $versionTag;
    }

    public function handle()
    {
        $getTag = DB ::table('taxonomy_pubstate_history')            //query to check if tag already exists in DB
                ->select('tag')
                ->where('document_id',$this->documentIdentifier)
                ->where('organization_id', $this->organizationId)
                ->where('is_deleted',0)
                ->where('tag',$this->versionTag)
                ->first();

        return $getTag;

    }
}