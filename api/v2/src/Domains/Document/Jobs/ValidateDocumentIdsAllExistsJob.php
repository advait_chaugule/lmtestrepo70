<?php
namespace App\Domains\Document\Jobs;
use DB;
use Lucid\Foundation\Job;

class ValidateDocumentIdsAllExistsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $documentIdentifier, string $organizationId)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $documentData = DB::table('documents')
                        ->select('organization_id', 'document_id', 'source_document_id','updated_at')
                        ->whereIn('document_id',$this->documentIdentifier)
                        ->where("organization_id",$this->organizationId)
                        ->where("is_deleted",'0')
                        ->get()->toArray();
        $invalidDocuments = array_diff($this->documentIdentifier,array_column($documentData,'document_id'));
        return empty($invalidDocuments) ? ['status'=> 'true', 'data' => $documentData] : ['status'=> 'false', 'data' => $invalidDocuments];
    }
}
