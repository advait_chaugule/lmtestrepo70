<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class ListItemIdForDocumentJob extends Job
{
    private $documentIdentifier;
    private $documentRepository;

    private $itemCollection;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($documentIdentifier)
    {
        //Set the document identifier
        $this->setDocumentIdentifier($documentIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository)
    {
        //Set the Document Repo handler
        $this->itemRepository       =   $itemRepository;
        
        $this->fetchItemListWithAssociation();

        return $this->getItemListWithAssociation();
    }

    // Public Getter and Setter methods
    public function setDocumentIdentifier($identifier){
        $this->documentIdentifier = $identifier;
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

    public function setItemListWithAssociation($collection) {
        $this->itemCollection   =   $collection;
    }

    public function getItemListWithAssociation() {
        return $this->itemCollection;
    }

    private function fetchItemListWithAssociation(){

        $documentIdentifier         =   $this->getDocumentIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['document_id', 'item_id'];
        $keyValuePairs      =   ['document_id'  =>  $documentIdentifier];
        $relations          =   ['itemAssociations'];

        $listItemIds        =   $this->itemRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn,  $keyValuePairs, $relations);
        $this->setItemListWithAssociation($listItemIds);
    }

    
}
