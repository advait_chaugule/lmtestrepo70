<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class GetDocumentByIdJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo)
    {
        $document = $documentRepo->find($this->data['id']);
        //$document->language = $document->language;
        return $document;
    }
}
