<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class CreateDocumentJob extends Job
{
    private $input;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo)
    {
        return $documentRepo->fillAndSave($this->input);
    }
}
