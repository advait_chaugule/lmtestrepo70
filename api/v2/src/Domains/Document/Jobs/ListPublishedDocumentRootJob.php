<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;
use DB;

class ListPublishedDocumentRootJob extends Job
{
    private $documentsFetched;
    private $documentRepository;
    private $parsedDocumentList;
    private $requestDataFilters;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(array $data)
    {
        //
        $this->setInput($data);
    }

    public function setParsedDocumentList(array $data) {
        $this->parsedDocumentList = $data;
    }

    public function getParsedDocumentList() {
        return $this->parsedDocumentList;
    }

    public function setInput(array $data) {
        $this->input = $data;
    }

    public function getInput(): array {
        return $this->input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepo,
        UserRepositoryInterface $userRepo)
    {
        $this->documentRepository = $documentRepo;
        $this->userRepository = $userRepo;

       
        $this->fetchAndSetDocuments();
        $this->prepareDocumentList();

        return $this->getParsedDocumentList();
    }

    public function getDocumentsFetched() {
        return $this->documentsFetched;
    }

    public function setDocumentsFetched($data) {
        $this->documentsFetched = $data;
    }

    private function fetchAndSetDocuments() {
        $input = $this->getInput();
        $documentFilters['organization_id'] = $input['organization_id'];

        $documentFilters['status'] =   '3';
        
        $documents = $this->documentRepository->listPublishedDocumentRoot($documentFilters);
        $this->setDocumentsFetched($documents);
    }

    private function prepareDocumentList() {
        $documentsFetched = $this->getDocumentsFetched();

        $parsedDocuments = [];
        $documentArr     = [];
        $documentPubArr  = [];
        if(count($documentsFetched)>0) {
            foreach($documentsFetched as $document) {
                array_push($documentArr,$document->document_id);
            }
            if(count($documentArr) > 0)
            {
                $taxonomyHistory = DB::table('taxonomy_pubstate_history')
                        
                        ->select('taxonomy_pubstate_history.*')
                        ->whereIn('taxonomy_pubstate_history.document_id',$documentArr)
                        ->where('taxonomy_pubstate_history.unpublish_date','=','0000-00-00 00:00:00')
                        ->get()
                        ->toArray();
                        if(!empty($taxonomyHistory))
                        {
                            foreach($taxonomyHistory as $taxonomyHistoryK => $taxonomyHistoryV)
                                {
                                    $documentPubArr[$taxonomyHistoryV->document_id] = $taxonomyHistoryV->publish_date;    
                                }
                        }
                        
                
            }
            foreach($documentsFetched as $document) {
                $documentId = $document->document_id;
                if ($this->userRepository->find($document->updated_by)){
                    $userDetail = $this->userRepository->find($document->updated_by)->toArray();
                    $updatedByName = $userDetail['first_name'].' '.$userDetail['last_name'];
                }else{
    
                $updatedByName ='';
                }
    
                //$updatedByName = $userDetail['first_name'].' '.$userDetail['last_name']; //Commented since its error if user details is blank
                /*
                  //"updated_at" => !empty($document->updated_at) ? $document->updated_at->toDateTimeString() : "",
                  Remove above code with published date replace with updated_at
                  */
                $parsedDocuments[] = [
                    "document_id" => $documentId,
                    "title" => !empty($document->title) ? $document->title : "",
                    "title_html" => !empty($document->title_html) ? $document->title_html : "",
                    "description" => !empty($document->description) ? $document->description : "",
                    "description_html" => !empty($document->description_html) ? $document->description_html : "",
                    "updated_at"   =>  array_key_exists($documentId,$documentPubArr) ? $documentPubArr[$documentId] : "",
                    "updated_by" => $updatedByName
                ];
            }
            $this->setParsedDocumentList($parsedDocuments);
        }
        
    }
}
