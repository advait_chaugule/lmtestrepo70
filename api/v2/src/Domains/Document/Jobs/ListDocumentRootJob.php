<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ArrayHelper;
use App\Services\Api\Traits\GroupDocumentTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\UserRepositoryInterface;

use Illuminate\Support\Facades\DB;

class ListDocumentRootJob extends Job
{
    use ArrayHelper,GroupDocumentTrait;

    private $documentRepository;
    private $projectRepository;

    private $documentType;

    private $requestDataFilters;
    private $documentsFetched;
    private $documentIds;
    private $projectsAssociatedWithDocuments;

    private $parsedDocumentList;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setAllowedDataFilters(["limit", "offset"]);
    }

    public function setRequestFilters(array $data) {
        $this->requestDataFilters = $data;
    }

    public function getRequestFilters(): array {
        return $this->requestDataFilters;
    }

    public function setAllowedDataFilters(array $data) {
        $this->allowedDataFilters = $data;
    }

    public function getAllowedDataFilters(): array {
        return $this->allowedDataFilters;
    }

    public function setSearchFilters(array $data) {
        $this->searchFilters = $data;
    }

    public function getSearchFilters(): array {
        return $this->searchFilters;
    }

    public function setDocumentsFetched($data) {
        $this->documentsFetched = $data;
    }

    public function getDocumentsFetched() {
        return $this->documentsFetched;
    }

    public function setDocumentIds(array $data){
        $this->documentIds = $data;
    }

    public function getDocumentIds(): array{
        return $this->documentIds;
    }

    public function setProjectsAssociatedWithDocuments($data){
        $this->projectsAssociatedWithDocuments = $data;
    }

    public function getProjectsAssociatedWithDocuments(){
        return $this->projectsAssociatedWithDocuments;
    }

    public function setParsedDocumentList(array $data) {
        $this->parsedDocumentList = $data;
    }

    public function getParsedDocumentList(): array {
        return !empty($this->parsedDocumentList)?($this->parsedDocumentList):array();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepo,
        ProjectRepositoryInterface $projectRepo,
        UserRepositoryInterface $userRepo,
        Request $request)
    {

        // set the database repository
        $this->documentRepository = $documentRepo;
        $this->projectRepository = $projectRepo;
        $this->userRepository = $userRepo;

        $this->documentType     =   !empty($request['document_type'])? $request['document_type'] : "";
        $this->documentListType =   !empty($request['document_list_type']) ? $request['document_list_type'] : "";

        // set filters from request body
        $this->setRequestFilters($request->all());

        // process and set filters for fetching documents
        $this->processAndSetFilters();

        // fetch documents and set them
        $this->fetchAndSetDocuments();

        // extract the document ids from documents fetched from db
        $this->extractAndSetDocumentIds();

        // fetch and set projects associated to documents
        $this->fethAndSetProjectsAssociatedToDocuments();

        $this->addProjectCountForEachDocumentAndPrepareDocumentList();

        return $this->getParsedDocumentList();
    
    }

    private function processAndSetFilters() {
        $requestFilters = $this->getRequestFilters();
        $allowedFilters = $this->getAllowedDataFilters();
        // set default filter attribute and values
        $filters = [
            "limit" => 100,
            "offset" => 0
        ];
        foreach($allowedFilters as $allowedFilter){
            // if filter allowed set it
            if(array_key_exists($allowedFilter, $requestFilters)){
                // override with requested filter values if allowed
                $filters[$allowedFilter] = (integer)$requestFilters[$allowedFilter];
            }
        }
        $this->setSearchFilters($filters);
    }

    private function fetchAndSetDocuments() {
        $searchFilterAttributeWithValues = $this->getSearchFilters();
        $input = $this->getRequestFilters();
        $searchFilterAttributeWithValues['organization_id']         =       $input['auth_user']['organization_id'];
        if($this->documentType==''){
            $searchFilterAttributeWithValues['document_type']           =       [];
        }else{
            $searchFilterAttributeWithValues['document_type']           =       [$this->documentType];
        }
        $searchFilterAttributeWithValues['document_list_type']      =       $this->documentListType;

        $searchFilterAttributeWithValues['adoption_status'] =   ['6'];

        if(isset($this->documentListType) && $this->documentListType == '2') {
            $searchFilterAttributeWithValues['adoption_status'] =   ['5','6','7'];
        }
        $searchFilterAttributeWithValues['import_status'] = 0; // to allow only import completed taxonomies
        //dd($searchFilterAttributeWithValues);
        
        $documents = $this->documentRepository->listDocumentRoot($searchFilterAttributeWithValues);
        $this->setDocumentsFetched($documents);
    }

    private function extractAndSetDocumentIds() {
        $documentsFetched = $this->getDocumentsFetched();
        $documentIds = $this->createArrayFromCommaeSeparatedString($documentsFetched->implode('document_id', ','));
        $this->setDocumentIds($documentIds);
    }

    private function fethAndSetProjectsAssociatedToDocuments() {
        $attributeIn = 'document_id';
        $containedInValues = $this->getDocumentIds();
        $returnCollection = true;
        $fieldsToReturn = ['project_id', 'document_id', 'is_deleted'];
        $keyValuePairs = ['is_deleted' => 0];
        $data = $this->projectRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                    $attributeIn, 
                    $containedInValues, 
                    $returnCollection, 
                    $fieldsToReturn,
                    $keyValuePairs                    
                );
        $this->setProjectsAssociatedWithDocuments($data);
    }

    private function addProjectCountForEachDocumentAndPrepareDocumentList() {
        $documentsFetched = $this->getDocumentsFetched();
        $parsedDocuments = [];
        $updatedbyuserList = [];
        $createdbyuserList = [];
        $updateduserList = [];
        $createduserList = [];
        $additionalList = [];
        $internal_name = [];
        $fieldnames = [];
        $finalparsedDoc = [];
        $internal_name_custom = [];
        $metadata_id_custom = [];
        $parsedaddDoc = [];
        $fieldname_custom = [];
        $finalList = [];
        $customList = [];
        $documentIdList = [];
        $publishedIdList = [];
        $documentPubArr  = [];
        $SourceTypeArray = [];
        if(count($documentsFetched)>0) {
            foreach($documentsFetched as $document) {
                $documentIdList[] = $document->document_id;
                if($document->updated_by != "")
                {
                    $updatedbyuserList[]=$document->updated_by;
                }
                // added to get created by user start
                if($document->created_by != "")
                {
                    $createdbyuserList[]=$document->created_by;
                }
                if($document->status == 3)
                {
                    $publishedIdList[] =  $document->document_id;
                }
                // added to get created by user end
            }

            if(count($publishedIdList) > 0)
            {
                $taxonomyHistory = DB::table('taxonomy_pubstate_history')
                        
                        ->select('taxonomy_pubstate_history.*')
                        ->whereIn('taxonomy_pubstate_history.document_id',$publishedIdList)
                        ->where('taxonomy_pubstate_history.unpublish_date','=','0000-00-00 00:00:00')
                        ->get()
                        ->toArray();
                        if(!empty($taxonomyHistory))
                        {
                            foreach($taxonomyHistory as $taxonomyHistoryK => $taxonomyHistoryV)
                                {
                                    $documentPubArr[$taxonomyHistoryV->document_id] = $taxonomyHistoryV->publish_date;    
                                }
                        }
                        
                
            }
            if(count($updatedbyuserList) > 0)
            {
                $getuserList = DB::table('users')
                ->select('user_id','first_name','last_name')
                ->whereIn('user_id', $updatedbyuserList)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
         
                foreach($getuserList as $getuserListK =>$getuserListV)
                {
                    $updateduserList[$getuserListV->user_id] = $getuserListV;

                }  
            }

            // added to get created by user start

            if(count($createdbyuserList) > 0)
            {
                $getCreatedUserList = DB::table('users')
                ->select('user_id','first_name','last_name')
                ->whereIn('user_id', $createdbyuserList)
                ->where('is_deleted','0')
                ->get()
                ->toArray();
         
                foreach($getCreatedUserList as $getCreatedUserListK =>$getCreatedUserListV)
                {
                    $createduserList[$getCreatedUserListV->user_id] = $getCreatedUserListV;

                }  
            }
           
            $input = $this->getRequestFilters();
            $userId = $input['auth_user']['user_id'];
            $organizationId = $input['auth_user']['organization_id'];
            
            $getUserSetting = DB::table('user_settings')
            ->where('user_id',$userId)
            ->where('organization_id',$organizationId)
            ->where('is_deleted',0)
            ->where('id_type',1)
            ->get()
            ->toArray();
           
            if(!empty($getUserSetting)){
                $setting_json = json_decode($getUserSetting[0]->json_config_value); 
                $is_user_default = $setting_json->is_user_default;
                $default_for_all_user = $setting_json->default_for_all_user;
                if($is_user_default == 1){
                    $taxonomy_list_setting = $setting_json->taxonomy_list_setting;
                }else if($default_for_all_user == 1){ 
                    $getUserSetting = DB::table('organizations')
                                    ->where('organization_id',$organizationId)
                                    ->where('is_deleted',0)
                                    ->get()
                                    ->toArray();
                    $setting_json = json_decode($getUserSetting[0]->json_config_value);
                    $taxonomy_list_setting = $setting_json->taxonomy_list_setting;
                }
            }else{
                    $getUserSetting = DB::table('organizations')
                    ->where('organization_id',$organizationId)
                    ->where('is_deleted',0)
                    ->get()
                    ->toArray();
                    $setting_json = json_decode($getUserSetting[0]->json_config_value);
                    if(!empty($setting_json)){
                      $taxonomy_list_setting = $setting_json->taxonomy_list_setting;
                    }
            }

            if(!empty($taxonomy_list_setting)){
                foreach($taxonomy_list_setting as $taxonomy_list_settingk=>$taxonomy_list_settingv){
                    if(isset($taxonomy_list_settingv->is_custom) && $taxonomy_list_settingv->is_custom == 1){
                      $internal_name_custom[] = $taxonomy_list_settingv->internal_name;
                      $metadata_id_custom[] = $taxonomy_list_settingv->metadata_id;
                    }else{
                      $internal_name[] = $taxonomy_list_settingv->internal_name;
                    }
                }

                foreach($internal_name as $internal_namek => $internal_namev){
                    $default_array = array('title','document_type','status','associated_projects_count','group','last_active_date','created_at','import_type','imported_by');
                     if(in_array($internal_namev,$default_array))
                     { 
                          unset($internal_name[$internal_namek]);
                     }
                } 

            }
           
            if(!empty($internal_name)){
                $fieldnames[] =  'documents.document_id';
               foreach($internal_name as $internal_namek => $internal_namev) {

                        if(strpos($internal_namev,'_')!==false){
                            $posnum = strpos($internal_namev,'_');
                            $table_name  = substr($internal_namev,0,$posnum);
                            $column_name = substr($internal_namev,$posnum+1);
                        }else{
                            $table_name    = "Default";
                        }

                        switch($table_name){
                            case "subject":
                                $subject = true;
                                if($column_name == "title"){
                                    $alias = 'subject_title';
                                }else if($column_name == "description"){
                                    $alias = 'subject_description';
                                }else if($column_name == "hierarchy_code"){
                                    $alias = 'subject_hierarchy_code';
                                }
                                if(isset($alias)){
                                    $fieldnames[] = 'subjects.'.$column_name.' as '.$alias;
                                   // $fieldname[] =  $column_name.' as '.$alias;
                                }else{
                                    $fieldnames[] = $internal_namev;   
                                }
                                unset($alias);
                                break;
                            case "concept":
                                $concept = true;
                                if($column_name == "title"){
                                    $alias = 'concept_title';
                                }else if($column_name == "description"){
                                    $alias = 'concept_description';
                                }else if($column_name == "hierarchy_code"){
                                    $alias = 'concept_hierarchy_code';
                                }
                                if(isset($alias)){
                                    $fieldnames[] = 'concepts.'.$column_name.' as '.$alias;
                                }else{
                                    $fieldnames[] = $internal_namev;  
                                }
                                unset($alias);
                                break;
                            case "license":
                                $license = true;
                                if($column_name == "title"){
                                    $alias = 'license_title';
                                }else if($column_name == "description"){
                                    $alias = 'license_description';
                                }else if($column_name == "hierarchy_code"){
                                    $alias = 'license_hierarchy_code';
                                }
                                if(isset($alias)){
                                    $fieldnames[] = 'licenses.'.$column_name.' as '.$alias;
                                   // $fieldname[] =  $column_name.' as '.$alias;
                                }else{
                                    $fieldnames[] = $internal_namev;  
                                }
                                unset($alias);
                                break;
                            case "language":
                                if($column_name == "name"){
                                    $alias = 'language_name';
                                }
                                if(isset($alias)){
                                    $fieldnames[] = 'languages.'.$column_name.' as '.$alias;
                                }else{
                                    $fieldnames[] = $internal_namev;  
                                }
                                unset($alias);
                                break;
                            default:
                                $default = true;
                                if($internal_namev == 'description'){                       //to resolve issue for Description, Bug id = UF-2704.
                                    $fieldnames[] = 'documents.description';
                                }else{
                                    $fieldnames[] = $internal_namev;
                                }
                                break;
                                
                            }
                           
                   }
                   
                $getadditionaldoc = DB::table('documents')
                        ->select($fieldnames)
                        ->leftJoin('document_subject', 'documents.document_id', '=', 'document_subject.document_id')
                        ->leftJoin('subjects', 'document_subject.subject_id', '=', 'subjects.subject_id')
                        ->leftJoin('licenses', 'documents.license_id', '=', 'licenses.license_id')
                        ->leftJoin('document_metadata', 'documents.document_id', '=', 'document_metadata.document_id')
                        ->leftJoin('languages', 'documents.language_id', '=', 'languages.language_id')
                        ->where('documents.organization_id','=',$organizationId)
                        ->get()
                        ->toarray();

                foreach($getadditionaldoc as $getadditionaldocK =>$getadditionaldocV)
                {
                    $additionalList[$getadditionaldocV->document_id] = (array)$getadditionaldocV;

                }  

            }

            if(!empty($metadata_id_custom)){
                $fieldname_custom[] =  'document_metadata.document_id';

                foreach($internal_name_custom as $internal_name_customk => $internal_name_customv) {
                        $fieldname_custom [] = 'metadata_value as '.$internal_name_customv;
                } 
                
                $getcustomdoc = DB::table('document_metadata')
                        ->select($fieldname_custom)
                        ->whereIn('metadata_id', $metadata_id_custom)
                        ->get()
                        ->toarray(); 
                        

                foreach($getcustomdoc as $getcustomdocK =>$getcustomdocV)
                {
                    $customList[$getcustomdocV->document_id] = (array)$getcustomdocV;
        
                }  

            }

            if(!empty($additionalList) && !empty($customList)){
                $finalList = array_merge($additionalList,$customList);
            }else if(!empty($additionalList) && empty($customList)){
                $finalList = $additionalList;
            }else if(!empty($customList) && empty($additionalList)){
                $finalList = $customList;
            }
           
             // added to get created by user end
             
             // added to get group of documents
             $groups = $this->getDocumentGroups($documentIdList,$userId);

             $fieldPossibleValues=DB::table('metadata')                                 // get field_possible_values from metadata table
                ->select('field_possible_values')
                ->where('internal_name','source_type')
                ->where('organization_id',$organizationId)
                ->first();

            $fieldValues = isset($fieldPossibleValues->field_possible_values) ? json_decode($fieldPossibleValues->field_possible_values) : [];    // convert json to array
            
            foreach($fieldValues as $fieldvalueK => $fieldvalueV ){                     // get required field value from field_possible_values 
                    $SourceTypeArray[$fieldvalueV->key]=$fieldvalueV->value;            // set required field value value in SourceTypeArray
            }

             foreach($documentsFetched as $document) {
                $documentId = $document->document_id;
               /* if ($this->userRepository->find($document->updated_by)){
                    $userDetail = $this->userRepository->find($document->updated_by)->toArray();
                    $updatedByName = $userDetail['first_name'].' '.$userDetail['last_name'];
                }else{
    
                    $updatedByName ='';
                }
                */

                if(count($updateduserList) > 0)
                {
                    if (array_key_exists($document->updated_by,$updateduserList))
                    {
                        $userDetail = $updateduserList[$document->updated_by];
                        $updatedByName = $userDetail->first_name.' '.$userDetail->last_name;
                    }
                    else
                    {
                        $updatedByName ='';
                    }
                }
                else
                {
                    $updatedByName ='';
                }

                // added to get created by user start
                if(count($createduserList) > 0)
                {
                    if (array_key_exists($document->created_by,$createduserList))
                    {
                        $userCreateDetail = $createduserList[$document->created_by];
                        $createdByName = $userCreateDetail->first_name.' '.$userCreateDetail->last_name;
                    }
                    else
                    {
                        $createdByName ='';
                    }
                }
                else
                {
                    $createdByName ='';
                }
                $parsedaddDoc = []; //Reinitialized since same document details were passed if custom metadata used in table setting
                if(!empty($finalList)){
                    if(array_key_exists($document->document_id,$finalList))
                    {
                        $parsedaddDoc = $finalList[$document->document_id];
                    }
                }
              
                // added to get created by user end

              
                //$updatedByName = $userDetail['first_name'].' '.$userDetail['last_name'];
                $sourceDocumentId = !empty($document->source_document_id) ? $document->source_document_id : "";
                if($document->import_type == 2){
                    $uri = $document->uri;
                    $uriArr   = explode("/", $uri);
                    $sourceDocumentId = $uriArr[sizeof($uriArr) - 1];
                }
                $sourceType = isset($SourceTypeArray[$document->source_type]) ? $SourceTypeArray[$document->source_type] :"";           //check isset for source_type since source_type already existing taxonomy is blank
                $parsedDocuments = [
                    "document_id" => $documentId,
                    "title" => !empty($document->title) ? $document->title : "",
                    "title_html" => !empty($document->title_html) ? $document->title_html : "",
                    "document_type" =>  !empty($document->document_type) ? $document->document_type : '1',
                    "created_at" => $document->created_at->toDateTimeString(),
                    "associated_projects_count" => $this->countProjectsAssociatedToDocument($documentId),
                    // set static value for now
                    "last_active_date" => array_key_exists($documentId,$documentPubArr) ? $documentPubArr[$documentId] : !empty($document->updated_at) ? $document->updated_at->toDateTimeString() : "",
                    "updated_by" => $updatedByName,
                    "adoption_status"=>$document->adoption_status,
                    "status" => $document->status,
                    "import_type" => !empty($document->import_type) ? $document->import_type : "",
                    "actual_import_type" =>!empty($document->actual_import_type) ? $document->actual_import_type : "",          // foe import type view column
                    "creator" => !empty($document->creator) ? $document->creator : "",
                    "imported_by" => $createdByName,
                    "uri" => !empty($document->uri) ? $document->uri : "",
                    "source_document_id"=> !empty($sourceDocumentId) ? $sourceDocumentId : "",
                    "groups" => isset($groups[$documentId]) ? $groups[$documentId] : [],
                    "is_locked" => $document->is_locked,
                    "source_type"=> $sourceType,                        // sourcr_type name for column display
                    "source_type_id"=> $document->source_type,          // source_type id to add conditon to import_type column
                ];
           
                if(!empty($parsedaddDoc)){
                    $parsedDocMerge = array_merge($parsedDocuments,$parsedaddDoc);          // array merge in new variable
                    $parsedDocMerge['source_type'] =$sourceType;                            // add source_type metadata to merged array
                    $finalparsedDoc[] = $parsedDocMerge;                                    // set merged array with source_type metadata to $finalparsedDoc
                }else{
                    $finalparsedDoc[] = $parsedDocuments;
                }
            }
             
            $this->setParsedDocumentList($finalparsedDoc);
        }
    }

    private function countProjectsAssociatedToDocument(string $documentId): int {
        $projectsAssociatedToDocuments = $this->getProjectsAssociatedWithDocuments();
        return $projectsAssociatedToDocuments->filter(function ($item, $key) use($documentId) {
            if($item->document_id===$documentId) {
                return $item;
            }
        })->count();
    }

}
