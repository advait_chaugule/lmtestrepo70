<?php
namespace App\Domains\Document\Jobs;
use App\Data\Models\TaxonomyPubStateHistory;
use Lucid\Foundation\Job;

class ValidateVersionExistsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, string $organizationId, string $version)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->organizationId = $organizationId;
        $this->version = $version;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $checkVersion = TaxonomyPubStateHistory::where([
            'document_id'=>$this->documentIdentifier,
            'organization_id'=>$this->organizationId,
            'publish_number'=>$this->version,
            'is_deleted' => 0
            ])->first();

        return isset($checkVersion->document_id) ? true : false;
    }
}
