<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ArrayHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ListDocumentByTypeJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $organization_id,String $document_type)
    {
        
        $this->organization_id = $organization_id;
        $this->document_type = $document_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $DocumentRepository)
    {
        $this->DocumentRepository = $DocumentRepository;
        $attributes = ['organization_id' => $this->organization_id,'document_type' => $this->document_type, 'is_deleted'=>0];
        $result=  $this->DocumentRepository->findByAttributes($attributes);       
        return  $result->toArray();
            
    }
}
