<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ValidatePublicReviewDocumentJob extends Job
{

    private $documentRepository;
    private $documentIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {
        $this->documentIdentifier = $documentIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        $this->documentRepository = $documentRepository;
        $document = $this->documentRepository->find($this->documentIdentifier);
        return $document->adoption_status===6;
    }
}
