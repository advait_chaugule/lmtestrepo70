<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class PublishDocumentJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $identifier;
    private $useridentifier;
    private $documentRepository;
    private $adoptionStatus;
    private $publishStatus;

     public function __construct($input,$user,$adoptionStatus,$publishStatus)
    {
        //Set Document Identifier
        $this->setIdentifier($input);
        $this->setUser($user);
        $this->adoptionStatus = $adoptionStatus;
        $this->publishStatus  = $publishStatus;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        //
        $this->documentRepository           =   $documentRepository;
        

            $this->publishDocument($this->publishStatus);
        
    }

    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }

    public function setUser($useridentifier){
        $this->useridentifier = $useridentifier;
    }

    public function getUser(){
        return $this->useridentifier;
    }
    
    private function publishDocument($publishStatus) {
        $documentIdentifier =   $this->getIdentifier();
        $userId =   $this->getUser();
        if($publishStatus==1) {
            $publishStatus = 3; // publish
        }else if($publishStatus==2){
            $publishStatus = 1; // Unpublish
        }

        $columnValueFilterPairs =   ['document_id'  =>  $documentIdentifier];
        $attributes             =   ['adoption_status'  => $this->adoptionStatus,
                                     'status'           => $publishStatus, // publish status
                                     'updated_at' => now()->toDateTimeString(),
                                     'updated_by' =>  $userId  
                                    ];
        //$deleteDocument     =    $this->documentRepository->delete($columnValueFilterPairs);
        $publishDocument      =    $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs,  $attributes);
        return $publishDocument;
    }
}
