<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class CheckProjectAssociatedToDocumentJob extends Job
{
    private $documentIdentifier;
    private $organizationIdentifier;

    private $projectRepository;

    private $projectList;
    private $projectEntity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, string $organizationIdentifier)
    {
        //Set the identifier variables
        $this->setDocumentIdentifier($documentIdentifier);
        $this->setOrganizationIdentifier($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProjectRepositoryInterface $projectRepository)
    {
        //Set the document repository handler
        $this->projectRepository    =   $projectRepository;

        $listAssociatedProjects     =   $this->fetchAssociatedProjectList();

        $this->setProjectList($listAssociatedProjects);

        $this->parseResponse();

        return $this->getProjectEntity();

    }

    //Public Setter and Getter methods
    public function setDocumentIdentifier($identifier){
        $this->documentIdentifier = $identifier;
    } 

    public function getDocumentIdentifier(){
        return $this->documentIdentifier;
    }

    public function setOrganizationIdentifier($identifier){
        $this->organizationIdentifier = $identifier;
    }

    public function getOrganizationIdentifier() {
        return $this->organizationIdentifier;
    }

    public function setProjectList($data){
        $this->projectList = $data;
    }

    public function getProjectList(){
        return $this->projectList;
    }

    public function setProjectEntity($data) {
        $this->projectEntity = $data;
    }

    public function getProjectEntity() {
        return $this->projectEntity;
    }

    private function fetchAssociatedProjectList() {
        $documentIdentifier     = $this->getDocumentIdentifier();
        $organizationIdentifier =   $this->getOrganizationIdentifier();

        $returnCollection   =   true;
        $fieldsToReturn     =   ['project_id', 'project_name'];
        $keyValuePairs      =   ['document_id' => $documentIdentifier, 'organization_id' => $organizationIdentifier, 'is_deleted' => '0'];
        $relations          =   ['document'];
        
        $projectAssociatedToDocument = $this->projectRepository->findByAttributesWithSpecifiedFields(
            $returnCollection,
            $fieldsToReturn,
            $keyValuePairs,
            $relations
        );
        
        return $projectAssociatedToDocument;
    }

    private function parseResponse() {
        $projectEntity          =   [];
        $projectAssociatedList  =   $this->getProjectList();

        foreach($projectAssociatedList as $project){
            $projectEntity[] = [
                'project_id'    =>  $project->project_id,
                'project_name'  =>  $project->project_name,
            ];
        }

        $this->setProjectEntity($projectEntity);
    }
}
