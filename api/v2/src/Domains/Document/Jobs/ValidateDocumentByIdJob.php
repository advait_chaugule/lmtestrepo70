<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Document\Validators\DcoumentExistsValidator;

class ValidateDocumentByIdJob extends Job
{

    public $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DcoumentExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
