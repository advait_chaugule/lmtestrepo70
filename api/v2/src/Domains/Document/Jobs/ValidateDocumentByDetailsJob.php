<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use DB;

class ValidateDocumentByDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $inputDetails)
    {
        $this->data = $inputDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $document_id = $this->data['document_id'];
        $organization_id = $this->data['organization_id'];
        $user_id= $this->data['user_id'];
        $adoption_status= $this->data['adoption_status'];
        $publish_status= $this->data['publish_status'];
        $flag = 1;
        if($document_id != '')
        {
            $documentData = DB::table('documents')->select('documents.*')->where('document_id',$document_id)->first();
            if($documentData)
            {
               
                if($documentData->organization_id == $organization_id)
                {
                    if($documentData->is_deleted == 0)
                    {
                        if($publish_status == 1 && $documentData->status == 3)
                        {
                            $flag = 4;
                        }
                        if($publish_status == 2 && $documentData->status == 1)
                        {
                            $flag = 5;
                        }
                        else {
                            $flag = 6;
                        }
                    }
                    else {
                        $flag = 3;
                    }
                }
                else {
                    $flag = 2;
                }
            }
            else {
                $flag = 1;
            }
        }
        else {
            $flag = 1;
        }

        return $flag;
    }
}
