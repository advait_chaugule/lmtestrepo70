<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;
use App\Data\Models\Organization;
use App\Services\Api\Traits\UuidHelperTrait;
use App\Data\Models\DocumentMetadata;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Services\Api\Traits\CaseFrameworkTrait;

class SaveDocumentAndRespectiveMetadataValuesJob extends Job
{   
    use UuidHelperTrait,CaseFrameworkTrait;

    private $documentRepository;
    private $languageRepository;
    private $licenseRepository;
    private $subjectRepository;
    private $nodeTypeRepository;

    private $requestUserDetails;
    private $existingDocument;
    private $basicDocumentModel;
    private $basicSubjectModel;
    private $basicLicenseModel;
    private $basicLanguageModel;
    private $customMetadataKeyValuePairs    =   [];

    private $savedSubject;
    private $savedLicense;
    private $savedLanguage;
    private $savedDocument;
    private $organizationCode;
    private $documentMetadataKeyValuePairs;
    private $domainName;

    /**
     * SaveDocumentAndRespectiveMetadataValuesJob constructor.
     * @param array $documentMetadataKeyValuePairs
     * @param $requestUrl
     */
    public function __construct(array $documentMetadataKeyValuePairs,$requestUrl)
    {
        $this->setDocumentMetadataKeyValuePairs($documentMetadataKeyValuePairs);
        $this->setCustomMetadataKeyValuePairs($documentMetadataKeyValuePairs);
        $this->setRequestUserDetails($documentMetadataKeyValuePairs);

        $organizationId = $this->getRequestUserDetails()["organization_id"];
        $this->setOrgCode($organizationId);
        $this->domainName = $requestUrl;
    }

    public function setDocumentMetadataKeyValuePairs(array $data) {
        // remove request user details before setting
        unset($data["auth_user"]);
        $this->documentMetadataKeyValuePairs = $data;
    }

    public function getDocumentMetadataKeyValuePairs(): array {
        return $this->documentMetadataKeyValuePairs;
    }

    public function setCustomMetadataKeyValuePairs(array $data) {
        if(isset($data['custom']) && $data['custom'] != ''){
            $this->customMetadataKeyValuePairs = $data['custom'];
        }        
    }

    public function getCustomMetadataKeyValuePairs(){
        return $this->customMetadataKeyValuePairs;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data["auth_user"];
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setExistingDocument($data) {
        $this->existingDocument = $data;
    }

    public function getExistingDocument() {
        return $this->existingDocument;
    }

    public function setBasicDocumentModel(array $data) {
        $this->basicDocumentModel = $data;
    }

    public function getBasicDocumentModel(): array {
        return $this->basicDocumentModel;
    }

    public function setBasicSubjectModel(array $data) {
        $this->basicSubjectModel = $data;
    }

    public function getBasicSubjectModel(): array {
        return $this->basicSubjectModel;
    }

    public function setBasicLicenseModel(array $data) {
        $this->basicLicenseModel = $data;
    }

    public function getBasicLicenseModel(): array {
        return $this->basicLicenseModel;
    }

    public function setBasicLanguageModel(array $data) {
        $this->basicLanguageModel = $data;
    }

    public function getBasicLanguageModel(): array {
        return $this->basicLanguageModel;
    }

    public function setSavedSubject($data) {
        $this->savedSubject = $data;
    }

    public function getSavedSubject() {
        return $this->savedSubject;
    }

    public function setSavedLicense($data) {
        $this->savedLicense = $data;
    }

    public function getSavedLicense() {
        return $this->savedLicense;
    }

    public function setSavedLanguage($data) {
        $this->savedLanguage = $data;
    }

    public function getSavedLanguage() {
        return $this->savedLanguage;
    }

    public function setSavedDocument($data) {
        $this->savedDocument = $data;
    }

    public function getSavedDocument() {
        return $this->savedDocument;
    }
    public function setOrgCode(string $data) {
        
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $data)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        return $this->organizationCode;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository
    )
    {
        // set db repositories
        $this->documentRepository   =   $documentRepository;
        $this->languageRepository   =   $languageRepository;
        $this->licenseRepository    =   $licenseRepository;
        $this->subjectRepository    =   $subjectRepository;
        $this->nodeTypeRepository   =   $nodeTypeRepository;

        $this->fetchAndSetExistingDocument();

        
        $this->createAndSetBasicDocumentModelAndOtherDependentEntities();

        // call relevant methods in a transaction block
        
            $this->saveAndSetSubject();
            $this->saveAndSetLicense();
            $this->saveAndSetLanguage();
            $this->updateAdditionalMetadataIfSimilarToCaseMetadata();
            $this->saveAndSetCustomMetadata();
            $this->saveAndSetDocument();
        
        
        return $this->getSavedDocument();
    }

    private function fetchAndSetExistingDocument() {
        $documentMetadataKeyValuePairs = $this->getDocumentMetadataKeyValuePairs();
        $documentId = $documentMetadataKeyValuePairs["document_id"];
        $document = $this->documentRepository->find($documentId);
        $this->setExistingDocument($document);
    }

    private function createAndSetBasicDocumentModelAndOtherDependentEntities() {
        $existingDocument               =   $this->getExistingDocument();
        $documentMetadataKeyValuePairs = $this->getDocumentMetadataKeyValuePairs();
        $documentId = $documentMetadataKeyValuePairs["document_id"];
        // parse basic document input and set document model
        $creator = !empty($documentMetadataKeyValuePairs["creator"]) ? $documentMetadataKeyValuePairs["creator"] : "";
        $title = !empty($documentMetadataKeyValuePairs["title"]) ? $documentMetadataKeyValuePairs["title"] : "";
        $documentType = !empty($documentMetadataKeyValuePairs["document_type"]) ? $documentMetadataKeyValuePairs["document_type"] : $existingDocument->document_type;
        $adoptionStatus = !empty($documentMetadataKeyValuePairs["adoption_status"]) ? $documentMetadataKeyValuePairs["adoption_status"] : "";
        $officialSourceUrl = !empty($documentMetadataKeyValuePairs["official_source_url"]) ? $documentMetadataKeyValuePairs["official_source_url"] : "";
        $publisher = !empty($documentMetadataKeyValuePairs["publisher"]) ? $documentMetadataKeyValuePairs["publisher"] : "";
        $publisher_html = !empty($documentMetadataKeyValuePairs["publisher_html"]) ? $documentMetadataKeyValuePairs["publisher_html"] : "";
        $description = !empty($documentMetadataKeyValuePairs["description"]) ? $documentMetadataKeyValuePairs["description"] : "";
        $version = !empty($documentMetadataKeyValuePairs["version"]) ? $documentMetadataKeyValuePairs["version"] : "";
        $statusStartDate = !empty($documentMetadataKeyValuePairs["status_start_date"]) ? $documentMetadataKeyValuePairs["status_start_date"] : "";
        $statusEndDate = !empty($documentMetadataKeyValuePairs["status_end_date"]) ? $documentMetadataKeyValuePairs["status_end_date"] : "";
        $notes = !empty($documentMetadataKeyValuePairs["notes"]) ? $documentMetadataKeyValuePairs["notes"] : "";
        $title_html = !empty($documentMetadataKeyValuePairs["title_html"]) ? $documentMetadataKeyValuePairs["title_html"] : "";
        $description_html = !empty($documentMetadataKeyValuePairs["description_html"]) ? $documentMetadataKeyValuePairs["description_html"] : "";
        $notes_html = !empty($documentMetadataKeyValuePairs["notes_html"]) ? $documentMetadataKeyValuePairs["notes_html"] : "";
        $subjectTitleHtml = !empty($documentMetadataKeyValuePairs["subject_title_html"]) ? $documentMetadataKeyValuePairs["subject_title_html"] : "";
        $subjectDescHtml = !empty($documentMetadataKeyValuePairs["subject_description_html"]) ? $documentMetadataKeyValuePairs["subject_description_html"] : "";
        $licenseTitleHtml = !empty($documentMetadataKeyValuePairs["license_title_html"]) ? $documentMetadataKeyValuePairs["license_title_html"] : "";
        $licenseTitle = !empty($documentMetadataKeyValuePairs["license_title"]) ? $documentMetadataKeyValuePairs["license_title"] : "";
        $basicDocumentModel = [
                                "document_id" => $documentId,
                                "creator" => $creator,
                                "title" => $title,
                                "document_type" => $documentType,
                                "official_source_url" => $officialSourceUrl,
                                "publisher" => $publisher,
                                "description" => $description,
                                "version" => $version,
                                "status_start_date" => $statusStartDate,
                                "status_end_date" => $statusEndDate,
                                "notes" => $notes,
                                "adoption_status"=>$adoptionStatus,
                                "publisher_html"=>$publisher_html,
                                "title_html"=>$title_html,
                                "description_html"=>$description_html,
                                "notes_html"=>$notes_html,
                                "subject_title_html"=>$subjectTitleHtml,
                                "subject_description_html"=>$subjectDescHtml,
                                "license_title_html"=>$licenseTitleHtml,
                                "license_title"=>$licenseTitle,
                            ];
                            if( !empty($documentMetadataKeyValuePairs["title_html"])){
                                $basicDocumentModel["title_html"] =$documentMetadataKeyValuePairs["title_html"];
                            }
                            if( !empty($documentMetadataKeyValuePairs["notes_html"])){
                                $basicDocumentModel["notes_html"] =$documentMetadataKeyValuePairs["notes_html"];
                            }
                            if( !empty($documentMetadataKeyValuePairs["description_html"])){
                                $basicDocumentModel["description_html"] =$documentMetadataKeyValuePairs["description_html"];
                            }
        $this->setBasicDocumentModel($basicDocumentModel);

        // parse basic subject input and set subject model
        $basicSubjectModel = [];
        $subjectTitle = !empty($documentMetadataKeyValuePairs["subject_title"]) ? $documentMetadataKeyValuePairs["subject_title"] : "";
        $subjectHierarchyCode = !empty($documentMetadataKeyValuePairs["subject_hierarchy_code"]) ? $documentMetadataKeyValuePairs["subject_hierarchy_code"] : "";
        $subjectDescription = !empty($documentMetadataKeyValuePairs["subject_description"]) ? $documentMetadataKeyValuePairs["subject_description"] : "";
            $basicSubjectModel = [
                "title" => $subjectTitle,
                "hierarchy_code" => $subjectHierarchyCode,
                "description" => $subjectDescription,
            ];
            if( !empty($documentMetadataKeyValuePairs["subject_title_html"])){
                $basicSubjectModel["title_html"] =$documentMetadataKeyValuePairs["subject_title_html"];
            }else{
                $basicSubjectModel["title_html"] ='';
            }
            if( !empty($documentMetadataKeyValuePairs["subject_description_html"])){
                $basicSubjectModel["description_html"] =$documentMetadataKeyValuePairs["subject_description_html"];
            }else{
                $basicSubjectModel["description_html"] ='';
            }
        $this->setBasicSubjectModel($basicSubjectModel);

        // parse basic license input and set license model
        $basicLicenseModel = [];
        $licenseTitle = !empty($documentMetadataKeyValuePairs["license_title"]) ? $documentMetadataKeyValuePairs["license_title"] : "";
        $licenseDescription = !empty($documentMetadataKeyValuePairs["license_description"]) ? $documentMetadataKeyValuePairs["license_description"] : "";
        $licenseText = !empty($documentMetadataKeyValuePairs["license_text"]) ? $documentMetadataKeyValuePairs["license_text"] : "";
            $basicLicenseModel = [
                "title" => $licenseTitle,
                "description" => $licenseDescription,
                "license_text" => $licenseText,
                
            ];
            if( !empty($documentMetadataKeyValuePairs["license_title_html"])){
                $basicLicenseModel["title_html"] =$documentMetadataKeyValuePairs["license_title_html"];
            }else{
                $basicLicenseModel["title_html"]='';
            }
            if( !empty($documentMetadataKeyValuePairs["license_description_html"])){
                $basicLicenseModel["description_html"] =$documentMetadataKeyValuePairs["license_description_html"];
            }else{
                $basicLicenseModel["description_html"]='';
            }
            if( !empty($documentMetadataKeyValuePairs["license_text_html"])){
                $basicLicenseModel["license_text_html"] =$documentMetadataKeyValuePairs["license_text_html"];
            }else{
                $basicLicenseModel["license_text_html"]='';
            }
        
        $this->setBasicLicenseModel($basicLicenseModel);

        // parse basic language input and set language model
        $language = !empty($documentMetadataKeyValuePairs["language"]) ? strtolower(trim($documentMetadataKeyValuePairs["language"])) : "";
        $basicLanguageModel = !empty($language) ? [ "short_code" => $language ] : [];

        $this->setBasicLanguageModel($basicLanguageModel);
    }

    private function saveAndSetSubject() {
        $subjectToSave = $this->getBasicSubjectModel();
        $subjectToSet = [];
        if(!empty($subjectToSave)) {
            $subjectTitleToSearchFor = $subjectToSave["title"];
            $existingSubjectModel = $this->subjectRepository->findBy("title", $subjectTitleToSearchFor);
            
            $orgCode = $this->getOrgCode();
            // if no subject exists, create one, otherwise set the existing
            if(empty($existingSubjectModel->subject_id)) {
                $genaratedSubjectId = $this->createUniversalUniqueIdentifier();
                $subjectToSave["subject_id"] = $genaratedSubjectId;
                $subjectToSave["source_subject_id"] = $genaratedSubjectId;
                $subjectToSave["organization_id"] = $this->getRequestUserDetails()["organization_id"];
                $subjectToSave["uri"] = $this->getCaseApiUri("CFSubjects", $genaratedSubjectId,true,$orgCode,$this->domainName);
                // save and map new subject with document and set it
                $subjectToSet = $this->subjectRepository->saveData($subjectToSave);
            }
            else {
                $subjectToSet = $this->subjectRepository->edit($existingSubjectModel->subject_id, $subjectToSave);
            }
        }
        $this->setSavedSubject($subjectToSet);
    }

    private function saveAndSetLicense() {
        $licenseToSave = $this->getBasicLicenseModel();
        $licenseToSet = [];
        if(!empty($licenseToSave)) {
            $licenseTitleToSearchFor = $licenseToSave["title"];
            $existingLicenseModel = $this->licenseRepository->findBy("title", $licenseTitleToSearchFor);
            
            $orgCode = $this->getOrgCode();
            // if no license exists, create one, otherwise set the existing
            if(empty($existingLicenseModel->license_id)) {
                $genaratedLicenseId = $this->createUniversalUniqueIdentifier();
                $licenseToSave["license_id"] = $genaratedLicenseId;
                $licenseToSave["organization_id"] = $this->getRequestUserDetails()["organization_id"];
                $licenseToSave["source_license_id"] = $genaratedLicenseId;
                $licenseToSave["uri"] = $this->getCaseApiUri("CFLicenses", $genaratedLicenseId,true,$orgCode,$this->domainName);
                // save and map new license with document and set it
                $licenseToSet = $this->licenseRepository->saveData($licenseToSave);
            }
            else {
                $licenseToSet = $this->licenseRepository->edit($existingLicenseModel->license_id, $licenseToSave);
            }
        }
        $this->setSavedLicense($licenseToSet);
    }

    private function saveAndSetLanguage() {
        $languageToSave = $this->getBasicLanguageModel();
        $languageToSet = [];
        if(!empty($languageToSave)) {
            $languageShortCodeToSearchFor = $languageToSave["short_code"];
            
            $existingLanguageModel = DB::table('languages')
                                ->select('language_id')
                                ->where('is_deleted','0')
                                ->where('short_code',$languageShortCodeToSearchFor)
                                ->where('organization_id',$this->getRequestUserDetails()["organization_id"])
                                ->get()
                                ->toArray();

            // if no language exists, create one, otherwise set the existing
            if(empty($existingLanguageModel[0]->language_id)) {
                $genaratedLanguageId = $this->createUniversalUniqueIdentifier();
                $languageToSave["language_id"] = $genaratedLanguageId;
                $languageToSave["name"] = ucwords($languageToSave["short_code"]);
                $languageToSave["organization_id"] = $this->getRequestUserDetails()["organization_id"];
                // save and map new language with document and set it
                $languageToSet = $this->languageRepository->saveData($languageToSave);
            }
            else {
                $languageToSet = $this->languageRepository->edit($existingLanguageModel[0]->language_id, $languageToSave);
            }
        }
        $this->setSavedLanguage($languageToSet);
    }

    private function updateAdditionalMetadataIfSimilarToCaseMetadata() {
        $existingCustomMetadataId                       =   [];
        $arrayOfMetadataInternalNameForNodeType         =   [];
        $documentMetadataKeyValuePairs                  =   $this->getDocumentMetadataKeyValuePairs();
        $documentId                                     =   $documentMetadataKeyValuePairs["document_id"];
        $document                                       =   $this->documentRepository->getDocumentDataAndAssociatedModels($documentId);
        $documentCustomMetadata                         =   $document->customMetadata()->where(['is_active' => '1', 'is_deleted' => '0'])->get();
        
        foreach($documentCustomMetadata as $existingCustomMetadata) {
            $existingCustomMetadataId[] =   $existingCustomMetadata->metadata_id;
        }

        $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($document->node_type_id);

        foreach($metadataList->metadata as $metadata) {
            $arrayOfMetadataInternalNameForNodeType[]  =   ['internal_name' =>  $metadata->internal_name, 'metadata_id' =>  $metadata->metadata_id];
        }

        $documentDataToUpdate   =   $this->getBasicDocumentModel();
        if($existingCustomMetadataId !== null) {
            foreach($arrayOfMetadataInternalNameForNodeType as $metadataInternalName) {
                $value  =   !empty($documentDataToUpdate[$metadataInternalName['internal_name']]) ? $documentDataToUpdate[$metadataInternalName['internal_name']]: "";
                $value_html  =   !empty($documentDataToUpdate[$metadataInternalName['internal_name'].'_html']) ? $documentDataToUpdate[$metadataInternalName['internal_name'].'_html']: "";
                if(in_array($metadataInternalName['metadata_id'], $existingCustomMetadataId) === true) {
                    $this->documentRepository->addOrUpdateCustomMetadataValue($documentId, $metadataInternalName['metadata_id'], $value,$value_html, '0', 'edit');
                }
            }
        }
    }

    private function saveAndSetCustomMetadata() {
        $existingCustomMetadataId       =   [];
        $customMetadataSet              =   $this->getCustomMetadataKeyValuePairs();
        $documentMetadataKeyValuePairs  =   $this->getDocumentMetadataKeyValuePairs();
        $documentId                     =   $documentMetadataKeyValuePairs["document_id"];
        $document                       =   $this->documentRepository->getDocumentDataAndAssociatedModels($documentId);
        $documentCustomMetadata         =   $document->customMetadata()->where(['is_active' => '1', 'is_deleted' => '0'])->get();
        
        foreach($documentCustomMetadata as $existingCustomMetadata) {
            $existingCustomMetadataId[] =   $existingCustomMetadata->metadata_id;
        }
        foreach($customMetadataSet as $metadata) {
            $metadata['metadata_value'] = !empty($metadata['metadata_value']) ? $metadata['metadata_value'] : "";
            $metadata['metadata_value_html'] = !empty($metadata['metadata_value_html']) ? $metadata['metadata_value_html'] : "";
            if(is_array($metadata['metadata_value_html']))
            {
               $metadata['metadata_value_html'] = "";
            }
            if(in_array($metadata['metadata_id'], $existingCustomMetadataId) === true) {
                $this->documentRepository->addOrUpdateCustomMetadataValue($documentId, $metadata['metadata_id'], $metadata['metadata_value'], $metadata['metadata_value_html'],'0', 'edit');
            }else {
                $this->documentRepository->addOrUpdateCustomMetadataValue($documentId, $metadata['metadata_id'], $metadata['metadata_value'], $metadata['metadata_value_html'],'0', 'insert');
            }
        }
    }

    private function saveAndSetDocument() {
        $orgCode            =   $this->getOrgCode();
        $documentToUpdate   =   $this->getBasicDocumentModel();
        $documentId         =   $documentToUpdate["document_id"];

        $savedLicense       =   $this->getSavedLicense();
       
        if(!empty($savedLicense["license_id"])) {
        $documentToUpdate["license_id"] = !empty($savedLicense["license_id"]) ? $savedLicense["license_id"] : "";    
        $documentToUpdate["source_license_uri_object"] = json_encode(
            [
                "identifier"    =>  $documentToUpdate["license_id"],
                "uri"           =>  $this->getCaseApiUri("CFLicenses", $documentToUpdate["license_id"], $orgCode,$this->domainName),
                "title" => isset($savedLicense["title"])?$savedLicense["title"]:''
            ]
        );
        }
        $savedLanguage = $this->getSavedLanguage();
        $documentToUpdate["language_id"] = !empty($savedLanguage["language_id"]) ? $savedLanguage["language_id"] : "";
        // update the document
        $updatedDocument = $this->documentRepository->edit($documentId, $documentToUpdate);
        // for Lisenses 

        // if subject has been assigned (new or existing) then remove exiting mapping and set new mapping
        // else remove previously created subject mapping as well as the subject itself
        $savedSubject = $this->getSavedSubject();
        if(!empty($savedSubject->subject_id)) {
            $sourceSubjectUriObject = json_encode(
                [
                    "identifier"    =>  $savedSubject->subject_id,
                    "uri"           =>  $this->getCaseApiUri("CFSubjects", $savedSubject->subject_id, $orgCode,$this->domainName),
                    "title"         =>  $savedSubject->title
                ]
            );
            $savedSubject->source_subject_uri_object = $sourceSubjectUriObject;
        }
        
        
        if(!empty($savedSubject)){
            $updatedDocument->subjects()->detach();
            $updatedDocument->subjects()->attach($savedSubject->subject_id);

            DB::table('document_subject')->where('document_id', $documentId)->update(['source_subject_uri_object' => $sourceSubjectUriObject]);
        }
        else {
           // $updatedDocument->subjects()->delete();
          //  $updatedDocument->subjects()->detach();
        }
        DB::table('documents')->where('document_id', $documentId)->update(['import_type' => 2]);
        // remove previously saved license if license is not set
        if(empty($updatedDocument->license_id) && !empty($this->getExistingDocument()->license_id)) {
            $this->licenseRepository->remove($this->getExistingDocument()->license_id);
        }

        // remove previously saved language if language is not set
        // if(empty($updatedDocument->language_id)) {
        //     $this->languageRepository->remove($this->getExistingDocument()->language_id);
        // }

        $this->setSavedDocument($updatedDocument);
    }
}