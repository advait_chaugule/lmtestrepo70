<?php
namespace App\Domains\Document\Jobs;

use App\Data\Repositories\Contracts\AssetRepositoryInterface;
use Lucid\Foundation\Job;

class ReorderAdminDocumentJob extends Job
{
    private $assetRepository;
    private $organizationIdentifier;
    private $requestData;
    private $fileIdentifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( string $organizationIdentifier, array $requestData)
    {
        $this->setOrganizationIdentifier($organizationIdentifier);
        $this->requestData  =   $requestData;
    }
    public function setOrganizationIdentifier($identifier) {
        $this->organizationIdentifier   =   $identifier;
    }

    public function getOrganizationIdentifier()
    {
        return $this->organizationIdentifier;
    }
    public function setFileIdentifier($identifier) {
        $this->fileIdentifier   =   $identifier;
    }

    public function getFileIdentifier() {
        return $this->fileIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AssetRepositoryInterface $assetRepository)
    {
        $this->assetRepository  =   $assetRepository;
        $editFileOrder  =   $this->editFileOrder();
        return $editFileOrder;
    }
    private function editFileOrder()   {
        $requestData = $this->requestData;
        foreach($requestData['file'] as $fileReorder) {
            $columnValueFilterPairs =   ['asset_id'    =>  $fileReorder['asset_id']];
            $attributes             =   ['asset_order'  => $fileReorder['asset_order']];

            $fileDetails    =   $this->assetRepository->editWithCustomizedFields($columnValueFilterPairs, $attributes);
        }

        return $requestData['file'];
    }
}
