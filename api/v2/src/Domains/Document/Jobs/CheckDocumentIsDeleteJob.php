<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class CheckDocumentIsDeleteJob extends Job
{
    private $identifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //set private attribute to input
        $this->identifier = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo)
    {
        //Check whether Role deleted
        $attributes = ['document_id' => $this->identifier, 'is_deleted' => '1'];
        return $documentRepo->findByAttributes($attributes)->isNotEmpty();
    }
}
