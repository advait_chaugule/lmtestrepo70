<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Services\Api\Traits\DateHelpersTrait;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\SubjectRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

class GetDocumentAndRespectiveMetadataValuesJob extends Job
{
    use DateHelpersTrait;

    private $documentRepository;
    private $languageRepository;
    private $licenseRepository;
    private $subjectRepository;
    private $threadRepository;

    private $documentIdentifier;
    private $document;
    private $language;
    private $license;
    private $subject;
    private $customMetadata = [];
    private $documentMetadataValues;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentId)
    {
        $this->setDocumentId($documentId);
    }

    public function setDocumentId(string $data) {
        $this->documentIdentifier = $data;
    }

    public function getDocumentId(): string {
        return $this->documentIdentifier;
    }

    public function setDocument($data) {
        $this->document = $data;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setLanguage($data) {
        $this->language = $data;
    }

    public function getLanguage() {
        return $this->language;
    }

    public function setLicense($data) {
        $this->license = $data;
    }

    public function getLicense() {
        return $this->license;
    }

    public function setSubject($data) {
        $this->subject = $data;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function setCustomMetadata($data) {
        $this->customMetadata = $data;
    }

    public function getCustomMetadata() {
        return $this->customMetadata;
    }

    public function setDocumentMetadataValues($data) {
        $this->documentMetadataValues = $data;
    }

    public function getDocumentMetadataValues() {
        return $this->documentMetadataValues;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        SubjectRepositoryInterface $subjectRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ThreadRepositoryInterface $threadRepository
    )
    {
        // set db repositories
        $this->documentRepository   =   $documentRepository;
        $this->languageRepository   =   $languageRepository;
        $this->licenseRepository    =   $licenseRepository;
        $this->subjectRepository    =   $subjectRepository;
        $this->nodeTypeRepository   =   $nodeTypeRepository;
        $this->threadRepository     =   $threadRepository;

        $this->fetchAndSetDocument();
        $this->fetchAndSetLanguageAssociatedToDocument();
        $this->fetchAndSetLicenseAssociatedToDocument();
        $this->fetchAndSetSubjectAssociatedToDocument();
        $this->fetchAndSetCustomMetadataAssociatedToDocument();


        $this->parseAndSetDocumentMetadataValues();

        return $this->getDocumentMetadataValues();
    }

    private function fetchAndSetDocument() {
        $documentIdentifier = $this->getDocumentId();
        $document = $this->documentRepository->find($documentIdentifier);
        $this->setDocument($document);
    }

    private function fetchAndSetLanguageAssociatedToDocument() {
        $savedDocument = $this->getDocument();
        $languageIdentifier = $savedDocument->language_id ?: "";
        if(!empty($languageIdentifier)) {
            $language = $this->languageRepository->find($languageIdentifier);
            if(!empty($language->language_id)) {
                $this->setLanguage($language);
            }
        }
    }

    private function fetchAndSetLicenseAssociatedToDocument() {
        $savedDocument = $this->getDocument();
        $licenseIdentifier = $savedDocument->license_id ?: "";
        if(!empty($licenseIdentifier)) {
            $license = $this->licenseRepository->find($licenseIdentifier);
            if(!empty($license)) {
                $this->setLicense($license);
            }
        }
    }

    private function fetchAndSetSubjectAssociatedToDocument() {
        $savedDocument = $this->getDocument();
        $subjectsAssociatedToDocument = $savedDocument->subjects;
        if($subjectsAssociatedToDocument->isNotEmpty()){
            $this->setSubject($subjectsAssociatedToDocument->first());
        }
    }

    private function fetchAndSetCustomMetadataAssociatedToDocument() {
        $customMetadataAssociatedToDocumentArray    =   [];
        $savedDocument                      =   $this->getDocument();
        $nodeTypeMetadata                   =   $this->nodeTypeRepository->getNodeTypeMetadata($savedDocument->node_type_id);
        $customMetadataAssociatedToDocument =   $savedDocument->customMetadata()->where(['is_active'  =>  '1', 'is_deleted'    => '0'])->get();

        foreach($customMetadataAssociatedToDocument as $customMetadata) {
            $customMetadataAssociatedToDocumentArray[] = $customMetadata->metadata_id;
        }

        foreach($nodeTypeMetadata->metadata as $metadata) {
            if(sizeOf($customMetadataAssociatedToDocumentArray) > 0) {
                if($metadata->is_custom == 1) {
                    if(in_array($metadata->metadata_id, $customMetadataAssociatedToDocumentArray) === false) {
                        $this->documentRepository->addOrUpdateCustomMetadataValue($savedDocument->document_id, $metadata->metadata_id, '','', '0' ,'insert');
                    }
                }   
            } else {
                if($metadata->is_custom == 1) {
                    $this->documentRepository->addOrUpdateCustomMetadataValue($savedDocument->document_id, $metadata->metadata_id, '','', '0','insert');
                }
            }
        }

        $customMetadataAssociatedToDocument =   $savedDocument->customMetadata()->where(['is_active'  =>  '1', 'is_deleted'    => '0'])->get();
        
        if($customMetadataAssociatedToDocument->isNotEmpty()) {
            $this->setCustomMetadata($customMetadataAssociatedToDocument);
        }        
    }

    private function checkIfCustomMetadataIsAdditionalToDocument() {
        $arrayOfAdditionalMetadataToDocument    =   [];
        $arrayOfNodetypeMetadataId              =   [];

        $savedDocument                  =   $this->getDocument(); 
        $nodeTypeMetadata               =   $this->nodeTypeRepository->getNodeTypeMetadata($savedDocument->node_type_id);

        $customMetadataAssociatedToDocument =   $this->getCustomMetadata();

        foreach($nodeTypeMetadata->metadata as $metadata) {
            $arrayOfNodetypeMetadataId[]  =   $metadata->metadata_id;
        }

        foreach($customMetadataAssociatedToDocument as $customMetadata) {
            if(in_array($customMetadata->metadata_id, $arrayOfNodetypeMetadataId)) {
                $arrayOfAdditionalMetadataToDocument[$customMetadata->metadata_id]  =  0;
            }
            else {
                $arrayOfAdditionalMetadataToDocument[$customMetadata->metadata_id]  =  1;
            }
        }

        return $arrayOfAdditionalMetadataToDocument;
        
    }

    private function countOpenCommentsOnDocument($documentIdentifier) {
        $countOpenComment   =   0;
        $attributeIn        =   'thread_source_id';
        $containedInValues  =   [$documentIdentifier];
        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_id', 'status'];
        $keyValuePairs      =   ['is_deleted'    => '0'];
        $notInKeyValuePairs =   ['status'   => '4'];

        //dd($containedInValues);

        $openComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                $attributeIn, 
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $notInKeyValuePairs )->toArray();

        return count($openComments);
    }

    private function parseAndSetDocumentMetadataValues() {
        $fieldTypeValues = config("default_enum_setting.metadata_field_type");

        $customMetadataKeyValuePairs    =   [];
        $savedDocument                                          = $this->getDocument();
        $savedLanguage                                          = $this->getLanguage();
        $savedLicense                                           = $this->getLicense();
        $savedSubject                                           = $this->getSubject();
        $savedCustomMetadata                                    = $this->getCustomMetadata();
        $arrayForCustomMetadataAsAdditionalMetadataToDocument   =   $this->checkIfCustomMetadataIsAdditionalToDocument();

        
        $documentMetadataKeyValuePairs = $savedDocument->toArray();
        $creator = !empty($documentMetadataKeyValuePairs["creator"]) ? $documentMetadataKeyValuePairs["creator"] : "";
        $adoptionStatus = !empty($documentMetadataKeyValuePairs["adoption_status"]) ? $documentMetadataKeyValuePairs["adoption_status"] : "";
        $status = !empty($documentMetadataKeyValuePairs["status"]) ? $documentMetadataKeyValuePairs["status"] : "";
        $title = !empty($documentMetadataKeyValuePairs["title"]) ? $documentMetadataKeyValuePairs["title"] : "";
        //$titleHtml = !empty($documentMetadataKeyValuePairs["title_html"]) ? $documentMetadataKeyValuePairs["title_html"] : "";
        $officialSourceUrl = !empty($documentMetadataKeyValuePairs["official_source_url"]) ? $documentMetadataKeyValuePairs["official_source_url"] : "";
        $publisher = !empty($documentMetadataKeyValuePairs["publisher"]) ? $documentMetadataKeyValuePairs["publisher"] : "";
        //$publisherHtml = !empty($documentMetadataKeyValuePairs["publisher_html"]) ? $documentMetadataKeyValuePairs["publisher_html"] : "";
        $description = !empty($documentMetadataKeyValuePairs["description"]) ? $documentMetadataKeyValuePairs["description"] : ""; 
        //$descriptionHtml = !empty($documentMetadataKeyValuePairs["description_html"]) ? $documentMetadataKeyValuePairs["description_html"] : ""; 
        $version = !empty($documentMetadataKeyValuePairs["version"]) ? $documentMetadataKeyValuePairs["version"] : "";
        $statusStartDate = ( !empty($documentMetadataKeyValuePairs["status_start_date"])  && 
                            $documentMetadataKeyValuePairs["status_start_date"] !== "0000-00-00 00:00:00") ?
                            $this->formatDateTimeToDateOnly($documentMetadataKeyValuePairs["status_start_date"]) : 
                            "";
        $statusEndDate = ( !empty($documentMetadataKeyValuePairs["status_end_date"]) && 
                         $documentMetadataKeyValuePairs["status_end_date"] !== "0000-00-00 00:00:00" ) ? 
                         $this->formatDateTimeToDateOnly($documentMetadataKeyValuePairs["status_end_date"]) : 
                         "";
        $notes = !empty($documentMetadataKeyValuePairs["notes"]) ? $documentMetadataKeyValuePairs["notes"] : "";
        //$notesHtml = !empty($documentMetadataKeyValuePairs["notes_html"]) ? $documentMetadataKeyValuePairs["notes_html"] : "";

        $language = !empty($savedLanguage) ? $savedLanguage->short_code : "";

        $licenseMetadataKeyValuePairs = !empty($savedLicense) ? $savedLicense->toArray() : [];
        $licenseTitle = !empty($licenseMetadataKeyValuePairs["title"]) ? $licenseMetadataKeyValuePairs["title"] : "";
        //$licenseTitleHtml = !empty($licenseMetadataKeyValuePairs["title_html"]) ? $licenseMetadataKeyValuePairs["title_html"] : "";
        $licenseDescription = !empty($licenseMetadataKeyValuePairs["description"]) ? $licenseMetadataKeyValuePairs["description"] : "";
        //$licenseDescriptionHtml = !empty($licenseMetadataKeyValuePairs["description_html"]) ? $licenseMetadataKeyValuePairs["description_html"] : "";
        $licenseText = !empty($licenseMetadataKeyValuePairs["license_text"]) ? $licenseMetadataKeyValuePairs["license_text"] : "";
        //$licenseTextHtml = !empty($licenseMetadataKeyValuePairs["license_text_html"]) ? $licenseMetadataKeyValuePairs["license_text_html"] : "";

        $subjectMetadataKeyValuePairs = !empty($savedSubject) ? $savedSubject->toArray() : [];
        $subjectTitle = !empty($subjectMetadataKeyValuePairs["title"]) ? $subjectMetadataKeyValuePairs["title"] : "";
        //$subjectTitleHtml = !empty($subjectMetadataKeyValuePairs["title_html"]) ? $subjectMetadataKeyValuePairs["title_html"] : "";
        $subjectHierarchyCode = !empty($subjectMetadataKeyValuePairs["hierarchy_code"]) ? $subjectMetadataKeyValuePairs["hierarchy_code"] : "";
        $subjectDescription = !empty($subjectMetadataKeyValuePairs["description"]) ? $subjectMetadataKeyValuePairs["description"] : "";
        //$subjectDescriptionHtml = !empty($subjectMetadataKeyValuePairs["description_html"]) ? $subjectMetadataKeyValuePairs["description_html"] : "";

       
        
        foreach($savedCustomMetadata as $customMetadata)    {
            $fieldPossibleValues = !empty($customMetadata->field_possible_values) ? explode(',' , $customMetadata->field_possible_values) : '';


            $customMetadataKeyValuePairs[]  = [
                'metadata_id'               =>  $customMetadata->pivot->metadata_id,
                'title'                     =>  $customMetadata->name,
                'field_type'                =>  $customMetadata->field_type,
                'field_possible_values'     =>  $fieldPossibleValues,  
                'metadata_value'            =>  $customMetadata->pivot->metadata_value,
                'metadata_value_html'       =>  $customMetadata->pivot->metadata_value_html,
                'is_additional_metadata'    =>  $arrayForCustomMetadataAsAdditionalMetadataToDocument[$customMetadata->pivot->metadata_id]
            ];         
        }

       $documentIdentifier = $this->getDocumentId();
        $documentMetadataValues = [
            "item_id" => $documentIdentifier,
            "source_document_id" => $documentMetadataKeyValuePairs['source_document_id'],
            "document_type"   => $documentMetadataKeyValuePairs['document_type'],
            "creator" => $creator,
            'adoption_status'=>$adoptionStatus,
            'status'=>$status,
            "title" => $title,
            //"title_html" => $titleHtml,
            "official_source_url" => $officialSourceUrl,
            "publisher" => $publisher,
           // "publisher_html" => $publisherHtml,
            "description" => $description,
            //"description_html" => $descriptionHtml,
            "subject_title" => $subjectTitle,
            //"subject_title_html" => $subjectTitleHtml,
            "subject_hierarchy_code" => $subjectHierarchyCode,
            "subject_description" => $subjectDescription,
            //"subject_description_html" => $subjectDescriptionHtml,
            "language" => !empty($language) ? $language : "",
            "display_text"  =>  !empty($savedLanguage) ? ucwords($savedLanguage->name).' ('.$savedLanguage->short_code.')' : "",
            "version" => $version,
            "status_start_date" => $statusStartDate,
            "status_end_date" => $statusEndDate,
            "license_title" => $licenseTitle,
            //"license_title_html" => $licenseTitleHtml,
            "license_description" => $licenseDescription,
            //"license_description_html" => $licenseDescriptionHtml,
            "license_text" => $licenseText,
            //"license_text_html" => $licenseTextHtml,
            "notes" => $notes,
            //"notes_html" => $notesHtml,
            "node_type_id" => $documentMetadataKeyValuePairs['node_type_id'],
            "node_type"     => $savedDocument->nodeType->title,
            "custom_metadata"   =>  $customMetadataKeyValuePairs,
            "open_comments_count"       =>  $this->countOpenCommentsOnDocument($documentMetadataKeyValuePairs['document_id'])

        ];
        if( !empty($documentMetadataKeyValuePairs["publisher_html"])){
            $documentMetadataValues["publisher_html"] =$documentMetadataKeyValuePairs["publisher_html"];
        }
        if( !empty($documentMetadataKeyValuePairs["title_html"])){
            $documentMetadataValues["title_html"] =$documentMetadataKeyValuePairs["title_html"];
        }
        if( !empty($documentMetadataKeyValuePairs["notes_html"])){
            $documentMetadataValues["notes_html"] =$documentMetadataKeyValuePairs["notes_html"];
        }
        if( !empty($documentMetadataKeyValuePairs["description_html"])){
            $documentMetadataValues["description_html"] =$documentMetadataKeyValuePairs["description_html"];
        }
        if( !empty($subjectMetadataKeyValuePairs["title_html"])){
            $documentMetadataValues["subject_title_html"] =$subjectMetadataKeyValuePairs["title_html"];
        }
        if( !empty($subjectMetadataKeyValuePairs["description_html"])){
            $documentMetadataValues["subject_description_html"] =$subjectMetadataKeyValuePairs["description_html"];
        }
        if( !empty($licenseMetadataKeyValuePairs["title_html"])){
            $documentMetadataValues["license_title_html"] =$licenseMetadataKeyValuePairs["title_html"];
        }
        if( !empty($licenseMetadataKeyValuePairs["description_html"])){
            $documentMetadataValues["license_description_html"] =$licenseMetadataKeyValuePairs["description_html"];
        }
        if( !empty($licenseMetadataKeyValuePairs["license_text_html"])){
            $documentMetadataValues["license_text_html"] =$licenseMetadataKeyValuePairs["license_text_html"];
        }
      
        $this->setDocumentMetadataValues($documentMetadataValues);
    }
}
