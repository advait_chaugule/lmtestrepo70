<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ValidateDocumentV3Job extends Job
{
    private $documentIdentifier;
    private $requestUserDetails;
    private $documentRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, array $requestUserDetails)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->requestUserDetails = $requestUserDetails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        $this->documentRepository = $documentRepository;

         $documentIdentifier =   $this->documentIdentifier;
         $input              =   $this->requestUserDetails;
        
         $organizationId     =   $input["organization_id"];
         $returnCollection           =   false;
         $fieldsToReturn             =   ["document_id", "title", "node_type_id", "project_id", "node_type_id", "adoption_status"];
        $conditionalKeyValuePair    =   [ "document_id" =>  $documentIdentifier, "organization_id" => $organizationId ];

        // $document = $this->documentRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalKeyValuePair);
        $document =DB::table('documents')->where('document_id' , $documentIdentifier)->where('organization_id' , $organizationId )->where('is_deleted','0')->get();
        if(!empty($document))
        {
            return true;
        }
        else
        {
            return "Document ID is not exist";
        }
    }
}
