<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class UpdateDocumentJob extends Job
{
    private $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepo)
    {
        $documentId = $this->input['document_id'];
        $this->input['import_type'] = 2;
        // remove id from the input array since it has been assigned to $id variable
        unset($this->input['document_id']);
        unset($this->input['auth_user']);
        
        return $documentRepo->edit($documentId, $this->input);
    }
}
