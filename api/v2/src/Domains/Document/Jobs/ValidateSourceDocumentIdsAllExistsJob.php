<?php
namespace App\Domains\Document\Jobs;
use DB;
use Lucid\Foundation\Job;

class ValidateSourceDocumentIdsAllExistsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $sourceDocumentIdentifier, string $organizationId)
    {
        $this->sourceDocumentIdentifier = $sourceDocumentIdentifier;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $documentData = DB::table('documents')
                        ->select('organization_id', 'document_id', 'source_document_id', 'updated_at')
                        ->whereIn('source_document_id',$this->sourceDocumentIdentifier)
                        ->where("organization_id",$this->organizationId)
                        ->where("is_deleted",'0')
                        ->get()->toArray();
        $invalidDocuments = array_diff($this->sourceDocumentIdentifier,array_column($documentData,'source_document_id'));
        return empty($invalidDocuments) ? ['status'=> 'true', 'data' => $documentData] : ['status'=> 'false', 'data' => $invalidDocuments];
    }
}
