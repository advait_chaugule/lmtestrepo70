<?php
namespace App\Domains\Document\Jobs;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

use Lucid\Foundation\Job;

class ValidateDocumentSourceIdJob extends Job
{
    private $documentIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */    public function __construct(string $documentIdentifier, string $organizationId)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $validator)
    {
        $dataToValidate = [ "source_document_id" => $this->documentIdentifier,  "organization_id" => $this->organizationId, "is_deleted" => '0'];
        $document = $validator->findByAttributes($dataToValidate);
    
        return $document->isNotEmpty() ? true : false;
    }
}
