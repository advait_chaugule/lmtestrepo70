<?php
namespace App\Domains\Document\Jobs;

use App\Domains\Document\Validators\DocumentExistsValidator as Validator;

use Lucid\Foundation\Job;

class ValidateDocumentJob extends Job
{
    private $documentIdentifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {
        $this->documentIdentifier = $documentIdentifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $dataToValidate = [ "document_id" => $this->documentIdentifier ];
        $validation = $validator->validate($dataToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
