<?php
namespace App\Domains\Document\Jobs;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

use Lucid\Foundation\Job;

class ValidateDocumentForPublishJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier, string $organizationId)
    {
        $this->documentIdentifier = $documentIdentifier;
        $this->organizationId = $organizationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository)
    {
        $dataToValidate = [ "document_id" => $this->documentIdentifier,  "organization_id" => $this->organizationId, "is_deleted" => '0', "status" => '3'];
        $document       = $documentRepository->findByAttributes($dataToValidate);
        return $document->isNotEmpty();
    }
}
