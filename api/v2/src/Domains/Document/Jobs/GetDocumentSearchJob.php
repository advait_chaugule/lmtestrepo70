<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ArrayHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;

class GetDocumentSearchJob extends Job
{    
    use ArrayHelper;

    private $documentRepository;
    private $projectRepository;

    private $input;

    private $documentsFetched;
    private $documentIds;
    private $projectsAssociatedWithDocuments;

    private $parsedDocumentList;

    private $requestDataFilters;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function setRequestFilters(array $data) {
        $this->requestDataFilters = $data;
    }
    public function getRequestFilters(): array {
        return $this->requestDataFilters;
    }

    public function __construct($input)
    {
        //
        $this->params = $input;
    }

    public function setDocumentsFetched($data) {
        $this->documentsFetched = $data;
    }

    public function getDocumentsFetched() {
        return $this->documentsFetched;
    }

    public function setDocumentIds(array $data){
        $this->documentIds = $data;
    }

    public function getDocumentIds(): array{
        return $this->documentIds;
    }

    public function setProjectsAssociatedWithDocuments($data){
        $this->projectsAssociatedWithDocuments = $data;
    }

    public function getProjectsAssociatedWithDocuments(){
        return $this->projectsAssociatedWithDocuments;
    }

    public function setParsedDocumentList(array $data) {
        $this->parsedDocumentList = $data;
    }

    public function getParsedDocumentList(): array {
        return $this->parsedDocumentList;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepo,
        ProjectRepositoryInterface $projectRepo,
        Request $request
    )
    {
        $this->setRequestFilters($request->all());
        // set the database repository
        $this->documentRepository = $documentRepo;
        $this->projectRepository = $projectRepo;

        // search documents and set them
        $this->searchFoDocumentsAndSetThem();

        // extract the document ids from documents fetched from db
        $this->extractAndSetDocumentIds();

        // fetch and set projects associated to documents
        $this->fethAndSetProjectsAssociatedToDocuments();

        $this->addProjectCountForEachDocumentAndPrepareDocumentList();

        return $this->getParsedDocumentList();
    }

    private function searchFoDocumentsAndSetThem() {
        // set the filters and their respective statuses to default
       $paginationFilters = $sortfilters = $searchFilters = [];
       $returnPaginatedData = $returnSortedData = false;
       $input = $this->getRequestFilters();
      
       $attributes = ['is_deleted' => '0', 'organization_id' => $input['auth_user']['organization_id']];
       
       // check and set parameters if pagination and sorting parameters are present
       if(!empty($this->params)){
           $returnPaginatedData = (array_key_exists('limit', $this->params) && array_key_exists('offset', $this->params)) ? true : false;
           $returnSortedData = (array_key_exists('orderBy', $this->params) && array_key_exists('sorting', $this->params)) ? true : false;
           $returnSearchData = (array_key_exists('search_key', $this->params)) ? true : false;
           // assign the input param array to both the filters
           $paginationFilters = $sortfilters = $searchFilters = $this->params;
       }
       
       // fetch and return the data
       $documents = $this->documentRepository->filterDocumentByAttributes(
                        $attributes, 
                        $returnPaginatedData, 
                        $paginationFilters, 
                        $returnSortedData, 
                        $sortfilters, 
                        $returnSearchData, 
                        'title', $searchFilters
                    );
        $this->setDocumentsFetched($documents["taxonomies"]);
    }

    private function extractAndSetDocumentIds() {
        $documentsFetched = $this->getDocumentsFetched();
        $documentIds = $this->createArrayFromCommaeSeparatedString($documentsFetched->implode('document_id', ','));
        $this->setDocumentIds($documentIds);
    }

    private function fethAndSetProjectsAssociatedToDocuments() {
        $attributeIn = 'document_id';
        $containedInValues = $this->getDocumentIds();
        $returnCollection = true;
        $fieldsToReturn = ['project_id', 'document_id'];
        $data = $this->projectRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
                    $attributeIn, 
                    $containedInValues, 
                    $returnCollection, 
                    $fieldsToReturn
                );
        $this->setProjectsAssociatedWithDocuments($data);
    }

    private function addProjectCountForEachDocumentAndPrepareDocumentList() {
        $documentsFetched = $this->getDocumentsFetched();
        $parsedDocuments = [];
        foreach($documentsFetched as $document) {
            $documentId = $document->document_id;
            $parsedDocuments[] = [
                "document_id" => $documentId,
                "title" => !empty($document->title) ? $document->title : "",
                "created_at" => $document->created_at->toDateTimeString(),
                "associated_projects_count" => $this->countProjectsAssociatedToDocument($documentId),
                // set static value for now
                "last_active_date" => "2018-04-10"
            ];
        }
        $this->setParsedDocumentList($parsedDocuments);
    }

    private function countProjectsAssociatedToDocument(string $documentId): int {
        $projectsAssociatedToDocuments = $this->getProjectsAssociatedWithDocuments();
        return $projectsAssociatedToDocuments->filter(function ($item, $key) use($documentId) {
                    if($item->document_id===$documentId) {
                        return $item;
                    }
                })->count();
    }
}
