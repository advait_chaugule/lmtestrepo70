<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class DeleteDocumentJob extends Job
{
    private $identifier;
    private $documentRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //Set Role Identifier
        $this->setIdentifier($input);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $documentRepository, ItemRepositoryInterface $itemRepository, ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        //Set Role Repo handler
        $this->documentRepository           =   $documentRepository;
        $this->itemRepository               =   $itemRepository;
        $this->itemAssociationRepository    =   $itemAssociationRepository;

            //Delete all item association's metadata for the document
            $this->deleteItemAssociationMetadata();
            
            //Delete all item association for the document
            $this->deleteItemAssociation();
            
            //Delete all item for the document
            $this->deleteAllItems();

            //Finally delete the document
            $this->deleteDocument();

        
    }

    //Public Getter and Setter methods
    public function setIdentifier($identifier){
        $this->identifier = $identifier;
    }

    public function getIdentifier(){
        return $this->identifier;
    }

    private function deleteItemAssociationMetadata(){
        $documentIdentifier =   $this->getIdentifier();
        $itemAssociationList = DB::table('item_association_metadata as aiam')
            ->join('item_associations as aia', 'aiam.item_association_id', '=', 'aia.item_association_id')
            ->where('aia.document_id',$documentIdentifier)
            ->where('aia.is_deleted',0)
            ->update([ 'aiam.is_deleted' => 1 ]);
    }

    private function deleteItemAssociation() {
        $documentIdentifier =   $this->getIdentifier();

        $columnValueFilterPairs     =   ['document_id'  =>  $documentIdentifier];
        $attributes                 =   ['is_deleted'   =>  '1'];
        //$deleteItemAssociation      =    $this->itemAssociationRepository->delete($columnValueFilterPairs);
        $deleteItemAssociation      =    $this->itemAssociationRepository->editWithCustomizedFields($columnValueFilterPairs,  $attributes);
        
    }

    private function deleteAllItems() {
        $documentIdentifier =   $this->getIdentifier();

        $columnValueFilterPairs =   ['document_id'  =>  $documentIdentifier];
        $attributes             =   ['is_deleted'   =>  '1'];
        //$deleteItem    =    $this->itemRepository->delete($columnValueFilterPairs);
        $deleteItem      =    $this->itemRepository->editWithCustomizedFields($columnValueFilterPairs,  $attributes);
    }

    private function deleteDocument() {
        $documentIdentifier =   $this->getIdentifier();

        $columnValueFilterPairs =   ['document_id'  =>  $documentIdentifier];
        $attributes             =   ['is_deleted'   =>  '1'];
        //$deleteDocument     =    $this->documentRepository->delete($columnValueFilterPairs);
        $deleteDocument      =    $this->documentRepository->editWithCustomizedFields($columnValueFilterPairs,  $attributes);
        return $deleteDocument;
    }
}
