<?php
namespace App\Domains\Document\Jobs;
use App\Data\Models\Document;
use App\Data\Models\TaxonomyPubStateHistory;
use App\Data\Models\User;
use Lucid\Foundation\Job;

class GetPublishHistoryListJob extends Job
{
    private $documentId;
    private $organizationId;
    private $userId;

    public function __construct($documentId,$organizationId,$userId)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
        $this->userId = $userId;
    }

    public function handle()
    {
        $publish = TaxonomyPubStateHistory::select('publish_number','publish_date','unpublish_date','published_by','unpublished_by','taxonomy_json_file_s3','tag')
                    ->where('document_id',$this->documentId)
                    ->where('organization_id',$this->organizationId)
                    ->where('is_deleted',0)
                    ->get()
                    ->toArray();

        $publishUserIds   = array_column($publish,'published_by');
        $unPublishUserIds = array_column($publish,'unpublished_by');

        $publishBy = User::select('user_id','first_name','last_name')
              ->whereIn('user_id',$publishUserIds)
              ->where('is_deleted',0)
              ->where('is_active',1)
              ->get()
              ->toArray();

        $unPublishBy = User::select('user_id','first_name','last_name')
            ->whereIn('user_id',$unPublishUserIds)
            ->where('is_deleted',0)
            ->where('is_active',1)
            ->get()
            ->toArray();

       $historyData =[];
       // map publish data starts here
       foreach ($publish as $publishK=>$publishV)
       {
           foreach ($publishBy as $publishByK=>$publishByV)
           {
                   $PubFullName   =  $publishByV['first_name'].' '.$publishByV['last_name'];
                   if($publishV['published_by']==$publishByV['user_id'])
                   {

                       $historyData[] =    ['publish_number'  =>  $publishV['publish_number'],
                                           'publish_date'   =>  $publishV['publish_date'],
                                           'unpublish_date' =>  $publishV['unpublish_date'],
                                           'published_by'   =>  $PubFullName,
                                           'unpublished_by' =>  $publishV['unpublished_by'],
                                           'taxonomy_json_file_s3' =>  $publishV['taxonomy_json_file_s3'],
                                           'tag'                   =>   $publishV['tag'],           //tag to store version
                                           ];
                   }

           }
       }
       // map publish data ends here

       // map unpublished user data start here
        $userHistoryData = [];

       foreach ($historyData as $historyDataK=>$historyDataV)
       {
           if($historyDataV['unpublished_by']=='')
           {
               $userHistoryData[] = $historyDataV;
               break;
           }
           foreach ($unPublishBy as $unPublishByK=> $unPublishByV)
           {
               if($historyDataV['unpublished_by']==$unPublishByV['user_id']) {
                   $unPubFullName =  $unPublishByV['first_name'].' '.$unPublishByV['last_name'];
                   $historyDataV['unpublished_by']= $unPubFullName;
                   $userHistoryData[] = $historyDataV;
               }

           }
       }
        // map unpublished user data end

       return $userHistoryData;
    }
}