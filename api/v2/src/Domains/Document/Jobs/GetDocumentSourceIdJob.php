<?php
namespace App\Domains\Document\Jobs;
use App\Data\Models\Document;
use Lucid\Foundation\Job;

class GetDocumentSourceIdJob extends Job
{
    private $documentId;
    private $organizationId;

    public function __construct($documentId,$organizationId)
    {
       $this->documentId     = $documentId;
       $this->organizationId = $organizationId;
    }

    public function handle()
    {
        $documentSource = Document::select('source_document_id')
                  ->where('document_id',$this->documentId)
                  ->where('organization_id',$this->organizationId)
                  ->where('is_deleted',0)
                  ->first();
        return $documentSource;


    }
}