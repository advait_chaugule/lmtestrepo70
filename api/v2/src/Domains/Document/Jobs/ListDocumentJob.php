<?php
namespace App\Domains\Document\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Services\Api\Traits\ArrayHelper;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;

class ListDocumentJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $input)
    {
        
        $this->identifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DocumentRepositoryInterface $DocumentRepository)
    {
        $this->DocumentRepository = $DocumentRepository;
        $attributes = ['organization_id' => $this->identifier, 'is_deleted'=>0];
        $result=  $this->DocumentRepository->findByAttributes($attributes);       
        return  $result->toArray();
            
    }
}
