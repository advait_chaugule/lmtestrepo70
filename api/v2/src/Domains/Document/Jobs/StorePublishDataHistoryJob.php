<?php
namespace App\Domains\Document\Jobs;
use App\Data\Models\Document;
use App\Data\Models\TaxonomyPubStateHistory;
use Lucid\Foundation\Job;
use App\Services\Api\Traits\UuidHelperTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class StorePublishDataHistoryJob extends Job
{
    use UuidHelperTrait;
    private $documentId;
    private $organizationId;
    private $userId;
    private $publishDate;
    private $publishStatus;
    private $jsonFile;
    public function __construct($documentId,$userId,$organizationId,$publishStatus,$uuid,$versionTag)
    {
        $this->documentId     = $documentId;
        $this->organizationId = $organizationId;
        $this->userId         = $userId;
        $this->publishStatus  = $publishStatus;
        $this->publishDate    = date("Y-m-d H:i:s");
        $this->jsonFile       = $uuid;
        $this->versionTag     = $versionTag;        //tag to store version
    }

    public function handle(){
        $this->prepareDataToStore($this->documentId,$this->organizationId,$this->userId,$this->publishDate,$this->publishStatus,$this->versionTag);
    }

    public function prepareDataToStore($documentId,$organizationId,$userId,$publishDate,$publishStatus,$versionTag)
    {
        $publishNum =0;
        if($publishStatus==1) {
            $publishNum = TaxonomyPubStateHistory::
                  where('document_id',$documentId)
                ->where('organization_id',$organizationId)
                ->where('is_deleted',0)
                ->max('publish_number');
            $publishNum = $publishNum+1;
        }

        $publishDate = date("Y-m-d H:i:s");

            if($publishStatus==1)
            {
                $publishData = [
                    'document_id'     => $documentId,
                    'publish_number'  => $publishNum,
                    'publish_date'    => $publishDate,
                    'unpublish_date'  => '',
                    'published_by'    => $userId,
                    'unpublished_by'  => '',
                    'organization_id' => $organizationId,
                    'is_deleted'      => 0,
                    'created_by'      => $userId,
                    'updated_by'      => '',
                    'taxonomy_json_file_s3' => $this->jsonFile.'.json',
                    'custom_json_file_s3' => $this->jsonFile.'_custom.json',
                    'tag'             => $versionTag,

                ];

                TaxonomyPubStateHistory::create($publishData);
            }

            $unPublishDate = date("Y-m-d H:i:s");

            if($publishStatus==2)
            {
                $updateTaxo = ['unpublish_date'=>$unPublishDate,
                               'unpublished_by'=>$userId,
                                'updated_by'=>$userId];
                                
                DB::table('taxonomy_pubstate_history')
                                 ->where('document_id',$documentId)
                                 ->where('organization_id',$organizationId)
                                 ->where('is_deleted',0)
                                 ->orderBy('publish_number','DESC')
                                 ->limit(1)
                                 ->update($updateTaxo);
            }

    }
}