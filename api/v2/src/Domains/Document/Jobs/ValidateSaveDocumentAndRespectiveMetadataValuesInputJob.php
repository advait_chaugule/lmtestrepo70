<?php
namespace App\Domains\Document\Jobs;

use App\Domains\Document\Validators\SaveDocumentAndRespectiveMetadataValuesInputValidator as Validator;

use Lucid\Foundation\Job;

class ValidateSaveDocumentAndRespectiveMetadataValuesInputJob extends Job
{

    private $dataToValidate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $dataToValidate)
    {
        $this->dataToValidate = $dataToValidate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Validator $validator)
    {
        $validation = $validator->validate($this->dataToValidate);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
