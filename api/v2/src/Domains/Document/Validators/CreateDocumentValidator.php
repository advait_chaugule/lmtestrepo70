<?php
namespace App\Domains\Document\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CreateDocumentValidator extends BaseValidator {

    protected $rules = [
        'title' => 'required|max:100',
        // 'creator' => 'required|max:25',
        // 'version' => 'max:10',
        // 'description' => 'max:1000',
        // 'note'      => 'max:1000',
        // 'language_id' => 'string|exists:languages,language_id',
        // 'adoption_status' => 'in:1,2,3,4',
        // 'status_start' => "date_format:Y-m-d",
        // 'status_end' => "date_format:Y-m-d"
    ];

     protected $messages = [
        'required'  =>  ":attribute is required.",
        // 'boolean' => ':attribute should be boolean.',
        // 'url' => ':attribute should be a url.',
        // 'string' => ':attribute should be string',
        // 'max:5' => 'Max characters should be 5'
     ];
    
}