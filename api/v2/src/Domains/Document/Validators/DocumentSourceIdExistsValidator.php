<?php
namespace App\Domains\Document\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class DocumentSourceIdExistsValidator extends BaseValidator {

    protected $rules = [
        'source_document_id' => 'required|exists:documents',
        'organization_id' => 'required|exists:documents'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
     ];
    
}