<?php
namespace App\Domains\Document\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class SaveDocumentAndRespectiveMetadataValuesInputValidator extends BaseValidator {

    protected $rules = [
        'title' => 'required',
        'official_source_url' => 'nullable',
        'status_start_date' => "nullable|date_format:Y-m-d",
        'status_end_date' => "nullable|date_format:Y-m-d"
    ];

     protected $messages = [
        'required'  =>  ":attribute is required.",
        // 'boolean' => ':attribute should be boolean.',
        // 'url' => ':attribute should be a url.',
        // 'string' => ':attribute should be string',
        // 'max:5' => 'Max characters should be 5'
     ];
    
}