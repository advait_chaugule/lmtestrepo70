<?php
namespace App\Domains\Document\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class DocumentExistsValidator extends BaseValidator {

    protected $rules = [
        'document_id' => 'required|exists:documents'
    ];

    protected $messages = [
        "required" => ":attribute is required.",
    ];

}