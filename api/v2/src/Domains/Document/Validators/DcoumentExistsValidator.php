<?php
namespace App\Domains\Document\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class DcoumentExistsValidator extends BaseValidator {

    protected $rules = [
        'document_id' => 'required|exists:documents'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];
    
}