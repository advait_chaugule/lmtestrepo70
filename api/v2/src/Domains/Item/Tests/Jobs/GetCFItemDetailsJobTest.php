<?php
namespace App\Domains\Item\Tests\Jobs;

use App\Domains\Item\Jobs\GetCFItemDetailsJob;
use Tests\TestCase;

class GetCFItemDetailsJobTest extends TestCase
{
    public function test_RequestUserOrganizationId()
    {
        $assets = new GetCFItemDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','56b543bf-2839-4b97-a83e-e291c89f2374');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setRequestUserOrganizationId($value);
        $this->assertEquals($value,$assets->getRequestUserOrganizationId());
    }

    public function test_ItemIdentifier()
    {
        $assets = new GetCFItemDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','56b543bf-2839-4b97-a83e-e291c89f2374');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }

    public function test_ItemDetailsFetchedFromDatabase()
    {
        $assets = new GetCFItemDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','56b543bf-2839-4b97-a83e-e291c89f2374');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemDetailsFetchedFromDatabase($value);
        $this->assertEquals($value,$assets->getItemDetailsFetchedFromDatabase());
    }

    public function test_CustomMetadata()
    {
        $assets = new GetCFItemDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','56b543bf-2839-4b97-a83e-e291c89f2374');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setCustomMetadata($value);
        $this->assertEquals($value,$assets->getCustomMetadata());
    }

    public function test_ParsedDataToReturn()
    {
        $assets = new GetCFItemDetailsJob('c4424f55-8900-446c-9c41-2f2c25d58c08','56b543bf-2839-4b97-a83e-e291c89f2374');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setParsedDataToReturn($value);
        $this->assertEquals($value,$assets->getParsedDataToReturn());
    }
}
