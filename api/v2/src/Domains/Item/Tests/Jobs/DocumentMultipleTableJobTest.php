<?php
namespace App\Domains\Item\Tests\Jobs;

use App\Domains\Item\Jobs\DocumentMultipleTableJob;
use Tests\TestCase;

class DocumentMultipleTableJobTest extends TestCase
{
    public function test_DocumentIdentifier()
    {
        $assets = new DocumentMultipleTableJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setDocumentIdentifier($value);
        $this->assertEquals($value,$assets->getDocumentIdentifier());
    }
}
