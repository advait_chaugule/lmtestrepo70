<?php
namespace App\Domains\Item\Tests\Jobs;

use App\Domains\Item\Jobs\UpdateCFItemMultipleTablesJob;
use Tests\TestCase;

class UpdateCFItemMultipleTablesJobTest extends TestCase
{
    public function test_ItemIdentifier()
    {
        $assets = new UpdateCFItemMultipleTablesJob('c4424f55-8900-446c-9c41-2f2c25d58c08');
        $value = 'a36a8ac7-4c8b-470a-b293-71ec7606c6a0';
        $assets->setItemIdentifier($value);
        $this->assertEquals($value,$assets->getItemIdentifier());
    }
}
