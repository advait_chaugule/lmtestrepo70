<?php

namespace App\Domains\Item\Listeners;

use App\Domains\Item\Events\UpdateItemInsideProjectEvent as Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

// parent class which actually changes the database connection to Reporting DB during runtime
use App\Foundation\ReportListenerHandler;
// database repository interface
use App\Data\Repositories\Contracts\ActivityLogRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
// custom helpers
use App\Services\Api\Traits\UuidHelperTrait;

class UpdateItemInsideProjectEventListener extends ReportListenerHandler implements ShouldQueue
{
    use InteractsWithQueue, UuidHelperTrait;

    public $eventType = "ITEM_EDITED_UNDER_PROJECT";
    public $subEventType = "ITEM_EDITED_UNDER_PROJECT";

    private $activityName;
    private $activitySubtypeActivityName;

    private $activityLogRepository;
    private $projectRepository;

    private $activityLogId;

    private $requestUserDetails;
    private $beforeEventRawData;
    private $afterEventRawData;

    private $beforeEventParsedData;
    private $afterEventParsedData;
    private $projectAssociated;
    private $activityLogData;

    private $beforeEventJson;
    private $afterEventJson;

    private $s3UploadedStatus;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        ActivityLogRepositoryInterface $activityLogRepository,
        ProjectRepositoryInterface $projectRepository
    )
    {
        // call the parent class constructor to change database configuration to reporting database
        parent::__construct();

        // set the database repositories
        $this->activityLogRepository = $activityLogRepository;
        $this->projectRepository = $projectRepository;

        $this->setActivityLogId($this->createUniversalUniqueIdentifier());
    }

    public function setActivityLogId(string $data) {
        $this->activityLogId = $data;
    }

    public function getActivityLogId(): string {
        return $this->activityLogId;
    }

    public function setRequestUserDetails(array $data) {
        $this->requestUserDetails = $data;
    }

    public function getRequestUserDetails(): array {
        return $this->requestUserDetails;
    }

    public function setBeforeEventRawData($data) {
        $this->beforeEventRawData = $data;
    }

    public function getBeforeEventRawData() {
        return $this->beforeEventRawData;
    }

    public function setAfterEventRawData($data) {
        $this->afterEventRawData = $data;
    }

    public function getAfterEventRawData() {
        return $this->afterEventRawData;
    }

    public function setBeforeEventParsedData(array $data) {
        $this->beforeEventParsedData = $data;
    }

    public function getBeforeEventParsedData(): array {
        return $this->beforeEventParsedData;
    }

    public function setAfterEventParsedData(array $data) {
        $this->afterEventParsedData = $data;
    }

    public function getAfterEventParsedData(): array {
        return $this->afterEventParsedData;
    }

    public function setProjectAssociated() {
        $afterEventRawData = $this->getAfterEventRawData();
        $projectId = $afterEventRawData->project_id;
        $returnCollection = false;
        $fieldsToReturn = [ "project_id", "project_name" ];
        $conditionalAttributePair = [ "project_id" => $projectId ];
        $project = $this->projectRepository->findByAttributesWithSpecifiedFields($returnCollection, $fieldsToReturn, $conditionalAttributePair);
        $this->projectAssociated = $project;
    }

    public function getProjectAssociated() {
        return $this->projectAssociated;
    }

    public function setActivityName() {
        $afterEventData = $this->getAfterEventParsedData();
        $activityName = $afterEventData["human_coding_scheme"] ?: $afterEventData["list_enumeration"];
        $this->activityName = $activityName;
    }

    public function getActivityName(): string {
        return $this->activityName;
    }

    public function setActivitySubtypeActivityName() {
        $projectAssociated = $this->getProjectAssociated();
        $this->activitySubtypeActivityName = $projectAssociated->project_name;
    }

    public function getActivitySubtypeActivityName(): string {
        return $this->activitySubtypeActivityName;
    }

    public function setUserDisplayName() {
        $userDetails = $this->getRequestUserDetails();
        $firstName = $userDetails["first_name"] ?: "";
        $lastName = $userDetails["last_name"] ?: "";
        $this->userDisplayName = $firstName . " " . $lastName;
    }

    public function getUserDisplayName(): string {
        return $this->userDisplayName;
    }

    public function setActivityLogData(array $data) {
        $this->activityLogData = $data;
    }

    public function getActivityLogData(): array {
        return $this->activityLogData;
    }

    public function getS3PackageFileName(): string {
        return $this->getActivityLogId().".zip";
    }

    public function setBeforeEventJson(string $data) {
        return $this->beforeEventJson = $data;
    }

    public function getBeforeEventJson(): string {
        return $this->beforeEventJson;
    }

    public function setAfterEventJson(string $data) {
        return $this->afterEventJson = $data;
    }

    public function getAfterEventJson(): string {
        return $this->afterEventJson;
    }

    public function setS3UploadedStatus($status) {
        $this->s3UploadedStatus = $status;
    }

    public function getS3UploadedStatus() {
        return $this->s3UploadedStatus;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
  
        try {
            // set before and after event raw data
            $eventData = $event->eventData;
            $this->setBeforeEventRawData($eventData['beforeEventRawData']);
            $this->setAfterEventRawData($eventData['afterEventRawData']);

            // set request user details
            $this->setRequestUserDetails($eventData['requestUserDetails']);

            // parse before and after event raw data and set them
            $this->parseBeforeEventRawData();
            $this->parseAfterEventRawData();

            // set project associated under which item is created
            $this->setProjectAssociated();

            if(!empty($this->getProjectAssociated()->project_id)) {

                $this->setActivityName();
                $this->setActivitySubtypeActivityName();
                $this->setUserDisplayName();

                // prepare before and after event json files and put them in a zip file
                $this->prepareBeforeEventJson();
                $this->prepareAfterEventJson();

                // save before and after event json to local storage temporarily
                $this->saveBeforeAndAfterEventJsonFilesToLocalStorage();

                // zip everything inside the folder containing before and after json
                $this->archiveBeforeAndAfterJsonFiles();

                // upload zip to s3
                $this->uploadArchiveFileToS3();

                if($this->getS3UploadedStatus()===true) {
                    // prepare the activity log data
                    $this->prepareActivityLogData();

                    // save activity data to activity log
                    $this->logActivityData();

                    // cleanup folder and archive from local storage
                    $this->cleanLocalStorage();
                }
            }
        }
        catch(\Exception $exception) {
            $this->logErrors($exception);
        
            // cleanup folder and archive from local storage
            $this->cleanLocalStorage();
        }
    }

    /**
     * Handle a task failure.
     *
     * @param  \App\Events\Event  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Event $event, $exception)
    {
        $this->logErrors($exception);
        
        // cleanup folder and archive from local storage
        $this->cleanLocalStorage();
    }

    private function parseBeforeEventRawData() {
        $beforeEventRawData = $this->getBeforeEventRawData();
        $parsedData = [
            "item_id" => $beforeEventRawData->item_id,
            "document_id" => !empty($beforeEventRawData->document_id) ? $beforeEventRawData->document_id : "",
            "full_statement" => !empty($beforeEventRawData->full_statement) ? $beforeEventRawData->full_statement : "",
            "alternative_label" => !empty($beforeEventRawData->alternative_label) ? $beforeEventRawData->alternative_label : "",
            "item_type_id" => !empty($beforeEventRawData->item_type_id) ? $beforeEventRawData->item_type_id : "",
            "human_coding_scheme" => !empty($beforeEventRawData->human_coding_scheme) ? $beforeEventRawData->human_coding_scheme : "",
            "list_enumeration" => !empty($beforeEventRawData->list_enumeration) ? $beforeEventRawData->list_enumeration : "",
            "abbreviated_statement" => !empty($beforeEventRawData->abbreviated_statement) ? $beforeEventRawData->abbreviated_statement : "",
            "concept_id" => !empty($beforeEventRawData->concept_id) ? $beforeEventRawData->concept_id : "",
            "notes" => !empty($beforeEventRawData->notes) ? $beforeEventRawData->notes : "",
            "language_id" => !empty($beforeEventRawData->language_id) ? $beforeEventRawData->language_id : "",
            "education_level" => !empty($beforeEventRawData->education_level) ? $beforeEventRawData->education_level : "",
            "license_id" => !empty($beforeEventRawData->license_id) ? $beforeEventRawData->license_id : "",
            "status_start_date" => !empty($beforeEventRawData->status_start_date) ? $beforeEventRawData->status_start_date : "",
            "status_end_date" => !empty($beforeEventRawData->status_end_date) ? $beforeEventRawData->status_end_date : "",
            "is_deleted" => $beforeEventRawData->is_deleted,
            "created_at" => $beforeEventRawData->created_at,
            "updated_at" => $beforeEventRawData->updated_at
        ];
        $this->setBeforeEventParsedData($parsedData);
    }

    private function parseAfterEventRawData() {
        $afterEventRawData = $this->getAfterEventRawData();
        $parsedData = [
            "item_id" => $afterEventRawData->item_id,
            "document_id" => !empty($afterEventRawData->document_id) ? $afterEventRawData->document_id : "",
            "full_statement" => !empty($afterEventRawData->full_statement) ? $afterEventRawData->full_statement : "",
            "alternative_label" => !empty($afterEventRawData->alternative_label) ? $afterEventRawData->alternative_label : "",
            "item_type_id" => !empty($afterEventRawData->item_type_id) ? $afterEventRawData->item_type_id : "",
            "human_coding_scheme" => !empty($afterEventRawData->human_coding_scheme) ? $afterEventRawData->human_coding_scheme : "",
            "list_enumeration" => !empty($afterEventRawData->list_enumeration) ? $afterEventRawData->list_enumeration : "",
            "abbreviated_statement" => !empty($afterEventRawData->abbreviated_statement) ? $afterEventRawData->abbreviated_statement : "",
            "concept_id" => !empty($afterEventRawData->concept_id) ? $afterEventRawData->concept_id : "",
            "notes" => !empty($afterEventRawData->notes) ? $afterEventRawData->notes : "",
            "language_id" => !empty($afterEventRawData->language_id) ? $afterEventRawData->language_id : "",
            "education_level" => !empty($afterEventRawData->education_level) ? $afterEventRawData->education_level : "",
            "license_id" => !empty($afterEventRawData->license_id) ? $afterEventRawData->license_id : "",
            "status_start_date" => !empty($afterEventRawData->status_start_date) ? $afterEventRawData->status_start_date : "",
            "status_end_date" => !empty($afterEventRawData->status_end_date) ? $afterEventRawData->status_end_date : "",
            "is_deleted" => $afterEventRawData->is_deleted,
            "created_at" => $afterEventRawData->created_at,
            "updated_at" => $afterEventRawData->updated_at
        ];
        $this->setAfterEventParsedData($parsedData);
    }

    private function prepareBeforeEventJson() {
        $beforeEventDataJson = json_encode($this->getBeforeEventParsedData());
        $this->setBeforeEventJson($beforeEventDataJson);
    }

    private function prepareAfterEventJson() {
        $afterEventDataJson = json_encode($this->getAfterEventParsedData());
        $this->setAfterEventJson($afterEventDataJson);
    }

    private function saveBeforeAndAfterEventJsonFilesToLocalStorage() {
        $beforeEventJson = $this->getBeforeEventJson();
        $afterEventJson = $this->getAfterEventJson();
        $activityLogId = $this->getActivityLogId();
        $this->storeBeforeAndAfterEventJsonFiles($activityLogId, $beforeEventJson, $afterEventJson);
    }

    private function archiveBeforeAndAfterJsonFiles() {
        $archivePackage = $this->getS3PackageFileName();
        $activityLogId = $this->getActivityLogId();
        $this->archiveActivityFolder($activityLogId, $archivePackage);
    }

    private function uploadArchiveFileToS3() {
        $archiveName = $this->getS3PackageFileName();
        $s3UploadedStatus = $this->uploadFileToS3($archiveName);
        $this->setS3UploadedStatus($s3UploadedStatus);
    }

    private function prepareActivityLogData() {
        $afterEventRawData = $this->getAfterEventRawData();
        $requestUserDetails = $this->getRequestUserDetails();
        
        $activityLogId = $this->getActivityLogId();
        $userId = $requestUserDetails["user_id"];
        $organizationId = $requestUserDetails["organization_id"];
        $activityEventType = $this->getEventTypeValue($this->eventType);
        $activityId = $afterEventRawData->item_id;
        $activityEventSubType = $this->getSubEventTypeValue($this->subEventType);
        $activityEventSubTypeActivityId = $afterEventRawData->project_id;
        $activityPackageS3FileId = $this->getS3PackageFileName();
        $cacheActivityName = $this->getActivityName();
        $cacheActivitySubtypeActivityName = $this->getActivitySubtypeActivityName();
        $cacheUserDisplayName = $this->getUserDisplayName();
        $activityLogTimestamp = now()->toDateTimeString();
        
        $activityLog = [
            "activity_log_id" => $activityLogId,
            "user_id" => $userId,
            "organization_id" => $organizationId,
            "activity_event_type" => $activityEventType,
            "activity_id" => $activityId,
            "activity_event_sub_type" => $activityEventSubType,
            "activity_event_sub_type_activity_id" => $activityEventSubTypeActivityId,
            "activity_package_s3_file_id" => $activityPackageS3FileId,
            "cache_activity_name" => $cacheActivityName,
            "cache_activity_subtype_activity_name" => $cacheActivitySubtypeActivityName,
            "cache_user_display_name" => $cacheUserDisplayName,
            "activity_log_timestamp" => $activityLogTimestamp
        ];

        $this->setActivityLogData($activityLog);
    }

    private function logActivityData() {
        $activityLog = $this->getActivityLogData();
        $this->activityLogRepository->saveSingleRecord($activityLog);
    }

    private function cleanLocalStorage() {
        $activityLogId = $this->getActivityLogId();
        $this->flushReportEventLocalStorage($activityLogId);
    }
}
