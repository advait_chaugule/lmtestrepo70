<?php
namespace App\Domains\Item\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CFItemIdsValidator extends BaseValidator {

    protected $rules = [
        'ids.*' => 'required|exists:taxonomies,id'
    ];

     protected $messages = [
        'required' => ':attribute is required.'
     ];
    
}