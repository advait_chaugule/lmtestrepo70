<?php
namespace App\Domains\Item\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class CFItemValidator extends BaseValidator {

    protected $rules = [
        'document_id' => 'required|exists:documents,document_id',
    //    'full_statement' => 'required|string|max:1000',
        'alternative_label' => 'string|nullable|max:100',
    //    'human_coding_scheme' => 'string|nullable|max:15',
        'list_enumeration' => 'string|nullable|max:10',
        'abbreviated_statement' => 'string|nullable|max:50',
        'education_level' => 'string|nullable|max:50',
        'notes' => 'string|nullable', 
    ];

     protected $messages = [
        'required' => ':attribute is required.',
        'url' => ':attribute should be of type url.',
        'string' => ':attribute should be of type string.',
        'max:1000' => ':attribute should be of maximum 1000 characters.',
        'max:100' => ':attribute should be of maximum 100 characters.',
        'max:50' => ':attribute should be of maximum 50 characters.',
        'max:15' => ':attribute should be of maximum 15 characters.',
        'max:10' => ':attribute should be of maximum 10 characters.'
     ];
    
}