<?php
namespace App\Domains\Item\Validators;

use Illuminate\Validation\ValidationException;

use App\Foundation\BaseValidator;

class ItemExistsValidator extends BaseValidator {

    protected $rules = [
        'item_id' => 'required|exists:items'
    ];

     protected $messages = [
        "required" => ":attribute is required.",
        "exists"  =>  ":attribute is invalid."
     ];
    
}