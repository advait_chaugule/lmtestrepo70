<?php
namespace App\Domains\Item\Jobs;
use Lucid\Foundation\Job;
use DB;

class UpdateItemAssociationMetadataJob extends Job
{
    public function __construct($userId,$organizationId,$itemAssociationId,$metadata,$nodeTypeId='')
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->itemAssociationId = $itemAssociationId;
        $this->metadata = $metadata;
        $this->nodeTypeId = $nodeTypeId;
    }
    
    public function handle()
    {
        $response = $this->UpdateItemAssociationMetadata($this->userId,$this->organizationId,$this->itemAssociationId,$this->metadata,$this->nodeTypeId);
        return $response;
    }

    private function UpdateItemAssociationMetadata($userId,$organizationId,$itemAssociationId,$metadata,$nodeTypeId)
    {
	// If metadata exists, update else insert
        $date = date('Y-m-d H:i:s');
        $metadataId = array_column($metadata,'metadata_id');
        $metadatavalues =  DB::table('item_association_metadata')
                            ->where('item_association_id',$itemAssociationId)
                            ->whereIn('metadata_id',$metadataId)
                            ->where('is_deleted',0)
                            ->get()
                            ->toArray();

        foreach($metadata as $metadataK => $metadataV){
            if(in_array($metadataV['metadata_id'], array_column($metadatavalues,'metadata_id'))){
                DB::table('item_association_metadata')
                ->where('item_association_id',$itemAssociationId)
                ->where('metadata_id',$metadataV['metadata_id'])
                ->where('is_deleted',0)
                ->update(['metadata_value' => $metadataV['metadata_value'],'node_type_id' => $nodeTypeId]);
            }
            else{
                $item_metadata = DB::table('item_association_metadata')
                ->insert(['item_association_id' => $itemAssociationId, 'metadata_id' => $metadataV['metadata_id'], 'metadata_value' => $metadataV['metadata_value'],
                    'is_additional' => $metadataV['is_additional'], 'is_deleted' => 0, 'created_at' => $date, 'updated_at' => $date,'sort_order' => $metadataV['sort_order'],'node_type_id' => $nodeTypeId]);
            }
        }
        return true;
    }
}