<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

class CheckItemHasProjectJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $item_id = $this->input['item_id']; 
        $getProjectItemDetails = DB::table('project_items')
        ->select('project_id', 'item_id')
        ->where('item_id',$item_id)
        ->get();
        $projectItemDetails = json_decode($getProjectItemDetails, true);
        return $projectItemDetails;
    }
}
