<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetChildrenNodesJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->data = $input;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $id = $this->data['item_id'];
        $cfTree = $itemRepo->getItemTree($id);
        return $cfTree;
    }
}
