<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetAllCommentDetailsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemsMappedWithProject)
    {
        $this->setRequestData($itemsMappedWithProject);
    }

    public function setRequestData(array $data) {
        $this->requestData = $data;
    }

    public function getRequestData(): array {
        return $this->requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
        $itemsMappedWithProject=$this->requestData;
        return $this->itemRepository->getItemDetails($itemsMappedWithProject);
    }
}
