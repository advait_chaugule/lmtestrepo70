<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class UpdateCFItemMultipleTablesJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemId)
    {
        $this->data = $itemId;
        $this->setItemIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $item_id = $this->data;

        $this->itemRepo =  $itemRepo;

        return $this->itemRepo->updateProjectAndDocument($item_id);
    }

    public function setItemIdentifier(string $identifier) {
        $this->itemIdentifier = $identifier; 
    }

    public function getItemIdentifier() {
        return $this->itemIdentifier;
    }

}
