<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Item\Validators\ItemExistsValidator;

class ValidateItemByIdJob extends Job
{

    public $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemExistsValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
