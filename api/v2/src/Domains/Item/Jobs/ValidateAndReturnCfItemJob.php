<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class ValidateAndReturnCfItemJob extends Job
{

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->data = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $item_id = $this->data['item_id'];
        $item = $itemRepo->find($item_id);
        
        return !empty($item->item_id) ? $item : false;
    }
}
