<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\DB;

use App\Data\Models\Document;
use App\Data\Models\Item;
use App\Data\Models\NodeType;
use App\Data\Models\Project;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;
use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\LicenseRepositoryInterface;
use App\Data\Repositories\Contracts\ConceptRepositoryInterface;

use App\Services\Api\Traits\StringHelper;
use App\Services\Api\Traits\DateHelpersTrait;

class CreateDeltaItemUpdatesSearchDataJob extends Job
{

    use StringHelper, DateHelpersTrait;

    private $documentRepository;
    private $itemRepository;
    private $nodeTypeRepository;
    private $projectRepository;
    private $itemAssociationRepository;
    private $metadataRepository;
    private $languageRepository;
    private $licenseRepository;
    private $conceptRepository;

    private $itemIds;
    private $items;
    private $documentIds;
    private $conceptIds;
    private $languageIds;
    private $licenseIds;
    private $organizationIds;
    private $nodeTypeIds;
    private $projectIds;

    private $projectItemMapping;

    private $documents;
    private $itemAssociations;
    private $concepts;
    private $languages;
    private $licenses;
    private $nodeTypes;
    private $projects;

    private $metadata;
    private $itemsMetadata;
    private $itemsSystemMetadata;
    private $itemsCustomMetadata;

    private $parsedItemDeltaUpdatesSearchData;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $itemIds)
    {
        $this->itemIds = $itemIds;
    }

    public function setItems(Collection $data) {
        $this->items = $data;
    }

    public function getItems(): Collection {
        return $this->items;
    }

    public function setDocuments(Collection $data) {
        $this->documents = $data;
    }

    public function getDocuments(): Collection {
        return $this->documents;
    }

    public function setItemAssociations(Collection $data) {
        $this->itemAssociations = $data;
    }

    public function getItemAssociations(): Collection {
        return $this->itemAssociations;
    }

    public function setConcepts(Collection $data) {
        $this->concepts = $data;
    }

    public function getConcepts(): Collection {
        return $this->concepts;
    }

    public function setLanguages(Collection $data) {
        $this->languages = $data;
    }

    public function getLanguages(): Collection {
        return $this->languages;
    }

    public function setLicenses(Collection $data) {
        $this->licenses = $data;
    }

    public function getLicenses(): Collection {
        return $this->licenses;
    }

    public function setNodeTypes(Collection $data) {
        $this->nodeTypes = $data;
    }

    public function getNodeTypes(): Collection {
        return $this->nodeTypes;
    }

    /******************************Setters and getters for project related data*******************************/

    public function setProjectItemMapping(Collection $data) {
        $this->projectItemMapping = $data;
    }

    public function getProjectItemMapping(): Collection {
        return $this->projectItemMapping;
    }

    public function setProjects(Collection $data) {
        $this->projects = $data;
    }

    public function getProjects(): Collection {
        return $this->projects;
    }

    /******************************Setters and getters for metadata related data*******************************/

    public function setMetadata(Collection $data) {
        $this->metadata = $data;
    }

    public function getMetadata(): Collection {
        return $this->metadata;
    }

    public function setItemsMetadata(Collection $data) {
        $this->itemsMetadata = $data;
    }

    public function getItemsMetadata(): Collection {
        return $this->itemsMetadata;
    }

    public function setItemsSystemMetadata(Collection $data) {
        $this->itemsSystemMetadata = $data;
    }

    public function getItemsSystemMetadata(): Collection {
        return $this->itemsSystemMetadata;
    }

    public function setItemsCustomMetadata(Collection $data) {
        $this->itemsCustomMetadata = $data;
    }

    public function getItemsCustomMetadata(): Collection {
        return $this->itemsCustomMetadata;
    }

    public function setParsedItemDeltaUpdatesSearchData(Collection $data) {
        $this->parsedItemDeltaUpdatesSearchData = $data;
    }

    public function getParsedItemDeltaUpdatesSearchData(): Collection {
        return $this->parsedItemDeltaUpdatesSearchData;
    }
    

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        DocumentRepositoryInterface $documentRepository,
        ItemRepositoryInterface $itemRepository,
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ProjectRepositoryInterface $projectRepository,
        ItemAssociationRepositoryInterface $itemAssociationRepository,
        MetadataRepositoryInterface $metadataRepository,
        LanguageRepositoryInterface $languageRepository,
        LicenseRepositoryInterface $licenseRepository,
        ConceptRepositoryInterface $conceptRepository
    )
    {
        // set db repositories
        $this->documentRepository = $documentRepository;
        $this->itemRepository = $itemRepository;
        $this->nodeTypeRepository = $nodeTypeRepository;
        $this->projectRepository = $projectRepository;
        $this->itemAssociationRepository = $itemAssociationRepository;
        $this->metadataRepository = $metadataRepository;
        $this->languageRepository = $languageRepository;
        $this->licenseRepository = $licenseRepository;
        $this->conceptRepository = $conceptRepository;

        $this->fetchAndSetItems();

        $items = $this->getItems();
        $this->extractAndSetIdsOfProvidedTypeFromItemCollection('document_id', $items);
        $this->extractAndSetIdsOfProvidedTypeFromItemCollection('concept_id', $items);
        $this->extractAndSetIdsOfProvidedTypeFromItemCollection('language_id', $items);
        $this->extractAndSetIdsOfProvidedTypeFromItemCollection('license_id', $items);
        $this->extractAndSetIdsOfProvidedTypeFromItemCollection('organization_id', $items);
        $this->extractAndSetIdsOfProvidedTypeFromItemCollection('node_type_id', $items);

        $this->fetchAndSetItemProjectMapping();
        $this->extractAndSetProjectIds();

        $this->fetchAndSetDocuments();
        $this->fetchAndSetItemAssociations();
        $this->fetchAndSetConcepts();
        $this->fetchAndSetLanguages();
        $this->fetchAndSetLicenses();
        $this->fetchAndSetNodeTypes();
        $this->fetchAndSetProjects();
        $this->fetchAndSetMetadata();
        $this->fetchAndSetItemMetadata();
        $this->parseAndSetItemSystemMetadata();

        $this->prepareAndSetSearchTaxonomyData();
        return $this->getParsedItemDeltaUpdatesSearchData();
    }

    private function fetchAndSetItems() {
        $attributeIn = 'item_id';
        $whereClauseAttributes = [];
        $items = $this->itemRepository->findByAttributeContainedIn($attributeIn, $this->itemIds, $whereClauseAttributes);
        $this->setItems($items);
    }

    private function extractAndSetIdsOfProvidedTypeFromItemCollection(string $idKey, Collection $items) {
        $extractedIds = $items->unique($idKey)->pluck($idKey)->toArray();
        switch ($idKey) {
            case 'document_id':
                $this->documentIds = $extractedIds;
                break;
            case 'concept_id':
                $this->conceptIds = $extractedIds;
                break;
            case 'language_id':
                $this->languageIds = $extractedIds;
                break;
            case 'license_id':
                $this->licenseIds = $extractedIds;
                break;
            case 'organization_id':
                $this->organizationIds = $extractedIds;
                break;
            case 'node_type_id':
                $this->nodeTypeIds = $extractedIds;
                break;
            default:
                # code...
                break;
        }
    }
    
    private function fetchAndSetItemProjectMapping() {
        $projectItemMapping = DB::table('project_items')->select('project_id', 'item_id')->whereIn('item_id', $this->itemIds)->get();
        // will require this when searching for item project name later
        $this->setProjectItemMapping($projectItemMapping);
    }

    private function extractAndSetProjectIds() {
        $projectItemMapping = $this->getProjectItemMapping();
        $itemProjectIds = $projectItemMapping->unique('project_id')->pluck('project_id')->toArray();
        $this->projectIds = $itemProjectIds;
    }

    private function fetchAndSetDocuments() {
        $attributeIn = 'document_id';
        $whereClauseAttributes = [];
        $documents = $this->documentRepository->findByAttributeContainedIn($attributeIn, $this->documentIds, $whereClauseAttributes);
        $this->setDocuments($documents);
    }

    private function fetchAndSetItemAssociations() {
        $returnCollection = true;
        $fieldsToReturn = [ 'item_association_id', 'origin_node_id', 'association_type' ];
        $conditionalClause = [];
        $attributeIn = 'document_id';
        $returnCollection = true;
        $itemAssociations = $this->itemAssociationRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $this->documentIds,
            $returnCollection,
            $fieldsToReturn,
            $conditionalClause
        );
        $this->setItemAssociations($itemAssociations);
    }

    private function fetchAndSetConcepts() {
        $attributeIn = 'concept_id';
        $whereClauseAttributes = [];
        $concepts = $this->conceptRepository->findByAttributeContainedIn($attributeIn, $this->conceptIds, $whereClauseAttributes);
        $this->setConcepts($concepts);
    }

    private function fetchAndSetLanguages() {
        $attributeIn = 'language_id';
        $whereClauseAttributes = [];
        $languages = $this->languageRepository->findByAttributeContainedIn($attributeIn, $this->languageIds, $whereClauseAttributes);
        $this->setLanguages($languages);
    }

    private function fetchAndSetLicenses() {
        $attributeIn = 'license_id';
        $whereClauseAttributes = [];
        $licenses = $this->licenseRepository->findByAttributeContainedIn($attributeIn, $this->licenseIds, $whereClauseAttributes);
        $this->setLicenses($licenses);
    }

    private function fetchAndSetNodeTypes() {
        $attributeIn = 'node_type_id';
        $whereClauseAttributes = [];
        $nodeTypes = $this->nodeTypeRepository->findByAttributeContainedIn($attributeIn, $this->nodeTypeIds, $whereClauseAttributes);
        $this->setNodeTypes($nodeTypes);
    }

    private function fetchAndSetProjects() {
        $attributeIn = 'project_id';
        $whereClauseAttributes = [];
        $projectCollection = $this->projectRepository->findByAttributeContainedIn($attributeIn, $this->projectIds, $whereClauseAttributes);
        $this->setProjects($projectCollection);
    }

    private function fetchAndSetMetadata() {
        $returnCollection = true;
        $fieldsToReturn = [ 'metadata_id', 'name', 'internal_name', 'is_custom', 'is_document', 'organization_id' ];
        $conditionalClause = [ 'is_deleted' => 0, 'is_active' => 1 ];
        $attributeIn = 'organization_id';
        $returnCollection = true;
        $metadataCollection = $this->metadataRepository->findByAttributesWithSpecifiedFieldsWithInQuery(
            $attributeIn, 
            $this->organizationIds,
            $returnCollection,
            $fieldsToReturn,
            $conditionalClause
        );
        $this->setMetadata($metadataCollection);
    }

    private function fetchAndSetItemMetadata() {
        $itemsMetaData = DB::table('item_metadata')->whereIn('item_id', $this->itemIds)->get();
        $this->setItemsMetadata($itemsMetaData);
    }

    private function parseAndSetItemSystemMetadata() {
        $metadataCollection = $this->getMetadata();
        $itemSystemMetadataCollection = $metadataCollection->whereIn('is_document', [0,2])->where('is_custom', 0);
        $this->setItemsSystemMetadata($itemSystemMetadataCollection);
    }

    private function parseAndSetItemCustomMetadata() {
        $metadataCollection = $this->getMetadata();
        $itemCustomMetadadataCollection = $metadataCollection->whereIn('is_document', [0,2])->where('is_custom', 1);
        $this->setItemsCustomMetadata($itemCustomMetadadataCollection);
    }

    private function prepareAndSetSearchTaxonomyData() {
        $itemCollection = $this->getItems();
        $parsedItemDeltaUpdatesSearchData = [];
        foreach($itemCollection as $item) {
            $parsedItemNodeSearchData = $this->createAndReturnItemNodeSearchData($item);
            $parsedItemDeltaUpdatesSearchData[] = $parsedItemNodeSearchData;
        }
        $searchDataCollection = collect($parsedItemDeltaUpdatesSearchData);
        $this->setParsedItemDeltaUpdatesSearchData($searchDataCollection);
    }

    private function createAndReturnItemNodeSearchData(Item $item): array {
        $identifier = $item->item_id;
        $parentIdentifier = $item->parent_id;
        $organizationIdentifier = $item->organization_id;
        $documentId = $item->document_id;
        $taxonomyName = $this->helperToSearchAndReturnTaxonomyName($documentId);
        $itemProjectId = $this->helperToReturnItemProjectId($identifier);
        $projectDetails = $this->helperToSearchAndReturnProjectDetails($itemProjectId);
        $projectName = $projectDetails['project_name'];
        $projectWorkflowId = $projectDetails['workflow_id'];
        $title = $taxonomyName;
        $humanCodingScheme = $item->human_coding_scheme ?: "";
        $listEnumeration = $item->list_enumeration ?: "";
        $fullStatement = $item->full_statement ?: "";
        $alternativeLabel = $item->alternative_label ?: "";
        $abbreviatedStatement = $item->abbreviated_statement ?: "";
        $nodeTypeId = !empty($item->node_type_id) ? $item->node_type_id : "";
        $nodeType = $this->helperToSearchAndReturnNodeTypeTitle($nodeTypeId);
        $type = 'item';
        $deleteOrAddStatusTextForCloudSearch = $item->is_deleted===1 ? 'delete' : 'add';
        $itemAssociationTypes = $this->helperToSearchAndReturnItemAssociationTypes($identifier);
        $updatedBy = $item->updated_by ?: "";
        $updateOn = (!empty($item->updated_at) && $item->updated_at!=="0000-00-00 00:00:00") ? 
                    $this->formatDateTimeToCloudSarchCompatibleFormat($item->updated_at->toDateTimeString()) : 
                    "1970-01-01T00:00:00Z";
        $itemSystemMetadataKeyValuePairsCsv = $this->helperToReturnItemSystemMetadataKeyValuePairsCsv($item);
        $customMetadataKeyValuePairsCsv = $this->helperToReturnItemCustomMetadataKeyValuePairsCsv($item);
        $itemNodesSearchData = [
            'id' => $identifier,
            'type' => $deleteOrAddStatusTextForCloudSearch,
            'fields' => [
                'identifier' => $identifier,
                'parent_identifier' => $parentIdentifier,
                'organization_identifier' => $organizationIdentifier,
                'document_id' => $documentId,
                'taxonomy_name' => $taxonomyName,
                'project_name' => $projectName,
                'title' => $title,
                'official_source_url' => '',
                'notes' => '',
                'publisher' => '',
                'description' => '',
                'human_coding_scheme' => $humanCodingScheme,
                'list_enumeration' => $listEnumeration,
                'full_statement' => $fullStatement,
                'alternative_label' => $alternativeLabel,
                'abbreviated_statement' => $abbreviatedStatement,
                'node_type' => $nodeType,
                'type' => $type,
                'node_type_id' => $nodeTypeId,
                'project_id' => $itemProjectId,
                'project_workflow_id' => $projectWorkflowId,
                'adoption_status' => 0, // set default value for item
                'item_association_type' => $itemAssociationTypes,
                'updated_by' => $updatedBy,
                'update_on' => $updateOn,
                'item_metadata' => $itemSystemMetadataKeyValuePairsCsv,
                'document_metadata' => '',
                'item_custom_metadata' => $customMetadataKeyValuePairsCsv // custom metadata key:value csv
            ]
        ];
        return $itemNodesSearchData;
    }

    private function helperToSearchAndReturnTaxonomyName(string $documentId): string {
        $documents = $this->getDocuments();
        $taxonomyNameToReturn = "";
        $searchResult = $documents->where('document_id', $documentId);
        if($searchResult->isNotEmpty()) {
            $document = $searchResult->first();
            $taxonomyNameToReturn = !empty($document->title) ? $document->title : "";
        }
        return $taxonomyNameToReturn;
    }

    private function helperToReturnItemProjectId(string $itemId): string {
        $projectItemMapping = $this->getProjectItemMapping();
        $projectIdToReturn = "";
        $searchResult = $projectItemMapping->where('item_id', $itemId);
        if($searchResult->isNotEmpty()) {
            $projectItemMap = $searchResult->first();
            $projectIdToReturn = $projectItemMap->project_id;
        }
        return $projectIdToReturn;
    }

    private function helperToSearchAndReturnProjectDetails(string $projectIdToSearchFor): array {
        $dataToReturn = [
            "project_name" => '',
            "workflow_id" => ''
        ];
        $projectCollection = $this->getProjects();
        $searchResult = $projectCollection->where("project_id", $projectIdToSearchFor);
        if($searchResult->isNotEmpty()) {
            $project = $searchResult->first();
            $projectName = $project->project_name ?: "";
            $projectWorkflowId = $project->workflow_id ?: "";
            $dataToReturn['project_name'] = $projectName;
            $dataToReturn['workflow_id'] = $projectWorkflowId;
        }
        return $dataToReturn;
    }

    private function helperToSearchAndReturnNodeTypeTitle(string $nodeTypeIdToSearchFor): string {
        $nodeTypesCollection = $this->getNodeTypes();
        $searchResult = $nodeTypesCollection->where("node_type_id", $nodeTypeIdToSearchFor);
        return $searchResult->isNotEmpty() ? ($searchResult->first()->title ?: "") : "";
    }

    private function helperToSearchAndReturnItemAssociationTypes(string $originNodeIdentifier): array {
        $associationTypesToReturn = [];
        $itemAssociations = $this->getItemAssociations();
        $searchData = $itemAssociations->where('origin_node_id', $originNodeIdentifier)->unique('association_type');
        if($searchData->isNotEmpty()) {
            $associationTypesToReturn = $searchData->values()->pluck('association_type')->toArray();
        }
        return $associationTypesToReturn;
    }

    private function helperToReturnItemCustomMetadataKeyValuePairsCsv(Item $item): string {
        $itemAllMetadata = $this->getItemsMetadata();
        $itemSystemMetadata = $this->getItemsSystemMetadata();
        $itemCustomMetadataKeyValuePairs = [];
        foreach($itemAllMetadata as $metadata) {
            $metadataValue = $metadata->metadata_value;
            $metadataId = $metadata->metadata_id;
            $systemMetadataSearchResult = $itemSystemMetadata->where('metadata_id', $metadataId);
            if($systemMetadataSearchResult->isEmpty() && !empty($metadataValue)) {
                $keyValueString = "$metadataId:$metadataValue";
                array_push($itemCustomMetadataKeyValuePairs, $keyValueString);
            }
        }
        // return $this->arrayContentToCommaeSeparatedString($itemCustomMetadataKeyValuePairs);
        return implode(", ",$itemCustomMetadataKeyValuePairs);
    }

    private function helperToReturnItemSystemMetadataKeyValuePairsCsv(Item $item): string {
        $itemSystemMetadata = $this->getItemsSystemMetadata();
        $itemArray = $item->toArray();
        $itemSystemMetadataKeyValuePairs = [];
        foreach($itemArray as $indexKey => $value) {
            if(!empty($value)) {
                $searchForMetadata = $itemSystemMetadata->where('internal_name', $indexKey);
                if($searchForMetadata->isNotEmpty()) {
                    $metadataId = $searchForMetadata->first()->metadata_id;
                    $keyValueString = "$metadataId:$value";
                    array_push($itemSystemMetadataKeyValuePairs, $keyValueString);
                }
            }
        }

        // create key value for 'license', 'language', 'concepts' system attributes 
        $licenseId = $item->license_id ?: "";
        $itemLicense = $this->helperToSearchAndReturnItemLicense($licenseId);
        if(!empty($itemLicense->license_id)){
            if(!empty($itemLicense->license_text)){
                $licenseTextSearchMetadata = $itemSystemMetadata->where('internal_name', 'license_text');
                if($licenseTextSearchMetadata->isNotEmpty()) {
                    $licenseTextMetadataId = $licenseTextSearchMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$licenseTextMetadataId:$itemLicense->license_text");
                }
            }
            if(!empty($itemLicense->description)){
                $licenseDescriptionSearchMetadata = $itemSystemMetadata->where('internal_name', 'license_description');
                if($licenseDescriptionSearchMetadata->isNotEmpty()) {
                    $licenseDescriptionMetadataId = $licenseDescriptionSearchMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$licenseDescriptionMetadataId:$itemLicense->description");
                }
            }
            if(!empty($itemLicense->title)){
                $licenseTitleSearchMetadata = $itemSystemMetadata->where('internal_name', 'license_title');
                if($licenseTitleSearchMetadata->isNotEmpty()) {
                    $licenseTitleMetadataId = $licenseTitleSearchMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$licenseTitleMetadataId:$itemLicense->title");
                }
            }
        }

        $languageId = $item->language_id ?: "";
        $itemLanguage = $this->helperToSearchAndReturnItemLanguage($languageId);
        if(!empty($itemLanguage->short_code)) {
            $languageSearchMetadata = $itemSystemMetadata->where('internal_name', 'language');
            if($languageSearchMetadata->isNotEmpty()) {
                $languageMetadataId = $itemSystemMetadata->first()->metadata_id;
                array_push($itemSystemMetadataKeyValuePairs, "$languageMetadataId:$itemLanguage->short_code");
            }
        }

        $conceptId = $item->concept_id ?: "";
        $itemConcept = $this->helperToSearchAndReturnItemConcept($conceptId);
        if(!empty($itemConcept->concept_id)) {
            if(!empty($itemConcept->title)) {
                $conceptSearchTitleMetadata = $itemSystemMetadata->where('internal_name', 'concept_title');
                if($conceptSearchTitleMetadata->isNotEmpty()) {
                    $conceptTitleMetadataId = $conceptSearchTitleMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptTitleMetadataId:$itemConcept->title");
                }
            }

            if(!empty($itemConcept->keywords)) {
                $conceptSearchKeywordsMetadata = $itemSystemMetadata->where('internal_name', 'concept_keywords');
                if($conceptSearchKeywordsMetadata->isNotEmpty()) {
                    $conceptKeywordsMetadataId = $conceptSearchKeywordsMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptKeywordsMetadataId:$itemConcept->keywords");
                }
            }
            
            if(!empty($itemConcept->hierarchy_code)) {
                $conceptSearchHierarchyCodeMetadata = $itemSystemMetadata->where('internal_name', 'concept_hierarchy_code');
                if($conceptSearchHierarchyCodeMetadata->isNotEmpty()) {
                    $conceptHierarchyCodeMetadataId = $conceptSearchHierarchyCodeMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptHierarchyCodeMetadataId:$itemConcept->hierarchy_code");
                }
            }
            
            if(!empty($itemConcept->description)) {
                $conceptSearchDescriptionMetadata = $itemSystemMetadata->where('internal_name', 'concept_description');
                if($conceptSearchDescriptionMetadata->isNotEmpty()) {
                    $conceptDescriptionCodeMetadataId = $conceptSearchDescriptionMetadata->first()->metadata_id;
                    array_push($itemSystemMetadataKeyValuePairs, "$conceptDescriptionCodeMetadataId:$itemConcept->description");
                }
            }
        }

        // return $this->arrayContentToCommaeSeparatedString($itemSystemMetadataKeyValuePairs);
        return implode(", ",$itemSystemMetadataKeyValuePairs);
    } 
    
    private function helperToSearchAndReturnItemLicense(string $licenseId) {
        $itemLicenses = $this->getLicenses();
        $searchResult = $itemLicenses->where('license_id', $licenseId);
        $dataToReturn = [];
        if($searchResult->isNotEmpty()) {
            $dataToReturn = $searchResult->first();
        }
        return $dataToReturn;
    }

    private function helperToSearchAndReturnItemLanguage(string $languageId) {
        $itemLanguages = $this->getLanguages();
        $searchResult = $itemLanguages->where('language_id', $languageId);
        $dataToReturn = [];
        if($searchResult->isNotEmpty()) {
            $dataToReturn = $searchResult->first();
        }
        return $dataToReturn;
    }

    private function helperToSearchAndReturnItemConcept(string $conceptId) {
        $itemConcepts = $this->getConcepts();
        $searchResult = $itemConcepts->where('concept_id', $conceptId);
        $dataToReturn = [];
        if($searchResult->isNotEmpty()) {
            $dataToReturn = $searchResult->first();
        }
        return $dataToReturn;
    }
}
