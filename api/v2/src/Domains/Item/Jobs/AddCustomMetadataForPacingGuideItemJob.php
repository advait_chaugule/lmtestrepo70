<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\MetadataRepositoryInterface;

class AddCustomMetadataForPacingGuideItemJob extends Job
{
    private $itemIdentifier;
    private $organizationIdentifier;
    private $metadataType;
    private $itemType;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, string $organizationIdentifier, string $metadataType, string $itemType)
    {
        //Set the identifier
        $this->itemIdentifier           =   $itemIdentifier;
        $this->organizationIdentifier   =   $organizationIdentifier;
        $this->metadataType             =   $metadataType;
        $this->itemType                 =   $itemType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MetadataRepositoryInterface $metadataRepository, ItemRepositoryInterface $itemRepository)
    {
        //Set the repo handler
        $this->metadataRepository   =   $metadataRepository;
        $this->itemRepository       =   $itemRepository;

        $returnCollection   =   false;
        $fieldsToReturn     =   ['metadata_id'];
        $keyValuePairs      =   ['is_document'  =>  $this->metadataType, 'organization_id'  =>  $this->organizationIdentifier];
        
        $metadataDetail =   $this->metadataRepository->findByAttributesWithSpecifiedFields($returnCollection,$fieldsToReturn,$keyValuePairs);

        if(!empty($metadataDetail))
        {
            $isAdditional = 0;
            $this->itemRepository->addOrUpdateCustomMetadataValue($this->itemIdentifier, $metadataDetail->metadata_id, $this->itemType, $this->itemType, $isAdditional,'insert');
        }
    }
}
