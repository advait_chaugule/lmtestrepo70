<?php
namespace App\Domains\Item\Jobs;

use Illuminate\Http\Request;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\DB;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Domains\Search\Events\UploadSearchDataToSqsEvent;
use App\Data\Models\Organization;
use App\Data\Models\NodeType;
class CreateCFItemJob extends Job
{
    use CaseFrameworkTrait;
    private $input;
    private $organizationCode;
    private $domainName;
    private $organizationId;

    /**
     * CreateCFItemJob constructor.
     * @param array $input
     * @param $requestUrl
     */
    public function __construct(array $input,$requestUrl)
    {
        $this->input = $input;
        $this->organizationId = $this->input['auth_user']['organization_id'];
        $this->domainName = $requestUrl;
    }
    public function setOrgCode(string $data) {
        
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $this->organizationId)->first()->org_code;
        $this->organizationCode = $organizationCode;
    }

    public function getOrgCode() {
        return $this->organizationCode;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ItemRepositoryInterface $itemRepo,
        DocumentRepositoryInterface $documentRepo,
        Request $request
    )
    {
        $organizationCode       =   Organization::select('org_code')->where('organization_id',$this->organizationId)->first()->org_code;
        $nodeTypeId = $this->input['node_type_id'];
        $nodeTypeTitle = NodeType::select('title')->where('node_type_id',$nodeTypeId)->get();
        $nodeTitle     = ['title'=>$nodeTypeTitle[0]->title,
                          'identifier'=>$nodeTypeId,
                          'uri'=>$this->getCaseApiUri("CFItemTypes",$nodeTypeId,true, $organizationCode,$this->domainName)
                          ];
                       
                          //print_r($nodeTitle );die;
        $sourceNodeUriObj = json_encode($nodeTitle,true);    
        $this->input['source_node_type_uri_object'] =  $sourceNodeUriObj ;
        $this->input['import_type'] = 2;                                        
        $this->itemRepo             =   $itemRepo;
        $organizationId = $request->input("auth_user")["organization_id"];
        $this->input["organization_id"] = $organizationId;
        $this->setOrgCode($organizationId);
        $parentId = $this->input['parent_id'];
        $itemId = $this->input['item_id'];
        $documentId = $this->input['document_id'];
        $orgCode  =   $this->getOrgCode();
        $uri = $this->getCaseApiUri("CFItems", $itemId,true,$orgCode,$this->domainName);
        $this->input['uri']= $uri;

        $projectId = "";
        // if parent id provided matches document id, then fetch project using document's details
        if($documentId===$parentId) {
            $document = $documentRepo->find($documentId);
            $projectId = $document->project_id ? $document->project_id: "";
        }
        // else use parent item to fetch project
        else {
            $item = $itemRepo->find($parentId);
            $project = $item->project()->first();
            if($project == null)
            {
                $projectId = "";
            }
            else
            {
                $projectId = $project->project_id ? $project->project_id: "";
            }
        }

        if($projectId!=="") {
            $itemRepo->addProjectItem($itemId, $projectId);
        }




        // $parentId = $this->input['parent_id'];
        // $itemId = $this->input['item_id'];

        // $parentItemDetails = $itemRepo->getItemDetailsWithProjectAndCaseAssociation($parentId);
        // if(!empty($parentItemDetails->project)) {
        //     $projectDetails = $parentItemDetails->project;
        //     $itemRepo->addProjectItem($itemId, $projectDetails[0]->project_id);
        // }

        $this->updateMetadataStatus();
        $this->raiseEventToUploadTaxonomySearchDataToSqs();
        
        return $itemRepo->fillAndSave($this->input);

    }

    private function raiseEventToUploadTaxonomySearchDataToSqs() {
        $itemId = $this->input['item_id'];
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_item"];
        $eventData = [
            "type_id" => $itemId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }
    
    private function updateMetadataStatus()
    {
        $itemId                         =   $this->input['item_id'];
        $nodeTypeId = $this->input['node_type_id'];
        $nodeIdMappedWithMedataId = DB::table('node_type_metadata')
        ->rightjoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
        ->select('node_type_metadata.node_type_id','node_type_metadata.metadata_id','metadata.is_custom','metadata.name','metadata.internal_name')
        ->where('node_type_metadata.node_type_id',$nodeTypeId)
        ->where('metadata.is_custom','1')
        ->where('metadata.is_deleted','0')
        ->get()
        ->toArray();

        $value = "";
        $value_html = "";
        $isAdditional=0;
        foreach($nodeIdMappedWithMedataId as $nodeIdMappedWithMedataIdK => $nodeIdMappedWithMedataIdV)
        {
            $this->itemRepo->addOrUpdateCustomMetadataValue($itemId, $nodeIdMappedWithMedataIdV->metadata_id, $value,$value_html, $isAdditional, 'insert');
        }
    }
}
