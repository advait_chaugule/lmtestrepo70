<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use DB;

class DeleteItemAssociationMetadataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($itemAssociationId,$metadataId)
    {
        $this->itemAssociationId    = $itemAssociationId;
        $this->metadataId           = $metadataId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = DB::table('item_association_metadata')
                        ->where('item_association_id', $this->itemAssociationId)
                        ->where('metadata_id', $this->metadataId)
                        ->update(['is_deleted'=> 1]);

        return $response;                
    }
}
