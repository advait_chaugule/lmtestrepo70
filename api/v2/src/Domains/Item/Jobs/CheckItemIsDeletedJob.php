<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class CheckItemIsDeletedJob extends Job
{
    private $itemIdentifier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->itemIdentifier = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        //validate not empty Item
        $attributes = ['item_id' => $this->itemIdentifier, 'is_deleted' => '1'];
        return $itemRepo->findByAttributes($attributes)->isNotEmpty();
    }
}
