<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class DocumentMultipleTableJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentId)
    {
        $this->data = $documentId;
        $this->setDocumentIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $documentId = $this->data;

        $this->itemRepo =  $itemRepo;

        return $this->itemRepo->updateDocument($documentId);
    }

    public function setDocumentIdentifier(string $identifier) {
        $this->itemIdentifier = $identifier; 
    }

    public function getDocumentIdentifier() {
        return $this->itemIdentifier;
    }
}
