<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Models\Item;
use App\Data\Models\ItemAssociation;
use App\Data\Models\Metadata;
use DB;

class DeleteItemJob extends Job
{
    private $itemIds;
    private $deleteStatusData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->itemIds = $inputs;
        $this->deleteStatusData = ["is_deleted" => 1];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo)
    {
        $input_item_ids=array();
        $itemRepo->getAllChildItemsV2($this->itemIds, $input_item_ids);        
        array_push($input_item_ids,$this->itemIds);

        
        Item::whereIn('item_id',$input_item_ids)->update(['is_deleted'=>1]);
        ItemAssociation::whereIn('origin_node_id',$input_item_ids)->update(['is_deleted'=>1]);
        ItemAssociation::whereIn('destination_node_id',$input_item_ids)->update(['is_deleted'=>1]);
        DB::table('item_metadata')->whereIn('item_id', $input_item_ids)->update(['is_deleted'=>1]);
        
    }
}
