<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\DB;

class CheckItemAssociationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($itemAssociationId,$organizationId)
    {
        $this->itemAssociationId = $itemAssociationId;
        $this->organizationId = $organizationId;
    }

     /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $itemAssociationId = $this->itemAssociationId;
        $organizationId = $this->organizationId;
        
        $query = DB::table('item_associations')
        ->select('item_association_id')
        ->where('item_association_id','=',$itemAssociationId)
        ->where('is_deleted','=',0)
        ->where('organization_id','=',$organizationId)->get();
        
        return $query->first() ? true : false;
    }

}