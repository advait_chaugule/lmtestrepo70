<?php
namespace App\Domains\Item\Jobs; 

use Lucid\Foundation\Job;
use Illuminate\Http\Request;

use App\Data\Repositories\Contracts\DocumentRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use App\Data\Repositories\Contracts\ThreadRepositoryInterface;

use App\Services\Api\Traits\CaseFrameworkTrait;
use App\Services\Api\Traits\DateHelpersTrait;
use DB;
use App\Data\Models\Item;
use App\Data\Models\Document;
use App\Data\Repositories\Contracts\ProjectRepositoryInterface;
class GetCFItemDetailsJob extends Job
{
    use CaseFrameworkTrait,DateHelpersTrait;

    private $repository;
    private $threadRepository;
    private $identifier;
    private $itemDetailsFetchedFromDatabase;
    private $customMetadata =   [];
    private $parsedDataToReturn;

    private $organizationId;

    private $itemAssociationsArray;

    private $_tempStorageForSavedDocument;
    private $_tempStorageForSavedItem;
    private $projectRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemIdentifier, string $organizationIdentifier)
    {
        $this->setItemIdentifier($itemIdentifier);
        $this->setRequestUserOrganizationId($organizationIdentifier);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ItemRepositoryInterface $repository, 
        DocumentRepositoryInterface $documentRepository, 
        NodeTypeRepositoryInterface $nodeTypeRepository,
        ThreadRepositoryInterface $threadRepository,
        Request $request,
        ProjectRepositoryInterface $projectRepository
    )
    {

        //set the database repository
        $this->repository           =   $repository;
        $this->documentRepository   =   $documentRepository;
        $this->nodeTypeRepository   =   $nodeTypeRepository;
        $this->threadRepository     =   $threadRepository;
        $this->projectRepository    =  $projectRepository;
        // call method to fetch Item details from database and set it
        $this->fetchItemDetailsFromDatabaseAndSetIt();
        // fetch and set item related custom metadata
        $this->fetchAndSetCustomMetadataAssociatedToItem();
        // parse the data and format the respose data accordingly
        $this->processDataToReturnAndSetIt();
        // finally return the processed data
        return $this->getParsedDataToReturn();

    }

    public function setRequestUserOrganizationId(string $identifier) {
        $this->organizationId = $identifier;
    }

    public function getRequestUserOrganizationId(): string {
        return $this->organizationId;
    }

    public function setItemIdentifier(string $identifier) {
        $this->identifier = $identifier;
    }

    public function getItemIdentifier(): string {
        return $this->identifier;
    }

    public function setItemDetailsFetchedFromDatabase($data) {
        $this->itemDetailsFetchedFromDatabase = $data;
    }

    public function getItemDetailsFetchedFromDatabase() {
        return $this->itemDetailsFetchedFromDatabase;
    }

    public function setCustomMetadata($data) {
        $this->customMetadata = $data;
    }

    public function getCustomMetadata() {
        return $this->customMetadata;
    }

    public function setParsedDataToReturn($data) {
        $this->parsedDataToReturn = $data;
    }

    public function getParsedDataToReturn() {
        return $this->parsedDataToReturn;
    }    

    private function fetchItemDetailsFromDatabaseAndSetIt() {
        $identifier = $this->getItemIdentifier();
        $data = $this->repository->getItemDetailsWithProjectAndCaseAssociation($identifier);
        $this->setItemDetailsFetchedFromDatabase($data);
    }

    private function fetchAndSetCustomMetadataAssociatedToItem() {
        $customMetadataArray                    =   [];
        $customMetadataAssociatedToItemArray    =   [];

        $itemDetails                    =   $this->getItemDetailsFetchedFromDatabase();
        $customMetadataAssociatedToItem =   $itemDetails->customMetadata()->where(['metadata.is_active' => '1', 'item_metadata.is_deleted' => '0', 'metadata.is_deleted' => '0'])->get();

        foreach($customMetadataAssociatedToItem as $customMetadata) {
            if($customMetadata->is_document != 3) {
                $customMetadataAssociatedToItemArray[] = $customMetadata->metadata_id;
            }
        }

        /* foreach($nodeTypeMetadata->metadata as $metadata) {
            if(sizeOf($customMetadataAssociatedToItemArray) > 0) {
                if($metadata->is_custom == 1) {
                    if(in_array($metadata->metadata_id, $customMetadataAssociatedToItemArray) === false) {
                        $this->repository->addOrUpdateCustomMetadataValue($itemDetails->item_id, $metadata->metadata_id, '', 'insert');
                    }
                }
            } else {
                if($metadata->is_custom == 1) {
                    $this->repository->addOrUpdateCustomMetadataValue($itemDetails->item_id, $metadata->metadata_id, '', 'insert');
                }
            }
        } */
        
        if($customMetadataAssociatedToItem->isNotEmpty()){
            foreach($customMetadataAssociatedToItem as $customMetadata) {
                if($customMetadata->is_document != 3) {
                    $customMetadataArray[]  =   $customMetadata->toArray();
                }
            }
        }

        $this->setCustomMetadata($customMetadataArray);
    }

    private function fetchAndSetAssociationOfItem($itemId) {
        $itemAssociationsArray          =   [];
        $itemDetails                    =   $this->repository->getItemDetailsWithProjectAndCaseAssociation($itemId);

        $itemAssociations               =   $itemDetails->itemAssociations()->get()->toArray();

        foreach($itemAssociations as $associations) {
            if($associations['association_type'] == 3 || $associations['association_type'] == 1) {
                $itemAssociationsArray[$associations['origin_node_id']] = ['item_association_id' => $associations['item_association_id']];
            }
            
        }

        return $itemAssociationsArray;
    }

    private function checkIfCustomMetadataIsAdditionalToItem() {
        $arrayOfAdditionalMetadataToItem    =   [];
        $arrayOfNodetypeMetadataId          =   [];

        $itemDetails                    =   $this->getItemDetailsFetchedFromDatabase();
        $nodeTypeMetadata               =   $this->nodeTypeRepository->getNodeTypeMetadata($itemDetails->node_type_id);

        $customMetadataAssociatedToItem =   $this->getCustomMetadata();
        if($nodeTypeMetadata!=null){
            foreach($nodeTypeMetadata->metadata as $metadata) {
                $arrayOfNodetypeMetadataId[]  =   $metadata->metadata_id;
            }
        }
        if(sizeOf($customMetadataAssociatedToItem) > 0) {
            foreach($customMetadataAssociatedToItem as $customMetadata) {
                if(in_array($customMetadata['metadata_id'], $arrayOfNodetypeMetadataId)) {
                    $arrayOfAdditionalMetadataToItem[$customMetadata['metadata_id']]  =  0;
                }
                else {
                    $arrayOfAdditionalMetadataToItem[$customMetadata['metadata_id']]  =  1;
                }
            }
        }

        

        return $arrayOfAdditionalMetadataToItem;
        
    }

    private function parseProjectData($projects): array {
        $parsedData = [];
        foreach($projects as $project){
            $parsedData[] = [
                "project_id"    => $project->project_id,
                "project_name"  => $project->project_name,
                "description"   => !empty($project->description) ? $project->description : "",
                "workflow_id"   => $project->workflow_id,
                "created_at"    => !empty($project->created_at) ? $project->created_at->toDateTimeString() : "",
            ];
        }
        return $parsedData;
    }

    private function getDestinationNodeTaxonomyName($destinationIdentifier){
        $destinationItemDetail = $this->repository->find($destinationIdentifier);
        if(!empty($destinationItemDetail)){
            $documentDetail = $this->documentRepository->find($destinationItemDetail->document_id);
            //print_r($documentDetail);
            $documentEntity = [
                'document_id'       => $documentDetail->document_id,
                'document_title'    => $documentDetail->title
            ];
        }else{
            $documentEntity = [
                'document_id' => "",
                'document_title' => ""
            ];
        }
        return $documentEntity;
        
    }

    private function parseCaseAssociationData($itemAssociations, $itemDestinationAssociations, $Item): array {
        $reverseAssociationArray    =   ['3','5','7'];
        $parsedAssociationData      =   [];
        $itemAssociationId          =   '';
        
        foreach($itemAssociations as $association){
            
            if($association->association_type != '4') {
                if($association->destination_document_id == $Item->document_id && $association->is_reverse_association == 0) {
                    $itemAssociationId      =   $association->item_association_id;
                    $originNode             =   $association->originNode;
                    $associatedDocument     =   $association->document;
                    $sequenceNumber         =   $association->sequence_number;
                    $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                    $associationTypeNumber  =   $association->association_type;
                    $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                    $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                    $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                    $description            =   !empty($association->description) ? $association->description: "";
                    $destinationNodeId      =   !empty($association->destination_node_id) ? $association->destination_node_id : '';
    
                    $destinationNodeTypeTitle           =   '';
                    $destinationNodeHumanCodingScheme   =   '';
                    $destinationNodeFullStatement       =   '';
                    $destinationNodeDocumentData        =   [
                        "document_id" => '',
                        "document_title" => ''
                    ];
                    
                    if($destinationNodeId!=='') {
                        
                        // destination node can be associated document of the self taxonomy
                        if($destinationNodeId===$associatedDocument->document_id) {
                            $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                        '';
                            $destinationNodeHumanCodingScheme = '';
                            $destinationNodeFullStatement = '';
                            $destinationNodeParentId = '';
                            // destination document details
                            $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                            $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                        }
                        else {
                            // destination node is an item/document of the self or other taxonomy within the same tenant
                            $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
    
    
                            if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                $type = $destinationItemNode['type'];
                                $nodeDetails = $destinationItemNode['data'];
                                $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                            '';
                                if($type==='item') {                                    
                                    $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                    $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                    $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';              
                                }
                                else {
                                    $destinationNodeHumanCodingScheme = '';
                                    $destinationNodeFullStatement = '';
                                    $destinationNodeParentId = '';
                                }
                            } else {
                                $destinationNodeParentId    =   '';
                                $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])? $externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])? $externalNodeTitleExtracted[4]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])? $externalNodeTitleExtracted[5]:"";
                                } else {
                                    if(sizeOf($externalNodeTitleExtracted) > 1) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    } else {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                   
                                    }
                                }
                            }
    
                            // fetch any destinantion node document detail within the same tenant
                            $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
    
                            if(empty($destinationNodeDocumentData['document_id'])) {
                                $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);

                                $destinationNodeDocumentData    =   [
                                    "document_id" => '',
                                    "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                ];
                            }
                        }
                    } else {
                        if(!empty($externalNodeTitle)) {
                            $externalNodeTitleExtracted = explode(":", $externalNodeTitle);
                            $destinationNodeId = '';
                            $destinationNodeParentId = '';
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                }
                                
                            }

                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }

                    
                    $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                    $projectType = 0;
                    $projectId   = '';
                    if($projectTypeData) {
                        $projectType     = $projectTypeData[0]['project_type'];
                        $projectId       = $projectTypeData[0]['project_id'];
                    }

                    if($projectType == '2') {
                        $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                        $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                    }
                    if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                    $parsedData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" =>  $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $associationTypeText,
                            "display_name" => $associationTypeText
                        ],
                        "document" => $associatedDocument,
                        "origin_node" => $originNode,
                        "destination_node" => [
                            "item_id" => $destinationNodeId,
                            "parent_id" => $destinationNodeParentId,
                            "item_association_id"   =>  $itemAssociationId,
                            "destination_node_url" => $externalNodeUrl,
                            "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                            "full_statement" => $destinationNodeFullStatement,
                            "node_type_title" => $destinationNodeTypeTitle,
                            "document" => $destinationNodeDocumentData,
                            "project_type"=> $projectType,
                            "project_id"=> $projectId,
                            "is_external"=> $isexternalURI
                        ],
                        "description" => $description,
                        "sequence_number" => $sequenceNumber
                    ];
    
                    //dd($parsedData);
                    array_push($parsedAssociationData, $parsedData);
                    
                }
            } else {
                $itemAssociationId      =   $association->item_association_id;
                $originNode             =   $association->originNode;
                $associatedDocument     =   $association->document;
                $sequenceNumber         =   $association->sequence_number;
                $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                $associationTypeNumber  =   $association->association_type;
                $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                $description            =   !empty($association->description) ? $association->description: "";
                $destinationNodeId      =   !empty($association->destination_node_id) ? $association->destination_node_id : '';

                $destinationNodeTypeTitle           =   '';
                $destinationNodeHumanCodingScheme   =   '';
                $destinationNodeFullStatement       =   '';
                $destinationNodeDocumentData        =   [
                    "document_id" => '',
                    "document_title" => ''
                ];
                
                if($destinationNodeId!=='') {
                    
                    // destination node can be associated document of the self taxonomy
                    if($destinationNodeId===$associatedDocument->document_id) {
                        
                        $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                    $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                    '';
                        $destinationNodeHumanCodingScheme = '';
                        $destinationNodeFullStatement = '';
                        $destinationNodeParentId = '';

                        // destination document details
                        $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                        $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                    }
                    else {
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);


                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId ='';
                            }
                        } else {
                            $destinationNodeParentId ='';
                            $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                }
                            }
                            
                        }

                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);

                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                } else {

                    $destinationNodeParentId ='';
                    
                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                    } else {
                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1]) ? $externalNodeTitleExtracted[1] :'';
                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2]) ? $externalNodeTitleExtracted[2] :'';
                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3]) ? $externalNodeTitleExtracted[3] :'';
                        } else {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0]) ? $externalNodeTitleExtracted[0] :'';
                        }
                        
                    }

                    

                    $destinationNodeDocumentData    =   [
                        "document_id" => '',
                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                    ];
                }
                $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);

                $projectType = 0;
                $projectId = '';
                if($projectTypeData) {
                    $projectType     = $projectTypeData[0]['project_type'];
                    $projectId       = $projectTypeData[0]['project_id'];
                }

                if($projectType == '2') {
                    $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    $itemAssociationId      =   $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                }

                //dd($destinationNodeDocumentData);
                if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                $parsedData = [
                    "item_association_id" => $itemAssociationId,
                    "created_at" =>  $createdAt,
                    "association_type" => [
                        "type_id" => $associationTypeNumber,
                        "type_name" => $associationTypeText,
                        "display_name" => $associationTypeText
                    ],
                    "document" => $associatedDocument,
                    "origin_node" => $originNode,
                    "destination_node" => [
                        "item_id" => $destinationNodeId,
                        "parent_id" => $destinationNodeParentId,
                        "item_association_id"   =>  $itemAssociationId,
                        "destination_node_url" => $externalNodeUrl,
                        "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                        "full_statement" => $destinationNodeFullStatement,
                        "node_type_title" => $destinationNodeTypeTitle,
                        "document" => $destinationNodeDocumentData,
                        "project_type"=>$projectType,
                        "project_id"=>$projectId,
                        "is_external"=> $isexternalURI
                    ],
                    "description" => $description,
                    "sequence_number" => $sequenceNumber
                ];

                //dd($parsedData);
                array_push($parsedAssociationData, $parsedData);
            } 
        }

        $parsedDestinationAssociationData = [];

        foreach($itemDestinationAssociations as $association){
            
            $destinationNodeTypeTitle           = '';
            $destinationNodeHumanCodingScheme   = '';
            $destinationNodeFullStatement       = '';
            $destinationNodeDocumentData        = [
                    "document_id" => '',
                    "document_title" => ''
                ];

            $associationTypeNumber  =   $association->association_type;
            $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);

            if(in_array($associationTypeNumber, $reverseAssociationArray)) {
                if(!empty($association->destination_document_id) && $association->destination_document_id == $Item->document_id && $association->is_reverse_association == 1) {
                    if($associationTypeNumber == 3) {
                        $associationTypeText    =   'hasPart';
                    } else if($associationTypeNumber == 5) {
                        $associationTypeText    =   'succeeds';
                    } else if($associationTypeNumber == 7) {
                        $associationTypeText    =   'replaces';
                    }

                    $itemAssociationId          = $association->item_association_id;
                    $createdAt                  = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
            
                    $originNode                 =   $association->destinationNode;
                    $externalNodeTitle          =   !empty($association->external_node_title) ? $association->external_node_title: "";
                    $externalNodeUrl            =   !empty($association->external_node_url) ? $association->external_node_url: "";
                    $description                =   !empty($association->description) ? $association->description: "";
                    $destinationNodeId          =   !empty($association->origin_node_id) ? $association->origin_node_id : '';

                    $associatedDocument         =   $association->document;
                    
                    if($destinationNodeId!=='') {
                        
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);


                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId ='';
                            }
                        } else {
                            $externalNodeTitleExtracted =   !empty($externalNodeTitle) ? explode(":", $externalNodeTitle) : "";

                            //print_r($externalNodeTitleExtracted);

                            if(sizeOf($externalNodeTitleExtracted)>0) {
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5]) ? $externalNodeTitleExtracted[5]: "";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[2]:"";
                                }
                            }
                        }

                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);

                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted) > 4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                    $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                    $projectType = 0;
                    $projectId = '';
                    if($projectTypeData) {
                        $projectType     = $projectTypeData[0]['project_type'];
                        $projectId       = $projectTypeData[0]['project_id'];
                    }

                    if($projectType == '2') {
                        $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                        $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                    }
                    if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                    $parsedDestinationData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" =>  $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $associationTypeText,
                            "display_name" => $associationTypeText
                        ],
                        "document" => $associatedDocument,
                        "origin_node" => $originNode,
                        "destination_node" => [
                            "item_id" => $destinationNodeId,
                            "parent_id" => $destinationNodeParentId,
                            "item_association_id"   =>  $itemAssociationId,
                            "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                            "full_statement" => $destinationNodeFullStatement,
                            "destination_node_url" => $externalNodeUrl,
                            "node_type_title" => $destinationNodeTypeTitle,
                            "document" => $destinationNodeDocumentData,
                            "project_type" => $projectType,
                            "project_id"  => $projectId,
                            "is_external"=> $isexternalURI
                        ],
                        "description" => $description
                    ];

                    array_push($parsedAssociationData, $parsedDestinationData);
                }
            }       
        }

        $sorted_array = array();
        foreach ($parsedAssociationData as $key => $row)
        {
            $sorted_array[$key] = $row['created_at'];
        }
        array_multisort($sorted_array, SORT_DESC, $parsedAssociationData);
        
        return $parsedAssociationData;
    }

    // new logic for view association starts

    private function parseCaseAssociationDataV2($itemAssociations, $itemDestinationAssociations, $Item): array {
        $reverseAssociationArray    =   ['3','5','7'];
        $parsedAssociationData      =   [];
        $itemAssociationId          =   '';
        $itemAssociationsList = $itemAssociations->toArray();
        $itemAssociationMetadataList = $this->getItemAssociationMetadata(array_column($itemAssociationsList,'item_association_id'));
        // forward associations start
        foreach($itemAssociations as $association){
            if(!isset($itemAssociationMetadataList[$association->item_association_id]))
                $itemAssociationMetadataList[$association->item_association_id] = [];
            
            if($association->association_type != '4') {
                if($association->source_document_id == $Item->document_id && $association->source_document_id == $association->destination_document_id) {
                    $itemAssociationId      =   $association->item_association_id;
                    $originNode             =   $association->sourceItemId;
                    $associatedDocument     =   $association->sourceDocumentId;
                    $sequenceNumber         =   $association->sequence_number;
                    $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                    $associationTypeNumber  =   $association->association_type;
                    $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                    $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                    $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                    $description            =   !empty($association->description) ? $association->description: "";
                    $destinationNodeId      =   !empty($association->target_item_id) ? $association->target_item_id : '';
    
                    $destinationNodeTypeTitle           =   '';
                    $destinationNodeHumanCodingScheme   =   '';
                    $destinationNodeFullStatement       =   '';
                    $destinationNodeDocumentData        =   [
                        "document_id" => '',
                        "document_title" => ''
                    ];
    
                    if($destinationNodeId!=='') {
                        
                        // destination node can be associated document of the self taxonomy
                        if($destinationNodeId===$associatedDocument->document_id) {
                            $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                        '';
                            $destinationNodeHumanCodingScheme = '';
                            $destinationNodeFullStatement = '';
                            $destinationNodeParentId = '';
                            // destination document details
                            $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                            $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                        }
                        else {
                            // destination node is an item/document of the self or other taxonomy within the same tenant
                            $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
    
    
                            if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                $type = $destinationItemNode['type'];
                                $nodeDetails = $destinationItemNode['data'];
                                $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                            '';
                                if($type==='item') {                                    
                                    $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                    $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                    $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';              
                                }
                                else {
                                    $destinationNodeHumanCodingScheme = '';
                                    $destinationNodeFullStatement = '';
                                    $destinationNodeParentId = '';
                                }
                            } else {
                                $destinationNodeParentId    =   '';
                                $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])? $externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])? $externalNodeTitleExtracted[4]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])? $externalNodeTitleExtracted[5]:"";
                                } else {
                                    if(sizeOf($externalNodeTitleExtracted) > 1) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    } else {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                   
                                    }
                                }
                            }
    
                            // fetch any destinantion node document detail within the same tenant
                            $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
    
                            if(empty($destinationNodeDocumentData['document_id'])) {
                                $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);

                                $destinationNodeDocumentData    =   [
                                    "document_id" => '',
                                    "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                ];
                            }
                        }
                    } else {
                        if(!empty($externalNodeTitle)) {
                            $externalNodeTitleExtracted = explode(":", $externalNodeTitle);
                            $destinationNodeId = '';
                            $destinationNodeParentId = '';
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                }
                                
                            }

                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }

                    
                    $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                    $projectType = 0;
                    $projectId   = '';
                    if($projectTypeData) {
                        $projectType     = $projectTypeData[0]['project_type'];
                        $projectId       = $projectTypeData[0]['project_id'];
                    }

                    if($projectType == '2') {
                        $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                        $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                    }
                    if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                    $parsedData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" =>  $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $associationTypeText,
                            "display_name" => $associationTypeText
                        ],
                        "document" => $associatedDocument,
                        "origin_node" => $originNode,
                        "destination_node" => [
                            "item_id" => $destinationNodeId,
                            "parent_id" => $destinationNodeParentId,
                            "item_association_id"   =>  $itemAssociationId,
                            "destination_node_url" => $externalNodeUrl,
                            "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                            "full_statement" => $destinationNodeFullStatement,
                            "node_type_title" => $destinationNodeTypeTitle,
                            "document" => $destinationNodeDocumentData,
                            "project_type"=> $projectType,
                            "project_id"=> $projectId,
                            "is_external"=> $isexternalURI
                        ],
                        "description" => $description,
                        "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                        "sequence_number" => $sequenceNumber
                    ];
    
                    
                    array_push($parsedAssociationData, $parsedData);
                    
                }
            } else {
                $itemAssociationId      =   $association->item_association_id;
                $originNode             =   $association->sourceItemId;
                $associatedDocument     =   $association->sourceDocumentId;
                $sequenceNumber         =   $association->sequence_number;
                $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                $associationTypeNumber  =   $association->association_type;
                $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                $description            =   !empty($association->description) ? $association->description: "";
                $destinationNodeId      =   !empty($association->target_item_id) ? $association->target_item_id : '';

                $destinationNodeTypeTitle           =   '';
                $destinationNodeHumanCodingScheme   =   '';
                $destinationNodeFullStatement       =   '';
                $destinationNodeDocumentData        =   [
                    "document_id" => '',
                    "document_title" => ''
                ];
                
                if($destinationNodeId!=='') {
                    
                    // destination node can be associated document of the self taxonomy
                    if($destinationNodeId===$associatedDocument->document_id) {
                        
                        $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                    $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                    '';
                        $destinationNodeHumanCodingScheme = '';
                        $destinationNodeFullStatement = '';
                        $destinationNodeParentId = '';

                        // destination document details
                        $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                        $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                    }
                    else {
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);


                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId ='';
                            }
                        } else {
                            $destinationNodeParentId ='';
                            $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                }
                            }
                            
                        }

                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);

                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                } else {

                    $destinationNodeParentId ='';
                    
                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                    } else {
                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1]) ? $externalNodeTitleExtracted[1] :'';
                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2]) ? $externalNodeTitleExtracted[2] :'';
                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3]) ? $externalNodeTitleExtracted[3] :'';
                        } else {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0]) ? $externalNodeTitleExtracted[0] :'';
                        }
                        
                    }

                    

                    $destinationNodeDocumentData    =   [
                        "document_id" => '',
                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                    ];
                }
                $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);

                $projectType = 0;
                $projectId = '';
                if($projectTypeData) {
                    $projectType     = $projectTypeData[0]['project_type'];
                    $projectId       = $projectTypeData[0]['project_id'];
                }

                if($projectType == '2') {
                    $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    $itemAssociationId      =   $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                }

                //dd($destinationNodeDocumentData);
                if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                $parsedData = [
                    "item_association_id" => $itemAssociationId,
                    "created_at" =>  $createdAt,
                    "association_type" => [
                        "type_id" => $associationTypeNumber,
                        "type_name" => $associationTypeText,
                        "display_name" => $associationTypeText
                    ],
                    "document" => $associatedDocument,
                    "origin_node" => $originNode,
                    "destination_node" => [
                        "item_id" => $destinationNodeId,
                        "parent_id" => $destinationNodeParentId,
                        "item_association_id"   =>  $itemAssociationId,
                        "destination_node_url" => $externalNodeUrl,
                        "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                        "full_statement" => $destinationNodeFullStatement,
                        "node_type_title" => $destinationNodeTypeTitle,
                        "document" => $destinationNodeDocumentData,
                        "project_type"=>$projectType,
                        "project_id"=>$projectId,
                        "is_external"=> $isexternalURI
                    ],
                    "description" => $description,
                    "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                    "sequence_number" => $sequenceNumber
                ];
                
                array_push($parsedAssociationData, $parsedData);
            } 
        }
        // forward associations end


        // reverse associations start
        $parsedDestinationAssociationData = [];
        $itemAssociationsList = $itemDestinationAssociations->toArray();
        $itemAssociationMetadataList = $this->getItemAssociationMetadata(array_column($itemAssociationsList,'item_association_id'));
        foreach($itemDestinationAssociations as $association){
            if(!isset($itemAssociationMetadataList[$association->item_association_id]))
                $itemAssociationMetadataList[$association->item_association_id] = [];
            
            $destinationNodeTypeTitle           = '';
            $destinationNodeHumanCodingScheme   = '';
            $destinationNodeFullStatement       = '';
            $destinationNodeDocumentData        = [
                    "document_id" => '',
                    "document_title" => ''
                ];

            $associationTypeNumber  =   $association->association_type;
            $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);

            if(in_array($associationTypeNumber, $reverseAssociationArray)) {
                if(!empty($association->target_document_id) && $association->target_document_id == $Item->document_id && $association->target_document_id == $association->destination_document_id) {
                    if($associationTypeNumber == 3) {
                        $associationTypeText    =   'hasPart';
                    } else if($associationTypeNumber == 5) {
                        $associationTypeText    =   'succeeds';
                    } else if($associationTypeNumber == 7) {
                        $associationTypeText    =   'replaces';
                    }

                    $itemAssociationId          = $association->item_association_id;
                    $createdAt                  = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
            
                    $originNode                 =   $association->targetItemId;
                    $externalNodeTitle          =   !empty($association->external_node_title) ? $association->external_node_title: "";
                    $externalNodeUrl            =   !empty($association->external_node_url) ? $association->external_node_url: "";
                    $description                =   !empty($association->description) ? $association->description: "";
                    $destinationNodeId          =   !empty($association->source_item_id) ? $association->source_item_id : '';

                    $associatedDocument         =   $association->targetDocumentId;
                    $sequenceNumber             =   $association->sequence_number;
                    if($destinationNodeId!=='') {
                        
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);


                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId ='';
                            }
                        } else {
                            $externalNodeTitleExtracted =   !empty($externalNodeTitle) ? explode(":", $externalNodeTitle) : "";

                            //print_r($externalNodeTitleExtracted);

                            if(sizeOf($externalNodeTitleExtracted)>0) {
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5]) ? $externalNodeTitleExtracted[5]: "";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[2]:"";
                                }
                            }
                        }

                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);

                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted) > 4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                    $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                    $projectType = 0;
                    $projectId = '';
                    if($projectTypeData) {
                        $projectType     = $projectTypeData[0]['project_type'];
                        $projectId       = $projectTypeData[0]['project_id'];
                    }

                    if($projectType == '2') {
                        $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                        $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                    }
                    if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                    $parsedDestinationData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" =>  $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $associationTypeText,
                            "display_name" => $associationTypeText
                        ],
                        "document" => $associatedDocument,
                        "origin_node" => $originNode,
                        "destination_node" => [
                            "item_id" => $destinationNodeId,
                            "parent_id" => $destinationNodeParentId,
                            "item_association_id"   =>  $itemAssociationId,
                            "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                            "full_statement" => $destinationNodeFullStatement,
                            "destination_node_url" => $externalNodeUrl,
                            "node_type_title" => $destinationNodeTypeTitle,
                            "document" => $destinationNodeDocumentData,
                            "project_type" => $projectType,
                            "project_id"  => $projectId,
                            "is_external"=> $isexternalURI
                        ],
                        "description" => $description,
                        "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                        "sequence_number" => $sequenceNumber
                    ];

                    array_push($parsedAssociationData, $parsedDestinationData);
                }
            }       
        }
        
        // reverse associations end

        $sorted_array = array();
        foreach ($parsedAssociationData as $key => $row)
        {
            $sorted_array[$key] = $row['created_at'];
        }
        array_multisort($sorted_array, SORT_DESC, $parsedAssociationData);
        
        return $parsedAssociationData;
    }

    // new logic for view association end
    
    /**
     * Get the association link reference from the node id selected.
     */
    public function getLinkedAssociation($getItemDestinationAssociations,$getItemAssociations,  $Item): array {
        $reverseAssociationArray    =   ['3','5','7'];
        $parsedAssociationData      =   [];
        $itemAssociationId          =   '';
        /**selected nodes are the destination node */
        foreach($getItemDestinationAssociations as $association){
            if($association->association_type != '4') {
                
                /**when the association with different taxonomy  */
                if($association->destination_document_id != $Item->document_id) {

                    if($association->is_reverse_association == 0){
                        $itemAssociationId      =   $association->item_association_id;
                        //$originNode             =   $association->originNode;
                        $associatedDocument     =   $association->document;
                        $sequenceNumber         =   $association->sequence_number;
                        $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                        $associationTypeNumber  =   $association->association_type;
                        $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                        $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                        $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                        $description            =   !empty($association->description) ? $association->description: "";
                        $destinationNodeId      =   !empty($association->origin_node_id) ? $association->origin_node_id : '';
        
                        $destinationNodeTypeTitle           =   '';
                        $destinationNodeHumanCodingScheme   =   '';
                        $destinationNodeFullStatement       =   '';
                        $destinationNodeDocumentData        =   [
                            "document_id" => '',
                            "document_title" => ''
                        ];
        
                        if($destinationNodeId!=='') {
                            
                            // destination node can be associated document of the self taxonomy
                            if($destinationNodeId===$associatedDocument->document_id) {
                                $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                            '';
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';
        
                                // destination document details
                                $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                                $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                            }
                            else {
                                // destination node is an item/document of the self or other taxonomy within the same tenant
                                $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
        
        
                                if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                    $type = $destinationItemNode['type'];
                                    $nodeDetails = $destinationItemNode['data'];
                                    $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                                $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                                '';
                                    if($type==='item') {
                                        $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                        $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                        $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                                    }
                                    else {
                                        $destinationNodeHumanCodingScheme = '';
                                        $destinationNodeFullStatement = '';
                                        $destinationNodeParentId = '';
                                    }
                                } else {
                                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])? $externalNodeTitleExtracted[3]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])? $externalNodeTitleExtracted[4]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])? $externalNodeTitleExtracted[5]:"";
                                    } else {
                                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        } else {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                    
                                        }
                                        
                                    }
                                    $destinationNodeParentId = '';
                                }
        
                                // fetch any destinantion node document detail within the same tenant
                                $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
        
                                if(empty($destinationNodeDocumentData['document_id'])) {
                                    $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);

                                    $destinationNodeDocumentData    =   [
                                        "document_id" => '',
                                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                    ];
                                }
                            }
                        } else {
                            if(!empty($externalNodeTitle)) {
                                $externalNodeTitleExtracted = explode(":", $externalNodeTitle);
                                $destinationNodeId = '';
                                $destinationNodeParentId = '';
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                                } else {
                                    if(sizeOf($externalNodeTitleExtracted) > 1) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    } else {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                    }
                                    
                                }

                                $destinationNodeDocumentData    =   [
                                    "document_id" => '',
                                    "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                ];
                            }
                        }
                        $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                        $projectType = 0;
                        $projectId = '';
                        if($projectTypeData) {
                            $projectType     = $projectTypeData[0]['project_type'];
                            $projectId       = $projectTypeData[0]['project_id'];
                        }

                        if($projectType == '2') {
                            $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                            $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                        }
                        if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                        {
                            $isexternalURI = 1;
                        }
                        else {
                            $isexternalURI = 0;
                        }
                        $parsedData = [
                            "item_association_id" => $itemAssociationId,
                            "created_at" =>  $createdAt,
                            "association_type" => [
                                "type_id" => $associationTypeNumber,
                                "type_name" => $associationTypeText,
                                "display_name" => $associationTypeText
                            ],
                            //"document" => $associatedDocument,
                            //"origin_node" => $originNode,
                            "source_node" => [
                                "item_id" => $destinationNodeId,
                                "parent_id" => $destinationNodeParentId,
                                "item_association_id" => $itemAssociationId,
                                "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                                "full_statement" => $destinationNodeFullStatement,
                                "node_type_title" => $destinationNodeTypeTitle,
                                "document" => $destinationNodeDocumentData,
                                "project_type"=>$projectType,
                                "project_id"  =>$projectId,
                                "is_external"=> $isexternalURI
                            ],
                            "description" => $description,
                            "sequence_number" => $sequenceNumber
                        ];
        
                        //dd($parsedData);
                        array_push($parsedAssociationData, $parsedData);
                    }
                    
                }
                /**asociation with same taxonomy */
                else 
                {
                    
                    if($association->association_type != '1' && $association->is_reverse_association == 0){
                        $itemAssociationId      =   $association->item_association_id;
                        $originNode             =   $association->destinationNode;
                        $associatedDocument     =   $association->document;
                        $sequenceNumber         =   $association->sequence_number;
                        $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                        $associationTypeNumber  =   $association->association_type;
                        $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                        $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                        $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                        $description            =   !empty($association->description) ? $association->description: "";
                        $destinationNodeId      =   !empty($association->origin_node_id) ? $association->origin_node_id: '';
        
                        $destinationNodeTypeTitle           =   '';
                        $destinationNodeHumanCodingScheme   =   '';
                        $destinationNodeFullStatement       =   '';
                        $destinationNodeDocumentData        =   [
                            "document_id" => '',
                            "document_title" => ''
                        ];
                        
                        if($destinationNodeId!=='') {
                            
                            // destination node can be associated document of the self taxonomy
                            if($destinationNodeId===$associatedDocument->document_id) {
                                
                                $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                            '';
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';
        
                                // destination document details
                                $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                                $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                            }
                            else {
                                // destination node is an item/document of the self or other taxonomy within the same tenant
                                $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
        
        
                                if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                    $type = $destinationItemNode['type'];
                                    $nodeDetails = $destinationItemNode['data'];
                                    $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                                $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                                '';
                                    if($type==='item') {
                                        $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                        $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                        $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                                    }
                                    else {
                                        $destinationNodeHumanCodingScheme = '';
                                        $destinationNodeFullStatement = '';
                                        $destinationNodeParentId = '';
                                    }
                                } else {
                                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                                    } else {
                                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        } else {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                        }
                                    }
                                    $destinationNodeParentId = '';
                                    
                                }
        
                                // fetch any destinantion node document detail within the same tenant
                                $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
        
                                if(empty($destinationNodeDocumentData['document_id'])) {
                                    $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                                    $destinationNodeDocumentData    =   [
                                        "document_id" => '',
                                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                    ];
                                }
                            }
                        } else {
                            $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1]) ? $externalNodeTitleExtracted[1] :'';
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2]) ? $externalNodeTitleExtracted[2] :'';
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3]) ? $externalNodeTitleExtracted[3] :'';
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0]) ? $externalNodeTitleExtracted[0] :'';
                                }
                                
                            }
                            $destinationNodeParentId = '';
        
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                        $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                        $projectType = 0;
                        $projectId  = '';
                        if($projectTypeData) {
                            $projectType     = $projectTypeData[0]['project_type'];
                            $projectId     = $projectTypeData[0]['project_id'];
                        }

                        if($projectType == '2') {
                            $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                            $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                        }
                        if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                        {
                            $isexternalURI = 1;
                        }
                        else {
                            $isexternalURI = 0;
                        }
                        $parsedData = [
                            "item_association_id" => $itemAssociationId,
                            "created_at" =>  $createdAt,
                            "association_type" => [
                                "type_id" => $associationTypeNumber,
                                "type_name" => $associationTypeText,
                                "display_name" => $associationTypeText
                            ],
                            "source_node" => [
                                "item_id" => $destinationNodeId,
                                "parent_id" => $destinationNodeParentId,
                                "item_association_id" => $itemAssociationId,
                                "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                                "full_statement" => $destinationNodeFullStatement,
                                "node_type_title" => $destinationNodeTypeTitle,
                                "document" => $destinationNodeDocumentData,
                                "project_type"=>$projectType,
                                "project_id"  =>$projectId,
                                "is_external"=> $isexternalURI
                            ],
                            "description" => $description,
                            "sequence_number" => $sequenceNumber
                        ];
                        array_push($parsedAssociationData, $parsedData);
                    
                    }
                }
            }else {
                $itemAssociationId      =   $association->item_association_id;
                //$originNode             =   $association->originNode;
                $associatedDocument     =   $association->document;
                $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                $associationTypeNumber  =   $association->association_type;
                $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                $description            =   !empty($association->description) ? $association->description: "";
                $destinationNodeId      =   !empty($association->origin_node_id) ? $association->origin_node_id : '';
                $destinationNodeTypeTitle           =   '';
                $destinationNodeHumanCodingScheme   =   '';
                $destinationNodeFullStatement       =   '';
                $destinationNodeDocumentData        =   [
                    "document_id" => '',
                    "document_title" => ''
                ];
                $sequenceNumber         =   $association->sequence_number;
                if($destinationNodeId!=='') {
                    
                    // destination node can be associated document of the self taxonomy
                    if($destinationNodeId===$associatedDocument->document_id) {
                        
                        $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                    $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                    '';
                        $destinationNodeHumanCodingScheme = '';
                        $destinationNodeFullStatement = '';
                        $destinationNodeParentId = '';
                        // destination document details
                        $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                        $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                    }
                    else {
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';
                            }
                        } else {
                            $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                }
                            }
                            $destinationNodeParentId = '';
                            
                        }
                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                } else {
                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                    } else {
                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1]) ? $externalNodeTitleExtracted[1] :'';
                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2]) ? $externalNodeTitleExtracted[2] :'';
                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3]) ? $externalNodeTitleExtracted[3] :'';
                        } else {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0]) ? $externalNodeTitleExtracted[0] :'';
                        }
                        
                    }
                    $destinationNodeParentId = '';
                    $destinationNodeDocumentData    =   [
                        "document_id" => '',
                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                    ];
                }
                $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                $projectType = 0;
                $projectId   = '';
                if($projectTypeData) {
                    $projectType     = $projectTypeData[0]['project_type'];
                    $projectId       = $projectTypeData[0]['project_id'];
                }

                if($projectType == '2') {
                    $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                    $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                }
                if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                $parsedData = [
                    "item_association_id" => $itemAssociationId,
                    "created_at" =>  $createdAt,
                    "association_type" => [
                        "type_id" => $associationTypeNumber,
                        "type_name" => $associationTypeText,
                        "display_name" => $associationTypeText
                    ],
                    // "document" => $associatedDocument,
                    // "origin_node" => $originNode,
                    "source_node" => [
                        "item_id" => $destinationNodeId,
                        "parent_id" => $destinationNodeParentId,
                        "item_association_id" => $itemAssociationId,
                        "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                        "full_statement" => $destinationNodeFullStatement,
                        "node_type_title" => $destinationNodeTypeTitle,
                        "document" => $destinationNodeDocumentData,
                        "project_type"=>$projectType,
                        "project_id"=>$projectId,
                        "is_external"=> $isexternalURI
                    ],
                    "description" => $description,
                    "sequence_number" => $sequenceNumber
                ];
                //dd($parsedData);
                array_push($parsedAssociationData, $parsedData);
            }
        }

        $parsedDestinationAssociationData = [];
        /** selected node is the source node */
        foreach($getItemAssociations as $association){
            
            $destinationNodeTypeTitle           = '';
            $destinationNodeHumanCodingScheme   = '';
            $destinationNodeFullStatement       = '';
            $destinationNodeDocumentData        = [
                    "document_id" => '',
                    "document_title" => ''
                ];

            $associationTypeNumber  =   $association->association_type;
            $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);

             if(in_array($associationTypeNumber, $reverseAssociationArray)) {
                if(!empty($association->destination_document_id) && $association->destination_document_id != $Item->document_id && $association->is_reverse_association == 1) {
                    if($associationTypeNumber == 3) {
                        $associationTypeText    =   'hasPart';
                    } else if($associationTypeNumber == 5) {
                        $associationTypeText    =   'succeeds';
                    } else if($associationTypeNumber == 7) {
                        $associationTypeText    =   'replaces';
                    }

                    $itemAssociationId          = $association->item_association_id;
                    $createdAt                  = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
            
                    //$originNode                 =   $association->destinationNode;
                    $externalNodeTitle          =   !empty($association->external_node_title) ? $association->external_node_title: "";
                    $externalNodeUrl            =   !empty($association->external_node_url) ? $association->external_node_url: "";
                    $description                =   !empty($association->description) ? $association->description: "";
                    $destinationNodeId          =   !empty($association->destination_node_id) ? $association->destination_node_id : '';

                    $associatedDocument         =   $association->document;
                    $sequenceNumber             =   $association->sequence_number;
                    if($destinationNodeId!=='') {
                        
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);


                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';                              
                            }
                        } else {
                            $externalNodeTitleExtracted =   !empty($externalNodeTitle) ? explode(":", $externalNodeTitle) : "";

                            if(sizeOf($externalNodeTitleExtracted)>0) {
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5]) ? $externalNodeTitleExtracted[5]: "";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[2]:"";
                                }
                            }
                        }

                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);

                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted) > 4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                    $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                    $projectType = 0;
                    $projectId   = '';
                    if($projectTypeData) {
                        $projectType     = $projectTypeData[0]['project_type'];
                        $projectId       = $projectTypeData[0]['project_id'];
                    }

                    if($projectType == '2') {
                        $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                        $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                    }
                    if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                    $parsedDestinationData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" =>  $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $associationTypeText,
                            "display_name" => $associationTypeText
                        ],
                        //"document" => $associatedDocument,
                        //"origin_node" => $originNode,
                        "source_node" => [
                            "item_id" => $destinationNodeId,
                            "parent_id" => $destinationNodeParentId,
                            "item_association_id"   => $itemAssociationId,
                            "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                            "full_statement" => $destinationNodeFullStatement,
                            "node_type_title" => $destinationNodeTypeTitle,
                            "document" => $destinationNodeDocumentData,
                            "project_type"=>$projectType,
                            "project_id"=>$projectId,
                            "is_external"=> $isexternalURI
                        ],
                        "description" => $description,
                        "sequence_number" => $sequenceNumber
                    ];

                    array_push($parsedAssociationData, $parsedDestinationData);
                } else {
                    if($association->is_reverse_association == 1) {
                        if($associationTypeNumber == 3) {
                            $associationTypeText    =   'hasPart';
                        } else if($associationTypeNumber == 5) {
                            $associationTypeText    =   'succeeds';
                        } else if($associationTypeNumber == 7) {
                            $associationTypeText    =   'replaces';
                        }
    
                        $itemAssociationId          = $association->item_association_id;
                        $createdAt                  = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                
                        //$originNode                 =   $association->destinationNode;
                        $externalNodeTitle          =   !empty($association->external_node_title) ? $association->external_node_title: "";
                        $externalNodeUrl            =   !empty($association->external_node_url) ? $association->external_node_url: "";
                        $description                =   !empty($association->description) ? $association->description: "";
                        $destinationNodeId          =   !empty($association->destination_node_id) ? $association->destination_node_id : '';
    
                        $associatedDocument         =   $association->document;
                        $sequenceNumber             =   $association->sequence_number;
                        if($destinationNodeId!=='') {
                            
                            // destination node is an item/document of the self or other taxonomy within the same tenant
                            $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
    
    
                            if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                $type = $destinationItemNode['type'];
                                $nodeDetails = $destinationItemNode['data'];
                                $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                            '';
                                if($type==='item') {
                                    $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                    $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                    $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                                }
                                else {
                                    $destinationNodeHumanCodingScheme = '';
                                    $destinationNodeFullStatement = '';
                                    $destinationNodeParentId = '';                              
                                }
                            } else {
                                $externalNodeTitleExtracted =   !empty($externalNodeTitle) ? explode(":", $externalNodeTitle) : "";
    
                                if(sizeOf($externalNodeTitleExtracted)>0) {
                                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5]) ? $externalNodeTitleExtracted[5]: "";
                                    } else {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[2]:"";
                                    }
                                }
                            }
    
                            // fetch any destinantion node document detail within the same tenant
                            $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
    
                            if(empty($destinationNodeDocumentData['document_id'])) {
                                $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                                $destinationNodeDocumentData    =   [
                                    "document_id" => '',
                                    "document_title" => (sizeOf($externalNodeTitleExtracted) > 4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                ];
                            }
                        }
                        $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                        $projectType = 0;
                        $projectId   = '';
                        if($projectTypeData) {
                            $projectType   = $projectTypeData[0]['project_type'];
                            $projectId     = $projectTypeData[0]['project_id'];
                        }

                        if($projectType == '2') {
                            $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                            $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                        }
                        if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                        {
                            $isexternalURI = 1;
                        }
                        else {
                            $isexternalURI = 0;
                        }
                        $parsedDestinationData = [
                            "item_association_id" => $itemAssociationId,
                            "created_at" =>  $createdAt,
                            "association_type" => [
                                "type_id" => $associationTypeNumber,
                                "type_name" => $associationTypeText,
                                "display_name" => $associationTypeText
                            ],
                            //"document" => $associatedDocument,
                            //"origin_node" => $originNode,
                            "source_node" => [
                                "item_id" => $destinationNodeId,
                                "parent_id" => $destinationNodeParentId,
                                "item_association_id" => $itemAssociationId,
                                "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                                "full_statement" => $destinationNodeFullStatement,
                                "node_type_title" => $destinationNodeTypeTitle,
                                "document" => $destinationNodeDocumentData,
                                "project_type"=>$projectType,
                                "project_id"=>$projectId,
                                "is_external"=> $isexternalURI
                            ],
                            "description" => $description,
                            "sequence_number" => $sequenceNumber
                        ];
    
                        array_push($parsedAssociationData, $parsedDestinationData);
                    }
                    
                }
            }
        }

        $sorted_array = array();
        foreach ($parsedAssociationData as $key => $row)
        {
            $sorted_array[$key] = $row['created_at'];
        }
        array_multisort($sorted_array, SORT_DESC, $parsedAssociationData);
        
        return $parsedAssociationData;
    }

    // new logic for linked associations start


    public function getLinkedAssociationV2($getItemDestinationAssociations,$getItemAssociations,  $Item): array {
        $reverseAssociationArray    =   ['3','5','7'];
        $parsedAssociationData      =   [];
        $itemAssociationId          =   '';
        $itemAssociationsList = $getItemDestinationAssociations->toArray();
        $itemAssociationMetadataList = $this->getItemAssociationMetadata(array_column($itemAssociationsList,'item_association_id'));    
        /**selected nodes are the destination node */
        foreach($getItemDestinationAssociations as $association){
            if(!isset($itemAssociationMetadataList[$association->item_association_id]))
                $itemAssociationMetadataList[$association->item_association_id] = [];
            if($association->association_type != '4') {
                
                /**when the association with different taxonomy  */
                if($association->source_document_id != $Item->document_id && $association->source_document_id == $association->destination_document_id) {    // added !in_array($association->association_type,$reverseAssociationArray)
                    
                    if($association){
                        $itemAssociationId      =   $association->item_association_id;
                        //$originNode             =   $association->originNode;
                        $associatedDocument     =   $association->sourceDocumentId;
                        $sequenceNumber         =   $association->sequence_number;
                        $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                        $associationTypeNumber  =   $association->association_type;
                        $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                        $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                        $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                        $description            =   !empty($association->description) ? $association->description: "";
                        $destinationNodeId      =   !empty($association->source_item_id) ? $association->source_item_id : '';
        
                        $destinationNodeTypeTitle           =   '';
                        $destinationNodeHumanCodingScheme   =   '';
                        $destinationNodeFullStatement       =   '';
                        $destinationNodeDocumentData        =   [
                            "document_id" => '',
                            "document_title" => ''
                        ];
        
                        if($destinationNodeId!=='') {
                            
                            // destination node can be associated document of the self taxonomy
                            if($destinationNodeId===$associatedDocument->document_id) {
                                $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                            '';
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';
        
                                // destination document details
                                $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                                $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                            }
                            else {
                                // destination node is an item/document of the self or other taxonomy within the same tenant
                                $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
        
        
                                if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                    $type = $destinationItemNode['type'];
                                    $nodeDetails = $destinationItemNode['data'];
                                    $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                                $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                                '';
                                    if($type==='item') {
                                        $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                        $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                        $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                                    }
                                    else {
                                        $destinationNodeHumanCodingScheme = '';
                                        $destinationNodeFullStatement = '';
                                        $destinationNodeParentId = '';
                                    }
                                } else {
                                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])? $externalNodeTitleExtracted[3]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])? $externalNodeTitleExtracted[4]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])? $externalNodeTitleExtracted[5]:"";
                                    } else {
                                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        } else {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                    
                                        }
                                        
                                    }
                                    $destinationNodeParentId = '';
                                }
        
                                // fetch any destinantion node document detail within the same tenant
                                $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
        
                                if(empty($destinationNodeDocumentData['document_id'])) {
                                    $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);

                                    $destinationNodeDocumentData    =   [
                                        "document_id" => '',
                                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                    ];
                                }
                            }
                        } else {
                            if(!empty($externalNodeTitle)) {
                                $externalNodeTitleExtracted = explode(":", $externalNodeTitle);
                                $destinationNodeId = '';
                                $destinationNodeParentId = '';
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                                } else {
                                    if(sizeOf($externalNodeTitleExtracted) > 1) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    } else {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                    }
                                    
                                }

                                $destinationNodeDocumentData    =   [
                                    "document_id" => '',
                                    "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                ];
                            }
                        }
                        $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                        $projectType = 0;
                        $projectId = '';
                        if($projectTypeData) {
                            $projectType     = $projectTypeData[0]['project_type'];
                            $projectId       = $projectTypeData[0]['project_id'];
                        }

                        if($projectType == '2') {
                            $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                            $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                        }
                        if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                        {
                            $isexternalURI = 1;
                        }
                        else {
                            $isexternalURI = 0;
                        }
                        $parsedData = [
                            "item_association_id" => $itemAssociationId,
                            "created_at" =>  $createdAt,
                            "association_type" => [
                                "type_id" => $associationTypeNumber,
                                "type_name" => $associationTypeText,
                                "display_name" => $associationTypeText
                            ],
                            //"document" => $associatedDocument,
                            //"origin_node" => $originNode,
                            "source_node" => [
                                "item_id" => $destinationNodeId,
                                "parent_id" => $destinationNodeParentId,
                                "item_association_id" => $itemAssociationId,
                                "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                                "full_statement" => $destinationNodeFullStatement,
                                "node_type_title" => $destinationNodeTypeTitle,
                                "document" => $destinationNodeDocumentData,
                                "project_type"=>$projectType,
                                "project_id"  =>$projectId,
                                "is_external"=> $isexternalURI
                            ],
                            "description" => $description,
                            "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                            "sequence_number" => $sequenceNumber
                        ];
                        
                        array_push($parsedAssociationData, $parsedData);
                    }
                    
                }
                /**asociation with same taxonomy */
                else 
                {
                    
                    if($association->association_type != '1' && !in_array($association->association_type,$reverseAssociationArray)){    // added !in_array($association->association_type,$reverseAssociationArray)
                        $itemAssociationId      =   $association->item_association_id;
                        ///$originNode             =   $association->sourceItemId;
                        $associatedDocument     =   $association->sourceDocumentId;
                        $sequenceNumber         =   $association->sequence_number;
                        $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                        $associationTypeNumber  =   $association->association_type;
                        $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                        $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                        $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                        $description            =   !empty($association->description) ? $association->description: "";
                        $destinationNodeId      =   !empty($association->source_item_id) ? $association->source_item_id: '';
        
                        $destinationNodeTypeTitle           =   '';
                        $destinationNodeHumanCodingScheme   =   '';
                        $destinationNodeFullStatement       =   '';
                        $destinationNodeDocumentData        =   [
                            "document_id" => '',
                            "document_title" => ''
                        ];
                        
                        if($destinationNodeId!=='') {
                            
                            // destination node can be associated document of the self taxonomy
                            if($destinationNodeId===$associatedDocument->document_id) {
                                
                                $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                            '';
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';
        
                                // destination document details
                                $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                                $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                            }
                            else {
                                // destination node is an item/document of the self or other taxonomy within the same tenant
                                $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
        
        
                                if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                    $type = $destinationItemNode['type'];
                                    $nodeDetails = $destinationItemNode['data'];
                                    $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                                $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                                '';
                                    if($type==='item') {
                                        $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                        $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                        $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                                    }
                                    else {
                                        $destinationNodeHumanCodingScheme = '';
                                        $destinationNodeFullStatement = '';
                                        $destinationNodeParentId = '';
                                    }
                                } else {
                                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                                    } else {
                                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        } else {
                                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                        }
                                    }
                                    $destinationNodeParentId = '';
                                    
                                }
        
                                // fetch any destinantion node document detail within the same tenant
                                $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
        
                                if(empty($destinationNodeDocumentData['document_id'])) {
                                    $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                                    $destinationNodeDocumentData    =   [
                                        "document_id" => '',
                                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                    ];
                                }
                            }
                        } else {
                            $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1]) ? $externalNodeTitleExtracted[1] :'';
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2]) ? $externalNodeTitleExtracted[2] :'';
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3]) ? $externalNodeTitleExtracted[3] :'';
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0]) ? $externalNodeTitleExtracted[0] :'';
                                }
                                
                            }
                            $destinationNodeParentId = '';
        
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                        $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                        $projectType = 0;
                        $projectId  = '';
                        if($projectTypeData) {
                            $projectType     = $projectTypeData[0]['project_type'];
                            $projectId     = $projectTypeData[0]['project_id'];
                        }

                        if($projectType == '2') {
                            $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                            $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                        }
                        if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                        {
                            $isexternalURI = 1;
                        }
                        else {
                            $isexternalURI = 0;
                        }
                        $parsedData = [
                            "item_association_id" => $itemAssociationId,
                            "created_at" =>  $createdAt,
                            "association_type" => [
                                "type_id" => $associationTypeNumber,
                                "type_name" => $associationTypeText,
                                "display_name" => $associationTypeText
                            ],
                            "source_node" => [
                                "item_id" => $destinationNodeId,
                                "parent_id" => $destinationNodeParentId,
                                "item_association_id" => $itemAssociationId,
                                "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                                "full_statement" => $destinationNodeFullStatement,
                                "node_type_title" => $destinationNodeTypeTitle,
                                "document" => $destinationNodeDocumentData,
                                "project_type"=>$projectType,
                                "project_id"  =>$projectId,
                                "is_external"=> $isexternalURI
                            ],
                            "description" => $description,
                            "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                            "sequence_number" => $sequenceNumber
                        ];
                        array_push($parsedAssociationData, $parsedData);
                    
                    }
                }
            }else 
            {   
                // this script is for exemplar
                $itemAssociationId      =   $association->item_association_id;
                //$originNode             =   $association->originNode;
                $associatedDocument     =   $association->sourceDocumentId;
                $createdAt              =   !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                $associationTypeNumber  =   $association->association_type;
                $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);
                $externalNodeTitle      =   !empty($association->external_node_title) ? $association->external_node_title: "";
                $externalNodeUrl        =   !empty($association->external_node_url) ? $association->external_node_url: "";
                $description            =   !empty($association->description) ? $association->description: "";
                $destinationNodeId      =   !empty($association->source_item_id) ? $association->source_item_id : '';
                $destinationNodeTypeTitle           =   '';
                $destinationNodeHumanCodingScheme   =   '';
                $destinationNodeFullStatement       =   '';
                $destinationNodeDocumentData        =   [
                    "document_id" => '',
                    "document_title" => ''
                ];
                $sequenceNumber         =   $association->sequence_number;
                if($destinationNodeId!=='') {
                    
                    // destination node can be associated document of the self taxonomy
                    if($destinationNodeId===$associatedDocument->document_id) {
                        
                        $destinationNodeTypeTitle = !empty($associatedDocument->node_type_id) ? 
                                                    $this->_helperToReturnItemOrDocumentNodeTypeTitle($associatedDocument->node_type_id) : 
                                                    '';
                        $destinationNodeHumanCodingScheme = '';
                        $destinationNodeFullStatement = '';
                        $destinationNodeParentId = '';
                        // destination document details
                        $destinationNodeDocumentData['document_id'] = $destinationNodeId;
                        $destinationNodeDocumentData['document_title'] = !empty($associatedDocument->title) ? $associatedDocument->title : '';
                    }
                    else {
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';
                            }
                        } else {
                            $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                            if(sizeOf($externalNodeTitleExtracted) > 4) {
                                $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                            } else {
                                if(sizeOf($externalNodeTitleExtracted) > 1) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0])?$externalNodeTitleExtracted[0]:"";
                                }
                            }
                            $destinationNodeParentId = '';
                            
                        }
                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                } else {
                    $externalNodeTitleExtracted  = explode(":", $externalNodeTitle);
                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5])?$externalNodeTitleExtracted[5]:"";
                    } else {
                        if(sizeOf($externalNodeTitleExtracted) > 1) {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1]) ? $externalNodeTitleExtracted[1] :'';
                            $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2]) ? $externalNodeTitleExtracted[2] :'';
                            $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3]) ? $externalNodeTitleExtracted[3] :'';
                        } else {
                            $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[0]) ? $externalNodeTitleExtracted[0] :'';
                        }
                        
                    }
                    $destinationNodeParentId = '';
                    $destinationNodeDocumentData    =   [
                        "document_id" => '',
                        "document_title" => (sizeOf($externalNodeTitleExtracted)>4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                    ];
                }
                $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                $projectType = 0;
                $projectId   = '';
                if($projectTypeData) {
                    $projectType     = $projectTypeData[0]['project_type'];
                    $projectId       = $projectTypeData[0]['project_id'];
                }

                if($projectType == '2') {
                    $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                    $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                }
                if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                $parsedData = [
                    "item_association_id" => $itemAssociationId,
                    "created_at" =>  $createdAt,
                    "association_type" => [
                        "type_id" => $associationTypeNumber,
                        "type_name" => $associationTypeText,
                        "display_name" => $associationTypeText
                    ],
                    // "document" => $associatedDocument,
                    // "origin_node" => $originNode,
                    "source_node" => [
                        "item_id" => $destinationNodeId,
                        "parent_id" => $destinationNodeParentId,
                        "item_association_id" => $itemAssociationId,
                        "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                        "full_statement" => $destinationNodeFullStatement,
                        "node_type_title" => $destinationNodeTypeTitle,
                        "document" => $destinationNodeDocumentData,
                        "project_type"=>$projectType,
                        "project_id"=>$projectId,
                        "is_external"=> $isexternalURI
                    ],
                    "description" => $description,
                    "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                    "sequence_number" => $sequenceNumber
                ];
                array_push($parsedAssociationData, $parsedData);
            }
        }

        $parsedDestinationAssociationData = [];
        /** selected node is the source node */
        $itemAssociationsList = $getItemAssociations->toArray();
        $itemAssociationMetadataList = $this->getItemAssociationMetadata(array_column($itemAssociationsList,'item_association_id'));
        foreach($getItemAssociations as $association){
            if(!isset($itemAssociationMetadataList[$association->item_association_id]))
                $itemAssociationMetadataList[$association->item_association_id] = [];
            
            $destinationNodeTypeTitle           = '';
            $destinationNodeHumanCodingScheme   = '';
            $destinationNodeFullStatement       = '';
            $destinationNodeDocumentData        = [
                    "document_id" => '',
                    "document_title" => ''
                ];

            $associationTypeNumber  =   $association->association_type;
            $associationTypeText    =   $this->returnAssociationType($associationTypeNumber);

             if(in_array($associationTypeNumber, $reverseAssociationArray) && $association->destination_document_id != $Item->document_id ) {     // added && $association->destination_document_id != $Item->document_id
                if(!empty($association->target_document_id) && $association->target_document_id != $Item->document_id) {
                    if($associationTypeNumber == 3) {
                        $associationTypeText    =   'hasPart';
                    } else if($associationTypeNumber == 5) {
                        $associationTypeText    =   'succeeds';
                    } else if($associationTypeNumber == 7) {
                        $associationTypeText    =   'replaces';
                    }

                    $itemAssociationId          = $association->item_association_id;
                    $createdAt                  = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
            
                    //$originNode                 =   $association->destinationNode;
                    $externalNodeTitle          =   !empty($association->external_node_title) ? $association->external_node_title: "";
                    $externalNodeUrl            =   !empty($association->external_node_url) ? $association->external_node_url: "";
                    $description                =   !empty($association->description) ? $association->description: "";
                    $destinationNodeId          =   !empty($association->target_item_id) ? $association->target_item_id : '';

                    $associatedDocument         =   $association->targetDocumentId;
                    $sequenceNumber         =   $association->sequence_number;
                    if($destinationNodeId!=='') {
                        
                        // destination node is an item/document of the self or other taxonomy within the same tenant
                        $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);


                        if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                            $type = $destinationItemNode['type'];
                            $nodeDetails = $destinationItemNode['data'];
                            $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                        $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                        '';
                            if($type==='item') {
                                $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                            }
                            else {
                                $destinationNodeHumanCodingScheme = '';
                                $destinationNodeFullStatement = '';
                                $destinationNodeParentId = '';                              
                            }
                        } else {
                            $externalNodeTitleExtracted =   !empty($externalNodeTitle) ? explode(":", $externalNodeTitle) : "";

                            if(sizeOf($externalNodeTitleExtracted)>0) {
                                if(sizeOf($externalNodeTitleExtracted) > 4) {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5]) ? $externalNodeTitleExtracted[5]: "";
                                } else {
                                    $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                    $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                    $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[2]:"";
                                }
                            }
                        }

                        // fetch any destinantion node document detail within the same tenant
                        $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);

                        if(empty($destinationNodeDocumentData['document_id'])) {
                            $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                            $destinationNodeDocumentData    =   [
                                "document_id" => '',
                                "document_title" => (sizeOf($externalNodeTitleExtracted) > 4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                            ];
                        }
                    }
                    $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                    $projectType = 0;
                    $projectId   = '';
                    if($projectTypeData) {
                        $projectType     = $projectTypeData[0]['project_type'];
                        $projectId       = $projectTypeData[0]['project_id'];
                    }

                    if($projectType == '2') {
                        $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                        $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                    }
                    if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                    {
                        $isexternalURI = 1;
                    }
                    else {
                        $isexternalURI = 0;
                    }
                    $parsedDestinationData = [
                        "item_association_id" => $itemAssociationId,
                        "created_at" =>  $createdAt,
                        "association_type" => [
                            "type_id" => $associationTypeNumber,
                            "type_name" => $associationTypeText,
                            "display_name" => $associationTypeText
                        ],
                        //"document" => $associatedDocument,
                        //"origin_node" => $originNode,
                        "source_node" => [
                            "item_id" => $destinationNodeId,
                            "parent_id" => $destinationNodeParentId,
                            "item_association_id"   => $itemAssociationId,
                            "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                            "full_statement" => $destinationNodeFullStatement,
                            "node_type_title" => $destinationNodeTypeTitle,
                            "document" => $destinationNodeDocumentData,
                            "project_type"=>$projectType,
                            "project_id"=>$projectId,
                            "is_external"=> $isexternalURI
                        ],
                        "description" => $description,
                        "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                        "sequence_number" => $sequenceNumber
                    ];

                    array_push($parsedAssociationData, $parsedDestinationData);
                } else {
                    if($association) {
                        if($associationTypeNumber == 3) {
                            $associationTypeText    =   'hasPart';
                        } else if($associationTypeNumber == 5) {
                            $associationTypeText    =   'succeeds';
                        } else if($associationTypeNumber == 7) {
                            $associationTypeText    =   'replaces';
                        }
    
                        $itemAssociationId          = $association->item_association_id;
                        $createdAt                  = !empty($association->created_at) ? $association->created_at->toDateTimeString() : "";
                
                        //$originNode                 =   $association->destinationNode;
                        $externalNodeTitle          =   !empty($association->external_node_title) ? $association->external_node_title: "";
                        $externalNodeUrl            =   !empty($association->external_node_url) ? $association->external_node_url: "";
                        $description                =   !empty($association->description) ? $association->description: "";
                        $destinationNodeId          =   !empty($association->target_item_id) ? $association->target_item_id : '';
    
                        $associatedDocument         =   $association->targetDocumentId;
                        $sequenceNumber             =   $association->sequence_number;
                        if($destinationNodeId!=='') {
                            
                            // destination node is an item/document of the self or other taxonomy within the same tenant
                            $destinationItemNode = $this->_helperToReturnItemOrDocumentInTheSameTenant($destinationNodeId);
    
    
                            if(!empty($destinationItemNode['data']) && !empty($destinationItemNode['type'])) {
                                $type = $destinationItemNode['type'];
                                $nodeDetails = $destinationItemNode['data'];
                                $destinationNodeTypeTitle = !empty($nodeDetails['node_type_id']) ? 
                                                            $this->_helperToReturnItemOrDocumentNodeTypeTitle($nodeDetails['node_type_id']) : 
                                                            '';
                                if($type==='item') {
                                    $destinationNodeHumanCodingScheme = !empty($nodeDetails['human_coding_scheme']) ? $nodeDetails['human_coding_scheme'] : '';
                                    $destinationNodeFullStatement = !empty($nodeDetails['full_statement']) ? $nodeDetails['full_statement'] : '';
                                    $destinationNodeParentId = !empty($nodeDetails['parent_id']) ? $nodeDetails['parent_id'] : '';
                                }
                                else {
                                    $destinationNodeHumanCodingScheme = '';
                                    $destinationNodeFullStatement = '';
                                    $destinationNodeParentId = '';                              
                                }
                            } else {
                                $externalNodeTitleExtracted =   !empty($externalNodeTitle) ? explode(":", $externalNodeTitle) : "";
    
                                if(sizeOf($externalNodeTitleExtracted)>0) {
                                    if(sizeOf($externalNodeTitleExtracted) > 4) {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[3]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[4])?$externalNodeTitleExtracted[4]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[5]) ? $externalNodeTitleExtracted[5]: "";
                                    } else {
                                        $destinationNodeTypeTitle           = isset($externalNodeTitleExtracted[1])?$externalNodeTitleExtracted[1]:"";
                                        $destinationNodeHumanCodingScheme   = isset($externalNodeTitleExtracted[2])?$externalNodeTitleExtracted[2]:"";
                                        $destinationNodeFullStatement       = isset($externalNodeTitleExtracted[3])?$externalNodeTitleExtracted[2]:"";
                                    }
                                }
                            }
    
                            // fetch any destinantion node document detail within the same tenant
                            $destinationNodeDocumentData = $this->_helperToReturnDestinationNodeDocumentDetails($destinationNodeId);
    
                            if(empty($destinationNodeDocumentData['document_id'])) {
                                $externalNodeTitleExtracted =   explode(":", $externalNodeTitle);
                                $destinationNodeDocumentData    =   [
                                    "document_id" => '',
                                    "document_title" => (sizeOf($externalNodeTitleExtracted) > 4) ? $externalNodeTitleExtracted[0].":".$externalNodeTitleExtracted[1].":".$externalNodeTitleExtracted[2]: $externalNodeTitleExtracted[0],
                                ];
                            }
                        }
                        $projectTypeData = $this->projectRepository->getProjectItemMappings($destinationNodeId);
                        $projectType = 0;
                        $projectId   = '';
                        if($projectTypeData) {
                            $projectType   = $projectTypeData[0]['project_type'];
                            $projectId     = $projectTypeData[0]['project_id'];
                        }

                        if($projectType == '2') {
                            $itemAssociationsArray  =   $this->fetchAndSetAssociationOfItem($destinationNodeId);
                    
                            $itemAssociationId = $itemAssociationsArray[$destinationNodeId]['item_association_id'];
                        }
                        if(!empty($destinationNodeDocumentData) && $destinationNodeDocumentData['document_id'] == '')
                        {
                            $isexternalURI = 1;
                        }
                        else {
                            $isexternalURI = 0;
                        }
                        $parsedDestinationData = [
                            "item_association_id" => $itemAssociationId,
                            "created_at" =>  $createdAt,
                            "association_type" => [
                                "type_id" => $associationTypeNumber,
                                "type_name" => $associationTypeText,
                                "display_name" => $associationTypeText
                            ],
                            //"document" => $associatedDocument,
                            //"origin_node" => $originNode,
                            "source_node" => [
                                "item_id" => $destinationNodeId,
                                "parent_id" => $destinationNodeParentId,
                                "item_association_id" => $itemAssociationId,
                                "human_coding_scheme" => $destinationNodeHumanCodingScheme,
                                "full_statement" => $destinationNodeFullStatement,
                                "node_type_title" => $destinationNodeTypeTitle,
                                "document" => $destinationNodeDocumentData,
                                "project_type"=>$projectType,
                                "project_id"=>$projectId,
                                "is_external"=> $isexternalURI
                            ],
                            "description" => $description,
                            "metadata" => isset($itemAssociationMetadataList[$itemAssociationId]) ? $itemAssociationMetadataList[$itemAssociationId] : '',
                            "sequence_number" => $sequenceNumber
                        ];
    
                        array_push($parsedAssociationData, $parsedDestinationData);
                    }
                    
                }
            }
        }

        $sorted_array = array();
        foreach ($parsedAssociationData as $key => $row)
        {
            $sorted_array[$key] = $row['created_at'];
        }
        array_multisort($sorted_array, SORT_DESC, $parsedAssociationData);
        
        return $parsedAssociationData;
    }


    // new logic for linked associations end

    private function countOpenCommentsOnEachNode($itemIdentifier) {
        $countOpenComment   =   0;
        $attributeIn        =   'thread_source_id';
        $containedInValues  =   [$itemIdentifier];
        $returnCollection   =   true;
        $fieldsToReturn     =   ['thread_id', 'status'];
        $keyValuePairs      =   ['is_deleted'    => '0'];
        $notInKeyValuePairs =   ['status'   => 4];

        $openComments   =   $this->threadRepository->findByAttributesWithSpecifiedFieldsWithInQueryWithNotEqualAttributes(
                $attributeIn, 
                $containedInValues,
                $returnCollection,
                $fieldsToReturn,
                $keyValuePairs,
                $notInKeyValuePairs )->toArray();

        return count($openComments);
    }

    private function processDataToReturnAndSetIt() {
        $parsedCustomMetadata                                   =   [];
        $savedCustomMetadata                                    =   $this->getCustomMetadata();
        $Item                                                   =   $this->getItemDetailsFetchedFromDatabase();
        $arrayForCustomMetadataAsAdditionalMetadataToItem       =   $this->checkIfCustomMetadataIsAdditionalToItem();

        $displayText    =   '';
        if(!empty($Item->language_id)){
            $languageName = $Item->language->name ?: "";
            $languageShortcode = $Item->language->short_code ?: "";
            
            if($languageShortcode!=="") {
                if($languageName!=="") {
                    $displayText = ucwords($languageName) . " " . "(" . $languageShortcode . ")";
                }
                else {
                    $displayText = $languageShortcode;
                }
            }
        }
        $parsedCustomMetadataId = [];
        if(sizeOf($savedCustomMetadata) > 0) {
            foreach($savedCustomMetadata as $customMetadata)    {
                $fieldPossibleValues = !empty($customMetadata['field_possible_values']) ? explode(',' , $customMetadata['field_possible_values']) : '';
                $parsedCustomMetadataId[] = $customMetadata['pivot']['metadata_id'];
                $parsedCustomMetadata[]  = [
                    'metadata_id'               =>  $customMetadata['pivot']['metadata_id'],
                    'title'                     =>  $customMetadata['name'],
                    'field_type'                =>  $customMetadata['field_type'],
                    'field_possible_values'     =>  $fieldPossibleValues,  
                    'metadata_value'            =>  $customMetadata['pivot']['metadata_value'],
                    'metadata_value_html'       =>  $customMetadata['pivot']['metadata_value_html'],
                    'is_additional_metadata'    =>  $arrayForCustomMetadataAsAdditionalMetadataToItem[$customMetadata['pivot']['metadata_id']]
                ];                        
            } 
        }
        # Code to solve UF-762 start
        $customNodeTypeMetadata = DB::table('node_type_metadata')
                                    ->leftJoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
                                    ->where('node_type_id', $Item->node_type_id)
                                    ->where('metadata.is_custom', 1)
                                    ->whereNotIn('metadata.metadata_id', $parsedCustomMetadataId)
                                    ->get();
        foreach($customNodeTypeMetadata as $metadata) {
            $fieldPossibleValues = !empty($metadata->field_possible_values) ? explode(',' , $metadata->field_possible_values) : '';
            $parsedCustomMetadata[] = [
                'metadata_id'               =>  $metadata->metadata_id,
                'title'                     =>  $metadata->name,
                'field_type'                =>  $metadata->field_type,
                'field_possible_values'     =>  $fieldPossibleValues,
                'metadata_value'            =>  '',
                'metadata_value_html'       =>  '',
                'is_additional_metadata'    =>  0
            ];
        }
        # Code to solve UF-762 end

        // ACMT-821 (filter out exemplar associations from the item association list)
        $conditionArray = [
            [ "organization_id", "=", $this->getRequestUserOrganizationId()],
            [ "association_type", "!=", $this->getSystemSpecifiedAssociationTypeNumber("exemplar")],
            ["is_deleted", "=", "0"]
        ];

        //$itemAssociations               =   $Item->itemAssociations()->where($conditionArray)->get();

        //$itemDestinationAssociations    =   $Item->itemDestinationAssociations()->where($conditionArray)->get();

        // new logic for view associations (get data for forward and reverse associations) start

        $itemAssociations               =   $Item->itemAssociationsV2()->where($conditionArray)->get();

        $itemDestinationAssociations    =   $Item->itemDestinationAssociationsV2()->where($conditionArray)->get();
        
        // new logic for view associations (get data for forward and reverse associations) end
        
        $data = [
            "item_id"                   =>  !empty($Item->item_id) ? $Item->item_id : "",
            "source_item_id"            =>  !empty($Item->source_item_id) ? $Item->source_item_id : "",
            "parent_id"                 =>  !empty($itemAssociations->count()) ? $Item->parent_id : "",
            "cf_entity_type"            => "cf_item",
            "full_statement"            =>  !empty($Item->full_statement) ? $Item->full_statement : "",
            "full_statement_html"       =>  !empty($Item->full_statement_html) ? $Item->full_statement_html : "",
            "alternative_label"         =>  !empty($Item->alternative_label) ? $Item->alternative_label : "",
            "alternative_label_html"    =>  !empty($Item->alternative_label_html) ? $Item->alternative_label_html : "",
            "human_coding_scheme"       =>  !empty($Item->human_coding_scheme) ? $Item->human_coding_scheme : "",
            "human_coding_scheme_html"  =>  !empty($Item->human_coding_scheme_html) ? $Item->human_coding_scheme_html : "",
            "list_enumeration"          =>  !empty($Item->list_enumeration) ? str_replace(":","",$Item->list_enumeration) : "",
            "abbreviated_statement"     =>  !empty($Item->abbreviated_statement) ? $Item->abbreviated_statement : "",
            "abbreviated_statement_html"=>  !empty($Item->abbreviated_statement_html) ? $Item->abbreviated_statement_html : "",
            "education_level"           =>  !empty($Item->education_level) ? $Item->education_level : "",
            "notes"                     =>  !empty($Item->notes) ? $Item->notes : "",
            "notes_html"                =>  !empty($Item->notes_html) ? $Item->notes_html : "",
            "concept_id"                =>  !empty($Item->concept->concept_id) ? $Item->concept->concept_id : "",
            "concept_title"             =>  !empty($Item->concept->title) ? $Item->concept->title : "",
            "concept_title_html"        =>  !empty($Item->concept->title_html) ? $Item->concept->title_html : "",
            "concept_keywords"          =>  !empty($Item->concept->keywords) ? $Item->concept->keywords : "",
            "concept_hierarchy_code"    =>  !empty($Item->concept->hierarchy_code) ? $Item->concept->hierarchy_code : "",
            "concept_description"       =>  !empty($Item->concept->description) ? $Item->concept->description : "",
            "concept_description_html"  =>  !empty($Item->concept->description_html) ? $Item->concept->description_html : "",
            "license_id"                =>  !empty($Item->license_id) ? $Item->license_id : "",
            "license_title"             =>  !empty($Item->license->title) ? $Item->license->title : "",
            "license_title_html"        =>  !empty($Item->license->title_html) ? $Item->license->title_html : "",
            "license_description"       =>  !empty($Item->license->description) ? $Item->license->description : "",
            "license_description_html"  =>  !empty($Item->license->description_html) ? $Item->license->description_html : "",
            "license_text"              =>  !empty($Item->license->license_text) ? $Item->license->license_text : "",
            "license_text_html"         =>  !empty($Item->license->license_text_html) ? $Item->license->license_text_html : "",
            "language"                  =>  !empty($languageShortcode) ? $languageShortcode : "",
            "display_text"              =>  !empty($displayText) ? $displayText : "",
            "node_type_id"              =>  !empty($Item->node_type_id) ? $Item->node_type_id : "",
            "node_type"              =>  !empty($Item->nodeType->title) ? $Item->nodeType->title : "",
            "document_id"               =>  !empty($Item->document_id) ? $Item->document_id : "",
            "projects_associated"       =>  !empty($Item->project->count()) ? $this->parseProjectData($Item->project) : [],
            "item_associations"         =>  !empty($itemAssociations->count()) ? $this->parseCaseAssociationDataV2($itemAssociations, $itemDestinationAssociations, $Item) : [],
            "linked_item"               =>  $this->getLinkedAssociationV2($itemDestinationAssociations, $itemAssociations, $Item),
            "status_start_date"         =>  (substr($Item->status_start_date,0,10)!="0000-00-00") ? substr($Item->status_start_date,0,10) : "",
            "status_end_date"           =>  (substr($Item->status_end_date,0,10)!="0000-00-00") ? substr($Item->status_end_date,0,10) : "",
            "custom_metadata"           =>  $parsedCustomMetadata,
            "created_at"                =>  $this->formatDateTimeToISO8601($Item->created_at),
            "updated_at"                =>  $this->formatDateTimeToISO8601($Item->updated_at),
            "is_deleted"                =>  !empty($Item->is_deleted) ? $Item->is_deleted : 0,
            "open_comments_count"       =>  $this->countOpenCommentsOnEachNode($Item->item_id)
        ];
        $this->setParsedDataToReturn($data);
    }

    private function fetchDestinationDocumentDetails(string $identifier): array {
        // $item = $this->repository->find($identifier);
        // $organizationId = $this->getRequestUserOrganizationId();

        $itemAlreadyQueried = $this->helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb($identifier, "item");

        if(!empty($itemAlreadyQueried)) {
            $item = $itemAlreadyQueried;
        }
        else {
            // $preparedItemQuery = Item::where("organization_id", $organizationId)
            //                             ->where("item_id", $identifier)
            //                             ->orWhere("source_item_id", $identifier);
            
            $preparedItemQuery = Item::where("item_id", $identifier)->orWhere("source_item_id", $identifier);

            $itemCollection = $preparedItemQuery->get();
            $item = $itemCollection->isNotEmpty() ? $itemCollection->first() : [];
        }

        $this->helperToSetDocumentAndItemAlreadyQueriedFromDb($item, $identifier, "item");
        

        if(!empty($item)){
            $documentId = $item->document_id;
            $documentAlreadyQueried = $this->helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb($documentId, "document");
            $document = !empty($documentAlreadyQueried) ? $documentAlreadyQueried : $this->documentRepository->find($documentId);
            $this->helperToSetDocumentAndItemAlreadyQueriedFromDb($document, $documentId, "document");
            $documentTitle = !empty($document->title) ? $document->title : "";
        }
        else {
            $documentId = "";
            $documentTitle = "";
        }
        $dataToReturn = [
            "document_id" => $documentId,
            "document_title" => $documentTitle
        ];
        return $dataToReturn;
    }

    private function helperToSearchAndReturnDocumentAndItemAlreadyQueriedFromDb(string $searchIdentifier, string $type) {
        if($type==="document") {
            return !empty($this->_tempStorageForSavedDocument[$searchIdentifier]) ? 
                    $this->_tempStorageForSavedDocument[$searchIdentifier] : 
                    [];
        }
        else {
            return !empty($this->_tempStorageForSavedItem[$searchIdentifier]) ? 
                    $this->_tempStorageForSavedItem[$searchIdentifier] :
                    [];
        }
    }

    private function helperToSetDocumentAndItemAlreadyQueriedFromDb($dataToSet, string $identifier, string $type) {
        if($type==="document") {
            $this->_tempStorageForSavedDocument[$identifier] = $dataToSet;
        }
        else {
            $this->_tempStorageForSavedItem[$identifier] = $dataToSet;
        }
    }

    /**********************************************ACMT-1074,1075*************************************************/

    private function _helperToReturnItemOrDocumentInTheSameTenant(string $identifier): array {
        $dataToReturn = [
            'type' => '',
            'data' => []
        ];

        $organizationId = $this->getRequestUserOrganizationId();
        //Code is chnage for ACMT-2662
        $preparedItemQuery = Item::where('organization_id', $organizationId)->where(function ($q) use ($identifier) {
                               // return $q->where('item_id', $identifier)->orWhere('source_item_id', $identifier);
                               return $q->where('item_id', $identifier);
                            });

        $itemResult = $preparedItemQuery->get();
        if($itemResult->isNotEmpty()) {
            $itemNode = $itemResult->first()->toArray();
            $dataToReturn['type'] = 'item';
            $dataToReturn['data'] = $itemNode;
        }
        else {
            $preparedDocumentQuery = Document::where('organization_id', $organizationId)->where(function ($q) use ($identifier) {
                                        // return $q->where('document_id', $identifier)->orWhere('source_document_id', $identifier);
                                        return $q->where('document_id', $identifier);
                                    });
            $documentResult = $preparedDocumentQuery->get();
            if($documentResult->isNotEmpty()) {
                $documentNode = $documentResult->first()->toArray();
                $dataToReturn['type'] = 'document';
                $dataToReturn['data'] = $documentNode;
            }
        }
        return $dataToReturn;
    }

    private function _helperToReturnDestinationNodeDocumentDetails(string $identifier): array {
        $destinationDocumentNodeId = '';
        $dataToReturn = [
            "document_id" => '',
            "document_title" => ''
        ];

        $result = $this->_helperToReturnItemOrDocumentInTheSameTenant($identifier);

        if(!empty($result['data']) && !empty($result['type'])) {
            $data = $result['data'];
            $destinationDocumentNodeId = !empty($data['document_id']) ? $data['document_id'] : '';
            $destinationNodeDocument = $this->documentRepository->find($destinationDocumentNodeId);
            $dataToReturn['document_id'] = !empty($destinationNodeDocument->document_id) ? $destinationNodeDocument->document_id : '';
            $dataToReturn['document_title'] = !empty($destinationNodeDocument->title) ? $destinationNodeDocument->title : '';
        }

        return $dataToReturn;
    }

    private function _helperToReturnItemOrDocumentNodeTypeTitle(string $nodeTypeId): string {
        $titleToReturn = '';
        $nodeTypeSearchResult = $this->nodeTypeRepository->find($nodeTypeId);
        if(!empty($nodeTypeSearchResult->node_type_id) && !empty($nodeTypeSearchResult->title)) {
            $titleToReturn = $nodeTypeSearchResult->title;
        }
        return $titleToReturn;
    }

    private function getItemAssociationMetadata($itemAssociationIdList){
        $itemAssociationMetadataList = array();
        $itemAssociationMetadataArray = DB::table('item_association_metadata')
                            ->select([
                                'item_association_metadata.item_association_id',
                                'item_association_metadata.metadata_id',
                                'metadata.name',
                                'item_association_metadata.metadata_value',
                                'item_association_metadata.is_additional as is_mandatory',
                                'item_association_metadata.sort_order'
                                ])
                            ->leftJoin('metadata', 'item_association_metadata.metadata_id', '=', 'metadata.metadata_id')
                            ->whereIn('item_association_metadata.item_association_id',$itemAssociationIdList)
                            ->where('item_association_metadata.is_deleted', 0)
                            ->get()
                            ->toArray();
        foreach($itemAssociationMetadataArray as $key => $value){
            if($value->metadata_value==null)
                $value->metadata_value = "";
            $itemAssociationMetadataList[$value->item_association_id][] = $value;
        }
        
        return $itemAssociationMetadataList;
    }

}