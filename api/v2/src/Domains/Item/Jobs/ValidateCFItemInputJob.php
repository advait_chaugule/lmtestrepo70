<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Domains\Item\Validators\CFItemValidator;

class ValidateCFItemInputJob extends Job
{
    private $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CFItemValidator $validator)
    {
        $validation = $validator->validate($this->input);
        return $validation===true ? $validation : $validation->messages()->getMessages();
    }
}
