<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemRepositoryInterface;

class GetItemsMappedToDocumentJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $documentIdentifier)
    {
        $this->data = $documentIdentifier;
        $this->setDocumentIdentifier($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepositoryInterface)
    {
        $documentIdentifier = $this->data;
        $this->itemRepositoryInterface = $itemRepositoryInterface;

        return $this->itemRepositoryInterface->getItemsMappedWithDocument($documentIdentifier);
    }

    public function setDocumentIdentifier(string $identifier) {
        $this->documentIdentifier = $identifier; 
    }

    public function getDocumentIdentifier() {
        return $this->documentIdentifier;
    }

}
