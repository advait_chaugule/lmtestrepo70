<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\LanguageRepositoryInterface;
use App\Data\Repositories\Contracts\ItemRepositoryInterface;
use App\Data\Repositories\Contracts\NodeTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Data\Models\Item;
use App\Data\Models\ItemMetadata;
use App\Data\Models\NodeType;
use App\Data\Models\Organization;
use App\Services\Api\Traits\CaseFrameworkTrait;


use App\Domains\Search\Events\UploadSearchDataToSqsEvent;

class UpdateCFItemJob extends Job
{

    use CaseFrameworkTrait;
    private $data;
    private $itemIdentifier;
    private $customMetadataKeyValuePairs;
    private $nodeTypeRepository;
    private $domainName;
    private $itemRepo;
    private $languageRepository;

    /**
     * UpdateCFItemJob constructor.
     * @param array $input
     * @param $requestUrl
     */
    public function __construct(array $input,$requestUrl)
    {
        $this->data = $input;
        $this->setItemIdentifier($this->data['item_id']);
        $this->setCustomMetadataKeyValuePairs($input);
        $this->domainName = $requestUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemRepositoryInterface $itemRepo, LanguageRepositoryInterface $languageRepository, NodeTypeRepositoryInterface $nodeTypeRepository)
    {
        $attributes     =   [];
        $item_id = $this->data['item_id'];

        $this->itemRepo             =   $itemRepo;
        $this->languageRepository   =   $languageRepository;
        $this->nodeTypeRepository   =   $nodeTypeRepository;
       
        $requestData    =   $this->data;

        foreach($requestData as $key => $value){
            $attributes[$key] =   !(empty($value))? $value :   "";
        }
        $nodeTypeId = $requestData['node_type_id'];
        $nodeTypeTitle = NodeType::select('title')->where('node_type_id',$nodeTypeId)->get();
        $organizationCode       =   Organization::select('org_code')->where('organization_id', $attributes['auth_user']['organization_id'])->first()->org_code;
        $nodeTitle     = ['title'=>$nodeTypeTitle[0]->title,
                          'identifier'=>$nodeTypeId,
                          'uri'=>$this->getCaseApiUri("CFItemTypes",$nodeTypeId,true, $organizationCode,$this->domainName)
                          ];
                       
        $sourceNodeUriObj = json_encode($nodeTitle,true);    
        $attributes['source_node_type_uri_object'] =  $sourceNodeUriObj ;
        $attributes['import_type'] = 2;                  

        if(array_key_exists('status_start_date', $attributes) || array_key_exists('status_end_date', $attributes)) {
            if($attributes['status_start_date'] == '') {
                $attributes['status_start_date']    =   null;
            }

            if($attributes['status_end_date'] == '') {
                $attributes['status_end_date']    =   null;
            }
        }

        // parse basic language input and set language model
        $language = !empty($attributes["language"]) ? strtolower(trim($attributes["language"])) : "";
        $languageToSave = !empty($language) ? [ "short_code" => $language ] : [];

        $languageToSet = [];
        $attributes["language_id"] = ""; // If language is blank, set language_id to blank. Hence Initilaization - UF-3579
        if(!empty($languageToSave)) {
            $languageShortCodeToSearchFor = $languageToSave["short_code"];
            
            $savedLanguage = DB::table('languages')
                                ->select('language_id')
                                ->where('is_deleted','0')
                                ->where('short_code',$languageShortCodeToSearchFor)
                                ->where('organization_id',$attributes['auth_user']['organization_id'])
                                ->get()
                                ->toArray();
            
            $attributes["language_id"] = !empty($savedLanguage[0]->language_id) ? $savedLanguage[0]->language_id : "";
        }
        
        $this->updateAdditionalMetadataIfSimilarToCaseMetadata();
        $this->saveAndSetCustomMetadata();

        $this->raiseEventToUploadTaxonomySearchDataToSqs();
        
        return $this->itemRepo->edit($item_id, $attributes);
    }

    public function setItemIdentifier(string $identifier) {
        $this->itemIdentifier = $identifier; 
    }

    public function getItemIdentifier() {
        return $this->itemIdentifier;
    }

    public function setCustomMetadataKeyValuePairs(array $data) {
        if(array_key_exists('custom',$data))
        {
            $this->customMetadataKeyValuePairs = $data['custom'];
        }
        else
        {
            $data['custom']="";
            $this->customMetadataKeyValuePairs = $data['custom'];
        }
    }

    public function getCustomMetadataKeyValuePairs(){
        return $this->customMetadataKeyValuePairs;
    }

    private function updateAdditionalMetadataIfSimilarToCaseMetadata() {
        $existingCustomMetadataId                       =   [];
        $arrayOfMetadataInternalNameForNodeType         =   [];
        $itemId                                         =   $this->getItemIdentifier();
        $item                                           =   $this->itemRepo->getItemDetailsWithProjectAndCaseAssociation($itemId);
        $itemCustomMetadata                             =   $item->customMetadata()->where(['is_active' => '1', 'metadata.is_deleted' => '0'])->get();
        
        foreach($itemCustomMetadata as $existingCustomMetadata) {
            $existingCustomMetadataId[] =   $existingCustomMetadata->metadata_id;
        }

        $metadataList   =   $this->nodeTypeRepository->getNodeTypeMetadata($item->node_type_id);

        foreach($metadataList->metadata as $metadata) {
            $arrayOfMetadataInternalNameForNodeType[]  =   ['internal_name' =>  $metadata->internal_name, 'metadata_id' =>  $metadata->metadata_id];
        }

        $requestData    =   $this->data;

        if($existingCustomMetadataId !== null) {
            foreach($arrayOfMetadataInternalNameForNodeType as $metadataInternalName) {
                $value  =   !empty($requestData[$metadataInternalName['internal_name']]) ? $requestData[$metadataInternalName['internal_name']]: "";
                $value_html  =   !empty($requestData[$metadataInternalName['internal_name'].'_html']) ? $requestData[$metadataInternalName['internal_name'].'_html']: "";
                $isAdditional = 0;
                if(in_array($metadataInternalName['metadata_id'], $existingCustomMetadataId) === true) {
                    $count = ItemMetadata::where(['item_id'=>$itemId,'metadata_id'=>$metadata['metadata_id'],'is_deleted'=>'0'])->count();
                    if($count==1){
                        $this->itemRepo->addOrUpdateCustomMetadataValue($itemId, $metadataInternalName['metadata_id'], $value, $value_html, $isAdditional, 'edit');
                    }
                }
            }
        }
        
       /* $arr =[];
        foreach($arrayOfMetadataInternalNameForNodeType as $data) {
            if(in_array($data['metadata_id'],$existingCustomMetadataId)===true) {
                $arr [] =$data['metadata_id'];
            }
        }
        */
       // $this->itemRepo->updateIsDelete($itemId,$arr);
    }

    private function saveAndSetCustomMetadata() {
        $existingCustomMetadataId       =   [];
        $customMetadataSet              =   $this->getCustomMetadataKeyValuePairs();
        if(empty($customMetadataSet))
        {
           $itemId                         =   $this->getItemIdentifier();
           $customMetadataSet = $this->getCustomDataWhileTaxonomyIsCreated($itemId); 
        }
        $itemId                         =   $this->getItemIdentifier();
        $item                           =   $this->itemRepo->getItemDetailsWithProjectAndCaseAssociation($itemId);
        $itemCustomMetadata             =   $item->customMetadata()->where(['is_active' => '1', 'metadata.is_deleted' => '0'])->get();
        
        foreach($itemCustomMetadata as $existingCustomMetadata) {
            $existingCustomMetadataId[] =   $existingCustomMetadata->metadata_id;
        }

        // foreach($existingCustomMetadataId as $customMetadata) {
        //     $item->customMetadata()->detach($customMetadata);
        // }
       $arr = array();
        if($customMetadataSet !== null && count($customMetadataSet) > 0) {
            foreach($customMetadataSet as $metadata) {
                $value  =   !empty($metadata['metadata_value']) ? $metadata['metadata_value']: "";
                $value_html  =   !empty($metadata['metadata_value_html']) ? $metadata['metadata_value_html']: "";
                if(is_array($value_html))
                {
                    $value_html = "";
                }
                $isAdditional = 0;
                if(in_array($metadata['metadata_id'], $existingCustomMetadataId) === true) {
                    $count = ItemMetadata::where(['item_id'=>$itemId,'metadata_id'=>$metadata['metadata_id'],'is_deleted'=>'0'])->count();
                    if($count==1){
                        $this->itemRepo->addOrUpdateCustomMetadataValue($itemId, $metadata['metadata_id'], $value,$value_html, $isAdditional, 'edit');
                        }
                } else {
                    $count = ItemMetadata::where(['item_id'=>$itemId,'metadata_id'=>$metadata['metadata_id'],'is_deleted'=>'0'])->count();
                    if($count==0){
                        $this->itemRepo->addOrUpdateCustomMetadataValue($itemId, $metadata['metadata_id'], $value,$value_html, $isAdditional, 'insert'); 
                    }
                } 
                 
                $arr [] = $metadata['metadata_id'];
            }
        }
         
            $this->updateMetadataStatus($arr);
       
       
    }

    private function raiseEventToUploadTaxonomySearchDataToSqs() {
        $itemId = $this->getItemIdentifier();
        $sqsUploadEventConfigurations = config("_search.taxonomy.sqs_upload_events");
        $eventType = $sqsUploadEventConfigurations["update_item"];
        $eventData = [
            "type_id" => $itemId,
            "event_type" => $eventType
        ];
        event(new UploadSearchDataToSqsEvent($eventData));
    }

    private function updateMetadataStatus($arr)
    {
        $itemId                         =   $this->getItemIdentifier();
        $item                           =   $this->itemRepo->getItemDetailsWithProjectAndCaseAssociation($itemId);
        $nodeTypeId                     =   $this->data['node_type_id'];
        $nodeIdMappedWithMedataId = DB::table('node_type_metadata')
        ->rightjoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
        ->select('node_type_metadata.node_type_id','node_type_metadata.metadata_id','metadata.is_custom','metadata.name','metadata.internal_name')
        ->where('node_type_metadata.node_type_id',$nodeTypeId)
        ->where('metadata.is_custom','1')
        ->where('metadata.is_deleted','0')
        ->get()
        ->toArray();
  //dump($nodeIdMappedWithMedataId);
 
        $listAllmetadataarr = $arr;

        $listMetadataId = DB::table('item_metadata')
        ->select('metadata_id')
        ->where('is_deleted','0')
        ->where('item_id',$itemId)
        ->get()
        ->toArray();
        if(!empty($listMetadataId))
        {
            $listCustmetadataarr=array();
            foreach($listMetadataId as $listMetadataIdK => $listMetadataIdV)
            {
                $listCustmetadataarr[]=$listMetadataIdV->metadata_id;
            }
        }
  
      
    if(!empty($listCustmetadataarr) && !empty($listAllmetadataarr) )
    {
        $result = array_diff($listCustmetadataarr, $listAllmetadataarr);
   
        if(!empty($result))
        {
            foreach($result as $resultK => $resultV)
            {
                DB::table('item_metadata')->where(['metadata_id' => $resultV, 'item_id'   =>  $itemId, 'is_additional'=>0])->update(['is_deleted' => "1"]);
            }
        }
      
    }
    else
    {
       $value = "";
       $value_html = "";
       $isAdditional = 0;
        foreach($nodeIdMappedWithMedataId as $nodeIdMappedWithMedataIdK => $nodeIdMappedWithMedataIdV)
        {
            if(!empty($listCustmetadataarr))
            {
                if(in_array($nodeIdMappedWithMedataIdV->metadata_id, $listCustmetadataarr) === false) {
                    $count = ItemMetadata::where(['item_id'=>$itemId,'metadata_id'=>$nodeIdMappedWithMedataIdV->metadata_id,'is_deleted'=>'0'])->count();
                    if($count==0){
                        $this->itemRepo->addOrUpdateCustomMetadataValue($itemId, $nodeIdMappedWithMedataIdV->metadata_id, $value,$value_html, $isAdditional, 'insert');
                    }
                 }
            }
            else
            {
                $count = ItemMetadata::where(['item_id'=>$itemId,'metadata_id'=>$nodeIdMappedWithMedataIdV->metadata_id,'is_deleted'=>'0'])->count();
                    if($count==0){
                        $this->itemRepo->addOrUpdateCustomMetadataValue($itemId, $nodeIdMappedWithMedataIdV->metadata_id, $value,$value_html, $isAdditional, 'insert');
                    }
            }
           
        }
    }
        
    }

    public function getCustomDataWhileTaxonomyIsCreated($itemId)
    {
        $nodeTypeId                     =   $this->data['node_type_id'];

        $metadataId = array();
        $metadataId = ItemMetadata::select('metadata_id')
                    ->where('item_id',$itemId)
                    ->get()
                    ->toArray();                        

        $nodeIdMappedWithMedataId = array();                    
        $nodeIdMappedWithMedataId = DB::table('node_type_metadata')
                                    ->rightjoin('metadata', 'metadata.metadata_id', '=', 'node_type_metadata.metadata_id')
                                    ->select('node_type_metadata.node_type_id','node_type_metadata.metadata_id','metadata.is_custom','metadata.name','metadata.internal_name')
                                    ->where('node_type_metadata.node_type_id',$nodeTypeId)
                                    ->where('metadata.is_custom','1')
                                    ->where('metadata.is_deleted','0')
                                    ->get();

        
        $getMetadataIdArray = array();
        foreach($metadataId as $metadataIdK=>$metadataIdV)
        {
            $getMetadataIdArray[]=$metadataIdV['metadata_id'];
        }

        $getnodeIdMappedWithMetadataIdArray = array();
        $nodeIdMappedWithMedataIdData = json_decode($nodeIdMappedWithMedataId, true);
        
        foreach($nodeIdMappedWithMedataIdData as $nodeIdMappedWithMedataIdDataK=>$nodeIdMappedWithMedataIdDataV)
        {
            $getnodeIdMappedWithMetadataIdArray[]=$nodeIdMappedWithMedataIdDataV['metadata_id'];
        }
                                    
        $result = array();
        $result = array_diff($getMetadataIdArray, $getnodeIdMappedWithMetadataIdArray);
        
        if(!empty($result))
        {
            foreach($result as $resultK => $resultV)
            {
                DB::table('item_metadata')->where(['metadata_id' => $resultV, 'item_id'   =>  $itemId, 'is_additional'=>0])->update(['is_deleted' => "1"]);
            }
        }

    }

}