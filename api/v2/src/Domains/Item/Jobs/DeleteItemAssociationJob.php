<?php
namespace App\Domains\Item\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Contracts\ItemAssociationRepositoryInterface;

class DeleteItemAssociationJob extends Job
{
    private $itemAssociationIdentifier;
    private $deleteStatusData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $itemAssociationIdentifier)
    {
        $this->itemAssociationIdentifier = $itemAssociationIdentifier;
        $this->deleteStatusData = ["is_deleted" => 1];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ItemAssociationRepositoryInterface $itemAssociationRepository)
    {
        return $itemAssociationRepository->edit($this->itemAssociationIdentifier, $this->deleteStatusData);
    }
}
