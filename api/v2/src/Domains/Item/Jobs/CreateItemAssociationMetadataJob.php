<?php
namespace App\Domains\Item\Jobs;
use Lucid\Foundation\Job;
use DB;

class CreateItemAssociationMetadataJob extends Job
{

    public function __construct($userId,$organizationId,$itemAssociationId,$metadata,$nodeTypeId='')
    {
        $this->userId         = $userId;
        $this->organizationId = $organizationId;
        $this->itemAssociationId = $itemAssociationId;
        $this->metadata = $metadata;
        $this->nodeTypeId = $nodeTypeId;
    }

    public function handle()
    {
        $response = $this->CreateItemAssociationMetadata($this->userId,$this->organizationId,$this->itemAssociationId,$this->metadata,$this->nodeTypeId);
        return $response;
    }

    private function CreateItemAssociationMetadata($userId,$organizationId,$itemAssociationId,$metadata,$nodeTypeId){
         
        $date = date('Y-m-d H:i:s');
        foreach($metadata as $metadataK => $metadataV) {
            $AssociationMetaData[] = [
                'item_association_id'   =>  $itemAssociationId,
                'metadata_id'    =>  $metadataV['metadata_id'],
                'metadata_value' =>  $metadataV['metadata_value'],
                'is_additional' =>  $metadataV['is_additional'],
                'is_deleted'   =>  0,
                'created_at' =>  $date,
                'updated_at' => $date,
                'sort_order' => $metadataV['sort_order'],
                'node_type_id' => $nodeTypeId
            ];
          
        }
        
        if(!empty($AssociationMetaData))
        {

            foreach($AssociationMetaData as $AssociationMetaDataK=>$AssociationMetaDataV)
            {

                $count = DB::table('item_association_metadata')
                ->where('item_association_id',$AssociationMetaDataV['item_association_id'])
                ->where('metadata_id',$AssociationMetaDataV['metadata_id'])
                ->where('is_deleted',0)
                ->count();

                if($count==0)
                {
                    $item_metadata = DB::table('item_association_metadata')
                    ->insert(['item_association_id' => $AssociationMetaDataV['item_association_id'], 'metadata_id' => $AssociationMetaDataV['metadata_id'], 'metadata_value' => $AssociationMetaDataV['metadata_value'],
                     'is_additional' => $AssociationMetaDataV['is_additional'], 'is_deleted' => $AssociationMetaDataV['is_deleted'], 'created_at' => $AssociationMetaDataV['created_at'],
                    'updated_at' => $AssociationMetaDataV['updated_at'],'sort_order' => $AssociationMetaDataV['sort_order'],'node_type_id' => $AssociationMetaDataV['node_type_id']]);
                }    

            }
            

        }    

        return true;
    }
}