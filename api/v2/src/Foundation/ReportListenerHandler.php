<?php

namespace App\Foundation;
use Illuminate\Support\Facades\Log;
use App\Services\Api\Traits\ErrorMessageHelper;
use App\Services\Api\Traits\ZipHelperTrait;
use App\Services\Api\Traits\FileHelper;

use Illuminate\Support\Facades\Storage;

class ReportListenerHandler
{
    use ErrorMessageHelper, ZipHelperTrait, FileHelper;

    private $eventActivityConfigurations;
    private $eventTypeValueMapping;
    private $subEventTypeValueMapping;

    private $localStoragePath;
    private $reportEventStoragePath;
    private $S3Storage;
    private $s3AuditFolderName;

    public function __construct()
    {
        // set configurations for event activity tracking
        $this->setEventActivityConfigurations();

        // set event type value map
        $this->setEventTypeValueMapping();
        // set sub activity event type value map
        $this->setSubEventTypeValueMapping();

        // set zip archive and other file paths
        $this->setLocalStoragePath(storage_path());
        $this->setReportEventStoragePath();
        $this->setS3FileStorage();
        $this->setS3AuditFolderName();
    }

    public function logErrors(\Exception $exception) {
        $parsedErrorMessage = $this->createErrorMessageFromException($exception);
        $reportEventTrackingError = "\n\n## REPORT_EVENT LOG START ##\n\n".$parsedErrorMessage."\n\n## REPORT_EVENT LOG END ##\n\n";
        // set the log file path
       // Log::useFiles(storage_path().'/logs/report_events_error.log');
        // log the error message
        //Log::error($reportEventTrackingError);
        Log::error($exception);
    }

    public function setEventActivityConfigurations() {
        $this->eventActivityConfigurations = config('event_activity');
    }

    public function getEventActivityConfigurations(): array {
        return $this->eventActivityConfigurations;
    }

    public function setEventTypeValueMapping(){
        $eventActivityConfigs = $this->getEventActivityConfigurations();
        $this->eventTypeValueMapping = $eventActivityConfigs['event_type_value'];
    }

    public function getEventTypeValueMapping(): array {
        return $this->eventTypeValueMapping;
    }

    public function getEventTypeValue(string $eventType): int {
        return $this->getEventTypeValueMapping()[$eventType];
    }

    public function setSubEventTypeValueMapping(){
        $eventActivityConfigs = $this->getEventActivityConfigurations();
        $this->subEventTypeValueMapping = $eventActivityConfigs['sub_event_type_value'];
    }

    public function getSubEventTypeValueMapping(): array {
        return $this->subEventTypeValueMapping;
    }

    public function getSubEventTypeValue(string $subEventType): int {
        return $this->getSubEventTypeValueMapping()[$subEventType];
    }

    public function setLocalStoragePath(string $data) {
        $this->localStoragePath = $data;
    }

    public function getLocalStoragePath(): string {
        return $this->localStoragePath;
    }

    public function setReportEventStoragePath() {
        $eventActivityConfigs = $this->getEventActivityConfigurations();
        $this->reportEventStoragePath = $eventActivityConfigs["event_activity_local_storage_details"]["REPORT_EVENT_STORAGE_PATH"];
    }

    public function getReportEventStoragePath(): string {
        return $this->reportEventStoragePath;
    }

    public function setS3FileStorage() {
        $this->S3Storage = Storage::disk('s3');
    }

    public function getS3FileStorage() {
        return $this->S3Storage;
    }

    public function setS3AuditFolderName() {
        $eventActivityConfigs = $this->getEventActivityConfigurations();
        $this->s3AuditFolderName = $eventActivityConfigs["s3_details"]["ARCHIVE_FOLDER_NAME"];
    }

    public function getS3AuditFolderName(): string {
        return $this->s3AuditFolderName;
    }

    public function getStoragePathForBeforeAndAfterEventJson(): string {
        return $this->getLocalStoragePath() . "/" . $this->getReportEventStoragePath();
    }

    /**
     * persist before and after event json files in a folder identified by the activity id
     */
    public function storeBeforeAndAfterEventJsonFiles(string $activityLogId, string $beforeEventJson, string $afterEventJson) {
        // create a folder with activity log id in local storage
        $pathToStoreJsonFiles = $this->getStoragePathForBeforeAndAfterEventJson() . "/" . $activityLogId;
        $this->createFolder($pathToStoreJsonFiles);

        // prefix the folder created above with the vefeore and after event json files
        $beforeEventJsonFileName = $pathToStoreJsonFiles . "/" . $activityLogId . "_before_event.json";
        $afterEventJsonFileName = $pathToStoreJsonFiles . "/" . $activityLogId . "_after_event.json";
        
        // call the helper to create before and after json files
        $this->createFile($beforeEventJsonFileName, $beforeEventJson);
        $this->createFile($afterEventJsonFileName, $afterEventJson);
    }

    /**
     * Usage: zipFolderContents('folderNameToZip', 'output.zip'); 
     */
    public function archiveActivityFolder(string $folderToZip, string $activityZipFile) {
        $storagePath = $this->getStoragePathForBeforeAndAfterEventJson();
        $folderPathToArchive = $storagePath . "/" . $folderToZip;
        $outputArchivePath = $storagePath . '/'. $activityZipFile;
        // call zip helper method
        $this->zipDir($folderPathToArchive, $outputArchivePath);
    }

    public function flushReportEventLocalStorage(string $activityLogId) {
        $storagePath = $this->getStoragePathForBeforeAndAfterEventJson();

        $archiveFilePath = $storagePath . "/" . $activityLogId . ".zip";
        $this->deleteFile($archiveFilePath);

        $jsonContainerFolder = $storagePath . "/" . $activityLogId;
        $this->deleteFolder($jsonContainerFolder);
    }

    public function uploadFileToS3(string $fileName) {
        $s3Disk = $this->getS3FileStorage();
        $archiveLocalStoragePath = $this->getStoragePathForBeforeAndAfterEventJson();
        $filePath = $archiveLocalStoragePath . "/" . $fileName;
        $fileContent = $this->getContent($filePath);
        $s3Destination = $this->getS3AuditFolderName() . "/" . $fileName;
        return $s3Disk->put($s3Destination, $fileContent);
    }
    
}
