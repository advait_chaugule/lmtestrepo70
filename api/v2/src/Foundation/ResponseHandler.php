<?php

/**
 * Use this Controller to handle all api responses.
 * Set HTTP status codes and return the json to the client
 */

namespace App\Foundation;

use Symfony\Component\HttpFoundation\Response as IlluminateResponse;

class ResponseHandler
{    
  /**
   * Set default HTTP status code
   */
  private $httpStatusCode = IlluminateResponse::HTTP_OK;

  private $_customStatus = "Default custom status.";

  private $message = "Default message.";

  private $data = [];

  private $headers = [];

  private $response = [];

  private $is_success = '';

  /**
   * Set the HTTP status code
   * @param integer $statusCode
   * @return object
   */
  private function setHttpStatusCode($httpStatusCode)
  {
    $this->httpStatusCode = $httpStatusCode;
    return $this;
  }

  private function getHttpStatusCode()
  {
    return $this->httpStatusCode;
  }

  /**
   * Set the custom status code
   * @param string $_status
   * @return object
   */
  private function setCustomStatus($_status)
  {
    $this->_customStatus = $_status;
    return $this;
  }

  /**
   * This method created and returns a 4 digit custom status code to be injected inside response body
   * @return integer
   */
  private function getCustomStatusCode()
  {
    return $this->_customStatus;
  }

  /**
   * Set the api response message
   *
   * @param string $message
   * @return object
   */
  private function setMessage($message = "Default Message.")
  {
    $this->message = $message;
    return $this;
  }

  private function getMessage()
  {
    return $this->message;
  }

  /**
   * Set the status message to be returned as the reponse
   *
   * @param array $data
   * @return object
   */
  private function setIsSuccess($is_success = '')
  {
    $this->is_success = $is_success;
    return $this;
  }

  /**
   * Get the status message to be returned as the reponse
   *
   * @param array $data
   * @return object
   */
  private function getIsSuccess()
  {
    return $this->is_success;
  }

  /**
   * Set the data to be returned as the reponse
   *
   * @param array $data
   * @return object
   */
  private function setData($data = [])
  {
    $this->data = $data;
    return $this;
  }

  private function getData()
  {
    return $this->data;
  }

  /**
   * set the response headers
   *
   * @param array $headers
   * @return void
   */
  private function setHeaders($headers = [])
  {
    $this->headers = $headers;
    return $this;
  }

  private function getHeaders()
  {
    return $this->headers;
  }

  /**
   * Create the api response structure for success
   * @return array
   */
  private function createSuccessResponse()
  {
    // $this->response = [
    //                       // '_status' => $this->getCustomStatusCode(),
    //                       '_status' => "acmt_200",
    //                       'response' => [
    //                         'message' => $this->getMessage(),
    //                         'data' => $this->getData(),
    //                         'http_status' => $this->getHttpStatusCode(),
    //                       ]
    //                   ];
    $this->response = [
                        'status' => $this->getHttpStatusCode(),
                        'data' => $this->getData(),
                        'message' => $this->getMessage(),
                        'success' => $this->getIsSuccess(),
                      ];
    return $this;
  }

  /**
   * Create the api response structure for error
   * @return array
   */
  private function createErrorResponse()
  {
    // $this->response = [
    //                       // '_status' => $this->getCustomStatusCode(),
    //                       '_status' => 'acmt_400',
    //                       'response' => [
    //                         'message' => $this->getMessage(),
    //                         'data' => null,
    //                         'http_status' => $this->getHttpStatusCode()
    //                       ]
    //                   ];
                      
    $this->response = [
                        'status' => $this->getHttpStatusCode(),
                        'data' => null,
                        'message' => $this->getMessage()
                      ];
    return $this;
  }

  private function getResponse()
  {
    return $this->response;
  }

  /**
   * Use this method to respond when resource is created
   * @param string $message
   * @return JSON Response
   */
  public function respondCreated(
    $data = [], 
    $message = 'Resource Created!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_CREATED)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setData($data)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource is created
   * @param string $message
   * @return JSON Response
   */
  public function respondCustomCreated(
    $data = [], 
    $message = 'Resource Created!',
    $_status = 'custom_status',
    $headers = [],
    $is_success = ''
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_CREATED)
                ->setCustomStatus($_status)
                ->setIsSuccess($is_success)
                ->setMessage($message)
                ->setData($data)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource(s) is updated
   * @param string $message
   * @return JSON Response
   */
  public function respondUpdated(
    $data = [], 
    $message = 'Resource Updated!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setData($data)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when there is validation error
   * @param string $message
   * @return JSON Response
   */
  public function respondCustomValidationError(
    $data = [],
    $message = 'Validation Error!',
    $_status = 'custom_status',
    $headers = [],
    $is_success = ''
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setIsSuccess($is_success)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setData($data)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource(s) is updated
   * @param string $message
   * @return JSON Response
   */
  public function respondDeleted(
    $message = 'Resource Deleted!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource(s) is found
   * @param string $message
   * @return JSON Response
   */
  public function respondFound(
    $data = [], 
    $message = 'Data Found!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setData($data)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource(s) is found
   * @param string $message
   * @return JSON Response
   */
  public function respondCustomFound(
    $data = [], 
    $message = 'Data Found!',
    $_status = 'custom_status',
    $headers = [],
    $is_success = ''
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setCustomStatus($_status)
                ->setIsSuccess($is_success)
                ->setMessage($message)
                ->setData($data)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource not found
   * @param string $message
   * @return JSON Response
   */
  public function respondNotFound(
    $message = 'Not Found!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when resource not found with success parameter
   * @param string $message
   * @return JSON Response
   */
  public function respondCustomNotFound(
    $message = 'Not Found!',
    $_status = 'custom_status',
    $headers = [],
    $is_success = ''
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_OK)
                ->setCustomStatus($_status)
                ->setIsSuccess($is_success)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createSuccessResponse()
                ->respond();
  }

  /**
   * Use this method to respond when any internal error occurred
   * @param string $message
   * @return JSON Response
   */
  public function respondInternalError(
    $message = 'Internal Error!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createErrorResponse()
                ->respond();
  }

  /**
   * Use this method to respond when validation error occurred
   * @param string $message
   * @return JSON Response
   */
  public function respondValidationError(
    $message = 'Validation Error!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createErrorResponse()
                ->respond();
  }

  /**
   * Use this method to respond when authorization error occurred
   * @param string $message
   * @return JSON Response
   */
  public function respondAuthorizationError(
    $message = 'Autorization Error!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_UNAUTHORIZED)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createErrorResponse()
                ->respond();
  }

  /**
   * Use this method to respond when permission error occurred
   * @param string $message
   * @return JSON Response
   */
  public function respondPermissionError(
    $message = 'Permission Error!',
    $_status = 'custom_status',
    $headers = []
  )
  {
    return $this->setHttpStatusCode(IlluminateResponse::HTTP_FORBIDDEN)
                ->setCustomStatus($_status)
                ->setMessage($message)
                ->setHeaders($headers)
                ->createErrorResponse()
                ->respond();
  }

  /**
   * Use this method to create and return a generic http response
   * @return JSON Response
   */
  private function respond()
  {
    return response()->json($this->getResponse(), $this->getHttpStatusCode(), $this->getHeaders());
  }

}
