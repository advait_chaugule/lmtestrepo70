<?php

namespace App\Foundation;

use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException as Exception;

/**
 * This base class will set AWS sqs configurations
 * load them and instantiate SQS clent
 */
class BaseSqsHandler
{

    protected $sqsConfigurations;
    protected $sqsClient;
    protected $sqsQueueUrl;

    public function __construct()
    {
        $this->setSqsConfigurations();
        $this->configureAndSetSqsClient();
        $this->setSqsQueueUrl();
    }

    public function setSqsConfigurations() {
        $this->sqsConfigurations = config('sqs_search_upload');
    }

    public function getSqsConfigurations(): array {
        return $this->sqsConfigurations;
    }

    public function getSqsClient(): SqsClient {
        return $this->sqsClient;
    }

    public function setSqsQueueUrl() {
        $sqsSearchUploadConfigurations = $this->getSqsConfigurations();
        $sqsQueueName = $sqsSearchUploadConfigurations["AWS_SQS_QUEUE"];
        $sqsPrefix = $sqsSearchUploadConfigurations["AWS_SQS_PREFIX"];
        $this->sqsQueueUrl = "{$sqsPrefix}/{$sqsQueueName}";
    }

    public function getSqsQueueUrl(): string {
        return $this->sqsQueueUrl;
    }

    private function configureAndSetSqsClient() {
        $sqsSearchUploadConfigurations = $this->getSqsConfigurations();
        $awsKey = $sqsSearchUploadConfigurations["AWS_KEY"];
        $awsSecret = $sqsSearchUploadConfigurations["AWS_SECRET"];
        $version = $sqsSearchUploadConfigurations["API_VERSION"];
        //$profile = $sqsSearchUploadConfigurations["PROFILE"];
        $region = $sqsSearchUploadConfigurations["REGION"];
        $clientConfigurations = [
            'credentials' => [
                'key' => $awsKey,
                'secret' => $awsSecret
            ],
            'version' => $version,
            //'profile' => $profile,
            'region' => $region
        ];
        // load configurations, initialse and set sqs client
        $this->sqsClient = new SqsClient($clientConfigurations);
    }

}
