<?php

namespace App\Foundation;

class BaseValidation
{
    public function make(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
      return $this->getValidationFactory()->make($data, $rules, $messages, $customAttributes);
    }
    
    public function getValidationFactory()
    {
      return app('Illuminate\Contracts\Validation\Factory');
    }
}