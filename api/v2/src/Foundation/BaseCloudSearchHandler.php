<?php

namespace App\Foundation;

use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Aws\CloudSearchDomain\Exception\CloudSearchDomainException as Exception;

/**
 * This base class will set cloud search configurations
 * load them and instantiate cloud search clent
 */
class BaseCloudSearchHandler
{

    protected $searchConfigurations;
    protected $searchClient;

    public function __construct()
    {
        $this->setSearchConfigurations();
        try{
            $this->configureAndSetCloudSearchClient();
        }
        catch(Exception $ex) {
            // dd($ex->getMessage());
        }
    }

    public function setSearchConfigurations() {
        $this->searchConfigurations = config('cloud_search');
    }

    public function getSearchConfigurations(): array {
        return $this->searchConfigurations;
    }

    public function getSearchClient(): CloudSearchDomainClient {
        return $this->searchClient;
    }

    private function configureAndSetCloudSearchClient() {
        $searchConfigurations = $this->getSearchConfigurations();
        // set the aws cloud search search client configurations
        $awsKey     = $searchConfigurations['AWS_KEY'];
        $awsSecret  = $searchConfigurations['AWS_SECRET'];
        $endpoint   = $searchConfigurations['SEARCH_ENDPOINT'];
        $version    = $searchConfigurations['API_VERSION'];

        if(!empty($awsKey) && !empty($awsSecret) && !empty($endpoint) && !empty($version)) {
            $searchClientConfigurations = [
                'credentials' => [
                    'key' => $awsKey,
                    'secret' => $awsSecret,
                    'token' => ''
                ],
                'endpoint' => $endpoint,
                'version' => $version
            ];
            // load configurations, initialse and set cloud search client
            $this->searchClient = new CloudSearchDomainClient($searchClientConfigurations);
        }
    }

}
