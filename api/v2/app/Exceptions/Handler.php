<?php

namespace Framework\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Data\Models\Report\ErrorLog;
use Illuminate\Support\Collection;
use App\Services\Api\Traits\UuidHelperTrait;

class Handler extends ExceptionHandler
{
    use UuidHelperTrait;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        $status  = config('db_logging.db_error_log');
        $request = \Request::url();
        $url     = parse_url($request);
        $path    = $url['path'];
        $realPath = str_replace('/api/v1/', '', $path);
        $pathArr =  explode(" ",$realPath);
        if($status==true) {
            $data = [
                'id'      => $this->createUniversalUniqueIdentifier(),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine(),
                'message' => $exception->getMessage(),
                'trace'   => $exception->getTraceAsString(),
            ];

            $service = config('acmt_services');
            $arrUrl  = $service['url'];
            $arr = [];
            foreach ($service as $arr =>$val) {
                $arr = array_keys($val);

            }
            $getMatchedKey = '';
            foreach($pathArr as $pathArrKey=>$pathArrValue)
            {
                foreach($arr as $arrKey=>$arrValue)
                {
                    if (strpos($pathArrValue, $arrValue) !== false)
                    {
                        $getMatchedKey = $arrValue;
                    }
                }
            }
            $serviceId = '';
            if (array_key_exists($getMatchedKey,$arrUrl )){
                    $matchData = $arrUrl[$getMatchedKey];
                    $serviceId =  $matchData['service_id'];
            }

            $dataArr =
                 ['id'                => $data['id'],
                 'service_logical_id' => $serviceId,
                 'error_summary'      => 'Line '.$data['line'].' '.$data['message'],
                 'trace_log'          => $data['trace']
                 ];
            ErrorLog::create($dataArr);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            // handle here
            return response()->json(['error' => [               
                "message" => '404'
               ]], 404);
        }

        $appEnv = env("APP_ENV");
        if(!empty($appEnv) && ($appEnv==="local" || $appEnv==="dev" || $appEnv==="qa")){
            return response()->json(['error' => [
                    "file_path" => $exception->getFile(),
                    "line_number" => $exception->getLine(),
                    "message" => $exception->getMessage()
                   ]], 500);
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
