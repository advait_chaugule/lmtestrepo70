<?php

namespace Framework\Exceptions;

use Exception;

class MyGzException extends Exception
{
    //
    public function report()
    {
        \Log::debug('gzuncompress error');
    }
}
