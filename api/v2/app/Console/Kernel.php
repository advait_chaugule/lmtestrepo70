<?php

namespace Framework\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

// use App\Services\Api\Console\Commands\ProcessEventTrackingLog;
// use App\Services\Api\Console\Commands\SyncDataWithReportingDatabase;
// use App\Services\Api\Console\Commands\UnzipExemplarAssetsOnS3;
// use App\Services\Api\Console\Commands\TestCommand;

use App\Services\Api\Console\Commands as ApiServiceCommands;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ApiServiceCommands\TestCommand::class,
        ApiServiceCommands\ProcessEventTrackingLog::class,
        ApiServiceCommands\SyncDataWithReportingDatabase::class,
        ApiServiceCommands\UnzipExemplarAssetsOnS3::class,
        ApiServiceCommands\CreateAndUploadTaxonomySearchData::class,
        ApiServiceCommands\FlushAllSearchData::class,
        ApiServiceCommands\CreateAndUploadAllTaxonomiesSearchData::class,
        ApiServiceCommands\UploadDeltaOrFullTaxonomySearchData::class,
        ApiServiceCommands\CreateAndUploadAllProjectSearchData::class,
        ApiServiceCommands\ChangeTaxonomyStatus::class,
        ApiServiceCommands\FlushAllDataFromCache::class,
        ApiServiceCommands\AddMetadataForPrevCreatedTenant::class,
        ApiServiceCommands\AutomaticallyStopPublicReview::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // set scheduled task to process event tracking logs (Run the task every day at midnight)
        $schedule->command(ApiServiceCommands\ProcessEventTrackingLog::class)->daily();
        // set scheduled task to sync data from application database to reporting database (Run the task every day at midnight)
        $schedule->command(ApiServiceCommands\SyncDataWithReportingDatabase::class)->daily();
        // set scheduled task to upload data to cloud search
        $schedule->command(ApiServiceCommands\UploadDeltaOrFullTaxonomySearchData::class)->everyMinute()->withoutOverlapping();
        //$schedule->command(ApiServiceCommands\CreateAndUploadAllProjectSearchData::class)->everyMinute()->withoutOverlapping();
        // run all taxonomy sync with cloud search every weekend
        if(config('app.env')=='prod') {
           // $schedule->command(ApiServiceCommands\CreateAndUploadAllTaxonomiesSearchData::class)->saturdays();
        }
        // set to automatically stop and start the public review that have expired(exceeded the end date) and start(exceeded the start date)
        $schedule->command(ApiServiceCommands\AutomaticallyStopPublicReview::class)->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
