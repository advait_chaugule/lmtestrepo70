<?php

namespace Framework\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Event;
use Storage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
         Schema::defaultStringLength(191);
         if(env("FORCE_HTTPS", false)){
            \URL::forceScheme('https');
         }
      if(env("QUERY_DEBUGGER_MODE", false))
        {
            Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) { 
                $query = vsprintf(str_replace('?', "'%s'", $query->sql), $query->bindings)." ".$query->time.'ms'.PHP_EOL;
                Storage::append('sql.log',$query);
            });
        }        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
