<?php

namespace Framework\Listeners\App\Listeners;

use Framework\Events\App\Events\NotificationsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationsEventListeners
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationsEvent  $event
     * @return void
     */
    public function handle(NotificationsEvent $event)
    {
        //
    }
}
