<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp()
    {
        parent::setUp();
        
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
    }

    public function tearDown()
    {
        parent::tearDown();
    }

}