<?php

use Illuminate\Database\Seeder;
use App\Services\Api\database\seeds\report\DummyDimUserSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(App\Services\Api\database\seeds\UserSeeder::class);        
       
        $this->call(App\Services\Api\database\seeds\LanguageSeeder::class);
        $this->call(App\Services\Api\database\seeds\SubjectSeeder::class);
        $this->call(App\Services\Api\database\seeds\LicenseSeeder::class);
        $this->call(App\Services\Api\database\seeds\ItemTypeSeeder::class);
        $this->call(App\Services\Api\database\seeds\RoleSeeder::class);
        $this->call(App\Services\Api\database\seeds\OrganizationSeeder::class);
        $this->call(App\Services\Api\database\seeds\PermissionSeeder::class);
        $this->call(App\Services\Api\database\seeds\RolePermissionSeeder::class);
        $this->call(App\Services\Api\database\seeds\StageSeeder::class);
        $this->call(App\Services\Api\database\seeds\WorkflowSeeder::class);
        $this->call(App\Services\Api\database\seeds\WorkflowStageSeeder::class);

        //METADATA SEEDERS
        $this->call(App\Services\Api\database\seeds\NodeTypeSeeder::class);
        $this->call(App\Services\Api\database\seeds\MetadataSeeder::class);
        $this->call(App\Services\Api\database\seeds\NodeTypeMetadataSeeder::class);
        
        // Dummy data seeders
        if($this->runDummySeeders()){
            $this->call(
                [
                    App\Services\Api\database\seeds\DummyUserSeeder::class,
                    App\Services\Api\database\seeds\DummyProjectSeeder::class
                ]
            );
        }
    }

    private function runDummySeeders(): bool {
        return config('database.seed_dummy_data_status');
    }
}
