<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcmtOrganizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizations', function ($table) {
            $table->integer('is_active')->default(1)->after('is_deleted');
            $table->string('created_by',36)->default('None')->after('created_at'); //the after method is optional.
            $table->string('updated_by',36)->default('None')->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations', function ($table) {
            $table->dropColumn(['is_active','created_by','updated_by']);
        });
    }
}
