<?php

return [

    /*
    | This will contain document, item, node_type sample data for testing
    */

    'document' => [
        'document_id' => 'dbe33507-c16d-43c2-a612-837c8b821d99',
        'title' => 'Case Document #1',
        'official_source_url' => 'http://www.test-taxonomy.com',
        'adoption_status' => 2,
        'notes' => 'sample note',
        'publisher' => 'Smarter Balanced Assessment Consortium',
        'description' => 'description',
        'version' => '1',
        'status_start_date' => '2018-05-05',
        'status_end_date' => '2018-06-10',
        'creator' => 'test1',
        'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
        'created_at' => '2018-11-26 07:19:00',
        'updated_at' => '2018-05-06 02:04:10',
        'source_document_id' => 'e0821446-2685-4a8a-9e2d-0bf3a09d06ed',
        'node_type_id' => '613e8742-feb0-4fb8-a224-81bc31acf008',
        'updated_by' => '4fa46dbd-e314-40cb-9439-929b94779fd6',
    ],
    'items' => [
        [
          'item_id' => 'f1752575-0b8d-47c3-b6d7-fcf4889fe9d5',
          'parent_id' => '',
          'document_id' => 'dbe33507-c16d-43c2-a612-837c8b821d99',
          'full_statement' => 'Child1',
          'alternative_label' => 'Test alternative label text',
          'human_coding_scheme' => 'CD.C1',
          'list_enumeration' => 'CD.C1',
          'abbreviated_statement' => 'test abbreviated statment',
          'notes' => 'test notes',
          'node_type_id' => '3afbc51a-9781-4bd5-9871-a494252ef100',
          'education_level' => 'k1,k2',
          'status_start_date' => '2018-05-05',
          'status_end_date' => '2018-06-06',
          'created_at' => '2018-11-26 07:24:31',
          'updated_at' => '2018-05-06 02:07:18',
          'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
          'source_item_id' => '5830c5e8-db60-4fc9-890e-a13fe41d7615',
          'updated_by' => '4fa46dbd-e314-40cb-9439-929b94779fd6',
        ],
        [
          'item_id' => '6b7905f3-082c-4a30-abf1-b82259294504',
          'parent_id' => '',
          'document_id' => 'dbe33507-c16d-43c2-a612-837c8b821d99',
          'full_statement' => 'Child2',
          'alternative_label' => 'Test alternative label text',
          'human_coding_scheme' => 'CD.C2',
          'list_enumeration' => 'CD.C2',
          'abbreviated_statement' => 'test2 abbreviated statment',
          'notes' => 'test2 notes',
          'node_type_id' => '0d555875-b02b-4c73-ad27-d3d3b9e1a234',
          'education_level' => 'k1',
          'status_start_date' => '2018-05-05',
          'status_end_date' => '2018-06-06',
          'created_at' => '2018-11-26 07:24:31',
          'updated_at' => '2018-05-06 02:07:18',
          'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
          'source_item_id' => '7a54203b-f428-4f28-a8c1-3bd1323fb8a5',
          'updated_by' => '4fa46dbd-e314-40cb-9439-929b94779fd6'
        ]
    ],
    'node_types' => [
        [
          'node_type_id' => '3afbc51a-9781-4bd5-9871-a494252ef100',
          'title' => 'test item type1',
          'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
          'description' => 'test dewscription item type',
          'hierarchy_code' => '1,c',
          'type_code' => 'xc',
          'updated_at' => '2018-11-26 07:36:16',
          'source_node_type_id' => 'd3127cf6-775e-422f-bbff-9eea1785d8d6',
          'created_by' => '4fa46dbd-e314-40cb-9439-929b94779fd6',
          'created_at' => '2018-11-26 07:36:16'
        ],
        [
          'node_type_id' => '0d555875-b02b-4c73-ad27-d3d3b9e1a234',
          'title' => 'test item type2',
          'organization_id' => 'a83db6c0-1a5e-428e-8384-c8d58d2a83ff',
          'description' => 'test dewscription item type2',
          'hierarchy_code' => '1,2',
          'type_code' => '33',
          'updated_at' => '2018-11-26 07:36:16',
          'source_node_type_id' => 'd2d74e30-7372-4430-9ed1-3c5186b65f9d',
          'created_by' => '4fa46dbd-e314-40cb-9439-929b94779fd6',
          'created_at' => '2018-11-26 07:36:16'
        ]
    ]

];
