<?php

return [

		/*
    |-------------------------------------------------------------------------------------
    | Store configurations related to Node Name and Metadata Name
    |-------------------------------------------------------------------------------------
    |
    */	

				"NodeTypeArray" => [
					"0"=>"Default",
					"1"=>"Strand",
					"2"=>"Conceptual Category",
					"3"=>"Component",
					"4"=>"Grade Level",
					"5"=>"Cluster",
					"6"=>"Document",
					"7"=>"Standard",
					"8"=>"Domain"
				],


				"DocumentOrderArr" => [
					"0"=>"Creator",
					"1"=>"Title",
					"2"=>"Official Source URL",
					"3"=>"Publisher",
					"4"=>"Description",
					"5"=>"Subject Title",
					"6"=>"Subject Hierarchy Code",
					"7"=>"Subject Description",
					"8"=>"Language Name",
					"9"=>"Version",
					"10"=>"Status Start Date",
					"11"=>"Status End Date",
					"12"=>"License Title",
					"13"=>"License Description",
					"14"=>"License Text",
					"15"=>"Notes",
				],

				"NondocumentOrderArr" => [
					"0"=>"Full Statement",
					"1"=>"Alternative Label",
					"2"=>"Official Source URL",
					"3"=>"Human Coding Scheme",
					"4"=>"List Enumeration",
					"5"=>"Abbreviated Statement",
					"6"=>"Concept Title",
					"7"=>"Concept Keywords",
					"8"=>"Concept Hierarchy Code",
					"9"=>"Concept Description",
					"10"=>"Notes",
					"11"=>"Language Name",
					"12"=>"Education Level",
					"13"=>"License Title",
					"14"=>"License Description",
					"15"=>"License Text",
					"16"=>"Status Start Date",
					"17"=>"Status End Date",
				]
				
		];		
				