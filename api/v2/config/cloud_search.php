<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store configurations related to cloud search
    |-------------------------------------------------------------------------------------
    |
    */

    "AWS_KEY" => env('AWS_SEARCH_KEY'),
    "AWS_SECRET" => env('AWS_SEARCH_SECRET'),
    "SEARCH_ENDPOINT" => env('AWS_SEARCH_ENDPOINT'),
    "API_VERSION" => '2013-01-01',
    "BATCH_CHUNK_COUNT" => 3000,
    "CHUNKED_BATCH_EXECUTION_DELAY_IN_SECONDS" => 11

];
