<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | Laravel's queue API supports an assortment of back-ends via a single
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "sync", "database", "beanstalkd", "sqs", "redis", "null"
    |
    */

    'default' => env('QUEUE_DRIVER', 'sync'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'retry_after' => 90,
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host' => 'localhost',
            'queue' => 'default',
            'retry_after' => 90,
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key' => env("AWS_KEY", "AKIAJYLGG5IQHTQG4QQQ"),
            'secret' => env("AWS_SECRET", "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"),
            'prefix' => env("AWS_SQS_PREFIX", "https://sqs.us-east-1.amazonaws.com/496891639123"),
            'queue' => env("AWS_SQS_QUEUE", "acmtdev"),
            'region' => env("AWS_REGION", "us-east-1")
        ],

        'import_json_sqs' => [
            'driver' => 'sqs',
            'key' => env("AWS_KEY", "AKIAJYLGG5IQHTQG4QQQ"),
            'secret' => env("AWS_SECRET", "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"),
            'prefix' => env("AWS_SQS_IMPORT_JSON_PREFIX", "https://sqs.us-east-1.amazonaws.com/496891639123"),
            'queue' => env("AWS_SQS_IMPORT_JSON_QUEUE", "acmt_dev_2"),
            'region' => env("AWS_REGION", "us-east-1")
        ],

        'import_api_sqs' => [
            'driver' => 'sqs',
            'key' => env("AWS_KEY", "AKIAJYLGG5IQHTQG4QQQ"),
            'secret' => env("AWS_SECRET", "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"),
            'prefix' => env("AWS_SQS_IMPORT_API_PREFIX", "https://sqs.us-east-1.amazonaws.com/496891639123"),
            'queue' => env("AWS_SQS_IMPORT_API_QUEUE", "acmt-dev-import-api-sqs"),
            'region' => env("AWS_REGION", "us-east-1")
        ],

        'export_csv_sqs' => [
            'driver' => 'sqs',
            'key' => env("AWS_KEY", "AKIAJYLGG5IQHTQG4QQQ"),
            'secret' => env("AWS_SECRET", "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"),
            'prefix' => env("AWS_SQS_EXPORT_CSV_PREFIX", "https://sqs.us-east-1.amazonaws.com/496891639123"),
            'queue' => env("AWS_SQS_EXPORT_CSV_QUEUE", "acmt-dev-export-csv-data"),
            'region' => env("AWS_REGION", "us-east-1")
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
            'queue' => 'default',
            'retry_after' => 90,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'failed_jobs',
    ],

];
