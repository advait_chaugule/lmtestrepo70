<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store configurations related to various assets related to exemplar case association
    | OR others
    |-------------------------------------------------------------------------------------
    |
    */

    "S3" => [
        "MAIN_ARCHIVE_FOLDER" => "ASSET",
        "SUB_FOLDER_EXEMPLAR" => "EXEMPLAR",
        "SUB_FOLDER_DOCUMENT" => "DOCUMENT",
        "SUB_FOLDER_ITEM" => "ITEM",
        "SUB_FOLDER_PROJECT" => "PROJECT",
        "SUB_FOLDER_ORGANIZATION" => "ORGANIZATION"
    ],

    "LOCAL" => [
        "LOCAL_TEMPORARY_ARCHIVE_PATH" => "temporary/asset"
    ],

    "ENUM_KEY_VALUE_PAIR" => [
        "ASSET_LINKED_TYPE" => [
            "DOCUMENT" => 1,
            "ITEM" => 2, 
            "ITEM_ASSOCIATION" => 3,
            "PROJECT" => 4,
            "ORGANIZATION"=>5
        ]
    ]
];
