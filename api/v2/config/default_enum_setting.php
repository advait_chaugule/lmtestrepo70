<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store enum mapping, action texts etc for various database or non-database entities.
    | For all event tracking stuffs
    |-------------------------------------------------------------------------------------
    |
    */

    
    "metadata_field_type" => [
        1 => "TEXT",
        2 => "LONG TEXT",
        3 => "LIST",
        4 => "DATE",
        5 => "TIME",
        6 => "NUMBER",
        7 => "DICTIONARY"
    ],

    "comment_status" => [
        1 => "Open",
        2 => "Need Clarification",
        3 => "Fixed",
        4 => "Closed",
        5 => "Archive",
    ],
    

];
