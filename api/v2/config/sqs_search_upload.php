<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store configurations related to sqs for uploading data to cloud search
    |-------------------------------------------------------------------------------------
    |
    */

    "AWS_KEY" => env('AWS_KEY'),
    "AWS_SECRET" => env('AWS_SECRET'),
    "PROFILE" => 'default',
    "REGION" => env('AWS_REGION'),
    "API_VERSION" => '2012-11-05',
    "AWS_SQS_QUEUE" => env('AWS_SQS_QUEUE_SEARCH'),
    "AWS_SQS_PREFIX" => env('AWS_SQS_PREFIX_SEARCH'),

];
