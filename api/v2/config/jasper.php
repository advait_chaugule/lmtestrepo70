<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Configurations related to Jasper Report Server.
    |-------------------------------------------------------------------------------------
    */

    'jasper_host' => env('JASPER_HOST', ''),
    'jasper_username' => env('JASPER_USERNAME', ''),
    'jasper_password' => env('JASPER_PASSWORD', ''),
    'jasper_file_type' => ['html'=>'html', 'pdf'=>'pdf', 'csv'=>'csv', 'xls'=>'xls', 'docx'=>'docx', 'rtf'=>'rtf', 'odt'=>'odt', 'ods'=>'ods', 'xlsx'=>'xlsx', 'pptx'=>'pptx'],
    'jasper_report_list' => ['taxonomy','table_taxonomy','table_taxonomy_3_column'],
    'jasper_report_download_size' => 3000,
    'jasper_servlet' => env('JASPER_SERVLET_HOST', ''),
];
