<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store enum mapping, action texts etc for various database or non-database entities.
    | For all event tracking stuffs
    |-------------------------------------------------------------------------------------
    |
    */


    "event_type_value" => [
        "PROJECT_CREATED" => 1,
        "PROJECT_DELETED" => 2,
        "ITEM_CREATED_UNDER_PROJECT" => 3,
        "ITEM_EDITED_UNDER_PROJECT" => 4,
        "ITEM_DELETED_UNDER_PROJECT" => 5,
        "USER_LOGIN" => 6,
        "USER_LOGOUT" => 7,
        "ITEMS_VIEWED_UNDER_PROJECT" => 8, // View nodes (items) assigned to a project (will treat this like project viewed event)
        "ITEM_VIEWED_UNDER_PROJECT" => 9, // View item details under a project
        "PROJECT_SESSION_STARTED" => 10, // When user is inside any project related page, this event will be raised on specific intervals
        "USER_SESSION_REFRESHED" => 11, // When user is allowed access to any private api using access-token
        "CFPACKAGE_ACCESS_LOG"   =>12
    ],
    "Notifications" => [
        'PROJECT_STAGE_CHANGE_NOTIFICATION' => 1,
        'ROLE_ASSIGN' => 2,
        'COMMENT_NOTIFICATION_ASSIGNEE_CHANGE' => 3,
        'COMMENT_ASSIGN_TO_NOTIFICATION' => 4,
        'COMMENT_REPLY_TO_NOTIFICATION' => 5,
        'COMMENT_STATUS_CHANGE_NOTIFICATION' => 6,
        'ASSIGN_PROJECT_ROLE' => 7,
        'REVOKE_PROJECT_ROLE' => 8,
        'PROJECT_NEW_REQUEST_RECEIVED'=>9,
        'PROJECT_ACCESS_RECEIVED'=>10,
        'TAXONOMY_PUBLISHED'=>11,
        'PUBLIC_REVIEW_PUBLISH'=>12,
        'STAGE_ROLE_ASSIGN' =>13,
        'PACING_GUIDES' =>14,
        'IMPORT_JSON' =>15,
        'BEFORE_IMPORT_CSV_SUMMARY' => 17,
        'AFTER_IMPORT_CSV_SUMMARY' => 18,
        'TAXONOMY_UNPUBLISHED'=>16,
        'TAXONOMY_EXPORTED'=>19,
        'SUBSCRIBED_TAXONOMY_UPDATED'=>20,
    ],

    'Notification_category'=>[
        'PROJECT_STAGE_CHANGE_NOTIFICATION' => 1,
        //'ROLE_ASSIGN' => 2, // comment
        'COMMENT_NOTIFICATION_ASSIGNEE_CHANGE' => 2,
        'COMMENT_ASSIGN_TO_NOTIFICATION' => 8,
        'COMMENT_REPLY_TO_NOTIFICATION' => 2,
        'COMMENT_STATUS_CHANGE_NOTIFICATION' => 2,
        'ASSIGN_PROJECT_ROLE' => 5,
        'REVOKE_PROJECT_ROLE' => 5,
        'PROJECT_NEW_REQUEST_RECEIVED'=>1,
        'PROJECT_ACCESS_RECEIVED'=>5,
        'TAXONOMY_PUBLISHED'=>3,
        'PUBLIC_REVIEW_PUBLISH'=>6,
        'STAGE_ROLE_ASSIGN'=>1,
        'PACING_GUIDES'=>4,
        'IMPORT_JSON'=>7,
        'BEFORE_IMPORT_CSV_SUMMARY' => 10,
        'AFTER_IMPORT_CSV_SUMMARY' => 11,
        'TAXONOMY_UNPUBLISHED' =>9,
        'TAXONOMY_EXPORTED' =>12,
        'SUBSCRIBED_TAXONOMY_UPDATED' =>13,
        ],

    "notification_message" =>[
    "1"=>"{{project_name}} of {{taxonomy_name}} is now moved to {{stage_name}} in the project workflow.",
    "2"=>"A comment was assigned to you on {{project_name}} of {{taxonomy_name}}.",
    "3"=>"A reply was added to a comment assigned to you on {{project_name}} of {{taxonomy_name}}.",
    "4"=>"A reply was added to a comment you created on {{project_name}} of {{taxonomy_name}}.",
    "5"=>"You have been assigned the role of {{role_name}} for the project {{project_name}}.",
    "6"=>"Your access on the {{project_name}} of {{taxonomy_name}} has been revoked.",
    "7"=>"{{username}} has requested access for the role(s) of {{list_of_roles}} for project {{project_name}}",
    "8"=>" You have now been granted access to the {{project_name}} of {{taxonomy_name}}.",
    "9"=>"The status of a comment you have been tagged to has been changed to {{status_node}} .",
    "10"=>"A comment is has been reopened by {{username}} is is assigned to {{username/you}} on {{project_name}} of {{taxonomy_name}}.",
    "11"=>"The {{taxonomy_name}} has been published.",
    "12"=>"The {{taxonomy_name}} is ready for public review. ",
    "13"=>"The {{pacing_guide_name}} has been published. ",
    "14" =>"{{project_name}} of {{taxonomy_name}} is now moved to {{review}} in the project workflow. Since you are a {{role}} on this project you can start {{role_activity}}.",
    "15" => "The import process for the Taxonomy {{taxonomy_name}} is complete.",
    "16" =>"The {{taxonomy_name}} has been unpublished.",
    "17" =>"Update available for subscribed taxonomy: {{taxonomy_name}}"
    ],

    "email_notification"=>[
        "1"=>"Stage status updated",//Done
        "2"=>"Role status update", //Done
        "3"=>"Assignee changed for the {{comment}} ",
        "4"=>"You have a reply for the {{comment}} ",
        "5"=>"Comment status updated ",
        "6"=>"Comment {{comment_name}} has been re-opened ",
        "7"=>"You have a comment ",
        "8"=>"{{username}} has requested access for the project {{project}}. ",
        "9"=>"You have been granted access for the project {{ project }} ",
        "10"=>"{{username}} has requested access for the project {{project_name}} ",
        "11"=>"You can no longer access {{project_name}} project ",
        "12"=>"The {{taxonomy}} taxonomy published for public review",
        "13"=>"The {{taxonomy}} taxonomy has been published", //Done
        "14"=>"The {{pacing_guide }} Pacing Guide has been published ",
        "15"=>"The {{taxonomy}} taxonomy has been imported",
        "16"=>"Update available for subscribed taxonomy: {{taxonomy_name}}",
    ],

    "email_message"=>[
        "1"=>"Stage Status updated for project {{project_name}} ",//Done
        "2"=>"Your assigned role for the project {{project_name}} is now {{active}}",//Done
        "3"=>"The {{assignee_name}} has been changed for the {{comment}} ",
        "4"=>"You have received a reply from {{reply_name}} for the {{comment}} ",
        "5"=>" Your {{comment}} status has been updated.",
        "6"=>"Comment {{comment_name}} has been re-opened ",
        "7"=>"{{username}} has assigned a comment to you",
        "8"=>" {{username}} has requested access for the project {{project}} ",
        "9"=>"You have been granted access for the project {{ project_name }}. You can now start {{ role_name }} for the project {{ project }} ",
        "10"=>"{{username}} has requested access for the project {{project}} ",
        "11"=>"You can no longer access {{project_name}} project ",
        "12"=>"The {{taxonomy}} taxonomy published for public review",
        "13"=>"The {{taxonomy}} taxonomy has been published. You can review the published taxonomy",//Done
        "14"=>"The {{pacing_guide}} Pacing Guide has been published. You can review the published Pacing Guide",
        "15" => "The import process for the Taxonomy {{taxonomy_name}} is complete. Please click on the button below to open the link.",
        "16" => "There is an update available for subscribed taxonomy {{taxonomy_name}}. Please click on the following button to open the link and know more. You can also copy and paste the URL {{taxonomy_details_url}} in your browser to launch."
    ],

    "sub_event_type_value" => [
        "ITEM_CREATED_UNDER_PROJECT" => 1,
        "ITEM_EDITED_UNDER_PROJECT" => 2,
        "ITEM_DELETED_UNDER_PROJECT" => 3,
        "ITEM_VIEWED_UNDER_PROJECT" => 4
    ],

    "s3_details" => [
        "ARCHIVE_FOLDER_NAME" => "AUDIT_ARCHIVE"
    ],

    "event_activity_local_storage_details" => [
        "REPORT_EVENT_STORAGE_PATH" => "app/temporary/event-tracking"
    ],

    "sync_batch_count" => (int) env("SYNC_BATCH_COUNT", 1000),

    "activity_templates" => [
        "1" => "{{cache_user_display_name}} created Project \"{{cache_activity_name}}\"",
        "2" => "{{cache_user_display_name}} deleted Project \"{{cache_activity_name}}\"",
        "3-1" => "{{cache_user_display_name}} created Node \"{{cache_activity_name}}\" in Project \"{{cache_activity_subtype_activity_name}}\"",
        "4-2" => "{{cache_user_display_name}} edited Node \"{{cache_activity_name}}\" in Project \"{{cache_activity_subtype_activity_name}}\"",
        "5-3" => "{{cache_user_display_name}} deleted Node \"{{cache_activity_name}}\" in Project \"{{cache_activity_subtype_activity_name}}\""
    ],

    "minimum_time_difference_between_project_session_activity_in_seconds" => 300,
	
	"Cache_Activity" => [
        "PUBLISH_TAXONOMY"   =>  "PUBTAX",
        "NOTES_GETALL"       =>  "NOTEGETALL",
        "FILES_GETALL"       =>  "FILESGETALL",
        "NODE_TYPES"         =>  "NODETYPES",
        "CFPACKAGE_CASE"     =>  "CFPACKAGE",
        "CFDocumentsAll_CASE"=> "CFDOCUMENTSAll",
        "CFDocument_CASE"    => "CFDOCUMENTS",
        "CFItem_CASE"        => "CFITEM",
        "CUSTOMDATA_CASE"    => "CUSTOMDATA",
        "CFCONCEPT_CASE"     => "CONCEPT",
        "CFITEMTYPE_CASE"    => "ITEMTYPE",
        "CFLICENCE_CASE"     => "LICENCE",
        "CFSUBJECT_CASE"     => "SUBJECT",
        "CFASSOCIATION_CASE" => "ASSOCIATION",
        "CFITEMASSOCIATION_CASE"=>"ITEMASSOCIATION",
        "UNPUBLISHTAXO_CASE"=>"UNPUBLISHTAXO",
    ],

"Import_Size" => [
        "JSON"  =>  2000000,
    ],        
"Import_Folder" => [
        "MAIN_ARCHIVE_FOLDER" => "IMPORT",
            "SUB_FOLDER_JSON"     => "JSON",
            "SUB_FOLDER_USER_CSV" => "USERS_CSV"
   ],
"Taxonomy_Folder"=>[
    "MAIN_ARCHIVE_FOLDER"=>"CFPACKAGES",
],
"HIERARCHY_FOLDER"=>[
    "MAIN_ARCHIVE_FOLDER"=>"TREE_HIERARCHY",
],
"Version_Folder"=>[
    "MAIN_ARCHIVE_FOLDER"=>"DRAFTS",
],
"Import_Folder_CSV" => [
        "MAIN_ARCHIVE_FOLDER" => "IMPORT",
            "SUB_FOLDER_CSV"     => "CSV",
	    "SECOND_SUB_FOLDER_CSV"     => "PROCESSED_CSV",	
   ]
];
