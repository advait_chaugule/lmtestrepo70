<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Configurations related to Protip.
    |-------------------------------------------------------------------------------------
    */

    "S3_FOLDER_NAME" => "PROTIP_JSON",
    "FILE_NAME" => "protip.json"
];
