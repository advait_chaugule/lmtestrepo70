<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store configurations related to various assets related to exemplar case association
    | OR others
    |-------------------------------------------------------------------------------------
    |
    */

    "ROLE" => [
        "PROJECT_PUBLIC_REVIEW_ROLE" => "PPR00",
		"TAXANOMY_PUBLIC_REVIEW_ROLE" => "PPR01",
		"SYSTEM_DEFAULT_ADMIN_ROLE" => "DAR00",
    ],

    "WORKFLOW" => [
        "DEFAULT_WORKFLOW_PROJECT_PUBLIC_REVIEW" => "WPR00"
    ]
];
