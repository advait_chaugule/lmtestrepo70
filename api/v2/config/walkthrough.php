<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Configurations related to Walkthrough.
    |-------------------------------------------------------------------------------------
    */

    "S3_FOLDER_NAME" => "WALKTHROUGH_JSON",
    "FILE_NAME" => "walkthrough.json"
];
