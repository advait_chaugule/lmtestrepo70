<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Configurations related to Input Controls.
    |-------------------------------------------------------------------------------------
    */

    "S3_FOLDER_NAME" => "INPUT_CONTROLS_JSON/",
    "FILE_NAME" => "inputControlsDetails.json"
];
