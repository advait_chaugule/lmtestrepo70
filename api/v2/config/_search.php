<?php

return [

    /*
    |-------------------------------------------------------------------------------------
    | Store configurations related to generic search
    |-------------------------------------------------------------------------------------
    |
    */

    "taxonomy" => [
        "search_fields" => [
            "title", 
            "official_source_url", 
            "notes", 
            "publisher", 
            "description", 
            "human_coding_scheme", 
            "list_enumeration", 
            "full_statement", 
            "alternative_label", 
            "abbreviated_statement",
            "project_name",
            "taxonomy_name",
            "item_metadata",
            "document_metadata",
            "item_custom_metadata",
            "organization_identifier"
        ],
        // Events when search data should be uploaded to SQS to be later processed by Cronjob
        "sqs_upload_events" => [
            "update_document" => "UpdateDocument", // when a single document node is updated
            "update_item" => "UpdateItem", // when a single item is updated/created
            "taxonomy_created" => "NewDocument", // when a full taxonomy is created (eg. CASE JSON Import / Manual taxonomy builder)
            "update_project" => "NewProject", // when a project is created and taxonomy is assigned ( will also raise taxonomy_created event ),
            "update_pacing_guide" => "NewPacingGuide"
        ]
    ]

];
