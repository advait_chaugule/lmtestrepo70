(function(){
    'use strict';

    angular.module('app').controller('HeaderController', ['$scope', '$location', '$rootScope', '$cookies', 'AuthorizationService', function($scope, $location, $rootScope, $cookies, AuthorizationService){
        var vm = this;
        $scope.user =  "";
        
        $scope.isLoggedIn = function() {
    		$scope.user = AuthorizationService.getLoggedInUser();
            if ($scope.user) {
            	return true;
            }
            return false;
	    };

    }])
})();

