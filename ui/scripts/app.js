(function () {
    // 'use strict';
    
   var acmtApp = angular.module('app', ['ui.router', 'ngCookies', 'ui.tree']);
   
    acmtApp.controller('MainController', ['$scope', function($scope){
        $scope.title = 'ACMT - Academic Compentency Management Tool'
    }]);
    acmtApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/main');

        $stateProvider.state('main', {
            url:'/main',
            controller: 'MainController',
            templateUrl: 'views/main.html',
        }).state('taxonomy', {
            url:'/taxonomy',
            controller: 'TaxonomyController',
            templateUrl: 'views/taxonomy.html',
        }).state('login', {
            url: '/login',
            controller: 'LoginController',
            templateUrl: 'views/login.html'
        }).state('taxonomy-tree', {
            url: '/taxonomy-tree',
            controller: 'TaxonomyTreeController',
            templateUrl: 'views/taxonomy-tree.html'
        })
        // $locationProvider.html5Mode(true);
        // $locationProvider.hashPrefix('');
    }]); 
    acmtApp.run(['$rootScope', '$location', '$cookies', '$http', function($rootScope, $location, $cookies, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authData;
        }
        
 
        // $rootScope.$on('$locationChangeStart', function (event, next, current) {
        //     // redirect to login page if not logged in and trying to access a restricted page
        //     var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
        //     var loggedIn = $rootScope.globals.currentUser;
        //     if (restrictedPage && !loggedIn) {
        //         $location.path('/login');
        //     }
        // });
    }]);
})();
