(function () {	
	'use strict';
	
	angular.module('app').directive('navbar', function () {
	    return {
	        restrict: 'E',
	        templateUrl: "views/header.html",
	        controller: 'HeaderController'
	    };
	});
})();
