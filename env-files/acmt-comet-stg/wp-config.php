<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
/* Websites behind load balancers or reverse proxies that support HTTP_X_FORWARDED_PROTO can be fixed by adding the following code to the wp-config.php file, above the require_once call: */
if ( 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
	$_SERVER['HTTPS'] = 'on';
	$_SERVER['SERVER_PORT'] = '443';
}
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_db');

/** MySQL database username */
define('DB_USER', 'acmtstg');

/** MySQL database password */
define('DB_PASSWORD', 'o82k4NeYC9');

/** MySQL hostname */
define('DB_HOST', 'acmt-comet-stage-reporting-rds.ce9bcc1pq6dg.us-east-1.rds.amazonaws.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~cVT(hQ!%?,*s`tt|8-:P=RQ9zJgTols=;CRXR.:Cb_Z:Ew1#Y1B?7oh$9e,4.XQ');
define('SECURE_AUTH_KEY',  'GMv@IPw`r1%gq^9(OSIwjr|RS^*r]t0I29k.v|j9_dg6N-ND@H#Z1?,J[tq9wkB=');
define('LOGGED_IN_KEY',    'Oc^ksnz^jTcF)J{%h*apDDmNjN8lFA%4inL3cJ<;[wm<2$_M[^r&d7v+lQD<U*R<');
define('NONCE_KEY',        '[b/R-|;&k1Csf~b$&wbZ-@E^psqUzZ[@,[VzrS^2HYRG.Pi:d8`I8bF:c{>P!|59');
define('AUTH_SALT',        '76iG_|Aj,xo~)$x?+i6#J5Z3vHh(TWVk$yJX@gc4f8%s~|~{[,NTg`{:y49.xEIT');
define('SECURE_AUTH_SALT', 'pM]%X8n>vCf9P#ho(nDJvpA.gsYu%ZL;]v^)|zK6!qNn}=tNQZ=*3=DK5w~Bu&d!');
define('LOGGED_IN_SALT',   'yizCd5XlOy~bS+u8L)SK2iP7V^O@SE@rR|whrQYMNt:{5;(vhM,;(nh!pG6}H^C8');
define('NONCE_SALT',       'qYxHqL}R~w2PVsl5%-8Fs915dIPzx=6KN]u$qgIaL!It04O{|inGO<f%x#QlexyY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
