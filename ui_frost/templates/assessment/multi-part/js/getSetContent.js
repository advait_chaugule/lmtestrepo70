window.multipart = {};

//$(document).ready(function () {

$('#generate_data').off().on('click', function (e) {
    var res = multipart.generateQuestionData();
    res = JSON.stringify(res);
});

multipart.generateQuestionData = function () {
    // get question details
    //var question_stem = multipart.replaceNewline($('#question_stem').val());
    var question_stem = common_assessment_utility.getFieldValue('question_stem', true);
    var question_title = $('#question_title').val();
    //var instruction_text = multipart.replaceNewline($('#instruction_text').val());
    var instruction_text = common_assessment_utility.getFieldValue('instruction_text', true);
    var advJSONData = {};
    var subquestions = [];
    var question_type = 'multi-part';
    advJSONData['question_type'] = {text: question_type};
    advJSONData['question_interaction'] = {text: 'clickable'};
    advJSONData['question_layout'] = {text: 'vertical'};
    advJSONData['question_stem'] = {text: question_stem};
    advJSONData['question_title'] = {text: $('#question_title').val()};
    advJSONData['instruction_text'] = {text: instruction_text};

    var total_subquestions = $('.question-container').length;
    var subQuestionsData = '';
    for (var i = 1; i <= total_subquestions; i++) {
        subquestions.push(JSON.parse($('#selected_question_' + i).val()));
    }
    advJSONData['subquestions'] = subquestions;
    //advJSONData['global_correct_feedback'] = {text: multipart.replaceNewline($('#global_correct_feedback').val())};
    //advJSONData['global_incorrect_feedback'] = {text: multipart.replaceNewline($('#global_incorrect_feedback').val())};
    advJSONData['global_correct_feedback'] = {text: common_assessment_utility.getFieldValue('global_correct_feedback', true)};
    advJSONData['global_incorrect_feedback'] = {text: common_assessment_utility.getFieldValue('global_incorrect_feedback', true)};
    if ($("input[name='media']:checked").val() === 'no-media') {
        advJSONData['media'] = {type: "no-media"};
    } else {
        if ($('#ext_res_title').val() == '') {
            advJSONData['media'] = {type: "no-media"};
        } else {
            if ($('#media_type').val() === 'brightcove') {
                advJSONData['media'] = {type: $('#media_type').val(), brightcove_id: $('#ext_res_title_id').val(), url: $('#ext_res_url').val(), height: $('#ext_res_height').val(), width: $('#ext_res_width').val()};
            } else {
                advJSONData['media'] = {type: $('#media_type').val(), url: $('#ext_res_url').val(), title: $('#ext_res_title').val(), asset_type: $('#ext_res_type').val()};
            }
        }


    }

    return advJSONData;
};


multipart.renderQuestionData = function (question_data) {
    var question_data_obj = question_data;
    if (typeof question_data !== "object") {
        question_data_obj = JSON.parse(question_data);
    }
    //$('#question_stem').val(multipart.replaceBr(question_data_obj.question_stem.text));
    common_assessment_utility.setFieldValue('question_stem', question_data_obj.question_stem.text, true)
    $('#question_title').val(question_data_obj.question_title.text);
    var remaining_length = 255 - question_data_obj.question_title.text.length;
    $('#chars').text(remaining_length);
    //$('#instruction_text').val(multipart.replaceBr(question_data_obj.instruction_text.text));
    //$('#global_correct_feedback').val(multipart.replaceBr(question_data_obj.global_correct_feedback.text));
    //$('#global_incorrect_feedback').val(multipart.replaceBr(question_data_obj.global_incorrect_feedback.text));
    common_assessment_utility.setFieldValue('instruction_text', question_data_obj.instruction_text.text, true);
    common_assessment_utility.setFieldValue('global_correct_feedback', question_data_obj.global_correct_feedback.text, true);
    common_assessment_utility.setFieldValue('global_incorrect_feedback', question_data_obj.global_incorrect_feedback.text, true);
    var subquestions = question_data_obj.subquestions.length;
    var subquestionRepeat = '';
    for (var i = 1; i <= subquestions; i++) {
        var parseData = JSON.parse(question_data_obj.subquestions[i - 1]);
        subquestionRepeat += '<div class="question-container ui-state-default" id="question_container_' + i + '"><input type="hidden" id="selected_question_' + i + '" value="' + parseData.question_id + '"><div class="col-md-6 title tooltips"><span class="tooltiptext">' + parseData.question_title.text + '</span>' + multipart.limitText(parseData.question_title.text) + '</div><div class="col-md-4">' + multipart.getType(parseData.question_type.text) + '</div><div class="col-md-2" style="text-align:right"><a href="javascript:void(0)" class="reorder" title="Reorder"><i class="fa fa-arrows dragQuestion" aria-hidden="true"></i></a><a href="javascript:void(0)" id="delete_button_' + i + '"  onclick="multipart.deleteQuestions(' + i + ')" title="Delete" class="delete"><i class="fa fa-trash" aria-hidden="true" style="color: #e82b2b;"></i></a></div></div>'
    }
    $('#question_sortable').html(subquestionRepeat);
};

multipart.renderSubquestion = function (data) {
    var subquestions = data.length;
    var subquestionRepeat = '';
    for (var i = 1; i <= subquestions; i++) {
        var parseData = JSON.parse(data[i - 1]);
        subquestionRepeat += '<div class="question-container ui-state-default" id="question_container_' + i + '"><input type="hidden" id="selected_question_' + i + '" value="' + parseData.question_id + '"><div class="col-md-6 title tooltips"><span class="tooltiptext">' + parseData.question_title.text + '</span>' + multipart.limitText(parseData.question_title.text) + '</div><div class="col-md-4">' + multipart.getType(parseData.question_type.text) + '</div><div class="col-md-2" style="text-align:right"><a href="javascript:void(0)" class="reorder" title="Reorder"><i class="fa fa-arrows dragQuestion" aria-hidden="true"></i></a><a href="javascript:void(0)" id="delete_button_' + i + '"  onclick="multipart.deleteQuestions(' + i + ')" title="Delete" class="delete"><i class="fa fa-trash" aria-hidden="true" style="color: #e82b2b;"></i></a></div></div>'
    }
    $('#question_sortable').html(subquestionRepeat);
};

setInterval(function () {
    $(".reorder").tooltip();
    $(".delete").tooltip();
    $(".title").tooltip();
}, 2000);


multipart.getType = function (string) {
    if (string == 'mcss' || string == 'mcms') {
        return "MCQ";
    } else if (string == 'in-context') {
        return "Incontext Reading Activities";
    } else {
        return "Open-Ended";
    }
};

multipart.limitText = function (string) {
    if (string.length > 50) {
        string = string.substring(0, 50) + '.....';
    }
    return string;
};


var maxLength = 255;
$(".requires").on('keyup', function () {
    if ($(this).val().length > 0) {
        if ($(this).attr('id') === 'question_title') {
            var length = $(this).val().length;
            $(this).parent().removeClass('has-error');
            var length = maxLength - length;
            $('#chars').text(length);
        } else {
            $(this).parent().removeClass('has-error');
        }
    } else {
        if ($(this).attr('id') === 'question_title') {
            $('#chars').text(255);
        }
        $(this).parent().addClass('has-error');
    }
});

$(".requires-modal").on('keyup', function () {
    if ($(this).val().length > 0) {
        $(this).parent().removeClass('has-error');
    } else {
        $(this).parent().addClass('has-error');
    }
});

multipart.replaceNewline = function (string) {
    var data = string.replace(/(\r\n|\n|\r)/gm, "<br/>");
    return data;
};

multipart.replaceBr = function (string) {
    var regex = /<br\s*[\/]?>/gi;
    var data = string.replace(regex, "\n");
    return data;
};

$("#question_sortable").sortable({
    handle: ".dragQuestion",
    cancel: '',
    start: function (event, ui) {
        ui.item.data('originIndex', ui.item.index());
    },
    stop: function (event, ui) {
        multipart.rearrangeChoice(event, ui);
    }
});

multipart.rearrangeChoice = function (event, ui) {
    // change all choices html if needed
    $('.question-container').each(function (index, value) {
        var oldIndex = $(this).attr('id').split('_')[2];
        var newIndex = index + 1;
        if (oldIndex != newIndex) {
            multipart.modifyQuestionId(newIndex, oldIndex, this);
        }
    });
};

multipart.deleteQuestions = function (id) {
    $('#question_container_' + id).remove();
    $('.question-container').each(function (index, value) {
        var oldIndex = $(this).attr('id').split('_')[2];
        var newIndex = index + 1;
        if (oldIndex != newIndex) {
            multipart.modifyQuestionId(newIndex, oldIndex, this);
        }
    });
};


multipart.modifyQuestionId = function (newIndex, oldIndex, curObj) {
    $(curObj).attr('id', 'question_container_' + newIndex);
    var questionId = 'selected_question_' + newIndex;
    $(curObj).find('#selected_question_' + oldIndex).attr('id', questionId);
    $(curObj).find('#delete_button_' + oldIndex).attr('id', 'delete_button_' + newIndex);
    $(curObj).find('#' + 'delete_button_' + newIndex).removeAttr('onclick');
    $(curObj).find('#' + 'delete_button_' + newIndex).attr('onclick', 'multipart.deleteQuestions(' + newIndex + ')');
};

// delete all instances
for (name in CKEDITOR.instances)
{
    CKEDITOR.instances[name].destroy(true);
}

// initialize ckeditor
$('.ckeditor').each(function (e) {
    CKEDITOR.replace(this.id);
});

common_assessment_utility.ckeditorBindEvent();

/*CKEDITOR.on("instanceReady", function (event) {
    $('.cke_button__insertimage_icon').css({
        'background-image': 'url(../../lib/ckeditor/plugins/customimage/icons/icons.png)',
        'background-position': '-17px -254px'
    });
    for (var prop in CKEDITOR.instances) {
        var oEditor = CKEDITOR.instances[prop];
        oEditor.on('mode', function () {
            if (this.mode == 'wysiwyg') {
                angular.element(document.getElementById('questionController')).scope().jsRegisterer(this.document.find('.ckImg').$);
            }
        });
        angular.element(document.getElementById('questionController')).scope().jsRegisterer(oEditor.document.find('.ckImg').$);
    }
});*/

