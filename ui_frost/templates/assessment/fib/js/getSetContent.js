window.fib = {};
fib.generateChoice = function (index) {
    var choiceHtml = '';
    choiceHtml += '<div class="choiceContainer ui-state-default" id="choiceContainer_' + index + '">' +
            '<div class="well choice-well-section">' +
            '<div id="ansChoice_' + index + '" class="ansChoice_' + index + ' add-deletebutton active">' +
            '<div class="ansChoiceQtn_inner">' +
            '<div class="choice-outer-option">' +
            '<div class="row">' +
            '<div class="col-md-12 col-sm-12">' +
            '<label for="noforeachblank' + index + '-noforeachblank">Blank Identifier e.g.[[' + index + ']]</label>' +
            '<input type="text" id="noforeachblank' + index + '-noforeachblank" name="noforeachblank' + index + '-noforeachblank" value="[[' + index + ']]" class="form-control" placeholder="Click here to type" tabindex="201" disabled="">' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-12 col-sm-12">' +
            '<label for="textforeachblank' + index + '-textforeachblank">Correct Answer( ||| separated )</label>' +
            '<input type="text" id="textforeachblank' + index + '-textforeachblank" name="textforeachblank' + index + '-textforeachblank" value="" class="form-control requires" placeholder="Click here to type" tabindex="202"><label class="error_show" id="correct_answer_error_' + index + '" style="display:none">Answer cannot be left empty</label>' +
            '</div></div></div>' +
            '<div id="show_option_' + index + '" style="display:none">' +
            '<div class="choice-inner-option" id="choice-inner-option_' + index + '">' +
            '<div class="choice-cont-option" id="choice-cont-option_' + index + '">' +
            '<div class="row">' +
            '<div class="col-md-6">' +
            '<label for="individual_' + index + '_feedback-correct_feedback">Choice ' + index + ' Correct Feedback</label>' +
            '<div class="form-control ckeditor" data-tooltip-code="global_authoring_choices_choice_' + index + '_correct_feedback_label" data-key="correct_feedback" id="individual_' + index + '_feedback-correct_feedback" name="individual_' + index + '_feedback-correct_feedback" savevalue="choices#val3" tabindex="203"></div></div>' +
            '<div class="col-md-6">' +
            '<label for="individual_' + index + '_incorrect_feedback-incorrect_feedback">Choice ' + index + ' Incorrect Feedback</label>' +
            '<div class="form-control ckeditor" data-tooltip-code="global_authoring_choices_choices_' + index + '_incorrect_feedback_label" data-key="incorrect_feedback" id="individual_' + index + '_incorrect_feedback-incorrect_feedback" name="individual_' + index + '_incorrect_feedback-incorrect_feedback" savevalue="choices#val3" tabindex="204"></div></div></div>' +
            '<div class="row"><div class="col-md-12"><div class="form-group">' +
            '<label for="individual_' + index + '_score" class="label-name-list">Choice ' + index + ' Score</label>' +
            '<input id="individual_' + index + '_score" name="individual_' + index + '_score" type="text" class="form-control individual_score">' +
            '</div></div></div>' +
            '<div class="clearfix"></div></div>' +
            '<div class="clearfix"></div>' +
            '</div></div></div></div>' +
            '<button type="button" class="btn chice-delete" onclick="fib.deleteChoice(' + index + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button> ' +
            '</div>' +
            '<div class="form-group choice-optionselect"><a class="" id="option_button_' + index + '" onclick="fib.toggleOption(' + index + ')"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="display-inline advance-option">Advanced Options</span></a></div>' +
            '</div>' +
            '<div class="clearfix"></div>';
    return choiceHtml;
};
fib.addChoice = function () {
    var current_index = $('.choiceContainer').length;
    var index = parseInt(current_index) + 1;
    var choiceHtml = fib.generateChoice(index);
    $('#choice_sortable').append(choiceHtml);
    $(".requires").on('keyup',fib.showAndRemoveError);
    fib.loadCKEditorToElement();
    if ($('.choiceContainer').length > 1) {
        $('.chice-delete').attr('disabled', false);
    }
};
fib.loadCKEditorToElement = function () {
    $('.ckeditor').each(function (e) {
        if (!CKEDITOR.instances[this.id]) {
            CKEDITOR.replace(this.id);
        }
    });
};
fib.deleteChoice = function (choice_index) {
    // if choices is more than one
    var total_container = $('.choiceContainer').length;
    if ($('.choiceContainer').length > 1) {
        $('#choiceContainer_' + choice_index).remove();

        $('.choiceContainer').each(function (index, value) {
            var oldIndex = $(this).attr('id').split('_')[1];
            var newIndex = index + 1;
            if (oldIndex != newIndex) {
                fib.modifyChoiceHtml(newIndex, oldIndex, this);
            }
        });
        fib.ckeditorDeleteInstances(1, total_container);
        fib.ckeditorCreateInstances();
        common_assessment_utility.ckeditorBindEvent();
        
        if ($('.choiceContainer').length == 1) {
            $('.chice-delete').attr('disabled', 'disabled');
            var choiceID = $('.choiceContainer').attr('id').split('_')[1];
            $('#textforeachblank' + choiceID + '-textforeachblank').addClass('requires');
            $(".requires").off('keyup').on('keyup', fib.showAndRemoveError);
        }else if($('.choiceContainer').length>1){
            if(!$('#textforeachblank1-textforeachblank').hasClass('requires')){
                $('#textforeachblank1-textforeachblank').addClass('requires');
                $(".requires").off('keyup').on('keyup', fib.showAndRemoveError);
            }
        }
    }
};
fib.generateQuestionData = function () {
    var question_stem = common_assessment_utility.getFieldValue('question_stem', true);
    var question_title = $('#question_title').val();
    var instruction_text = common_assessment_utility.getFieldValue('instruction_text', true);
    var textwith_blanks = common_assessment_utility.getFieldValue('textwith_blanks', true);
    var advJSONData = {};
    var question_type = 'fib';
    advJSONData['question_type'] = {text: question_type};
    advJSONData['question_layout'] = {text: ''};
    advJSONData['question_stem'] = {text: question_stem};
    advJSONData['question_title'] = {text: question_title};
    advJSONData['instruction_text'] = {text: instruction_text};
    advJSONData['textwithblanks'] = {text: textwith_blanks};
    var total_answers = $('.choiceContainer').length;
    var correct_answer = [];
    var errorFlag=0;
    if (fib.blankCountSyncWithText(textwith_blanks, total_answers)) {
        $('.textWithBlankError').parent().removeClass('has-error');
        for (var i = 1; i <= total_answers; i++) {
            var fib_answer_value = $('#textforeachblank' + i + '-textforeachblank').val();
            var fib_answer_correct_feedback = common_assessment_utility.getFieldValue('individual_' + i + '_feedback-correct_feedback', true);
            var fib_answer_incorrect_feedback = common_assessment_utility.getFieldValue('individual_' + i + '_incorrect_feedback-incorrect_feedback', true);
            var fib_answer_score = $('#individual_' + i + '_score').val() == '' ? 0 : $('#individual_' + i + '_score').val();
            if (fib_answer_value != '') {
                var textforeachblank = [];
                if (fib_answer_value.indexOf('|||') != -1) {
                    var eachAnswerArr = fib_answer_value.split('|||');
                    for (var j = 0; j < eachAnswerArr.length; j++)
                    {
                        if (eachAnswerArr[j] != '') {
                            textforeachblank.push({text: eachAnswerArr[j]});
                        }
                    }
                } else {
                    textforeachblank.push({text: fib_answer_value});
                }
                correct_answer.push({blankid: i, noforeachblank: '[[' + i + ']]', textforeachblank: textforeachblank, correct_feedback: fib_answer_correct_feedback, incorrect_feedback: fib_answer_incorrect_feedback, score: fib_answer_score, interaction: "textentry", inputtype: "alphanumeric"});
            } else {
                //error
                if (!$('#textforeachblank' + i + '-textforeachblank').parent().hasClass('has-error'))
                {
                    $('#textforeachblank' + i + '-textforeachblank').parent().addClass('has-error');
                }
                if ($('#correct_answer_error_' + i).css('display') != 'block')
                {
                    $('#correct_answer_error_' + i).css('display', 'block');
                }
                errorFlag=1;
            }
        }
        if(errorFlag){
            return false;
        }
        advJSONData['choices'] = correct_answer;
        advJSONData['global_correct_feedback'] = {text: common_assessment_utility.getFieldValue('global_correct_feedback', true)};
        advJSONData['global_incorrect_feedback'] = {text: common_assessment_utility.getFieldValue('global_incorrect_feedback', true)};
        if ($("input[name='media']:checked").val() === 'no-media') {
            advJSONData['media'] = {type: "no-media"};
        } else {
            if ($('#ext_res_title').val() == '') {
                advJSONData['media'] = {type: "no-media"};
            } else {
                if ($('#media_type').val() === 'brightcove') {
                    advJSONData['media'] = {type: $('#media_type').val(), brightcove_id: $('#ext_res_title_id').val(), url: $('#ext_res_url').val(), height: $('#ext_res_height').val(), width: $('#ext_res_width').val()};
                } else {
                    advJSONData['media'] = {type: $('#media_type').val(), url: $('#ext_res_url').val(), title: $('#ext_res_title').val(), asset_type: $('#ext_res_type').val()};
                }
            }

        }
    } else {
        //error for textwith_blanks and answers count mismatch
        //vars used->textwith_blanks, total_answers
        if(fib.listAllIndexInText(textwith_blanks).length) {
            if (fib.isIdentifierSequesntial(textwith_blanks)) {
                //identifiers are sequential in Text with blanks.therefore count mismatch of identifier and correct answers
                var blankCount = fib.getBlankCountInText(textwith_blanks);
                if (blankCount) {
                    $('.textWithBlankError').html('Number of correct answers should match with no of blanks present in the Text with Blanks');
                    $('.textWithBlankError').parent().addClass('has-error');
                    if (parseInt(blankCount) > parseInt(total_answers)) {
                        var choiceLength = $('.choiceContainer').length;
                        for (var i = 1; i <= choiceLength; i++) {
                            if ($('#textforeachblank' + i + '-textforeachblank').val() == '') {
                                if (!$('#textforeachblank' + i + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + i + '-textforeachblank').parent().addClass('has-error');
                                }
                                if ($('#correct_answer_error_' + i).css('display') != 'block')
                                {
                                    $('#correct_answer_error_' + i).css('display', 'block');
                                }
                            }else{
                                if ($('#textforeachblank' + i + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + i + '-textforeachblank').parent().removeClass('has-error');
                                }
                                if ($('#correct_answer_error_' + i).css('display') == 'block')
                                {
                                    $('#correct_answer_error_' + i).css('display', 'none');
                                }
                            }
                        }
                    } else {//blank count < total answers
                        var indexArr = fib.listAllIndexInText(textwith_blanks);
                        for (var i = 0; i < indexArr.length; i++) {
                            var index = indexArr[i];
                            if ($('#textforeachblank' + index + '-textforeachblank').val() == '') {
                                if (!$('#textforeachblank' + index + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + index + '-textforeachblank').parent().addClass('has-error');
                                }
                                if ($('#correct_answer_error_' + index).css('display') != 'block')
                                {
                                    $('#correct_answer_error_' + index).css('display', 'block');
                                }
                            }else{
                                if ($('#textforeachblank' + index + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + index + '-textforeachblank').parent().removeClass('has-error');
                                }
                                if ($('#correct_answer_error_' + index).css('display') == 'block')
                                {
                                    $('#correct_answer_error_' + index).css('display', 'none');
                                }
                            }
                        }
                    }
                }else{
                    $('.textWithBlankError').parent().removeClass('has-error');
                }
            } else {
                $('.textWithBlankError').html('Identifiers(i.e. [[i]])are not sequential in the Text with Blanks');
                $('.textWithBlankError').parent().addClass('has-error');
                //$('#textWithBlankErrorVisibility').val("true");
            }
        } else {
           $('.textWithBlankError').html('Text with Blanks must contain atleast one Identifier(i.e. [[1]])');
           $('.textWithBlankError').parent().addClass('has-error'); 
        }
               
        return false;
    }
    return advJSONData;
};
fib.listAllIndexInText=function(textwith_blanks){
    var indexArr=[];
    var blankCountInText = textwith_blanks.split("[[").length - 1;
    for (var i = 1; i <= blankCountInText; i++) {
            if (textwith_blanks.indexOf("[[" + i + "]]") != -1) {
                indexArr.push(i);
                break;
            }
        }
    return indexArr;    
};
fib.isIdentifierSequesntial=function(textwith_blanks){
    var isSequential=true;
    var blankCountInText = textwith_blanks.split("[[").length - 1;
    for (var i = 1; i <= blankCountInText; i++) {
            if (textwith_blanks.indexOf("[[" + i + "]]") == -1) {
                isSequential = false;
                break;
            }
        }
    return isSequential;    
};
fib.getBlankCountInText=function(textwith_blanks){
    return textwith_blanks.split("[[").length - 1;
};
fib.blankCountSyncWithText = function (textwith_blanks, total_answers) {
    var isCorrect = true;
    var blankCountInText = textwith_blanks.split("[[").length - 1;
    if (blankCountInText == total_answers) {
        for (var i = 1; i <= total_answers; i++) {
            if (textwith_blanks.indexOf("[[" + i + "]]") == -1) {
                isCorrect = false;
                break;
            }
        }
    } else {
        isCorrect = false;
    }
    return isCorrect;
}

fib.toggleOption = function (id) {
    $('#show_option_' + id).slideToggle();
};
fib.checkNumber = function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

        fib.renderQuestionData = function (question_data) {
            var question_data_obj = question_data;
            if (typeof question_data !== "object") {
                question_data_obj = JSON.parse(question_data);
            }
            // render question details
            common_assessment_utility.setFieldValue('question_stem', question_data_obj.question_stem.text, true);
            $('#question_title').val(question_data_obj.question_title.text);
            var remaining_length = 255 - question_data_obj.question_title.text.length;
            $('#chars').text(remaining_length);
            //$('#instruction_text').val(mcq.replaceBr(question_data_obj.instruction_text.text));
            common_assessment_utility.setFieldValue('instruction_text', question_data_obj.instruction_text.text, true);
            common_assessment_utility.setFieldValue('textwith_blanks', question_data_obj.textwithblanks.text, true);
            // render choices
            var total_answer = question_data_obj.choices.length;

            for (var i = 0; i < total_answer; i++) {
                if (i > 0) {
                    fib.addChoice();
                }
                var textforeachblankMerged = '';
                var eachblankTextCount = question_data_obj.choices[i].textforeachblank.length;
                if (eachblankTextCount > 1) {
                    for (var j = 0; j < eachblankTextCount; j++) {
                        textforeachblankMerged += question_data_obj.choices[i].textforeachblank[j].text;
                        if (j != eachblankTextCount - 1) {
                            textforeachblankMerged += '|||';
                        }
                    }
                } else if (eachblankTextCount == 1) {
                    textforeachblankMerged += question_data_obj.choices[i].textforeachblank[0].text;
                }

                $('#textforeachblank' + question_data_obj.choices[i].blankid + '-textforeachblank').val(textforeachblankMerged);
                common_assessment_utility.setFieldValue('individual_' + question_data_obj.choices[i].blankid + '_feedback-correct_feedback', question_data_obj.choices[i].correct_feedback, true);
                common_assessment_utility.setFieldValue('individual_' + question_data_obj.choices[i].blankid + '_incorrect_feedback-incorrect_feedback', question_data_obj.choices[i].incorrect_feedback, true);
                $('#individual_' + question_data_obj.choices[i].blankid + '_score').val(question_data_obj.choices[i].score);
            }
            // render feedback
            common_assessment_utility.setFieldValue('global_correct_feedback', question_data_obj.global_correct_feedback.text, true);
            common_assessment_utility.setFieldValue('global_incorrect_feedback', question_data_obj.global_incorrect_feedback.text, true);


            if ($('.choiceContainer').length == 1) {
                $('.chice-delete').attr('disabled', 'disabled');
            }
        };

        fib.modifyChoiceHtml = function (newIndex, oldIndex, curObj) {

            var newId = 'choiceContainer_' + newIndex;
            var blankId = 'noforeachblank' + newIndex + '-noforeachblank';
            var choiceId = 'textforeachblank' + newIndex + '-textforeachblank';
            var feedabackId = 'individual_' + newIndex + '_feedback-correct_feedback';
            var incorrectfeedabackId = 'individual_' + newIndex + '_incorrect_feedback-incorrect_feedback';
            var individualScoreId = 'individual_' + newIndex + '_score';
            var option_button_id = 'option_button_' + newIndex;
            newId = newId.toString();
            feedabackId = feedabackId.toString();
            incorrectfeedabackId = incorrectfeedabackId.toString();
            individualScoreId = individualScoreId.toString();
            choiceId = choiceId.toString();
            option_button_id = option_button_id.toString();
            // set new container id
            $(curObj).attr('id', newId);
            $(curObj).find('#noforeachblank' + oldIndex + '-noforeachblank').attr({id: blankId, name: blankId, value: '[[' + newIndex + ']]'});
            $(curObj).find("#ansChoice_" + oldIndex).removeClass('ansChoice_' + oldIndex);
            $(curObj).find("#ansChoice_" + oldIndex).addClass('ansChoice_' + newIndex);
            $(curObj).find("#ansChoice_" + oldIndex).attr('id', 'ansChoice_' + newIndex);

            $(curObj).find('#textforeachblank' + oldIndex + '-textforeachblank').attr({id: choiceId, name: choiceId});
            $(curObj).find('#correct_answer_error_' + oldIndex).attr('id', 'correct_answer_error_' + newIndex);
            $(curObj).find('#individual_' + oldIndex + '_feedback-correct_feedback').attr({id: feedabackId, name: feedabackId, 'data-tooltip-code': 'global_authoring_choices_choice_' + newIndex + '_correct_feedback_label', savevalue: 'choices#val' + newIndex});
            $(curObj).find('#individual_' + oldIndex + '_incorrect_feedback-incorrect_feedback').attr({id: incorrectfeedabackId, name: incorrectfeedabackId, 'data-tooltip-code': 'global_authoring_choices_choices_' + newIndex + '_incorrect_feedback_label', savevalue: 'choices#val' + newIndex});
            $(curObj).find('#individual_' + oldIndex + '_score').attr({id: individualScoreId, name: individualScoreId});

            $(curObj).find('.chice-delete').removeAttr('onclick');
            $(curObj).find('.chice-delete').attr('onclick', 'fib.deleteChoice(' + newIndex + ')');

            $(curObj).find('#option_button_' + oldIndex).attr('id', option_button_id);
            $(curObj).find('#show_option_' + oldIndex).attr('id', 'show_option_' + newIndex);

            $(curObj).find('#' + option_button_id).removeAttr('onclick');
            $(curObj).find('#' + option_button_id).attr('onclick', 'fib.toggleOption(' + newIndex + ')');
            $(curObj).find("label[for='noforeachblank" + oldIndex + "-noforeachblank']").html('Blank Identifier e.g.[[' + newIndex + ']]');
            $(curObj).find("label[for='noforeachblank" + oldIndex + "-noforeachblank']").attr('for', 'noforeachblank' + newIndex + '-noforeachblank');
            $(curObj).find("label[for='textforeachblank" + oldIndex + "-textforeachblank']").attr('for', 'textforeachblank' + newIndex + '-textforeachblank');
            $(curObj).find('#choice-inner-option_' + oldIndex).attr('id', '#choice-inner-option_' + newIndex);
            $(curObj).find('#choice-cont-option_' + oldIndex).attr('id', '#choice-cont-option_' + newIndex);
            $(curObj).find("label[for='individual_" + oldIndex + "_feedback-correct_feedback']").attr('for', 'individual_' + newIndex + '_feedback-correct_feedback');
            $(curObj).find("label[for='individual_" + oldIndex + "_incorrect_feedback-incorrect_feedback']").attr('for', 'individual_' + newIndex + '_incorrect_feedback-incorrect_feedback');
            $(curObj).find("label[for='individual_" + oldIndex + "_score']").attr('for', 'individual_' + newIndex + '_score');
            $(curObj).find("label[for='individual_" + newIndex + "_feedback-correct_feedback']").html('Choice ' + newIndex + ' Feedback');
            $(curObj).find("label[for='individual_" + newIndex + "_incorrect_feedback-incorrect_feedback']").html('Choice ' + newIndex + ' Incorrect Feedback');
            $(curObj).find("label[for='individual_" + newIndex + "_score']").html('Choice ' + newIndex + ' Score');
        };
        fib.showAndRemoveError = function () {
            if ($(this).val().length > 0) {
                if ($(this).attr('id') === 'question_title') {
                    var length = $(this).val().length;
                    $(this).parent().removeClass('has-error');
                    var length = maxLength - length;
                    $('#chars').text(length);
                } else {
                    $(this).parent().removeClass('has-error');
                }
            } else {
                if ($(this).attr('id') === 'question_title') {
                    $('#chars').text(255);
                }
                $(this).parent().addClass('has-error');
            }
        }

        /*ckeditor operation function */

        fib.ckeditorDeleteInstances = function (start, total_container) {
            // delete all the choices container
            for (var i = start; i <= total_container; i++) {
                if (typeof CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'].destroy();
                }

                if (typeof CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'].destroy();
                }

            }
        };




        fib.ckeditorCreateInstances = function () {
            var total_choice = $('.choiceContainer').length;
            for (var i = 1; i <= total_choice; i++) {
                if (typeof CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'].destroy();
                }
                CKEDITOR.replace('individual_' + i + '_feedback-correct_feedback');
                CKEDITOR.replace('individual_' + i + '_incorrect_feedback-incorrect_feedback');
            }
        };
        
        fib.rearrangeChoice = function (event, ui) {
            // change all choices html if needed
            $('.choiceContainer').each(function (index, value) {
                var oldIndex = $(this).attr('id').split('_')[1];
                var newIndex = index + 1;
                if (oldIndex != newIndex) {
                    fib.modifyChoiceHtml(newIndex, oldIndex, this);
                }
            });
        };
        /*ckeditor operation function ends*/

//intially will be called
        fib.loadCKEditorToElement();
        var maxLength = 255;

        $(".requires").off('keyup').on('keyup', fib.showAndRemoveError);

        $('#addAnswerChoice').off('click').on('click', function (e) {
            fib.addChoice();
            fib.loadCKEditorToElement();
            common_assessment_utility.initializeCkeditor();
            common_assessment_utility.ckeditorBindEvent();
        });
        $(document.body).on('keydown', '.individual_score', function (e) {
            fib.checkNumber(e);
        });

        // delete all instances
        for (name in CKEDITOR.instances)
        {
            CKEDITOR.instances[name].destroy(true);
        }

        // initialize ckeditor
        $('.ckeditor').each(function (e) {
            CKEDITOR.replace(this.id);
        });

        common_assessment_utility.ckeditorBindEvent();
        
       /* $("#choice_sortable").sortable({
            handle: ".chice-drag",
            cancel: '',
            start: function (event, ui) {
                ui.item.data('originIndex', ui.item.index());
            },
            stop: function (event, ui) {
                fib.rearrangeChoice(event, ui);
                // delete ckeditor instances of all choices
                var total_container = $('.choiceContainer').length;
                fib.ckeditorDeleteInstances(1, total_container);
                // create ckeditor new instances for all choices
                fib.ckeditorCreateInstances();
                common_assessment_utility.ckeditorBindEvent();
            },
        });*/