window.fibdrag = {};
fibdrag.generateChoice = function (index) {
    var choiceHtml = '';
    choiceHtml += '<div class="choiceContainer ui-state-default" id="choiceContainer_' + index + '">' +
            '<div class="well choice-well-section">' +
            '<div id="ansChoice_' + index + '" class="ansChoice_' + index + ' add-deletebutton active">' +
            '<div class="ansChoiceQtn_inner">' +
            '<div class="choice-outer-option">' +
            '<div class="row">' +
            '<div class="col-md-12 col-sm-12">' +
            '<label for="noforeachblank' + index + '-noforeachblank">Blank Identifier e.g.[[' + index + ']]</label>' +
            '<input type="text" id="noforeachblank' + index + '-noforeachblank" name="noforeachblank' + index + '-noforeachblank" value="[[' + index + ']]" class="form-control" placeholder="Click here to type" tabindex="201" disabled="">' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-12 col-sm-12">' +
            '<label for="textforeachblank' + index + '-textforeachblank">Correct Answer</label>' +
            '<input type="text" id="textforeachblank' + index + '-textforeachblank" name="textforeachblank' + index + '-textforeachblank" value="" class="form-control requires" placeholder="Click here to type" tabindex="202"><label class="error_show" id="correct_answer_error_' + index + '" style="display:none">Answer cannot be left empty</label>' +
            '</div></div></div>' +
            '<div id="show_option_' + index + '" style="display:none">' +
            '<div class="choice-inner-option" id="choice-inner-option_' + index + '">' +
            '<div class="choice-cont-option" id="choice-cont-option_' + index + '">' +
            '<div class="row">' +
            '<div class="col-md-6">' +
            '<label for="individual_' + index + '_feedback-correct_feedback">Choice ' + index + ' Correct Feedback</label>' +
            '<div class="form-control ckeditor" data-tooltip-code="global_authoring_choices_choice_' + index + '_correct_feedback_label" data-key="correct_feedback" id="individual_' + index + '_feedback-correct_feedback" name="individual_' + index + '_feedback-correct_feedback" savevalue="choices#val3" tabindex="203"></div></div>' +
            '<div class="col-md-6">' +
            '<label for="individual_' + index + '_incorrect_feedback-incorrect_feedback">Choice ' + index + ' Incorrect Feedback</label>' +
            '<div class="form-control ckeditor" data-tooltip-code="global_authoring_choices_choices_' + index + '_incorrect_feedback_label" data-key="incorrect_feedback" id="individual_' + index + '_incorrect_feedback-incorrect_feedback" name="individual_' + index + '_incorrect_feedback-incorrect_feedback" savevalue="choices#val3" tabindex="204"></div></div></div>' +
            '<div class="row"><div class="col-md-12"><div class="form-group">' +
            '<label for="individual_' + index + '_score" class="label-name-list">Choice ' + index + ' Score</label>' +
            '<input id="individual_' + index + '_score" name="individual_' + index + '_score" type="text" class="form-control individual_score">' +
            '</div></div></div>' +
            '<div class="clearfix"></div></div>' +
            '<div class="clearfix"></div>' +
            '</div></div></div></div>' +
            '<button type="button" class="btn chice-delete" onclick="fibdrag.deleteChoice(' + index + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button> ' +
            '</div>' +
            '<div class="form-group choice-optionselect"><a class="" id="option_button_' + index + '" onclick="fibdrag.toggleOption(' + index + ')"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="display-inline advance-option">Advanced Options</span></a></div>' +
            '</div>' +
            '<div class="clearfix"></div>';
    return choiceHtml;
};
fibdrag.addChoice = function () {
    var current_index = $('.choiceContainer').length;
    var index = parseInt(current_index) + 1;
    var choiceHtml = fibdrag.generateChoice(index);
    $('#choice_sortable').append(choiceHtml);
    $(".requires").off('keyup').on('keyup', fibdrag.showAndRemoveError);
    fibdrag.loadCKEditorToElement();
     if ($('.choiceContainer').length > 1) {
                    $('.chice-delete').attr('disabled', false);
                }
};
fibdrag.loadCKEditorToElement = function () {
    $('.ckeditor').each(function (e) {
        if (!CKEDITOR.instances[this.id]) {
            CKEDITOR.replace(this.id);
        }
    });
};

fibdrag.deleteChoice = function (choice_index) {
    // if choices is more than one
    var total_container = $('.choiceContainer').length;
    if ($('.choiceContainer').length > 1) {
        $('#choiceContainer_' + choice_index).remove();

        $('.choiceContainer').each(function (index, value) {
            var oldIndex = $(this).attr('id').split('_')[1];
            var newIndex = index + 1;
            if (oldIndex != newIndex) {
                fibdrag.modifyChoiceHtml(newIndex, oldIndex, this);
            }
        });
        fibdrag.ckeditorDeleteInstances(1, total_container);
        fibdrag.ckeditorCreateInstances();
        common_assessment_utility.ckeditorBindEvent();

        if ($('.choiceContainer').length == 1) {
            $('.chice-delete').attr('disabled', 'disabled');
            var choiceID = $('.choiceContainer').attr('id').split('_')[1];
            $('#textforeachblank' + choiceID + '-textforeachblank').addClass('requires');
            $(".requires").off('keyup').on('keyup', fibdrag.showAndRemoveError);
        }else if($('.choiceContainer').length>1){
            if(!$('#textforeachblank1-textforeachblank').hasClass('requires')){
                $('#textforeachblank1-textforeachblank').addClass('requires');
                $(".requires").off('keyup').on('keyup', fibdrag.showAndRemoveError);
            }
        }
    }
};
fibdrag.modifyChoiceHtml = function (newIndex, oldIndex, curObj) {

    var newId = 'choiceContainer_' + newIndex;
    var blankId = 'noforeachblank' + newIndex + '-noforeachblank';
    var choiceId = 'textforeachblank' + newIndex + '-textforeachblank';
    var feedabackId = 'individual_' + newIndex + '_feedback-correct_feedback';
    var incorrectfeedabackId = 'individual_' + newIndex + '_incorrect_feedback-incorrect_feedback';
    var individualScoreId = 'individual_' + newIndex + '_score';
    var option_button_id = 'option_button_' + newIndex;
    newId = newId.toString();
    feedabackId = feedabackId.toString();
    incorrectfeedabackId = incorrectfeedabackId.toString();
    individualScoreId = individualScoreId.toString();
    choiceId = choiceId.toString();
    option_button_id = option_button_id.toString();
    // set new container id
    $(curObj).attr('id', newId);
    $(curObj).find('#noforeachblank' + oldIndex + '-noforeachblank').attr({id: blankId, name: blankId, value: '[[' + newIndex + ']]'});
    $(curObj).find("#ansChoice_" + oldIndex).removeClass('ansChoice_' + oldIndex);
    $(curObj).find("#ansChoice_" + oldIndex).addClass('ansChoice_' + newIndex);
    $(curObj).find("#ansChoice_" + oldIndex).attr('id', 'ansChoice_' + newIndex);

    $(curObj).find('#textforeachblank' + oldIndex + '-textforeachblank').attr({id: choiceId, name: choiceId});
    $(curObj).find('#correct_answer_error_' + oldIndex).attr('id', 'correct_answer_error_' + newIndex);
    $(curObj).find('#individual_' + oldIndex + '_feedback-correct_feedback').attr({id: feedabackId, name: feedabackId, 'data-tooltip-code': 'global_authoring_choices_choice_' + newIndex + '_correct_feedback_label', savevalue: 'choices#val' + newIndex});
    $(curObj).find('#individual_' + oldIndex + '_incorrect_feedback-incorrect_feedback').attr({id: incorrectfeedabackId, name: incorrectfeedabackId, 'data-tooltip-code': 'global_authoring_choices_choices_' + newIndex + '_incorrect_feedback_label', savevalue: 'choices#val' + newIndex});
    $(curObj).find('#individual_' + oldIndex + '_score').attr({id: individualScoreId, name: individualScoreId});

    $(curObj).find('.chice-delete').removeAttr('onclick');
    $(curObj).find('.chice-delete').attr('onclick', 'fibdrag.deleteChoice(' + newIndex + ')');

    $(curObj).find('#option_button_' + oldIndex).attr('id', option_button_id);
    $(curObj).find('#show_option_' + oldIndex).attr('id', 'show_option_' + newIndex);

    $(curObj).find('#' + option_button_id).removeAttr('onclick');
    $(curObj).find('#' + option_button_id).attr('onclick', 'fibdrag.toggleOption(' + newIndex + ')');
    $(curObj).find("label[for='noforeachblank" + oldIndex + "-noforeachblank']").html('Blank Identifier e.g.[[' + newIndex + ']]');
    $(curObj).find("label[for='noforeachblank" + oldIndex + "-noforeachblank']").attr('for', 'noforeachblank' + newIndex + '-noforeachblank');
    $(curObj).find("label[for='textforeachblank" + oldIndex + "-textforeachblank']").attr('for', 'textforeachblank' + newIndex + '-textforeachblank');
    $(curObj).find('#choice-inner-option_' + oldIndex).attr('id', '#choice-inner-option_' + newIndex);
    $(curObj).find('#choice-cont-option_' + oldIndex).attr('id', '#choice-cont-option_' + newIndex);
    $(curObj).find("label[for='individual_" + oldIndex + "_feedback-correct_feedback']").attr('for', 'individual_' + newIndex + '_feedback-correct_feedback');
    $(curObj).find("label[for='individual_" + oldIndex + "_incorrect_feedback-incorrect_feedback']").attr('for', 'individual_' + newIndex + '_incorrect_feedback-incorrect_feedback');
    $(curObj).find("label[for='individual_" + oldIndex + "_score']").attr('for', 'individual_' + newIndex + '_score');
    $(curObj).find("label[for='individual_" + newIndex + "_feedback-correct_feedback']").html('Choice ' + newIndex + ' Feedback');
    $(curObj).find("label[for='individual_" + newIndex + "_incorrect_feedback-incorrect_feedback']").html('Choice ' + newIndex + ' Incorrect Feedback');
    $(curObj).find("label[for='individual_" + newIndex + "_score']").html('Choice ' + newIndex + ' Score');
};
fibdrag.modifyDistractorHtml = function (newIndex, oldIndex, curObj) {
    var containerOldID = 'distractorContainer_' + oldIndex;
    var containerNewID = 'distractorContainer_' + newIndex;
    var divOldID = 'distractor_' + oldIndex;
    var divNewID = 'distractor_' + newIndex;
    var textOldID = 'distractorText' + oldIndex;
    var textNewID = 'distractorText' + newIndex;
    var RemoveButtOldID = 'removeDistractorButton' + oldIndex;
    var RemoveButtNewID = 'removeDistractorButton' + newIndex;
    $(curObj).attr('id', containerNewID);
    $(curObj).find('#' + divOldID).removeClass(divOldID);
    $(curObj).find('#' + divOldID).addClass(divNewID);
    $(curObj).find('#' + divOldID).attr('id', divNewID);
    $(curObj).find('#' + textOldID).attr({'id': textNewID, 'name': textNewID});
    $(curObj).find("label[for='" + textOldID + "']").attr('id', 'distractor_error_' + newIndex);
    $(curObj).find("label[for='" + textOldID + "']").attr('for', textNewID);
    $(curObj).find('#' + RemoveButtOldID).attr('id', RemoveButtNewID);
    $('#' + RemoveButtNewID).attr('onclick', 'fibdrag.removeDistractor(' + newIndex + ')');
};
fibdrag.generateQuestionData = function () {
    var question_stem = common_assessment_utility.getFieldValue('question_stem', true);
    var question_title = $('#question_title').val();
    var instruction_text = common_assessment_utility.getFieldValue('instruction_text', true);
    var textwith_blanks = common_assessment_utility.getFieldValue('textwith_blanks',true);
    var advJSONData = {};
    var question_type = 'fib-dragdrop';
    advJSONData['question_type'] = {text: question_type};
    advJSONData['question_interaction'] = {text: 'describing'};
    advJSONData['question_layout'] = {text: 'bottom'};
    advJSONData['question_stem'] = {text: question_stem};
    advJSONData['question_title'] = {text: question_title};
    advJSONData['instruction_text'] = {text: instruction_text};
    advJSONData['textwithblanks'] = {text: textwith_blanks};
    var total_answers = $('.choiceContainer').length;
    var correct_answer = [];
    var errorFlag=0;
    if (fibdrag.blankCountSyncWithText(textwith_blanks, total_answers)) {
        $('.textWithBlankError').parent().removeClass('has-error');
        for (var i = 1; i <= total_answers; i++) {
            var fibdrag_answer_value = $('#textforeachblank' + i + '-textforeachblank').val();
            var fibdrag_answer_arr=[];
            fibdrag_answer_arr.push({text:fibdrag_answer_value});
            var fibdrag_answer_correct_feedback = common_assessment_utility.getFieldValue('individual_' + i + '_feedback-correct_feedback',true);
            var fibdrag_answer_incorrect_feedback = common_assessment_utility.getFieldValue('individual_' + i + '_incorrect_feedback-incorrect_feedback',true);
            var fibdrag_answer_score = $('#individual_' + i + '_score').val() == '' ? 0 : $('#individual_' + i + '_score').val();
            if (fibdrag_answer_value != '') {
                correct_answer.push({blankid: i,noforeachblank:'[['+i+']]',textforeachblank:fibdrag_answer_arr, correct_feedback: fibdrag_answer_correct_feedback, incorrect_feedback: fibdrag_answer_incorrect_feedback, score: fibdrag_answer_score,asess: "",interaction: "dragdrop",inputtype: "alphanumeric"});
            } else {
                //error message display
                if (!$('#textforeachblank' + i + '-textforeachblank').parent().hasClass('has-error'))
                {
                    $('#textforeachblank' + i + '-textforeachblank').parent().addClass('has-error');
                }
                if ($('#correct_answer_error_' + i).css('display') != 'block')
                {
                    $('#correct_answer_error_' + i).css('display', 'block');
                }
                errorFlag=1;
            }
        }
        if(errorFlag){
            return false;
        }
        advJSONData['choices'] = correct_answer;
        var distractor=[];
        //distractor data part
            distractorCount=$('.distractorContainer').length;
            if(distractorCount>0){
               for(var i=1;i<=distractorCount;i++){
                  var distractorText=$('#distractorText' + i).val();
                  if(distractorText!=''){
                      distractor.push({distractorid:i,text:distractorText});
                  }else{
                      //error for particular distractor
                      $('#distractor_error_'+i).css('display', 'block');
                      return false;
                  }
               }
               advJSONData['distractors']=distractor;
            }else{
                //error for must have atleast one distractor
                $('#distractor_error_'+1).css('display', 'block');
                return false;
            }            
        //distractor data part end
        advJSONData['global_correct_feedback'] = {text: common_assessment_utility.getFieldValue('global_correct_feedback',true)};
        advJSONData['global_incorrect_feedback'] = {text: common_assessment_utility.getFieldValue('global_incorrect_feedback',true)};
        if ($("input[name='media']:checked").val() === 'no-media') {
            advJSONData['media'] = {type: "no-media"};
        } else {
            if ($('#ext_res_title').val() == '') {
                advJSONData['media'] = {type: "no-media"};
            } else {
                if ($('#media_type').val() === 'brightcove') {
                    advJSONData['media'] = {type: $('#media_type').val(), brightcove_id: $('#ext_res_title_id').val(), url: $('#ext_res_url').val(), height: $('#ext_res_height').val(), width: $('#ext_res_width').val()};
                } else {
                    advJSONData['media'] = {type: $('#media_type').val(), url: $('#ext_res_url').val(), title: $('#ext_res_title').val(), asset_type: $('#ext_res_type').val()};
                }
            }

        }
    } else {
        //error for textwith_blanks and answers count mismatch
        //vars used->textwith_blanks, total_answers
        if(fibdrag.listAllIndexInText(textwith_blanks).length) {
            if (fibdrag.isIdentifierSequesntial(textwith_blanks)) {
                //identifiers are sequential in Text with blanks.therefore count mismatch of identifier and correct answers
                var blankCount = fibdrag.getBlankCountInText(textwith_blanks);
                if (blankCount) {
                    $('.textWithBlankError').html('Number of correct answers should match with no of blanks present in the Text with Blanks');
                    $('.textWithBlankError').parent().addClass('has-error');
                    if (parseInt(blankCount) > parseInt(total_answers)) {
                        var choiceLength = $('.choiceContainer').length;
                        for (var i = 1; i <= choiceLength; i++) {
                            if ($('#textforeachblank' + i + '-textforeachblank').val() == '') {
                                if (!$('#textforeachblank' + i + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + i + '-textforeachblank').parent().addClass('has-error');
                                }
                                if ($('#correct_answer_error_' + i).css('display') != 'block')
                                {
                                    $('#correct_answer_error_' + i).css('display', 'block');
                                }
                            }else{
                                if ($('#textforeachblank' + i + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + i + '-textforeachblank').parent().removeClass('has-error');
                                }
                                if ($('#correct_answer_error_' + i).css('display') == 'block')
                                {
                                    $('#correct_answer_error_' + i).css('display', 'none');
                                }
                            }
                        }
                    } else {//blank count < total answers
                        var indexArr = fibdrag.listAllIndexInText(textwith_blanks);
                        for (var i = 0; i < indexArr.length; i++) {
                            var index = indexArr[i];
                            if ($('#textforeachblank' + index + '-textforeachblank').val() == '') {
                                if (!$('#textforeachblank' + index + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + index + '-textforeachblank').parent().addClass('has-error');
                                }
                                if ($('#correct_answer_error_' + index).css('display') != 'block')
                                {
                                    $('#correct_answer_error_' + index).css('display', 'block');
                                }
                            }else{
                                if ($('#textforeachblank' + index + '-textforeachblank').parent().hasClass('has-error'))
                                {
                                    $('#textforeachblank' + index + '-textforeachblank').parent().removeClass('has-error');
                                }
                                if ($('#correct_answer_error_' + index).css('display') == 'block')
                                {
                                    $('#correct_answer_error_' + index).css('display', 'none');
                                }
                            }
                        }
                    }
                }else{
                    $('.textWithBlankError').parent().removeClass('has-error');
                }
            } else {
                $('.textWithBlankError').html('Identifiers(i.e. [[i]])are not sequential in the Text with Blanks');
                $('.textWithBlankError').parent().addClass('has-error');
                //$('#textWithBlankErrorVisibility').val("true");
            }
        } else {
           $('.textWithBlankError').html('Text with Blanks must contain atleast one Identifier(i.e. [[1]])');
                $('.textWithBlankError').parent().addClass('has-error'); 
        }
               
        return false;
    }
    return advJSONData;
};

fibdrag.listAllIndexInText=function(textwith_blanks){
    var indexArr=[];
    var blankCountInText = textwith_blanks.split("[[").length - 1;
    for (var i = 1; i <= blankCountInText; i++) {
            if (textwith_blanks.indexOf("[[" + i + "]]") != -1) {
                indexArr.push(i);
                break;
            }
        }
    return indexArr;    
};
fibdrag.isIdentifierSequesntial=function(textwith_blanks){
    var isSequential=true;
    var blankCountInText = textwith_blanks.split("[[").length - 1;
    for (var i = 1; i <= blankCountInText; i++) {
            if (textwith_blanks.indexOf("[[" + i + "]]") == -1) {
                isSequential = false;
                break;
            }
        }
    return isSequential;    
};
fibdrag.getBlankCountInText=function(textwith_blanks){
    return textwith_blanks.split("[[").length - 1;
};

fibdrag.blankCountSyncWithText = function (textwith_blanks, total_answers) {
    var isCorrect = true;
    var blankCountInText = textwith_blanks.split("[[").length - 1;
    if (blankCountInText == total_answers) {
        for (var i = 1; i <= total_answers; i++) {
            if (textwith_blanks.indexOf("[[" + i + "]]") == -1) {
                isCorrect = false;
                break;
            }
        }
    } else {
        isCorrect = false;
    }
    return isCorrect;
}

fibdrag.toggleOption = function (id) {
    $('#show_option_' + id).slideToggle();
};
fibdrag.checkNumber = function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

fibdrag.renderQuestionData = function (question_data) {
            var question_data_obj = question_data;
            if (typeof question_data !== "object") {
                question_data_obj = JSON.parse(question_data);
            }
            // render question details
            common_assessment_utility.setFieldValue('question_stem', question_data_obj.question_stem.text, true);
            $('#question_title').val(question_data_obj.question_title.text);
            var remaining_length = 255 - question_data_obj.question_title.text.length;
            $('#chars').text(remaining_length);
            //$('#instruction_text').val(mcq.replaceBr(question_data_obj.instruction_text.text));
            common_assessment_utility.setFieldValue('instruction_text', question_data_obj.instruction_text.text, true);
            common_assessment_utility.setFieldValue('textwith_blanks',question_data_obj.textwithblanks.text,true);
            // render choices
            var total_answer = question_data_obj.choices.length;
            for (var i = 0; i < total_answer; i++) {
                if(i>0){
                     fibdrag.addChoice();
                     //fibdrag.ckeditorCreateInstances();
                }              
                $('#textforeachblank' + question_data_obj.choices[i].blankid + '-textforeachblank').val(question_data_obj.choices[i].textforeachblank[0]['text']);
                common_assessment_utility.setFieldValue('individual_' + question_data_obj.choices[i].blankid + '_feedback-correct_feedback', question_data_obj.choices[i].correct_feedback, true);
                common_assessment_utility.setFieldValue('individual_' + question_data_obj.choices[i].blankid + '_incorrect_feedback-incorrect_feedback', question_data_obj.choices[i].incorrect_feedback, true);
                $('#individual_' + question_data_obj.choices[i].blankid + '_score').val(question_data_obj.choices[i].score);
            }
            //render distractor
            var total_distractor=question_data_obj.distractors.length;
            
            for (var i = 0; i < total_distractor; i++) {
                if(i>0){
                    fibdrag.addDistractor();
                    //fibdrag.ckeditorCreateDistractorInstances();
                }
                $('#distractorText' + question_data_obj.distractors[i].distractorid).val(question_data_obj.distractors[i].text);
            }
            // render feedback
            common_assessment_utility.setFieldValue('global_correct_feedback', question_data_obj.global_correct_feedback.text, true);
            common_assessment_utility.setFieldValue('global_incorrect_feedback', question_data_obj.global_incorrect_feedback.text, true);


            if ($('.choiceContainer').length == 1) {
                $('.chice-delete').attr('disabled', 'disabled');
            }
            if($('.distractorContainer').length == 1){
                $('.distractor-delete').attr('disabled', 'disabled');
            }
        };
        fibdrag.removeDistractor = function (distractor_index) {
            // if choices is more than one
            var total_container = $('.distractorContainer').length;
            if ($('.distractorContainer').length > 1) {
                $('#distractorContainer_' + distractor_index).remove();

                $('.distractorContainer').each(function (index, value) {
                    var oldIndex = $(this).attr('id').split('_')[1];
                    var newIndex = index + 1;
                    if (oldIndex != newIndex) {
                        fibdrag.modifyDistractorHtml(newIndex, oldIndex, this);
                    }
                });               
                if ($('.distractorContainer').length == 1) {
                    $('.distractor-delete').attr('disabled', 'disabled');
                    var distractorID = $('.distractorContainer').attr('id').split('_')[1];
                    $('#distractorText' + distractorID).addClass('requires');
                    $(".requires").off('keyup').on('keyup', fibdrag.showAndRemoveError);
                }else if($('.distractorContainer').length>1){
                    if(!$('#distractorText1').hasClass('requires')){
                        $('#distractorText1').addClass('requires');
                    }
                }
            }
        };
        fibdrag.generateDistractor = function (index) {
            var distractorHtml = '';
            distractorHtml += '<div class="distractorContainer ui-state-default" id="distractorContainer_' + index + '">' +
                    '<div class="well choice-well-section">' +
                    '<div id="distractor_' + index + '" class="distractor_' + index + ' add-deletebutton active">' +
                    '<div class="ansChoiceQtn_inner"><div class="choice-outer-option">' +
                    '<div class="row"><div class="col-md-12 col-sm-12">' +
                    '<input type="text" id="distractorText' + index + '" name="distractorText' + index + '" value="" class="form-control requires" placeholder="Click here to type" tabindex="202"><label class="error_show" id="distractor_error_' + index + '" for="distractorText' + index + '" style="display:none">Distractor cannot be left empty</label>' +
                    '</div></div>' +
                    '</div></div>' +
                    '</div>' +
                    '<button type="button" class="btn chice-delete distractor-delete" id="removeDistractorButton' + index + '" onclick="fibdrag.removeDistractor(' + index + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                    '</div>' +
                    '</div>';
            return distractorHtml;
        };
        fibdrag.addDistractor = function () {
            var current_index = $('.distractorContainer').length;
            var index = parseInt(current_index) + 1;
            var distractorHtml = fibdrag.generateDistractor(index);
            $('#distractor_sortable').append(distractorHtml);
            $(".requires").off('keyup').on('keyup', fibdrag.showAndRemoveError);
            if ($('.distractorContainer').length > 1) {
                $('.distractor-delete').attr('disabled', false);
            }
        };
        fibdrag.showAndRemoveError=function () {
            if ($(this).val().length > 0) {
                if ($(this).attr('id') === 'question_title') {
                    var length = $(this).val().length;
                    $(this).parent().removeClass('has-error');
                    var length = maxLength - length;
                    $('#chars').text(length);
                } else {
                    $(this).parent().removeClass('has-error');
                }
            } else {
                if ($(this).attr('id') === 'question_title') {
                    $('#chars').text(255);
                }
                $(this).parent().addClass('has-error');
            }
        }
        //intially will be called
        fibdrag.loadCKEditorToElement();

        fibdrag.rearrangeChoice = function (event, ui) {
            // change all choices html if needed
            $('.choiceContainer').each(function (index, value) {
                var oldIndex = $(this).attr('id').split('_')[1];
                var newIndex = index + 1;
                if (oldIndex != newIndex) {
                    fibdrag.modifyChoiceHtml(newIndex, oldIndex, this);
                }
            });
        };
        /*ckeditor operation function */

        fibdrag.ckeditorDeleteInstances = function (start, total_container) {
            // delete all the choices container
            for (var i = start; i <= total_container; i++) {
                
                if (typeof CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'].destroy();
                }

                if (typeof CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'].destroy();
                }

            }
        };




        fibdrag.ckeditorCreateInstances = function () {
            var total_choice = $('.choiceContainer').length;
            for (var i = 1; i <= total_choice; i++) {
                if (typeof CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_feedback-correct_feedback'].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_incorrect_feedback-incorrect_feedback'].destroy();
                }
                CKEDITOR.replace('individual_' + i + '_feedback-correct_feedback');
                CKEDITOR.replace('individual_' + i + '_incorrect_feedback-incorrect_feedback');
            }
        };
        /*ckeditor operation function ends*/


        // delete all instances
        for (name in CKEDITOR.instances)
        {
            CKEDITOR.instances[name].destroy(true);
        }

        // initialize ckeditor
        $('.ckeditor').each(function (e) {
            CKEDITOR.replace(this.id);
        });

        common_assessment_utility.ckeditorBindEvent();



        var maxLength = 255;
        $(".requires").on('keyup',fibdrag.showAndRemoveError);

        $('#addAnswerChoice').off('click').on('click', function (e) {
            fibdrag.addChoice();
            fibdrag.loadCKEditorToElement();
            common_assessment_utility.initializeCkeditor();
            common_assessment_utility.ckeditorBindEvent();
        });
        $(document.body).on('keydown', '.individual_score', function (e) {
            fibdrag.checkNumber(e);
        });

        $('#addDistractor').off('click').on('click', function (e) {
            fibdrag.addDistractor();            
        });
        
         // delete all instances
        for (name in CKEDITOR.instances)
        {
            CKEDITOR.instances[name].destroy(true);
        }

        // initialize ckeditor
        $('.ckeditor').each(function (e) {
            CKEDITOR.replace(this.id);
        });

        common_assessment_utility.ckeditorBindEvent();
        
     /*   $("#choice_sortable").sortable({
            handle: ".chice-drag",
            cancel: '',
            start: function (event, ui) {
                ui.item.data('originIndex', ui.item.index());
            },
            stop: function (event, ui) {
                fibdrag.rearrangeChoice(event, ui);
                // delete ckeditor instances of all choices
                var total_container = $('.choiceContainer').length;
                fibdrag.ckeditorDeleteInstances(1, total_container);
                // create ckeditor new instances for all choices
                fibdrag.ckeditorCreateInstances();
                common_assessment_utility.ckeditorBindEvent();
            },
        });*/