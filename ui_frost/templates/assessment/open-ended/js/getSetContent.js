window.openended = {};

//$(document).ready(function () {

$('#generate_data').off().on('click', function (e) {
    var res = common_assessment_utility.generateQuestionData();
    res = JSON.stringify(res);
});
$("input[name='response_type']:first").prop('checked', true);

$("input[name='response_type']").on('click', function () {
    if ($(this).val() == 'with-response') {
        $('#grade_container').removeClass('disabled-grade');
        $("input[name='grade_type']:first").prop('checked', true);
        $('#with-response').show();
        $('#without-response').hide();
        $('#with-grading').hide();
    } else {
        $('#grade_container').addClass('disabled-grade');
        $("input[name='grade_type']").prop('checked', false);
        $('#with-response').hide();
        $('#without-response').show();
        $('#with-grading').hide();
    }
});

$("input[name='grade_type']").on('click', function () {
    if ($(this).val() == 'with-grading') {
        $('#with-response').hide();
        $('#without-response').hide();
        $('#with-grading').show();
    } else {
        $('#with-response').show();
        $('#without-response').hide();
        $('#with-grading').hide();
    }
});


//console.log($("input[name='response_type']:checked").val());

//});


openended.generateQuestionData = function () {
    // get question details
    //var question_stem = openended.replaceNewline($('#question_stem').val());
    var question_stem = common_assessment_utility.getFieldValue('question_stem', true);
    var question_title = $('#question_title').val();
    //var instruction_text = openended.replaceNewline($('#instruction_text').val());
    var instruction_text = common_assessment_utility.getFieldValue('instruction_text', true);
    var advJSONData = {};
    var question_type = 'open-ended';
    advJSONData['question_type'] = {text: question_type};
    advJSONData['question_interaction'] = {text: 'clickable'};
    advJSONData['question_layout'] = {text: 'vertical'};
    advJSONData['question_stem'] = {text: question_stem};
    advJSONData['question_title'] = {text: $('#question_title').val()};
    advJSONData['instruction_text'] = {text: instruction_text};

    advJSONData['response_type'] = {text: $("input[name='response_type']:checked").val()};
    if ($("input[name='response_type']:checked").val() === 'with-response') {
        advJSONData['grading_type'] = {text: $("input[name='grade_type']:checked").val()};
        if ($("input[name='grade_type']:checked").val() === 'without-grading') {
            advJSONData['sample_answer'] = {text: common_assessment_utility.getFieldValue('sample_answer', true)};
            advJSONData['feedback'] = {text: common_assessment_utility.getFieldValue('feedback', true)};
        } else {
            advJSONData['feedback'] = {text: common_assessment_utility.getFieldValue('feedback_grade', true)};
        }

    } else {
        advJSONData['answer'] = {text: common_assessment_utility.getFieldValue('answer', true)};
    }

    if ($("input[name='media']:checked").val() === 'no-media') {
        advJSONData['media'] = {type: "no-media"};
    } else {
        if ($('#ext_res_title').val() == '') {
            advJSONData['media'] = {type: "no-media"};
        } else {
            if ($('#media_type').val() === 'brightcove') {
                advJSONData['media'] = {type: $('#media_type').val(), brightcove_id: $('#ext_res_title_id').val(), url: $('#ext_res_url').val(), height: $('#ext_res_height').val(), width: $('#ext_res_width').val()};
            } else {
                advJSONData['media'] = {type: $('#media_type').val(), url: $('#ext_res_url').val(), title: $('#ext_res_title').val(), asset_type: $('#ext_res_type').val()};
            }
        }


    }

    return advJSONData;
};


openended.renderQuestionData = function (question_data) {
    var question_data_obj = question_data;
    if (typeof question_data !== "object") {
        question_data_obj = JSON.parse(question_data);
    }
    //$('#question_stem').val(openended.replaceBr(question_data_obj.question_stem.text));
    common_assessment_utility.setFieldValue('question_stem', question_data_obj.question_stem.text, true);
    $('#question_title').val(question_data_obj.question_title.text);
    var remaining_length = 255 - question_data_obj.question_title.text.length;
    $('#chars').text(remaining_length);
    //$('#instruction_text').val(openended.replaceBr(question_data_obj.instruction_text.text));
    common_assessment_utility.setFieldValue('instruction_text', question_data_obj.instruction_text.text, true);

    if (question_data_obj.response_type.text === 'without-response') {
        $("input[name='response_type']:first").prop('checked', true);
        //$('#answer').val(openended.replaceBr(question_data_obj.answer.text));
        common_assessment_utility.setFieldValue('answer', question_data_obj.answer.text, true);
    } else {
        $("input[name='response_type']:last").prop('checked', true);
        $('#grade_container').removeClass('disabled-grade');
        if (question_data_obj.grading_type.text === 'without-grading') {
            $("input[name='grade_type']:first").prop('checked', true);
            $('#with-response').show();
            $('#without-response').hide();
            $('#with-grading').hide();
            //$('#sample_answer').val(openended.replaceBr(question_data_obj.sample_answer.text));
            common_assessment_utility.setFieldValue('sample_answer', question_data_obj.sample_answer.text, true);
            //$('#feedback').val(openended.replaceBr(question_data_obj.feedback.text));
            common_assessment_utility.setFieldValue('feedback', question_data_obj.feedback.text, true);
        } else {
            $("input[name='grade_type']:last").prop('checked', true);
            $('#with-response').hide();
            $('#without-response').hide();
            $('#with-grading').show();
            //$('#feedback_grade').val(openended.replaceBr(question_data_obj.feedback.text));
            common_assessment_utility.setFieldValue('feedback_grade', question_data_obj.feedback_grade.text, true);
        }
    }

};





var maxLength = 255;
$(".requires").on('keyup', function () {
    if ($(this).val().length > 0) {
        if ($(this).attr('id') === 'question_title') {
            var length = $(this).val().length;
            $(this).parent().removeClass('has-error');
            var length = maxLength - length;
            $('#chars').text(length);
        } else {
            $(this).parent().removeClass('has-error');
        }
    } else {
        if ($(this).attr('id') === 'question_title') {
            $('#chars').text(255);
        }
        $(this).parent().addClass('has-error');
    }
});

$(".requires-modal").on('keyup', function () {
    if ($(this).val().length > 0) {
        $(this).parent().removeClass('has-error');
    } else {
        $(this).parent().addClass('has-error');
    }
});

openended.replaceNewline = function (string) {
    var data = string.replace(/(\r\n|\n|\r)/gm, "<br/>");
    return data;
};

openended.replaceBr = function (string) {
    var regex = /<br\s*[\/]?>/gi;
    var data = string.replace(regex, "\n");
    return data;
};

// delete all instances
for (name in CKEDITOR.instances)
{
    CKEDITOR.instances[name].destroy(true);
}

// initialize ckeditor
$('.ckeditor').each(function (e) {
    CKEDITOR.replace(this.id);
});

common_assessment_utility.ckeditorBindEvent();

/*CKEDITOR.on("instanceReady", function (event) {
    $('.cke_button__insertimage_icon').css({
        'background-image': 'url(../../lib/ckeditor/plugins/customimage/icons/icons.png)',
        'background-position': '-17px -254px'
    });
    for (var prop in CKEDITOR.instances) {
        var oEditor = CKEDITOR.instances[prop];
        oEditor.on('mode', function () {
            if (this.mode == 'wysiwyg') {
                angular.element(document.getElementById('questionController')).scope().jsRegisterer(this.document.find('.ckImg').$);
            }
        });
        angular.element(document.getElementById('questionController')).scope().jsRegisterer(oEditor.document.find('.ckImg').$);
    }
});*/


