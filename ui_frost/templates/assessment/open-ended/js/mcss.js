var ts = {
    "name": "MCSS",
    "code": "mcss",
    "item": {
        "question": {
            "id": "question",
            "label": "global_authoring_question_section_header_label",            
            "hints": "Some instruction goes here.",
            "fields": {
                "question_stem": {
                    "type": "textarea",
                    "label": "global_authoring_question_question_stem_label",
                    "value": "",
                    "id": "question_stem",
                    "tooltip": "global_authoring_question_question_stem_label",
                    "oldValue": "question_text#val1",
                    "processing": "normal",
                    "toolbar": {
                        "bold": false,
                        "italics": false
                    },
                    "key": "text",
                    "displayInstantPreview": true
                },
                "question_title": {
                    "type": "only_long_text",
                    "label": "global_authoring_question_question_title_label",
                    "value": "",
                    "id": "question_title",
                    "tooltip": "global_authoring_question_question_title_label",
                    "oldValue": "question_title",
                    "processing": "normal",
                    "toolbar": [],
                    "key": "text",
                    "displayInstantPreview": false,
                    "maxLength":30
                },
                "instruction_text": {
                    "type": "textarea",
                    "label": "global_authoring_question_instruction_text_label",
                    "value": "",
                    "id": "instruction_text",
                    "tooltip": "global_authoring_question_instruction_text_label",
                    "oldValue": "instruction_text#val1",
                    "processing": "normal",
                    "isVisible":true,
                    "toolbar": {
                        "bold": false,
                        "italics": false
                    },
                    "key": "text",
                    "displayInstantPreview": false
                }
            }
        },
        "choices": {
            "id": "choices",
            "label": "global_authoring_choices_section_header_label",
            "hints": "Some choice instruction goes here.",
            "cardinality": "single",
            "default": 4,
            "isdynamic": true,
            "addcoicefield": true,
            "multiresponsefield": true,
            "displayMore": true,
            "sortable": true,
            "previewsettings": {
                "questiontype": {
                    "type": "questiontype",
                    "name": "Type",
                    "key": "text",
                    "jsonfield": "question_type",
                    "renderHtml": false
                },
                 "interaction": {
                    "type": "simple_dropdown",
                    "name": "interactiontype",
                    "label": "global_authoring_choices_interaction_type_label",
                    "key": "text",
                    "jsonfield": "question_interaction",
                    "interactionblockid":"interaction_block_id",
                    "renderHtml": true,
                    "fields": {
                        "clickable": {
                            "default": true,
                            "isVisible": true,
                            "label": "global_authoring_choices_interaction_type_clickable"
                        },
                        "selectable": {
                            "default": false,
                            "isVisible": true,
                            "label": "global_authoring_choices_interaction_type_selectable"
                        }
                    }
                },
                "layout": {
                    "type": "simple_dropdown",
                    "name": "layouttype",
                    "label": "global_authoring_choices_layout_label",
                    "key": "text",
                    "jsonfield": "question_layout",
                    "renderHtml": true,
                    "fields": {
                        "vertical": {
                            "default": true,
                            "isVisible": true,
                            "label": "global_authoring_choices_layout_vertical"
                        },
                        "horizontal": {
                            "default": false,
                            "isVisible": true,
                            "label": "global_authoring_choices_layout_horizontal"
                        }
                    }
                }
            },
            "fields": {
                "choiceid": {
                    "type": "choiceradio",
                    "label": "global_authoring_mcq_choices_{id}_label",
                    "value": "",
                    "processing": "choice",
                    "id": "assess{id}",
                    "oldValue": "choices#val1",
                    "tooltip": "global_authoring_mcq_choices_{id}_label",
                    "key": "choiceid",
                    "displayInMore" : false,
                    "renderHtml"  : false
                },
                "assess": {
                    "type": "choiceradio",
                    "label": "global_authoring_mcq_choices_{id}_label",
                    "value": "",
                    "processing": "choice",
                    "id": "assess{id}",
                    "oldValue": "choices#val1",
                    "tooltip": "global_authoring_mcq_choices_{id}_label",
                    "key": "assess",
                    "displayInstantPreview": true,	
                    "displayInMore" : false,
                    "renderHtml"  : true
                },
                "choicestext": {
                    "type": "only_textarea",
                    "label": "Choices Text",
                    "value": "Choices value",
                    "processing": "choice",
                    "id": "choices{id}",
                    "oldValue": "choices#val2",
                    "tooltip": "Choices{id}",
                    "toolbar": [],
                    "showlayouttoggle": true,
                    "key": "text",
                    "displayInstantPreview": true,	
                    "displayInMore" : false,
                    "renderHtml"  : true
                },
                "choicefeedback": {
                    "type": "only_textarea",
                    "label": "global_authoring_mcq_choices_choice_{id}_correct_feedback_label",
                    "value": "",
                    "processing": "choice",
                    "id": "individual_{id}_feedback",
                    "oldValue": "choices#val3",
                    "tooltip": "global_authoring_mcq_choices_choice_{id}_correct_feedback_label",
                    "toolbar": [],
                    "key": "correct_feedback",
                    "displayInstantPreview": false,
                    "displayInMore" : true,
                    "renderHtml"  : true
                },
                 "choiceincorrectfeedback": {
                    "type": "only_textarea",
                    "label": "global_authoring_choices_mcq_choices_{id}_incorrect_feedback_label",
                    "value": "",
                    "processing": "choice",
                    "id": "individual_{id}_incorrect_feedback",
                    "oldValue": "choices#val3",
                    "tooltip": "global_authoring_choices_mcq_choices_{id}_incorrect_feedback_label",
                    "toolbar": [],
                    "key": "incorrect_feedback",
                    "displayInstantPreview": false,
                    "displayInMore" : true,
                    "renderHtml"  : true
                },
                "choicescore": {
                    "type": "only_text",
                    "label": "global_authoring_mcq_choices_choices_{id}_score_label",
                    "value": "",
                    "processing": "choice",
                    "id": "individual_{id}_score",
                    "oldValue": "choices#val4",
                    "tooltip": "global_authoring_mcq_choices_choices_{id}_score_label",
                    "toolbar": [],
                    "key": "score",
                    "displayInstantPreview": false,
                    "displayInMore" : true,
                    "renderHtml"  : true
                },
                "pinanswer": {
                    "type": "choicecheckbox",
                    "label": "global_authoring_mcq_choices_choices_{id}_pin_answer_label",
                    "value": "Pin Answer",
                    "processing": "choice",
                    "id": "pinanswer{id}",
                    "oldValue": "choices#val5",
                    "tooltip": "global_authoring_mcq_choices_choices_{id}_pin_answer_label",
                    "key": "pinanswer",
                    "displayInstantPreview": false,
                    "displayInMore" : true,
                    "renderHtml"  : true
                }
            },
            "correctanswer": {
                "id": "correct_answer",
                "renderHtml": false
            }
        },
        "feedback": {
            "id": "feedback",
            "label": "global_authoring_feedback_section_header_label",
            "hints": "Some Feedback goes here.",
            "fields": {
                "global_correct_feedback": {
                    "type": "textarea",
                    "label": "global_authoring_feedback_global_correct_feedback_label",
                    "value": "",
                    "id": "global_correct_feedback",
                    "tooltip": "global_authoring_feedback_global_correct_feedback_label",
                    "oldValue": "correct_feedback#val1",
                    "processing": "normal",
                    "toolbar": {
                        "bold": false,
                        "italics": false
                    },
                    "key": "text"
                },
                "global_incorrect_feedback": {
                    "type": "textarea",
                    "label": "global_authoring_feedback_global_incorrect_feedback_label",
                    "value": "",
                    "id": "global_incorrect_feedback",
                    "tooltip": "global_authoring_feedback_global_incorrect_feedback_label",
                    "oldValue": "incorrect_feedback#val1",
                    "processing": "normal",
                    "toolbar": [],
                    "key": "text"
                }
            }
        },
        "hints":{
            "id": "hints",
            "label": "global_authoring_hints_section_header_label",
            "hints": "Some hint instruction goes here.",
            "default": 1,
            "isdynamic": true,
            "addhintfield": true,
            "displayMore"   : false,
            "fields": {
                "hintid": {
                    "type": "deletebutton",
                    "label": "global_authoring_hints_{id}_label",
                    "value": "",
                    "processing": "hint",
                    "id": "assess{id}",
                    "oldValue": "hints",
                    "tooltip": "global_authoring_hints_{id}_label",
                    "key": "hintid",	
                    "displayInMore" : false,
                    "renderHtml"  : false
                },
                "hintdelete": {
                    "type": "deletebutton",
                    "label": "global_authoring_hints_{id}_label",
                    "value": "",
                    "processing": "hint",
                    "id": "hint{id}",
                    "oldValue": "hints",
                    "tooltip": "Hint {id}",
                    "key": "assess",	
                    "displayInMore" : false,
                    "renderHtml"  : true
                },
                "hinttext": {
                    "type": "only_textarea",
                    "label": "Hints Text",
                    "value": "Hints value",
                    "processing": "hint",
                    "id": "hints{id}",
                    "oldValue": "hints#val1",
                    "tooltip": "global_authoring_hints_{id}_label",
                    "toolbar": [],
                    "showlayouttoggle": true,
                    "key": "text",
                    "displayInstantPreview": true,	
                    "displayInMore" : false,
                    "renderHtml"  : true					
                }
            }
        },
        "add_exhibit":{
            "id": "exhibit",
            "label": "global_authoring_exhibit_section_header",
            "hints": "Exhibit",
            "fields": {                
                "exhibit": {
                    "type": "textarea",
                    "label": "global_authoring_exhibit_inner_text",
                    "value": "",
                    "id": "exhibit",
                    "tooltip": "global_authoring_exhibit_inner_text",
                    "oldValue": "image#val1",
                    "processing": "normal",
                    "isVisible":true,
                    "toolbar": {
                        "bold": false,
                        "italics": false
                    },
                    "key": "path"
                },
                "exhibit_type": {
                    "type": "textarea",
                    "label": "Exhibit/Graphic",
                    "value": "",
                    "id": "exhibit_type",              
                    "key": "exhibit_type",
                    "isVisible":false ,
                    "renderHtml"  : false
                    
                }
            }
        },     
        "settings": {
            "id": "settings",
            "label": "global_authoring_settings_section_header_label",
            "hints": "Some Settings comes here.",
            "fields": {
                "score": {
                    "type": "dropdown",
                    "label": "global_authoring_settings_score_label",
                    "id": "score",
                    "value": "1",
                    "tooltip": "global_authoring_settings_score_label",
                    "choices": {
                        "1": "1",
                        "2": "2",
                        "3": "3",
                        "4": "4",
                        "5": "5",
                        "6": "6",
                        "7": "7",
                        "8": "8",
                        "9": "9",
                        "10": "10"
                    }
                }
            }
        } 
    }
};