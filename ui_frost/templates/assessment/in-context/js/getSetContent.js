window.incontext = {};
incontext.generateChoice = function (parent_id, index) {

    var choiceHtml = '';
    choiceHtml += '<div class="choiceContainer ui-state-default choiceContainer_' + parent_id + '" id="choiceContainer_' + parent_id + '_' + index + '">';
    choiceHtml += '<div class="well choice-well-section"><button type="button" class="btn chice-drag"><i class="fa fa-bars" aria-hidden="true"></i></button><button type="button" class="btn chice-delete chice-delete_' + parent_id + '" onclick="incontext.deleteChoice(' + parent_id + ',' + index + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button><div id="ansChoice_' + parent_id + '_' + index + '" class="">';
    choiceHtml += '<div class="ansChoiceQtn_inner">';
    choiceHtml += '<label for="ansChoice_' + parent_id + '_' + index + '" class="label-name-list">Choice ' + index + '</label>';
    choiceHtml += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><input type="checkbox" id="assess_' + parent_id + '_' + index + '_check" name="assess_' + parent_id + '_' + index + '_check" class="assess_check"><input type="radio" id="assess_' + parent_id + '_' + index + '_radio" name="assess_' + parent_id + '" class="assess_radio"></span>';
    choiceHtml += '<textarea name="choices_' + parent_id + '_' + index + '" id="choices_' + parent_id + '_' + index + '" class="form-control requires text-wrap ckeditor" row="1"></textarea></div><label class="error_show" style="display:none">Choice should not be blank</label></div>';
    choiceHtml += '<div class="" id="show_option_' + parent_id + '_' + index + '" style="display:none">';
    if ($('#cmn-toggle-1').prop('checked')) {
        choiceHtml += '<div class="form-group row feedback_area" style="display:none">';
    } else {
        choiceHtml += '<div class="form-group row feedback_area">';
    }

    choiceHtml += '<div class="col-sm-6">';
    choiceHtml += '<label for="individual_' + parent_id + '_' + index + '_feedback" class="label-name-list">Choice ' + index + ' Feedback</label>';
    choiceHtml += '<textarea name="individual_' + parent_id + '_' + index + '_feedback" id="individual_' + parent_id + '_' + index + '_feedback" class="form-control text-wrap ckeditor"  row="1"></textarea></div>';
    choiceHtml += '<div class="col-sm-6">';
    choiceHtml += '<label for="individual_' + parent_id + '_' + index + '_incorrect_feedback" class="label-name-list">Choice ' + index + ' Incorrect Feedback</label>';
    choiceHtml += '<textarea name="individual_' + parent_id + '_' + index + '_incorrect_feedback" id="individual_' + parent_id + '_' + index + '_incorrect_feedback" class="form-control text-wrap ckeditor"  row="1"></textarea></div></div>';
    choiceHtml += '<div class="form-group"><label for="individual_' + parent_id + '_' + index + '_score" class="label-name-list">Choice ' + index + ' Score</label>';
    choiceHtml += '<input id="individual_' + parent_id + '_' + index + '_score"  name="individual_' + parent_id + '_' + index + '_score" type="text" class="form-control individual_score" /></div></div>';
    choiceHtml += '<div class="clearfix"></div>';
    choiceHtml += '</div></div></div>';
    choiceHtml += '<div class="form-group choice-optionselect"><a class="" id="option_button_' + parent_id + '_' + index + '" onclick="incontext.toggleOption(' + parent_id + ',' + index + ')"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="display-inline advance-option">Advanced Options</span></a></div></div>';
    return choiceHtml;
};


incontext.addChoice = function (parent_id) {
    var total_choice = $('.choiceContainer_' + parent_id).length;
    var index = total_choice + 1;
    var choiceHtml = incontext.generateChoice(parent_id, index);
    $('#choice_sortable_' + parent_id).append(choiceHtml);
    // change choice type based on cmn-toggle-1
    if ($('#cmn-toggle-1').prop('checked')) {
        $('#assess_' + parent_id + '_' + index + '_check').show(); //assess_1_check assess_1_radio
        $('#assess_' + parent_id + '_' + index + '_radio').hide();

    } else {
        $('#assess_' + parent_id + '_' + index + '_check').hide();
        $('#assess_' + parent_id + '_' + index + '_radio').show();
    }

    $(".requires").on('keyup', function () {
        if ($(this).val().length > 0) {
            $(this).parent().removeClass('has-error');
        } else {
            $(this).parent().addClass('has-error');
        }
    });

    if ($('.choiceContainer_' + parent_id).length > 1) {
        $('.chice-delete_' + parent_id).removeAttr('disabled');
    }

    $(".individual_score").keydown(function (e) {
        incontext.checkNumber(e);
    });

    // initialize Ckeditor for new choices
    common_assessment_utility.initializeCkeditor();
    common_assessment_utility.ckeditorBindEvent();

};

incontext.addHint = function () {
    var total_hint = $('.hintContainer').length;
    var index = total_hint + 1;
    var choiceHtml = incontext.generateHint(index);
    $('#hint_sortable').append(choiceHtml);
};

incontext.checkNumber = function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }


//
//$(document).ready(function () {
        var maxLength = 255;
        $(".requires").on('keyup', function () {
            if ($(this).val().length > 0) {
                if ($(this).attr('id') === 'question_title') {
                    var length = $(this).val().length;
                    $(this).parent().removeClass('has-error');
                    var length = maxLength - length;
                    $('#chars').text(length);
                } else {
                    $(this).parent().removeClass('has-error');
                }
            } else {
                if ($(this).attr('id') === 'question_title') {
                    $('#chars').text(255);
                }
                $(this).parent().addClass('has-error');
            }
        });


        $('.assess').off().on('change', function (e) {
            if (!$('#cmn-toggle-1').prop('checked')) {
                $(".assess").prop('checked', false);
            }
            $(this).prop('checked', true);
        });

        $('#generate_data').off().on('click', function (e) {
            var res = incontext.generateQuestionData();
            res = JSON.stringify(res);
        });

        $('#cmn-toggle-1').off().on('change', function (e) {
            incontext.toggleChoice();
        });

        $('.assess_check').hide();
        $('.singel-ans').addClass('active');
        $('.multiple-ans').addClass('inactive');

        $("#choice_sortable_1").sortable({
            handle: ".chice-drag",
            cancel: '',
            start: function (event, ui) {
                ui.item.data('originIndex', ui.item.index());
            },
            stop: function (event, ui) {
                var parent_id = $(this).attr('id').split('_')[2];
                incontext.rearrangeChoice(event, ui, parent_id);
                // delete ckeditor instances of all choices
                var totalContainer = $('.choiceContainer_' + parent_id).length;
                incontext.ckeditorDeleteInstances(totalContainer, parent_id, false);
                // create ckeditor new instances for all choices
                incontext.ckeditorCreateInstances(parent_id, false);
                common_assessment_utility.ckeditorBindEvent();
            },
        });

        $(".individual_score").keydown(function (e) {
            incontext.checkNumber(e);
        });

        /*$("#hint_sortable").sortable({
         handle: ".chice-drag",
         cancel: '',
         });*/

//});

        incontext.toggleOption = function (parent_id, id) {
            $('#show_option_' + parent_id + '_' + id).slideToggle();
        };

        incontext.toggleChoice = function () {
            $('.assess_radio').prop('checked', false);
            $('.assess_check').prop('checked', false);
            if ($('#cmn-toggle-1').prop('checked')) {
                $('.singel-ans').removeClass('active');
                $('.singel-ans').addClass('inactive');
                $('.multiple-ans').removeClass('inactive');
                $('.multiple-ans').addClass('active');
                $('.assess_radio').hide();
                $('.assess_check').show();
                $('.feedback_area').hide();
            } else
            {
                $('.singel-ans').removeClass('inactive');
                $('.singel-ans').addClass('active');
                $('.multiple-ans').removeClass('active');
                $('.multiple-ans').addClass('inactive');
                $('.assess_check').hide();
                $('.assess_radio').show();
                $('.feedback_area').show();
            }
        };

        incontext.toggleFeedback = function () {
            if ($('#cmn-toggle-1').prop('checked')) {
                $('.feedback_area').hide();
            } else {
                $('.feedback_area').show();
            }
        }


        incontext.generateQuestionData = function () {
            // get question details
            //var question_stem = incontext.replaceNewline($('#question_stem').val());
            var question_stem = common_assessment_utility.getFieldValue('question_stem', true);
            var question_title = $('#question_title').val();
            //var instruction_text = incontext.replaceNewline($('#instruction_text').val());
            var instruction_text = common_assessment_utility.getFieldValue('instruction_text', true);
            var advJSONData = {};
            var question_type = 'in-context';
            advJSONData['question_type'] = {text: question_type};
            advJSONData['question_response_type'] = {text: ($('#cmn-toggle-1').prop('checked')) ? 'mcms' : 'mcss'};
            advJSONData['question_interaction'] = {text: 'clickable'};
            advJSONData['question_layout'] = {text: 'vertical'};
            advJSONData['question_stem'] = {text: question_stem};
            advJSONData['question_title'] = {text: $('#question_title').val()};
            advJSONData['instruction_text'] = {text: instruction_text};
            var sentences = [];
            var total_sentences = $('.sentence').length;
            for (var j = 1; j <= total_sentences; j++) {
                var sentence_text = $('#sentence_' + j).val();
                //var sentence_text = common_assessment_utility.getFieldValue('sentence_' + j, true)
                var choices = [];
                var total_choice = $('.choice-box_' + j + ' > .choiceContainer_' + j).length;
                //var total_choice = common_assessment_utility.getFieldValue('.choice-box_' + j + ' > .choiceContainer_' + j,true).length;
                var assess_value = false;
                var correct_answer = []
                for (var i = 1; i <= total_choice; i++) {
                    if ($('#choices_' + j + '_' + i).val() != null) {
                        assess_value = false;
                        if ($('#cmn-toggle-1').prop('checked')) {
                            if ($('#assess_' + j + '_' + i + '_check').prop('checked')) {
                                assess_value = true;
                                correct_answer.push(i);
                            }
                        } else {
                            if ($('#assess_' + j + '_' + i + '_radio').prop('checked')) {
                                assess_value = true;
                                correct_answer.push(i);
                            }
                        }
                        // push choice inside a choices data
                        //choices.push({choiceid: i, assess: assess_value, text: incontext.replaceNewline($('#choices_' + j + '_' + i).val()), correct_feedback: incontext.replaceNewline($('#individual_' + j + '_' + i + '_feedback').val()), incorrect_feedback: incontext.replaceNewline($('#individual_' + j + '_' + i + '_incorrect_feedback').val()), score: $('#individual_' + j + '_' + i + '_score').val(), pinanswer: 0});
                        choices.push({choiceid: i, assess: assess_value, text: common_assessment_utility.getFieldValue('choices_' + j + '_' + i, true), correct_feedback: common_assessment_utility.getFieldValue('individual_' + j + '_' + i + '_feedback', true), incorrect_feedback: common_assessment_utility.getFieldValue('individual_' + j + '_' + i + '_incorrect_feedback', true), score: $('#individual_' + j + '_' + i + '_score').val(), pinanswer: 0});
                    }

                }
                sentences.push({sentence_id: j, sentence: {text: sentence_text}, question_title: {text: sentence_text}, question_stem: {text: sentence_text}, instruction_text: {text: ''}, question_type: advJSONData['question_response_type'], question_interaction: {text: 'clickable'}, question_layout: {text: 'vertical'}, choices: choices, correct_answer: correct_answer.join(','), global_correct_feedback: {text: ''}, global_incorrect_feedback: {text: ''}, media: {type: 'no-mdeia'}});
            }
            advJSONData['sentences'] = sentences;

            if ($("input[name='media']:checked").val() === 'no-media') {
                advJSONData['media'] = {type: "no-media"};
            } else {
                if ($('#ext_res_title').val() == '') {
                    advJSONData['media'] = {type: "no-media"};
                } else {
                    if ($('#media_type').val() === 'brightcove') {
                        advJSONData['media'] = {type: $('#media_type').val(), brightcove_id: $('#ext_res_title_id').val(), url: $('#ext_res_url').val(), height: $('#ext_res_height').val(), width: $('#ext_res_width').val()};
                    } else {
                        advJSONData['media'] = {type: $('#media_type').val(), url: $('#ext_res_url').val(), title: $('#ext_res_title').val(), asset_type: $('#ext_res_type').val()};
                    }
                }

            }
            return advJSONData;
        };


        incontext.renderQuestionData = function (question_data) {
            var question_data_obj = question_data;
            if (typeof question_data !== "object") {
                question_data_obj = JSON.parse(question_data);
            }

            if (question_data_obj.question_response_type.text == 'mcms') {
                $('#cmn-toggle-1').prop('checked', true);
                incontext.toggleChoice();
            }

            // render question details
            //$('#question_stem').val(incontext.replaceBr(question_data_obj.question_stem.text));
            common_assessment_utility.setFieldValue('question_stem', question_data_obj.question_stem.text, true);
            $('#question_title').val(question_data_obj.question_title.text);
            var remaining_length = 255 - question_data_obj.question_title.text.length;
            $('#chars').text(remaining_length);
            //$('#instruction_text').val(incontext.replaceBr(question_data_obj.instruction_text.text));
            //console.info(question_data_obj.instruction_text.text);
            common_assessment_utility.setFieldValue('instruction_text', question_data_obj.instruction_text.text, true);
            // render choices
            var total_sentences = question_data_obj.sentences.length;
            for (var i = 1; i <= total_sentences; i++) {
                //Render Sentences
                var sentence_data = question_data_obj.sentences[i - 1];
                if (i > 1) {
                    incontext.addSentence();
                }
                $('#sentence_' + i).val(sentence_data.sentence.text);
                //common_assessment_utility.setFieldValue('sentence_' + i, sentence_data.sentence.text, true);
                var choices_length = sentence_data.choices.length;
                //Remove Extra Choice Fields
                if (choices_length < 2) {
                    $('#choiceContainer_' + i + '_2').remove();
                    $('#choiceContainer_' + i + '_3').remove();
                    // delete second and third choice from frist sentence
                    incontext.ckeditorDeleteChoicesInstances(2, 3, i);
                } else if (choices_length < 3) {
                    $('#choiceContainer_' + i + '_3').remove();
                    // delete third choice from frist sentence
                    incontext.ckeditorDeleteChoicesInstances(3, 3, i);
                }
                //Render Choices
                var choice_type = '_radio';
                for (var j = 1; j <= choices_length; j++) {
                    if (j > 3) {
                        incontext.addChoice(i);
                    }

                    if (sentence_data.question_type.text == 'mcms') {
                        choice_type = '_check';
                    }

                    if (JSON.parse(sentence_data.choices[j - 1].assess)) { // convert into boolean
                        $('#assess_' + i + '_' + j + choice_type).prop('checked', true);
                    }
                    //$('#choices_' + i + '_' + j).val(incontext.replaceBr(sentence_data.choices[j - 1].text));
                    //$('#individual_' + i + '_' + j + '_feedback').val(incontext.replaceBr(sentence_data.choices[j - 1].correct_feedback));
                    //$('#individual_' + i + '_' + j + '_incorrect_feedback').val(incontext.replaceBr(sentence_data.choices[j - 1].incorrect_feedback));                    
                    common_assessment_utility.setFieldValue('choices_' + i + '_' + j, sentence_data.choices[j - 1].text, true);
                    common_assessment_utility.setFieldValue('individual_' + i + '_' + j + '_feedback', sentence_data.choices[j - 1].correct_feedback, true);
                    common_assessment_utility.setFieldValue('individual_' + i + '_' + j + '_incorrect_feedback', sentence_data.choices[j - 1].incorrect_feedback, true);
                    $('#individual_' + i + '_' + j + '_score').val(sentence_data.choices[j - 1].score);
                }

                if ($('.choiceContainer_' + i).length == 1) {
                    $('.chice-delete_' + i).attr('disabled', 'disabled');
                }
            }

        };

        incontext.deleteChoice = function (parent_id, choice_index) {
            // if choices is more than one
            var totalContainer = $('.choiceContainer_' + parent_id).length;
            if ($('.choiceContainer_' + parent_id).length > 1) {
                $('#choiceContainer_' + parent_id + '_' + choice_index).remove();

                if ($('.choiceContainer_' + parent_id).length == 1) {
                    $('.chice-delete_' + parent_id).attr('disabled', 'disabled');
                }

                $('.choiceContainer_' + parent_id).each(function (index, value) {
                    var oldIndex = $(this).attr('id').split('_')[2];
                    var newIndex = index + 1;
                    if (oldIndex != newIndex) {
                        incontext.modifyChoiceHtml(parent_id, newIndex, oldIndex, this);
                    }
                });
                // delete ckeditor instances of all choices
                incontext.ckeditorDeleteInstances(totalContainer, parent_id, false);
                // create ckeditor new instances for all choices
                incontext.ckeditorCreateInstances(parent_id, false);
                common_assessment_utility.ckeditorBindEvent();
            }
        };

        incontext.deleteHint = function (hint_index) {
            if ($('.hintContainer').length > 1) {
                $('#hintContainer_' + hint_index).remove();
                var total_choice = $('.hintContainer').length;
                for (i = 1; i <= total_choice + 1; i++) {
                    if ($('#hintContainer_' + i).html()) {
                        if (i > hint_index) {
                            var newId = 'hint_' + (i - 1);
                            newId = newId.toString();
                            $('#hint_' + i).attr('id', newId);
                        }

                    } else {
                        console.log('container not found');
                    }
                }
            }

        }




        $(".requires-modal").on('keyup', function () {
            if ($(this).val().length > 0) {
                $(this).parent().removeClass('has-error');
            } else {
                $(this).parent().addClass('has-error');
            }
        });

        incontext.rearrangeChoice = function (event, ui, parent_id) {
            // change all choices html if needed
            $('.choiceContainer_' + parent_id).each(function (index, value) {
                var oldIndex = $(this).attr('id').split('_')[2];
                var newIndex = index + 1;
                if (oldIndex != newIndex) {
                    incontext.modifyChoiceHtml(parent_id, newIndex, oldIndex, this);
                }
            });
        };

        incontext.modifyChoiceHtml = function (parent_id, newIndex, oldIndex, curObj) {

            var newId = 'choiceContainer_' + parent_id + '_' + newIndex;
            var choiceId = 'choices_' + parent_id + '_' + newIndex;
            var checkId = 'assess_' + parent_id + '_' + newIndex + '_check';
            var radioId = 'assess_' + parent_id + '_' + newIndex + '_radio';
            var feedabackId = 'individual_' + parent_id + '_' + newIndex + '_feedback';
            var incorrectfeedabackId = 'individual_' + parent_id + '_' + newIndex + '_incorrect_feedback';
            var individualScoreId = 'individual_' + parent_id + '_' + newIndex + '_score';
            var option_button_id = 'option_button_' + parent_id + '_' + newIndex;
            radioId = radioId.toString();
            checkId = checkId.toString();
            newId = newId.toString();
            feedabackId = feedabackId.toString();
            incorrectfeedabackId = incorrectfeedabackId.toString();
            individualScoreId = individualScoreId.toString();
            choiceId = choiceId.toString();
            option_button_id = option_button_id.toString();
            // set new container id
            $(curObj).attr('id', newId);
            $(curObj).find("#ansChoice_" + oldIndex).attr('id', 'ansChoice_' + parent_id + '_' + newIndex);
            $(curObj).find("label[for='ansChoice_" + parent_id + '_' + oldIndex + "']").attr('for', 'ansChoice_' + parent_id + '_' + newIndex);
            $(curObj).find("label[for='ansChoice_" + parent_id + '_' + newIndex + "']").html('Choice ' + newIndex);
            $(curObj).find('.assess_check').attr('id', checkId);
            $(curObj).find('.assess_radio').attr('id', radioId);
            $(curObj).find('#choices_' + parent_id + '_' + oldIndex).attr({id: choiceId, name: choiceId});

            $(curObj).find('#individual_' + parent_id + '_' + oldIndex + '_feedback').attr({id: feedabackId, name: feedabackId});
            $(curObj).find('#individual_' + parent_id + '_' + oldIndex + '_incorrect_feedback').attr({id: incorrectfeedabackId, name: incorrectfeedabackId});
            $(curObj).find('#individual_' + parent_id + '_' + oldIndex + '_score').attr({id: individualScoreId, name: individualScoreId});

            $(curObj).find('.chice-delete_' + parent_id).removeAttr('onclick');
            $(curObj).find('.chice-delete_' + parent_id).attr('onclick', 'incontext.deleteChoice( ' + parent_id + ',' + newIndex + ')');

            $(curObj).find('#option_button_' + parent_id + '_' + oldIndex).attr('id', option_button_id);
            $(curObj).find('#show_option_' + parent_id + '_' + oldIndex).attr('id', 'show_option_' + parent_id + '_' + newIndex);

            $(curObj).find('#' + option_button_id).removeAttr('onclick');
            $(curObj).find('#' + option_button_id).attr('onclick', 'incontext.toggleOption(' + parent_id + ',' + newIndex + ')');

            $(curObj).find("label[for='individual_" + parent_id + '_' + oldIndex + "_feedback']").attr('for', 'individual_' + parent_id + '_' + newIndex + '_feedback');
            $(curObj).find("label[for='individual_" + parent_id + '_' + oldIndex + "_incorrect_feedback']").attr('for', 'individual_' + parent_id + '_' + newIndex + '_incorrect_feedback');
            $(curObj).find("label[for='individual_" + parent_id + '_' + oldIndex + "_score']").attr('for', 'individual_' + parent_id + '_' + newIndex + '_score');
            $(curObj).find("label[for='individual_" + parent_id + '_' + newIndex + "_feedback']").html('Choice ' + newIndex + ' Feedback');
            $(curObj).find("label[for='individual_" + parent_id + '_' + newIndex + "_incorrect_feedback']").html('Choice ' + newIndex + ' Incorrect Feedback');
            $(curObj).find("label[for='individual_" + parent_id + '_' + newIndex + "_score']").html('Choice ' + newIndex + ' Score');
        };

//$("textarea").keyup(function () {
//    var data = $(this).val().replace(/[\r\n]/g, "<br/>");
//    $(this).val(data);
//});

        incontext.replaceNewline = function (string) {
            var data = string.replace(/(\r\n|\n|\r)/gm, "<br/>");
            return data;
        };

        incontext.replaceBr = function (string) {
            var regex = /<br\s*[\/]?>/gi;
            var data = string.replace(regex, "\n");
            return data;
        };


        incontext.addSentence = function () {
            var sentenceVal = $('.sentence').length + 1;
            var strVar = "";
            strVar += "<div class=\"sentence\" style='margin-top:10px' id=\"sentence_div_" + sentenceVal + "\">";
            strVar += "  <div class=\"sentence-block\">";
            strVar += "    <label class=\"sentenceLabel\">Sentence " + sentenceVal + "<\/label>";
            strVar += "<a href=\"javascipt:void(0)\" class=\"deleteSentence\" onclick=\"incontext.deleteSentence(" + sentenceVal + ")\" style=\"float: right;font-size: 20px;color: #d9534f;\"><i class=\"fa fa-trash-o\"></i></a>";
            strVar += "    <textarea class=\"form-control requires sentence-block-text\" id=\"sentence_" + sentenceVal + "\"><\/textarea><label class='error_show' style='display:none'>Sentence should not be blank</label>";
            strVar += "    <div class=\"choice-point-ANSWER\"><label>Answer Choices<\/label> <a class=\"btn btn-default btn-add-block\" id=\"add_choice\" onclick=\"incontext.addChoice(" + sentenceVal + ")\"><i class=\"fa fa-plus\"><\/i> Add<\/a><\/div>";
            strVar += "  <\/div>";
            strVar += "  <div class=\"clearfix\"><\/div>";
            strVar += "  <div class=\"ui-sortable choice-box choice-box_" + sentenceVal + "\" id=\"choice_sortable_" + sentenceVal + "\">";
            strVar += "    <div class=\"choiceContainer ui-state-default choiceContainer_" + sentenceVal + "\" id=\"choiceContainer_" + sentenceVal + "_1\">";
            strVar += "      <div class=\"well choice-well-section\">";
            strVar += "        <div id=\"ansChoice_" + sentenceVal + "_1\" class=\"\">";
            strVar += "          <div class=\"ansChoiceQtn_inner\">";
            strVar += "            <div class=\"form-group\">";
            strVar += "              <label for=\"ansChoice_" + sentenceVal + "_1\" class=\"label-name-list\">Choice 1<\/label>";
            strVar += "              <div class=\"input-group\"> ";
            strVar += "                <span class=\"input-group-addon\"> ";
            strVar += "                <input type=\"checkbox\" id=\"assess_" + sentenceVal + "_1_check\" name=\"assess_1_check\" class=\"assess_check\">";
            strVar += "                <input type=\"radio\" id=\"assess_" + sentenceVal + "_1_radio\" name=\"assess_" + sentenceVal + "\" class=\"assess_radio\">";
            strVar += "                <\/span> ";
            strVar += "                <textarea name=\"choices_" + sentenceVal + "_1\" id=\"choices_" + sentenceVal + "_1\" class=\"form-control requires text-wrap ckeditor\" rows=\"1\"><\/textarea>";
            strVar += "              <\/div>";
            strVar += "              <label class=\"error_show\" style=\"display:none\">Choice should not be blank<\/label>";
            strVar += "            <\/div>";
            strVar += "            <div class=\"\" id=\"show_option_" + sentenceVal + "_1\" style=\"display:none\">";
            strVar += "              <div class=\"form-group row feedback_area\">";
            strVar += "                <div class=\"col-sm-6\">";
            strVar += "                  <label for=\"individual_" + sentenceVal + "_1_feedback\" class=\"label-name-list\">Choice 1 Correct Feedback<\/label>";
            strVar += "                  <textarea name=\"individual_" + sentenceVal + "_1_feedback\" id=\"individual_" + sentenceVal + "_1_feedback\" class=\"form-control text-wrap ckeditor\" rows=\"1\"><\/textarea>";
            strVar += "                <\/div>";
            strVar += "                <div class=\"col-sm-6\">";
            strVar += "                  <label for=\"individual_" + sentenceVal + "_1_incorrect_feedback\" class=\"label-name-list\">Choice 1 Incorrect Feedback<\/label>";
            strVar += "                  <textarea name=\"individual_" + sentenceVal + "_1_incorrect_feedback\" id=\"individual_" + sentenceVal + "_1_incorrect_feedback\" class=\"form-control text-wrap ckeditor\"  row=\"1\"><\/textarea>";
            strVar += "                <\/div>";
            strVar += "              <\/div>";
            strVar += "              <div class=\"form-group\">";
            strVar += "                <label for=\"individual_" + sentenceVal + "_1_score\" class=\"label-name-list\">Choice 1 Score<\/label>";
            strVar += "                <input id=\"individual_" + sentenceVal + "_1_score\"  name=\"individual_" + sentenceVal + "_1_score\" type=\"text\" class=\"form-control individual_score\"\/>";
            strVar += "              <\/div>";
            strVar += "            <\/div>";
            strVar += "            <div class=\"clearfix\"><\/div>";
            strVar += "          <\/div>";
            strVar += "        <\/div>";
            strVar += "        <button type=\"button\" class=\"btn chice-drag\">";
            strVar += "        <i class=\"fa fa-bars\" aria-hidden=\"true\"><\/i>";
            strVar += "        <\/button>";
            strVar += "        <button type=\"button\" class=\"btn chice-delete chice-delete_" + sentenceVal + "\" onclick=\"incontext.deleteChoice(" + sentenceVal + ",1)\">";
            strVar += "        <i class=\"fa fa-trash-o\" aria-hidden=\"true\"><\/i>";
            strVar += "        <\/button>";
            strVar += "      <\/div>";
            strVar += "      <div class=\"form-group choice-optionselect\">";
            strVar += "        <a class=\"\" id=\"option_button_" + sentenceVal + "_1\" onclick=\"incontext.toggleOption(" + sentenceVal + ",1)\">";
            strVar += "        <i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"><\/i> ";
            strVar += "        <span class=\"display-inline advance-option\">Advanced Options<\/span>";
            strVar += "        <\/a>";
            strVar += "      <\/div>";
            strVar += "    <\/div>";
            strVar += "    <div class=\"choiceContainer ui-state-default choiceContainer_" + sentenceVal + "\" id=\"choiceContainer_" + sentenceVal + "_2\">";
            strVar += "      <div class=\"well choice-well-section\">";
            strVar += "        <div id=\"ansChoice_" + sentenceVal + "_2\" class=\"\">";
            strVar += "          <div class=\"ansChoiceQtn_inner\">";
            strVar += "            <div class=\"form-group\">";
            strVar += "              <label for=\"ansChoice_" + sentenceVal + "_2\" class=\"label-name-list\">Choice 2<\/label>";
            strVar += "              <div class=\"input-group\">";
            strVar += "                <span class=\"input-group-addon\">";
            strVar += "                <input type=\"checkbox\" id=\"assess_" + sentenceVal + "_2_check\" name=\"assess_" + sentenceVal + "_2_check\" class=\"assess_check\">";
            strVar += "                <input type=\"radio\" id=\"assess_" + sentenceVal + "_2_radio\" name=\"assess_" + sentenceVal + "\" class=\"assess_radio\">";
            strVar += "                <\/span>";
            strVar += "                <textarea name=\"choices_" + sentenceVal + "_2\" id=\"choices_" + sentenceVal + "_2\" class=\"form-control requires ckeditor\"  row=\"1\"><\/textarea>";
            strVar += "              <\/div>";
            strVar += "              <label class=\"error_show\" style=\"display:none\">Choice should not be blank<\/label>";
            strVar += "            <\/div>";
            strVar += "            <div class=\"\" id=\"show_option_" + sentenceVal + "_2\" style=\"display:none\">";
            strVar += "              <div class=\"form-group row feedback_area\">";
            strVar += "                <div class=\"col-sm-6\">";
            strVar += "                  <label for=\"individual_" + sentenceVal + "_2_feedback\" class=\"label-name-list\">Choice 2 Correct Feedback<\/label>";
            strVar += "                  <textarea name=\"individual_" + sentenceVal + "_2_feedback\" id=\"individual_" + sentenceVal + "_2_feedback\" class=\"form-control ckeditor\"  rows=\"1\"><\/textarea>";
            strVar += "                <\/div>";
            strVar += "                <div class=\"col-sm-6\">";
            strVar += "                  <label for=\"individual_" + sentenceVal + "_2_incorrect_feedback\" class=\"label-name-list\">Choice 2 Incorrect Feedback<\/label>";
            strVar += "                  <textarea name=\"individual_" + sentenceVal + "_2_incorrect_feedback\" id=\"individual_" + sentenceVal + "_2_incorrect_feedback\" class=\"form-control ckeditor\"  rows=\"1\"><\/textarea>";
            strVar += "                <\/div>";
            strVar += "              <\/div>";
            strVar += "              <div class=\"form-group\">";
            strVar += "                <label for=\"individual_" + sentenceVal + "_2_score\" class=\"label-name-list\">Choice 2 Score<\/label>";
            strVar += "                <input id=\"individual_" + sentenceVal + "_2_score\"  name=\"individual_" + sentenceVal + "_2_score\" type=\"text\" class=\"form-control individual_score\" \/>";
            strVar += "              <\/div>";
            strVar += "            <\/div>";
            strVar += "            <div class=\"clearfix\"><\/div>";
            strVar += "          <\/div>";
            strVar += "        <\/div>";
            strVar += "        <button type=\"button\" class=\"btn chice-drag\">";
            strVar += "        <i class=\"fa fa-bars\" aria-hidden=\"true\"><\/i>";
            strVar += "        <\/button>";
            strVar += "        <button type=\"button\" class=\"btn chice-delete chice-delete_" + sentenceVal + "\" onclick=\"incontext.deleteChoice(" + sentenceVal + ",2)\">";
            strVar += "        <i class=\"fa fa-trash-o\" aria-hidden=\"true\"><\/i>";
            strVar += "        <\/button>";
            strVar += "      <\/div>";
            strVar += "      <div class=\"form-group choice-optionselect\"><a class=\"\" id=\"option_button_" + sentenceVal + "_2\" onclick=\"incontext.toggleOption(" + sentenceVal + ",2)\"><i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"><\/i> <span class=\"display-inline advance-option\">Advanced Options<\/span><\/a><\/div>";
            strVar += "    <\/div>";
            strVar += "    <div class=\"choiceContainer ui-state-default choiceContainer_" + sentenceVal + "\" id=\"choiceContainer_" + sentenceVal + "_3\">";
            strVar += "      <div class=\"well choice-well-section\">";
            strVar += "        <div id=\"ansChoice_" + sentenceVal + "_3\" class=\"\">";
            strVar += "          <div class=\"ansChoiceQtn_inner\">";
            strVar += "            <div class=\"form-group\">";
            strVar += "              <label for=\"ansChoice_" + sentenceVal + "_3\" class=\"label-name-list\">Choice 3<\/label>";
            strVar += "              <div class=\"input-group\">";
            strVar += "                <span class=\"input-group-addon\">";
            strVar += "                <input type=\"checkbox\" id=\"assess_" + sentenceVal + "_3_check\" name=\"assess_" + sentenceVal + "_3_check\" class=\"assess_check\">";
            strVar += "                <input type=\"radio\" id=\"assess_" + sentenceVal + "_3_radio\" name=\"assess_" + sentenceVal + "\" class=\"assess_radio\">";
            strVar += "                <\/span>";
            strVar += "                <textarea name=\"choices_" + sentenceVal + "_3\" id=\"choices_" + sentenceVal + "_3\" class=\"form-control requires text-wrap ckeditor\"  row=\"1\"><\/textarea>";
            strVar += "              <\/div>";
            strVar += "              <label class=\"error_show\" style=\"display:none\">Choice should not be blank<\/label>";
            strVar += "            <\/div>";
            strVar += "            <div class=\"\" id=\"show_option_" + sentenceVal + "_3\" style=\"display:none\">";
            strVar += "              <div class=\"form-group row feedback_area\">";
            strVar += "                <div class=\"col-sm-6\">";
            strVar += "                  <label for=\"individual_" + sentenceVal + "_3_feedback\" class=\"label-name-list\">Choice 3 Correct Feedback<\/label>";
            strVar += "                  <textarea name=\"individual_" + sentenceVal + "_3_feedback\" id=\"individual_" + sentenceVal + "_3_feedback\" class=\"form-control text-wrap ckeditor\"  row=\"1\"><\/textarea>";
            strVar += "                <\/div>";
            strVar += "                <div class=\"col-sm-6\">";
            strVar += "                  <label for=\"individual_" + sentenceVal + "_3_incorrect_feedback\" class=\"label-name-list\">Choice 3 Incorrect Feedback<\/label>";
            strVar += "                  <textarea name=\"individual_" + sentenceVal + "_3_incorrect_feedback\" id=\"individual_" + sentenceVal + "_3_incorrect_feedback\" class=\"form-control text-wrap ckeditor\"  row=\"1\"><\/textarea>";
            strVar += "                <\/div>";
            strVar += "              <\/div>";
            strVar += "              <div class=\"form-group\">";
            strVar += "                <label for=\"individual_" + sentenceVal + "_3_score\" class=\"label-name-list\">Choice 3 Score<\/label>";
            strVar += "                <input id=\"individual_" + sentenceVal + "_3_score\"  name=\"individual_" + sentenceVal + "_3_score\" type=\"text\" class=\"form-control individual_score\" \/>";
            strVar += "              <\/div>";
            strVar += "            <\/div>";
            strVar += "            <div class=\"clearfix\"><\/div>";
            strVar += "          <\/div>";
            strVar += "          <button type=\"button\" class=\"btn chice-drag\"><i class=\"fa fa-bars\" aria-hidden=\"true\"><\/i><\/button><button type=\"button\" class=\"btn chice-delete chice-delete_" + sentenceVal + "\" onclick=\"incontext.deleteChoice(" + sentenceVal + ",3)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"><\/i><\/button>";
            strVar += "        <\/div>";
            strVar += "      <\/div>";
            strVar += "      <div class=\"form-group choice-optionselect\"><a class=\"\" id=\"option_button_" + sentenceVal + "_3\" onclick=\"incontext.toggleOption(" + sentenceVal + ",3)\"><i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"><\/i> <span class=\"display-inline advance-option\">Advanced Options<\/span><\/a><\/div>";
            strVar += "    <\/div>";
            strVar += "  <\/div>";
            strVar += "<\/div>";
            $('.appendData').append(strVar);
            $("#choice_sortable_" + sentenceVal).sortable({
                handle: ".chice-drag",
                cancel: '',
                start: function (event, ui) {
                    ui.item.data('originIndex', ui.item.index());
                },
                stop: function (event, ui) {
                    var parent_id = $(this).attr('id').split('_')[2];
                    incontext.rearrangeChoice(event, ui, parent_id);
                    // delete ckeditor instances of all choices
                    var totalContainer = $('.choiceContainer_' + parent_id).length;
                    incontext.ckeditorDeleteInstances(totalContainer, parent_id, false);
                    // create ckeditor new instances for all choices
                    incontext.ckeditorCreateInstances(parent_id, false);
                    common_assessment_utility.ckeditorBindEvent();
                },
            });

            if ($('#cmn-toggle-1').prop('checked')) {
                $('.assess_check').show(); //assess_1_check assess_1_radio
                $('.assess_radio').hide();

            } else {
                $('.assess_check').hide();
                $('.assess_radio').show();
            }

            $(".individual_score").keydown(function (e) {
                incontext.checkNumber(e);
            });

            incontext.toggleFeedback();
            $('.deleteSentence').attr('style', 'float: right;font-size: 20px;color: #d9534f;');
            // initialize  ckeditor
            common_assessment_utility.initializeCkeditor();
            common_assessment_utility.ckeditorBindEvent();
        };

        incontext.deleteSentence = function (parent_id) {
            //debugger;
            // total senetence and choices length
            var totalSentenceArray = [];
            var totalSentence = $('.sentence').length;
            for (var i = 1; i <= totalSentence; i++) {
                var totalContainer = $('.choiceContainer_' + i).length;
                totalSentenceArray[i] = {'totalContainer': totalContainer};
            }

            if ($('.sentence').length > 1) {
                $('#sentence_div_' + parent_id).remove();
                incontext.reorderDeleteSentence();
                $('.deleteSentence').removeAttr('disabled');
                if ($('.sentence').length == 1) {
                    $('.deleteSentence').attr('style', 'float: right;font-size: 20px;color: #d9534f;cursor: not-allowed;');
                }
                // delete ckeditor instances of all choices
                incontext.ckeditorDeleteInstances(totalSentenceArray, parent_id, true);
                // create ckeditor new instances for all choices
                incontext.ckeditorCreateInstances(parent_id, true);
                common_assessment_utility.ckeditorBindEvent();
            } else {
                $('.deleteSentence').attr('disabled', 'disabled');
            }
        };

        incontext.reorderDeleteSentence = function () {
            $('.sentence').each(function (index, value) {
                var oldIndex = $(this).attr('id').split('_')[2];
                var newIndex = index + 1;
                if (oldIndex != newIndex) {
                    incontext.newOrderSentence(newIndex, oldIndex, this);
                }
            });
        };

        incontext.newOrderSentence = function (newIndex, oldIndex, curObj) {
            var sentenceId = 'sentence_div_' + newIndex;
            $(curObj).attr('id', sentenceId);
            $(curObj).find("#sentence_" + oldIndex).attr('id', 'sentence_' + newIndex);
            $(curObj).find('.deleteSentence').removeAttr('onclick');
            $(curObj).find('.deleteSentence').attr('onclick', 'incontext.deleteSentence(' + newIndex + ')');
            $(curObj).find('.sentenceLabel').html('Sentence ' + newIndex);
            $('.choiceContainer_' + oldIndex).attr('class', 'choiceContainer ui-state-default choiceContainer_' + newIndex);
            $(curObj).find('#add_choice').removeAttr('onclick');
            $(curObj).find('#add_choice').attr('onclick', 'incontext.addChoice(' + newIndex + ')');
            $(curObj).find('.choice-box_' + oldIndex).attr('class', 'ui-sortable choice-box choice-box_' + newIndex);
            $(curObj).find('.choice-box_' + newIndex).attr('id', 'choice_sortable_' + newIndex);

            $('.choiceContainer_' + newIndex).each(function (index, value) {
                var i = index + 1;
                $(this).attr('id', 'choiceContainer_' + newIndex + '_' + i);
                $(this).find('#ansChoice_' + oldIndex + '_' + i).attr('id', 'ansChoice_' + newIndex + '_' + i);
                $(this).find("label[for='ansChoice_" + oldIndex + '_' + i + "']").attr('for', 'ansChoice_' + newIndex + '_' + i);
                $(this).find("label[for='ansChoice_" + oldIndex + '_' + i + "']").html('Choice ' + i);
                $(this).find('.assess_check').attr('id', 'assess_' + newIndex + '_' + i + '_check');
                $(this).find('.assess_radio').attr('id', 'assess_' + newIndex + '_' + i + '_radio');
                var choiceId = 'choices_' + newIndex + '_' + i;
                $(this).find('#choices_' + oldIndex + '_' + i).attr({id: choiceId, name: choiceId});
                var feedabackId = 'individual_' + newIndex + '_' + i + '_feedback';
                $(this).find('#individual_' + oldIndex + '_' + i + '_feedback').attr({id: feedabackId, name: feedabackId});
                var incorrectfeedabackId = 'individual_' + newIndex + '_' + i + '_incorrect_feedback';
                $(this).find('#individual_' + oldIndex + '_' + i + '_incorrect_feedback').attr({id: incorrectfeedabackId, name: incorrectfeedabackId});
                var individualScoreId = 'individual_' + newIndex + '_' + i + '_score';
                $(this).find('#individual_' + oldIndex + '_' + i + '_score').attr({id: individualScoreId, name: individualScoreId});

                $(this).find('.chice-delete_' + oldIndex).removeAttr('onclick');
                $(this).find('.chice-delete_' + oldIndex).attr('onclick', 'incontext.deleteChoice( ' + newIndex + ',' + i + ')');
                var option_button_id = 'option_button_' + newIndex + '_' + i;
                $(this).find('#option_button_' + oldIndex + '_' + i).attr('id', option_button_id);
                $(this).find('#show_option_' + oldIndex + '_' + i).attr('id', 'show_option_' + newIndex + '_' + i);

                $(this).find('#' + option_button_id).removeAttr('onclick');
                $(this).find('#' + option_button_id).attr('onclick', 'incontext.toggleOption(' + newIndex + ',' + i + ')');

                $(this).find("label[for='individual_" + oldIndex + '_' + i + "_feedback']").attr('for', 'individual_' + newIndex + '_' + i + '_feedback');
                $(this).find("label[for='individual_" + oldIndex + '_' + i + "_incorrect_feedback']").attr('for', 'individual_' + newIndex + '_' + i + '_incorrect_feedback');
                $(this).find("label[for='individual_" + oldIndex + '_' + i + "_score']").attr('for', 'individual_' + newIndex + '_' + i + '_score');
                $(this).find("label[for='individual_" + newIndex + '_' + i + "_feedback']").html('Choice ' + i + ' Feedback');
                $(this).find("label[for='individual_" + newIndex + '_' + i + "_incorrect_feedback']").html('Choice ' + i + ' Incorrect Feedback');
                $(this).find("label[for='individual_" + newIndex + '_' + i + "_score']").html('Choice ' + i + ' Score');

            });
        };

        incontext.ckeditorDeleteInstances = function (totalContainer, index, fromSentence) {
            // delete all the choices container
            if (fromSentence) { // delete sentence instances
                if (totalContainer.constructor === Array && totalContainer.length) {
                    var totalSentence = totalContainer.length;
                    for (var i = 1; i <= totalSentence; i++) {
                        // delete instance from sentence
                        //if (typeof CKEDITOR.instances['sentence_' + i] == "object") {
                        // CKEDITOR.instances['sentence_' + i].destroy();
                        // delete choices instance from sentence
                        //console.log(totalContainer[i]);
                        if (totalContainer[i] !== undefined) {
                            incontext.ckeditorDeleteChoicesInstances(1, totalContainer[i]['totalContainer'], i);
                        }
                        //}
                    }
                }

            } else { // delete choice instances
                incontext.ckeditorDeleteChoicesInstances(1, totalContainer, index);
            }
        };

        incontext.ckeditorCreateInstances = function (index, fromSentence) {
            if (fromSentence) {
                var totalSentence = $('.sentence').length;
                for (var i = 1; i <= totalSentence; i++) {
                    /*if (typeof CKEDITOR.instances['sentence_' + i] == "object") {
                     CKEDITOR.instances['sentence_' + i].destroy();
                     }
                     CKEDITOR.replace('sentence_' + i);*/
                    // create instance of choices
                    var totalContainer = $('.choiceContainer_' + i).length;
                    incontext.ckeditorCreateChoicesInstances(totalContainer, i);
                }

            } else {
                var totalContainer = $('.choiceContainer_' + index).length;
                incontext.ckeditorCreateChoicesInstances(totalContainer, index);
            }
        };

        incontext.ckeditorDeleteChoicesInstances = function (start, totalContainer, index) {
            for (var i = start; i <= totalContainer; i++) {
                if (typeof CKEDITOR.instances['choices_' + index + '_' + i] == "object") {
                    CKEDITOR.instances['choices_' + index + '_' + i].destroy();
                    //CKEDITOR.remove('choices_' + i);
                }
                if (typeof CKEDITOR.instances['individual_' + index + '_' + i + '_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + index + '_' + i + '_feedback'].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + index + '_' + i + '_incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + index + '_' + i + '_incorrect_feedback'].destroy();
                }
            }
        }

        incontext.ckeditorCreateChoicesInstances = function (totalContainer, index) {
            for (var i = 1; i <= totalContainer; i++) {
                if (typeof CKEDITOR.instances['choices_' + index + '_' + i] == "object") {
                    CKEDITOR.instances['choices_' + index + '_' + i].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + index + '_' + i + '_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + index + '_' + i + '_feedback'].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + index + '_' + i + '_incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + index + '_' + i + '_incorrect_feedback'].destroy();
                }
                CKEDITOR.replace('choices_' + index + '_' + i);
                CKEDITOR.replace('individual_' + index + '_' + i + '_feedback');
                CKEDITOR.replace('individual_' + index + '_' + i + '_incorrect_feedback');
            }
        }

        // delete all instances
        for (name in CKEDITOR.instances)
        {
            CKEDITOR.instances[name].destroy(true);
        }

        // initialize ckeditor
        $('.ckeditor').each(function (e) {
            CKEDITOR.replace(this.id);
        });

        common_assessment_utility.ckeditorBindEvent();

        /*CKEDITOR.on("instanceReady", function (event) {
         $('.cke_button__insertimage_icon').css({
         'background-image': 'url(../../lib/ckeditor/plugins/customimage/icons/icons.png)',
         'background-position': '-17px -254px'
         });
         for (var prop in CKEDITOR.instances) {
         var oEditor = CKEDITOR.instances[prop];
         oEditor.on('mode', function () {
         if (this.mode == 'wysiwyg') {
         angular.element(document.getElementById('questionController')).scope().jsRegisterer(this.document.find('.ckImg').$);
         }
         });
         angular.element(document.getElementById('questionController')).scope().jsRegisterer(oEditor.document.find('.ckImg').$);
         }
         });*/