window.mcq = {};
mcq.generateChoice = function (index) {

    var choiceHtml = '';
    choiceHtml += '<div class="choiceContainer ui-state-default" id="choiceContainer_' + index + '">';
    choiceHtml += '<div class="well choice-well-section"><button type="button" class="btn chice-drag"><i class="fa fa-bars" aria-hidden="true"></i></button><button type="button" class="btn chice-delete" onclick="mcq.deleteChoice(' + index + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button><div id="ansChoice_' + index + '" class="">';
    choiceHtml += '<div class="ansChoiceQtn_inner">';
    choiceHtml += '<label for="ansChoice_' + index + '" class="label-name-list">Choice ' + index + '</label>';
    choiceHtml += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><input type="checkbox" id="assess_' + index + '_check" name="assess_' + index + '_check" class="assess_check"><input type="radio" id="assess_' + index + '_radio" name="assess" class="assess_radio"></span>';
    choiceHtml += '<textarea name="choices_' + index + '" id="choices_' + index + '" class="form-control requires text-wrap ckeditor" row="1"></textarea></div><label class="error_show" style="display:none">Choice should not be blank</label></div>';
    choiceHtml += '<div class="" id="show_option_' + index + '" style="display:none">';
    if ($('#cmn-toggle-1').prop('checked')) {
        choiceHtml += '<div class="form-group row feedback_area" style="display:none">';
    } else {
        choiceHtml += '<div class="form-group row feedback_area">';
    }

    choiceHtml += '<div class="col-sm-6">';
    choiceHtml += '<label for="individual_' + index + '_feedback" class="label-name-list">Choice ' + index + ' Feedback</label>';
    choiceHtml += '<textarea name="individual_' + index + '_feedback" id="individual_' + index + '_feedback" class="form-control text-wrap ckeditor"  row="1"></textarea></div>';
    choiceHtml += '<div class="col-sm-6">';
    choiceHtml += '<label for="individual_' + index + '_incorrect_feedback" class="label-name-list">Choice ' + index + ' Incorrect Feedback</label>';
    choiceHtml += '<textarea name="individual_' + index + '_incorrect_feedback" id="individual_' + index + '_incorrect_feedback" class="form-control text-wrap ckeditor"  row="1"></textarea></div></div>';
    choiceHtml += '<div class="form-group"><label for="individual_' + index + '_score" class="label-name-list">Choice ' + index + ' Score</label>';
    choiceHtml += '<input id="individual_' + index + '_score"  name="individual_' + index + '_score" type="text" class="form-control individual_score" /></div></div>';
    choiceHtml += '<div class="clearfix"></div>';
    choiceHtml += '</div></div></div>';
    choiceHtml += '<div class="form-group choice-optionselect"><a class="" id="option_button_' + index + '" onclick="mcq.toggleOption(' + index + ')"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="display-inline advance-option">Advanced Options</span></a></div></div>';
    return choiceHtml;
};

mcq.generateHint = function (index) {
    var choiceHtml = '';
    choiceHtml += '<div class="hintContainer ui-state-default" id="hintContainer_' + index + '">';
    choiceHtml += '<div id="hintWrapper_' + index + '" class="hintChoice_' + index + ' hint-innerblock"><div class="hint_inner"><div class="input-group"><span class="input-group-addon"><button class="btn chice-drag" type="button"><i class="fa fa-bars" aria-hidden="true"></i></button></span>';
    choiceHtml += '<textarea  name="hint_' + index + '" id="hint_' + index + '" class="form-control ckeditor" rows="1"></textarea>';
    choiceHtml += '<span class="input-group-addon"><button class="btn chice-delete" type="button" onclick="mcq.deleteHint(' + index + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span></div></div></div></div>';


    return choiceHtml;
};

mcq.addChoice = function () {
    var total_choice = $('.choiceContainer').length;
    var index = total_choice + 1;
    var choiceHtml = mcq.generateChoice(index);
    $('#choice_sortable').append(choiceHtml);
    // change choice type based on cmn-toggle-1
    if ($('#cmn-toggle-1').prop('checked')) {
        $('#assess_' + index + '_check').show(); //assess_1_check assess_1_radio
        $('#assess_' + index + '_radio').hide();

    } else {
        $('#assess_' + index + '_check').hide();
        $('#assess_' + index + '_radio').show();
    }

    $(".requires").on('keyup', function () {
        if ($(this).val().length > 0) {
            $(this).parent().removeClass('has-error');
        } else {
            $(this).parent().addClass('has-error');
        }
    });

    if ($('.choiceContainer').length > 1) {
        $('.chice-delete').removeAttr('disabled');
        $('.switch').removeAttr('style');
    }

    $(".individual_score").keydown(function (e) {
        mcq.checkNumber(e);
    });
    common_assessment_utility.initializeCkeditor();
    common_assessment_utility.ckeditorBindEvent();
};

mcq.addHint = function () {
    var total_hint = $('.hintContainer').length;
    var index = total_hint + 1;
    var choiceHtml = mcq.generateHint(index);
    $('#hint_sortable').append(choiceHtml);
};

mcq.checkNumber = function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }


//
//$(document).ready(function () {
        var maxLength = 255;
        $(".requires").on('keyup', function () {
            if ($(this).val().length > 0) {
                if ($(this).attr('id') === 'question_title') {
                    var length = $(this).val().length;
                    $(this).parent().removeClass('has-error');
                    var length = maxLength - length;
                    $('#chars').text(length);
                } else {
                    $(this).parent().removeClass('has-error');
                }
            } else {
                if ($(this).attr('id') === 'question_title') {
                    $('#chars').text(255);
                }
                $(this).parent().addClass('has-error');
            }
        });

        $('#add_choice').off().on('click', function (e) {
            mcq.addChoice();
            $('.ckeditor').each(function (e) {
                if (!CKEDITOR.instances[this.id]) {
                    CKEDITOR.replace(this.id);
                }
            })
        });

        $('#add_hint').off().on('click', function (e) {
            mcq.addHint();
        });

        $('.assess').off().on('change', function (e) {
            if (!$('#cmn-toggle-1').prop('checked')) {
                $(".assess").prop('checked', false);
            }
            $(this).prop('checked', true);
        });

        $('#generate_data').off().on('click', function (e) {
            var res = mcq.generateQuestionData();
            res = JSON.stringify(res);
        });

        $('#cmn-toggle-1').off().on('change', function (e) {
            mcq.toggleChoice();
        });

        $('.assess_check').hide();
        $('.singel-ans').addClass('active');
        $('.multiple-ans').addClass('inactive');

        $("#choice_sortable").sortable({
            handle: ".chice-drag",
            cancel: '',
            start: function (event, ui) {
                ui.item.data('originIndex', ui.item.index());
            },
            stop: function (event, ui) {
                mcq.rearrangeChoice(event, ui);
                // delete ckeditor instances of all choices
                var total_container = $('.choiceContainer').length;
                mcq.ckeditorDeleteInstances(1, total_container);
                // create ckeditor new instances for all choices
                mcq.ckeditorCreateInstances();
                common_assessment_utility.ckeditorBindEvent();
            },
        });

        $(".individual_score").keydown(function (e) {
            mcq.checkNumber(e);
        });

        /*$("#hint_sortable").sortable({
         handle: ".chice-drag",
         cancel: '',
         });*/

//});

        mcq.toggleOption = function (id) {
            $('#show_option_' + id).slideToggle();
        };

        mcq.toggleChoice = function () {
            $('.assess_radio').prop('checked', false);
            $('.assess_check').prop('checked', false);
            if ($('#cmn-toggle-1').prop('checked')) {
                $('.singel-ans').removeClass('active');
                $('.singel-ans').addClass('inactive');
                $('.multiple-ans').removeClass('inactive');
                $('.multiple-ans').addClass('active');
                $('.assess_radio').hide();
                $('.assess_check').show();
                $('.feedback_area').hide();
            } else
            {
                $('.singel-ans').removeClass('inactive');
                $('.singel-ans').addClass('active');
                $('.multiple-ans').removeClass('active');
                $('.multiple-ans').addClass('inactive');
                $('.assess_check').hide();
                $('.assess_radio').show();
                $('.feedback_area').show();
            }
        };


        mcq.generateQuestionData = function () {
            // get question details
            //var question_stem = mcq.replaceNewline($('#question_stem').val());
            //var question_stem = CKEDITOR.instances['question_stem'].getData();
            var question_stem = common_assessment_utility.getFieldValue('question_stem', true);
            var question_title = $('#question_title').val();
            //var instruction_text = mcq.replaceNewline($('#instruction_text').val());
            var instruction_text = common_assessment_utility.getFieldValue('instruction_text', true);
            var advJSONData = {};
            var question_type = ($('#cmn-toggle-1').prop('checked')) ? 'mcms' : 'mcss';
            advJSONData['question_type'] = {text: question_type};
            advJSONData['question_interaction'] = {text: 'clickable'};
            advJSONData['question_layout'] = {text: 'vertical'};
            advJSONData['question_stem'] = {text: question_stem};
            advJSONData['question_title'] = {text: $('#question_title').val()};
            advJSONData['instruction_text'] = {text: instruction_text};
            // get choices
            var choices = [];
            var total_choice = $('.choiceContainer').length;
            var assess_value = false;
            var correct_answer = []
            for (var i = 1; i <= total_choice; i++) {
                //if ($('#choices_' + i).val() != null) {
                var mcq_choice_value = common_assessment_utility.getFieldValue('choices_' + i, true);
                if (mcq_choice_value != null) {
                    assess_value = false;
                    if ($('#cmn-toggle-1').prop('checked')) {
                        if ($('#assess_' + i + '_check').prop('checked')) {
                            assess_value = true;
                            correct_answer.push(i);
                        }
                    } else {
                        if ($('#assess_' + i + '_radio').prop('checked')) {
                            assess_value = true;
                            correct_answer.push(i);
                        }
                    }
                    // push choice inside a choices data
                    //choices.push({choiceid: i, assess: assess_value, text: mcq.replaceNewline($('#choices_' + i).val()), correct_feedback: mcq.replaceNewline($('#individual_' + i + '_feedback').val()), incorrect_feedback: mcq.replaceNewline($('#individual_' + i + '_incorrect_feedback').val()), score: $('#individual_' + i + '_score').val(), pinanswer: 0});

                    choices.push({choiceid: i, assess: assess_value, text: common_assessment_utility.getFieldValue('choices_' + i, true), correct_feedback: common_assessment_utility.getFieldValue('individual_' + i + '_feedback', true), incorrect_feedback: common_assessment_utility.getFieldValue('individual_' + i + '_incorrect_feedback', true), score: $('#individual_' + i + '_score').val(), pinanswer: 0});
                }

            }
            advJSONData['correct_answer'] = correct_answer.join(',');
            advJSONData['choices'] = choices;
            //advJSONData['global_correct_feedback'] = {text: mcq.replaceNewline($('#global_correct_feedback').val())};
            //advJSONData['global_incorrect_feedback'] = {text: mcq.replaceNewline($('#global_incorrect_feedback').val())};
            advJSONData['global_correct_feedback'] = {text: common_assessment_utility.getFieldValue('global_correct_feedback', true)};
            advJSONData['global_incorrect_feedback'] = {text: common_assessment_utility.getFieldValue('global_incorrect_feedback', true)};
            var hints = [];
            var total_hint = $('.hintContainer').length;
            for (var i = 1; i <= total_hint; i++) {
                hints.push({'hintid': i, text: $('#hint_' + i).val()});
            }
            advJSONData['hints'] = hints;
            advJSONData['settings'] = {score: $('#settings').val()};
            if ($("input[name='media']:checked").val() === 'no-media') {
                advJSONData['media'] = {type: "no-media"};
            } else {
                if ($('#ext_res_title').val() == '') {
                    advJSONData['media'] = {type: "no-media"};
                } else {
                    if ($('#media_type').val() === 'brightcove') {
                        advJSONData['media'] = {type: $('#media_type').val(), brightcove_id: $('#ext_res_title_id').val(), url: $('#ext_res_url').val(), height: $('#ext_res_height').val(), width: $('#ext_res_width').val()};
                    } else {
                        advJSONData['media'] = {type: $('#media_type').val(), url: $('#ext_res_url').val(), title: $('#ext_res_title').val(), asset_type: $('#ext_res_type').val()};
                    }
                }

            }

            return advJSONData;
        };


        mcq.renderQuestionData = function (question_data) {
            var question_data_obj = question_data;
            if (typeof question_data !== "object") {
                question_data_obj = JSON.parse(question_data);
            }
            //alert('Hei');
            //alert(question_data_obj);
            //console.log(question_data_obj.question_stem.text);


            if (question_data_obj.question_type.text == 'mcms') {
                $('#cmn-toggle-1').prop('checked', true);
                mcq.toggleChoice();
            }

            // render question details
            //$('#question_stem').val(mcq.replaceBr(question_data_obj.question_stem.text));            
            common_assessment_utility.setFieldValue('question_stem', question_data_obj.question_stem.text, true);
            $('#question_title').val(question_data_obj.question_title.text);
            var remaining_length = 255 - question_data_obj.question_title.text.length;
            $('#chars').text(remaining_length);
            //$('#instruction_text').val(mcq.replaceBr(question_data_obj.instruction_text.text));
            common_assessment_utility.setFieldValue('instruction_text', question_data_obj.instruction_text.text, true);
            // render choices
            var total_choice = question_data_obj.choices.length;
            var choice_type = '_radio';

            if (total_choice < 2) {
                $('#choiceContainer_2').remove();
                $('#choiceContainer_3').remove();
                // delete instances
                mcq.ckeditorDeleteInstances(2, 3);

            } else if (total_choice < 3) {
                $('#choiceContainer_3').remove();
                //delete instance
                mcq.ckeditorDeleteInstances(3, 3);
            }

            for (var i = 0; i < total_choice; i++) {
                if (i > 2) { // generate html after third choice
                    mcq.addChoice();
                }
                if (question_data_obj.question_type.text == 'mcms') {
                    choice_type = '_check';
                }
                if (JSON.parse(question_data_obj.choices[i].assess)) { // convert into boolean
                    $('#assess_' + question_data_obj.choices[i].choiceid + choice_type).prop('checked', true);
                }
                //$('#choices_' + question_data_obj.choices[i].choiceid).val(mcq.replaceBr(question_data_obj.choices[i].text))
                //$('#individual_' + question_data_obj.choices[i].choiceid + '_feedback').val(mcq.replaceBr(question_data_obj.choices[i].correct_feedback));
                //$('#individual_' + question_data_obj.choices[i].choiceid + '_incorrect_feedback').val(mcq.replaceBr(question_data_obj.choices[i].incorrect_feedback));
                common_assessment_utility.setFieldValue('choices_' + question_data_obj.choices[i].choiceid, question_data_obj.choices[i].text, true);
                common_assessment_utility.setFieldValue('individual_' + question_data_obj.choices[i].choiceid + '_feedback', question_data_obj.choices[i].correct_feedback, true);
                common_assessment_utility.setFieldValue('individual_' + question_data_obj.choices[i].choiceid + '_incorrect_feedback', question_data_obj.choices[i].incorrect_feedback, true);
                $('#individual_' + question_data_obj.choices[i].choiceid + '_score').val(question_data_obj.choices[i].score);
            }
            // render feedback
            //$('#global_correct_feedback').val(mcq.replaceBr(question_data_obj.global_correct_feedback.text));
            //$('#global_incorrect_feedback').val(mcq.replaceBr(question_data_obj.global_incorrect_feedback.text));
            common_assessment_utility.setFieldValue('global_correct_feedback', question_data_obj.global_correct_feedback.text, true);
            common_assessment_utility.setFieldValue('global_incorrect_feedback', question_data_obj.global_incorrect_feedback.text, true);
            // render hints
            /*var total_hint = question_data_obj.hints.length;
             for (var i = 0; i < total_hint; i++) {
             if (i > 0) { // generate html after 1 hint
             mcq.addHint();
             }
             //$('#hint_' + question_data_obj.hints[i].hintid).val(question_data_obj.hints[i].text);
             common_assessment_utility.setFieldValue('hint_' + question_data_obj.hints[i].hintid, question_data_obj.hints[i].text, true);
             }*/

            // render settings
            $('#settings').val(question_data_obj.settings.score);

            if ($('.choiceContainer').length == 1) {
                $('.chice-delete').attr('disabled', 'disabled');
                $('.switch').attr('style', 'pointer-events:none;opacity: 0.5;')
            }
        };

        mcq.deleteChoice = function (choice_index) {
            // if choices is more than one
            var total_container = $('.choiceContainer').length;
            if ($('.choiceContainer').length > 1) {
                $('#choiceContainer_' + choice_index).remove();

                if ($('.choiceContainer').length == 1) {
                    $('.chice-delete').attr('disabled', 'disabled');
                    $('.switch').attr('style', 'pointer-events:none;opacity: 0.5;')
                }

                $('.choiceContainer').each(function (index, value) {
                    var oldIndex = $(this).attr('id').split('_')[1];
                    var newIndex = index + 1;
                    if (oldIndex != newIndex) {
                        mcq.modifyChoiceHtml(newIndex, oldIndex, this);
                    }
                });
                mcq.ckeditorDeleteInstances(1, total_container);
                mcq.ckeditorCreateInstances();
                common_assessment_utility.ckeditorBindEvent();

            }
        };

        mcq.deleteHint = function (hint_index) {
            if ($('.hintContainer').length > 1) {
                $('#hintContainer_' + hint_index).remove();
                var total_choice = $('.hintContainer').length;
                for (i = 1; i <= total_choice + 1; i++) {
                    if ($('#hintContainer_' + i).html()) {
                        if (i > hint_index) {
                            var newId = 'hint_' + (i - 1);
                            newId = newId.toString();
                            $('#hint_' + i).attr('id', newId);
                        }

                    } else {
                        console.log('container not found');
                    }
                }
            }

        }




        $(".requires-modal").on('keyup', function () {
            if ($(this).val().length > 0) {
                $(this).parent().removeClass('has-error');
            } else {
                $(this).parent().addClass('has-error');
            }
        });

        mcq.rearrangeChoice = function (event, ui) {
            // change all choices html if needed
            $('.choiceContainer').each(function (index, value) {
                var oldIndex = $(this).attr('id').split('_')[1];
                var newIndex = index + 1;
                if (oldIndex != newIndex) {
                    mcq.modifyChoiceHtml(newIndex, oldIndex, this);
                }
            });
        };

        mcq.modifyChoiceHtml = function (newIndex, oldIndex, curObj) {

            var newId = 'choiceContainer_' + newIndex;
            var choiceId = 'choices_' + newIndex;
            var checkId = 'assess_' + newIndex + '_check';
            var radioId = 'assess_' + newIndex + '_radio';
            var feedabackId = 'individual_' + newIndex + '_feedback';
            var incorrectfeedabackId = 'individual_' + newIndex + '_incorrect_feedback';
            var individualScoreId = 'individual_' + newIndex + '_score';
            var option_button_id = 'option_button_' + newIndex;
            radioId = radioId.toString();
            checkId = checkId.toString();
            newId = newId.toString();
            feedabackId = feedabackId.toString();
            incorrectfeedabackId = incorrectfeedabackId.toString();
            individualScoreId = individualScoreId.toString();
            choiceId = choiceId.toString();
            option_button_id = option_button_id.toString();
            // set new container id
            $(curObj).attr('id', newId);
            $(curObj).find("#ansChoice_" + oldIndex).attr('id', 'ansChoice_' + newIndex);
            $(curObj).find("label[for='ansChoice_" + oldIndex + "']").attr('for', 'ansChoice_' + newIndex);
            $(curObj).find("label[for='ansChoice_" + newIndex + "']").html('Choice ' + newIndex);
            $(curObj).find('.assess_check').attr('id', checkId);
            $(curObj).find('.assess_radio').attr('id', radioId);
            $(curObj).find('#choices_' + oldIndex).attr({id: choiceId, name: choiceId});

            $(curObj).find('#individual_' + oldIndex + '_feedback').attr({id: feedabackId, name: feedabackId});
            $(curObj).find('#individual_' + oldIndex + '_incorrect_feedback').attr({id: incorrectfeedabackId, name: incorrectfeedabackId});
            $(curObj).find('#individual_' + oldIndex + '_score').attr({id: individualScoreId, name: individualScoreId});

            $(curObj).find('.chice-delete').removeAttr('onclick');
            $(curObj).find('.chice-delete').attr('onclick', 'mcq.deleteChoice(' + newIndex + ')');

            $(curObj).find('#option_button_' + oldIndex).attr('id', option_button_id);
            $(curObj).find('#show_option_' + oldIndex).attr('id', 'show_option_' + newIndex);

            $(curObj).find('#' + option_button_id).removeAttr('onclick');
            $(curObj).find('#' + option_button_id).attr('onclick', 'mcq.toggleOption(' + newIndex + ')');

            $(curObj).find("label[for='individual_" + oldIndex + "_feedback']").attr('for', 'individual_' + newIndex + '_feedback');
            $(curObj).find("label[for='individual_" + oldIndex + "_incorrect_feedback']").attr('for', 'individual_' + newIndex + '_incorrect_feedback');
            $(curObj).find("label[for='individual_" + oldIndex + "_score']").attr('for', 'individual_' + newIndex + '_score');
            $(curObj).find("label[for='individual_" + newIndex + "_feedback']").html('Choice ' + newIndex + ' Feedback');
            $(curObj).find("label[for='individual_" + newIndex + "_incorrect_feedback']").html('Choice ' + newIndex + ' Incorrect Feedback');
            $(curObj).find("label[for='individual_" + newIndex + "_score']").html('Choice ' + newIndex + ' Score');
        };

        //$("textarea").keyup(function () {
        //    var data = $(this).val().replace(/[\r\n]/g, "<br/>");
        //    $(this).val(data);
        //});

        mcq.replaceNewline = function (string) {
            var data = string.replace(/(\r\n|\n|\r)/gm, "<br/>");
            return data;
        };

        mcq.replaceBr = function (string) {
            var regex = /<br\s*[\/]?>/gi;
            var data = string.replace(regex, "\n");
            return data;
        };

        mcq.ckeditorDeleteInstances = function (start, total_container) {
            // delete all the choices container
            for (var i = start; i <= total_container; i++) {
                if (typeof CKEDITOR.instances['choices_' + i] == "object") {
                    CKEDITOR.instances['choices_' + i].destroy();
                    //CKEDITOR.remove('choices_' + i);
                }

                if (typeof CKEDITOR.instances['individual_' + i + '_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_feedback'].destroy();
                }

                if (typeof CKEDITOR.instances['individual_' + i + '_incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_incorrect_feedback'].destroy();
                }

            }
        };

        mcq.ckeditorCreateInstances = function () {
            var total_choice = $('.choiceContainer').length;
            for (var i = 1; i <= total_choice; i++) {
                if (typeof CKEDITOR.instances['choices_' + i] == "object") {
                    CKEDITOR.instances['choices_' + i].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + i + '_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_feedback'].destroy();
                }
                if (typeof CKEDITOR.instances['individual_' + i + '_incorrect_feedback'] == "object") {
                    CKEDITOR.instances['individual_' + i + '_incorrect_feedback'].destroy();
                }
                CKEDITOR.replace('choices_' + i);
                CKEDITOR.replace('individual_' + i + '_feedback');
                CKEDITOR.replace('individual_' + i + '_incorrect_feedback');
            }
        };

        // delete all instances
        for (name in CKEDITOR.instances)
        {
            CKEDITOR.instances[name].destroy(true);
        }

        // initialize ckeditor
        $('.ckeditor').each(function (e) {
            CKEDITOR.replace(this.id);
        });

        common_assessment_utility.ckeditorBindEvent();

        /* CKEDITOR.on("instanceReady", function (event) {
         $('.cke_button__insertimage_icon').css({
         'background-image': 'url(../../lib/ckeditor/plugins/customimage/icons/icons.png)',
         'background-position': '-17px -254px'
         });
         for (var prop in CKEDITOR.instances) {
         var oEditor = CKEDITOR.instances[prop];
         oEditor.on('mode', function () {
         if (this.mode == 'wysiwyg') {
         angular.element(document.getElementById('questionController')).scope().jsRegisterer(this.document.find('.ckImg').$);
         }
         });
         angular.element(document.getElementById('questionController')).scope().jsRegisterer(oEditor.document.find('.ckImg').$);
         }
         });*/

