CKEDITOR.plugins.add('customimage', {
    icons: 'customimage',
    init: function(editor) {
        editor.addCommand('insertImg', {
            exec: function() {
                htmlEditor.getMediaImages();
            }
        });
        editor.ui.addButton('insertImage', {
            label: 'Image',
            command: 'insertImg',
            toolbar: 'insert'
        });
    }
});