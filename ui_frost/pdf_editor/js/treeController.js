function toggleToc() {
    /*jQuery('.toc-btn').on('click', function () {
     //alert(1)
     })*/
}

function tocInit() {
    var rootEl = jQuery('.nested_with_switch');

    rootEl.empty();

    function createExpandCollapseTree(dom) {
        rootEl.html(dom);
        bindExpandCollapseTree();
        refreshDragDrop();
    }

    function bindExpandCollapseTree() {
        rootEl.find('li').on('click', function (e) {
            e.stopPropagation();
        });

        rootEl.find('.expandToc').off('click').on('click', function (e) {
            var parElm = jQuery(this).parent();
            parElm.find('li').each(function () {
                jQuery(this).toggle();
            });
            if (jQuery(this).children('.fa').hasClass('fa-caret-right')) {
                jQuery(this).children('.fa').removeClass('fa-caret-right').addClass('fa-caret-down');
            } else {
                jQuery(this).children('.fa').removeClass('fa-caret-down').addClass('fa-caret-right');
            }
        });

        rootEl.find('.contPreview').off('click').on('click', function (e) {
            var dataId = jQuery(this).parent().data('id'),
                    dataTitle = jQuery(this).parent().data('title'),
                    dataType = jQuery(this).parent().data('type');
            if (dataType == 7) {
                swal({
                    title: "Warning!",
                    text: "Assessment node is not editable",
                    type: "warning",
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                var parentTocCollapse = jQuery("#tocBtn").parent();
                                setTimeout(function () {
                                    parentTocCollapse.addClass("open");
                                },50);
                            }
                        });
                return;
            }
            else {
                var pathToGo = "index.html?access_token=" + access_token + "&project_id=" + project_id + "&node_id=" + dataId + "&node_title=" + dataTitle + "&node_type_name=&node_type_id=" + dataType + "&project_type_id=" + project_type_id + "&section_id= ";
            }

            //Content change tracker

            var lastContent = htmlEditor.getLatestSavedContent();
            if (lastContent !== htmlEditor.currContent) {
                swal({
                    title: "You are about to leave the page",
                    text: " Want to save the content?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                    allowOutsideClick: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        htmlEditor.saveContent();
                        setTimeout(function () {
                            window.location.href = pathToGo;
                        }, 1000);

                    } else {
                        window.location.href = pathToGo;
                    }
                });
            } else {
                window.location.href = pathToGo;
            }

        });
    }

    var getTOCResponse = (function () {
        createExpandCollapseTree(htmlEditor.editorTOC);
    })();

    function findItem(array, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].title.split(',').indexOf(value) >= 0) {
                return objGroup[i];
            }
        }
    }

    function refreshDragDrop() {
        setTimeout(function () {
            try {
                jQuery("ol.nested_with_switch").sortable("refresh");
            }
            catch (exc) {
                //console.log('Window says: '+ exc);
            }

        }, 500);
    }

    /*var createDragDrop = function(){
     var oldContainer;
     var group = jQuery("ol.nested_with_switch").sortable({
     group: 'serialization',
     afterMove: function (placeholder, container) {
     if(oldContainer != container){
     if(oldContainer)
     oldContainer.el.removeClass("active");
     container.el.addClass("active");
     
     oldContainer = container;
     }
     },
     onDrop: function ($item, container, _super) {
     container.el.removeClass("active");
     _super($item, container);
     getJSON()
     refreshDragDrop();
     }
     });
     
     function getJSON(){
     var data = group.sortable("serialize").get();
     var jsonString = JSON.stringify(data, null, ' ');
     jsonString = jsonString.substring(1, jsonString.length-1);
     jsonString = '{"children":' + jsonString + '}';
     //console.log(jsonString)
     }
     getJSON()
     }();*/
}