htmlEditor.editorEvents = function() {
    htmlEditor.populateMeta();
    htmlEditor.setUserInputMetaClass();

    //    remove the highlighclasses and unwanted elements on pressing escape button.
    jQuery(".cke_wysiwyg_frame").contents().find("body").on("keydown mousedown", function(e) {
        if (e.type === "keydown") {
            if (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 65 && e.keyCode <= 90 || e.keyCode >= 96 && e.keyCode <= 111 || e.keyCode == 32 || e.keyCode == 8 || e.keyCode >= 186) {
                htmlEditor.contentUpdated = true;
            }
            if (e.which === 27) {
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
                htmlEditor.removeFrameSettings();
            }
            else if (e.keyCode === 13) {
                return;
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());    //Remove unwanteds
                var content = jQuery(this).find("*");                //get the content
                htmlEditor.generateIds(content);                 //generate ids for all elements.
                
                try {
                    var thisElm = jQuery(htmlEditor.getSelectedNode()).parent();
                    var isOlUl = thisElm[0].tagName === 'UL' || jQuery(htmlEditor.getSelectedNode()).parent()[0].tagName === 'OL' ? true : false;
                    if (isOlUl) {
                        jQuery(thisElm).find('li').each(function() {
                            var thisElm = jQuery(this);
                            var hasPara = thisElm.find('p').length > 0 ? true : false;
                            if (!hasPara) {
                                var thisText = thisElm.text();
                                var addPara = '<p>' + thisText + '</p>';
                                thisElm.html(addPara);
                            }
                        });
                        htmlEditor.populateMeta();
                    }
                    htmlEditor.contentUpdated = true;
                }
                catch (exc) {
                }
            }
            htmlEditor.removeFrameSettings();
        }
        else if (e.type == "mousedown") {
            if (e.which === 1) {
                if (jQuery(e.target).parents(".converted").length) {
                }
            }
        }
        htmlEditor.checkUserAct(e);
    });

    //          ==========Escape parent on double pressing the enter key============
    var returnCount = 0;
    jQuery(".cke_wysiwyg_frame").contents().find("body").off("keyup").on("keyup", function(e) {
        if (e.keyCode == 13) {
            returnCount++;
            setTimeout(function() {
                if (returnCount >= 2) {
                    escaper();
                }
                returnCount = 0;
            }, 300)
        }
    });
    //          ====================================================================

    htmlEditor.bindProfileEvents();
    jQuery('.cke_button_icon').on('click', function() {
        var enableCheck = jQuery(this).hasClass('cke_button__link_icon') || jQuery(this).hasClass('cke_button__insertimage_icon') ? false : true;
        if (enableCheck)
            htmlEditor.enableSave();
    });
};

htmlEditor.enableSave = function() {
    setTimeout(function() {
        var lastContent = htmlEditor.getLatestSavedContent();
        if (lastContent !== htmlEditor.currContent || htmlEditor.mathMlEdited){
            jQuery('#save_modal').removeAttr('disabled');
        }
        else{
            if(arguments[0]==true){//Forcefully enable save
                jQuery('#save_modal').removeAttr('disabled');
            }
            else{
                htmlEditor.disableSave();
            }
        }
            
    }, 1000);
}

htmlEditor.disableSave = function() {
    jQuery('#save_modal').attr('disabled', 'disabled');
}

/****************************************************/



//Function that sets user added classes for elements
htmlEditor.setUserInputMetaClass = function() {
    var oUserInputMetaList = {};
    jQuery.when(oUserInputMetaList = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.patternClasses)).done(function() {
        oUserInputMetaList = JSON.parse(oUserInputMetaList.responseText).data.pattern_classes;

        for (var key in oUserInputMetaList) {
            var keyUpper = key.toUpperCase();
            if (classSet[keyUpper] !== 'undefined') {
                try {
                    for (var i = 0; i < oUserInputMetaList[key].length; i++) {
                        classSet[keyUpper].push(oUserInputMetaList[key][i]);
                    }
                }
                catch (exc) {
                }
            }
        }
    });
}


//Function to populate metaclasses and highligh selection inside the editor.
htmlEditor.populateMeta = function() {
    htmlEditor.metaArr = {};
    htmlEditor.topLevelPatternClass = new Array();
    jQuery(editor_patterns).each(function(i, v) {
        if(v.meta_class_options != undefined)
        {
            var metaData = JSON.parse(v.meta_class_options);
        }
        jQuery(metaData).each(function(i1, v1) {
            var key = v1.default_class;
            htmlEditor.topLevelPatternClass.push(v1.default_class);
            htmlEditor.metaArr[key] = new Array();
            jQuery(v1.pattern_classes).each(function(i2, v2) {
                var val = v2.value;
                if (val.length > 0) {
                    htmlEditor.metaArr[key].push(val);
                    htmlEditor.topLevelPatternClass.push(val);
                }
            });
        });
    });
    htmlEditor.topLevelPatternClass.push('converted gadget');
    htmlEditor.getCurrentPattern();
};


htmlEditor.disableToolbar = function() {
    /*jQuery('a.cke_button').each(function(){
     var thisElm = jQuery(this);
     thisElm.attr("onclick","");
     thisElm.addClass('cke_button_disabled');
     });*/
};

htmlEditor.enableToolbar = function() {
    /*jQuery('a.cke_button').each(function(indx){
     var thisElm = jQuery(this);
     thisElm.attr("onclick",htmlEditor.aOnclickValueArr[indx]);
     thisElm.removeClass('cke_button_disabled');
     });*/
};

htmlEditor.showImageOptions = function(image) {
    CKEDITOR.instances.editor1.getSelection().removeAllRanges();
    var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    var frame = jQuery(image);
    var ht = frame.outerHeight();                                    // height of the image selected.
    var wide = frame.outerWidth();                                   // width of the image selected.
    var offset = frame.offset();                                // Offset of the image selected.
    htmlEditor.removeUnwanteds(the_editor);
    var type = frame.attr("type");

    var dataPos = frame.attr("data-pos");
    var overlay = '<div class="image-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del-image" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set-image" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT IMAGE</div></div>';
    console.log(the_editor);
    the_editor.append(overlay);

    //The delete functionality of the image
    the_editor.find(".to-del-image:visible").off().on("click", function() {
        var selElmId = frame.parents("[id]").attr("id");
        frame.remove();
        if (jQuery("#sourceVal:visible").length) {
            splitview.updateCode(selElmId);         //Remove elem code from split source.
        }
//        jQuery("iframe.cke_wysiwyg_frame").contents().find("body").trigger("keyup");
        htmlEditor.removeUnwanteds(the_editor);
        htmlEditor.enableSave();
    });

    the_editor.find(".to-set-image").off().on("click", function() {
        htmlEditor.getMediaImages(image);
    })
}

htmlEditor.getCurrentPattern = function() {
    var metaArr = htmlEditor.metaArr;
    var pattClsLst = htmlEditor.topLevelPatternClass

    var tagListToHighlight = ['SECTION', 'ASIDE', 'OL', 'UL', 'BLOCKQUOTE'];
    var tagListToHide = ['HEADER', 'P', 'SPAN', 'STRONG', 'EM', 'I', 'U', 'IMG', 'TD', 'TBODY', 'TR', 'TABLE'];
    jQuery('.cke_wysiwyg_frame').contents().find("body").off('click').on('click', "*", function(e) {
        /*if (jQuery('.cke_editable').find('.chapter').length === 0)
         return;*/
        if (e.target.tagName === "IMG" && !e.target.classList.contains('cke_reset')) {
            htmlEditor.showImageOptions(e.target)
            return false;
        }
        jQuery('.cke_button__link').removeClass('cke_button_on');
        function settingOverlay(this_frm) {                     // Function to show setting options for videos & gadget pattern.
            e.stopPropagation();
            var this_ifrm = this_frm;
            htmlEditor.showSettingForFrame(this_ifrm);
        }

        if (jQuery(this).parents(".converted").length || jQuery(this).hasClass("converted")) {
            var converted_elem = jQuery(this).hasClass("converted") ? jQuery(this) : jQuery(this).parents(".converted");
            settingOverlay(converted_elem);

        }
        else {
            var thisElm = jQuery(this);
            var tagName = thisElm[0].tagName;
            if (tagName === 'A')
            {
                jQuery('.cke_button__link').addClass('cke_button_on');
            }
            else {
                //jQuery('.cConfig').remove();
                //thisElm.append('<button class="cConfig" onClick="htmlEditor.classConfig(this)">+</button>')
                //thisElm.append('<button class="cConfig" onClick="htmlEditor.classConfig(this)">+</button>')
            }


            var tagElmToHighlight = '';
            function classNotDefinedChecker(this_elm) {
                var clsName = this_elm.attr('class');
                var tagName = this_elm[0].tagName;
                try {
                    if (tagName === 'HEADER') {
                        return false;
                    }
                    else if (jQuery.inArray(tagName, tagListToHighlight) !== -1) {
                        return true;
                    }
                    else {
                        var allClasses = clsName.split(' ');
                        for (var i = 0; i < allClasses.length; i++) {
                            if (jQuery.inArray(allClasses[i], pattClsLst) !== -1) {
                                return true;
                            }
                            break;
                        }
                    }
                }
                catch (exc) {
                    if (jQuery.inArray(tagName, tagListToHighlight) !== -1) {
                        tagElmToHighlight = this_elm;
                    }
                    return jQuery.inArray(clsName, pattClsLst) !== -1 && this_elm[0].tagName !== 'HEADER' ? true : false;
                }
            }

            function isTrueClass(arr) {
                for (var i = 0; i < arr.length; i++) {
                    var key = arr[i];
                    if (metaArr[key] !== undefined)
                        return metaArr[key]
                    else
                        return false;
                }
            }
            try {
                var this_class = false;
                this_class = classNotDefinedChecker(thisElm);
                while (!this_class) {
                    thisElm = thisElm.parent();
                    if (thisElm[0].tagName === 'BODY')
                        break;
                    this_class = classNotDefinedChecker(thisElm);
                }
                if (this_class) {
                    var classArr = thisElm.attr('class').split(' ');
                    this_class = isTrueClass(classArr);
                }

            }
            catch (exc) {
            }




            function setSettings(elm) {
                var tagName = elm[0].tagName;
                //
                /*if(jQuery.inArray(tagName, tagListToHide) !== -1){
                 (elm.parent()).trigger('click');
                 }
                 else{*/
                jQuery('.highlight_on_drop').removeClass('highlight_on_drop');
                var className = (elm[0].className).split('highlightSelected')[0];
                jQuery('.meta-dropdown').removeClass('open');
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
                elm.addClass('highlightSelected');
                if (elm.hasClass("row")) {
                    elm.append("<span class='descOfTarget' contenteditable='false'>" + tagName + " " + className + "</span><div class='toolBox' contenteditable='false'><span class='glyphicon glyphicon-trash tool' title='Delete'></span><span class='glyphicon glyphicon-cog tool' title='Settings'></span></div>");
                }
                else {
                    elm.append("<span class='descOfTarget' contenteditable='false'>" + tagName + " " + className + "</span><div class='toolBox' contenteditable='false'><span class='glyphicon glyphicon-trash tool' title='Delete'></span></div>");
                }
                actionOnSmartClick();
                //}

            }
            function actionOnSmartClick() {
//                jQuery(".cke_wysiwyg_frame").contents().find("body").find('.glyphicon-trash').off('click').on('click', function() {
//                    var oHighlightedElm = jQuery(".cke_wysiwyg_frame").contents().find("body").find('.highlightSelected');
//                    oHighlightedElm.deleteTOCNode();
//                    /*if(oHighlightedElm.hasClass("chapter")){
//                     jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
//                     htmlEditor.bindIframeDragDrop();
//                     htmlEditor.disableToolbar();
//                     }*/
//                    oHighlightedElm.remove();
//                    htmlEditor.removeFrameSettings();
//                    htmlEditor.bindIframeDragDrop();
//                });

                jQuery(".cke_wysiwyg_frame").contents().find("body").find(".tool").off("click").on("click", function() {
                    var oHighlightedElm = jQuery(".cke_wysiwyg_frame").contents().find("body").find('.highlightSelected');
                    var parId = oHighlightedElm.parents("[id]").attr("id");
                    var gridDetails = {};
                    gridDetails.classList = [];
                    gridDetails.elements = [];
                    if (jQuery(this).hasClass("glyphicon-trash")) {
                        oHighlightedElm.deleteTOCNode();
                        /*if(oHighlightedElm.hasClass("chapter")){
                         jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                         htmlEditor.bindIframeDragDrop();
                         htmlEditor.disableToolbar();
                         }*/
                        oHighlightedElm.remove();
                        if (jQuery("#sourceVal:visible").length) {
                            splitview.updateCode(parId);            //remove the code from the split source
                        }
                        htmlEditor.removeFrameSettings();
                        htmlEditor.enableSave();
                        
                    }
                    else {
                        populateboxes(oHighlightedElm);             //populate initial values
                        jQuery("#setRowsModal").find(".num-of-grid").on("change", function() {
                            generateBoxForGrids();                  //create textboxes for input
                        });
                        jQuery("#setRowsModal").find(".setting_conf").off("click").on("click", function() {
                            setClasses(oHighlightedElm);            //set the new classes.
                        });
                    }

//                    function to populate the details in the popup
                    function populateboxes(elem) {
                        var row = elem;
                        var toSearch = "col-";
                        gridDetails.rowNums = sendNums(row, toSearch, 9);//9 is the length of 'col-md-xx';
//                        debugger;
                        jQuery("#setRowsModal").find(".num-of-grid").find("option[value='" + gridDetails.rowNums.length + "']").attr("selected", true);
                        jQuery("#setRowsModal").modal("show");
                        generateBoxForGrids("init");            //send init to reflect the last value used
                    }

//                    function to remove the concerned class that matches reg ex
                    function sendNums(row, toSearch, length) {
                        var arr = [];
                        var regEx = new RegExp(toSearch, "g");
                        jQuery(row).children("section").each(function() {
                            var thisElm = jQuery(this);
                            var classes = thisElm.attr("class");
                            var res = classes.match(regEx);
                            if (res) {
                                var classIndex = classes.search(toSearch);
                                if (classIndex > -1) {
                                    var rowClass = jQuery.trim(classes.substr(classIndex, length));
                                    gridDetails.classList.push(rowClass);       //Push the class name to be replaced later
                                    var number = rowClass.split("-")[2];
                                    arr.push(number);
                                }
                                gridDetails.elements.push(thisElm);
                            }
                        });
                        return arr;
                    }

//                    function to generate the reuired number of textboxes
                    function generateBoxForGrids() {
                        var numOfGrids = parseInt(jQuery("#setRowsModal").find(".num-of-grid").val());
                        var classVal = 12 / numOfGrids;
                        var str = '';
                        for (var i = 0; i < numOfGrids; i++) {
                            str += '<div class="col-md-' + classVal + '"><input name="term" type="number" class="form-control  grid-vals" required="" min="1" max="12" step="1" ';
                            if (arguments[0] == "init") {           //If settings clicked, reflect values used
                                str += 'value="' + gridDetails.rowNums[i] + '"';
                            }
                            str += '></div>';
                        }
                        jQuery(".val-for-grids").html(str);
                        jQuery("#setRowsModal").on("keypress", "[type='number']", function(evt) {
                            evt.preventDefault();
                        });
                    }

//                    function to set the classes according to the input
                    function setClasses(elem) {
                        var row = elem;
                        var newClasses = [];
                        var vals = [];
                        var total = 0;
                        var valid = true;
                        jQuery("#setRowsModal").find(".grid-vals").each(function() {     //check if inputs are valid
                            var thisVal = jQuery(this).val();
                            if (thisVal && thisVal < 12) {    //Value cannot be equal to 12 as atleast 2 rows are required
                                var val = parseInt(thisVal);
                                vals.push(val);
                                var cls = "col-md-" + val;          //Create class name from the input value
                                newClasses.push(cls);
                            }
                            else {
                                valid = false;
                                return false;
                            }
                        })
                        if (valid) {
                            for (var i = 0; i < vals.length; i++) {         //Calculate the sum of the values
                                total += vals[i];
                            }
                            // Check if summation =12, proceed with generating classes and replacinf them.
                            if (total === 12) {
                                if (newClasses.length >= gridDetails.elements.length) {
                                    for (var i = 0; i < newClasses.length; i++) {
                                        var theElm = row.find(gridDetails.elements[i]);
                                        if (theElm.length) {
                                            theElm.removeClass(gridDetails.classList[i]).addClass(newClasses[i]);
                                        }
                                        else {
                                            var newSection = '<section class="' + newClasses[i] + ' newlyadded"><p>Text goes here</p></section>';
                                            row.find(gridDetails.elements[i - 1]).after(newSection);
                                            gridDetails.elements[i] = row.find(".newlyadded").last();
                                        }
                                    }
                                }
                                else {
                                    for (var i = 0; i < gridDetails.elements.length; i++) {
                                        if (newClasses[i] === undefined) {
                                            row.find(gridDetails.elements[i]).remove();
                                        }
                                        else {
                                            row.find(gridDetails.elements[i]).removeClass(gridDetails.classList[i]).addClass(newClasses[i]);
                                        }
                                    }
                                }
                                row.find(".newlyadded").removeClass("newlyadded");
                                jQuery("#setRowsModal").modal("hide");      //Close modal
                            }
                            else if (total < 12 || total > 12) {
                                swal("Invalid Values!! The summation should be equal to 12");
                            }
                            else {
                                swal("Invalid values");
                            }
                        }
                        else {
                            swal("Invalid values");                 //Invalid inputs. as in characters or number>=12
                        }

                        try {
                            htmlEditor.editorEvents();      //bind events to newly added elements
                        }
                        catch (exc) {
                            console.log("Unable to bind events");
                        }
                    }
//                    htmlEditor.bindIframeDragDrop();
                })
            }

            function setMetaContent(data, elm) {
                if (elm.hasClass('chapter') || elm.hasClass('frontmatter') || elm.hasClass('backmatter') || elm.hasClass('bodymatter')) {
                    data = false;
                }

                if (!data) {
                    jQuery('.meta-dropdown .dropdown-menu').empty();
                    jQuery('#dropdownMenu1').attr('disabled', true);
                    return false;
                }
                else {
                    jQuery('#dropdownMenu1').removeAttr('disabled');
                }
                var innerHtml = '';
                var activeLength = 0;
                for (var i = 0; i < data.length; i++) {
                    var elm_class = elm.attr('class').split(' ');
                    if (data[i].length > 0) {
                        if (jQuery.inArray(data[i], elm_class) !== -1) {
                            innerHtml += '<li class="active" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        }
                        else {
                            innerHtml += '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        }
                    }
                    if (data.length === 1)
                        jQuery('#dropdownMenu1').attr('disabled', true);
                }
                jQuery('.meta-dropdown .dropdown-menu li').remove();
                jQuery('.meta-dropdown .dropdown-menu').html(innerHtml);
                jQuery('.meta-dropdown .dropdown-menu li a').off('click').on('click', function(e) {
                    var activeLength = jQuery('.meta-dropdown').find('.active').length; // Check for the active Classes.
                    htmlEditor.contentUpdated = true;
                    var thisPar = jQuery(this).parent();
                    activeLength = jQuery('.meta-dropdown').find('.active').length;
                    var classToAdd = jQuery.trim(thisPar.text());
                    if (activeLength <= 0 && !thisPar.hasClass("active")) {               //Activate class if not activated
                        jQuery(this).parent().addClass('active');
                        elm.addClass(classToAdd);
                        elm.trigger('click');
                    }
                    else if (activeLength > 0) {                                                            //Toggle if more than 1 activated.
                        if (thisPar.hasClass("active")) {
                            thisPar.removeClass("active");
                            elm.removeClass(classToAdd);
                        }
                        else {
                            thisPar.addClass("active");
                            elm.addClass(classToAdd);
                            elm.trigger('click');
                        }
                    }
                    e.stopPropagation();
                    elm.trigger('click');
                    htmlEditor.liveUpdateView();
                    htmlEditor.enableSave();
                });
            }

            try {
                var showSettings = jQuery('#highlighter').hasClass('active');
                if (showSettings) {
                    var tagName = thisElm[0].tagName;
                    //
                    if (jQuery.inArray(tagName, tagListToHide) !== -1) {
                        (thisElm.parent()).trigger('click');
                    }
                    else {
                        setSettings(thisElm);
                    }

                }

                if (typeof this_class === 'object' && htmlEditor.classConfig(thisElm) && this_class.length > 0) {
                    this_class = this_class.concat(htmlEditor.classConfig(thisElm)).unique();
                    setMetaContent(this_class, thisElm);
                }
                else if (typeof this_class === 'object' && !htmlEditor.classConfig(thisElm)) {
                    setMetaContent(this_class, thisElm);
                }
                else {
                    try {
                        this_class = this_class.concat(htmlEditor.classConfig(thisElm)).unique();
                        setMetaContent(this_class, thisElm);
                    }
                    catch (exc) {
                        this_class = (htmlEditor.classConfig(thisElm)).unique();
                        setMetaContent(this_class, thisElm);
                    }
                }

            }
            catch (exc) {
            }


        }
        e.stopPropagation();
    });
    jQuery('.tree_view,.right-pane').on('mouseover', function() {
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());    //Remove the highlights.
        htmlEditor.removeFrameSettings();
    });
}

htmlEditor.classConfig = function(thisObj) {
    var thisTagname = jQuery(thisObj)[0].tagName;
    return (typeof classSet[thisTagname] !== 'undefined' ? classSet[thisTagname] : false)
};

htmlEditor.getEditorSelection = function() {
    var selection = CKEDITOR.instances.editor1.getSelection();
    return selection;
}

htmlEditor.removeFrameSettings = function() {
    var editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    editor.find(".iframe-setting").remove();

    var removeClasses = ["indicator", "highlightSelected", "highlight_on_drop", "highlight"]; // Add classes thats not required in the mode
    var removeElems = ["toolBox", "descOfTarget"]; // Add classes of the elements that needs to be removed.

    for (var i = 0; i < removeClasses.length; i++) {                // Removes the unwanted classes from the given frame.
        editor.find("." + removeClasses[i]).removeClass(removeClasses[i]);
    }

    for (var k = 0; k < removeElems.length; k++) {                  // Removes unwanted elements from the given frame.
        editor.find("." + removeElems[k]).remove();
    }
}
Array.prototype.unique = function() {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};