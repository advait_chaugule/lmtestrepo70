var patternRules = {
    emptyNode: ['section.chapter'],
    cke_editable: {
        notAllowed: ['chapter'],
        allowed: []
    },
    chapter: {
        notAllowed: ['chapter'],
        allowed: ['*']
    },
    frontmatter: {
        notAllowed: ['chapter', 'sidebar'],
        allowed: ['*']
    },
    bodymatter: {
        notAllowed: [''],
        allowed: ['*']
    },
    backmatter: {
        notAllowed: ['chapter'],
        allowed: ['*']
    }
}

/*var patternRules = {
 chapter: {
 allowed: {},
 not_allowed: {
 section : ['chapter']
 },
 },
 frontmatter: {
 allowed: {
 aside : ['vignette','objectiveset','sidebar'],
 section : ['introduction','keywordset'],
 nav : ['toc','toc_brief','loi','page-list','landmarks','toc_alternative'],
 ol : ['objectivelist','keywordlist'],
 li : ['objective'],
 blockquote : ['epigraph'] 
 },
 not_allowed: {}
 },
 bodymatter: {
 allowed: {
 section: {
 level : ['n','n-1']
 },
 p : ['continued'],
 div : ['blockquotegroup','source'],
 pre : [],
 code : [],
 blockquote : [],
 ol : ['answerlist','biblioentrylist','rearnotelist','keywordlist','objectivelist','practicelist','steplist','tocentrylist','orderedlist'],
 figure : ['figure','informalfigure','table','informaltable','opener','inline']
 },
 not_allowed: {}
 },
 backmatter: {
 allowed: {},
 not_allowed: {}
 },
 };*/

var oneTimeDrop = {
    chapter: ["frontmatter", "bodymatter", "backmatter", "header"],
    dropped: []
};
// Add the section names you dont wanna delete using the left sortable tree.
var dontDelete = [];

var _toBeConverted = [".gadget", ".widget", "video", "iframe.exwidget"];


var dropSetting = {
    "*": ["chapter"], // Single Drop
    "level2_section": ["level 1 section"], // Not Allowed in
    "level3_section": ["level 1 section", "level 2 section"],
    "level4_section": ["level 1 section", "level 2 section", "level 3 section"],
    "level5_section": ["level 1 section", "level 2 section", "level 3 section", "level 4 section"],
    "converted gadget": ["*"], // For Patterns containing multiple classes
    "A": ["*"], // Tag that won't allow drop
}

var tableContent = ['TR', 'TD'];

var majorClasses = ["frontmatter", "bodymatter", "backmatter"];

//var excludetags= ["HTML", "BODY","SPAN.url-link","P.mode-text","DIV.converted","DIV.lm-rochak-out","DIV.lm-rochak","STRONG"];

var excludetags = ["HTML", "BODY","DIV.clearfix"];
var excludeChildrenIn = ["DIV.converted"];
var addAfter = ["P", "P.enable-sort", "SPAN", "DIV.converted", "DIV.converted gadget"];
var tagsWithoutContent= ["IFRAME","IMG","OBJECT","SOURCE"];
var widgetClasses = ["flashcard", "showhide", "slideline"];

var widgetScope = {}

var profileList = [
    {
        "name" : "print",
        "subProfile" : []
    },
    {
        "name" : "digital",
        "subProfile" : []
    },
    {
        "name" : "",
        "subProfile" : []
    }
]