//Convert the element in "_toBeConverted" into div
htmlEditor.resetQuadData = function () {
    htmlEditor.aContent = [];
    htmlEditor.cCounter = 0;
}

htmlEditor.resetQuadData();
htmlEditor.VideoModelBool = true;

htmlEditor.convertFrameToDiv = function () {
    for (var i = 0; i < _toBeConverted.length; i++) {
        jQuery(".cke_wysiwyg_frame").contents().find(_toBeConverted[i]).each(function () {
            var thisFrame = jQuery(this);
            var theSrc = "";
            var poster = "";
            var patt_name = "";
            var id = thisFrame.attr("id") === undefined ? "" : thisFrame.attr("id");
            thisFrame.removeClass("highlight_on_drop");
            var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
            var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
            var theClasses = thisFrame.attr("class") === undefined ? "" : thisFrame.attr("class");
            var divContent = "";
            var scrolling;
            // Functions to generate the placeholders for elements mentioned in _toBeConverted variable.
            function forIframe() {
                theSrc = thisFrame.attr("src") === undefined ? thisFrame.attr("data") : thisFrame.attr("src");
                var isobject = thisFrame.prop("tagName") === "IFRAME" ? false : thisFrame.hasClass("converted") ? thisFrame.attr("isobject") : true,
                        profileSettings = htmlEditor.getProfileSettings(thisFrame);
                var gadgetID = thisFrame.attr('gadget-id') ? thisFrame.attr('gadget-id') : thisFrame.attr('data-gadget-id');
                scrolling = thisFrame.attr("scrolling") === undefined ? "yes" : thisFrame.attr("scrolling");
                patt_name = "Gadget";
                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '"contenteditable="false" src="' + theSrc + '" isobject=' + isobject + ' id=' + id + ' scrolling=' + scrolling + ' style="height: ' + theHeight + 'px; width:' + theWidth + 'px;" ' + profileSettings + ' data-gadget-id=' + gadgetID + '><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Gadget</strong><p class="mode-text"' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div></div></div>';
            }

            function forVideo() {
                theSrc = thisFrame.find("source").attr("src") === undefined ? thisFrame.attr("src") : thisFrame.find("source").attr("src");
                poster = !thisFrame.is("[poster]") ? "" : thisFrame.attr("poster");
                patt_name = "Video";
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);
                if (poster.length) {
                    poster = poster.replace("video", "images");
                    divContent = '<div type=' + _toBeConverted[i] + ' class="converted ' + theClasses + '"contenteditable="false" src="' + theSrc + '" id=' + id + ' poster=' + poster + ' style="height: ' + theHeight + 'px; width:' + theWidth + 'px;background-image:url(' + poster + '); background-size: 100% 100%;background-repeat: no-repeat;" ' + profileSettings + '><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-video-o" ' + profileSettings + '> </div></div><strong>Video</strong><p class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode))</p><span class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div><div></div></div></div>';
                }
                else {
                    divContent = '<div type=' + _toBeConverted[i] + ' class="converted ' + theClasses + '"contenteditable="false" src="' + theSrc + '" id=' + id + ' style="height: ' + theHeight + 'px; width:' + theWidth + 'px;" ' + profileSettings + '><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-video-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Video</strong><p class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode))</p><span class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div><div></div></div></div>';
                }
            }

            function forObject() {
                theSrc = thisFrame.attr("data");
                patt_name = "Object";
                var datauuid = thisFrame.attr("data-uuid");
                var paramName = thisFrame.children().attr("name");
                var paramVal = thisFrame.children().attr("value");
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);

                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '"contenteditable="false" src="' + theSrc + '" id=' + id + ' datauuid=' + datauuid + ' paramName=' + paramName + ' paramVal= ' + paramVal + ' ' + profileSettings + '><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Gadget</strong><p class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div></div></div>';
            }
            //console.log(_toBeConverted[i])
            function forQuadType() {
                theSrc = thisFrame.attr("data-qID");
                scrolling = thisFrame.attr("scrolling") === undefined ? "yes" : thisFrame.attr("scrolling");
                patt_name = "Quad";
                var qID = thisFrame.attr('data-qID') === undefined ? "" : thisFrame.attr('data-qID');
                var iframeEntityType = thisFrame.attr('data-iframeEntityType') === undefined ? "" : thisFrame.attr('data-iframeEntityType');
                var iframeEntityTypeText = iframeEntityType == 3 ? ' Question' : iframeEntityType == 2 ? ' Assessment' : '';
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);
                                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '"contenteditable="false" data-qID="' + qID + '" data-iframeEntityType ="' + iframeEntityType + '"  src="' + theSrc + '" id=' + id + ' scrolling="' + scrolling + '" ' + profileSettings + ' height="' + theHeight + '" width="' + theWidth + '"><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Quad</strong><p class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>' + patt_name + iframeEntityTypeText + ': ' + qID + '</span></div></div></div>';
  }

            function forExWidgets() {
                //Build mode
                theSrc = thisFrame.attr("src");
                patt_name = "Widget";

                var dataPath = thisFrame.attr("data-path"),
                        widgetType = thisFrame.attr('widget-type'),
                        configPath = "",
                        configFrameEl = jQuery('#exwidgetModal').find('iframe'),
                        configModalHeaderEl = jQuery('#exwidgetModal').find('#modalBoxTitle'),
                        widget_id = thisFrame.attr('widget-id'),
                        profileSettings = htmlEditor.getProfileSettings(thisFrame);

                if (widget_id) {
                    try {
                        configPath = widgetScope.configPage[widget_id];
                    }
                    catch (exc) {
                        //console.log(exc);
                    }

                }

                configFrameEl.attr('src', configPath); //Sets widgets dynamic frame path
                //configModalHeaderEl.html('Edit ' + (widgetType.toUpperCase())); //Sets widgets modal header value

                /*var setModalSizeAsFrame = (function(){
                 var frameElHt = jQuery(configFrameEl.contents()[0]).height(),
                 winHt = jQuery(window).height(),
                 assignHeight = frameElHt > winHt/2 ? winHt : frameElHt;
                 
                 configFrameEl.height(assignHeight);
                 })();*/

                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '"contenteditable="false" src="' + theSrc + '" id=' + id + ' style="height: ' + theHeight + 'px; width:100%;" data-path=' + dataPath + ' widget-id=' + widget_id + ' widget-type=' + widgetType + ' ' + profileSettings + '><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Widget</strong><p class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>Type: ' + (widgetType.toUpperCase()) + ' </span></div></div></div>';
            }

            switch (_toBeConverted[i]) {
                case ".gadget":
                    forIframe();
                    break;

                case "video":
                    forVideo();
                    break;

                case ".gadget[data]":
                    forObject();
                    break;

                case ".widget":
                    forQuadType();
                    break;

                case "iframe.exwidget":
                    forExWidgets();
                    break;
            }
            if (thisFrame.next().prop("tagName") === "P") {
                thisFrame.replaceWith(divContent);
            }
            else {
                thisFrame.replaceWith(divContent + "<p>&nbsp;</p>");
            }
        })
    }
    //htmlEditor.bindEvents();
};

//Convert div back to its element
htmlEditor.convertDivToFrame = function (selectFrame) {
    jQuery(selectFrame).find("div.converted").each(function () {
        var thisDiv = jQuery(this);
        var theSrc = thisDiv.attr("src");
        var poster = !thisDiv.is("[poster]") ? "" : thisDiv.attr("poster");
        var theId = thisDiv.attr("id");
        var allowScroll = thisDiv.attr("scrolling");
        var theHeight = parseInt(thisDiv.height());
        var theWidth = parseInt(thisDiv.width());
        thisDiv.removeClass("converted");                   // Remove the identifier class.
        var theClasses = thisDiv.attr("class");
        var elem = thisDiv.attr("type");
        var origElem = "";
        var profileSettings = htmlEditor.getProfileSettings(thisDiv);

        function forIframe() {

            if (thisDiv.attr("isobject") === "true") {
                var gadgetID = thisDiv.attr('gadget-id') ? thisDiv.attr('gadget-id') : thisDiv.attr('data-gadget-id');
                origElem = '<object class="' + theClasses + '" data="' + theSrc + '" id=' + theId + ' height="' + theHeight + 'px" width="' + theWidth + 'px" data-offlinesupport="yes" ' + profileSettings + ' data-gadget-id=' + gadgetID + '><span class="fallback" ' + profileSettings + '> Fallback for Gadgets</span></object>';
            }
            else {
                origElem = '<iframe id=' + theId + ' name="frame' + theId + '" class="' + theClasses + '"src="' + theSrc + '" ' + profileSettings + ' style="border:none;overflow:auto;" height="' + theHeight + '" width="' + theWidth + '"></iframe>';
            }
        }

        function forVideo() {
            if (poster.length) {
                origElem = '<video id=' + theId + ' class="' + theClasses + '" height="' + theHeight + '" width="' + theWidth + '" poster=' + poster + ' controls ' + profileSettings + '><source src="' + theSrc + '" type="video/mp4" ' + profileSettings + '><span class="fallback" ' + profileSettings + '>Sorry, it appears your system  either does not support video playback or cannot play the  MP4 format or WebM format provided.</span></video>';
            }
            else {
                origElem = '<video id=' + theId + ' class="' + theClasses + '" height="' + theHeight + '" width="' + theWidth + '" controls ' + profileSettings + '><source src="' + theSrc + '" type="video/mp4" ' + profileSettings + '><span class="fallback" ' + profileSettings + '>Sorry, it appears your system  either does not support video playback or cannot play the  MP4 format or WebM format provided.</span></video>';
            }
        }

        function forObject() {
            var datauuid = thisDiv.attr("datauuid");
            var paramName = thisDiv.attr("paramName");
            var paramVal = thisDiv.attr("paramVal");
            origElem = '<object type="text/html" id=' + theId + ' data-uuid=' + datauuid + ' class="' + theClasses + '" data="' + theSrc + '" ' + profileSettings + '><param value="' + paramVal + '" name="' + paramName + '" ' + profileSettings + ' /></object>';
        }
        var qID = '';
        function forQuadType() {
            //debugger
            qID = thisDiv.attr('data-qID');
            var iframeEntityType = thisDiv.attr('data-iframeEntityType') === undefined ? "" : thisDiv.attr('data-iframeEntityType');
            var source = clientUrl + '/widgets/quad/questionPreview.html';
            theSrc = source;
            htmlEditor.getQuadResponse(qID, iframeEntityType);
            //console.log(htmlEditor.switch_btn_state);
            var jPath = jQuery.trim(htmlEditor.dataPath.path);
            //origElem = '<object id="' + theId + '" name="frame' + theId + '" class="' + theClasses + '" data="' + theSrc + '" height="auto" width="' + theWidth + '" data-qID="' + qID + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '" ' + profileSettings + '></object>';
            origElem = '<object id="' + theId + '" name="frame' + theId + '" class="' + theClasses + '" data="' + theSrc + '" style="border:none;overflow:auto;" data-qID="' + qID + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '" ' + profileSettings + '><span class="fallback"' + profileSettings + '>Fallback for assessment</span></object>';

        }

        function forExWidgets() {
            //Preview mode
            var widgetType = thisDiv.attr("widget-type"),
                    dataPath = thisDiv.attr("data-path"),
                    widget_id = thisDiv.attr('widget-id'),
                    playerPath = widgetScope.launchPage[widget_id];

            if (widget_id) {
                playerPath = widgetScope.launchPage[widget_id];
            }
            origElem = '<iframe id=' + theId + ' name="frame' + theId + '" class="' + theClasses + '"src="' + playerPath + '" height="' + theHeight + '" width="100%" frameBorder="0" data-path=' + dataPath + ' widget-id="' + widget_id + '" widget-type="' + widgetType + '" ' + profileSettings + '></iframe>';
        }

        switch (elem) {
            case ".gadget":
                forIframe();
                break;

            case "video":
                forVideo();
                break;

            case ".gadget[data]":
                forObject();
                break;

            case ".widget":
                forQuadType();
                break;

            case "iframe.exwidget":
                forExWidgets();
                break;
        }
        thisDiv.replaceWith(origElem);
        if (elem == 'iframe.widget') {
            var objRef = (jQuery(selectFrame).contents().find('#' + theId))[0];

            //htmlEditor.getQuadResponse(qID);
            if (objRef !== undefined) {
                try {
                    var jData = JSON.parse(htmlEditor.dataPath.data),
                            jPath = jQuery.trim(htmlEditor.dataPath.path);
                    htmlEditor.aContent.push({
                        'el': objRef,
                        'data': jData,
                        'path': jPath
                    });
                }
                catch (exc) {
                    htmlEditor.aContent.push({
                        'el': objRef,
                        'data': '',
                        'path': jPath
                    });
                }
            }

            if (jQuery(selectFrame).contents().find('.widget').length == htmlEditor.aContent.length) {

                var updateFrameCont = function () {
                    var frameEl = htmlEditor.aContent[htmlEditor.cCounter].el,
                            frameData = htmlEditor.aContent[htmlEditor.cCounter].data,
                            frameDataPath = htmlEditor.aContent[htmlEditor.cCounter].path;


                    try {
                        if (frameData.length != 0) {
                            jQuery(frameEl).attr('data-path', frameDataPath);
                        }

                        htmlEditor.cCounter++;
                        if (htmlEditor.cCounter < jQuery('#testCont').contents().find('.widget').length) {
                            //setTimeout(function(){
                            updateFrameCont();
                            //},500);
                        }
                        else {
                            htmlEditor.resetQuadData();
                            /*htmlEditor.aContent = [];
                             htmlEditor.cCounter = 0;*/
                        }
                    }
                    catch (exc) {
                        //setTimeout(function(){
                        updateFrameCont();
                        //},500);
                    }
                }
                updateFrameCont();
            }
        }
    })
};

htmlEditor.getProfileSettings = function (el) {
    var data_profile_deliveryformat = el.attr('data-profile-deliveryformat');
    var data_parent_added_profile = el.attr('data-parent-added-profile');
    
    var hasProfileSettings = (typeof data_profile_deliveryformat !== undefined || !data_profile_deliveryformat) ? "data-profile-deliveryformat = '" + data_profile_deliveryformat+"'" : false,
            isProfileAddedByParent = (typeof data_parent_added_profile != undefined || !data_parent_added_profile) ? "data-parent-added-profile = '" + data_parent_added_profile+"'" : false;

    return hasProfileSettings + ' ' + isProfileAddedByParent;
}

//Show the settings option on the div element created.
htmlEditor.showSettingForFrame = function (frame) {
    var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    try {
        htmlEditor.removeFrameSettings();
    }
    catch (exc) {

    }
    //console.log(the_editor.removeClass("highlightSelected"));
    var ht = frame.height();                                    // height of the gadget or the target elem.
    var wide = frame.width();                                   // width of the target element.
    var offset = frame.offset();                                // Offset of the target element.

    var type = frame.attr("type");

    var dataPos = frame.attr("data-pos");

    var overlay = '';
    if (frame.hasClass('widget')) {
        overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div><div class="toolBoxLarge to-refresh" contenteditable="false"><span class="glyphicon glyphicon-refresh" title=""></span> REFRESH </div></div>';
    }
    else if (frame.hasClass('exwidget')) {
        overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div> <div class="toolBoxLarge to-copy" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> REUSE EXISTING </div> </div>';
    }
    else {
        overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div></div>';
    }
    //console.log(the_editor);
    the_editor.append(overlay);
    var scrolled = the_editor.scrollTop();
    if (scrolled > offset.top) {
        the_editor.scrollTop(offset.top);
    }
    function showModalCloseBtn(status) {
        if (status) {
            jQuery('#exwidgetModal button.close').show();
            jQuery('#widget_copy_modal button.close').show();
            jQuery('#quadModal button.close').show();
        }
        else {
            jQuery('#exwidgetModal button.close').hide();
            jQuery('#widget_copy_modal button.close').hide();
            jQuery('#quadModal button.close').hide();
        }
    }
    the_editor.find(".iframe-setting .toolBoxLarge.to-copy").off().on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();
        showModalCloseBtn(false);
        htmlEditor.thisFrame = frame;
        htmlEditor.intiateWidgetClone(frame);
        //console.log(frame);
        jQuery('#widget_copy_modal').modal('show');

        /*jQuery('#widgetCopyCont').off('click').on('click', function(){
         alert(1) 
         });*/
    });

    the_editor.find(".iframe-setting .toolBoxLarge.to-set").off().on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();

        switch (type) {
            case "video":
                showModalCloseBtn(true);
                if (htmlEditor.VideoModelBool) {
                    htmlEditor.openVideoModal(frame);
                } else {
                    swal("Video Uploading in Progress.");
                }
                break;

            case ".gadget":
                //console.log("gadget");
                showModalCloseBtn(true);
                htmlEditor.enableSetting(frame);
                break;

            case ".gadget[data]":
                showModalCloseBtn(true);
                htmlEditor.enableSettingForObject(frame);
                break;

            case ".widget":
                //console.log("widget");
                showModalCloseBtn(false);
                htmlEditor.enableQuadSetting(frame);
                break;

            case "iframe.exwidget":
                //console.log("iframe.widget");
                showModalCloseBtn(false);
                htmlEditor.exwidgetSetting(frame);
                htmlEditor.enableSave();
        }
    });
    //htmlEditor.widgetDelSet = [];
    the_editor.find(".iframe-setting .toolBoxLarge.to-del").off().on("click", function (e) {
        /*var widget_data_id = jQuery(frame).attr('id'),
         widget_id = jQuery(frame).attr('widget-id'),
         object_id = node_id;
         
         htmlEditor.widgetDelSet.push({
         "project_id" : project_id,
         "widget_id" : widget_id,
         "object_id" : object_id,
         "widget_data_id" : widget_data_id
         });*/

        e.stopPropagation();
        e.preventDefault();
        var frameParId = jQuery(frame).parents("[id]").attr("id");
        jQuery(frame).remove();

        //        jQuery(".iframe-setting").addClass("hideElm");
        the_editor.find(".iframe-setting").deleteTOCNode();
        jQuery(this).parents(".iframe-setting").remove();
        if (jQuery("#sourceVal:visible").length) {
            splitview.updateCode(frameParId);
        }
        htmlEditor.enableSave();
    });

    the_editor.find(".iframe-setting .toolBoxLarge.to-refresh").off().on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();
        
        var qID = jQuery(frame).attr('data-qID');
        var iframeEntityType = jQuery(frame).attr('data-iframeEntityType');
        if (qID != undefined)
            sessionStorage.removeItem(qID);
            htmlEditor.getQuadResponse(qID, iframeEntityType);
        htmlEditor.enableSave();
    });

    //    the_editor.find(".iframe-setting").on("mouseleave", function(){           //remove the overlay on mouseout
    //        jQuery(this).remove();
    //    })
}

// Settings in the modal window of video element.
htmlEditor.openVideoModal = function (this_Vid) {
    htmlEditor.checkedVideoPath = "";
    var this_elm = this_Vid;
    var page_num = 1;
//    htmlEditor.tabIndex = 0;
    jQuery(".paginate").hide();
    jQuery('#videoModal').find('.search_text').val('');
    htmlEditor.newVidObj.url = this_elm.attr("src");
    var getFrom = htmlEditor.PXEmappedAPI.getAllVideos;
    htmlEditor.getMediaVideo(page_num, false, getFrom);
    jQuery("#videoModal .loc-or-global").addClass("active");    //Select the local by default.

    //Set default values
//    jQuery('#videoTerm').val(this_elm.attr('src'));
    jQuery('#videoHeight').val(this_elm.height());
    jQuery('#videoWidth').val(this_elm.width());

    jQuery('#videoModal').modal('show');
    jQuery('.video_conf').off('click').on('click', function () {
//        if (jQuery('#videoModal .box-theme.active').length) {
//            htmlEditor.newVidObj.url = jQuery('#videoModal .box-theme.active').find("img").attr("videoPath");
//        }
        htmlEditor.chngVideoSrc(this_elm);
    });

    jQuery("#video_thumb .inputfrom").empty();
    jQuery('.video_get_media').attr("disabled", false);
//    jQuery('#videoTerm').val(this_elm.attr('src'));

    jQuery(".video_conf").attr("disabled", true);
    jQuery('#videoHeight, #videoWidth').on('change keyup', function () {
        var position = this.selectionStart;
        var videoHeight = jQuery('#videoHeight').val();
        var videoWidth = jQuery('#videoWidth').val();
        var sanitized1 = "";
        var sanitized2 = "";

        sanitized1 = videoHeight.replace(/[^0-9]/g, '');
        jQuery('#videoHeight').val(sanitized1);
        this.selectionEnd = position;

        sanitized2 = videoWidth.replace(/[^0-9]/g, '');
        jQuery('#videoWidth').val(sanitized2);
        this.selectionEnd = position;


        if (sanitized1 == "" || sanitized2 == "") {

            jQuery(".video_conf").attr("disabled", true);
        } else {
            jQuery(".video_conf").attr("disabled", false);
        }

    });

    var getRepositoryType = function () {                            //get the repository type, i.e, local / global.
        var fetchFrom = jQuery("#videoModal .loc-or-global.active").text();
        var fromUrl;
        var actives = jQuery("#videoModal .loc-or-global.active").length;
        if (actives > 1) {
            fromUrl = htmlEditor.PXEmappedAPI.getAllVideos;
        }
        else {
            if (fetchFrom.toLowerCase() === "local") {
                fromUrl = htmlEditor.PXEmappedAPI.getLocalVidsAPI;
            }
            else if (fetchFrom.toLowerCase() === "global") {
                fromUrl = htmlEditor.PXEmappedAPI.getGlobalVidsAPI;
            }
            else if (fetchFrom.toLowerCase() === "all") {
                fromUrl = htmlEditor.PXEmappedAPI.getAllVideos;
            }
        }
        return fromUrl;                                             //Return the URL to be used.
    }

    jQuery('#videoModal').find('.search_btn').off('click').on('click', function () {
        var url = getRepositoryType();
        htmlEditor.getMediaVideo(page_num, true, url);
    });
    jQuery('#videoModal').find('.clear_text').off('click').on('click', function () {
        var url = getRepositoryType();
        jQuery('#videoModal').find('.search_text').val('');
        htmlEditor.getMediaVideo(page_num, false, url);
    });

    jQuery("#videoModal .loc-or-global").off().on("click", function () {
//        if (!jQuery(this).hasClass("active")) {
//            jQuery(".loc-or-global").removeClass("active");
        jQuery(this).toggleClass("active");
        htmlEditor.getMediaVideo(page_num, false, getRepositoryType());
//            htmlEditor.callImages(page_num, false, getRepositoryType());
//            jQuery("#imgCont").attr("disabled", true);
//        }
    })
};

//Change the description on the editor about the video.
htmlEditor.newVidObj = {};
htmlEditor.newVidObj.id = [];
htmlEditor.chngVideoSrc = function (this_elm) {
//    var video_src = jQuery('#videoTerm').val(); 
    var video_src = htmlEditor.newVidObj.url;
    var video_height, video_width;
    video_height = jQuery('#videoHeight').val() > 0 ? jQuery('#videoHeight').val() : false;
    video_width = jQuery('#videoWidth').val() > 0 ? jQuery('#videoWidth').val() : false;
    var videoElm = this_elm;
    //        Send the video back to the server to be mapped against local/global asset list
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
        type: 'POST',
        data: {
            'project_id': project_id,
            'is_global': '',
            'object_id': JSON.stringify(htmlEditor.newVidObj.id)
        },
        async: true,
        xhrFields: {
            withCredentials: true
        },
        success: function (data, textStatus, jqXHR) {

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("unable to put up the request")
        }
    })
    if (video_height) {
        if (video_height >= 50 && video_height < 125 || video_height < 50) {
            videoElm.find(".lm-rochak").addClass("width125");
        }
        else {
            videoElm.find(".lm-rochak").removeClass("width125");
        }
        videoElm.height(video_height);
    }
    if (video_width)
        videoElm.width(video_width);
//    if (htmlEditor.newVidObj.url.length) {
//        videoElm.attr('src', video_src);
//    }
//    else {
//        video_src = jQuery('#videoModal .box-theme.active').find("img").attr("videoPath");
    videoElm.attr('src', video_src);
//    }
    videoElm.css("background-size", video_width + "px " + video_height + "px");
    var createInfo = 'Video URL: ' + video_src;
    videoElm.find(".url-link").html(createInfo + "<div> </div>");
    videoElm.trigger("click");

    //        videoElm.load();

    htmlEditor.enableSave();
    jQuery('#videoModal').modal('hide');
};

htmlEditor.newGadgetObj = {};             //Holds the details of the gadget, while setting is being changed.

// Settings modal window for the iframe elements
htmlEditor.enableSetting = function (theFrame) {
    htmlEditor.checkedWidgetPath = "";
    jQuery(".iframe_conf").attr("disabled", true);
    jQuery('#iframeModal').modal('show');

    jQuery('#iframeModal').find('.search_text').val('');
    htmlEditor.newGadgetObj.gadgetId = [];
    var page_num = 1;
    htmlEditor.getGadgetList(page_num, false, htmlEditor.PXEmappedAPI.getAllGadgets);
    jQuery("#iframeModal .loc-or-global").addClass("active");    //Select the local by default.
    var scrollStatus = "no";
    var this_ifrm = theFrame;
    var this_ifrm_hgt = this_ifrm.height();
    var this_ifrm_wdth = this_ifrm.width();
    var this_ifrm_url = jQuery.trim(this_ifrm.attr('src'));
    var this_ifrm_scroll = this_ifrm.attr("scrolling");

    jQuery('#iframeHeight').on('keydown', dotChecker);
    jQuery('#iframeHeight').on('keyup', function (e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    jQuery('#iframeWidth').on('keydown', dotChecker);
    jQuery('#iframeWidth').on('keyup', function (e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    function dotChecker(e) {
        var contArray = (jQuery(this).val()).split('');
        var dotLength = 0;
        jQuery(contArray).each(function (i, v) {
            if (v === '.')
                dotLength++;
        });

        if (dotLength > 0 || (e.keyCode === 110 || e.keyCode === 190))
            return false;

        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    jQuery('#iframeTerm').val(this_ifrm_url);
    jQuery('#iframeHeight').val(this_ifrm_hgt);
    jQuery('#iframeWidth').val(this_ifrm_wdth);

    if (this_ifrm_scroll == "yes") {
        jQuery("#iframeScroll").prop("checked", true);
    }
    else {
        jQuery("#iframeScroll").prop("checked", false);
    }

    var getRepositoryType = function () {                            //get the repository type, i.e, local / global.
        var fetchFrom = jQuery("#iframeModal .loc-or-global.active").text();
        var fromUrl;
        var actives = jQuery("#iframeModal .loc-or-global.active").length;
        if (actives > 1) {
            fromUrl = htmlEditor.PXEmappedAPI.getAllGadgets;
        }
        else {
            if (fetchFrom.toLowerCase() === "local") {
                fromUrl = htmlEditor.PXEmappedAPI.getLocalGadgets;
            }
            else if (fetchFrom.toLowerCase() === "global") {
                fromUrl = htmlEditor.PXEmappedAPI.getGlobalGadgets;
            }
            else if (fetchFrom.toLowerCase() === "all") {
                fromUrl = htmlEditor.PXEmappedAPI.getAllGadgets;
            }
        }
        return fromUrl;                                             //Return the URL to be used.
    }

    jQuery('#iframeModal').find('.search_btn').off('click').on('click', function () {
        var url = getRepositoryType();
        htmlEditor.getGadgetList(page_num, true, url);
    });
    jQuery('#iframeModal').find('.clear_text').off('click').on('click', function () {
        var url = getRepositoryType();
        jQuery('#iframeModal').find('.search_text').val('');
        htmlEditor.getGadgetList(page_num, false, url);
    });

    jQuery('#iframeTerm').on("change", function () {
        jQuery(".iframe_conf").attr("disabled", false);
    })
    jQuery("#iframeModal .loc-or-global").off().on("click", function () {
//        if (!jQuery(this).hasClass("active")) {
//            jQuery(".loc-or-global").removeClass("active");
        jQuery(this).toggleClass("active");
        htmlEditor.getGadgetList(page_num, false, getRepositoryType());
//            htmlEditor.callImages(page_num, false, getRepositoryType());
//            jQuery("#imgCont").attr("disabled", true);
//        }
    })
    jQuery('#iframeHeight').on('keydown',function(){
    //console.log(this_ifrm_hgt);
    jQuery(".iframe_conf").attr("disabled", false);
          
         });
    jQuery('#iframeWidth').on('keydown',function(){
        jQuery(".iframe_conf").attr("disabled", false);
    });

    jQuery('#iframeModal .iframe_conf').off('click').on('click', function () {
        htmlEditor.enableSave();
        var gadgetObj, iframeTermVal;
         if(jQuery('#iframeHeight').val().length==0){
           jQuery('#iframeHeight').val(150);
           jQuery('#iframeWidth').val(940);
        }
         if(jQuery('#iframeWidth').val().length==0){
           jQuery('#iframeHeight').val(150);  
           jQuery('#iframeWidth').val(940);
        }
        var iframeHeight = jQuery.trim(jQuery('#iframeHeight').val());
        var iframeWidth = jQuery.trim(jQuery('#iframeWidth').val());
        iframeTermVal = jQuery.trim(jQuery('#iframeTerm').val());

        //check if the user has selected some local gadget or entered an external URL, and proceed accrodingly .
        if (htmlEditor.newGadgetObj.gadgetId.length && !iframeTermVal.length) {
            //        Send the gadget back to the server to be mapped against local/global asset list
            jQuery.ajax({
                url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
                type: 'POST',
                data: {
                    'project_id': project_id,
                    'is_global': '',
                    'object_id': JSON.stringify(htmlEditor.newGadgetObj.gadgetId)
                },
                async: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function (data, textStatus, jqXHR) {

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("unable to put up the request")
                }
            })

            var url = htmlEditor.PXEmappedAPI.getGadgetManifest + "/object_id/" + htmlEditor.newGadgetObj.gadgetId + "?access_token=" + access_token + '&project_id=' + project_id;     //Url to get the gadget page.


            jQuery.when(gadgetObj = htmlEditor.ajaxCalls(url)).then(function () {        //Get the url of gadget
                iframeTermVal = gadgetObj.responseJSON.data.manifest.launch_file;
                this_ifrm.attr("isobject", true);               //mark this particular gadget for converting into object
                setValues();
            })
        }
        else {
            this_ifrm.attr("isobject", false);                  //mark this particular gadget for converting into object
            setValues();
        }

        function setValues() {
            if (iframeHeight >= 50 && iframeHeight <= 125 || iframeHeight < 50) {  //Reduce the font-size & stuffs if height is less.
                this_ifrm.find(".lm-rochak").addClass("width125");
            }
            else {
                this_ifrm.find(".lm-rochak").removeClass("width125");
            }

            if (jQuery("#iframeScroll").is(":checked")) {
                scrollStatus = "yes";
            }
            else {
                scrollStatus = "no";
            }

            var iframeScroll = scrollStatus;

            if (iframeTermVal.length && iframeHeight > 0 && iframeWidth > 0) {
//                if (iframeHeight < 500) {
////                    swal("Gadget Height cannot be lesser than 500. Applying values...")
//                    iframeHeight = 500;
//                }
                this_ifrm.attr("src", iframeTermVal);
                this_ifrm.css("height", iframeHeight);
                this_ifrm.css("width", iframeWidth);
                this_ifrm.attr("scrolling", iframeScroll);
                var createInfo = 'Gadget URL: ' + iframeTermVal; //+'<br> Height: '+iframeHeight+'px <br> Width: '+iframeWidth+'px';
                this_ifrm.find(".url-link").html(createInfo);
                this_ifrm.trigger("click");
                if (this_ifrm.attr('gadget-id')) {
                    this_ifrm.attr('gadget-id', htmlEditor.newGadgetObj.gadgetId);
                }
                if (this_ifrm.attr('data-gadget-id')) {
                    this_ifrm.attr('data-gadget-id', htmlEditor.newGadgetObj.gadgetId);
                }
                //                this_ifrm.find(".gadgetURL").text(iframeTermVal);
                //                this_ifrm.find(".gadgetHt").text(iframeHeight);
                //                this_ifrm.find(".gadgetWidth").text(iframeWidth);
                jQuery('#iframeModal').modal('hide');
            }
            else {
                swal("Input Correct values");
            }
        }
    });
};

// Settings modal window for the Quad elements
htmlEditor.enableQuadSetting = function (theFrame) {
    jQuery('#quadModal').modal('show');
    var scrollStatus = "no";
    var this_ifrm = theFrame;
    var this_ifrm_hgt = this_ifrm.height();
    var this_ifrm_wdth = this_ifrm.width();
    var this_ifrm_url = jQuery.trim(this_ifrm.attr('src'));
    var this_qID = jQuery.trim(this_ifrm.attr('data-qID')) == undefined ? '' : jQuery.trim(this_ifrm.attr('data-qID'));
    var this_entity_type_id = this_ifrm.attr('data-iframeentitytype') == undefined || this_ifrm.attr('data-iframeentitytype').length == 0 ? '0' : this_ifrm.attr('data-iframeentitytype');
    var this_ifrm_scroll = this_ifrm.attr("scrolling");

    function checkConfirm(flg) {
        this_entity_type_id = jQuery("#quadFrameEntityType :selected").attr('value');
        var iframeTermVal = jQuery.trim(jQuery('#quadFrameTerm').val());
        var iframeHeight = jQuery.trim(jQuery('#quadFrameHeight').val());
        var iframeWidth = jQuery.trim(jQuery('#quadFrameWidth').val());

        if (flg && Number(this_entity_type_id) && iframeTermVal.length && iframeHeight > 0 && iframeWidth > 0) {
            jQuery('#quadModal .iframe_conf').removeAttr('disabled');
        }
        else {
            jQuery('#quadModal .iframe_conf').attr('disabled', 'true');
        }
    }

    jQuery('#quadFrameEntityType').on('change', function () {
        checkConfirm(true);
    });

    jQuery('#quadFrameTerm, #quadFrameHeight, #quadFrameWidth').on('keyup', function () {
        checkConfirm(true);
    });

    jQuery('#quadFrameHeight').on('keydown', dotChecker);
    jQuery('#quadFrameHeight').on('keyup', function (e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    jQuery('#quadFrameWidth').on('keydown', dotChecker);
    jQuery('#quadFrameWidth').on('keyup', function (e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    function dotChecker(e) {
        var contArray = (jQuery(this).val()).split('');
        var dotLength = 0;
        jQuery(contArray).each(function (i, v) {
            if (v === '.')
                dotLength++;
        });

        if (dotLength > 0 && (e.keyCode === 110 || e.keyCode === 190))
            return false;

        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    jQuery('#quadFrameTerm').val(this_qID);
    jQuery('#quadFrameEntityType').val(this_entity_type_id);
    jQuery('#quadFrameHeight').val(this_ifrm_hgt);
    jQuery('#quadFrameWidth').val(this_ifrm_wdth);
    if (this_ifrm_scroll == "yes") {
        jQuery("#quadFrameScroll").prop("checked", true);
    }
    else {
        jQuery("#quadFrameScroll").prop("checked", false);
    }

    jQuery('#quadModal .iframe_conf').off('click').on('click', function () {
        htmlEditor.enableSave();
        var iframeTermVal = jQuery.trim(jQuery('#quadFrameTerm').val());
         var iframeHeight = jQuery.trim(jQuery('#quadFrameHeight').val());
        var iframeWidth = jQuery.trim(jQuery('#quadFrameWidth').val());
        var iframeEntityType = jQuery('#quadFrameEntityType').val();
        htmlEditor.getQuadResponse(iframeTermVal, iframeEntityType);
        if (iframeHeight >= 50 && iframeHeight <= 125 || iframeHeight < 50) {  //Reduce the font-size & stuffs if height is less.
            this_ifrm.find(".lm-rochak").addClass("width125");
        }
        else {
            this_ifrm.find(".lm-rochak").removeClass("width125");
        }

        if (jQuery("#quadFrameScroll").is(":checked")) {
            scrollStatus = "yes";
        }
        else {
            scrollStatus = "no";
        }

        var iframeScroll = scrollStatus;
        if (iframeTermVal.length && iframeHeight > 0 && iframeWidth > 0) {
            var source = clientUrl + '/widgets/quad/questionPreview.html';
            this_ifrm.attr("src", source);
            this_ifrm.attr("data-qID", iframeTermVal);
            this_ifrm.attr('data-iframeEntityType', iframeEntityType);
            this_ifrm.css("height", iframeHeight);
            this_ifrm.css("width", iframeWidth);
            this_ifrm.attr("scrolling", iframeScroll);
            var typeText = this_ifrm.attr('data-iframeEntityType') == 3 ? 'Question' : this_ifrm.attr('data-iframeEntityType') == 2 ? 'Assessment' : '';
            var createInfo = 'Quad ' + typeText + ': ' + iframeTermVal; //+'<br> Height: '+iframeHeight+'px <br> Width: '+iframeWidth+'px';
            this_ifrm.find(".url-link").html(createInfo);
            this_ifrm.trigger("click");
            //                this_ifrm.find(".gadgetURL").text(iframeTermVal);
            //                this_ifrm.find(".gadgetHt").text(iframeHeight);
            //                this_ifrm.find(".gadgetWidth").text(iframeWidth);
            jQuery('#quadModal').modal('hide');
        }
        else {
            swal("Input Correct values");
        }
    });
    checkConfirm(false);
};
var qidGetItem = "";
htmlEditor.getQuadResponse = function (qID, type) {
    var apiURL = htmlEditor.PXEmappedAPI.quadAPI;
    //debugger;
    var param = {
        'repo_id': qID,
        'project_id': project_id,
        'project_type_id': project_type_id,
        'entity_type_id': type,
    };
    htmlEditor.dataPath = {"data": "", "path": ""};
    if (sessionStorage.getItem(qID)) {
        var qIDdata = JSON.parse(sessionStorage.getItem(qID));
        htmlEditor.dataPath.data = qIDdata.data;
        htmlEditor.dataPath.path = qIDdata.path;

    } else {
        jQuery.ajax({
            url: apiURL,
            async: false,
            method: 'GET',
            data: param,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data) {
                var json_value = data.json_value;
                var json_file_path = data.json_file_path;
                var quadJsonData = {'data': json_value, 'path': json_file_path};
                sessionStorage.setItem(qID, JSON.stringify(quadJsonData));
                var qIDdata = JSON.parse(sessionStorage.getItem(qID));
                htmlEditor.dataPath.data = qIDdata.data;
                htmlEditor.dataPath.path = qIDdata.path;
                qidGetItem = sessionStorage.getItem("quadIds");

                if (qidGetItem == null) {
                    qidGetItem = sessionStorage.setItem("quadIds", qID);
                } else {
                    var searchQid = qidGetItem.indexOf(qID);
                    if (searchQid == -1) {
                        qidGetItem += ',' + qID;
                        sessionStorage.setItem("quadIds", qidGetItem);
                    }
                }
            },
            error: function () {
                alert("Error occured");
            }

        });

    }
};

htmlEditor.enableSettingForObject = function (frame) {
    jQuery('#objectModal').modal('show');
    var oldurl = frame.attr("src");
    jQuery("#objectURL").val(oldurl);
    jQuery(".object_conf").off().on("click", function () {
        var newurl = jQuery("#objectURL").val().trim();
        frame.attr("src", newurl);
        frame.find(".url-link").text(newurl);
        jQuery('#objectModal').modal('hide');
    })
}

//Settings panel on clicking the edit button for external widgets.
htmlEditor.exwidgetSetting = function (frame) {
    //config page

    htmlEditor.thisFrame = frame;
    var theClasses = htmlEditor.thisFrame.attr("class") === undefined ? "" : htmlEditor.thisFrame.attr("class"),
            widgetType = htmlEditor.thisFrame.attr('widget-type'),
            configPath = "",
            configFrameEl = jQuery('#exwidgetModal').find('iframe'),
            configModalHeaderEl = jQuery('#exwidgetModal').find('#modalBoxTitle'),
            widget_id = htmlEditor.thisFrame.attr('widget-id');

    if (widget_id) {
        try {
            configPath = widgetScope.configPage[widget_id];
        }
        catch (exc) {
            console.log(exc);
        }

    }

    configFrameEl.attr('src', configPath); //Sets widgets dynamic frame path
    configModalHeaderEl.html('Edit ' + (widgetType.toUpperCase())); //Sets widgets modal header value
    jQuery("#exwidgetModal").modal("show");
    return;
//    var editPath = clientUrl + '/widgets/flashcard/config/edit.html'
    jQuery("#fcConf").attr("disabled", false);
    var path = thisFrame.attr("data-path");
    var height = thisFrame.height();
    var wide = thisFrame.width();
    jQuery("#exwidgetModal").modal("show");
    jQuery("#widgetHeight").val(height);
    jQuery("#widgetWidth").val(wide);
    jQuery("#add-fields").off().on("click", function () {
        var str = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled selected>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" disabled/></td><td><input type="text" class="form-control fc-desc" placeholder="Description" disabled/></td></tr>';
        jQuery("table#editFC").append(str);
    })
    if (path !== "undefined") {             //If the values are already saved once, show them to be edited.
        jQuery.ajax({
            url: path,
            async: false,
            method: 'GET',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data) {
                var valAsStr = data.replace("var ex_widgets = ", "");
                var semiparsed = JSON.parse(valAsStr);
                var realVal = JSON.parse(semiparsed);
                populateFields(realVal);
            },
            error: function () {
                alert("Error occured..Retry");
            }
        })

        //Populate options based on the response received
        function populateFields(dataPack) {
            var actName = dataPack.quiz[0].TemplateName;
            var flashes = dataPack.quiz[0].OptionStem;
            var str = "";
            var strGenerator = function (optiontype) {
                var nstr;
                switch (optiontype) {
                    case "text":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text" selected>Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><input type="text" class="form-control fc-value" placeholder="Enter Text" value="' + flashes[i].OptionValue + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;

                    case "image":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text">Text</option><option value="image" selected>Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><button class="fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" value="' + flashes[i].OptionValue + '" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" value="' + flashes[i].OptionTitle + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;

                    case "video":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video" selected>Video</option><option value="audio">Audio</option></select></td><td><button class="fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" value="' + flashes[i].OptionValue + '" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" value="' + flashes[i].OptionTitle + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;

                    case "audio":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio" selected>Audio</option></select></td><td><button class="fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" value="' + flashes[i].OptionValue + '" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" value="' + flashes[i].OptionTitle + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;
                }
                return nstr;
            }
            for (var i = 0; i < flashes.length; i++) {
                str += strGenerator(flashes[i].OptionDataType);
            }
            jQuery("table#editFC").html(str);
        }
    }
    else {
        var str = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled selected>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" disabled/></td><td><input type="text" class="form-control fc-desc" placeholder="Description" disabled/></td></tr>';
        jQuery("table#editFC").html(str);
    }


//    jQuery("#exwidgetModal").modal("show");
    var id = thisFrame.attr("id");
    var source = clientUrl + "/widgets/flashcard/index.html";
    thisFrame.attr("src", source);
//    var editContainer = jQuery("#exwidgetModal").find(".editWidgetOptions");
//    editContainer.load(editPath, function() {

    //To be executed on successfully saving the widget data
    function successHandler(data) {
        jQuery("#exwidgetModal").modal("hide");
        thisFrame.attr("data-path", data.json_file_path);
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents())
    }

    //Call the api to save the changes made
    function saveWidgetData(widgetCont, repoId) {
        var param = {"project_id": project_id, "project_type_id": project_type_id, "ex_widgets": widgetCont, "repo_id": repoId};
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.saveWidgetDataAPI,
            async: false,
            method: 'POST',
            data: param,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data) {
                successHandler(data);
            },
            error: function () {
                alert("Error occured..Retry");
            }
        })
    }

    //On hitting the confirm Button
    jQuery("#fcConf").off().on("click", function () {
        jQuery(this).attr("disabled", true);
        var arrOfInputs = [];
        jQuery("table#editFC").find("tr").each(function () {         //Creat the object that is to be saved.
            var obj = {};
            var type = jQuery(this).find(".fc-type").val();
            try {
                var title = jQuery(this).find(".fc-title").val().trim();
            }
            catch (exc) {

            }
            var valueFC = jQuery(this).find(".fc-value").val().trim();
            var desc = jQuery(this).find(".fc-desc").val().trim();
            obj.OptionDataType = type;
            obj.OptionTitle = title;
            obj.OptionValue = valueFC;
            obj.OptionDescription = desc;
            arrOfInputs.push(obj);
        });

        var ht = jQuery("#widgetHeight").val().trim();
        var wid = jQuery("#widgetWidth").val().trim();
        thisFrame.css("height", ht);
        thisFrame.css("width", wid);

        var dataToBeSent = {
            "quiz": [{
                    "TemplateName": "FlashCard",
                    "TemplateType": "FlashCard",
                    "QuestionId": id,
                    "QuestionText": "Question text goes here",
                    "OptionStem": arrOfInputs
                }]
        }
        var dataAsStr = JSON.stringify(dataToBeSent);
        saveWidgetData(dataAsStr, id);
    })
//    });
}


//    Change the input types depending on selection
htmlEditor.changeInputType = function (thisSel) {
    var type = thisSel.value;
    switch (type) {
        case "text":
            forTextType();
            break;

        case "image":
            forImageType();
            break;
    }

    function forTextType() {
        var html = '<td><input type="text" class="form-control fc-value" placeholder="Enter Text"/></td>';
//        jQuery(thisSel).parents("tr").find(".fc-title").parent().replaceWith(html);
        if (jQuery(thisSel).parents("tr").find(".fc-value").length) {
            jQuery(thisSel).parents("tr").find(".fc-value").parent("td").remove();
        }
        jQuery(thisSel).parents("tr").find(".fc-desc").attr("disabled", false);
        jQuery(thisSel).parents("tr").find(".fc-title").parent().replaceWith(html);
    }

    function forImageType() {
        var html = '<td><button class="btn btn-primary fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title"/></td>';
        if (jQuery(thisSel).parents("tr").find(".fc-title").length) {
            jQuery(thisSel).parents("tr").find(".fc-title").parent().replaceWith(html);
        }
        else {
            jQuery(thisSel).parents("tr").find(".fc-value").parent().replaceWith(html);
        }
        jQuery(thisSel).parents("tr").find(".fc-desc").attr("disabled", false);
    }

    jQuery(document).on("click", ".fc-asset-selector", function () {
        jQuery("#assetSelector").modal("show");
        var theValueField = jQuery(this).siblings(".fc-value");

        var num_of_items = 10;          //Set no. of items in a given page.
        getAllImages(1, num_of_items);
//Fetching the images from asset library. Similar to htmlhtmlEditor.callImages();
        function getAllImages(page, num_of_items) {
            var numPerPage = num_of_items;
            var params = {"pageNumber": page, "itemsPerPage": numPerPage};

            jQuery.ajax({
                url: htmlEditor.PXEmappedAPI.getImagesAPI,
                async: true,
                type: "GET",
                data: params, // object_type=104[image]
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function (data) {
                    populateImages(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Error occured while fetching Images")
                },
            });
        }


        function populateImages(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                var imgContainers = jQuery("#assetSelector").find(".inner-image-content");
                var str = "";
                jQuery("#assetSelector .paginate").show();
                for (var i = 0; i < d.length; i++) {
                    str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg" src="' + d[i].asset_location + '" alt=""/></div><div class="box-theme-title">' + d[i].original_name + '</div></div></div>';
                }
                str += '<div class="clearfix"></div>';
//                if (page == 1) {        //Pagination implementation
                var total_images = data.totalAssets;
                var max_page = Math.ceil(total_images / num_of_items);
                //Pagination using plugin.
                jQuery("#assetSelector .paginate").bootpag({
                    total: max_page, // total pages
                    page: 1, // default page
                    maxVisible: 10, // visible pagination
                    leaps: true         // next/prev leaps through maxVisible
                }).on("page", function (event, num) {
                    getAllImages(num, num_of_items);
                });
//                }
                imgContainers.html(str);

                var container = imgContainers;
                container.find('.box-theme').off("click").on("click", function () {
                    if (jQuery(this).hasClass('active')) {
                        jQuery(this).removeClass('active');
                        jQuery(this).find('.check-box').removeClass('checked');
                    }
                    else {
                        jQuery('.box-theme').removeClass('active');
                        jQuery(this).addClass('active');
                        jQuery(this).find('.check-box').addClass('checked');
                    }
                    var imgUrl = jQuery('.box-theme.active').find("img").attr("src");
                    if (imgUrl !== undefined) {
                        jQuery("#widgetConf").attr("disabled", false);
                        theValueField.val(imgUrl);
                        jQuery("#assetSelector").modal("hide");
                    }
                    else {
                        jQuery("#imgCont").attr("disabled", true);
                    }
                });
            }
            else {
//                if (page == 1)
                jQuery("#assetSelector .modal-content-container").html("<p>No assets Found. Upload some and retry.</p>");
                jQuery("#assetSelector .paginate").hide();
            }
        }
    })
}
htmlEditor.getWidgetType = function (cl) {
    var clList = [],
            wigdetType = "";
    try {
        clList = cl.split(' ');
    }
    catch (exc) {
        clList = cl;
    }
    //widgetClasses
    for (var ind = 0; ind < clList.length; ind++) {
        if (jQuery.inArray(clList[ind], widgetClasses) != -1) {
            wigdetType = clList[ind];
        }
    }

    if (!wigdetType.length)
        wigdetType = false;

    return wigdetType;
}