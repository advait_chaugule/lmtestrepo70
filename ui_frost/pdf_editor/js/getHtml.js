/******************************************************************/
/** JS to generate and Pdf editor - Kaushik Mitra **/
/** Dependency - jQuery.min.js**/
/******************************************************************/
var htmlEditor = {};
var tocCreatedFlag = false;
  htmlEditor.retagMode="disable_retagging";
htmlEditor.pdfMetaTagWrapper = {};
htmlEditor.selectedTextToHtml = function (e) {
    var ckEditorInstance = CKEDITOR.instances.editor1.getSelection();
    htmlEditor.selectedText = false;
    htmlEditor.startElementTag = "";
    htmlEditor.endElementTag = "";
    if (ckEditorInstance.getType() === CKEDITOR.SELECTION_TEXT && ckEditorInstance.getSelectedText() !== "") {
        htmlEditor.selectedText = true;
        htmlEditor.startElementTag = jQuery(ckEditorInstance.getRanges()[0].startContainer.$).is('body') ? ckEditorInstance.getRanges()[0].startContainer.$.firstChild : jQuery(ckEditorInstance.getRanges()[0].startContainer.$.parentNode).is('body') ? ckEditorInstance.getRanges()[0].startContainer.$ : jQuery(ckEditorInstance.getRanges()[0].startContainer.$).is('figure') ? ckEditorInstance.getRanges()[0].startContainer.$ : ckEditorInstance.getRanges()[0].startContainer.$.parentNode;
        htmlEditor.endElementTag = jQuery(ckEditorInstance.getRanges()[0].endContainer.$).is('body') ? ckEditorInstance.getRanges()[0].endContainer.$.firstChild : jQuery(ckEditorInstance.getRanges()[0].endContainer.$).is('figure') ? ckEditorInstance.getRanges()[0].endContainer.$ : ckEditorInstance.getRanges()[0].endContainer.$.parentNode;
    } else if (ckEditorInstance.getType() === CKEDITOR.SELECTION_ELEMENT) {
        if (jQuery(ckEditorInstance.getSelectedElement().$).is('img')) {
            htmlEditor.selectedText = true;
            htmlEditor.startElementTag = htmlEditor.endElementTag = ckEditorInstance.getSelectedElement().$;
        } else {
            jQuery('#htmltagModal').hide();
        }
    }
    // if (typeof ckEditorInstance.getSelection().getSelectedText() != "undefined") {
    //     htmlEditor.selectedText = ckEditorInstance.getSelection().getSelectedText();
    //     //htmlEditor.startElementTag = ckEditorInstance.getSelection().getStartElement().$;
    //     htmlEditor.startElementTag = jQuery(ckEditorInstance.getSelection().getRanges()[0].startContainer.$).is('body') ? ckEditorInstance.getSelection().getRanges()[0].startContainer.$.firstChild : jQuery(ckEditorInstance.getSelection().getRanges()[0].startContainer.$.parentNode).is('body') ? ckEditorInstance.getSelection().getRanges()[0].startContainer.$ : ckEditorInstance.getSelection().getRanges()[0].startContainer.$.parentNode;
    //     htmlEditor.endElementTag = jQuery(ckEditorInstance.getSelection().getRanges()[0].endContainer.$).is('body') ? ckEditorInstance.getSelection().getRanges()[0].endContainer.$.firstChild : ckEditorInstance.getSelection().getRanges()[0].endContainer.$.parentNode;
    //     // ckEditorInstance.getSelection().getRanges()[0].endContainer.$.parentNode;
    // }
    // else if (typeof document.selection != "undefined" && document.selection.type == "Text") {
    //   text = document.selection.createRange().text;
    // }
    if (htmlEditor.selectedText) {
        // jQuery('#htmltagModal').show().css({ 'position': 'absolute', "left": e.pageX + 180, "top": e.pageY + 75, });
        jQuery('#htmltagModal').show();
    }
};
htmlEditor.populateTagList = function () {
    var i, serviceContent;
    jQuery.when(serviceContent = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.pdfMetaTag)).done(function () {
        var pdfMetaTagObj = serviceContent.responseJSON.data,
            taglistName = pdfMetaTagObj.list,
            html = '<option value="dummy">Select HTML option</option>';
        for (i = 0; i < taglistName.length; i++) {
            html += '<option value="' + taglistName[i].element_name + '">' + taglistName[i].element_name + '</option>';
            htmlEditor.pdfMetaTagWrapper[taglistName[i].element_name] = taglistName[i].element_value;
        }
        jQuery('#htmltagModal').empty().html(html);
        jQuery('.tagList').off('change').on('change', function () {
            var selectedOption = jQuery('.tagList :selected').text();
            jQuery('#htmltagModal').hide();
            htmlEditor.selectedText = false;
            switch(htmlEditor.retagMode){
                case 'enable_retagging':
                    htmlEditor.retag(selectedOption);
                    break;
                default:
                    htmlEditor.wrapWithElement(selectedOption);
                    break;
                    
            }
//            htmlEditor.wrapWithElement(selectedOption);
            jQuery(".tagList").each(function () {
                this.selectedIndex = 0;
            });
        });
    });
};
htmlEditor.toggleTagMode=function(){
    htmlEditor.retagMode=jQuery("#tagMode option:selected").val();
}
htmlEditor.removeTag=function(){
           var elmIds=htmlEditor.getElmId();
     if (elmIds.idOfStart === elmIds.idOfEnd) {
        $(".cke_wysiwyg_frame").contents().find('#' + elmIds.idOfStart).unwrap();
    } else {
        var nextUntilSelector = $(".cke_wysiwyg_frame").contents().find("#" + elmIds.idOfEnd);
     $(".cke_wysiwyg_frame").contents().find('#' + elmIds.idOfStart).nextUntil(nextUntilSelector).andSelf().next().andSelf().unwrap();
    }
    }

htmlEditor.retag=function(selectedOption){
htmlEditor.removeTag();
htmlEditor.wrapWithElement(selectedOption);
    
}
htmlEditor.wrapWithElement = function (tagSelection) {
    var elmIds=htmlEditor.getElmId();
     if (elmIds.idOfStart === elmIds.idOfEnd) {
        $(".cke_wysiwyg_frame").contents().find('#' + elmIds.idOfStart).wrap(htmlEditor.pdfMetaTagWrapper[tagSelection]);
    } else {
        var nextUntilSelector = $(".cke_wysiwyg_frame").contents().find("#" + elmIds.idOfEnd);
     $(".cke_wysiwyg_frame").contents().find('#' + elmIds.idOfStart).nextUntil(nextUntilSelector).andSelf().next().andSelf().wrapAll(htmlEditor.pdfMetaTagWrapper[tagSelection]);
    }
    flag = true;
    var content = jQuery(".cke_wysiwyg_frame").contents().find("*"); //get the content
    htmlEditor.generateIds(content);
};
htmlEditor.bindEventsToCk = function () {
    jQuery(".cke_wysiwyg_frame").contents().off('mouseup').on('mouseup', function (e) {
        jQuery('#htmltagModal').hide();
        htmlEditor.selectedText = false;
        htmlEditor.selectedTextToHtml(e);
    });
    //To enable the functionality of selecting all the text using ctrl+a
    jQuery(".cke_wysiwyg_frame").contents().off("keydown").on("keydown", function (eventObj) {
        if (eventObj.ctrlKey) {
            if (eventObj.keyCode == 65) {
                htmlEditor.startElementTag = jQuery(".cke_wysiwyg_frame").contents().find('body')[0].firstChild;
                htmlEditor.endElementTag = jQuery(".cke_wysiwyg_frame").contents().find('body')[0].lastChild;
                htmlEditor.selectedText = true;
                jQuery('#htmltagModal').show();
            }
        }
    });
};
 htmlEditor.getElmId=function(){
       var idOfStart = jQuery(htmlEditor.startElementTag).closest('p').length !== 0 ? jQuery(htmlEditor.startElementTag).closest('p').attr('id') : jQuery(htmlEditor.startElementTag).closest('section').length !== 0 ? jQuery(htmlEditor.startElementTag).closest('section').attr('id') : jQuery(htmlEditor.startElementTag).closest('figure').length !== 0 ? jQuery(htmlEditor.startElementTag).closest('figure').attr('id') : jQuery(htmlEditor.startElementTag).attr('id'),
        idOfEnd = jQuery(htmlEditor.endElementTag).closest('p').length !== 0 ? jQuery(htmlEditor.endElementTag).closest('p').attr('id') : jQuery(htmlEditor.endElementTag).closest('section').length !== 0 ? jQuery(htmlEditor.endElementTag).closest('section').attr('id') : jQuery(htmlEditor.endElementTag).closest('figure').length !== 0 ? jQuery(htmlEditor.endElementTag).closest('figure').attr('id') : jQuery(htmlEditor.endElementTag).attr('id');
     return {
         idOfStart:idOfStart,
         idOfEnd:idOfEnd
     };
 };

htmlEditor.generateIds = function (elem) {
    var numOfelem = elem.length;
    for (var i = 0; i < numOfelem; i++) {
        var checkForId = elem[i].hasAttribute("id") && elem[i].id.length; //if id against this element exists
        var checkNATag = elem[i].tagName === "A" || elem[i].tagName === "STRONG" || elem[i].tagName === "EM" || elem[i].tagName === "SUB" || elem[i].tagName === "SUP" || elem[i].tagName === "U" || jQuery(elem[i]).hasClass('math-tex') ? true : false;
        var checkGadget = jQuery(elem[i]).parents(".converted").length ? true : false; //If placeholder
        if (!(checkForId || checkNATag || checkGadget)) {
            elem[i].id = guid()();
        }
    }
};
htmlEditor.ajaxCalls = function (url) { //Make all ajax calls using this function
    return jQuery.ajax({
        url: url,
        async: true,
        beforeSend: function (xhr) {
            //            htmlEditor.createLoader("editor");
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Unable to fetch the data. " + textStatus);
            window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
    });
};
htmlEditor.getJsonValue = function () { // Fetch the content to be displayed.
    var editorContent;
    jQuery.when(editorContent = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.getContentAPI)).done(function () {
        var contObj = editorContent.responseJSON;
        var content = contObj.data.description;
        htmlEditor.objStatus = contObj.data.progress_status;
        htmlEditor.islocked = contObj.data.is_locked;
        user_id = contObj.data.user_id;
        htmlEditor.editorTOC = contObj.data.editor_toc;
        var lockedbyuser = contObj.data.locked_by_user;
        jQuery('.e-block-title').html(contObj.data.node_title);
        htmlEditor.pdfStatus();
        // if (lockedbyuser !== undefined) {
        //     if (user_id === lockedbyuser || user_id === 1) { // 1 = admin
        //         if (htmlEditor.islocked) {
        //             jQuery("#lockPage").find(".fa-unlock").removeClass("fa-unlock").addClass("fa-lock");
        //             htmlEditor.islocked = true;
        //         }
        //     }
        //     else {
        //         window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        //     }
        // }

        //        var current_node = contObj.data.object_location;        //Holds the section Id current node maps to.
        //        var section_id = current_node.split("#")[1];
        htmlEditor.current_node = section_id;

        htmlEditor.cssFiles = contObj.data.theme_location;
        htmlEditor.glossaryFile = contObj.data.glossary_file_name;
        try {
            var validation_response = JSON.parse(contObj.data.validation_response);
            // htmlEditor.showValidationContent(validation_response);
        } catch (exc) {
            jQuery(".error-count").hide();
        }
        // htmlEditor.initiateEditor();
        //CKEDITOR.on('instanceReady', function (ev) {
        tocCreatedFlag = false;
        htmlEditor.setData(content);
        jQuery(".cke_wysiwyg_frame").attr("id", "editor-frame");


        // Enable drag drop and other functionalities after redo/undo.
        //CKEDITOR.instances.editor1.on('afterCommandExec', handleAfterCommandExec);
        // function handleAfterCommandExec(event)
        // {
        //     var commandName = event.data.name;
        //     // For 'redo/ undo' commmand
        //     if (commandName === 'redo' || commandName === 'undo')
        //         htmlEditor.bindEvents();

        //     if (commandName === 'source') {

        //         //Change the arrow direction of the button
        //         jQuery(".lm-edit-more").removeClass("active").find("span.fa").removeClass("fa-chevron-right");
        //         jQuery(".lm-edit-more").parents(".lm-edt-pane").removeClass("active").attr({'disabled': 'disabled'});

        //         htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
        //         var oFrameElm = jQuery(".cke_wysiwyg_frame"),
        //                 isPreviewMode = oFrameElm.length;
        //         if (isPreviewMode) {
        //             setTimeout(function () {
        //                 jQuery(".lm-edit-more").parents(".lm-edt-pane").removeAttr('disabled');
        //                 //htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
        //                 htmlEditor.bindIframeDragDrop();
        //                 htmlEditor.convertFrameToDiv();
        //             }, 100)
        //         }
        //         htmlEditor.convertFrameToDiv();
        //         htmlEditor.bindEvents();
        //         //alert(jQuery(".cke_wysiwyg_frame").contents().find("body").find('*').length)
        //         /*jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
        //          htmlEditor.bindIframeDragDrop();console.log()
        //          htmlEditor.disableToolbar();*/
        //     }
        // }

        // CKEDITOR.instances.editor1.on('beforeCommandExec', handleBeforeCommandExec);
        // function handleBeforeCommandExec(event)
        // {
        //     var cmdName = event.data.name;
        //     // For 'source' commmand
        //     if (cmdName === 'source') {
        //         htmlEditor.convertDivToFrame(jQuery(".cke_wysiwyg_frame").contents().find("body"));
        //         htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents().find("body"));
        //     }
        // }
        // htmlEditor.aOnclickValueArr = [];
        // jQuery('a.cke_button').each(function () {
        //     htmlEditor.aOnclickValueArr.push(jQuery(this).attr("onclick"));
        // });
        // });
        var projectName = contObj.data.object_name;
        var chapName = contObj.data.node_title;
        jQuery("#node_title").html(chapName);
        jQuery("#project_name").html(projectName);
        $('#pdfStatus option[value="' + contObj.data.progress_status + '"]').prop('selected', 'selected');
    });
};
htmlEditor.setData = function (content) {
    // Set the fetched content in the editor.
    var cloneFrame = jQuery("#test_pdf");
    cloneFrame.hide();
    cloneFrame.html(content);
    // htmlEditor.imageTimeStamp(cloneFrame, true);
    content = cloneFrame.html();
    CKEDITOR.instances.editor1.setData(content);
    cloneFrame.empty();
    var frame = jQuery("iframe.cke_wysiwyg_frame");
    var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
    var last_users = "";
    //Initiate track for a concurrent user.
    var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=INSERT", {
        withCredentials: true
    });
    var listener = function (event) {
        //console.log(event.data);
        var user_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id"); //Get current user version.
        var resp = JSON.parse(event.data);
        var userlist = resp.users;
        var latest_version = resp.latest_version_id;
        var users = userlist.join();
        if (userlist.length) {
            htmlEditor.allowLocking = false;
            jQuery(".concurrent-info").removeClass("hideElm").find("span.con-user").text(users);
            //Check if a merge option needs to be provided to the user.
            if (user_ver !== latest_version && !htmlEditor.manualSelection) {
                htmlEditor.needsToBeMerged = true;
            } else {
                htmlEditor.needsToBeMerged = false;
            }

            if (htmlEditor.manualSelection && user_ver !== latest_version) {
                htmlEditor.createVersion = false;
            } else {
                htmlEditor.createVersion = true;
            }
            //            }
        } else {
            htmlEditor.allowLocking = true;
            jQuery(".concurrent-info").addClass("hideElm");
            if (user_ver !== latest_version && htmlEditor.manualSelection) {
                htmlEditor.createVersion = false;
            }
        }
        if (htmlEditor.allowLocking) {
            jQuery("#lockPage").attr("disabled", false);
        } else {
            jQuery("#lockPage").attr("disabled", true);
        }
        if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').children().length > 0 && tocCreatedFlag === false) {
            jQuery("iframe.cke_wysiwyg_frame").contents().find('body').createTOC();
            tocCreatedFlag = true;
        }
        jQuery(".loading-g").hide();
    };
    es.addEventListener("message", listener);
    // htmlEditor.currContent = htmlEditor.getLatestSavedContent();
    htmlEditor.bindEventsToCk();
    //=====================================
    setTimeout(function () {
        htmlEditor.currContent = htmlEditor.getLatestSavedContent();
    }, 1000);

    // jQuery.when((function () {
    //     setTimeout(function () {
    //         htmlEditor.currContent = htmlEditor.getLatestSavedContent();
    //         try {
    //             var posOfNode = frame.contents().find("#" + htmlEditor.current_node).offset().top;  //Get offset of the node section in the current content.
    //             frame.contents().scrollTop(posOfNode);              //Focus the content according to the section id.
    //         }
    //         catch (exc) {
    //             frame.contents().scrollTop(0);
    //         }
    //     }, 1000);

    //     frame.contents().find('body').createTOC();
    // })()).done(function () {
    //     try {
    //         htmlEditor.convertFrameToDiv();

    //     }
    //     catch (exc) {
    //     }
    //     htmlEditor.bindEvents();

    //     //If widget overlay is not created due to slow API response
    //     var framesUnconvertedLength = frame.contents().find('body').find('iframe').length;
    //     var frameConvertChecker = function () {
    //         if (framesUnconvertedLength) {
    //             htmlEditor.convertFrameToDiv();
    //             htmlEditor.bindEvents();
    //             framesUnconvertedLength = frame.contents().find('body').find('iframe').length;
    //             frameConvertChecker();
    //         }
    //         else
    //             return false;
    //     };
    //     frameConvertChecker();
    //     /**/
    // });
    //Store the content to map any changes in the content.
};
htmlEditor.pdfStatus = function () {
    var serviceContent;
    jQuery.when(serviceContent = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.objectStatus)).done(function () {
        var contObj = serviceContent.responseJSON.data;
        var option = '';
        delete contObj["1098"];
        for (var key in contObj) {
            if (contObj.hasOwnProperty(key)) {
                option += '<option value="' + key + '">' + contObj[key] + '</option>';
            }
        }
        jQuery('#pdfStatus').html(option);
    });
};
htmlEditor.saveContent = function (isVersion) {
    htmlEditor.webWorker(1,isVersion); // pass true to create version on saving.
    //jQuery("#save_toc_id").button('loading');
    tocCreatedFlag = false
    if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').children().length > 0 && tocCreatedFlag === false) {
        jQuery("iframe.cke_wysiwyg_frame").contents().find('body').createTOC();
        tocCreatedFlag = true;
    }
    //alert('content saved');
};
setInterval(function(){
    
htmlEditor.saveContent(1)}, 10*60*1000); // Time in milliseconds
htmlEditor.discardContent = function () {
    htmlEditor.getJsonValue();
    alert('content discarded');
};
htmlEditor.getContent = function () {
    var chapToBeSaved = CKEDITOR.instances.editor1.getData(); //+ htmlEditor.extraContents;
    //     var trialFrame = jQuery(".editor-container5 iframe#trial_load").contents();
    //     trialFrame.find("body").html(chapToBeSaved);
    //     htmlEditor.removeUnwanteds(trialFrame); // Remove classes & elements before saving.
    //     htmlEditor.convertDivToFrame(trialFrame);
    //     htmlEditor.imageTimeStamp(trialFrame.find("body"), false);
    //     var toBeSaved = trialFrame.find("body").html();
    // //    htmlEditor.setData(toBeSaved);
    // //    toBeSaved = CKEDITOR.instances.editor1.getData();
    // //    htmlEditor.convertFrameToDiv();
    //     trialFrame.find("body").empty();
    return chapToBeSaved;
};
htmlEditor.webWorker = function (showresponse, versioning, version_id) {
    //     htmlEditor.backToDashboard();
    //     htmlEditor.backToDashboardFlag=false;
    // }
    var w;
    if (typeof (Worker) !== "undefined") { // Check if webworker is supported.
        if (typeof (w) == "undefined") {
            w = new Worker("js/save_at_back.js");
        }
        var values = {},
            progress_status = jQuery('#pdfStatus').find(":selected").val();
        values.url = htmlEditor.PXEmappedAPI.saveContentAPI;
        versioning = versioning ? versioning : "";
        version_id = version_id ? version_id : "";
        values.data = {
            content: htmlEditor.getContent(),
            node_id: node_id,
            project_id: project_id,
            node_title: node_title,
            versioning: versioning,
            version_id: version_id,
            progress_status: progress_status
        };
        w.postMessage(values); // Send data value to the webworker to save it.
        //Check if response needs to be rendered to the user after successful save.
        if (showresponse) {
            w.onmessage = function (event) {
                htmlEditor.currContent = htmlEditor.getLatestSavedContent();
                if(versioning!==1){
                       alert('content saved');
                }
             
                var e = JSON.parse(event.data);
                var new_content = e.data.description;
                try {
                    if (jQuery("#saveModal")) {
                        jQuery("#saveModal").modal("hide");
                        jQuery("#save_toc_id").button('reset');

                    }
                    //htmlEditor.setData(new_content);
                    //htmlEditor.disableSave();
                } catch (exc) {

                }

                try { //Give option to post a saving note while hitting save button
                    var save_note = jQuery("#savingnote:visible").val().trim(); //get the note 
                    var dataForSavingNotes = {
                        version_id: e.data.version_id,
                        comment: save_note
                    }
                    jQuery.ajax({
                        url: htmlEditor.PXEmappedAPI.saveSavingNote,
                        type: 'POST',
                        data: dataForSavingNotes,
                        xhrFields: {
                            withCredentials: true
                        },
                        crossDomain: true,
                        async: true,
                        success: function (data, textStatus, jqXHR) {
                            jQuery("#savingnote").val("");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("There was some error posting the comment");
                        }
                    });
                } catch (exc) {

                }
                // htmlEditor.getAllVersions();
                // htmlEditor.editorEvents();
                // jQuery("#save_toc_id").button('reset');
                //                htmlEditor.validateContent();
                // if (htmlEditor.createVersion) {                 //If new version is created, fetch all the version list.
                //     htmlEditor.createVersion = false;
                //     htmlEditor.autoMerge = false;
                // }
                // jQuery("#savingnote").val("");
                // jQuery("#saveModal").modal("hide");
                // htmlEditor.mathMlEdited = false;
            };
        }
    } else {
        alert("Sorry! No Web Worker support.");
    }
};
htmlEditor.setContentWindowHeight = function () {
    var win_height = jQuery(window).height(),
        header_height = jQuery('.e-block-heading').outerHeight(),
        footer_height = jQuery('.cke_bottom.cke_reset_all').outerHeight(),
        toolbar_ht = jQuery(".cke_top.cke_reset_all").outerHeight(),
        editor_height = win_height - (header_height + footer_height + toolbar_ht + 25),
        left_pane_height = (win_height) - (header_height) - 10;

    jQuery(".cke_contents").css("height", editor_height + "px");
    jQuery(".e-left-panel").css("height", left_pane_height + "px");
};
//var editor_patterns;
// Function to remove unwanted elements & classes from a given frame.
htmlEditor.removeUnwanteds = function (frame) {
    var removeClasses = ["indicator", "highlightSelected", "highlight_on_drop", "highlight"]; // Add classes thats not required in the mode
    var removeElems = ["toolBox", "descOfTarget", "iframe-setting", "image-setting"]; // Add classes of the elements that needs to be removed.

    for (var i = 0; i < removeClasses.length; i++) { // Removes the unwanted classes from the given frame.
        frame.find("." + removeClasses[i]).removeClass(removeClasses[i]);
    }

    for (var k = 0; k < removeElems.length; k++) { // Removes unwanted elements from the given frame.
        frame.find("." + removeElems[k]).remove();
    }
    htmlEditor.removeFrameSettings();
};
// Function to populate metaclasses and highligh selection inside the editor.
// htmlEditor.populateMeta = function () {
//     htmlEditor.metaArr = {};
//     htmlEditor.topLevelPatternClass = new Array();
//     jQuery(editor_patterns).each(function (i, v) {
//         if (v.meta_class_options != undefined) {
//             var metaData = JSON.parse(v.meta_class_options);
//         }
//         jQuery(metaData).each(function (i1, v1) {
//             var key = v1.default_class;
//             htmlEditor.topLevelPatternClass.push(v1.default_class);
//             htmlEditor.metaArr[key] = new Array();
//             jQuery(v1.pattern_classes).each(function (i2, v2) {
//                 var val = v2.value;
//                 if (val.length > 0) {
//                     htmlEditor.metaArr[key].push(val);
//                     htmlEditor.topLevelPatternClass.push(val);
//                 }
//             });
//         });
//     });
//     htmlEditor.topLevelPatternClass.push('converted gadget');
//     htmlEditor.getCurrentPattern();
// };

htmlEditor.getCurrentPattern = function () {
    var metaArr = htmlEditor.metaArr;
    var pattClsLst = htmlEditor.topLevelPatternClass;

    var tagListToHighlight = ['SECTION', 'ASIDE', 'OL', 'UL', 'BLOCKQUOTE'];
    var tagListToHide = ['HEADER', 'P', 'SPAN', 'STRONG', 'EM', 'I', 'U', 'IMG', 'TD', 'TBODY', 'TR', 'TABLE'];
    jQuery('.cke_wysiwyg_frame').contents().find("body").off('click').on('click', "*", function (e) {
        /*if (jQuery('.cke_editable').find('.chapter').length === 0)
         return;*/
        if (e.target.tagName === "IMG" && !e.target.classList.contains('cke_reset')) {
            htmlEditor.showImageOptions(e.target)
            return false;
        }
        jQuery('.cke_button__link').removeClass('cke_button_on');

        function settingOverlay(this_frm) { // Function to show setting options for videos & gadget pattern.
            e.stopPropagation();
            var this_ifrm = this_frm;
            htmlEditor.showSettingForFrame(this_ifrm);
        }

        if (jQuery(this).parents(".converted").length || jQuery(this).hasClass("converted")) {
            var converted_elem = jQuery(this).hasClass("converted") ? jQuery(this) : jQuery(this).parents(".converted");
            settingOverlay(converted_elem);

        } else {
            var thisElm = jQuery(this);
            var tagName = thisElm[0].tagName;
            if (tagName === 'A') {
                jQuery('.cke_button__link').addClass('cke_button_on');
            } else {
                //jQuery('.cConfig').remove();
                //thisElm.append('<button class="cConfig" onClick="htmlEditor.classConfig(this)">+</button>')
                //thisElm.append('<button class="cConfig" onClick="htmlEditor.classConfig(this)">+</button>')
            }


            var tagElmToHighlight = '';

            function classNotDefinedChecker(this_elm) {
                var clsName = this_elm.attr('class');
                var tagName = this_elm[0].tagName;
                try {
                    if (tagName === 'HEADER') {
                        return false;
                    } else if (jQuery.inArray(tagName, tagListToHighlight) !== -1) {
                        return true;
                    } else {
                        var allClasses = clsName.split(' ');
                        for (var i = 0; i < allClasses.length; i++) {
                            if (jQuery.inArray(allClasses[i], pattClsLst) !== -1) {
                                return true;
                            }
                            break;
                        }
                    }
                } catch (exc) {
                    if (jQuery.inArray(tagName, tagListToHighlight) !== -1) {
                        tagElmToHighlight = this_elm;
                    }
                    return jQuery.inArray(clsName, pattClsLst) !== -1 && this_elm[0].tagName !== 'HEADER' ? true : false;
                }
            }

            function isTrueClass(arr) {
                for (var i = 0; i < arr.length; i++) {
                    var key = arr[i];
                    if (metaArr[key] !== undefined)
                        return metaArr[key]
                    else
                        return false;
                }
            }
            try {
                var this_class = false;
                this_class = classNotDefinedChecker(thisElm);
                while (!this_class) {
                    thisElm = thisElm.parent();
                    if (thisElm[0].tagName === 'BODY')
                        break;
                    this_class = classNotDefinedChecker(thisElm);
                }
                if (this_class) {
                    var classArr = thisElm.attr('class').split(' ');
                    this_class = isTrueClass(classArr);
                }

            } catch (exc) {}




            function setSettings(elm) {
                var tagName = elm[0].tagName;
                //
                /*if(jQuery.inArray(tagName, tagListToHide) !== -1){
                 (elm.parent()).trigger('click');
                 }
                 else{*/
                jQuery('.highlight_on_drop').removeClass('highlight_on_drop');
                var className = (elm[0].className).split('highlightSelected')[0];
                jQuery('.meta-dropdown').removeClass('open');
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
                elm.addClass('highlightSelected');
                if (elm.hasClass("row")) {
                    elm.append("<span class='descOfTarget' contenteditable='false'>" + tagName + " " + className + "</span><div class='toolBox' contenteditable='false'><span class='glyphicon glyphicon-trash tool' title='Delete'></span><span class='glyphicon glyphicon-cog tool' title='Settings'></span></div>");
                } else {
                    elm.append("<span class='descOfTarget' contenteditable='false'>" + tagName + " " + className + "</span><div class='toolBox' contenteditable='false'><span class='glyphicon glyphicon-trash tool' title='Delete'></span></div>");
                }
                actionOnSmartClick();
                //}

            }

            function actionOnSmartClick() {
                //                jQuery(".cke_wysiwyg_frame").contents().find("body").find('.glyphicon-trash').off('click').on('click', function() {
                //                    var oHighlightedElm = jQuery(".cke_wysiwyg_frame").contents().find("body").find('.highlightSelected');
                //                    oHighlightedElm.deleteTOCNode();
                //                    /*if(oHighlightedElm.hasClass("chapter")){
                //                     jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                //                     htmlEditor.bindIframeDragDrop();
                //                     htmlEditor.disableToolbar();
                //                     }*/
                //                    oHighlightedElm.remove();
                //                    htmlEditor.removeFrameSettings();
                //                    htmlEditor.bindIframeDragDrop();
                //                });

                jQuery(".cke_wysiwyg_frame").contents().find("body").find(".tool").off("click").on("click", function () {
                    var oHighlightedElm = jQuery(".cke_wysiwyg_frame").contents().find("body").find('.highlightSelected');
                    var parId = oHighlightedElm.parents("[id]").attr("id");
                    var gridDetails = {};
                    gridDetails.classList = [];
                    gridDetails.elements = [];
                    if (jQuery(this).hasClass("glyphicon-trash")) {
                        oHighlightedElm.deleteTOCNode();
                        /*if(oHighlightedElm.hasClass("chapter")){
                         jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                         htmlEditor.bindIframeDragDrop();
                         htmlEditor.disableToolbar();
                         }*/
                        oHighlightedElm.remove();
                        if (jQuery("#sourceVal:visible").length) {
                            splitview.updateCode(parId); //remove the code from the split source
                        }
                        htmlEditor.removeFrameSettings();
                        htmlEditor.enableSave();

                    } else {
                        populateboxes(oHighlightedElm); //populate initial values
                        jQuery("#setRowsModal").find(".num-of-grid").on("change", function () {
                            generateBoxForGrids(); //create textboxes for input
                        });
                        jQuery("#setRowsModal").find(".setting_conf").off("click").on("click", function () {
                            setClasses(oHighlightedElm); //set the new classes.
                        });
                    }

                    //                    function to populate the details in the popup
                    function populateboxes(elem) {
                        var row = elem;
                        var toSearch = "col-";
                        gridDetails.rowNums = sendNums(row, toSearch, 9); //9 is the length of 'col-md-xx';
                        //                        debugger;
                        jQuery("#setRowsModal").find(".num-of-grid").find("option[value='" + gridDetails.rowNums.length + "']").attr("selected", true);
                        jQuery("#setRowsModal").modal("show");
                        generateBoxForGrids("init"); //send init to reflect the last value used
                    }

                    //                    function to remove the concerned class that matches reg ex
                    function sendNums(row, toSearch, length) {
                        var arr = [];
                        var regEx = new RegExp(toSearch, "g");
                        jQuery(row).children("section").each(function () {
                            var thisElm = jQuery(this);
                            var classes = thisElm.attr("class");
                            var res = classes.match(regEx);
                            if (res) {
                                var classIndex = classes.search(toSearch);
                                if (classIndex > -1) {
                                    var rowClass = jQuery.trim(classes.substr(classIndex, length));
                                    gridDetails.classList.push(rowClass); //Push the class name to be replaced later
                                    var number = rowClass.split("-")[2];
                                    arr.push(number);
                                }
                                gridDetails.elements.push(thisElm);
                            }
                        });
                        return arr;
                    }

                    //                    function to generate the reuired number of textboxes
                    function generateBoxForGrids() {
                        var numOfGrids = parseInt(jQuery("#setRowsModal").find(".num-of-grid").val());
                        var classVal = 12 / numOfGrids;
                        var str = '';
                        for (var i = 0; i < numOfGrids; i++) {
                            str += '<div class="col-md-' + classVal + '"><input name="term" type="number" class="form-control  grid-vals" required="" min="1" max="12" step="1" ';
                            if (arguments[0] == "init") { //If settings clicked, reflect values used
                                str += 'value="' + gridDetails.rowNums[i] + '"';
                            }
                            str += '></div>';
                        }
                        jQuery(".val-for-grids").html(str);
                        jQuery("#setRowsModal").on("keypress", "[type='number']", function (evt) {
                            evt.preventDefault();
                        });
                    }

                    //                    function to set the classes according to the input
                    function setClasses(elem) {
                        var row = elem;
                        var newClasses = [];
                        var vals = [];
                        var total = 0;
                        var valid = true;
                        jQuery("#setRowsModal").find(".grid-vals").each(function () { //check if inputs are valid
                            var thisVal = jQuery(this).val();
                            if (thisVal && thisVal < 12) { //Value cannot be equal to 12 as atleast 2 rows are required
                                var val = parseInt(thisVal);
                                vals.push(val);
                                var cls = "col-md-" + val; //Create class name from the input value
                                newClasses.push(cls);
                            } else {
                                valid = false;
                                return false;
                            }
                        })
                        if (valid) {
                            for (var i = 0; i < vals.length; i++) { //Calculate the sum of the values
                                total += vals[i];
                            }
                            // Check if summation =12, proceed with generating classes and replacinf them.
                            if (total === 12) {
                                if (newClasses.length >= gridDetails.elements.length) {
                                    for (var i = 0; i < newClasses.length; i++) {
                                        var theElm = row.find(gridDetails.elements[i]);
                                        if (theElm.length) {
                                            theElm.removeClass(gridDetails.classList[i]).addClass(newClasses[i]);
                                        } else {
                                            var newSection = '<section class="' + newClasses[i] + ' newlyadded"><p>Text goes here</p></section>';
                                            row.find(gridDetails.elements[i - 1]).after(newSection);
                                            gridDetails.elements[i] = row.find(".newlyadded").last();
                                        }
                                    }
                                } else {
                                    for (var i = 0; i < gridDetails.elements.length; i++) {
                                        if (newClasses[i] === undefined) {
                                            row.find(gridDetails.elements[i]).remove();
                                        } else {
                                            row.find(gridDetails.elements[i]).removeClass(gridDetails.classList[i]).addClass(newClasses[i]);
                                        }
                                    }
                                }
                                row.find(".newlyadded").removeClass("newlyadded");
                                jQuery("#setRowsModal").modal("hide"); //Close modal
                            } else if (total < 12 || total > 12) {
                                swal("Invalid Values!! The summation should be equal to 12");
                            } else {
                                swal("Invalid values");
                            }
                        } else {
                            swal("Invalid values"); //Invalid inputs. as in characters or number>=12
                        }

                        try {
                            htmlEditor.editorEvents(); //bind events to newly added elements
                        } catch (exc) {
                            console.log("Unable to bind events");
                        }
                    }
                    //                    htmlEditor.bindIframeDragDrop();
                })
            }

            function setMetaContent(data, elm) {
                if (elm.hasClass('chapter') || elm.hasClass('frontmatter') || elm.hasClass('backmatter') || elm.hasClass('bodymatter')) {
                    data = false;
                }

                if (!data) {
                    jQuery('.meta-dropdown .dropdown-menu').empty();
                    jQuery('#dropdownMenu1').attr('disabled', true);
                    return false;
                } else {
                    jQuery('#dropdownMenu1').removeAttr('disabled');
                }
                var innerHtml = '';
                var activeLength = 0;
                for (var i = 0; i < data.length; i++) {
                    var elm_class = elm.attr('class').split(' ');
                    if (data[i].length > 0) {
                        if (jQuery.inArray(data[i], elm_class) !== -1) {
                            innerHtml += '<li class="active" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        } else {
                            innerHtml += '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        }
                    }
                    if (data.length === 1)
                        jQuery('#dropdownMenu1').attr('disabled', true);
                }
                jQuery('.meta-dropdown .dropdown-menu li').remove();
                jQuery('.meta-dropdown .dropdown-menu').html(innerHtml);
                jQuery('.meta-dropdown .dropdown-menu li a').off('click').on('click', function (e) {
                    var activeLength = jQuery('.meta-dropdown').find('.active').length; // Check for the active Classes.
                    htmlEditor.contentUpdated = true;
                    var thisPar = jQuery(this).parent();
                    activeLength = jQuery('.meta-dropdown').find('.active').length;
                    var classToAdd = jQuery.trim(thisPar.text());
                    if (activeLength <= 0 && !thisPar.hasClass("active")) { //Activate class if not activated
                        jQuery(this).parent().addClass('active');
                        elm.addClass(classToAdd);
                        elm.trigger('click');
                    } else if (activeLength > 0) { //Toggle if more than 1 activated.
                        if (thisPar.hasClass("active")) {
                            thisPar.removeClass("active");
                            elm.removeClass(classToAdd);
                        } else {
                            thisPar.addClass("active");
                            elm.addClass(classToAdd);
                            elm.trigger('click');
                        }
                    }
                    e.stopPropagation();
                    elm.trigger('click');
                    htmlEditor.liveUpdateView();
                    htmlEditor.enableSave();
                });
            }

            try {
                var showSettings = jQuery('#highlighter').hasClass('active');
                if (showSettings) {
                    var tagName = thisElm[0].tagName;
                    //
                    if (jQuery.inArray(tagName, tagListToHide) !== -1) {
                        (thisElm.parent()).trigger('click');
                    } else {
                        setSettings(thisElm);
                    }

                }

                if (typeof this_class === 'object' && htmlEditor.classConfig(thisElm) && this_class.length > 0) {
                    this_class = this_class.concat(htmlEditor.classConfig(thisElm)).unique();
                    setMetaContent(this_class, thisElm);
                } else if (typeof this_class === 'object' && !htmlEditor.classConfig(thisElm)) {
                    setMetaContent(this_class, thisElm);
                } else {
                    try {
                        this_class = this_class.concat(htmlEditor.classConfig(thisElm)).unique();
                        setMetaContent(this_class, thisElm);
                    } catch (exc) {
                        this_class = (htmlEditor.classConfig(thisElm)).unique();
                        setMetaContent(this_class, thisElm);
                    }
                }

            } catch (exc) {}


        }
        e.stopPropagation();
    });
    jQuery('.tree_view,.right-pane').on('mouseover', function () {
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents()); //Remove the highlights.
        htmlEditor.removeFrameSettings();
    });
};

htmlEditor.getLatestSavedContent = function () {
    var content = CKEDITOR.instances.editor1.getData();
    jQuery("#test_pdf").html(content);
    jQuery("#test_pdf").find("*").removeAttr("data-pos");
    var finalContent = jQuery("#test_pdf").html();
    jQuery("#test_pdf").empty();
    return finalContent;
};

htmlEditor.removeFrameSettings = function () {
    var editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    editor.find(".iframe-setting").remove();

    var removeClasses = ["indicator", "highlightSelected", "highlight_on_drop", "highlight"]; // Add classes thats not required in the mode
    var removeElems = ["toolBox", "descOfTarget"]; // Add classes of the elements that needs to be removed.

    for (var i = 0; i < removeClasses.length; i++) { // Removes the unwanted classes from the given frame.
        editor.find("." + removeClasses[i]).removeClass(removeClasses[i]);
    }

    for (var k = 0; k < removeElems.length; k++) { // Removes unwanted elements from the given frame.
        editor.find("." + removeElems[k]).remove();
    }
};
htmlEditor.checkToBackToDashboard = function () {
    htmlEditor.backToDashboardFlag = true;
    setTimeout(function () {
        var lastContent = htmlEditor.getLatestSavedContent();
        if (lastContent !== htmlEditor.currContent) {
            swal({
                title: "You are about to leave the page",
                text: " Want to save the content?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                allowOutsideClick: false
            }, function (isConfirm) {
                if (isConfirm) {
                    htmlEditor.saveContent();
                    htmlEditor.backToDashboard();
                } else {
                    htmlEditor.backToDashboard();
                }
            });
        } else {
            htmlEditor.backToDashboard();
        }
    }, 1000);
};
htmlEditor.removeDuplicates=function(content){
var dups = [];
var arr = Array.prototype.filter.call(content,function(el) {
console.log(el)
  // If it is not a duplicate, return true
  if (dups.indexOf(el.getAttribute('id')) == -1) {
       dups.push(el.getAttribute('id'));
        return true;
     }
    else{
         el.remove();
    }
      
});
    // debugger
return arr;
}
htmlEditor.backToDashboard = function () {
    //Initiate track for a concurrent user. Remove a user in case he leaves the page.
    var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.releaseLock,
        data: {
            'object_id': node_id,
            'user_id': user_id
        },
        type: 'POST',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            if (data.status === 200) {
                var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=REMOVE", {
                    withCredentials: true
                });
                var listener = function (event) {
                    window.location = clientUrl + '#/edit_toc/' + project_id;
                };
                es.addEventListener("message", listener);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Lock not released")
        }
    })

    //=====================================

};

$(document).ready(function () {
    jQuery(".loading-g").show();
    jQuery('#htmltagModal').hide();
    CKEDITOR.replace('editor1');
    htmlEditor.populateTagList();
    CKEDITOR.on("instanceReady", function () {
        jQuery(".cke_button").off('click').on('click', function () {
            htmlEditor.bindEventsToCk();
        });
        /*For content that came from Backend */
        var content = jQuery(".cke_wysiwyg_frame").contents().find("*"); //get the content
        htmlEditor.generateIds(content);
        htmlEditor.setContentWindowHeight();
        CKEDITOR.instances.editor1.on('mode', function (e) {
            /*For content that came from Source Tab manually */
              var content = jQuery(".cke_wysiwyg_frame").contents().find("*"); //get the content
              htmlEditor.generateIds(content);
//            if(e.editor.mode==="wysiwyg"){
//                 content=htmlEditor.removeDuplicates(content);
//            }
           
        });
        htmlEditor.getJsonValue();
    });
});

var guid = function () {
    var separator = '';

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    function s4_alpha() {
        var max = 122,
            min = 97;
        var return_str = "";
        for (var i = 0; i <= 3; i++) {
            var rand = Math.floor(Math.random() * (max - min + 1)) + min
            return_str += String.fromCharCode(rand);
        }
        return return_str;
    }
    return function () {
        return s4_alpha() + s4() + separator + s4() + separator + s4() + separator + s4() + separator + s4() + s4() + s4();
    };
};