var classSet = {
    OL : ['answerlist','biblioentrylist','rearnotelist','keywordlist','objectivelist','ol_lower-alpha','ol_lower-roman','ol_upper-alpha','ol_upper-roman','choices','matchlist','practicelist','staticlist','steplist','tocentrylist'],
    UL : ['answerlist','biblioentrylist','rearnotelist','index-entry-list','keywordlist','objectivelist','choices','matchlist','practicelist','steplist','ul_none'],
    ASIDE: ['boxright', 'boxleft', 'boxblue', 'boxyellow'],
	SECTION: ['objectiveset', 'objectiveblock'],
    BLOCKQUOTE:['alignright', 'alignleft'] 
}