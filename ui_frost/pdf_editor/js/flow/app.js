/*global angular */
'use strict';

/**
 * The main app module
 * @name app
 * @type {angular.Module}
 */
var app = angular.module('app', ['flow', 'ngSanitize'])
        .config(['flowFactoryProvider', function(flowFactoryProvider) {
                flowFactoryProvider.defaults = {
                    target: 'upload.php',
                    permanentErrors: [404, 500, 501],
                    maxChunkRetries: 1,
                    chunkRetryInterval: 5000,
                    simultaneousUploads: 1
                };
                flowFactoryProvider.on('catchAll', function(event) {
                    //console.log('catchAll', arguments);
                });
                // Can be used with different implementations of Flow.js
                // flowFactoryProvider.factory = fustyFlowFactory;
            }]);

app.run(function($rootScope, $templateCache, $sce, $compile) {

});
app.controller('UploadImageController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = {taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset'};
    var loc = jQuery("#uploadImage");
//    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "image/jpeg" || fileType === "image/gif" || fileType === "image/png" || fileType === "image/bmp") {
            jQuery("#img_modal").find(".file-upload-wrapper").hide();
            jQuery("#img_modal").find(".fileupload input").attr("disabled", true);
            jQuery("#img_modal").find(".text").show().text("0%");
        }
        else {
            swal("Please upload images only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        var progress = Math.ceil(objFlow.progress() * 100);
        function renderProgress(progress)
        {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#img_modal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#img_modal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#img_modal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#img_modal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#img_modal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 75 && progress <= 100) {
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#img_modal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#img_modal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#img_modal").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //var location = response.location;
        var location = response.asset_location
        htmlEditor.newImgObj.url = location;        //Insert the image in the editor.
        $timeout(function() {
            jQuery("#imgCont").trigger("click");
            jQuery("#img_modal").find(".loader-spiner").css("border", "0");
            jQuery("#img_modal").find(".fileupload input").attr("disabled", false);
            jQuery("#img_modal").find(".file-upload-wrapper").show();
            jQuery("#img_modal").find(".text").text("").hide();
            jQuery("#img_modal").modal("hide");
        }, 2000)

//            htmlEditor.callImages(1, false, htmlEditor.imageObj);     //Populate the new image in asset lib.

//        $timeout(function() {
//            loc.find(".progress.image-loding-button").parent().remove()
//        }, 1000);
    }
});

app.controller('UploadVideoController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = {taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset'};
    var loc = jQuery("#uploadVid");
//    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "video/mp4") {
            jQuery("#videoModal").find(".file-upload-wrapper").hide();
            jQuery("#videoModal").find(".fileupload input").attr("disabled", true);
            jQuery("#videoModal").find(".text").show().text("0%");
        }
        else {
            swal("Please upload mp4 videos only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
         htmlEditor.VideoModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);
        function renderProgress(progress)
        {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#videoModal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#videoModal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#videoModal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#videoModal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#videoModal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.VideoModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#videoModal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#videoModal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#videoModal").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //var location = response.location;
        var location = response.asset_location
        htmlEditor.newVidObj.url = location;
        $timeout(function() {
            jQuery(".video_conf").trigger("click");
            jQuery("#videoModal").find(".loader-spiner").css("border", "0");
            jQuery("#videoModal").find(".fileupload input").attr("disabled", false);
            jQuery("#videoModal").find(".file-upload-wrapper").show();
            jQuery("#videoModal").find(".text").text("").hide();
            jQuery("#videoModal").modal("hide");
        }, 2000);
    }
});

app.controller('UploadGadgetController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = {taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset'};
    var loc = jQuery("#uploadGadget");
//    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "application/x-zip-compressed" || fileName.slice(-3) === 'zip') {
            jQuery("#iframeModal").find(".file-upload-wrapper").hide();
            jQuery("#iframeModal").find(".fileupload input").attr("disabled", true);
            jQuery("#iframeModal").find(".text").show().text("0%");
        }
        else {
            swal("Please upload .zip files only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        var progress = Math.ceil(objFlow.progress() * 100);
        function renderProgress(progress)
        {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#iframeModal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#iframeModal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#iframeModal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#iframeModal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#iframeModal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            else if (progress >= 75 && progress <= 100) {
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#iframeModal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#iframeModal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#iframeModal").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
//        var location = response.location;
        var asset_id = response.asset_id;
        htmlEditor.newGadgetObj.gadgetId.length = 0;
        htmlEditor.newGadgetObj.gadgetId.push(asset_id);
        jQuery('#iframeTerm').val("");
        $timeout(function() {
            jQuery("#iframeModal .iframe_conf").trigger("click");
            jQuery("#iframeModal").find(".loader-spiner").css("border", "0");
            jQuery("#iframeModal").find(".fileupload input").attr("disabled", false);
            jQuery("#iframeModal").find(".file-upload-wrapper").show();
            jQuery("#iframeModal").find(".text").text("").hide();
            jQuery("#iframeModal").modal("hide");
        }, 2000);
    }
});