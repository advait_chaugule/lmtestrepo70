$(function () {
    var i = -1;
    var pageNumber = 1,
        itemsPerPage = 12;
    var pageData = [];
    var mark_btn_state="Mark All";
    $('#loader').show();
    jQuery.ajax({
        url: htmlEditor.CoursemappedAPI.customtags,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            fillOptions(data.data.list);

        }
    });
    var saveTaggedContent = function (data) {
        $('.save').addClass('disabled');
        jQuery.ajax({
            url: htmlEditor.CoursemappedAPI.saveTopLevelContent,
            async: true,
            method: 'POST',
            data: data,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data, message) {
                $('.modal-body').html(data.message);
                handleBtns();
				taggedContent=[];
            }
        });
    }
    var getHtmlObjects = function () {
        jQuery.ajax({
            url: htmlEditor.CoursemappedAPI.pdfObjects + '&pageNumber=' + pageNumber + '&itemsPerPage=' + itemsPerPage + '',
            async: true,
            method: 'GET',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data) {
                  mark_btn_state="Mark All";
                  $(".mark_all").text(mark_btn_state);
                data.data.object.map(function (item, key) {
                    createDataFactory(item, key);
                    createThumbNails(item.description, item.id,item.page_number, data.total, data.data.object.length, item.is_marked);
                });
            }
        });
    }
    getHtmlObjects();

  function toggle_markings(){
   
      var item_len=  $('.imgDiv').length;
      for(i=0;i<item_len;i++){
          if($('.imgDiv').eq(i).find('.checked').length==0&&mark_btn_state==="Mark All"){
              $('.imgDiv').eq(i).trigger('click');
          }
           if($('.imgDiv').eq(i).find('.checked').length!==0&&mark_btn_state==="Unmark All"){
              $('.imgDiv').eq(i).trigger('click');
          }
           
      }
      if(mark_btn_state==="Mark All"){
          mark_btn_state="Unmark All";
      }
      else{
          mark_btn_state="Mark All";
      }
         $(".mark_all").text(mark_btn_state);
   
  }
    function hideBtns() {
        $('.tag-name-list').removeAttr('style').removeClass('displayInline').addClass('displayNone');
        $('.save').removeClass('displayInline').addClass('displayNone');

    }

    function handleBtns() {
        $('.save').removeClass('disabled');
        $('.done-tagging').hide();
        hideBtns();
    }
    var createDataFactory = function (item, key) {
        pageData[item.id]=item;
    }
    var taggedContent = [];
    var bindEvents = function () {
        $(".mark_all").off().on('click',function(){
           toggle_markings(); 
        });
        $('.chckbox').off().on('click', function (event) {
            event.stopPropagation();
            fillModalContent($(this).attr('index'));
        });
        $('.tag-name-list').off().on('change', function (e) {
            if ($('.tag-name-list').val()) {
                $('.save').addClass('displayInline');
            } else {
                $('.save').addClass('displayNone');
            }

        });
        $('.close-modal').off().on('click', function () {
            hideBtns();
        });
        var mergedContent = "";
        $('.done-tagging').off().on('click', function () {
            mergedContent = "";
            for (i in taggedContent) {
                mergedContent += taggedContent[i];
            }

            if (!mergedContent) {
                mergedContent = "NO CONTENTS SELECTED!";
            }
            $('.modal-body').html(mergedContent);
            $('.tag-name-list').css({
                "display": "block"
            });
            if ($('.tag-name-list').val()) {
                $('.save').addClass('displayInline');
            }

        });
        $('.save').off().on('click', function () {
            var selectedDivs = [];
            Array.prototype.forEach.apply($('.checked'), [(i, k) => {
                $(i).removeClass('checked')
                var index = $(i).parents('.imgDiv').addClass('disabled marked').attr('data-index');
                selectedDivs.push(pageData[index].id);
            }]);
            var tag = $(".tag-name-list option:selected").val();
            var startTagEnd = tag.indexOf('>');
            var wrappedContent = tag.slice(0, startTagEnd + 1) + mergedContent + tag.slice(startTagEnd + 1);
            var data = {
                marked_objects: JSON.stringify(selectedDivs),
                description: wrappedContent,
                element_name: $(".tag-name-list option:selected").text()
            }
            saveTaggedContent(data);

        });
        $('.bck-dashboard').off().on('click', function () {
            window.location.href = clientUrl + '/#/pdf_import_list/' + project_id;
        });
        $('.loadMore').off().on('click', function () {
            $('#loader').show();
            $('.done-tagging').hide();
            $(this).addClass('disabled');
            pageNumber += 1;
            getHtmlObjects();
        });
    }
    var fillOptions = function (options) {
        for (var i = 0; i < options.length; i++) {
            $('.tag-name-list').append('<option value="' + options[i].element_value + '">' + options[i].element_name + '</option>');
        }
    };
    var createThumbNails = function (resp, id,page_Number, totalObjects, itemsLoaded, is_marked) {
        var content = resp;
        $("#temp").empty();
        $("#temp").append(content);
        html2canvas($("#temp"), {
            onrendered: function (canvas) {
                var isChecked = is_marked > 0 ? "marked oldObject" : "";
                var disabled = is_marked > 0 ? "disabled" : "";
                $('<div class="imgDiv ' + disabled + " " + isChecked + '" data-index="' + id+ '" data-sort="'+page_Number+'" ><span class="content-name">Page-' + page_Number + '</span><div class="images-select"><input type="checkbox" id="cbtest" disabled=""><label for="cbtest" class="check-box "></label></div><span  index=' + id + ' data-toggle="modal" data-target=".modal" class="chckbox glyphicon glyphicon-eye-open" value="' + page_Number + '"></span></div>').append($("<img/>", {
                    id: "image",
                    src: canvas.toDataURL("image/png"),
                    width: "100%"
                })).off().on('click', function (event) {
                    var opr;
                    if ($(this).find('.check-box').hasClass('checked')) {
                        $(this).find('.check-box').removeClass('checked');
                        opr = "remove";
                    } else {
                        $(this).find('.check-box').addClass('checked');
                        opr = "add";
                    }
                    pushContent(content,id, opr);
                    toggleFinishBtn();
                }).appendTo($("#show_img"));
                bindEvents();
                if ($('.imgDiv').not('.sorted').length === itemsLoaded) {
                    sortElements();
                }
                if ($('.imgDiv').length === totalObjects) {
                    $('.loadMore').hide();
                }
                $("#temp").empty();
            }
        });
    }

    function toggleFinishBtn() {
        if ($('.checked').not('.oldObject').length) {
            $('.done-tagging').show();
        } else {
            $('.done-tagging').hide();
        }
    }

    function sortElements() {
        var $wrapper = $("#show_img");
        $wrapper.find('.imgDiv').not('.sorted').sort(function (a, b) {
                return +a.dataset.sort - +b.dataset.sort;
            })
            .appendTo($wrapper);
        $wrapper.find('.imgDiv').addClass('sorted');
        $('#loader').hide();
        $('.loadMore').removeClass('disabled');
    }

    function pushContent(contentToAdd, id, opr) {
        if (opr === "remove") {
            taggedContent[id] = "";
            return false;
        }
        taggedContent[id] = contentToAdd;
    }
    var fillModalContent = function (index) {

        $('.modal-body').html(" ");
        $('.modal-body').html(pageData[index].description);
        $('.modal').modal();

    }


});