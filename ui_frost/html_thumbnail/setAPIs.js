////        ======= Function to get the params of various keys =========
String.prototype.getParam = function(str) {
    str = str.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]*" + str + "=([^&#]*)");
    var results = regex.exec(this);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

var str = decodeURIComponent(window.location);
var project_id = str.getParam("project_id");
var node_id = str.getParam("node_id");
var node_title = str.getParam("title");
var node_type_name = str.getParam("node_type_name");
var node_type_id = str.getParam("node_type_id");
var access_token = str.getParam("access_token");
var version_id = str.getParam("version_id");
var section_id = str.getParam("section_id");
var glossaryId = "";
var import_id=str.getParam("import_id");
var project_type_id = str.getParam("project_type_id");
var user_id = "";
//        ===============================================================

var htmlEditor = {};

htmlEditor.CoursemappedAPI = {
    "pdfObjects": serviceEndPoint + '/api/v1/pdf-objects/project_id/' + project_id + '/import_id/'+import_id+'?access_token=' + access_token,
    "customtags": serviceEndPoint + 'api/v1/content-elements?access_token=' + access_token + '&top_level=1',
    "saveTopLevelContent": serviceEndPoint + 'api/v1/pdf-objects/project_id/' + project_id + '?access_token=' + access_token
};
// api/v1/content-elements??access_token=c2bf0e22d1243eacf182d1831b15cd43584fad4f&top_level=1
// api/v1/pdf-objects/project_id/2