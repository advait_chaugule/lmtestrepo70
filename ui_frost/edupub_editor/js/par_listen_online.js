/* 
 * Frost
 * Author - Supriyo Chakroborty
 * Dependency jQuery
 * Dependency browser should support post-message
 */


var allFrameLoaded = 0,
        parFrameEl = '';
function listenChildMessage(msg) {
    var childReq = msg.data,
            reqType = (childReq.type).toUpperCase();

    if (reqType == 'GET') {
        parFrameEl = 'testCont';

        if (jQuery('#' + parFrameEl).contents().find('iframe.exwidget').length >= allFrameLoaded)
            allFrameLoaded++;

    }
    else if (reqType == 'POST') {
        parFrameEl = '#widFrame';

    }
    else if (reqType == 'PUT') {
        try {
            var dataPath = htmlEditor.thisFrame.attr("data-path"),
                    ref = htmlEditor.thisFrame.attr("id");

            //jQuery(".cke_wysiwyg_frame").contents().find('#' + ref).attr('data-path', dataPath);

            var childRes = {
                'type': 'GET',
                'frame_id': ref,
                'data_path': dataPath
            }

            sendResponse('POST', 'widFrame', childRes);
        }
        catch (exc) {
        }

        //console.log(dataPath)
    }


    switch (reqType) {
        case "GET":
            if (jQuery('#' + parFrameEl).contents().find('iframe.exwidget').length == allFrameLoaded)
                getChildDefaultAttributes();
            break;

        case "POST":
            setData(htmlEditor.thisFrame.attr('id'), childReq.dataset);
            break;

        default:
            //console.log('Sorry, something went wrong!!');
            break;
    }
}

if (window.addEventListener) {
    window.addEventListener("message", listenChildMessage, false);
} else {
    window.attachEvent("onmessage", listenChildMessage);
}

function setData(ref, data) {
    var dataPath = setWidgetData(ref, data);
    jQuery(".cke_wysiwyg_frame").contents().find('#' + ref).attr('data-path', dataPath);

    var childRes = {
        'frame_id': ref,
        'data_path': dataPath
    }

    sendResponse('POST', 'widFrame', childRes);
}

function setWidgetData(ref, data) {
    var path = "",
            dataToSent = JSON.stringify(data);

    var reqParam = {"project_id": project_id, "project_type_id": project_type_id, "ex_widgets": dataToSent, "repo_id": ref};
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.saveWidgetDataAPI,
        async: false,
        method: 'POST',
        data: reqParam,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            path = data.json_file_path;
        },
        error: function () {
            alert("Error occured..Retry");
        }
    });
    return path;
}

var getChildDefaultAttributes = function () {
    parFrameEl = 'testCont';
    var childIdSet = iterateChildFrames('id'),
            childData = iterateChildFrames('data-path');

    for (var ind = 0; ind < childIdSet.length; ind++) {
        var childRes = {
            'type': 'GET',
            'frame_id': childIdSet[ind],
            'data_path': childData[ind]
        }
        sendResponse('GET', childIdSet[ind], childRes);

    }
    allFrameLoaded = 0;
};

function sendResponse(type, ref, obj) {
    if (type == 'POST') {
        var frameId = ref,
                frame = (jQuery('body').find('#' + frameId))[0];

        frame.contentWindow.postMessage(obj, "*");
    }
    else if (type == 'GET') {
        parFrameEl = 'testCont';
        var frameId = ref,
                frame = (jQuery('body').find('#' + parFrameEl).contents().find('body').find('#' + frameId))[0];

        frame.contentWindow.postMessage(obj, "*");
    }

}

function iterateChildFrames(atr) {
    var attrSet = [];
    jQuery('body').find('#' + parFrameEl).contents().find('body').find('iframe.exwidget').each(function () {
        var elId = jQuery(this).attr(atr);
        attrSet.push(elId);
    });
    return attrSet;
}