htmlEditor.getMediaImages = function(imgobj) {
    jQuery("#spinner").show();
    htmlEditor.advanced_search = {};
    htmlEditor.advanced_search.params = {};
    htmlEditor.advanced_search.enableSearch = false;
    htmlEditor.advanced_search.active = false;
    var ctrlSel = 'div[ng-controller=advancedImageSearchController]';

    angular.element(ctrlSel).scope().advanced_search.enableSearch = false;
    angular.element(ctrlSel).scope().advanced_search.active = false;
    angular.element(ctrlSel).scope().advanced_search.isCollapsed = false;
    angular.element(ctrlSel).scope().applyAdvancedClear();
    angular.element(ctrlSel).scope().$apply();
    htmlEditor.getMediaImagesArgs = arguments;
    htmlEditor.checkedImagePath = "";
    htmlEditor.checkedImagePath = "";
    jQuery("#img_modal").modal("show");
    jQuery('#img_modal').find('.search_text').val('');
    jQuery("#img_modal .modal-content-container").html(""); //Clean the div for new imageset
    jQuery("#imgHt, #imgWidth, #imgAlt").val(""); //Empty last records
    jQuery("#imgCont").attr("disabled", true);
    //Select the first tab by default
    jQuery("#img_modal .modal-content").find(".active").removeClass("active");
    jQuery(".nav-for-image.img-only").find("li").eq(0).addClass("active");
    jQuery(".tab-content.image-tab-content .tab-pane").eq(0).addClass("active");
    htmlEditor.imageObj = imgobj; //the image element.
    htmlEditor.newImgObj = {};
    htmlEditor.newImgObj.id = [];
    htmlEditor.newImgObj.url = imgobj ? imgobj.src : ""; //Store the url of the selected image.
    jQuery("#img_modal .loc-or-global").addClass("active"); //Select the local by default.

    if (imgobj) { //initaial values inserted in the relevant fields.
        var ht = imgobj.height ? jQuery(imgobj).height() : "";
        var wid = imgobj.width ? jQuery(imgobj).width() : "";
        var alt = imgobj.alt ? imgobj.alt : "";
        jQuery("#imgHt").val(ht);
        jQuery("#imgWidth").val(wid);
        jQuery("#imgAlt").val(alt);
    }

    var enableConfirmBtn = function() { //Enable the confirm button if height width or alt text are changed for some image.
        if (htmlEditor.newImgObj.url.length) {
            setTimeout(function() { //Settimeout to get the instantaneous value
                var currht = parseInt(jQuery("#imgHt").val());
                jQuery("#imgHt").val(currht);
                var currwide = parseInt(jQuery("#imgWidth").val());
                jQuery("#imgWidth").val(currwide);
                if (currht > 0 && currwide > 0) {
                    jQuery("#imgCont").attr("disabled", false);
                } else {
                    jQuery("#imgCont").attr("disabled", true);
                }
            }, 100);
        }
    }
    var page_num = 1;
    var preserveRatio = true;

    var getRepositoryType = function() {
        //get the repository type, i.e, local / global.
        var fetchFrom = jQuery("#img_modal .loc-or-global.active").text();
        var fromUrl;
        var actives = jQuery("#img_modal .loc-or-global.active").length;
        if (actives > 1) {
            fromUrl = htmlEditor.PXEmappedAPI.getAllImages;
        } else {
            if (fetchFrom.toLowerCase() === "local") {
                fromUrl = htmlEditor.PXEmappedAPI.getLocalImagesAPI;
            } else if (fetchFrom.toLowerCase() === "global") {
                fromUrl = htmlEditor.PXEmappedAPI.getGlobalImagesAPI;
            } else if (fetchFrom.toLowerCase() === "all") {
                fromUrl = htmlEditor.PXEmappedAPI.getAllImages;
            }
        }
        return fromUrl; //Return the URL to be used.
    }

    htmlEditor.callImages(page_num, false, getRepositoryType());

    jQuery("#img_modal .loc-or-global").off().on("click", function() {
        //        if (!jQuery(this).hasClass("active")) {
        //            jQuery(".loc-or-global").removeClass("active");
        jQuery(this).toggleClass("active");
        if (htmlEditor.advanced_search.enableSearch) {
            htmlEditor.callImages(page_num, false, serviceEndPoint + 'api/v1/asset-search');
        } else {
            htmlEditor.callImages(page_num, false, getRepositoryType());
        }
        jQuery("#imgCont").attr("disabled", true);
        //        }
    })

    jQuery('#img_modal').find('.search_btn').off('click').on('click', function() {
        htmlEditor.advanced_search.enableSearch = false;
        htmlEditor.advanced_search.active = false;
        angular.element(ctrlSel).scope().advanced_search.enableSearch = false;
        angular.element(ctrlSel).scope().advanced_search.active = false;
        angular.element(ctrlSel).scope().$apply();
        var url = getRepositoryType();
        htmlEditor.callImages(page_num, true, url);
    });
    jQuery('#img_modal').find('.clear_text').off('click').on('click', function() {
        var url = getRepositoryType();
        jQuery('#img_modal').find('.search_text').val('');
        htmlEditor.callImages(page_num, false, url);
    });

    jQuery("#imgrestore").off("click").on("click", function() {
        htmlEditor.getOriginalSize(htmlEditor.newImgObj.url, function(dimension) {
            jQuery("#imgHt").val(dimension.height);
            jQuery("#imgWidth").val(dimension.width);
            jQuery("#imgCont").attr("disabled", false);
        });

    });

    jQuery("#imglock").off("click").on("click", function() {
        jQuery(this).find("span.fa").toggleClass("fa-unlock");
        if (jQuery(this).find("span.fa").hasClass("fa-unlock")) {
            preserveRatio = false;
        } else {
            preserveRatio = true;
        }
    });

    jQuery("#imgHt").off("change").on("change", function() {
        if (preserveRatio) {
            htmlEditor.getOriginalSize(htmlEditor.newImgObj.url, function(dimension) {
                var htRatio = (dimension.height / (dimension.height + dimension.width));
                var widRatio = 1 - htRatio;
                var currHt = jQuery("#imgHt").val();
                var newWidth = parseInt(currHt * widRatio / htRatio);
                jQuery("#imgWidth").val(newWidth);
            });
        }
        enableConfirmBtn();
    })

    jQuery("#imgWidth").off("change").on("change", function() {
        if (preserveRatio) {
            htmlEditor.getOriginalSize(htmlEditor.newImgObj.url, function(dimension) {
                var htRatio = (dimension.height / (dimension.height + dimension.width));
                var widRatio = 1 - htRatio;
                var currWidth = jQuery("#imgWidth").val();
                var newHt = parseInt(currWidth * htRatio / widRatio);
                jQuery("#imgHt").val(newHt);
            });
        }
        enableConfirmBtn();
    })

    jQuery("#imgAlt").off("change").on("change", function() {
        enableConfirmBtn();
    })
}

htmlEditor.callImages = function(page, search_flag, url) {
    var request_type = "GET";
    if (jQuery("#exwidgetModal").css('display') === 'block') {
        jQuery('li').find('a').each(function() {
            if (jQuery(this).attr('href') === '#configImage') {
                jQuery(this).hide();
            }
        });

    }
    if (jQuery("#exwidgetModal").css('display') === 'none') {
        jQuery('li').find('a').each(function() {
            if (jQuery(this).attr('href') === '#configImage') {
                jQuery(this).show();
            }
        });

    }
    jQuery("#img_modal .loc-or-global").attr("disabled", true);
    var num_of_items = 10;
    var searchText = window.btoa(jQuery('#img_modal').find('.search_text').val());
    var params = {
        "pageNumber": page,
        "itemsPerPage": num_of_items,
        "object_types": "104",
        "project_id": project_id,
        "search_text": searchText,
        "taxonomy_id": 0
    };

    // is advance search is active
    if (htmlEditor.advanced_search.enableSearch) {
        htmlEditor.advanced_search.params.access_token = access_token;
        htmlEditor.advanced_search.params.asset_local_global = JSON.stringify(htmlEditor.getLocalGlobal());
        htmlEditor.advanced_search.params.pageNumber = page;
        htmlEditor.advanced_search.params.itemsPerPage = num_of_items;
        htmlEditor.advanced_search.params.project_id = project_id;
        request_type = "POST";
        params = htmlEditor.advanced_search.params;
    }

    jQuery.ajax({
        url: url,
        async: true,
        type: request_type,
        data: params, // object_type=104[image]
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            //console.log("abc");
            var img_containers = jQuery("#img_modal .modal-content-container");


            var d = data.data;
            if (d != null && d.length > 0) {
                var str = "";
                jQuery(".paginate").show();
                for (var i = 0; i < d.length; i++) {
                    var timeStamp = new Date().getTime(),
                        srcWithTimeStamp = d[i].asset_location + '?' + timeStamp;
                    if (htmlEditor.checkedImagePath == d[i].asset_location) {
                        str += '<div class="get-container"><div class="box-theme active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="' + srcWithTimeStamp + '" alt="" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '" ><a title=' + d[i].original_name + '></a>' + d[i].original_name + '</div></div></div>';
                    } else {
                        str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="' + srcWithTimeStamp + '" alt="" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '" ><a title=' + d[i].original_name + '></a>' + d[i].original_name + '</div></div></div>';
                    }

                }
                jQuery("#spinner").hide();
                str += '<div class="clearfix"></div>';
                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_images = data.totalAssets;
                    var max_page = Math.ceil(total_images / num_of_items);
                    //Pagination using plugin.
                    jQuery('#img_modal').find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").on("page", function(event, num) {
                        htmlEditor.callImages(num, search_flag, url);
                    });
                }
                img_containers.html(str);
            } else {
                if (page == 1)
                    jQuery("#spinner").hide();
                jQuery("#img_modal .modal-content-container").html("<p class='col-md-12'>No assets Found.</p>");
                jQuery(".paginate").hide();
            }
            htmlEditor.bindImageEvent();
            jQuery("#img_modal .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({
                title: "Error!",
                text: "Some error occured while fetching the assets."
            });
        }
    });
};

htmlEditor.getLocalGlobal = function() {
    var asset_local_global = {};
    var fetchFrom = jQuery("#img_modal .loc-or-global.active").text();
    var actives = jQuery("#img_modal .loc-or-global.active").length;
    if (actives > 1) {
        asset_local_global.local = true;
        asset_local_global.global = true;
    } else {
        if (fetchFrom.toLowerCase() === "local") {
            asset_local_global.local = true;
            asset_local_global.global = false;
        } else if (fetchFrom.toLowerCase() === "global") {
            asset_local_global.local = false;
            asset_local_global.global = true;
        } else if (fetchFrom.toLowerCase() === "all") {
            asset_local_global.local = true;
            asset_local_global.global = true;
        }
    }
    return asset_local_global;
}

htmlEditor.userProjectPermission = {};

htmlEditor.currentUserPermissions = function() {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            htmlEditor.userProjectPermission = data.data.project_permissions; // Save permission object locally.
        }
    })
}

htmlEditor.bindImageEvent = function() {

    var container = jQuery("#img_modal").find(".modal-content-container");
    container.find('.box-theme').off("click").on("click", function() {
        var is_global = jQuery(this).find('img').attr('class').split('_')[1];
        if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
            jQuery('.box-theme').removeClass('active');
            swal("Permission Denied!");
        } else {
            if (jQuery(this).hasClass('active')) {
                jQuery(this).removeClass('active');
                jQuery(this).find('.check-box').removeClass('checked');
                htmlEditor.checkedImagePath = "";
            } else {
                htmlEditor.checkedImagePath = jQuery(this).find('img').attr('src').split('?')[0];
                jQuery('.box-theme').removeClass('active');
                jQuery(this).addClass('active');
                jQuery(this).find('.check-box').addClass('checked');
            }
        }
        var imgUrl = jQuery('.box-theme.active').find("img").attr("src");
        var imgId = jQuery('.box-theme.active').find("img").attr("id");
        if (imgUrl !== undefined) {
            jQuery("#imgCont").attr("disabled", false);
            htmlEditor.newImgObj.url = imgUrl;
            htmlEditor.newImgObj.id.length = 0;
            htmlEditor.newImgObj.id.push(imgId);
            jQuery("#imgrestore").trigger("click");
        } else {
            jQuery("#imgCont").attr("disabled", true);
        }
    });
    //container.find('.box-theme').eq(htmlEditor.checkedIndex).trigger('click');

    function finalizedImgNSetting(imgUrl) {

        var configHt = jQuery("#imgHt").val();
        var configWid = jQuery("#imgWidth").val();
        var configAlt = jQuery("#imgAlt").val();
        //        Send the image back to the server to be mapped against local/global asset list
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
            type: 'POST',
            data: {
                'project_id': project_id,
                'is_global': '',
                'object_id': JSON.stringify(htmlEditor.newImgObj.id)
            },
            async: true,
            xhrFields: {
                withCredentials: true
            },
            success: function(data, textStatus, jqXHR) {

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("unable to put up the request")
            }
        })
        try {
            htmlEditor.imageObj.removeAttribute("data-cke-saved-src"); //Get rid of the previous saved image.
        } catch (exc) {

        }
        if (htmlEditor.imageObj) {
            htmlEditor.imageObj.src = imgUrl;
            //htmlEditor.imageObj.height = configHt;
            //htmlEditor.imageObj.width = configWid;
            jQuery(htmlEditor.imageObj).removeAttr('style')
            jQuery(htmlEditor.imageObj).attr(
                "height", configHt);
            jQuery(htmlEditor.imageObj).attr("width", configWid);

            htmlEditor.imageObj.alt = configAlt;
        } else {
            if (htmlEditor.getMediaImagesArgs[0] == false) {
                //Widget type behavior that sends widget response
                var imageRes = {
                    'url': imgUrl,
                    'alt': configAlt,
                    'width': configWid,
                    'height': configHt
                };
                setWidgetImages(imageRes);
            } else {
                //Default behavior that detects editor cursor
                var oEditor = CKEDITOR.instances.editor1;
                var html = "<img src='" + imgUrl + "' alt='" + configAlt + "' width='" + configWid + "' height= '" + configHt + "'/>";
                var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
                oEditor.insertElement(newElement);
            }

        }
        //htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
    }

    jQuery("#imgCont").off("click").on("click", function() {
        htmlEditor.enableSave();
        finalizedImgNSetting(htmlEditor.newImgObj.url);
    });


}

htmlEditor.getOriginalSize = function(theimg, fn) {
    if (theimg.length) {
        var image = new Image();
        var dimension = {};
        image.src = theimg;
        image.onload = function() {

                dimension.width = this.width;
                dimension.height = this.height;
                fn(dimension);
                //console.log(dimension)
            }
            //return dimension;
    } else {
        swal("Please Select an image")
    }
}

htmlEditor.getRepositoryType = function() {
    //get the repository type, i.e, local / global.
    var fetchFrom = jQuery("#img_modal .loc-or-global.active").text();
    var fromUrl;
    var actives = jQuery("#img_modal .loc-or-global.active").length;
    if (actives > 1) {
        fromUrl = htmlEditor.PXEmappedAPI.getAllImages;
    } else {
        if (fetchFrom.toLowerCase() === "local") {
            fromUrl = htmlEditor.PXEmappedAPI.getLocalImagesAPI;
        } else if (fetchFrom.toLowerCase() === "global") {
            fromUrl = htmlEditor.PXEmappedAPI.getGlobalImagesAPI;
        } else if (fetchFrom.toLowerCase() === "all") {
            fromUrl = htmlEditor.PXEmappedAPI.getAllImages;
        }
    }
    return fromUrl; //Return the URL to be used.
}