//Functionalities for linking in editor.
htmlEditor.addGlossary = function(termObj) {
    var term_text = '';
    var term_elem = termObj.getStartElement().$;
    var is_integer = /^\d+$/.test(termObj.getSelectedText());
    var term_int_val = parseInt(termObj.getSelectedText());
    htmlEditor.footnoteEdit = false;
    htmlEditor.totalFootnote = 0;
    jQuery.each(jQuery(".cke_wysiwyg_frame").contents().find('.footnote'), function(index, value) {
        var footnote_int_val = parseInt(jQuery(jQuery(value).html()).html());
        if (term_int_val == footnote_int_val) {
            htmlEditor.totalFootnote++;
        }
    });
    if (htmlEditor.totalFootnote) {
        if (term_elem.classList.contains("footnote") || (jQuery(term_elem).prop("tagName") == 'SUP' && jQuery(term_elem).parent().hasClass('footnote'))) {
            htmlEditor.footnoteEdit = true;
        }
    }
    // check footnot exist in page
    htmlEditor.footnoteEnable = false;
    htmlEditor.footnoteData = {};
    if (term_elem.classList.contains("keyword")) {
        try {
            if (is_integer) {
                htmlEditor.footnoteEnable = true;
            }
            term_text = term_elem.text.trim();
            htmlEditor.selectedText = term_elem.text;
        } catch (exc) {
            if (is_integer) {
                htmlEditor.footnoteEnable = true;
            }
            term_text = term_elem.textContent.trim();
            htmlEditor.selectedText = term_elem.textContent;
        }

    } else if ((term_elem.classList.contains("footnote") && is_integer) || (jQuery(term_elem).prop("tagName") == 'SUP' && is_integer) || is_integer) {
        htmlEditor.footnoteEnable = true;
        term_text = termObj.getSelectedText().trim();
        htmlEditor.selectedText = termObj.getSelectedText();
    } else {
        term_text = termObj.getSelectedText().trim();
        htmlEditor.selectedText = termObj.getSelectedText();
    }

    var glossaryTermData = 'term_text=' + encodeURIComponent(term_text);
    if (term_text != '') {
        //term_elem.classList.contains("keyword")
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.getGlossaryAPI,
            async: true,
            method: 'GET',
            data: glossaryTermData,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                htmlEditor.glossaryData = data;
                if (htmlEditor.footnoteEnable) {
                    htmlEditor.getFootnote(term_int_val, termObj);
                } else {
                    htmlEditor.openAnchorModal(termObj);
                }
            },
            error: function() {
                alert("Error occured");
            }
        });
    } else {
        swal('No text selected.');
    }
}

htmlEditor.getFootnote = function(term_int_val, termObj) {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.getFootnoteAPI + '/footnote_id/' + term_int_val + '?access_token=' + access_token,
        async: true,
        method: 'GET',
        data: {},
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            htmlEditor.footnoteData = data;
            htmlEditor.openAnchorModal(termObj);
        },
        error: function() {
            alert("Error occured");
        }
    });
};

htmlEditor.saveGlossary = function() {
    //    var glossaryTermData =    'term=' + jQuery("#glosTerm").html() +  "&description=" + jQuery("#glosDesc").val() + "&project_id=" + project_id;
    var parser = new DOMParser;
    var dom = parser.parseFromString(jQuery("#glosTerm").html().trim(), 'text/html');
    var decodedString = dom.body.textContent;
    var glossaryTermData = {
        "term": decodedString,
        "description": jQuery("#glosDesc").val().trim(),
        "project_id": project_id
    }
    if ((jQuery("#glosTerm").html().trim()).length > 0 && (jQuery("#glosDesc").val().trim()).length > 0 != '') {
        jQuery('#glosMsg').html('');
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.saveGlossaryAPI + '/project_id/' + project_id + '?access_token=' + access_token,
            async: true,
            method: 'POST',
            data: glossaryTermData,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (data.status == '200') {
                    //                    if (htmlEditor.toAddClass == 1) {
                    htmlEditor.glossaryData = data;
                    htmlEditor.setGlossaryData();
                    //                    }
                } else {
                    //swal(data.message);
                    //jQuery('#glosMsg').html(data.error.term[0]);
                    jQuery('#glosMsg').html('The Glossary term can not be greater than 255 characters.');
                    //swal(data.error.term[0]);
                }
            },
            error: function() {
                alert("Error occured");
            }
        });
    } else {
        swal('Glossary term/description can not be empty');
    }
}

htmlEditor.editGlossary = function() {
    glossaryId = htmlEditor.glossaryData.data.glossary_detail[0].id;
    //    var glossaryTermData = 'term=' + jQuery("#glosTerm").html() +
    //            "&description=" + jQuery("#glosDesc").val() + "&project_id=" + project_id + '&_method=PUT';
    var parser = new DOMParser;
    var dom = parser.parseFromString(jQuery("#glosTerm").html().trim(), 'text/html');
    var decodedString = dom.body.textContent;
    var glossaryTermData = {
        'term': decodedString,
        "description": jQuery("#glosDesc").val().trim(),
        "project_id": project_id,
        '_method': 'PUT'
    }
    if ((jQuery("#glosTerm").html().trim()).length > 0 && (jQuery("#glosDesc").val().trim()).length > 0 != '') {
        jQuery('#glosMsg').html('');
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.editGlossaryAPI + "/project_id/" + project_id + '/glossary_id/' + glossaryId + "?access_token=" + access_token,
            async: true,
            method: 'PUT',
            data: glossaryTermData,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (data.status == '200') {
                    //                    if (htmlEditor.toAddClass == 1) {
                    htmlEditor.glossaryData = data;
                    htmlEditor.setGlossaryData();
                    //                    }
                } else {
                    //swal(data.message);
                    //jQuery('#glosMsg').html(data.error.term[0]);
                    jQuery('#glosMsg').html('The Glossary term can not be greater than 255 characters.');
                }
            },
            error: function() {
                alert("Error occured");
            }
        });
    } else {
        swal('Glossary term/description can not be empty');
    }
};

htmlEditor.saveFootnote = function() {
    //    var glossaryTermData =    'term=' + jQuery("#glosTerm").html() +  "&description=" + jQuery("#glosDesc").val() + "&project_id=" + project_id;
    var footnoteData = {
        "footnote_id": jQuery("#fnId").html().trim(),
        "description": jQuery("#fnDesc").val().trim(),
    }
    if ((jQuery("#fnId").html().trim()).length > 0 && (jQuery("#fnDesc").val().trim()).length > 0 != '') {

        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.saveFootnoteAPI,
            async: true,
            method: 'POST',
            data: footnoteData,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (data.status == '200') {
                    //                    if (htmlEditor.toAddClass == 1) {
                    htmlEditor.footnoteData = data;
                    htmlEditor.setFootnoteData();
                    //                    }
                } else {
                    swal(data.message);
                }
            },
            error: function() {
                alert("Error occured");
            }
        });
    } else {
        swal('Footnote id/description can not be empty');
    }
}


htmlEditor.editFootnote = function() {
    var id = htmlEditor.footnoteData.data.id;
    var footnoteData = {
        "footnote_id": jQuery("#fnId").html().trim(),
        "description": jQuery("#fnDesc").val().trim(),
        '_method': 'PUT'
    }
    if ((jQuery("#fnId").html().trim()).length > 0 && (jQuery("#fnDesc").val().trim()).length > 0 != '') {
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.editFootnoteAPI + '/' + id + "?access_token=" + access_token,
            async: true,
            method: 'PUT',
            data: footnoteData,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (typeof data.data.id != "undefined") {
                    htmlEditor.footnoteData = data;
                    htmlEditor.setFootnoteData();
                } else {
                    swal(data.message)
                }
            },
            error: function() {
                alert("Error occured");
            }
        });
    } else {
        swal('Footnote id/description can not be empty');
    }
};

htmlEditor.setHTMLForTab = function(tabName) {
    switch (tabName) {


        case "URL":
            jQuery('#anchorCont').prop('disabled', false);
            var currEl = jQuery(CKEDITOR.instances.editor1.getSelection().getStartElement());
            var href = typeof currEl.attr('href') === 'undefined' ? '' : currEl.attr('href');

            var parEl = jQuery(CKEDITOR.instances.editor1.getSelection().getStartElement().$).parent();
            var tagName = parEl[0].tagName === 'A' ? true : false;
            //Replaced pageref > objective
            var className = parEl.attr('class') === 'pageref' && tagName ? true : parEl.attr('class') === 'xref' && tagName ? true : false;
            if (className) {
                href = typeof parEl.attr('href') === 'undefined' ? '' : parEl.attr('href');
            }

            jQuery(".tab-content").removeClass("advanced-tab glossary-tab footnote-tab");
            jQuery(".tab-content").addClass("url-tab");
            jQuery("#link").html('<div class="form-group"><label for="urlHolder">URL</label><input type="url" class="form-control" id="urlHolder" placeholder="Enter URL"  value="' + href + '"></div>');
            break;
        case "Advanced":
            jQuery('#anchorCont').prop('disabled', false);
            jQuery(".tab-content").removeClass("url-tab").removeClass("glossary-tab");
            jQuery(".tab-content").addClass("advanced-tab");
            jQuery("#link").html('<div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="true"><span id="selectLink1">Select Link Type</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="htmlEditor.fetchChapters()">Page</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="htmlEditor.choosePattern()">Pattern</a></li></ul></div>');
            break;
        case "Glossary":
            //            var nodeType = htmlEditor.getSelectedNode();
            jQuery('#anchorCont').prop('disabled', false);
            var nodeType = CKEDITOR.instances.editor1.getSelection().getStartElement().$;
            nodeType = nodeType.tagName;
            nodeType = nodeType === 'FIGURE' ? true : false;
            if (!nodeType) {
                //                nodeType = htmlEditor.getSelectedNode();
                nodeType = CKEDITOR.instances.editor1.getSelection().getStartElement().$;
                nodeType = nodeType.parentNode.tagName;
                nodeType = nodeType === 'FIGURE' ? true : false;
            }

            //htmlEditor.selectedText
            jQuery(".tab-content").removeClass("url-tab advanced-tab footnote-tab");
            jQuery(".tab-content").addClass("glossary-tab");
            var linkStr = '';
            //console.log(htmlEditor.glossaryData.data);
            if (typeof htmlEditor.glossaryData != "undefined" && htmlEditor.glossaryData.status == '200') {
                var term = htmlEditor.glossaryData.data.glossary_detail[0].term;
                var desc = htmlEditor.glossaryData.data.glossary_detail[0].description;
                var glosGuid = htmlEditor.glossaryData.data.glossary_detail[0].id;
                linkStr = '<div class="form-group"><label for="glosTerm">Glossary Term</label><!--<div class="alert alert-info" role="alert" id="glosTerm">' + CKEDITOR.instances.editor1.getSelection().getSelectedText().trim() + '</div>--><p class="alert alert-info" role="alert" id="glosTerm">' + CKEDITOR.instances.editor1.getSelection().getSelectedText().trim() + '</p><span id="glosMsg" class="text-danger" style="margin-bottom:10px;display: block;"></span><div class="clearfix"></div><label>Glossary Description</label><!--<input type="url" class="form-control" id="glosDesc" placeholder="" value="' + desc + '">--><textarea type="url" class="form-control" id="glosDesc" placeholder="">' + desc + '</textarea></div>';
            } else if (nodeType) {
                linkStr = '<div class="form-group"><div class="alert alert-info" role="alert" id="glosTerm">Figure can not be a Glossary term.</div></div>'
            } else {
                linkStr = '<div class="form-group"><label for="glosTerm">Glossary Term</label><!--<div class="alert alert-info" role="alert" id="glosTerm">' + htmlEditor.selectedText.trim() + '</div>--><p class="alert alert-info" role="alert" id="glosTerm">' + htmlEditor.selectedText.trim() + '</p><span id="glosMsg" class="text-danger" style="margin-bottom:10px;display: block;"></span><div class="clearfix"></div><label>Glossary Description</label><!--<input type="url" class="form-control" id="glosDesc" placeholder="" value="">--><textarea type="url" class="form-control" id="glosDesc" placeholder=""></textarea></div>';
            }
            jQuery("#link").html(linkStr);
            break;
        case "Footnote":
            jQuery(".tab-content").removeClass("url-tab advanced-tab glossary-tab");
            jQuery(".tab-content").addClass("footnote-tab");
            if (typeof htmlEditor.footnoteData.data != "undefined" && htmlEditor.footnoteData.data != null) {
                if (htmlEditor.footnoteEdit) {
                    linkStr = '<div class="form-group"><label for="glosTerm">Footnote Id</label><div class="alert alert-info" role="alert" id="fnId">' + htmlEditor.footnoteData.data.footnote_id + '</div><label>Footnote Description</label><input type="text" class="form-control" id="fnDesc" placeholder="" value="' + htmlEditor.footnoteData.data.description + '"></div>';
                } else if (htmlEditor.totalFootnote) {
                    jQuery('#anchorCont').prop('disabled', true);
                    linkStr = '<div>Footnote Id: ' + htmlEditor.footnoteData.data.footnote_id + ' already exists</div>';
                } else {
                    linkStr = '<div class="form-group"><label for="glosTerm">Footnote Id</label><div class="alert alert-info" role="alert" id="fnId">' + htmlEditor.selectedText.trim() + '</div><label>Footnote Description</label><input type="text" class="form-control" id="fnDesc" placeholder="" value=""></div>';
                }
            } else {
                if (parseInt(htmlEditor.selectedText.trim()) > 2147483647) {
                    linkStr = '<div>Footnote Id should not be more than 2147483647.</div>';
                    // disable the continue button
                    jQuery('#anchorCont').prop('disabled', true);
                } else {
                    linkStr = '<div class="form-group"><label for="glosTerm">Footnote Id</label><div class="alert alert-info" role="alert" id="fnId">' + htmlEditor.selectedText.trim() + '</div><label>Footnote Description</label><input type="text" class="form-control" id="fnDesc" placeholder="" value=""></div>';
                }

            }
            jQuery("#link").html(linkStr);
    }
};

htmlEditor.fetchChapters = function() {
    //debugger;
    jQuery(".showOptions").remove();
    jQuery("#dropdownMenu3").remove();
    jQuery(".link-dropdown").eq(1).remove();
    jQuery("#selectLink1").text("Page");
    jQuery.ajax({
        url: serviceEndPoint + "api/v1/project-nodes/" + project_id + '?access_token=' + access_token,
        type: 'GET',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            var chapters = data.data;
            var contHtml = "<div class='showOptions'>";
            for (var i = 0; i < chapters.length; i++) {
                //contHtml += "<div class='chapName' title='" + chapters[i].name + "' url='slug_" + chapters[i].node_id + "'><div class='chapter-details'>" + chapters[i].name + "</div></div>";
                contHtml += "<div class='chapName' title='" + chapters[i].name + "' url='" + chapters[i].url + "'><div class='chapter-details'>" + chapters[i].name + "</div></div>";
            }
            contHtml += "<div class='clearfix'></div></div>";
            jQuery("#link").append(contHtml);
            //jQuery('[data-toggle="tooltip"]').tooltip()
            jQuery(".chapName").off("click").on("click", function() {

                jQuery(".chapName").removeClass("active");
                jQuery(this).addClass("active");
                var chapURL = jQuery(this).attr("url");
                jQuery("#anchorCont").off("click").on("click", function() {
                    //Replaced pageref > objective
                    htmlEditor.anchorContClick(chapURL, 'pageref');
                });
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //console.log(textStatus)
        }
    });
};
htmlEditor.choosePattern = function() {
    //debugger;
    jQuery(".showOptions").remove();
    jQuery("#selectLink1").text("Pattern");
    jQuery.ajax({
        url: serviceEndPoint + "api/v1/project-nodes/" + project_id + '?access_token=' + access_token,
        type: 'GET',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {

            jQuery("#dropdownMenu3").remove();
            var chap_details = data.data;
            var pattHtml = '<div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="true"><span class="selectedOpt">Select Page</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">';
            jQuery(chap_details).each(function(i, v) {
                pattHtml += '<li role="presentation" class="patterns" chapUrl="' + v.url + '" nodeId="' + v.node_id + '"><a role="menuitem" tabindex="-1" href="javascript:void(0)">' + (v.name).toUpperCase() + '</a></li>';
            });
            pattHtml += "</ul></div>";
            jQuery("#link").append(pattHtml);
            jQuery(".patterns").off("click").on("click", function() {
                //debugger;
                var page_name = (jQuery(this).find('a').text());
                if (jQuery(this).attr('chapUrl').indexOf('#') != -1) {
                    var page_path = jQuery(this).attr('chapUrl').slice(0, jQuery(this).attr('chapUrl').indexOf('#'));
                } else {
                    var page_path = jQuery(this).attr('chapUrl');
                }
                var node_id = jQuery(this).attr('nodeId');
                var pattern_set_url = serviceEndPoint + "api/v1/project-node-patterns/project_id/" + project_id + "/node_id/" + node_id + '?access_token=' + access_token;
                jQuery('#dropdownMenu3').find('.selectedOpt').html(page_name);
                jQuery(".showOptions").remove();
                jQuery.ajax({
                    url: pattern_set_url,
                    type: 'GET',
                    async: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function(data, textStatus, jqXHR) {
                        //debugger;
                        var contHtml = "<div class='showOptions'>";
                        jQuery(data.data.patterns).each(function(i, v) {
                            //console.log(page_name + " == " + page_path);
                            var chap_id = v.id === false ? '' : v.id;
                            if (v.class == "gadget" || v.element == "video" || v.class == "widget") {
                                //contHtml += "<div class='chapName' url='slug_" + node_id + '#' + chap_id + "'><div class='chapter-details'>" + v.title + "</div></div>";
                                contHtml += "<div class='chapName' url='" + page_path + '#' + chap_id + "'><div class='chapter-details'>" + v.title + "</div></div>";
                            } else {
                                //contHtml += "<div class='chapName' url='slug_" + node_id + '#' + chap_id + "'><div class='chapter-details'>" + v.title + "</div><span>" + v.text + "</span></div>";
                                contHtml += "<div class='chapName' url='" + page_path + '#' + chap_id + "'><div class='chapter-details'>" + v.title + "</div><span>" + v.text + "</span></div>";
                            }
                        });
                        contHtml += "<div class='clearfix'></div></div>";
                        jQuery("#link").append(contHtml);
                        jQuery(".chapName").off("click").on("click", function() {
                            //debugger;
                            jQuery(".chapName").removeClass("active");
                            jQuery(this).addClass("active");
                            var chapURL = jQuery(this).attr("url");
                            jQuery("#anchorCont").off("click").on("click", function() {
                                htmlEditor.anchorContClick(chapURL, 'xref');
                            });
                        });
                        /*var contHtml = "<div class='showOptions'>";
                         for (var i = 0; i < chapters.length; i++) {
                         contHtml += "<div class='chapName' url='" + chapters[i].url + "'><div class='chapter-details'>" + chapters[i].name + "</div></div>";
                         }
                         contHtml += "<div class='clearfix'></div></div>";
                         jQuery("#link").append(contHtml);*/
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal("Error. Could not Fetch Patterns.");
                    }
                });
            });
            return;
            var pattHtml = '<div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="true"><span class="selectedOpt">Select Page</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">';
            for (var i = 0; i < data.length; i++) {
                pattHtml += '<li role="presentation" class="patterns"><a role="menuitem" tabindex="-1" href="javascript:void(0)">' + data[i].pattern + '</a></li>';
            }
            pattHtml += "</ul></div>";
            jQuery("#link").append(pattHtml);
            return;
            jQuery(".patterns").off("click").on("click", function() {
                jQuery(".showOptions").empty();
                var selected = jQuery(this).text();
                jQuery(this).parent().siblings().find(".selectedOpt").text(selected);
                var contHtml = "<div class='showOptions'>";
                for (var i = 0; i < data.length; i++) {
                    if (data[i].pattern == selected) {
                        for (var j = 0; j < data[i].presentIn.length; j++) {
                            contHtml += "<div class='chapName' url='" + data[i].presentIn[j].url + "'><div class='chapter-title'>" + selected + "</div><div class='chapter-details'>in " + data[i].presentIn[j].chap + "</div></div>";
                        }
                    }
                }
                contHtml += "<div class='clearfix'></div></div>";
                jQuery("#link").append(contHtml);
                jQuery(".chapName").off("click").on("click", function() {
                    jQuery(".chapName").removeClass("active");
                    jQuery(this).addClass("active");
                    var chapURL = jQuery(this).attr("url");
                    jQuery("#anchorCont").off("click").on("click", function() {
                        htmlEditor.anchorContClick(chapURL);
                    });
                });
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Error. Could not Fetch Pages.");
        }
    });
};
htmlEditor.openAnchorModal = function(selectedObj) {
    //    htmlEditor.currNode = window.getSelection().focusNode.parentElement;
    htmlEditor.currNode = selectedObj.getStartElement().$;
    //    var node_type = htmlEditor.getSelectedNode();
    var node_type = selectedObj.getStartElement().$;
    var footnote_id = parseInt(htmlEditor.selectedText);
    htmlEditor.anchorInFigure = '';
    var modalContainer = jQuery("#link_modal .modal-content-container");
    modalContainer.html("");
    jQuery("#anchorCont").prop("disabled", false);
    var this_url = '';
    try {
        if (node_type.parentElement.tagName === 'FIGURE') {
            htmlEditor.anchorInFigure = node_type;
            this_url = node_type.getAttribute('href');
        } else {
            var node_child = htmlEditor.currNode;
            if (node_child.tagName === 'A') {
                this_url = node_child.getAttribute('href');
            }
        }
    } catch (exc) {}



    //setTimeout(function () {
    try {
        var reg = new RegExp(htmlEditor.glossaryFile, 'g');
        if (node_type.tagName === 'A') {
            var checkUrl = (node_type.getAttribute("href")).match(reg);
            var hrefCheck = checkUrl ? true : false;
        }
        //        var hrefCheck = (node_type.getAttribute("href")).substring(0, 14) === htmlEditor.glossaryFile ? true : false;
        if (node_type.className === 'keyword' && hrefCheck) {
            if (htmlEditor.isGlobalProject) {
                var modalContent = '<div role="tabpanel" style="min-height:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
            } else {
                var modalContent = '<div role="tabpanel" style="min-height:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
            }

            if (htmlEditor.footnoteEnable) {
                if (htmlEditor.isGlobalProject) {
                    modalContent = '<div role="tabpanel" style="min-height:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                } else {
                    modalContent = '<div role="tabpanel" style="min-height:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                }

            }
            modalContainer.html(modalContent);
            var linkStr = '';
            var status = htmlEditor.glossaryData.status;
            if (status == '200') {
                var term = htmlEditor.glossaryData.data.glossary_detail[0].term;
                var desc = htmlEditor.glossaryData.data.glossary_detail[0].description;
                var glosGuid = htmlEditor.glossaryData.data.glossary_detail[0].id;
                linkStr = '<div class="form-group"><label for="glosTerm">Glossary Term</label><!--<div class="alert alert-info" role="alert" id="glosTerm">' + term.trim() + '</div>--><p class="alert alert-info" role="alert" id="glosTerm">' + term.trim() + '</p><span id="glosMsg" class="text-danger" style="margin-bottom:10px;display: block;"></span><div class="clearfix"></div><label>Glossary Description</label><!--<input type="url" class="form-control" id="glosDesc" placeholder="" value="' + desc + '">--><textarea type="url" class="form-control" id="glosDesc" placeholder="">' + desc + '</textarea></div>';
            } else {
                linkStr = '<div class="form-group"><label for="glosTerm">Glossary Term</label><!--<div class="alert alert-info" role="alert" id="glosTerm">' + htmlEditor.selectedText.trim() + '</div>--><p class="alert alert-info" role="alert" id="glosTerm">' + htmlEditor.selectedText.trim() + '</p><span id="glosMsg" class="text-danger" style="margin-bottom:10px;display: block;"></span><div class="clearfix"></div><label>Glossary Description</label><!--<input type="url" class="form-control" id="glosDesc" placeholder="" value="">--><textarea type="url" class="form-control" id="glosDesc" placeholder=""></textarea></div>';
            }
            jQuery("#link").html(linkStr);
        } else if (htmlEditor.footnoteEnable) {
            if (htmlEditor.isGlobalProject) {
                modalContainer.html('<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>');
            } else {
                modalContainer.html('<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>');
            }
            var linkStr = '';
            if (typeof htmlEditor.footnoteData.data != "undefined" && htmlEditor.footnoteData.data != null) {
                if (htmlEditor.footnoteEdit) {
                    linkStr = '<div class="form-group"><label for="glosTerm">Footnote Id</label><div class="alert alert-info" role="alert" id="fnId">' + htmlEditor.footnoteData.data.footnote_id + '</div><label>Footnote Description</label><input type="text" class="form-control" id="fnDesc" placeholder="" value="' + htmlEditor.footnoteData.data.description + '"></div>';
                } else if (htmlEditor.totalFootnote) {
                    linkStr = '<div>Footnote Id: ' + htmlEditor.selectedText.trim() + ' already exists</div>';
                    // disable the continue button
                    jQuery('#anchorCont').prop('disabled', true);
                } else {
                    linkStr = '<div class="form-group"><label for="glosTerm">Footnote Id</label><div class="alert alert-info" role="alert" id="fnId">' + htmlEditor.selectedText.trim() + '</div><label>Footnote Description</label><input type="text" class="form-control" id="fnDesc" placeholder="" value=""></div>';
                }
            } else {
                if (parseInt(htmlEditor.selectedText.trim()) > 2147483647) {
                    linkStr = '<div>Footnote Id should not be more than 2147483647.</div>';
                    // disable the continue button
                    jQuery('#anchorCont').prop('disabled', true);
                } else {
                    linkStr = '<div class="form-group"><label for="glosTerm">Footnote Id</label><div class="alert alert-info" role="alert" id="fnId">' + htmlEditor.selectedText.trim() + '</div><label>Footnote Description</label><input type="text" class="form-control" id="fnDesc" placeholder="" value=""></div>';
                }
            }
            jQuery("#link").html(linkStr);
        } else {
            var currEl = jQuery(selectedObj.getStartElement());
            var parEl = jQuery(selectedObj.getStartElement().$).parent();
            var tagName = parEl[0].tagName === 'A' ? true : false;
            //Replaced pageref > objective
            var className = parEl.attr('class') === 'pageref' && tagName ? true : parEl.attr('class') === 'xref' && tagName ? true : false;

            if (!className) {
                tagName = jQuery(CKEDITOR.instances.editor1.getSelection().getStartElement().$)[0].tagName === 'A' ? true : false;
                //Replaced pageref > objective
                className = currEl.attr('class') === 'pageref' && tagName ? true : currEl.attr('class') === 'xref' && tagName ? true : false;
            }
            if (htmlEditor.isGlobalProject) {
                var modalHtml = '<div role="tabpanel" style="min-heighvar modalHtmlt:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
            } else {
                var modalHtml = '<div role="tabpanel" style="min-heighvar modalHtmlt:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
            }

            if (className) {
                modalContainer.html(modalHtml);
                jQuery("#link").html('<div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="true"><span id="selectLink1">Select Link Type</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="htmlEditor.fetchChapters()">Page</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="htmlEditor.choosePattern()">Pattern</a></li></ul></div>');
            } else {
                if (htmlEditor.isGlobalProject) {
                    modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                } else {
                    modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li   role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                }
                modalContainer.html(modalHtml);
                jQuery("#link").html('<div class="form-group"><label for="urlHolder">URL</label><input type="url" class="form-control" id="urlHolder" placeholder="Enter URL" value="' + this_url + '"></div>');
            }
        }
    } catch (exc) {
        var currEl = jQuery(selectedObj.getStartElement());
        var tagName = jQuery(selectedObj.getStartElement().$).parent()[0].tagName === 'FIGURE' ? true : false;
        //Replaced pageref > objective
        var className = currEl.attr('class') === 'pageref' ? true : currEl.attr('class') === 'xref' ? true : false;
        var href = typeof currEl.attr('href') === 'undefined' ? '' : currEl.attr('href');
        var modalHtml = '<div role="tabpanel" style="min-heighvar modalHtmlt:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content glossary-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
        if (className) {
            if (footnote_id) {
                if (htmlEditor.isGlobalProject) {
                    modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content footnote-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                } else {
                    modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs" ><ul class="nav nav-tabs" role="tablist"><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content footnote-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                }

            }
            modalContainer.html(modalHtml);
            jQuery("#link").html('<div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="true"><span id="selectLink1">Select Link Type</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="htmlEditor.fetchChapters()">Page</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="htmlEditor.choosePattern()">Pattern</a></li></ul></div>');
        } else {
            if (htmlEditor.isGlobalProject) {
                modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
            } else {
                modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a></li><li   role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
            }
            if (footnote_id) {
                if (htmlEditor.isGlobalProject) {
                    modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li   role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                } else {
                    modalHtml = '<div role="tabpanel" style="min-height:150px" class="modal-tabs"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="Link" role="tab" data-toggle="tab">URL</a></li><li   role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="glossary" role="tab" data-toggle="tab">Glossary</a></li><li role="presentation"><a href="javascript:void(0)" onclick="htmlEditor.setHTMLForTab(this.text)" aria-controls="footnote" role="tab" data-toggle="tab">Footnote</a></li></ul><div class="tab-content url-tab"><div role="tabpanel" class="tab-pane active" id="link"></div><div class="clearfix"></div></div></div>';
                }

            }
            modalContainer.html(modalHtml);
            jQuery("#link").html('<div class="form-group"><label for="urlHolder">URL</label><input type="url" class="form-control" id="urlHolder" placeholder="Enter URL" value="' + this_url + '"></div>');
        }
    }

    //    htmlEditor.getSelecttedContent();
    jQuery("#anchorCont").off("click").on("click", function() {
        var curr_tab = '';
        jQuery('#link_modal .modal-content-container .nav-tabs li').each(function() {
            if (jQuery(this).hasClass('active')) {
                curr_tab = jQuery(this).find('a').text();
                curr_tab = curr_tab.toLowerCase();
                return false;
            }
        });
        if (curr_tab === 'glossary') {
            var status = htmlEditor.glossaryData.status;
            if (status == '200') {
                htmlEditor.editGlossary();
            } else {
                htmlEditor.saveGlossary();
            }
        } else if (curr_tab === 'footnote') {
            if (htmlEditor.footnoteData.data !== null) {
                htmlEditor.editFootnote();
            } else {
                htmlEditor.saveFootnote();
            }
        } else if (curr_tab === 'url') {
            //----------------------dynamically appends http:// in URL if not supplied by user-------------------------

            // var link = /^(http|https):/.test(jQuery("#urlHolder").val()) ? jQuery("#urlHolder").val() : 'http://' + jQuery("#urlHolder").val();
            // var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
            // if (!re.test(link)) {
            //     // swal("URL is not valid");
            //     jQuery('#link').append('<span style="color:red;" id="errMsg">Please Enter a Valid URL with http://, https:// or ftp://</span>');
            //     return false;
            // } else {
            // htmlEditor.anchorContClick(link.trim(), 'ulink');
            // }
            htmlEditor.anchorContClick(jQuery("#urlHolder").val().trim(), 'ulink');

        }
    });
};
htmlEditor.setGlossaryData = function() {
    var glosGuid = htmlEditor.glossaryData.data.glossary_detail[0].link_id;
    var anchorUrl = htmlEditor.glossaryFile + '#' + glosGuid;
    htmlEditor.anchorContClick(anchorUrl, 'keyword');
    jQuery('.modal .close').trigger('click');
};
htmlEditor.setFootnoteData = function() {
    var link_id = htmlEditor.footnoteData.data.link_id;
    var anchorUrl = htmlEditor.footnoteFile + '#' + link_id;
    htmlEditor.anchorContClick(anchorUrl, 'footnote');
    jQuery('.modal .close').trigger('click');
};
htmlEditor.anchorContClick = function(url, className) {
    if (url) {
        var invalid = false;
        if ((url.indexOf('http://') < 0) && (url.indexOf('https://') < 0) && (url.indexOf('ftp://') < 0)) {
            invalid = true;
        }
        if (invalid && className === "ulink") {
            jQuery('#errMsg').remove();
            jQuery('#link').append('<span style="color:red;" id="errMsg">Please Enter a Valid URL with http://, https:// or ftp://</span>');
        } else {
            var editorSelect = CKEDITOR.instances.editor1.getSelection();
            if (window.getSelection) {
                // not IE case
                var anchorInFigure = (htmlEditor.anchorInFigure.tagName === 'A')
                if (anchorInFigure) {
                    htmlEditor.anchorInFigure.setAttribute("href", url);
                    htmlEditor.anchorInFigure.className = className;
                } else {
                    var newElement = document.createElement("a");
                    //                var documentFragment = htmlEditor.selRange.extractContents();
                    var documentFragment = editorSelect.getSelectedElement() ? editorSelect.getSelectedElement() : editorSelect.getSelectedText();
                    var isAnchor = (htmlEditor.currNode.tagName === 'A');
                    var isGlossary = (htmlEditor.currNode.className === 'keyword');
                    if (isAnchor && !isGlossary) {
                        try {
                            newElement = htmlEditor.currNode;
                            newElement.setAttribute("href", url);
                            if (className == 'ulink') {
                                jQuery(newElement).attr("target", "_blank");
                            }
                            newElement.className = className;
                            htmlEditor.selRange.insertNode(newElement);
                            htmlEditor.selObj.removeAllRanges();
                        } catch (exc) {
                            //                        jQuery(htmlEditor.currNode).empty();
                            //                        jQuery(htmlEditor.currNode).attr('class', className)
                            //                        jQuery(htmlEditor.currNode).html(htmlEditor.selectedText);
                            htmlEditor.currNode.dataset.ckeSavedHref = url; //Change the data attribute of current node.
                            jQuery(htmlEditor.currNode).attr("href", url);
                        }
                    } else if (isGlossary) {
                        jQuery(htmlEditor.currNode).empty();
                        jQuery(htmlEditor.currNode).attr('class', className)
                        jQuery(htmlEditor.currNode).html(htmlEditor.selectedText);
                        jQuery(htmlEditor.currNode).attr("href", url);
                    } else if (className == 'footnote') {
                        if (!htmlEditor.footnoteEdit) {
                            // for footnote
                            jQuery(newElement).attr('class', 'footnote');
                            jQuery(newElement).attr('epub:type', 'noteref');
                            jQuery(newElement).attr('id', htmlEditor.footnoteData.data.ref_id)
                            jQuery(newElement).attr("href", url);
                            jQuery(newElement).html('<sup>' + htmlEditor.selectedText + '</sup>');

                            editorSelect.getRanges()[0].deleteContents(); //Remove selection
                            editorSelect.getNative().getRangeAt(0).insertNode(newElement);
                        }
                    } else {
                        newElement.text = documentFragment;
                        //                    newElement.appendChild(documentFragment);
                        newElement.className = className;
                        newElement.setAttribute("href", url);
                        if (className == 'ulink') {
                            jQuery(newElement).attr("target", "_blank");
                            jQuery(newElement).attr("class", ""); // this line was added to resolve iframe's inside external links were not opening in new tab.
                        }
                        editorSelect.getRanges()[0].deleteContents(); //Remove selection
                        editorSelect.getNative().getRangeAt(0).insertNode(newElement);
                        //                    htmlEditor.selRange.insertNode(newElement);
                        //                    htmlEditor.selObj.removeAllRanges();
                    }

                }

                //Replaced pageref > objective
                jQuery('.keyword, .xref, .pageref').off('click').on('click', function(e) {
                    e.stopPropagation();
                });
                jQuery('.cke_button__link').addClass('cke_button_on');
            } else if (document.selection && document.selection.createRange && document.selection.type != "None") {
                // IE case
                var range = document.selection.createRange();
                var selectedText = range.htmlText;
                var newText = '<a class="keyword" href=' + url + '>' + selectedText + '</a>';
                document.selection.createRange().pasteHTML(newText);
                jQuery('.keyword').off('click').on('click', function(e) {
                    e.stopPropagation();
                });
                jQuery('.cke_button__link').addClass('cke_button_on');
            }
            jQuery(".close").trigger("click");
            htmlEditor.bindEvents();
            htmlEditor.enableSave();
            htmlEditor.liveUpdateView();
        }
    } else {
        swal("URL Empty");
    }

};