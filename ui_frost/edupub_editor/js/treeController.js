function toggleToc() {
    /*jQuery('.toc-btn').on('click', function () {
     //alert(1)
     })*/
}

function tocInit() {
    var rootEl = jQuery('.nested_with_switch');

    rootEl.empty();

    function createExpandCollapseTree(dom) {
        rootEl.html(dom);
        bindExpandCollapseTree();
        refreshDragDrop();
    }

    function bindExpandCollapseTree() {
        rootEl.find('li').on('click', function(e) {
            e.stopPropagation();
        });

        rootEl.find('.expandToc').off('click').on('click', function(e) {
            var parElm = jQuery(this).parent();
            parElm.find('li').each(function() {
                jQuery(this).toggle();
            });
            if (jQuery(this).children('.fa').hasClass('fa-caret-right')) {
                jQuery(this).children('.fa').removeClass('fa-caret-right').addClass('fa-caret-down');
            } else {
                jQuery(this).children('.fa').removeClass('fa-caret-down').addClass('fa-caret-right');
            }
        });

        rootEl.find('.contPreview').off('click').on('click', function(e) {
            // debugger;
            var dataId = jQuery(this).parent().data('id'),
                dataTitle = jQuery(this).parent().data('title'),
                dataType = jQuery(this).parent().data('type');

                if (dataType == 216) {
                swal({
                        title: "Warning!",
                        text: "Assessment node is not editable",
                        type: "warning",
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var parentTocCollapse = jQuery("#tocBtn").parent();
                            setTimeout(function() {
                                parentTocCollapse.addClass("open");
                            }, 50);
                        }
                    });
                return;
            } else {
                var pathToGo = "index.html?access_token=" + access_token + "&project_id=" + project_id + "&node_id=" + dataId + "&node_title=" + dataTitle + "&node_type_name=&node_type_id=" + dataType + "&project_type_id=" + project_type_id + "&section_id= ";
            }

            // added to hold section wise auto scroll
            var fileWithSectionName = jQuery(this).parent().data('file');
            var sectionId;
            if (fileWithSectionName.indexOf("#") > 0) {
                var sectionTrackerArray = fileWithSectionName.split("#");
                if (sectionTrackerArray[1]) {
                    sectionId = sectionTrackerArray[1];
                }
            }
            // ends

            if (dataTitle == "footnote") {
                swal({
                    type: "warning",
                    text: "Cannot navigate to the editor for footnote"
                });
                return false;
            } else {
                var pathToGo = "index.html?access_token=" + access_token + "&project_id=" + project_id + "&node_id=" + dataId + "&node_title=" + encodeURIComponent(dataTitle) + "&node_type_name=&node_type_id=" + dataType + "&project_type_id=" + project_type_id + "&section_id=" + sectionId;
            }
            //Content change tracker

            var lastContent = htmlEditor.getLatestSavedContent();
            var curr_ver = jQuery(".version_num").attr("version_id"); //Get current user version.
            var checkObject = htmlEditor.checkLockObject(dataId);
            if (checkObject == 0) {
                if (lastContent.replace(/\n/g, "") !== (htmlEditor.currContent).replace(/\n/g, "")) {
                    // if (lastContent !== htmlEditor.currContent) {
                    swal({
                        title: "You are about to leave the page",
                        type: "warning",
                        html: "<div style=\"height:100px;\"><center><h4>Want to save the content?</h4></center></br></br></br></br>\n\
                            <button style=\"background: #673AB7 !important; border: 1px solid #673AB7 !important; border-radius: 3px; padding: 5px 20px; text-transform: uppercase; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnA\" >Yes</button> " +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnB\" >No</button>" +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\"type=\"button\" id=\"btnC\" >Cancel</button></div>",
                        showConfirmButton: false
                    });
                    jQuery(document).on('click', "#btnA", function() {
                        htmlEditor.saveContent();
                        setTimeout(function() {
                            var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=REMOVE", { withCredentials: true });
                            window.location.href = pathToGo;
                        }, 1000);
                    });

                    jQuery(document).on('click', "#btnB", function() {
                        var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=REMOVE", { withCredentials: true });
                        window.location.href = pathToGo;
                    });

                    jQuery(document).on('click', "#btnC", function() {
                        //alert(this.id);
                    });
                } else {
                    var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=REMOVE", { withCredentials: true });
                    window.location.href = pathToGo;
                }
            }
        });
    }

    var getTOCResponse = (function() {
        createExpandCollapseTree(htmlEditor.editorTOC);
    })();

    function findItem(array, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].title.split(',').indexOf(value) >= 0) {
                return objGroup[i];
            }
        }
    }

    function refreshDragDrop() {
        setTimeout(function() {
            try {
                jQuery("ol.nested_with_switch").sortable("refresh");
            } catch (exc) {
                //console.log('Window says: '+ exc);
            }

        }, 500);
    }

    /*var createDragDrop = function(){
     var oldContainer;
     var group = jQuery("ol.nested_with_switch").sortable({
     group: 'serialization',
     afterMove: function (placeholder, container) {
     if(oldContainer != container){
     if(oldContainer)
     oldContainer.el.removeClass("active");
     container.el.addClass("active");

     oldContainer = container;
     }
     },
     onDrop: function ($item, container, _super) {
     container.el.removeClass("active");
     _super($item, container);
     getJSON()
     refreshDragDrop();
     }
     });

     function getJSON(){
     var data = group.sortable("serialize").get();
     var jsonString = JSON.stringify(data, null, ' ');
     jsonString = jsonString.substring(1, jsonString.length-1);
     jsonString = '{"children":' + jsonString + '}';
     //console.log(jsonString)
     }
     getJSON()
     }();*/
}