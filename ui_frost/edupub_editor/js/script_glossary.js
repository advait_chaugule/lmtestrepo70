//Frost - 16-Oct-2014 Supriyo Chakroborty

var jQuery = $.noConflict();
var htmlEditor = {};
var returnVal = {
    "content": '',
    "leftPanelTree": ''
};
var returnPatterns = {};
////        ======= Function to get the params of various keys =========
String.prototype.getParam = function (str) {
    str = str.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]*" + str + "=([^&#]*)");
    var results = regex.exec(this);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

var str = decodeURIComponent(window.location);
var project_id = str.getParam("project_id");
var node_id = str.getParam("node_id");
var node_title = str.getParam("title");
var node_type_name = str.getParam("node_type_name");
var node_type_id = str.getParam("node_type_id");
var access_token = str.getParam("access_token");
//===============================================================
//var serviceEndPoint = 'http://localhost:8081/';
htmlEditor.EDUPUBmappedAPI = {
    "getContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token + '&project_id=' + project_id + '&s3=0',
    "getPatternsAPI": serviceEndPoint + 'api/v1/project-pattern?access_token=' + access_token + '&project_id=' + project_id + '&pattern_status=1001',
    "saveContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token,
    "getLeftPaneAPI": serviceEndPoint + 'api/v1/content-left-panel?access_token=' + access_token,
    "getGlossaryAPI": serviceEndPoint + 'api/v1/glossary?access_token=' + access_token
};


htmlEditor.onDocumentReady = function () {
    htmlEditor.getJsonValue();
};

jQuery(window).resize(function () {
    htmlEditor.onWindowResize();
});

htmlEditor.onWindowResize = function () {
    var winHeight = jQuery(document).height()
    var HeaderHeight = jQuery('.header-container').height();
    jQuery(".pattern-list-container").css("height", (winHeight - HeaderHeight));

}

htmlEditor.getJsonValue = function () {
    var queryString = '&project_id=' + project_id;
    jQuery.ajax({
        url: htmlEditor.EDUPUBmappedAPI.getGlossaryAPI + queryString,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            jQuery(".loading-g").hide();
            jQuery("#node_title").html(node_title);
            jQuery("#project_name").html(data.project_name);
            if (data.status == '200') {
                var glossary_list = data.data.glossary_detail;
                //console.log(data.data.glossary_detail);
                var glossaryListString = '';
                for (i = 0; i < glossary_list.length; i++) {
                    glossaryListString += "<dt style='line-height:0.5px;'><textarea>" + glossary_list[i].term + "</textarea></dt><dd style='line-height:0.5px;'><textarea>" + glossary_list[i].description + "</textarea></dd>";
                }
                /*var winHeight = jQuery(document).height()
                 var HeaderHeight = jQuery('.header-container').height();                
                 jQuery(".pattern-list-container").css("height", (winHeight-HeaderHeight));
                 */
                htmlEditor.onWindowResize();
                jQuery("#glossaryList").html(glossaryListString);
            } else {
                jQuery("#glossaryList").html("<dt>No glossary terms defined yet. Start adding now.</dt>");
            }
        },
        error: function () {
            jQuery(".loading-g").hide();
            jQuery("#glossaryList").html("<dt>No glossary terms defined yet. Start adding now.</dt>");
        }
    });
    return returnVal;
};

jQuery(document).ready(function () {
    htmlEditor.onDocumentReady();
    htmlEditor.onWindowResize();
});

htmlEditor.backToDashboard = function () {
    window.location = clientUrl + '#/edit_toc/' + project_id;
}
htmlEditor.goToGlossary = function () {
    window.location = clientUrl + '#/glossary/' + project_id;
}