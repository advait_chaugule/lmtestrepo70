var createSortable = function (obj) {
    var frame = obj.frame;                          //Class of iframes.
    var draggableBlocks = obj.draggable;                 //Class of draggables.
    var exclusion = obj.excludeTags;                   //Array consisting of excluded Tags
    var pattObj = obj.patterns;                       //object containing patterns.
    var doNotInsert = obj.doNotInsertIn;                 // Array consisting tags that cannot allow patterns drop within itself / get appended.
    var excludeChildrenIn = obj.excludeChildrenIn;       //Array containing elements not to be sorted within
    var placeholder = obj.placeholder === undefined ? "sort_highlight" : obj.placeholder;       //Class of placeholder
    var detailClass = obj.detailClass === undefined ? "elm_name" : obj.detailClass;     //Class of elem detail container
    var containment = obj.containment === undefined ? "editor-container5" : obj.containment; //Containment class
    var indexOfDragged = 0;
    var frameBody = jQuery(frame).contents().find("body");
    var framePos = jQuery(frame).offset();
    var frameScrolled = frameBody.scrollTop();
    var frameHeight = jQuery(frame).height();
    var frameDoc = jQuery(frame).contents()[0];
    var _notblocklevel = ["P", "SPAN", "STRONG", "A", "H1", "H2", "H3", "H4", "H5", "H6", "BR", "EM", "SUP", "SUB", "HEADER"];    //Mention tags that are block level, and wont allow ant pattern to be dropped inside tehm.
    var lastPos, lastElm;
    var timer;              // a timeout would be associated to change the method of drop.

    var onStart = function (patt) {
        indexOfDragged = jQuery(patt).index();
        //Uncomment if you don't wanna allow any independent "p" tag in editor.
//        if (frameBody.children().length && frameBody.children().children().text() !== "") {
//            frameBody.children().eq(0).find("*").addClass("enhance-p");
//        }
//        else {
//            frameBody.html("<p class='enable-sort'>&nbsp;</p>");
//        }
        //console.log(frameBody.html())
        if ((frameBody.children().length === 1 && (!frameBody.children().children().length) && (frameBody.text().trim() === "")) || !frameBody.children().length) {
            frameBody.html("<p>&nbsp;</p>")
        }
        else {
            frameBody.children().find("*").addClass("enhance-p");
        }
    }
    var currDragElmClass = "";
    var currDragTagName = "";
    var isContentUpdated = false,
            prevContentState = 0,
            currentContentState = 0;
    var onDrag = function (e, ui) {
        //debugger;
        var helperPos = ui.helper.offset();
        var currRespPosTop = helperPos.top - framePos.top;
        var currRespPosLeft = helperPos.left;
        currDragElmClass = jQuery(ui.helper).attr('default-class');
        currDragTagName = jQuery(ui.helper).attr('tag-type');
        prevContentState = frameBody.find('*').length;
        //console.log(prevContentState)
        try {
            frameBody.find("." + placeholder).remove();
            try {
                var currElmList = frameDoc.elementsFromPoint(currRespPosLeft, currRespPosTop);
                var currElm = currElmList[0];
            }
            catch (exc) {               //in firefox
                var currElmList = frameDoc.elementFromPoint(currRespPosLeft, currRespPosTop);
                var currElm = currElmList;
            }
            var currElmTag = currElm.tagName;
            var currElmClass = currElm.className.replace(/\senhance-p|enhance-p/gi, "").trim();      //remove the dummy classname from display
            var currElmParentTag = currElm.parentElement.tagName;
            var currElmParentClass = currElm.parentElement.className.replace(/\senhance-p|enhance-p/gi, "").trim();
            var exactElm = currElmClass === "" ? currElmTag : currElmTag + "." + currElmClass;
            var existence = -1;                 //if -1, elements will be droppable
            if (frameBody.children().length) {
                if (jQuery.inArray(currElmParentTag, _notblocklevel) === -1) {          //Check if the parent of the drop target is not a block level tag.
                    existence = jQuery.inArray(currElmTag, exclusion);
                }
                else {
                    existence = 0;
                }
            }
            else {
                existence = -1;
            }

            var actionStatus, corrStatus;    //Check direction of mouse movement. corrstatus keeps Corresponding status to the current one.

            // Function to append/prepend the placeholder in the iframe.
            function positionHolder(pos) {
                var parentTagnClass = currElmParentTag === "BODY" ? "" : (currElmParentTag + "." + currElmParentClass + " -> ");
                var placeholderHTML;
                function generatePlaceholderHTML(loc) {           //Function to generate HTML for placeholders.
                    var str = "<div class=" + placeholder + "><div class=" + detailClass + ">" + loc + "- " + parentTagnClass + currElmTag + " : " + currElmClass + "</div></div>";
                    return str;
                }
                switch (pos) {
                    case "append":
                        placeholderHTML = generatePlaceholderHTML("In");
                         frameBody.find("." + placeholder).remove();
                        jQuery(currElm).append(placeholderHTML);
                        corrStatus = "after";
                        break;

                    case "prepend":
                        placeholderHTML = generatePlaceholderHTML("In");
                        frameBody.find("." + placeholder).remove();
                        jQuery(currElm).prepend(placeholderHTML);
                        corrStatus = "before";
                        break;

                    case "after":
                        placeholderHTML = generatePlaceholderHTML("After");
                         frameBody.find("." + placeholder).remove();
                        jQuery(currElm).after(placeholderHTML);
                        corrStatus = "after";
                        break;

                    case "before":
                        placeholderHTML = generatePlaceholderHTML("Before");
                         frameBody.find("." + placeholder).remove();
                        jQuery(currElm).before(placeholderHTML);
                        corrStatus = "before";
                        break;
                }
                var diff = frameBody.find("." + placeholder).offset().top - currRespPosTop - jQuery(frame).contents().scrollTop();
                if (!(diff <= 200 && diff >= -200)) {
                    frameBody.find("." + placeholder).remove();
                }

            }
            for (var p = 0; p < excludeChildrenIn.length; p++) {
                if (jQuery(currElm).parents(excludeChildrenIn[p]).length) {
                    existence = 0;
                }
            }

            // Defining editor drop rules
            if (existence === -1) {
                actionStatus = jQuery.inArray(exactElm, doNotInsert) === -1 ? (jQuery.inArray(currElmTag, _notblocklevel) === -1 ? (currRespPosTop < lastPos ? "prepend" : "append") : (currRespPosTop < lastPos ? "before" : "after")) : (currRespPosTop < lastPos ? "before" : "after");      //Check if tag is listed in the donotinsert array.
                positionHolder(actionStatus);
            }

            //Scroll the contents if draggable's offset is greater than frame's.
            if (helperPos.top > frameHeight) {
                var scrolled = jQuery(frameDoc).scrollTop();
                jQuery(frameDoc).scrollTop(scrolled + 10);
            }
            else if (helperPos.top <= (framePos.top + 20)) {
                var scrolledUp = jQuery(frameDoc).scrollTop();
                jQuery(frameDoc).scrollTop(scrolledUp - 10);
            }

            clearTimeout(timer);

            timer = setTimeout(function () {
                if (currElm === lastElm) {
                    frameBody.find("." + placeholder).remove();
                    positionHolder(corrStatus);
                }
            }, 2000);

            lastPos = currRespPosTop;
            lastElm = currElm;
        }
        catch (exc) {

        }
    }

    var onStop = function (e, ui) {
        clearTimeout(timer);
        if (!indexOfDragged) {
            frameBody.find("." + placeholder).remove();
        }
        setTimeout(function () {
            frameBody.find("." + placeholder).remove();
        }, 1500)

        frameBody.find(".enhance-p").removeClass("enhance-p");

        var snippet, thisId, thisIndx, thisParent, currElm;
        try {
            snippet = pattObj[indexOfDragged - 1].pattern;
            thisId = guid();
            thisIndx = frameBody.find("." + placeholder).index();
            thisParent = frameBody.find("." + placeholder).parent();
            frameBody.find("." + placeholder).replaceWith(snippet);
            currElm = (thisParent.children().eq(thisIndx));
            currElm.attr('id', thisId);
            if(currElm[0].className==='dataset'||  currElm[0].className==='multiplevideo' || currElm.children().hasClass('brightCoveVideo')){
                    currentContentState = frameBody.find('*').length+1;
            }
            else{
            currentContentState = frameBody.find('*').length;
        }
         
            isContentUpdated = prevContentState != currentContentState ? true : false;
            if (isContentUpdated) {
                try {
                    //Create TOC tree
                    if(thisParent.tagName() == 'BODY'){
                        thisParent.createTOC();
                    }else{
                        currElm.createTOC(thisParent);
                    }
                    
                }
                catch (exc) {
                    console.log(exc)
                }

                //Set Rule
                currElm.setLevelRule();

                htmlEditor.populateMeta();
                htmlEditor.convertFrameToDiv();
                htmlEditor.enableSave();
                try {
                    htmlEditor.bindEvents();
                }
                catch (exc) {
                    htmlEditor.editorEvents();
                }

                var content = frameBody.find("*");                //get the content
                htmlEditor.generateIds(content);                //generate ids for all elements.
            }
        }
        catch (exc) {
            frameBody.find("." + placeholder).remove();
        }
    }

    var drag_settings = {
        iframeFix: true,
        helper: function () {
            var thisAttr = jQuery(this).attr('default-class'),
                    cloneElm = jQuery(this).clone();
            cloneElm.attr('default-class', thisAttr);
            return cloneElm;
        },
        revert: "invalid",
        containment: jQuery(".cke_wysiwyg_frame"),     // jQuery(".cke_wysiwyg_frame")  containment
        scroll: false,
        start: function () {
            onStart(this);
        },
        drag: function (e, ui) {
            onDrag(e, ui);
        },
        stop: function (e, ui) {
            onStop(e, ui);
        }
    }
    jQuery(draggableBlocks).draggable(drag_settings);
}