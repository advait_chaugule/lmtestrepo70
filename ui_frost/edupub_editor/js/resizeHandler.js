//      ======================Fuction to control the ui on changein screen resolution======================
htmlEditor.onWindowResize = function() {                            // Function to handle window resize.
    var win_height = jQuery(window).height(),
            win_width = jQuery(window).width(),
            header_height = jQuery('header.lm').height(),
            resize_cont_height = jQuery('.resizer-container').innerHeight(),
            footer_height = jQuery('.cke_bottom.cke_reset_all').outerHeight(),
            toolbar_ht = jQuery(".cke_top.cke_reset_all").outerHeight(),
            editor_height = win_height - (header_height + footer_height) - toolbar_ht,
            left_pane_height = (win_height) - (header_height),
            pattern_width = jQuery(".right-pane").hasClass("hideElm") ? 0 : jQuery(".right-pane").width(),
            middle_width = win_width - pattern_width,
            getchromeVersion = function() {                    //Function to get the version of chrome.
                var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
                if(raw!==null)
                    return raw[2];
                else
                    return false;
            },
            chromeVerison = parseInt(getchromeVersion()),                         //Get the version of chrome
            isFirefox = typeof InstallTrigger !== 'undefined';
    if (isFirefox || chromeVerison>=46)
        jQuery(".middle-pane").css('margin-left', '-4px');
    jQuery(".middle-pane").width(middle_width);
    jQuery(".cke_contents").css("height", editor_height + "px");
    jQuery(".CodeMirror-wrap").css("height", editor_height + "px");
    jQuery(".left-pane").css("height", left_pane_height + "px");
    jQuery('.test-pane').css("height", (left_pane_height - (resize_cont_height + 1)) + "px");
    jQuery(".edt-rt,.right-pane").css("height", left_pane_height);
    jQuery(".split-source-container").css("height",left_pane_height-72);
    var nav_tab_ht = jQuery(".edt-rt").find(".nav.nav-tabs").outerHeight() == 0 ? 41 : jQuery(".edt-rt").find(".nav.nav-tabs").outerHeight(),
            toggler_ht = jQuery(".edt-rt").find(".lm-editor-bar").outerHeight(),
            tab_cont_ht = left_pane_height - nav_tab_ht - toggler_ht - jQuery('.profiling_el').height();
    jQuery(".edt-rt").find(".lm-big .tab-content").css({"height": tab_cont_ht, "overflow": "auto"});
    htmlEditor.calculateWidgetsOnResize();
};