htmlEditor.getCallout = function () {
  jQuery("#calloutModal").modal("show");
  enableButtons(false);
}
function enableButtons(flag) {
  if (flag) {
    jQuery('.controlButton').removeAttr('disabled')
  } else {
    jQuery('.controlButton').attr('disabled', 'disabled');
  }
}

jQuery(window).load(function () {
  var canvas = new fabric.Canvas('myCanvas', {
    preserveObjectStacking: true
  });
  fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';
  // Do some initializing stuff
// Do some initializing stuff
  fabric.Object.prototype.set({
    transparentCorners: false,
    padding: 7
  });

  jQuery('#uploadImg').click(function () {
    var elem = document.getElementById('theFile');
    if (elem && document.createEvent) {
      var evt = document.createEvent("MouseEvents");
      evt.initEvent("click", true, false);
      elem.dispatchEvent(evt);
    }
  });


//var openFile = function (event) {
  jQuery('#theFile').change(function () {
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function () {
      var dataURL = reader.result;
      jQuery('#imagePlaceholder').attr('src', dataURL);
      jQuery('.cutom-modal-overlay').show();
      setTimeout(function () {
        jQuery('.cutom-modal-overlay').hide();
        loadCanvas();
      }, 2000);

    };
    reader.readAsDataURL(input.files[0]);
  });


  var SelectObject = function (ObjectName, textValue) {
    canvas.getObjects().forEach(function (o) {
      if (o.id === ObjectName) {
        canvas.setActiveObject(o);
        var textToSet = textValue;
        if (o.item(0).getStrokeWidth() != 0) {
          textToSet = textValue.substring(0, 5);
        } else {
          textToSet = wrapCanvasText(textValue, canvas, maxWidth, 500);
        }
        o.item(1).setText(textToSet);
        canvas.renderAll();
      }
    })
  };

  var SelectForCircle = function (ObjectName, status) {
    canvas.getObjects().forEach(function (o) {
      if (o.id === parseInt(ObjectName)) {
        canvas.setActiveObject(o);
        if (status == 1) {
          o.item(0).setFill('white');
          o.item(0).setStroke('red');
          o.item(0).setStrokeWidth(2);
          o.item(1).setTop(o.item(1).getTop() + 20);
          if (o.item(1).getText().length > 5) {
            o.item(1).setText(o.item(1).getText().substring(0, 5));
          }
        } else {
          o.item(0).setStrokeWidth(0);
          o.item(0).setFill('rgba(0,0,0,0)');
          o.item(1).setTop(o.item(1).getTop() - 20);
          var textValue = jQuery('#label-' + o.id).val();
          o.setWidth(200);
          o.setHeight(200);
          maxWidth = 140;
          var textToSet = wrapCanvasText(textValue, canvas, maxWidth, 500);
          o.item(1).setText(textToSet);
        }
        canvas.renderAll();
      }
      switchColors(jQuery('.changeColor'));
      canvas.renderAll();
    })
  };

  jQuery('#addRow').click(function () {
    var uniqid = Date.now();
    var newId = uniqid;
    var content = '<tr>' +
            '<td width="60%"><input type="text" class="labeltext form-control input input-sm" id="label-' + newId + '" data-id="' + newId + '" value="Add Label"></td>' +
            '<td width="40%" style="padding: 14px"><input type="checkbox" class="checkbox" id="check-' + newId + '" data-id="' + newId + '" style="float: left;"></td>' +
            '</tr>';
    jQuery('#appendRow').append(content);
    jQuery('.labeltext').keyup(function () {
      canvas.getObjects().forEach(function (o) {
        if (o.id === newId) {
          canvas.setActiveObject(o);
          if (o.item(0).strokeWidth != 0) {
            jQuery('.labeltext').attr('maxlength', 5);
            o.setWidth(100);
            maxWidth = 50;
          } else {
            jQuery('.labeltext').attr('maxlength', 50);
            o.setWidth(200);
            o.setHeight(200);
            maxWidth = 140;
            //maxHeight = 400;
          }
          canvas.renderAll();
        }
      });
      var valueData = jQuery('#label-' + newId).val();
      SelectObject(newId, valueData, maxWidth);
    });

    jQuery(".checkbox").click(function () {
      var elementID = jQuery(this).attr("data-id");
      if (jQuery(this).is(":checked")) {
        SelectForCircle(elementID, 1);
      } else {
        SelectForCircle(elementID, 0);
      }
    });

    var line = makeLine([250, 50, 250, 200], newId);

    canvas.add(line);
    var circle = makeCircle(line.get('x1'), line.get('y1'), null, line);
    var text = makeText(line.get('x1'), line.get('y1'), null, line);
    var triangle = makeTriangle(newId, line.get('x2'), line.get('y2'), line);
    var g = new fabric.Group([circle, text], {
      id: newId,
      hasControls: false,
      hasBorders: false,
      lockScalingY: true,
      lockScalingX: true,
      lockRotation: true
    });
    g.line1 = null;
    g.line2 = line;
    // "add" rectangle onto canvas
    canvas.add(g, triangle);

    switchColors(jQuery('.changeColor'));
    canvas.on('object:moving', function (e) {
      boundObject(e);
    });
    canvas.renderAll();
  });

  function boundObject(e) {
    var obj = e.target;
    // if object is too big ignore
    if (obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width) {
      return;
    }
    obj.setCoords();
    // top-left  corner
    if (obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0) {
      obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top);
      obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left);
    }
    // bot-right corner
    if (obj.getBoundingRect().top + obj.getBoundingRect().height > obj.canvas.height || obj.getBoundingRect().left + obj.getBoundingRect().width > obj.canvas.width) {
      obj.top = Math.min(obj.top, obj.canvas.height - obj.getBoundingRect().height + obj.top - obj.getBoundingRect().top);
      obj.left = Math.min(obj.left, obj.canvas.width - obj.getBoundingRect().width + obj.left - obj.getBoundingRect().left);
    }

    var p = e.target;
    if (e.target.type != "triangle" && e.target.type != "image") {
      if ((p.item(0).get("type") == "group" && p.item(1).get("type") == "triangle") || (p.item(0).get("type") == "triangle" && p.item(1).get("type") == "triangle") || (p.item(0).get("type") == "group" && p.item(1).get("type") == "group")) {
        p.lockMovementX = true;
        p.lockMovementY = true;
      } else {
        p.lockMovementX = false;
        p.lockMovementY = false;
        p.line1 && p.line1.set({'x2': p.left, 'y2': p.top});
        p.line2 && p.line2.set({'x1': p.left, 'y1': p.top});
        canvas.renderAll();
      }
    } else {
      p.lockMovementX = false;
      p.lockMovementY = false;
      p.line1 && p.line1.set({'x2': p.left, 'y2': p.top});
      p.line2 && p.line2.set({'x1': p.left, 'y1': p.top});
      canvas.renderAll();
    }
  }


  function makeTriangle(newId, left, top, line1, line2, line3, line4) {
    var t = new fabric.Triangle({
      id: 'tri-' + newId,
      left: left,
      top: top,
      stroke: 'red',
      strokeWidth: 1,
      width: 15, height: 15,
      fill: 'yellow',
      selectable: true,
      originX: 'center',
      angle: 180,
      opacity: 0
    });

    t.hasControls = t.hasBorders = false;
    t.line1 = line1;
    t.line2 = line2;
    t.line3 = line3;
    t.line4 = line4;
    return t;

  }

  function makeLine(coords, newId) {
    return new fabric.Line(coords, {
      id: 'line-' + newId,
      fill: 'red',
      stroke: 'red',
      strokeWidth: 2,
      hasControls: false,
      hasBorders: false,
      selectable: false,
    });
  }

  function makeCircle(left, top, line1, line2, line3, line4) {
    var c = new fabric.Circle({
      left: left,
      top: top,
      strokeWidth: 0,
      radius: 30,
      fill: 'rgba(0,0,0,0)',
      stroke: '#666',
      hasControls: false,
      selectable: false
    });
    c.hasControls = c.hasBorders = false;

    c.line1 = line1;
    c.line2 = line2;
    c.line3 = line3;
    c.line4 = line4;

    return c;
  }

  function makeText(left, top, line1, line2, line3, line4) {
    var text = new fabric.IText('Add Label', {
      top: top - 20,
      left: left,
      lineHeight: 1,
      fontSize: 16,
      fontFamily: 'Sans-Serif',
      selectable: false,
      fill: 'red'
    });

    text.line1 = line1;
    text.line2 = line2;
    text.line3 = line3;
    text.line4 = line4;

    return text;
  }

  var selectedElement = null;
  canvas.on('object:selected', function (e) {
    canvas.getObjects().forEach(function (o) {
      if (o.get('type') == 'triangle') {
        o.setOpacity(0);
      }
    });
    e.target.setOpacity(1);
  });

  canvas.on('selection:cleared', function (e) {
    canvas.getObjects().forEach(function (o) {
      if (o.get('type') == 'triangle') {
        o.setOpacity(0);
      }
    });
  });

  jQuery('#export').click(function () {
    canvas.getObjects().forEach(function (o) {
      if (o.get('type') == 'triangle') {
        o.setOpacity(0);
      }
    });
    canvas.deactivateAll().renderAll();
    var canvas1 = document.getElementById('myCanvas');
    var dataURL = canvas1.toDataURL();
    jQuery('.cutom-modal-overlay').show();
    jQuery.ajax({url: htmlEditor.PXEmappedAPI.callOutAPI,
      async: true,
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      data: {image: dataURL},
      method: 'POST',
      success: function (result) {
        setImageToEditor(result.data.s3_path);
        jQuery('#appendRow').html("");
        jQuery('.changeColor').attr('checked', false);
        jQuery("#calloutModal").modal("hide");
        jQuery('.cutom-modal-overlay').hide();
        canvas.clear();
      }});
  });

  function setImageToEditor(imgUrl) {
    var oEditor = CKEDITOR.instances.editor1;
    var html = "<img src='" + imgUrl + "' alt='' width='' height= ''/>";
    var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
    oEditor.insertElement(newElement);
  }

  function changeColor(flag) {
    if (flag) {
      canvas.getObjects().forEach(function (o) {
        if (o.get('type') == 'group') {
          o.item(0).setStroke('black');
          o.item(1).setFill('black');
        } else {
          o.setStroke('black');
        }
      });
    } else {
      canvas.getObjects().forEach(function (o) {
        if (o.get('type') == 'group') {
          o.item(0).setStroke('red');
          o.item(1).setFill('red');
        } else {
          o.setStroke('red');
        }
      });
    }
    canvas.renderAll();
  }


  jQuery(".changeColor").click(function () {
    switchColors(this);
  });

  function switchColors(element) {
    if (jQuery(element).is(":checked")) {
      changeColor(1);
    } else {
      changeColor(0);
    }
  }

  function wrapCanvasText(t, canvas, maxW, maxH) {
    if (typeof maxH === "undefined") {
      maxH = 0;
    }
    var words = t.split(" ");
    var formatted = '';
    var lineHeight = t.lineHeight;

    // adjust for vertical offset
    var maxHAdjusted = maxH > 0 ? maxH - lineHeight : 0;
    var context = canvas.getContext("2d");


    context.font = t.fontSize + "px " + t.fontFamily;
    var currentLine = "";
    var breakLineCount = 0;

    for (var n = 0; n < words.length; n++) {

      var isNewLine = currentLine == "";
      var testOverlap = currentLine + ' ' + words[n];

      // are we over width?
      var w = context.measureText(testOverlap).width;

      if (w < maxW) { // if not, keep adding words
        currentLine += words[n] + ' ';
        formatted += words[n] + ' ';
      } else {
        // if this hits, we got a word that need to be hypenated
        if (isNewLine) {
          var wordOverlap = "";
          // test word length until its over maxW
          for (var i = 0; i < words[n].length; ++i) {

            wordOverlap += words[n].charAt(i);
            var withHypeh = wordOverlap + "-";

            if (context.measureText(withHypeh).width >= maxW) {
              // add hyphen when splitting a word
              withHypeh = wordOverlap.substr(0, wordOverlap.length - 2) + "-";
              // update current word with remainder
              words[n] = words[n].substr(wordOverlap.length - 1, words[n].length);
              formatted += withHypeh; // add hypenated word
              break;
            }
          }
        }
        n--; // restart cycle
        formatted += '\n';
        breakLineCount++;
        currentLine = "";
      }
      if (maxHAdjusted > 0 && (breakLineCount * lineHeight) > maxHAdjusted) {
        // add ... at the end indicating text was cutoff
        formatted = formatted.substr(0, formatted.length - 3) + "...\n";
        break;
      }
    }
    // get rid of empy newline at the end
    formatted = formatted.substr(0, formatted.length - 1);

    return formatted;
  }

  jQuery("#closeMainCallout").off().on("click", function () {
    jQuery('#appendRow').html("");
    jQuery('.changeColor').attr('checked', false);
    canvas.clear();
  });

  jQuery("#closeMainCalloutButton").off().on("click", function () {
    jQuery('#appendRow').html("");
    jQuery('.changeColor').attr('checked', false);
    canvas.clear();
  });


  jQuery("#getImgFromAsset").click(function () {
    var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    var assetPickerHeight = jQuery(window).height();
    var modalHeight = (50 * assetPickerHeight) / 100;
    jQuery("#imgCallout_modal").find('.modal-content-container').css({
      'max-height': modalHeight
    });
    the_editor.find(".image-setting").remove();
    if (htmlEditor.ImageModelBool) {
      htmlEditor.getMediaImagesforCallout(event.target);
    } else {
      swal("Image Uploading in Progress.");
    }

  });




  htmlEditor.getMediaImagesforCallout = function (imgobj) {
    jQuery("#spinner").show();

    htmlEditor.getMediaImagesArgs = arguments;
    htmlEditor.checkedImagePath = "";
    jQuery("#imgCallout_modal").modal("show");
    jQuery('#imgCallout_modal').find('.search_text').val('');
    jQuery("#imgCallout_modal .modal-content-container").html(""); //Clean the div for new imageset
    jQuery("#imgHt, #imgWidth, #imgAlt").val(""); //Empty last records
    jQuery("#imgCallout_modal #imgCont").attr("disabled", true);
    //Select the first tab by default 
    jQuery("#imgCallout_modal .modal-content").find(".active").removeClass("active");
    //jQuery(".nav-for-image.img-only").find("li").eq(0).addClass("active");
    //jQuery(".tab-content.image-tab-content .tab-pane").eq(0).addClass("active");
    jQuery("#imgCallout_modal .tab-pane").eq(0).addClass("active");
    htmlEditor.imageObj = imgobj; //the image element.
    htmlEditor.newImgObj = {};
    htmlEditor.newImgObj.id = [];
    htmlEditor.newImgObj.url = imgobj ? imgobj.src : ""; //Store the url of the selected image.
    jQuery("#imgCallout_modal .loc-or-global").addClass("active"); //Select the local by default.

    if (imgobj) { //initaial values inserted in the relevant fields.
      var ht = imgobj.height ? jQuery(imgobj).height() : "";
      var wid = imgobj.width ? jQuery(imgobj).width() : "";
      var alt = imgobj.alt ? imgobj.alt : "";
      jQuery("#imgHt").val(ht);
      jQuery("#imgWidth").val(wid);
      jQuery("#imgAlt").val(alt);
    }

    var enableConfirmBtn = function () { //Enable the confirm button if height width or alt text are changed for some image.
      if (htmlEditor.newImgObj.url.length) {
        setTimeout(function () { //Settimeout to get the instantaneous value
          var currht = parseInt(jQuery("#imgHt").val());
          jQuery("#imgHt").val(currht);
          var currwide = parseInt(jQuery("#imgWidth").val());
          jQuery("#imgWidth").val(currwide);
          if (currht > 0 && currwide > 0) {
            jQuery("#imgCallout_modal #imgCont").attr("disabled", false);
          } else {
            jQuery("#imgCallout_modal #imgCont").attr("disabled", true);
          }
        }, 100);
      }
    }
    var page_num = 1;
    var preserveRatio = true;

    var getRepositoryType = function () {
      //get the repository type, i.e, local / global.
      var fetchFrom = jQuery("#imgCallout_modal .loc-or-global.active").text();
      var fromUrl;
      var actives = jQuery("#imgCallout_modal .loc-or-global.active").length;
      if (actives > 1) {
        fromUrl = htmlEditor.PXEmappedAPI.getAllImages;
      } else {
        if (fetchFrom.toLowerCase() === "local") {
          fromUrl = htmlEditor.PXEmappedAPI.getLocalImagesAPI;
        } else if (fetchFrom.toLowerCase() === "global") {
          fromUrl = htmlEditor.PXEmappedAPI.getGlobalImagesAPI;
        } else if (fetchFrom.toLowerCase() === "all") {
          fromUrl = htmlEditor.PXEmappedAPI.getAllImages;
        }
      }
      return fromUrl; //Return the URL to be used.
    }

    htmlEditor.callImagesCallout(page_num, false, getRepositoryType());

    jQuery("#imgCallout_modal .loc-or-global").off().on("click", function () {
      //        if (!jQuery(this).hasClass("active")) {
      //            jQuery(".loc-or-global").removeClass("active");
      jQuery(this).toggleClass("active");
      htmlEditor.callImagesCallout(page_num, false, getRepositoryType());
      jQuery("#imgCallout_modal #imgCont").attr("disabled", true);
      //        }
    })

    jQuery('#imgCallout_modal').find('.search_btn').off('click').on('click', function () {
      var url = getRepositoryType();
      htmlEditor.callImagesCallout(page_num, true, url);
    });
    jQuery('#imgCallout_modal').find('.clear_text').off('click').on('click', function () {
      var url = getRepositoryType();
      jQuery('#imgCallout_modal').find('.search_text').val('');
      htmlEditor.callImagesCallout(page_num, false, url);
    });

    jQuery("#imgrestore").off("click").on("click", function () {
      htmlEditor.getOriginalSize(htmlEditor.newImgObj.url, function (dimension) {
        jQuery("#imgHt").val(dimension.height);
        jQuery("#imgWidth").val(dimension.width);
        jQuery("#imgCallout_modal #imgCont").attr("disabled", false);
      });

    });

    jQuery("#imglock").off("click").on("click", function () {
      jQuery(this).find("span.fa").toggleClass("fa-unlock");
      if (jQuery(this).find("span.fa").hasClass("fa-unlock")) {
        preserveRatio = false;
      } else {
        preserveRatio = true;
      }
    });

    jQuery("#imgHt").off("change").on("change", function () {
      if (preserveRatio) {
        htmlEditor.getOriginalSize(htmlEditor.newImgObj.url, function (dimension) {
          var htRatio = (dimension.height / (dimension.height + dimension.width));
          var widRatio = 1 - htRatio;
          var currHt = jQuery("#imgHt").val();
          var newWidth = parseInt(currHt * widRatio / htRatio);
          jQuery("#imgWidth").val(newWidth);
        });
      }
      enableConfirmBtn();
    })

    jQuery("#imgWidth").off("change").on("change", function () {
      if (preserveRatio) {
        htmlEditor.getOriginalSize(htmlEditor.newImgObj.url, function (dimension) {
          var htRatio = (dimension.height / (dimension.height + dimension.width));
          var widRatio = 1 - htRatio;
          var currWidth = jQuery("#imgWidth").val();
          var newHt = parseInt(currWidth * htRatio / widRatio);
          jQuery("#imgHt").val(newHt);
        });
      }
      enableConfirmBtn();
    })
    jQuery("#imgAlt").off("change").on("change", function () {
      enableConfirmBtn();
    })
  }


  htmlEditor.callImagesCallout = function (page, search_flag, url) {

    if (jQuery("#exwidgetModal").css('display') === 'block') {
      jQuery('li').find('a').each(function () {
        if (jQuery(this).attr('href') === '#configImage') {
          jQuery(this).hide();
        }
      });

    }
    if (jQuery("#exwidgetModal").css('display') === 'none') {
      jQuery('li').find('a').each(function () {
        if (jQuery(this).attr('href') === '#configImage') {
          jQuery(this).show();
        }
      });

    }
    jQuery("#imgCallout_modal .loc-or-global").attr("disabled", true);
    var num_of_items = 10;
    var searchText = window.btoa(jQuery('#imgCallout_modal').find('.search_text').val());
    var params = {
      "pageNumber": page,
      "itemsPerPage": num_of_items,
      "object_types": "104",
      "project_id": project_id,
      "search_text": searchText,
      "taxonomy_id": 0
    };
    jQuery.ajax({
      url: url,
      async: true,
      type: "GET",
      data: params, // object_type=104[image]
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      success: function (data) {
        var img_containers = jQuery("#imgCallout_modal .modal-content-container");
        var d = data.data;
        if (d != null && d.length > 0) {
          var str = "";
          jQuery(".paginate").show();
          for (var i = 0; i < d.length; i++) {
            var timeStamp = new Date().getTime(),
                    srcWithTimeStamp = d[i].asset_location + '?' + timeStamp;
            if (htmlEditor.checkedImagePath == d[i].asset_location) {
              str += '<div class="get-container"><div class="box-theme active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="' + srcWithTimeStamp + '" alt="" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '" ><a title=' + d[i].original_name + '></a>' + d[i].original_name + '</div></div></div>';
            } else {
              str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="' + srcWithTimeStamp + '" alt="" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '" ><a title=' + d[i].original_name + '></a>' + d[i].original_name + '</div></div></div>';
            }

          }
          jQuery("#imgCallout_modal #spinner").hide();
          str += '<div class="clearfix"></div>';
          if (page == 1) { //Pagination implementation
            var navStr = "";
            var total_images = data.totalAssets;
            var max_page = Math.ceil(total_images / num_of_items);
            //Pagination using plugin.
            jQuery('#imgCallout_modal').find(".paginate").bootpag({
              total: max_page, // total pages
              page: 1, // default page
              maxVisible: 10, // visible pagination
              leaps: true // next/prev leaps through maxVisible
            }).off("page").on("page", function (event, num) {
              htmlEditor.callImagesCallout(num, search_flag, url);
            });
          }
          img_containers.html(str);
        } else {
          if (page == 1)
            jQuery("#imgCallout_modal #spinner").hide();
          jQuery("#imgCallout_modal .modal-content-container").html("<p class='col-md-12'>No assets Found.</p>");
          jQuery(".paginate").hide();
        }
        htmlEditor.bindImageEventCallout();
        jQuery("#imgCallout_modal .loc-or-global").attr("disabled", false);
      },
      error: function () {
        swal({
          title: "Error!",
          text: "Some error occured while fetching the assets."
        });
      }
    });
  };



  htmlEditor.bindImageEventCallout = function () {
    var container = jQuery("#imgCallout_modal").find(".modal-content-container");
    container.find('.box-theme').off("click").on("click", function () {
      var is_global = jQuery(this).find('img').attr('class').split('_')[1];
      if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
        jQuery('.box-theme').removeClass('active');
        swal("Permission Denied!");
      } else {
        if (jQuery(this).hasClass('active')) {
          jQuery(this).removeClass('active');
          jQuery(this).find('.check-box').removeClass('checked');
          htmlEditor.checkedImagePath = "";
        } else {
          htmlEditor.checkedImagePath = jQuery(this).find('img').attr('src').split('?')[0];
          jQuery('.box-theme').removeClass('active');
          jQuery(this).addClass('active');
          jQuery(this).find('.check-box').addClass('checked');
        }
      }
      var imgUrl = jQuery('.box-theme.active').find("img").attr("src");
      var imgId = jQuery('.box-theme.active').find("img").attr("id");
      if (imgUrl !== undefined) {
        jQuery("#imgCallout_modal #imgCont").attr("disabled", false);
        htmlEditor.newImgObj.url = imgUrl;
        htmlEditor.newImgObj.id.length = 0;
        htmlEditor.newImgObj.id.push(imgId);
        jQuery("#imgrestore").trigger("click");
      } else {
        jQuery("#imgCallout_modal #imgCont").attr("disabled", true);
      }
    });
    //container.find('.box-theme').eq(htmlEditor.checkedIndex).trigger('click');

    function finalizedImgNSettingCallout(imgUrl) {
      var configHt = jQuery("#imgHt").val();
      var configWid = jQuery("#imgWidth").val();
      var configAlt = jQuery("#imgAlt").val();
      //Send the image back to the server to be mapped against local/global asset list
      jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
        type: 'POST',
        data: {
          'project_id': project_id,
          'is_global': '',
          'object_id': JSON.stringify(htmlEditor.newImgObj.id)
        },
        async: true,
        xhrFields: {
          withCredentials: true
        },
        success: function (data, textStatus, jqXHR) {

        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log("unable to put up the request")
        }
      })
      try {
        htmlEditor.imageObj.removeAttribute("data-cke-saved-src"); //Get rid of the previous saved image.
      } catch (exc) {

      }
      if (htmlEditor.imageObj) {
        htmlEditor.imageObj.src = imgUrl;
        //htmlEditor.imageObj.height = configHt;
        //htmlEditor.imageObj.width = configWid;
        jQuery(htmlEditor.imageObj).removeAttr('style')
        jQuery(htmlEditor.imageObj).attr(
                "height", configHt);
        jQuery(htmlEditor.imageObj).attr("width", configWid);

        htmlEditor.imageObj.alt = configAlt;
      } else {
        if (htmlEditor.getMediaImagesArgs[0] == false) {
          //Widget type behavior that sends widget response
          var imageRes = {
            'url': imgUrl,
            'alt': configAlt,
            'width': configWid,
            'height': configHt
          };
          setWidgetImages(imageRes);
        } else {
          //Default behavior that detects editor cursor
          var oEditor = CKEDITOR.instances.editor1;
          var html = "<img src='" + imgUrl + "' alt='" + configAlt + "' width='" + configWid + "' height= '" + configHt + "'/>";
          var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
          oEditor.insertElement(newElement);
        }
      }
    }

    jQuery("#imgCallout_modal #imgCont").off("click").on("click", function () {
      jQuery("#imgCallout_modal").modal("hide");
      jQuery('#imagePlaceholder').attr('crossOrigin', 'anonymous');
      var dataURL = jQuery('.box-theme.active').find("img").attr("src");
      jQuery('.cutom-modal-overlay').show();
      jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.callOutImageProcess,
        type: 'POST',
        data: {
          'image_url': dataURL
        },
        async: true,
        xhrFields: {
          withCredentials: true
        },
        success: function (data, textStatus, jqXHR) {
          if (data.status == 200) {
            jQuery('#imagePlaceholder').attr('src', data.data.base64);
            setTimeout(function () {
              jQuery('.cutom-modal-overlay').hide();
              loadCanvas();
            }, 2000);
          } else {
            jQuery('.cutom-modal-overlay').hide();
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log("unable to put up the request")
        }
      });
    });

    jQuery("#imgCallout_modal #closeCallout").off().on("click", function () {
      jQuery("#imgCallout_modal").modal("hide");
    });
  }


  function loadCanvas() {
    // load image from data url
    var imgElement = document.getElementById('imagePlaceholder');
    var imgInstance = new fabric.Image(imgElement, {
      crossOrigin: 'anonymous',
      left: canvas.width / 2,
      top: canvas.height / 2,
      angle: 0,
      opacity: 1,
    });
    imgInstance.filters.push(
            new fabric.Image.filters.Resize({
              resizeType: 'hermite',
              scaleX: 0.6,
              scaleY: 0.6
            })
            );
    imgInstance.applyFilters();
    canvas.add(imgInstance);
    canvas.on('object:moving', function (e) {
      boundObject(e);
    });
    canvas.renderAll();
    enableButtons(true);
  }

});






