var classSet = {
    OL: [''],
    UL: [''],
    ASIDE: [''],
    SECTION: [''],
    FIGURE: ['lm_img_lightbox']
};

var epubTypeSet = {
    OL: ['answerlist', 'biblioentrylist', 'rearnotelist', 'keywordlist', 'objectivelist', 'ol_lower-alpha', 'ol_lower-roman', 'ol_upper-alpha', 'ol_upper-roman', 'choices', 'matchlist', 'practicelist', 'staticlist', 'steplist', 'tocentrylist'],
    UL: ['answerlist', 'biblioentrylist', 'rearnotelist', 'index-entry-list', 'keywordlist', 'objectivelist', 'choices', 'matchlist', 'practicelist', 'steplist', 'ul_none'],
    ASIDE: ['annotation', 'annotation_instructor', 'footnote', 'footnote_symboled', 'pullquote', 'sidebar', 'vignette', 'example'],
    SECTION: ['answersetdiv', 'appendix', 'backmatter', 'bibliodiv', 'bibliography', 'bodymatter', 'card', 'case', 'chapter', 'creditset', 'creditsetdiv', 'conclusion', 'copyright-page', 'dedicationset', 'dedication', 'drama', 'example', 'feature', 'frontmatter', 'glossary', 'glossdiv', 'halftitlepage', 'index-group', 'interstitial', 'introduction', 'keywordset', 'listgroup', 'metadata', 'module', 'name-index', 'notice', 'objectiveset', 'part', 'practice', 'practicediv', 'preface', 'procedure', 'rearnotes', 'rearnotesdiv', 'scene', 'seriespage', 'subject-index', 'titlepage', 'volume']
}