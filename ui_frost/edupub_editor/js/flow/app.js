/*global angular */
'use strict';

/**
 * The main app module
 * @name app
 * @type {angular.Module}
 */
var app = angular.module('app', ['flow', 'ngSanitize', 'ui.select2', 'ivh.treeview', 'mgcrea.ngStrap.datepicker', 'mgcrea.ngStrap.timepicker', 'ui.bootstrap'], function($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.withCredentials = true;

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function(obj) {
        var query = '',
            name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            } else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            } else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});


app.config(['flowFactoryProvider', 'ivhTreeviewOptionsProvider', function(flowFactoryProvider, ivhTreeviewOptionsProvider) {
    flowFactoryProvider.defaults = {
        target: 'upload.php',
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1
    };
    flowFactoryProvider.on('catchAll', function(event) {
        //console.log('catchAll', arguments);
    });
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
    /**
     *  default setup of ivh tree view config
     */
    ivhTreeviewOptionsProvider.set({
        idAttribute: 'id',
        labelAttribute: 'title',
        defaultSelectedState: false,
        validate: true
    });

}]);

app.run(function($rootScope, $templateCache, $sce, $compile) {

});
app.controller('UploadImageController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = { taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset' };
    var loc = jQuery("#uploadImage");
    //    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "image/jpeg" || fileType === "image/gif" || fileType === "image/png" || fileType === "image/bmp") {
            jQuery("#img_modal").find(".file-upload-wrapper").hide();
            jQuery("#img_modal").find(".fileupload input").attr("disabled", true);
            jQuery("#img_modal").find(".text").show().text("0%");
        } else {
            swal("Please upload images only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        htmlEditor.ImageModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#img_modal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#img_modal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#img_modal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#img_modal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#img_modal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.ImageModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#img_modal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#img_modal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#img_modal").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //var location = response.location;
        var location = response.asset_location
        htmlEditor.newImgObj.url = location; //Insert the image in the editor.
        $timeout(function() {
            jQuery("#imgCont").trigger("click");
            jQuery("#img_modal").find(".loader-spiner").css("border", "0");
            jQuery("#img_modal").find(".fileupload input").attr("disabled", false);
            jQuery("#img_modal").find(".file-upload-wrapper").show();
            jQuery("#img_modal").find(".text").text("").hide();
            jQuery("#img_modal").modal("hide");
        }, 2000)

        //            htmlEditor.callImages(1, false, htmlEditor.imageObj);     //Populate the new image in asset lib.

        //        $timeout(function() {
        //            loc.find(".progress.image-loding-button").parent().remove()
        //        }, 1000);
    }
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////

app.controller('UploadAdController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = { taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset' };
    var loc = jQuery("#uploadAd");
    //    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "") {
            fileType = (fileObj.file.name).slice((fileObj.file.name).lastIndexOf('.'), (fileObj.file.name).length);
        }
        if (fileType === ".vtt") {
            jQuery("#uploadAd").find(".file-upload-wrapper").hide();
            jQuery("#uploadAd").find(".fileupload input").attr("disabled", true);
            jQuery("#uploadAd").find(".text").show().text("0%");
        } else {
            swal("Please upload .vtt type files only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        htmlEditor.ImageModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#uploadAd").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#uploadAd").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadAd").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#uploadAd").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadAd").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.ImageModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#uploadAd").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadAd").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#uploadAd").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //var location = response.location;
        var location = response.asset_location
        htmlEditor.newVidObj.url = location; //Insert the image in the editor.
        $timeout(function() {
            jQuery(".audio_conf").trigger("click");
            jQuery("#uploadAd").find(".loader-spiner").css("border", "0");
            jQuery("#uploadAd").find(".fileupload input").attr("disabled", false);
            jQuery("#uploadAd").find(".file-upload-wrapper").show();
            jQuery("#uploadAd").find(".text").text("").hide();
            jQuery("#audioModal").modal("hide");
        }, 2000);
    }
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////
app.controller('UploadVttController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = { taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset' };
    var loc = jQuery("#uploadVtt");
    //    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "") {
            fileType = (fileObj.file.name).slice((fileObj.file.name).lastIndexOf('.'), (fileObj.file.name).length);
        }
        if (fileType === ".vtt") {
            jQuery("#uploadVtt").find(".file-upload-wrapper").hide();
            jQuery("#uploadVtt").find(".fileupload input").attr("disabled", true);
            jQuery("#uploadVtt").find(".text").show().text("0%");
        } else {
            swal("Please upload .vtt type files only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        htmlEditor.ImageModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#uploadVtt").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#uploadVtt").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadVtt").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#uploadVtt").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadVtt").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.ImageModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#uploadVtt").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadVtt").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#uploadVtt").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //var location = response.location;
        var location = response.asset_location
        htmlEditor.newVidObj.url = location; //Insert the image in the editor.
        $timeout(function() {
            jQuery(".audio_conf").trigger("click");
            jQuery("#uploadVtt").find(".loader-spiner").css("border", "0");
            jQuery("#uploadVtt").find(".fileupload input").attr("disabled", false);
            jQuery("#uploadVtt").find(".file-upload-wrapper").show();
            jQuery("#uploadVtt").find(".text").text("").hide();
            jQuery("#videoModal").modal("hide");
        }, 2000);
    }
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////
app.controller('UploadAudioController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = { taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset' };
    var loc = jQuery("#uploadAudio");
    //    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "") {
            fileType = (fileObj.file.name).slice((fileObj.file.name).lastIndexOf('.'), (fileObj.file.name).length);
        }
        if (fileType === "audio/mp3" || fileType === ".mp3") {
            jQuery("#uploadAudio").find(".file-upload-wrapper").hide();
            jQuery("#uploadAudio").find(".fileupload input").attr("disabled", true);
            jQuery("#uploadAudio").find(".text").show().text("0%");
        } else {
            swal("Please upload .mp3 type files only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        htmlEditor.ImageModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#uploadAudio").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#uploadAudio").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadAudio").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#uploadAudio").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadAudio").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.ImageModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#uploadAudio").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#uploadAudio").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#uploadAudio").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //var location = response.location;
        var location = response.asset_location
        htmlEditor.newVidObj.url = location; //Insert the image in the editor.
        $timeout(function() {
            jQuery(".audio_conf").trigger("click");
            jQuery("#uploadAudio").find(".loader-spiner").css("border", "0");
            jQuery("#uploadAudio").find(".fileupload input").attr("disabled", false);
            jQuery("#uploadAudio").find(".file-upload-wrapper").show();
            jQuery("#uploadAudio").find(".text").text("").hide();
            jQuery("#audioModal").modal("hide");
        }, 2000);
    }
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////
app.controller('UploadVideoController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = { taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset' };
    var loc = jQuery("#uploadVid");
    //    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "") {
            fileType = (fileObj.file.name).slice((fileObj.file.name).lastIndexOf('.'), (fileObj.file.name).length);
        }
        if (fileType === "video/mp4") {
            jQuery("#videoModal").find(".file-upload-wrapper").hide();
            jQuery("#videoModal").find(".fileupload input").attr("disabled", true);
            jQuery("#videoModal").find(".text").show().text("0%");
        } else {
            swal("Please upload .mp4 type files only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile, type) {
        htmlEditor.VideoModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                if (type === "video") {
                    jQuery("#videoModal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "audio") {
                    jQuery("#audioModal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "vtt") {
                    jQuery("#uploadVtt").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                }
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                if (type === "video") {
                    jQuery("#videoModal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#videoModal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "audio") {
                    jQuery("#audioModal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#audioModal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "vtt") {
                    jQuery("#uploadVtt").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#uploadVtt").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                }
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                if (type === "video") {
                    jQuery("#videoModal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#videoModal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "audio") {
                    jQuery("#audioModal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#audioModal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "vtt") {
                    jQuery("#uploadVtt").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#uploadVtt").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                }

            } else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.VideoModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                if (type === "video") {
                    jQuery("#videoModal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#videoModal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "audio") {
                    jQuery("#audioModal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#audioModal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                } else if (type === "vtt") {
                    jQuery("#uploadVtt").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                    jQuery("#uploadVtt").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                }

            }
            if (type === "video") {
                jQuery("#videoModal").find(".text").html(progress + "%");
            } else if (type === "audio") {
                jQuery("#audioModal").find(".text").html(progress + "%");
            } else if (type === "vtt") {
                jQuery("#uploadVtt").find(".text").html(progress + "%");
            }


        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result, type) {
        if (result != 'Error:The file type is not supported') {
            var response = JSON.parse(result);
            var location = response.asset_location
            htmlEditor.newVidObj.url = location;
        } else {
            swal('Error:The file type is not supported');
        }

        //var location = response.location;


        //

        $timeout(function() {
            //as video modal controller has been used for audio modal,and vtt upload all the audio modal and vtt upload modification is also written here.
            if (type === 'vtt') {
                jQuery(".video_conf").trigger("click");
                jQuery("#uploadVtt").find(".loader-spiner").css("border", "0");
                jQuery("#uploadVtt").find(".fileupload input").attr("disabled", false);
                jQuery("#uploadVtt").find(".file-upload-wrapper").show();
                jQuery("#uploadVtt").find(".text").text("").hide();
                jQuery("#uploadVtt").modal("hide");
            } else if (type === 'video') {
                jQuery(".video_conf").trigger("click");
                jQuery("#videoModal").find(".loader-spiner").css("border", "0");
                jQuery("#videoModal").find(".fileupload input").attr("disabled", false);
                jQuery("#videoModal").find(".file-upload-wrapper").show();
                jQuery("#videoModal").find(".text").text("").hide();
                jQuery("#videoModal").modal("hide");
            } else if (type === 'audio') {
                jQuery(".audio_conf").trigger("click");
                jQuery("#audioModal").find(".loader-spiner").css("border", "0");
                jQuery("#audioModal").find(".fileupload input").attr("disabled", false);
                jQuery("#audioModal").find(".file-upload-wrapper").show();
                jQuery("#audioModal").find(".text").text("").hide();
                jQuery("#audioModal").modal("hide");
            }
        }, 2000);
    }
});

app.controller('UploadGadgetController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'asset';
    $scope.$flow.opts.query = { taxonomy_id: 0, project_id: project_id, project_type_id: project_type_id, flowRepoDir: 'asset' };
    var loc = jQuery("#uploadGadget");
    //    var fileDet = loc.find(".file-upload-details");
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "application/x-zip-compressed" || fileName.slice(-3) === 'zip') {
            jQuery("#iframeModal").find(".file-upload-wrapper").hide();
            jQuery("#iframeModal").find(".fileupload input").attr("disabled", true);
            jQuery("#iframeModal").find(".text").show().text("0%");
        } else {
            swal("Please upload .zip files only.");
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#iframeModal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#iframeModal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#iframeModal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#iframeModal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#iframeModal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 75 && progress <= 100) {
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#iframeModal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#iframeModal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#iframeModal").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);
        //        var location = response.location;
        var asset_id = response.asset_id;
        htmlEditor.newGadgetObj.gadgetId.length = 0;
        htmlEditor.newGadgetObj.gadgetId.push(asset_id);
        jQuery('#iframeTerm').val("");
        $timeout(function() {
            jQuery("#iframeModal .iframe_conf").trigger("click");
            jQuery("#iframeModal").find(".loader-spiner").css("border", "0");
            jQuery("#iframeModal").find(".fileupload input").attr("disabled", false);
            jQuery("#iframeModal").find(".file-upload-wrapper").show();
            jQuery("#iframeModal").find(".text").text("").hide();
            jQuery("#iframeModal").modal("hide");
        }, 2000);
    }
});

app.controller('UploadFlashcarddataController', function($scope, $sce, $compile, $timeout) {
    $scope.$flow.defaults.flowRepoDir = 'flashcard_data';
    $scope.$flow.opts.query = {
        project_id: project_id,
        project_type_id: project_type_id,
        flowRepoDir: 'flashcard_data'
    };
    var fileName = "";
    var fileType = "";
    $scope.getFile = function(flowObj, fileObj) {
        $scope.$flow.opts.query = {
            project_id: project_id,
            project_type_id: project_type_id,
            flowRepoDir: 'flashcard_data',
            widget_type: htmlEditor.newFlashcardDataObj.data_widget_type,
            repo_id: htmlEditor.newFlashcardDataObj.id,
            widget_id: htmlEditor.newFlashcardDataObj.data_widget_id,
            object_id: node_id,
            access_token: access_token
        };
        fileName = fileObj.name;
        fileType = fileObj.file.type;
        if (fileType === "text/plain") {
            jQuery("#widget_import_modal").find(".file-upload-wrapper").hide();
            jQuery("#widget_import_modal").find(".fileupload input").attr("disabled", true);
            jQuery("#widget_import_modal").find(".text").show().text("0%");
        } else {
            swal({ title: "Alert!", text: "Please upload txt file only." });
            return false;
        }
    };

    $scope.showProgress = function(objFlow, objFile) {
        htmlEditor.ImageModelBool = false;
        var progress = Math.ceil(objFlow.progress() * 100);

        function renderProgress(progress) {
            progress = Math.floor(progress);
            if (progress < 25) {
                var angle = -90 + (progress / 100) * 360;
                jQuery("#widget_import_modal").find(".animate-0-25-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 25 && progress < 50) {
                var angle = -90 + ((progress - 25) / 100) * 360;
                jQuery("#widget_import_modal").find(".animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#widget_import_modal").find(".animate-25-50-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 50 && progress < 75) {
                var angle = -90 + ((progress - 50) / 100) * 360;
                jQuery("#widget_import_modal").find(".animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#widget_import_modal").find(".animate-50-75-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            } else if (progress >= 75 && progress <= 100) {
                if (progress == 100) {
                    htmlEditor.ImageModelBool = true;
                }
                var angle = -90 + ((progress - 75) / 100) * 360;
                jQuery("#widget_import_modal").find(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b").css("transform", "rotate(0deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
                jQuery("#widget_import_modal").find(".animate-75-100-b").css("transform", "rotate(" + angle + "deg)").find(".loader-spiner").css("border", "5px solid #3F51B5");
            }
            jQuery("#widget_import_modal").find(".text").html(progress + "%");

        }
        renderProgress(progress);
    };
    $scope.getResponse = function(fl, file, result) {
        var response = JSON.parse(result);

        if (response.status === 200) {
            //var location = response.location;
            var json_file_path = response.json_file_path;
            htmlEditor.newFlashcardDataObj.data_path = json_file_path; //Insert the image in the editor.
            jQuery(htmlEditor.newFlashcardDataObj.frame).attr("data-path", json_file_path);
        } else {
            //swal(response.message);
            swal({ title: "Alert!", text: response.message });
        }

        $timeout(function() {
            jQuery("#flashCont").trigger("click");
            jQuery("#widget_import_modal").find(".loader-spiner").css("border", "0");
            jQuery("#widget_import_modal").find(".fileupload input").attr("disabled", false);
            jQuery("#widget_import_modal").find(".file-upload-wrapper").show();
            jQuery("#widget_import_modal").find(".text").text("").hide();
            jQuery("#widget_import_modal").modal("hide");
        }, 2000)
    }
});