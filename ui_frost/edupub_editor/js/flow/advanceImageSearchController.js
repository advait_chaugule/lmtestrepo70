'use strict';
app.controller('advancedImageSearchController', ['$scope', '$rootScope', '$log', '$filter', '$controller', '$compile', '$window', 'ivhTreeviewMgr', 'advancedSearchService', function($scope, $rootScope, $log, $filter, $controller, $compile, $window, ivhTreeviewMgr, advancedSearchService) {
    $scope.advanced_search = {};
    $scope.taxonomyStatus = 1001;
    advancedSearchService.getTaxonomy($scope, $scope.node_id).then(function(data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.children)) {
                $scope.advanced_search.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
            }
        }

    });
    var tags = [];
    var object_tags = [];
    $scope.advanced_search.tag = {
        multiple: true,
        data: tags
    };
    $scope.asset_filter = { "images": true, "videos": false, "other": false, "gadgets": false };
    $scope.asset_local_global = { "local": false, "global": false };
    $scope.project_id = project_id;
    $scope.node_id = 0;
    $scope.advanced_search.enableSearch = false;
    $scope.advanced_search.title = '';
    $scope.advanced_search.active = false;
    $scope.advanced_search.isCollapsed = false;
    $scope.advanced_search.heading = 'Advanced Search Result:';
    $scope.advanced_search.key_data = {};
    $scope.advanced_search.value_data = {};
    $scope.advanced_search.totalKeyArea = 0;
    $scope.advanced_search.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    advancedSearchService.getTagsByObject($scope, $scope.node_id).then(function(data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.tags)) {
                angular.forEach(data.data.tags, function(value, key) {
                    tags[key] = { id: value.id, text: value.text };
                });
                $scope.advanced_search.tag.data = tags;
                if (angular.isDefined(data.data.object_tags)) {
                    angular.forEach(data.data.object_tags, function(value, key) {
                        object_tags[key] = { id: value.id, text: value.text };
                    });
                    $scope.advanced_search.tagValue = object_tags;
                }
            }
        }

    });

    // get owners
    var owners = [];
    var object_owners = [];

    $scope.advanced_search.owner = {
        multiple: true,
        data: owners
    };

    advancedSearchService.getUsers($scope).then(function(data) {
        if (data.status == 200) {
            if (data.data != null) {
                angular.forEach(data.data, function(value, key) {
                    owners[key] = { id: value.id, text: value.username };
                });
                $scope.advanced_search.owner.data = owners;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    advancedSearchService.getKeyValuesByObject($scope, $scope.node_id).then(function(data) {
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values)) {
                angular.forEach(data.data.key_values, function(value) {
                    $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                });

                setTimeout(function() {
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").empty();
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                    // angular.element("#selectVid .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                    // angular.element("#selectGadget .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));

                }, 800);
            }
        }
    });

    $scope.applyAdvancedSearch = function() {
        $scope.getAdvancedSearchFields();
        htmlEditor.callImages(1, false, serviceEndPoint + 'api/v1/asset-search');
        angular.element(document.getElementsByClassName('search_text')[0]).val("");
    };
    $scope.onChangeKeyUpdateValue = function(key) {
        var keyId = $scope.advanced_search.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function(value) {
            if (value.id == keyId) {
                if (value.key_type == 'select_list') {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function(valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    var valueInput = '<input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    var valueInput = '<input type="text" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="advanced_search.value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };
    $scope.addKeyValue = function() {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        // angular.element("#selectImg .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        // angular.element("#selectGadget .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };
    $scope.removeKeyValue = function(keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.advanced_search.key_data['key_' + keyArray[1]];
        delete $scope.advanced_search.value_data['key_' + keyArray[1]];
        if (angular.element(".advanced-search-key-values-area").length == 0) {
            $scope.advanced_search.totalKeyArea = 1;
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
            // angular.element("#selectVid .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
            // angular.element("#selectGadget .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function(key) {
        var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };

    $scope.getAdvancedSearchFields = function() {
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.active = true;
        $scope.advanced_search.isCollapsed = false;
        htmlEditor.advanced_search.enableSearch = true;
        htmlEditor.advanced_search.active = true;
        htmlEditor.advanced_search.params = {};
        htmlEditor.advanced_search.params.advanced_search_enable = $scope.advanced_search.active;
        htmlEditor.advanced_search.params.tags = JSON.stringify($scope.advanced_search.tagValue);
        htmlEditor.advanced_search.params.owners = JSON.stringify($scope.advanced_search.ownerValue);
        htmlEditor.advanced_search.params.key_data = JSON.stringify($scope.advanced_search.key_data);
        htmlEditor.advanced_search.params.value_data = JSON.stringify($scope.advanced_search.value_data);
        htmlEditor.advanced_search.params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
        htmlEditor.advanced_search.params.advanced_search_title = window.btoa($scope.advanced_search.title);
        htmlEditor.advanced_search.params.asset_filter = JSON.stringify($scope.asset_filter);
        // return htmlEditor.advanced_search.params;
        // htmlEditor.callImages(1, false, serviceEndPoint + 'api/v1/asset-search');
        // // blank normal search text value
        // angular.element(document.getElementsByClassName('search_text')[0]).val("");
    };

    $scope.setLocalGlobal = function() {
        var fetchFrom = jQuery("#img_modal .loc-or-global.active").text();
        var actives = jQuery("#img_modal .loc-or-global.active").length;
        if (actives > 1) {
            $scope.asset_local_global.local = true;
            $scope.asset_local_global.global = true;
        } else {
            if (fetchFrom.toLowerCase() === "local") {
                $scope.asset_local_global.local = true;
                $scope.asset_local_global.global = false;
            } else if (fetchFrom.toLowerCase() === "global") {
                $scope.asset_local_global.local = false;
                $scope.asset_local_global.global = true;
            } else if (fetchFrom.toLowerCase() === "all") {
                $scope.asset_local_global.local = true;
                $scope.asset_local_global.global = true;
            }
        }
    }

    $scope.clearSearch = function() {
        //debugger;
        $scope.advanced_search.active = false;
        //$scope.searchModel.search_text = '';
        $scope.advanced_search.enableSearch = false;
        angular.element(document.getElementsByClassName('search_text')[0]).val("");
        htmlEditor.advanced_search.enableSearch = false;
        htmlEditor.advanced_search.active = false;
        htmlEditor.callImages(1, false, htmlEditor.getRepositoryType());
        // htmlEditor.getMediaVideo(1, false, htmlEditor.getRepositoryTypeForVideo('video'), 'forVideo');
    };

    $scope.toggleAdvanceSearch = function() {
        if ($scope.advanced_search.isCollapsed) {
            $scope.advanced_search.isCollapsed = false;
            if (htmlEditor.advanced_search.active) {
                $scope.advanced_search.enableSearch = true;
            }
        } else {
            $scope.advanced_search.isCollapsed = true;
            $scope.advanced_search.enableSearch = false;
        }
    };

    $scope.applyAdvancedClear = function() {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").html('');
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        // angular.element("#selectVid .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        // angular.element("#selectGadget .advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        $scope.advanced_search.tagValue = [];
        $scope.advanced_search.ownerValue = [];
        $scope.advanced_search.title = '';
        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
    };

    $scope.generateAdvanceSearchTerm = function() {
        var searchTerm = [];
        var tagList = $scope.advanced_search.tagValue;
        if (tagList.length > 0) {
            angular.forEach(tagList, function(tag) {
                searchTerm.push(tag.text);
            });
        }
        var ownerList = $scope.advanced_search.ownerValue;
        if (ownerList.length > 0) {
            angular.forEach(ownerList, function(owner) {
                searchTerm.push(owner.text);
            });
        }
        var taxonomyList = $scope.selectedTaxonomy;
        if (taxonomyList.length > 0) {
            angular.forEach(taxonomyList, function(taxonomy) {
                searchTerm.push(taxonomy);
            });
        }

        if ($scope.advanced_search.title != '') {
            searchTerm.push($scope.advanced_search.title);
        }

    };

    $scope.selectedTaxonomy = [];
    $scope.selectedTaxonomyIds = [];

    $scope.$watch('advanced_search.taxonomy', function(newValues) {
        $scope.selectedTaxonomy.length = 0;
        $scope.selectedTaxonomyIds.length = 0;
        angular.forEach(newValues, function(item) {
            if (item.selected == true) {
                $scope.selectedTaxonomy.push(item.title);
                $scope.selectedTaxonomyIds.push(item.id);
            }
        });
    }, true);
}]);