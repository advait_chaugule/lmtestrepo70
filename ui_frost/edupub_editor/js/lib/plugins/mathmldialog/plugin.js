/**
 * ustom plugin for mathml insertion
 */
CKEDITOR.plugins.add('mathmldialog', {
  icons: 'MathML',
  init: function (editor) {
    CKEDITOR.dialog.add("mathmlDialog", this.path + "dialogs/mathmldialog.js");
    editor.addCommand('openMathmlDialog', new CKEDITOR.dialogCommand('mathmlDialog'));

    editor.ui.addButton('MathmlDialog', {
      label: 'Insert Mathml',
      command: 'openMathmlDialog',
      toolbar: 'insert',
      icon: this.path + 'images/MathML.png'
    });
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;
      if (editor.elementPath().contains('math') == null) {
        var tagName = '';
      } else {
        var tagName = editor.elementPath().contains('math').getName();
      }
      if (tagName == 'math') {
        var tagData = editor.elementPath().contains('math').getHtml();
        evt.data.dialog = 'mathmlDialog';
      }
    });
    editor.on('mode', function (evt) {
      if (evt.editor.document == undefined) {
        return false;
      } else {
        var elements = evt.editor.document.getElementsByTag('math');
        for (var i = 0; i < elements.count(); ++i) {
          elements.getItem(i).removeClass('math_edit');
        }
        htmlEditor.editorEvents();
      }
    });
  }
});