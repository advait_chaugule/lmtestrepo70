CKEDITOR.plugins.add('createmath', {
    icons: 'createmath',
    init: function(editor) {
        editor.addCommand('createEqn', {
            exec: function() {
                htmlEditor.createMath();
            }
        });
        editor.ui.addButton('createmath', {
            label: 'Create Math Equations',
            command: 'createEqn',
            toolbar: 'insert'
        });
    }
});