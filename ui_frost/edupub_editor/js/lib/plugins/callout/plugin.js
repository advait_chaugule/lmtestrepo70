CKEDITOR.plugins.add('callout', {
    icons: 'customimage',
    init: function(editor) {
        editor.addCommand('insertCallout', {
            exec: function() {
                htmlEditor.getCallout();
            }
        });
        editor.ui.addButton('insertCallout', {
            label: 'Callout',
            command: 'insertCallout',
            toolbar: 'insert'
        });
    }
});