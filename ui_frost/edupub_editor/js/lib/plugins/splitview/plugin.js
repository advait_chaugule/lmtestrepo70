CKEDITOR.plugins.add('splitview', {
    icons: 'splitview',
    init: function(editor) {
        editor.addCommand('splitView', {
            exec: function() {
                htmlEditor.splitView();
            }
        });
        editor.ui.addButton('splitview', {
            label: 'Split Editor and Source',
            command: 'splitView',
            toolbar: 'document'
        });
    }
});