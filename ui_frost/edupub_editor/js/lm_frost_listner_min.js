function getWidgetImages() {
    htmlEditor.getMediaImages(!1)
}
function setWidgetImages() {
    var t = {type: "IMAGE", image_detail: arguments[0]}, e = "widFrame", a = jQuery("body").find("#" + e)[0];
    a.contentWindow.postMessage(t, "*")
}
!function () {
    function t(t) {
     
        s = f.getInstance();
    
        var i = t.data;
        var n = (i.type === undefined) ? '' : i.type.toUpperCase();
        if ("GET" === n && !htmlEditor.reqFromClonedWidget/* && s.find("iframe.exwidget").length >= o*/)
            o++;
        else if ("GET" === n && htmlEditor.reqFromClonedWidget) {
            s = jQuery("#widget_copy_modal");
            var c = s.find("iframe").attr("data-path"), m = {type: "GET", frame_id: "widClonePrvFrame", data_path: c};
            d("GET", "widClonePrvFrame", m)
        } else if ("PUT" === n)
            try {
                var c = htmlEditor.thisFrame.attr("data-path"), l = htmlEditor.thisFrame.attr("id"), m = {type: "GET", frame_id: l, data_path: c, images: h};
                d("POST", "widFrame", m)
            } catch (p) {
            }
        else
            "CLOSE" === n ? e() : "SIZE" === n && (g = [], g.push({height: i.height, width: i.width})/*, r(i.frame_id, 0)*/);
        switch (n) {
            case"GET":
                var w = i.height, v = i.width;
            g.push({height: w, width: v})/*, s.find("iframe.exwidget").length === o && u(g);*/
                break;
            case"POST":
                var E = htmlEditor.thisFrame.attr("widget-id") ? htmlEditor.thisFrame.attr("widget-id") : htmlEditor.thisFrame.attr("data-widget-id");
                a(htmlEditor.thisFrame.attr("id"), i.dataset, E);
                break;
            case"IMAGE":
                getWidgetImages()
            }
    }
    function e() {
        jQuery("#exwidgetModal").find("button.close").trigger("click")
    }
    function a(t, e, a) {
        var r = i(t, e, a);
        jQuery(".cke_wysiwyg_frame").contents().find("#" + t).attr("data-path", r);
        var n = {frame_id: t, data_path: r};
        d("POST", "widFrame", n)
    }
    function i(t, a, i) {
        var r = "", d = a, n = {project_id: project_id, project_type_id: project_type_id, ex_widgets: d, repo_id: t, object_id: node_id, widget_id: i};
        return jQuery.ajax({url: htmlEditor.PXEmappedAPI.saveWidgetDataAPI, async: !1, method: "POST", data: n, xhrFields: {withCredentials: !0}, crossDomain: !0, success: function (t) {
                var a = "200" == t.status;
                r = t.json_file_path, a && e()
            }, error: function () {
                alert("Error occured..Retry")
            }}), r
    }
//    function r(t, e) {
//        var a = t, i = s.find("#" + a)[0], r = Number(g[e].height) + 40;
//        try {
//            m > r ? i.height = r : i.height = m
//        } catch (d) {
//        }
//    }
    function d(t, e, a) {
        if ("POST" === t) {
            var i = e, r = jQuery("body").find("#" + i)[0];
            r.contentWindow.postMessage(a, "*")
        } else if ("GET" === t) {
            var i = e, r = s.find("#" + i)[0];
            r.contentWindow.postMessage(a, "*")
        }
    }
    function n(t) {
        var e = [];
        return s.find("iframe.exwidget").each(function () {
            var a = jQuery(this).attr(t);
            e.push(a)
        }), e
    }
    var o = 0, s = "", h = [], c = "file" === parent.location.href.substring(0, 4), g = [], m = 610, f = function () {
        return{getInstance: function () {/*var t = jQuery("#testCont").length; return t?jQuery("#testCont").contents():jQuery("body")*/
            }, getAbsolutePath: function (t) {
                  if(e.protocol===undefined){
                    return t;
                }
                var e = document.createElement("a");
                return e.href = t, e.protocol + "//" + e.host + e.pathname + e.search + e.hash
            }}
    }();
    window.addEventListener ? window.addEventListener("message", t, !1) : window.attachEvent("onmessage", t);
    var u = function () {
        var t = n("id"), e = n("data-path");
        if (c)
            for (var a = 0; a < e.length; a++) {
                var i = f.getAbsolutePath(e[a]);
                e[a] = i
            }
        for (var a = 0; a < t.length; a++) {
            var r = {type: "GET", frame_id: t[a], data_path: e[a]};
            d("GET", t[a], r)
        }
        o = 0
    }
}();