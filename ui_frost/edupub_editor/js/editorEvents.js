//    setTimeout(function(){jQuery('.cke_button__insertimage_icon').off().on("click", function () {
//        var a = jQuery(window).height();
//        var modalHeight = (50 * a) / 100;
//        jQuery("#fromAsset").find('.modal-content-container').css({
//            'max-height': modalHeight
//        });
//        });
//    },5000);
math = [];
htmlEditor.editorEvents = function() {
    htmlEditor.populateMeta();
    htmlEditor.setUserInputMetaClass();
    htmlEditor.saveDataSetSelection = "";
    htmlEditor.saveVideoSetSelection = "";
    htmlEditor.getDataset;
    htmlEditor.videoSet;
    jQuery(".cke_wysiwyg_frame").off().on('mouseout', function() {
        htmlEditor.removeUnwanteds(jQuery(this).contents());
    });
    //    remove the highlighclasses and unwanted elements on pressing escape button.
    jQuery(".cke_wysiwyg_frame").contents().find("body").on("keydown mousedown", function(e) {
        if (e.type === "keydown") {
            if (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 65 && e.keyCode <= 90 || e.keyCode >= 96 && e.keyCode <= 111 || e.keyCode == 32 || e.keyCode == 8 || e.keyCode >= 186) {
                htmlEditor.contentUpdated = true;
            }
            if (e.which === 27) {
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
                htmlEditor.removeFrameSettings();
            } else if (e.keyCode === 13) {
                return;
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents()); //Remove unwanteds
                var content = jQuery(this).find("*"); //get the content
                htmlEditor.generateIds(content); //generate ids for all elements.

                try {
                    var thisElm = jQuery(htmlEditor.getSelectedNode()).parent();
                    var isOlUl = thisElm[0].tagName === 'UL' || jQuery(htmlEditor.getSelectedNode()).parent()[0].tagName === 'OL' ? true : false;
                    if (isOlUl) {
                        jQuery(thisElm).find('li').each(function() {
                            var thisElm = jQuery(this);
                            var hasPara = thisElm.find('p').length > 0 ? true : false;
                            if (!hasPara) {
                                var thisText = thisElm.text();
                                var addPara = '<p>' + thisText + '</p>';
                                thisElm.html(addPara);
                            }
                        });
                        htmlEditor.populateMeta();
                    }
                    htmlEditor.contentUpdated = true;
                } catch (exc) {}
            } else if (e.keyCode == 46) {
                if (math.length == 1) {
                    e.preventDefault();
                    math.remove();
                } else {
                    return true;
                }
            } else if (e.keyCode == 8) {
                if (math.length == 1) {
                    e.preventDefault();
                    math.remove();
                } else {
                    return true;
                }
            }
            htmlEditor.removeFrameSettings();
        } else if (e.type == "mousedown") {
            if (math.length == 1) {
                math.find('math').removeClass('math_edit');
                math = jQuery(e.target).parents('math').parent();
                jQuery(e.target).parents('math').addClass('math_edit');
            } else {
                math = jQuery(e.target).parents('math').parent();
                jQuery(e.target).parents('math').addClass('math_edit');
            }


            if (e.which === 1) {
                if (jQuery(e.target).parents(".converted").length) {

                }
            }
        }
        htmlEditor.checkUserAct(e);
    });

    //          ==========Escape parent on double pressing the enter key============
    var returnCount = 0;
    jQuery(".cke_wysiwyg_frame").contents().find("body").off("keyup").on("keyup", function(e) {
        if (e.keyCode == 13) {
            returnCount++;
            setTimeout(function() {
                if (returnCount >= 2) {
                    escaper();
                }
                returnCount = 0;
            }, 300)
        }
    });
    //          ====================================================================

    htmlEditor.bindProfileEvents();
    jQuery('.cke_button_icon').on('click', function() {
        var enableCheck = jQuery(this).hasClass('cke_button__link_icon') || jQuery(this).hasClass('cke_button__insertimage_icon') ? false : true;
        if (enableCheck)
            htmlEditor.enableSave();
    });
};
htmlEditor.enableSave = function() {
    //console.log("save")
    
    setTimeout(function() {
        var lastContent = htmlEditor.getLatestSavedContent();
        if (lastContent !== htmlEditor.currContent || htmlEditor.mathMlEdited) {
            jQuery('#save_modal').removeAttr('disabled');
        } else {
            if (arguments[0] == true) { //Forcefully enable save
                jQuery('#save_modal').removeAttr('disabled');
            } else {
                htmlEditor.disableSave();
            }
        }

    }, 1000);
};
htmlEditor.disableSave = function() {
    jQuery('#save_modal').attr('disabled', 'disabled');
};
/****************************************************/
//Function that sets user added classes for elements
htmlEditor.setUserInputMetaClass = function() {
    var oUserInputMetaList = {};
    jQuery.when(oUserInputMetaList = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.patternClasses)).done(function() {
        oUserInputMetaList = JSON.parse(oUserInputMetaList.responseText).data.pattern_classes;
        for (var key in oUserInputMetaList) {
            var keyUpper = key.toUpperCase();
            if (classSet[keyUpper] !== 'undefined') {
                try {
                    for (var i = 0; i < oUserInputMetaList[key].length; i++) {
                        classSet[keyUpper].push(oUserInputMetaList[key][i]);
                    }
                } catch (exc) {}
            }
        }
    });
};
//Function to populate metaclasses and highligh selection inside the editor.
htmlEditor.populateMeta = function() {
    htmlEditor.metaArr = {};
    htmlEditor.topLevelPatternClass = new Array();
    jQuery(editor_patterns).each(function(i, v) {
        if (v.meta_class_options != undefined) {
            var metaData = JSON.parse(v.meta_class_options);
        }
        //console.log(metaData);
        jQuery(metaData).each(function(i1, v1) {
            var key = v1.default_class;
            htmlEditor.topLevelPatternClass.push(v1.default_class);
            htmlEditor.metaArr[key] = new Array();
            jQuery(v1.pattern_classes).each(function(i2, v2) {
                var val = v2.value;
                //console.info('=====================');
                //console.info(val);
                //console.info('++++++++++++++++++++++');
                if (typeof val!='undefined' && val.length > 0) {
                    htmlEditor.metaArr[key].push(val);
                    htmlEditor.topLevelPatternClass.push(val);
                }
            });
        });
    });
    htmlEditor.topLevelPatternClass.push('converted gadget');
    setTimeout(function() {
        htmlEditor.getCurrentPattern();
    }, 50);
};
htmlEditor.disableToolbar = function() {
    /*jQuery('a.cke_button').each(function(){
     var thisElm = jQuery(this);
     thisElm.attr("onclick","");
     thisElm.addClass('cke_button_disabled');
     });*/
};
htmlEditor.enableToolbar = function() {
    /*jQuery('a.cke_button').each(function(indx){
     var thisElm = jQuery(this);
     thisElm.attr("onclick",htmlEditor.aOnclickValueArr[indx]);
     thisElm.removeClass('cke_button_disabled');
     });*/
};
htmlEditor.ImageModelBool = true;
htmlEditor.showImageOptions = function(image) {
    CKEDITOR.instances.editor1.getSelection().removeAllRanges();
    var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    var frame = jQuery(image);
    var frameParent=frame.parent();
    var frameChild=frame.parent().children().length;
    var ht = frame.outerHeight(); // height of the image selected.
    var wide = frame.outerWidth(); // width of the image selected.
    var offset = frame.offset(); // Offset of the image selected.
    htmlEditor.removeUnwanteds(the_editor);
    var type = frame.attr("type");

    var dataPos = frame.attr("data-pos");
    var overlay = '<div class="image-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del-image" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set-image" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT IMAGE</div></div>';
    //console.log(the_editor)
    the_editor.append(overlay);

    //The delete functionality of the image
    the_editor.find(".to-del-image:visible").off().on("click", function() {
        var selElmId = frame.parents("[id]").attr("id");
        frame.remove();
        frame.deleteTOCNode();
        if (jQuery("#sourceVal:visible").length) {
            splitview.updateCode(selElmId); //Remove elem code from split source.
        }
         if(frameChild == 1){
         frameParent.remove();
        }
        //        jQuery("iframe.cke_wysiwyg_frame").contents().find("body").trigger("keyup");
        htmlEditor.removeUnwanteds(the_editor);
        htmlEditor.enableSave();
    });

    the_editor.find(".to-set-image").off().on("click", function() {
        var assetPickerHeight = jQuery(window).height();
        var modalHeight = (50 * assetPickerHeight) / 100;
        jQuery("#fromAsset").find('.modal-content-container').css({
            'max-height': modalHeight
        });
        the_editor.find(".image-setting").remove();
        if (htmlEditor.ImageModelBool) {
            htmlEditor.getMediaImages(image);
        } else {
            swal("Image Uploading in Progress.");
        }

    });
};
htmlEditor.DatasetModelBool = true;
htmlEditor.showDatasetOptions = function(image) {
    CKEDITOR.instances.editor1.getSelection().removeAllRanges();
    var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    var frame = jQuery(image);
    var ht = frame.outerHeight(); // height of the image selected.
    var wide = frame.outerWidth(); // width of the image selected.
    var offset = frame.offset(); // Offset of the image selected.
    htmlEditor.removeUnwanteds(the_editor);
    var type = frame.attr("type");

    var dataPos = frame.attr("data-pos");
    var overlay = '<div class="image-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del-dataset" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span></div><div class="toolBoxLarge to-set-dataset" contenteditable="false"><span class="glyphicon glyphicon-pencil tool" title="Configure"></span></div><div class="toolBoxLarge to-conf-dataset" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span></div></div>';
    //console.log(the_editor)
    the_editor.append(overlay);
    if (frame.parents('.dataset').length) {
        htmlEditor.getDataset = frame.parents('.dataset')[0];
        // console.log(htmlEditor.videoSet);

    } else if (image.getAttribute('class') === 'dataset') {
        htmlEditor.getDataset = image;
        //console.log(htmlEditor.videoSet);

    }

    //    jQuery('#testCont').contents().find('body').find('.dataset')[0].off().on('click', function () {
    //         // alert();
    //      });

    //    jQuery(".cke_wysiwyg_frame").contents().find(".dataset").on('mouseenter', function (e) {
    //        
    //        if (e.toElement.parentNode.getAttribute('class') === 'dataset') {
    //            htmlEditor.getDataset = e.toElement.parentNode;
    //
    //        }
    //        if (e.toElement.getAttribute('class') === 'dataset') {
    //            htmlEditor.getDataset = e.toElement;
    //
    //        }
    //    });
    //The delete functionality of the image
    the_editor.find(".to-conf-dataset").off().on("click", function(event) {

        //mainParentDiv = jQuery(this).parent().parent().find(".dataset");
        //console.log(mainParentDiv);
        //mainParentDiv =
        //mainParentDiv = jQuery(this);


        var params = {};
        params.dataApi = htmlEditor.PXEmappedAPI.dataSetApi;
        //params.param = {project_id: project_id, access_token:access_token};

        jQuery.ajax({
            url: params.dataApi,
            //            async: false,
            method: 'GET',
            //data: params.param,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (data.projectPermissionGranted == 401) {
                    swal({ title: "Warning!", type: "warning", text: "Sorry! Access Prohibited!" });
                } else {

                    
                    if (data.data.dataset_list == null) {
                        swal({ title: "Warning!", type: "warning", text: "No resource list found" });
                    } else {
                        var myHtml = "<div><table class='table table-hover' style='border:1px solid #ccc'><thead style='background-color: #eaeaea;'><tr><th style='width:60%;'>TITLE<th style='text-align:center'>FILES</th></th></tr></thead><tbody>";

                        jQuery("#DatasetModal").modal('show');
                        jQuery('#DatasetModal .dataset_conf').attr("disabled", true);
                        jQuery.each(data.data.dataset_list, function(key, value) {

                            if (frame.parents('.dataset').attr('data-dataset-id') === value.id) {
                                myHtml += "<tr><td><div class='radio'><label class='wordBreak'><input type='radio' name='dataset' data-value-title='" + value.title + "'  value='" + value.id + "'checked>" + value.title + "</label></div></td><td style='text-align:center'><span style='padding-top: 10px;display: block;'>" + value.COUNT_RECORDS + "</span></td></tr>";
                            } else {
                                myHtml += "<tr><td><div class='radio'><label class='wordBreak'><input type='radio' name='dataset' data-value-title='" + value.title + "'  value='" + value.id + "'>" + value.title + "</label></div></td><td style='text-align:center'><span style='padding-top: 10px;display: block;'>" + value.COUNT_RECORDS + "</span></td></tr>";
                            }
                        });

                        myHtml += "</tbody></table></div>"
                        jQuery("#datasetLoader").html(myHtml);
                        //             jQuery('input[name="dataset"]').on('change',function(){
                        //                htmlEditor.saveDataSetSelection=jQuery(this).attr('value');
                        //
                        //    });
                        jQuery('input[name=dataset]').change(function() {
                            jQuery('#DatasetModal .dataset_conf').attr("disabled", false);
                            htmlEditor.enableSave();
                        });

                    }
                }
            },
            error: function(data) {
                swal("Sorry! Error occured");
            }
        });

    });

    jQuery("#DatasetModal .dataset_conf").off('click').on('click', function() {


        htmlEditor.saveDataSetSelection = jQuery('input[name=dataset]:checked').attr('value');
        //console.log(htmlEditor.saveDataSetSelection);
        htmlEditor.getDataset.setAttribute("data-dataset-id", htmlEditor.saveDataSetSelection);
        
        var params = {};
        params.dataApi = htmlEditor.PXEmappedAPI.saveDataSetSourcePath;
        params.data = { dataset_id: htmlEditor.saveDataSetSelection, project_id: project_id };
        jQuery.ajax({
            url: params.dataApi + '?access_token=' + access_token,
            //            async: false,
            method: 'GET',
            data: params.data,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                //console.log(data);
                
                htmlEditor.getDataset.setAttribute("data-path", data.json_file_path);
            },
        });
        jQuery('#DatasetModal').modal('hide');
        htmlEditor.enableSave();
    });

    the_editor.find(".to-del-dataset:visible").off().on("click", function() {
        var selElmId = frame.parents("[id]").attr("id");
        //edited bhy tarique
        if (jQuery(frame[0].parentElement.parentElement).hasClass('dataset')) {
            frame[0].parentElement.parentElement.remove(); //added by parveen to remove parent section element when dataset is deleted.
        }
        frame[0].parentElement.remove();
        if (jQuery("#sourceVal:visible").length) {
            splitview.updateCode(selElmId); //Remove elem code from split source.
        }
        //        jQuery("iframe.cke_wysiwyg_frame").contents().find("body").trigger("keyup");
        htmlEditor.removeUnwanteds(the_editor);
        frame.deleteTOCNode();
        htmlEditor.enableSave();
    });
    the_editor.find(".to-set-dataset").off().on("click", function() {
        var assetPickerHeight = jQuery(window).height();
        var modalHeight = (50 * assetPickerHeight) / 100;
        jQuery("#fromAsset").find('.modal-content-container').css({
            'max-height': modalHeight
        });
        the_editor.find(".image-setting").remove();
        if (htmlEditor.DatasetModelBool) {
            htmlEditor.getMediaImages(image);
        } else {
            swal("Image Uploading in Progress.");
        }

    });
};
htmlEditor.multipleModelBool = true;
htmlEditor.multipleVideoOptions = function(image) {
    

    CKEDITOR.instances.editor1.getSelection().removeAllRanges();
    var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    var frame = jQuery(image);
    var ht = frame.outerHeight(); // height of the image selected.
    var wide = frame.outerWidth(); // width of the image selected.
    var offset = frame.offset(); // Offset of the image selected.
    htmlEditor.removeUnwanteds(the_editor);
    var type = frame.attr("type");

    var dataPos = frame.attr("data-pos");
    var overlay = '<div class="image-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del-video" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> </div><div class="toolBoxLarge to-set-video" contenteditable="false"><span class="glyphicon glyphicon-pencil tool" title="Configure"></span></div><div class="toolBoxLarge to-conf-video" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span></div></div>';
    //console.log(the_editor)
    the_editor.append(overlay);
    if (frame.parents('.multiplevideo').length) {
        htmlEditor.videoSet = frame.parents('.multiplevideo')[0];
        // console.log(htmlEditor.videoSet);

    } else if (image.getAttribute('class') === 'multiplevideo') {
        htmlEditor.videoSet = image;
        //console.log(htmlEditor.videoSet);

    }

    //    jQuery('#testCont').contents().find('body').find('.dataset')[0].off().on('click', function () {
    //         // alert();
    //      });

    //    jQuery(".cke_wysiwyg_frame").contents().find(".multiplevideo").on('mouseenter', function (e) {
    //
    //        if (e.toElement.parentNode.getAttribute('class') === 'multiplevideo') {
    //           htmlEditor.videoSet = e.toElement.parentNode;
    //          // console.log(htmlEditor.videoSet);
    //
    //        }
    //        if (e.toElement.getAttribute('class') === 'multiplevideo') {
    //            htmlEditor.videoSet = e.toElement;
    //            //console.log(htmlEditor.videoSet);
    //
    //        }
    //    });
    //The delete functionality of the image
    the_editor.find(".to-conf-video").off().on("click", function(event) {
        jQuery("#videoLoader").html('');
        //var this_ifrm = theFrame;

        //jQuery('#multipleVideoModal .multipleVideo_conf').attr("disabled", true);
        //jQuery('#AssessmentModal .assessment_conf').attr("disabled", false);
        var params = {};
        params.dataApi = htmlEditor.PXEmappedAPI.getvideoPlaylistApi;
        params.param = { project_id: project_id };
        jQuery.ajax({
            url: params.dataApi,
            //            async: false,
            method: 'GET',
            data: params.param,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (data.projectPermissionGranted == 401) {
                    swal({ title: "Warning!", type: "warning", text: "Sorry! Access Prohibited!" });
                } else {
                    
                    if (data.data === null) {
                        swal({ title: "Warning!", type: "warning", text: "No video list found" });
                    } else if (data.playlist.length == 0) {
                        swal({ title: "Warning!", type: "warning", text: "No video list found" });
                    } else {
                        jQuery('#multipleVideoModal').modal('show');
                        jQuery('#multipleVideoModal .multipleVideo_conf').attr("disabled", true);
                        myHtml = "<div><table class='table table-hover' style='border:1px solid #ccc'><thead style='background-color: #eaeaea;'><tr><th style='width:60%;'>TITLE<th style='text-align:center'>FILES</th></th></tr></thead><tbody>";
                        jQuery.each(data.playlist, function(key, value) {

                            
                            if (frame.parents('.multiplevideo').attr('data-multiplevideo-id') === value.playlist_id) {

                                myHtml += "<tr><td><div class='radio'><label class='wordBreak'><input type='radio' name='multiVideo' id='" + value.playlist_id + "' value='" + value.key + "' checked>   " + value.key + "</label></div></td><td style='text-align:center'><span style='padding-top: 10px;display: block;'>" + value.total + "</span></td></tr>";
                            } else {
                                myHtml += "<tr><td><div class='radio'><label class='wordBreak'><input type='radio' name='multiVideo' id='" + value.playlist_id + "' value='" + value.key + "'>   " + value.key + "</label></div></td><td style='text-align:center'><span style='padding-top: 10px;display: block;'>" + value.total + "</span></td></tr>";
                            }
                        });

                        myHtml += "</tbody></table></div>";
                        
                        jQuery("#videoLoader").append(myHtml);
                        jQuery('input[name=multiVideo]').change(function() {
                            jQuery('#multipleVideoModal .multipleVideo_conf').attr("disabled", false);
                            htmlEditor.enableSave();
                        });

                    }
                }
            },
            error: function(data) {
                swal("Sorry! Access Prohibited");
            }
        });
    });


    jQuery("#multipleVideoModal .multipleVideo_conf").off('click').on('click', function() {

        htmlEditor.saveVideoSetSelection = jQuery('input[name=multiVideo]:checked').attr('id');
        console.log(htmlEditor.saveDataSetSelection);
        htmlEditor.videoSet.setAttribute("data-multiplevideo-id", htmlEditor.saveVideoSetSelection);
        
        var params = {};
        params.dataApi = htmlEditor.PXEmappedAPI.getVideoList;
        jQuery.ajax({
            url: params.dataApi + htmlEditor.saveVideoSetSelection + '?access_token=' + access_token,
            //            async: false,
            method: 'GET',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                //console.log(data);
                
                //htmlEditor.getDataset.setAttribute("data-path",data.json_file_path);
                //console.log(data);
                htmlEditor.videoSet.setAttribute("data-path", data.data.dataSetFile);
                jQuery('#multipleVideoModal').modal('hide');
            }

        });
    });
    the_editor.find(".to-del-video:visible").off().on("click", function() {
        var selElmId = frame.parents("[id]").attr("id");
        //edited by tarique
        if (jQuery(frame[0].parentElement.parentElement).hasClass('multiplevideo')) {
            frame[0].parentElement.parentElement.remove(); //added by parveen to remove parent section element when multivideo is deleted.
        }
        frame[0].parentElement.remove();
        if (jQuery("#sourceVal:visible").length) {
            splitview.updateCode(selElmId); //Remove elem code from split source.
        }
        //        jQuery("iframe.cke_wysiwyg_frame").contents().find("body").trigger("keyup");
        htmlEditor.removeUnwanteds(the_editor);
        htmlEditor.enableSave();
    });
    the_editor.find(".to-set-video").off().on("click", function() {
        var assetPickerHeight = jQuery(window).height();
        var modalHeight = (50 * assetPickerHeight) / 100;
        jQuery("#fromAsset").find('.modal-content-container').css({
            'max-height': modalHeight
        });
        the_editor.find(".image-setting").remove();
        if (htmlEditor.multipleModelBool) {
            htmlEditor.getMediaImages(image);
        } else {
            swal("Image Uploading in Progress.");
        }

    });
};
htmlEditor.getCurrentPattern = function() {
    var metaArr = htmlEditor.metaArr;
    var pattClsLst = htmlEditor.topLevelPatternClass;

    var tagListToHighlight = ['SECTION', 'ASIDE', 'OL', 'UL', 'BLOCKQUOTE', 'FIGURE'];
    var tagListToHide = ['HEADER', 'P', 'SPAN', 'STRONG', 'EM', 'I', 'U', 'IMG', 'TD', 'TBODY', 'TR', 'TABLE'];
    jQuery('.cke_wysiwyg_frame').contents().find("body").off('click').on('click', "*", function(e) {
        /*if (jQuery('.cke_editable').find('.chapter').length === 0)
         return;*/
        /*if (e.target.tagName === "IMG" && !e.target.classList.contains('cke_reset')) {
         htmlEditor.showImageOptions(e.target);
         return false;
         }*/
        
        if (e.target.parentElement.className === "figure" && !e.target.classList.contains('cke_reset')) {
            
            htmlEditor.showImageOptions(e.target);
            return false;
        } else if (e.target.tagName == "IMG" && !jQuery(e.target).parents('.dataset').length && !jQuery(e.target).parents('.multiplevideo').length && !e.target.classList.contains('cke_reset')) {
            
            htmlEditor.showImageOptions(e.target);
            return false;
        } else if (e.target.parentElement.className === "informalfigure" && !e.target.classList.contains('cke_reset')) {
            
            htmlEditor.showImageOptions(e.target);
            return false;
        } else if (jQuery(e.target).parents('.dataset').length && !e.target.classList.contains('cke_reset')) {
            
            htmlEditor.showDatasetOptions(e.target);
            //alert();
            return false;
        } else if (jQuery(e.target).parents('.multiplevideo').length && !e.target.classList.contains('cke_reset')) {
            
            htmlEditor.multipleVideoOptions(e.target);
            //alert();
            return false;
        }

        jQuery('.cke_button__link').removeClass('cke_button_on');

        function settingOverlay(this_frm) {
            // Function to show setting options for videos & gadget pattern.

            e.stopPropagation();
            var this_ifrm = this_frm;
            htmlEditor.showSettingForFrame(this_ifrm);
        }

        if (jQuery(this).parents(".converted").length || jQuery(this).hasClass("converted")) {
            var converted_elem = jQuery(this).hasClass("converted") ? jQuery(this) : jQuery(this).parents(".converted");
            settingOverlay(converted_elem);

        } else {
            var thisElm = jQuery(this);
            var tagName = thisElm[0].tagName;
            if (tagName === 'A') {
                jQuery('.cke_button__link').addClass('cke_button_on');
            } else {
                //jQuery('.cConfig').remove();
                //thisElm.append('<button class="cConfig" onClick="htmlEditor.classConfig(this)">+</button>')
                //thisElm.append('<button class="cConfig" onClick="htmlEditor.classConfig(this)">+</button>')
            }


            var tagElmToHighlight = '';

            function classNotDefinedChecker(this_elm) {
                var clsName = this_elm.attr('class');
                var tagName = this_elm[0].tagName;
                try {
                    if (tagName === 'HEADER') {
                        return false;
                    } else if (jQuery.inArray(tagName, tagListToHighlight) !== -1) {
                        return true;
                    } else {
                        var allClasses = clsName.split(' ');
                        for (var i = 0; i < allClasses.length; i++) {
                            if (jQuery.inArray(allClasses[i], pattClsLst) !== -1) {
                                return true;
                            }
                            break;
                        }
                    }
                } catch (exc) {
                    if (jQuery.inArray(tagName, tagListToHighlight) !== -1) {
                        tagElmToHighlight = this_elm;
                    }
                    return jQuery.inArray(clsName, pattClsLst) !== -1 && this_elm[0].tagName !== 'HEADER' ? true : false;
                }
            }

            function isTrueClass(arr) {
                for (var i = 0; i < arr.length; i++) {
                    var key = arr[i];
                    if (metaArr[key] !== undefined)
                        return metaArr[key]
                    else
                        return false;
                }
            }
            try {
                var this_class = false;
                this_class = classNotDefinedChecker(thisElm);
                while (!this_class) {
                    thisElm = thisElm.parent();
                    if (thisElm[0].tagName === 'BODY')
                        break;
                    this_class = classNotDefinedChecker(thisElm);
                }
                if (this_class) {
                    var classArr = thisElm.attr('class').split(' ');
                    this_class = isTrueClass(classArr);
                }

            } catch (exc) {}

            function setSettings(elm) {
                
                var tagName = elm[0].tagName;
                /*if(jQuery.inArray(tagName, tagListToHide) !== -1){
                 (elm.parent()).trigger('click');
                 }
                 else{*/
                if (tagName != 'BODY' && tagName != 'HTML') {
                    jQuery('.highlight_on_drop').removeClass('highlight_on_drop');
                    var className = (elm[0].className).split('highlightSelected')[0];
                    jQuery('.meta-dropdown').removeClass('open');
                    jQuery('.epub-dropdown').removeClass('open');
                    htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
                    elm.addClass('highlightSelected');
                    /*if (elm.hasClass('highlightSelected')) {
                     jQuery("button.create").attr("disabled", false);
                     }*/
                    elm.append("<span class='descOfTarget' contenteditable='false'>" + tagName + " " + className + "</span><div class='toolBox' contenteditable='false'><span class='glyphicon glyphicon-trash tool' title='Delete'></span></div>");
                    removeOnTrashClick();
                }
                //}
            }

            function removeOnTrashClick() {
                jQuery(".cke_wysiwyg_frame").contents().find("body").find('.glyphicon-trash').off('click').on('click', function() {
                    var oHighlightedElm = jQuery(".cke_wysiwyg_frame").contents().find("body").find('.highlightSelected');
                    var parId = oHighlightedElm.parents("[id]").attr("id");
                    oHighlightedElm.deleteTOCNode();
                    /*if(oHighlightedElm.hasClass("chapter")){
                     jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                     htmlEditor.bindIframeDragDrop();
                     htmlEditor.disableToolbar();
                     }*/
                    oHighlightedElm.remove();
                    if (jQuery("#sourceVal:visible").length) {
                        splitview.updateCode(parId); //remove the code from the split source
                    }
                    htmlEditor.removeFrameSettings();
                    htmlEditor.bindIframeDragDrop();
                    htmlEditor.enableSave();
                });
            }

            function setMetaContent(data, elm) {
                if (elm.hasClass('chapter') || elm.hasClass('frontmatter') || elm.hasClass('backmatter') || elm.hasClass('bodymatter')) {
                    data = false;
                }

                if (!data) {
                    jQuery('.meta-dropdown .dropdown-menu').empty();
                    jQuery('#dropdownMenu1').attr('disabled', true);
                    return false;
                } else {
                    jQuery('#dropdownMenu1').removeAttr('disabled');
                }
                var innerHtml = '';
                var activeLength = 0;
                for (var i = 0; i < data.length; i++) {
                    var elm_class = elm.attr('class').split(' ');
                    if (data[i].length > 0) {
                        if (jQuery.inArray(data[i], elm_class) !== -1) {
                            innerHtml += '<li class="active" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        } else {
                            innerHtml += '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        }
                    }
                    if (data.length === 1)
                        jQuery('#dropdownMenu1').attr('disabled', true);
                }
                jQuery('.meta-dropdown .dropdown-menu li').remove();
                jQuery('.meta-dropdown .dropdown-menu').html(innerHtml);
                jQuery('.meta-dropdown .dropdown-menu li a').off('click').on('click', function(e) {
                    var activeLength = jQuery('.meta-dropdown').find('.active').length; // Check for the active Classes.
                    htmlEditor.contentUpdated = true;
                    var thisPar = jQuery(this).parent();
                    activeLength = jQuery('.meta-dropdown').find('.active').length;
                    var classToAdd = jQuery.trim(thisPar.text());
                    if (activeLength <= 0 && !thisPar.hasClass("active")) { //Activate class if not activated
                        jQuery(this).parent().addClass('active');
                        elm.addClass(classToAdd);
                        elm.trigger('click');
                    } else if (activeLength > 0) { //Toggle if more than 1 activated.
                        if (thisPar.hasClass("active")) {
                            thisPar.removeClass("active");
                            elm.removeClass(classToAdd);
                        } else {
                            thisPar.addClass("active");
                            elm.addClass(classToAdd);
                            elm.trigger('click');
                        }
                    }
                    e.stopPropagation();
                    elm.trigger('click');
                    htmlEditor.liveUpdateView();
                    htmlEditor.enableSave();
                });
            }

            function setEpubContent(data, elm) {
                /*if (elm.hasClass('chapter') || elm.hasClass('frontmatter') || elm.hasClass('backmatter') || elm.hasClass('bodymatter')) {
                 data = false;
                 }*/

                if (!data) {
                    jQuery('.epub-dropdown .dropdown-menu').empty();
                    jQuery('#dropdownMenu2').attr('disabled', true);
                    return false;
                } else {
                    jQuery('#dropdownMenu2').removeAttr('disabled');
                }
                var innerHtml = '';
                var activeLength = 0;
                for (var i = 0; i < data.length; i++) {
                    var elm_class = elm.attr('epub:type').split(' ');
                    if (data[i].length > 0) {
                        if (jQuery.inArray(data[i], elm_class) !== -1) {
                            innerHtml += '<li class="active" role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        } else {
                            innerHtml += '<li role="presentation"><a role="menuitem" tabindex="-1" href="#">' + data[i] + '</a></li>';
                        }
                    }
                    if (data.length === 1)
                        jQuery('#dropdownMenu2').attr('disabled', true);
                }
                jQuery('.epub-dropdown .dropdown-menu li').remove();
                jQuery('.epub-dropdown .dropdown-menu').html(innerHtml);
                jQuery('.epub-dropdown .dropdown-menu li a').off('click').on('click', function(e) {
                    var activeLength = jQuery('.epub-dropdown').find('.active').length; // Check for the active Classes.
                    //console.log("activeLength" + activeLength);
                    htmlEditor.contentUpdated = true;
                    var thisPar = jQuery(this).parent();
                    activeLength = jQuery('.epub-dropdown').find('.active').length;
                    var classToAdd = jQuery.trim(thisPar.text());
                    var epub_attr = elm.attr("epub:type").split(" ").unique();
                    var elm_epub_attr = elm.attr("epub:type");
                    if (activeLength <= 0 && !thisPar.hasClass("active")) { //Activate class if not activated
                        jQuery(this).parent().addClass('active');
                        //console.info("XX1" + classToAdd);
                        elm.attr("epub:type", classToAdd);
                        elm.trigger('click');
                    } else if (activeLength > 0) { //Toggle if more than 1 activated.
                        if (thisPar.hasClass("active")) {
                            thisPar.removeClass("active");
                            //console.info("XX2X" + epub_attr);
                            //epub_attr.splice(jQuery.inArray(classToAdd, epub_attr), 1);
                            epub_attr = jQuery.grep(epub_attr, function(value) {
                                return value != classToAdd;
                            });
                            /*epub_attr = epub_attr.split(',');*/
                            //console.info("XX2Y" + epub_attr);
                            //console.info("XXZZ" + epub_attr.length);
                            if (epub_attr.length > 0) {
                                elm_epub_attr = epub_attr.join(' ');
                            } else {
                                elm_epub_attr = "";
                            }
                            elm.attr("epub:type", elm_epub_attr);
                            elm.trigger('click');
                        } else {
                            //console.info("XX3" + classToAdd);
                            thisPar.addClass("active");
                            //elm.addClass(classToAdd);
                            for (var i = 0; i < epub_attr.length; i++) {
                                if (jQuery.inArray(epub_attr[i], classToAdd) === -1) {
                                    elm.attr("epub:type", elm_epub_attr + ' ' + classToAdd);
                                }
                            }
                            elm.trigger('click');
                        }
                    }
                    e.stopPropagation();
                    elm.trigger('click');
                    htmlEditor.liveUpdateView();
                    htmlEditor.enableSave();
                });
            }

            try {
                var showSettings = jQuery('#highlighter').hasClass('active');
                if (showSettings) {
                    var tagName = thisElm[0].tagName;
                    //
                    if (jQuery.inArray(tagName, tagListToHide) !== -1) {
                        (thisElm.parent()).trigger('click');
                    } else {
                        setSettings(thisElm);
                    }

                }

                if (typeof this_class === 'object' && htmlEditor.classConfig(thisElm) && this_class.length > 0) {
                    this_class = this_class.concat(htmlEditor.classConfig(thisElm)).unique();

                    // added for style selector drop elements build up based on informalfigure/figure
                    var elemClass = thisElm.attr('class');
                    if (elemClass.indexOf('informalfigure') === 0) {
                        for (var i = 0; i < this_class.length; i++) {
                            if (this_class[i] === 'figure') this_class.splice(i, 1);
                        }
                    } else {
                        for (var i = 0; i < this_class.length; i++) {
                            if (this_class[i] === 'informalfigure') this_class.splice(i, 1);
                        }
                    }
                    // ends

                    setMetaContent(this_class, thisElm);
                } else if (typeof this_class === 'object' && !htmlEditor.classConfig(thisElm)) {
                    setMetaContent(this_class, thisElm);
                } else {
                    try {
                        this_class = this_class.concat(htmlEditor.classConfig(thisElm)).unique();
                        setMetaContent(this_class, thisElm);
                    } catch (exc) {
                        this_class = (htmlEditor.classConfig(thisElm)).unique();
                        setMetaContent(this_class, thisElm);
                    }
                }


                try {
                    var this_epub = {};
                    if (typeof htmlEditor.epubConfig(thisElm) === 'object') {
                        this_epub = htmlEditor.epubConfig(thisElm).unique();
                    }
                    setEpubContent(this_epub, thisElm);
                } catch (exc) {

                }

            } catch (exc) {}


        }
        e.stopPropagation();
    });
    jQuery('.tree_view,.right-pane').on('mouseover', function() {
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents()); //Remove the highlights.
        htmlEditor.removeFrameSettings();
    });
};
htmlEditor.classConfig = function(thisObj) {
    var thisTagname = jQuery(thisObj)[0].tagName;
    return (typeof classSet[thisTagname] !== 'undefined' ? classSet[thisTagname] : false);
    e.preventDefault();
};
htmlEditor.epubConfig = function(thisObj) {
    var thisTagname = jQuery(thisObj)[0].tagName;
    //console.info(epubTypeSet[thisTagname]);
    return (typeof epubTypeSet[thisTagname] !== 'undefined' ? epubTypeSet[thisTagname] : false);
    e.preventDefault();
};
htmlEditor.getEditorSelection = function() {
    var selection = CKEDITOR.instances.editor1.getSelection();
    return selection;
};
htmlEditor.removeFrameSettings = function() {
    var editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    editor.find(".iframe-setting").remove();
    editor.find(".image-setting").remove();

    var removeClasses = ["indicator", "highlightSelected", "highlight_on_drop", "highlight"]; // Add classes thats not required in the mode
    var removeElems = ["toolBox", "descOfTarget"]; // Add classes of the elements that needs to be removed.

    for (var i = 0; i < removeClasses.length; i++) { // Removes the unwanted classes from the given frame.
        editor.find("." + removeClasses[i]).removeClass(removeClasses[i]);
        /*if(removeClasses[i] == 'highlightSelected'){
         jQuery("button.create").attr("disabled", true);
         }*/
    }

    for (var k = 0; k < removeElems.length; k++) { // Removes unwanted elements from the given frame.
        editor.find("." + removeElems[k]).remove();
    }
};
Array.prototype.unique = function() {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};