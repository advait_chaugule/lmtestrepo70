//added for glossary popup
$(window).on("message", function(data) {
    //mock implementation of parents message listener
    var iframeElement = jQuery("#testCont");
    iframeWindow = iframeElement[0].contentWindow;
    var dataPath = iframeElement.attr("data-glossary-path");
    iframeWindow.postMessage({
        "data_path": dataPath,
        "type": "glossary"
    }, "*");

});

var jQuery = $.noConflict();
jQuery('.test-pane').css({ 'overflow': 'hidden' });
var htmlEditor = {};
htmlEditor.infoObj = {};
htmlEditor.islocked = false;
htmlEditor.dataPath = "";
htmlEditor.scrollHider = "";
var editor_patterns;
htmlEditor.isGlobalProject = false;
jQuery(window).on('message', function(data) {
    if (data.originalEvent.data === 'showAlert') {
        swal("' Only a-z, A-Z, 0-9, _, :, -, ., ?, ', / are supported'");
    }
    htmlEditor.scrollHider = data.originalEvent.data;
    jQuery('.test-pane').css({ 'overflow': htmlEditor.scrollHider });
});
var returnVal = {
    "content": '',
    "leftPanelTree": '',
    "img_urls": '',
    "validation_response_error": false
};

var guid = function() {
    var separator = '';

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    function s4_alpha() {
        var max = 122,
            min = 97;
        var return_str = "";
        for (var i = 0; i <= 3; i++) {
            var rand = Math.floor(Math.random() * (max - min + 1)) + min
            return_str += String.fromCharCode(rand);
        }
        return return_str;
    }
    return function() {
        return s4_alpha() + s4() + separator + s4() + separator + s4() + separator + s4() + separator + s4() + s4() + s4();
    };
};

jQuery(document).ready(function() {
    htmlEditor.onDocumentReady();
    htmlEditor.setPageTitle();
});

jQuery(window).resize(function() {
    htmlEditor.onWindowResize();
});

htmlEditor.setPageTitle = function() {
    var titleTag = jQuery("head").find("title");
    if (titleTag.length) {
        titleTag.text(node_title);
    } else {
        jQuery("head").append("<title>" + node_title + "</title>");
    }
};

htmlEditor.onDocumentReady = function() {
    htmlEditor.hideTocandSidebar();
    for (name in CKEDITOR.instances) {
        CKEDITOR.instances[name].destroy(); //Destroy all instances of the editor.
    }
    htmlEditor.checkProjectPermissionForUser();
    htmlEditor.getJsonValue();
    jQuery.when(htmlEditor.loadProjectDetailWIthPermissions()).done(function() {
        if (htmlEditor.permissionTo.showPattern) {
            htmlEditor.loadPatterns();
            // htmlEditor.getQuad();
        } else {
            jQuery(".right-pane").addClass("hideElm");
        }

        if (!htmlEditor.permissionTo.save) {
            jQuery('#save_toc_id').addClass('hideElm');
        }

        if (!htmlEditor.permissionTo.smartHighlight) {
            jQuery('#highlighter').addClass('hideElm').removeClass("active");
        }

        if (htmlEditor.permissionTo.viewSourceCode) {
            jQuery('.cke_button__source').removeClass('hideElm');
        }
        htmlEditor.callTickets();
        htmlEditor.getUsers(); // Fetches all the users associated with the project.
        htmlEditor.getStatus();
        htmlEditor.getAllVersions();
        htmlEditor.checkUserAct();
        htmlEditor.hideTocandSidebar();
        return true;
    })
};

htmlEditor.ajaxCalls = function(url) {
    //Make all ajax calls using this function
    return jQuery.ajax({
        url: url,
        async: true,
        beforeSend: function(xhr) {
            //            htmlEditor.createLoader("editor");
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            //if (data.projectPermissionGranted) {
            //window.location.assign(clientUrl + "/#edit_toc/" + project_id);
            //}
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Unable to fetch the data. " + textStatus);
            window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
    });
};

htmlEditor.getAccessToken = function(url) { //Make all ajax calls using this function
    return jQuery.ajax({
        url: url,
        async: false,
        beforeSend: function(xhr) {
            //            htmlEditor.createLoader("editor");
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {

        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Unable to fetch the data. " + textStatus);
            //window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
    });
};

htmlEditor.testLogin = function(params) { //Make all ajax calls for login
    var loginData = { "username": params.username, "password": params.password };
    return jQuery.ajax({
        url: serviceEndPoint + 'api/v1/login?access_token=' + params.access_token,
        async: false,
        method: 'POST',
        data: loginData,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {},
        error: function() {
            alert("Error occured");
        }
    });
};

htmlEditor.changeLockStatus = function() {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.updateLockTimeAPI,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {},
        error: function(data) {
            //alert("Error occured");
        }
    });
};

htmlEditor.checkUserAct = function(evt) {
    try {
        if (evt.type == 'keydown') {
            htmlEditor.enableSave();
        }
    } catch (exc) {
        //console.log("Window says: " + exc);
    }

    if (htmlEditor.islocked) {
        if (htmlEditor.interval) {
            clearInterval(htmlEditor.interval);
            htmlEditor.interval = 0;
        }

        htmlEditor.interval = setInterval(function() {
            //Function that would fire on interval
            htmlEditor.timeout = setTimeout(function() {
                htmlEditor.saveContent();
                htmlEditor.backToDashboard();
            }, 60000);
            swal({
                //            title: "You have been inactive for 5 minutes",
                //            html:     'You can use <b>bold text</b>, ' +     '<a href="//github.com">links</a> ' +     'and other HTML tags',
                html: "<h1>You have been inactive for 5 minutes</h1><p style='font-size:20px;'>Do you want to leave the page? </p><br><p style='font-size:12px;'>You would be automatically redirected to Dashboard in 60seconds</p>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {
                    htmlEditor.saveContent();
                    htmlEditor.backToDashboard();
                } else {
                    htmlEditor.checkUserAct();
                    if (htmlEditor.timeout) {
                        clearTimeout(htmlEditor.timeout);
                    }
                }
            });

        }, 300000); //Default Value=300000

        //    htmlEditor.interval = setInterval(function() {
        //        //Function that would fire on interval
        //        htmlEditor.changeLockStatus();
        //        swal({
        //            title: "You are inactive for 5 minutes",
        //            text: "Do you want to leave the page?",
        //            type: "warning",
        //            showCancelButton: true,
        //            confirmButtonColor: "#DD6B55",
        //            confirmButtonText: "Yes",
        //            cancelButtonText: "No",
        //            closeOnConfirm: true,
        //            closeOnCancel: true,
        //            timer: 10000,
        //        }, function(isConfirm) {
        //            if (isConfirm) {
        //                htmlEditor.saveContent();
        //                htmlEditor.backToDashboard();
        //            } else {
        //                htmlEditor.checkUserAct();
        //            }
        //        });
        //    }, 30000);
    } else {
        clearTimeout(htmlEditor.interval);
    }
};

htmlEditor.initiateEditor = function() {
    try {
        CKEDITOR.instances.editor1.removeAllListeners(); // just in case to prevent the customconfig error being faced.
        delete CKEDITOR.instances.editor1;
    } catch (exc) {

    }
    CKEDITOR.replace('editor1');

    CKEDITOR.on('dialogDefinition', function(ev) { //function to remove cell spacing and cell padding in ck-editor table
        //console.log(ev);
        var dialogName = ev.data.name;
        // console.log(dialogName);
        var dialogDefinition = ev.data.definition;
        if (dialogName == 'table' || dialogName == 'tableProperties') {
            var infoTab = dialogDefinition.getContents('info');
            //console.info(infoTab.get('txtCellPad'));
            infoTab.get('txtBorder').default = '';
            //infoTab.get('txtWidth').default = '';
            infoTab.get('txtCellSpace').default = '';
            infoTab.get('txtCellPad').default = '';
            infoTab.get('txtCellPad').controlStyle = 'display:none';
            infoTab.get('txtCellPad').label = '';
            infoTab.get('txtCellSpace').controlStyle = 'display:none';
            infoTab.get('txtCellSpace').label = '';
            infoTab.get('txtBorder').controlStyle = 'display:none';
            infoTab.get('txtBorder').label = '';
        }

    });
};

htmlEditor.contentUpdated = false;

htmlEditor.checkToBackToDashboard = function() {

    htmlEditor.backToDashboardFlag = true;
    setTimeout(function() {
        jQuery("iframe.cke_wysiwyg_frame").contents().find('.cfiHighlighter').removeClass('cfiHighlighter');
        var lastContent = htmlEditor.getLatestSavedContent();
        if (lastContent !== htmlEditor.currContent) {
            swal({
                title: "You are about to leave the page",
                type: "warning",
                html: "<div style=\"height:100px;\"><center><h4>Want to save the content?</h4></center></br></br></br></br>\n\
                        <button style=\"background: #673AB7 !important; border: 1px solid #673AB7 !important; border-radius: 3px; padding: 5px 20px; text-transform: uppercase; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnA\" >Yes</button> " +
                    "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnB\" >No</button>" +
                    "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\"type=\"button\" id=\"btnC\" >Cancel</button></div>",
                showConfirmButton: false
            });
            jQuery(document).on('click', "#btnA", function() {
                htmlEditor.saveContent();
            });

            jQuery(document).on('click', "#btnB", function() {
                htmlEditor.backToDashboard();
            });

            jQuery(document).on('click', "#btnC", function() {
                //alert(this.id);
                htmlEditor.backToDashboardFlag = false;
            });
        } else {
            htmlEditor.backToDashboard();
        }
    }, 1000);
}
htmlEditor.checkContentFrom = function() {
    var currentLoc = window.location.href;
    //var contentFrom = currentLoc.substring(currentLoc.indexOf("&content_from=") + ("&content_from=").length);
    if (content_from === "global_repo_content" || content_from === "global_repo_template") {
        return 'global';
    } else if (content_from === 'local_template') {
        return 'loc_temp';
    } else {
        return 'other';
    }
}
htmlEditor.backToDashboard = function() {
    //Initiate track for a concurrent user. Remove a user in case he leaves the page.
    //var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
    var curr_ver = jQuery(".version_num").attr("version_id"); //Get current user version.
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.releaseLock,
        data: {
            'object_id': node_id,
            'user_id': user_id
        },
        type: 'POST',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            if (data.status === 200) {
                var pageLoc = window.location.href;
                var reg = new RegExp(/^\d+$/);
                if (pageLoc.indexOf("redirect_project_id=")) {
                    var redirectId = pageLoc.substring((pageLoc.indexOf("redirect_project_id=") + ('redirect_project_id=').length), pageLoc.indexOf("&content_from"));
                    if (reg.test(redirectId)) {
                        htmlEditor.redirectProjectId = redirectId;
                    }
                }
                var contentFrom = htmlEditor.checkContentFrom();
                if (contentFrom === 'global') {
                    //alert(content_from);
                    window.location = clientUrl + '#/global_repo/' + (content_from == 'global_repo_content' ? 'ContentObject' : 'TEMPLATES');
                } else if (contentFrom === 'loc_temp') {
                    window.location = clientUrl + '#/template/' + htmlEditor.redirectProjectId;
                } else if (contentFrom === 'other') {
                    var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=REMOVE", { withCredentials: true });
                    var listener = function(event) {                       
                       window.location = clientUrl + '#/edit_toc/' + project_id;
                    };
                    es.addEventListener("message", listener);
                }


            } else {
                if (htmlEditor.checkContentFrom()) {
                    window.location = clientUrl + '#/global_repo/' + (content_from == 'global_repo_content' ? 'ContentObject' : 'TEMPLATES');
                } else {                    
                   window.location = clientUrl + '#/edit_toc/' + project_id;
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Your login session expired, Please re login.");            
            window.location = clientUrl + '#/edit_toc/' + project_id;
        }
    })

    //=====================================

};
// Function to remove unwanted elements & classes from a given frame.
htmlEditor.removeUnwanteds = function(frame) {
    var removeClasses = ["indicator", "highlightSelected", "highlight_on_drop", "highlight"]; // Add classes thats not required in the mode
    var removeElems = ["toolBox", "descOfTarget", "iframe-setting", "image-setting"]; // Add classes of the elements that needs to be removed.

    for (var i = 0; i < removeClasses.length; i++) { // Removes the unwanted classes from the given frame.
        frame.find("." + removeClasses[i]).removeClass(removeClasses[i]);
    }

    for (var k = 0; k < removeElems.length; k++) { // Removes unwanted elements from the given frame.
        frame.find("." + removeElems[k]).remove();
    }
    htmlEditor.removeFrameSettings();
};

htmlEditor.changeEditorClass = function() {
    jQuery('.cke_button_icon').addClass('icons');
};

//htmlEditor.getSelectedNode = function()
//{
//    if (document.selection)
//        return document.selection.createRange().parentElement();
//    else
//    {
//        var selection = window.getSelection();
//        var isFigure = typeof window.getSelection().getRangeAt(0).startContainer.className !== "undefined" ? true : false;
//        if (selection.rangeCount > 0 && !isFigure) {
//            return selection.getRangeAt(0).startContainer.parentNode;
//        }
//        else {
//            return selection.getRangeAt(0).startContainer;
//        }
//    }
//}

htmlEditor.loadProjectDetailWIthPermissions = function() {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(projectDeatils) {
            htmlEditor.isGlobalProject = projectDeatils.data.is_global_project;
            htmlEditor.configureOnPermission(projectDeatils.data.project_permissions)
                //            user_id = projectDeatils.data.user_id;
        },
        error: function() {
            //alert("Error occured");
        }
    });
}

htmlEditor.checkProjectPermissionForUser = function() {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(projectDeatils) {
            if (projectDeatils.status != '200') {
                window.location.assign(clientUrl + "/#edit_toc/" + project_id);
            }
        },
        error: function() {
            window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
    });
}

htmlEditor.permissionTo = {};

htmlEditor.configureOnPermission = function(permObj) {
    htmlEditor.permissionTo.viewSourceCode = permObj["editor.sourcecode.view"].grant;
    htmlEditor.permissionTo.save = permObj["toc.toc-content.save"].grant;
    htmlEditor.permissionTo.smartHighlight = permObj["editor.smarthighlight"].grant;
    htmlEditor.permissionTo.dragPattern = permObj["editor.pattern.draggable"].grant;
    htmlEditor.permissionTo.showPattern = permObj["pattern.show.all"].grant;


    //    setTimeout(function() {
    //        if (!allowSourceCode) {
    //            jQuery('.cke_button__source').addClass('hideElm');
    //        }
    //        else {
    //            jQuery('.cke_button__source').removeClass('hideElm');
    //        }
    //
    //        if (!allowSave) {
    //            jQuery('#save_toc_id').addClass('hideElm');
    //        }
    //        else {
    //            jQuery('#save_toc_id').removeClass('hideElm');
    //        }
    //
    //        if (!allowSmartHighlight) {
    //            jQuery('#highlighter').addClass('hideElm');
    //        }
    //        else {
    //            jQuery('#highlighter').removeClass('hideElm');
    //        }
    //
    //    }, 1000)
};
var eCtrlPressed = false;
var onKeyPressFired = false;
htmlEditor.bindEvents = function() {
    /*click event binded for image asset toolbar*/
    jQuery('.cke_button__insertimage_icon').off().on("click", function() {
        var assetPickerHeight = jQuery(window).height();
        var modalHeight = (50 * assetPickerHeight) / 100;
        jQuery("#fromAsset").find('.modal-content-container').css({
            'max-height': modalHeight
        });
    });
    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('change', function(e) {
            if (!onKeyPressFired) {
                htmlEditor.liveUpdateView();
            }

            htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents()); //Remove unwanteds
            var content = jQuery(".cke_wysiwyg_frame").contents().find("body").find("*"); //get the content
            htmlEditor.generateIds(content); //generate ids for all elements.
            htmlEditor.enableSave();
        });
    }
    jQuery("body").scrollTop(0);
    //Pattern Related Events
    if (jQuery(".cke_wysiwyg_frame").contents().find("body").find(".chapter") /*.length*/ ) {
        htmlEditor.bindIframeDragDrop(); //Enable dragging of pattern widgets.
        //Create a new pattern on the fly.
        jQuery('.addPattern').off('click').on('click', function() {
            jQuery('#addPatternModal .sub-title').remove();
            jQuery('#addPatternModal .modal-header')
                .html('Add Pattern<button type="button" class="close" data-dismiss="modal" ng-click="cancel()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>')
                .after('<div class="sub-title">You are trying to add a new pattern</div>');
            jQuery('#addPatternModal').modal('show');
            var patternKey = jQuery('#patternKey');
            var patternCont = jQuery('#patternCont');
            patternKey.val('');
            patternCont.val('');
            var addDynamicPattern = function(arr, indx) { //Function to append the newly added pattern
                htmlEditor.postNewPattern(arr, indx);
            };
            htmlEditor.postNewPattern = function(arr, indx) {
                var patternData = { "project_id": project_id, "title": arr[indx].title, "pattern": arr[indx].pattern };
                jQuery.ajax({
                    url: htmlEditor.PXEmappedAPI.saveProjectPatternAPI + '/project_id/' + project_id + '?access_token=' + access_token,
                    async: true,
                    method: 'POST',
                    data: patternData,
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function(data) {
                        if (data.status == '200') {
                            // var append_after = (jQuery('.menu-block').length-1);
                            // jQuery(".menu-block").eq(append_after).after("<div class='menu-block ui-draggable'><span class='glyphicon glyphicon-th' style='font-size:9px;color:#fff;'></span><div class='tab-name'>" + arr[indx].title + "</div></div>");
                            htmlEditor.loadPatterns();
                            setTimeout(function() {
                                jQuery('#addPatternModal').modal('hide');
                            }, 2500);
                            setTimeout(function() {
                                swal({ title: "Success!", text: "Pattern added." });
                            }, 2502);
                        } else {
                            swal("" + data.error.title + "");
                            jQuery('.menu-block.ui-draggable').each(function() {
                                jQuery(this).css("display", "block");
                            });
                            //jQuery('#addPatternModal').modal('hide');
                        }
                    },
                    error: function() {
                        alert("Error occured");
                    }
                });
            };
            jQuery('#patternConf').off('click').on('click', function(e) {
                if (jQuery('#patternKey').length !== 0 && jQuery('#patternCont').val().length !== 0) {
                    patternKey = (jQuery.trim(jQuery('#patternKey').val())).toUpperCase();
                    patternCont = jQuery.trim(jQuery('#patternCont').val());
                    editor_patterns.push({
                        'title': patternKey,
                        'pattern': patternCont
                    });
                    addDynamicPattern(editor_patterns, editor_patterns.length - 1);
                    htmlEditor.bindIframeDragDrop();
                    htmlEditor.calculateWidgetsOnResize();
                    e.preventDefault();
                }
            });
        });
    }

    //open the tree whenever the table of contents needs to be opened.
    jQuery("#tocBtn").off("click").on("click", function() {
        if (!jQuery('button.expandToc').find(".fa-caret-down").length) {
            jQuery('button.expandToc').trigger("click");
        }
    })

    // Keep a track of user actions
    jQuery(document).on("click mouseover keyup keydown scroll", function(evt) {
        eCtrlPressed = evt.ctrlKey;
        htmlEditor.checkUserAct();
    });
    // Commented to solve issue Smart_Highlighter.
    /*jQuery("iframe").contents().find("body").on("click keyup keydown scroll", function (evt) {
     if (evt.keyCode === 46 || evt.keyCode === 8) {
     if (jQuery("iframe").contents().find("body").html().length < 12) {
     jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
     htmlEditor.bindIframeDragDrop();
     }
     }
     var upto = 'section';
     var currSelElm = jQuery(htmlEditor.getEditorSelection().getStartElement().jQuery).parents(upto);
     var mapId = jQuery(currSelElm[0]).attr('data-pos');
     var elmText = jQuery(currSelElm[0]).text().substr(0, 15) + '...';
     var oParentElm = jQuery(".tree_view");
     var tocElm = oParentElm.find("*[map=" + mapId + "]");
     var currNode = tocElm.find('.desc').eq(0);
     currNode.text(elmText);
     htmlEditor.checkUserAct();
     });*/

    jQuery('#highlighter').off('click').on('click', function() {
        jQuery(this).toggleClass('active');
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
    });

    jQuery("#lockPage").off("click").on("click", function() {
            if (jQuery(this).find(".fa-lock").length) {
                jQuery.ajax({
                    url: htmlEditor.PXEmappedAPI.releaseLock,
                    data: {
                        'object_id': node_id,
                        'user_id': user_id
                    },
                    type: 'POST',
                    async: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function(data, textStatus, jqXHR) {
                        if (data.status === 200) {
                            jQuery("#lockPage").find(".fa-lock").removeClass("fa-lock").addClass("fa-unlock");
                            htmlEditor.islocked = false;
                            clearTimeout(htmlEditor.timeout);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                    }
                })
            } else {
                jQuery.ajax({
                    url: htmlEditor.PXEmappedAPI.lockPage,
                    data: {
                        'object_id': node_id,
                        'user_id': user_id
                    },
                    type: 'POST',
                    async: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function(data, textStatus, jqXHR) {
                        if (data.status === 200) {
                            jQuery("#lockPage").find(".fa-unlock").removeClass("fa-unlock").addClass("fa-lock");
                            htmlEditor.islocked = true;
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                    }
                })
            }
        })
        // Toggle display of tickets & validation
    jQuery(".lm-edit-more").off().on("click", function() {
        var disabled_pane = jQuery(this).parents(".lm-edt-pane").attr("disabled");

        if (typeof(disabled_pane) === 'undefined') {
            //remove the unwanted highlights while opening the right panel.
            htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());

            jQuery(this).toggleClass("active").find("span.fa").toggleClass("fa-chevron-right"); //Change the arrow direction of the button
            jQuery(this).parents(".lm-edt-pane").toggleClass("active");
            if (!jQuery(this).hasClass("active")) {
                jQuery(".cke_wysiwyg_frame").contents().find(".indicator").removeClass("indicator");
                try {
                    jQuery(".activatedTab").trigger("click"); //Close the open accordion tab, if any.
                    jQuery(".action-link.cancel").trigger("click"); // Close all the textareas.
                } catch (exc) {

                }
            }
        }

    });

    //          ====================Slider controller===============================

    jQuery("#prevArrow").off("click").on("click", function() {
        htmlEditor.initial -= htmlEditor.numOfWidgetsDisplayed; //htmlEditor.numOfWidgets;
        htmlEditor.initial = htmlEditor.initial < 0 ? 0 : htmlEditor.initial;
        var upto = htmlEditor.initial + htmlEditor.numOfWidgets;
        jQuery("#nextArrow").attr("disabled", false);
        jQuery("#nextArrow").css("opacity", "1");
        htmlEditor.createWidgets(htmlEditor.initial, upto);
    });
    jQuery("#nextArrow").off("click").on("click", function() {
        htmlEditor.numOfWidgetsDisplayed = htmlEditor.numOfWidgets + htmlEditor.numOfHidddenWidgets;
        htmlEditor.initial += htmlEditor.numOfWidgetsDisplayed; //htmlEditor.numOfWidgets;
        htmlEditor.initial = htmlEditor.initial < 0 ? 0 : htmlEditor.initial;
        var upto = htmlEditor.initial + htmlEditor.numOfWidgets;
        jQuery("#prevArrow").attr("disabled", false);
        jQuery("#prevArrow").css("opacity", "1");
        htmlEditor.createWidgets(htmlEditor.initial, upto);
    });
    //          ====================================================================

    //Main page events to switch state between build/test
    if (!htmlEditor.switch_btn_state) {
        htmlEditor.switch_btn_state = 'build';
    }
    // htmlEditor.switch_btn_state = 'build';
    jQuery('.switch.right .switch-button-background').off('click').on('click', function(e) {
        if (htmlEditor.switch_btn_state == 'build') {
            htmlEditor.switch_btn_state = 'test';
        } else {
            htmlEditor.switch_btn_state = 'build';
            setTimeout(function() {
                    jQuery(".switch").css({"pointer-events":"all","opacity":"1"});
                }, 1000);
        }
        htmlEditor.checkUserAct();
        htmlEditor.onSwitchStateChange();
    });
    jQuery('.switch.right .switch-button-label').off('click').on('click', function() {
        if (jQuery(this).hasClass('active'))
            return;
        jQuery('.switch-button-background').trigger('click');
    });
    //View Port manager.
    jQuery('.resize-view li').off('click').on('click', function() {
        htmlEditor.viewChange(jQuery(this).index());
        htmlEditor.checkUserAct();
    });
    //Editor Events

    jQuery(".cke_button__source").off().on("click", function() {
        if (!jQuery(this).hasClass("cke_button_disabled")) {
            try {
                jQuery('.cont_link').toggleClass('cke_button_disabled', '');
                if (!jQuery('.cont_link').hasClass('cke_button_disabled')) {
                    jQuery(".lm-edit-more").attr("disabled", false); //Enable the toggler button to bring in right pane.
                    htmlEditor.populateMeta();
                    jQuery("#save_toc_id").attr("disabled", false); //Disable the save button when viewing source.
                    setTimeout(function() {
                        htmlEditor.editorEvents();
                        htmlEditor.bindIframeDragDrop();
                    }, 100);
                } else {
                    if (jQuery('button.dropdown-toggle').attr('disabled') === undefined) {
                        jQuery('button.dropdown-toggle').attr({ 'disabled': 'disabled' });
                    }
                    jQuery('#tocBtn').removeAttr('disabled');
                    if (jQuery(".lm-edit-more").hasClass("active")) { // Expand the editor size and disable toggler button.
                        jQuery(".lm-edit-more").trigger("click");
                    }
                    jQuery("#save_toc_id").attr("disabled", true); //Disable the save button when viewing source.
                    jQuery(".lm-edit-more").attr("disabled", true);
                    var eventCheckInterval = setInterval(function() {
                        if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').length === 0) {} else {
                            jQuery("iframe.cke_wysiwyg_frame").contents().find('body').createTOC();
                            clearInterval(eventCheckInterval);
                        }
                    }, 500);
                }
                htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
            } catch (exc) {

            }
        }
    });
    htmlEditor.editorEvents();

    //Compare versions

    jQuery("#compare_btn").off().on("click", function() {
        jQuery(this).button('loading');

        jQuery("#changeView").attr("disabled", true);
        var verLatest = jQuery("#compareDropdown1").val();
        var verPrev = jQuery("#compareDropdown2").val();
        var viewType = jQuery("#changeView").val();
        var type = viewType; // Check the type of result desired. 1= table form.
        jQuery("#updateNsave").attr("disabled", true); //Disable the save button while comparison is being made.

        /*if (viewType) {
         type = 2;
         }
         else {
         type = 1;
         }*/
        //        var type = 1;     // Check the type of result desired. 1= table form.
        htmlEditor.getComparativeResult(verPrev, verLatest, type);
    })
    tocInit();
};
// Functionality to toggle between switch & Build mode.
htmlEditor.onSwitchStateChange = function() {
    if (htmlEditor.switch_btn_state == 'build') {
        //jQuery('body').css('cursor', 'default');
        jQuery(".switch").css({"pointer-events":"none","opacity":".5"});
        jQuery("#multipleVideoModalPreview").modal('hide');
        jQuery('.switch.right .switch-button-label').removeClass('active');
        jQuery('.switch-button-label.build').addClass('active');
        htmlEditor.switchBuildTest(false);
        jQuery('.panel-refresh-reload').removeClass('hideElm');
    } else {
        htmlEditor.setTestProfileState(0);
        if (jQuery('.cke_button__source').hasClass('cke_button_on')) {
            jQuery(".cke_button_on").trigger("click");
        }
        //jQuery("#displayOptions").multiselect();
        jQuery('.switch.right .switch-button-label').removeClass('active');
        jQuery('.switch-button-label.test').addClass('active');
        htmlEditor.switchBuildTest(true);
        jQuery('.panel-refresh-reload').addClass('hideElm');
    }
    htmlEditor.onWindowResize();
};

//To open project preview from test mode
htmlEditor.goToPreview = function() {
    jQuery("#testPreviewModal").off().on('hidden.bs.modal', function() {
        jQuery("#testPreviewModal iframe").attr('src', '');
    });
    var ts_hash = '?_ts=' + Math.round(new Date().getTime() / 1000);
    jQuery("#testPreviewModal iframe").attr("src", clientUrl + '#/project_preview_test/' + project_id + ts_hash);
}

htmlEditor.switchBuildTest = function(flag) {
    if (flag) {
        htmlEditor.removeFrameSettings();
        jQuery('.left-pane').addClass('hideElm');
        jQuery('.middle-pane').addClass('hideElm');
        jQuery('.right-pane').addClass('hideElm');
        jQuery('.resizer-container').removeClass('hideElm');
        jQuery('.test-pane, .view-wrapper').removeClass('hideElm');
        jQuery('.test-pane *').on('click', function(e) {
            jQuery("#message").html("Change to Build Mode to see the changes").show().fadeOut(2000);
            return false;
        });
        jQuery("#highlighter").hide();
        htmlEditor.saveTempContent();

        /*
         //        htmlEditor.viewChange(0);
         var data = CKEDITOR.instances.editor1.getData();
         var elm = jQuery('.test-pane .view-wrapper');
         htmlEditor.includeCSS(elm); //Include css relevant to the project in the test mode.
         elm.contents().find('.testContainer').html(data);

         var htmlStr = '<div id="myModal" class="modal"><div class="modal-content"><div class="modal-cover"><span class="content"></span><button class="close">x</button></div><div class="modal-body" ></div></div></div>';
         elm.contents().find('.testContainer').append(htmlStr);
         var modal = elm.contents().find('.testContainer').find("#myModal");
         var modal_content = elm.contents().find('.testContainer').find("#myModal").find(".modal-content");
         var modalType = elm.contents().find('.testContainer').find("#myModal").find(".modal-content").find(".content");
         var close = elm.contents().find('.testContainer').find("#myModal").find(".modal-content").find(".close");
         var modal_body = elm.contents().find('.testContainer').find("#myModal").find(".modal-content").find(".modal-body");

         elm.contents().find('body').find('.dataset').on('click', function (e) {
         e.stopPropagation();

         elm.contents().find('body').css('overflow', 'hidden');
         if (e.toElement.parentNode.getAttribute('class') === 'dataset') {
         htmlEditor.datasetId = e.toElement.parentNode.getAttribute('data-dataset-id');
         htmlEditor.dataPath = e.toElement.parentNode.getAttribute('data-path');
         }

         else if (e.toElement.parentNode.parentNode.getAttribute('class') === 'dataset') {
         htmlEditor.datasetId = e.toElement.parentNode.parentNode.getAttribute('data-dataset-id');
         htmlEditor.dataPath = e.toElement.parentNode.parentNode.getAttribute('data-path');
         }
         else {
         htmlEditor.datasetId = e.toElement.getAttribute('data-dataset-id');
         htmlEditor.dataPath = e.toElement.getAttribute('data-path');
         }
         if (htmlEditor.dataPath === '') {
         modal_body.html('');
         myHtml = "no dataset records found";
         modalType.text('Data Set');
         modal_body.append(myHtml);
         modal.show();
         }
         else {

         modalType.text('Data Set');
         modal_body.html('');
         modal.show();
         jQuery.ajax({
         url: htmlEditor.dataPath,
         //async: false,
         method: 'GET',
         xhrFields: {
         withCredentials: true
         },
         crossDomain: true,
         success: function (data) {
         if (data.indexOf('advJson') === -1) {
         modal_body.html('');
         myHtml = "no dataset records found";
         modal_body.append(myHtml)
         }
         var parseDataSet = JSON.parse(data.replace("var advJsonData =", "")).collection.options;
         if (parseDataSet === undefined || htmlEditor.datasetId == null) {
         modal_body.html('');
         myHtml = "no dataset records found";
         modal_body.append(myHtml)
         }
         else {
         var html = "<ul class='lmDatasetUl'>";
         for (var i = 0; i < parseDataSet.length; i++) {
         html += '<li class="labelDatasetClass"><a style="display: block;" href=' + parseDataSet[i].mediaDetails.mediaPath + ' target="_blank">' + parseDataSet[i].labelName + '</a></li>'
         }
         html += "</ul>"

         modal_body.append(html);
         htmlEditor.focusableElementsString = 'a[href], area[href], input:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contentecitable]';
         htmlEditor.focusableElements = elm.contents()[0].querySelectorAll(htmlEditor.focusableElementsString);
         htmlEditor.focusableElements = Array.prototype.slice.call(htmlEditor.focusableElements);
         htmlEditor.firstTabStop = htmlEditor.focusableElements[0];
         htmlEditor.lastTabStop = htmlEditor.focusableElements[htmlEditor.focusableElements.length - 1];
         htmlEditor.firstTabStop.focus();
         }
         }
         });
         }

         });
         //multiple video pop up

         elm.contents().find('body').find('.multiplevideo').on('click', function (e) {
         e.stopPropagation();
         elm.contents().find('body').css('overflow', 'hidden');
         if (e.toElement.parentNode.getAttribute('class') === 'multiplevideo') {
         htmlEditor.multipleVideoId = e.toElement.parentNode.getAttribute('data-multiplevideo-id');
         htmlEditor.multipleVideoDataPath = e.toElement.parentNode.getAttribute('data-path');
         }

         else if (e.toElement.parentNode.parentNode.getAttribute('class') === 'multiplevideo') {
         htmlEditor.multipleVideoId = e.toElement.parentNode.parentNode.getAttribute('data-multiplevideo-id');
         htmlEditor.multipleVideoDataPath = e.toElement.parentNode.parentNode.getAttribute('data-path');
         }
         else {
         htmlEditor.multipleVideoId = e.toElement.getAttribute('data-dataset-id');
         htmlEditor.multipleVideoDataPath = e.toElement.getAttribute('data-path');
         }

         if (htmlEditor.multipleVideoDataPath === '') {
         modal_body.html('');
         myHtml = "no video records found";
         modalType.text('MultiVideo');
         modal_body.append(myHtml);
         modal.show();
         }
         else {
         modalType.text('MultiVideo');
         modal_body.html('');
         modal_content.addClass("lm_multi_vd");
         var htmlVideo = "<div class='lm_col_right'><div id='bright_video' class='embed-responsive embed-responsive-16by9'></div></div>";
         modal.show();
         modal_body.html();
         jQuery.ajax({
         url: htmlEditor.multipleVideoDataPath,
         //  async: false,
         method: 'GET',
         xhrFields: {
         withCredentials: true
         },
         crossDomain: true,
         success: function (data) {
         if (data.indexOf('advJson') === -1) {
         modal_body.html('');
         myHtml = "no video records found";
         modal_body.append(myHtml)
         }
         else {

         var parseDataSet = JSON.parse(data.replace("var advJsonData =", "")).collection.options;
         if (parseDataSet === undefined || parseDataSet.length === 0 || htmlEditor.multipleVideoId === null) {
         modal_body.html('');
         myHtml = "no video records found";
         modal_body.append(myHtml)
         }
         else {
         var myHtml = "<ul class='vidUl lm_col_left'>";
         for (var i = 0; i < parseDataSet.length; i++) {
         if (i === 0) {
         myHtml += '<li><a  href="javascript:void(0)" class="videoClass active" video_id="' + parseDataSet[i].mediaDetails.mediaId + '">' + parseDataSet[i].labelName + '</a></li>'
         }
         else {
         myHtml += '<li><a  href="javascript:void(0)" class="videoClass" video_id="' + parseDataSet[i].mediaDetails.mediaId + '">' + parseDataSet[i].labelName + '</a></li>'
         }

         }
         myHtml += "</ul>";

         modal_body.append(myHtml);

         modal_body.append(htmlVideo);
         modal_body.find('#bright_video').append('<iframe allowfullscreen="true" class="brightCoveVideo" height="300" id="wyjx83caac2b05d04bfab509428aefd5" mozallowfullscreen="true" name="framewyjx83caac2b05d04bfab509428aefd5" src="http://players.brightcove.net/2402232200001/default_default/index.html?videoId=123" style="border:none;overflow:auto;" webkitallowfullscreen="true" width="400"></iframe>');
         modal_body.append('<span style="clear:both"></span>');

         htmlEditor.brightCoveFrame = modal_body.find(".brightCoveVideo");

         htmlEditor.focusableElementsString = 'a[href], area[href], input:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contentecitable]';

         htmlEditor.focusableElements = elm.contents()[0].querySelectorAll(htmlEditor.focusableElementsString);
         htmlEditor.focusableElements = Array.prototype.slice.call(htmlEditor.focusableElements);
         htmlEditor.firstTabStop = htmlEditor.focusableElements[0];
         htmlEditor.lastTabStop = htmlEditor.focusableElements[htmlEditor.focusableElements.length - 1];
         htmlEditor.firstTabStop.focus();

         // video iframe elements
         //                            htmlEditor.focusableElementsBrightCove= htmlEditor.brightCoveFrame.contents()[0].querySelectorAll(htmlEditor.focusableElementsString);
         //                            htmlEditor.focusableElementsBrightCove = Array.prototype.slice.call(htmlEditor.focusableElementsBrightCove);
         //                            htmlEditor.lastTabStopBrightCove = htmlEditor.focusableElementsBrightCove[htmlEditor.focusableElementsBrightCove.length - 1];



         var brightCove = modal_body.find('.brightCoveVideo');
         var vdoClass = modal_body.find('.videoClass');
         var bright_CoveSrc = brightCove.attr('src').replace(brightCove.attr('src').split('=')[1], vdoClass.eq(0).attr('video_id'));
         brightCove.attr('src', bright_CoveSrc);
         vdoClass.on('click', function () {
         bright_CoveSrc = brightCove.attr('src').replace(brightCove.attr('src').split('=')[1], jQuery(this).attr('video_id'));
         brightCove.attr('src', bright_CoveSrc);

         });
         vdoClass.click(function () {
         vdoClass.removeClass('active');
         jQuery(this).addClass('active');
         });

         }
         }
         }
         });
         }
         });

         elm.contents().find('.testContainer').off().on('keydown', function (e) {
         if (e.keyCode == 27) {
         htmlEditor.hideModal();
         } else {
         htmlEditor.trapTabKey(e);
         }
         });

         htmlEditor.trapTabKey = function (e) {
         if (e.keyCode === 9) {
         if (e.shiftKey) {
         if (elm.contents()[0].activeElement === htmlEditor.firstTabStop) {
         e.preventDefault();
         htmlEditor.lastTabStop.focus();
         }
         } else {
         if (elm.contents()[0].activeElement === htmlEditor.lastTabStop) {
         e.preventDefault();
         htmlEditor.firstTabStop.focus();
         }
         //                    else if (htmlEditor.brightCoveFrame.contents()[0].activeElement === htmlEditor.lastTabStopBrightCove) {
         //                        e.preventDefault();
         //                        htmlEditor.firstTabStop.focus();
         //                    }
         }
         }
         }

         modal.off().on('click', function (event) {
         if (event.target == modal[0]) {
         htmlEditor.hideModal();
         }
         });
         close.on('click', function () {
         htmlEditor.hideModal();
         });
         htmlEditor.hideModal = function () {
         elm.contents().find('body').css('overflow', 'auto');
         modal.hide();
         modal_content.removeClass("lm_multi_vd");
         };
         htmlEditor.convertDivToFrame(elm.contents());
         htmlEditor.removeUnwanteds(elm.contents());
         elm.contents().find('body').find('.multiplevideo').find("*").each(function (key, value) {
         htmlEditor.stopPropagation(value);
         htmlEditor.mousePointer(value);
         });
         elm.contents().find('body').find('.dataset').find("*").each(function (key, value) {
         htmlEditor.stopPropagation(value);
         htmlEditor.mousePointer(value);
         });
         */
        jQuery('iframe.view-wrapper').contents().find('body *').on('click', function(e) {
            var tagName = jQuery(this)[0].tagName;
            if (tagName === 'A') {
                swal("Links do not function in Build or Test mode.")
                return false;
            }
        });
        setTimeout(function() {
            jQuery('.resize-view li').eq(0).trigger("click");
        }, 100);

        var frameEl = jQuery('#testCont').contents().find('body');
        frameEl.append('<style>.hideElm{display: none}</style>');
        /*jQuery(window).on("message", function (data) {
         //mock implementation of parents message listener
         var assessmentElement = elm.contents().find(".assessment");
         jQuery.each(assessmentElement, function (key, iframeElement) {
         iframeWindow = iframeElement.contentWindow;
         jQuery(iframeElement).attr("height", 350);
         jQuery(iframeElement).attr("width", 768);
         var dataPath = jQuery(iframeElement).attr("data-path");
         iframeWindow.postMessage({
         "data_path": dataPath
         }, "*");
         });
         });*/
    } else {
        jQuery('#testCont').attr("src", "");
        //var elm = jQuery('.test-pane .view-wrapper').contents().find('body');
        //elm.empty();
        //var receiver = document.getElementsByClassName('cke_wysiwyg_frame')[0].contentWindow;
        //var privateCont = {"api":"http://api.app.frost"};
        //receiver.postMessage("emptyBody^", privateCont.api);

        jQuery('.glyphicon').removeClass('hideElm');
        jQuery('.left-pane').removeClass('hideElm');
        jQuery('.middle-pane').removeClass('hideElm');
        if (!jQuery("#sourceVal:visible").length)
            jQuery('.right-pane').removeClass('hideElm');
        jQuery('.resizer-container').addClass('hideElm');
        jQuery('.test-pane').addClass('hideElm');
        jQuery("#highlighter").show();
        //htmlEditor.convertFrameToDiv();
        htmlEditor.populateMeta();
    }
};
htmlEditor.stopPropagation = function(value) {
    if (value.tagName === 'VIDEO' || value.className === 'gadget' || value.className === 'widget' || value.className === 'brightCoveVideo' || value.className === 'assessment' || value.className === 'exwidget' || value.className === 'figure' || value.className === 'informalfigure' || value.tagName === 'table' || value.className === 'figure') {
        value.addEventListener('click', function(e) {
            e.stopPropagation();
        });
        for (var i = 0; i < value.childElementCount; i++) {
            value.childNodes[i].addEventListener('click', function(e) {
                e.stopPropagation();
            });

        }
    }
}

htmlEditor.mousePointer = function(value) {
    if (value.tagName === 'VIDEO' || value.className === 'gadget' || value.className === 'widget' || value.className === 'brightCoveVideo' || value.className === 'assessment' || value.className === 'exwidget' || value.className === 'informalfigure' || value.tagName === 'table' || value.className === 'figure') {
        value.addEventListener('mouseover', function(e) {
            value.style.cursor = "default";
        });
        for (var i = 0; i < value.childElementCount; i++) {
            value.childNodes[i].addEventListener('mouseover', function(e) {
                this.style.cursor = "default";
            });

        }
    }
}
htmlEditor.viewChange = function(val) {
    jQuery('.test-pane').css({ 'overflow': 'hidden' });
    jQuery('.resize-view li').removeClass('active');
    jQuery('.resize-view li').eq(val).addClass('active');
    var height = jQuery(".test-pane").height() - 6;
    switch (val) {
        case 0:

            // var height = 554;
            var width = '100%';
            htmlEditor.setViewStyle(height, width);
            break;
        case 1:
            //var height = 554;
            var width = 1024;
            htmlEditor.setViewStyle(height, width);
            break;
        case 2:
            //var height = 554;
            var width = 600;
            htmlEditor.setViewStyle(height, width);
            break;
        case 3:
            // var height = 554;
            var width = 320;
            htmlEditor.setViewStyle(height, width);
            break;
        default:
    }
};
htmlEditor.setViewStyle = function(height, width) {
    var elm = jQuery('.test-pane .view-wrapper');
    elm[0].height = height;
    elm[0].width = width;
    elm.removeClass('hideElm');
    var win = document.getElementById("testCont").contentWindow;
    win.postMessage(true, "*");
};
//============  Test/Build change functionality ends=============================

//Add link button and metadata dropdowns to the editor.
htmlEditor.addToolbarBtn = function() {
    jQuery('.cont_link').remove();
    jQuery('.cke_button__insertimage').after("<a class='cont_link cke_button cke_button__link' title='Add Link'><span class='cke_button_icon cke_button__link_icon icons '>&nbsp;</span></a>");
    jQuery('.cont_link').off('click').on('click', function() {
        var selObj = htmlEditor.getEditorSelection();
        var selText = selObj.getSelectedText().trim();
        if (selText) {
            jQuery(this).attr({ 'data-toggle': 'modal', 'data-target': '#link_modal' });
        } else {
            jQuery(this).attr({ 'data-toggle': '', 'data-target': '' });
        }
        htmlEditor.addGlossary(selObj);
    });
    htmlEditor.patternMetadataInit();
    htmlEditor.epubTypeMetadataInit();
};
htmlEditor.patternMetadataInit = function() {
    jQuery('.meta-dropdown').remove();
    jQuery('.cke_button__source').parent().parent().after('<div class="dropdown meta-dropdown"> <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"> Styles <span class="caret"></span> </button> <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1"></ul></div>');
    jQuery('#dropdownMenu1').attr('disabled', true);
};
//Add EPUB-TYPE dropdown at toolbar

htmlEditor.epubTypeMetadataInit = function() {
    jQuery('.epub-dropdown').remove();
    jQuery('.cke_button__source').parent().parent().after('<div class="dropdown epub-dropdown"> <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="true"> EPUB type <span class="caret"></span> </button> <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2"></ul></div>');
    jQuery('#dropdownMenu2').attr('disabled', true);
};