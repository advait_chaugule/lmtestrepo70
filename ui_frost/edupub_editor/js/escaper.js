//Hit return key twice to activate doubleenter and take the cursor out of thec urrent pattern.

var escaper = function() {
    var _possibleParents = ["SECTION", "ASIDE", "FIGURE"];                      //The tags present in the pattern
    var currentPara = jQuery(htmlEditor.getEditorSelection().getStartElement().$); //Current location of cursor.
    currentPara.parents().each(function() {
        if (jQuery.inArray(this.tagName, _possibleParents) !== -1) {
            var theparent = jQuery(this);
            if (!theparent.parent().hasClass("chapter") && !theparent.hasClass("chapter") && !currentPara.text().length) {//Check if the parent is in the main "chapter" class or is the chapter itself or the double enter has been hit in between texts.
                theparent.after("<p id='pnew'>&nbsp;</p>");
                currentPara.prev("p").remove();
                currentPara.remove();
                // Cursor position should be on the newly created paragraph.
                var editor = CKEDITOR.instances.editor1;
                var range = editor.createRange();
                range.setStart(editor.document.getById('pnew'), 0);
                range.setEnd(editor.document.getById('pnew').getFirst(), 0); // <em>b^ar</em>
                editor.getSelection().selectRanges([range]);
                
                //Remove the dummy id
                editor.document.getById('pnew').removeAttribute("id");
            }
            return false;
        }
    })
}