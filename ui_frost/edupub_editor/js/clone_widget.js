/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
htmlEditor.reqFromClonedWidget = false;
htmlEditor.getWidgetCloneData = function(path, param, reqType) {
    var ajaxRes = {};
    jQuery.ajax({
        url: path,
        async: false,
        method: reqType,
        data: param,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(response) {
            ajaxRes = response;
        },
        error: function(err) {
            console.log('Window says: ' + err);
        }
    });
    return ajaxRes;
};

htmlEditor.intiateWidgetClone = function(frame) {
    //debugger;

    if (htmlEditor.thisFrame != frame)
        htmlEditor.thisFrame = frame;
    hideWidPreview();
    var frame_widgetId = htmlEditor.thisFrame.attr('widget-id') ? htmlEditor.thisFrame.attr('widget-id') : htmlEditor.thisFrame.attr('data-widget-id'),
        getCloneWidProjectListAPI = htmlEditor.PXEmappedAPI.getCloneWidProjectList + frame_widgetId + "?access_token=" + access_token,
        projectList = {},
        pageList = {},
        widgetList = {},
        modalContainer = jQuery('#widget_copy_modal').find('.tab-content'),

        framePlayer = widgetScope.launchPage[frame_widgetId],
        frameGUID = htmlEditor.thisFrame.attr('id');
    modalContainer.html('');
    try {
        projectList = htmlEditor.getWidgetCloneData(getCloneWidProjectListAPI, '', 'GET').data;
    } catch (exc) {
        projectList = false;
    }

    if (projectList) {
        generateWidProjList(projectList);
    }

    function generateWidProjList(obj) {

        var htmlStr = '<div role="tabpanel" class="tab-pane active"><div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" id="prjctName" aria-expanded="true"><span id="selectProject1">Select Project</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3" id="widProjList" style="max-height:80px">';

        jQuery.each(obj, function(i, v) {
            var projectId = v.id,
                projectName = v.name;
            htmlStr += '<li role="presentation" projId="' + projectId + '"><a role="menuitem" tabindex="-1" href="javascript:void(0)">' + projectName + '</a></li>';
        });
        htmlStr += '</ul></div></div><div class="clearfix"></div><div class="widClonePageList"></div>';

        modalContainer.html(htmlStr);

        var bindWidProjectListEvent = (function() {

            modalContainer.find('#widProjList li').on('click', function() {
                var projID = jQuery(this).attr('projId');
                var page_name = (jQuery(this).find('a').text());
                jQuery("#selectProject1").text(page_name);
                getWidPageList(projID);
                hideWidPreview();
            });
            if (htmlEditor.isGlobalProject) {
                jQuery('#prjctName').hide();
                getWidPageList(project_id);
            }
        })();
    }

    function getWidPageList(projID) {
        var getParam = {
            "project_id": projID,
            "widget_id": frame_widgetId
        };
        try {
            pageList = htmlEditor.getWidgetCloneData(htmlEditor.PXEmappedAPI.getCloneWidPageList, getParam, 'GET').data;
        } catch (exc) {
            pageList = false;
        }

        if (pageList) {
            generateWidPageList(pageList, projID)
        }
    }

    function generateWidPageList(obj, projID) {
        var htmlStr = '<div role="tabpanel" class="tab-pane active"><div class="dropdown link-dropdown"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true"><span id="selectedPage1">Select Page</span><span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3" id="widPageList" style="max-height:80px">';

        jQuery.each(obj, function(i, v) {
            var nodeId = v.id,
                projectName = v.name;
            htmlStr += '<li role="presentation" nodeId="' + nodeId + '"><a role="menuitem" tabindex="-1" href="javascript:void(0)">' + projectName + '</a></li>';
        });
        htmlStr += '</ul></div></div><div class="clearfix"></div><div class="widCloneWidgetList"></div>';
        modalContainer.find('.widClonePageList').html(htmlStr);

        var bindWidPageListEvent = (function() {
            modalContainer.find('#widPageList li').on('click', function() {
                var projectID = projID,
                    nodeID = jQuery(this).attr('nodeId');
                var page_name = (jQuery(this).find('a').text());
                jQuery("#selectedPage1").text(page_name);
                getWidgetList(projectID, nodeID);
                hideWidPreview();
            });
        })();
    }

    function getWidgetList(projID, nodeID) {
        //debugger;
        var getParam = {
            "project_id": projID,
            "widget_id": frame_widgetId,
            "object_id": nodeID
        };

        try {
            widgetList = htmlEditor.getWidgetCloneData(htmlEditor.PXEmappedAPI.getCloneWidgetList, getParam, 'GET').data;
        } catch (exc) {
            widgetList = false;
        }

        if (widgetList) {
            generateWidList(widgetList, projID, nodeID)
        }
    }

    function generateWidList(obj, projID, nodeID) {
        var htmlStr = '<div class="showOptions">';
        jQuery.each(obj, function(i, v) {
            var widName = v.name,
                repoID = v.repo_id,
                jsonPath = v.json_file_path;

            htmlStr += '<div class="chapName"><div class="chapter-details" projId="' + projID + '" widId="' + frame_widgetId + '" objId="' + nodeID + '" repoId="' + repoID + '" dataPath="' + jsonPath + '">' + widName + '</div></div>';
        });
        htmlStr += '<div class="clearfix"></div></div>';
        modalContainer.find('.widCloneWidgetList').html(htmlStr);

        var postObjParam = {},
            bindWidListEvent = (function() {

                modalContainer.find('.widCloneWidgetList .chapName').on('click', function() {
                    //debugger;
                    postObjParam = {};
                    jQuery('.chapName').removeClass('activeSelectedWidget');
                    jQuery(this).addClass('activeSelectedWidget');
                    var projectID = jQuery(this).find('.chapter-details').attr('projid'),
                        objectID = jQuery(this).find('.chapter-details').attr('objid'),
                        repoID = jQuery(this).find('.chapter-details').attr('repoid'),
                        dataPath = jQuery(this).find('.chapter-details').attr('datapath');
                    postObjParam = {
                        "projectID": projectID,
                        "objectID": objectID,
                        "repoID": repoID,
                        "dataPath": dataPath
                    };
                    setWidClonePreview(framePlayer, dataPath);
                });

                jQuery('#widgetCopyCont').on('click', function() {
                    cloneWidData(postObjParam);
                });
            })();

        function setWidClonePreview(player, data) {
            var playerFrame = jQuery('#widget_copy_modal').find('iframe');
            playerFrame.attr({
                'data-path': data,
                'src': player
            });
            htmlEditor.reqFromClonedWidget = true;
            showWidPreview();
        }
    }

    function cloneWidData(param) {
        //htmlEditor.PXEmappedAPI.setCloneWidgetDataPath
        var setParam = {
                "project_id": param.projectID,
                "widget_id": frame_widgetId,
                "object_id": param.objectID,
                "repo_id": param.repoID,
                "copy_to_object_id": node_id,
                "copy_to_project_id": project_id,
                "copy_to_project_type_id": project_type_id,
                "new_widget_data_id": frameGUID
            },
            newPath = (htmlEditor.getWidgetCloneData(htmlEditor.PXEmappedAPI.setCloneWidgetDataPath, setParam, 'POST')).json_file_path;

        htmlEditor.thisFrame.attr('data-path', newPath);
        htmlEditor.reqFromClonedWidget = false;
        jQuery('#widget_copy_modal').modal('hide');
        htmlEditor.enableSave();
    }

    function hideWidPreview() {
        jQuery('.wid-preview').slideUp();
    }

    function showWidPreview() {
        jQuery('.wid-preview').slideDown();
    }

};