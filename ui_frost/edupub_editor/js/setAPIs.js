////        ======= Function to get the params of various keys =========
String.prototype.getParam = function(str) {
    str = str.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]*" + str + "=([^&#]*)");
    var results = regex.exec(this);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

var str = decodeURIComponent(window.location);
var project_id = str.getParam("project_id");
var node_id = str.getParam("node_id");
var node_title = str.getParam("title");
var node_type_name = str.getParam("node_type_name");
var node_type_id = str.getParam("node_type_id");
var access_token = str.getParam("access_token");
var version_id = str.getParam("version_id");
var section_id = str.getParam("section_id");
var glossaryId = "";
var project_type_id = str.getParam("project_type_id");
var user_id = "";
var content_from = str.getParam("content_from");
//        ===============================================================

htmlEditor.PXEmappedAPI = {
    "getContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token + '&version_id=' + version_id + '&project_id=' + project_id + '&s3=0',
    "getPatternsAPI": serviceEndPoint + 'api/v1/project-pattern?access_token=' + access_token + '&project_id=' + project_id + '&pattern_status=1001&called_from=editor',
    "getWidgetAPI": serviceEndPoint + 'api/v1/widget-to-editor' + '?access_token=' + access_token + '&project_id=' + project_id,
    "saveContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token,
    "saveSavingNote": serviceEndPoint + 'api/v1/version-comment/object_id/' + node_id + '/project_id/' + project_id + '?access_token=' + access_token,
    "getLeftPaneAPI": serviceEndPoint + 'api/v1/content-left-panel?access_token=' + access_token,
    "getGlossaryAPI": serviceEndPoint + 'api/v1/glossary?access_token=' + access_token + '&project_id=' + project_id,
    "saveGlossaryAPI": serviceEndPoint + 'api/v1/glossary',
    "editGlossaryAPI": serviceEndPoint + 'api/v1/glossary',
    "saveProjectPatternAPI": serviceEndPoint + 'api/v1/project-pattern',
    "getAllReviews": serviceEndPoint + "api/v1/ticket-all/project_id/" + project_id + "/is_global/0?access_token=" + access_token + "&object_id=" + node_id,
    "createCommentAPI": serviceEndPoint + 'api/v1/ticket-reply?access_token=' + access_token,
    "createTicketAPI": serviceEndPoint + 'api/v1/ticket?access_token=' + access_token,
    "getUsersAPI": serviceEndPoint + 'api/v1/project-users/project_id/' + project_id + '?access_token=' + access_token,
    "getAllStatus": serviceEndPoint + 'api/v1/ticket-types-status?access_token=' + access_token,
    "getAllVersionsAPI": serviceEndPoint + "api/v1/version-all/project_id/" + project_id + "?access_token=" + access_token + "&object_id=" + node_id,
    "validateContentAPI": serviceEndPoint + 'api/v1/validate-content/project_id/' + project_id + '/object_id/' + node_id + '?access_token=' + access_token,
    "getAllImages": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": true, "videos": false, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": true })),
    "getLocalImagesAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": true, "videos": false, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": false })),
    "getGlobalImagesAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": true, "videos": false, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": false, "global": true })),
    "getAllVideos": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": true, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": true })),
    "getAllVtt": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "vtt": true, "videos": false, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": true })),
    "getLocalVtt": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "vtt": true, "videos": false, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": false })),
    "getGlobalVtt": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "vtt": true, "videos": false, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": false, "global": true })),
    "getLocalVidsAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": true, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": false })),
    "getGlobalVidsAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": true, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": false, "global": true })),

    "getAllAudios": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": false, "audio": true, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": true })),
    "getLocalAudiosAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": false, "audio": true, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": false })),
    "getGlobalAudiosAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": false, "audio": true, "gadgets": false })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": false, "global": true })),


    "updateLockTimeAPI": serviceEndPoint + 'api/v1/update-lock-time?access_token=' + access_token + '&object_id=' + node_id,
    "changeObjectStatusAPI": serviceEndPoint + "api/v1/change-status?access_token=" + access_token + "&object_id=" + node_id + "&editor_flag_on=0&editor_flag_off=1",
    "projectDetailWithPermissionsAPI": serviceEndPoint + "api/v1/projects/project_id/" + project_id + "?access_token=" + access_token,
    "patternClasses": serviceEndPoint + "api/v1/project-pattern-classes?access_token=" + access_token + "&project_id=" + project_id,
    "compareVersions": serviceEndPoint + "api/v1/compare-files?access_token=" + access_token,
    "compareContentToBeMerged": serviceEndPoint + "api/v1/compare-files-runtime?access_token=" + access_token,
    "quadAPI": serviceEndPoint + "api/v1/quad-object?access_token=" + access_token,
    "tocAPI": serviceEndPoint + "api/v1/projects/project_id/" + project_id + "?access_token=" + access_token,
    "saveWidgetDataAPI": serviceEndPoint + "api/v1/external-widget?access_token=" + access_token,
    "getGadgetManifest": serviceEndPoint + "api/v1/widget-manifest", //The remaining part in getGadgetPage fucntion.
    "getAllGadgets": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": false, "gadgets": true })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": true })),
    "getLocalGadgets": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": false, "gadgets": true })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": true, "global": false })),
    "getGlobalGadgets": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({ "images": false, "videos": false, "gadgets": true })) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({ "local": false, "global": true })),
    "sendAssetsForCheck": serviceEndPoint + "api/v1/change-access?access_token=" + access_token,
    "getCloneWidProjectList": serviceEndPoint + "api/v1/project-by-widget/widget_id/",
    "getCloneWidPageList": serviceEndPoint + "api/v1/object-by-project?access_token=" + access_token,
    "getCloneWidgetList": serviceEndPoint + "api/v1/widget-by-object?access_token=" + access_token,
    "setCloneWidgetDataPath": serviceEndPoint + "api/v1/copy-widget?access_token=" + access_token,
    "lockPage": serviceEndPoint + 'api/v1/lock-object?access_token=' + access_token,
    "releaseLock": serviceEndPoint + 'api/v1/release-locked-object?access_token=' + access_token,
    "importExternalWidgetData": serviceEndPoint + 'api/v1/import-external-widget-data?access_token=' + access_token,
    "assessmentGetUrl": serviceEndPoint + 'api/v1/assessment/project_id/' + project_id + '?access_token=' + access_token + '&assessment_local_global={"local":true,"global":true}',

    "localAssessmentGetUrl": serviceEndPoint + 'api/v1/assessment/project_id/' + project_id + '?access_token=' + access_token + '&assessment_local_global={"local":true,"global":false}',
    "globalAssessmentGetUrl": serviceEndPoint + 'api/v1/assessment/project_id/' + project_id + '?access_token=' + access_token + '&assessment_local_global={"local":false,"global":true}',

    "questionGetUrl": serviceEndPoint + 'api/v1/assessment-question',
    "assessmentDataSaveUrl": serviceEndPoint + 'api/v1/assessment-object?access_token=' + access_token + "&project_id=" + project_id + "&project_type_id=" + project_type_id,
    "dataSetApi": serviceEndPoint + 'api/v1/dataset-editor/project_id/' + project_id + '?access_token=' + access_token + '&collection_type=DATASET',
    "getvideoPlaylistApi": serviceEndPoint + 'api/v1/multivideo?access_token=' + access_token + '&project_id_' + project_id,
    "getVideoList": serviceEndPoint + 'api/v1/multivideo-record/',
    "saveFootnoteAPI": serviceEndPoint + 'api/v1/footnote/project_id/' + project_id + '/object_id/' + node_id + '?access_token=' + access_token,
    "editFootnoteAPI": serviceEndPoint + 'api/v1/footnote/project_id/' + project_id + '/object_id/' + node_id + '/footnote_id',
    "getFootnoteAPI": serviceEndPoint + 'api/v1/footnote/project_id/' + project_id + '/object_id/' + node_id,
    "getDatasetListRecord": serviceEndPoint + 'api/v1/dataset-records/',
    "saveDataSetSourcePath": serviceEndPoint + 'api/v1/dataset-json',
    "saveTempContentAPI": serviceEndPoint + 'api/v1/toc-temp-content?access_token=' + access_token,
    "saveCfiStateAPI": serviceEndPoint + '/api/v1/custom-cfi/project_id/' + project_id + '/object_id/' + node_id + '?access_token=' + access_token,
    "customCfiAPI": serviceEndPoint + '/api/v1/custom-cfi/project_id/' + project_id + '/object_id/' + node_id
};