/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*Test Case for Permission API*/

describe("Project Permissions API", function () {
    var value;
    setTimeout(function () {
        jQuery('.jasmine_html-reporter').parent().children().eq(0).remove();
        jQuery('.sweet-alert').remove();
    }, 100);
    beforeEach(function (done) {
        setTimeout(function () {
            value = 0;
            done();
        }, 1);
    });

    it("", function (done) {
        value++;
        expect(value).toBeGreaterThan(0);
        done();
    });

    describe("Calling AJAX", function () {
        var originalTimeout, permissionStatus;
        beforeEach(function () {
            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        });

        it("Getting Permissions", function (done) {
            permissionStatus = htmlEditor.loadProjectDetailWIthPermissions();
            setTimeout(function () {
                done();
            }, 1000);
        });

        afterEach(function () {
            jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
            expect(permissionStatus.status).toEqual(200);
        });

        it("Status of Patterns Population", function () {
            var apiResponseToPatterns = htmlEditor.permissionTo.showPattern;

            if (apiResponseToPatterns === false) {
                expect(jQuery(".right-pane").hasClass('hideElm')).toBeTruthy();
            } else {
                expect(jQuery(".right-pane").hasClass('hideElm')).toBeFalsy();
            }
        });

        it("Status of Allow Save", function () {
            var apiAllowSave = htmlEditor.permissionTo.save;

            if (apiAllowSave === false) {
                expect(jQuery('#save_toc_id').hasClass('hideElm')).toBeTruthy();
            } else {
                expect(jQuery('#save_toc_id').hasClass('hideElm')).toBeFalsy();
            }
        });

        it("Status of Smart Highlight", function () {
            var apiSmartHighlight = htmlEditor.permissionTo.smartHighlight;

            if (apiSmartHighlight === false) {
                expect(jQuery('#highlighter').hasClass('hideElm')).toBeTruthy();
            } else {
                expect(jQuery('#highlighter').hasClass('hideElm')).toBeFalsy();
            }
        });

        it("Status of View Source", function () {
            var apiToViewSource = htmlEditor.permissionTo.viewSourceCode;

            if (apiToViewSource === false) {
                expect(jQuery('.cke_button__source').hasClass('hideElm')).toBeTruthy();
            } else {
                expect(jQuery('.cke_button__source').hasClass('hideElm')).toBeFalsy();
            }
        });
    });
});