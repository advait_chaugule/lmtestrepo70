/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var frostAPISettings = {
    'access_token': 'c389f4985b89aa7ec0169a85cad90ec0af897bd6',
    'project_id': '1',
    'node_id': 'fizl6401031a83e154bf685a49fc6966',
    'node_title': 'Testing',
    'node_type_name': '',
    'node_type_id': '212',
    'project_type_id': '4',
    'section_id': '',
    'version_id': '',
    'glossaryId': '',
    'user_id': ''
};

/**
 * 
 * Get Access Token and do login
 */
jQuery.when(accessTokenObj = htmlEditor.getAccessToken(oauthUrl)).done(function () {
    accessTokenObj = accessTokenObj.responseJSON;
    frostAPISettings.access_token = accessTokenObj.access_token;
    var params = {};
    params.username = "admin";
    params.password = "admin";
    params.access_token = accessTokenObj.access_token;
    jQuery.when(loginRes = htmlEditor.testLogin(params)).done(function () {
        //console.log(loginRes);
    });
});

var access_token = frostAPISettings.access_token,
        project_id = frostAPISettings.project_id,
        node_id = frostAPISettings.node_id,
        node_title = frostAPISettings.node_title,
        node_type_name = frostAPISettings.node_type_name,
        node_type_id = frostAPISettings.node_type_id,
        project_type_id = frostAPISettings.project_type_id,
        section_id = frostAPISettings.section_id,
        version_id = frostAPISettings.version_id,
        glossaryId = frostAPISettings.glossaryId,
        user_id = frostAPISettings.user_id;

htmlEditor.PXEmappedAPI = {
    "getContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token + '&version_id=' + version_id + '&project_id=' + project_id + '&s3=0',
    "getPatternsAPI": serviceEndPoint + 'api/v1/project-pattern?access_token=' + access_token + '&project_id=' + project_id + '&pattern_status=1001',
    "getWidgetAPI": serviceEndPoint + 'api/v1/widget-to-editor' + '?access_token=' + access_token + '&project_id=' + project_id,
    "saveContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token,
    "saveSavingNote": serviceEndPoint + 'api/v1/version-comment/object_id/' + node_id + '/project_id/' + project_id + '?access_token=' + access_token,
    "getLeftPaneAPI": serviceEndPoint + 'api/v1/content-left-panel?access_token=' + access_token,
    "getGlossaryAPI": serviceEndPoint + 'api/v1/glossary?access_token=' + access_token + '&project_id=' + project_id,
    "saveGlossaryAPI": serviceEndPoint + 'api/v1/glossary',
    "editGlossaryAPI": serviceEndPoint + 'api/v1/glossary',
    "saveProjectPatternAPI": serviceEndPoint + 'api/v1/project-pattern',
    "getAllReviews": serviceEndPoint + "api/v1/ticket-all/project_id/" + project_id + "/is_global/0?access_token=" + access_token + "&object_id=" + node_id,
    "createCommentAPI": serviceEndPoint + 'api/v1/ticket-reply?access_token=' + access_token,
    "createTicketAPI": serviceEndPoint + 'api/v1/ticket?access_token=' + access_token,
    "getUsersAPI": serviceEndPoint + 'api/v1/project-users/project_id/' + project_id + '?access_token=' + access_token,
    "getAllStatus": serviceEndPoint + 'api/v1/ticket-types-status?access_token=' + access_token,
    "getAllVersionsAPI": serviceEndPoint + "api/v1/version-all/project_id/" + project_id + "?access_token=" + access_token + "&object_id=" + node_id,
    "validateContentAPI": serviceEndPoint + 'api/v1/validate-content/project_id/' + project_id + '/object_id/' + node_id + '?access_token=' + access_token,
    "getAllImages": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": true, "videos": false, "gadgets": false})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": true, "global": true})),
    "getLocalImagesAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": true, "videos": false, "gadgets": false})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": true, "global": false})),
    "getGlobalImagesAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": true, "videos": false, "gadgets": false})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": false, "global": true})),
    "getAllVideos": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": false, "videos": true, "gadgets": false})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": true, "global": true})),
    "getLocalVidsAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": false, "videos": true, "gadgets": false})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": true, "global": false})),
    "getGlobalVidsAPI": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": false, "videos": true, "gadgets": false})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": false, "global": true})),
    "updateLockTimeAPI": serviceEndPoint + 'api/v1/update-lock-time?access_token=' + access_token + '&object_id=' + node_id,
    "changeObjectStatusAPI": serviceEndPoint + "api/v1/change-status?access_token=" + access_token + "&object_id=" + node_id + "&editor_flag_on=0&editor_flag_off=1",
    "projectDetailWithPermissionsAPI": serviceEndPoint + "api/v1/projects/project_id/" + project_id + "?access_token=" + access_token,
    "patternClasses": serviceEndPoint + "api/v1/project-pattern-classes?access_token=" + access_token + "&project_id=" + project_id,
    "compareVersions": serviceEndPoint + "api/v1/compare-files?access_token=" + access_token,
    "compareContentToBeMerged": serviceEndPoint + "api/v1/compare-files-runtime?access_token=" + access_token,
    "quadAPI": serviceEndPoint + "api/v1/quad-object?access_token=" + access_token,
    "tocAPI": serviceEndPoint + "api/v1/projects/project_id/" + project_id + "?access_token=" + access_token,
    "saveWidgetDataAPI": serviceEndPoint + "api/v1/external-widget?access_token=" + access_token,
    "getGadgetManifest": serviceEndPoint + "api/v1/widget-manifest", //The remaining part in getGadgetPage fucntion.
    "getAllGadgets": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": false, "videos": false, "gadgets": true})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": true, "global": true})),
    "getLocalGadgets": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": false, "videos": false, "gadgets": true})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": true, "global": false})),
    "getGlobalGadgets": serviceEndPoint + "api/v1/assets?access_token=" + access_token + "&project_id=" + project_id + "&taxonomy_id=0&asset_filter=" + encodeURIComponent(JSON.stringify({"images": false, "videos": false, "gadgets": true})) + "&asset_local_global=" + encodeURIComponent(JSON.stringify({"local": false, "global": true})),
    "sendAssetsForCheck": serviceEndPoint + "api/v1/change-access?access_token=" + access_token,
    "getCloneWidProjectList": serviceEndPoint + "api/v1/project-by-widget/widget_id/",
    "getCloneWidPageList": serviceEndPoint + "api/v1/object-by-project?access_token=" + access_token,
    "getCloneWidgetList": serviceEndPoint + "api/v1/widget-by-object?access_token=" + access_token,
    "setCloneWidgetDataPath": serviceEndPoint + "api/v1/copy-widget?access_token=" + access_token,
    "lockPage": serviceEndPoint + 'api/v1/lock-object?access_token=' + access_token,
    "releaseLock": serviceEndPoint + 'api/v1/release-locked-object?access_token=' + access_token
};

var swal = function (msg) {
};

var CKEDITOR = {};