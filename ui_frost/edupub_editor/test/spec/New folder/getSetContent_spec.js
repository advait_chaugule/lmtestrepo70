describe("Test on Getting and Setting Content", function() {
    it("Creating dummy frame", function(){
        jQuery('body').append('<iframe class="cke_wysiwyg_frame" style="display:none"></iframe>');
    });

    it('Checking value of variable htmlEditor.needsToBeMerged', function() {
        expect(htmlEditor.needsToBeMerged).toEqual(false);
    });

    it("Calling method getJsonValue", function() {
        htmlEditor.getJsonValue();
    });

    it("Calling method to Lock", function() {
        var user = 1,
            userId = 1;
        htmlEditor.setLockStatus(user, userId);
    });

    it("Checking if statement", function() {
        var user = 1,
            userId = 1;
        htmlEditor.islocked = true;
        htmlEditor.setLockStatus(user, userId);
        expect(jQuery("#lockPage").find(".fa-unlock").hasClass('fa-unlock')).toBeFalsy();
    });

    it("Checking else statement", function() {
        var user = 2,
            userId = 1;
        htmlEditor.islocked = true;
        htmlEditor.setLockStatus(user, userId);
    });

    it("Checking current node", function() {
        var current_node = 1;
        htmlEditor.getJsonValueSetCurrentNode(current_node);
        expect(htmlEditor.current_node).toEqual(current_node);
    });

    it("Calling method getJsonValueOnDone", function() {
        htmlEditor.getJsonValueOnDone(contentAPIResponse);
    });

    it("Checking lock status", function() {
        var lockStatus = contentAPIResponse.data.is_locked;
        expect(htmlEditor.islocked).toEqual(lockStatus);
    });

    it("Calling method to add Image Timestamp", function() {
        jQuery('body').append('<img class="sample_img_1" src="sample_img_1.jpg" style="display:none"/><img class="sample_img_2" src="sample_img_2.jpg" style="display:none"/>');
        htmlEditor.imageTimeStamp(jQuery('body'),true);
        jQuery('body').find('.sample_img_1').remove();
        jQuery('body').find('.sample_img_2').remove();
    });

    it("Checking method Image Timestamp if statement", function(){
        var imgStr = '<img class="sample_img_1" src="sample_img_1.jpg" style="display:none"/><img class="sample_img_2" src="sample_img_2.jpg" style="display:none"/>'
        jQuery('body').append(imgStr);
        htmlEditor.imageTimeStamp(jQuery('body'),true);
        jQuery('body').find('.sample_img_1').remove();
        jQuery('body').find('.sample_img_2').remove();
    });

    it("Checking method Image Timestamp else statement", function(){
        var imgStr = '<img class="sample_img_1" src="sample_img_1.jpg" style="display:none"/><img class="sample_img_2" src="sample_img_2.jpg" style="display:none"/>'
        jQuery('body').append(imgStr);
        htmlEditor.imageTimeStamp(jQuery('body'),false);
        jQuery('body').find('.sample_img_1').remove();
        jQuery('body').find('.sample_img_2').remove();
    });

    it("Calling method to add Video Timestamp", function() {
        var videoStr = '<video class="sample_video_1" style="display:none"><source src="sample_video_1.mp4"></source></video><video class="sample_video_2" style="display:none"><source src="sample_video_2.mp4"></source></video>';
        jQuery('body').append(videoStr);
        htmlEditor.videoTimeStamp (jQuery('body'),true);
        jQuery('body').find('.sample_video_1').remove();
        jQuery('body').find('.sample_video_2').remove();

    });

    it("Checking method Video Timestamp if statement", function(){
        var videoStr = '<video class="sample_video_1" style="display:none"><source src="sample_video_1.mp4"></source></video><video class="sample_video_2" style="display:none"><source src="sample_video_2.mp4"></source></video>';
        jQuery('body').append(videoStr);
        htmlEditor.videoTimeStamp (jQuery('body'),true);
        jQuery('body').find('.sample_video_1').remove();
        jQuery('body').find('.sample_video_2').remove();
    });
    
    it("Checking method Video Timestamp else statement", function(){
        var videoStr = '<video class="sample_video_1" style="display:none"><source src="sample_video_1.mp4"></source></video><video class="sample_video_2" style="display:none"><source src="sample_video_2.mp4"></source></video>';
        jQuery('body').append(videoStr);
        htmlEditor.videoTimeStamp (jQuery('body'),false);
        jQuery('body').find('.sample_video_1').remove();
        jQuery('body').find('.sample_video_2').remove();
    });

    it("Calling method setData",function(){
        htmlEditor.setData(contentAPIResponse.data.description);
    });

    it("Calling method getLatestSavedContent", function(){
        htmlEditor.getLatestSavedContent();
    });

    it("Calling method loadPatterns", function(){
        htmlEditor.loadPatterns();
    });

    it("Calling method loadWidgets", function(){
        htmlEditor.loadWidgets(patternResponse);
    });
});