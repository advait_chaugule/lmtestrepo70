
var jQuery = $.noConflict();

var htmlEditor = {};

htmlEditor.infoObj = {};
htmlEditor.islocked = false;

var editor_patterns;

var returnVal = {
    "content": '',
    "leftPanelTree": '',
    "img_urls": '',
    "validation_response_error": false
};

var guid = function () {
    var separator = '';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    function s4_alpha() {
        var max = 122, min = 97;
        var return_str = "";
        for (var i = 0; i <= 3; i++) {
            var rand = Math.floor(Math.random() * (max - min + 1)) + min
            return_str += String.fromCharCode(rand);
        }
        return return_str;
    }
    return function () {
        return s4_alpha() + s4() + separator + s4() + separator + s4() + separator + s4() + separator + s4() + s4() + s4();
    };
};

jQuery(document).ready(function () {
    htmlEditor.onDocumentReady();
    htmlEditor.setPageTitle();
});

jQuery(window).resize(function () {
    htmlEditor.onWindowResize();
});

htmlEditor.setPageTitle = function () {
    var titleTag = jQuery("head").find("title");
    if (titleTag.length) {
        titleTag.text(node_title);
    }
    else {
        jQuery("head").append("<title>" + node_title + "</title>");
    }
};

htmlEditor.onDocumentReady = function () {
    for (name in CKEDITOR.instances)
    {
        CKEDITOR.instances[name].destroy();             //Destroy all instances of the editor.
    }
    htmlEditor.getJsonValue();
    jQuery.when(htmlEditor.loadProjectDetailWIthPermissions()).done(function () {
        if (htmlEditor.permissionTo.dragPattern) {
            htmlEditor.loadPatterns();
        }
        else {
            jQuery(".right-pane").addClass("hideElm");
        }

        if (!htmlEditor.permissionTo.save) {
            jQuery('#save_toc_id').addClass('hideElm');
        }

        if (!htmlEditor.permissionTo.smartHighlight) {
            jQuery('#highlighter').addClass('hideElm').removeClass("active");
        }

        if (htmlEditor.permissionTo.viewSourceCode) {
            jQuery('.cke_button__source').removeClass('hideElm');
        }
        htmlEditor.callTickets();
        htmlEditor.getUsers(); // Fetches all the users associated with the project.
        htmlEditor.getStatus();
        htmlEditor.getAllVersions();
        htmlEditor.checkUserAct();
    })


};

htmlEditor.ajaxCalls = function (url) {                          //Make all ajax calls using this function
    return jQuery.ajax({
        url: url,
        async: true,
        beforeSend: function (xhr) {
            //            htmlEditor.createLoader("editor");
        },
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {

        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Unable to fetch the data. " + textStatus);
            //window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
    });
};

htmlEditor.changeLockStatus = function () {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.updateLockTimeAPI,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
        },
        error: function () {
            //alert("Error occured");
        }
    });
};

htmlEditor.checkUserAct = function (evt) {
    try {
        if (evt.type == 'keydown') {
            htmlEditor.enableSave();
        }
    }
    catch (exc) {
        //console.log("Window says: " + exc);
    }

    if (htmlEditor.islocked) {
        if (htmlEditor.interval) {
            clearInterval(htmlEditor.interval);
            htmlEditor.interval = 0;
        }

        htmlEditor.interval = setInterval(function () {
            //Function that would fire on interval
            htmlEditor.timeout = setTimeout(function () {
                htmlEditor.saveContent();
                htmlEditor.backToDashboard();
            }, 60000);
            swal({
                //            title: "You have been inactive for 5 minutes",
                //            html:     'You can use <b>bold text</b>, ' +     '<a href="//github.com">links</a> ' +     'and other HTML tags',
                html: "<h1>You have been inactive for 5 minutes</h1><p style='font-size:20px;'>Do you want to leave the page? </p><br><p style='font-size:12px;'>You would be automatically redirected to Dashboard in 60seconds</p>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    htmlEditor.saveContent();
                    htmlEditor.backToDashboard();
                } else {
                    htmlEditor.checkUserAct();
                    if (htmlEditor.timeout) {
                        clearTimeout(htmlEditor.timeout);
                    }
                }
            });

        }, 300000); //Default Value=300000

        //    htmlEditor.interval = setInterval(function() {
        //        //Function that would fire on interval
        //        htmlEditor.changeLockStatus();
        //        swal({
        //            title: "You are inactive for 5 minutes",
        //            text: "Do you want to leave the page?",
        //            type: "warning",
        //            showCancelButton: true,
        //            confirmButtonColor: "#DD6B55",
        //            confirmButtonText: "Yes",
        //            cancelButtonText: "No",
        //            closeOnConfirm: true,
        //            closeOnCancel: true,
        //            timer: 10000,
        //        }, function(isConfirm) {
        //            if (isConfirm) {
        //                htmlEditor.saveContent();
        //                htmlEditor.backToDashboard();
        //            } else {
        //                htmlEditor.checkUserAct();
        //            }
        //        });
        //    }, 30000);
    } else {
        clearTimeout(htmlEditor.interval);
    }
};

htmlEditor.initiateEditor = function () {
    try {
        CKEDITOR.instances.editor1.removeAllListeners(); // just in case to prevent the customconfig error being faced.
        delete CKEDITOR.instances.editor1;
    }
    catch (exc) {

    }
    try{
        CKEDITOR.replace('editor1');
    }
    catch(exc){
        console.log("Editor not found");
    }
    
};

htmlEditor.contentUpdated = false;

htmlEditor.checkToBackToDashboard = function () {
    setTimeout(function () {
        var lastContent = htmlEditor.getLatestSavedContent();
        if (lastContent !== htmlEditor.currContent) {
//    if (htmlEditor.contentUpdated) {
            swal({
                title: "You are about to leave the page",
                text: " Want to save the content?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                allowOutsideClick: false
            }, function (isConfirm) {
                if (isConfirm) {
                    htmlEditor.saveContent();
                    htmlEditor.backToDashboard();
                } else {
                    htmlEditor.backToDashboard();
                }
            });
        }
        else {
            htmlEditor.backToDashboard();
        }
    }, 1000);
}

htmlEditor.backToDashboard = function () {
    //Initiate track for a concurrent user. Remove a user in case he leaves the page.
    var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");

    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.releaseLock,
        url: htmlEditor.PXEmappedAPI.releaseLock,
                data: {
                    'object_id': node_id,
                    'user_id': user_id
                },
        type: 'POST',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            if (data.status === 200) {
                var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=REMOVE", {withCredentials: true});
                var listener = function (event) {
                    window.location = clientUrl + '#/edit_toc/' + project_id;
                };
                es.addEventListener("message", listener);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Lock not released")
        }
    })

    //=====================================

};
// Function to remove unwanted elements & classes from a given frame.
htmlEditor.removeUnwanteds = function (frame) {
    var removeClasses = ["indicator", "highlightSelected", "highlight_on_drop", "highlight"]; // Add classes thats not required in the mode
    var removeElems = ["toolBox", "descOfTarget", "iframe-setting", "image-setting"]; // Add classes of the elements that needs to be removed.

    for (var i = 0; i < removeClasses.length; i++) {                // Removes the unwanted classes from the given frame.
        frame.find("." + removeClasses[i]).removeClass(removeClasses[i]);
    }

    for (var k = 0; k < removeElems.length; k++) {                  // Removes unwanted elements from the given frame.
        frame.find("." + removeElems[k]).remove();
    }
    htmlEditor.removeFrameSettings();
};

htmlEditor.changeEditorClass = function () {
    jQuery('.cke_button_icon').addClass('icons');
};

//htmlEditor.getSelectedNode = function()
//{
//    if (document.selection)
//        return document.selection.createRange().parentElement();
//    else
//    {
//        var selection = window.getSelection();
//        var isFigure = typeof window.getSelection().getRangeAt(0).startContainer.className !== "undefined" ? true : false;
//        if (selection.rangeCount > 0 && !isFigure) {
//            return selection.getRangeAt(0).startContainer.parentNode;
//        }
//        else {
//            return selection.getRangeAt(0).startContainer;
//        }
//    }
//}

htmlEditor.loadProjectDetailWIthPermissions = function () {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (projectDeatils) {
            htmlEditor.configureOnPermission(projectDeatils.data.project_permissions)
//            user_id = projectDeatils.data.user_id;
        },
        error: function () {
            //alert("Error occured");
        }
    });
}

htmlEditor.permissionTo = {};

htmlEditor.configureOnPermission = function (permObj) {
    htmlEditor.permissionTo.viewSourceCode = permObj["editor.sourcecode.view"].grant;
    htmlEditor.permissionTo.save = permObj["toc.toc-content.save"].grant;
    htmlEditor.permissionTo.smartHighlight = permObj["editor.smarthighlight"].grant;
    htmlEditor.permissionTo.dragPattern = permObj["editor.pattern.draggable"].grant;

    //    setTimeout(function() {
    //        if (!allowSourceCode) {
    //            jQuery('.cke_button__source').addClass('hideElm');
    //        }
    //        else {
    //            jQuery('.cke_button__source').removeClass('hideElm');
    //        }
    //
    //        if (!allowSave) {
    //            jQuery('#save_toc_id').addClass('hideElm');
    //        }
    //        else {
    //            jQuery('#save_toc_id').removeClass('hideElm');
    //        }
    //
    //        if (!allowSmartHighlight) {
    //            jQuery('#highlighter').addClass('hideElm');
    //        }
    //        else {
    //            jQuery('#highlighter').removeClass('hideElm');
    //        }
    //
    //    }, 1000)
};
var eCtrlPressed = false;

htmlEditor.bindEvents = function () {
    jQuery("body").scrollTop(0);
    //Pattern Related Events
    if (jQuery(".cke_wysiwyg_frame").contents().find("body").find(".chapter")/*.length*/) {
        htmlEditor.bindIframeDragDrop(); //Enable dragging of pattern widgets.
        //Create a new pattern on the fly.
        jQuery('.addPattern').off('click').on('click', function () {
            jQuery('#addPatternModal .sub-title').remove();
            jQuery('#addPatternModal .modal-header')
                    .html('Add Pattern<button type="button" class="close" data-dismiss="modal" ng-click="cancel()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>')
                    .after('<div class="sub-title">You are trying to add a new pattern</div>');
            jQuery('#addPatternModal').modal('show');
            var patternKey = jQuery('#patternKey');
            var patternCont = jQuery('#patternCont');
            var addDynamicPattern = function (arr, indx) {                   //Function to append the newly added pattern
                var append_after = (jQuery('.menu-block').length - 1);
                jQuery(".menu-block").eq(append_after).after("<div class='menu-block'><span class='glyphicon glyphicon-th' style='font-size:9px;color:#fff;'></span><div class='tab-name'>" + arr[indx].title + "</div></div>");
                htmlEditor.postNewPattern(arr, indx);
            };
            htmlEditor.postNewPattern = function (arr, indx) {
                var patternData = {"project_id": project_id, "title": arr[indx].title, "pattern": arr[indx].pattern};
                jQuery.ajax({
                    url: htmlEditor.PXEmappedAPI.saveProjectPatternAPI + '/project_id/' + project_id + '?access_token=' + access_token,
                    async: true,
                    method: 'POST',
                    data: patternData,
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function (data) {
                        if (data.status == '200') {
                            jQuery('#addPatternModal').modal('hide');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function () {
                        //alert("Error occured");
                    }
                });
            };
            jQuery('#patternConf').off('click').on('click', function (e) {
                if (patternKey.val().length !== 0 && patternCont.val().length !== 0) {
                    patternKey = (jQuery.trim(patternKey.val())).toUpperCase();
                    patternCont = jQuery.trim(patternCont.val());
                    editor_patterns.push({
                        'title': patternKey,
                        'pattern': patternCont
                    });
                    addDynamicPattern(editor_patterns, editor_patterns.length - 1);
                    htmlEditor.bindIframeDragDrop();
                    e.preventDefault();
                }
            });
        });
    }

    //open the tree whenever the table of contents needs to be opened.
    jQuery("#tocBtn").off("click").on("click", function () {
        if (!jQuery('button.expandToc').find(".fa-caret-down").length) {
            jQuery('button.expandToc').trigger("click");
        }
    })

    // Keep a track of user actions
    jQuery(document).on("click mouseover keyup keydown scroll", function (evt) {
        eCtrlPressed = evt.ctrlKey;
        htmlEditor.checkUserAct();
    });

    jQuery("iframe").contents().find("body").on("click keyup keydown scroll", function (evt) {
        if (evt.keyCode === 46 || evt.keyCode === 8) {
            if (jQuery("iframe").contents().find("body").html().length < 12) {
                jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                htmlEditor.bindIframeDragDrop();
            }
        }
        var upto = 'section';
        var currSelElm = jQuery(htmlEditor.getEditorSelection().getStartElement().$).parents(upto);
        var mapId = jQuery(currSelElm[0]).attr('data-pos');
        var elmText = jQuery(currSelElm[0]).text().substr(0, 15) + '...';
        var oParentElm = jQuery(".tree_view");
        var tocElm = oParentElm.find("*[map=" + mapId + "]");
        var currNode = tocElm.find('.desc').eq(0);
        currNode.text(elmText);
        htmlEditor.checkUserAct();
    });

    jQuery('#highlighter').off('click').on('click', function () {
        jQuery(this).toggleClass('active');
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
    });

    jQuery("#lockPage").off("click").on("click", function () {
        if (jQuery(this).find(".fa-lock").length) {
            jQuery.ajax({
                url: htmlEditor.PXEmappedAPI.releaseLock,
                data: {
                    'object_id': node_id,
                    'user_id': user_id
                },
                type: 'POST',
                async: true,
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function (data, textStatus, jqXHR) {
                    if (data.status === 200) {
                        jQuery("#lockPage").find(".fa-lock").removeClass("fa-lock").addClass("fa-unlock");
                        htmlEditor.islocked = false;
                        clearTimeout(htmlEditor.timeout);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            })
        }
        else {
            jQuery.ajax({
                url: htmlEditor.PXEmappedAPI.lockPage,
                data: {
                    'object_id': node_id,
                    'user_id': user_id
                },
                type: 'POST',
                async: true,
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function (data, textStatus, jqXHR) {
                    if (data.status === 200) {
                        jQuery("#lockPage").find(".fa-unlock").removeClass("fa-unlock").addClass("fa-lock");
                        htmlEditor.islocked = true;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            })
        }
    })
    // Toggle display of tickets & validation
    jQuery(".lm-edit-more").off().on("click", function () {
        //remove the unwanted highlights while opening the right panel.
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());

        jQuery(this).toggleClass("active").find("span.fa").toggleClass("fa-chevron-right"); //Change the arrow direction of the button
        jQuery(this).parents(".lm-edt-pane").toggleClass("active");
        if (!jQuery(this).hasClass("active")) {
            jQuery(".cke_wysiwyg_frame").contents().find(".indicator").removeClass("indicator");
            try {
                jQuery(".activatedTab").trigger("click");                       //Close the open accordion tab, if any.
                jQuery(".action-link.cancel").trigger("click");                 // Close all the textareas.
            }
            catch (exc) {

            }
        }
    });

    //          ====================Slider controller===============================

    jQuery("#prevArrow").off("click").on("click", function () {
        htmlEditor.initial -= htmlEditor.numOfWidgets;
        htmlEditor.initial = htmlEditor.initial < 0 ? 0 : htmlEditor.initial;
        var upto = htmlEditor.initial + htmlEditor.numOfWidgets;
        jQuery("#nextArrow").attr("disabled", false);
        jQuery("#nextArrow").css("opacity", "1");
        htmlEditor.createWidgets(htmlEditor.initial, upto);
    });
    jQuery("#nextArrow").off("click").on("click", function () {
        htmlEditor.initial += htmlEditor.numOfWidgets;
        htmlEditor.initial = htmlEditor.initial < 0 ? 0 : htmlEditor.initial;
        var upto = htmlEditor.initial + htmlEditor.numOfWidgets;
        jQuery("#prevArrow").attr("disabled", false);
        jQuery("#prevArrow").css("opacity", "1");
        htmlEditor.createWidgets(htmlEditor.initial, upto);
    });
    //          ====================================================================

    //Main page events to switch state between build/test
    htmlEditor.switch_btn_state = 'build';
    jQuery('.switch.right .switch-button-background').off('click').on('click', function (e) {
        if (htmlEditor.switch_btn_state == 'build') {
            htmlEditor.switch_btn_state = 'test';
        }
        else {
            htmlEditor.switch_btn_state = 'build';
        }
        htmlEditor.checkUserAct();
        htmlEditor.onSwitchStateChange();
    });
    jQuery('.switch.right .switch-button-label').off('click').on('click', function () {
        if (jQuery(this).hasClass('active'))
            return;
        jQuery('.switch-button-background').trigger('click');
    });
    //View Port manager.
    jQuery('.resize-view li').off('click').on('click', function () {
        htmlEditor.viewChange(jQuery(this).index());
        htmlEditor.checkUserAct();
    });
    //Editor Events

    jQuery(".cke_button__source").off().on("click", function () {
        try {
            jQuery('.cont_link').toggleClass('cke_button_disabled', '');
            if (!jQuery('.cont_link').hasClass('cke_button_disabled')) {
                jQuery(".lm-edit-more").attr("disabled", false); //Enable the toggler button to bring in right pane.
                htmlEditor.populateMeta();
                jQuery("#save_toc_id").attr("disabled", false); //Disable the save button when viewing source.
                setTimeout(function () {
                    htmlEditor.editorEvents();
                    htmlEditor.bindIframeDragDrop();
                }, 100);
            }
            else {
                if (jQuery('button.dropdown-toggle').attr('disabled') === undefined) {
                    jQuery('button.dropdown-toggle').attr({'disabled': 'disabled'});
                }
                jQuery('#tocBtn').removeAttr('disabled');
                if (jQuery(".lm-edit-more").hasClass("active")) {           // Expand the editor size and disable toggler button.
                    jQuery(".lm-edit-more").trigger("click");
                }
                jQuery("#save_toc_id").attr("disabled", true); //Disable the save button when viewing source.
                jQuery(".lm-edit-more").attr("disabled", true);
                var eventCheckInterval = setInterval(function () {
                    if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').length === 0) {
                    }
                    else {
                        jQuery("iframe.cke_wysiwyg_frame").contents().find('body').createTOC();
                        clearInterval(eventCheckInterval);
                    }
                }, 500);
            }
            htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
        }
        catch (exc) {

        }
    });
    htmlEditor.editorEvents();

    //Compare versions

    jQuery("#compare_btn").off().on("click", function () {
        jQuery(this).button('loading');

        jQuery("#changeView").attr("disabled", true);
        var verLatest = jQuery("#compareDropdown1").val();
        var verPrev = jQuery("#compareDropdown2").val();
        var viewType = jQuery("#changeView").val();
        var type = viewType;       // Check the type of result desired. 1= table form.
        jQuery("#updateNsave").attr("disabled", true);      //Disable the save button while comparison is being made.

        /*if (viewType) {
         type = 2;
         }
         else {
         type = 1;
         }*/
//        var type = 1;     // Check the type of result desired. 1= table form.
        htmlEditor.getComparativeResult(verPrev, verLatest, type);
    })
    tocInit();
};
// Functionality to toggle between switch & Build mode.
htmlEditor.onSwitchStateChange = function () {
    if (htmlEditor.switch_btn_state == 'build') {
        jQuery('.switch.right .switch-button-label').removeClass('active');
        jQuery('.switch-button-label.build').addClass('active');
        htmlEditor.switchBuildTest(false);
        jQuery('.panel-refresh-reload').removeClass('hideElm');
    }
    else {
        if (jQuery('.cke_button__source').hasClass('cke_button_on')) {
            jQuery(".cke_button_on").trigger("click");
        }
        //        jQuery("#displayOptions").multiselect();
        jQuery('.switch.right .switch-button-label').removeClass('active');
        jQuery('.switch-button-label.test').addClass('active');
        htmlEditor.switchBuildTest(true);
        jQuery('.panel-refresh-reload').addClass('hideElm');
    }
    htmlEditor.onWindowResize();
};
htmlEditor.switchBuildTest = function (flag) {
    if (flag) {
        //        jQuery('.iframe-setting').addClass('hideElm');
        htmlEditor.removeFrameSettings();
        jQuery('.left-pane').addClass('hideElm');
        jQuery('.middle-pane').addClass('hideElm');
        jQuery('.right-pane').addClass('hideElm');
        jQuery('.resizer-container').removeClass('hideElm');
        jQuery('.test-pane, .view-wrapper').removeClass('hideElm');
        jQuery('.test-pane *').on('click', function (e) {
            jQuery("#message").html("Change to Build Mode to see the changes").show().fadeOut(2000);
            return false;
        });
        jQuery("#highlighter").hide();
        //        htmlEditor.viewChange(0);
        var data = CKEDITOR.instances.editor1.getData();
        var elm = jQuery('.test-pane .view-wrapper');
        htmlEditor.includeCSS(elm);                     //Include css relevant to the project in the test mode.
        elm.contents().find('.testContainer').html(data);
        htmlEditor.convertDivToFrame(elm.contents());
        htmlEditor.removeUnwanteds(elm.contents());
        jQuery('iframe.view-wrapper').contents().find('body *').on('click', function (e) {
            var tagName = jQuery(this)[0].tagName;
            if (tagName === 'A')
            {
                swal("Links do not function in Build or Test mode.")
                return false;
            }
        });
        setTimeout(function () {
            jQuery('.resize-view li').eq(0).trigger("click");
        }, 100);
    }
    else {
        var elm = jQuery('.test-pane .view-wrapper').contents().find('body');
        elm.empty();
        jQuery('.glyphicon').removeClass('hideElm');
        jQuery('.left-pane').removeClass('hideElm');
        jQuery('.middle-pane').removeClass('hideElm');
        jQuery('.right-pane').removeClass('hideElm');
        jQuery('.resizer-container').addClass('hideElm');
        jQuery('.test-pane').addClass('hideElm');
        jQuery("#highlighter").show();
//        htmlEditor.convertFrameToDiv();
        htmlEditor.populateMeta();
    }
};
htmlEditor.viewChange = function (val) {
    jQuery('.resize-view li').removeClass('active');
    jQuery('.resize-view li').eq(val).addClass('active');
    switch (val) {
        case 0:
            var height = jQuery(".test-pane").height() - 6;
            var width = '100%';
            htmlEditor.setViewStyle(height, width);
            break;
        case 1:
            var height = 768;
            var width = 1024;
            htmlEditor.setViewStyle(height, width);
            break;
        case 2:
            var height = 800;
            var width = 600;
            htmlEditor.setViewStyle(height, width);
            break;
        case 3:
            var height = 480;
            var width = 320;
            htmlEditor.setViewStyle(height, width);
            break;
        default:
    }
};
htmlEditor.setViewStyle = function (height, width) {
    var elm = jQuery('.test-pane .view-wrapper');
    elm[0].height = height;
    elm[0].width = width;
    elm.removeClass('hideElm');
    var win = document.getElementById("testCont").contentWindow;
    win.postMessage(true, "*");
};
//============  Test/Build change functionality ends=============================

//Add link button and metadata dropdowns to the editor.
htmlEditor.addToolbarBtn = function () {
    jQuery('.cont_link').remove();
    jQuery('.cke_button__insertimage').after("<a class='cont_link cke_button cke_button__link' title='Add Link'><span class='cke_button_icon cke_button__link_icon icons '>&nbsp;</span></a>");
    jQuery('.cont_link').off('click').on('click', function () {
        var selObj = htmlEditor.getEditorSelection();
        var selText = selObj.getSelectedText().trim();
        if (selText) {
            jQuery(this).attr({'data-toggle': 'modal', 'data-target': '#link_modal'});
        }
        else {
            jQuery(this).attr({'data-toggle': '', 'data-target': ''});
        }
        htmlEditor.addGlossary(selObj);
    });
    htmlEditor.patternMetadataInit();
};
htmlEditor.patternMetadataInit = function () {
    jQuery('.meta-dropdown').remove();
    jQuery('.cke_button__source').parent().parent().after('<div class="dropdown meta-dropdown"> <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"> Styles <span class="caret"></span> </button> <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1"></ul></div>');
    jQuery('#dropdownMenu1').attr('disabled', true);
};