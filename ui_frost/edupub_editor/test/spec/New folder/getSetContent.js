htmlEditor.needsToBeMerged = false; //Checks if theres a change in the latest version
htmlEditor.setLockStatus = function(lockedbyuser, user_id) {
    if (lockedbyuser !== undefined) {
        if (user_id === lockedbyuser || user_id === 1) { // 1 = admin
            if (htmlEditor.islocked) {
                jQuery("#lockPage").find(".fa-unlock").removeClass("fa-unlock").addClass("fa-lock");
                htmlEditor.islocked = true;
            }
        } else {
            window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
    }
};

htmlEditor.getJsonValueOnDone = function(editorContent) {
    var contObj = editorContent;
    var content = contObj.data.description;
    htmlEditor.islocked = contObj.data.is_locked;
    user_id = contObj.data.user_id;
    htmlEditor.editorTOC = contObj.data.editor_toc;
    var lockedbyuser = contObj.data.locked_by_user;
    htmlEditor.setLockStatus(lockedbyuser, user_id);

    //        var current_node = contObj.data.object_location;        //Holds the section Id current node maps to.
    //        var section_id = current_node.split("#")[1];
    htmlEditor.getJsonValueSetCurrentNode(section_id);
    htmlEditor.cssFiles = contObj.data.theme_location;
    htmlEditor.glossaryFile = contObj.data.glossary_file_name;
    try {
        var validation_response = JSON.parse(contObj.data.validation_response);
        htmlEditor.showValidationContent(validation_response);
    } catch (exc) {
        jQuery(".error-count").hide();
    }
    htmlEditor.initiateEditor();

    CKEDITOR.on('instanceReady', function(ev) {
        htmlEditor.setData(content);
        jQuery(".cke_wysiwyg_frame").attr("id", "editor-frame");
        htmlEditor.changeEditorClass();
        htmlEditor.addToolbarBtn();
        if (!htmlEditor.permissionTo.viewSourceCode) {
            jQuery('.cke_button__source').addClass('hideElm');
        }

        // Enable drag drop and other functionalities after redo/undo.
        CKEDITOR.instances.editor1.on('afterCommandExec', handleAfterCommandExec);

        function handleAfterCommandExec(event) {
            var commandName = event.data.name;
            // For 'redo/ undo' commmand
            if (commandName === 'redo' || commandName === 'undo')
                htmlEditor.bindEvents();

            if (commandName === 'source') {
                //                    htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
                var oFrameElm = jQuery(".cke_wysiwyg_frame"),
                    isPreviewMode = oFrameElm.length;
                if (isPreviewMode) {
                    setTimeout(function() {
                        htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
                        htmlEditor.bindIframeDragDrop();
                    }, 1000)
                }
                //alert(jQuery(".cke_wysiwyg_frame").contents().find("body").find('*').length)
                /*jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                 htmlEditor.bindIframeDragDrop();console.log()
                 htmlEditor.disableToolbar();*/
            }
        }

        CKEDITOR.instances.editor1.on('beforeCommandExec', handleBeforeCommandExec);

        function handleBeforeCommandExec(event) {
            var cmdName = event.data.name;
            // For 'source' commmand
            if (cmdName === 'source')
                htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents().find("body"));
        }
        htmlEditor.aOnclickValueArr = [];
        jQuery('a.cke_button').each(function() {
            htmlEditor.aOnclickValueArr.push(jQuery(this).attr("onclick"));
        });
    });
    var projectName = contObj.data.object_name;
    var chapName = contObj.data.node_title;
    jQuery("#node_title").html(chapName);
    jQuery("#project_name").html(projectName);
}

htmlEditor.getJsonValueSetCurrentNode = function(section_id) {
    htmlEditor.current_node = section_id;
};
htmlEditor.getJsonValue = function() { // Fetch the content to be displayed.
    var editorContent;

    jQuery.when(editorContent = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.getContentAPI)).done(function() {
        htmlEditor.getJsonValueOnDone(editorContent.responseJSON);
    });
};
htmlEditor.imageTimeStamp = function(el, req) {
    var refEl = el,
        images = refEl.find('img'),
        addTimeStamp = req;

    jQuery(images).each(function() {
        if (addTimeStamp) {
            var timeStamp = new Date().getTime(),
                thisElm = jQuery(this),
                srcWithTimeStamp = thisElm.attr('src') + '?' + timeStamp;
            thisElm.attr('src', srcWithTimeStamp);
        } else {
            var thisElm = jQuery(this),
                upto = '?',
                srcWithoutTimeStamp = thisElm.attr('src').split(upto)[0];
            thisElm.attr('src', srcWithoutTimeStamp);
        }
    });
    htmlEditor.videoTimeStamp(el, req)
}

htmlEditor.videoTimeStamp = function(el, req) {
    var refEl = el,
        videos = refEl.find('video').find('source'),
        addTimeStamp = req;

    jQuery(videos).each(function() {
        if (addTimeStamp) {
            var timeStamp = new Date().getTime(),
                thisElm = jQuery(this),
                srcWithTimeStamp = thisElm.attr('src') + '?' + timeStamp;
            thisElm.attr('src', srcWithTimeStamp);
        } else {
            var thisElm = jQuery(this),
                upto = '?',
                srcWithoutTimeStamp = thisElm.attr('src').split(upto)[0];
            thisElm.attr('src', srcWithoutTimeStamp);
        }
    });
}

htmlEditor.setData = function(content) { // Set the fetched content in the editor.
    htmlEditor.disableSave();
    var cloneFrame = jQuery("#testCont").contents().find('body');
    cloneFrame.html(content);
    htmlEditor.imageTimeStamp(cloneFrame, true);
    content = cloneFrame.html();
    try {
        CKEDITOR.instances.editor1.setData(content);
    } catch (exc) {
        console.log("Editor not defined");
    }

    var frame = jQuery("iframe.cke_wysiwyg_frame");
    htmlEditor.includeCSS(frame);
    htmlEditor.onWindowResize();
    jQuery(".loading-g").hide();
    var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
    var last_users = "";
    //Initiate track for a concurrent user.
    var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=INSERT", {
        withCredentials: true
    });
    var listener = function(event) {
        //console.log(event.data);
        var user_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id"); //Get current user version.
        var resp = JSON.parse(event.data);
        var userlist = resp.users;
        var latest_version = resp.latest_version_id;
        var users = userlist.join();
        if (userlist.length) {
            htmlEditor.allowLocking = false;
            jQuery(".concurrent-info").removeClass("hideElm").find("span.con-user").text(users);
            //Check if a merge option needs to be provided to the user.
            if (user_ver !== latest_version && !htmlEditor.manualSelection) {
                htmlEditor.needsToBeMerged = true;
            } else {
                htmlEditor.needsToBeMerged = false;
            }

            if (htmlEditor.manualSelection && user_ver !== latest_version) {
                htmlEditor.createVersion = false;
            } else {
                htmlEditor.createVersion = true;
            }
            //            }
        } else {
            htmlEditor.allowLocking = true;
            jQuery(".concurrent-info").addClass("hideElm");
            if (user_ver !== latest_version && htmlEditor.manualSelection) {
                htmlEditor.createVersion = false;
            }
        }
        if (htmlEditor.allowLocking) {
            jQuery("#lockPage").attr("disabled", false);
        } else {
            jQuery("#lockPage").attr("disabled", true);
        }
    };
    es.addEventListener("message", listener);
    //=====================================

    jQuery.when((function() {
        setTimeout(function() {
            htmlEditor.currContent = htmlEditor.getLatestSavedContent();
            try {
                var posOfNode = frame.contents().find("#" + htmlEditor.current_node).offset().top; //Get offset of the node section in the current content.
                frame.contents().scrollTop(posOfNode); //Focus the content according to the section id.
            } catch (exc) {
                frame.contents().scrollTop(0);
            }
        }, 1000);

        frame.contents().find('body').createTOC();
    })()).done(function() {
        try {
            htmlEditor.convertFrameToDiv();

        } catch (exc) {}
        htmlEditor.bindEvents();

        //If widget overlay is not created due to slow API response
        var framesUnconvertedLength = frame.contents().find('body').find('iframe').length;
        var frameConvertChecker = function() {
            if (framesUnconvertedLength) {
                htmlEditor.convertFrameToDiv();
                htmlEditor.bindEvents();
                framesUnconvertedLength = frame.contents().find('body').find('iframe').length;
                frameConvertChecker();
            } else
                return false;
        };
        frameConvertChecker();
        /**/
    });
    //Store the content to map any changes in the content.

}

htmlEditor.getLatestSavedContent = function() {
    try {
        var content = CKEDITOR.instances.editor1.getData();
        jQuery("#trial_load").contents().find("body").html(content);
        htmlEditor.removeUnwanteds(jQuery("#trial_load").contents().find("body"));
        jQuery("#trial_load").contents().find("body").find("*").removeAttr("ticket-id").removeAttr("data-pos");
        var finalContent = jQuery("#trial_load").contents().find("body").html();
        return finalContent;
    }catch (exc){
        console.log("Editor not defined");
    };

}

htmlEditor.includeCSS = function(frame) { // Include redseriff css in editor.
    var editorFrame = frame; //jQuery("iframe.cke_wysiwyg_frame");
    var cssFiles = htmlEditor.cssFiles;
    if (editorFrame.hasClass("cke_wysiwyg_frame")) { //Check if the css is included in editor.
        var linkHTML = '<link rel="stylesheet" type="text/css" href="css/iframe.css"/><link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">';
        editorFrame.contents().find('style').remove();
    } else {
        var linkHTML = '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">'; //Default it to include font awesome
    }
    editorFrame.contents().find('head').find('link').remove();
    if (cssFiles != null) {
        for (var i = 0; i < cssFiles.length; i++) {
            linkHTML += '<link rel="stylesheet" type="text/css" href="' + cssFiles[i] + '"/>';
        }
    }
    editorFrame.contents().find('head').append(linkHTML);
};

htmlEditor.loadPatterns = function() { //Fetch the patterns for this project
    var patternQueryString = '&project_id=' + project_id + '&pattern_status=1001';
    var url = htmlEditor.PXEmappedAPI.getPatternsAPI + patternQueryString;
    var patternObj;
    jQuery.when(patternObj = htmlEditor.ajaxCalls(url)).done(function() {
        var patterns = patternObj.responseJSON.data;
        htmlEditor.loadWidgets(patterns);
    });
}

htmlEditor.loadWidgets = function(object) {
    var widgetUrl = htmlEditor.PXEmappedAPI.getWidgetAPI,
        patternArr = [],
        widgetObj,
        statusCheck = '1001';

    jQuery.ajax({
        url: widgetUrl,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(response) {
            try {
                widgetObj = response.data;
                var widgetList = widgetObj.list;

                widgetScope = widgetObj.widget_config;

                if (widgetList.length) {
                    for (var idx = 0; idx < widgetList.length; idx++) {
                        //if (widgetList[idx].status == statusCheck) {
                        patternArr.push({
                            pattern: widgetList[idx].code_snippet,
                            default_class: 'exwidget',
                            default_tag: 'iframe',
                            meta_class_options: '[{"pattern_classes":[{"value":"exwidget"}],"default_class":"exwidget"}]',
                            pattern_classes: '[{"value":"exwidget"}]',
                            pattern_type: 0,
                            project_id: "1",
                            project_type_id: 4,
                            status: widgetList[idx].status,
                            tenant_id: 1,
                            title: widgetList[idx].name,
                            toc_type: 0
                        });
                        //}

                    }
                    editor_patterns = jQuery.merge(object, patternArr);
                    htmlEditor.setWidgetsPatterns(editor_patterns);
                }
            } catch (exc) {
                editor_patterns = object;
                htmlEditor.setWidgetsPatterns(editor_patterns);
            }

        },
        error: function() {
            //alert("Error occured..Retry");
            editor_patterns = object;
            htmlEditor.setWidgetsPatterns(editor_patterns);
        }
    });
};

htmlEditor.setWidgetsPatterns = function(object) {
    var patterns = object;
    jQuery(".right-pane").empty();
    jQuery(".right-pane").html('<div class="panel-refresh-reload" >Patterns</div>');
    htmlEditor.totalWidgets = patterns.length;
    var statusCheck = '1001';
    for (var i = 0; i < htmlEditor.totalWidgets; i++) {
        if (patterns[i].status == statusCheck) {
            jQuery(".right-pane").append("<div class='menu-block' default-class=" + patterns[i].default_class + " tag-type=" + patterns[i].default_tag + "><span class='glyphicon glyphicon-th' style='font-size:9px;color:#fff;'></span><div class='tab-name'>" + patterns[i].title + "</div></div>");
        } else {
            jQuery(".right-pane").append("<div class='menu-block hideElm' default-class=" + patterns[i].default_class + " tag-type=" + patterns[i].default_tag + "><span class='glyphicon glyphicon-th' style='font-size:9px;color:#fff;'></span><div class='tab-name'>" + patterns[i].title + "</div></div>");
        }
    }
    editor_patterns = patterns;

    jQuery(".right-pane").append("<div class='sliderArrows'><button id='prevArrow' class='sprite'>Prev</button> <button class='glyphicon glyphicon-plus addPattern'></button> <button id='nextArrow' class='sprite'>Next</button></div>");
    jQuery(".menu-block").hide();
    htmlEditor.initial = 0;
    jQuery('.cke_top .cke_toolbox').after("<div class='clearfix'></div>");
    htmlEditor.calculateWidgetsOnResize();
    htmlEditor.bindIframeDragDrop();
    htmlEditor.bindEvents();
}

htmlEditor.calculateWidgetsOnResize = function() { //Calculate number of patterns to be displayed.
    var rightHeight = jQuery(".right-pane").height() - (jQuery('.sliderArrows').height() + jQuery('.panel-refresh-reload').height());
    var widgetHeight = jQuery(".menu-block").eq(0).outerHeight();
    jQuery(".menu-block").each(function() { //Get the maximum height among all patterns.
        var this_height = jQuery(this).outerHeight();
        if (this_height > widgetHeight) {
            widgetHeight = this_height;
        }
    });
    htmlEditor.numOfWidgets = Math.floor(rightHeight / widgetHeight) - 2;
    htmlEditor.numOfWidgets = htmlEditor.numOfWidgets > htmlEditor.totalWidgets ? htmlEditor.totalWidgets : htmlEditor.numOfWidgets;
    //    htmlEditor.numOfWidgets --;                                     // To adjust to the current height of the pane.
    var displayUpto = htmlEditor.numOfWidgets + htmlEditor.initial;
    htmlEditor.createWidgets(htmlEditor.initial, displayUpto);
};

htmlEditor.createWidgets = function(startFrom, UptoInd) { //Display the widgets/ Patterns
    jQuery(".menu-block").hide();
    for (var i = startFrom; i < UptoInd; i++) {
        jQuery(".menu-block").eq(i).show();
    }
    jQuery("#nextArrow").attr("disabled", false).css("opacity", "1");
    jQuery("#prevArrow").attr("disabled", false).css("opacity", "1");
    if (htmlEditor.totalWidgets <= UptoInd) {
        jQuery("#nextArrow").attr("disabled", true);
        jQuery("#nextArrow").css("opacity", "0.5");
    }
    if (htmlEditor.initial == 0) {
        jQuery("#prevArrow").attr("disabled", true);
        jQuery("#prevArrow").css("opacity", "0.5");
    }
};

//Fetch the videos for insertion
htmlEditor.getMediaVideo = function(page, search_flag, url) {
    jQuery("#videoModal .loc-or-global").attr("disabled", true);
    var num_of_items = 10;
    if (search_flag) {
        var searchText = jQuery('#videoModal').find('.search_text').val();
        var params = {
            "pageNumber": page,
            "itemsPerPage": num_of_items,
            "object_types": "103",
            "project_id": project_id,
            "search_text": searchText,
            "taxonomy_id": 0
        };
    } else {
        var params = {
            "pageNumber": page,
            "itemsPerPage": num_of_items
        };
    }
    jQuery("#video_thumb .inputfrom").empty();
    jQuery(".video_conf").attr("disabled", true);
    jQuery.ajax({
        url: url,
        async: true,
        type: "GET",
        data: params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                jQuery(".paginate").show();
                var str = "";
                for (var i = 0; i < d.length; i++) {
                    str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg" src="../img/blank.jpg" alt="" videoPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title">' + d[i].original_name + '</div></div></div>';
                }
                jQuery("#video_thumb .inputfrom").html(str);
                //                jQuery(".modal-content-container").append('<div class="clearfix"></div>');
                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_images = data.totalAssets;
                    var max_page = Math.ceil(total_images / num_of_items);
                    //Pagination using plugin.
                    jQuery('#videoModal').find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").off("page").on("page", function(event, num) {
                        htmlEditor.getMediaVideo(num, search_flag, url);
                    });
                }
                jQuery('.box-theme').off("click").on("click", function() {
                    var videoUrl;
                    if (jQuery(this).hasClass('active')) {
                        jQuery(this).removeClass('active');
                        jQuery(this).find('.check-box').removeClass('checked');
                        videoUrl = "";
                    } else {
                        jQuery('.box-theme').removeClass('active');
                        jQuery(this).addClass('active');
                        jQuery(this).find('.check-box').addClass('checked');
                        videoUrl = jQuery('#videoModal .box-theme.active').find("img").attr("videoPath");
                    }
                    htmlEditor.newVidObj.url = videoUrl;
                    htmlEditor.newVidObj.id.length = 0;
                    htmlEditor.newVidObj.id.push(jQuery('#videoModal .box-theme.active').find("img").attr("id"));
                    //                    var videoUrl = jQuery('.active').find("img").attr("videoPath");
                    if (videoUrl !== undefined && videoUrl.length) {
                        jQuery(".video_conf").attr("disabled", false);
                    } else {
                        jQuery(".video_conf").attr("disabled", true);
                    }

                });
                jQuery('.video_get_media').attr("disabled", true);
                //                jQuery(".paginate").show();
            } else {
                if (page == 1)
                    jQuery("#video_thumb .inputfrom").html("<p  class='col-md-12'>No assets Found.</p>");
                jQuery(".paginate").hide();
            }
            jQuery("#videoModal .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({
                title: "Error!",
                text: "Some error occured while fetching the assets."
            });
        }
    });
};


//Fetch the list of gadgets.
htmlEditor.getGadgetList = function(page, search_flag, url) {
    jQuery("#iframeModal .loc-or-global").attr("disabled", true);
    var num_of_items = 10;
    if (search_flag) {
        var searchText = jQuery('#iframeModal').find('.search_text').val();
        var params = {
            "pageNumber": page,
            "itemsPerPage": num_of_items,
            "object_types": "103",
            "project_id": project_id,
            "search_text": searchText,
            "taxonomy_id": 0
        };
    } else {
        var params = {
            "pageNumber": page,
            "itemsPerPage": num_of_items
        };
    }

    jQuery("#iframeModal .listed-gadgets").empty();
    //    jQuery(".video_conf").attr("disabled", true);
    jQuery.ajax({
        url: url,
        async: true,
        type: "GET",
        data: params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                jQuery(".paginate").show();
                var str = "";
                for (var i = 0; i < d.length; i++) {
                    str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg" src="../img/blank.jpg" alt="" gadgetId = "' + d[i].id + '"/></div><div class="box-theme-title">' + d[i].original_name + '</div></div></div>';
                }
                jQuery("#iframeModal .listed-gadgets").html(str);

                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_images = data.totalAssets;
                    var max_page = Math.ceil(total_images / num_of_items);
                    //Pagination using plugin.
                    jQuery('#iframeModal').find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").on("page", function(event, num) {
                        htmlEditor.getGadgetList(num, search_flag, url);
                    });
                }

                jQuery('.box-theme').off("click").on("click", function() {
                    if (jQuery(this).hasClass('active')) {
                        jQuery(this).removeClass('active');
                        jQuery(this).find('.check-box').removeClass('checked');
                        htmlEditor.newGadgetObj.gadgetId.length = 0;
                    } else {
                        jQuery('.box-theme').removeClass('active');
                        jQuery(this).addClass('active');
                        jQuery(this).find('.check-box').addClass('checked');
                        jQuery('#iframeTerm').val("");
                        htmlEditor.newGadgetObj.gadgetId.push(jQuery('.listed-gadgets').find('.active').find("img").attr("gadgetid"));
                    }

                    if (htmlEditor.newGadgetObj.gadgetId.length || jQuery('#iframeTerm').val().length) {
                        jQuery(".iframe_conf").attr("disabled", false);
                    } else {
                        jQuery(".iframe_conf").attr("disabled", true);
                    }

                });
                //                jQuery('.video_get_media').attr("disabled", true);
            } else {
                if (page == 1)
                    jQuery("#iframeModal .listed-gadgets").html("<p  class='col-md-12'>No assets Found.</p>");
                jQuery(".paginate").hide();
            }
            jQuery("#iframeModal .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({
                title: "Error!",
                text: "Some error occured while fetching the assets."
            });
        }
    });
}

//assign guids to every element in the editor

htmlEditor.generateIds = function(elem) {
    var numOfelem = elem.length;
    for (var i = 0; i < numOfelem; i++) {
        var checkForId = elem[i].hasAttribute("id") && elem[i].id.length; //if id against this element exists
        var checkNATag = elem[i].tagName === "A" ? true : false; //If anchor tag
        var checkGadget = jQuery(elem[i]).parents(".converted").length ? true : false; //If placeholder
        if (!(checkForId || checkNATag || checkGadget)) {
            elem[i].id = guid()();
        }
    }
}