describe("Test cases to get report on new_script.js", function () {
    var url;
    beforeEach(function () {
        url = htmlEditor.PXEmappedAPI.getPatternsAPI;
    });

    afterEach(function () {
        url = null;
    });

//    it("calls the ajaxCalls() for patterns", function () {
//        htmlEditor.ajaxCalls(url);
//    });

    it("calls the guid() function", function () {
        var returnValue = guid();
        expect(typeof returnValue).toBe("function");
        var anotherReturn = returnValue();
        var anotherReturn2 = returnValue();
        var anotherReturn3 = returnValue();
        expect(anotherReturn !== anotherReturn2).toBe(true);
        expect(anotherReturn !== anotherReturn3).toBe(true);
    });

    it("calls the setPageTitle() function", function () {
        var jqreturn = {
            find: jasmine.createSpy("find")
        };
        var findRtrn = {
            length: true,
            text: jasmine.createSpy("text")
        };
        node_title = "some val";
        jqreturn.find.and.returnValue(findRtrn);
        spyOn(window, "jQuery").and.returnValue(jqreturn);
        htmlEditor.setPageTitle();
        expect(findRtrn.text).toHaveBeenCalledWith(node_title);
    });
    it("calls the onDocumentReady() function", function () {
        var jqreturn = {
            find: jasmine.createSpy("find"),
            append: jasmine.createSpy("append")
        };
        node_title = "some val";
        jqreturn.find.and.returnValue({
            length: false,
            text: jasmine.createSpy("text")
        });
        spyOn(window, "jQuery").and.returnValue(jqreturn);
        htmlEditor.setPageTitle();
        expect(jqreturn.append).toHaveBeenCalledWith("<title>" + node_title + "</title>");
    });

    it("should receive ajax calls response", function () {
        spyOn(jQuery, 'ajax').and.callFake(function (params) {
            params.success({}, {}, {});
            params.error({}, {}, {});
            params.beforeSend({});
        });
        htmlEditor.ajaxCalls(url);
        expect(jQuery.ajax.calls.mostRecent().args[0]["url"]).toEqual(url);
    });

    describe("It tests setting page title of a node", function () {
        beforeEach(function () {
            jqreturn = {
                find: jasmine.createSpy("find"),
                append: jasmine.createSpy("append")
            };
            node_title = "some val";
        });

        afterEach(function () {
            jqreturn = {};
        });
        it("calls the setPageTitle() function and test if condition", function () {
            var findRtrn = {
                length: true,
                text: jasmine.createSpy("text")
            };

            jqreturn.find.and.returnValue(findRtrn);
            spyOn(window, "jQuery").and.returnValue(jqreturn);
            htmlEditor.setPageTitle();
            expect(findRtrn.text).toHaveBeenCalledWith(node_title);
        });
        it("calls the setPageTitle() function and test else condition", function () {
            jqreturn.find.and.returnValue({
                length: false,
                text: jasmine.createSpy("text")
            });
            spyOn(window, "jQuery").and.returnValue(jqreturn);
            htmlEditor.setPageTitle();
            expect(jqreturn.append).toHaveBeenCalledWith("<title>" + node_title + "</title>");
        });
    });

    describe('Test on changeLockStatus() function', function () {
        it("test changeLockStatus() function is called", function () {
            spyOn(jQuery, 'ajax').and.callFake(function (e) {
                //var data = jasmine.createSpy({});
                e.success({});
                e.error({});
            });

            htmlEditor.changeLockStatus();
            expect(jQuery.ajax).toHaveBeenCalled();
            expect(jQuery.ajax.calls.mostRecent().args[0].url).toEqual(htmlEditor.PXEmappedAPI.updateLockTimeAPI);
            //jQuery.ajax.calls.mostRecentCall.args[0].success(data);
            expect(jQuery.ajax.calls.mostRecent().args[0].success).toBeDefined();
            expect(jQuery.ajax.calls.mostRecent().args[0].error).toBeDefined();

        });
    });
    describe('Test on addToolbarBtn() function', function () {
        beforeEach(function () {
            spyOn(htmlEditor, 'addToolbarBtn').and.callThrough();
        });

        afterEach(function () {
            //htmlEditor = {};
        })
        it("test addToolbarBtn() is defined", function () {
            expect(htmlEditor.addToolbarBtn()).not.toBeDefined();
        });
    });

    describe('Test on patternMetadataInit() function', function () {
        beforeEach(function () {
            spyOn(htmlEditor, 'patternMetadataInit').and.callThrough();
        });
        it("test patternMetadataInit() function is defined", function () {
            //htmlEditor.patternMetadataInit();
            expect(htmlEditor.patternMetadataInit()).not.toBeDefined();
        });
    });
    
});