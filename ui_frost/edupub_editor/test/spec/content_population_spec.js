/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

describe("Ajax Tests For Polpulating Content", function () {
    var configuration = {url: htmlEditor.PXEmappedAPI.getContentAPI};
    it("should make an Ajax request to the correct URL", function () {
        spyOn(jQuery, "ajax");
        sendRequest(undefined, configuration);
        expect(jQuery.ajax.calls.mostRecent().args[0]["url"]).toEqual(configuration.url);
    });

    it("should receive a successful response", function () {
        spyOn(jQuery, "ajax").and.callFake(function (e) {
            e.success({});
        });

        var callbacks = {
            checkForInformation: jasmine.createSpy(),
            displayErrorMessage: jasmine.createSpy(),
        };

        sendRequest(callbacks, configuration);
        expect(callbacks.checkForInformation).toHaveBeenCalled();  //Verifies this was called
        expect(callbacks.displayErrorMessage).not.toHaveBeenCalled();  //Verifies this was NOT called
    });

    it("should receive an Ajax error", function () {
        spyOn(jQuery, "ajax").and.callFake(function (e) {
            e.error({});
        });

        var callbacks = {
            displayErrorMessage: jasmine.createSpy()
        };

        sendRequest(callbacks, configuration);
        expect(callbacks.displayErrorMessage).toHaveBeenCalled();

    });
    describe("Checking method getJsonValue", function () {
        it("Spying on method getJsonValue", function () {
            spyOn(htmlEditor, "getJsonValue");
            htmlEditor.getJsonValue();
            expect(htmlEditor.getJsonValue).toHaveBeenCalled();  //Verifies this was called
        });
        
        it("Checking method htmlEditor.stripScripts", function(){
            var checkingStr = "<div>Checking<script></script></div>",
            expectedStrOutput = "<div>Checking</div>",
            expectedScriptOutput = "<script></script>";
            expect(htmlEditor.stripScripts(checkingStr).content).toBe(expectedStrOutput);
            expect(htmlEditor.stripScripts(checkingStr).script).toBe(expectedScriptOutput);
        });

        it("Checking whether locked by user", function () {
            if (htmlEditor.islocked == 0) {
                expect(htmlEditor.islocked).toBe(false);
            } else {
                expect(htmlEditor.islocked).toBe(true);
            }

        });

        it("Checking current node to be section_id", function () {
            expect('' + htmlEditor.current_node + '').toBe(section_id);
        });
        
        it("Calling Content validation", function(){
            spyOn(htmlEditor, 'showValidationContent');
            htmlEditor.showValidationContent();
            expect(htmlEditor.showValidationContent).toHaveBeenCalled();
        });
        
        it("Calling method htmlEditor.setData", function(){
            spyOn(htmlEditor, 'setData');
            htmlEditor.setData();
            expect(htmlEditor.setData).toHaveBeenCalled();
        });
        
        it("Calling method htmlEditor.changeEditorClass", function(){
            spyOn(htmlEditor, 'changeEditorClass');
            htmlEditor.changeEditorClass();
            expect(htmlEditor.changeEditorClass).toHaveBeenCalled();
        });
        
        it("Calling method htmlEditor.convertFrameToDiv", function(){
            spyOn(htmlEditor,'convertFrameToDiv');
            htmlEditor.convertFrameToDiv();
            expect(htmlEditor.convertFrameToDiv).toHaveBeenCalled();
        });
        
        it("Calling method htmlEditor.bindEvents", function(){
            spyOn(htmlEditor,'bindEvents');
            htmlEditor.bindEvents();
            expect(htmlEditor.bindEvents).not.toHaveBeenCalled();
        });
        
        it("Checking method htmlEditor.imageTimeStamp", function(){
            var iSrc = ''
            jQuery('body').append('<img src='+iSrc+'</img>')
        });
    });
});




function sendRequest(callbacks, configuration) {
    jQuery.ajax({
        url: configuration.url,
        dataType: "json",
        success: function (data) {
            callbacks.checkForInformation(data);
        },
        error: function (data) {
            callbacks.displayErrorMessage();
        },
        timeout: configuration.remainingCallTime
    });
}