'use strict';
// dataset controller
app.controller('datasetcontroller', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'datasetService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, datasetService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.project_id = $routeParams.project_id;
        $scope.project_type_id = 4;
        $scope.set_type = $routeParams.set_type=='dataset'?'Resource':$routeParams.set_type;
        $scope.func_set_type=$routeParams.set_type;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.project_name = '';
        $scope.getProject = function () {
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project = data;
                        $scope.applyPermission = 1;
                        $scope.project.project_permissions = data.project_permissions;
                    }
                });
            }
        };

        $scope.getProject();

        $scope.pageNumber = 1;
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;

        $scope.listDatasets = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            datasetService.getDatasets($scope).then(function (data) {
                $scope.datasetList = data.data.dataset_list;
                $scope.datasetTotal = data.data.total;
            });
        };
        $scope.listDatasets(1);

        $scope.pageChangeHandlerDataset = function (newPage) {
            $scope.listDatasets(newPage);
        };

        $scope.showAddForm = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/dataset/dataset_add_form.html',
                controller: 'DatasetModalInstanceController',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    datasetRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "Please fill up the details below";
                    },
                    modalFormTile: function () {
                        return "Add " + $filter('capitalize_first')($scope.set_type);
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_theme: function () {
                        return $scope.project.theme;
                    },
                    setType: function () {
                        return $scope.set_type;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.dataset_id = '';
                datasetService.getDatasets($scope).then(function (data) {
                    $scope.datasetList = data.data.dataset_list;
                    $scope.datasetTotal = data.data.total;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editDatasetEntry = function (id) {
            var datasetRec = {};
            $scope.dataset_id = id;
            angular.element('#editDataset' + id).attr('disabled', 'disabled');
            datasetService.getDatasetDetails($scope).then(function (data) {
                datasetRec = data.data.dataset_list;
                var datasetTitle = datasetRec.title;
                var datasetDescription = datasetRec.description;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/dataset/dataset_add_form.html',
                    controller: 'DatasetModalInstanceController',
                    size: 'lg',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        datasetRec: function () {
                            return datasetRec;
                        },
                        setType: function () {
                            return $scope.set_type;
                        },
                        modalFormText: function () {
                            return "Edit " + $scope.set_type + " '" + $filter('strLimit')(datasetTitle, 20) + "'";
                        },
                        modalFormTile: function () {
                            return "Edit " + $filter('capitalize_first')($scope.set_type);
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    $scope.dataset_id = '';
                    datasetService.getDatasets($scope).then(function (data) {
                        $scope.datasetList = data.data.dataset_list;
                        $scope.datasetTotal = data.data.total;

                    });
                }, function () {
                    angular.element('#editDataset' + id).removeAttr('disabled');
                    $log.info('Modal dismissed at: ' + new Date());
                    $scope.dataset_id = '';
                });
            });
        };

        $scope.deleteDatasetEntry = function (id) {
            SweetAlert.swal({
                title: "Are you sure you want to proceed?",
                text: "You will not be able to recover this " + $filter('capitalize_first')($scope.set_type) + " !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, proceed",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    datasetService.delDatasets(id, $scope).then(function (data) {
                        $scope.listDatasets(1);
                        if (data.status == 200) {
                            SweetAlert.swal("Success", $filter('capitalize_first')($scope.set_type) + " has been deleted", "success");
                        } else {
                            SweetAlert.swal("Cancelled", data.message, "error");
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Process Cancelled!", "error");
                }
            });
        };
        $scope.openPopup= function(entity_id){
                       SweetAlert.swal({title:"Notification",text:"Asset has passed the License period.Please update asset/license information.",showCancelButton: false});
        }
    }]);

// Modal instance controller [add/edit record operation]
app.controller('DatasetModalInstanceController', function ($scope, $filter,$routeParams,datasetService, sessionService, $modalInstance, modalFormText, modalFormTile, project_id, project_permissions, project_theme, datasetRec, setType, SweetAlert) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.project = {'project_permissions': project_permissions};
    $scope.pattern_classes = [{value: ''}];
    $scope.project_theme = project_theme;
    $scope.set_type = setType;
    $scope.func_set_type=$routeParams.set_type;
    if (datasetRec.id) {
        $scope.dataset_id = datasetRec.id;
        $scope.title = datasetRec.title;
        $scope.description = datasetRec.description;
        $scope.showedit = datasetRec.id;
    } else {
        $scope.showedit = 0;
    }

    $scope.addDataset = function () {
        $scope.formError = {'title': ''};
        datasetService.saveDataset($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                datasetService.getDatasets($scope).then(function (data) {
                    $modalInstance.close();
                    $scope.datasetList = data.data.dataset_list;
                    $scope.datasetTotal = data.data.total;
                    var message = $filter('capitalize_first')($scope.set_type) + " has been created";
                    SweetAlert.swal("Success", message, "success");
                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }
        });
    };

    $scope.editDataset = function () {
        $scope.formError = {'title': ''};
        datasetService.editDataset($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                datasetService.getDatasets($scope).then(function (data) {
                    $scope.datasetList = data.data.dataset_list;
                    $modalInstance.close();
                    var message = $filter('capitalize_first')($scope.set_type) + " updated successfully";
                    SweetAlert.swal("Success", message, "success");
                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});