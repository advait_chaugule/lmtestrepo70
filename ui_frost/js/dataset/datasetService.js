'use strict';
app.service('datasetService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        // Service to build the dataset list
        this.getDatasets = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            if (angular.isDefined($scope.dataset_id) && $scope.dataset_id != '') {
                queryString += '&dataset_id=' + $scope.dataset_id;
            }

            if (angular.isDefined($scope.func_set_type) && $scope.func_set_type != '') {
                queryString += '&collection_type=' + $scope.func_set_type;
            }

            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            $http.get(serviceEndPoint + 'api/v1/dataset/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        // service to save new dataset entry
        this.saveDataset = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/dataset?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                title: $scope.title,
                description: $scope.description,
                collection_type: $scope.func_set_type
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
            });

            return deferred.promise;
        };

        // Service to update dataset entry         
        this.editDataset = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/dataset/dataset_id/' + $scope.dataset_id + '?access_token=' + sessionService.get('access_token'), {
                title: $scope.title,
                project_id: $scope.project_id,
                description: $scope.description,
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        /*
         * Delete Dataset entry         
         */
        this.delDatasets = function (dataset_id, $scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }

            $http.delete(serviceEndPoint + 'api/v1/dataset/dataset_id/' + dataset_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Saves record entry for each dataset entry         
         */
        this.savesRecordforDataset = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/dataset-record/dataset_id/' + $scope.dataset_id + '?access_token=' + sessionService.get('access_token'), {
                label: $scope.label,
                object_id: $scope.asset_object_id,
                object_type: $scope.ext_res_type,
                project_id: $scope.project_id,
                url: $scope.ext_res_url
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        /*
         * Updates record entry for each dataset entry         
         */
        this.updateDatasetrecord = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/dataset-record/dataset_id/' + $scope.dataset_id + '?access_token=' + sessionService.get('access_token'), {
                recordId: $scope.recordId,
                label: $scope.label,
                object_id: $scope.asset_object_id,
                object_type: $scope.ext_res_type,
                project_id: $scope.project_id,
                url: $scope.ext_res_url,
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        // Service to insert dataset records
        this.getDatasetRecords = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            if (angular.isDefined($scope.dataset_id) && $scope.dataset_id != '') {
                queryString += '&dataset_id=' + $scope.dataset_id;
            }
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            $http.get(serviceEndPoint + 'api/v1/dataset-record/dataset_id/' + $scope.dataset_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        // Service to update dataset json
        this.updateDatasetJson = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/dataset-json/dataset_id/' + $scope.dataset_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        /*
         * Delete Dataset record entry       
         */
        this.removeDatasetRecord = function (record_id, $scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }
            $http.delete(serviceEndPoint + 'api/v1/dataset-record/record_id/' + record_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        // Service to get dataset details
        this.getDatasetDetails = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            $http.get(serviceEndPoint + 'api/v1/dataset/dataset_id/' + $scope.dataset_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
    }]);

