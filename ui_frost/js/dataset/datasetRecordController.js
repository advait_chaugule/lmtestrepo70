/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



app.controller('DatasetViewInstanceController', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'datasetService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, datasetService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.project_id = $routeParams.project_id;
        $scope.project_type_id = 4;
        $scope.editDisable = false;
        $scope.addrecordbutton = false;
        $scope.urlBox = false;
        $scope.pickerShow = true;
        $scope.pickedData = false;
        $scope.displaytoggle = false;
        $scope.afterPickerShow = false;
        $scope.loading = true;
        $scope.set_type = $routeParams.set_type=='dataset'?'Resource':$routeParams.set_type;
        $scope.func_set_type=$routeParams.set_type;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.project_name = '';
        $scope.getProject = function () {
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project = data;
                        $scope.applyPermission = 1;
                    }
                });
            }
        };

        $scope.getProject();

        $scope.showedit = 0;
        $scope.dataset_id = $routeParams.collection_id;
        $scope.pattern_classes = [{value: ''}];

        $scope.ext_res_opt_val = {
            type: 'asset'
        };

        $scope.cancel = function () {
            angular.element('#viewDataset' + $scope.dataset_id).removeAttr('disabled');
            $modalInstance.dismiss('cancel');
        };

        $scope.addNewRecordRow = function () {
            angular.element('.edit_container_url').hide();
            angular.element('.edit_container_url').hide();
            angular.element('.edit_container_title').show();
            angular.element('.edit_button').show();
            angular.element('.save_button').hide();
            angular.element('.remove_button').show();
            angular.element('.cancle_button').hide();
            angular.element('.container_title_label').show();
            angular.element('.edit_container_title_label').hide();
            angular.element('#for_new_entry').show();
            angular.element('#id_ext_res_url').val('');
            $scope.editDisable = true;
            $scope.addrecordbutton = true;
            $scope.label = '';
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.pickedData = false;
            $scope.urlBox = false;
            $scope.pickerShow = true;
            $scope.formError = {'label': '', 'url': ''};
        };

        $scope.showUrlBox = function () {
            $scope.urlBox = true;
            $scope.formError = {'url': ''};
            $scope.pickerShow = false;
        };

        $scope.deleteUrl = function () {
            $scope.urlBox = false;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.pickedData = false;
            $scope.pickerShow = true;
            $scope.formError = {'url': ''};
        };

        //Assets Modal starts here
        $scope.ext_res_title = null;
        $scope.showAssestModal = function (record_id) {
            if (angular.isDefined('record_id')) {
                var record_id = record_id;
            }
            var size = 'lg';
            var modal_form_title = 'Asset Library';
            var modalInstanceForAssets = $modal.open({
                templateUrl: 'templates/dataset/externalResourceModalContent.html',
                controller: 'ModalDatasetAssetsController',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    ext_res_url: function () {
                        return $scope.ext_res_url;
                    }
                }
            });

            modalInstanceForAssets.result.then(function (modalCloseParameter) {
                $scope.ext_res_url = modalCloseParameter.ext_res_location;
                $scope.asset_object_id = modalCloseParameter.asset_object_id;
                $scope.ext_res_title = modalCloseParameter.ext_res_title;
                $scope.ext_res_type = modalCloseParameter.ext_res_type;
                $scope.afterPickerShow = true;
                angular.element('#id_ext_res_title_' + record_id).html($scope.ext_res_title);
                $scope.pickedData = true;
                $scope.pickerShow = false;
            });
        };

        $scope.pageNumber = 1;
        $scope.itemsPerPage = 5;
        $scope.currentPage = 1;
        $scope.listDataRecords = function (pageNumber) {
            $scope.loading = false;
            $scope.datasetRecordList = {};
            $scope.editDisable = false;
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 5;
            $scope.dataset_id = $routeParams.collection_id;
            datasetService.getDatasetRecords($scope).then(function (data) {
                $scope.datasetRecordList = data.data.dataset_record_list;
                $scope.loading = true;
                $scope.datasetRecordTotal = data.data.total;
            });
        };

        $scope.listDataRecords(1);

        //For Pagination Of Dashboard
        $scope.pageChangeHandler = function (newPage) {
            $scope.listDataRecords(newPage);
        };

        $scope.savesRecordforDataset = function () {
            $scope.dataset_id = $routeParams.collection_id;
            datasetService.savesRecordforDataset($scope).then(function (savedResponse) {
                if (savedResponse.status == '200') {
                    $scope.formError = {'label': '', 'url': ''};
                    SweetAlert.swal("Success", "Record added successfully", "success");
                    $scope.addrecordbutton = false;
                    angular.element('#selectedResourceContainer').html('');
                    $scope.label = '';
                    $scope.ext_res_url = '';
                    $scope.editDisable = false;
                    angular.element('#for_new_entry').attr("style", "display: none");
                    $scope.listDataRecords(1);
                } else {
                    if (angular.isDefined(savedResponse.error.label)) {
                        $scope.formError = {'label': ''};
                        $scope.formError['label'] = savedResponse.error.label['0'];
                        $scope.label = '';
                    } else if (angular.isDefined(savedResponse.error.url)) {
                        $scope.formError = {'url': ''};
                        $scope.formError['url'] = savedResponse.error.url['0'];
                        $scope.ext_res_url = '';
                    }
                }
            });
        };

        $scope.removeItem = function (recordID) {
            SweetAlert.swal({
                title: "Are you sure you want to proceed?",
                text: "You will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, proceed",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    datasetService.removeDatasetRecord(recordID, $scope).then(function (response) {
                        $scope.dataset_id = $routeParams.collection_id;
                        $scope.editDisable = false;
                        if (response.status == 200) {
                            SweetAlert.swal("Success", "Record has been deleted", "success");
                        } else {
                            SweetAlert.swal("Cancelled", response.message, "error");
                        }
                        $scope.listDataRecords(1);
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Process Cancelled!", "error");
                }
            });
        };

        $scope.declineform = function () {
            $scope.editDisable = false;
            angular.element('#for_new_entry').attr("style", "display: none");
            $scope.addrecordbutton = false;
            $scope.label = '';
            $scope.video_id = '';
            angular.element('#selectedResourceContainer').html('');

            $scope.dataset_id = $routeParams.collection_id;
            SweetAlert.swal("Cancelled", "Operation Cancelled!", "error");
            $scope.formError = {'label': '', 'url': ''};
            $scope.listDataRecords(1);
        };

        $scope.showEditBoxes = function (id, url) {
            $scope.ext_res_url = url;
            angular.element('.edit_container_url').hide();
            angular.element('.edit_container_title').show();
            angular.element('.edit_button').show();
            angular.element('.save_button').hide();
            angular.element('.remove_button').show();
            angular.element('.cancle_button').hide();
            angular.element('.container_title_label').show();
            angular.element('.edit_container_title_label').hide();
            $scope.editDisable = false;
            $scope.pickedData = true;
            $scope.pickerShow = false;
            $scope.urlBox = false;
            $scope.editDisable = true;
            $scope.formError = {'label': '', 'url': ''};
            angular.element('#s_button_' + id).hide();
            angular.element('#edit_button_' + id).show();
            angular.element('#display_container_label_' + id).hide();
            angular.element('#edit_container_label_' + id).show();
            angular.element('#display_container_url_' + id).hide();
            angular.element('#edit_container_url_' + id).show();
            angular.element('#cancelButton' + id).show();
            angular.element('#removeButton' + id).hide();
            console.log(angular.element('#filename' + id).val());
            $scope.ext_res_title = angular.element('#filename' + id).val();
        };

        $scope.cancelRecordEdit = function (id) {
            $scope.dataset_id = $routeParams.collection_id;
            $scope.formError = {'label': '', 'url': ''};
            $scope.editDisable = false;
            SweetAlert.swal("Cancelled", "Process Cancelled!", "error");
            $scope.listDataRecords(1);
        };

        $scope.updateRecord = function (id) {
            $scope.editDisable = false;
            $scope.dataset_id = $routeParams.collection_id;
            if ($scope.ext_res_url == '' || $scope.ext_res_url == 'undefined') {
                $scope.ext_res_url = angular.element('#id_ext_res_url' + id).val();
            }
            $scope.label = angular.element('#label_' + id).val();
            $scope.recordId = id;

            datasetService.updateDatasetrecord($scope).then(function (savedResponse) {
                if (savedResponse.status == '200') {
                    $scope.formError = {'label': '', 'url': ''};
                    SweetAlert.swal("Success", "Record updated successfully", "success");
                    $scope.addrecordbutton = false;
                    console.log($scope.ext_res_title);
                    angular.element('#selectedResourceContainer').html($scope.ext_res_title);
                    $scope.label = '';
                    $scope.ext_res_url = '';
                    angular.element('#for_new_entry').attr("style", "display: none");
                    angular.element('#s_button_' + id).show();
                    angular.element('#edit_button_' + id).hide();
                    angular.element('#display_container_label_' + id).show();
                    angular.element('#edit_container_label_' + id).hide();
                    angular.element('#display_container_url_' + id).show();
                    angular.element('#edit_container_url_' + id).hide();
                    $scope.listDataRecords(1);
                    $scope.afterPickerShow = false;

                } else {
                    if (angular.isDefined(savedResponse.error.label)) {
                        $scope.formError = {'label': ''};
                        $scope.formError['label'] = savedResponse.error.label['0'];
                        $scope.label = '';
                    } else if (angular.isDefined(savedResponse.error.url)) {
                        $scope.formError = {'url': ''};
                        $scope.formError['url'] = savedResponse.error.url['0'];
                        $scope.ext_res_url = '';
                    }
                }
            });
        }

        $scope.updateDatasetJson = function () {
            $scope.dataset_id = $routeParams.collection_id;
            datasetService.updateDatasetJson($scope).then(function (savedResponse) {
                if (savedResponse.status == '200') {
                    datasetService.getDatasets($scope).then(function (data) {
                        $scope.datasetList = data.data.dataset_list;
                        SweetAlert.swal("Success", "Dataset details updated successfully", "success");
                        $scope.listDataRecords(1);
                    });
                } else {
                    console.log(savedResponse.status);
                }
            });
        };

        $scope.showDatasetAsset = function (size, id) {
            var datasetRec = {};
            var size = 'lg';
            $scope.dataset_id = id;
            angular.element('#viewDataset' + id).attr('disabled', 'disabled');
            datasetService.getDatasets($scope).then(function (data) {
                datasetRec = data.data.dataset_list;
                var datasetTitle = datasetRec[0].title;
            });
        };
        $scope.showDatasetAsset();


        $scope.addNewMultivideoRow = function () {
            $scope.addrecordbutton = true;
            $scope.isNumber();
            $scope.editDisable = true;
            $scope.formError = {'label': '', 'url': ''};
            angular.element('#for_new_entry').show();
        };

        $scope.saveMultivideoRow = function () {
            $scope.formError = {'label': '', 'url': ''};
            $scope.dataset_id = $routeParams.collection_id;
            $scope.label = $scope.label;
            $scope.asset_object_id = $scope.video_id;
            if (angular.isDefined($scope.video_id)) {
                //$scope.ext_res_url = 'https://players.brightcove.net/2402232200001/default_default/index.html?videoId=' + $scope.video_id;
                $scope.ext_res_url = bright_cove_url + $scope.video_id;
            } else {
                $scope.ext_res_url = '';
            }
            if (angular.isDefined($scope.label) && $scope.asset_object_id != '') {
                datasetService.savesRecordforDataset($scope).then(function (savedResponse) {
                    if (savedResponse.status == '200') {
                        SweetAlert.swal("Success", "Record added successfully", "success");
                        angular.element('#addrecordbutton').removeAttr('disabled');
                        angular.element('#selectedResourceContainer').html('');
                        $scope.label = '';
                        $scope.video_id = '';
                        $scope.ext_res_url = '';
                        angular.element('#for_new_entry').attr("style", "display: none");
                        $scope.listDataRecords(1);
                        $scope.isNumber();
                        $scope.addrecordbutton = false;
                        $scope.editDisable = false;
                    } else {
                        if (angular.isDefined(savedResponse.error.label) && angular.isDefined(savedResponse.error.url)) {
                            $scope.formError = {'label': ''};
                            $scope.formError['label'] = savedResponse.error.label['0'];
                            if (!angular.isDefined($scope.asset_object_id)) {
                                $scope.formError['url'] = 'Video ID is needed';
                            }
                        } else if (!angular.isDefined($scope.asset_object_id)) {
                            $scope.formError = {'url': ''};
                            $scope.formError['url'] = 'Video ID is needed';
                        } else if (angular.isDefined(savedResponse.error.label)) {
                            $scope.formError['label'] = savedResponse.error.label['0'];
                        }
                    }
                });
            } else {
                if (!angular.isDefined($scope.label) || $scope.label == '') {
                    $scope.formError['label'] = 'Title filed is required';
                } else if (!angular.isDefined($scope.asset_object_id) || $scope.asset_object_id == '') {
                    $scope.formError['url'] = 'Video ID is Needed';
                } else {
                    $scope.formError['url'] = 'Video ID is Needed';
                }

            }
        };

        $scope.editVideoRecord = function (record_id) {
            $scope.formError = {'label': '', 'url': ''};
            angular.element('#addRecord' + record_id).hide();
            angular.element('#addRecordID' + record_id).hide();
            angular.element('#editRecord' + record_id).show();
            angular.element('#editRecordID' + record_id).show();
            angular.element('#editButton' + record_id).hide();
            angular.element('#cancelButton' + record_id).show();
            angular.element('#saveButton' + record_id).show();
            angular.element('#removeButton' + record_id).hide();
            angular.element('#previewButton' + record_id).hide();
            $scope.isNumber();
            $scope.addrecordbutton = true;
            $scope.editDisable = true;
        };

        $scope.cancelVideoEdit = function (record_id) {
            $scope.formError = {'label': '', 'url': ''};
            angular.element('#addRecord' + record_id).show();
            angular.element('#addRecordID' + record_id).show();
            angular.element('#editRecord' + record_id).hide();
            angular.element('#editRecordID' + record_id).hide();
            angular.element('#editButton' + record_id).show();
            angular.element('#cancelButton' + record_id).hide();
            angular.element('#saveButton' + record_id).hide();
            angular.element('#removeButton' + record_id).show();
            angular.element('#previewButton' + record_id).show();
            $scope.addrecordbutton = false;
            $scope.editDisable = false;
            $scope.listDataRecords($scope.currentPage);
            $scope.isNumber();
            $scope.label = '';
            $scope.video_id = '';
            $scope.ext_res_url = '';
        };

        $scope.updateVideoRccord = function (record_id) {
            $scope.formError = {'label': '', 'url': ''};
            $scope.dataset_id = $routeParams.collection_id;
            $scope.label = angular.element('#editData' + record_id).val();
            $scope.asset_object_id = angular.element('#editText' + record_id).val();
            if (angular.isDefined($scope.asset_object_id)) {
                //$scope.ext_res_url = 'https://players.brightcove.net/2402232200001/default_default/index.html?videoId=' + $scope.asset_object_id;
                $scope.ext_res_url = bright_cove_url + $scope.asset_object_id;
            } else {
                $scope.ext_res_url = '';
            }

            $scope.recordId = record_id;
            if (angular.isDefined($scope.label) && $scope.asset_object_id != '') {
                datasetService.updateDatasetrecord($scope).then(function (savedResponse) {
                    if (savedResponse.status == '200') {
                        $scope.formError = {'label': '', 'url': ''};
                        SweetAlert.swal("Success", "Record updated successfully", "success");
                        $scope.addrecordbutton = false;
                        $scope.editDisable = false;
                        $scope.label = '';
                        $scope.ext_res_url = '';
                        angular.element('#addRecord' + record_id).show();
                        angular.element('#addRecordID' + record_id).show();
                        angular.element('#editRecord' + record_id).hide();
                        angular.element('#editRecordID' + record_id).hide();
                        angular.element('#editButton' + record_id).show();
                        angular.element('#cancelButton' + record_id).hide();
                        angular.element('#saveButton' + record_id).hide();
                        angular.element('#removeButton' + record_id).show();
                        angular.element('#previewButton' + record_id).show();
                        $scope.listDataRecords($scope.currentPage);
                        $scope.isNumber();
                        //$scope.listDataRecords();

                    } else {
                        if (angular.isDefined(savedResponse.error.label)) {
                            $scope.formError = {'label': ''};
                            $scope.formError['label'] = savedResponse.error.label['0'];
                        } else if (angular.isDefined($scope.ext_res_url)) {
                            $scope.formError = {'url': ''};
                            $scope.formError['url'] = 'Video ID is Needed';
                        } else if (angular.isDefined(savedResponse.error.label)) {
                            $scope.formError['label'] = savedResponse.error.label['0'];
                        }
                    }
                });
            } else {
                if ($scope.label == '') {
                    $scope.formError['label'] = 'Title filed is required';
                } else {
                    $scope.formError['label'] = '';
                }
                if ($scope.asset_object_id == '') {
                    $scope.formError['url'] = 'Video ID is Needed';
                } else {
                    $scope.formError['url'] = '';
                }

            }
        };

        $scope.previewVideoEdit = function (video_id) {
            var size = 'md';
            var modal_form_title = 'Brightcove Preview';

            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project = data;
                        $scope.applyPermission = 1;
                    }
                });
            }
            var modalInstanceForAssets = $modal.open({
                templateUrl: 'templates/dataset/previewModalContent.html',
                controller: 'ModalVideoPreviewController',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    video_id: function () {
                        return video_id;
                    }
                }
            });

            modalInstanceForAssets.result.then(function (modalCloseParameter) {
            });
        };

        $scope.isNumber = function () {
            $(".label_id").keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode === 86 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        };

    }]);

//Assets Modal Controller 
app.controller('ModalDatasetAssetsController', function ($scope, $timeout, $modalInstance, assetService, datasetService, objectService, project_id, modalFormTitle, ext_res_url, keyValueService, tagService, taxonomyService, userService, ivhTreeviewMgr, $compile, $filter) {

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.modalFormTitle = modalFormTitle;
    $scope.project_id = project_id;
    $scope.taxonomy_id = 0;
    $scope.asset_filter = {"images": true, "audio": true, "videos": true, "gadgets": true, "other": true};
    $scope.asset_local_global = {"local": true, "global": true};
    $scope.pageNumber = 1;
    $scope.itemsPerPage = 8;
    $scope.ext_res_location = ext_res_url;
    $scope.getAssets = function (pageNumber) {
        $scope.removeErrors = 1;
        $scope.showCheckAll = 0;
        $scope.showMakeGlobal = 0;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = 8;

        assetService.getMimes().then(function (mimes) {
            angular.element('#submit_resource').attr('disabled', 'disabled');
            $scope.mimes = mimes.data;
            assetService.getAssets($scope).then(function (data) {
                $scope.assets = data.data;
                angular.forEach($scope.assets, function (value, key) {
                    if (value.asset_location == $scope.ext_res_location) {
                        value.Selected = $scope.ext_res_location;
                        value.Style = "opacity:1 !important;";
                        angular.element('#submit_resource').attr('disabled', false);
                        $timeout(function () {
                            angular.element('#submit_resource').focus();
                        });
                    }
//          else if ($scope.ext_res_location == '') {
//            angular.element('#submit_resource').attr('disabled', 'disabled');
//          } else {
//            angular.element('#submit_resource').attr('disabled', false);
//          }
                });
                $scope.totalAssets = data.totalAssets;
                $scope.removeErrors = angular.isDefined(data.totalAssets) ? data.totalAssets : 0;
            });
        });
    };

    //$scope.getAssets(1);

    //Get Project Detail
    $scope.getProject = function () {
        $scope.project_name = '';
        if ($scope.object_id != '') {
            objectService.getProjectDetails($scope).then(function (data) {
                $scope.project_name = data.name;
                $scope.project_type_id = data.project_type_id;
                $scope.toc_creation_status = data.toc_creation_status;
                $scope.project = data;
                $scope.actual_permission = data.project_permissions['asset.use_global'].grant;
            });
        }
    };
    $scope.getProject();

    //For Pagination Of Dashboard
    $scope.pageChangeHandlerAsset = function (newPage) {
        $scope.getAssets(newPage);
        angular.element('.label-checkbox').removeAttr('style');
    };

    $scope.$watch('search_text', function (val) {
        $scope.advanced_search.active = false;
        $scope.advanced_search.enableSearch = false;
        $scope.searchModel = {"search_text": val};
        $scope.getAssets(1);
    });

    $scope.selectOption = function (asset_id, asset_location, title, type, is_global) {
        if (is_global === 1 && $scope.actual_permission === false) {
            angular.element('.label-checkbox').removeAttr('style');
            angular.element('#submit_resource').attr('disabled', 'disabled');
            SweetAlert.swal("Cancelled", "Permission Denied!", "error");
        } else {
            angular.element('.label-checkbox').removeAttr('style');
            angular.element('#check-box-label-' + asset_id).attr('style', 'opacity:1 !important;');
            $scope.ext_res_location = asset_location;
            $scope.ext_res_title = title;
            $scope.ext_res_type = type;
            $scope.asset_id = asset_id;
            setTimeout(function () {
                angular.element('#submit_resource').focus();
            }, 200);
            angular.element('#submit_resource').attr('disabled', false);

        }
    };

    $scope.submitResource = function () {
        var modalCloseParameter = {ext_res_location: $scope.ext_res_location, ext_res_title: $scope.ext_res_title, ext_res_type: $scope.ext_res_type, asset_object_id: $scope.asset_id};
        $modalInstance.close(modalCloseParameter);
        angular.element('#selectedResourceContainer').html('');
        var selectedResourceContainerHtml = '<div id="newContainer"><div class="clearfix"></div><div class="form-group">';

        if ($scope.ext_res_type == 'image') {
            selectedResourceContainerHtml += '<img src="' + $scope.ext_res_location + '" height="40px" width="40px">';
        } else {
            selectedResourceContainerHtml += '<span style="word-break: break-all;padding: 3px;color: #7b2503;display: block;">' + $scope.ext_res_title + '</span>';
        }
        selectedResourceContainerHtml += '<input type="hidden" value="' + $scope.asset_id + '" ng-model="asset_object_id">';
        selectedResourceContainerHtml += '</div></div>';

        angular.element('#selectedResourceContainer').append($compile(selectedResourceContainerHtml)($scope));

        angular.element('#id_ext_res_url').attr("value", $scope.ext_res_location);
    };

    $scope.submitResource = function () {
        var modalCloseParameter = {ext_res_location: $scope.ext_res_location, ext_res_title: $scope.ext_res_title, ext_res_type: $scope.ext_res_type, asset_object_id: $scope.asset_id};
        $modalInstance.close(modalCloseParameter);
        angular.element('#selectedResourceContainer').html('');
        var selectedResourceContainerHtml = '<div id="newContainer"><div class="clearfix"></div><div class="form-group">';

        if ($scope.ext_res_type == 'image') {
            selectedResourceContainerHtml += '<img src="' + $scope.ext_res_location + '" height="40px" width="40px">';
        } else {
            selectedResourceContainerHtml += '<span style="word-break: break-all;padding: 3px;color: #7b2503;display: block;">' + $scope.ext_res_title + '</span>';
        }
        selectedResourceContainerHtml += '<input type="hidden" value="' + $scope.asset_id + '" ng-model="asset_object_id">';
        selectedResourceContainerHtml += '</div></div>';

        angular.element('#selectedResourceContainer').append($compile(selectedResourceContainerHtml)($scope));

        angular.element('#id_ext_res_url').attr("value", $scope.ext_res_location);
    };
    
        //=========================================//
    //Advance Search For Metadata = Starts Here
    //=========================================//
    $scope.node_id = 0;
    $scope.advanced_search = {};
    $scope.advanced_search.enableSearch = false;
    $scope.advanced_search.title = '';
    $scope.advanced_search.active = false;
    $scope.advanced_search.isCollapsed = false;
    $scope.advanced_search.heading = 'Advanced Search Result:';

    $scope.advanced_search.key_data = {};
    $scope.advanced_search.value_data = {};
    $scope.advanced_search.totalKeyArea = 0;
    $scope.advanced_search.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.advanced_search.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
            }
        }

    });

    // get tags 
    var tags = [];
    var object_tags = [];


    $scope.advanced_search.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.advanced_search.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tagValue = object_tags;
                }
            }
        }

    });

    // get owners 
    var owners = [];
    var object_owners = [];

    $scope.advanced_search.owner = {
        multiple: true,
        data: owners
    };

    userService.getUsers($scope).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null)
            {
                angular.forEach(data.data, function (value, key) {
                    owners[key] = {id: value.id, text: value.username};
                });
                $scope.advanced_search.owner.data = owners;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
//        if (data.projectPermissionGranted == 401) {
//            SweetAlert.swal("Cancelled", "Access prohibited!", "error");
//            $location.path("dashboard");
//        }
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }

            if (data.count == 0) {
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);                    
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.advanced_search.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                } else if (value.key_type == 'time_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.advanced_search.key_data['key_' + keyArray[1]];
        delete $scope.advanced_search.value_data['key_' + keyArray[1]];
        if (angular.element(".advanced-search-key-values-area").length == 0)
        {
            $scope.advanced_search.totalKeyArea = 1;
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';        
        return keySelect;
    };

    $scope.applyAdvancedSearch = function () {
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.active = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.getAssets(1);
    };

    $scope.clearSearch = function () {
        $scope.advanced_search.active = false;
        $scope.search_text = '';
        $scope.advanced_search.enableSearch = false;
        $scope.getAssets(1);
    };

    $scope.toggleAdvanceSearch = function () {
        if ($scope.advanced_search.isCollapsed) {
            $scope.advanced_search.isCollapsed = false;
            if (htmlEditor.advanced_search.active) {
                $scope.advanced_search.enableSearch = true;
            }
        } else {
            $scope.advanced_search.isCollapsed = true;
            $scope.advanced_search.enableSearch = false;
        }
    };

    $scope.applyAdvancedClear = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
        $scope.advanced_search.tagValue = [];
        $scope.advanced_search.ownerValue = [];
        $scope.advanced_search.title = '';
        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
    };

    $scope.generateAdvanceSearchTerm = function () {
        var searchTerm = [];
        var tagList = $scope.advanced_search.tagValue;
        if (tagList.length > 0) {
            angular.forEach(tagList, function (tag) {
                searchTerm.push(tag.text);
            });
        }
        var ownerList = $scope.advanced_search.ownerValue;
        if (ownerList.length > 0) {
            angular.forEach(ownerList, function (owner) {
                searchTerm.push(owner.text);
            });
        }
        var taxonomyList = $scope.selectedTaxonomy;
        if (taxonomyList.length > 0) {
            angular.forEach(taxonomyList, function (taxonomy) {
                searchTerm.push(taxonomy);
            });
        }

        if ($scope.advanced_search.title != '') {
            searchTerm.push($scope.advanced_search.title);
        }

    };

    $scope.selectedTaxonomy = [];
    $scope.selectedTaxonomyIds = [];

    $scope.$watch('advanced_search.taxonomy', function (newValues) {
        $scope.selectedTaxonomy.length = 0;
        $scope.selectedTaxonomyIds.length = 0;
        angular.forEach(newValues, function (item) {
            if (item.selected == true) {
                $scope.selectedTaxonomy.push(item.title);
                $scope.selectedTaxonomyIds.push(item.id);
            }
        });
    }, true);
    
});

app.controller('ModalVideoPreviewController', function ($scope, $sce, $modalInstance, assetService, datasetService, project_id, modalFormTitle, video_id, $compile) {
    $scope.modalFormTitle = modalFormTitle;
    $scope.project_id = project_id;
    $scope.video_id = video_id;
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    };
    //$scope.url = {src: 'https://players.brightcove.net/2402232200001/default_default/index.html?videoId=' + video_id};
    $scope.url = {src: bright_cove_url + video_id};
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


