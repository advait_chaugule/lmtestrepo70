'use strict';

var app = angular.module('TemplatesApp', [
    'flow',
    "ngRoute",
    "ngDragDrop",
    "ui.sortable",
    "checklist-model",
    "angular-loading-bar",
    'ui.bootstrap',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngSanitize',
    'ui.select2',
    'ivh.treeview',
    'mgcrea.ngStrap.datepicker',
    'mgcrea.ngStrap.timepicker'
], function ($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
});




app.run(function ($rootScope, $location, loginService, sessionService, $templateCache, $window, utilityService, $modalStack) {
    window.onunload = function () {
        //Do your stuff here
        loginService.logout();
        return false;
    }
    var routespermission = ['dashboard', 'createproject'];  //route that require login
    $rootScope.$on('$routeChangeStart', function () {

        //Makeing scroll top
        $window.scrollTo(0, 0);

        // Making fullScreen disable for rest of the screen 
        // fullScreen will be set while only on toc_content page
        $rootScope.fullScreen = '';
        var page = $location.path();
        page = page.slice(1, page.length);

        if (page.indexOf('dashboard') == -1) {
            $rootScope.show_dashboard_success_msg = 0;
            $rootScope.myprojectActive = 1;
            $rootScope.adminActive = 0;
            $rootScope.metadataActive = 0;
            $rootScope.widgetActive = 0;
            $rootScope.globalrepoActive = 0;
        }
        $rootScope.myprojectActive = 0;
        $rootScope.adminActive = 0;
        $rootScope.metadataActive = 0;
        $rootScope.widgetActive = 0;
        $rootScope.globalrepoActive = 0;
        if (page.indexOf('dashboard') != -1) {
            $rootScope.myprojectActive = 1;
        } else if (page.indexOf('settings') != -1) {
            $rootScope.adminActive = 1;
        } else if (page.indexOf('metadata') != -1) {
            $rootScope.metadataActive = 1;
        } else if (page.indexOf('widget') != -1) {
            $rootScope.widgetActive = 1;
        } else if (page.indexOf('global_repo') != -1) {
            $rootScope.globalrepoActive = 1;
        } else {
            $rootScope.myprojectActive = 0;
            $rootScope.adminActive = 0;
            $rootScope.metadataActive = 0;
            $rootScope.widgetActive = 0;
            $rootScope.globalrepoActive = 0;
        }

        if (page.indexOf('edit_toc') == -1) {
            $rootScope.show_edit_success_msg = 0;
            $rootScope.show_success_msg = 0;
        }


        if (routespermission.indexOf(page) != -1 && !loginService.islogged())
        {
            $location.path('login');
            return true;
        } else if (loginService.islogged()) {
            //$rootScope.checkUserStatus();
            var userinfo = JSON.parse(sessionService.get('userinfo'));
            var username = loginService.ucfirst(userinfo.username);
            $rootScope.loggedinuser = {"id": userinfo.id, "name": username};
        }
    });

    $rootScope.checkUserStatus = function () {
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        // loginService.userStatusCheck(userinfo.id).then(function (data) {
        //     if (angular.isDefined(data)) {
        //         if (angular.isDefined(data.http_status) && String(data.http_status) == '400') {
        //             //Anything goes wrong on user status check
        //             loginService.logout();
        //             return false;
        //         } else if (String(data.status) == '200') {
        //             if (data.data.status == '2000') {
        //                 //If the user is not active
        //                 loginService.logout();
        //                 return false;
        //             }
        //         }
        //     } else {
        //         loginService.logout();
        //         return false;
        //     }

        // });
    }
    $rootScope.goto = function (page) {
        $location.path(page);
    };

    // Removing template cache
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        if (typeof (current) !== 'undefined') {
            //console.log($templateCache.get(current.templateUrl));
            $templateCache.remove(current.templateUrl);
        }
    });

    //** Show Meno class starts on body tag//
    $rootScope.showMenuClass = '';
    $rootScope.toggleShowMenuClass = function () {
        $rootScope.showMenuClass = $rootScope.showMenuClass === '' ? 'show-menu' : '';
    };
    //** Show Meno class ends//   

    $rootScope.clearCache = function () {
        //console.log('template cache is removed');
        //console.log($templateCache.get("template/tooltip/tooltip-html-unsafe-popup.html"));
        //$templateCache.removeAll();
    }

    // logic here to show/hide Searchbox based upon $location.path()  
    // Show asset searchbox only on asset page.
    $rootScope.$on('$routeChangeSuccess', function () {
        var action_path = $location.path();
        /*if (action_path.indexOf("asset") == '-1') {
         $rootScope.showSearchBox = false;
         }*/

        $rootScope.header_search_type = '';
        $rootScope.showSearchBox = false;
        if (action_path.indexOf("asset") != -1) {
            $rootScope.header_search_type = 'Asset';
            $rootScope.showSearchBox = true;
        } else if (action_path.indexOf("dashboard") != -1) {
            $rootScope.header_search_type = 'Project';
            $rootScope.showSearchBox = true;
        } else if (action_path.indexOf("global_repo") != -1) {
            $rootScope.header_search_type = 'GlobalRepo';
            $rootScope.showSearchBox = true;
        } else if (action_path.indexOf("taxonomies") != -1) {
            $rootScope.header_search_type = 'Taxonomies';
            $rootScope.showSearchBox = true;
        }

    });

    $rootScope.formatDate = function (date) {
        var t = date.split(/[- :]/);
        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
        var dateOut = new Date(d);
        return dateOut;
    };

    $rootScope._lang = _lang; //Accessing Global Language Variable into Angular
    $rootScope._rule = _rule; //Accessing Global Rule Variable into Angular

    var d = new Date();
    $rootScope.current_year = d.getFullYear();

    $rootScope.$on('$routeChangeStart', function (event) {
        var top = $modalStack.getTop();
        if (top) {
            $modalStack.dismiss(top.key);
        }
    });

});

/*app.run(function($rootScope, $templateCache) {
 $rootScope.$on('$viewContentLoaded', function() {
 $templateCache.removeAll();
 });
 });*/

 app.filter('firstLetter', function () {
    return function (input, key, letter) {
        input = input || [];
        var out = [];
        input.forEach(function (item) {
            console.log('item: ', item[key][0].toLowerCase());
            console.log('letter: ', letter);
            if (item[key][0].toLowerCase() == letter.toLowerCase()) {
                out.push(item);
            }
        });
        return out;
    }
});;


