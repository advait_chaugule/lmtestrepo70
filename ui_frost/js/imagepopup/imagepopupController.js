'use strict';
// image popup controller
app.controller('imagepopupcontroller', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'imagepopupService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, imagepopupService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {

    $scope.access_token = sessionService.get('access_token');
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    var userinfo = JSON.parse(sessionService.get('userinfo'));
    $scope.project_id = $routeParams.project_id;
    $scope.project_type_id = 4;
    $scope.set_type = $routeParams.set_type;
    if (!$scope.access_token || userinfo == null) {
      $location.path('logout');
      return true;
    }

    $scope.project_name = '';
    $scope.getProject = function () {
      if ($scope.project_id != '') {
        objectService.getProjectDetails($scope).then(function (data) {
          if (data == null) {
            $location.path("edit_toc/" + $scope.project_id);
          } else if (angular.isObject(data)) {
            $scope.project_name = data.name;
            $scope.project = data;
            $scope.applyPermission = 1;
            $scope.project.project_permissions = data.project_permissions;
          }
        });
      }
    };

    $scope.getProject();

    //// listing
    $scope.pageNumber = 1;
    $scope.itemsPerPage = 10;
    $scope.currentPage = 1;

    $scope.addButtonDisabled = false;
    $scope.removeButtonDisabled = true;

    $scope.listLogs = function (pageNumber) {
      $scope.currentPage = pageNumber;
      $scope.pageNumber = pageNumber;
      $scope.itemsPerPage = 10;
      imagepopupService.getLogs($scope).then(function (data) {
        $scope.logList = data.data.log_list;
        $scope.logTotal = data.data.total;
        $scope.lastAction = data.data.last_action;
        $scope.active_status_count = data.data.active_status_count;
        if ($scope.active_status_count > 0) {
          $scope.addButtonDisabled = false;
          $scope.removeButtonDisabled = false;
          $scope.process_text = '';
        }
        else if ($scope.active_status_count == 0 & $scope.lastAction == 0) {
          $scope.addButtonDisabled = false;
          $scope.removeButtonDisabled = false;
          $scope.process_text = '';
        } else {
          $scope.addButtonDisabled = true;
          $scope.removeButtonDisabled = true;
          $scope.process_text = "Image Popup ADD/REMOVE action is in progress. Please comeback later to check the log.";
        }
      });
    };
    $scope.listLogs(1);

    $scope.pageChangeHandlerDataset = function (newPage) {
      $scope.listLogs(newPage);
    };
    ////// listing

    // add class
    $scope.bulkApplyClass = function () {
      SweetAlert.swal({
        title: "Are you sure you want to proceed?",
        text: "This action will apply image popup class in figures",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, proceed",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          imagepopupService.applyClassBulk($scope).then(function (data) {
            $scope.listLogs(1);
            $scope.addButtonDisabled = true;
            $scope.removeButtonDisabled = true;
            if (data.status == 200) {
              SweetAlert.swal("Success", "Process has been initiated. Please comeback later to check the updated log.", "success");
            } else {
              SweetAlert.swal("Cancelled", data.message, "error");
            }
          });
        } else {
          SweetAlert.swal("Cancelled", "Process Cancelled!", "error");
        }
      });
    };

    // remove class
    $scope.bulkRemoveClass = function () {
      SweetAlert.swal({
        title: "Are you sure you want to proceed?",
        text: "This action will remove image popup class from figures",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, proceed",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          imagepopupService.removeClassBulk($scope).then(function (data) {
            $scope.listLogs(1);
            $scope.addButtonDisabled = true;
            $scope.removeButtonDisabled = true;
            if (data.status == 200) {
              SweetAlert.swal("Success", "Process has been initiated. Please comeback later to check the updated log.", "success");
            } else {
              SweetAlert.swal("Cancelled", data.message, "error");
            }
          });
        } else {
          SweetAlert.swal("Cancelled", "Process Cancelled!", "error");
        }
      });
    };

  }]);