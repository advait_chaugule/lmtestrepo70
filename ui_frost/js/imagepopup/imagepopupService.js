'use strict';
app.service('imagepopupService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        // Service to find and apply class for each nodes of a project
        this.applyClassBulk = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            $http.get(serviceEndPoint + 'api/v1/imagepopup/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
        // Service to find and remove class for each nodes of a project
        this.removeClassBulk = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            $http.delete(serviceEndPoint + 'api/v1/imagepopup/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
        // Service to build the log list
        this.getLogs = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            $http.get(serviceEndPoint + 'api/v1/imagepopup/logs/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
    }]);

