/*****
 * Controller for template
 * Author - Alamgir Hossain Sk * 
 */

'use strict';
app.controller('template', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'templateService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', '$window', function ($scope, $http, sessionService, $location, objectService, templateService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter, $window) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.user_id = userinfo.id;

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.currentOrderByField = 'title';
        $scope.reverseSort = false;
        $scope.searchModel = {};
        //console.log(sessionService.get('user_permissions'));
        //console.log($scope.user_permissions);

        $scope.project_id = $routeParams.project_id;

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.template_local_global = {};
        $scope.template_local_global.local = true;
        $scope.template_local_global.global = false;

        if($scope.user_permissions['global_template.show.all'].grant) {
            $scope.template_local_global.global = true;
        } 

        $scope.pageNumber = 1;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.listTemplates = function (pageNumber) {
            //alert('test');
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            templateService.getTemplate($scope).then(function (data) {
                $scope.templateList = data.data;
                $scope.totalTemplates = data.totalTemplates;
            });
        };

       $scope.listTemplates(1);

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerTemplate = function (newPage) {
            $scope.listTemplates(newPage);
        };

        $scope.addTemplateForm = function (size) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/template/myModalContent.html',
                controller: 'ModalInstanceTemplateCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    templateRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a new template";
                    },
                    modalFormTile: function () {
                        return "Add Template";
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_theme: function () {
                        return $scope.project.theme;
                    }
                }
            });
            modalInstance.result.then(function () {
                //Saving TOC    
                $scope.template_id = '';
                templateService.getTemplate($scope).then(function (data) {
                    $scope.templateList = data.data;
                    $scope.totalTemplates = data.totalTemplates;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editTemplate = function (size, id) {
            var templateRec = {};
            $scope.template_id = id;
            templateService.getTemplateDetail($scope).then(function (data) {
                templateRec = data.data;
                var templateTitle = templateRec.object.name;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/template/myModalContent.html',
                    controller: 'ModalInstanceTemplateCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        templateRec: function () {
                            return templateRec;
                        },
                        modalFormText: function () {
                            return "You are trying to edit template [" + $filter('strLimit')(templateTitle, 50) + "]";
                        },
                        modalFormTile: function () {
                            return "Edit Template";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        },
                        id: function () {
                            return $scope.id;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    //Saving TOC    
                    $scope.id = '';
                    templateService.getTemplate($scope).then(function (data) {
                        $scope.templateList = data.data;
                        $scope.totalTemplates = data.totalTemplates;
                    });
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.viewTemplate = function (size, id) {
            var templateRec = {};
            $scope.template_id = id;
            templateService.getTemplateDetail($scope).then(function (data) {
                templateRec = data.data;

                var modalInstance = $modal.open({
                    templateUrl: 'templates/template/templateModalContentView.html',
                    controller: 'ModalInstanceTemplateCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        templateRec: function () {
                            return templateRec;
                        },
                        modalFormText: function () {
                            return "You are trying to view a template";
                        },
                        modalFormTile: function () {
                            return "Preview Template";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        }
                    }
                });

                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };


        $scope.deleteTemplate = function (id, project_id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this template!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    templateService.delTemplate(id, project_id).then(function () {
                        $scope.id = '';
                        $scope.listTemplates(1);
                    });
                    SweetAlert.swal("Success", "Template has been deleted", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Template is safe :)", "error");
                }
            });

        };

        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        }

        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    $scope.project_name = data.name;
                    $scope.toc_creation_status = data.toc_creation_status;
                    $scope.project = data;//{};
                    $scope.project.project_permissions = data.project_permissions;
                    $scope.project.theme = data.theme_location;                    
                    if($scope.project.project_permissions['template.show.all'].grant) {
                        $scope.pattern_local_global.local = true;
                    }
                    $scope.listTemplates(1);
                });
            }
        }
        $scope.getProject();

        $scope.changeStatus = function (id) {
            templateService.changeStatus(id, $scope).then(function () {
                $scope.id = '';
            });
        }

        $scope.gotoEditor = function (template_id) {
            $scope.template_id = template_id;
            templateService.getTemplateDetail($scope).then(function (data) {
                if (data.status == '200') {
                    var templateData = data.data;
                    //console.log(templateData.project_id);
                    //console.info($scope.project_id);
                    $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + templateData.project.id + '&node_id=' + templateData.object.id + '&node_title=' + templateData.object.name + '&node_type_name=&node_type_id=&project_type_id=' + templateData.project.project_type_id + '&ref=TEMPLATE&template_id=' + templateData.object.id + '&redirect_project_id=' + $scope.project_id+'&content_from=local_template';
                }
            });

        }

        $scope.apply_filter = function () {
            if ($scope.template_local_global.global == false && $scope.template_local_global.local == false) {
                $scope.template_local_global.local = true;
                //$scope.template_local_global.global = true;
            }
            $scope.listTemplates(1);
        }
        $scope.changeAccess = function (id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "Want to change Template access!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    templateService.makeGlobal(id, $scope).then(function (data) {
                        $scope.listTemplates($scope.currentPage);
                    });
                    SweetAlert.swal("Success", "Template access changed successfully.", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Template is safe :)", "error");
                }
            });
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.listTemplates(1);
        };

        // apply searching
        $scope.do_search = function () {
            $scope.listTemplates(1);

        };
        $scope.licenseNotification=function(){
            SweetAlert.swal({title:"Notification",text:"Asset used on the content has passed the License period.Please go to asset management and update asset/license information.",showCancelButton: false});
        };

    }]);


app.controller('ModalInstanceTemplateCtrl', function ($scope, templateService, sessionService, $modalInstance, modalFormText, modalFormTile, templateRec, project_id, project_permissions, project_theme, $window, $sce, $timeout, SweetAlert) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.project = {'project_permissions': project_permissions};
    $scope.project_theme = project_theme;
    $scope.s3_file_path='';
    if (angular.isDefined(templateRec.object) && angular.isDefined(templateRec.object.id)) {
        //Preview template
        $scope.showedit = templateRec.object.id;
        $scope.object_details = templateRec;
        $scope.id = templateRec.object.id;
        $scope.title = templateRec.object.name;
        $scope.content = templateRec.object.description;
        $scope.theme_location = templateRec.object.theme_location;
        $scope.file_path = templateRec.object.file_path;
        $scope.file_path = $sce.trustAsResourceUrl($scope.file_path);
        $scope.s3_file_path = templateRec.s3_file_path;
        //console.log($scope.s3_file_path);
        $scope.getActualPath = templateRec.object.file_name;
        $scope.errorMessage = '';
        setTimeout(function () {
            setContentIntoIframe();
        }, 500);
    } else {
        $scope.showedit = 0;
    }

    $scope.add = function () {
        $scope.formError = {'title': ''};
        //if (angular.isUndefined($scope.template_title) == false && angular.isUndefined($scope.template_template) == false) {
        templateService.saveTemplate($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                var templateData = savedResponse.data[0];
                $modalInstance.close();
                
                 $timeout(function () {
                    $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + templateData.project_id + '&node_id=' + templateData.id + '&node_title=' + templateData.title + '&node_type_name=&node_type_id=&project_type_id=' + templateData.project_type_id + '&ref=TEMPLATE&template_id=' + templateData.id + '&redirect_project_id=' + $scope.project_id+'&content_from=local_template';
                }, 1000, false);
            
                
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }

            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'title': '', 'template': ''};
        //if (angular.isUndefined($scope.title) == false && angular.isUndefined($scope.template) == false) {
        templateService.editTemplate($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                SweetAlert.swal("Success", savedResponse.message, "success");
                $modalInstance.close();     
                /*templateService.getTemplate($scope).then(function (data) {
                    $scope.templateList = data.data;                    
                });*/
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
                if (angular.isDefined(savedResponse.error.template)) {
                    $scope.formError['template'] = savedResponse.error.template['0'];
                }

            }


        });
        //}
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    //Manage template_classes    
    $scope.addClass = function () {
        $scope.template_classes.push({value: ''});
    };
    $scope.removeClass = function (index) {
        // console.log(index);
        $scope.template_classes.splice(index, 1);
    };
    //

});



app.controller('global_template', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'templateService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', '$window', function ($scope, $http, sessionService, $location, objectService, templateService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter, $window) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.user_id = userinfo.id;
                                                                                                                                                                                                                                                                             
        $scope.currentOrderByField = 'title';
        $scope.reverseSort = false;


        $scope.project_id = '';

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.template_local_global = {};
        $scope.template_local_global.local = false;
        $scope.template_local_global.global = false;
        
        if($scope.user_permissions['global_template.show.all'].grant) {
            $scope.template_local_global.global = true;
        }

        $scope.pageNumber = 1;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.listTemplates = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            templateService.getTemplate($scope).then(function (data) {
                $scope.templateList = data.data;
                $scope.totalTemplates = data.totalTemplates;
            });
        };

        $scope.listTemplates(1);

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerTemplate = function (newPage) {
            $scope.listTemplates(newPage);
        };
        
        $scope.viewTemplate = function (size, id) {
            var templateRec = {};
            $scope.template_id = id;
            templateService.getTemplateDetail($scope).then(function (data) {
                templateRec = data.data;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/template/templateModalContentView.html',
                    controller: 'ModalInstanceTemplateCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        templateRec: function () {
                            return templateRec;
                        },
                        modalFormText: function () {
                            return "You are trying to view a template";
                        },
                        modalFormTile: function () {
                            return "Preview Template";
                        },
                        project_id: function () {
                            return 0;
                        },
                        project_permissions: function () {
                            return '';
                        },
                        project_theme: function () {
                            return '';
                        }
                    }
                });

                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.deleteGlobalTemplate = function (id, project_id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this template!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    templateService.delTemplate(id, project_id).then(function () {
                        $scope.id = '';
                        $scope.listTemplates(1);
                    });
                    SweetAlert.swal("Success", "Template has been deleted", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Template is safe :)", "error");
                }
            });

        };

        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        };
        
        $scope.changeStatus = function (id, project_id) {
            templateService.changeStatus(id, $scope).then(function () {
                $scope.id = '';
            });
        };

        $scope.gotoEditor = function (template_id) {
            $scope.template_id = template_id;
            templateService.getTemplateDetail($scope).then(function (data) {
                if (data.status == '200') {
                    var templateData = data.data[0];
                    $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + templateData.project_id + '&node_id=' + templateData.id + '&node_title=' + templateData.title + '&node_type_name=&node_type_id=&project_type_id=' + templateData.project_type_id + '&ref=TEMPLATE&template_id=' + templateData.id + '&redirect_project_id=' + $scope.project_id+'&content_from=global_repo_template';
                }
            });

        };

        $scope.apply_filter = function () {
            if ($scope.template_local_global.global == false && $scope.template_local_global.local == false) {
                //$scope.template_local_global.local = true;
                //$scope.template_local_global.global = true;
            }
            $scope.listTemplates(1);
        };

        $scope.changeAccess = function (id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "Want to change Template access!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    templateService.makeGlobal(id, $scope).then(function (data) {
                        $scope.listTemplates($scope.currentPage);
                    });
                    SweetAlert.swal("Success", "Template access changed successfully.", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Template is safe :)", "error");
                }
            });
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.listTemplates(1);
        };

        $scope.$on('apply-advaced-search', function (event, args) {
            $scope.listTemplates(1);
        });

        $scope.$on('clear-advaced-search', function (event, args) {
            $scope.listTemplates(1);
        });

        $scope.$on('apply-search', function (event, args) {
            $scope.listTemplates(1);
        });
        /*
         * @author Meghamala Mukherjee
         * @dated 7-9-2017
         * @description add template to glabal repository
         */
         $scope.addGlobalTemplateForm=function(size){
            var modalInstance = $modal.open({
                templateUrl: 'templates/template/myModalContent.html',
                controller: 'ModalInstanceGlobalTemplateCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    templateRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a new template";
                    },
                    modalFormTile: function () {
                        return "Add Template";
                    }                    
                }
            });
            modalInstance.result.then(function () {
                //Saving TOC    
                $scope.template_id = '';
                templateService.getTemplate($scope).then(function (data) {
                    $scope.templateList = data.data;
                    $scope.totalTemplates = data.totalTemplates;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });            
         };
         
         /*
         * @author Koushik Samanta
         * @dated 06 Oct 2017
         * @description Edit template to glabal repository
         */
         $scope.editGlobalTemplateForm=function(size, template){
            var modalInstance = $modal.open({
                templateUrl: 'templates/template/edit_global_template.html',
                controller: 'ModalInstanceGlobalTemplateCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    templateRec: function () {
                        return template;
                    },
                    modalFormText: function () {
                        return "You are trying to add a new template";
                    },
                    modalFormTile: function () {
                        return "Add Template";
                    }                    
                }
            });
            modalInstance.result.then(function () {
                //Saving TOC    
                $scope.template_id = '';
                templateService.getTemplate($scope).then(function (data) {
                    $scope.templateList = data.data;
                    $scope.totalTemplates = data.totalTemplates;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });            
         };
         
         
         //Go to Desired Templete editor
    $scope.goToTemplateEditor = function(templateData){        
        $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id="+templateData.project_id + '&node_id=' + templateData.id + '&node_title=' + encodeURIComponent(templateData.title) + '&node_type_name=&node_type_id=&project_type_id=' + templateData.project_type_id + '&ref=TEMPLATE&template_id=' + templateData.id + '&redirect_project_id=&is_global=1'+'&content_from=global_repo_template';
        }

    }]);
/*
 * @author Meghamala Mukherjee
 * @dated 7-9-2017
 * @description add Global Template Modal Controller
 */
app.controller('ModalInstanceGlobalTemplateCtrl', function ($scope, templateService, sessionService, $modalInstance, modalFormText, modalFormTile, templateRec, $window, $sce, $timeout, SweetAlert) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    console.log(templateRec);
    $scope.title='';
    if (angular.isDefined(templateRec.id) && templateRec.id!='') {
        $scope.showedit = templateRec.id;
        //$scope.object_details = templateRec;
        $scope.id = templateRec.id;
        $scope.title = templateRec.title;
        $scope.errorMessage = '';
        $scope.project_type_id=templateRec.project_type_id;
        $scope.project_id = templateRec.project_id;
    } else {
        $scope.showedit = 0;
    }
    

    $scope.add = function () {
        $scope.formError = {'title': ''};
        $scope.project_type_id=5;
        $scope.is_global=1;
        //if (angular.isUndefined($scope.template_title) == false && angular.isUndefined($scope.template_template) == false) {
        templateService.saveGlobalTemplate($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                SweetAlert.swal("Success", savedResponse.message, "success");
                //console.log(savedResponse);
                var templateData = savedResponse.data[0];
                //console.log(templateData);
                $modalInstance.close();                
                /*$timeout(function () {
                    $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id="+templateData.project_id + '&node_id=' + templateData.id + '&node_title=' + templateData.title + '&node_type_name=&node_type_id=&project_type_id=' + templateData.project_type_id + '&ref=TEMPLATE&template_id=' + templateData.id + '&redirect_project_id=&is_global=1'+'&content_from=global_repo_template';
                }, 1000, false);*/
            
                
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'title': '', 'template': ''};
        //if (angular.isUndefined($scope.title) == false && angular.isUndefined($scope.template) == false) {
        templateService.editGlobalTemplate($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
               $modalInstance.close(); 
               SweetAlert.swal("Success", savedResponse.message, "success");
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }
        });
        //}
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    //Manage template_classes    
    $scope.addClass = function () {
        $scope.template_classes.push({value: ''});
    };
    $scope.removeClass = function (index) {
        // console.log(index);
        $scope.template_classes.splice(index, 1);
    };    

});