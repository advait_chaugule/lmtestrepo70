/*****
 * Specific services object type =  project
 * Author - Alamgir Hossain Sk  
 */
'use strict';
var returnTemplates = '';
app.service('templateService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        /*
         * Service to get templates         
         */
        this.getTemplate = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.id)) {
                queryString += "&id=" + $scope.id;
            }
            if (angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }
            if (angular.isDefined($scope.template_status)) {
                queryString += '&status=' + $scope.template_status;
            }
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }
            if (angular.isDefined($scope.on_use)) {
                queryString += '&on_use=' + $scope.on_use;
            }

            if (angular.isDefined($scope.searchModel) && $scope.searchModel.search_text) {
                queryString += '&search_text=' + window.btoa($scope.searchModel.search_text);
            }

            if (angular.isDefined($scope.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }
            queryString += '&template_local_global=' + (angular.isDefined($scope.template_local_global) ? JSON.stringify($scope.template_local_global) : '{"local":' + $scope.template_local_global.local + ',"global":' + $scope.template_local_global.global + '}');

            //'{"local":false,"global":true}'
            //$scope.template_local_global.local
            //console.info('Hello');
            //console.info(serviceEndPoint + 'api/v1/project-template?access_token=' + sessionService.get('access_token') + queryString);

            $http.get(serviceEndPoint + 'api/v1/project-template?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                //console.log(data);
                returnTemplates = data
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {

                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to get template

        /*
         * Service to create template         
         */
        this.saveTemplate = function ($scope, $rootScope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-template/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                title: $scope.title
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /*
         * @author Meghamala Mukherjee
         * @description save global template
         * @dated 7-9-2017
         * @param {type} $scope
         * @param {type} $rootScope
         * @returns {$q@call;defer.promise}
         */
        this.saveGlobalTemplate = function ($scope, $rootScope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-template/global_template?access_token=' + sessionService.get('access_token'), {
                project_type_id: $scope.project_type_id,
                title: $scope.title
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /**
         * Service to edit template         
         */
        this.editTemplate = function ($scope, $rootScope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-template/template_id/' + $scope.id + '/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                project_id: $scope.project_id,
                title: $scope.title,
                template: $scope.template
                        //template_classes: angular.toJson($scope.template_classes)
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }        
        /**
         * Service to edit Global template         
         */
        this.editGlobalTemplate = function ($scope, $rootScope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-template-global/template_id/' + $scope.id + '/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                project_id: $scope.project_id,
                title: $scope.title
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        
        /*
         * Delete Pattern         
         */
        this.delTemplate = function (template_id, project_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/project-template/template_id/' + template_id + '/project_id/' + project_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (template_id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-template-activate-inactivate/template_id/' + template_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.validateTemplate = function ($scope, $rootScope) {
            console.log($scope);
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.title)) {
                queryString += "&title=" + $scope.title;
            }
            if (angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }

            $http.get(serviceEndPoint + 'api/v1/project-template-validate?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        this.getTemplateDetail = function ($scope, $rootScope) {
            var deferred = $q.defer();
            var queryString = '';

            $http.get(serviceEndPoint + 'api/v1/project-template/' + $scope.template_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                //console.log(data);
                returnTemplates = data
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.makeGlobal = function (ids, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/template-change-access/template_id/' + ids + '?access_token=' + sessionService.get('access_token'), {
                template_id: ids,
                project_id: $scope.project_id,
                action: 'GLOBAL'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);

