/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';

app.service('assetUsageService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        //Start - Method to get user count
        this.getUserCount = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-users-count/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        //Method to get asset count        
        this.getAssetCount = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-assets-count/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        //Method to get widget count        
        this.getWidgetCount = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-widgets-count/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        //Method to get pattern count        
        this.getPatternCount = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-patterns-count/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        //Method to get ticket count        
        this.getTicketCount = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-tickets-count/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        //Method to get all tickets        
        this.getProjectTickets = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            params.pageNumber = $scope.pageNumber;
            params.itemsPerPage = $scope.itemsPerPage;

            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-tickets/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        //Method to get all widgets        
        this.getProjectWidgets = function ($scope) {

            var params = {};
            params.access_token = sessionService.get('access_token');
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
            }
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-widgets/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

    }]);


