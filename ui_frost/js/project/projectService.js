/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';

app.service('projectService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        //Start - Method to get workflow
        this.workflow = function () {
            var params = {};
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/getworkflowlist',
                params: params,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        //End - Method to get workflow

        this.saveproject = function(formdata){
            var params = formdata;
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/project/create',
                data: params,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
            
        }

        this.projectTaxonomyMapping = function(formdata){
            var params = formdata;
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: serviceEndPoint + '/api/v1/project/taxanomyMapping',
                data: params,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
            
        }

        this.getprojectbyId = function(id){
            var params = {};
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project/'+id,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.updateproject = function(formdata){
            var params = formdata;
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/project/taxanomyMapping',
                data: params,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
            
        }

        this.savefile = function(filemodel){
            var deferred = $q.defer();
            var fd = new FormData();
            fd.append('case_json', filemodel);
            $http({
                method: 'POST',
                url: serviceEndPoint+ 'api/v1/importCFPackageJson/',
                headers: {
                    'Content-Type': undefined,
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
                data: fd,
                transformRequest: angular.identity
            })
            .success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                //$rootScope.unauthorised_redirection(data.status);
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        this.editProjectDetails = function(formdata, project_id) {
            var params = formdata;
            var deferred = $q.defer();
            var url = serviceEndPoint + 'api/v1/project/edit/' + project_id;
            var configOptions = 
            {
                'headers': {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            $http.put(url, params, configOptions)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    //deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
                });
            return deferred.promise;

            //params.access_token = sessionService.get('access_token');

            // $http({
            //     method: 'POST',
            //     url: serviceEndPoint + 'api/v1/project/edit/' + project_id,
            //     data: params,
            //     headers:  {
            //         'Authorization': sessionService.get('access_token'),
            //         'Accept': 'application/json'
            //     },
            // }).success(function (data, status, headers, config) {
            //     //console.log(data);
            //     deferred.resolve(data);//Return the project list
            // }).error(function (data, status, headers, config) {
            //     $rootScope.unauthorised_redirection(data.status);
            // });
            // return deferred.promise;
            
        }



    }]);

