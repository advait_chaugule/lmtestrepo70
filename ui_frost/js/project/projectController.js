/*****
 * Controller for project
 */

'use strict';
app.controller('createproject', ['$scope', '$http', 'sessionService', '$location', 'objectService',
    'projectService', '$rootScope', '$routeParams', '$filter', 'userService', 'aclService', 'limitToFilter',
    '$timeout', 'SweetAlert', '$modal', '$q', '$controller', 'taxonomyService', '$window',
    function ($scope, $http, sessionService, $location,
        objectService, projectService, $rootScope, $routeParams, $filter, userService, aclService, limitToFilter,
        $timeout, SweetAlert, $modal, $q, $controller, taxonomyService, $window) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.showMsg = 0;
        $scope.hideBackToDashboard = 'true';

        //$scope.$flow.defaults.repoDir = 'test';     
        $scope.response = 0;
        $scope.project_name = "";
        $scope.wflow = "";
        $scope.description = "";
        $scope.submitted = false;
        $scope.selectedDraftTaxonomy = "";
        $scope.selectedNode = "";
        $scope.taxonomy = {
            'children': []
        };
        $scope.selectedTaxonmyIds = [];
        $scope.$watch('leftPaneHeight()', function (val) {
            $scope.tempVal = val;

            $timeout(function () {
                $scope.rightPaneLinkHeight = $scope.tempVal;
            }, 3000, false);

        });
        $scope.leftPaneHeight = function () {
            return jQuery(".toc-panel-left").css("height");
        }

        $scope.clickedOnce = false;

        projectService.workflow().then(function (data) {
            $scope.workflow = data.data;
            //selectedKey = $scope.arrayIterate($scope.getworkflowlist, 4);
            //$scope.formdata.project_type = $scope.project_types[selectedKey];
        });
        taxonomyService.listTaxonomies().then(function (result) {
            $scope.draftTaxonomy = result.data;
        });
        $scope.saveProject = function (formdata, page, callpage) {
            $scope.clickedOnce = true;
            $scope.submitted = true;
            $scope.errorMessage = '';
            $scope.error = [];
            if (formdata.$valid == true && $scope.selectedTaxonmyIds.length > 0) {
                var param_data = {
                    project_name: $scope.project_name,
                    workflow_id: $scope.wflow.workflow_id,
                    description: $scope.description,
                };
                projectService.saveproject(param_data).then(function (data) {
                    if (data.status == 201) {
                        if ($scope.selectedTaxonmyIds.length > 0) {
                            var body = {
                                project_id: data.data['project_id'],
                                project_name: data.data['project_name'],
                                description: $scope.description,
                                workflow_id: $scope.wflow.workflow_id,
                                is_active: data.data['is_active'],
                                workflow_title: '',
                                created_at: data.data['created_at'],
                                updated_at: data.data['updated_at'],
                                taxonomy_id: $scope.selectedTaxonmyIds.join(',')
                            };
                            projectService.projectTaxonomyMapping(body).then(function (data) {
                                if (data.status == 200) {
                                    $rootScope.show_dashboard_success_msg = 1;
                                    $scope.redirpage = page;
                                    if (callpage == 'mainpage') {
                                        // $scope.redirectPage();
                                        setTimeout(function () {
                                            swal({
                                                title: "Project Created",
                                                text: "A New Project Created Succesfully!",
                                                type: "success"
                                            }, function () {
                                                $window.location.href = '#/taxonomy-authoring/' + body.project_id;
                                                //$location.path('taxonomy-authoring/'+id);
                                            });
                                        });
                                        // $location.path('manage-taxonomy/'+body.taxonomy_id);
                                    }
                                }
                            });
                        } else {
                            $rootScope.show_dashboard_success_msg = 1;
                            $scope.redirpage = page;
                            if (callpage == 'mainpage') {
                                $scope.redirectPage();
                            }
                        }


                    }
                });
            } else {
                $scope.clickedOnce = false;
                if (formdata.project_name && formdata.project_name.$error.required) {
                    $scope.error.project_name = 'This field is required.';
                }
                if (formdata.wflow && formdata.wflow.$error.required) {
                    $scope.error.wflow = 'This field is required.';
                }
                if (formdata.selectedDraftTaxonomy && formdata.selectedDraftTaxonomy.$error.required) {
                    $scope.error.selectedDraftTaxonomy = 'This field is required.';
                }

                if ($scope.selectedTaxonmyIds.length === 0) {
                    $scope.error.selectedNode = 'Please select aleast one node.';
                }
               var scrollToPos=$(document).find('.text-danger:first').offset().top + $('header').height();
               $('html, body').animate({
                   scrollTop:scrollToPos
               }, 600)
            }
        };
        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        }

        $scope.onTaxonomySelected = function (event) {
            $scope.taxonomy = {
                'children': []
            };
            $scope.selectedTaxonmyIds = [];
            if ($scope.selectedDraftTaxonomy) {
                // $scope.selectedTaxonmyIds = $scope.selectedDraftTaxonomy.id;
                $scope.getTaxonomy($scope.selectedDraftTaxonomy.id);
            }
        }

        $scope.getTaxonomy = function (taxonomy_id, cfItemID, data_message) {
            taxonomyService.getTaxonomy(taxonomy_id).then(function (data) {
                if (data.data == null || data.data == '') {
                    window.location.href = '#/dashboard';
                } else {
                    $scope.taxonomy = data.data;
                    // $scope.getId = data.data.id;
                    $scope.total = data.count;
                    $scope.message = data_message;
                    if ($scope.message != 'Data updated successfully.' && $scope.message != 'CFItem created successfully.') {
                        setTimeout(function () {
                            angular.element(".lirow").eq(0).addClass('active');
                            angular.element('.link-to-preview').triggerHandler('click');
                            $(".toc-panel-container").animate({
                                scrollTop: 0
                            }, 600);
                        }, 300);
                    } else if ($scope.message == 'CFItem created successfully.') {
                        setTimeout(function () {
                            var taxonomyNodes = $('.lirow');
                            angular.element(taxonomyNodes).removeClass('active');
                            angular.element('#' + cfItemID).addClass('active');
                            angular.element('#' + cfItemID).find('span.link-to-preview').triggerHandler('click');
                            var scrollToPos = $('#' + cfItemID).find('span.link-to-preview').offset().top + $(".toc-panel-container").scrollTop() - $('.toc-panel-container').offset().top - 20;
                            $(".toc-panel-container").animate({
                                scrollTop: scrollToPos
                            }, 600);
                        }, 300);
                    } else if ($scope.message == 'Data updated successfully.') {
                        setTimeout(function () {
                            angular.element('#' + cfItemID).addClass('active');
                            angular.element('#' + cfItemID).find('span.link-to-preview').triggerHandler('click');
                        }, 300)

                    }


                }

            });
        };
        //Used to minimize and maximize to Taxonomy
        $scope.toggleMinimized = function (child) {
           
            child.minimized = !child.minimized;
        };
        $scope.openTaxonomyEditForm = function (size, cf_id, cf_entity_type, parent_id) {
            // var taxonomyNodes = $('.taxonomy_node');
            // $(taxonomyNodes).click(function () {
            //     var className=$(this).find('.cf_title').attr('class');
            //     if(className.indexOf('active')>-1){
            //          $(this).find('.cf_title ').removeClass('active');
            //     }
            //     else{
            //         $(this).find('.cf_title').addClass('active');
            //     }
            // });
            if ($scope.selectedDraftTaxonomy && $scope.selectedDraftTaxonomy.id === cf_id) {
                return;
            }
            $scope.error.selectedNode = '';
            console.log(' openTaxonomyEditForm  ', cf_id);
            // check if alredy selected 
            var index = $scope.selectedTaxonmyIds.indexOf(cf_id);
            if (index > -1) {
                // already selected remove it
                $scope.selectedTaxonmyIds.splice(index, 1);
            } else {
                // add it to selected list
                $scope.selectedTaxonmyIds.push(cf_id);
            }
            console.log('openTaxonomyEditForm  ', $scope.selectedTaxonmyIds.join());
        }
        // $scope.checkNodes = function () {
        //     if ($scope.selectedAllNodes) {
        //         $scope.selectedAllNodes = false;
        //     } else {
        //         $scope.selectedAllNodes = true;
        //     }
        // };

    }
]);


app.controller('editProject', ['$scope', '$http', 'sessionService', '$location', 'objectService',
    'projectService', '$rootScope', '$routeParams', '$filter', 'userService', 'aclService', 'limitToFilter',
    '$timeout', 'SweetAlert', '$modal', '$q', '$controller', '$window',
    function ($scope, $http, sessionService, $location,
        objectService, projectService, $rootScope, $routeParams, $filter, userService, aclService, limitToFilter,
        $timeout, SweetAlert, $modal, $q, $controller, $window) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.showMsg = 0;
        $scope.hideBackToDashboard = 'true';

        //$scope.$flow.defaults.repoDir = 'test';     
        $scope.response = 0;
        $scope.project_name = "";
        $scope.wflow = "";
        $scope.description = "";
        $scope.submitted = false;

        $scope.$watch('leftPaneHeight()', function (val) {
            $scope.tempVal = val;

            $timeout(function () {
                $scope.rightPaneLinkHeight = $scope.tempVal;
            }, 3000, false);

        });
        $scope.leftPaneHeight = function () {
            return jQuery(".toc-panel-left").css("height");
        }

        $scope.clickedOnce = false;
        var selectedKey = '';
        $scope.arrayIterateobj = function (lists, id) {
            angular.forEach(lists, function (value, key) {
                if (value.workflow_id == id) {
                    selectedKey = key;
                }
            });
            return selectedKey;
        }

        projectService.workflow().then(function (data) {
            $scope.workflow = data.data;
            //selectedKey = $scope.arrayIterate($scope.getworkflowlist, 4);
            //$scope.formdata.project_type = $scope.project_types[selectedKey];

            //get project details and set ui-binded inputs
            projectService.getprojectbyId($routeParams.project_id).then(function (res) {
                $scope.project_name = res.data.project_name;
                $scope.description = res.data.description;
                $scope.wflow = res.data.workflow_id;
                var selectedworkflow = $scope.arrayIterateobj($scope.workflow, res.data.workflow_id);
                $scope.wflow = $scope.workflow[selectedworkflow];
            });
        });

        $scope.updateProjectDetails = function (formdata) {
            $scope.clickedOnce = true;
            $scope.submitted = true;
            $scope.errorMessage = '';
            $scope.error = [];
            if (formdata.$valid == true) {
                var param_data = {
                    project_name: $scope.project_name,
                    workflow_id: $scope.wflow.workflow_id,
                    description: $scope.description
                };
                projectService.editProjectDetails(param_data, $routeParams.project_id).then(function (data) {
                    if (data.status == 200) {
                        // $timeout(function () {
                        //     swal();
                        // }, function(){
                        //     $window.location.href = "#/dashboard";
                        //     console.log("edit project succesfully");
                        // });
                        setTimeout(function () {
                            swal({
                                    title: "Success",
                                    text: "Project Edited Successfully",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        $window.location.href = '#/dashboard';
                                        // $location.path("taxonomies/");
                                    }
                                });
                        }, 1);
                    } else {
                        swal("Some error occured.");
                    }
                });
            } else {
                $scope.clickedOnce = false;
                if (formdata.project_name && formdata.project_name.$error.required) {
                    $scope.error.project_name = 'This field is required.';
                }
                if (formdata.wflow && formdata.wflow.$error.required) {
                    $scope.error.wflow = 'This field is required.';
                }
                if (formdata.selectedDraftTaxonomy && formdata.selectedDraftTaxonomy.$error.required) {
                    $scope.error.selectedDraftTaxonomy = 'This field is required.';
                }
            }

        };
        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        }
    }    
]);