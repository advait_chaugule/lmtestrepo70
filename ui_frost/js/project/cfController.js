/*****
 * Controller for CF document
 *  
 */
'use strict';
app.controller('createcfdocController', ['$scope', '$http', 'sessionService', '$location', 
 'projectService','cfService', '$rootScope', '$routeParams', '$filter', 'userService', 'aclService', 'limitToFilter',
  '$timeout', 'SweetAlert', '$modal', '$q', '$controller', function ($scope, $http, sessionService, $location, 
    projectService,cfService, $rootScope, $routeParams, $filter, userService, aclService, limitToFilter, 
    $timeout, SweetAlert, $modal, $q, $controller) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.showMsg = 0;
        $scope.hideBackToDashboard = 'true';
        $rootScope.show_projectsetup_success_msg=0;

        $scope.createcfdocform={};
        $scope.submitted = false;

        // get master data
        cfService.getCFMasterData().then(function(res){
            var masterdata = res.data;
            $scope.language = masterdata["language"];
            $scope.adoption_status = masterdata["adoption_status"];
            $scope.publisher = masterdata["publisher"];
            $scope.organization = masterdata["organization"];
            $scope.getdata();
        });

        //initialize static values
        //$scope.language = [{"id":"42ef69f9-8b02-43ee-85e2-83a5de6998b9", "name":"English", "short_code":"en"}];
        // $scope.adoption_status = [{"id":"private_draft", "value":"Private Draft"},
        // {"id":"draft", "value":"Draft"},
        // {"id":"adopted", "value":"Adopted"},
        // {"id":"deprecated", "value":"deprecated"}];
        // $scope.publisher = [{"id":"a6d6e9a1-d4dc-49de-ae3c-b29858aa5518","name":"Publisher 1"}];
        // $scope.organization = [{"id":"933680a5-d116-4577-bb5b-4fc4ace437cb","name":"Organization 1"}];


        $scope.$watch('leftPaneHeight()', function (val) {
            $scope.tempVal = val;

            $timeout(function () {
                $scope.rightPaneLinkHeight = $scope.tempVal;
            }, 3000, false);

        });
        $scope.leftPaneHeight = function () {
            return jQuery(".toc-panel-left").css("height");
        }

        $scope.clickedOnce = false;
        $scope.taxonomy_edit = false;
        $scope.taxonomy_id = "";

        var selectedKey = '';
        $scope.arrayIterateobj = function (lists, id) {
            angular.forEach(lists, function (value, key) {
                if (value.id == id) {
                    selectedKey = key;
                }
            });
            return selectedKey;
        }


        //get project details
        $scope.getdata = function(){
            projectService.getprojectbyId($routeParams.project_id).then(function(res){
                $scope.projectdet = res.data;
                $scope.buttontxt = "Create Taxonomy";
                $scope.taxonomy_id = "";
                $scope.button_managetaxonomy_txt ="";
                if (res.data.taxonomy_id !=null){
                    $scope.taxonomy_edit = true;
                    $scope.taxonomy_id = res.data.taxonomy_id;
                    $scope.buttontxt = "Save CFDOC"
                    $scope.button_managetaxonomy_txt = "Manage Taxonomy"
                    //fetch taxonomy details
                    cfService.getcfdocbyId($scope.taxonomy_id).then(function(res){
                        $scope.taxonomy_data = res.data;
                        var cfdata = res.data;
                        //load taxonomy data
                        //console.log(cfdata);
                        $scope.createcfdocform.cfdoc_title = cfdata.title;
                        $scope.createcfdocform.cfdoc_creator = cfdata.creator;
                        
                        $scope.createcfdocform.cfdoc_official_source_url = cfdata.official_source_url;
                        $scope.createcfdocform.cfdoc_url_name = cfdata.url_name;
                        $scope.createcfdocform.cfdoc_version = cfdata.version;
                        $scope.createcfdocform.cfdoc_description = cfdata.description;
                        $scope.createcfdocform.cfdoc_note = cfdata.note;
                        $scope.createcfdocform.cfdoc_status_start_date = cfdata.status_start_date;
                        $scope.createcfdocform.cfdoc_status_end_date = cfdata.status_end_date;
                        //set organization
                        var selectedorg = $scope.arrayIterateobj($scope.organization,cfdata.organization_id)
                        $scope.createcfdocform.cfdoc_organization_id = $scope.organization[selectedorg];
                        //publisher
                        var selectedpub = $scope.arrayIterateobj($scope.publisher,cfdata.publisher_id)
                        $scope.createcfdocform.cfdoc_publisher_id = $scope.publisher[selectedpub];
                        //language
                        var selectedlang = $scope.arrayIterateobj($scope.language,cfdata.language_id)
                        $scope.createcfdocform.cfdoc_language_id = $scope.language[selectedlang];
                        //
                        var selecteadoptionstatus = $scope.arrayIterateobj($scope.adoption_status,cfdata.adoption_status)
                        $scope.createcfdocform.cfdoc_adoption_status = $scope.adoption_status[selecteadoptionstatus];


                    });

                }
            });
        }

        $scope.redirect_to = function(taxonomy_id){
            $location.path('manage-taxonomy/'+taxonomy_id);
        }
        $scope.savecfdoc= function (formdata) {
            $scope.clickedOnce = true;
            $scope.submitted = true;
            $scope.errorMessage = '';
            $scope.error = [];
            if (formdata.$valid == true) {
                var temp = $scope.createcfdocform;
                var param_data = {};

                if(!angular.isUndefined($scope.createcfdocform.cfdoc_title)){
                    param_data['title'] = $scope.createcfdocform['cfdoc_title'];
                }
                if($scope.createcfdocform.hasOwnProperty('cfdoc_creator')){
                    param_data['creator'] = $scope.createcfdocform['cfdoc_creator'];
                }
                if(!angular.isUndefined($scope.createcfdocform.cfdoc_organization_id) && $scope.createcfdocform.cfdoc_organization_id != null ){
                    param_data['organization_id'] = $scope.createcfdocform['cfdoc_organization_id']['id'];
                }
                if($scope.createcfdocform.hasOwnProperty('cfdoc_official_source_url')){
                    param_data['official_source_url'] = $scope.createcfdocform['cfdoc_official_source_url'];
                }
                if(!angular.isUndefined($scope.createcfdocform.cfdoc_publisher_id) && $scope.createcfdocform.cfdoc_publisher_id != null){
                    param_data['publisher_id'] = $scope.createcfdocform['cfdoc_publisher_id']['id'];
                }
                if($scope.createcfdocform.hasOwnProperty('cfdoc_url_name')){
                    param_data['url_name'] = $scope.createcfdocform['cfdoc_url_name'];
                }
                if($scope.createcfdocform.hasOwnProperty('cfdoc_version')){
                    param_data['version'] = $scope.createcfdocform['cfdoc_version'];
                }
                if($scope.createcfdocform.hasOwnProperty('cfdoc_description')){
                    param_data['description'] = $scope.createcfdocform['cfdoc_description'];
                }
                if(!angular.isUndefined($scope.createcfdocform.cfdoc_note)){
                    param_data['note'] = $scope.createcfdocform['cfdoc_note'];
                }
                if($scope.createcfdocform.cfdoc_status_start_date){
                    if ($scope.createcfdocform.cfdoc_status_start_date instanceof Date){
                        param_data['status_start_date'] = $scope.createcfdocform['cfdoc_status_start_date'].toISOString().slice(0, 19).replace('T', ' ');
                    }
                    // else{
                    //     param_data['status_start_date'] = Date.parse( $scope.createcfdocform['cfdoc_status_start_date']).toISOString().slice(0, 19).replace('T', ' ');
                    // }
                    
                }

                if($scope.createcfdocform.cfdoc_status_end_date){
                    if ($scope.createcfdocform.cfdoc_status_end_date instanceof Date){
                        param_data['status_end_date'] = $scope.createcfdocform['cfdoc_status_end_date'].toISOString().slice(0, 19).replace('T', ' ');
                        console.log(param_data['status_end_date']);
                    }
                    // else{
                    //     param_data['cfdoc_status_end_date'] = Date.parse( $scope.createcfdocform['cfdoc_status_end_date']).toISOString().slice(0, 19).replace('T', ' ');
                    // }
                    
                }
                if(!angular.isUndefined($scope.createcfdocform.cfdoc_language_id) && $scope.createcfdocform.cfdoc_language_id != null){
                    param_data['language_id'] = $scope.createcfdocform['cfdoc_language_id']['id'];
                }
                if(!angular.isUndefined($scope.createcfdocform.cfdoc_adoption_status) && $scope.createcfdocform.cfdoc_adoption_status != null){
                    param_data['adoption_status'] = $scope.createcfdocform['cfdoc_adoption_status']['id'];
                }


                if($scope.taxonomy_edit){
                    param_data['isupdate'] = $scope.taxonomy_edit;
                    param_data['taxonomy_id'] = $scope.taxonomy_id;
                }
                if($scope.taxonomy_edit){
                    cfService.savecfdoc(param_data).then(function(res){
                        if(res.status==200){
                            $rootScope.show_projectsetup_success_msg = 1;
                            var redirpage = "/manage-taxonomy/"+res.data.id;
                            //$location.path(redirpage);
                            swal("CF doc has been updated successfully");
                        }
                    });

                }else{
                    //create new
                    cfService.createcfdoc(param_data).then(function(res){
                        if(res.status==201){
                            //$rootScope.show_dashboard_success_msg = 1;
                            //update project taxonomy mapping
                            var paramdata = {project_id:$scope.projectdet.project_id,
                            taxonomy_id:res.data.id};
                            var redirpage = "/manage-taxonomy/"+res.data.id;
                            $location.path(redirpage);
                            /*projectService.updateproject(paramdata).then(function(res){
                                if(res.status==200){
                                    var redirpage = "/manage-taxonomy/"+res.data.taxonomy_id;
                                    $location.path(redirpage);
                                }
                            });*/
                            
                        }
                    });
                }
                
            }
            else{
                $scope.clickedOnce = false;
                if (formdata.cfdoc_title && formdata.cfdoc_title.$error.required) {
                    $scope.error.cfdoc_title = 'This field is required.';
                }
                if (formdata.cfdoc_creator && formdata.cfdoc_creator.$error.required) {
                    $scope.error.cfdoc_creator = 'This field is required.';
                }
                if (formdata.cfdoc_official_source_url && formdata.cfdoc_official_source_url.$invalid) {
                    $scope.error.cfdoc_official_source_url = 'Invalid URL';
                }         
            }

        };

    }]);