/*****
 * Generic pdfImport Services
 * Author - Alamgir Hossain Sk * 
 */
'use strict';

app.service('pdfImportService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        /**
         * Get pdf list
         */
        this.getPdfList = function ($scope, project_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            $http.get(serviceEndPoint + 'api/v1/imported-pdf-list/'+project_id+'?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);

