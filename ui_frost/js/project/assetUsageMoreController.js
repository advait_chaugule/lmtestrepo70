/*****
 * Controller for project asset usage
 * Author - Alamgir Hossain Sk *
 */

'use strict';
app.controller('asset_usage_more', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'projectService', '$rootScope', '$routeParams', '$filter', 'userService', 'aclService', 'assetUsageService', 'assetService', 'patternService', 'limitToFilter', '$timeout', 'SweetAlert', '$modal', '$q', function ($scope, $http, sessionService, $location, objectService, projectService, $rootScope, $routeParams, $filter, userService, aclService, assetUsageService, assetService, patternService, limitToFilter, $timeout, SweetAlert, $modal, $q) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.project_id = $routeParams.project_id;
        $rootScope.showSearchBox = false;
        $scope.showMoreIcon = false;
        $scope.searchModel = {};
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        var tabElm = angular.element(document.querySelector('[data-target="#' + $routeParams.type + '"]'));
        tabElm.click();

        objectService.getProjectDetails($scope).then(function (data) {
            if (data == null) {
                $location.path("edit_toc/" + $scope.project_id);
            } else if (angular.isObject(data)) {
                $scope.project = data;
            }
        });

        //For asset listing in statistics page
        $scope.advanced_search = {};
        $scope.advanced_search.active = false;
        $scope.asset_filter = {};
        $scope.asset_local_global = {};
        $scope.asset_filter.images = true;
        $scope.asset_filter.videos = true;
        $scope.asset_filter.gadgets = true;
        $scope.asset_filter.other = true;
        $scope.asset_local_global.local = true;
        $scope.asset_local_global.global = false;
        $scope.totalAssets = 0;

        //For assets listing in more page
        assetService.getAssets($scope).then(function (data) {
            $scope.assetList = data.data;
            if ($scope.assetList == null) {
                $scope.assetList = 0;
            }
        });

        //For patterns listing in more page
        patternService.getPattern($scope).then(function (data) {
            $scope.patternList = data.data;
        });

        //For issues listing in more page        
        assetUsageService.getProjectTickets($scope).then(function (data) {
            $scope.ticketList = data.data;
        });

        //For widgets listing in  more page        
        assetUsageService.getProjectWidgets($scope).then(function (data) {
            $scope.widgetList = data.data;
        });

        //For users listing in  more page
        projectService.get_mapapped_users($scope).then(function (data) {
            $scope.userList = data.data;
        });


    }]);
