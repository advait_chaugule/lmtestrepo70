'use strict';

app.controller('projectimport', ['$scope', '$http', 'sessionService', '$location', 'objectService',
    'projectService', '$rootScope', '$routeParams', '$filter', 'userService', 'aclService', 'limitToFilter',
    '$timeout', 'SweetAlert', '$modal', '$q', '$controller', '$window',
    function ($scope, $http, sessionService, $location,
        objectService, projectService, $rootScope, $routeParams, $filter, userService, aclService, limitToFilter,
        $timeout, SweetAlert, $modal, $q, $controller, $window) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.upload_disable = true;

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.$watch('leftPaneHeight()', function (val) {
            $scope.tempVal = val;

            $timeout(function () {
                $scope.rightPaneLinkHeight = $scope.tempVal;
            }, 3000, false);

        });
        $scope.leftPaneHeight = function () {
            return jQuery(".toc-panel-left").css("height");
        }

        $scope.flowOptions = {
            target: 'api/v1/importCFPackageJson/',
            headers: {
                Authorization: sessionService.get('access_token')
            }
        };

        console.log($scope.flowOptions);

        $scope.fileuploadUrl =
            $scope.response = 0;
        $scope.project_name = "";
        $scope.view_create_taxonomy = "CREATE_TAXONOMY";
        $scope.isdropenabled = true;
        $scope.casefile = "";


        $scope.project_setup = function (project_id) {
            $location.path("projectsetup/" + project_id);
        }

        //fetch project name
        projectService.getprojectbyId($routeParams.project_id).then(function (res) {
            $scope.projectdet = res.data;
            $scope.project_name = res.data.project_name;
            $scope.project_id = res.data.project_id;
            if (res.data.taxonomy_id == null) {
                $scope.view_create_taxonomy = "CREATE_TAXONOMY";
                $scope.upload_disable = false;
            } else {
                $scope.upload_disable = true;
                $location.path("projectsetup/" + $scope.project_id);
            }
        });

        $scope.importfile = function (case_json) {
            // $scope.upload_disable = true;
            projectService.savefile(case_json).then(function (res) {
                if (res.status == 200) {
                    setTimeout(function () {
                        swal({
                                title: "Success!",
                                text: res.message,
                                type: "success",
                                confirmButtonText: "OK"
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    $window.location.href = '#/taxonomies';
                                }
                            });
                    }, 0);
                  
                } else if (res.status == 500) {
                    setTimeout(function () {
                        swal({
                                //title: "",
                                text: "CF doc already existed",
                                type: "warning",
                                confirmButtonText: "OK"
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    $window.location.href = '#/taxonomies';
                                    // $location.path("taxonomies/");
                                }
                            });
                    }, 1);

                } else if (res.status == 400) {
                    if (res.message == 'Package already exists.') {
                        setTimeout(function () {
                            swal({
                                    //title: "",
                                    text: "Package already exists.",
                                    type: "warning",
                                    confirmButtonText: "OK"
                                },
                                
                                function (isConfirm) {
                                    if (isConfirm) {
                                        $window.location.href = '#/taxonomies';
                                        // $location.path("taxonomies/");
                                    }
                                });
                        }, 1);
                    } else {
                        setTimeout(function () {
                            swal({
                                    title: "Error",
                                    text: "Please upload a valid JSON file.",
                                    type: "error",
                                    confirmButtonText: "OK"
                                },

                            );
                        }, 1);
                    }


                }
            });
        }

        // flow
        $scope.$watch('$flow.files.length', function (val) {
            if (val == 1) {
                $scope.isdropenabled = false;
            }
            if (val > 1) {
                $scope.$flow.files.pop();
                SweetAlert.swal("Cancelled", "Only one package at a time", "error");
            }
        });


        $scope.upload_cancel = function () {
            $scope.isdropenabled = true;
            $scope.project_status = 3000;
            projectService.change_project_status($scope);
            //jQuery("#customProcess").prop("disabled", false);
        }
        $scope.uploadPackage = function () {
            //jQuery("#customProcess").prop("disabled", true);
            $scope.$flow.defaults.flowRepoDir = $scope.project_id;
            $scope.$flow.opts.query = {};
            $scope.$flow.resume();
        };
        $scope.update_after_import = function (fl, file, result) {
            //$scope.getProject($scope);
            $scope.$flow.cancel();
            //$scope.edit_toc($scope.project_id);
        }


        $scope.$watch('$flow.progress()', function (v) {
            $rootScope.file_timer = $filter('number')(v * 100, 0);
        });




    }
]);