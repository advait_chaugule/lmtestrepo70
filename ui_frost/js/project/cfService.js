/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';

app.service('cfService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        this.createcfdoc = function(formdata){
            var params = formdata;
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/cfdoc/create',
                data: params,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
            
        }

        this.getcfdocbyId = function(id){
            var params = {};
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/cfdoc/'+id,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.savecfdoc = function(formdata){

            var params = formdata;
            var deferred = $q.defer();
            var url = serviceEndPoint + 'api/v1/cfdoc/edit/'+params['taxonomy_id'];
            var config ={'headers': {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            }};
            params["_method"] = "PUT"
            $http.post(url, params,config).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                //deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        }

        this.getCFMasterData = function(){
            var params = {};
            //params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/getCFMasterData',
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        }


    }]);

