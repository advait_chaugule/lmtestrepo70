'use strict';

app.controller('pdfImport', ['$scope', '$http', 'sessionService', '$location', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', 'pdfImportService', 'objectService', function ($scope, $http, sessionService, $location, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter, pdfImportService, objectService) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        var project_id = $routeParams.project_id;
        $scope.project_id = project_id;
        $scope.disableAutoTagging = false;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
//        if (userinfo.id !== 1) {
//            $rootScope.goto('dashboard');
//        }

        // get project details
        $scope.getProject = function ($scope) {
            objectService.getProjectDetails($scope).then(function (data) {
                if (data == null) {
                    $location.path("edit_toc/" + $scope.project_id);
                } else if (angular.isObject(data)) {
                    $scope.project_name = data.name;
                    $scope.project_type_id = data.project_type_id;
                }
            });
        };
        $scope.getProject($scope);

        //Call Widgets        
        $scope.currentPage = 1;
        $scope.pageNumber = '';
        $scope.itemsPerPage = '';
        $scope.pdfImportList = {};
        $scope.getPdfList = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = '10';
            pdfImportService.getPdfList($scope, project_id).then(function (data) {
                if (data.data == null || data.data == '') {
                    $scope.pdfImportList = {};
                    $scope.total = data.total;
                } else {
                    $scope.total = data.total;
                    $scope.pdfImportList = data.data.list;
                }


            });
        };
        $scope.getPdfList($scope.currentPage);
        ///        
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerPdf = function (newPage) {
            $scope.getPdfList(newPage);
        };

        $scope.goToHtmlThumnails = function (id) {
            window.location = clientUrl + '/html_thumbnail/index.html?access_token=' + $scope.access_token + '&project_id=' + project_id + '&import_id=' + id;
        };

        $scope.backToDashboard = function () {
            $location.path('edit_toc/' + project_id);
        };

        //Setting up of FLow to upload multiple PDFs
        $scope.$flow.defaults.flowRepoDir = project_id;
        $scope.$flow.opts.target = 'api/v1/upload-multiple-pdf';
        $scope.$flow.opts.query = {
            project_id: project_id,
            project_type_id: 5,
            pageRange: 'all',
            disableAutoTagging: $scope.disableAutoTagging,
        };

        $scope.addParamCancel = function () {
            $scope.$flow.cancel();
            $scope.disableAutoTagging = false;
        };

        $scope.addParamConfirm = function () {
            jQuery("#uploadPDFParamModal").modal("hide");
            jQuery(".uploading-container").show();
            $scope.$flow.opts.query = {project_type_id: $scope.project_type_id, pageRange: $scope.pageRange, disableAutoTagging: $scope.disableAutoTagging};
            $scope.$flow.resume();
            $scope.disableAutoTagging = false;
        };

        $scope.uploadPackage = function () {
            $scope.pageRange = 'all';
            jQuery(".uploading-container").hide();
            jQuery("#uploadPDFParamModal").modal("show");
            $scope.$flow.defaults.flowRepoDir = project_id;
            // $scope.$flow.opts.query = {project_type_id: 5, pageRange: 'all',autoTagging: $scope.autoTagging};
            // $scope.$flow.resume();
        };

        $scope.upload_cancel = function () {
            $scope.$flow.cancel();
            $scope.disableAutoTagging = false;
        }



        $scope.$watch('$flow.progress()', function (v) {
            $rootScope.file_timer = $filter('number')(v * 100, 0);
        });

        $scope.update_after_import = function (fl, file, result) {
            $scope.getPdfList(1);
            $scope.$flow.cancel();
        };




        // mark multiple pdf 
        $scope.selectedPdf = [];
        $scope.showHtmlThumbnailsBtn = false;
        $scope.onSelectPdf = function (pdfId) {
            var idx = $scope.selectedPdf.indexOf(pdfId);
            // is currently selected
            if (idx > -1) {
                $scope.selectedPdf.splice(idx, 1);
            }
            else { // is newly selected
                $scope.selectedPdf.push(pdfId);
            }
            if ($scope.selectedPdf.length > 1) {
                $scope.showHtmlThumbnailsBtn = true;
            } else {
                $scope.showHtmlThumbnailsBtn = false;
            }
        };

        $scope.goToMergeHtmlThumnails = function () {
            var importIds = $scope.selectedPdf.join(",");
            importIds = encodeURIComponent(importIds);
            window.location = clientUrl + '/html_thumbnail/index.html?access_token=' + $scope.access_token + '&project_id=' + project_id + '&import_id=' + importIds;
        };

    }]);

