/*****
 * Controller for project asset usage
 * Author - Arikh Akher * 
 */

'use strict';
app.controller('asset_usage', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'projectService', '$rootScope', '$routeParams', '$filter', 'userService', 'aclService', 'assetUsageService', 'assetService', 'patternService', 'limitToFilter', '$timeout', 'SweetAlert', '$modal', '$q', 'tocService', function ($scope, $http, sessionService, $location, objectService, projectService, $rootScope, $routeParams, $filter, userService, aclService, assetUsageService, assetService, patternService, limitToFilter, $timeout, SweetAlert, $modal, $q, tocService) {

        $rootScope.chkAccessTokenInt = 1000;
        $scope.access_token = sessionService.get('access_token');
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.loggedid_user = userinfo.id;
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.project_id = $routeParams.project_id;
        $scope.userCount = 0;
        $scope.assetCount = 0;
        $scope.widgetCount = 0;
        $scope.patternCount = 0;
        $scope.ticketCount = 0;
        $rootScope.showSearchBox = false;
        $scope.showMoreIcon = true;
        $scope.searchModel = {};
        //$scope.isdropenabled = true;
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        objectService.getProjectDetails($scope).then(function (data) {
            if (data == null) {
                $location.path("edit_toc/" + $scope.project_id);
            } else if (angular.isObject(data)) {
                $scope.project = data;
            }
        });

        assetUsageService.getUserCount($scope).then(function (data) {
            $scope.userCount = data.data;
            //console.log(data);
        });

        assetUsageService.getAssetCount($scope).then(function (data) {
            $scope.assetCount = data.data;
            //console.log(data);
        });

        assetUsageService.getWidgetCount($scope).then(function (data) {
            $scope.widgetCount = data.data;
            //console.log(data);
        });

        assetUsageService.getPatternCount($scope).then(function (data) {
            $scope.patternCount = data.data;
            //console.log(data);
        });

        assetUsageService.getTicketCount($scope).then(function (data) {
            $scope.ticketCount = data.data;
            //console.log(data.data);
        });

        //For asset listing in statistics page
        $scope.advanced_search = {};
        $scope.advanced_search.active = false;
        $scope.asset_filter = {};
        $scope.asset_local_global = {};
        $scope.asset_filter.images = true;
        $scope.asset_filter.videos = true;
        $scope.asset_filter.gadgets = true;
        $scope.asset_filter.other = true;
        $scope.asset_local_global.local = true;
        $scope.asset_local_global.global = false;
        $scope.totalAssets = 0;
        $scope.pageNumber = 1;
        $scope.itemsPerPage = 6;

        projectService.get_mapapped_users($scope).then(function (data) {
            $scope.userList = data.data;
        });

        assetService.getAssets($scope).then(function (data) {
            $scope.assetList = data.data;
            if ($scope.assetList == null) {
                $scope.assetList = 0;
            }
            //console.log(data.data);
        });

        //For patterns listing in statistics page
        patternService.getPattern($scope).then(function (data) {
            $scope.patternList = data.data;
            //console.log(data);
        });

        //For issues listing in statistics page        
        assetUsageService.getProjectTickets($scope).then(function (data) {
            $scope.ticketList = data.data;
            //console.log(data);
        });

        //For widgets listing in  statistics page        
        assetUsageService.getProjectWidgets($scope).then(function (data) {
            $scope.widgetList = data.data;
            //console.log(data);
        });

        $scope.copy_project = function (pid) {
            $scope.project_id = $routeParams.project_id;
            $scope.user_id = $scope.loggedid_user;
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You are going to copy current project, after success it will appear in the project listing dashboard!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, copy it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    if (angular.isDefined($scope.project_id)) {
                        tocService.copyProject($scope).then(function (result) {
                            if (result.status == 400 && angular.isDefined(result.projectPermissionGranted) && result.projectPermissionGranted == 401) {
                                SweetAlert.swal("Cancelled", result.message, "warning");
                            }
                        });
                    }
                } else {
                    SweetAlert.swal("Cancelled", "Copy is cancelled :)", "error");
                }
            });

            //alert($scope.project_id);

        }


    }]);
