'use strict';
app.config(['$routeProvider', '$httpProvider', 'flowFactoryProvider', 'ivhTreeviewOptionsProvider', function ($routeProvider, $httpProvider, flowFactoryProvider, ivhTreeviewOptionsProvider) {

        //Used for Session handling for Cross Domain
        $httpProvider.defaults.withCredentials = true;
        //$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
        //console.log($httpProvider.defaults.headers);
        //$httpProvider.defaults.headers.common["Authorization"] = 'Bearer 06412c08d81c1966d08f16c11e9d8c81b7574bb3'//+localStorage.getItem('access_token');

        // ACMT Routes
        $routeProvider.when('/', {
            templateUrl: 'templates/login.html',
            //controller: "login"
        });
        $routeProvider.when('/dashboard', {
            templateUrl: 'templates/dashboard.html'
        });
        $routeProvider.when('/createproject', {
            templateUrl: 'templates/project/createproject.html'
        });
        $routeProvider.when('/projectsetup/:project_id', {
            templateUrl: 'templates/project/project_setup.html'
        });
        $routeProvider.when('/taxonomysetup/', {
            templateUrl: 'templates/taxonomy/taxonomy_setup.html'
        });
       /* $routeProvider.when('/project-import/:project_id', {
            templateUrl: 'templates/project/project_import.html'
        });*/
        $routeProvider.when('/project-import/', {
            templateUrl: 'templates/project/project_import.html'
        });
        $routeProvider.when('/taxonomyimport/', {
            templateUrl: 'templates/taxonomy/taxonomy_import.html'
        });
        $routeProvider.when('/logout', {
            templateUrl: 'templates/logout.html'
        });

        $routeProvider.when('/manage-taxonomy/:taxonomy_id', {
            templateUrl: 'templates/taxonomy/list.html',
        });

        $routeProvider.when('/taxonomy-authoring/:taxonomy_id', {
            templateUrl: 'templates/taxonomy/authoring.html',
        });

        $routeProvider.when('/projectedit/:project_id', {
            templateUrl: 'templates/project/project_edit.html',
        });
        
        $routeProvider.when('/settings', {
            templateUrl: 'templates/settings.html'
        });
        
        $routeProvider.when('/allocate_acl/:role_id', {
            templateUrl: 'templates/acl/allocate_acl.html'
        });
              
        $routeProvider.when('/roles', {
            templateUrl: 'templates/acl/role_list.html'
        });
        $routeProvider.when('/user', {
            templateUrl: 'templates/user/user_list.html'
        });
        $routeProvider.when('/taxonomies', {
            templateUrl: 'templates/taxonomy/taxonomies.html',
        });
        //end
        /*
        $routeProvider.when('/editproject/:project_id', {
            templateUrl: 'templates/project/createproject.html'
        });
        
        $routeProvider.when('/project-user/:project_id', {
            templateUrl: 'templates/project/project_user.html'
        });
        $routeProvider.when('/test_harness', {
            templateUrl: 'templates/test_harness.html',
            controller: "Test_harness"
        });
       
        $routeProvider.when('/toc_content', {
            templateUrl: 'templates/toc/toc_content.html'
        });
        $routeProvider.when('/edit_toc/:project_id', {
            templateUrl: 'templates/toc/edit_toc.html'
        });
        $routeProvider.when('/project_import/:project_id', {
            templateUrl: 'templates/project/project_import.html'
        });
        $routeProvider.when('/pdf_import_list/:project_id', {
            templateUrl: 'templates/project/pdf_import_list.html'
        });
        $routeProvider.when('/project_preview/:project_id', {
            templateUrl: 'templates/project_preview/preview.html'
        });
        $routeProvider.when('/project_preview_test/:project_id', {
            templateUrl: 'templates/project_preview/preview_test.html'
        });
        $routeProvider.when('/search_replace/:project_id/:search_text', {
            templateUrl: 'templates/project/search_replace/search_replace.html'
        });
        $routeProvider.when('/object_pattern/list/:object_id', {
            templateUrl: 'templates/object_pattern/list.html',
            controller: "object_pattern"
        });
        $routeProvider.when('/asset', {
            templateUrl: 'templates/asset/list.html'
        });
        $routeProvider.when('/pattern/:project_id', {
            templateUrl: 'templates/pattern/list.html'
        });
        $routeProvider.when('/asset/:project_id', {
            templateUrl: 'templates/asset/list.html'
        });
        $routeProvider.when('/global_repo', {
            templateUrl: 'templates/asset/global_repo.html'
        });
        $routeProvider.when('/global_repo/:global_tab', {
            templateUrl: 'templates/asset/global_repo.html'
        });
        $routeProvider.when('/global-lo-content/:object_id', {
            templateUrl: 'templates/global_repo/global_content_lo.html'
        });
        $routeProvider.when('/forget_passwd', {
            templateUrl: 'templates/user/forget_password.html'
        });
        $routeProvider.when('/resetpassword', {
            templateUrl: 'templates/user/reset_password.html'
        });
        $routeProvider.when('/create_password/:password_token', {
            templateUrl: 'templates/user/create_password.html'
        });
        $routeProvider.when('/profile', {
            templateUrl: 'templates/user/profile.html'
        });
        $routeProvider.when('/glossary', {
            templateUrl: 'templates/glossary/list.html'
        });
        $routeProvider.when('/glossary/:project_id', {
            templateUrl: 'templates/glossary/list.html'
        });
        $routeProvider.when('/glossary_view', {
            templateUrl: 'simple_editor/glossary_view.html'
        });
        $routeProvider.when('/plugin/flow', {
            templateUrl: 'templates/plugin/flow.html'
        });

        $routeProvider.when('/create_user', {
            templateUrl: 'templates/user/create_user.html'
        });
        $routeProvider.when('/allocate_acl_user/:user_id', {
            templateUrl: 'templates/acl/allocate_acl_user.html'
        });

        $routeProvider.when('/exportlist', {
            templateUrl: 'templates/project/exportlist.html'
        });

        $routeProvider.when('/exportlist/:project_id', {
            templateUrl: 'templates/project/exportlist.html'
        });

        $routeProvider.when('/comment/:project_id', {
            templateUrl: 'templates/comment/comment.html'
        });

        $routeProvider.when('/global_comment/:project_id', {
            templateUrl: 'templates/comment/global_comment.html'
        });

        $routeProvider.when('/sidecomment', {
            templateUrl: 'templates/sidecomment/comment.html'
        });

        $routeProvider.when('/compare-versions/:project_id/:object_id', {
            templateUrl: 'templates/compare/compare_versions.html'
        });

        $routeProvider.when('/help', {
            templateUrl: 'templates/project/help.html'
        });

        $routeProvider.when('/activity', {
            templateUrl: 'templates/activity_log/activity_log.html',
        });

        $routeProvider.when('/taxonomy', {
            templateUrl: 'templates/taxonomy/list.html',
        });

        // $routeProvider.when('/taxonomy/:taxonomy_id', {
        //     templateUrl: 'templates/taxonomy/list.html',
        // });

        $routeProvider.when('/manage-taxonomy/:taxonomy_id', {
            templateUrl: 'templates/taxonomy/list.html',
        });

        $routeProvider.when('/tag', {
            templateUrl: 'templates/tag/list.html',
        });
        $routeProvider.when('/template/:project_id', {
            templateUrl: 'templates/template/list.html'
        });
        $routeProvider.when('/metadata', {
            templateUrl: 'templates/metadata/list.html'
        });
        $routeProvider.when('/widget', {
            templateUrl: 'templates/widget/list.html'
        });
        $routeProvider.when('/widget/form', {
            templateUrl: 'templates/widget/widget_form.html'
        });
        $routeProvider.when('/widget/form/:widget_id', {
            templateUrl: 'templates/widget/widget_form.html'
        });
        $routeProvider.when('/project_asset_usage/:project_id', {
            templateUrl: 'templates/project/project_asset_usage.html'
        });
        $routeProvider.when('/project_asset_usage_more/:project_id/:type', {
            templateUrl: 'templates/project/project_asset_usage_more.html'
        });
        $routeProvider.when('/error_log', {
            templateUrl: 'templates/activity_log/system_error_log.html'
        });
        $routeProvider.when('/assessment/:project_id', {
            templateUrl: 'templates/assessment/list.html'
        });
        $routeProvider.when('/assessment_question/:assessment_id/:project_id', {
            templateUrl: 'templates/assessment/question_list.html'
        });
        $routeProvider.when('/assessment_question/:assessment_id/:project_id/global_repo', {
            templateUrl: 'templates/assessment/question_list.html'
        });
        $routeProvider.when('/assessment_question/:assessment_id/:project_id/:question_type_id', {
            templateUrl: 'templates/assessment/question.html'
        });
        $routeProvider.when('/assessment_question/:assessment_id/:project_id/:question_type_id/global_repo', {
            templateUrl: 'templates/assessment/question.html'
        });
        $routeProvider.when('/assessment_question/:assessment_id/:project_id/:question_type_id/:question_id', {
            templateUrl: 'templates/assessment/question.html'
        });        
        $routeProvider.when('/assessment_question/:assessment_id/:project_id/:question_type_id/:question_id/global_repo', {
            templateUrl: 'templates/assessment/question.html'
        });  
        $routeProvider.when('/management/:set_type/:project_id',{
             templateUrl: 'templates/dataset/dataset_list.html'
         });        
        
        $routeProvider.when('/management/:set_type/:project_id/:collection_id',{
             templateUrl: 'templates/dataset/dataset_view_form.html'
         });
         
        $routeProvider.when('/imagepopup/:project_id', {
            templateUrl: 'templates/imagepopup/imagepopup.html'
        });
        
        $routeProvider.when('/linkcheck/:project_id', {
            templateUrl: 'templates/linkcheck/linkcheck.html'
        });*/
        
        $routeProvider.otherwise({redirectTo: '/'});

        /*
         * chunk upload by flow.js start
         */
        flowFactoryProvider.defaults = {
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 1
        };
        flowFactoryProvider.on('catchAll', function (event) {
            //console.log('catchAll', arguments);
        });

        /*
         * chunk upload by flow.js end
         */

        /**
         *  default setup of ivh tree view config
         */
        ivhTreeviewOptionsProvider.set({
            idAttribute: 'id',
            labelAttribute: 'title',
            defaultSelectedState: false,
            validate: true
        });


    }]);

