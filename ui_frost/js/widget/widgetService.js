/*****
 * Generic Object Services
 * Author - Arikh Akher * 
 */
'use strict';

app.service('widgetService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        // Start -- Method to save widget
        this.save = function (formdata, $scope) {
            var deferred = $q.defer();
            var fd = new FormData();
            var type_id ='';
            if(angular.isDefined($scope.formData.type) && angular.isDefined($scope.formData.type.id)) {
                type_id = $scope.formData.type.id;
            }
            fd.append('name', (angular.isDefined($scope.formData.name) ? $scope.formData.name : ''));
            fd.append('type', type_id);
            fd.append('description', (angular.isDefined($scope.formData.description) ? $scope.formData.description : ''));
            fd.append('widget_file', (angular.isDefined($scope.formData.widget_file) ? $scope.formData.widget_file : ''));
            //fd.append('_method', 'POST');
            //fd.append('access_token', sessionService.get('access_token'));
            var apiUrl = serviceEndPoint + 'api/v1/widget' + '?access_token=' + sessionService.get('access_token');
            $http({
                method: 'POST',
                url: apiUrl,
                data: fd,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to save widget

        // Start -- Method to update widget
        this.update = function (formdata, $scope) {
            var deferred = $q.defer();
            var fd = new FormData();
            fd.append('name', (angular.isDefined(formdata.name) ? formdata.name : ''));
            fd.append('type', (angular.isDefined($scope.formData.type) ? $scope.formData.type : ''));
            fd.append('description', (angular.isDefined(formdata.description) ? formdata.description : ''));
            fd.append('widget_file', (angular.isDefined(formdata.widget_file) ? formdata.widget_file : ''));
            fd.append('id', (angular.isDefined($scope.widget_id) ? $scope.widget_id : ''));
            fd.append('_method', 'PUT');
            //fd.append('access_token', sessionService.get('access_token'));
            var apiUrl = serviceEndPoint + 'api/v1/widget' + '?access_token=' + sessionService.get('access_token');
            $http({
                method: 'POST',
                url: apiUrl,
                data: fd,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to update widget
        /**
         * Get Mime Types
         */
        this.getWidgets = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }

            $http.get(serviceEndPoint + 'api/v1/widget?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (widget_id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/widget-activate-inactivate/widget_id/' + widget_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getWidgetsById = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var widget_id = $scope.widget_id;
            var deferred = $q.defer();

            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            $http.get(serviceEndPoint + 'api/v1/widget/widget_id/' + widget_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getWidgetTypes = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/widget-types?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);

