'use strict';

app.controller('Test_harness', ['$scope', '$http', '$location', '$window', function($scope, $http, $location, $window) {

        var host = serviceEndPoint;
        $scope.menu_label = 'API List';
        $scope.form_panel = 0;
        $scope.output = '';
        $scope.status = 0;
        $scope.loading = 0;
        $scope.apiUrl = '';
        $scope.menuItems = new Array(
                {label: 'API List', id: 0, url: '', method: '', visible: true},
//        {label: 'Login', id: 1, url: 'api/v1/login', method: 'POST'},
        {label: 'Get Token', id: 1, url: '', method: 'POST', visible: false},
        {label: 'List Objects', id: 3, url: 'api/v1/objects', method: 'GET', visible: true},
        {label: 'Create Object', id: 2, url: 'api/v1/objects', method: 'POST', visible: true},
        {label: 'Object Details', id: 10, url: 'api/v1/objects/{object_id}', method: 'GET', visible: true},
        {label: 'Delete Object', id: 11, url: 'api/v1/objects/{object_id}', method: 'DELETE', visible: true},
        {label: 'Update Object', id: 22, url: 'api/v1/objects/{object_id}', method: 'POST', visible: true}
        /*,
         {label: 'Create Tag', id: 4, url: 'api/v1/tag', method: 'POST'},
         {label: 'Delete Tag', id: 13, url: 'api/v1/tag', method: 'DELETE'},
         {label: 'Create Taxonomy', id: 5, url: 'api/v1/taxonomy', method: 'POST'},
         {label: 'Edit Taxonomy', id: 16, url: 'api/v1/taxonomy', method: 'PUT'},
         {label: 'Delete Taxonomy', id: 14, url: 'api/v1/taxonomy', method: 'DELETE'},
         {label: 'Create Metadata', id: 6, url: 'api/v1/metadata', method: 'POST'},
         {label: 'Edit Metadata', id: 15, url: 'api/v1/metadata', method: 'PUT'},
         {label: 'Delete Metadata', id: 12, url: 'api/v1/metadata', method: 'DELETE'},
         {label: 'Checkin', id: 7, url: 'api/v1/content/checkin', method: 'POST'},
         {label: 'Checkout', id: 8, url: 'api/v1/content/checkout', method: 'GET'},
         {label: 'Cancel Checkout', id: 9, url: 'api/v1/content/checkout/cancel', method: 'GET'}
         */
        );
        if ($scope.status != 0) {
            $scope.access_token = $window.sessionStorage.getItem('access_token');
        }


        $scope.selectedMenu = function(menu) {
            $scope.formdata = {};
            $scope.formdata.user_id = 1;
            $scope.menu_label = menu.label;
            $scope.form_panel = menu.id;
            $scope.api = menu.url;
            $scope.visible = menu.visible;
            $scope.method = menu.method;
            $scope.outputData = '';
            $scope.status = 0;
            if ($scope.form_panel == 1) {
                $scope.callUrl = ($scope.form_panel != 0) ? oauthUrl : '';
            } else {
                $scope.callUrl = ($scope.form_panel != 0) ? host + $scope.api : '';
            }
            $scope.apiUrl = 'Service End Point: ' + $scope.callUrl;

            if ($scope.access_token) {
                //$scope.formdata.access_token = $scope.access_token;
            }
            if ($scope.access_token) {
                $scope.access_token_txt = 'Access Token: ' + $scope.access_token;
            }
        };
        $scope.execute = function(formdata) {
            if (($scope.form_panel != 0 && formdata && Object.keys(formdata).length > 0 && $scope.form_panel != 2 && $scope.form_panel != 7 && $scope.form_panel != 22) || $scope.form_panel == 3 || $scope.form_panel == 1) {
                //if (!formdata) {
                //    formdata = {};
                //}

                $scope.loading = 1;
                $scope.status = 0;
                /*
                 formdata.apiUrl = host + $scope.api;
                 formdata.method = $scope.method;
                 $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                 $http({
                 method: $scope.method,
                 url: rootUrl + '/oauth/consumer2.php',
                 params: formdata
                 }).success(function(data, status, headers, config) {
                 var str = JSON.stringify(data, undefined, 4);
                 formatResponse(str);
                 $scope.status = data.status;
                 $scope.loading = 0;
                 $window.sessionStorage.setItem('access_token', data.data.token);
                 $scope.access_token = $window.sessionStorage.getItem('access_token');
                 }).error(function(data, status, headers, config) {
                 var str = JSON.stringify(data, undefined, 4);
                 formatResponse(str);
                 $scope.status = 400;
                 $scope.loading = 0;
                 });
                 */

                //String replacing the url
                $scope.callUrl = $scope.callUrl.replace("{object_id}", $scope.formdata.object_id);

                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                $http({
                    method: $scope.method,
                    url: $scope.callUrl,
                    params: formdata
                }).success(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = data.status;
                    $scope.loading = 0;
                    if ($scope.form_panel == 1) {
                        $window.sessionStorage.setItem('access_token', data.access_token);
                        $scope.access_token = $window.sessionStorage.getItem('access_token');
                    }
                }).error(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = 400;
                    $scope.loading = 0;
                });



            }
            if ($scope.form_panel == 2 || $scope.form_panel == 7 || $scope.form_panel == 22) {
                $scope.loading = 1;
                $scope.status = 0;
                /*
                 var fd = new FormData();
                 fd.append('filedata', $scope.formdata.filedata);
                 fd.append('apiUrl', host + $scope.api);
                 fd.append('method', $scope.method);
                 if ($scope.form_panel == 7) {
                 fd.append('object_id', $scope.formdata.object_id);
                 }
                 $http.post(rootUrl + '/oauth/consumer2.php', fd, {
                 transformRequest: angular.identity,
                 headers: {'Content-Type': undefined}
                 }).success(function(data, status, headers, config) {
                 var str = JSON.stringify(data, undefined, 4);
                 formatResponse(str);
                 $scope.status = data.status;
                 $scope.loading = 0;
                 }).error(function(data, status, headers, config) {
                 var str = JSON.stringify(data, undefined, 4);
                 formatResponse(str);
                 $scope.status = data.status;
                 $scope.loading = 0;
                 });
                 */

                var fd = new FormData();                
                if($scope.file_type == 'file'){
                    fd.append('file', $scope.formdata.file);
                }else if($scope.file_type == 'file_content'){
                    fd.append('file_content', $scope.formdata.file_content);
                }
                fd.append('user_id', $scope.formdata.user_id);
                if ($scope.form_panel == 2) {
                    fd.append('object_type_id', $scope.formdata.object_type_id.value);
                    fd.append('project_type_id', 1);                    
                }
                console.info($scope);
                fd.append('object_name', $scope.formdata.object_name);
                fd.append('description', $scope.formdata.description);
                if ($scope.form_panel == 22) {
                    fd.append('_method', 'PUT');
                }
                
                
                //fd.append('access_token', $scope.formdata.access_token);
                if ($scope.form_panel == 7) {
                    fd.append('object_id', $scope.formdata.object_id);
                }
                //String replacing the url
                if(angular.isDefined($scope.formdata.object_id)){
                    $scope.callUrl = $scope.callUrl.replace("{object_id}", $scope.formdata.object_id);
                }
                console.log($scope);
                console.log(fd);
                console.log($scope.formdata.file);
                $http({
                    method: $scope.method,
                    data:fd,
                    url: $scope.callUrl + '?access_token=' + $scope.formdata.access_token,
                    //params: { formdata: $scope.formdata, file: $scope.formdata.file },                    
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    /*transformRequest: function (data) {
                        var formData2 = new FormData();
                        formData2.append("formData", angular.toJson($scope.formdata));
                        formData2.append("file", $scope.formdata.file);
                        return formData2;
                    },*/
                }).success(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = data.status;
                    $scope.loading = 0;
                }).error(function(data, status, headers, config) {
                    
                });
                
                /*$http.put($scope.callUrl + '?access_token=' + $scope.formdata.access_token, 
                {
                    object_id: $scope.formdata.object_id,
                    object_name: $scope.formdata.object_name,
                    description: $scope.formdata.description,
                    file: $scope.formdata.file
                },
                {                   
                    transformRequest: function(data) {
                        var formData = new FormData();
                        formData.append("object_id", data.object_id);
                        formData.append("object_name", data.object_name);
                        formData.append("description", angular.toJson(data.description));
                        formData.append("file", data.file);                        
                        return formData;                
                    },
                    headers: {'Content-Type': undefined}
                }).success(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = data.status;
                    $scope.loading = 0;
                }).error(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = data.status;
                    $scope.loading = 0;
                });*/
                
                /*$http.put($scope.callUrl + '?access_token=' + $scope.formdata.access_token, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = data.status;
                    $scope.loading = 0;
                }).error(function(data, status, headers, config) {
                    var str = JSON.stringify(data, undefined, 4);
                    formatResponse(str);
                    $scope.status = data.status;
                    $scope.loading = 0;
                });*/

            }

        };

        $scope.syntaxHighlight = function(json) {
            json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
        };

        function formatResponse(str) {
            $scope.outputData = $scope.syntaxHighlight(str);
        }

        $scope.colors = [
            {name: 'project', value: 1},
            {name: 'learning object', value: 2},
            {name: 'page', value: 3},
            {name: 'widget', value: 4},
            {name: 'image', value: 5},
            {name: 'audio', value: 6},
            {name: 'video', value: 7}
        ];
        $scope.object_type_id = $scope.colors[2];
    }]);

