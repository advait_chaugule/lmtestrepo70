'use strict';

app.controller('widget', ['$scope', '$http', 'sessionService', '$location', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', 'widgetService', function ($scope, $http, sessionService, $location, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter, widgetService) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        if (userinfo.id !== 1) {
            $rootScope.goto('dashboard');
        }
        $scope.currentOrderByField = 'name';
        $scope.reverseSort = false;
        $scope.search_text = '';
        $scope.widget_id = '';
        $scope.widgetTypes = {};
        $scope.formData = {};

        //Call Widgets        
        $scope.currentPage = 1;
        $scope.pageNumber = '';
        $scope.itemsPerPage = '';
        $scope.widgets = {};
        $scope.getWidgets = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = '10';
            widgetService.getWidgets($scope).then(function (data) {
                if (data.data == null || data.data == '') {
                    $scope.widgets = {};
                    $scope.total = data.total;
                } else {
                    $scope.total = data.total;
                    $scope.widgets = data.data.list;
                }

            });
        };
        $scope.getWidgets($scope.currentPage);
        ///        
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerWidget = function (newPage) {
            $scope.getWidgets(newPage);
        };

        $scope.getWidgetTypes = function () {
            widgetService.getWidgetTypes($scope).then(function (data) {
                if (data.data == null || data.data == '') {
                    $scope.widgetTypes = {};
                } else {
                    $scope.widgetTypes = data.data;
                }
            });
        };


        $scope.getWidgetTypes();


        $scope.addWidgetForm = function () {
            $location.path('widget/form');
        }

        $scope.editWidgetForm = function (id) {
            $location.path('widget/form/' + id);
        }

        if (angular.isDefined($routeParams.widget_id)) {
            $scope.widget_form_button = 'Edit';
            $scope.widget_form_title = 'Edit Widget';
            $scope.widget_id = $routeParams.widget_id;
            $scope.add_btn = false;
            $scope.edit_btn = true;
            //console.info($scope.widget_id);
            widgetService.getWidgetsById($scope).then(function (result) {
                setTimeout(function () {
                    var res = result.data.widget;
                    $scope.formData.name = res.name;
                    var widget_type_index = $scope.getWidgetTypeIndex(res.type);
                    $scope.formData.type = $scope.widgetTypes[widget_type_index];
                    $scope.formData.description = res.description;
                }, 500);
            });
        } else {
            $scope.widget_form_title = 'Add Widget';
            $scope.widget_form_button = 'Add';
            $scope.add_btn = true;
            $scope.edit_btn = false;
        }
        //===========Save Widgets========
        $scope.clickedOnce = false;
        $scope.submitted = false;
        $scope.add_widget = function (formData) {
            $scope.clickedOnce = true;
            $scope.submitted = true;
            $scope.errorMessage = '';
            $scope.error = [];
            //if (formData.$valid == true) {
            widgetService.save(formData, $scope).then(function (data) {
                if (data.status == 200) {
                    $location.path('widget');
                } else {
                    $scope.clickedOnce = false;
                    if (angular.isDefined(data.error) && data.error) {
                        if (angular.isDefined(data.error.name) && data.error.name[0]) {
                            $scope.error.name = data.error.name[0];
                        }
                        if (angular.isDefined(data.error.type) && data.error.type[0]) {
                            $scope.error.type = data.error.type[0];
                        }
                        if (angular.isDefined(data.error.widget_file) && data.error.widget_file[0]) {
                            $scope.error.widget_file = data.error.widget_file[0];
                        }
                    } else {

                    }
                }
            });
            //}
        }
        $scope.edit_widget = function (formData) {
            $scope.clickedOnce = true;
            $scope.submitted = true;
            $scope.errorMessage = '';
            $scope.error = [];
            //if (formData.$valid == true) {
            widgetService.update(formData, $scope).then(function (data) {
                if (data.status == 200) {
                    $location.path('widget');
                } else {
                    $scope.clickedOnce = false;
                    if (angular.isDefined(data.error) && data.error) {
                        if (angular.isDefined(data.error.name) && data.error.name[0]) {
                            $scope.error.name = data.error.name[0];
                        }
                    }
                }
            });
            //}
        }


        $scope.changeStatus = function (id) {
            widgetService.changeStatus(id, $scope).then(function () {
            });
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.getWidgets(1);
        };

        // apply searching
        $scope.do_search = function () {
            $scope.getWidgets(1);
        };

        $scope.getWidgetTypeIndex = function (type_id) {
            var index_id = 0;
            angular.forEach($scope.widgetTypes, function (value, key) {
                if (value.id == type_id) {
                    index_id = key;
                }
            });
            return index_id;
        };


    }]);

