/*****
 * Services for Login
 * Author - Arikh Akher
 */

'use strict';

//Start of loginService
app.service('loginService', ['$http', '$q', '$location', 'tokenService', 'sessionService', function ($http, $q, $location, tokenService, sessionService) {

    this.login = function ($scope) {
        var deferred = $q.defer();

        //var tokenDataPromise = tokenService.generate();
        //$scope.user.access_token = data.access_token;
        //$http.defaults.headers.common.Authorization = 'Bearer '+$scope.user.access_token
        //$http.post(serviceEndPoint + 'api/v1/login', {
        $http.post(serviceEndPoint + 'api/v1/validateLogin', {
            username: $scope.user.username,
            password: $scope.user.password,
            // headers : {'Authorization': 'Bearer '+$scope.user.access_token}
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
            $scope.response = data;
            if ($scope.response.status == 200) {
                var uid = $scope.response.data.user_id;
                if (uid) {
                    //sessionService.set('uid', uid);
                    sessionService.set('userinfo', JSON.stringify($scope.response.data));
                    sessionService.set('access_token', $scope.response.data.active_access_token);

                    //Set SSO information
                    if (sso.enable == true && angular.isDefined($scope.response.data.sso_accessible_platforms)) {
                        sessionService.set('sso_accessible_platforms', JSON.stringify($scope.response.data.sso_accessible_platforms));
                    }
                }
                else {
                    $scope.error_msg = 'Invalid Credentials';
                    console.log(data.message);

                    $location.path('login');
                }
            } else {
                $scope.error_msg = 'Invalid Credentials';
                // $location.path('login');
            }

        }).error(function (data, status, headers, config) {
            deferred.resolve(data);
        });

        /*tokenDataPromise.then(function (data) {

            $scope.user.access_token = data.access_token;
            //$http.defaults.headers.common.Authorization = 'Bearer '+$scope.user.access_token
            //$http.post(serviceEndPoint + 'api/v1/login', {
            $http.post(serviceEndPoint + 'api/v1/login?access_token=' + $scope.user.access_token, {
                username: $scope.user.username,
                password: $scope.user.password,
                // headers : {'Authorization': 'Bearer '+$scope.user.access_token}
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
                $scope.response = data;
                if ($scope.response.status == 200) {

                    var uid = $scope.response.data.id;
                    if (uid) {
                        //sessionService.set('uid', uid);
                        sessionService.set('userinfo', JSON.stringify($scope.response.data));
                        sessionService.set('access_token', $scope.user.access_token);
                    }
                    else {
                        $scope.error_msg = 'Invalid Credentials';
                        $location.path('login');
                    }
                } else {
                    $scope.error_msg = 'Invalid Credentials';
                    //$location.path('login');
                }

            }).error(function (data, status, headers, config) {
                $scope.responsedata = data;
                $location.path('login');
            });
        });*/
        return deferred.promise;
    };

    this.logout = function ($scope) {
        var deferred = $q.defer();
        var access_token = sessionService.get('access_token');
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        //$scope.user_id = JSON.parse(sessionService.get('userinfo')).id;

        //if (!access_token || userinfo == null) {
        //console.log('I am in logout block!');
        //$location.path('login');
        //}

        $http({
            method: 'GET',
            url: serviceEndPoint + 'api/v1/logout',
            headers: {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            },
        }).success(function (data, status, headers, config) {
            //sessionService.destroy('uid');
            sessionService.destroy('userinfo');
            sessionService.destroy('user_permissions');
            sessionService.destroy('access_token');
            sessionService.destroy('viewMode');
            localStorage.clear();
            window.localStorage.clear();

            $location.path('login');

        }).error(function (data, status, headers, config) {
            //Proper Error handling will be done here.
            $location.path('login');
        });

        return deferred.promise;
    };

    this.userPermisions = function ($scope) {
        $scope.access_token = sessionService.get('access_token');
        var deferred = $q.defer();
        sessionService.set('user_permissions', JSON.stringify([]));
        $http({
            method: 'GET',
            url: serviceEndPoint + 'api/v1/permissions?access_token=' + $scope.access_token
        }).success(function (data, status, headers, config) {
            $scope.user_permissions = data.data;
            sessionService.set('user_permissions', JSON.stringify($scope.user_permissions));
            deferred.resolve($scope.user_permissions);
        }).error(function (data, status, headers, config) {

        });
        return deferred.promise;
    };

    this.islogged = function () {
        var user_id = 0;

        var userinfo = sessionService.get('userinfo');
        var access_token = sessionService.get('access_token');

        if (userinfo != '' && access_token != '') {
            userinfo = JSON.parse(userinfo);
            if (userinfo != null) {
                user_id = userinfo.user_id;
                if (user_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    };

    this.ucfirst = function (param) {
        //console.log("I am called !"+param);
        var output = param.charAt(0).toUpperCase() + param.substring(1);
        return output;
    };


    this.forgetPassword = function ($scope) {
        var deferred = $q.defer();
        var params = {};
        tokenService.generate().then(function (data) {
            params.access_token = data.access_token;
            params.email = $scope.email;
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/checkuser',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {

            });
        });

        return deferred.promise;
    };

    this.createPassword = function ($scope) {
        var deferred = $q.defer();
        var params = {};
        tokenService.generate().then(function (data) {
            params.access_token = data.access_token;
            //params.email = $scope.emailid;
            params.new_password = $scope.password1;
            params.password_token = $scope.password_token;
            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/createpassword',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {

            });
        });

        return deferred.promise;
    };

    this.userStatusCheck = function (user_id) {
        var deferred = $q.defer();
        var access_token = sessionService.get('access_token');
        var params = {};
        $http({
            method: 'GET',
            url: serviceEndPoint + 'api/v1/user-status/' + user_id + '?access_token=' + access_token,
            params: params
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);//Return the roles list
        }).error(function (data, status, headers, config) {
            //Logout Browser is full with cookie
            var http_status = { "http_status": "400" };
            deferred.resolve(http_status);
            return false;
        });
        return deferred.promise;
    }

    this.verifyPasswordToken = function ($scope) {
        var deferred = $q.defer();
        var access_token = sessionService.get('access_token');
        var params = {};
        $http({
            method: 'GET',
            url: serviceEndPoint + 'api/v1/verify-password-token/' + $scope.password_token + '?access_token=' + access_token,
            params: params
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);//Return the roles list
            //$location.path('login');


        }).error(function (data, status, headers, config) {
            //$location.path('login');
        });
        return deferred.promise;
    }
}]);
