'use strict';

app.controller('login', ['$scope', 'loginService', '$rootScope', 'sessionService', '$interval', '$location', '$http', '$routeParams', function($scope, loginService, $rootScope, sessionService, $interval, $location, $http, $routeParams) {
        $rootScope.chkAccessTokenInt = 6000000;
        $scope.user = {};
        $scope.formData = {};
        $scope.submitted = false;
        $interval.cancel($rootScope.intervalPromise);
        $scope.signin = function(formData) {
            $scope.error = [];
            $scope.submitted = true;
            //loginService.login($scope); //Call login service              
            if (formData.$valid) {
                $scope.user.username = $scope.username;
                $scope.user.password = $scope.password;
                loginService.login($scope).then(function(data) {
                    if (data.status == 200) {
                        loginService.userPermisions($scope).then(function(user_permissions) {
                            $location.path('dashboard');
                            return true;
                        });
						$location.path('dashboard');
                        return true;
                        //$rootScope.session_access_token = sessionService.get('access_token');
                    } else  {
                        $scope.responsedata=data;
//                        if ($scope.response.error.username[0]) {
//                            //$scope.error.username = $scope.response.error.username[0];
//                        }
//                        if ($scope.response.error.password[0]) {
//                            //$scope.error.password = $scope.response.error.password[0];
//                        }
                
                        $scope.password = null;
                    }
                });
            } else {
                if ($scope.formData.username.$error.required) {
                    $scope.error_msg = 'All fields are required.';
                }
                if ($scope.formData.password.$error.required) {
                    $scope.error_msg = 'All fields are required.';
                }
            }

        };

        $scope.forgetPasswd = function(formData) {

            if (formData.$valid) {
                loginService.forgetPassword($scope).then(function(data) {
                    $scope.error_msg = data.message;
                    $scope.status = data.status;
                });
            } else {
                if ($scope.formData.email.$error.required) {
                    $scope.status = 400;
                    $scope.error_msg = 'All fields are required.';
                }
            }
        };

        $scope.createPasswd = function(formData) {

            if (formData.$valid) {
                //$scope.emailid = $routeParams.emailid;
                $scope.password1 = formData.password1.$modelValue;
                $scope.password2 = formData.password2.$modelValue;
                
                if($scope.formData.password1.$modelValue.length < 5) {
                     $scope.status = 400;
                    $scope.error_msg = 'New passwords are not same.';
                    $scope.error_msg = 'Password should contain at-least 5 characters.';
                } else {
                    if ($scope.password1 == $scope.password2) {
                        loginService.createPassword($scope).then(function(data) {
                            $scope.error_msg = data.message;
                            $scope.status = data.status;
                        });
                    } else {
                        $scope.status = 400;
                        $scope.error_msg = 'New passwords are not same.';
                    }
                }
            } else {
                if ($scope.formData.password1.$error.required || $scope.formData.password2.$error.required) {
                    $scope.status = 400;
                    $scope.error_msg = 'All fields are required.';
                }
            }
        };


        $scope.show_login_page = false;
        // Check If 
        if (loginService.islogged()) {
            //debugger;
            $location.path('dashboard');
            // $http.get(serviceEndPoint + 'api/v1/check_user_session?access_token=' + sessionService.get('access_token'), {
            // }).success(function(data, status, headers, config) {
            //     if (data.status == '200') {
            //         $location.path('dashboard');
            //         //return true;        
            //     } else {
            //         $location.path('logout');
            //     }
            // }).error(function(data, status, headers, config) {
            //     $rootScope.unauthorised_redirection(data.status);
            // });
            /*console.log('dashboard=1212');
             $location.path('dashboard');
             return true;*/
        } else {
            $scope.show_login_page = true;
        }
        
        $scope.verifyPasswordTokenCalled = false;
        if (angular.isDefined($routeParams.password_token) && $routeParams.password_token) {
            $scope.password_token  = $routeParams.password_token;
            loginService.verifyPasswordToken($scope).then(function(data) {                
                $scope.verifyPasswordTokenCalled = true;
                $scope.password_token_expired = data.data.expired;
                $scope.error_msg = data.message;
                
            });
        }


    }]);


app.controller('logout', ['$scope', 'loginService', '$location', '$rootScope', '$interval', function($scope, loginService, $location, $rootScope, $interval) {
        //Clear cache is called while log out 
        //debugger;
        $rootScope.clearCache();
        jQuery('.editor-loader').remove();
        jQuery('body').css('overflow', '');
        $rootScope.chkAccessTokenInt = 6000000;
        loginService.logout($scope);
        $interval.cancel($rootScope.intervalPromise);


    }]);


