/*****
 * Services for TOC
 * Author - Koushik Samanta
 *  
 */
'use strict';
//Start of aclService
var returnVal = '';
var returnTree = '';
var node_id = '';
var project_id = '';
app.service('tocService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {
        this.saveContent = function ($scope, redirectPage) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            params.content = $scope.text;
            var deferred = $q.defer();
            var node_title = jQuery('#node_title').html();
            node_title = node_title.replace(/&nbsp;/gi, '').trim();
            if (node_title != '') {
                $http.post(serviceEndPoint + 'api/v1/toc-content/' + $scope.node_id + '?access_token=' + params.access_token, {
                    content: $scope.text,
                    node_id: $scope.node_id,
                    project_id: $scope.project_id,
                    node_title: node_title
                }).success(function (data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(data);
                    //return deferred.promise;
                    if (redirectPage) {
                        //$location.url('/edit_toc/' + $scope.object_id);
                        $location.url(redirectPage);
                    }

                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                alert('Node title can not be empty');
            }
            return deferred.promise;
        };
        this.saveToc = function ($scope) {
            var deferred = $q.defer();
            /*var params = {};
             params.toc = $scope.data;
             params.object_id = $scope.object_id;
             params.access_token = sessionService.get('access_token');*/
            /*$http({
             method: 'POST',
             url: serviceEndPoint + 'api/v1/toc',
             headers: {'Content-Type': 'application/json'},
             params: params
             }).success(function(data, status, headers, config) {
             deferred.resolve(data);
             }).error(function(data, status, headers, config) {
             deferred.resolve(data);
             });*/
            //console.info($scope.data);
            $http.post(serviceEndPoint + 'api/v1/toc?access_token=' + sessionService.get('access_token'), {
                object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id),
                project_id: $scope.project_id,
                child_node_title: (angular.isUndefined($scope.child_node_title) ? '' : $scope.child_node_title),
                child_node_type: (angular.isUndefined($scope.child_node_type) ? '' : $scope.child_node_type),
                toc: JSON.stringify($scope.data)
            }).success(function (data, status, headers, config) {
                //console.log($scope.data);
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.checkFileName = function ($scope) {            
            /*console.log({
             project_id: $scope.project_id,
             child_node_title: $scope.node_title,
             object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id),
             pageAction: $scope.pageAction,
             section_page: $scope.section_page,
             file: $scope.file
             });*/
            var deferred = $q.defer();
            //console.log($scope.assessment_id);
            $http.post(serviceEndPoint + 'api/v1/validate-toc-node?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                child_node_title: $scope.node_title,
                object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id),
                pageAction: $scope.pageAction,
                section_page: $scope.section_page,
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                file: $scope.file,
                assessment_id: $scope.assessment_id,
                asset_id: (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id),
                child_node_type: $scope.ext_child_node_type,
                ext_res_url: (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url),
                ext_res_type: (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type)
            }).success(function (data, status, headers, config) {
                //console.log(data);
                $scope.checkChildNode = data;
                deferred.resolve($scope.checkChildNode);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.updateNodeTitle = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/update-node-title?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                node_title: $scope.child_node_title,
                object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id)
            }).success(function (data, status, headers, config) {

                deferred.resolve($scope.checkChildNode);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }


        var currentObject = this;
        this.getToc = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            params.project_id = $scope.project_id;
            params.object_id = $scope.object_id;
            params.access_token = sessionService.get('access_token');
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/toc',
                headers: {'Content-Type': 'application/json'},
                params: params
            }).success(function (data, status, headers, config) {
                if (data) {
                    $scope.orgToc = data;
                } else {
                    $scope.orgToc = currentObject.orgToc();
                }
                deferred.resolve($scope.orgToc);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /**
         * Get Toc Types
         * @param {type} $scope
         * @returns {undefined}
         */
        this.getTocTypes = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_type_id=' + $scope.project_type_id + '&type_id=' + $scope.parent_node_type;
            $http.get(serviceEndPoint + 'api/v1/toc-type?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                $scope.TocTypes = data.data;
                deferred.resolve($scope.TocTypes);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /**
         * Get Toc Content
         * @param {type} $scope
         * @returns {undefined}
         */
        this.getTocContent = function ($scope) {
            //console.info($scope.project_details);
            var node_id = $scope.node_id;
            project_id = $scope.project_details.id;
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/toc-content/' + $scope.node_id + '?access_token=' + sessionService.get('access_token') + '&project_id=' + project_id + '&s3=0'
            }).success(function (data, status, headers, config) {

                $scope.text = data;
                //console.log(data.status);                
                //if ( (data.status == '400' && $scope.toc_type == 'Chapter') || ($scope.toc_type == 'Chapter' && data.data.content=='') || ($scope.toc_type == 'Chapter' && angular.isUndefined(data.data.content)) ) {
                if ($scope.toc_type == 'Chapter' && (data.status == '400' || data.data.description == '' || angular.isUndefined(data.data.description))) {
                    // toc_type is Chapter and content is blank than call default content
                    $http({
                        method: 'GET',
                        url: clientUrl + 'simple_editor/json/content.json'
                    }).success(function (data, status, headers, config) {
                        $scope.node_title = $scope.nodeTitle;
                        $scope.object_name = $scope.object_details.name;
                        returnVal = data;
                    }).error(function (data, status, headers, config) {

                    });
                } else {
                    if (data.data) {
                        $scope.object_name = data.data.object_name;
                        $scope.node_title = data.data.node_title;
                        returnVal = {
                            "content": data.data.description,
                            "leftPanelTree": data.data.leftPanelTree
                        };
                    } else {
                        returnVal = {
                            "content": ""
                        };
                    }

                }
                htmlEditor.init();
                //console.info(data.data.description);

            }).error(function (data, status, headers, config) {

            });
        }

        this.deleteTreeApi = function ($scope, toc, node_id) {
            $http.post(serviceEndPoint + 'api/v1/toc-content?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                object_id: node_id,
                toc: JSON.stringify(toc),
                _method: 'DELETE'
            }).success(function (data, status, headers, config) {
                //console.log(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
        }

        this.initiateExport = function ($scope) {
            var deferred = $q.defer();
            
            var params = {};
            params.project_id = $scope.project.id;
            params.project_type_id = $scope.project.project_type_id;
            params.cfi_domain_name = encodeURIComponent($scope.cfi_domain_name);
            params.cfi_book_id = encodeURIComponent($scope.cfi_book_id);
            params.exportType = $scope.exportType;
            params.courseExportOption = $scope.course_export_option ? $scope.course_export_option : 'html';
            if(angular.isDefined($scope.course_export_shell_options)){
                params.courseExportShellOption = $scope.course_export_shell_options;
            }
            console.log(params);
            $http.post(serviceEndPoint + 'api/v1/toc/project-export-request/project_id/' + $scope.project.id + '?access_token=' + sessionService.get('access_token'), params).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.rearrangeTocTree = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/toc-tree-rearrange?access_token=' + sessionService.get('access_token'), {
                object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id),
                project_id: $scope.project_id,
                toc: JSON.stringify($scope.data),
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.edit_toc_node = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/toc-node-edit?access_token=' + sessionService.get('access_token'), {
                object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id),
                project_id: $scope.project_id,
                child_node_title: (angular.isUndefined($scope.child_node_title) ? '' : $scope.child_node_title),
                file: (angular.isUndefined($scope.file) ? '' : $scope.file),
                child_node_type: (angular.isUndefined($scope.child_node_type) ? '' : $scope.child_node_type),
                section_page: $scope.section_page,
                parent_node_id: $scope.parent_node_id,
                toc: JSON.stringify($scope.data),
                tags: (angular.isUndefined($scope.tags) ? '' : JSON.stringify($scope.tags)),
                key_data: (angular.isUndefined($scope.key_data) ? '' : JSON.stringify($scope.key_data)),
                value_data: (angular.isUndefined($scope.value_data) ? '' : JSON.stringify($scope.value_data)),
                taxonomy: (angular.isUndefined($scope.taxonomy) ? '' : JSON.stringify($scope.taxonomy)),
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                assessment_id: (angular.isUndefined($scope.assessment_id) ? '' : $scope.assessment_id),
                asset_id: (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id),
                ext_res_link: (angular.isUndefined($scope.ext_res_link) ? '' : $scope.ext_res_link),
                ext_res_url: (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url),
                ext_res_type: (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type),
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.add_toc_node = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/toc-node-add?access_token=' + sessionService.get('access_token'), {
                object_id: (angular.isUndefined($scope.node_id) ? '' : $scope.node_id),
                project_id: $scope.project_id,
                child_node_title: (angular.isUndefined($scope.child_node_title) ? '' : $scope.child_node_title),
                file: (angular.isUndefined($scope.file) ? '' : $scope.file),
                child_node_type: (angular.isUndefined($scope.child_node_type) ? '' : $scope.child_node_type),
                section_page: $scope.section_page,
                parent_node_id: $scope.parent_node_id,
                parent_node_root_type: $scope.parent_node_root_type,
                template_id: (angular.isUndefined($scope.template_id) ? '' : $scope.template_id),
                content_object_id: (angular.isUndefined($scope.content_object_id) ? '' : $scope.content_object_id),
                content_project_id: (angular.isUndefined($scope.content_project_id) ? '' : $scope.content_project_id),
                toc: JSON.stringify($scope.data),
                tags: (angular.isUndefined($scope.tags) ? '' : JSON.stringify($scope.tags)),
                key_data: (angular.isUndefined($scope.key_data) ? '' : JSON.stringify($scope.key_data)),
                value_data: (angular.isUndefined($scope.value_data) ? '' : JSON.stringify($scope.value_data)),
                taxonomy: (angular.isUndefined($scope.taxonomy) ? '' : JSON.stringify($scope.taxonomy)),
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                assessment_id: (angular.isUndefined($scope.assessment_id) ? '' : $scope.assessment_id),
                asset_id: (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id),
                ext_res_link: (angular.isUndefined($scope.ext_res_link) ? '' : $scope.ext_res_link),
                ext_res_url: (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url),
                ext_res_type: (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type),
                parent_lo_id: (angular.isUndefined($scope.parent_lo_id) ? '' : $scope.parent_lo_id),
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.delete_toc_node = function ($scope, children_toc, child, parent_node_id) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/toc-node-delete?access_token=' + sessionService.get('access_token'), {
                object_id: child.id,
                title: child.title,
                file: child.file,
                project_id: $scope.project_id,
                toc: JSON.stringify($scope.data), //modified TOC
                children_toc: JSON.stringify(children_toc), //Children TOC, need to delete all
                section_page: child.section_page,
                parent_node_id: parent_node_id,
                _method: 'DELETE'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.getLeftPane = function ($scope, redirectPage) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/content-left-panel?access_token=' + sessionService.get('access_token'), {
                content: $scope.text
            }).success(function (data, status, headers, config) {
                returnTree = {
                    "leftPanelTree": data.data
                };
//console.info(data);

                htmlEditor.generateLeftTree(data.data);
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //Create a file name while adding a node 
        this.createFileName = function ($scope) {
            var page_name = '';
            if ($scope.project_type_id == 1) {
                //For PXE Project
                if ($scope.node_type == 2) {
                    //Bodymatter
                    //Create FileName For section and page both
                    page_name = $scope.node_title + '.xhtml';
                } else if ($scope.node_type == 1 || $scope.node_type == 3) {
                    //Frontmatter/Rearmatter
                    //Create FileName For page only
                    page_name = ($scope.section_page == 'page' ? $scope.node_title + '.xhtml' : '');
                }
            } else if ($scope.project_type_id == 2) {
                //For LearningMaterial
                //Create FileName For page only
                page_name = ($scope.section_page == 'page' ? $scope.node_title + '.xhtml' : '');
            } else if ($scope.project_type_id == 3) {
                //For Visual 
                //Create FileName For page only
                page_name = ($scope.section_page == 'page' ? $scope.node_title + '.html' : '');
            } else if ($scope.project_type_id == 5) {
                //For Course 
                //Create FileName For page only
                page_name = ($scope.section_page == 'page' ? $scope.node_title + '.html' : '');
            }

            return page_name;
        };
        /**
         * Get Ticket type count
         * @param {type} $scope
         * @returns {undefined}
         */
        this.getTicketStatusCount = function ($scope) {
            var deferred = $q.defer();
            var query_string = '&check_license_validity='+$scope.check_license_validity+'&check_update_availability='+$scope.check_update_availability;
            $http.get(serviceEndPoint + 'api/v1/ticket-status-count/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token')+query_string, {
            }).success(function (data, status, headers, config) {                
                if (angular.isObject(data.data)) {
                    $scope.TocStatusCount = data.data;    
                    //console.log($scope.TocStatusCount);
                    deferred.resolve({TocStatusCount: $scope.TocStatusCount, TicketTotalCountsByStatus: data.TicketTotalCountsByStatus.TicketTotalCountsByStatus, total_ticket: data.TicketTotalCountsByStatus.total_ticket});
                }
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        
        /**
         * Get ProjectObject Status
         * @param {type} $scope
         * @returns {undefined}
         */
        this.getProjectObjectStatus = function ($scope, object_id) {
            var deferred = $q.defer();
            var api = 'api/v1/project-objects-progress-status/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token');
            if(object_id!=''){
                api = 'api/v1/project-objects-progress-status/project_id/' + $scope.project_id + '/object_id/'+object_id+'?access_token=' + sessionService.get('access_token');
            }            
            $http.get(serviceEndPoint + api, {
            }).success(function (data, status, headers, config) {
                $scope.projObjStatus = data.data;
                deferred.resolve({projObjStatus: $scope.projObjStatus});
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.getFileCompare = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/compare-files?access_token=' + sessionService.get('access_token'), {
                file1_versionid: $scope.file1 ? $scope.file1.version_id : '',
                file2_versionid: $scope.file2 ? $scope.file2.version_id : '',
                project_id: $scope.project_id ? $scope.project_id : 0,
                object_id: $scope.object_id ? $scope.object_id : ''
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /**
         * Copy project API request
         * @param {project_id} $scope
         * @returns {undefined}
         */
        this.copyProject = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            queryString += '&user_id=' + $scope.user_id;
            $http.get(serviceEndPoint + 'api/v1/copy-project?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                //$scope.data = data;
                //deferred.resolve($scope.data);
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.lockEntry = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            queryString += '&user_id=' + $scope.user_id;
            queryString += '&trigger=' + $scope.trigger;
            queryString += '&get_lock_status=' + $scope.getLockStatus;
            $http.get(serviceEndPoint + 'api/v1/store-data-lock?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.getLockEntry = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            queryString += '&user_id=' + $scope.user_id;
            $http.get(serviceEndPoint + 'api/v1/get-data-lock?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.checkObjectLock = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&object_id=' + $scope.object_id;
            queryString += '&user_id=' + $scope.user_id;
            $http.get(serviceEndPoint + 'api/v1/get-object-lock?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }


        this.partialExportService = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            queryString += '&cfi_domain_name=' + encodeURIComponent($scope.cfi_domain_name);
            queryString += '&cfi_book_id=' + encodeURIComponent($scope.cfi_book_id);
            queryString += '&partial_toc_nodes=' + angular.toJson($scope.resultTocNode);
            $http.post(serviceEndPoint + 'api/v1/partial-export?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.updateImportStatus = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            $http.post(serviceEndPoint + 'api/v1/update-import-status?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.searchText = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            queryString += '&search_text=' + $scope.search_text;
            $http.get(serviceEndPoint + 'api/v1/search-text?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.getLOToc = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id;
            queryString += '&object_id=' + $scope.object_id;
            $http.get(serviceEndPoint + 'api/v1/get-lo-toc?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }       
      
        this.makeGlobal = function (ids, $scope) {
            var deferred = $q.defer();
            var isLocal = 1;
            if ($scope.local_global) {
                isLocal = 0;
            }
            $http.post(serviceEndPoint + 'api/v1/change-node-access?access_token=' + sessionService.get('access_token') + '&object_id=' + ids + '&project_id=' + $scope.project_id + '&is_global=' + isLocal, {
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                assessment_id: (angular.isUndefined($scope.assessment_id) ? '' : $scope.assessment_id),
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }; 
        
        this.isUpdateAvailable = function($scope){
            var deferred = $q.defer();
            if(angular.isDefined($scope.object_id)){
                $http.get(serviceEndPoint + 'api/v1/is-toc-update-available?access_token=' + sessionService.get('access_token') + '&object_id=' + $scope.object_id , {
                }).success(function (data, status, headers, config) {
                    if (data.projectPermissionGranted == 401) {
                        $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                    }
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;  
            }
        };
        
        this.updateContent = function($scope){
            var deferred = $q.defer();
            if(angular.isDefined($scope.object_id)){
                $http.post(serviceEndPoint + 'api/v1/update-content?access_token=' + sessionService.get('access_token') + '&object_id=' + $scope.object_id , {
                }).success(function (data, status, headers, config) {
                    if (data.projectPermissionGranted == 401) {
                        $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                    }
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;  
            }
        };
        
        this.getSourceContentObjectId=function($scope){
            var deferred = $q.defer();
            if(angular.isDefined($scope.object_id)){
                $http.get(serviceEndPoint + 'api/v1/get-source-content-object?access_token=' + sessionService.get('access_token') + '&object_id=' + $scope.object_id , {
                }).success(function (data, status, headers, config) {
                    if (data.projectPermissionGranted == 401) {
                        $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                    }
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;  
            }
        };
        
        this.cancelContentUpdate=function($scope){
             var deferred = $q.defer();
            if(angular.isDefined($scope.object_id)){
                $http.post(serviceEndPoint + 'api/v1/cancel-content-update?access_token=' + sessionService.get('access_token') + '&object_id=' + $scope.object_id , {
                }).success(function (data, status, headers, config) {
                    if (data.projectPermissionGranted == 401) {
                        $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                    }
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;  
            }
        };
        
        this.tocLicenseAndUpdateAvailabilityCheck=function($scope){
             var deferred = $q.defer();
            if(angular.isDefined($scope.object_id)){
                $http.post(serviceEndPoint + 'api/v1/toc-licensevalidity-update-check/project_id/'+$scope.project_id+'?access_token=' + sessionService.get('access_token') , {
                }).success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;  
            }
        };
    }]);