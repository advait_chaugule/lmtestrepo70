/*****
 * Controller for TOC
 * Author - Arikh Akher * 
 */

'use strict';
var access_token = '';
var returnPatterns = '';
var toc_type = '';
var tooltipDesc = '';
app.controller('toc_content', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$route', '$rootScope', 'objectService', 'patternService', '$controller', 'glossaryService', function ($scope, sessionService, tocService, $location, $routeParams, $route, $rootScope, objectService, patternService, $controller, glossaryService) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        // passing access_token to simple editor
        access_token = $scope.access_token;
        $scope.user_id = sessionService.get('uid');

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
        } else {

            jQuery('html.ng-scope').append('<div class="editor-loader"></div>');
            $rootScope.fullScreen = 'fullscreen';
            $scope.nocache = Math.floor((Math.random() * 6) + 1);

            $scope.project_id = $routeParams.project_id;
            $scope.node_id = $routeParams.node_id;
            $scope.nodeTitle = $routeParams.title;
            $scope.toc_type = toc_type = $routeParams.type;
            $scope.node_type = $routeParams.typeId;
            $scope.text = '';
            // Fetch Object Details
            objectService.getProjectDetails($scope).then(function (data) {
                $scope.project_details = data;
                $scope.object_name = '';
                $scope.node_title = '';
                tocService.getTocContent($scope);
            });


            $scope.showMsg = 0;
            $scope.message = '';
            $scope.saveContent = function (redirectPage) {
                $scope.showMsg = 1;
                $scope.message = 'Please wait...';
                $scope.text = '';
                $scope.text = htmlEditor.getContent();//htmlEditor.getContent();
                //console.log($('#wysihtml5-editor').val());              
                tocService.saveContent($scope, redirectPage).then(function (data) {
                    $scope.status = data.status;
                    $scope.message = data.message;
                });
            };

            $scope.getLeftPane = function () {
                jQuery('html.ng-scope').append('<div class="editor-loader"></div>');
                $scope.showMsg = 1;
                $scope.message = 'Please wait...';
                $scope.text = '';
                $scope.text = htmlEditor.getContent();//htmlEditor.getContent();
                //console.log($('#wysihtml5-editor').val());              
                tocService.getLeftPane($scope).then(function (data) {
                    $scope.status = data.status;
                    $scope.message = data.message;
                });
            }

            $scope.pattern_status = '1001'; // Only Active Patterns
            patternService.getPattern($scope).then(function (data) {
                $scope.patternList = data.data;
                //console.log(patternList);
                returnPatterns = data;
            });

            $scope.$flow.defaults.flowRepoDir = 'asset';
            $scope.$flow.opts.query = {taxonomy_id: 0, project_id: $scope.project_id};

            $scope.getUploadedFiles = function ($flow, $file, $response) {
                var res = JSON.parse($response);
                console.log(res.location);
                htmlEditor.insertImage(res.location);
            };

            angular.extend(this, $controller('glossary', {$scope: $scope}));
            $scope.addGlossary = function () {
                $scope.term_text = '';
                $scope.term_text = htmlEditor.getSelecttedContent();//htmlEditor.getContent();
                if ($scope.term_text) {
                    glossaryService.getGlossary($scope).then(function (data) {
                        if (data.data) {
                            $scope.editGlossary('', data.data.id);
                        } else {
                            $scope.addGlossaryForm('', $scope.term_text);
                        }
                    });
                }
            };

        }
    }]);




