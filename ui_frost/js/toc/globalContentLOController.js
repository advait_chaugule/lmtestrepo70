/*
 * controller for handling listing of LO page
 * authored by: Meghamala Mukherjee
 */

'use strict';

app.controller('globalContentLO', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', 'assetService', '$modal', 'objectService', '$filter', '$controller', 'taxonomyService', 'keyValueService', 'tagService', 'userService', 'ivhTreeviewMgr', '$compile', 'tocService', '$window', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, assetService, $modal, objectService, $filter, $controller, taxonomyService, keyValueService, tagService, userService, ivhTreeviewMgr, $compile, tocService, $window) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_id = sessionService.get('uid');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.currentOrderByField = 'name';
        $scope.reverseSort = false;
        $scope.disablePreviewClick=false;
        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }
        $scope.object_id=$routeParams.object_id; //It is basically a object under a LO
        //$rootScope.showSearchBox = false;
        $scope.total = 0;
        
        
        $scope.showSearchBox = true;
        $scope.searchModel = {};
        $scope.searchModel.search_text = '';

        $scope.advanced_search = {};
        $scope.advanced_search.enableSearch = false;
        $scope.advanced_search.title = '';
        $scope.advanced_search.active = false;
        $scope.advanced_search.isCollapsed = false;
        $scope.advanced_search.heading = 'Advanced Search Result:';

        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        $scope.advanced_search.totalKeyArea = 0;
        $scope.advanced_search.taxonomy = [];
        

        $scope.contents = {};
        //Call Assets
        $scope.currentPage = 1;
        $scope.pageNumber = '';
        $scope.itemsPerPage = '';
        $scope.project_type=5;
        $scope.is_global=1;
        $scope.parent_learning_object_name='';
        $scope.removeErrors=1;
        $scope.getLOContents = function (pageNumber) {
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            assetService.getLOContents($scope).then(function (data) {
                $scope.parent_learning_object_name=data.data.learning_object.parent_learning_object.detail.name;
                $scope.contents = data.data.learning_object.childs;
                $scope.total = data.data.total;
                $scope.removeErrors = data.data.total;
            });
        };
        $scope.getLOContents(1);
        $scope.newPageNumber = 1;
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerContent = function (newPage) {
            $scope.newPageNumber = newPage;            
            $scope.getLOContents(newPage);
        };
        
        
        $scope.openGlobalLOChildAddModal = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/add_global_child_learning_object.html',
                controller: 'ModalGlobalChildCtrl',
                size: 'lg',
                backdrop: 'static',
                keyboard: false,    
                resolve: {
                    parent_learning_object_id: function () {
                        return $scope.object_id
                    },
                    item: function () {
                        return ''
                    }
                }
            });
            modalInstance.result.then(function () {
               $scope.getLOContents(1);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        
        /*
         * @description add Global content
         * @author Koushik Samanta  
         * @param {type} size
         * @returns {undefined}
         */
        $scope.editGlobalContentObjectForm = function (list, size) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/edit_global_content_object.html',
                controller: 'ModalGlobalContentObjectAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    list: function () {
                        return list
                    },
                    newPageNumber: function () {
                        return $scope.newPageNumber
                    },
                    modal_header1_lebel: function () {
                        return 'Update Content'
                    },
                    modal_header2_lebel: function () {
                        return 'You are trying to update content'
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {                
                $scope.getLOContents(1)
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        
       $scope.deleteContent = function (id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this content!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    var param={};
                    param.id=id;
                    param.parent_learning_object_id=$scope.object_id;
                    assetService.delContent(param).then(function (data) {
                        var response = data.status;
                        if(response == 200) {
                            $scope.getLOContents(1);
                            SweetAlert.swal("Success", "Content has been deleted", "success");
                        } else {
                            SweetAlert.swal("Error", "Something went wrong", "error");
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Content is safe :)", "error");
                }
            });
        };

        $scope.cancel = function () {
            $('.right-panel-navpanel').hide();
            $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-12');
        };  
        
        $scope.backGlobalContentObject=function(){
             $window.location.href ='#/global_repo/ContentObject';
        };
        
        $scope.previewContent = function (size, id) {
            if($scope.disablePreviewClick==false){
                var contentRec = {};
                $scope.disablePreviewClick = true;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/toc/globalModalContentView.html',
                    controller: 'ModalInstanceContentCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        object_id: function () {
                            return id;
                        },
                        modalFormText: function () {
                            return "You are trying to view a content";
                        },
                        modalFormTile: function () {
                            return "Preview Content";
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    $scope.disablePreviewClick = false;
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }            
        };
        $scope.previewExtResource = function(content){
            $window.open(content.metadata.ext_res_url, '_blank');
        };
         //Go to Desired editor
        $scope.goToEditor = function (list) {
            //console.log(list);
            $scope.project_id = list.project_id;            
            tocService.checkObjectLock($scope).then(function (data) {
                if (Number(data.current_lock)) {
                    SweetAlert.swal("Cancelled", "You cannot access this content right now, as it is locked. Please try again later!", "error");
                    return false;
                } else {
                    if (list.project_type_id == 4) {
                        //Edupub
                        $window.location.href = "edupub_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + list.project_id + '&node_id=' + list.id + '&node_title=' + encodeURIComponent(list.name) + '&node_type_name=&node_type_id=&project_type_id=' + list.project_type_id + '&section_id=&user_id=' + $scope.user_id+'&content_from=global_repo_lo_content&lo_parent_object_id=' + $scope.object_id;
                    } else if (list.project_type_id == 5) {
                        //Course
                        if(list.object_type_id=='101'){
                            $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + list.project_id + '&node_id=' + list.id + '&node_title=' + encodeURIComponent(list.name) + '&node_type_name=&node_type_id=&project_type_id=' + list.project_type_id + '&section_id=&user_id=' + $scope.user_id+'&content_from=global_repo_lo_content&lo_parent_object_id=' + $scope.object_id;
                        }else if(list.object_type_id=='118'){
                            $window.location.href = "/#/global-lo-content/"+list.id+"?access_token=" + sessionService.get('access_token') + '&section_id=&user_id=' + $scope.user_id;
                        }
                    }
                }
            });
        }
}]);

//ModalGlobalChildAddCtrl
app.controller('ModalGlobalChildCtrl', function ($scope, sessionService, $modalInstance, assetService, patternService, templateService, $filter, tagService, keyValueService, taxonomyService, $compile, SweetAlert, ivhTreeviewMgr, parent_learning_object_id, item, objectService, $modal) {
    
    $scope.parent_learning_object_id = parent_learning_object_id;
    $scope.project_type = 5; //For Online Course type Project
    $scope.template_id = ''; 
    $scope.selected_template={};
    $scope.item = item;
    $scope.object_name='';
    $scope.section_page='content_screen';
    $scope.isConfirmClick=false;
    $scope.is_assessment=false;
    
    $scope.object_name_placeholder = 'Name of  Content Screen';
    $scope.updateObjectNameLabel = function(label, section_page){
        $scope.object_name_placeholder = 'Name of '+label;
        
        $scope.is_assessment=false;
        $scope.section_page = section_page;
        if($scope.section_page=='assessment'){
            $scope.is_assessment=true;
        }
    }
        
    //Get All templates
    $scope.template_status = 1001;
    $scope.template_local_global = {};
    $scope.template_local_global.local = false;
    $scope.template_local_global.global = true;
    templateService.getTemplate($scope).then(function (data) {
        $scope.templates = data.data;
    });
    
    $scope.showedit=false;
    if(angular.isDefined($scope.item.id) && $scope.item.id!=''){
        $scope.showedit=true;
        $scope.object_id=$scope.item.id;
        $scope.object_name=$scope.item.name;  
        $scope.section_page=$scope.item.section_page;
    }
    
    $scope.deleteAsset = function () {
        $scope.frost_resource_asset_name = '';
        $scope.ext_res_url = '';
        $scope.assessment_id = '';
        $scope.assessment_details = '';
       
        $scope.ext_res_type = "";
        $scope.ext_res_url = "";
        $scope.ext_res_title = '';
        $scope.asset_id = "";
    };

    $scope.onChangeResourceType = function () {
        $scope.ext_res_url = "";
        $scope.ext_res_title = null;
        $scope.formError['external_resources_message'] = '';
        $scope.formError['resource_asset_message'] = '';
    };

    $scope.deleteAssessment = function () {
        $scope.ext_res_url = '';
        $scope.assessment_id = '';
        $scope.assessment_details = '';
       
        $scope.ext_res_type = "";
        $scope.ext_res_url = "";
        $scope.ext_res_title = '';
        $scope.asset_id = "";
    };

    $scope.onChangeAssessmentType = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = {};
        $scope.formError['assessment_id'] = '';
    };
    
    //Set Global Project Id
    $scope.project_type_id = $scope.project_type;
    objectService.getGlobalProjectId($scope).then(function (data) {
        $scope.global_project_id = data.global_project_id;
    });
    
    
    
    //Add Global Toc content
    $scope.add = function () {
        $scope.formError = {object_name: '', project_type: '', templete_id: ''};
        var param = {};
        param.object_name = $scope.object_name;
        param.project_type = $scope.project_type;
        param.templete_id = '';
        param.section_page=$scope.section_page;
        param.parent_learning_object_id=$scope.parent_learning_object_id;
        $scope.isConfirmClick=true;
        if ($scope.project_type == 5) {
            //For Online course 
            param.templete_id = (angular.isUndefined($scope.selected_template) ? '' : $scope.selected_template.id);
        }

        assetService.addGlobalContentObject($scope, param).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                SweetAlert.swal("Success", savedResponse.message, "success");
                $modalInstance.close();
            } else {
                if (angular.isDefined(savedResponse.error.object_name)) {
                    $scope.formError['object_name'] = savedResponse.error.object_name['0'];
                }
                if (angular.isDefined(savedResponse.error.project_type)) {
                    $scope.formError['project_type'] = savedResponse.error.project_type['0'];
                }
                if (angular.isDefined(savedResponse.error.templete_id)) {
                    $scope.formError['templete_id'] = savedResponse.error.templete_id['0'];
                }
                if (angular.isDefined(savedResponse.error.assessment_source)) {
                    $scope.formError['assessment_source'] = savedResponse.error.assessment_source['0'];
                }
                if (angular.isDefined(savedResponse.error.assessment_id)) {
                    $scope.formError['assessment_id'] = savedResponse.error.assessment_id['0'];
                }
                if (angular.isDefined(savedResponse.error.ext_res_type)) {
                    $scope.formError['ext_res_type'] = savedResponse.error.ext_res_type['0'];
                }
                if (angular.isDefined(savedResponse.error.asset_id)) {
                    $scope.formError['asset_id'] = savedResponse.error.asset_id['0'];
                }
                if (angular.isDefined(savedResponse.error.resource_asset_message)) {
                    $scope.formError['resource_asset_message'] = savedResponse.error.resource_asset_message['0'];
                }
                if (angular.isDefined(savedResponse.error.external_resources_message)) {
                    $scope.formError['external_resources_message'] = savedResponse.error.external_resources_message['0'];
                }
                $scope.isConfirmClick=false;
            }            
        });
    };
    
    //Edit Global Toc content
    $scope.edit = function () {
        $scope.formError = {object_name: '', project_type: '', templete_id: ''};
        var param = {};
        param.object_name = $scope.object_name;
        param.object_id = $scope.object_id;
        param.project_id = list.project_id;  
        $scope.tags = $scope.metadata.tagValue;
        assetService.editGlobalContentObject($scope, param).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                SweetAlert.swal("Success", savedResponse.message, "success");
                $modalInstance.close({newPageNumber: $scope.newPageNumber});
            } else {
                if (angular.isDefined(savedResponse.error.object_name)) {
                    $scope.formError['object_name'] = savedResponse.error.object_name['0'];
                }
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.set_section_page=function(project_type){
        if(project_type=='5'){
           $scope.section_page='content_screen';
        }else if(project_type=='4'){
            $scope.section_page='';
        }
    };
    
    $scope.addAssessmentModal = function () {
        var size = 'lg';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/assessmentModalContent.html',
            controller: 'ModalTocAssessmentCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                project_id: function () {
                    return $scope.global_project_id;
                },
                assessment_id: function () {
                    return $scope.assessment_id;
                },
                assessment_details: function () {
                    return $scope.assessment_details;
                },
                is_global_project: function () {
                    return true;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.assessment_details = modalCloseParameter.assessment_details;
            $scope.assessment_id = modalCloseParameter.assessment_details.id;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.asset_id = '';
        });
    };
    
    //Assets Modal 
    $scope.ext_res_title = null;
    $scope.addAssestModal = function () {
        var size = 'lg';
        var modal_form_title = 'Asset Library';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/externalResourceModalContent.html',
            controller: 'ModalCourseAssetsCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                project_id: function () {
                    return $scope.global_project_id;
                },
                ext_res_url: function () {
                    return $scope.ext_res_url;
                },
                from_global_repo: function(){
                    return true;
                }
            }
        });

        modalInstance.result.then(function (modalCloseParameter) {
            $scope.ext_res_url = modalCloseParameter.ext_res_location;
            $scope.ext_res_title = modalCloseParameter.ext_res_title;
            $scope.assessment_id = '';
            $scope.assessment_source = '';
            $scope.asset_id = modalCloseParameter.asset_id;
            $scope.set_asset_thumbnail();
            //$scope.ext_res_type = modalCloseParameter.ext_res_type;
        });
    };
    $scope.set_asset_thumbnail = function () {
        $scope.frost_resource_asset_type = '';
        $scope.frost_resource_asset_name = '';
        if (angular.isDefined($scope.ext_res_url) && $scope.ext_res_url != '') {
            var pp = $scope.ext_res_url.split('/');
            if (angular.isDefined(pp[6]) && pp[6] != '') {
                assetService.getAssetById(pp[6]).then(function (data) {
                    //console.log(data.data);
                    $scope.frost_resource_asset_type = data.data.object_type_name;
                    $scope.frost_resource_asset_name = data.data.name;
                });
            }
        }
    }
    
});


