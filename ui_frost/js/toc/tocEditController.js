/*****
 * Controller for TOC
 * Author - Arikh Akher 
 ***/

'use strict';

app.controller('toc_edit', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$rootScope', '$modal', '$log', 'objectService', 'SweetAlert', '$window', '$filter', '$timeout', '$controller', '$interval', 'tocNodeRuleService', function ($scope, sessionService, tocService, $location, $routeParams, $rootScope, $modal, $log, objectService, SweetAlert, $window, $filter, $timeout, $controller, $interval, tocNodeRuleService) {
        //jQuery(".live .ui-sortable").nestedSortable({handle: '.toggle'});        
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.user_id = userinfo.id;
        $scope.project_id = $routeParams.project_id;
        $scope.objArr = new Array();
        $scope.outerIndex = 0;
        $scope.show_lock_btn = 0;
        // Fetch Toc Types  
        $scope.TocTypes = '{}';
        $scope.exclude_types = '';
        $scope.trigger = 0;


        $scope.lock_project = function () {
            $scope.trigger = 1;
            tocService.lockEntry($scope).then(function (data) {
                $scope.project_details.is_locked = Number(data.current_lock);
            });
        }
        //PDF-HTML Condition to show download imscc button/pdf to html        
        $scope.show_download_imscc = false;
        $scope.download_imscc_link = '';
        $scope.loadToc = function () {
            tocService.getTocTypes($scope).then(function (TocTypes) {
                $scope.TocTypes = TocTypes;

                // Fetch Object Details
                objectService.getProjectDetails($scope).then(function (data) {
                    if (!angular.isObject(data)) {
                        $scope.show_project_error_msg = true;
                        return true;
                    }
                    else if (data.is_global_project) {
                        $scope.show_project_error_msg = true;
                        return true;
                    }
                    $scope.check_license_validity=true;
                    $scope.check_update_availability=true;
                    tocService.getProjectObjectStatus($scope, '').then(function (projObjStatusResponse) {                        
                        $scope.projObjStatus = projObjStatusResponse.projObjStatus;
                        //console.log($scope.projObjStatus);
                        tocService.getTicketStatusCount($scope).then(function (TocStatusCountResponse) {
                            //console.log(TocStatusCountResponse);
                            //var TocStatusCount = {};
                            //TocStatusCount = TocStatusCountResponse.ticket_status_count
                            $scope.ticket_status_count = TocStatusCountResponse.TocStatusCount.ticket_status_count;
                            $scope.TicketTotalCountsByStatus = TocStatusCountResponse.TicketTotalCountsByStatus;
                            $scope.total_ticket = TocStatusCountResponse.total_ticket;
                            $scope.update_date_obj = {};
                            $scope.proj_obj_status = {};
                            angular.forEach($scope.projObjStatus, function (key, value) {
                                $scope.proj_obj_status[value] = key.progress_status_value;
                            });
                            angular.forEach($scope.ticket_status_count, function (key, value) {
                                $scope.update_date_obj[value] = key.object_updated_at;
                            });
                            $scope.project_details = $scope.project = data;
                            //$rootScope.project_export_count = data.project_export_count;

                            if ($scope.project_details.created_by == $scope.user_id || $scope.user_id == 1) {
                                $scope.show_lock_btn = 1;
                            }

                            if ($scope.project_details.created_by != $scope.user_id && $scope.project_details.is_locked == 1 && $scope.user_id != 1) {
                                SweetAlert.swal("Cancelled", "You cannot access this project right now as it is locked!", "error");
                                $rootScope.goto("dashboard");
                            }

                            $scope.data = JSON.parse(data.toc);
                            console.info($scope.ticket_status_count);
                            console.info($scope.data);


                            //Project Type Specific Controller
                            var dst = {};
                            if ($scope.project_details.project_type_id == '1') {
                                angular.extend(dst, $controller('pxe', {$scope: $scope}));
                            } else if ($scope.project_details.project_type_id == '2') {
                                angular.extend(dst, $controller('lm', {$scope: $scope}));
                            } else if ($scope.project_details.project_type_id == '3') {
                                angular.extend(dst, $controller('vc', {$scope: $scope}));
                            } else if ($scope.project_details.project_type_id == '4') {
                                angular.extend(dst, $controller('edupub', {$scope: $scope}));
                            } else if ($scope.project_details.project_type_id == '5') {
                                angular.extend(dst, $controller('course', {$scope: $scope}));
                            }
                            $scope.toc_node_rule = tocNodeRuleService.toc_node_rule($scope);
                            //console.log($scope.toc_node_rule);
                            //======//

                        });
                    });


                    //$scope.project_details = data;
                    //$scope.data = JSON.parse(data.toc);
                    $scope.tocRowHoverClass = '';
                    //                    /$scope.project = data.project_permissions;//{'project_permissions': data.project_permissions};

                    if (data.toc_creation_status == 3001) {
                        $rootScope.show_toc = 1;
                    } else {
                        $rootScope.show_toc = 0;
                    }

                    /*tocService.getTicketStatusCount($scope).then(function () {
                     objectService.getTocObjectStatus($scope, 0, 0).then(function (data) {
                     $scope.contentStatus = new Array();
                     $scope.contentStatus = data.status_list;
                     //console.info($scope.contentStatus);
                     });
                     });*/
                    var import_package_info = angular.fromJson(data.import_package_info);
                    $scope.asset_import_info = "<ul>";
                    angular.forEach(import_package_info, function (val, key) {
                        if (key == 'audio_counter' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of audios imported:' + val + '</li>';
                        }
                        if (key == 'video_counter' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of video(s) imported:' + val + '</li>';
                        }
                        if (key == 'image_counter' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of image(s) imported:' + val + '</li>';
                        }
                        if (key == 'html_count' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of page(s) imported:' + val + '</li>';
                        }
                        if (key == 'glossary_counter' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of glossary(s) imported:' + val + '</li>';
                        }
                        if (key == 'widget_counter' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of widget(s) imported:' + val + '</li>';
                        }
                        if (key == 'gadget_counter' && val !== 0) {
                            $scope.asset_import_info += '<li>Number of gadget(s) imported:' + val + '</li>';
                        }

                        //PDF-HTML Condition to show download imscc button/pdf to html
                        if (key == 'mime' && val.indexOf('pdf') !== -1) {
                            $scope.show_download_imscc = true;
                            $scope.download_imscc_link = serviceEndPoint + 'api/v1/pdf-imscc-download/' + $scope.project_id;
                        }
                    });
                    $scope.asset_import_info += "</ul>";
                    if (data.import_package_info_status == 1002) {
                        $scope.show_success_msg = 1;
                        tocService.updateImportStatus($scope);
                    }


                });
            });
        }
        $scope.loadToc();
        angular.extend(this, $controller('createproject', {$scope: $scope}));
        $scope.$watch('leftPaneHeight()', function (val) {
            $scope.tempVal = val;
            $timeout(function () {
                $scope.rightPaneLinkHeight = $scope.tempVal;
            }, 1000, false);
        });

        $scope.leftPaneHeight = function () {
            return jQuery(".toc-panel-left").css("height");
        }

        //$scope.project = "{}";
        /*$scope.$watch('project', function (newVal, oldValue) {
         //console.log(newVal);
         if(angular.isDefined(newVal) && angular.isDefined(newVal.id)){
         console.info(newVal.id);
         $scope.project = newVal;     
         }                   
         });*/

        // Fetch Toc and orgToc
        /*tocService.getToc($scope).then(function(data) {
         $scope.data = data;
         });*/

        $scope.exportHistory = function (size, pid) {
            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/project/modal_exportlist.html',
                controller: 'ModalExportHistory',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    project_id: function () {
                        return pid;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };

        $scope.data = {
            children: [{
                    id: guid('-'),
                    title: "Add here..",
                    type: '2',
                    root_type: '2',
                    children: []
                }]
        };

        $scope.toggleMinimized = function (child) {
            child.minimized = !child.minimized;
        };

        $scope.remove = function (child, parent_node_id) {
            var toc = {};
            function walk(target) {
                var children = target.children, i;
                if (children) {
                    i = children.length;
                    while (i--) {
                        if (children[i] === child) {
                            //Delete Starts
                            toc = {
                                children: [children[i]]
                            };

                            //console.log(node_ids);
                            return children.splice(i, 1);
                        } else {
                            walk(children[i]);
                        }
                    }
                }
            }

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this node!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    //console.log(child);
                    jQuery.when(walk($scope.data)).done(function () {
                        tocService.delete_toc_node($scope, toc, child, parent_node_id).then(function (data) {
                            if (data.status == 200) {
                                SweetAlert.swal("Success", data.message, "success");
                                tocService.getTicketStatusCount($scope).then(function (TocStatusCountResponse) {
                                    //var TocStatusCount = {};
                                    //TocStatusCount = TocStatusCountResponse.ticket_status_count
                                    $scope.ticket_status_count = TocStatusCountResponse.TocStatusCount.ticket_status_count;
                                    $scope.TicketTotalCountsByStatus = TocStatusCountResponse.TicketTotalCountsByStatus;
                                    $scope.total_ticket = TocStatusCountResponse.total_ticket;
                                });
                            } else {
                                SweetAlert.swal("Cancelled", data.message, "error");
                                $scope.loadToc();
                            }
                            $scope.changeLockStatus();
                        });
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Node is safe :)", "error");
                }
            });
        };

        /*$scope.update = function (event, ui) {
            var root = event.target,
                    item = ui.item,
                    parent = item.parent(),
                    target = (parent[0] === root) ? $scope.data : parent.scope().child,
                    child = item.scope().child,
                    index = item.index();

            target.children || (target.children = []);

            function walk(target, child) {
                var children = target.children, i;
                if (children) {
                    i = children.length;
                    while (i--) {
                        if (children[i] === child) {
                            return children.splice(i, 1);
                        } else {
                            walk(children[i], child);
                        }
                    }
                }
            }
            walk($scope.data, child);
            target.children.splice(index, 0, child);
            
            console.log(child.id+' = '+child.section_page + ' = '+child.title);
            
            //Saving TOC
            tocService.rearrangeTocTree($scope);
        };*/

        $scope.external_link = function (url) {
            var win = window.open(url, '_blank');
            win.focus();
        }

        $scope.items = ['item1', 'item2', 'item3'];



        // Get TOC Type Details
        $scope.showTypedetails = function (type_id) {
            var found = $filter('getById')($scope.TocTypes, type_id);
            if (found) {
                return found.title;
            }
        }


        $scope.changeLockStatus = function (child) {
            /*$scope.setLock = 0;
             if (child) {
             $scope.object_id = child.id;
             $scope.flag_on = 1;
             $scope.flag_off = 0;
             }*/
            objectService.getTocObjectStatus($scope).then(function (data) {

                //$scope.object_status = data.object_status;
                $scope.objStatus = data.status_list;
                //$scope.objLockedUser = data.locked_by_user_list;
                //$scope.setLock = 1;
                //console.info(Object.keys($scope.objStatus).length);
            });
        };

        objectService.releaseLockStatusAuto($scope).then(function (data) {
            $scope.changeLockStatus();
        });

        // Go to TOC Content Editor
        $scope.gotoContentEditor = function (child, project_id, version_id) {
            //$scope.changeLockStatus(child);
            $scope.object_id = child.id;
            tocService.checkObjectLock($scope).then(function (data) {
                //console.info(Number(data.current_lock));                
                if (Number(data.current_lock)) {
                    SweetAlert.swal("Cancelled", "You cannot access this content right now, as it is locked. Please try again later!", "error");
                    $rootScope.goto("edit_toc/" + project_id);
                    return false;
                } else {
                    tocService.getProjectObjectStatus($scope, child.id).then(function (objStatus) {
                        //return false;
                        var type_title = '';//$scope.showTypedetails(child.type);
                        if ($scope.project_details.project_type_id == 2) {
                            //Learningmate Editor
                            $window.location.href = "EDITOR/express.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&project_type_id=' + $scope.project_details.project_type_id;
                        } else if ($scope.project_details.project_type_id == 1) {
                            //PXE Editor
                            //$location.url("toc_content?access_token=" + $scope.access_token + "&node_id=" + child.id + "&project_id=" + project_id + "&title=" + child.title + "&type=" + type_title + "&typeId=" + child.type);      
                            if (child.type == 101 || angular.lowercase(child.title) == 'glossary') {
                                $window.location.href = "simple_editor/glossary_view.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + child.title + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id + '&content_from=';
                            } else {
                                var vid = version_id ? '&version_id=' + version_id : "";
                                $window.location.href = "simple_editor/index.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + child.title + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id + vid + '&content_from=';
                            }

                        } else if ($scope.project_details.project_type_id == 3) {
                            //Visual or Bootstrap Editor
                            $window.location.href = "bootstrap_editor/index.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + child.title + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id;
                        } else if ($scope.project_details.project_type_id == 4) {
                            //Visual or Bootstrap Editor
                            if (child.type == 101 && angular.lowercase(child.title) == 'glossary') {
                                $window.location.href = "edupub_editor/glossary_view.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + encodeURIComponent(child.title) + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id + '&content_from=';
                            } else {
                                $window.location.href = "edupub_editor/index.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + encodeURIComponent(child.title) + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id + '&section_id=' + child.file.split("#")[1] + '&user_id=' + $scope.user_id + '&content_from=';
                            }


                        } else if ($scope.project_details.project_type_id == 5) {
                            //console.log(objStatus.projObjStatus);
                            //Course Editor
                            if ((objStatus.projObjStatus[child.id].progress_status == "1098" || objStatus.projObjStatus[child.id].progress_status == "1099")) {
                                $window.location.href = "pdf_editor/index.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + encodeURIComponent(child.title) + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id;
                            } else {
                                $window.location.href = "course_editor/index.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + encodeURIComponent(child.title) + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id + '&content_from=';
                            }

                        }
                    });

                }
            });

            //$scope.$watch('setLock', function (val) {
            //if (val && $scope.object_status == 1001) {

            //}
            //});
        }


        $scope.gotoPreview = function (child, project_id, version_id) {
            //var type_title = $scope.showTypedetails(child.type);
            var type_title = '';
            if ($scope.project_details.project_type_id == 1 || $scope.project_details.project_type_id == 4 || $scope.project_details.project_type_id == 5) { //for PXE editor 
                var vid = version_id ? '&version_id=' + version_id : "";
                $window.location.href = "content_preview/index.html?access_token=" + $scope.access_token + "&project_id=" + project_id + '&node_id=' + child.id + '&node_title=' + child.title + '&node_type_name=' + type_title + '&node_type_id=' + child.type + '&project_type_id=' + $scope.project_details.project_type_id + vid + '&user_id=' + $scope.user_id;
            }
        }


        $scope.getVersions = function (size, object) {

            $scope.pid = $routeParams.project_id;
            $scope.oid = object.id;
            objectService.get_version_list($scope).then(function (data) {
                $scope.versionList = data.data;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/project/modal_versions.html',
                    controller: 'ModalVersions',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        project_id: function () {
                            return $scope.project_id
                        },
                        object: function () {
                            return object;
                        },
                        versionList: function () {
                            return $scope.versionList;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });

            });

        };


        $scope.compareVersions = function (size, object) {

            $scope.pid = $routeParams.project_id;
            $scope.oid = object.id;
            objectService.get_version_list($scope).then(function (data) {
                $scope.versionList = data.data;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/project/compare_versions.html',
                    controller: 'CompareModalVersions',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        project_id: function () {
                            return $scope.project_id
                        },
                        object: function () {
                            return object;
                        },
                        versionList: function () {
                            return $scope.versionList;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });

            });

        };



        //        if ($rootScope.show_success_msg == 1) {
        //            setTimeout(function () {
        //                $rootScope.show_success_msg = 0;
        //            }, 5000);
        //        }

        $scope.copy_project = function () {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You are going to copy current project, after success it will appear in the project listing dashboard!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, copy it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    if (angular.isDefined($scope.project_id)) {
                        tocService.copyProject($scope).then(function (result) {
                            if (result.status == 400 && angular.isDefined(result.projectPermissionGranted) && result.projectPermissionGranted == 401) {
                                $rootScope.move_unauthorised_user_to_toc_with_message(result.projectPermissionGranted);
                                SweetAlert.swal("Cancelled", result.message, "warning");
                            }
                            if (result.status == 200) {
                                SweetAlert.swal("Success", "Project copied successfully", "success");
                                $scope.loadToc();
                            }
                        });
                    }
                } else {
                    SweetAlert.swal("Cancelled", "Copy is cancelled :)", "error");
                }
            });
        }

        $scope.searchReplace = function () {
            SweetAlert.swal({
                title: "Search Text",
                html: "<p><textarea id='search_text' class='form-control' autofocus></textarea>",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Search",
                cancelButtonText: "Cancel!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    if (angular.isDefined($scope.project_id) && $('#search_text').val()) {
                        $scope.search_text = $('#search_text').val();
                        $window.location.href = "/#/search_replace/" + $scope.project_id + "/" + $scope.search_text;
                    } else {
                        SweetAlert.swal({html: 'Please enter a search text'});
                    }
                } else {
                    SweetAlert.swal("Cancelled", "Replace is cancelled :)", "error");
                }
            });
        };
    }]);
//End of Toc Edit Controller

app.controller('ModalExportHistory', function ($scope, project_id, project_permissions, $modalInstance) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalVersions', function ($scope, $modalInstance, project_id, object, versionList, project_permissions) {
    $scope.versionList = versionList;
    $scope.project_permissions = project_permissions;
    $scope.versionListCount = $scope.versionList.length;
    $scope.object = object;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('CompareModalVersions', function ($scope, $modalInstance, project_id, object, versionList, project_permissions, tocService) {
    $scope.versionList = versionList;
    $scope.project_permissions = project_permissions;
    $scope.versionListCount = $scope.versionList.length;
    $scope.object = object;
    $scope.project_id = project_id;
    $scope.file1 = {};
    $scope.file2 = {};
    //var dmp = new diff_match_patch();

    $scope.compareFiles = function (file1, file2) {
        $scope.file1 = file1;
        $scope.file2 = file2;
        //console.info($scope.object);         //console.log(file2.version_id);
        tocService.getFileCompare($scope).then(function (data) {
            //var d = dmp.diff_main(data.file1_content, data.file2_content);
            //dmp.diff_cleanupSemantic(d);
            //var ds = dmp.diff_prettyHtml(d);

            //$scope.compare_result = data.data;
            jQuery('iframe#iframeId').contents().find('#outputDiv').html(data.data);
        });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


app.controller('ExportOption', function ($scope, $modalInstance, $controller, tocService, SweetAlert) {
    angular.extend(this, $controller('toc_edit', {$scope: $scope}));
    $scope.export_type = {
        option: 1
    };
    $scope.submit = function () {
        $("#export_btn").attr("disabled", true);
        if ($scope.export_type.option == '4') {
            $scope.project_details.project_type_id = 4;
            $scope.exportType = 'edupub';
        }


        tocService.initiateExport($scope).then(function (data) {

            if (data.status == 200) {
                var error_response = '';
                tocService.getTicketStatusCount($scope).then(function (data) {
                    if (parseInt(data.TicketTotalCountsByStatus['OPEN']) > 0 || parseInt(data.TicketTotalCountsByStatus['IN PROGRESS']) > 0) {
                        error_response += "--------------------------------------------------------";
                        error_response += "<div>Following are issues available in your package:</div>";
                    }
                    if (parseInt(data.TicketTotalCountsByStatus['OPEN'])) {
                        error_response += '<div>OPEN=>' + parseInt(data.TicketTotalCountsByStatus['OPEN']) + '</div>';
                    }
                    if (parseInt(data.TicketTotalCountsByStatus['IN PROGRESS'])) {
                        error_response += '<div> IN PROGRESS=>' + parseInt(data.TicketTotalCountsByStatus['IN PROGRESS']) + '</div>';
                    }
                });

                $modalInstance.close();
                setTimeout(function () {
                    SweetAlert.swal({title: 'Success', html: data.message + "<div style='width:100%;overflow-y:auto;max-height:250px;'>" + error_response + "</div>"});
                }, 500);
                //<div style='width:100%;overflow-y:auto;max-height:250px;'>" + error_response + "</div>"
                $scope.project.project_export_count = 1;
                //$rootScope.project_export_count = 1;
            }
            $("#export_btn").removeAttr("disabled");
        });
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('PartialExport', function ($scope, $modalInstance, $controller, SweetAlert, tocService, project_id, modalFormTile, modalFormText, cfi_domain_name, cfi_book_id, data, $rootScope) {
    $scope.project_id = project_id;
    $scope.modalFormTile = modalFormTile;
    $scope.modalFormText = modalFormText;
    $scope.cfi_domain_name = cfi_domain_name;
    $scope.cfi_book_id = cfi_book_id;
    $scope.data = data;
    $scope.selectedTocNode = {};
    $scope.resultTocNode = {};
    $scope.export = function () {
        var cnt = 0;
        angular.forEach($scope.selectedTocNode, function (v, k) {
            if (v === true) {
                $scope.resultTocNode[cnt] = k;
                cnt++;
            }
        });

        if (cnt < 1) {
            SweetAlert.swal("Please choose node to export", data.message, "error");
        } else {
            tocService.partialExportService($scope).then(function (data) {
                if (data.status == 200) {
                    $modalInstance.close();
                    SweetAlert.swal("Success", data.message, "success");
                } else {
                    $modalInstance.close();
                    SweetAlert.swal("Cancelled", data.message, "error");
                }
            });
        }
    };
    //$rootScope.$broadcast('SOME_TAG', $rootScope.project_export_count);
    //console.info($rootScope.project_export_count);
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


/*
 * GLobal Content Usage Modal window Controller
 */
app.controller('ModalContentObjectUsagCtrl', function ($scope, $location, $modalInstance, $window, SweetAlert, assetService, sessionService, objectService, asset_id) {
    $scope.asset_id = asset_id;

    $scope.asset_usage_list = [];
    //For pagination
    $scope.currentPage = 1;
    $scope.itemsPerPage = 10;
    //
    $scope.asset_usage_total = 0;
    $scope.getAssetUsageList = function (pageNumber) {
        $scope.pageNumber = pageNumber;
        assetService.getAssetUsageList($scope).then(function (data) {
            if (data.status == '200') {
                $scope.asset_usage_list = data.data.list;
                $scope.asset_usage_total = data.data.total;
            }
        });
    }
    $scope.getAssetUsageList(1);

    $scope.pageChangeHandlerObject = function (newPageNumber) {
        $scope.getAssetUsageList(newPageNumber);
    }

    $scope.goToProjectDashboard = function (list) {
        $scope.project_id = list.project_id;
        objectService.getProjectDetails($scope).then(function (data) {
            if (data != null) {
                $location.path("edit_toc/" + list.project_id);
            } else {
                SweetAlert.swal({html: 'Either the project does not exist or you are no longer allocated to this project.', width: 540, type: "warning"});
            }
        });
    }


    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});