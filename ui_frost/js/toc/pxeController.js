'use strict';

app.controller('pxe', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$rootScope', '$modal', '$log', 'objectService', 'SweetAlert', '$window', '$filter', '$timeout', '$controller', '$interval', function ($scope, sessionService, tocService, $location, $routeParams, $rootScope, $modal, $log, objectService, SweetAlert, $window, $filter, $timeout, $controller, $interval) {

        $scope.addNodeForm = function (size, child, modalFormText, node_type) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/pxeModalContent.html',
                controller: 'ModalPXEAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    node_type: function () {
                        return node_type;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                jQuery(".editor_button").attr("disabled", true);
                $scope.node_id = modalCloseParameter.node_id;
                $scope.child_node_title = modalCloseParameter.child_node_title;
                $scope.child_node_type = modalCloseParameter.child_node_type;
                $scope.section_page = modalCloseParameter.section_page;
                $scope.file = modalCloseParameter.file;
                $scope.applyMetadata = modalCloseParameter.applyMetadata;
                $scope.parent_node_id = modalCloseParameter.parent_node_id;
                $scope.parent_node_root_type = modalCloseParameter.parent_node_root_type;
                if ($scope.applyMetadata) {
                    $scope.tags = modalCloseParameter.tags;
                    $scope.key_data = modalCloseParameter.key_data;
                    $scope.value_data = modalCloseParameter.value_data;
                    $scope.taxonomy = modalCloseParameter.taxonomy;
                }
                //Saving TOC
                tocService.add_toc_node($scope).then(function (data) {
                    $scope.child_node_title = '';
                    $scope.pageAction = '';
                    $scope.section_page = '';
                    jQuery(".editor_button").attr("disabled", false);
                    $scope.changeLockStatus();
                });
                /*tocService.saveToc($scope).then(function () {
                 $scope.child_node_title = '';
                 $scope.pageAction = '';
                 });*/
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editChildForm = function (size, child, modalFormText, parent_node_id) {

            $scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/pxeModalContent.html',
                controller: 'ModalPXEEditCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return $scope.child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    parent_node_id: function () {
                        return parent_node_id;
                    },
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                //jQuery("#" + child.id).attr("disabled", true);
                jQuery(".editor_button").attr("disabled", true);
                $scope.node_id = modalCloseParameter.node_id;
                $scope.child_node_title = modalCloseParameter.child_node_title;
                $scope.section_page = modalCloseParameter.section_page;
                $scope.file = modalCloseParameter.file;
                $scope.parent_node_id = modalCloseParameter.parent_node_id;
                $scope.applyMetadata = modalCloseParameter.applyMetadata;
                if ($scope.applyMetadata) {
                    $scope.tags = modalCloseParameter.tags;
                    $scope.key_data = modalCloseParameter.key_data;
                    $scope.value_data = modalCloseParameter.value_data;
                    $scope.taxonomy = modalCloseParameter.taxonomy;
                }
                //Saving TOC
                tocService.edit_toc_node($scope).then(function (data) {
                    $scope.child_node_title = '';
                    $scope.pageAction = '';
                    $scope.section_page = '';
                    jQuery(".editor_button").attr("disabled", false);
                    $scope.changeLockStatus();
                    if (data.status == 400) {
                        SweetAlert.swal("Cancelled", "Content is locked so cannot be modified", "error");
                        $scope.loadToc();
                    }
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


app.controller('ModalPXEAddCtrl', function ($scope, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, node_type, tagService, keyValueService, taxonomyService, $compile) {
    $scope.child = child;
    $scope.node_title = '';
    $scope.node_type = '';
    $scope.node_id = 0;
    $scope.section_page = 'page';
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = 'Add Node';
    $scope.project_id = project_id;
    $scope.isNodeAddDisabled = false;
    $scope.showTocType = true;
    $scope.showSectionPage = true;
    $scope.showPartRadio = true;
    $scope.project_type_id = project_details.project_type_id;
    $scope.formError = {'node_title': ''}
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.applyMetadata = '';
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.metadata = {};
    if (project_permissions['tag.apply'].grant) {
        $scope.applyMetadata = 1;
    } else {
        $scope.applyMetadata = 0;
    }
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
            }
        }

    });
    // get tags 
    var tags = [];
    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                });
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });

    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row key-values-area" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div></div></div>';
        return keySelect;
    };


    //Decide Node type and section page according to project_type_id
    if ($scope.project_type_id == 1) {
        // Incase of PXE
        $scope.node_type = node_type;
        $scope.showTocType = false;
        if (node_type == 1) {
            //If it is frontmatter, donot show section_page radio button
            $scope.showSectionPage = false;
            $scope.section_page = 'page';
        }

        if (child.type == 5) {
            //You can add only page inside Part
            $scope.section_page = 'page';
            $scope.showSectionPage = false;
        }


    } else {
        $scope.node_type = '';
        $scope.showTocType = true;
    }

    $scope.choose_section_page = function (section_page) {
        $scope.section_page = section_page;
        if ($scope.section_page == 'section' && $scope.project_type_id != 1) {
            $scope.showTocType = true;
        } else {
            $scope.showTocType = false;
        }
    }
    // Fetch Toc Types  
    /*tocService.getTocTypes($scope).then(function(data) {
     //console.log($scope.TocTypes);
     //console.log($scope.exclude_types);
     //getSelectedTocTypes($scope.TocTypes, $scope.exclude_types);
     $scope.selected_option = $scope.TocTypes[$scope.node_type];
     });*/

    // Fetch Toc Types            
    tocService.getTocTypes($scope).then(function (data) {
        selectedKey = $scope.arrayIterate(data);
        $scope.selected_option = data[selectedKey];
    });
    //Array index find for selected type
    var selectedKey = '';
    $scope.arrayIterate = function (lists) {
        angular.forEach(lists, function (value, key) {
            if (value.id == $scope.node_type) {
                selectedKey = key;
            }
        });
        return selectedKey;
    }


    $scope.section_page_disabled = false;
    $scope.add = function (child) {
        $scope.isNodeAddDisabled = true;
        $scope.pageAction = 'ADD';

        objectService.getProjectDetails($scope).then(function (data) {
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {
                //var page_name = $scope.file = tocService.createFileName($scope);
                tocService.checkFileName($scope).then(function (data) {
                    if (data.status == 200 && ($scope.section_page == 'page' || $scope.section_page == 'section')) {
                        //Check node title and node type and section
                        if ((angular.isUndefined($scope.node_title) == false && $scope.node_title != '') && (($scope.section_page == 'section') || $scope.section_page == 'page')) {
                            //var checkPattern = /^([a-zA-Z0-9_.-]+\s?)*$/.test($scope.node_title);
                            $scope.file = $scope.node_title.allReplace({'\\s': '_', ':': '_', '-': '_', '\\.': '_'}) + '.xhtml';
                            $scope.file = $scope.file.toLowerCase();
                            var node_id = guid('-');
                            var node_type = '';//(angular.isDefined($scope.selected_option) ? $scope.selected_option.id : '');

                            //Adding part in bodymatter
                            if ($scope.section_page == 'section') {
                                node_type = 5; // NodeTypeId For Part
                            } else if ($scope.section_page == 'page') {
                                node_type = 6; // NodeTypeId For Chapter
                                if (angular.lowercase($scope.node_title) == 'glossary') {
                                    node_type = 101; //NodeTypeId For Glossary
                                }
                            }

                            //var page_name = tocService.createFileName($scope); //($scope.section_page == 'page' ? $scope.node_title + '.xhtml' : '');
                            child.children.push({
                                id: node_id,
                                title: $scope.node_title,
                                file: $scope.file, //$scope.node_title + '.xhtml', //node_id + '.xhtml',
                                type: node_type,
                                root_type: child.root_type,
                                section_page: $scope.section_page,
                                children: []
                            });

                            //console.log(child);

                            var modalCloseParameter = {node_id: node_id, child_node_title: $scope.node_title, child_node_type: node_type, section_page: $scope.section_page, 'file': $scope.file, 'parent_node_id': $scope.child.id, 'parent_node_root_type': child.root_type, 'applyMetadata': $scope.applyMetadata};
                            if ($scope.applyMetadata) {
                                modalCloseParameter.tags = $scope.metadata.tagValue;
                                modalCloseParameter.key_data = $scope.key_data;
                                modalCloseParameter.value_data = $scope.value_data;
                                modalCloseParameter.taxonomy = $scope.taxonomy;
                            }
                            $modalInstance.close(modalCloseParameter);

                        } else {
                            if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                            {
                                setTimeout(function () {
                                    angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                }, 100);
                            }
                            $scope.formError = {'node_title': '', 'node_type': ''};
                            if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                                $scope.formError['node_title'] = 'Only a-z, A-Z, 0-9, _, :, -, . are supported.';
                            }

                            /*if (angular.isDefined($scope.selected_option) && $scope.selected_option.id) {
                             $scope.formError['node_type'] = 'Please choose this field properly';
                             }*/

                        }
                    } else {
                        if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                        {
                            setTimeout(function () {
                                angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                            }, 100);
                        }
                        $scope.isNodeAddDisabled = false;
                        $scope.errMsg = (data.message != null) ? data.message : "";

                        if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                            $scope.formError['node_title'] = 'Only a-z, A-Z, 0-9, _, :, -, . are supported.';
                        } else {
                            $scope.formError['node_title'] = '';
                            if ($scope.errMsg) {
                                $scope.formError['node_title'] = $scope.errMsg;
                            }
                        }
                        //console.log(angular.isDefined($scope.selected_option));
                        //console.log($scope.selected_option.id)
                        $scope.formError['node_type'] = '';
                        /*if (!angular.isDefined($scope.selected_option) || !angular.isDefined($scope.selected_option.id)) {
                         $scope.formError['node_type'] = 'Please choose this field properly';
                         }*/

                    }
                });
            } else {
                alert('Permission denied');
            }

        });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


app.controller('ModalPXEEditCtrl', function ($scope, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, parent_node_id, tagService, keyValueService, taxonomyService, $compile, ivhTreeviewMgr) {
    $scope.child = child;
    $scope.node_title = child.title;
    $scope.node_type = child.root_type;
    $scope.node_id = child.id;
    $scope.section_page = child.section_page;
    $scope.modalFormText = modalFormText + ' ' + $scope.node_title + '. ';
    $scope.modalFormTile = 'Edit Node';
    $scope.project_id = project_id;
    $scope.showTocType = false;
    $scope.showSectionPage = false;
    $scope.project_type_id = project_details.project_type_id;

    $scope.formError = {'node_title': ''}
    $scope.metadata = {};
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.applyMetadata = '';
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

    if (project_permissions['tag.apply'].grant) {
        $scope.applyMetadata = 1;
    } else {
        $scope.applyMetadata = 0;
    }
    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.taxonomy, $scope.taxonomy);
            }
        }

    });
    // get tags 
    var tags = [];
    var object_tags = [];

    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.metadata.tagValue = object_tags;
                }
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                });

                setTimeout(function () {
                    // populate old key values
                    if (Object.keys(data.data.object_key_values).length)
                    {
                        angular.forEach(data.data.object_key_values, function (object_key_value) {
                            $scope.totalKeyArea = $scope.totalKeyArea + 1;
                            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                            $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                            $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                            if (object_key_value.key_type == 'select_list')
                            {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                            } else {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                            }

                        });
                    } else {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                    }
                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row key-values-area" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div></div></div>';
        return keySelect;
    };


    // Fetch Toc Types            
    tocService.getTocTypes($scope).then(function (data) {
        //$scope.selected_option = $scope.node_type;//$scope.TocTypes[$scope.node_type];  

        selectedKey = $scope.arrayIterate(data);
        $scope.selected_option = data[selectedKey];
    });

    //Array index find for selected type
    var selectedKey = '';
    $scope.arrayIterate = function (lists) {
        angular.forEach(lists, function (value, key) {
            if (value.id == $scope.node_type) {
                selectedKey = key;
            }
        });
        return selectedKey;
    }
    //selectedKey = $scope.arrayIterate($scope.list);
    //

    //section_page_disabled    
    $scope.section_page = $scope.child.section_page;
    $scope.section_page_disabled = true;
    if (angular.isUndefined($scope.child.section_page)) {
        $scope.section_page = 'section';
    }
    //==

    /*$scope.$watch('node_title', function (val) {
        if (angular.isUndefined(val) == true || val == '') {
            $scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'}
        } else {
            $scope.formError = {'node_title': ''}
        }
    });*/
    $scope.add = function (child) {
        $scope.pageAction = 'EDIT';

        objectService.getProjectDetails($scope).then(function (data) {
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {
                if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '') {

                    $scope.file = tocService.createFileName($scope);
                    tocService.checkFileName($scope).then(function (data) {
                        //if ((data.status == 200 && $scope.section_page == 'page') || $scope.section_page == 'section') {
                        if (data.status == 200) {
                            //console.log(angular.isUndefined($scope.node_title)+'=='+$scope.node_title+'=='+angular.isUndefined($scope.selected_option));
                            //if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '' && angular.isUndefined($scope.selected_option) == false) {
                            if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '') {
                                child.title = $scope.node_title;
                                var page_name = tocService.createFileName($scope);
                                child.file = page_name;//$scope.node_title + '.xhtml';
                                //child.type = $scope.selected_option.id;
                                //child.section_page = $scope.section_page;
                                //console.log(child);
                                var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title, section_page: $scope.section_page, 'file': page_name, 'parent_node_id': parent_node_id, 'applyMetadata': $scope.applyMetadata};
                                if ($scope.applyMetadata) {
                                    modalCloseParameter.tags = $scope.metadata.tagValue;
                                    modalCloseParameter.key_data = $scope.key_data;
                                    modalCloseParameter.value_data = $scope.value_data;
                                    modalCloseParameter.taxonomy = $scope.taxonomy;
                                }
                                $modalInstance.close(modalCloseParameter);
                            } else {
                                if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                                {
                                    setTimeout(function () {
                                        angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                    }, 100);
                                }
                                $scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'}
                            }
                        } else {
                            if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                            {
                                setTimeout(function () {
                                    angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                }, 100);
                            }
                            $scope.errMsg = (data.message != null) ? data.message : "";
                            $scope.formError = {'node_title': $scope.errMsg}
                        }
                    });
                } else {
                    if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                    {
                        setTimeout(function () {
                            angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                        }, 100);
                    }
                    $scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'}
                }
            } else {
                alert('Permission denied');
            }

        });

        /*tocService.checkFileName($scope).then(function (data) {
         if (data.status == 200) {
         if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '' && angular.isUndefined($scope.selected_option) == false) {
         child.title = $scope.node_title;
         child.file = $scope.node_title + '.xhtml';
         child.type = $scope.selected_option.id;
         
         var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title};
         $modalInstance.close(modalCloseParameter);
         }
         } else {
         alert(data.message);
         }
         });*/

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


});