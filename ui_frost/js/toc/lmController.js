'use strict';

app.controller('lm', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$rootScope', '$modal', '$log', 'objectService', 'SweetAlert', '$window', '$filter', '$timeout', '$controller', '$interval', function ($scope, sessionService, tocService, $location, $routeParams, $rootScope, $modal, $log, objectService, SweetAlert, $window, $filter, $timeout, $controller, $interval) {

        $scope.addNodeForm = function (size, child, modalFormText, node_type) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/lmModalContent.html',
                controller: 'ModalTocAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    node_type: function () {
                        return node_type;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                jQuery(".editor_button").attr("disabled", true);
                $scope.node_id = modalCloseParameter.node_id;
                $scope.child_node_title = modalCloseParameter.child_node_title;
                $scope.child_node_type = modalCloseParameter.child_node_type;
                $scope.section_page = modalCloseParameter.section_page;
                $scope.file = modalCloseParameter.file;
                $scope.parent_node_id = modalCloseParameter.parent_node_id;
                $scope.parent_node_root_type = modalCloseParameter.parent_node_root_type;
                //Saving TOC
                tocService.add_toc_node($scope).then(function (data) {
                    $scope.child_node_title = '';
                    $scope.pageAction = '';
                    $scope.section_page = '';
                    jQuery(".editor_button").attr("disabled", false);
                    $scope.changeLockStatus();
                });
                /*tocService.saveToc($scope).then(function () {
                 $scope.child_node_title = '';
                 $scope.pageAction = '';
                 });*/
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editChildForm = function (size, child, modalFormText, parent_node_id) {

            $scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/lmModalContent.html',
                controller: 'ModalTocEditCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return $scope.child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    parent_node_id: function () {
                        return parent_node_id;
                    },
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                //jQuery("#" + child.id).attr("disabled", true);
                jQuery(".editor_button").attr("disabled", true);
                $scope.node_id = modalCloseParameter.node_id;
                $scope.child_node_title = modalCloseParameter.child_node_title;
                $scope.section_page = modalCloseParameter.section_page;
                $scope.file = modalCloseParameter.file;
                $scope.parent_node_id = modalCloseParameter.parent_node_id;
                //Saving TOC
                tocService.edit_toc_node($scope).then(function (data) {
                    $scope.child_node_title = '';
                    $scope.pageAction = '';
                    $scope.section_page = '';
                    jQuery(".editor_button").attr("disabled", false);
                    $scope.changeLockStatus();
                    if (data.status == 400) {
                        SweetAlert.swal("Cancelled", "Content is locked so cannot be modified", "error");
                        $scope.loadToc();
                    }
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


app.controller('ModalTocAddCtrl', function ($scope, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, node_type) {
    $scope.child = child;
    $scope.node_title = '';
    $scope.node_type = '';
    $scope.section_page = 'page';
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = 'Add Node';
    $scope.project_id = project_id;
    $scope.isNodeAddDisabled = false;
    $scope.showTocType = true;
    $scope.showSectionPage = true;
    $scope.showPartRadio = true;
    $scope.project_type_id = project_details.project_type_id;
    $scope.formError = {'node_title': ''}

    //Decide Node type and section page according to project_type_id
    if ($scope.project_type_id == 1) {
        // Incase of PXE
        $scope.node_type = node_type;
        $scope.showTocType = false;
        if (node_type == 1) {
            //If it is frontmatter, donot show section_page radio button
            $scope.showSectionPage = false;
            $scope.section_page = 'page';
        }

        if (child.type == 5) {
            //You can add only page inside Part
            $scope.section_page = 'page';
            $scope.showSectionPage = false;
        }


    } else {
        $scope.node_type = '';
        $scope.showTocType = true;
    }

    $scope.choose_section_page = function (section_page) {
        $scope.section_page = section_page;
        if ($scope.section_page == 'section' && $scope.project_type_id != 1) {
            $scope.showTocType = true;
        } else {
            $scope.showTocType = false;
        }
    }
    // Fetch Toc Types  
    /*tocService.getTocTypes($scope).then(function(data) {
     //console.log($scope.TocTypes);
     //console.log($scope.exclude_types);
     //getSelectedTocTypes($scope.TocTypes, $scope.exclude_types);
     $scope.selected_option = $scope.TocTypes[$scope.node_type];
     });*/

    // Fetch Toc Types            
    tocService.getTocTypes($scope).then(function (data) {
        selectedKey = $scope.arrayIterate(data);
        $scope.selected_option = data[selectedKey];
    });
    //Array index find for selected type
    var selectedKey = '';
    $scope.arrayIterate = function (lists) {
        angular.forEach(lists, function (value, key) {
            if (value.id == $scope.node_type) {
                selectedKey = key;
            }
        });
        return selectedKey;
    }


    $scope.section_page_disabled = false;
    $scope.add = function (child) {
        $scope.isNodeAddDisabled = true;
        $scope.pageAction = 'ADD';
        objectService.getProjectDetails($scope).then(function (data) {
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {
                var page_name = $scope.file = tocService.createFileName($scope);
                tocService.checkFileName($scope).then(function (data) {
                    if (data.status == 200 && ($scope.section_page == 'page' || $scope.section_page == 'section')) {
                        //Check node title and node type and section
                        if ((angular.isUndefined($scope.node_title) == false && $scope.node_title != '') && ((angular.isUndefined($scope.selected_option) == false && $scope.section_page == 'section') || $scope.section_page == 'page')) {

                            var node_id = guid('-');
                            var node_type = (angular.isDefined($scope.selected_option) ? $scope.selected_option.id : '');

                            //Adding part in bodymatter
                            if ($scope.section_page == 'section') {
                                node_type = 5; // NodeTypeId For Part
                            } else if ($scope.section_page == 'page') {
                                node_type = 6; // NodeTypeId For Chapter
                                if (angular.lowercase($scope.node_title) == 'glossary') {
                                    node_type = 101; //NodeTypeId For Glossary
                                }
                            }

                            //var page_name = tocService.createFileName($scope); //($scope.section_page == 'page' ? $scope.node_title + '.xhtml' : '');
                            child.children.push({
                                id: node_id,
                                title: $scope.node_title,
                                file: page_name, //$scope.node_title + '.xhtml', //node_id + '.xhtml',
                                type: node_type,
                                root_type: child.root_type,
                                section_page: $scope.section_page,
                                children: []
                            });

                            //console.log(child);

                            var modalCloseParameter = {node_id: node_id, child_node_title: $scope.node_title, child_node_type: node_type, section_page: $scope.section_page, 'file': page_name, 'parent_node_id': $scope.child.id, 'parent_node_root_type': child.root_type};
                            $modalInstance.close(modalCloseParameter);

                        } else {
                            $scope.formError = {'node_title': '', 'node_type': ''};
                            if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                                $scope.formError['node_title'] = 'Please fill out this field properly';
                            }

                            if (angular.isDefined($scope.selected_option) && $scope.selected_option.id) {
                                $scope.formError['node_type'] = 'Please choose this field properly';
                            }

                        }
                    } else {
                        $scope.isNodeAddDisabled = false;
                        $scope.errMsg = (data.message != null) ? data.message : "";

                        if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                            $scope.formError['node_title'] = 'Please fill out this field properly';
                        } else {
                            $scope.formError['node_title'] = '';
                            if ($scope.errMsg) {
                                $scope.formError['node_title'] = $scope.errMsg;
                            }
                        }
                        //console.log(angular.isDefined($scope.selected_option));
                        //console.log($scope.selected_option.id)
                        $scope.formError['node_type'] = '';
                        if (!angular.isDefined($scope.selected_option) || !angular.isDefined($scope.selected_option.id)) {
                            $scope.formError['node_type'] = 'Please choose this field properly';
                        }

                    }
                });
            } else {
                alert('Permission denied');
            }

        });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


app.controller('ModalTocEditCtrl', function ($scope, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, parent_node_id) {
    $scope.child = child;
    $scope.node_title = child.title;
    $scope.node_type = child.root_type;
    $scope.node_id = child.id;
    $scope.section_page = child.section_page;

    $scope.modalFormText = modalFormText + ' ' + $scope.node_title + '. ';
    $scope.modalFormTile = 'Edit Node';
    $scope.project_id = project_id;
    $scope.showTocType = false;
    $scope.showSectionPage = false;
    $scope.project_type_id = project_details.project_type_id;

    $scope.formError = {'node_title': ''}

    // Fetch Toc Types            
    tocService.getTocTypes($scope).then(function (data) {
        //$scope.selected_option = $scope.node_type;//$scope.TocTypes[$scope.node_type];  

        selectedKey = $scope.arrayIterate(data);
        $scope.selected_option = data[selectedKey];
    });

    //Array index find for selected type
    var selectedKey = '';
    $scope.arrayIterate = function (lists) {
        angular.forEach(lists, function (value, key) {
            if (value.id == $scope.node_type) {
                selectedKey = key;
            }
        });
        return selectedKey;
    }
    //selectedKey = $scope.arrayIterate($scope.list);
    //

    //section_page_disabled    
    $scope.section_page = $scope.child.section_page;
    $scope.section_page_disabled = true;
    if (angular.isUndefined($scope.child.section_page)) {
        $scope.section_page = 'section';
    }
    //==

    $scope.$watch('node_title', function (val) {
        if (angular.isUndefined(val) == true || val == '') {
            $scope.formError = {'node_title': 'Please fill out this field properly'}
        } else {
            $scope.formError = {'node_title': ''}
        }
    });
    $scope.add = function (child) {
        $scope.pageAction = 'EDIT';

        objectService.getProjectDetails($scope).then(function (data) {
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {

                $scope.file = tocService.createFileName($scope);
                tocService.checkFileName($scope).then(function (data) {
                    //if ((data.status == 200 && $scope.section_page == 'page') || $scope.section_page == 'section') {
                    if (data.status == 200) {
                        //console.log(angular.isUndefined($scope.node_title)+'=='+$scope.node_title+'=='+angular.isUndefined($scope.selected_option));
                        //if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '' && angular.isUndefined($scope.selected_option) == false) {
                        if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '') {
                            child.title = $scope.node_title;
                            var page_name = tocService.createFileName($scope);
                            child.file = page_name;//$scope.node_title + '.xhtml';
                            //child.type = $scope.selected_option.id;
                            //child.section_page = $scope.section_page;
                            //console.log(child);
                            var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title, section_page: $scope.section_page, 'file': page_name, 'parent_node_id': parent_node_id};
                            $modalInstance.close(modalCloseParameter);
                        } else {
                            $scope.formError = {'node_title': 'Please fill out this field properly'}
                        }
                    } else {
                        $scope.errMsg = (data.message != null) ? data.message : "";
                        $scope.formError = {'node_title': $scope.errMsg}
                    }
                });
            } else {
                alert('Permission denied');
            }

        });

        /*tocService.checkFileName($scope).then(function (data) {
         if (data.status == 200) {
         if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '' && angular.isUndefined($scope.selected_option) == false) {
         child.title = $scope.node_title;
         child.file = $scope.node_title + '.xhtml';
         child.type = $scope.selected_option.id;
         
         var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title};
         $modalInstance.close(modalCloseParameter);
         }
         } else {
         alert(data.message);
         }
         });*/

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


});