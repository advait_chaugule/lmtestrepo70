'use strict';

app.controller('edupub', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$rootScope', '$modal', '$log', 'objectService', 'SweetAlert', '$window', '$filter', '$timeout', '$controller', '$interval', 'assetService', function ($scope, sessionService, tocService, $location, $routeParams, $rootScope, $modal, $log, objectService, SweetAlert, $window, $filter, $timeout, $controller, $interval, assetService) {

        $scope.update = function (event, ui) {
            var root = event.target,
                    item = ui.item,
                    parent = item.parent(),
                    target = (parent[0] === root) ? $scope.data : parent.scope().child,
                    child = item.scope().child,
                    index = item.index();

            target.children || (target.children = []);

            function walk(target, child) {
                var children = target.children, i;
                if (children) {
                    i = children.length;
                    while (i--) {
                        if (children[i] === child) {
                            return children.splice(i, 1);
                        } else {
                            walk(children[i], child);
                        }
                    }
                }
            }
            walk($scope.data, child);
            target.children.splice(index, 0, child);            
            //Saving TOC
            tocService.rearrangeTocTree($scope);
        };
        
        $scope.addNodeForm = function (size, child, modalFormText, node_type, parent_node_type) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/edupubModalContent.html',
                controller: 'ModalEdupubAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    node_type: function () {
                        return node_type;
                    },
                    parent_node_type: function () {
                        return parent_node_type;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                if (modalCloseParameter.reuse_modal) {
                    $scope.open_existing_content_modal('lg', modalCloseParameter.child, modalCloseParameter.project_id, modalCloseParameter.project_type_id);
                } else {
                    jQuery(".editor_button").attr("disabled", true);
                    $scope.node_id = modalCloseParameter.node_id;
                    $scope.child_node_title = modalCloseParameter.child_node_title;
                    $scope.child_node_type = modalCloseParameter.child_node_type;
                    $scope.applyMetadata = modalCloseParameter.applyMetadata;
                    $scope.section_page = modalCloseParameter.section_page;
                    $scope.file = modalCloseParameter.file;
                    $scope.parent_node_id = modalCloseParameter.parent_node_id;
                    $scope.parent_node_root_type = modalCloseParameter.parent_node_root_type;
                    $scope.assessment_id = modalCloseParameter.assessment_id;
                    $scope.assessment_source = modalCloseParameter.assessment_source;
                    $scope.ext_res_url = modalCloseParameter.ext_res_url;
                    $scope.ext_res_type = modalCloseParameter.ext_res_type;
                    if ($scope.applyMetadata) {
                        $scope.tags = modalCloseParameter.tags;
                        $scope.key_data = modalCloseParameter.key_data;
                        $scope.value_data = modalCloseParameter.value_data;
                        $scope.taxonomy = modalCloseParameter.taxonomy;
                    }
                    $scope.content_object_id = modalCloseParameter.content_object_id;
                    $scope.content_project_id = modalCloseParameter.content_project_id;
                    $scope.ext_res_link = modalCloseParameter.ext_res_link;
                    $scope.asset_id = modalCloseParameter.asset_id;
                    //Saving TOC                    
                    tocService.add_toc_node($scope).then(function (data) {

                        $scope.child_node_title = '';
                        $scope.pageAction = '';
                        $scope.section_page = '';
                        jQuery(".editor_button").attr("disabled", false);
                        $scope.changeLockStatus();

                        objectService.getProjectDetails($scope).then(function (data) {
                            setTimeout(function () {
                                child = JSON.parse(data.toc);
                            }, 1000);
                        });
                        //$scope.loadToc();
                    });
                    /*tocService.saveToc($scope).then(function () {
                     $scope.child_node_title = '';
                     $scope.pageAction = '';
                     });*/
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };



        $scope.editChildForm = function (size, child, modalFormText, parent_node_id) {

            $scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/edupubModalContent.html',
                controller: 'ModalEdupubEditCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return $scope.child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    parent_node_id: function () {
                        return parent_node_id;
                    },
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                //jQuery("#" + child.id).attr("disabled", true);
                jQuery(".editor_button").attr("disabled", true);
                $scope.node_id = modalCloseParameter.node_id;
                $scope.child_node_title = modalCloseParameter.child_node_title;
                $scope.child_node_type = modalCloseParameter.child_node_type;
                $scope.section_page = modalCloseParameter.section_page;
                $scope.file = modalCloseParameter.file;
                $scope.parent_node_id = modalCloseParameter.parent_node_id;
                $scope.applyMetadata = modalCloseParameter.applyMetadata;
                if ($scope.applyMetadata) {
                    $scope.tags = modalCloseParameter.tags;
                    $scope.key_data = modalCloseParameter.key_data;
                    $scope.value_data = modalCloseParameter.value_data;
                    $scope.taxonomy = modalCloseParameter.taxonomy;
                }
                $scope.assessment_id = modalCloseParameter.assessment_id;
                $scope.assessment_source = modalCloseParameter.assessment_source;
                $scope.ext_res_url = modalCloseParameter.ext_res_url;
                $scope.ext_res_type = modalCloseParameter.ext_res_type;
                $scope.asset_id = modalCloseParameter.asset_id;

                //Saving TOC
                tocService.edit_toc_node($scope).then(function (data) {
                    $scope.child_node_title = '';
                    $scope.pageAction = '';
                    $scope.section_page = '';
                    jQuery(".editor_button").attr("disabled", false);
                    $scope.changeLockStatus();
                    /*objectService.getProjectDetails($scope).then(function (data) {
                     $scope.data = JSON.parse(data.toc);
                     });*/
                    if (data.status == 400) {
                        SweetAlert.swal("Cancelled", "Content is locked so cannot be modified", "error");
                        $scope.loadToc();
                    }
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.open_existing_content_modal = function (size, child, project_id, project_type_id) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/edupubModalGlobalContent.html',
                controller: 'ModalEdupubGlobalCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return child;
                    },
                    modalFormText: function () {
                        return 'Choose Content';
                    },
                    project_id: function () {
                        return project_id;
                    },
                    project_type_id: function () {
                        return project_type_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                if (modalCloseParameter.projectPermissionGranted == true) {
                    $scope.node_id = modalCloseParameter.node_id;
                    $scope.child_node_title = modalCloseParameter.child_node_title;
                    $scope.child_node_type = modalCloseParameter.child_node_type;
                    $scope.section_page = modalCloseParameter.section_page;
                    $scope.file = modalCloseParameter.file;
                    $scope.parent_node_id = modalCloseParameter.parent_node_id;
                    $scope.parent_node_root_type = modalCloseParameter.parent_node_root_type;
                    $scope.template_id = '';
                    $scope.content_object_id = modalCloseParameter.content_object_id;
                    $scope.content_project_id = modalCloseParameter.content_project_id;
                    //debugger;
                    //Saving TOC
                    tocService.add_toc_node($scope).then(function (data) {
                        $scope.child_node_title = '';
                        $scope.pageAction = '';
                        $scope.section_page = '';
                        jQuery(".editor_button").attr("disabled", false);
                        $scope.changeLockStatus();
                        setTimeout(function () {
                            $window.location.href = "/#/edit_toc/" + $scope.project_id;
                        }, 500);
                    });
                }

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


app.controller('ModalEdupubAddCtrl', function ($scope, $location, $filter, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, node_type, parent_node_type, tagService, keyValueService, taxonomyService, $compile, $modal, sessionService, SweetAlert, assetService) {
    $scope.ts_hash = '?_ts=' + new Date().getTime();
    $scope.child = child;
    $scope.node_title = '';
    $scope.node_type = '';
    $scope.node_id = 0;
    $scope.section_page = 'page';
    $scope.child_type = 'page';
    $scope.show_resource_options = true;
    $scope.ext_res_type = ''
    $scope.ext_res_url = '';
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = 'Add Node';
    $scope.project_id = project_id;
    $scope.isNodeAddDisabled = false;
    $scope.showTocType = true;
    $scope.showSectionPage = false;
    $scope.showPartRadio = true;
    $scope.applyMetadata = '';
    $scope.project_type_id = project_details.project_type_id;
    $scope.formError = {'node_title': ''}
    $scope.parent_node_type = parent_node_type;
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.show_make_global = false;
    $scope.reuse = true;
    $scope.project = {'project_permissions': project_permissions};
    $scope.metadata = {};
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.assessment_source = '';
    $scope.is_assessment = 0;
    $scope.assessment_id = '';
    $scope.frost_resource_asset_type = '';
    $scope.frost_resource_asset_name = '';
    $scope.asset_id = '';
    $scope.placeholder = "Name of Node";

    if (project_permissions['tag.apply'].grant) {
        $scope.applyMetadata = 1;
    } else {
        $scope.applyMetadata = 0;
    }

    $scope.access_token = sessionService.get('access_token');
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));

    $scope.re_use_button = true;
    if (!$scope.user_permissions['global_content.show.all'].grant) {
        $scope.re_use_button = false;
    }

    // taxonomy 
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
            }
        }
        if (data.count === 0) {
            setTimeout(function () {
                angular.element('#taxonomyStatus').html('No active taxonomy available in the management');
            }, 500);
        }

    });

    // get tags 
    var tags = [];
    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
//        if (data.projectPermissionGranted == 401) {
//            SweetAlert.swal("Cancelled", "Access prohibited!", "error");
//            $location.path("dashboard");
//        }
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }

            if (data.count == 0) {
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });

    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="key-values-area row" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };



    //Decide Node type and section page according to project_type_id
    if ($scope.project_type_id == 1) {
        // Incase of PXE
        $scope.node_type = node_type;
        $scope.showTocType = false;
        if (node_type == 1) {
            //If it is frontmatter, donot show section_page radio button
            $scope.showSectionPage = false;
            $scope.section_page = 'page';
        }

        if (child.type == 5) {
            //You can add only page inside Part
            $scope.section_page = 'page';
            $scope.showSectionPage = false;
        }
    } else {
        $scope.node_type = '';
        $scope.showTocType = true;
    }

    $scope.choose_section_page = function (node_type_id) {
        $scope.is_assessment = 0;
        $scope.ext_res_type = '';
        $scope.ext_res_url = '';
        $scope.asset_id = '';
        $scope.assessment_source = '';
        $scope.assessment_id = '';
            
        if (node_type_id == "213") {
            $scope.placeholder = "Name of Resource";            
            $scope.section_page = 'link';            
        } else if(node_type_id=='216'){
            $scope.placeholder = "Name of Assessment";
            $scope.is_assessment = 1;
            $scope.section_page = 'assessment';
        } else {
            $scope.placeholder = "Name of Node";     
            $scope.section_page = 'page';
        }
        $scope.child_type = node_type_id;
    }

    if ($scope.child.root_type == 3 && $scope.child.type == 214) {
        $scope.parent_node_type = 3;
    }

    // Fetch Toc Types            
    tocService.getTocTypes($scope).then(function (data) {
        selectedKey = $scope.arrayIterate(data);
        $scope.selected_option = data[selectedKey];
    });
    //Array index find for selected type
    var selectedKey = '';
    $scope.arrayIterate = function (lists) {
        angular.forEach(lists, function (value, key) {
            if (value.id == $scope.node_type) {
                selectedKey = key;
            }
        });
        return selectedKey;
    }

    //Add assets
    //Specific for External Resource only - Start
    $scope.ext_res_type = '';
    $scope.ext_res_url = '';


    //Specific for External Resource only - End
    $scope.flag = 0;
    $scope.key_duplicate_check = function () {
        var new_array = [];
        angular.forEach($scope.key_data, function (value, key) {
            new_array.push(value);
        });
        var counts = {}, i, value;
        for (i = 0; i < new_array.length; i++) {
            value = new_array[i];
            if (typeof counts[value] === "undefined") {
                counts[value] = 1;
            } else {
                counts[value]++;
            }
        }
        angular.forEach(counts, function (value, key) {
            if (value > 1) {
                $scope.flag = 1;
            } else {
                $scope.flag = 0;
            }
        });
    }
    $scope.section_page_disabled = false;
    $scope.add = function (child) {

        $scope.key_duplicate_check();
        $scope.isNodeAddDisabled = true;
        $scope.pageAction = 'ADD';
        objectService.getProjectDetails($scope).then(function (data) {
            //debugger;
            if (data == null) {
                SweetAlert.swal("Cancelled", "Access prohibited!", "error");
                $location.path("dashboard");
            }
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {
                //var page_name = $scope.file = tocService.createFileName($scope);
                if (angular.isDefined($scope.selected_option) && $scope.selected_option != null) {
                    var node_type = ((angular.isDefined($scope.selected_option)) ? $scope.selected_option.id : '');
                } else {
                    var node_type = '';
                }
                //if ((angular.isUndefined($scope.node_title) == false && $scope.node_title != '') && (node_type != '') && ($scope.flag === 0)) {
                    //This is done to allow all special characters on node title
                    //But every special charas will be replaced by underscore
                    //$scope.file = $scope.node_title.allReplace({'\\s': '_', ':': '_', '-': '_', '\\.': '_', '\\?': '_', '\'': '_', '/': '_'}) + '.xhtml';
                    var nt = $scope.node_title.substr(0, 40);
                    
                    if ($scope.node_title.toLowerCase() == 'glossary' || $scope.node_title.toLowerCase() == 'footnote') {
                        //By passing if the node is glossary or footnote
                        $scope.file = $scope.node_title.toLowerCase() + '.xhtml';
                    } else {
                        $scope.file = nt.replace(/[^A-Za-z0-9]+/ig, '_') + small_guid('-') + '.xhtml';
                        $scope.file = $scope.file.toLowerCase();
                    }

                    //console.log($scope.file);
                    $scope.ext_child_node_type = node_type;
                    $scope.formError['node_title'] = '';

                    tocService.checkFileName($scope).then(function (data) {
                        var validation_error = 0;
                        //if (data.status == 200 && ($scope.section_page == 'page' || $scope.section_page == 'section')) {
                        if (data.status == 200) {
                            $scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";
                            if (node_type == 213) { //settings for external resource only
                                $scope.section_page = 'link';
                                $scope.file = "";
                                if ($scope.ext_res_type == '') {
                                    $scope.formError['ext_res_type'] = 'Please select type';
                                    validation_error = 1;
                                }
                                if ($scope.ext_res_type == 'url' || $scope.ext_res_type == 'asset') {
                                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                        validation_error = 1;
                                        $scope.formError['resource_asset_message'] = 'Please select asset';
                                    }
                                    if ($scope.ext_res_type == 'url' && $scope.external_resources_message != '') {
                                        validation_error = 1;
                                    }
                                }

                                /*if (!$scope.ext_res_url) {
                                 $scope.formError['ext_res_url'] = "External Resource URL should not be blank";
                                 $scope.isNodeAddDisabled = false;
                                 return;
                                 }*/
                            } else if (node_type == 214) {
                                $scope.section_page = 'section';
                                $scope.file = "";
                            } else if (node_type == 216) {
                                $scope.section_page = 'assessment';
                            if ($scope.is_assessment == 1 && $scope.assessment_id == '' && angular.isDefined($scope.assessment_id)) {
                                $scope.formError['assessment_id'] = 'Please fill out this field properly';
                                $scope.isNodeAddDisabled = false;
                             return;
                             }
                             } else {
                                $scope.ext_res_url = '';
                                $scope.ext_res_type = '';
                            }
                            if (validation_error) {
                                $scope.isNodeAddDisabled = false;
                                return;
                            }
                            var node_id = guid('-');
                            child.children.push({
                                id: node_id,
                                title: $scope.node_title,
                                file: $scope.file, //page_name, //$scope.node_title + '.xhtml', //node_id + '.xhtml',
                                type: node_type,
                                root_type: child.root_type,
                                section_page: $scope.section_page,
                                ext_res_link: $scope.ext_res_url,
                                ext_res_type: $scope.ext_res_type,
                                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                                assessment_id: angular.isDefined($scope.assessment_id) ? $scope.assessment_id : '',
                                asset_id: angular.isDefined($scope.asset_id) ? $scope.asset_id : '',
                                children: []
                            });
                            var modalCloseParameter = {node_id: node_id, child_node_title: $scope.node_title, child_node_type: node_type, section_page: $scope.section_page, 'file': $scope.file, 'parent_node_id': $scope.child.id, 'parent_node_root_type': child.root_type, content_object_id: '', content_project_id: '', reuse_modal: '', 'applyMetadata': $scope.applyMetadata, ext_res_link: $scope.ext_res_url, 'assessment_id': $scope.assessment_id};
                            modalCloseParameter.ext_res_url = (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url);
                            modalCloseParameter.ext_res_type = (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type);
                            modalCloseParameter.asset_id = (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id);
                            if ($scope.applyMetadata) {
                                modalCloseParameter.tags = $scope.metadata.tagValue;
                                modalCloseParameter.key_data = $scope.key_data;
                                modalCloseParameter.value_data = $scope.value_data;
                                modalCloseParameter.taxonomy = $scope.taxonomy;
                            }
                            modalCloseParameter.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                            $modalInstance.close(modalCloseParameter);
                        } else {
                            $scope.formError=[];
                            $scope.isNodeAddDisabled = false;
                            if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                            {
                                setTimeout(function () {
                                    angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                }, 100);
                            }

                            $scope.errMsg = (data.message != null) ? data.message : "";
                            $scope.errMsgAssessment = (data.assessment_message != null) ? data.assessment_message : "";
                            /*$scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";
                            if ($scope.errMsg == "Only a-z, A-Z, 0-9, _, :, -, ., ?, ', / are supported.") {
                                $scope.formError['node_title'] = $scope.errMsg;
                                $scope.formError['ext_res_url'] = '';
                            } else if ($scope.errMsg == 'File or Same title exist, Please use different name') {
                                $scope.formError['node_title'] = $scope.errMsg;
                                $scope.formError['ext_res_url'] = '';
                            } else {
                                $scope.formError['ext_res_url'] = $scope.errMsg;
                            }
                            if ($scope.errMsgAssessment) {
                                $scope.formError['assessment_id'] = $scope.errMsgAssessment;
                            }
                            if ($scope.external_resources_message) {
                                $scope.formError['external_resources_message'] = $scope.external_resources_message;
                            }
                            if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                $scope.formError['resource_asset_message'] = 'Please select asset';
                            }
                            if ($scope.ext_res_type == '') {
                                $scope.formError['ext_res_type'] = 'Please select type';
                            }*/
                            if (angular.isDefined(data.error.node_title) && data.error.node_title!='') {
                                $scope.formError['node_title'] =  data.error.node_title;
                            }
                            if (angular.isDefined(data.error.node_type) && data.error.node_type!='') {
                                $scope.formError['node_type'] =  data.error.node_type;
                            }
                            if (angular.isDefined(data.error.assessment_id) && data.error.assessment_id!='') {
                                $scope.formError['assessment_id'] = data.error.assessment_id;
                            }
                            if (angular.isDefined(data.error.assessment_source) && data.error.assessment_source!='') {
                                $scope.formError['assessment_source'] = data.error.assessment_source;
                            }                            
                            if (angular.isDefined(data.error.ext_res_url) && data.error.ext_res_url!='') {
                                $scope.formError['ext_res_url'] = data.error.ext_res_url;
                            }
                            if (angular.isDefined(data.error.resource_asset_message) && data.error.resource_asset_message!='') {
                                $scope.formError['resource_asset_message'] = data.error.resource_asset_message;
                            }
                            if (angular.isDefined(data.error.external_resources_message) && data.error.external_resources_message!='') {
                                $scope.formError['external_resources_message'] = data.error.external_resources_message;
                            }                            
                            if (angular.isDefined(data.error.ext_res_type) && data.error.ext_res_type!='') {
                                $scope.formError['ext_res_type'] = data.error.ext_res_type;
                            }
                            
                            
                        }
                    });

                /*} else {
                    $scope.isNodeAddDisabled = false;
                    $scope.formError['node_title'] = '';
                    if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                        $scope.formError['node_title'] = "Please fill out this field properly.";//"Only a-z, A-Z, 0-9, _, :, -, ., ?, ', / are supported.";
                    }
                    $scope.formError['node_type'] = '';
                    if (angular.isDefined($scope.selected_option) && $scope.selected_option != null) {
                        var node_type = ((angular.isDefined($scope.selected_option)) ? $scope.selected_option.id : '');
                    } else {
                        var node_type = '';
                    }

                    if (node_type == '') {
                        $scope.formError['node_type'] = 'Please fill out this field properly.';
                    }

                    if ($scope.ext_res_type == 'url') {
                        var isValidUrl = $scope.validateUrl($scope.ext_res_url);
                        if (!isValidUrl) {
                            $scope.formError['external_resources_message'] = 'Please Enter a Valid URL with http://, https:// or ftp://';
                        } else {
                            $scope.formError['external_resources_message'] = '';
                        }

                    }
                    
                    if ($scope.is_assessment == 1 && $scope.assessment_source == '') {
                        $scope.formError['assessment_source'] = 'Please select type';
                    }

                    if ($scope.is_assessment == 1 && $scope.assessment_id == '') {
                        if ($scope.assessment_source == 'FROST') {
                            $scope.formError['assessment_id'] = 'Please select assessment';
                        } else {
                            $scope.formError['assessment_id'] = 'Please fill out this field properly';
                        }
                    }

                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                        $scope.formError['resource_asset_message'] = 'Please select asset';
                    }
                    if ($scope.ext_res_type == '') {
                        $scope.formError['ext_res_type'] = 'Please select type';
                    }
                    if ($scope.flag > 0) {
                        $scope.formError = {
                            duplicate_value: 'Duplicate value not allowed'
                        };
                    }
                }*/
            } else {
                alert('Permission denied');
            }

        });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.validateUrl = function (url) {
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (pattern.test(url)) {
            return true;
        }
        return false;
    };

    ///////////////////////////////////////////////////////
    $scope.reuse_existing_content = function (child, project_id, project_type_id) {
        if ($scope.user_permissions['global_content.show.all'].grant) {
            var modalCloseParameter = {child: child, project_id: project_id, project_type_id: project_type_id, reuse_modal: true};
            $modalInstance.close(modalCloseParameter);
        } else {
            SweetAlert.swal("Cancelled", "Permission Denied!", "error");
        }
    }


    /*$scope.radio_show = {
     type: 'url'
     }
     
     $scope.show_flag = false;
     $scope.$watch('radio_show.type', function (val) {
     if (val != 'url') {
     $scope.show_flag = false;
     } else {
     $scope.show_flag = true;
     }
     })*/

    //Assets Modal 
    $scope.ext_res_title = null;
    $scope.addAssestModal = function () {
        var size = 'lg';
        var modal_form_title = 'Asset Library';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/externalResourceModalContent.html',
            controller: 'ModalEdupubAssetsCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                project_id: function () {
                    return $scope.project_id;
                },
                ext_res_url: function () {
                    return $scope.ext_res_url;
                }
            }
        });

        modalInstance.result.then(function (modalCloseParameter) {
            $scope.ext_res_url = modalCloseParameter.ext_res_location;
            $scope.ext_res_title = modalCloseParameter.ext_res_title;
            $scope.asset_id = modalCloseParameter.asset_id;
            $scope.assessment_source = '';
            $scope.assessment_id = '';
            $scope.set_asset_thumbnail();
            //$scope.ext_res_type = modalCloseParameter.ext_res_type;
        });
    };

    $scope.onChangeResourceType = function () {
        $scope.ext_res_url = "";
        $scope.ext_res_title = null;
        $scope.formError['external_resources_message'] = '';
        $scope.formError['resource_asset_message'] = '';
        $scope.formError['ext_res_type'] = '';
    };

    $scope.deleteAsset = function () {
        $scope.frost_resource_asset_name = '';
        $scope.ext_res_url = '';
    };

    $scope.onChangeAssessmentType = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = {};
        $scope.formError['assessment_id'] = '';
        $scope.formError['assessment_source'] = '';
    };

    $scope.deleteAssessment = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = '';
    };
    
    $scope.addAssessmentModal = function () {
        var size = 'lg';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/assessmentModalContent.html',
            controller: 'ModalTocAssessmentCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                project_id: function () {
                    return $scope.project_id;
                },
                assessment_id: function () {
                    return $scope.assessment_id;
                },
                is_global_project: function () {
                    return false;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {            
            $scope.assessment_details = modalCloseParameter.assessment_details;
            $scope.assessment_id = modalCloseParameter.assessment_details.id;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.asset_id = '';
        });
    };

    $scope.set_asset_thumbnail = function () {
        $scope.frost_resource_asset_type = '';
        $scope.frost_resource_asset_name = '';
        if (angular.isDefined($scope.ext_res_url) && $scope.ext_res_url != '') {
            var pp = $scope.ext_res_url.split('/');
            if (angular.isDefined(pp[6]) && pp[6] != '') {
                assetService.getAssetById(pp[6]).then(function (data) {
                    $scope.frost_resource_asset_type = data.data.object_type_name;
                    $scope.frost_resource_asset_name = data.data.name;
                });
            }
        }
    };

    ////////////////////////////////////////////////////////
});

//Assets Modal Controller 
app.controller('ModalEdupubAssetsCtrl', function ($scope, $modalInstance, tocService, assetService, project_id, modalFormTitle, ext_res_url, objectService, SweetAlert) {

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.ts_hash = '?_ts=' + new Date().getTime();
    $scope.modalFormTitle = modalFormTitle;
    $scope.project_id = project_id;
    $scope.taxonomy_id = 0;
    $scope.asset_filter = {"images": true, "videos": true, "audio": true, "other": true, "gadgets": true};
    $scope.asset_local_global = {"local": true, "global": true};
    $scope.pageNumber = 1;
    $scope.itemsPerPage = 8;
    $scope.ext_res_location = ext_res_url;
    $scope.asset_id = '';
    $scope.isAddAssetDisabled = true;

    $scope.getAssets = function (pageNumber) {
        $scope.removeErrors = 1;
        $scope.showCheckAll = 0;
        $scope.showMakeGlobal = 0;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = 8;
        assetService.getMimes().then(function (mimes) {
            if ($scope.asset_id != '') {
                $scope.isAddAssetDisabled = false;
                //angular.element('#submit_resource').attr('disabled', false);
            }
            $scope.mimes = mimes.data;
            assetService.getAssets($scope).then(function (data) {
                $scope.assets = data.data;
                //$scope.assets.Selected = ext_res_url;
                angular.forEach($scope.assets, function (value, key) {
                    if (value.asset_location == $scope.ext_res_location) {
                        value.Selected = $scope.ext_res_location;
                        value.Style = "opacity:1 !important;";
                        //angular.element('#submit_resource').attr('disabled', 'disabled');
                    } else {
                        //angular.element('#submit_resource').attr('disabled', false);
                    }
                });
                $scope.totalAssets = data.totalAssets;
                $scope.removeErrors = angular.isDefined(data.totalAssets) ? data.totalAssets : 0;
//                if ($scope.ext_res_location == '') {
//                    angular.element('#submit_resource').attr('disabled', 'disabled');
//                }
            });
        });
    };
    //$scope.getAssets(1);

    //Get Project Detail
    $scope.getProject = function () {
        $scope.project_name = '';
        if ($scope.object_id != '') {
            objectService.getProjectDetails($scope).then(function (data) {
                $scope.project_name = data.name;
                $scope.project_type_id = data.project_type_id;
                $scope.toc_creation_status = data.toc_creation_status;
                $scope.project = data;
                $scope.actual_permission = data.project_permissions['asset.use_global'].grant;
            });
        }
    };
    $scope.getProject();

    //For Pagination Of Dashboard
    $scope.pageChangeHandlerAsset = function (newPage) {
        $scope.getAssets(newPage);
        angular.element('.label-checkbox').removeAttr('style');
    };

    $scope.$watch('search_text', function (val) {
        $scope.searchModel = {"search_text": val};
        $scope.getAssets(1);
    });


    $scope.selectOption = function (asset_id, asset_location, title, type, is_global) {
        if (is_global === 1 && $scope.actual_permission === false) {
            angular.element('.label-checkbox').removeAttr('style');
            $scope.isAddAssetDisabled = true;
            //angular.element('#submit_resource').attr('disabled', 'disabled');
            SweetAlert.swal("Cancelled", "Permission Denied!", "error");
        } else {
            angular.element('.label-checkbox').removeAttr('style');
            angular.element('#check-box-label-' + asset_id).attr('style', 'opacity:1 !important;');
            $scope.ext_res_location = asset_location;
            $scope.ext_res_title = title;
            $scope.ext_res_type = type;
            $scope.asset_id = asset_id;
            //angular.element('#submit_resource').attr('disabled', false);
            $scope.isAddAssetDisabled = false;
            setTimeout(function () {
                angular.element('#submit_resource').focus();
            }, 200);
        }
    };

    $scope.submitResource = function () {
        if ($scope.ext_res_type == 'widget') {
            $scope.ext_res_location += '/launch.html';
        }
        var modalCloseParameter = {ext_res_location: $scope.ext_res_location, ext_res_title: $scope.ext_res_title, ext_res_type: $scope.ext_res_type, asset_id: $scope.asset_id};
        $modalInstance.close(modalCloseParameter);
    };


});

app.controller('ModalEdupubEditCtrl', function ($scope, $filter, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, parent_node_id, tagService, keyValueService, taxonomyService, $compile, ivhTreeviewMgr, SweetAlert, assetService, sessionService, $modal, $location, assessmentService ) {
    $scope.ts_hash = '?_ts=' + new Date().getTime();
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.child = child;
    $scope.node_title = child.title;
    $scope.node_type = child.root_type;
    $scope.node_id = child.id;
    $scope.child_type = child.type;
    $scope.section_page = child.section_page;
    $scope.ext_res_type = (angular.isUndefined(child.ext_res_type) ? '' : child.ext_res_type);
    $scope.ext_res_url = (angular.isUndefined(child.ext_res_link) ? '' : child.ext_res_link);
    $scope.asset_id = (angular.isDefined(child.asset_id) ? child.asset_id : '');
    $scope.is_assessment = (angular.isDefined(child.section_page) && child.section_page == 'assessment' ? child.section_page : 0);
    $scope.assessment_source = (angular.isDefined(child.assessment_source) ? child.assessment_source : '');
    $scope.assessment_id = (angular.isDefined(child.assessment_id) ? child.assessment_id : '');
    $scope.assessment_details = {};

    //console.log(child);
    $scope.modalFormText = modalFormText + ' ' + $scope.node_title + '. ';
    $scope.modalFormTile = 'Edit Node';
    $scope.project_id = project_id;
    $scope.showTocType = false;
    $scope.showSectionPage = false;
    $scope.project_type_id = project_details.project_type_id;
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.project = {'project_permissions': project_permissions};
    $scope.applyMetadata = '';
    $scope.formError = {'node_title': ''};
    $scope.metadata = {};

    //Fetch Object Details   
    $scope.reuse = true;
    $scope.show_make_global = true;
    $scope.make_global = false;
    $scope.disable_global_checkbox = false;
    $scope.frost_resource_asset_type = '';
    $scope.frost_resource_asset_name = '';

    if ($scope.child_type == "213") {
        $scope.placeholder = "Name of Resource";
    } else {
        $scope.placeholder = "Name of Node";
    }

    objectService.getObjectDetailsById($scope.node_id).then(function (data) {
        if (data.data.is_global == '1') {
            $scope.disable_global_checkbox = true;
            if ($scope.user_permissions['tag.apply'].grant) {
                $scope.applyMetadata = $scope.user_permissions['tag.apply'].grant;
            } else {
                $scope.applyMetadata = 0;
            }
        } else {
            if (project_permissions['tag.apply'].grant) {
                $scope.applyMetadata = project_permissions['tag.apply'].grant;
            } else {
                $scope.applyMetadata = 0;
            }
        }
    });

    // get taxonomy 
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.taxonomy, $scope.taxonomy);
            }
            if (data.count === 0) {
                setTimeout(function () {
                    angular.element('#taxonomyStatus').html('No active taxonomy available in the management');
                }, 500);
            }
        }

    });

    // get tags 
    var tags = [];
    var object_tags = [];

    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.metadata.tagValue = object_tags;
                }
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
//        if (data.projectPermissionGranted == 401) {
//            SweetAlert.swal("Cancelled", "Access prohibited!", "error");
//            $location.path("dashboard");
//        }
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });

                setTimeout(function () {
                    // populate old key values
                    if (Object.keys(data.data.object_key_values).length)
                    {
                        angular.forEach(data.data.object_key_values, function (object_key_value) {
                            $scope.totalKeyArea = $scope.totalKeyArea + 1;
                            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                            $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                            $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                            if (object_key_value.key_type == 'select_list')
                            {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                            } else {
                                if (object_key_value.key_type == 'number_entry') {
                                    $scope.value_data['key_' + $scope.totalKeyArea] = parseFloat(object_key_value.value);
                                } else {
                                    $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                                }
                            }

                        });
                    } else {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                    }
                }, 500);
            }

            if (data.count == 0) {
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row key-values-area" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };

    // Fetch Toc Types            
    tocService.getTocTypes($scope).then(function (data) {
        //$scope.selected_option = $scope.node_type;//$scope.TocTypes[$scope.node_type];  

        selectedKey = $scope.arrayIterate(data);
        $scope.selected_option = data[selectedKey];
    });

    //Array index find for selected type
    var selectedKey = '';
    $scope.arrayIterate = function (lists) {
        angular.forEach(lists, function (value, key) {
            if (value.id == $scope.node_type) {
                selectedKey = key;
            }
        });
        return selectedKey;
    };

    //selectedKey = $scope.arrayIterate($scope.list);
    //

    //section_page_disabled
    $scope.section_page = $scope.child.section_page;
    $scope.section_page_disabled = true;
    if (angular.isUndefined($scope.child.section_page)) {
        $scope.section_page = 'section';
    }
    //==

    $scope.$watch('node_title', function (val) {
        if (angular.isUndefined(val) == true || val == '') {
            //$scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'}
            $scope.formError = {'node_title': 'Please fill out this field properly.'};
        } else {
            $scope.formError = {'node_title': ''};
        }
    });

    $scope.flag = 0;
    $scope.key_duplicate_check = function () {
        var new_array = [];
        angular.forEach($scope.key_data, function (value, key) {
            new_array.push(value);
        });
        var counts = {}, i, value;
        for (i = 0; i < new_array.length; i++) {
            value = new_array[i];
            if (typeof counts[value] === "undefined") {
                counts[value] = 1;
            } else {
                counts[value]++;
            }
        }
        angular.forEach(counts, function (value, key) {
            if (value > 1) {
                $scope.flag = 1;
            } else {
                $scope.flag = 0;
            }
        });
    }

    $scope.add = function (child) {
        //console.log(child);
        $scope.pageAction = 'EDIT';
        $scope.key_duplicate_check();
        objectService.getProjectDetails($scope).then(function (data) {
            if (data == null) {
                SweetAlert.swal("Cancelled", "Access prohibited!", "error");
                $location.path("dashboard");
            }
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {

                //if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '' && ($scope.flag === 0)) {
                    $scope.formError['node_title'] = '';
                    //This is done to allow all special characters on node title
                    //But every special charas will be replaced by underscore
                    //$scope.file = $scope.node_title.allReplace({'\\s': '_', ':': '_', '-': '_', '\\.': '_', '\\?': '_', '\'': '_', '/': '_'}) + '.xhtml';
                    var nt = $scope.node_title.substr(0, 40);
                    if ($scope.node_title.toLowerCase() == 'glossary' || $scope.node_title.toLowerCase() == 'footnote') {
                        //By passing if the node is glossary or footnote
                        $scope.file = $scope.node_title.toLowerCase() + '.xhtml';
                    } else {
                        $scope.file = nt.replace(/[^A-Za-z0-9]+/ig, '_') + small_guid('-') + '.xhtml';
                        $scope.file = $scope.file.toLowerCase();
                    }
                    $scope.ext_child_node_type = $scope.child_type;                    
                    tocService.checkFileName($scope).then(function (data) {
                        var validation_error = 0;
                        if (data.status == 200) {
                            $scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";
                            child.title = $scope.node_title;                            
                            if ($scope.child_type == 216) {
                                child.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                                child.assessment_id = angular.isDefined($scope.assessment_id) ? $scope.assessment_id : '';
                            }
                            if ($scope.child_type == 213) {
                                if ($scope.ext_res_type == '') {
                                    $scope.formError['ext_res_type'] = 'Please select type';
                                    validation_error = 1;
                                }
                                if ($scope.ext_res_type == 'url' || $scope.ext_res_type == 'asset') {
                                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                        $scope.formError['resource_asset_message'] = 'Please select asset';
                                        validation_error = 1;
                                    }
                                }                                
                            } else if ($scope.child_type == 214) {
                                $scope.section_page = 'section';
                                $scope.file = "";
                            }
                            if (validation_error) {
                                return;
                            }
                            
                            
                            child.ext_res_link = (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url);
                            child.ext_res_type = (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type);
                            child.asset_id = (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id);
                                
                            var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title, child_node_type: child.type, section_page: $scope.section_page, 'file': $scope.file, 'parent_node_id': parent_node_id, 'applyMetadata': $scope.applyMetadata, 'assessment_id': $scope.assessment_id};
                            modalCloseParameter.ext_res_url = (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url);
                            modalCloseParameter.ext_res_type = (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type);
                            modalCloseParameter.asset_id = (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id);
                            if ($scope.applyMetadata) {
                                modalCloseParameter.tags = $scope.metadata.tagValue;
                                modalCloseParameter.key_data = $scope.key_data;
                                modalCloseParameter.value_data = $scope.value_data;
                                modalCloseParameter.taxonomy = $scope.taxonomy;
                            }
                            modalCloseParameter.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                            $modalInstance.close(modalCloseParameter);
                        } else {
                            // switch tab to details if it is in metadata tab
                            if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                            {
                                setTimeout(function () {
                                    angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                }, 100);
                            }
                            $scope.errMsg = (data.message != null) ? data.message : "";
                            $scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";
                            $scope.errMsgAssessment = (data.assessment_message != null) ? data.assessment_message : "";
                            /*if ($scope.errMsg == "Only a-z, A-Z, 0-9, _, :, -, ., ?, ', / are supported.") {
                                $scope.formError['node_title'] = $scope.errMsg;
                                $scope.formError['ext_res_url'] = '';
                            } else if ($scope.errMsg == "File or Same title exist, Please use different name") {
                                $scope.formError['node_title'] = $scope.errMsg;
                            } else {
                                $scope.formError['ext_res_url'] = $scope.errMsg;
                            }
                            if ($scope.errMsgAssessment) {
                                $scope.formError['assessment_id'] = $scope.errMsgAssessment;
                            }
                            if ($scope.external_resources_message) {
                                $scope.formError['external_resources_message'] = $scope.external_resources_message;
                            }
                            if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                $scope.formError['resource_asset_message'] = 'Please select asset';
                            }
                            if ($scope.ext_res_type == '') {
                                $scope.formError['ext_res_type'] = 'Please select type';
                            }*/
                            if (angular.isDefined(data.error.node_title) && data.error.node_title!='') {
                                $scope.formError['node_title'] =  data.error.node_title;
                            }
                            if (angular.isDefined(data.error.assessment_id) && data.error.assessment_id!='') {
                                $scope.formError['assessment_id'] = data.error.assessment_id;
                            }
                            if (angular.isDefined(data.error.assessment_source) && data.error.assessment_source!='') {
                                $scope.formError['assessment_source'] = data.error.assessment_source;
                            }                            
                            if (angular.isDefined(data.error.ext_res_url) && data.error.ext_res_url!='') {
                                $scope.formError['ext_res_url'] = data.error.ext_res_url;
                            }
                            if (angular.isDefined(data.error.resource_asset_message) && data.error.resource_asset_message!='') {
                                $scope.formError['resource_asset_message'] = data.error.resource_asset_message;
                            }
                            if (angular.isDefined(data.error.external_resources_message) && data.error.external_resources_message!='') {
                                $scope.formError['external_resources_message'] = data.error.external_resources_message;
                            }                            
                            if (angular.isDefined(data.error.ext_res_type) && data.error.ext_res_type!='') {
                                $scope.formError['ext_res_type'] = data.error.ext_res_type;
                            }
                        }
                    });

                /*} else {
                    //$scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'}
                    $scope.formError = {'node_title': 'Please fill out this field properly.'};
                    
                    if ($scope.flag > 0) {
                        $scope.formError = {
                            duplicate_value: 'Duplicate value not allowed'
                        };
                    }
                    
                    if ($scope.assessment_source == '') {
                        $scope.formError['assessment_source'] = 'Please select type';
                    }
                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                        $scope.formError['resource_asset_message'] = 'Please select asset';
                    }
                    if ($scope.ext_res_type == '') {
                        $scope.formError['ext_res_type'] = 'Please select type';
                    }
                    if ($scope.ext_res_type == 'url') {
                        var isValidUrl = $scope.validateUrl($scope.ext_res_url);
                        if (!isValidUrl) {
                            $scope.formError['external_resources_message'] = 'Please Enter a Valid URL with http://, https:// or ftp://';
                        } else {
                            $scope.formError['external_resources_message'] = '';
                        }

                    }
                    if ($scope.assessment_id == '') {
                        if ($scope.assessment_source == 'FROST') {
                            $scope.formError['assessment_id'] = 'Please select assessment';
                        } else {
                            $scope.formError['assessment_id'] = 'Please fill out this field properly';
                        }
                    }                    
                }*/
            } else {
                alert('Permission denied');
            }
        });
    };

    $scope.validateUrl = function (url) {
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (pattern.test(url)) {
            return true;
        }
        return false;
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    //Used to make a object global
    $scope.makeGlobal = function (object_id) {
        if (!$scope.make_global) {
            $scope.make_global = true;
        } else {
            $scope.make_global = false;
            return false;
        }
        SweetAlert.swal({
            title: "Are you sure?",
            text: _lang['content_global_confirm_message'],
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var ids = [];
                ids.push(object_id);
                ids = JSON.stringify(ids);
                //console.log(ids);
                $scope.local_global = '';
                tocService.makeGlobal(ids, $scope).then(function (data) {
                    //console.log(data);
                    if (data.status === 200) {
                        //$scope.show_make_global = false;
                        $scope.disable_global_checkbox = true;
                    }
                });
                SweetAlert.swal("Success", "Object Content access changed successfully.", "success");
            } else {
                SweetAlert.swal("Cancelled", "Object Content is safe :)", "error");
            }
        });
    }

    //Specific for External Resource only - Start
    $scope.ext_res_opt = false;
    $scope.ext_res_opt_val = {
        type: 'url'
    };

    if (child.type == 213) {
        $scope.ext_res_opt = true;
        //$scope.show_make_global = false;
        $scope.make_global = false;
        $scope.disable_global_checkbox = false;
    }


    //Assets Modal 
    $scope.ext_res_title = '';
    $scope.addAssestModal = function () {
        var size = 'lg';
        var modal_form_title = 'Asset Library';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/externalResourceModalContent.html',
            controller: 'ModalEdupubAssetsCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                project_id: function () {
                    return $scope.project_id;
                },
                ext_res_url: function () {
                    return $scope.ext_res_url_return;
                }
            }
        });

        modalInstance.result.then(function (modalCloseParameter) {
            $scope.ext_res_url = modalCloseParameter.ext_res_location;
            $scope.ext_res_title = modalCloseParameter.ext_res_title;
            $scope.asset_id = modalCloseParameter.asset_id;
            //$scope.ext_res_type = modalCloseParameter.ext_res_type;
            $scope.ext_res_url_return = modalCloseParameter.ext_res_location;
            $scope.assessment_id = '';
            $scope.assessment_source = '';
            $scope.set_asset_thumbnail();
            console.log(modalCloseParameter);
        });
    };

    $scope.addAssessmentModal = function () {
        var size = 'lg';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/assessmentModalContent.html',
            controller: 'ModalTocAssessmentCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                project_id: function () {
                    return $scope.project_id;
                },
                assessment_id: function () {
                    return $scope.assessment_id;
                },
                is_global_project: function () {
                    return false;
                }
                
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.assessment_details = modalCloseParameter.assessment_details;
            $scope.assessment_id = modalCloseParameter.assessment_details.id;//modalCloseParameter.assessment_id;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.asset_id = '';
        });
    };
    $scope.setAssessmentTitle = function () {
        if (angular.isDefined($scope.assessment_source) && $scope.assessment_source == 'FROST') {
            assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                $scope.assessmemnts = data.data.assessment_list;
                if (angular.isDefined(data.data.assessment_list[0])) {
                    $scope.assessment_details = data.data.assessment_list[0];
                }
            });
        }
    };
    $scope.setAssessmentTitle();

    $scope.set_asset_thumbnail = function () {
        $scope.frost_resource_asset_type = '';
        $scope.frost_resource_asset_name = '';
        if (angular.isDefined($scope.ext_res_url) && $scope.ext_res_url != '') {
            var pp = $scope.ext_res_url.split('/');
            if (angular.isDefined(pp[6]) && pp[6] != '') {
                assetService.getAssetById(pp[6]).then(function (data) {
                    $scope.frost_resource_asset_type = data.data.object_type_name;
                    $scope.frost_resource_asset_name = data.data.name;
                });
            }
        }
    }
    $scope.set_asset_thumbnail();

    $scope.onChangeResourceType = function () {
        $scope.ext_res_url = "";
        $scope.ext_res_title = null;
        $scope.formError['external_resources_message'] = '';
        $scope.formError['resource_asset_message'] = '';
        $scope.formError['ext_res_type'] = '';
    };

    $scope.deleteAsset = function () {
        $scope.frost_resource_asset_name = '';
        $scope.ext_res_url = '';
    };

    $scope.deleteAssessment = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = '';
    };

    $scope.onChangeAssessmentType = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = {};
        $scope.formError['assessment_id'] = '';
        $scope.formError['assessment_source'] = '';
    };



});

app.controller('ModalEdupubGlobalCtrl', function ($scope, $filter, $location, $modalInstance, modalFormText, child, assetService, $modal, objectService, aclService, SweetAlert, project_id, project_type_id, project_permissions, taxonomyService, keyValueService, tagService, ivhTreeviewMgr, $compile, userService) {
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = 'Global Repository';
    $scope.project_id = project_id;
    $scope.project_type_id = project_type_id;
    $scope.project = {'project_permissions': project_permissions};
    $scope.search_text = '';
    $scope.currentPage = 1;
    $scope.pageNumber = '';
    $scope.itemsPerPage = '';
    $scope.removeErrors = 1;
    $scope.disableSelect = false;
    $scope.listGlobalContents = function (pageNumber) {
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = 10;
        objectService.getGlobalContents($scope).then(function (data) {
            $scope.globalContents = data.data;
            $scope.totalGLobalContents = data.total;
            $scope.removeErrors = 1;
            if ($scope.totalGLobalContents == 0) {
                $scope.removeErrors = 0;
            }
        });
    };
    $scope.listGlobalContents(1);
    //For Pagination Of Dashboard
    $scope.pageChangeHandlerDashboard = function (newPage) {
        $scope.listGlobalContents(newPage);
    };
    $scope.searchGlobal = function () {
        $scope.advanced_search.active = false;
        $scope.advanced_search.heading = 'Search Result For: ' + $scope.search_text;
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.listGlobalContents(1);
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.selectContent = function (content) {
        //$scope.disableSelect = true;
        $scope.permission_identifier = 'toc.toc-content.use_global';
        aclService.check_permission($scope).then(function (data) {
            if (data.status == 200 && data.data.grant == true) {
                var node_id = guid('');
                var child_node_type = 212;
                var section_page = 'page';
                //var file = node_id + '_' + content.name.allReplace({'\\s': '_', ':': '_', '-': '_', '\\.': '_', '\\?': '_', '\'': '_', '/': '_'}) + '.xhtml';
                var nt = content['name'].substr(0, 40);
                var file = node_id + '_' + nt.replace(/[^A-Za-z0-9]+/ig, '_') + small_guid('-') + '.xhtml';
                file = file.toLowerCase();
                child.children.push({
                    id: node_id,
                    title: content.name,
                    file: file,
                    type: child_node_type,
                    root_type: 2,
                    section_page: section_page,
                    template_id: '',
                    children: []
                });

                var modalCloseParameter = {
                    node_id: node_id, child_node_title: content.name, child_node_type: child_node_type, section_page: section_page, file: file,
                    parent_node_id: content.id, parent_node_root_type: '', content_object_id: content.id, content_project_id: content.project_id, template_id: '', projectPermissionGranted: data.data.grant
                };
                $modalInstance.close(modalCloseParameter);
            } else {
                if (angular.isDefined(data.projectPermissionGranted) && data.data.grant == false) {
                    SweetAlert.swal("Cancelled", data.message, "warning");
                }

                var modalCloseParameter = {
                    projectPermissionGranted: data.data.grant
                };
                //$modalInstance.close(modalCloseParameter);
            }
        });

    };

    // global content review part
    $scope.globalContentPreview = function (size, id) {
        var contentRec = {};
        $scope.id = id;
        assetService.getGlobalContentDetails($scope).then(function (data) {
            contentRec = data.data;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/globalModalContentView.html',
                controller: 'ModalInstanceContentCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    contentRec: function () {
                        return contentRec;
                    },                        
                    object_id: function () {
                        return id;
                    },
                    modalFormText: function () {
                        return "You are trying to view a content";
                    },
                    modalFormTile: function () {
                        return "Preview Content";
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
            });
        });
    };

    $scope.node_id = 0;
    $scope.advanced_search = {};
    $scope.advanced_search.enableSearch = false;
    $scope.advanced_search.title = '';
    $scope.advanced_search.active = false;
    $scope.advanced_search.isCollapsed = false;
    $scope.advanced_search.heading = 'Advanced Search Result:';

    $scope.advanced_search.key_data = {};
    $scope.advanced_search.value_data = {};
    $scope.advanced_search.totalKeyArea = 0;
    $scope.advanced_search.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.advanced_search.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
            }
        }

    });

    // get tags 
    var tags = [];
    var object_tags = [];


    $scope.advanced_search.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.advanced_search.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tagValue = object_tags;
                }
            }
        }

    });

    // get owners 
    var owners = [];
    var object_owners = [];

    $scope.advanced_search.owner = {
        multiple: true,
        data: owners
    };

    userService.getUsers($scope).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null)
            {
                angular.forEach(data.data, function (value, key) {
                    owners[key] = {id: value.id, text: value.username};
                });
                $scope.advanced_search.owner.data = owners;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
//        if (data.projectPermissionGranted == 401) {
//            SweetAlert.swal("Cancelled", "Access prohibited!", "error");
//            $location.path("dashboard");
//        }
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values)) {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    }
                });

                setTimeout(function () {
                    $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));

                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.advanced_search.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                } else if (value.key_type == 'time_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.advanced_search.key_data['key_' + keyArray[1]];
        delete $scope.advanced_search.value_data['key_' + keyArray[1]];
        if (angular.element(".advanced-search-key-values-area").length == 0)
        {
            $scope.advanced_search.totalKeyArea = 1;
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };

    $scope.applyAdvancedSearch = function () {
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.active = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.search_text = '';
        $scope.listGlobalContents(1);
    };

    $scope.clearSearch = function () {
        $scope.advanced_search.active = false;
        $scope.search_text = '';
        $scope.advanced_search.enableSearch = false;
        $scope.listGlobalContents(1);
    };

    $scope.toggleAdvanceSearch = function () {
        if ($scope.advanced_search.isCollapsed) {
            $scope.advanced_search.isCollapsed = false;
            if ($scope.advanced_search.active) {
                $scope.advanced_search.enableSearch = true;
            }
        } else {
            $scope.advanced_search.enableSearch = false;
            $scope.advanced_search.isCollapsed = true;
        }

    };

    $scope.applyAdvancedClear = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
        $scope.advanced_search.tagValue = [];
        $scope.advanced_search.ownerValue = [];
        $scope.advanced_search.title = '';
        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
    };

    $scope.generateAdvanceSearchTerm = function () {
        var searchTerm = [];
        var tagList = $scope.advanced_search.tagValue;
        if (tagList.length > 0) {
            angular.forEach(tagList, function (tag) {
                searchTerm.push(tag.text);
            });
        }
        var ownerList = $scope.advanced_search.ownerValue;
        if (ownerList.length > 0) {
            angular.forEach(ownerList, function (owner) {
                searchTerm.push(owner.text);
            });
        }
        var taxonomyList = $scope.selectedTaxonomy;
        if (taxonomyList.length > 0) {
            angular.forEach(taxonomyList, function (taxonomy) {
                searchTerm.push(taxonomy);
            });
        }

        if ($scope.advanced_search.title != '') {
            searchTerm.push($scope.advanced_search.title);
        }

    };

    $scope.selectedTaxonomy = [];
    $scope.selectedTaxonomyIds = [];

    $scope.$watch('advanced_search.taxonomy', function (newValues) {
        $scope.selectedTaxonomy.length = 0;
        $scope.selectedTaxonomyIds.length = 0;
        angular.forEach(newValues, function (item) {
            if (item.selected == true) {
                $scope.selectedTaxonomy.push(item.title);
                $scope.selectedTaxonomyIds.push(item.id);
            }
        });
    }, true);

});

String.prototype.allReplace = function (obj) {
    var retStr = this;
    for (var x in obj) {
        retStr = retStr.replace(new RegExp(x, 'g'), obj[x]);
    }
    return retStr;
};