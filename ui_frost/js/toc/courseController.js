'use strict';

app.controller('course', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$rootScope', '$modal', '$log', 'objectService', 'SweetAlert', '$window', '$filter', '$timeout', '$controller', '$interval', 'assetService', function ($scope, sessionService, tocService, $location, $routeParams, $rootScope, $modal, $log, objectService, SweetAlert, $window, $filter, $timeout, $controller, $interval, assetService, aclService) {

        $scope.update = function (event, ui) {
            var root = event.target,
                    item = ui.item,
                    parent = item.parent(),
                    target = (parent[0] === root) ? $scope.data : parent.scope().child,
                    child = item.scope().child,
                    index = item.index();

            target.children || (target.children = []);

            function walk(target, child) {
                var children = target.children, i;
                if (children) {
                    i = children.length;
                    while (i--) {
                        if (children[i] === child) {
                            return children.splice(i, 1);
                        } else {
                            walk(children[i], child);
                        }
                    }
                }
            }
            walk($scope.data, child);
            target.children.splice(index, 0, child);
            
            //console.log(child.id+' = '+child.section_page + ' = '+child.title);
            //console.log(target);
            //$scope.itarate_child(child);

            //Saving TOC
            $scope.rearrangeTocTree = function () {
                tocService.rearrangeTocTree($scope).then(function (data) {
                    if (data.status == 200) {
                        $location.path("edit_toc/" + $scope.project_id);
                    }
                });
            }
            $scope.rearrangeTocTree();

        };

        /*$scope.itarate_child = function(child){            
         if (child && child.children) {
         for (var i = 0; i<=child.children.length; i++) {
         console.info(child);  
         $scope.itarate_child(child.children[i]);
         }
         }
         }*/

        $scope.addNodeForm = function (size, child, modalFormText, node_type) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/courseModalContent.html',
                controller: 'ModalCourseAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    node_type: function () {
                        return node_type;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                if (modalCloseParameter.reuse_modal) {
                    $scope.open_existing_content_modal('lg', modalCloseParameter.child, modalCloseParameter.project_id, modalCloseParameter.project_type_id);
                } else {
                    jQuery(".editor_button").attr("disabled", true);
                    $scope.node_id = modalCloseParameter.node_id;
                    $scope.child_node_title = modalCloseParameter.child_node_title;
                    $scope.child_node_type = modalCloseParameter.child_node_type;
                    $scope.section_page = modalCloseParameter.section_page;
                    $scope.file = modalCloseParameter.file;
                    $scope.parent_node_id = modalCloseParameter.parent_node_id;
                    $scope.parent_node_root_type = modalCloseParameter.parent_node_root_type;
                    $scope.template_id = modalCloseParameter.template_id;
                    $scope.content_object_id = modalCloseParameter.content_object_id;
                    $scope.content_project_id = modalCloseParameter.content_project_id;
                    $scope.applyMetadata = modalCloseParameter.applyMetadata;
                    $scope.assessment_id = modalCloseParameter.assessment_id;
                    $scope.assessment_source = modalCloseParameter.assessment_source;
                    $scope.ext_res_url = modalCloseParameter.ext_res_url;
                    $scope.ext_res_type = modalCloseParameter.ext_res_type;
                    $scope.parent_lo_id = modalCloseParameter.parent_lo_id;
                    $scope.asset_id = modalCloseParameter.asset_id;
                    if ($scope.applyMetadata) {
                        $scope.tags = modalCloseParameter.tags;
                        $scope.key_data = modalCloseParameter.key_data;
                        $scope.value_data = modalCloseParameter.value_data;
                        $scope.taxonomy = modalCloseParameter.taxonomy;
                    }

                    //Saving TOC
                    tocService.add_toc_node($scope).then(function (data) {
                        $scope.child_node_title = '';
                        $scope.pageAction = '';
                        $scope.section_page = '';
                        jQuery(".editor_button").attr("disabled", false);
                        $scope.changeLockStatus();
                        objectService.getProjectDetails($scope).then(function (data) {
                            setTimeout(function () {
                                child = JSON.parse(data.toc);
                                //$scope.loadToc();
                            }, 1000);
                        });
                        //$scope.loadToc();
                    });
                }

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editChildForm = function (size, child, modalFormText, parent_node_id) {

            $scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/courseModalContent.html',
                controller: 'ModalCourseEditCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return $scope.child;
                    },
                    modalFormText: function () {
                        return modalFormText
                    },
                    project_id: function () {
                        return $scope.project_id
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_details: function () {
                        return $scope.project_details;
                    },
                    parent_node_id: function () {
                        return parent_node_id;
                    },
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                //jQuery("#" + child.id).attr("disabled", true);
                jQuery(".editor_button").attr("disabled", true);
                $scope.node_id = modalCloseParameter.node_id;
                $scope.child_node_title = modalCloseParameter.child_node_title;
                $scope.section_page = modalCloseParameter.section_page;
                $scope.file = modalCloseParameter.file;
                $scope.parent_node_id = angular.isDefined(modalCloseParameter.parent_node_id) ? modalCloseParameter.parent_node_id : '';
                $scope.applyMetadata = modalCloseParameter.applyMetadata;
                if ($scope.applyMetadata) {
                    $scope.tags = modalCloseParameter.tags;
                    $scope.key_data = modalCloseParameter.key_data;
                    $scope.value_data = modalCloseParameter.value_data;
                    $scope.taxonomy = modalCloseParameter.taxonomy;
                }
                $scope.ext_res_url = modalCloseParameter.ext_res_url;
                $scope.ext_res_type = modalCloseParameter.ext_res_type;
                $scope.assessment_id = modalCloseParameter.assessment_id;
                $scope.assessment_source = modalCloseParameter.assessment_source;
                $scope.asset_id = modalCloseParameter.asset_id;
                /*console.info('modalCloseParameter');
                 console.log(modalCloseParameter);
                 debugger;*/
                //Saving TOC
                tocService.edit_toc_node($scope).then(function (data) {
                    $scope.child_node_title = '';
                    $scope.pageAction = '';
                    $scope.section_page = '';
                    jQuery(".editor_button").attr("disabled", false);
                    $scope.changeLockStatus();
                    if (data.status == 400) {
                        SweetAlert.swal("Cancelled", "Content is locked so cannot be modified", "error");
                        $scope.loadToc();
                    }
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.open_existing_content_modal = function (size, child, project_id, project_type_id) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/courseModalGlobalContent.html',
                controller: 'ModalCourseGlobalCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    child: function () {
                        return child;
                    },
                    modalFormText: function () {
                        return 'Choose Content';
                    },
                    project_id: function () {
                        return project_id;
                    },
                    project_type_id: function () {
                        return project_type_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                angular.forEach(modalCloseParameter, function (v, k) {
                    if (v.projectPermissionGranted == true) {
                        $scope.node_id = v.node_id;
                        $scope.child_node_title = v.child_node_title;
                        $scope.child_node_type = v.child_node_type;
                        $scope.section_page = v.section_page;
                        $scope.file = v.file;
                        $scope.parent_node_id = v.parent_node_id;
                        $scope.parent_node_root_type = v.parent_node_root_type;
                        $scope.template_id = '';
                        $scope.content_object_id = v.content_object_id;
                        $scope.content_project_id = v.content_project_id;

                        //Saving TOC
                        tocService.add_toc_node($scope).then(function (data) {
                            $scope.child_node_title = '';
                            $scope.pageAction = '';
                            $scope.section_page = '';
                            jQuery(".editor_button").attr("disabled", false);
                            $scope.changeLockStatus();
                            //$scope.loadToc();
                            setTimeout(function () {
                                $window.location.href = "/#/edit_toc/" + $scope.project_id;
                            }, 500);
                        });
                    }
                });


            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };



        //Setting up of FLow to upload multiple PDFs
        $scope.$flow.defaults.flowRepoDir = $scope.project_id;
        $scope.$flow.opts.target = 'api/v1/upload-multiple-pdf';
        $scope.$flow.opts.query = {
            project_id: $scope.project_id,
            project_type_id: $scope.project_type_id,
            pageRange: 'all'
        };
        /*$scope.uploadPackage = function(){
         alert('gsgsg');
         }*/
        //==========================================//

    }]);


app.controller('ModalCourseAddCtrl', function ($scope, $filter, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, node_type, templateService, tagService, keyValueService, taxonomyService, $compile, $modal, assetService) {
    $scope.child = child;
    $scope.node_title = '';
    $scope.node_type = '';
    $scope.node_id = 0;
    $scope.section_page = 'page';
    //$scope.show_resource_options=true;
    $scope.ext_res_type = ''
    $scope.ext_res_url = '';
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = 'Add Node';
    $scope.project_id = project_id;
    $scope.isNodeAddDisabled = false;
    $scope.showTemplate = true;
    $scope.showSectionPage = true;
    $scope.showPartRadio = false;
    $scope.applyMetadata = '';
    $scope.project_type_id = project_details.project_type_id;
    $scope.assessment_source = '';
    $scope.placeholder = 'Name of Content Screen';
    $scope.formError = {'node_title': '', 'external_resources_message': ''}
    $scope.project = {'project_permissions': project_permissions};
    $scope.metadata = {};
    if (project_permissions['tag.apply'].grant) {
        $scope.applyMetadata = 1;
    } else {
        $scope.applyMetadata = 0;
    }
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.show_make_global = false;
    $scope.taxonomyStatus = 1001;
    $scope.is_assessment = 0;
    $scope.assessment_id = '';
    $scope.assessment_details = {};
    $scope.asset_id = '';
    $scope.frost_resource_asset_type = '';
    $scope.frost_resource_asset_name = '';

    angular.element("input:radio[name=groupName]:first").val();

    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
            }
        }

    });
    $scope.section_page_disabled = false;

    // get tags 
    var tags = [];
    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    }
                });
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });

    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row key-values-area" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div></div></div>';
        return keySelect;
    };

    //Fetch Templates
    $scope.template_status = 1001;
    $scope.template_local_global = {};
    $scope.template_local_global.local = true;
    $scope.template_local_global.global = true;
    $scope.on_use = 1;
    templateService.getTemplate($scope).then(function (data) {
        $scope.templates = data.data;
    });


    $scope.add = function (child) {
        //$scope.isNodeAddDisabled = true; 
        $scope.pageAction = 'ADD';
        $scope.formError = {'node_title': '', 'external_resources_message': '', 'resource_asset_message': ''}
        objectService.getProjectDetails($scope).then(function (data) {
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {

                var page_name = '';
                $scope.node_title = angular.isUndefined($scope.node_title) ? '' : $scope.node_title;
                if ($scope.node_title != '') {
                    //page_name = $scope.file = $scope.node_title.allReplace({'\\s': '_', ':': '_', '-': '_', '\\.': '_'}) + '.html';                    
                    var nt = $scope.node_title.substr(0, 40);
                    $scope.file = page_name = nt.replace(/[^A-Za-z0-9]+/ig, '_') + small_guid('-') + '.html';
                }
                //console.info($scope.node_title+'<>'+page_name);
                tocService.checkFileName($scope).then(function (data) {

                    if (data.status == 200) {
                        //var template_id = (angular.isDefined($scope.selected_option) ? $scope.selected_option.id : '');
                        //Check node title and node type
                        $scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";
                        if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '') {

                            $scope.formError['node_title'] = "";
                            var node_id = guid('-');
                            var template_id = (angular.isDefined($scope.selected_option) && !($scope.selected_option === null) ? $scope.selected_option.id : '');
                            var content_object_id = (angular.isDefined($scope.selected_content_option) ? $scope.selected_content_option.id : '');
                            //page_name = $scope.file = data.data.file_name;
                            if ($scope.section_page == 'section') {
                                node_type = 5; // NodeTypeId For Folder
                                page_name = '';
                            } else if ($scope.section_page == 'page') {
                                node_type = 6; // NodeTypeId For Content                                
                            } else if ($scope.section_page == 'lmo') {
                                node_type = 6; // NodeTypeId For Learning Objects
                                page_name = '';
                            } else if ($scope.section_page == 'assessment') {
                                node_type = 7; // NodeTypeId For Assessment 

                                if ($scope.is_assessment == 1 && $scope.assessment_source == '') {
                                    $scope.formError['assessment_source'] = 'Please select type';
                                    return;
                                }
                                if ($scope.is_assessment == 1 && $scope.assessment_id == '' && angular.isDefined($scope.assessment_id)) {
                                    if ($scope.assessment_source == 'FROST') {
                                        $scope.formError['assessment_id'] = 'Please select assessment';
                                    } else {
                                        $scope.formError['assessment_id'] = 'Please fill out this field properly';
                                    }
                                    return;
                                }
                            } else if ($scope.section_page == 'link') {
                                if ($scope.ext_res_type == '') {
                                    $scope.formError['ext_res_type'] = 'Please select type';
                                    return;
                                }
                                // External assets
                                if ($scope.ext_res_type == 'url' || $scope.ext_res_type == 'asset') {
                                    node_type = 213;
                                    page_name = '';
                                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                        $scope.formError['resource_asset_message'] = 'Please select asset';
                                        return;
                                    }
                                }
                            }
                            var parent_lo_id = $scope.child.section_page == 'lmo' ? $scope.child.id : (angular.isDefined($scope.child.parent_lo_id) ? $scope.child.parent_lo_id : '');

                            //var page_name = tocService.createFileName($scope); //($scope.section_page == 'page' ? $scope.node_title + '.xhtml' : '');
                            child.children.push({
                                id: node_id,
                                title: $scope.node_title,
                                file: page_name, //$scope.node_title + '.xhtml', //node_id + '.xhtml',
                                type: node_type,
                                root_type: child.root_type,
                                section_page: $scope.section_page,
                                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                                assessment_id: angular.isDefined($scope.assessment_id) ? $scope.assessment_id : '',
                                ext_res_type: $scope.ext_res_type,
                                ext_res_link: $scope.ext_res_url,
                                asset_id: $scope.asset_id,
                                template_id: template_id,
                                parent_lo_id: parent_lo_id,
                                children: []
                            });

                            //console.log(child);

                            var modalCloseParameter = {node_id: node_id, child_node_title: $scope.node_title, child_node_type: node_type, section_page: $scope.section_page, 'file': page_name, 'parent_node_id': $scope.child.id, 'parent_node_root_type': child.root_type, 'template_id': template_id, 'content_object_id': content_object_id, content_project_id: '', reuse_modal: '', 'applyMetadata': $scope.applyMetadata, 'assessment_id': $scope.assessment_id, parent_lo_id: parent_lo_id};
                            modalCloseParameter.ext_res_url = (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url);
                            modalCloseParameter.ext_res_type = (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type);
                            modalCloseParameter.asset_id = (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id);
                            if ($scope.applyMetadata) {
                                modalCloseParameter.tags = $scope.metadata.tagValue;
                                modalCloseParameter.key_data = $scope.key_data;
                                modalCloseParameter.value_data = $scope.value_data;
                                modalCloseParameter.taxonomy = $scope.taxonomy;
                            }
                            modalCloseParameter.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                            $modalInstance.close(modalCloseParameter);
                        } else {
                            //console.log(33);
                            if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                            {
                                setTimeout(function () {
                                    angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                }, 100);
                            }
                            $scope.formError = {'node_title': '', 'node_type': ''};
                            if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                                $scope.formError['node_title'] = 'Only a-z, A-Z, 0-9, _, :, -, . are supported.';
                            }

                            if (angular.isDefined($scope.selected_option) && !($scope.selected_option === null) && $scope.selected_option.id) {
                                $scope.formError['node_type'] = 'Please choose this field properly.';
                            }

                            if ($scope.is_assessment == 1 && $scope.assessment_source == '') {
                                $scope.formError['assessment_source'] = 'Please select type';
                            }

                            if ($scope.is_assessment == 1 && $scope.assessment_id == '') {
                                if ($scope.assessment_source == 'FROST') {
                                    $scope.formError['assessment_id'] = 'Please select assessment';
                                } else {
                                    $scope.formError['assessment_id'] = 'Please fill out this field properly';
                                }
                            }
                            if ($scope.ext_res_type == '') {
                                $scope.formError['ext_res_type'] = 'Please select type';
                            }
                            if ($scope.external_resources_message) {
                                $scope.formError['external_resources_message'] = $scope.external_resources_message;
                            }
                        }
                    } else {
                        if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                        {
                            setTimeout(function () {
                                angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                            }, 100);
                        }
                        $scope.isNodeAddDisabled = false;
                        $scope.errMsg = (data.message != null) ? data.message : "";
                        $scope.errMsgAssessment = (data.assessment_message != null) ? data.assessment_message : "";
                        $scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";

                        if (angular.isUndefined($scope.node_title) == true || $scope.node_title == '') {
                            $scope.formError['node_title'] = 'Only a-z, A-Z, 0-9, _, :, -, . are supported.';
                        }

                        //added by luckychowdury@17.1.2018
                        if ($scope.errMsg) {
                            $scope.formError['node_title'] = $scope.errMsg;
                        }

                        if ($scope.errMsgAssessment) {
                            $scope.formError['assessment_id'] = $scope.errMsgAssessment;
                        }
                        if ($scope.external_resources_message) {
                            $scope.formError['external_resources_message'] = $scope.external_resources_message;
                        }

                        if ($scope.is_assessment == 1 && $scope.assessment_source == '') {
                            $scope.formError['assessment_source'] = 'Please select type';
                        }

                        if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                            $scope.formError['resource_asset_message'] = 'Please select asset';
                        }

//                        if ($scope.ext_res_type == 'url') {
//                            var isValidUrl = $scope.validateUrl($scope.ext_res_url);
//                            if (!isValidUrl) {
//                                $scope.formError['external_resources_message'] = 'Please Enter a Valid URL with http://, https:// or ftp://';
//                            } else {
//                                $scope.formError['external_resources_message'] = '';
//                            }
//
//                        }

                        if ($scope.ext_res_type == '') {
                            $scope.formError['ext_res_type'] = 'Please select type';
                        }

                        //console.log(angular.isDefined($scope.selected_option));
                        //console.log($scope.selected_option.id)
                        $scope.formError['node_type'] = '';
                        if (!angular.isDefined($scope.selected_option) || ($scope.selected_option === null) || !angular.isDefined($scope.selected_option.id)) {
                            $scope.formError['node_type'] = 'Please choose this field properly.';
                        }

                    }
                });


            } else {
                alert('Permission denied');
            }

        });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.choose_section_page = function (section_page) {
        $scope.section_page = section_page;
        $scope.updateLabel(section_page);
        $scope.assessment_source = '';
        $scope.assessment_id = "";
        $scope.assessment_details = {};
        $scope.is_assessment = 0;
        $scope.ext_res_url = '';
        $scope.ext_res_type = '';
        if ($scope.section_page == 'section' || $scope.section_page == 'assessment') {
            $scope.showTemplate = false;
            if ($scope.section_page == 'assessment') {
                $scope.is_assessment = 1;
            }
        } else if ($scope.section_page == 'link') {
            $scope.showTemplate = false;
        } else if ($scope.section_page == 'lmo') {
            $scope.showTemplate = false;
        } else {
            $scope.showTemplate = true;
            $scope.ext_res_url = '';
            $scope.ext_res_type = '';
        }
    }

    $scope.updateLabel = function (nodeType) {
        if (nodeType == "section") {
            $scope.placeholder = "Name of Section/Module";
        } else if (nodeType == "lmo") {
            $scope.placeholder = "Name of Learning Object";
        } else if (nodeType == "page") {
            $scope.placeholder = "Name of Content Screen";
        } else if (nodeType == "assessment") {
            $scope.placeholder = "Name of Assessment";
        } else if (nodeType == "link") {
            $scope.placeholder = "Name of Resource";
        }
    };

    $scope.choose_section_page_resource = function (ext_res_type) {
        $scope.ext_res_type = ext_res_type;
    }
    ///////////////////////////////////////////////////////
    $scope.reuse_existing_content = function (child, project_id, project_type_id) {
        //console.log(child);
        //console.info(project_id);
        //$modalInstance.dismiss('cancel');
        //debugger;
        var modalCloseParameter = {child: child, project_id: project_id, project_type_id: project_type_id, reuse_modal: true};
        $modalInstance.close(modalCloseParameter);
    }
    ////////////////////////////////////////////////////////

    //Assets Modal 
    $scope.ext_res_title = null;
    $scope.addAssestModal = function () {
        var size = 'lg';
        var modal_form_title = 'Asset Library';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/externalResourceModalContent.html',
            controller: 'ModalCourseAssetsCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                project_id: function () {
                    return $scope.project_id;
                },
                ext_res_url: function () {
                    return $scope.ext_res_url;
                },
                from_global_repo: function () {
                    return false;
                }
            }
        });

        modalInstance.result.then(function (modalCloseParameter) {
            $scope.ext_res_url = modalCloseParameter.ext_res_location;
            $scope.ext_res_title = modalCloseParameter.ext_res_title;
            $scope.assessment_id = '';
            $scope.assessment_source = '';
            $scope.asset_id = modalCloseParameter.asset_id;
            $scope.set_asset_thumbnail();
            //$scope.ext_res_type = modalCloseParameter.ext_res_type;
        });
    };

    $scope.deleteAsset = function () {
        $scope.frost_resource_asset_name = '';
        $scope.ext_res_url = '';
    };

    $scope.onChangeResourceType = function () {
        $scope.ext_res_url = "";
        $scope.ext_res_title = null;
        $scope.formError['external_resources_message'] = '';
        $scope.formError['resource_asset_message'] = '';
    };

    $scope.onChangeAssessmentType = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = {};
        $scope.formError['assessment_id'] = '';
    };

    $scope.deleteAssessment = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = '';
    };

    $scope.addAssessmentModal = function () {
        var size = 'lg';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/assessmentModalContent.html',
            controller: 'ModalTocAssessmentCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                project_id: function () {
                    return $scope.project_id;
                },
                assessment_id: function () {
                    return $scope.assessment_id;
                },
                assessment_details: function () {
                    return $scope.assessment_details;
                },
                is_global_project: function () {
                    return false;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.assessment_details = modalCloseParameter.assessment_details;
            $scope.assessment_id = modalCloseParameter.assessment_details.id;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.asset_id = '';
        });
    };

    $scope.set_asset_thumbnail = function () {
        $scope.frost_resource_asset_type = '';
        $scope.frost_resource_asset_name = '';
        if (angular.isDefined($scope.ext_res_url) && $scope.ext_res_url != '') {
            var pp = $scope.ext_res_url.split('/');
            if (angular.isDefined(pp[6]) && pp[6] != '') {
                assetService.getAssetById(pp[6]).then(function (data) {
                    //console.log(data.data);
                    $scope.frost_resource_asset_type = data.data.object_type_name;
                    $scope.frost_resource_asset_name = data.data.name;
                });
            }
        }
    }
});


app.controller('ModalCourseEditCtrl', function ($scope, $filter, $modalInstance, tocService, objectService, child, modalFormText, project_id, project_permissions, project_details, parent_node_id, tagService, keyValueService, taxonomyService, $compile, ivhTreeviewMgr, SweetAlert, assetService, sessionService, $modal, assessmentService) {
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.child = child;
    $scope.node_title = child.title;
    $scope.node_type = child.root_type;
    $scope.node_id = child.id;
    $scope.section_page = child.section_page;
    $scope.ext_res_type = (angular.isUndefined(child.ext_res_type) ? '' : child.ext_res_type);
    $scope.ext_res_url = (angular.isUndefined(child.ext_res_link) ? '' : child.ext_res_link);
    $scope.is_assessment = (angular.isDefined(child.section_page) && child.section_page == 'assessment' ? 1 : 0);
    $scope.assessment_source = (angular.isDefined(child.assessment_source) ? child.assessment_source : '');
    $scope.assessment_id = (angular.isDefined(child.assessment_id) ? child.assessment_id : '');
    $scope.assessment_details = {};
    $scope.asset_id = (angular.isDefined(child.asset_id) ? child.asset_id : '');

    //$scope.show_resource_options=false;
    $scope.modalFormText = modalFormText + ' ' + $scope.node_title + '. ';
    $scope.modalFormTile = 'Edit Node';
    $scope.project_id = project_id;
    $scope.showTemplate = false;
    $scope.showSectionPage = false;
    $scope.project_type_id = project_details.project_type_id;
    $scope.applyMetadata = '';
    $scope.metadata = {};
    $scope.formError = {'node_title': ''};
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.frost_resource_asset_type = '';
    $scope.frost_resource_asset_name = '';

    $scope.project = {'project_permissions': project_permissions};

    // set label
    $scope.placeholder = 'Name of Content Screen';
    if ($scope.section_page == "section") {
        $scope.placeholder = "Name of Section/Module";
    } else if ($scope.section_page == "lmo") {
        $scope.placeholder = "Name of Learning Object";
    } else if ($scope.section_page == "page") {
        $scope.placeholder = "Name of Content Screen";
    } else if ($scope.section_page == "assessment") {
        $scope.placeholder = "Name of Assessment";
    } else if ($scope.section_page == "link") {
        $scope.placeholder = "Name of Resource";
    }

    //Fetch Object Details
    $scope.show_make_global = true;
    $scope.make_global = false;
    $scope.disable_global_checkbox = false;
    objectService.getObjectDetailsById($scope.node_id).then(function (data) {
        if (data.data.is_global == '1') {
            $scope.disable_global_checkbox = true;
            if ($scope.user_permissions['tag.apply'].grant) {
                $scope.applyMetadata = $scope.user_permissions['tag.apply'].grant;
            } else {
                $scope.applyMetadata = 0;
            }
        } else {
            if (project_permissions['tag.apply'].grant) {
                $scope.applyMetadata = project_permissions['tag.apply'].grant;
            } else {
                $scope.applyMetadata = 0;
            }
        }
    });

    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.taxonomy, $scope.taxonomy);
            }
        }
    });
    // get tags 
    var tags = [];
    var object_tags = [];

    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.metadata.tagValue = object_tags;
                }
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    }
                });

                setTimeout(function () {
                    // populate old key values
                    if (Object.keys(data.data.object_key_values).length)
                    {
                        angular.forEach(data.data.object_key_values, function (object_key_value) {
                            $scope.totalKeyArea = $scope.totalKeyArea + 1;
                            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                            $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                            $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                            if (object_key_value.key_type == 'select_list')
                            {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                            } else {
                                if (object_key_value.key_type == 'number_entry') {
                                    $scope.value_data['key_' + $scope.totalKeyArea] = parseFloat(object_key_value.value);
                                } else {
                                    $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                                }
                            }

                        });
                    } else {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                    }
                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row key-values-area" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-minus"></i></button></div></div></div>';
        return keySelect;
    };

    //section_page_disabled    
    $scope.section_page = $scope.child.section_page;
    $scope.section_page_disabled = true;
    if (angular.isUndefined($scope.child.section_page)) {
        $scope.section_page = 'section';
    }
    //==

    /*$scope.$watch('node_title', function (val) {
     console.log(val);
     //var re = /^([a-zA-Z0-9:_.-]+\s?)*$/;
     //var patt = new RegExp("/^([a-zA-Z0-9:_.-]+\\s?)*$/");
     var res = val.match(/^([a-zA-Z0-9:_.-]+\s?)*$/);
     //var res = patt.test(val);
     console.log(res);
     if ( res == true || val == '') {
     $scope.formError = {'node_title': 'Please fill out this field properly. [a-z,A-Z,0-9,-,_,.,:," "] are allowed'};
     } else {
     $scope.formError = {'node_title': ''};
     }
     });*/

    $scope.add = function (child) {
        $scope.pageAction = 'EDIT';
        objectService.getProjectDetails($scope).then(function (data) {
            $scope.project = {'project_permissions': data.project_permissions};
            if ($scope.project.project_permissions['toc.node.add'].grant) {

                if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '') {
                    $scope.formError['node_title'] = '';
                    var nt = $scope.node_title.substr(0, 40);
                    if ($scope.node_title.toLowerCase() == 'glossary' || $scope.node_title.toLowerCase() == 'footnote') {
                        //By passing if the node is glossary or footnote
                        $scope.file = $scope.node_title.toLowerCase() + '.html';
                    } else {
                        $scope.file = nt.replace(/[^A-Za-z0-9]+/ig, '_') + small_guid('-') + '.html';
                        $scope.file = $scope.file.toLowerCase();
                    }
                    tocService.checkFileName($scope).then(function (data) {
                        if (data.status == 200) {
                            var validation_error = 0;
                            if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '') {


                                if (angular.isDefined(child.section_page) && child.section_page == 'assessment') {
                                    //child.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                                    //child.assessment_id = angular.isDefined($scope.assessment_id) ? $scope.assessment_id : '';
                                    child.asset_id = '';

                                    if ($scope.assessment_source == '') {
                                        validation_error = 1;
                                        $scope.formError['assessment_source'] = 'Please select type';
                                    }
                                    if ($scope.assessment_id == '') {
                                        if ($scope.assessment_source == 'FROST') {
                                            validation_error = 1;
                                            $scope.formError['assessment_id'] = 'Please select assessment';
                                        } else {
                                            validation_error = 1;
                                            $scope.formError['assessment_id'] = 'Please fill out this field properly';
                                        }
                                    }
                                }

                                //Keeping File blank for folder
                                if ($scope.section_page == 'section') {
                                    $scope.file = '';
                                }

                                if ($scope.section_page == 'link') {
                                    if ($scope.ext_res_type == 'url' || $scope.ext_res_type == 'asset') {
                                        if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                            validation_error = 1;
                                            $scope.formError['resource_asset_message'] = 'Please select asset';
                                        }
                                    }
                                    if ($scope.ext_res_type == '') {
                                        validation_error = 1;
                                        $scope.formError['ext_res_type'] = 'Please select type';
                                    }
                                }

                                if (validation_error) {
                                    return;
                                }

                                child.title = $scope.node_title;
                                child.ext_res_link = (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url);
                                child.ext_res_type = (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type);
                                child.asset_id = (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id);
                                //var page_name = $scope.file = tocService.createFileName($scope); 
                                child.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                                child.assessment_id = angular.isDefined($scope.assessment_id) ? $scope.assessment_id : '';
                                var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title, section_page: $scope.section_page, 'file': $scope.file, 'parent_node_id': parent_node_id, 'applyMetadata': $scope.applyMetadata, 'assessment_id': $scope.assessment_id};
                                modalCloseParameter.ext_res_url = (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url);
                                modalCloseParameter.ext_res_type = (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type);
                                modalCloseParameter.asset_id = (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id);
                                if ($scope.applyMetadata) {
                                    modalCloseParameter.tags = $scope.metadata.tagValue;
                                    modalCloseParameter.key_data = $scope.key_data;
                                    modalCloseParameter.value_data = $scope.value_data;
                                    modalCloseParameter.taxonomy = $scope.taxonomy;
                                }
                                modalCloseParameter.assessment_source = angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '';
                                $modalInstance.close(modalCloseParameter);
                            } else {
                                $scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'}
                            }
                        } else {
                            if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                            {
                                setTimeout(function () {
                                    angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                                }, 100);
                            }
                            $scope.errMsg = (data.message != null) ? data.message : "";
                            $scope.external_resources_message = (data.external_resources_message != null) ? data.external_resources_message : "";
                            $scope.errMsgAssessment = (data.assessment_message != null) ? data.assessment_message : "";
                            $scope.formError = {'node_title': $scope.errMsg}
                            if ($scope.errMsgAssessment) {
                                $scope.formError['assessment_id'] = $scope.errMsgAssessment;
                            }

                            if ($scope.assessment_source == '') {
                                $scope.formError['assessment_source'] = 'Please select type';
                            }
                            if ($scope.external_resources_message) {
                                $scope.formError['external_resources_message'] = $scope.external_resources_message;
                            }
                            if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                $scope.formError['resource_asset_message'] = 'Please select asset';
                            }
                            if ($scope.ext_res_type == '') {
                                $scope.formError['ext_res_type'] = 'Please select type';
                            }
                            if ($scope.section_page == 'link') {
                                if ($scope.ext_res_type == 'url' || $scope.ext_res_type == 'asset') {
                                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                                        $scope.formError['resource_asset_message'] = 'Please select asset';
                                    }
                                }
                            }
                            if ($scope.assessment_id == '') {
                                if ($scope.assessment_source == 'FROST') {
                                    $scope.formError['assessment_id'] = 'Please select assessment';
                                } else {
                                    $scope.formError['assessment_id'] = 'Please fill out this field properly';
                                }
                            }

                        }
                    });
                } else {
                    if (angular.element("ul#modalContentTab li.active").find('a').attr('data-target') == '#METADATA')
                    {
                        setTimeout(function () {
                            angular.element("ul#modalContentTab li").first().find('a').trigger('click');
                        }, 100);
                    }
                    $scope.formError = {'node_title': 'Only a-z, A-Z, 0-9, _, :, -, . are supported.'};
                    if ($scope.assessment_source == '') {
                        $scope.formError['assessment_source'] = 'Please select type';
                    }
                    if ($scope.ext_res_type == 'asset' && $scope.ext_res_url == '') {
                        $scope.formError['resource_asset_message'] = 'Please select asset';
                    }
                    if ($scope.ext_res_type == '') {
                        $scope.formError['ext_res_type'] = 'Please select type';
                    }
                    if ($scope.ext_res_type == 'url') {
                        var isValidUrl = $scope.validateUrl($scope.ext_res_url);
                        if (!isValidUrl) {
                            $scope.formError['external_resources_message'] = 'Please Enter a Valid URL with http://, https:// or ftp://';
                        } else {
                            $scope.formError['external_resources_message'] = '';
                        }

                    }
                    if ($scope.assessment_id == '') {
                        if ($scope.assessment_source == 'FROST') {
                            $scope.formError['assessment_id'] = 'Please select assessment';
                        } else {
                            $scope.formError['assessment_id'] = 'Please fill out this field properly';
                        }
                    }

                }
            } else {
                alert('Permission denied');
            }

        });

        /*tocService.checkFileName($scope).then(function (data) {
         if (data.status == 200) {
         if (angular.isUndefined($scope.node_title) == false && $scope.node_title != '' && angular.isUndefined($scope.selected_option) == false) {
         child.title = $scope.node_title;
         child.file = $scope.node_title + '.xhtml';
         child.type = $scope.selected_option.id;
         
         var modalCloseParameter = {node_id: child.id, child_node_title: $scope.node_title};
         $modalInstance.close(modalCloseParameter);
         }
         } else {
         alert(data.message);
         }
         });*/

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.validateUrl = function (url) {
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (pattern.test(url)) {
            return true;
        }
        return false;
    };



    //Used to make a object global
    $scope.makeGlobal = function (object_id) {
        if (!$scope.make_global) {
            $scope.make_global = true;
        } else {
            $scope.make_global = false;
            return false;
        }
        SweetAlert.swal({
            title: "Are you sure?",
            text: _lang['content_global_confirm_message'],
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            //console.log(isConfirm);
            if (isConfirm) {
                var ids = [];
                ids.push(object_id);
                ids = JSON.stringify(ids);
                //console.log(ids);
                $scope.local_global = '';
                assetService.makeGlobal(ids, $scope).then(function (data) {
                    //console.log(data);
                    if (data.status === 200) {
                        //$scope.show_make_global = false;
                        $scope.disable_global_checkbox = true;
                    }
                });
                SweetAlert.swal("Success", "Object Content access changed successfully.", "success");
            } else {
                SweetAlert.swal("Cancelled", "Object Content is safe :)", "error");
            }
        });
    }

//    if (child.type == 213) {
//        $scope.show_make_global = false;
//        $scope.make_global = false;
//    }

    //Assets Modal 
    $scope.ext_res_title = '';
    $scope.addAssestModal = function () {
        var size = 'lg';
        var modal_form_title = 'Asset Library';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/externalResourceModalContent.html',
            controller: 'ModalCourseAssetsCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                project_id: function () {
                    return $scope.project_id;
                },
                ext_res_url: function () {
                    return $scope.ext_res_url_return;
                },
                from_global_repo: function () {
                    return false;
                }
            }
        });

        modalInstance.result.then(function (modalCloseParameter) {
            $scope.ext_res_url = modalCloseParameter.ext_res_location;
            $scope.ext_res_title = modalCloseParameter.ext_res_title;
            //$scope.ext_res_type = modalCloseParameter.ext_res_type;
            $scope.ext_res_url_return = modalCloseParameter.ext_res_location;
            $scope.asset_id = modalCloseParameter.asset_id;
            $scope.assessment_id = '';
            $scope.assessment_source = '';
            $scope.set_asset_thumbnail();
        });
    };

    $scope.addAssessmentModal = function () {
        var size = 'lg';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/assessmentModalContent.html',
            controller: 'ModalTocAssessmentCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                project_id: function () {
                    return $scope.project_id;
                },
                assessment_id: function () {
                    return $scope.assessment_id;
                },
                is_global_project: function () {
                    return false;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.assessment_details = modalCloseParameter.assessment_details;
            $scope.assessment_id = modalCloseParameter.assessment_details.id;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.asset_id = '';
        });
    };

    $scope.setAssessmentTitle = function () {
        if (angular.isDefined($scope.assessment_source) && $scope.assessment_source == 'FROST') {
            assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                $scope.assessmemnts = data.data.assessment_list;
                if (angular.isDefined(data.data.assessment_list[0])) {
                    $scope.assessment_details = data.data.assessment_list[0];
                }
            });
        }
    };

    $scope.setAssessmentTitle();

    $scope.set_asset_thumbnail = function () {
        $scope.frost_resource_asset_type = '';
        $scope.frost_resource_asset_name = '';
        if (angular.isDefined($scope.ext_res_url) && $scope.ext_res_url != '') {
            var pp = $scope.ext_res_url.split('/');
            if (angular.isDefined(pp[6]) && pp[6] != '') {
                assetService.getAssetById(pp[6]).then(function (data) {
                    $scope.frost_resource_asset_type = data.data.object_type_name;
                    $scope.frost_resource_asset_name = data.data.name;
                });
            }
        }
    }
    $scope.set_asset_thumbnail();

    $scope.deleteAsset = function () {
        $scope.frost_resource_asset_name = '';
        $scope.ext_res_url = '';
    };

    $scope.onChangeResourceType = function () {
        $scope.ext_res_url = "";
        $scope.ext_res_title = null;
        $scope.formError['external_resources_message'] = '';
        $scope.formError['resource_asset_message'] = '';
    };

    $scope.deleteAssessment = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = '';
    };

    $scope.onChangeAssessmentType = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = {};
        $scope.formError['assessment_id'] = '';

    };


});

app.controller('ModalCourseGlobalCtrl', function ($scope, $modalInstance, $filter, modalFormText, child, objectService, assetService, $modal, aclService, SweetAlert, project_id, project_type_id, project_permissions, taxonomyService, keyValueService, tagService, ivhTreeviewMgr, $compile, userService, tocService) {
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = 'Global Repository';
    $scope.project_id = project_id;
    $scope.project_type_id = project_type_id;
    $scope.project = {'project_permissions': project_permissions};
    $scope.removeErrors = 1;
    $scope.search_text = '';
    $scope.currentPage = 1;
    $scope.pageNumber = '';
    $scope.itemsPerPage = '';
    $scope.disableSelect = false;
    $scope.listGlobalContents = function (pageNumber) {
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = 10;
        objectService.getGlobalContents($scope).then(function (data) {
            $scope.globalContents = data.data;
            $scope.totalGLobalContents = data.total;
            $scope.removeErrors = 1;
            if ($scope.totalGLobalContents == 0) {
                $scope.removeErrors = 0;
            }
        });
    };
    $scope.listGlobalContents(1);
    //For Pagination Of Dashboard
    $scope.pageChangeHandlerDashboard = function (newPage) {
        $scope.listGlobalContents(newPage);
    };
    $scope.searchGlobal = function () {
        $scope.advanced_search.active = false;
        $scope.advanced_search.heading = 'Search Result For: ' + $scope.search_text;
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.listGlobalContents(1);
    }

    // global content review part
    $scope.globalContentPreview = function (size, id) {
        var contentRec = {};
        $scope.id = id;
        assetService.getGlobalContentDetails($scope).then(function (data) {
            contentRec = data.data;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/globalModalContentView.html',
                controller: 'ModalInstanceContentCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    contentRec: function () {
                        return contentRec;
                    },
                    object_id: function () {
                        return id;
                    },
                    modalFormText: function () {
                        return "You are trying to view a content";
                    },
                    modalFormTile: function () {
                        return "Preview Content";
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
            });
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.selectContent = function (content) {
        //console.log(content);
        $scope.disableSelect = true;
        $scope.permission_identifier = 'toc.toc-content.use_global';

        //Storing original project id in a backup variable to restore.
        $scope.content_project_id = $scope.project_id;
        aclService.check_permission($scope).then(function (data) {
            if (data.status == 200 && data.data.grant == true) {
                $scope.object_id = content.id;
                $scope.project_id = content.project_id;
                tocService.getToc($scope).then(function (res) {
                    //console.log(res.section_page);
                    //console.log(child.section_page);
                    if ((res.section_page == 'lmo' || child.section_page == 'section') && false) {
                        //Just ignoring it
                        //console.log('==if==');
                        if (child.section_page != 'section') {
                            SweetAlert.swal("Error", "Folder/Learning Object cannot be added under LO or content", "error");
                            $modalInstance.close({});
                            return;
                        } else {
                            tocService.getLOToc($scope).then(function (res) {
                                var modalCloseParameter = new Array();
                                angular.forEach(res.linear_toc, function (value, key) {
                                    var node_id = value.id;
                                    var child_node_type = value.type;
                                    var section_page = value.section_page;
                                    //var file = node_id + '.xhtml';
                                    var file = node_id + '.html';
                                    modalCloseParameter.push({node_id: node_id,
                                        child_node_title: value.title,
                                        child_node_type: child_node_type,
                                        section_page: section_page,
                                        file: (child_node_type != 5) ? file : '',
                                        parent_node_id: (key == 0) ? child.id : value.parent_id,
                                        parent_node_root_type: '',
                                        content_object_id: value.content_object_id,
                                        content_project_id: $scope.project_id,
                                        template_id: '',
                                        projectPermissionGranted: data.data.grant});
                                });
                                child.children.push(res.org_toc);
                                //console.info(modalCloseParameter);
                                //return;
                                $scope.project_id = $scope.content_project_id;
                                $modalInstance.close(modalCloseParameter);
                                swal("Warning", "Content reusing is under process, please do not refresh the page", "warning");
                            });
                        }
                    } else {
                        //console.log('==else==');
                        //return;
                        var node_id = guid('');
                        var child_node_type = angular.isDefined(res.type) ? res.type : '6';
                        var section_page = angular.isDefined(res.section_page) ? res.section_page : 'page';
                        //var file = node_id + '.xhtml';
                        var file = node_id + '.html';
                        child.children.push({
                            id: node_id,
                            title: content.name,
                            file: file,
                            type: child_node_type,
                            root_type: 2,
                            section_page: section_page,
                            template_id: '',
                            children: []
                        });

                        var modalCloseParameter = [{
                                node_id: node_id, child_node_title: content.name, child_node_type: child_node_type, section_page: section_page,
                                file: file, parent_node_id: content.id, parent_node_root_type: '', content_object_id: content.id,
                                content_project_id: content.project_id, template_id: '', projectPermissionGranted: data.data.grant
                            }];
                        $scope.project_id = $scope.content_project_id;
                        $modalInstance.close(modalCloseParameter);
                    }
                });
            } else {
                //SweetAlert.swal("Cancelled", data.message, "warning");
                if (angular.isDefined(data.projectPermissionGranted) && data.data.grant == false) {
                    SweetAlert.swal("Cancelled", data.message, "warning");
                }

                var modalCloseParameter = {
                    projectPermissionGranted: data.data.grant
                };
                $modalInstance.close(modalCloseParameter);
            }
        });
    };

    $scope.node_id = 0;
    $scope.advanced_search = {};
    $scope.advanced_search.enableSearch = false;
    $scope.advanced_search.title = '';
    $scope.advanced_search.active = false;
    $scope.advanced_search.isCollapsed = false;
    $scope.advanced_search.heading = 'Advanced Search Result:';

    $scope.advanced_search.key_data = {};
    $scope.advanced_search.value_data = {};
    $scope.advanced_search.totalKeyArea = 0;
    $scope.advanced_search.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.advanced_search.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
            }
        }

    });

    // get tags 
    var tags = [];
    var object_tags = [];


    $scope.advanced_search.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.advanced_search.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tagValue = object_tags;
                }
            }
        }

    });

    // get owners 
    var owners = [];
    var object_owners = [];

    $scope.advanced_search.owner = {
        multiple: true,
        data: owners
    };

    userService.getUsers($scope).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null)
            {
                angular.forEach(data.data, function (value, key) {
                    owners[key] = {id: value.id, text: value.username};
                });
                $scope.advanced_search.owner.data = owners;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    }
                });

                setTimeout(function () {
                    $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));

                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.advanced_search.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.advanced_search.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                } else if (value.key_type == 'time_entry') {
                    $scope.advanced_search.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.advanced_search.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.advanced_search.key_data['key_' + keyArray[1]];
        delete $scope.advanced_search.value_data['key_' + keyArray[1]];
        if (angular.element(".advanced-search-key-values-area").length == 0)
        {
            $scope.advanced_search.totalKeyArea = 1;
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };

    $scope.applyAdvancedSearch = function () {
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.active = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.search_text = '';
        $scope.listGlobalContents(1);
    };

    $scope.clearSearch = function () {
        $scope.advanced_search.active = false;
        $scope.search_text = '';
        $scope.advanced_search.enableSearch = false;
        $scope.listGlobalContents(1);
    };

    $scope.toggleAdvanceSearch = function () {
        if ($scope.advanced_search.isCollapsed) {
            $scope.advanced_search.isCollapsed = false;
            if ($scope.advanced_search.active) {
                $scope.advanced_search.enableSearch = true;
            }
        } else {
            $scope.advanced_search.enableSearch = false;
            $scope.advanced_search.isCollapsed = true;
        }

    };

    $scope.applyAdvancedClear = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
        $scope.advanced_search.tagValue = [];
        $scope.advanced_search.ownerValue = [];
        $scope.advanced_search.title = '';
        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
    };

    $scope.generateAdvanceSearchTerm = function () {
        var searchTerm = [];
        var tagList = $scope.advanced_search.tagValue;
        if (tagList.length > 0) {
            angular.forEach(tagList, function (tag) {
                searchTerm.push(tag.text);
            });
        }
        var ownerList = $scope.advanced_search.ownerValue;
        if (ownerList.length > 0) {
            angular.forEach(ownerList, function (owner) {
                searchTerm.push(owner.text);
            });
        }
        var taxonomyList = $scope.selectedTaxonomy;
        if (taxonomyList.length > 0) {
            angular.forEach(taxonomyList, function (taxonomy) {
                searchTerm.push(taxonomy);
            });
        }

        if ($scope.advanced_search.title != '') {
            searchTerm.push($scope.advanced_search.title);
        }

    };

    $scope.selectedTaxonomy = [];
    $scope.selectedTaxonomyIds = [];
    $scope.$watch('advanced_search.taxonomy', function (newValues) {
        $scope.selectedTaxonomy.length = 0;
        $scope.selectedTaxonomyIds.length = 0;
        angular.forEach(newValues, function (item) {
            if (item.selected == true) {
                $scope.selectedTaxonomy.push(item.title);
                $scope.selectedTaxonomyIds.push(item.id);
            }
        });
    }, true);

});

//Assets Modal Controller 
app.controller('ModalCourseAssetsCtrl', function ($scope, $modalInstance, tocService, assetService, project_id, modalFormTitle, ext_res_url, objectService, SweetAlert, from_global_repo) {

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.ts_hash = '?_ts=' + new Date().getTime();
    $scope.modalFormTitle = modalFormTitle;
    $scope.project_id = project_id;
    $scope.taxonomy_id = 0;
    $scope.asset_filter = {"images": true, "videos": true, "audio": true, "other": true, "gadgets": true};
    $scope.asset_local_global = {"local": true, "global": true};
    if (from_global_repo == true) {
        $scope.asset_local_global.local = false;
    }
    $scope.pageNumber = 1;
    $scope.itemsPerPage = 8;
    $scope.ext_res_location = ext_res_url;
    $scope.asset_id = '';
    $scope.isAddAssetDisabled = true;

    //angular.element('#submit_resource').attr('ng-disabled', 'true');
    $scope.getAssets = function (pageNumber) {
        $scope.removeErrors = 1;
        $scope.showCheckAll = 0;
        $scope.showMakeGlobal = 0;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = 8;
        assetService.getMimes().then(function (mimes) {
            if ($scope.asset_id != '') {
                $scope.isAddAssetDisabled = false;
                //angular.element('#submit_resource').attr('disabled', false);
            }
            $scope.mimes = mimes.data;
            assetService.getAssets($scope).then(function (data) {
                $scope.assets = data.data;
                //$scope.assets.Selected = ext_res_url;
                angular.forEach($scope.assets, function (value, key) {
                    if (value.asset_location == $scope.ext_res_location) {
                        value.Selected = $scope.ext_res_location;
                        value.Style = "opacity:1 !important;";
                        //angular.element('#submit_resource').attr('disabled', 'disabled');
                    } else {
                        //angular.element('#submit_resource').attr('disabled', false);
                    }
                });
                $scope.totalAssets = data.totalAssets;
                $scope.removeErrors = angular.isDefined(data.totalAssets) ? data.totalAssets : 0;
//                if ($scope.ext_res_location == '') {
//                    angular.element('#submit_resource').attr('disabled', 'disabled');
//                }
            });
        });
    };
    //$scope.getAssets(1);

    //Get Project Detail
    $scope.getProject = function () {
        $scope.project_name = '';
        if ($scope.object_id != '') {
            objectService.getProjectDetails($scope).then(function (data) {
                $scope.project_name = data.name;
                $scope.project_type_id = data.project_type_id;
                $scope.toc_creation_status = data.toc_creation_status;
                $scope.project = data;
                $scope.actual_permission = data.project_permissions['asset.use_global'].grant;
            });
        }
    };
    $scope.getProject();

    //For Pagination Of Dashboard
    $scope.pageChangeHandlerAsset = function (newPage) {
        //$scope.ext_res_location = '';
        //$scope.ext_res_title = '';
        //$scope.ext_res_type = '';
        //$scope.asset_id = '';
        $scope.getAssets(newPage);
        angular.element('.label-checkbox').removeAttr('style');
    };

    $scope.$watch('search_text', function (val) {
        $scope.searchModel = {"search_text": val};
        $scope.getAssets(1);
    });


    $scope.selectOption = function (asset_id, asset_location, title, type, is_global) {
        if (is_global === 1 && $scope.actual_permission === false) {
            angular.element('.label-checkbox').removeAttr('style');
            $scope.isAddAssetDisabled = true;
            //angular.element('#submit_resource').attr('disabled', 'disabled');
            SweetAlert.swal("Cancelled", "Permission Denied!", "error");
        } else {
            angular.element('.label-checkbox').removeAttr('style');
            angular.element('#check-box-label-' + asset_id).attr('style', 'opacity:1 !important;');
            $scope.ext_res_location = asset_location;
            $scope.ext_res_title = title;
            $scope.ext_res_type = type;
            $scope.asset_id = asset_id;
            $scope.isAddAssetDisabled = false;
            //angular.element('#submit_resource').attr('disabled', false);
            setTimeout(function () {
                angular.element('#submit_resource').focus();
            }, 200);
        }
    };

    $scope.submitResource = function () {
        if ($scope.ext_res_type == 'widget') {
            $scope.ext_res_location += '/launch.html';
        }
        var modalCloseParameter = {ext_res_location: $scope.ext_res_location, ext_res_title: $scope.ext_res_title, ext_res_type: $scope.ext_res_type, asset_id: $scope.asset_id};
        $modalInstance.close(modalCloseParameter);
    };


});



//Toc Assessment Modal Controller 
app.controller('ModalTocAssessmentCtrl', function ($scope, $modalInstance, $modal, project_id, assessment_id, assessmentService, SweetAlert, is_global_project, taxonomyService, keyValueService, tagService, ivhTreeviewMgr, $compile, userService) {

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.project_id = project_id;
    $scope.assessment_id = '';
    $scope.total_assessments = '';
    $scope.search_text = '';
    $scope.assessment_local_global = {};
    $scope.assessment_local_global.local = true;
    $scope.assessment_local_global.global = true;
    $scope.selected_assessment_id = '';
    $scope.assessment_details = {};
    $scope.is_global_project = is_global_project;
    if ($scope.is_global_project == true) {
        $scope.assessment_local_global.local = false;
    }

    $scope.getAssessments = function () {
        assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
            $scope.assessmemnts = data.data.assessment_list;
            $scope.total_assessments = data.data.total;
        });
    };
    $scope.getAssessments();

    $scope.setAssessmentId = function (selected_assessment_id) {
        $scope.selected_assessment_id = selected_assessment_id;
    }

    $scope.submitResource = function () {
        $scope.assessmemnts;
        angular.forEach($scope.assessmemnts, function (value, key) {
            if (value.shortcode == $scope.selected_assessment_id) {
                $scope.assessment_details = value;
            }
        });
        var modalCloseParameter = {assessment_details: $scope.assessment_details};
        $modalInstance.close(modalCloseParameter);
    };

    $scope.apply_filter = function () {
        $scope.getAssessments();
    };
    $scope.preview_question = function (search_name) {
        $scope.preview_button_disabled = true;
        var size = 'lg';
        var modal_form_title = 'Assessment Preview';
        var modalInstance = $modal.open({
            templateUrl: 'templates/assessment/previewModal.html',
            controller: 'GlobalModalPreviewCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                shortcode: function () {
                    return search_name;
                },
                entity_type_id: function () {
                    return 2;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.preview_button_disabled = false;
        }, function () {
            $scope.preview_button_disabled = false;
        });
    };
    // metadata search
    $scope.node_id = 0;
    $scope.advanced_search = {};
    $scope.advanced_search.enableSearch = false;
    $scope.advanced_search.title = '';
    $scope.advanced_search.active = false;
    $scope.advanced_search.isCollapsed = false;
    $scope.advanced_search.heading = 'Advanced Search Result:';

    $scope.advanced_search.key_data = {};
    $scope.advanced_search.value_data = {};
    $scope.advanced_search.totalKeyArea = 0;
    $scope.advanced_search.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.advanced_search.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
            }
        }

    });

    // get tags 
    var tags = [];
    var object_tags = [];


    $scope.advanced_search.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.advanced_search.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tagValue = object_tags;
                }
            }
        }

    });

    // get owners 
    var owners = [];
    var object_owners = [];

    $scope.advanced_search.owner = {
        multiple: true,
        data: owners
    };

    userService.getUsers($scope).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null)
            {
                angular.forEach(data.data, function (value, key) {
                    owners[key] = {id: value.id, text: value.username};
                });
                $scope.advanced_search.owner.data = owners;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    }
                });

                setTimeout(function () {
                    $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));

                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.advanced_search.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.advanced_search.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                } else if (value.key_type == 'time_entry') {
                    $scope.advanced_search.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.advanced_search.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.advanced_search.key_data['key_' + keyArray[1]];
        delete $scope.advanced_search.value_data['key_' + keyArray[1]];
        if (angular.element(".advanced-search-key-values-area").length == 0)
        {
            $scope.advanced_search.totalKeyArea = 1;
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };

    $scope.applyAdvancedSearch = function () {
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.active = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.search_text = '';
        $scope.getAssessments();
    };

    $scope.search = function () {
        $scope.advanced_search.active = false;
        $scope.advanced_search.heading = 'Search Result For: ' + $scope.search_text;
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.getAssessments();
    }

    $scope.clearSearch = function () {
        $scope.advanced_search.active = false;
        $scope.search_text = '';
        $scope.advanced_search.enableSearch = false;
        $scope.getAssessments();
    };

    $scope.toggleAdvanceSearch = function () {
        if ($scope.advanced_search.isCollapsed) {
            $scope.advanced_search.isCollapsed = false;
            if ($scope.advanced_search.active) {
                $scope.advanced_search.enableSearch = true;
            }
        } else {
            $scope.advanced_search.enableSearch = false;
            $scope.advanced_search.isCollapsed = true;
        }

    };

    $scope.applyAdvancedClear = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
        $scope.advanced_search.tagValue = [];
        $scope.advanced_search.ownerValue = [];
        $scope.advanced_search.title = '';
        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
    };

    $scope.generateAdvanceSearchTerm = function () {
        var searchTerm = [];
        var tagList = $scope.advanced_search.tagValue;
        if (tagList.length > 0) {
            angular.forEach(tagList, function (tag) {
                searchTerm.push(tag.text);
            });
        }
        var ownerList = $scope.advanced_search.ownerValue;
        if (ownerList.length > 0) {
            angular.forEach(ownerList, function (owner) {
                searchTerm.push(owner.text);
            });
        }
        var taxonomyList = $scope.selectedTaxonomy;
        if (taxonomyList.length > 0) {
            angular.forEach(taxonomyList, function (taxonomy) {
                searchTerm.push(taxonomy);
            });
        }

        if ($scope.advanced_search.title != '') {
            searchTerm.push($scope.advanced_search.title);
        }

    };

    $scope.selectedTaxonomy = [];
    $scope.selectedTaxonomyIds = [];
    $scope.$watch('advanced_search.taxonomy', function (newValues) {
        $scope.selectedTaxonomy.length = 0;
        $scope.selectedTaxonomyIds.length = 0;
        angular.forEach(newValues, function (item) {
            if (item.selected == true) {
                $scope.selectedTaxonomy.push(item.title);
                $scope.selectedTaxonomyIds.push(item.id);
            }
        });
    }, true);


});