/*****
 * Services for TOC
 * Author - Koushik Samanta
 *  
 */
'use strict';
app.service('tocNodeRuleService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {
        //$rule_array['root_type']['type'] = array('create_child'=>true, 'create_file'=>true);

        this.toc_node_rule = function ($scope) {
            //var deferred = $q.defer();                     
            var toc_node_rule = {};
            if ($scope.project_details.project_type_id == '1') {
                //Project Type :: PXE
                toc_node_rule = {
                    1:
                            {
                                1: {"create_child": true, "create_file": ''},
                                5: {"create_child": '', "create_file": true},
                                6: {"create_child": '', "create_file": true},
                                100: {"create_child": '', "create_file": true}
                            },
                    2:
                            {
                                2: {"create_child": true, "create_file": ''},
                                5: {"create_child": true, "create_file": true},
                                6: {"create_child": '', "create_file": true},
                                100: {"create_child": '', "create_file": true}
                            },
                    3:
                            {
                                3: {"create_child": true, "create_file": ''},
                                5: {"create_child": true, "create_file": true},
                                101: {"create_child": '', "create_file": true},
                                6: {"create_child": '', "create_file": true},
                                100: {"create_child": '', "create_file": true}
                            }
                };
            } else if ($scope.project_details.project_type_id == '2') {
                //Project Type :: LearningMaterial
                toc_node_rule = {
                    "2":
                            {
                                "6": {"create_child": true, "create_file": true}
                            },
                };
            } else if ($scope.project_details.project_type_id == '3') {
                //Project Type :: Visual Content Editor
                toc_node_rule = {
                    "2":
                            {
                                "6": {"create_child": true, "create_file": true}
                            },
                };
            } else if ($scope.project_details.project_type_id == '4') {
                //Project Type :: EBUP / EDUPUB
                toc_node_rule = {
                    1:
                            {
                                1: {"create_child": true, "create_file": ''},
                                5: {"create_child": '', "create_file": true},
                                6: {"create_child": '', "create_file": true},
                                100: {"create_child": '', "create_file": true}
                            },
                    2:
                            {
                                2: {"create_child": true, "create_file": ''},
                                5: {"create_child": true, "create_file": true},
                                211: {"create_child": true, "create_file": true},
                                212: {"create_child": '', "create_file": true},
                                214: {"create_child": true, "create_file": ''},
                                6: {"create_child": '', "create_file": true},
                                100: {"create_child": '', "create_file": true}
                            },
                    3:
                            {
                                3: {"create_child": true, "create_file": ''},
                                101: {"create_child": '', "create_file": true},
                                6: {"create_child": '', "create_file": true},
				214: {"create_child": true, "create_file": ''},
                                100: {"create_child": '', "create_file": true}
                            },
                   101:
                            {
                                101: {"create_child": '', "create_file": true}                                
                            }
                };
            } else if ($scope.project_details.project_type_id == '5') {
                //Project Type :: Course
                toc_node_rule = {
                    1:
                            {
                                1: {"create_child": true, "create_file": true}
                            },
                    2:
                            {
                                2: {"create_child": true, "create_file": true},
                                6: {"create_child": true, "create_file": true},
                                5: {"create_child": true, "create_file": true},
                            },
                    3:
                            {
                                3: {"create_child": true, "create_file": true}
                            }
                };
            }
            return toc_node_rule;
        };
    }]);