/*****
 * Controller for global content
 * Author - Alamgir Hossain Sk * 
 */

'use strict';

app.controller('global_content', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', 'assetService', '$modal', 'objectService', '$filter', '$controller', 'taxonomyService', 'keyValueService', 'tagService', 'userService', 'ivhTreeviewMgr', '$compile', 'tocService', '$window', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, assetService, $modal, objectService, $filter, $controller, taxonomyService, keyValueService, tagService, userService, ivhTreeviewMgr, $compile, tocService, $window) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_id = sessionService.get('uid');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.project_id = '';
        $scope.currentOrderByField = 'name';
        $scope.reverseSort = false;
        $scope.disablePreviewClick=false;
        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }
        //$rootScope.showSearchBox = false;
        $scope.totalContents = 0;

        $scope.contents = {};
        //Call Assets
        $scope.currentPage = 1;
        $scope.pageNumber = '';
        $scope.itemsPerPage = '';
        $scope.getContents = function (pageNumber) {
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 12;
            assetService.getGlobalContents($scope).then(function (data) {
                $scope.contents = data.data;
                $scope.totalContents = data.totalContents;
                $scope.removeErrors = data.totalContents;
            });
        };
        $scope.getContents(1);
        $scope.newPageNumber = 1;
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerContent = function (newPage) {
            $scope.newPageNumber = newPage;            
            $scope.getContents(newPage);
        };

        /*$scope.deleteAsset = function (id, project_id, newNumber) {
         SweetAlert.swal({
         title: "Are you sure?",
         text: "You will not be able to recover this asset!",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         confirmButtonText: "Yes, delete it!",
         cancelButtonText: "No, cancel!",
         closeOnConfirm: false,
         closeOnCancel: false
         }, function (isConfirm) {
         if (isConfirm) {
         var viewMode = $scope.viewMode;
         jQuery('#asset_' + id + viewMode).hide();
         assetService.delAsset(id, project_id).then(function () {
         $scope.getAssets(newNumber);
         });
         SweetAlert.swal("Success", "Asset has been deleted", "success");
         } else {
         SweetAlert.swal("Cancelled", "Asset is safe :)", "error");
         }
         });
         };*/

        $scope.editbox = 0;
        $scope.viewbox = 0;
        $scope.filterbox = 0;

        $scope.isConfirmClick=false;
        /*
         * @description add Global content
         * @author Koushik Samanta  
         * @param {type} size
         * @returns {undefined}
         */
        $scope.addGlobalContentObjectForm = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/add_global_content_object.html',
                controller: 'ModalGlobalContentObjectAddCtrl',
                size: 'lg',
                backdrop: 'static',
                keyboard: false,    
                resolve: {
                    list: function () {
                        return {}
                    },
                    newPageNumber: function () {
                        return 1
                    },                    
                    modal_header1_lebel: function () {
                        return 'Create Content Object'
                    },
                    modal_header2_lebel: function () {
                        return 'You are trying to add new content object'
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.newPageNumber = $scope.currentPage = 1;
                $scope.currentOrderByField='created_at';
                $scope.reverseSort=true;
                $scope.getContents(1);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        /*
         * @description add Global content
         * @author Koushik Samanta  
         * @param {type} size
         * @returns {undefined}
         */
        $scope.editGlobalContentObjectForm = function (list, size) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/edit_global_content_object.html',
                controller: 'ModalGlobalContentObjectAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    list: function () {
                        return list
                    },
                    newPageNumber: function () {
                        return $scope.newPageNumber
                    },
                    modal_header1_lebel: function () {
                        return 'Update Content Object'
                    },
                    modal_header2_lebel: function () {
                        return 'You are trying to update content object'
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {                
                $scope.newPageNumber = modalCloseParameter.newPageNumber;                
                $scope.getContents($scope.newPageNumber);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.previewContent = function (size, id) {                  
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/globalModalContentView.html',
                controller: 'ModalInstanceContentCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    object_id: function () {
                        return id;
                    },
                    modalFormText: function () {
                        return "You are trying to view a content";
                    },
                    modalFormTile: function () {
                        return "Preview Content";
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
                $scope.disablePreviewClick = false;
                $log.info('Modal dismissed at: ' + new Date());
            });       
        };

        $scope.previewLOContent = function (size, content) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/LOModalContentView.html',
                controller: 'LODataController', 
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    content: function () {
                        return content;
                    },
                    project_id: function () {
                        return content.project_id;
                    },
                    modalFormText: function () {
                        return "You are trying to view a content";
                    },
                    modalFormTile: function () {
                        return "Preview Content";
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        
        $scope.previewExtResource = function(content){
            $window.open(content.metadata.ext_res_url, '_blank');
        };

        $scope.deleteContent = function (id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this content!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                var param={};
                param.id=id;
                if (isConfirm) {
                    assetService.delContent(param).then(function (data) {
                        var response = data.status;
                        if(response == 200) {
                            $scope.getContents(1);
                            SweetAlert.swal("Success", "Content has been deleted", "success");
                        } else {
                            SweetAlert.swal("Error", "Something went wrong", "error");
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Content is safe :)", "error");
                }
            });
        };

        $scope.cancel = function () {
            $('.right-panel-navpanel').hide();
            $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-12');
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.getContents(1);
        };

        $scope.$on('apply-advaced-search', function (event, args) {
            $scope.getContents(1);
        });

        $scope.$on('clear-advaced-search', function (event, args) {
            $scope.getContents(1);
        });

        $scope.$on('apply-search', function (event, args) {
            $scope.getContents(1);
        });

        //Go to Desired editor
        $scope.goToEditor = function (list) {
            //console.log(list);
            $scope.project_id = list.project_id;
            $scope.object_id = list.id;
            tocService.checkObjectLock($scope).then(function (data) {
                if (Number(data.current_lock)) {
                    SweetAlert.swal("Cancelled", "You cannot access this content right now, as it is locked. Please try again later!", "error");
                    return false;
                } else {
                    if (list.project_type_id == 4) {
                        //Edupub
                        $window.location.href = "edupub_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + list.project_id + '&node_id=' + list.id + '&node_title=' + encodeURIComponent(list.name) + '&node_type_name=&node_type_id=&project_type_id=' + list.project_type_id + '&section_id=&user_id=' + $scope.user_id+'&content_from=global_repo_content';
                    } else if (list.project_type_id == 5) {
                        //Course
                        if(list.object_type_id=='101'){
                            $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + list.project_id + '&node_id=' + list.id + '&node_title=' + encodeURIComponent(list.name) + '&node_type_name=&node_type_id=&project_type_id=' + list.project_type_id + '&section_id=&user_id=' + $scope.user_id+'&content_from=global_repo_content';
                        }else if(list.object_type_id=='118'){
                            $window.location.href = "/#/global-lo-content/"+list.id+"?access_token=" + sessionService.get('access_token') + '&section_id=&user_id=' + $scope.user_id;
                        }
                    }
                }
            });
        }


    }]);

//ModalGlobalContentObjectAddCtrl
app.controller('ModalGlobalContentObjectAddCtrl', function ($scope, sessionService, $modalInstance, assetService, patternService, templateService, list, $filter, tagService, keyValueService, taxonomyService, $compile, $modal, SweetAlert, ivhTreeviewMgr, newPageNumber, modal_header1_lebel, modal_header2_lebel, objectService, assessmentService) {
    $scope.project_type = 4;
    $scope.newPageNumber = newPageNumber;    
    $scope.selected_template = {};
    $scope.list = list;
    $scope.object_id = '';
    $scope.modal_header1_lebel = modal_header1_lebel;
    $scope.modal_header2_lebel = modal_header2_lebel;
    $scope.object_name_placeholder = 'Name of Content Screen';
    $scope.is_assessment=false;
    $scope.assessment_source='';
    $scope.section_page = 'content_screen';
    
    $scope.updateObjectNameLabel = function(label, section_page){
        $scope.object_name_placeholder = 'Name of '+label;        
        $scope.is_assessment=false;
        $scope.section_page = section_page;
        if($scope.section_page=='assessment'){
            $scope.is_assessment=true;
        }        
    }
    
    $scope.setAssessmentTitle = function (assessment_id, global_project_id) {
        if (angular.isDefined($scope.assessment_source) && $scope.assessment_source == 'FROST') {
            assessmentService.getAssessments($scope, global_project_id).then(function (data) {
                $scope.assessmemnts = data.data.assessment_list;
                if (angular.isDefined(data.data.assessment_list[0])) {
                    $scope.assessment_details = data.data.assessment_list[0];
                }
            });
        }
    };
    $scope.set_asset_thumbnail = function () {
        $scope.frost_resource_asset_type = '';
        $scope.frost_resource_asset_name = '';
        if (angular.isDefined($scope.ext_res_url) && $scope.ext_res_url != '') {
            var pp = $scope.ext_res_url.split('/');
            if (angular.isDefined(pp[6]) && pp[6] != '') {
                assetService.getAssetById(pp[6]).then(function (data) {
                    //console.log(data.data);
                    $scope.frost_resource_asset_type = data.data.object_type_name;
                    $scope.frost_resource_asset_name = data.data.name;
                });
            }
        }
    }
    
    //This is used for Edit Mode
    if(angular.isDefined($scope.list.id) && $scope.list.id!==''){ 
        //console.log($scope.list);
        $scope.object_name = $scope.list.name;
        $scope.object_id = $scope.list.id;       
        switch($scope.list.object_type_id){
            case 101:
                $scope.object_name_placeholder = 'Name of Content Screen';
                $scope.updateObjectNameLabel('Content Screen', 'content_screen');
                break;
            case 114:
                $scope.object_name_placeholder = 'Name of Resource';
                $scope.updateObjectNameLabel('Resource', 'link');
                $scope.ext_res_type=angular.isDefined($scope.list.metadata.ext_res_type) ? $scope.list.metadata.ext_res_type : '';
                $scope.ext_res_url=angular.isDefined($scope.list.metadata.ext_res_url) ? $scope.list.metadata.ext_res_url : '';
                $scope.asset_id=angular.isDefined($scope.list.metadata.asset_id) ? $scope.list.metadata.asset_id : '';
                break;
            case 115:
                $scope.object_name_placeholder = 'Name of Resource';
                $scope.updateObjectNameLabel('Resource', 'link');
                $scope.ext_res_type=angular.isDefined($scope.list.metadata.ext_res_type) ? $scope.list.metadata.ext_res_type : '';
                $scope.ext_res_url=angular.isDefined($scope.list.metadata.ext_res_url) ? $scope.list.metadata.ext_res_url : '';
                $scope.asset_id=angular.isDefined($scope.list.metadata.asset_id) ? $scope.list.metadata.asset_id : '';
                $scope.set_asset_thumbnail();
                break;
            case 116:
                $scope.object_name_placeholder = 'Name of Assessment';
                $scope.updateObjectNameLabel('Assessment', 'assessment');
                $scope.assessment_source=angular.isDefined($scope.list.metadata.assessment_source) ? $scope.list.metadata.assessment_source : '';
                $scope.assessment_id=angular.isDefined($scope.list.metadata.assessment_id) ? $scope.list.metadata.assessment_id : '';
                break;
            case 117:
                $scope.object_name_placeholder = 'Name of Assessment';
                $scope.updateObjectNameLabel('Assessment', 'assessment');
                $scope.assessment_source=angular.isDefined($scope.list.metadata.assessment_source) ? $scope.list.metadata.assessment_source : '';
                $scope.assessment_id=angular.isDefined($scope.list.metadata.assessment_id) ? $scope.list.metadata.assessment_id : '';
                $scope.setAssessmentTitle($scope.assessment_id, $scope.list.project_id);
                break;
            case 118:
                $scope.object_name_placeholder = 'Name of Learning Object';
                $scope.updateObjectNameLabel('Learning Object', 'lo');
                break;
            default:
                break;
        }
    }    
    $scope.applyMetadata = 1;       
    
    $scope.deleteAsset = function () {
        $scope.frost_resource_asset_name = '';
        $scope.ext_res_url = '';
        $scope.assessment_id = '';
        $scope.assessment_details = '';
       
        $scope.ext_res_type = "";
        $scope.ext_res_url = "";
        $scope.ext_res_title = '';
        $scope.asset_id = "";
    };

    $scope.onChangeResourceType = function () {
        $scope.ext_res_url = "";
        $scope.ext_res_title = null;
        
        $scope.asset_id = "";
        $scope.assessment_source = "";
        $scope.assessment_id = "";
        $scope.formError['external_resources_message'] = '';
        $scope.formError['resource_asset_message'] = '';
    };

    $scope.deleteAssessment = function () {
        $scope.assessment_id = '';
        $scope.ext_res_url = '';
        $scope.assessment_id = '';
        $scope.assessment_details = '';
       
        $scope.ext_res_type = "";
        $scope.ext_res_url = "";
        $scope.ext_res_title = '';
        $scope.asset_id = "";
    };

    $scope.onChangeAssessmentType = function () {
        $scope.assessment_id = '';
        $scope.assessment_details = {};
        $scope.formError['assessment_id'] = '';
        
        $scope.ext_res_type = "";
        $scope.ext_res_url = "";
        $scope.ext_res_title = '';
        $scope.asset_id = "";
    };
    
    //Set Global Project Id
    $scope.project_type_id = $scope.project_type;
    objectService.getGlobalProjectId($scope).then(function (data) {
        $scope.global_project_id = data.global_project_id;
    });

    //====Metadata Starts======//
    $scope.tags = [];
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.metadata = {};
    // taxonomy 
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.object_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.children)) { 
                $scope.taxonomy = data.data.children;  
                $scope.findSelectedTaxonomy({children:$scope.taxonomy});
                // expand nodes
                /*setTimeout(function () {
                    // expand nodes
                    ivhTreeviewMgr.expandRecursive($scope.taxonomy, $scope.taxonomy);   
                }, 1000);*/                             
            }            
        }
        if (data.count === 0) {
            setTimeout(function () {
                angular.element('#taxonomyStatus').html('No active taxonomy available in the management');
            }, 500);
        }
    });
    //This method is used to select parent indeterminately
    $scope.findSelectedTaxonomy = function(taxonomy_arr){
        if (taxonomy_arr.children.length>0) {
            for (var i = 0; i < taxonomy_arr.children.length; i += 1) {
                var currentChild = taxonomy_arr.children[i];
                $scope.findSelectedTaxonomy(currentChild);
                if(currentChild.selected==true){
                    ivhTreeviewMgr.select($scope.taxonomy, currentChild);
                }
            }
        }        
    };
    
    
    
    // get tags 
    var tags = [];
    var object_tags = [];
    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.object_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.tags)) {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.metadata.tagValue = object_tags;
                }
            }
        }
    });
    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.object_id).then(function (data) {
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });
            }
            setTimeout(function () {
                // populate old key values
                if (Object.keys(data.data.object_key_values).length)
                {
                    angular.forEach(data.data.object_key_values, function (object_key_value) {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                        $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                        if (object_key_value.key_type == 'select_list')
                        {
                            $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                        } else {
                            if (object_key_value.key_type == 'number_entry') {
                                $scope.value_data['key_' + $scope.totalKeyArea] = parseFloat(object_key_value.value);
                            } else {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                            }
                        }

                    });
                } else {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }
            }, 500);
        }
    });
    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };
    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" ng-keypress="keyPressedEvent($event)" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" step="any" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="key-values-area row" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };
    //====Metadata Ends======//



    //Get All templates
    $scope.template_status = 1001;
    $scope.template_local_global = {};
    $scope.template_local_global.local = false;
    $scope.template_local_global.global = true;
    templateService.getTemplate($scope).then(function (data) {
        $scope.templates = data.data;
    });
    //Add Global Toc content
    $scope.add = function () {
        $scope.isConfirmClick=true;
        $scope.formError = {object_name: '', project_type: '', templete_id: ''};
        var param = {};
        param.object_name = $scope.object_name;
        param.project_type = $scope.project_type;
        param.templete_id = '';
        param.section_page=$scope.section_page;
        if ($scope.project_type == 5) {
            //For Online course		
            param.templete_id = ( angular.isUndefined($scope.selected_template) || ($scope.selected_template === null) ? '' : $scope.selected_template.id);
        }

        assetService.addGlobalContentObject($scope, param).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                SweetAlert.swal("Success", savedResponse.message, "success");
                $modalInstance.close();
            } else {
                if (angular.isDefined(savedResponse.error.object_name)) {
                    $scope.formError['object_name'] = savedResponse.error.object_name['0'];
                }
                if (angular.isDefined(savedResponse.error.project_type)) {
                    $scope.formError['project_type'] = savedResponse.error.project_type['0'];
                }
                if (angular.isDefined(savedResponse.error.templete_id)) {
                    $scope.formError['templete_id'] = savedResponse.error.templete_id['0'];
                }
                if (angular.isDefined(savedResponse.error.assessment_source)) {
                    $scope.formError['assessment_source'] = savedResponse.error.assessment_source['0'];
                }
                if (angular.isDefined(savedResponse.error.assessment_id)) {
                    $scope.formError['assessment_id'] = savedResponse.error.assessment_id['0'];
                }
                if (angular.isDefined(savedResponse.error.ext_res_type)) {
                    $scope.formError['ext_res_type'] = savedResponse.error.ext_res_type['0'];
                }
                if (angular.isDefined(savedResponse.error.asset_id)) {
                    $scope.formError['asset_id'] = savedResponse.error.asset_id['0'];
                }
                if (angular.isDefined(savedResponse.error.resource_asset_message)) {
                    $scope.formError['resource_asset_message'] = savedResponse.error.resource_asset_message['0'];
                }
                if (angular.isDefined(savedResponse.error.external_resources_message)) {
                    $scope.formError['external_resources_message'] = savedResponse.error.external_resources_message['0'];
                }
                
                $scope.isConfirmClick=false;
            }
        });
        
    };
    
    //Edit Global Toc content
    $scope.edit = function () {
        $scope.formError = {object_name: '', project_type: '', templete_id: ''};
        var param = {};
        param.object_name = $scope.object_name;
        param.object_id = $scope.object_id;
        param.project_id = list.project_id;  
        $scope.tags = $scope.metadata.tagValue;
        $scope.isConfirmClick=true;
        assetService.editGlobalContentObject($scope, param).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                SweetAlert.swal("Success", savedResponse.message, "success");
                $modalInstance.close({newPageNumber: $scope.newPageNumber});
            } else {
                if (angular.isDefined(savedResponse.error.object_name)) {
                    $scope.formError['object_name'] = savedResponse.error.object_name['0'];
                }
                if (angular.isDefined(savedResponse.error.assessment_source)) {
                    $scope.formError['assessment_source'] = savedResponse.error.assessment_source['0'];
                }
                if (angular.isDefined(savedResponse.error.assessment_id)) {
                    $scope.formError['assessment_id'] = savedResponse.error.assessment_id['0'];
                }
                if (angular.isDefined(savedResponse.error.ext_res_type)) {
                    $scope.formError['ext_res_type'] = savedResponse.error.ext_res_type['0'];
                }
                if (angular.isDefined(savedResponse.error.resource_asset_message)) {
                    $scope.formError['resource_asset_message'] = savedResponse.error.resource_asset_message['0'];
                }
                if (angular.isDefined(savedResponse.error.external_resources_message)) {
                    $scope.formError['external_resources_message'] = savedResponse.error.external_resources_message['0'];
                }                
                $scope.isConfirmClick=false;
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.set_section_page=function(project_type){
        if(project_type=='5'){
           $scope.section_page='lo';
        }else if(project_type=='4'){
            $scope.section_page='';
        }
        $scope.project_type_id = project_type;
        objectService.getGlobalProjectId($scope).then(function (data) {
            $scope.global_project_id = data.global_project_id;
        });
    };
    
    $scope.addAssessmentModal = function () {
        var size = 'lg';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/assessmentModalContent.html',
            controller: 'ModalTocAssessmentCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                project_id: function () {
                    return $scope.global_project_id;
                },
                assessment_id: function () {
                    return $scope.assessment_id;
                },
                assessment_details: function () {
                    return $scope.assessment_details;
                },
                is_global_project: function () {
                    return true;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.assessment_details = modalCloseParameter.assessment_details;
            $scope.assessment_id = modalCloseParameter.assessment_details.id;
            $scope.ext_res_url = '';
            $scope.ext_res_title = '';
            $scope.asset_id = '';
        });
    };
    
    //Assets Modal 
    $scope.ext_res_title = null;
    $scope.addAssestModal = function () {
        var size = 'lg';
        var modal_form_title = 'Asset Library';
        var modalInstance = $modal.open({
            templateUrl: 'templates/toc/externalResourceModalContent.html',
            controller: 'ModalCourseAssetsCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                project_id: function () {
                    return $scope.global_project_id;
                },
                ext_res_url: function () {
                    return $scope.ext_res_url;
                },
                from_global_repo: function(){
                    return true;
                }
            }
        });

        modalInstance.result.then(function (modalCloseParameter) {
            $scope.ext_res_url = modalCloseParameter.ext_res_location;
            $scope.ext_res_title = modalCloseParameter.ext_res_title;
            $scope.assessment_id = '';
            $scope.assessment_source = '';
            $scope.asset_id = modalCloseParameter.asset_id;
            $scope.set_asset_thumbnail();
            //$scope.ext_res_type = modalCloseParameter.ext_res_type;
        });
    };
    
    
    
    
    
});