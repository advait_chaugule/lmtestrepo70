/*****
 * Services for TaxonomyServices
 * Author - Koushik Samanta 
 *  
 */
'use strict';

//Start of aclService
app.service('taxonomyService', ['$http', '$q', 'sessionService', '$rootScope', function ($http, $q, sessionService, $rootScope) {

    /*
     * Service to add Taxonomy
     */
    $rootScope.taxonomy_id = '5e5eb57f-89ea-4654-9e65-600cad6f84e8';

    this.addTaxonomy = function (formdata) {
        var params = formdata;
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: serviceEndPoint + 'api/v1/cfitem/create',
            data: params,
            headers: {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            },
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });

        return deferred.promise;
    }
    /*
     * Service to add Taxonomy
     */
    this.editTaxonomy = function (formdata, cfitem_id) {

        var params = formdata;
        var deferred = $q.defer();
        var url = serviceEndPoint + 'api/v1/cfitem/edit/' + cfitem_id;
        var config = {
            'headers': {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            }
        };
        params["_method"] = "PUT"

        $http.post(url, params, config).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            //deferred.resolve(data);
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    }
    /*
     * Service to add Taxonomy
     */
    this.delTaxonomy = function ($scope, taxonomy_id) {
        var params = [];
        var deferred = $q.defer();
        var url = serviceEndPoint + 'api/v1/taxonomy/delete/' + taxonomy_id;
        var config = {
            'headers': {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            }
        };
        params["_method"] = "DELETE"

        $http.post(url, params, config).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            //deferred.resolve(data);
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    }
    /*
     * Service to asset taxonomy mapping
     */
    /*this.taxonomyObjectMapping = function ($scope) {
        var deferred = $q.defer();
        $http.post(serviceEndPoint + 'api/v1/taxonomy-object?access_token=' + sessionService.get('access_token'), {
            taxonomy_id: $scope.taxonomy_id,
            asset_ids: $scope.asset_ids
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    }*/

    /**
     * Get Taxonomy Tree
     */
    this.getTaxonomy = function (taxonomy_id) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();
        var queryString = '';

        $http.get(serviceEndPoint + 'api/v1/getCFPackage/' + taxonomy_id, {
            headers: new Headers({
                'Authorization': params.access_token,
            })
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    this.getCFItemAssociations = function(itemId){
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();
        var queryString = '';

        $http.get(serviceEndPoint + 'api/v1/CFItemAssociations/' + itemId, {
            headers: new Headers({
                'Authorization': params.access_token,
            })
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    }


    this.getMappedTaxonomy = function (project_id) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();
        var queryString = '';

        $http.get(serviceEndPoint + 'api/v1/project/nodes_mapped/' + project_id, {
            headers: new Headers({
                'Authorization': params.access_token,
            })
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    /**
     * Get Taxonomy Detail By Id
     */
    this.getTaxonomyDetailsById = function ($scope, taxonomy_id) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();

        var queryString = '';
        $http.get(serviceEndPoint + '/api/v1/getTaxonomyDetails/{taxonomyId}' + taxonomy_id + '?access_token=' + sessionService.get('access_token') + queryString, {}).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    /**
     * Get CfItem Detail By Id
     */
    this.getCfItemDetailsById = function ($scope, cfitem_id) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();

        var queryString = '';
        $http.get(serviceEndPoint + 'api/v1/cfitem/fetch/' + cfitem_id, {
            headers: new Headers({
                'Authorization': params.access_token,
            })
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    /**
     * Get Taxonomy By Object Id
     */
    this.getTaxonomyByObject = function ($scope, object_id) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();

        var queryString = '';
        $http.get(serviceEndPoint + 'api/v1/taxonomy/taxonomy_id/' + taxonomy_id + '?access_token=' + sessionService.get('access_token') + queryString, {}).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    /**
     * Get Taxonomy By Object Id
     */
    this.rearrangeTaxonomyTree = function (arrayOfOrder, taxonomy_id, parent_id) {

        var deferred = $q.defer();
        var requestUrl = serviceEndPoint + 'api/v1/taxonomy/rearrange/' + taxonomy_id;

        var requestData = {
            'target_id': parent_id,
            'arrayOfOrder': arrayOfOrder
        };

        var config = {
            'headers': {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            }
        };

        $http.post(requestUrl, requestData, config).success(function (data, status, headers, config) {
            console.log(data);
            //deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            //deferred.resolve(data);
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    this.getMetadataUsage = function ($scope, taxonomy_id) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();
        var queryString = '';
        if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
            queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
        }
        $http.get(serviceEndPoint + 'api/v1/objects-taxonomy-usage/taxonomy_id/' + taxonomy_id + '?access_token=' + sessionService.get('access_token') + queryString, {}).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    /*
     * Change status to active or inactive
     */
    this.changeStatus = function (id, $scope) {
        var deferred = $q.defer();
        $http.post(serviceEndPoint + 'api/v1/taxonomy-status/taxonomy_id/' + id + '?access_token=' + sessionService.get('access_token'), {
            _method: 'PUT'
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            deferred.resolve(data);
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    // get list of taxonomies
    this.listTaxonomies = function ($scope) {
        var params = {};
        params.Authorization = sessionService.get('access_token');
        // params.exclude_role_ids = angular.isDefined($scope.exclude_role_ids) ? JSON.stringify($scope.exclude_role_ids) : '[]';
        var deferred = $q.defer();

        $http({
            method: 'GET',
            headers: params,
            url: serviceEndPoint + 'api/v1/taxonomy/list',
            params: params
        }).success(function (data, status, headers, config) {
            console.log(data);
            console.log(new Date());
            deferred.resolve(data); //Return the roles list
        }).error(function (data, status, headers, config) {
            // console.log("Not Working");
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };

    this.searchTaxonomies = function (search) {
        search = encodeURIComponent(search);
        var params = {};
        params.Authorization = sessionService.get('access_token');
        // params.exclude_role_ids = angular.isDefined($scope.exclude_role_ids) ? JSON.stringify($scope.exclude_role_ids) : '[]';
        var deferred = $q.defer();

        $http({
            method: 'GET',
            headers: params,
            url: serviceEndPoint + 'api/v1/taxonomy/filter/title?search_key=' + search,
            params: params
        }).success(function (data, status, headers, config) {
            console.log(data);
            deferred.resolve(data); //Return the roles list
        }).error(function (data, status, headers, config) {
            // console.log("Not Working");
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    }
    /*
     * Export taxonomy tree as JSON
     */
    // this.exportJson = function (id) {
    //     var params = {};
    //     params.access_token = sessionService.get('access_token');
    //     var deferred = $q.defer();
    //     window.location.href = serviceEndPoint + 'api/v1/exportCFPackageJson/'+ id;
    // };

    this.caseAssociationListTypes = function ($scope) {
        var params = {};
        params.Authorization = sessionService.get('access_token');
        // params.exclude_role_ids = angular.isDefined($scope.exclude_role_ids) ? JSON.stringify($scope.exclude_role_ids) : '[]';
        var deferred = $q.defer();

        $http({
            method: 'GET',
            headers: params,
            url: serviceEndPoint + 'api/v1/caseAssociation/listTypes',
            params: params
        }).success(function (data, status, headers, config) {
            console.log(data);
            deferred.resolve(data); //Return the roles list
        }).error(function (data, status, headers, config) {
            // console.log("Not Working");
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };


    this.createAssociation = function (origin_id, destination_ids, association_type) {
        var params = {
            origin_node_id: origin_id,
            destination_node_ids: destination_ids,
            association_type_id: association_type
        };
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: serviceEndPoint + 'api/v1/caseAssociation/create',
            data: params,
            headers: {
                'Authorization': sessionService.get('access_token'),
                'Accept': 'application/json'
            },
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });

        return deferred.promise;
    };

    this.getProjectDetail = function (projectid) {
        var params = {};
        params.access_token = sessionService.get('access_token');
        var deferred = $q.defer();
        $http.get(serviceEndPoint + 'api/v1/project/' + projectid, {
            headers: new Headers({
                'Authorization': params.access_token,
            })
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status, headers, config) {
            $rootScope.unauthorised_redirection(data.status);
        });
        return deferred.promise;
    };
}]);