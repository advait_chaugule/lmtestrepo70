/*****
 * Controller for project
 * Author - Arikh Akher * 
 */

'use strict';
app.controller('taxonomy', ['$scope', '$http', 'sessionService', '$location', 'taxonomyService', 'cfService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$window', function ($scope, $http, sessionService, $location, taxonomyService, cfService, $rootScope, $modal, $log, SweetAlert, $routeParams, $window) {

    $scope.access_token = sessionService.get('access_token');
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    var userinfo = JSON.parse(sessionService.get('userinfo'));

    //$scope.project_id = $routeParams.project_id;
    $scope.taxonomy_id = $routeParams.taxonomy_id;
    $scope.searchModel = {};
    $scope.searchModel.search_text = '';
    $scope.language = '';
    $scope.item_type = '';
    $scope.selectionType = "import";
    if (!$scope.access_token || userinfo == null) {
        $location.path('logout');
        return true;
    }

    var selectedKey = '';
    $scope.arrayIterateobj = function (lists, id) {
        angular.forEach(lists, function (value, key) {
            if (value.id == id) {
                selectedKey = key;
            }
        });
        return selectedKey;
    }


    //get cfitem details
    // $scope.getdata = function () {
    //     $scope.taxonomy_full_statement = taxonomy.full_statement;
    //     $scope.taxonomy_human_coding_scheme = taxonomy.human_coding_scheme;
    //     $scope.taxonomy_list_enumeration = taxonomy.list_enumeration;
    //     $scope.taxonomy_abbreviated_statement = taxonomy.abbreviated_statement;
    //     $scope.taxonomy_education_level = taxonomy.education_level;
    //     $scope.taxonomy_note = taxonomy.note;
    //     $scope.taxonomy_concept_keywords = taxonomy.concept_keyword;
    //     $scope.taxonomy_concept_uri = taxonomy.concept_uri;
    //     $scope.taxonomy_license_uri = taxonomy.license_uri;

    //     var selectedlanguage = $scope.arrayIterateobj($scope.language, taxonomy.language_id);
    //     $scope.taxonomy_language_id = $scope.language[selectedlanguage];

    //     var selecteditemytype = $scope.arrayIterateobj($scope.item, taxonomy.item_type_id);
    //     $scope.taxonomy_item_type_id = $scope.item_type[selecteditemytype];
    // }

    //Call Taxonomy

    $scope.taxonomy = { 'children': [] };
    $scope.getTaxonomy = function (taxonomy_id, cfItemID, data_message) {
        taxonomyService.getTaxonomy(taxonomy_id).then(function (data) {
            if (data.data == null || data.data == '') {
                window.location.href = '#/taxonomies';
            } else {
                $scope.taxonomy = data.data;
                // $scope.getId = data.data.id;
                $scope.total = data.count;
                $scope.message = data_message;
                if ($scope.message != 'Data updated successfully.' && $scope.message != 'CFItem created successfully.') {
                    setTimeout(function () {
                        angular.element(".cf_title").eq(0).addClass('active');
                        angular.element('.link-to-preview').triggerHandler('click');
                        $(".toc-panel-container").animate({ scrollTop: 0 }, 600);
                    }, 300);
                }
                else if ($scope.message == 'CFItem created successfully.') {
                    setTimeout(function () {
                        var taxonomyNodes = $('.lirow');
                        angular.element(taxonomyNodes).removeClass('active');
                        angular.element('#' + cfItemID).find('.cf_title').addClass('active');
                        angular.element('#' + cfItemID).find('span.link-to-preview').triggerHandler('click');
                        var scrollToPos = $('#' + cfItemID).find('span.link-to-preview').offset().top + $(".toc-panel-container").scrollTop() - $('.toc-panel-container').offset().top - 20;
                        $(".toc-panel-container").animate({ scrollTop: scrollToPos }, 600);
                    }, 300);
                }
                else if ($scope.message == 'Data updated successfully.') {
                    setTimeout(function () {
                        angular.element('#' + cfItemID).find('.cf_title').addClass('active');
                        angular.element('#' + cfItemID).find('span.link-to-preview').triggerHandler('click');
                    }, 300)

                }


            }

        });
    };

    $scope.getMappedTaxonomy = function (project_id,cfItemID, data_message) {
        console.log('getMappedTaxonomy ',project_id);
        taxonomyService.getMappedTaxonomy(project_id).then(function (data) {
            if (data.data == null || data.data == '') {
                window.location.href = '#/dashboard';
            } else {
                $scope.taxonomy = data.data;
                // $scope.getId = data.data.id;
                $scope.total = data.count;
                $scope.message = data_message;
                if ($scope.message != 'Data updated successfully.' && $scope.message != 'CFItem created successfully.') {
                    setTimeout(function () {
                        angular.element(".cf_title").eq(0).addClass('active');
                        angular.element('.link-to-preview').triggerHandler('click');
                        $(".toc-panel-container").animate({ scrollTop: 0 }, 600);
                    }, 300);
                }
                else if ($scope.message == 'CFItem created successfully.') {
                    setTimeout(function () {
                        var taxonomyNodes = $('.lirow');
                        angular.element(taxonomyNodes).removeClass('active');
                        angular.element('#' + cfItemID).find('.cf_title').addClass('active');
                        angular.element('#' + cfItemID).find('span.link-to-preview').triggerHandler('click');
                        var scrollToPos = $('#' + cfItemID).find('span.link-to-preview').offset().top + $(".toc-panel-container").scrollTop() - $('.toc-panel-container').offset().top - 20;
                        $(".toc-panel-container").animate({ scrollTop: scrollToPos }, 600);
                    }, 300);
                }
                else if ($scope.message == 'Data updated successfully.') {
                    setTimeout(function () {
                        angular.element('#' + cfItemID).find('.cf_title').addClass('active');
                        angular.element('#' + cfItemID).find('span.link-to-preview').triggerHandler('click');
                    }, 300)

                }


            }

        });
    };
    // $scope.getMappedTaxonomy($routeParams.taxonomy_id);
    $scope.getTaxonomy($routeParams.taxonomy_id);
    ///
    $scope.create_button = false;
    // $scope.openTaxonomyModal = function (size, taxonomy_parent_id, entity_type) {
    //     console.log(taxonomy_parent_id);
    //     $scope.create_button = true;
    //     var modalInstance = $modal.open({
    //         templateUrl: 'templates/taxonomy/taxonomyModalContent.html',
    //         controller: 'ModalInstanceTaxonomyCtrl',
    //         size: size,
    //         backdrop: 'static',
    //         keyboard: false,
    //         resolve: {
    //             modalFormText: function () {
    //                 return "You are trying to add a Taxonomy.";
    //             },
    //             modalFormTile: function () {
    //                 return "Add Taxonomy";
    //             },
    //             taxonomy: function () {
    //                 return $scope.taxonomy;
    //             },
    //             taxonomy_parent_id: function () {
    //                 return taxonomy_parent_id;
    //             }
    //         }
    //     });
    //     modalInstance.result.then(function () {
    //         $scope.getTaxonomy();
    //         $scope.create_button = false;
    //     }, function () {
    //         $scope.create_button = false;
    //         //$log.info('Modal dismissed at: ' + new Date());
    //     });
    // };


    // Add dummy data
    // $scope.addDummy = function (formdata) {
    //     console.log("Added");
    //     $scope.formdata = formdata;
    //     $scope.formError = { 'taxonomy_full_statement': '', 'taxonomy_human_coding_scheme': '', 'error': '' };
    //     if (formdata.$valid == true) {
    //         var param_data = {};
    //         if (!angular.isUndefined($scope.taxonomy_parent_id)) {
    //             param_data['parent_id'] = $scope.taxonomy_parent_id;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_full_statement)) {
    //             param_data['full_statement'] = $scope.taxonomy_full_statement;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_human_coding_scheme)) {
    //             param_data['human_coding_scheme'] = $scope.taxonomy_human_coding_scheme;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_list_enumeration)) {
    //             param_data['list_enumeration'] = $scope.taxonomy_list_enumeration;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_abbreviated_statement)) {
    //             param_data['abbreviated_statement'] = $scope.taxonomy_abbreviated_statement;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_education_level)) {
    //             param_data['education_level'] = $scope.taxonomy_education_level;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_note)) {
    //             param_data['note'] = $scope.taxonomy_note;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_concept_keywords)) {
    //             param_data['concept_keywords'] = $scope.taxonomy_concept_keywords;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_concept_uri)) {
    //             param_data['concept_uri'] = $scope.taxonomy_concept_uri;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_license_uri)) {
    //             param_data['license_uri'] = $scope.taxonomy_license_uri;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_language_id)) {
    //             param_data['language_id'] = $scope.taxonomy_language_id.id;
    //         }

    //         if (!angular.isUndefined($scope.taxonomy_item_type_id)) {
    //             param_data['item_type_id'] = $scope.taxonomy_item_type_id.id;
    //         }

    //         param_data['root_node_id'] = $scope.root_node_id;

    //         taxonomyService.addTaxonomy(param_data).then(function (data) {
    //             if (data.status == 201) {
    //                 $scope.resetForm();
    //                 $scope.getTaxonomy($routeParams.taxonomy_id)
    //                 $scope.cancel();
    //                 // SweetAlert.swal({
    //                 //     title: "Success!",
    //                 //     text: data.message,
    //                 //     type: "success",
    //                 //     showCancelButton: false,
    //                 //     confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
    //                 //     closeOnConfirm: false,
    //                 // });
    //             }
    //             else {
    //                 SweetAlert.swal({
    //                     title: "Error!",
    //                     text: data.message,
    //                     type: "error",
    //                     showCancelButton: false,
    //                     confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
    //                     closeOnConfirm: false,
    //                 });
    //             }
    //         });
    //     }
    //     else {
    //         if (formdata.taxonomy_full_statement && formdata.taxonomy_full_statement.$error.required) {
    //             $scope.formError['taxonomy_full_statement'] = 'This field is required.';
    //         }

    //         if (formdata.taxonomy_human_coding_scheme && formdata.taxonomy_human_coding_scheme.$error.required) {
    //             $scope.formError['taxonomy_human_coding_scheme'] = 'This field is required.';
    //         }
    //     }
    // };

    // Add dummy data
    // $scope.dummyData = function(){
    //     var param_data = {};
    //     taxonomyService.addTaxonomy(param_data).then(function (data) {
    //         if (data.status == 201) {
    //             $scope.resetForm();
    //             $scope.getTaxonomy($routeParams.taxonomy_id)
    //             $scope.cancel();
    //             // SweetAlert.swal({
    //             //     title: "Success!",
    //             //     text: data.message,
    //             //     type: "success",
    //             //     showCancelButton: false,
    //             //     confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
    //             //     closeOnConfirm: false,
    //             // });
    //         }
    //         else {
    //             SweetAlert.swal({
    //                 title: "Error!",
    //                 text: data.message,
    //                 type: "error",
    //                 showCancelButton: false,
    //                 confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
    //                 closeOnConfirm: false,
    //             });
    //         }
    //     });
    // }


    //// ADD TAXONOMY AS CHILD ITEM 
    $scope.openTaxonomyAddForm = function (size, taxonomy_parent_id, entity_type) {
        $scope.cancel();
        $scope.resetForm();
        $scope.taxonomyAddForm = true;

        // $scope.modalFormText = "You are trying to add a Taxonomy item.";
        $scope.modalFormTile = "Add Taxonomy Item";

        $scope.taxonomy_parent_id = taxonomy_parent_id;
        $scope.root_node_id = $scope.taxonomy_id;

        // //Open Taxonomy add form
        $scope.showAdd = true;
        $scope.showEdit = false;

        // Add Taxonomy
        $scope.add = function (formdata) {
            $scope.formdata = formdata;
            $scope.formError = { 'taxonomy_full_statement': '', 'taxonomy_human_coding_scheme': '', 'error': '' };

            var param_data = {};
            if (!angular.isUndefined($scope.taxonomy_parent_id)) {
                param_data['parent_id'] = $scope.taxonomy_parent_id;
            }

            if (!angular.isUndefined($scope.taxonomy_full_statement)) {
                param_data['full_statement'] = "untitled";
            }

            if (!angular.isUndefined($scope.taxonomy_human_coding_scheme)) {
                param_data['human_coding_scheme'] = $scope.taxonomy_human_coding_scheme;
            }

            if (!angular.isUndefined($scope.taxonomy_list_enumeration)) {
                param_data['list_enumeration'] = $scope.taxonomy_list_enumeration;
            }

            if (!angular.isUndefined($scope.taxonomy_abbreviated_statement)) {
                param_data['abbreviated_statement'] = $scope.taxonomy_abbreviated_statement;
            }

            if (!angular.isUndefined($scope.taxonomy_education_level)) {
                param_data['education_level'] = $scope.taxonomy_education_level;
            }

            if (!angular.isUndefined($scope.taxonomy_note)) {
                param_data['note'] = $scope.taxonomy_note;
            }

            if (!angular.isUndefined($scope.taxonomy_concept_keywords)) {
                param_data['concept_keywords'] = $scope.taxonomy_concept_keywords;
            }

            if (!angular.isUndefined($scope.taxonomy_concept_uri)) {
                param_data['concept_uri'] = $scope.taxonomy_concept_uri;
            }

            if (!angular.isUndefined($scope.taxonomy_license_uri)) {
                param_data['license_uri'] = $scope.taxonomy_license_uri;
            }

            if (!angular.isUndefined($scope.taxonomy_language_id)) {
                param_data['language_id'] = $scope.taxonomy_language_id.id;
            }

            if (!angular.isUndefined($scope.taxonomy_item_type_id)) {
                param_data['item_type_id'] = $scope.taxonomy_item_type_id.id;
            }

            param_data['root_node_id'] = $routeParams.taxonomy_id;

            taxonomyService.addTaxonomy(param_data).then(function (data) {
                if (data.status == 201) {
                    $scope.resetForm();
                    $scope.getTaxonomy($routeParams.taxonomy_id, data.data.id, data.message);
                    // $scope.currentcfItemID=data.data.id;
                    $scope.cancel();
                    // SweetAlert.swal({
                    //     title: "Success!",
                    //     text: data.message,
                    //     type: "success",
                    //     showCancelButton: false,
                    //     confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
                    //     closeOnConfirm: false,
                    // });
                }
                else {
                    SweetAlert.swal({
                        title: "Error!",
                        text: data.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
                        closeOnConfirm: false,
                    });
                }
            });
            // else {
            //     if (formdata.taxonomy_full_statement && formdata.taxonomy_full_statement.$error.required) {
            //         $scope.formError['taxonomy_full_statement'] = 'This field is required.';
            //     }

            //     if (formdata.taxonomy_human_coding_scheme && formdata.taxonomy_human_coding_scheme.$error.required) {
            //         $scope.formError['taxonomy_human_coding_scheme'] = 'This field is required.';
            //     }
            // }
        };

    };

    // EDIT TAXONOMY AS CHILD ITEM & DOC
    $scope.openTaxonomyEditForm = function (size, cf_id, cf_entity_type, parent_id) {
        var taxonomyNodes = $('.taxonomy_node');
        $(taxonomyNodes).click(function () {
            $(taxonomyNodes).find('.cf_title').removeClass('active');
            $(this).parent().find('.cf_title').addClass('active');
        });
        $(".tab-contents").animate({ scrollTop: 0 }, 600);

        if (cf_entity_type == 'cf_doc') {
            $scope.cancel();
            $scope.taxonomyRootEditForm = true;
            $scope.modalFormText = "You are trying to Edit a Taxonomy.";
            $scope.modalFormTile = "Edit Taxonomy";

            $scope.taxonomy_id = cf_id;
            $scope.root_node_id = $scope.taxonomy_id;

            $scope.createcfdocform = {};

            // get master data
            cfService.getCFMasterData().then(function (res) {
                var masterdata = res.data;
                $scope.language = masterdata["language"];
                $scope.adoption_status = masterdata["adoption_status"];
                $scope.publisher = masterdata["publisher"];
                $scope.organization = masterdata["organization"];
                $scope.getdata();

            });

            var selectedKey = '';
            $scope.arrayIterateobj = function (lists, id) {
                angular.forEach(lists, function (value, key) {
                    if (value.id == id) {
                        selectedKey = key;
                    }
                });
                return selectedKey;
            }
            //get project details
            $scope.getdata = function () {
                cfService.getcfdocbyId($scope.taxonomy_id).then(function (res) {
                    $scope.taxonomy_data = res.data;
                    var cfdata = res.data;

                    $scope.createcfdocform.cfdoc_id = cfdata.id;
                    $scope.createcfdocform.cfdoc_title = cfdata.title;
                    $scope.createcfdocform.cfdoc_creator = cfdata.creator;

                    $scope.createcfdocform.cfdoc_official_source_url = cfdata.official_source_url;
                    $scope.createcfdocform.cfdoc_url_name = cfdata.url_name;
                    $scope.createcfdocform.cfdoc_version = cfdata.version;
                    $scope.createcfdocform.cfdoc_description = cfdata.description;
                    $scope.createcfdocform.cfdoc_note = cfdata.note;
                    $scope.createcfdocform.cfdoc_status_start_date = cfdata.status_start_date;
                    $scope.createcfdocform.cfdoc_status_end_date = cfdata.status_end_date;
                    //set organization
                    var selectedorg = $scope.arrayIterateobj($scope.organization, cfdata.organization_id)
                    $scope.createcfdocform.cfdoc_organization_id = $scope.organization[selectedorg];
                    //publisher
                    var selectedpub = $scope.arrayIterateobj($scope.publisher, cfdata.publisher_id)
                    $scope.createcfdocform.cfdoc_publisher_id = $scope.publisher[selectedpub];
                    //language
                    var selectedlang = $scope.arrayIterateobj($scope.language, cfdata.language_id)
                    $scope.createcfdocform.cfdoc_language_id = $scope.language[selectedlang];
                    //
                    var selecteadoptionstatus = $scope.arrayIterateobj($scope.adoption_status, cfdata.adoption_status)
                    $scope.createcfdocform.cfdoc_adoption_status = $scope.adoption_status[selecteadoptionstatus];


                });
            }

            // Update Taxonomy
            $scope.editTaxonomyDoc = function (formdata) {
                $scope.errorMessage = '';
                $scope.error = [];
                if (formdata.$valid == true) {
                    var temp = $scope.createcfdocform;
                    var param_data = {};

                    if (!angular.isUndefined($scope.createcfdocform.cfdoc_title)) {
                        param_data['title'] = $scope.createcfdocform['cfdoc_title'];
                    }
                    if ($scope.createcfdocform.hasOwnProperty('cfdoc_creator')) {
                        param_data['creator'] = $scope.createcfdocform['cfdoc_creator'];
                    }
                    if (!angular.isUndefined($scope.createcfdocform.cfdoc_organization_id) && $scope.createcfdocform.cfdoc_organization_id != null) {
                        param_data['organization_id'] = $scope.createcfdocform['cfdoc_organization_id']['id'];
                    }
                    if ($scope.createcfdocform.hasOwnProperty('cfdoc_official_source_url')) {
                        param_data['official_source_url'] = $scope.createcfdocform['cfdoc_official_source_url'];
                    }
                    if (!angular.isUndefined($scope.createcfdocform.cfdoc_publisher_id) && $scope.createcfdocform.cfdoc_publisher_id != null) {
                        param_data['publisher_id'] = $scope.createcfdocform['cfdoc_publisher_id']['id'];
                    }
                    if ($scope.createcfdocform.hasOwnProperty('cfdoc_url_name')) {
                        param_data['url_name'] = $scope.createcfdocform['cfdoc_url_name'];
                    }
                    if ($scope.createcfdocform.hasOwnProperty('cfdoc_version')) {
                        param_data['version'] = $scope.createcfdocform['cfdoc_version'];
                    }
                    if ($scope.createcfdocform.hasOwnProperty('cfdoc_description')) {
                        param_data['description'] = $scope.createcfdocform['cfdoc_description'];
                    }
                    if ($scope.createcfdocform.hasOwnProperty('cfdoc_note')) {
                        param_data['note'] = $scope.createcfdocform['cfdoc_note'];
                    }
                    if ($scope.createcfdocform.cfdoc_status_start_date) {
                        if ($scope.createcfdocform.cfdoc_status_start_date instanceof Date) {
                            param_data['status_start_date'] = $scope.createcfdocform['cfdoc_status_start_date'].toISOString().slice(0, 19).replace('T', ' ');
                        }
                        // else{
                        //     param_data['status_start_date'] = Date.parse( $scope.createcfdocform['cfdoc_status_start_date']).toISOString().slice(0, 19).replace('T', ' ');
                        // }

                    }

                    if ($scope.createcfdocform.cfdoc_status_end_date) {
                        if ($scope.createcfdocform.cfdoc_status_end_date instanceof Date) {
                            param_data['status_end_date'] = $scope.createcfdocform['cfdoc_status_end_date'].toISOString().slice(0, 19).replace('T', ' ');
                        }
                        // else{
                        //     param_data['cfdoc_status_end_date'] = Date.parse( $scope.createcfdocform['cfdoc_status_end_date']).toISOString().slice(0, 19).replace('T', ' ');
                        // }

                    }
                    if (!angular.isUndefined($scope.createcfdocform.cfdoc_language_id) && $scope.createcfdocform.cfdoc_language_id != null) {
                        param_data['language_id'] = $scope.createcfdocform['cfdoc_language_id']['id'];
                    }

                    if (!angular.isUndefined($scope.createcfdocform.cfdoc_adoption_status) && $scope.createcfdocform.cfdoc_adoption_status != null) {
                        param_data['adoption_status'] = $scope.createcfdocform['cfdoc_adoption_status']['id'];
                    }

                    param_data['taxonomy_id'] = $scope.taxonomy_id;
                    param_data['root_node_id'] = $scope.root_node_id;


                    cfService.savecfdoc(param_data).then(function (res) {
                        if (res.status == 200) {
                            $scope.getTaxonomy($routeParams.taxonomy_id);
                            $scope.cancel();
                            // SweetAlert.swal({
                            //     title: "Success!",
                            //     text: res.message,
                            //     type: "success",
                            //     showCancelButton: false,
                            //     confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
                            //     closeOnConfirm: false,
                            // });
                        }
                    });

                }
                else {
                    $scope.clickedOnce = false;
                    if (formdata.cfdoc_title && formdata.cfdoc_title.$error.required) {
                        $scope.error.cfdoc_title = 'This field is required.';
                    }
                    if (formdata.cfdoc_creator && formdata.cfdoc_creator.$error.required) {
                        $scope.error.cfdoc_creator = 'This field is required.';
                    }
                    if (formdata.cfdoc_official_source_url && formdata.cfdoc_official_source_url.$invalid) {
                        $scope.error.cfdoc_official_source_url = 'Invalid URL';
                    }
                }
            };
        }
        else {
            taxonomyService.getCfItemDetailsById($scope, cf_id).then(function (data) {
                $scope.cancel();
                $scope.resetForm();

                $scope.taxonomyEditForm = true;

                $scope.modalFormText = "You are trying to Edit a Taxonomy.";
                $scope.modalFormTile = "Edit Taxonomy";

                $scope.taxonomy_parent_id = parent_id;
                $scope.root_node_id = $routeParams.taxonomy_id;//$scope.taxonomy_id;    

                //Open Taxonomy add form
                $scope.showAdd = false;
                $scope.showEdit = true;

                cfService.getCFMasterData().then(function (res) {
                    var masterdata = res.data;
                    $scope.language = masterdata["language"];
                    $scope.item_type = masterdata["item_type"];
                    $scope.getdata();

                });

                $scope.getdata = function () {
                    $scope.taxonomy_id = data.data.id;
                    $scope.taxonomy_full_statement = data.data.full_statement;
                    $scope.taxonomy_human_coding_scheme = data.data.human_coding_scheme;
                    $scope.taxonomy_list_enumeration = data.data.list_enumeration;
                    $scope.taxonomy_abbreviated_statement = data.data.abbreviated_statement;
                    $scope.taxonomy_education_level = data.data.education_level;
                    $scope.taxonomy_note = data.data.note;
                    $scope.taxonomy_concept_keywords = data.data.concept_keyword;
                    $scope.taxonomy_concept_uri = data.data.concept_uri;
                    $scope.taxonomy_license_uri = data.data.license_uri;

                    var selectedlanguage = $scope.arrayIterateobj($scope.language, data.data.language_id);
                    $scope.taxonomy_language_id = $scope.language[selectedlanguage];

                    var selecteditemytype = $scope.arrayIterateobj($scope.item, data.data.item_type_id);
                    $scope.taxonomy_item_type_id = $scope.item_type[selecteditemytype];
                }

                // Update Taxonomy Child
                $scope.edit = function (formdata) {
                    $scope.formError = {
                        'taxonomy_full_statement': '', 'taxonomy_human_coding_scheme': '', 'taxonomy_concept_uri': '',
                        'taxonomy_license_uri': '', 'error': ''
                    };
                    if (formdata.$valid == true) {
                        var param_data = {};
                        if (!angular.isUndefined($scope.taxonomy_parent_id)) {
                            param_data['parent_id'] = $scope.taxonomy_parent_id;
                        }

                        if (!angular.isUndefined($scope.taxonomy_full_statement)) {
                            param_data['full_statement'] = $scope.taxonomy_full_statement;
                        }

                        if (!angular.isUndefined($scope.taxonomy_human_coding_scheme)) {
                            param_data['human_coding_scheme'] = $scope.taxonomy_human_coding_scheme;
                        }

                        if (!angular.isUndefined($scope.taxonomy_list_enumeration)) {
                            param_data['list_enumeration'] = $scope.taxonomy_list_enumeration;
                        }

                        if (!angular.isUndefined($scope.taxonomy_abbreviated_statement)) {
                            param_data['abbreviated_statement'] = $scope.taxonomy_abbreviated_statement;
                        }

                        if (!angular.isUndefined($scope.taxonomy_education_level)) {
                            param_data['education_level'] = $scope.taxonomy_education_level;
                        }

                        if (!angular.isUndefined($scope.taxonomy_note)) {
                            param_data['note'] = $scope.taxonomy_note;
                        }

                        if (!angular.isUndefined($scope.taxonomy_concept_keywords)) {
                            param_data['concept_keywords'] = $scope.taxonomy_concept_keywords;
                        }

                        if (!angular.isUndefined($scope.taxonomy_concept_uri)) {
                            param_data['concept_uri'] = $scope.taxonomy_concept_uri;
                        }

                        if (!angular.isUndefined($scope.taxonomy_license_uri)) {
                            param_data['license_uri'] = $scope.taxonomy_license_uri;
                        }

                        if (!angular.isUndefined($scope.taxonomy_language_id) && $scope.taxonomy_language_id != null) {
                            param_data['language_id'] = $scope.taxonomy_language_id.id;
                        }

                        if (!angular.isUndefined($scope.taxonomy_item_type_id) && $scope.taxonomy_item_type_id != null) {
                            param_data['item_type_id'] = $scope.taxonomy_item_type_id.id;
                        }
                        param_data['root_node_id'] = $routeParams.taxonomy_id;// $scope.root_node_id; 


                        taxonomyService.editTaxonomy(param_data, cf_id).then(function (data) {
                            if (data.status == 200) {
                                $scope.resetForm();
                                $scope.getTaxonomy($routeParams.taxonomy_id, data.data.id, data.message);
                                $scope.cancel();
                                // SweetAlert.swal({
                                //     title: "Success!",
                                //     text: data.message,
                                //     type: "success",
                                //     showCancelButton: false,
                                //     confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
                                //     closeOnConfirm: false,
                                // });
                            }
                        });
                    }
                    else {
                        if (formdata.taxonomy_full_statement && formdata.taxonomy_full_statement.$error.required) {
                            $scope.formError['taxonomy_full_statement'] = 'This field is required.';
                        }

                        if (formdata.taxonomy_human_coding_scheme && formdata.taxonomy_human_coding_scheme.$error.required) {
                            $scope.formError['taxonomy_human_coding_scheme'] = 'This field is required.';
                        }

                        if (formdata.taxonomy_concept_uri && formdata.taxonomy_concept_uri.$invalid) {
                            $scope.formError['taxonomy_concept_uri'] = 'Invalid URL.';
                        }

                        if (formdata.taxonomy_license_uri && formdata.taxonomy_license_uri.$invalid) {
                            $scope.formError['taxonomy_license_uri'] = 'Invalid URL.';
                        }
                    }
                };
            });
        }
    };

    //Delete Taxonomy and their children
    $scope.deleteTaxonomy = function (taxonomy_id) {
        //console.log(taxonomy_id);

        SweetAlert.swal({
            title: "Are you sure?",
            text: "You will not be able to recover this taxonomy!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                taxonomyService.delTaxonomy($scope, taxonomy_id).then(function (data) {                   
                    if (data.status == 200) {
                        $scope.getTaxonomy($routeParams.taxonomy_id);
                         SweetAlert.swal("Success", "Taxonomy has been deleted", "success");
                        // $window.location.href = '#/taxonomies';
                    } else {
                        SweetAlert.swal("Cancelled", data.message, "warning");
                    }
                });
            } else {
                SweetAlert.swal("Cancelled", "Taxonomy is safe :)", "error");
            }
        });
    }


    //Used to minimize and maximize to Taxonomy
    $scope.toggleMinimized = function (child) {
        child.minimized = !child.minimized;
    };
    //

    /// REARRANGE TAXONOMY CHILD
    $scope.update = function (event, ui) {
        var root = event.target,
            item = ui.item,
            parent = item.parent(),
            target = (parent[0] === root) ? $scope.taxonomy : parent.scope().child,
            child = item.scope().child,
            index = item.index();

        target.children || (target.children = []);

        function walk(target, child) {
            var children = target.children, i;
            if (children) {
                i = children.length;
                while (i--) {
                    if (children[i] === child) {
                        return children.splice(i, 1);
                    } else {
                        walk(children[i], child);
                    }
                }
            }
        }
        walk($scope.taxonomy, child);


        target.children.splice(index, 0, child);
        //console.log(target.children.length);

        var arrayOfOrder = [];
        var counter = 0;
        $.each(target.children, function (i, children) {
            arrayOfOrder.push({ id: children['id'], order: children['order_id'] });
        });

        var target_id = 0;
        if (angular.isDefined(target.id)) {
            target_id = target.id;
        }

        //Saving TOC
        taxonomyService.rearrangeTaxonomyTree(arrayOfOrder, child.id, target_id).then(function (data) {


        });
    };

    $scope.openTaxonmonyUsageModal = function (size, taxonomy_id) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/taxonomy/usageModalContent.html',
            controller: 'ModalInstanceTaxonomyMetadataCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormText: function () {
                    return "";
                },
                modalFormTile: function () {
                    return "Metadata Taxonomy Usage";
                },
                taxonomy_id: function () {
                    return taxonomy_id;
                },
                user_permissions: function () {
                    return $scope.user_permissions;
                }
            }
        });
        modalInstance.result.then(function () {

        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

    // $scope.changeStatus = function (id) {
    //     taxonomyService.changeStatus(id, $scope).then(function (data) {
    //         if (data.status == 200) {
    //             $scope.getTaxonomy();
    //         } else {
    //             angular.element("#taxonomy-" + id).attr("checked", false);
    //             $scope.getTaxonomy();
    //             /*if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
    //              SweetAlert.swal("Cancelled", data.message, "warning");
    //              }*/
    //         }
    //     });
    // };

    /*
     * Export taxonomy tree as JSON
     */
    $scope.exportJson = function () {
        var params = {};
        params.access_token = sessionService.get('access_token');
        //var deferred = $q.defer();
        window.location.href = serviceEndPoint + 'api/v1/exportCFPackageJson/' + $routeParams.taxonomy_id;
    };

    //get cfitem details
    $scope.resetForm = function () {
        $scope.taxonomy_full_statement = 'Untitled';
        $scope.taxonomy_human_coding_scheme = 'Untitled';
        $scope.taxonomy_list_enumeration = '';
        $scope.taxonomy_abbreviated_statement = '';
        $scope.taxonomy_education_level = '';
        $scope.taxonomy_note = '';
        $scope.taxonomy_concept_keywords = '';
        $scope.taxonomy_concept_uri = '';
        $scope.taxonomy_license_uri = '';

        $scope.taxonomy_language_id = '';
        $scope.taxonomy_item_type_id = '';
    }

    // CANCEL ADD EDIT
    $scope.cancel = function () {

        $scope.taxonomyAddForm = false;
        $scope.taxonomyEditForm = false;
        $scope.taxonomyRootEditForm = false;
    };

    // Taxonomy list
    $scope.showTaxonomyList = function () {
        taxonomyService.listTaxonomies().then(function (result) {
            $scope.tax_data = result.data;
            angular.forEach($scope.tax_data,function(x){
                    console.log(x.id);                
                })
        });
    }
    $scope.showTaxonomyList();

    $scope.redirect_taxonomy= function(){
				$location.path('taxonomyimport/');					
    }
    $scope.onSelection = function(type){
        $scope.selectionType = type;
    }
    $scope.taxonomy_setup = function(){
        if($scope.selectionType==='import'){
            $location.path('project-import/');
        }else if($scope.selectionType==='build'){
            $location.path("taxonomysetup/");
        }
    }
    $scope.createTax = function(formdata){
         cfService.createcfdoc().then(function(res){
                        if(res.status==200){
                            $rootScope.show_projectsetup_success_msg = 1;
                            var redirpage = "/manage-taxonomy/"+res.data.id;
                            //$location.path(redirpage);
                            swal("CF doc has been updated successfully");
                        }
                    });
    }

    $scope.setUpTaxonomies = function(){
        alert("hehkleh");
    }
    
    $scope.showTaxonomyDetails = function(id, name) {
        
            $location.path('manage-taxonomy/'+id);

    }
    $scope.$on('apply-advaced-search', function (event, args) {
       
    });

    $scope.$on('clear-advaced-search', function (event, args) {
      
    });

    $scope.$on('apply-search', function (event, args) {
        taxonomyService.searchTaxonomies(args).then(function (result) {
            $scope.tax_data = result.data.taxonomies;
            $scope.tax_count = result.data.count;
            console.log($scope.tax_count);
            angular.forEach($scope.tax_data,function(x){
                    console.log(x.id);
                
                }) 
        });
    });

    $scope.CaseAssociationTypeList = function(){
        taxonomyService.caseAssociationListTypes().then(function (result) {
            $scope.listAssociation = result.data;
            angular.forEach($scope.listAssociation,function(as){
                console.log(as.display_name);
            
            })
        }); 
    }
    $scope.CaseAssociationTypeList();

}]);


// /*
//  * Create Taxonomy Modal window Controller
//  */
// app.controller('ModalInstanceTaxonomyCtrl', function ($scope, assetService, taxonomyService, cfService, SweetAlert, $modalInstance, modalFormText, modalFormTile, taxonomy, taxonomy_parent_id, root_node_id) {

//     $scope.modalFormText = modalFormText;
//     $scope.modalFormTile = modalFormTile;
//     $scope.taxonomyModal = taxonomy;

//     $scope.taxonomy_parent_id = taxonomy_parent_id;
//     $scope.root_node_id = root_node_id;

//     console.log($scope.root_node_id);

//     $scope.language = '';
//     $scope.item_type = '';

//     cfService.getCFMasterData().then(function (res) {
//         var masterdata = res.data;
//         $scope.language = masterdata["language"];
//         $scope.item_type = masterdata["item_type"];
//         $scope.getdata();

//     });

//     var selectedKey = '';
//     $scope.arrayIterateobj = function (lists, id) {
//         angular.forEach(lists, function (value, key) {
//             if (value.id == id) {
//                 selectedKey = key;
//             }
//         });
//         return selectedKey;
//     }

//     //get cfitem details
//     $scope.getdata = function () {
//         $scope.taxonomy_full_statement = taxonomy.full_statement;
//         $scope.taxonomy_human_coding_scheme = taxonomy.human_coding_scheme;
//         $scope.taxonomy_list_enumeration = taxonomy.list_enumeration;
//         $scope.taxonomy_abbreviated_statement = taxonomy.abbreviated_statement;
//         $scope.taxonomy_education_level = taxonomy.education_level;
//         $scope.taxonomy_note = taxonomy.note;
//         $scope.taxonomy_concept_keywords = taxonomy.concept_keyword;
//         $scope.taxonomy_concept_uri = taxonomy.concept_uri;
//         $scope.taxonomy_license_uri = taxonomy.license_uri;

//         var selectedlanguage = $scope.arrayIterateobj($scope.language, taxonomy.language_id);
//         $scope.taxonomy_language_id = $scope.language[selectedlanguage];

//         var selecteditemytype = $scope.arrayIterateobj($scope.item, taxonomy.item_type_id);
//         $scope.taxonomy_item_type_id = $scope.item_type[selecteditemytype];
//     }

//     //Used to minimize and maximize to Taxonomy
//     $scope.toggleMinimizedModal = function (childModal) {
//         childModal.minimized = !childModal.minimized;
//     };


//     //Open Taxonomy add form
//     $scope.showAdd = true;
//     $scope.showEdit = false;
//     if (angular.isDefined(taxonomy.id) && taxonomy.id != '') {
//         $scope.taxonomy_parent_id = taxonomy.parent_id;
//         $scope.showAdd = false;
//         $scope.showEdit = true;
//     }

//     $scope.redirectPage = function () {
//         $location.path($scope.redirpage);
//     }


// });



/*
 *  Taxonomy Metadata Modal window Controller
 */
app.controller('ModalInstanceTaxonomyMetadataCtrl', function ($scope, $rootScope, $location, $modal, $log, taxonomyService, objectService, SweetAlert, $modalInstance, assetService, modalFormText, modalFormTile, taxonomy_id, user_permissions) {
    $scope.total = '';
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.user_permissions = user_permissions;
    $scope.taxonomyModal = {};
    $scope.taxonomyMetadataObjects = {};
    $scope.currentPage = 1;
    $scope.pageNumber = '';
    $scope.itemsPerPage = '';
    //Open taxonomy metadata usage
    $scope.taxonomy_title = '';
    $scope.taxonomy_description = '';
    $scope.showPagination = false;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.getObjects = function (pageNumber) {
        $scope.currentPage = pageNumber;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = '10';
        taxonomyService.getMetadataUsage($scope, taxonomy_id).then(function (data) {
            if (data.data.taxonomy == null || data.data.taxonomy == '') {
                $scope.total = 0;
                $scope.taxonomyModal = {};
                $scope.taxonomyMetadataObjects = {};
            } else {
                var taxonomy = data.data.taxonomy;
                if (angular.isDefined(taxonomy.id) && taxonomy.id != '') {
                    $scope.taxonomy_title = taxonomy.title;
                    $scope.taxonomy_description = taxonomy.description;
                }
                $scope.total = data.data.total;
                if (data.data.total > data.data.totalContents) {
                    $scope.showPagination = true;
                }
                $scope.taxonomyMetadataObjects = data.data.metadata_objects;
            }

        });
    };

    $scope.getObjects(1);

    $scope.pageChangeHandlerObject = function (pageNumber) {
        $scope.getObjects(pageNumber);
    };
    $scope.redirctToProject = function (object_id, toc_creation_status, usage_type) {
        switch (usage_type) {
            case 'ASSESSMENT':
                $scope.checkProjectExists(object_id);
                break;
            case 'OBJECT':
                if (parseInt(toc_creation_status) == 3001) {
                    $scope.checkProjectExists(object_id);
                } else {
                    $rootScope.show_toc = 0;
                    $location.path('project_import/' + object_id);
                }
                break;
            default:
        }
    };
    $scope.checkProjectExists = function (object_id) {
        $scope.project_id = object_id;
        objectService.getProjectDetails($scope).then(function (data) {
            if (data != null) {
                $rootScope.show_toc = 1;
                $location.path("edit_toc/" + object_id);
            } else {
                $rootScope.show_toc = 0;
                SweetAlert.swal({ html: 'Either the project does not exist or you are no longer allocated to this project.', width: 540, type: "warning" });
            }
        });
    };
    $scope.preview_button_disabled = false;
    $scope.previewAssessment = function (preview_info) {
        $scope.preview_button_disabled = true;
        var size = 'lg';
        var modal_form_title = 'Assessment Preview';
        var previewInfo = JSON.parse(preview_info);
        var modalInstance = $modal.open({
            templateUrl: 'templates/assessment/previewModal.html',
            controller: 'ModalPreviewCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return modal_form_title;
                },
                shortcode: function () {
                    return previewInfo.shortcode;
                },
                project_id: function () {
                    return previewInfo.project_id;
                },
                project_type_id: function () {
                    return previewInfo.project_type_id;
                },
                entity_type_id: function () {
                    return 2;
                }
            }
        });
        modalInstance.result.then(function (modalCloseParameter) {
            $scope.preview_button_disabled = false;
        }, function () {
            $scope.preview_button_disabled = false;
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.previewAsset = function (size, preview_info) {
        $scope.applyMetadata = 0;
        var preview_infos = JSON.parse(preview_info);
        var modalInstance = $modal.open({
            templateUrl: 'templates/asset/assetModalContent.html',
            controller: 'ModalAssetCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormTitle: function () {
                    return preview_infos.modal_form_title;
                },
                object_id: function () {
                    return preview_infos.object_id;
                },
                project_id: function () {
                    return preview_infos.project_id;
                },
                displayOption: function () {
                    return false;
                },
                user_permissions: function () {
                    return $scope.user_permissions;
                },
                project_permissions: function () {
                    return true;
                }
            }
        });
    };
    $scope.previewContent = function (size, id) {
        var contentRec = {};
        $scope.id = id;
        assetService.getGlobalContentDetails($scope).then(function (data) {
            contentRec = data.data;
            var modalInstance = $modal.open({
                templateUrl: 'templates/toc/globalModalContentView.html',
                controller: 'ModalInstanceContentCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    contentRec: function () {
                        return contentRec;
                    },
                    object_id: function () {
                        return id;
                    },
                    modalFormText: function () {
                        return "You are trying to view a content";
                    },
                    modalFormTile: function () {
                        return "Preview Content";
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        });
    };
});