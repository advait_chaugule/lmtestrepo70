'use strict';
// image popup controller
app.controller('linkcheckcontroller', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'linkcheckService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, linkcheckService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {
    $scope.currentOrderByField = 'name';
    $scope.reverseSort = false;
    $scope.access_token = sessionService.get('access_token');
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    var userinfo = JSON.parse(sessionService.get('userinfo'));
    $scope.project_id = $routeParams.project_id;
    $scope.project_type_id = 4;
    $scope.set_type = $routeParams.set_type;
    if (!$scope.access_token || userinfo == null) {
      $location.path('logout');
      return true;
    }

    $scope.project_name = '';
    $scope.getProject = function () {
      if ($scope.project_id != '') {
        objectService.getProjectDetails($scope).then(function (data) {
          if (data == null) {
            $location.path("edit_toc/" + $scope.project_id);
          } else if (angular.isObject(data)) {
            $scope.project_name = data.name;
            $scope.project = data;
            $scope.applyPermission = 1;
            $scope.project.project_permissions = data.project_permissions;
          }
        });
      }
    };
    
    $scope.getProject();

    //// listing
    $scope.pageNumber = 1;
    $scope.itemsPerPage = 10;
    $scope.currentPage = 1;

    $scope.linkChecker = function () {
            linkcheckService.projectValidation($scope).then(function (data) {
                $scope.link_validation_status = data.link_validation_status;
                $scope.showTable = false;
            });
    };
    $scope.listBrokenLinks = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            linkcheckService.projectBrokenLinks($scope).then(function (data) {
                $scope.link_validation_status = data.link_validation_status;
            if(data.link_validation_status == 3) {
                $scope.linkList = data.data;
                $scope.totalLinks = data.totalLinks;
                $scope.showTable = data.status; 
            } else{
                $scope.showTable = false; 
            }
         });
     };
        
    $scope.listBrokenLinks(1);
    $scope.pageChangeHandlerLink = function (newPage) {
            $scope.listBrokenLinks(newPage);
        };
    ////// listing

    // add class
    /*$scope.linkChecker = function () {
        $scope.listLinks(1);
    };*/
    $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.listBrokenLinks(1);
        };


  }]);