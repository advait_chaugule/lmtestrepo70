'use strict';
app.service('linkcheckService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        // Service to find all links and urls in project
        this.projectValidation = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }
            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }
           /* $http.get(serviceEndPoint + 'api/v1/linkcheck/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });*/
            $http.post(serviceEndPoint + 'api/v1/linkcheck/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        this.projectBrokenLinks = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }
            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }
            $http.get(serviceEndPoint + 'api/v1/brokenlinks/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };


    }]);

