app.filter('htmlDecode', function () {
    return function (input) {
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    };
});
app.filter('innerHtml', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
});

app.filter('trustedUrl', function ($sce) {
    return function (val) {
        return $sce.trustAsResourceUrl(val);
    };
});

app.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});

app.filter('capitalize_first', function () {
    return function (input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});


app.filter('bytes', function () {
    return function (bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes))
            return '-';
        if (typeof precision === 'undefined')
            precision = 1;
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }
});


app.filter('getById', function () {
    return function (input, id) {
        var i = 0, len = input.length;
        for (; i < len; i++) {
            if (+input[i].id == +id) {
                return input[i];
            }
        }
        return null;
    }
});

app.filter('firstWord', function () {
    return function (input) {
        //console.log(delimiter);
        var input = input.split('.');
        //console.log(input[0]);
        return input[0];
    }
});
/*
 * String Substr and add ...
 */
app.filter('strLimit', ['$filter', function ($filter) {
        return function (input, limit) {
            if (!input)
                return;
            if (input.length <= limit) {
                return input;
            }
            return $filter('limitTo')(input, limit) + '...';
        };
    }]);

/**
 *  Format date
 */
app.filter('customDateFormat', function ($filter) {
    return function (input, format) {
        if (!input)
            return '';
        if (format) {
            var _date = $filter('date')(new Date(input), format);
        } else {
            var _date = $filter('date')(new Date(input), 'yyyy-MM-dd HH:mm:ss');
        }
        return _date.toUpperCase();
    }
});

app.filter('highlight_txt', function ($sce) {
    return function (text, phrase) {
        //console.info(phrase);
        if (phrase)
            text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
                    '<span style="background-color: yellow" class="highlightTextSearch">$1</span>')
        //console.log(text);
        return text;
        //return $sce.trustAsHtml(text)
    }
});

app.filter('underscoreless', function () {
  return function (input) {
      return input.replace(/_/g, ' ');
  };
});