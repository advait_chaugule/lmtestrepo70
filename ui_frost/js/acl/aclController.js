'use strict';

app.controller('allocate_acl', ['$scope', '$location', 'sessionService', 'aclService', 'cfpLoadingBar', '$modal', '$log', '$timeout', 'SweetAlert', '$routeParams', 'userService', 'loginService', '$rootScope', function ($scope, $location, sessionService, aclService, cfpLoadingBar, $modal, $log, $timeout, SweetAlert, $routeParams, userService, loginService, $rootScope) {
    $scope.access_token = sessionService.get('access_token');
    var userinfo = JSON.parse(sessionService.get('userinfo'));
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    // Check if the session is active	
    // $rootScope.checkActiveSession();
    if (!$scope.access_token || userinfo == null) {
        $location.path('logout');
        return true;
    }
    console.log("userINfo   ", userinfo);
    // console.log($scope.user_permissions);`

    // if (userinfo.id !== 1) {
    //     $rootScope.goto('dashboard');
    // }
    if (userinfo.roles[0].status !== 1) {
        $rootScope.goto('dashboard');
    }

    $scope.selectedRole = userinfo.roles[0].role_id; // $routeParams.role_id;
    $scope.selectedUser = userinfo.user_id; // $routeParams.user_id;

    $scope.checkedPermission = [];
    $scope.user_role_permission = {};

    //Get Roles
    $scope.getProjectRole = function () {
        aclService.projectRoles().then(function (data) {

            $scope.roles = data.data;
            $scope.showPermission = 0;
            $scope.showRole = 1;
            var role = $scope.roleIterate($scope.roles, $scope.selectedRole);
            $scope.selectedRoleKey = role;
        });
    }

    $scope.getProjectRole();
    $scope.getRole = function () {
        aclService.roles($scope).then(function (data) {
            $scope.roles = data.data;
            $scope.showPermission = 0;
            $scope.showRole = 1;
            var role = $scope.roleIterate($scope.roles, $scope.selectedRole);
            $scope.selectedRoleKey = role;
           // $scope.selectedRoleKey = $scope.roles[selectedKey];

        });
    }
    $scope.getRole();

    $scope.setRole = function (role) {
        $scope.selectedRole = role;
    };
    $scope.acl_roles = {};


    $scope.managePermission = function () {
        $scope.showPermission = 1;
        $scope.showRole = 0;
        $scope.showMsg = 0;
        var isCheckedSelectedAll = 0;
        aclService.rolePermission($scope).then(function (data) {
            $scope.user_role_permission =  data.data ;
            angular.forEach($scope.user_role_permission, function(item){
                item.grant = false;
            });
            /*aclService.generateRolePermissionArray($scope, data.data).then(function (user_role_permission) {
                $scope.user_role_permission = user_role_permission;

                angular.forEach($scope.user_role_permission, function (item) {
                    angular.forEach(item, function (val) {
                        if (val.grant == false) {
                            isCheckedSelectedAll++;
                        }
                    });
                });
                if (isCheckedSelectedAll > 0) {
                    $scope.selectedAll = false;
                } else {
                    $scope.selectedAll = true;
                }
            });*/
        });
    };

    //get permissions for role
    if ($scope.selectedRole) {
        $scope.managePermission();
    }
    //get permissions for user
    if ($scope.selectedUser) {
        $scope.managePermission();
        $scope.user_id = $scope.selectedUser;
        userService.getUserDetail($scope).then(function (data) {
            $scope.userDetails = data.data;
        });
    }

    $scope.assignPermission = function (role_id) {
        $scope.setRole(role_id);
        $location.path('allocate_acl/' + role_id);
        //$timeout($scope.managePermission, 3000, false);
    };

    $scope.cancelPermission = function () {
        $location.path('settings');
    };
    $scope.cancelPermissionUser = function () {
        $location.path('settings');
    };

    //Save Permissions for Specific Role
    $scope.savePermission = function () {
        $scope.showMsg = 1;
        if ($scope.selectedRole) {
            //aclService.managePermission($scope);
            //aclService.manageProjectRolePermission($scope).then(function(data) {
            aclService.manageRolePermission($scope).then(function (data) {
                //System Permission Disallow
                //Do logout from the system
                if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                    loginService.logout();
                }
                if (data.status == 200) {
                    $scope.getRole();
                    SweetAlert.swal("Success", data.message, "success");
                }

            });
        } else {
            $scope.status = 400;
            $scope.message = 'Please select a Role';
        }
    };

    //Save Permissions for Specific User
    $scope.savePermissionUser = function () {
        $scope.showMsg = 1;
        if ($scope.selectedUser) {
            //console.log($scope.user_role_permission);
            aclService.managePermissionUser($scope);
        } else {
            $scope.status = 400;
            $scope.message = 'Please select a User';
        }
    }


    $scope.addRoleForm = function (size) {
        // debugger;
        //$scope.child = child;
        var modalInstance = $modal.open({
            templateUrl: 'templates/acl/roleModalContent.html',
            controller: 'ModalRoleAddCtrl',
            size: size,
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormText: function () {
                    return "You are trying to add a new Role";
                },
                modalFormTile: function () {
                    return "Add Role";
                },
                role_id: function () {
                    return '';
                },
                hasIssue: function () {
                    return 0;
                }
            }
        });
        modalInstance.result.then(function () {
            //aclService.projectRoles().then(function(data) {
            aclService.roles($scope).then(function (data) {
                $scope.roles = data.data;
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.editRoleForm = function (size, role_id) {

        //$scope.child = child;
        var modalInstance = $modal.open({
            templateUrl: 'templates/acl/roleModalContent.html',
            controller: 'ModalRoleAddCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                modalFormText: function () {
                    return "You are trying to edit a Role";
                },
                modalFormTile: function () {
                    return "Edit Role";
                },
                role_id: function () {
                    return role_id;
                },
                hasIssue: function () {
                    return 0;
                }
            }
        });
        modalInstance.result.then(function () {
            //Saving TOC    
            //aclService.projectRoles().then(function(data) {
            aclService.roles($scope).then(function (data) {
                //console.info(data.data);
                $scope.roles = data.data;
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.deleteRole = function (id) {
       
                SweetAlert.swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this role!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        //aclService.delProjectRole(id, $scope).then(function(data) {   
                        aclService.delRole(id, $scope).then(function (data) {
                            //System Permission Disallow
                            //Do logout from the system
                            if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                                SweetAlert.swal({
                                    title: "Cancelled",
                                    text: '',
                                    type: "error",
                                }, function (isConfirm) {
                                    loginService.logout();
                                });

                            } else {
                                if (data.status == '200') {
                                    SweetAlert.swal("Success", data.message, "success");
                                    //aclService.projectRoles().then(function(data) {
                                    aclService.roles($scope).then(function (data) {
                                        $scope.roles = data.data;
                                    });
                                } else {
                                    SweetAlert.swal("Error", data.message, "error");

                                }

                            }

                        });

                    } else {
                        SweetAlert.swal("Cancelled", "Role is safe :)", "error");
                    }
                });
            
           
      
// -----
        /*aclService.delRole(id, $scope).then(function (data) {
            console.log(' data ',data);
            if (data.status == '200') {
                SweetAlert.swal("Success", data.message, "success");
                //aclService.projectRoles().then(function(data) {
                aclService.roles($scope).then(function (data) {
                    $scope.roles = data.data;
                });
            } else {
                SweetAlert.swal("Error", data.message, "error");

            }
        });*/
    };
    //Check / uncheck All
    $scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = false;
        } else {
            $scope.selectedAll = true;
        }
        // angular.forEach($scope.user_role_permission, function (item) {
        //     angular.forEach(item, function (val) {
        //         val.grant = $scope.selectedAll;
        //     });

        // });
    };

    $scope.countCheck = function (event) {
        if (event.target.checked == false) {
            angular.element('#checkbox_for_all').removeAttr('checked');
            $scope.selectedAll = false;
        } else {
            var isCheckedSelectedAll = 0;
            angular.forEach($scope.user_role_permission, function (item) {
               
                if (item.grant == false) {
                    isCheckedSelectedAll++;
                }
                /*angular.forEach(item, function (val) {
                    if (val.grant == false) {
                        isCheckedSelectedAll++;
                    }
                });*/
            });
            if (isCheckedSelectedAll > 1) {
                $scope.selectedAll = false;
            } else {
                $scope.selectedAll = true;
            }
        }

    }

    //Array index find for selected type
    var selectedKey = '';
    $scope.roleIterate = function (roles, role_id) {
     var role = null;
        roles.forEach(element => {
            if (element.role_id == role_id) {
            role = element;
            }
           
        });
       
        return role;
    }

}]);


app.controller('ModalRoleAddCtrl', function ($scope, aclService, $modalInstance, modalFormText, modalFormTile, role_id, $timeout, hasIssue, loginService) {
    $scope.showedit = 0;
    $scope.hasIssue = hasIssue;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.formError = {
        'role_name': '',
        'role_desc': ''
    };
    if (role_id) {
        $scope.showedit = 1;
        $scope.selectedRole = role_id;
        //aclService.getProjectRoleById(role_id, $scope).then(function(data) {
        aclService.getRoleById(role_id, $scope).then(function (data) {
            //System Permission Disallow
            //Do logout from the system
            if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                $modalInstance.close();
                loginService.logout();
            } else {
                $scope.role_name = data.data.name;
                $scope.role_desc = data.data.description;
                $scope.role_id = role_id;
            }

        });
    }

    $scope.role_keypress = function () {
        $('#saveBtn').attr("disabled", false);
        $('#editBtn').attr("disabled", false);
    }

    $scope.add = function () {
        $scope.hasIssue = 0;
        $('#saveBtn').attr("disabled", "disabled");
        $scope.formError = {
            'role_name': '',
            'role_desc': ''
        };
        //aclService.saveProjectRole($scope).then(function(response) {
        aclService.saveRole($scope).then(function (response) {
            // console.log(response);
            // $modalInstance.close();

            //System Permission Disallow
            //Do logout from the system
            if (angular.isDefined(response.systemPermissionGranted) && response.systemPermissionGranted == '401') {
                $modalInstance.close();
                loginService.logout();
            }

            if (response.status == '201') {
                //aclService.projectRoles().then(function(data) {
                aclService.roles($scope).then(function (data) {
                    $scope.roles = data.data;
                });
                $modalInstance.close();
                swal("Role added successfully");
                
            } else if (response.status == '400') {
                if(angular.isUndefined($scope.role_name) == true){
                     $scope.formError['role_name'] = 'Please fill out this field properly';
                     }            
                     if(angular.isUndefined($scope.role_desc) == true){
                     $scope.formError['role_desc'] = 'Please fill out this field properly';
                     }
                }
            


        });

        // /*}else{            
        //  if(angular.isUndefined($scope.role_name) == true){
        //  $scope.formError['role_name'] = 'Please fill out this field properly';
        //  }            
        //  if(angular.isUndefined($scope.role_desc) == true){
        //  $scope.formError['role_desc'] = 'Please fill out this field properly';
        //  }
         
        //  }*/
    };

    $scope.edit = function () {
        // $scope.submitted == true;
        $('#editBtn').attr("disabled", "disabled");
        $scope.formError = {
            'role_name': '',
            'role_desc': ''
        };
        //if (angular.isUndefined($scope.role_name) == false && angular.isUndefined($scope.role_desc) == false) {
        //aclService.editProjectRole($scope).then(function(response) {
        aclService.editRole($scope).then(function (response) {
            console.log("edit working");
            /*aclService.projectRoles().then(function (data) {
             $scope.roles = data.data;
             });
             $modalInstance.close();*/


            //System Permission Disallow
            //Do logout from the system
            if (angular.isDefined(response.systemPermissionGranted) && response.systemPermissionGranted == '401') {
                $modalInstance.close();
                loginService.logout();
            }


            if (response.status == '200') {
                //aclService.projectRoles().then(function(data) {
                aclService.roles($scope).then(function (data) {
                    $scope.roles = data.data;
                });
                $modalInstance.close();
                swal("Role edited successfully");
            } else if (response.status == '400') {
                if(angular.isUndefined($scope.role_name) == true){
                    $scope.formError['role_name'] = 'Please fill out this field properly';
                    }            
                    if(angular.isUndefined($scope.role_desc) == true){
                    $scope.formError['role_desc'] = 'Please fill out this field properly';
                    }
            }

        });


        /*}else{
         if(angular.isUndefined($scope.role_name) == true){
         $scope.formError['role_name'] = 'Please fill out this field properly';
         }            
         if(angular.isUndefined($scope.role_desc) == true){
         $scope.formError['role_desc'] = 'Please fill out this field properly';
         }
         }*/
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});