/*****
 * Services for ACL
 * Author - Koushik Samanta 
 *  
 */
'use strict';

//Start of aclService
app.service('aclService', ['$http', '$q', 'sessionService', '$rootScope', '$filter', function ($http, $q, sessionService, $rootScope, $filter) {

        this.roles = function ($scope) {
            var params = {};
            params.Authorization = sessionService.get('access_token');
            // params.exclude_role_ids = angular.isDefined($scope.exclude_role_ids) ? JSON.stringify($scope.exclude_role_ids) : '[]';
            var deferred = $q.defer();

            $http({
                method: 'GET',
                headers:params,
                url: serviceEndPoint + 'api/v1/role/list',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        this.projectRoles = function () {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/role/get/',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        //Save Permissions for Specific Role
        this.managePermission = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/acl-permission-role?access_token=' + sessionService.get('access_token'), {
                role_id: $scope.selectedRole,
                permissions: JSON.stringify($scope.user_role_permission)
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        //Save Permissions for Specific Role
        this.manageRolePermission = function ($scope) {
            var deferred = $q.defer();
           
            var role_permissions = [];
            angular.forEach($scope.user_role_permission, function (item) {
                //console.info(item);
               /* angular.forEach(item, function (value) {
                    var exclude_metadata_array = [6000, 6001, 6002, 6003, 6004, 6005];
                    if (jQuery.inArray(value.value, exclude_metadata_array) === -1) {
                        role_permissions.push({
                            description: value.description,
                            grant: value.grant,
                            value: value.value
                        });
                    } else {
                        role_permissions = this.prepareMatadataPermissionBeforeSave($scope, value, role_permissions);
                    }
                }, this);*/
                if(item.grant){
                    role_permissions.push({
                        permission_id: item.permission_id,
                        grant: item.grant
                    });
                }
            }, this);

            //this.prepareMatadataPermissio
            //nBeforeSave($scope);   


            //console.log(role_permissions);
            var params = {};
            params.Authorization = sessionService.get('access_token');
            params.Accept = 'application/json';
            $http({                
                    method: 'POST',
                    url: serviceEndPoint + 'api/v1/permission/assign/' + $scope.selectedRole,
                    headers: params,
                    data:{'permission':JSON.stringify(role_permissions)}                                
            })
            .success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        this.prepareMatadataPermissionBeforeSave = function ($scope, permissions, role_permissions) {
            //var deferred = $q.defer();
            var metaDataPermission = {"6000": [600, 620, 630], "6001": [601, 621, 631], "6002": [602, 622, 632], "6003": [603, 623, 633], "6004": [604, 624, 634], "6005": [605, 625, 635]};
            angular.forEach(metaDataPermission[permissions.value], function (v, k) {
                //console.log(' v=>'+v+' k=> '+k);    
                role_permissions.push({
                    description: '',
                    grant: permissions.grant,
                    value: v
                });
            });
            //deferred.resolve(role_permissions);            
            //return deferred.promise;
            return role_permissions;
        };

        //Save Permissions for Specific Role
        this.manageProjectRolePermission = function ($scope) {
            var deferred = $q.defer();

            var role_permissions = [];
            angular.forEach($scope.user_role_permission, function (item) {
                angular.forEach(item, function (value) {
                    role_permissions.push({
                        description: value.description,
                        grant: value.grant,
                        value: value.value
                    });
                });
            });

            $http.post(serviceEndPoint + 'api/v1/project-permission-role?access_token=' + sessionService.get('access_token'), {
                role_id: $scope.selectedRole,
                permissions: JSON.stringify(role_permissions)//JSON.stringify($scope.user_role_permission)
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };
        //Save Permissions for Specific User
        this.managePermissionUser = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/acl-permission-user?access_token=' + sessionService.get('access_token'), {
                user_id: $scope.selectedUser,
                permissions: JSON.stringify($scope.user_role_permission)
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;

        };


        this.rolePermission = function ($scope) {
            var params = {};
            params.Authorization = sessionService.get('access_token');
            params.role_id = $scope.selectedRole;
            params.user_id = $scope.selectedUser;
            params.system_permission = '301,300';
            var deferred = $q.defer();

            $http({
                method: 'GET',
                headers:params,
                //url: serviceEndPoint + 'api/v1/project-role-permission',
                url: serviceEndPoint + '/api/v1/permission/role/'+params.role_id,
                // url: serviceEndPoint + ' /api/v1/permission/list/'+params.role_id,
                params: params
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };

        this.saveRole = function ($scope) {
            var params = {};
            params.Authorization = sessionService.get('access_token');
            params.Accept = 'application/json';
            var data = {};
            data.name = $scope.role_name;
            data.description = $scope.role_desc;
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/role/create',
                headers: params,
                data:data
              
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };
        this.saveProjectRole = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            params.role_name = $scope.role_name;
            params.role_desc = $scope.role_desc;
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/project-role',
                params: params
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };

        this.delRole = function (id, $scope) {
            var params = {};
            params.Authorization = sessionService.get('access_token');
           
            var deferred = $q.defer();

            $http({
                method: 'DELETE',
                url: serviceEndPoint + 'api/v1/role/delete/' + id,
                headers: params
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };
        this.delProjectRole = function (id, $scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'DELETE',
                url: serviceEndPoint + 'api/v1/project-role/' + id,
                params: params
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };

        this.getRoleById = function (id, $scope) {
            var params = {};
            params.Authorization = sessionService.get('access_token');
            var deferred = $q.defer();
            

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/role/get/' + id,
                headers: params
            }).success(function (data, status, headers, config) {
                console.log("get role by ID Success");
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };

        this.getProjectRoleById = function (id, $scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-role/' + id,
                params: params
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list error
            });

            return deferred.promise;
        };

        this.editRole = function ($scope) {
            var params = {};
            params.Authorization = sessionService.get('access_token');
            params.Accept = 'application/json';
            var data = {};
            data.name = $scope.role_name;
            data.description = $scope.role_desc;
            var deferred = $q.defer();
            $http({                
                    method: 'POST',
                    url: serviceEndPoint + 'api/v1/role/update/' + $scope.role_id,
                    headers: params,
                    data:data
                                 
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;

        };
        this.editProjectRole = function ($scope) {

            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-role/' + $scope.role_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                role_name: $scope.role_name,
                role_desc: $scope.role_desc
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;

        };

        /*
         * Re arrange role permission array
         */
        this.generateRolePermissionArray = function ($scope, role_permission) {
            var deferred = $q.defer();
            //console.log(role_permission);
            var new_role_permission = {};
            var excluded_permission_array = [802];
            var exclude_metadata_array = [1, 600, 601, 602, 603, 604, 605, 620, 621, 622, 623, 624, 625, 630, 631, 632, 633, 634, 635]; //634=>Key-value 
            excluded_permission_array = excluded_permission_array.concat(exclude_metadata_array);
            if ($scope.selectedRole != 1) {
                //Exclude RolePermissionAlocation, Manage User, Manage Role permission, if it is not Admin
                //exclude_metadata_array.push(101, 102, 103, 104, 107, 109, 110, 111, 112, 113, 114);
                excluded_permission_array.push(107);
            }
//            console.log(excluded_permission_array);
            angular.forEach(role_permission, function (value, key) {
                if (jQuery.inArray(value.id, excluded_permission_array) === -1) {
                    //console.log(jQuery.inArray(value.id, excluded_permission_array));
                    var identifier = $filter('firstWord')(key);
                    if (!new_role_permission[identifier]) {
                        new_role_permission[identifier] = [];
                    }
                    new_role_permission[identifier].push({
                        identifier: identifier,
                        description: value.description,
                        grant: value.grant,
                        value: value.id
                    });
                }
            });

            //Prepare Metadata Permission set
            this.prepareMetaData($scope).then(function (metaDataPermission) {
                new_role_permission['metadata'] = [];
                new_role_permission['metadata'] = metaDataPermission;
                deferred.resolve(new_role_permission);
            });
            //console.log(metaDataPermission);
            //console.log(new_role_permission);
            //return new_role_permission;


            return deferred.promise;
        }
        /**
         * Prepare MetaData Permission Array
         * @param {type} $scope
         * @returns {unresolved}
         */
        this.prepareMetaData = function ($scope) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/role-permission/role_id/' + $scope.selectedRole + '/permission_id/620,621,622,623,624,625' + '?access_token=' + sessionService.get('access_token'),
            }).success(function (data, status, headers, config) {
                var metaDataPermission = [{
                        identifier: 'metadata.show.all',
                        description: 'Show Metadata',
                        grant: data.data['620'].grant,
                        value: 6000
                    }, {
                        identifier: 'metadata.create',
                        description: 'Create Metadata',
                        grant: data.data['621'].grant,
                        value: 6001
                    }, {
                        identifier: 'metadata.edit',
                        description: 'Edit Metadata',
                        grant: data.data['622'].grant,
                        value: 6002
                    }, {
                        identifier: 'metadata.delete',
                        description: 'Delete Metadata',
                        grant: data.data['623'].grant,
                        value: 6003
                    }, {
                        identifier: 'metadata.active.inactive',
                        description: 'Active/Inactive Metadata',
                        grant: data.data['624'].grant,
                        value: 6004
                    }, {
                        identifier: 'metadata.apply',
                        description: 'Apply Metadata',
                        grant: data.data['625'].grant,
                        value: 6005
                    }];
                deferred.resolve(metaDataPermission);
            }).error(function (data, status, headers, config) {

            });
            return deferred.promise;
        }

        /**
         * Save users on to project
         * @returns {undefined}
         */
        this.associateProjectUser = function ($scope) {
            var deferred = $q.defer();
            $scope.showMsg = 1;
            $http.post(serviceEndPoint + 'api/v1/project-user/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                username: $scope.userRoleMap.username,
                user_id: $scope.userRoleMap.user_id,
                role_id: $scope.userRoleMap.role['id'],
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                $scope.status = data.status;
                $scope.message = data.message;

                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        /**
         * Update users on to project
         * @returns {undefined}
         */
        this.updateProjectUser = function ($scope) {
            var deferred = $q.defer();
            $scope.showMsg = 1;
            var queryString = '';
            $http.post(serviceEndPoint + 'api/v1/project-user/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                username: $scope.userRoleMap.username,
                role_id: $scope.userRoleMap.role['id'],
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        /**
         * Delete user from project user map
         * @returns {undefined}
         */
        this.deleteProjectUser = function ($scope) {
            var deferred = $q.defer();
            $scope.showMsg = 1;
            var queryString = '';
            /*if (angular.isDefined($scope.userRoleMap)) {
             queryString += '&user_role_map=' + angular.toJson($scope.userRoleMap);
             }*/

            $http.post(serviceEndPoint + 'api/v1/project-user/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'DELETE',
                username: $scope.userRoleMap.username,
                role_id: $scope.userRoleMap.role_id,
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                $scope.status = data.status;
                $scope.message = data.message;

                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

        this.count_user_by_role = function (role_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            params.role_id = role_id;
            $http({
                method: 'GET',
                // url: serviceEndPoint + 'api/v1/project-users/role_id',
                url: serviceEndPoint + 'api/v1/role/delete/'+role_id,
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.check_permission = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            if (angular.isDefined($scope.project_id)) {
                params.project_id = $scope.project_id;
            }
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/check-permission/identifier/' + $scope.permission_identifier,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);