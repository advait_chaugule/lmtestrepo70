/*****
 * Services for KeyValue Services
 * Author - Alamgir Hossain Sk 
 *  
 */
'use strict';

//Start of KeyValue Services
app.service('keyValueService', ['$http', '$q', 'sessionService', '$rootScope', function ($http, $q, sessionService, $rootScope) {

        /*
         * Service to add Key Value
         */
        this.addKeyValue = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/key-value?access_token=' + sessionService.get('access_token'), {
                key_name: $scope.key_name,
                key_type: $scope.key_type,
                values: JSON.stringify($scope.select_values)
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to edit Key Value
         */
        this.editKeyValue = function ($scope, key) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/key-value/' + key.id + '?access_token=' + sessionService.get('access_token'), {
                key_name: $scope.key_name,
                key_type: $scope.key_type,
                values: JSON.stringify($scope.select_values),
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to delete  Key Value
         */
        this.delKeyValue = function ($scope, key_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/key-value/' + key_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        /*
         * Service asset to key value mapping
         */
        this.keyValueObjectMapping = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/tag-object-mapping?access_token=' + sessionService.get('access_token'), {
                tag_id: $scope.tag_id,
                asset_ids: $scope.asset_ids
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        /**
         * Get keys
         */
        this.getKeyValue = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }

            $http.get(serviceEndPoint + 'api/v1/key-value?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /**
         * Get Key value Detail By Id
         */
        this.getKeyDetailsByKey = function ($scope, key_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/key-value/' + key_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/key-value-status/' + id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getKeyValuesByObject = function ($scope, objet_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';

            if (angular.isDefined($scope.project_id) && angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }

            $http.get(serviceEndPoint + 'api/v1/key-value-object/' + objet_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                /*if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }*/ //Commented as it was redirecting the user though Global permissions are all checked FROS-2082
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getMetadataKeyUsage = function ($scope, key_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            if(angular.isDefined($scope.data.selectValue)) {
                queryString += '&value_id=' + $scope.data.selectValue;
            }
            $http.get(serviceEndPoint + 'api/v1/objects-key-usage/key_id/' + key_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getMetadataValueUsage = function ($scope, key_id, value_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            $http.get(serviceEndPoint + 'api/v1/objects-key-value-usage/key_id/' + key_id + '/value_id/'+ value_id +'?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };


    }]);