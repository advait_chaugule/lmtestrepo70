/*****
 * Controller for Key value
 * Author - Alamgir Hossain Sk * 
 */

'use strict';
app.controller('key_value', ['$scope', '$http', 'sessionService', '$location', 'keyValueService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', function ($scope, $http, sessionService, $location, keyValueService, $rootScope, $modal, $log, SweetAlert, $routeParams) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.project_id = $routeParams.project_id;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.currentOrderByField = 'key_name';
        $scope.reverseSort = false;
        $scope.search_text = '';
        //Call Key
        $scope.currentPage = 1;
        $scope.pageNumber = 1;
        $scope.itemsPerPage = '';
        $scope.keys = {};
        $scope.getKeyValue = function (pageNumber) {
            $scope.pageNumber = pageNumber;
            $scope.currentPage = pageNumber;
            $scope.itemsPerPage = '10';
            keyValueService.getKeyValue($scope).then(function (data) {
                if (data.status == 200) {
                    $scope.total = data.total;
                    if (data.data !== null) {
                        $scope.keys = data.data.list;
                    } else {
                        $scope.keys = {};
                    }
                }

            });
        };
        $scope.getKeyValue($scope.currentPage);
        ///

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerKayValue = function (newPage) {
            $scope.getKeyValue(newPage);
        };

        $scope.create_button = false;
        $scope.openKeyValueAddModal = function (size) {
            $scope.create_button = true;
            var modalInstance = $modal.open({
                templateUrl: 'templates/key_value/keyValueModalContent.html',
                controller: 'ModalInstanceKeyValueCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormText: function () {
                        return "You are trying to add a Key Value.";
                    },
                    modalFormTile: function () {
                        return "Add Key Value";
                    },
                    key: function () {
                        return '';
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.currentOrderByField = 'key_name';
                $scope.reverseSort = false;
                $scope.create_button = false;
                $scope.getKeyValue($scope.currentPage);
            }, function () {
                $scope.create_button = false;
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.edit_button = false;
        $scope.openKeyValueEditModal = function (size, key_id) {
            $scope.edit_button = true;
            keyValueService.getKeyDetailsByKey($scope, key_id).then(function (data) {
                var modalInstance = $modal.open({
                    templateUrl: 'templates/key_value/keyValueModalContent.html',
                    controller: 'ModalInstanceKeyValueCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        modalFormText: function () {
                            return "You are trying to Edit a Key Value.";
                        },
                        modalFormTile: function () {
                            return "Edit Key Value";
                        },
                        key: function () {
                            return data.data.key;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    $scope.currentOrderByField = 'key_name';
                    $scope.reverseSort = false;
                    $scope.edit_button = false;
                    $scope.getKeyValue($scope.currentPage);
                }, function () {
                    $scope.edit_button = false;
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            });
        };
        //Delete key and there values
        $scope.deleteKeyValue = function (key_id, current) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this key value!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    keyValueService.delKeyValue($scope, key_id).then(function (data) {
                        if (data.status == 200) {
                            if (current !== 'undefined') {
                                $scope.getKeyValue(current);
                            } else {
                                $scope.getKeyValue($scope.currentPage);
                            }

                            SweetAlert.swal("Success", "Key has been deleted", "success");
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                SweetAlert.swal("Cancelled", data.message, "warning");
                            }
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Key is safe :)", "error");
                }
            });
        };
        $scope.changeStatus = function (id) {
            keyValueService.changeStatus(id, $scope).then(function (data) {
                if (data.status == 200) {
                    $scope.getKeyValue($scope.currentPage);
                } else {
                    if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                        SweetAlert.swal("Cancelled", data.message, "warning");
                    }
                }
            });
        };
        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.getKeyValue($scope.currentPage);
        };
        // apply searching
        $scope.do_search = function () {
            $scope.getKeyValue(1);
        };
        $scope.openKeyUsageModal = function (size, key_id) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/key_value/keyUsageModalContent.html',
                controller: 'ModalInstanceKeyMetadataCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormText: function () {
                        return "";
                    },
                    modalFormTile: function () {
                        return "Metadata Key Usage";
                    },
                    key_id: function () {
                        return key_id;
                    },
                    user_permissions: function(){
                        return $scope.user_permissions;
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.key_value = [];
        /*
         *  Open value usage of a key
         */
        $scope.onChangeValue = function (size, key_id) {
            var value_id = $scope.key_value[key_id];
            // check usage is more than zero
            var usage_count = 0;
            angular.forEach($scope.keys, function (key_details, index) {
                if (key_details.id == key_id) {
                    var values = key_details.values;
                    angular.forEach(values, function (value, key) {
                        if (value.value_id == value_id) {
                            usage_count = value.usage_count;
                        }
                    });
                }
            });
            if (usage_count > 0) {
                var modalInstance = $modal.open({
                    templateUrl: 'templates/key_value/valueUsageModalContent.html',
                    controller: 'ModalInstanceValueMetadataCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        modalFormText: function () {
                            return "";
                        },
                        modalFormTile: function () {
                            return "Metadata Value Usage";
                        },
                        key_id: function () {
                            return key_id;
                        },
                        value_id: function () {
                            return value_id;
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            }
        };

    }]);
/*
 * Create Key value Modal window Controller
 */
app.controller('ModalInstanceKeyValueCtrl', function ($scope, $filter, keyValueService, $modalInstance, modalFormText, modalFormTile, key, $compile) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.keyValueModal = key;
    //Open key value add form
    $scope.key_name = '';
    $scope.key_type = 'text_entry';
    $scope.showAdd = true;
    $scope.showEdit = false;
    $scope.select_values = [];
    $scope.value = '';
    $scope.uses = 0;
    $scope.formError = {'key_name': '', 'value': '', 'error': ''};
    $scope.key_type_array = ['text_entry','date_entry','time_entry','number_entry'];
    if (angular.isDefined(key.id) && key.id != '') {
        $scope.key_name = key.key_name;
        $scope.key_type = key.key_type;
        $scope.showAdd = false;
        $scope.showEdit = true;
        // populate values
        if ($scope.key_type == 'select_list')
        {
            angular.forEach(key.values, function (select_value) {
                $scope.select_values.push({"id": select_value.value_id, 'value': select_value.value, 'uses': select_value.usage_count});
            });
        }

    }

    // Add key
    $scope.add = function () {
        if (angular.isUndefined($scope.key_name) == false) {
            if ($scope.key_name == '')
            {
                $scope.formError['key_name'] = 'The key name field is required.';
            } else {
                $scope.formError['key_name'] = '';
                if (($scope.key_type == 'select_list' && document.querySelectorAll(".select-value").length) || ($scope.key_type_array.indexOf($scope.key_type)>=0))
                {
                    keyValueService.addKeyValue($scope).then(function (data) {
                        if (data.status == 200) {
                            $modalInstance.close();
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                $scope.formError['error'] = data.message;
                            }
                            if (angular.isDefined(data.error.key_name)) {
                                $scope.formError['key_name'] = data.error.key_name['0'];
                            }
                        }
                    });
                } else {
                    $scope.formError['value'] = 'Please add select value.';
                }
            }

        }
    };
    // Edit Key
    $scope.edit = function () {

        if (angular.isUndefined($scope.key_name) == false) {
            if ($scope.key_name == '')
            {
                $scope.formError['key_name'] = 'The key name field is required.';
            } else {
                $scope.formError['key_name'] = '';
                if (($scope.key_type == 'select_list' && document.querySelectorAll(".select-value").length) || ($scope.key_type_array.indexOf($scope.key_type)>=0))
                {
                    keyValueService.editKeyValue($scope, key).then(function (data) {
                        if (data.status == 200) {
                            $modalInstance.close();
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                $scope.formError['error'] = data.message;
                            }
                            if (angular.isDefined(data.error.key_name)) {
                                $scope.formError['key_name'] = data.error.key_name['0'];
                            }
                        }
                    });
                } else {
                    $scope.formError['value'] = 'Please add select value.';
                }
            }
        }
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.addValue = function () {
        // check value is blank and exist in array
        var valid = true;
        if ($scope.value == '')
        {
            valid = false;
            $scope.formError['value'] = 'The value name may not be blank.';
        } else if ($scope.value.length > 255) {
            valid = false;
            $scope.formError['value'] = 'The value name may not be greater than 255 characters.';
        } else {
            if ($scope.select_values.length) {
                angular.forEach($scope.select_values, function (select_value) {
                    if (select_value.value.toLowerCase() == $scope.value.toLowerCase()) // value is already exists
                    {
                        valid = false;
                    }
                });
            }
        }

        if (valid)
        {
            $scope.select_values.push({"id": "0", 'value': $scope.value, 'uses': $scope.uses});
            $scope.value = '';
            $scope.formError['value'] = '';
        }
    };
    $scope.editSelectValue = function (event, select_value) {
        var index = $scope.getSelectIndex(select_value);
        if (index === -1) {
            //alert( "Something gone wrong" );
        }

        var editEl = angular.element(event.target);
        var allSelectArray = document.querySelectorAll(".select-value");
        angular.forEach(allSelectArray, function (elem, elem_index) {
            if (elem_index == index)
            {
                if (editEl.hasClass('value-edit'))
                {
                    angular.element(elem).html('<input type="text" class="form-control" ng-model="select_value_' + index + '" /><span class="highlight text-danger">{{formError["select_value_' + index + '"]}}</span>');
                    $compile(elem)($scope);
                    $scope['select_value_' + index] = select_value;
                    editEl.html('Save');
                    editEl.removeClass('value-edit');
                    editEl.addClass('value-saved');
                } else {
                    // validate select value
                    var valid = true;
                    if ($scope['select_value_' + index] == '')
                    {
                        valid = false;
                    } else if ($scope['select_value_' + index].length > 255) {
                        valid = false;
                        $scope.formError['select_value_' + index] = 'Value name may not be greater than 255 characters.';
                    } else {
                        if ($scope.select_values.length) {
                            angular.forEach($scope.select_values, function (select_value, sel_index) {
                                if (select_value.value == $scope['select_value_' + index] && sel_index != index) // value is already exists
                                {
                                    valid = false;
                                }
                            });
                        }
                    }

                    if (valid)
                    {
                        $scope.select_values[index].value = $scope['select_value_' + index];
                        angular.element(elem).html($filter('strLimit')($scope['select_value_' + index], 40));
                        editEl.removeClass('value-saved');
                        editEl.addClass('value-edit');
                        editEl.html('Edit');
                    }
                }
            }
        });
    };
    $scope.removeSelectValue = function (select_value) {
        var index = $scope.getSelectIndex(select_value);
        if (index === -1) {
            //alert( "Something gone wrong" );
        }
        $scope.select_values.splice(index, 1);
    };
    $scope.getSelectIndex = function (select_value) {
        var index = -1;
        var valueArr = eval($scope.select_values);
        for (var i = 0; i < valueArr.length; i++) {
            if (valueArr[i].value === select_value) {
                index = i;
                break;
            }
        }
        return index;
    };

});
/*
 *  Key Metadata Modal window Controller
 */
app.controller('ModalInstanceKeyMetadataCtrl', function ($scope,$log,$location,$rootScope,$modal,keyValueService,objectService,assetService,SweetAlert,$modalInstance, modalFormText, modalFormTile, key_id,user_permissions) {
    $scope.key_id = key_id;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.user_permissions=user_permissions;
    $scope.keyModel = {};
    $scope.keyMetadataObjects = {};
    $scope.currentPage = 1;
    $scope.pageNumber = '';
    $scope.itemsPerPage = '';
    //Open key metadata usage
    $scope.key_title = '';
    $scope.key_type = '';
    $scope.data = {};

    $scope.showPagination = false;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    // get key
    $scope.getKeys = function () {
        keyValueService.getMetadataKeyUsage($scope, key_id).then(function (data) {
            if (data.data == null || data.data == '') {
                $scope.total = 0;
                $scope.keyModel = {};
                $scope.keyMetadataObjects = {};
            } else {
                var key = data.data.key;
                $scope.keyModel = key;
                $scope.data.valueModel = data.data.values;
                if (angular.isDefined(key.key_type) && key.key_type != '') {
                    $scope.key_type = key.key_type;
                }
                if (angular.isDefined(key.id) && key.id != '') {
                    $scope.key_title = key.key_name;
                }
            }
        });
    };
    $scope.getKeys();

    $scope.getObjects = function (pageNumber) {
        $scope.currentPage = pageNumber;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = '10';
        keyValueService.getMetadataKeyUsage($scope, key_id).then(function (data) {
            if (data.data == null || data.data == '') {
                $scope.total = 0;
                $scope.keyModel = {};
                $scope.keyMetadataObjects = {};
            } else {
                $scope.total = data.data.total;
                if (data.data.total > data.data.totalContents) {
                    $scope.showPagination = true;
                }
                $scope.keyMetadataObjects = data.data.metadata_objects;
            }

        });
    };
    $scope.getObjects(1);

    $scope.pageChangeHandlerObject = function (pageNumber) {
        $scope.getObjects(pageNumber);
    };

    $scope.onChangeValue = function () {
        $scope.getObjects(1);
    };
$scope.redirctToProject=function(object_id, toc_creation_status,usage_type){
        switch (usage_type) {
            case 'ASSESSMENT':
                $scope.checkProjectExists(object_id);
                break;
            case 'OBJECT':
                if (parseInt(toc_creation_status) == 3001) {
                    $scope.checkProjectExists(object_id);
                } else {
                    $rootScope.show_toc = 0;
                    $location.path('project_import/' + object_id);
                }
                break;
            default:
        }                
    };
    $scope.checkProjectExists=function(object_id){
        $scope.project_id = object_id;
        objectService.getProjectDetails($scope).then(function (data) {
            if (data != null) {
                $rootScope.show_toc = 1;
                $location.path("edit_toc/" + object_id);
            }else{
                $rootScope.show_toc = 0;
                SweetAlert.swal({html: 'Either the project does not exist or you are no longer allocated to this project.', width: 540, type: "warning"});
            } 
        });
    };
	
 $scope.preview_button_disabled = false;
        $scope.previewAssessment = function (preview_info) {
            $scope.preview_button_disabled = true;
            var size = 'lg';
            var modal_form_title = 'Assessment Preview';
            var previewInfo=JSON.parse(preview_info);
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/previewModal.html',
                controller: 'ModalPreviewCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    shortcode: function () {
                        return previewInfo.shortcode;
                    },
                    project_id: function () {
                        return previewInfo.project_id;
                    },
                    project_type_id: function () {
                        return previewInfo.project_type_id;
                    },
                    entity_type_id: function () {
                        return 2;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.preview_button_disabled = false;
            }, function () {
                $scope.preview_button_disabled = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

$scope.previewAsset = function (size, preview_info) {
            $scope.applyMetadata=0;
            var preview_infos=JSON.parse(preview_info);
            var modalInstance = $modal.open({
                templateUrl: 'templates/asset/assetModalContent.html',
                controller: 'ModalAssetCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return preview_infos.modal_form_title;
                    },
                    object_id: function () {
                        return preview_infos.object_id;
                    },
                    project_id: function () {
                        return preview_infos.project_id;
                    },
                    displayOption: function () {
                        return false;
                    },
                    user_permissions: function () {
                        return $scope.user_permissions;
                    },
                    project_permissions: function () {
                        return true;
                    }
                }
            });          
        };
		
		
$scope.previewContent=function (size, id) {
            var contentRec = {};
            $scope.id = id;
            assetService.getGlobalContentDetails($scope).then(function (data) {
                contentRec = data.data;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/toc/globalModalContentView.html',
                    controller: 'ModalInstanceContentCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        contentRec: function () {
                            return contentRec;
                        },
                        object_id: function () {
                            return id;
                        },
                        modalFormText: function () {
                            return "You are trying to view a content";
                        },
                        modalFormTile: function () {
                            return "Preview Content";
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };
});