/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';
var returnPatterns = '';
app.service('errorLogService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        /*
         * Service to get system error log         
         */
        this.getErrorLog = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            $http.get(serviceEndPoint + 'api/v1/errorlog?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the activity json             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
    }]);

