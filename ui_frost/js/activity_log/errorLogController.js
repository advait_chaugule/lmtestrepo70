/*****
 * Controller for TOC
 * Author - Arikh Akher * 
 */

'use strict';
var access_token = '';
var returnPatterns = '';
var toc_type = '';
var tooltipDesc = '';
app.controller('error_log', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$route', '$rootScope', 'errorLogService', function ($scope, sessionService, tocService, $location, $routeParams, $route, $rootScope, errorLogService) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        // passing access_token to simple editor
        access_token = $scope.access_token;
        $scope.userdetails = JSON.parse(sessionService.get('userinfo'));
        $scope.user_id = $scope.userdetails.id;

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
        } else {
            $scope.show_error_log = function ($scope) {
                errorLogService.getErrorLog($scope).then(function (data) {
                    if (data.data != null) {
                        $scope.display_error_content = data.data;
                    }
                    else {
                        $scope.display_error_content = '';
                    }
                });
            }

            $scope.show_error_log($scope);

        }
    }]);