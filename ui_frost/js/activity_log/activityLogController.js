/*****
 * Controller for TOC
 * Author - Arikh Akher * 
 */

'use strict';
var access_token = '';
var returnPatterns = '';
var toc_type = '';
var tooltipDesc = '';
app.controller('activity_log', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$route', '$rootScope', 'activityLogService', function ($scope, sessionService, tocService, $location, $routeParams, $route, $rootScope, activityLogService) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        // passing access_token to simple editor
        access_token = $scope.access_token;
        $scope.userdetails = JSON.parse(sessionService.get('userinfo'));
        $scope.user_id = $scope.userdetails.id;

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
        } else {
            if ($scope.user_id !== 1) {
                $rootScope.goto('dashboard');
            }
            //call the service and fetch the activity logs listed therein.
//            $scope.pageNumber = pageNumber;
//            $scope.itemsPerPage = '';
            $scope.currentPage = 1;
            $scope.pageNumber = '';
            $scope.itemsPerPage = '';
            $scope.totalLog = 1;
            $scope.getActivityLog = function (pageNumber) {
                $scope.pageNumber = pageNumber;
                $scope.itemsPerPage = 20;
                activityLogService.getLogs($scope).then(function (data) {
                    if (data.data != null) {
                        $scope.loggedActivity = data.data["ActivityLog"];
                        $scope.totalLog = data.data["totalLog"];
                    }
                    else {
                        $scope.loggedActivity = [{"message": "No activity Logged.", "startdate": ""}];
                        $scope.totalLog = 0;
                    }
                });
            }
            $scope.getActivityLog(1);
            //For Pagination Of Dashboard
            $scope.pageChangeHandlerActivityLog = function (newPage) {
                $scope.getActivityLog(newPage);
            };


            //Static approach
//            $scope.reverse = true;
//            $scope.logJson = {"status": 200, "data": {"ActivityLog": [{"message": "admin has created a project, named \"pxe1\" on 2015-07-13 17:06:00 ", "startdate": "2015-07-13 05:06:01"}, {"message": "Default TOC has been created for project \"Edupub\" by \"admin\" ", "startdate": "2015-07-13 05:28:35"}, {"message": "admin has created a project, named \"Edupub\" on 2015-07-13 17:28:29 ", "startdate": "2015-07-13 05:28:35"}, {"message": "Default TOC has been created for project \"pxe1\" by \"admin\" ", "startdate": "2015-07-13 06:16:37"}, {"message": "admin has created a project, named \"Pxe 2\" on 2015-07-14 16:10:31 ", "startdate": "2015-07-14 04:10:32"}]}, "message": null, "error": null};
//
//            $scope.loggedActivity = $scope.logJson.data["ActivityLog"];
            $scope.reverse = true;
            $scope.order = function () {
                $scope.reverse = !$scope.reverse;
            }

        }
    }]);