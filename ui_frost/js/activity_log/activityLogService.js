/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';
var returnPatterns='';
app.service('activityLogService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function($http, $q, sessionService, $location, $rootScope) {

        /*
         * Service to get patterns         
         */
        this.getLogs = function($scope) {            
            var deferred = $q.defer();
            var queryString = '';
            
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber+'&itemsPerPage=' + $scope.itemsPerPage;
            } 


            
            
            $http.get(serviceEndPoint + 'api/v1/userlog/user_id/'+$scope.user_id+'?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function(data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the activity json             
            }).error(function(data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to get pattern

    }]);

