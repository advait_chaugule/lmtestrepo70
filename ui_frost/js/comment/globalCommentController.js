'use strict';
app.controller('global_comment', ['$scope', '$http', 'Comment', '$routeParams', 'sessionService', 'objectService', 'SweetAlert', 'globalCommentService', 'projectService', '$location', function ($scope, $http, Comment, $routeParams, sessionService, objectService, SweetAlert, globalCommentService, projectService, $location) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.loggedid_user = userinfo.id;
        $scope.project_id = $routeParams.project_id;

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.ticket_status = [];
        $scope.ticket_issue_types = [];
        $scope.project_users = [];
        //$scope.commentData['ticket_issue_types'] = 1;
        $scope.commentData = {'text': '', 'ticket_issue_types': '', 'ticket_status': '', 'project_users': ''};
        $scope.replyData = [];

        var type_arr = [];
        var status_arr = [];
        var proj_users_arr;
        $scope.prepareTicketTypes = function () {
            //Preparing Ticket Type Array            
            for (var i in $scope.ticket_issue_types) {
                if ($scope.ticket_issue_types.hasOwnProperty(i)) {
                    var obj = {};
                    obj['id'] = i;
                    obj['name'] = $scope.ticket_issue_types[i];
                    type_arr.push(obj);
                }
            }
            $scope.ticket_issue_types = type_arr;
            $scope.commentData.ticket_issue_types = type_arr[1];
        }

        $scope.getTicketTypeStatus = function () {
            globalCommentService.getTicketTypeStatus($scope).then(function (data) {
                $scope.ticket_status = data.data.ticket_status;
                $scope.ticket_issue_types = data.data.ticket_issue_types;

                //Preparing Ticket Type Array
                $scope.prepareTicketTypes();

                $scope.$watch('scope.commentData.ticket_issue_types', function (newValue, oldValue) {
                    //Preparing Ticket Status Array                    
                    for (var i in $scope.ticket_status) {
                        if ($scope.ticket_status.hasOwnProperty(i)) {
                            var obj = {};
                            obj['id'] = i;
                            obj['name'] = $scope.ticket_status[i];
                            status_arr.push(obj);
                        }
                    }
                    $scope.ticket_status = status_arr;
                    $scope.commentData.ticket_status = status_arr[0];
                });

                $scope.$watch('scope.commentData.ticket_status', function (newValue, oldValue) {
                    projectService.get_mapapped_users($scope).then(function (data) {
                        $scope.project_users = data.data;
                        $scope.commentData.project_users = $scope.project_users[0];
                        proj_users_arr = data.data;
                    });
                });

            });
        }
        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project_description = data.description;
                        $scope.toc_creation_status = data.toc_creation_status;
                        $scope.project = data;
                        $scope.project.project_permissions = data.project_permissions;
                        $scope.project.theme = data.theme_location;

                        //Get Ticket Type Status
                        $scope.getTicketTypeStatus();
                    }
                });
            }
        }
        $scope.getProject();

        $scope.$watch('commentData.project_users', function (newValue, oldValue) {
            //console.log(newValue);
            $scope.getComments();
        }, true);

        $scope.formError = {'text': ''};
        $scope.submitComment = function () {
            $scope.formError = {'text': '', 'project_users': '', 'ticket_issue_types': '', 'ticket_status': ''};
            // save the comment. pass in comment data from the form
            // use the function we created in our service
            $scope.commentResponse = {'showMsg': 0, 'message': '', 'status': ''};
            $scope.commentData.project_id = $routeParams.project_id;
            globalCommentService.saveTicket($scope, $scope.commentData).then(function (data) {
                $scope.commentResponse.showMsg = 1;
                $scope.commentResponse.message = data.message;
                $scope.commentResponse.status = data.status;
                if (data.status == '400') {
                    $scope.formError.text = (angular.isDefined(data.error.issue_description[0]) ? data.error.issue_description[0] : '');
                    $scope.formError.project_users = (angular.isDefined(data.error.assigned_to[0]) ? data.error.assigned_to[0] : '');
                    $scope.formError.ticket_issue_types = (angular.isDefined(data.error.type[0]) ? data.error.type[0] : '');
                    $scope.formError.ticket_status = (angular.isDefined(data.error.status[0]) ? data.error.status[0] : '');


                } else {
                    $scope.commentData.text = '';
                    $scope.getComments();
                    $scope.commentData.ticket_issue_types = type_arr[1];
                    $scope.commentData.ticket_status = status_arr[0];
                    $scope.project_users = proj_users_arr;
                    $scope.commentData.project_users = $scope.project_users[0];
//          $scope.commentData.project_users = proj_users_arr;
                }

            });
        }

        $scope.getComments = function () {
            globalCommentService.getComments($scope).then(function (data) {
                $scope.comments = data.data.ticket;
                //console.log($scope.comments);
                $scope.propertyName = 'time';
                $scope.reverse = true;
                var rp = {};
                for (var i in $scope.comments) {
                    if ($scope.comments.hasOwnProperty(i)) {
                        var obj = {
                            'text': '',
                            'ticket_issue_types': $scope.commentData.ticket_issue_types,
                            'project_users': $scope.commentData.project_users,
                            'ticket_status': $scope.commentData.ticket_status
                        };
                        var obj1 = $scope.comments[i].ticket_id;
                        rp[obj1] = obj;
                    }
                }
                $scope.replyData = rp;
            });
        };
        //$scope.getComments();

        $scope.openReplyForm = function (ticket_unique_id) {
            jQuery('#reply_form' + ticket_unique_id).slideDown();
        }
        $scope.closeReplyForm = function (ticket_unique_id) {
            jQuery('#reply_form' + ticket_unique_id).slideUp();
        }

//$scope.replyFormError = {'0':{'text': ''}};
        $scope.replyFormError = {};
        $scope.submitReply = function (parent_ticket_id) {
            //$scope.loading = true;
            $scope.replyFormError[parent_ticket_id.ticket_id] = {'text': ''};
            // save the comment. pass in comment data from the form
            // use the function we created in our service
            //$scope.replyFormError = {'showMsg':'0'};
            $scope.replyData.project_id = $routeParams.project_id;
            $scope.replyData.parent_ticket_id = parent_ticket_id;
            $scope.replyData.project_usres = [];
            //console.log($scope.replyData.parent_ticket_id);
            //console.log($scope.replyData);
            //debugger;
            globalCommentService.saveReply($scope, $scope.replyData).then(function (data) {
//                /$scope.showMsg = 1;
                $scope.message = data.message;
                $scope.commentResponse = {'showMsg': 0, 'message': '', 'status': ''};
                //$scope.status = $scope.commentStatus = data.status;
                if (data.status == '400') {
                    if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == '401') {
                        $scope.replyFormError[parent_ticket_id.ticket_id] = {'text': angular.isDefined(data.message) ? data.message : ''};
                    } else if (angular.isDefined(data.error.text)) {
                        $scope.replyFormError[parent_ticket_id.ticket_id] = {'text': angular.isDefined(data.error.text[0]) ? data.error.text[0] : ''};
                    } else if (angular.isDefined(data.error.ticket_id)) {
                        $scope.replyFormError[parent_ticket_id.ticket_id] = {'text': angular.isDefined(data.error.ticket_id[0]) ? data.error.ticket_id[0] : ''};
                    }
                    //console.log($scope.replyFormError[parent_ticket_id.ticket_id].text); 
                } else {
                    $scope.commentResponse.showMsg = 0;
                    $scope.commentData.text = '';
                    $scope.getComments();
                }

            });
        }

        /**
         * Deleting a Ticket
         * @param {type} comment
         * @returns {undefined}
         */
        $scope.deleteTicket = function (comment) {
            $scope.commentResponse = {'showMsg': 0, 'message': '', 'status': ''};
            SweetAlert.swal({
                title: "Are you sure?",
                text: "This ticket and related comments will be deleted. You will not be able to recover it!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    globalCommentService.deleteTicket($scope, comment).then(function (data) {
                        if (data.status == 400) {
                            SweetAlert.swal("Cancelled", data.message, "error");
                        } else {
                            SweetAlert.swal("Deleted", data.message, "success");
                            $scope.getComments();
                            $scope.commentResponse.showMsg = 0;
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Comment is safe :)", "error");
                }
            });
        }
        /**
         * Deleting a Reply
         * @param {type} comment
         * @returns {undefined}
         */
        $scope.deleteReply = function (comment) {
            $scope.commentResponse = {'showMsg': 0, 'message': '', 'status': ''};
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover it!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    globalCommentService.deleteReply($scope, comment).then(function (data) {
                        if (data.status == 400) {
                            SweetAlert.swal("Cancelled", data.message, "error");
                        } else {
                            SweetAlert.swal("Deleted", data.message, "success");
                            $scope.getComments();
                            $scope.commentResponse.showMsg = 0;
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Comment is safe :)", "error");
                }
            });
        }



        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            globalCommentService.sortByType($scope, propertyName, $scope.reverse).then(function (data) {
                $scope.comments = data.data.ticket;
                //console.log($scope.comments);
                var rp = {};
                for (var i in $scope.comments) {
                    if ($scope.comments.hasOwnProperty(i)) {
                        var obj = {
                            'text': '',
                            'ticket_issue_types': $scope.commentData.ticket_issue_types,
                            'project_users': $scope.commentData.project_users,
                            'ticket_status': $scope.commentData.ticket_status
                        };
                        var obj1 = $scope.comments[i].ticket_id;

                        rp[obj1] = obj;
                    }
                }
                $scope.replyData = rp;
            });
            $scope.propertyName = propertyName;
        }

//=================================================================================//      
// object to hold all the data for the new comment form
//$scope.commentData = {};

// loading variable to show the spinning loading icon
//$scope.loading = true;
// get all the comments first and bind it to the $scope.comments object
// use the function we created in our service
// GET ALL COMMENTS ==============
        /*Comment.get($routeParams.project_id)
         .success(function (data) {
         console.info(data);
         $scope.comments = data.data;
         $scope.project = data.project;
         $scope.loading = false;
         });*/

//$scope.formError = {'text': ''};
// function to handle submitting the form
// SAVE A COMMENT ================
        /*$scope.submitComment = function () {
         $scope.loading = true;
         $scope.formError = {'text': ''};
         // save the comment. pass in comment data from the form
         // use the function we created in our service
         $scope.showMsg = 0;
         $scope.commentData.project_id = $routeParams.project_id;
         Comment.save($scope.commentData)
         .success(function (data) {                        
         $scope.showMsg = 1;
         $scope.message = data.message;
         $scope.status = $scope.commentStatus = data.status;
         if(data.status=='400'){
         $scope.formError = {'text': angular.isDefined(data.error.text[0]) ? data.error.text[0] : ''};
         }else{
         $scope.commentData.text = '';
         }      
         // if successful, we'll need to refresh the comment list
         Comment.get($routeParams.project_id)
         .success(function (getData) {
         $scope.comments = getData.data;
         $scope.loading = false;
         });
         
         })
         .error(function (data) {
         console.log(data);
         });
         };*/

// function to handle deleting a comment
// DELETE A COMMENT ====================================================
        /*$scope.deleteComment = function (id) {
         
         SweetAlert.swal({
         title: "Are you sure?",
         text: "You will not be able to recover it!",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         confirmButtonText: "Yes, delete it!",
         cancelButtonText: "No, cancel!",
         closeOnConfirm: false,
         closeOnCancel: false
         }, function (isConfirm) {
         if (isConfirm) {
         $scope.loading = true;
         // use the function we created in our service
         Comment.destroy(id).then(function (data) {
         SweetAlert.swal("Success", data.message, "success");
         // if successful, we'll need to refresh the comment list
         Comment.get($routeParams.project_id)
         .success(function (getData) {
         $scope.comments = getData.data;
         $scope.loading = false;
         });
         });
         
         
         } else {
         SweetAlert.swal("Cancelled", "Comment is safe :)", "error");
         }
         });
         
         
         };*/
//================================================================================//  






    }]);