'use strict';
app.controller('commentController', ['$scope', '$http', 'Comment', '$routeParams', 'sessionService', 'objectService', 'SweetAlert' ,function ($scope, $http, Comment, $routeParams, sessionService, objectService, SweetAlert) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));     
        $scope.loggedid_user = userinfo.id;
        $scope.project_id = $routeParams.project_id;

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        
        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    $scope.project_name = data.name;
                    $scope.project_description = data.description;
                    $scope.toc_creation_status = data.toc_creation_status;
                    $scope.project = data;//{};
                    $scope.project.project_permissions = data.project_permissions;
                    $scope.project.theme = data.theme_location;
                });
            }
        }
        $scope.getProject();
        
        // object to hold all the data for the new comment form
        $scope.commentData = {};

        // loading variable to show the spinning loading icon
        $scope.loading = true;
        // get all the comments first and bind it to the $scope.comments object
        // use the function we created in our service
        // GET ALL COMMENTS ==============
        Comment.get($routeParams.project_id)
                .success(function (data) {
                    console.info(data);
                    $scope.comments = data.data;
                    $scope.project = data.project;
                    $scope.loading = false;
                });

        $scope.formError = {'text': ''};
        // function to handle submitting the form
        // SAVE A COMMENT ================
        $scope.submitComment = function () {
            $scope.loading = true;
            $scope.formError = {'text': ''};
            // save the comment. pass in comment data from the form
            // use the function we created in our service
            $scope.showMsg = 0;
            $scope.commentData.project_id = $routeParams.project_id;
            Comment.save($scope.commentData)
                    .success(function (data) {                        
                        $scope.showMsg = 1;
                        $scope.message = data.message;
                        $scope.status = $scope.commentStatus = data.status;
                        if(data.status=='400'){
                            $scope.formError = {'text': angular.isDefined(data.error.text[0]) ? data.error.text[0] : ''};
                        }else{
                            $scope.commentData.text = '';
                        }                        
                        

                        // if successful, we'll need to refresh the comment list
                        Comment.get($routeParams.project_id)
                                .success(function (getData) {
                                    $scope.comments = getData.data;
                                    $scope.loading = false;
                                });

                    })
                    .error(function (data) {
                        console.log(data);
                    });
        };

        // function to handle deleting a comment
        // DELETE A COMMENT ====================================================
        $scope.deleteComment = function (id) {
            
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover it!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $scope.loading = true;
                    // use the function we created in our service
                    Comment.destroy(id).then(function (data) {
                        SweetAlert.swal("Success", data.message, "success");
                        // if successful, we'll need to refresh the comment list
                        Comment.get($routeParams.project_id)
                                .success(function (getData) {
                                    $scope.comments = getData.data;
                                    $scope.loading = false;
                                });
                    });
                    
                    
                } else {
                    SweetAlert.swal("Cancelled", "Comment is safe :)", "error");
                }
            });
            
            
        };

    }]);