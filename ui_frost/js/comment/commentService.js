'use strict';
app.factory('Comment', ['$http', 'sessionService', '$q', function ($http, sessionService, $q) {

        return {
            // get all the comments
            get: function (project_id) {
                return $http.get(serviceEndPoint + 'api/v1/comments/' + project_id + '?access_token=' + sessionService.get('access_token'));
            },
            // save a comment (pass in comment data)
            save: function (commentData) {
                return $http({
                    method: 'POST',
                    url: serviceEndPoint + 'api/v1/comments?access_token=' + sessionService.get('access_token'),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param(commentData)
                });
            },
            // destroy a comment
            destroy: function (id) {

                var deferred = $q.defer();
                $http.delete(serviceEndPoint + 'api/v1/comments/' + id + '?access_token=' + sessionService.get('access_token') + '&_method=DELETE', {
                }).success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    deferred.resolve(data);
                    $rootScope.unauthorised_redirection(data.status);
                });
                return deferred.promise;

                //return $http.delete(serviceEndPoint + 'api/v1/comments/' + id + '?access_token=' + sessionService.get('access_token'));
            }
        }

    }]);

/**
 Global Comment Services
 */
app.service('globalCommentService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        //Start - Method to get ticket-types-status
        this.getTicketTypeStatus = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';

            $http.get(serviceEndPoint + 'api/v1/ticket-types-status?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to get ticket-types-status

        this.saveTicket = function ($scope, commentData) {
            var deferred = $q.defer();
            var queryString = '';
            var fd = new FormData();
            fd.append('project_id', $scope.project_id);
            fd.append('object_id', 0);
            fd.append('title', 'title');
            fd.append('issue_description', $scope.commentData.text);
            fd.append('assigned_to', (angular.isDefined($scope.commentData.project_users.user_id) ? $scope.commentData.project_users.user_id : ''));
            fd.append('type', (angular.isDefined($scope.commentData.ticket_issue_types.id) ? $scope.commentData.ticket_issue_types.id : ''));
            fd.append('status', (angular.isDefined($scope.commentData.ticket_status.id) ? $scope.commentData.ticket_status.id : ''));
            fd.append('ticket_attachment', (angular.isDefined($scope.commentData.ticket_attachment) ? $scope.commentData.ticket_attachment : ''));
            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/global-ticket?access_token=' + sessionService.get('access_token') + queryString,
                data: fd,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            /*$http.post(serviceEndPoint + 'api/v1/global-ticket?access_token=' + sessionService.get('access_token') + queryString, {
             project_id: $scope.project_id,
             object_id: '0',
             title: 'title',
             issue_description: $scope.commentData.text,
             assigned_to: $scope.commentData.project_users.user_id,
             type: $scope.commentData.ticket_issue_types.id,
             status: $scope.commentData.ticket_status.id
             }).success(function (data, status, headers, config) {   
             deferred.resolve(data);//Return the project list             
             }).error(function (data, status, headers, config) {
             $rootScope.unauthorised_redirection(data.status);
             });*/
            return deferred.promise;
        };
        this.getComments = function ($scope) {
            var deferred = $q.defer();
            var sort_type = 'time';
            var sort_status = 'desc';
            var queryString = '&sort_type=' + sort_type + '&sort_status=' + sort_status;

            $http.get(serviceEndPoint + 'api/v1/ticket-all/project_id/' + $scope.project_id + '/is_global/1?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.saveReply = function ($scope, replyData) {
            var deferred = $q.defer();
            var queryString = '';
            //console.log($scope.replyData[$scope.replyData.parent_ticket_id.ticket_unique_id]);
            //console.info($scope.replyData.parent_ticket_id.id);

            $http.post(serviceEndPoint + 'api/v1/global-ticket-reply?access_token=' + sessionService.get('access_token') + queryString, {
                parent_ticket_id: $scope.replyData.parent_ticket_id.ticket_id,
                project_id: $scope.project_id,
                object_id: '0',
                comment: $scope.replyData[$scope.replyData.parent_ticket_id.ticket_id].text != '' ? $scope.replyData[$scope.replyData.parent_ticket_id.ticket_id].text : '',
                assigned_to: $scope.replyData[$scope.replyData.parent_ticket_id.ticket_id].project_users.user_id,
                type: $scope.replyData[$scope.replyData.parent_ticket_id.ticket_id].ticket_issue_types.id,
                status: $scope.replyData[$scope.replyData.parent_ticket_id.ticket_id].ticket_status.id
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.deleteTicket = function ($scope, comment) {
            var deferred = $q.defer();
            var queryString = '';
            console.info(comment);
            $http.post(serviceEndPoint + 'api/v1/ticket/ticket_id/' + comment.ticket_id + '/project_id/' + $scope.project_id + '/object_id/0?access_token=' + sessionService.get('access_token') + queryString, {
                _method: 'DELETE'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.deleteReply = function ($scope, reply) {
            var deferred = $q.defer();
            var queryString = '';
            console.info(reply);
            $http.post(serviceEndPoint + 'api/v1/ticket-comment/ticket_coment_id/' + reply.ticket_comment_id + '/ticket_id/' + reply.ticket_id + '/project_id/' + $scope.project_id + '/object_id/0?access_token=' + sessionService.get('access_token') + queryString, {
                _method: 'DELETE'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.sortByType = function ($scope, sort_type, sort_status) {
            var deferred = $q.defer();
            var queryString = '&sort_type=' + sort_type + '&sort_status=' + sort_status;
            $http.get(serviceEndPoint + 'api/v1/ticket-all/project_id/' + $scope.project_id + '/is_global/1?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }



    }]);

