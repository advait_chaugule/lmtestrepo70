/*****
 * Controller for element
 * Author - Parveen Khan * 
 */

'use strict';
app.controller('element', ['$scope', '$http', 'sessionService', '$location', 'elementService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', function ($scope, $http, sessionService, $location, elementService, $rootScope, $modal, $log, SweetAlert, $routeParams) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.project_id = $routeParams.project_id;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.currentOrderByField = 'element_name';
        $scope.reverseSort = false;
        $scope.search_text = '';
        //Call Element
        $scope.currentPage = 1;
        $scope.pageNumber = 1;
        $scope.itemsPerPage = '';
        $scope.elements = {};
        $scope.getElement = function (pageNumber) {
            $scope.pageNumber = pageNumber;
            $scope.currentPage = pageNumber;
            $scope.itemsPerPage = '10';
            elementService.getElement($scope).then(function (data) {
                if (data.status == 200) {
                    $scope.total = data.total;
                    if (data.data !== null) {
                        $scope.elements = data.data.list;
                    } else {
                        $scope.elements = {};
                    }
                }

            });
        };
        $scope.getElement($scope.currentPage);
        ///

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerElement = function (newPage) {
            $scope.getElement(newPage);
        };

        $scope.create_button = false;
        $scope.openElementModal = function (size) {
            $scope.create_button = true;
            var modalInstance = $modal.open({
                templateUrl: 'templates/element/elementModalContent.html',
                controller: 'ModalInstanceElementCtrl',
                size: size,
                backdrop: 'static',
                elementboard: false,
                resolve: {
                    modalFormText: function () {
                        return "You are trying to add an Element.";
                    },
                    modalFormTile: function () {
                        return "Add Element";
                    },
                    element: function () {
                        return '';
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.currentOrderByField = 'element_name';
                $scope.reverseSort = false;
                $scope.create_button = false;
                $scope.getElement($scope.currentPage);
            }, function () {
                $scope.create_button = false;
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.edit_button = false;
        $scope.openElementEditModal = function (size, element_id) {
            $scope.edit_button = true;
            elementService.getElementDetailsById($scope, element_id).then(function (data) {
                var modalInstance = $modal.open({
                    templateUrl: 'templates/element/elementModalContent.html',
                    controller: 'ModalInstanceElementCtrl',
                    size: size,
                    backdrop: 'static',
                    elementboard: false,
                    resolve: {
                        modalFormText: function () {
                            return "You are trying to Edit a Element.";
                        },
                        modalFormTile: function () {
                            return "Edit Element";
                        },
                        element: function () {
                            return data.data.element;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    $scope.currentOrderByField = 'element_name';
                    $scope.reverseSort = false;
                    $scope.edit_button = false;
                    $scope.getElement($scope.currentPage);
                }, function () {
                    $scope.edit_button = false;
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            });
        };
        //Delete element and there values
        $scope.deleteElement = function (element_id, current) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this element!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    elementService.delElement($scope, element_id).then(function (data) {
                        if (data.status == 200) {
                            if (current !== 'undefined') {
                                $scope.getElement(current);
                            } else {
                                $scope.getElement($scope.currentPage);
                            }

                            SweetAlert.swal("Success", "Element has been deleted", "success");
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                SweetAlert.swal("Cancelled", data.message, "warning");
                            }
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Element is safe :)", "error");
                }
            });
        };
        $scope.changeStatus = function (id) {
            elementService.changeStatus(id, $scope).then(function (data) {
                if (data.status == 200) {
                    $scope.getElement($scope.currentPage);
                } else {
                    if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                        SweetAlert.swal("Cancelled", data.message, "warning");
                    }
                }
            });
        };
        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.getElement($scope.currentPage);
        };
        // apply searching
        $scope.do_search = function () {
            $scope.getElement(1);
        };
        $scope.openElementUsageModal = function (size, element_id) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/element/elementUsageModalContent.html',
                controller: 'ModalInstanceElementMetadataCtrl',
                size: size,
                backdrop: 'static',
                elementboard: false,
                resolve: {
                    modalFormText: function () {
                        return "";
                    },
                    modalFormTile: function () {
                        return "Metadata Element Usage";
                    },
                    element_id: function () {
                        return element_id;
                    }
                }
            });
            modalInstance.result.then(function () {

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.element = [];
        /*
         *  Open value usage of a element
         */
        $scope.onChangeValue = function (size, element_id) {
            var value_id = $scope.element[element_id];
            // check usage is more than zero
            var usage_count = 0;
            angular.forEach($scope.elements, function (element_details, index) {
                if (element_details.id == element_id) {
                    var values = element_details.values;
                    angular.forEach(values, function (value, element) {
                        if (value.value_id == value_id) {
                            usage_count = value.usage_count;
                        }
                    });
                }
            });
            if (usage_count > 0) {
                var modalInstance = $modal.open({
                    templateUrl: 'templates/element/elementModalContent.html',
                    controller: 'ModalInstanceElementCtrl',
                    size: size,
                    backdrop: 'static',
                    elementboard: false,
                    resolve: {
                        modalFormText: function () {
                            return "";
                        },
                        modalFormTile: function () {
                            return "Metadata Value Usage";
                        },
                        element_id: function () {
                            return element_id;
                        },
                        value_id: function () {
                            return value_id;
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            }
        };

    }]);
/*
 * Create Element value Modal window Controller
 */
app.controller('ModalInstanceElementCtrl', function ($scope, $filter, elementService, $modalInstance, modalFormText, modalFormTile, element, $compile) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.elementModal = element;
    //Open element value add form
    $scope.element_name = '';
    $scope.element_type = 'text_entry';
    $scope.showAdd = true;
    $scope.showEdit = false;
    $scope.select_values = [];
    $scope.value = '';
    $scope.uses = 0;
    $scope.formError = {'element_name': '', 'value': '', 'error': ''};
    if (angular.isDefined(element.id) && element.id != '') {
        $scope.element_name = element.element_name;
        $scope.element_value = element.element_value;
        $scope.element_type = element.element_type;
        $scope.showAdd = false;
        $scope.showEdit = true;
        // populate values
        if ($scope.element_type == 'select_list')
        {
            angular.forEach(element.values, function (select_value) {
                $scope.select_values.push({"id": select_value.value_id, 'value': select_value.value, 'uses': select_value.usage_count});
            });
        }

    }
    
    // Add Element
    $scope.add = function () {
        $scope.formError = {'element_name': '', 'element_value': '', 'error': ''};
        if (angular.isUndefined($scope.element_name) == false) {
            elementService.addElement($scope).then(function (data) {
                if (data.status == 200) {
                    $modalInstance.close();
                } else {
                    if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                        $scope.formError['error'] = data.message;
                    }
                    if(angular.isDefined(data.error.element_name)){
                        $scope.formError['element_name'] = data.error.element_name['0'];
                    }
                    if(angular.isDefined(data.error.element_value)){
                        $scope.formError['element_value'] = data.error.element_value['0'];
                    }
                }
            });
        }
    };
    
     // Add Element
    $scope.edit = function () {
        $scope.formError = {'taxonomy_text': '', 'taxonomy_description': '', 'error': ''};
        if (angular.isUndefined($scope.element_name) == false) {
            elementService.editElement($scope, element).then(function (data) {
                if (data.status == 200) {
                    $modalInstance.close();
                } else {
                    if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                        $scope.formError['error'] = data.message;
                    }
                    if(angular.isDefined(data.error.element_name)){
                        $scope.formError['element_name'] = data.error.element_name['0'];
                    }
                    if(angular.isDefined(data.error.element_value)){
                        $scope.formError['element_value'] = data.error.element_value['0'];
                    }
                }
            });
        }
    };
    
    // Edit Element
    /*$scope.edit = function () {

        if (angular.isUndefined($scope.element_name) == false) {
            if ($scope.element_name == '')
            {
                $scope.formError['element_name'] = 'The element name field is required.';
            } else {
                $scope.formError['element_name'] = '';
                if (($scope.element_type == 'select_list' && document.querySelectorAll(".select-value").length) || $scope.element_type == 'text_entry')
                {
                    elementService.editElement($scope, element).then(function (data) {
                        if (data.status == 200) {
                            $modalInstance.close();
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                $scope.formError['error'] = data.message;
                            }
                            if (angular.isDefined(data.error.element_name)) {
                                $scope.formError['element_name'] = data.error.element_name['0'];
                            }
                        }
                    });
                } else {
                    $scope.formError['value'] = 'Please add select value.';
                }
            }
        }
    };*/
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
   
    $scope.editSelectValue = function (event, select_value) {
        var index = $scope.getSelectIndex(select_value);
        if (index === -1) {
            //alert( "Something gone wrong" );
        }

        var editEl = angular.element(event.target);
        var allSelectArray = document.querySelectorAll(".select-value");
        angular.forEach(allSelectArray, function (elem, elem_index) {
            if (elem_index == index)
            {
                if (editEl.hasClass('value-edit'))
                {
                    angular.element(elem).html('<input type="text" class="form-control" ng-model="select_value_' + index + '" /><span class="highlight text-danger">{{formError["select_value_' + index + '"]}}</span>');
                    $compile(elem)($scope);
                    $scope['select_value_' + index] = select_value;
                    editEl.html('Save');
                    editEl.removeClass('value-edit');
                    editEl.addClass('value-saved');
                } else {
                    // validate select value
                    var valid = true;
                    if ($scope['select_value_' + index] == '')
                    {
                        valid = false;
                    } else if ($scope['select_value_' + index].length > 255) {
                        valid = false;
                        $scope.formError['select_value_' + index] = 'Value name may not be greater than 255 characters.';
                    } else {
                        if ($scope.select_values.length) {
                            angular.forEach($scope.select_values, function (select_value, sel_index) {
                                if (select_value.value == $scope['select_value_' + index] && sel_index != index) // value is already exists
                                {
                                    valid = false;
                                }
                            });
                        }
                    }

                    if (valid)
                    {
                        $scope.select_values[index].value = $scope['select_value_' + index];
                        angular.element(elem).html($filter('strLimit')($scope['select_value_' + index], 40));
                        editEl.removeClass('value-saved');
                        editEl.addClass('value-edit');
                        editEl.html('Edit');
                    }
                }
            }
        });
    };
    $scope.removeSelectValue = function (select_value) {
        var index = $scope.getSelectIndex(select_value);
        if (index === -1) {
            //alert( "Something gone wrong" );
        }
        $scope.select_values.splice(index, 1);
    };
    $scope.getSelectIndex = function (select_value) {
        var index = -1;
        var valueArr = eval($scope.select_values);
        for (var i = 0; i < valueArr.length; i++) {
            if (valueArr[i].value === select_value) {
                index = i;
                break;
            }
        }
        return index;
    };

});
/*
 *  Element Metadata Modal window Controller
 */
app.controller('ModalInstanceElementMetadataCtrl', function ($scope, elementService, $modalInstance, modalFormText, modalFormTile, element_id) {
    $scope.element_id = element_id;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.elementModel = {};
    $scope.elementMetadataObjects = {};
    $scope.currentPage = 1;
    $scope.pageNumber = '';
    $scope.itemsPerPage = '';
    //Open element metadata usage
    $scope.element_title = '';
    $scope.element_type = '';
    $scope.data = {};

    $scope.showPagination = false;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    // get element
    $scope.getElements = function () {
        elementService.getMetadataElementUsage($scope, element_id).then(function (data) {
            if (data.data == null || data.data == '') {
                $scope.total = 0;
                $scope.elementModel = {};
                $scope.elementMetadataObjects = {};
            } else {
                var element = data.data.element;
                $scope.elementModel = element;
                $scope.data.valueModel = data.data.values;
                if (angular.isDefined(element.element_type) && element.element_type != '') {
                    $scope.element_type = element.element_type;
                }
                if (angular.isDefined(element.id) && element.id != '') {
                    $scope.element_title = element.element_name;
                }
            }
        });
    };
    $scope.getElements();

    $scope.getObjects = function (pageNumber) {
        $scope.currentPage = pageNumber;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = '10';
        elementService.getMetadataElementUsage($scope, element_id).then(function (data) {
            if (data.data == null || data.data == '') {
                $scope.total = 0;
                $scope.elementModel = {};
                $scope.elementMetadataObjects = {};
            } else {
                $scope.total = data.data.total;
                if (data.data.total > data.data.totalContents) {
                    $scope.showPagination = true;
                }
                $scope.elementMetadataObjects = data.data.metadata_objects;
            }

        });
    };
    $scope.getObjects(1);

    $scope.pageChangeHandlerObject = function (pageNumber) {
        $scope.getObjects(pageNumber);
    };

    $scope.onChangeValue = function () {
        $scope.getObjects(1);
    };

});