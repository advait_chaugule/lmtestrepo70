/*****
 * Services for ElementServices
 * Author - Parveen Khan
 *  
 */
'use strict';

//Start of aclService
app.service('elementService', ['$http', '$q', 'sessionService', '$rootScope', function ($http, $q, sessionService, $rootScope) {

        /*
         * Service to add Element
         */
        this.addElement = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/content-elements?access_token=' + sessionService.get('access_token'), {
                element_name: $scope.element_name,
                element_value: $scope.element_value,
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to add Element
         */
        this.editElement = function ($scope, element) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/content-elements/element_id/' + element.id + '?access_token=' + sessionService.get('access_token'), {
                element_name: $scope.element_name,
                element_value: $scope.element_value,
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to add Element
         */
        this.delElement = function ($scope, element_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/content-elements/element_id/' + element_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to asset element mapping
         */
        /*this.elementObjectMapping = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/element-object?access_token=' + sessionService.get('access_token'), {
                element_id: $scope.element_id,
                asset_ids: $scope.asset_ids
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }*/

        
         /**
         * Get Elements
         */
        this.getElement = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }

            $http.get(serviceEndPoint + 'api/v1/content-elements?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
        /**
         * Get Element Detail By Id
         */
        this.getElementDetailsById = function ($scope, element_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/content-elements/element_id/' + element_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /**
         * Get Element By Object Id
         */
        this.getElementByObject = function ($scope, object_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/content-elements/element_id/' + element_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /**
         * Get Element By Object Id
         */
        this.rearrangeElementTree = function ($scope, element_id, parent_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var requestUrl = serviceEndPoint + 'api/v1/element-rearrange/element_id/' + element_id + '/parent_id/' + parent_id + '?access_token=' + sessionService.get('access_token');
            var requestData = {'children': JSON.stringify($scope.element.children)};
            var requestConfig = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
            $http.put(requestUrl, requestData, requestConfig).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getMetadataUsage = function ($scope, element_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            $http.get(serviceEndPoint + 'api/v1/objects-element-usage/element_id/' + element_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/content-elements-status/element_id/' + id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);