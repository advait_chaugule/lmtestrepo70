/*****
 * Services for Session Management
 * Author - Arikh Akher
 */


'use strict';

app.service('sessionService', ['$http', function($http) {

        this.set = function(key, value) {

            return localStorage.setItem(key, value);
        };
        this.get = function(key) {
            // debugger;
            return localStorage.getItem(key);
        };
        this.destroy = function(key) {
            //$http.post('data/destroy_session.php');
            return localStorage.removeItem(key);
        };

    }])
