'use strict';

var guid = (function (separator) {
    var separator = angular.isUndefined(separator) ? '' : separator;
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    function s4_alpha() {
        var max = 122, min = 97;
        var return_str = "";
        for (var i = 0; i <= 3; i++) {
            var rand = Math.floor(Math.random() * (max - min + 1)) + min
            return_str += String.fromCharCode(rand);
        }
        return return_str;
    }
    return function () {
        return s4_alpha() + s4() + separator + s4() + separator + s4() + separator + s4() + separator + s4() + s4() + s4();
    };
})();
var small_guid = (function (separator) {
    var separator = angular.isUndefined(separator) ? '' : separator;
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    function s4_alpha() {
        var max = 122, min = 97;
        var return_str = "";
        for (var i = 0; i <= 3; i++) {
            var rand = Math.floor(Math.random() * (max - min + 1)) + min
            return_str += String.fromCharCode(rand);
        }
        return return_str;
    }
    return function () {
        return s4();
    };
})();

var getNestedChildren = function (arr, parent_id) {
    var output = []
    for (var i in arr) {
        if (arr[i].parent_id == parent_id) {
            var nodes = getNestedChildren(arr, arr[i].id);
            if (nodes.length) {
                arr[i].nodes = nodes;
            }
            output.push(arr[i]);
        }
    }
    return output;
};

app.service('utilityService', ['$http', '$q', '$rootScope', '$location', 'SweetAlert', function ($http, $q, $rootScope, $location, SweetAlert) {
        //Redirect to login page, if Oauth expires 
        $rootScope.unauthorised_redirection = function (status) {
            if (status == 500) {
                $location.path('logout');
            }
        };

        $rootScope.move_unauthorised_user_to_toc_with_message = function (status, project_id) {
            if (status == 401) {
                if (project_id) {
                    $location.path("edit_toc/" + project_id);
                } else {
                    SweetAlert.swal("Cancelled", "Access prohibited!", "error");
                    $location.path("dashboard");
                }
            }
        };

        $rootScope.get_status_name = function (id) {
            var status_name = '';
            switch (id) {
                case '1000':
                    status_name = 'No';
                    break;
                case '1001':
                    status_name = 'Yes';
                    break;
                default:
                    status_name = 'No';
            }
            return status_name;
        }



    }]);
