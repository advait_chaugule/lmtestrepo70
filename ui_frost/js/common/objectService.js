/*****
 * Generic Object Services
 * Author - Arikh Akher * 
 */
'use strict';

app.service('objectService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        this.list = function ($scope) {
            var params = {};
            var user_id = 0;
            var search_text='';
            //params.access_token = sessionService.get('access_token');
            params.pageNumber = $scope.pageNumber;
            params.itemsPerPage = $scope.itemsPerPage;
            if (angular.isDefined($scope.search_text)) {
            //     var customEncodeURIComponent = function (tURL) {
            //         return encodeURIComponent(tURL).replace(/[!'()]/g, escape).replace("_", "%5F"); 
            //   };
                search_text = encodeURIComponent($scope.search_text);
                // search_text = $scope.search_text;
                console.log(search_text);
            }
            //debugger;
            var userinfo = JSON.parse(sessionService.get('userinfo'));
            if (userinfo != null) {
                user_id = userinfo.user_id;
            }
            
            var deferred = $q.defer();
            var api = 'api/v1/getProjectList';
            if(search_text){
                api ='api/v1/searchProject?search_key='+search_text;
            }
            
            $http({
                method: 'GET',
                url: serviceEndPoint + api,
                headers:  {
                    'Authorization': sessionService.get('access_token'),
                    'Accept': 'application/json'
                },
                //params: params
            }).success(function (res, status, headers, config) {
                deferred.resolve(res.data);//Return the project list
            }).error(function (res, status, headers, config) {
                //console.info("Object was not called properly.." + data);
                $rootScope.unauthorised_redirection(data.status);

            });
            return deferred.promise;

        };

        // Start -- Method to create project
        this.save = function (formdata, $scope) {
            var userinfo = JSON.parse(sessionService.get('userinfo'));
            if (userinfo != null) {
                formdata.user_id = userinfo.id;
            }
            var deferred = $q.defer();
            formdata.access_token = sessionService.get('access_token');
            formdata.project_type_id = formdata.project_type;
            formdata.object_id = $scope.object_id;

            var fd = new FormData();
            fd.append('package_file', $scope.formdata.packageFile);
            fd.append('user_id', formdata.user_id);
            fd.append('project_type_id', $scope.formdata.project_type.id);
            fd.append('project_name', $scope.formdata.project_name);
            fd.append('description', $scope.formdata.description);
            fd.append('project_isbn', $scope.formdata.project_isbn);
            fd.append('project_discipline', (angular.isUndefined($scope.formdata.project_discipline) ? '' : $scope.formdata.project_discipline));
            fd.append('project_author', $scope.formdata.project_author);
            fd.append('publication_date', $scope.formdata.publication_date);
            fd.append('users', JSON.stringify($scope.users));

            //fd.append('theme_id', '');
            //console.log($scope.formdata);   
            if ($scope.formdata.project_type.id == 1) {
                fd.append('theme_id', 1);
            } else {
                if (!angular.isUndefined($scope.formdata.theme)) {
                    if ($scope.formdata.theme != null) {
                        fd.append('theme_id', $scope.formdata.theme.id);
                    }
                }
            }

            fd.append('project_cover', $scope.formdata.project_cover);
            fd.append('project_cover_thumbnail', $scope.formdata.project_cover_thumbnail);
            var apiUrl = serviceEndPoint + 'api/v1/project' + '?access_token=' + formdata.access_token;
            if ($scope.project_id) {
                fd.append('project_id', $scope.project_id);
                fd.append('_method', 'PUT');
                apiUrl = serviceEndPoint + 'api/v1/project/project_id/' + $scope.project_id + '?access_token=' + formdata.access_token;
            }

            $http({
                method: 'POST',
                url: apiUrl,
                data: fd,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
                // headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}  // set the headers so angular passing info as form data (not request payload)
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to create project


        //Start - Method to get object types
        this.objecttypes = function () {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/object-types',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);

            });
            return deferred.promise;

        };
        //End - Method to get object types

        //Start - Method to get object types
        this.getProjectDetails = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/projects/project_id/' + $scope.project_id,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data.data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        //End - Method to get object types


        /**
         * Get Searched result of Assets
         */
        this.getSearchedObjects = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/object-search?access_token=' + sessionService.get('access_token'), {
                search_text: $scope.search_text
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /*
         * Update TOC Created Status
         */
        this.create_toc = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-toc-create/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };


        this.get_version_list = function ($scope) {
            var params = {};
            access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/version-all/project_id/' + $scope.pid + "?access_token=" + access_token + "&object_id=" + $scope.oid,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        this.getTocObjectStatus = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.object_id)) {
                queryString += "&object_id=" + $scope.object_id;
                queryString += "&editor_flag_on=" + $scope.flag_on;
                queryString += "&editor_flag_off=" + $scope.flag_off;
                queryString += "&user_id=" + $scope.user_id;
            } else {
                queryString += "&project_id=" + $scope.project_id;
            }
            $http.get(serviceEndPoint + 'api/v1/change-status?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.releaseLockStatusAuto = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.project_id)) {
                queryString += "&project_id=" + $scope.project_id;
            }
            $http.get(serviceEndPoint + 'api/v1/release-lock?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.getObjectDetailsById = function (id) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/object-details/' + id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

        this.getObjectDetailsByFilename = function ($scope, project_id, file_name) {
            var ts = Math.round(new Date().getTime() / 1000);
            var params = {};
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/toc-content-preview/project_id/' + project_id + '?access_token=' + sessionService.get('access_token') + '&file_name=' + file_name + '&_ts=' + ts,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Get list of global contents
         */
        this.getGlobalContents = function ($scope) {
            var params = {};
            params.pageNumber = $scope.pageNumber;
            params.itemsPerPage = $scope.itemsPerPage;
            params.project_type_id = $scope.project_type_id;
            params.access_token = sessionService.get('access_token');
            if (angular.isDefined($scope.search_text)) {
                params.search_text = window.btoa($scope.search_text);
            }
            // for advance search
            if (angular.isDefined($scope.advanced_search) && $scope.advanced_search.active) {
                params.advanced_search_enable = $scope.advanced_search.active;
                params.tags = JSON.stringify($scope.advanced_search.tagValue);
                params.owners = JSON.stringify($scope.advanced_search.ownerValue);
                params.key_data = JSON.stringify($scope.advanced_search.key_data);
                params.value_data = JSON.stringify($scope.advanced_search.value_data);
                params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
                params.advanced_search_title = window.btoa($scope.advanced_search.title);
                var deferred = $q.defer();
                $http.post(serviceEndPoint + 'api/v1/global-content-object-search/project_id/' + $scope.project_id, params)
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);//Return the project list
                        }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/global-content-object/project_id/' + $scope.project_id,
                    params: params
                }).success(function (data, status, headers, config) {
                    deferred.resolve(data);//Return the project list
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            }

            return deferred.promise;
        };

        /*
         * Get project ticket count
         */
        this.getProjectTicketCount = function ($scope) {
            var params = {};
            params.project_ids = JSON.stringify($scope.project_ids);
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-ticket-count',
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
        
        /*
         * Get global project ID
         */
        this.getGlobalProjectId = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/global_project/' + $scope.project_type_id,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data.data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };

        this.isReplicatedContent=function ($scope){
            alert("in service"+ $scope.object_id);
            return false;
//            var deferred = $q.defer();
//            $http({
//                method: 'GET',
//                url: serviceEndPoint + 'api/v1/is-replicated-content/object_id/'+$scope.object_id+ '?access_token=' + sessionService.get('access_token') ,
//            }).success(function (data, status, headers, config) {
//                deferred.resolve(data);//Return isReplicatedContent for particular content_object
//            }).error(function (data, status, headers, config) {
//                $rootScope.unauthorised_redirection(data.status);
//            });
//            return deferred.promise;
        };
    }]);

