'use strict';

//Start of token generation
app.factory('tokenService', ['$http', '$q', function ($http, $q) {
        var generate = function () {
            var deferred = $q.defer();
            /*$http.get(oauthUrl, {
             }).success(function (data, status, headers, config) {                
             deferred.resolve(data);   
             }).error(function (data, status, headers, config) {
             console.info("Token service was not called properly.." + data);
             });*/
            $http.post(oauthUrl, {
                client_id: oauth_client_id,
                client_secret: oauth_client_secret,
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                 console.info("Token service was not called properly.." + data);
            });
            
            return deferred.promise;
        };
        return {generate: generate};
    }]);
