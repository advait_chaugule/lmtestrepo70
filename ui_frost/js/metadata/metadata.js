'use strict';

app.controller('metadata', ['$scope', '$location', '$http', 'sessionService', '$rootScope', function ($scope, $location, $http, sessionService, $rootScope) {
        $scope.access_token = sessionService.get('access_token');
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));

        // if (!$scope.user_permissions['key_value.show.all'].grant && !$scope.user_permissions['tag.show.all'].grant && !$scope.user_permissions['taxonomy.show.all'].grant) {
        //     $rootScope.goto('dashboard');
        // }

    }]);
