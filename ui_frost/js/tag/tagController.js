/*****
 * Controller for tag
 * Author - Alamgir Hossain Sk *
 */

'use strict';
app.controller('tag', ['$scope', '$http', 'sessionService', '$location', 'tagService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', function ($scope, $http, sessionService, $location, tagService, $rootScope, $modal, $log, SweetAlert, $routeParams) {

    $scope.access_token = sessionService.get('access_token');
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    var userinfo = JSON.parse(sessionService.get('userinfo'));
    $scope.project_id = $routeParams.project_id;
    if (!$scope.access_token || userinfo == null) {
      $location.path('logout');
      return true;
    }

    $scope.currentOrderByField = 'title';
    $scope.reverseSort = false;
    $scope.search_text = '';
    //Call Tag
    $scope.currentPage = 1;
    $scope.pageNumber = 1;
    $scope.itemsPerPage = '';
    $scope.tags = {};
    $scope.getTags = function (pageNumber) {
      $scope.pageNumber = pageNumber;
      $scope.currentPage = pageNumber;
      $scope.itemsPerPage = '10';
      tagService.getTags($scope).then(function (data) {
        if (data.status == 200) {
          $scope.total = data.total;
          if (data.data !== null) {
            $scope.tags = data.data.list;
          } else {
            $scope.tags = {};
          }
        }
      });
    };
    $scope.getTags($scope.currentPage);
    ///

    //For Pagination Of Dashboard
    $scope.pageChangeHandlerTag = function (newPage) {
      $scope.getTags(newPage);
    };

    $scope.create_button = false;
    $scope.openTagAddModal = function (size) {
      $scope.create_button = true;
      var modalInstance = $modal.open({
        templateUrl: 'templates/tag/tagModalContent.html',
        controller: 'ModalInstanceTagCtrl',
        size: size,
        backdrop: 'static',
        keyboard: false,
        resolve: {
          modalFormText: function () {
            return "You are trying to add a Tag. Comma seperated values are acceptable.";
          },
          modalFormTile: function () {
            return "Add Tag";
          },
          tag: function () {
            return '';
          }
        }
      });
      modalInstance.result.then(function () {
        $scope.currentOrderByField = 'title';
        $scope.reverseSort = false;
        $scope.getTags($scope.currentPage);
        $scope.create_button = false;
      }, function () {
        $scope.create_button = false;
        //$log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.edit_button = false;
    $scope.openTagEditModal = function (size, tag_id) {
      $scope.edit_button = true;
      tagService.getTagDetailsById($scope, tag_id).then(function (data) {
        var modalInstance = $modal.open({
          templateUrl: 'templates/tag/tagModalContent.html',
          controller: 'ModalInstanceTagCtrl',
          size: size,
          backdrop: 'static',
          keyboard: false,
          resolve: {
            modalFormText: function () {
              return "You are trying to Edit a Tag.";
            },
            modalFormTile: function () {
              return "Edit Tag";
            },
            tag: function () {
              return data.data.tag;
            }
          }
        });
        modalInstance.result.then(function () {
          $scope.currentOrderByField = 'title';
          $scope.reverseSort = false;
          $scope.getTags($scope.currentPage);
          $scope.edit_button = false;
        }, function () {
          $scope.edit_button = false;
          //$log.info('Modal dismissed at: ' + new Date());
        });
      });
    };
    //Delete Tag and their children
    $scope.deleteTag = function (tag_id, current) {
      SweetAlert.swal({
        title: "Are you sure?",
        text: "You will not be able to recover this tag!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          tagService.delTag($scope, tag_id).then(function (data) {
            if (data.status == 200) {
              if (current !== 'undefined') {
                $scope.getTags(current);
              } else {
                $scope.getTags($scope.currentPage);
              }
              SweetAlert.swal("Success", "Tag has been deleted", "success");
            } else {
              if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                SweetAlert.swal("Cancelled", data.message, "warning");
              }
            }
          });
        } else {
          SweetAlert.swal("Cancelled", "Tag is safe :)", "error");
        }
      });
    };
    $scope.applySorting = function (field) {
      if ($scope.currentOrderByField != field) {
        $scope.reverseSort = false;
      } else {
        if ($scope.reverseSort) {
          $scope.reverseSort = false;
        } else {
          $scope.reverseSort = true;
        }
      }
      $scope.currentOrderByField = field;
      $scope.getTags($scope.currentPage);
    };
    // apply searching
    $scope.do_search = function () {
      $scope.getTags(1);
    };
    $scope.openTagUsageModal = function (size, tag_id) {
      //tagService.getMetadataUsage($scope, tag_id).then(function (data) {
      var modalInstance = $modal.open({
        templateUrl: 'templates/tag/usageModalContent.html',
        controller: 'ModalInstanceTagMetadataCtrl',
        size: size,
        backdrop: 'static',
        keyboard: false,
        resolve: {
          modalFormText: function () {
            return "";
          },
          modalFormTile: function () {
            return "Metadata Tag Usage";
          },
          tag_id: function () {
            return tag_id;
          },
          user_permissions: function(){
            return $scope.user_permissions;
          }
        }
      });
      modalInstance.result.then(function () {
        //$scope.getTags(1);
      }, function () {
        //$log.info('Modal dismissed at: ' + new Date());
      });
      //});
    };

    $scope.changeStatus = function (id) {
      tagService.changeStatus(id, $scope).then(function (data) {
        if (data.status == 200) {
          $scope.getTags($scope.currentPage);
        } else {
          /*if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
           SweetAlert.swal("Cancelled", data.message, "warning");
           }*/
        }
      });
    };
  }]);
/*
 * Create Tag Modal window Controller
 */
app.controller('ModalInstanceTagCtrl', function ($scope, assetService, tagService, $modalInstance, modalFormText, modalFormTile, tag) {
  $scope.showedit = 0;
  $scope.modalFormText = modalFormText;
  $scope.modalFormTile = modalFormTile;
  $scope.tagModal = tag;
  //Open Tag add form
  $scope.tag_title = '';
  $scope.tag_description = '';
  $scope.showAdd = true;
  $scope.showEdit = false;
  if (angular.isDefined(tag.id) && tag.id != '') {
    $scope.tag_title = tag.title;
    $scope.tag_description = tag.description;
    $scope.showAdd = false;
    $scope.showEdit = true;
  }

  // Add Tag
  $scope.add = function () {
    $scope.formError = {'tag_title': '', 'tag_description': '', 'error': ''};
    if (angular.isUndefined($scope.tag_title) == false) {
      tagService.addTag($scope).then(function (data) {
        if (data.status == 200) {
          $modalInstance.close();
        } else {
          if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
            $scope.formError['error'] = data.message;
          }
          if (angular.isDefined(data.error.title)) {
            $scope.formError['tag_title'] = data.error.title['0'];
          }
        }
      });
    }
  };
  // Edit Tag
  $scope.edit = function () {
    $scope.formError = {'tag_title': '', 'tag_description': '', 'error': ''};
    if (angular.isUndefined($scope.tag_title) == false) {
      tagService.editTag($scope, tag).then(function (data) {
        if (data.status == 200) {
          $modalInstance.close();
        } else {
          if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
            $scope.formError['error'] = data.message;
          }
          if (angular.isDefined(data.error.title)) {
            $scope.formError['tag_title'] = data.error.title['0'];
          }
        }
      });
    }
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
/*
 *  Tag Metadata Modal window Controller
 */
app.controller('ModalInstanceTagMetadataCtrl', function ($scope,$rootScope,$location,$modal,$log,assetService,objectService,SweetAlert,tagService, $modalInstance, modalFormText, modalFormTile, tag_id,user_permissions) {
  $scope.total = '';
  $scope.modalFormText = modalFormText;
  $scope.modalFormTile = modalFormTile;
  $scope.user_permissions=user_permissions;
  $scope.tagModal = {};
  $scope.tagMetadataObjects = {};
  $scope.currentPage = 1;
  $scope.pageNumber = '';
  $scope.itemsPerPage = '';
  //Open tag metadata usage
  $scope.tag_title = '';
  $scope.tag_description = '';
  $scope.showPagination = false;
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.getObjects = function (pageNumber) {
    $scope.currentPage = pageNumber;
    $scope.pageNumber = pageNumber;
    $scope.itemsPerPage = '10';
    tagService.getMetadataUsage($scope, tag_id).then(function (data) {
      if (data.data.tag == null || data.data.tag == '') {
        $scope.total = 0;
        $scope.tagModal = {};
        $scope.tagMetadataObjects = {};
      } else {
        var tag = data.data.tag;
        if (angular.isDefined(tag.id) && tag.id != '') {
          $scope.tag_title = tag.title;
          $scope.tag_description = tag.description;
        }
        $scope.total = data.data.total;
        if (data.data.total > data.data.totalContents) {
          $scope.showPagination = true;
        }
        $scope.tagMetadataObjects = data.data.metadata_objects;
      }

    });
  };

  $scope.getObjects(1);

  $scope.pageChangeHandlerObject = function (pageNumber) {
    $scope.getObjects(pageNumber);
  };
  
$scope.redirctToProject=function(object_id, toc_creation_status,usage_type){
        switch (usage_type) {
            case 'ASSESSMENT':
                $scope.checkProjectExists(object_id);
                break;
            case 'OBJECT':
                if (parseInt(toc_creation_status) == 3001) {
                    $scope.checkProjectExists(object_id);
                } else {
                    $rootScope.show_toc = 0;
                    $location.path('project_import/' + object_id);
                }
                break;
            default:
        }                
    };
    $scope.checkProjectExists=function(object_id){
        $scope.project_id = object_id;
        objectService.getProjectDetails($scope).then(function (data) {
            if (data != null) {
                $rootScope.show_toc = 1;
                $location.path("edit_toc/" + object_id);
            }else{
                $rootScope.show_toc = 0;
                SweetAlert.swal({html: 'Either the project does not exist or you are no longer allocated to this project.', width: 540, type: "warning"});
            } 
        });
    };
   
   $scope.preview_button_disabled = false;
        $scope.previewAssessment = function (preview_info) {
            $scope.preview_button_disabled = true;
            var size = 'lg';
            var modal_form_title = 'Assessment Preview';
            var previewInfo=JSON.parse(preview_info);
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/previewModal.html',
                controller: 'ModalPreviewCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    shortcode: function () {
                        return previewInfo.shortcode;
                    },
                    project_id: function () {
                        return previewInfo.project_id;
                    },
                    project_type_id: function () {
                        return previewInfo.project_type_id;
                    },
                    entity_type_id: function () {
                        return 2;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.preview_button_disabled = false;
            }, function () {
                $scope.preview_button_disabled = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    
    $scope.previewAsset = function (size, preview_info) {
            $scope.applyMetadata=0;
            var preview_infos=JSON.parse(preview_info);
            var modalInstance = $modal.open({
                templateUrl: 'templates/asset/assetModalContent.html',
                controller: 'ModalAssetCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return preview_infos.modal_form_title;
                    },
                    object_id: function () {
                        return preview_infos.object_id;
                    },
                    project_id: function () {
                        return preview_infos.project_id;
                    },
                    displayOption: function () {
                        return false;
                    },
                    user_permissions: function () {
                        return $scope.user_permissions;
                    },
                    project_permissions: function () {
                        return true;
                    }
                }
            });
        };
        
   $scope.previewContent=function (size, id) {
            var contentRec = {};
            $scope.id = id;
            assetService.getGlobalContentDetails($scope).then(function (data) {
                contentRec = data.data;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/toc/globalModalContentView.html',
                    controller: 'ModalInstanceContentCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        contentRec: function () {
                            return contentRec;
                        },                        
                        object_id: function () {
                            return id;
                        },
                        modalFormText: function () {
                            return "You are trying to view a content";
                        },
                        modalFormTile: function () {
                            return "Preview Content";
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };
});


