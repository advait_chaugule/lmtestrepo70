/*****
 * Services for TagServices
 * Author - Koushik Samanta 
 *  
 */
'use strict';

//Start of aclService
app.service('tagService', ['$http', '$q', 'sessionService', '$rootScope', function ($http, $q, sessionService, $rootScope) {

        /*
         * Service to add Tag
         */
        this.addTag = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/tags?access_token=' + sessionService.get('access_token'), {
                title: $scope.tag_title,
                description: $scope.tag_description
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to add Tag
         */
        this.editTag = function ($scope, tag) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/tags/tag_id/' + tag.id + '?access_token=' + sessionService.get('access_token'), {
                title: $scope.tag_title,
                description: $scope.tag_description,
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to add Tag
         */
        this.delTag = function ($scope, tag_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/tags/tag_id/' + tag_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Service to asset tag mapping
         */
        this.tagObjectMapping = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/tag-object-mapping?access_token=' + sessionService.get('access_token'), {
                tag_id: $scope.tag_id,
                asset_ids: $scope.asset_ids
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        /**
         * Get Mime Types
         */
        this.getTags = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }

            $http.get(serviceEndPoint + 'api/v1/tags?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /**
         * Get Tag Detail By Id
         */
        this.getTagDetailsById = function ($scope, tag_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/tags/tag_id/' + tag_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getTagsByObject = function ($scope, object_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/tag-object/' + object_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getMetadataUsage = function ($scope, tag_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            $http.get(serviceEndPoint + 'api/v1/objects-tag-usage/tag_id/' + tag_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/tag-status/tag_id/' + id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);