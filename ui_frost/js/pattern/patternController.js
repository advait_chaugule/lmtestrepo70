/*****
 * Controller for project
 * Author - Arikh Akher * 
 */

'use strict';
app.controller('pattern', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'patternService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, patternService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.currentOrderByField = 'title';
        $scope.reverseSort = false;
        $scope.project_id = $routeParams.project_id;
        $scope.searchModel = {};

        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.pattern_local_global = {};
        $scope.pattern_local_global.local = false;
        $scope.pattern_local_global.global = false;

        if ($scope.user_permissions['global_pattern.show.all'].grant) {
            $scope.pattern_local_global.global = true;
        }

        $scope.pageNumber = 1;
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;
        $scope.listPatterns = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            patternService.getPattern($scope).then(function (data) {
                $scope.patternList = data.data;
                $scope.totalPatterns = data.totalPatterns;
            });
        };

        //$scope.listPatterns(1);

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerPattern = function (newPage) {
            $scope.pattern_id = '';
            $scope.listPatterns(newPage);
        };

        $scope.addPatternForm = function (size) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/pattern/myModalContent.html',
                controller: 'ModalInstanceAddCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    patternRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a new pattern";
                    },
                    modalFormTile: function () {
                        return "Add Pattern";
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_theme: function () {
                        return $scope.project.theme;
                    }
                }
            });
            modalInstance.result.then(function () {
                //Saving TOC    
                $scope.pattern_id = '';
                patternService.getPattern($scope).then(function (data) {
                    $scope.patternList = data.data;
                    $scope.totalPatterns = data.totalPatterns;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.edit_button = false;
        $scope.editPattern = function (size, id) {
            $scope.edit_button = true;
            var patternRec = {};
            $scope.pattern_id = id;
            patternService.getPattern($scope).then(function (data) {
                patternRec = data.data;
                var patternTitle = patternRec[0].title;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/pattern/myModalContent.html',
                    controller: 'ModalInstanceAddCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        patternRec: function () {
                            return patternRec;
                        },
                        modalFormText: function () {
                            return "You are trying to edit pattern [" + $filter('strLimit')(patternTitle, 20) + "]";
                        },
                        modalFormTile: function () {
                            return "Edit Pattern";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    //Saving TOC    
                    $scope.pattern_id = '';
                    $scope.edit_button = false;
                    patternService.getPattern($scope).then(function (data) {
                        $scope.patternList = data.data;
                        $scope.totalPatterns = data.totalPatterns;
                    });
                }, function () {
                    $scope.edit_button = false;
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.preview_button = false;
        $scope.viewPattern = function (size, id) {
            $scope.preview_button = true;
            var patternRec = {};
            $scope.pattern_id = id;
            patternService.getPattern($scope).then(function (data) {
                patternRec = data.data;

                var modalInstance = $modal.open({
                    templateUrl: 'templates/pattern/myModalContentView.html',
                    controller: 'ModalInstanceAddCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        patternRec: function () {
                            return patternRec;
                        },
                        modalFormText: function () {
                            return "You are trying to view a pattern";
                        },
                        modalFormTile: function () {
                            return "Preview Pattern";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        }
                    }
                });

                modalInstance.result.then(function () {

                }, function () {
                    $scope.preview_button = false;
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };


        $scope.deletePattern = function (id, project_id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this pattern!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    patternService.delPattern(id, project_id).then(function (data) {
                        if (data.status == 200) {
                            $scope.pattern_id = '';
                            $scope.listPatterns(1);
                            SweetAlert.swal("Success", "Pattern has been deleted", "success");
                        } else {
                            SweetAlert.swal({
                                title: "Cancelled",
                                html: data.message,
                                type: "error",
                            });
                        }
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Pattern is safe :)", "error");
                }
            });

        };

        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        }
        $scope.applyPermission = 0;
        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.toc_creation_status = data.toc_creation_status;
                        $scope.project = data;//{};
                        $scope.applyPermission = 1;
                        $scope.project.project_permissions = data.project_permissions;
                        $scope.project.theme = data.theme_location;
                        if ($scope.project.project_permissions['pattern.show.all'].grant) {
                            $scope.pattern_local_global.local = true;
                        }
                        $scope.listPatterns(1);
                    }
                });
            }
        }
        $scope.getProject();

        $scope.changeStatus = function (pattern_id, project_id) {
            $scope.project_id = project_id;
            patternService.changeStatus(pattern_id, $scope).then(function () {
                $scope.pattern_id = '';
            });
        }

        $scope.apply_filter = function () {
            $scope.pattern_id = '';
            if ($scope.pattern_local_global.global == false && $scope.pattern_local_global.local == false) {
                //$scope.pattern_local_global.local = true;
                //$scope.pattern_local_global.global = true;
            }
            $scope.listPatterns(1);
        }

        $scope.changeAccess = function (id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: _lang['content_global_confirm_message'],
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    patternService.makeGlobal(id, $scope).then(function (data) {
                        if (data.status == 200) {
                            $scope.listPatterns(1);
                            SweetAlert.swal("Success", "Pattern access changed successfully.", "success");
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                SweetAlert.swal("Cancelled", data.message, "warning");
                            }
                        }
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Pattern is safe :)", "error");
                }
            });
        };


        $scope.applySorting = function (field) {
            $scope.pattern_id = '';
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.listPatterns(1);
        };

        // apply searching
        $scope.do_search = function () {
            $scope.pattern_id = '';
            $scope.listPatterns(1);

        };

    }]);


app.controller('ModalInstanceAddCtrl', function ($scope, patternService, sessionService, $modalInstance, modalFormText, modalFormTile, patternRec, project_id, project_permissions, project_theme) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.project = {'project_permissions': project_permissions};
    $scope.pattern_classes = [{value: ''}];
    $scope.project_theme = project_theme;

    if (patternRec.length) {

//        var patternContent = patternRec[0].pattern.replace(/..\/images/g, 'images');
        $scope.pattern_id = patternRec[0].id;
        $scope.pattern_title = patternRec[0].title;
        $scope.pattern_content = patternRec[0].pattern;
        $scope.pattern_classes = JSON.parse(patternRec[0].pattern_classes);
        //$scope.pattern_classes = $scope.pattern_classes.all;
        //$scope.project_permissions['pattern.update'].grant
        //console.log($scope.project.project_permissions);
        $scope.showedit = patternRec.length;

        /*$scope.$watch('pattern_classes', function(newValue, oldValue) {
         $scope.pattern_classes = newValue;
         });*/

    } else {
        $scope.showedit = 0;
    }

    $scope.add = function () {
        $scope.formError = {'pattern_title': '', 'pattern_content': ''};
        //if (angular.isUndefined($scope.pattern_title) == false && angular.isUndefined($scope.pattern_content) == false) {
        patternService.savePattern($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                patternService.getPattern($scope).then(function (data) {
                    $scope.patternList = data.data;
                    $modalInstance.close();
                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['pattern_title'] = savedResponse.error.title['0'];
                }
                if (angular.isDefined(savedResponse.error.pattern)) {
                    $scope.formError['pattern_content'] = savedResponse.error.pattern['0'];
                }

            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'pattern_title': '', 'pattern_content': ''};
        //if (angular.isUndefined($scope.pattern_title) == false && angular.isUndefined($scope.pattern_content) == false) {
        patternService.editPattern($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                patternService.getPattern($scope).then(function (data) {
                    $scope.patternList = data.data;
                    $modalInstance.close();
                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['pattern_title'] = savedResponse.error.title['0'];
                }
                if (angular.isDefined(savedResponse.error.pattern)) {
                    $scope.formError['pattern_content'] = savedResponse.error.pattern['0'];
                }

            }


        });
        //}
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    //Manage pattern_classes    
    $scope.addClass = function () {
        $scope.pattern_classes.push({value: ''});
    };
    $scope.removeClass = function (index) {
        // console.log(index);
        $scope.pattern_classes.splice(index, 1);
    };
    //

});

app.controller('global_pattern', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'patternService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, patternService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.currentOrderByField = 'title';
        $scope.reverseSort = false;
        $scope.formdata = {};
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.pattern_local_global = {};
        $scope.pattern_local_global.local = false;
        $scope.pattern_local_global.global = false;

        if ($scope.user_permissions['global_pattern.show.all'].grant) {
            $scope.pattern_local_global.global = true;
        }

        $scope.pageNumber = 1;
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;
        $scope.listPatterns = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            patternService.getPattern($scope).then(function (data) {
                $scope.patternList = data.data;
                $scope.totalPatterns = data.totalPatterns;
            });
        };

        $scope.listPatterns(1);

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerPattern = function (newPage) {
            $scope.pattern_id = '';
            $scope.listPatterns(newPage);
        };

        $scope.preview_button = false;
        $scope.viewPattern = function (size, id) {
            $scope.preview_button = true;
            var patternRec = {};
            $scope.pattern_id = id;
            patternService.getPattern($scope).then(function (data) {
                patternRec = data.data;

                var modalInstance = $modal.open({
                    templateUrl: 'templates/pattern/myModalContentView.html',
                    controller: 'ModalInstanceAddCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        patternRec: function () {
                            return patternRec;
                        },
                        modalFormText: function () {
                            return "You are trying to view a pattern";
                        },
                        modalFormTile: function () {
                            return "Preview Pattern";
                        },
                        project_id: function () {
                            return 0;
                        },
                        project_permissions: function () {
                            return '';
                        },
                        project_theme: function () {
                            return '';
                        }
                    }
                });

                modalInstance.result.then(function () {

                }, function () {
                    $scope.preview_button = false;
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        }

        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.toc_creation_status = data.toc_creation_status;
                        $scope.project = {};
                        $scope.project.project_permissions = data.project_permissions;
                        $scope.project.theme = data.theme_location;
                        if ($scope.project.project_permissions['pattern.show.all'].grant) {
                            $scope.pattern_local_global.local = true;
                        }
                        $scope.listPatterns(1);
                    }
                });
            }
        }
        //$scope.getProject();

        $scope.changeStatus = function (pattern_id) {
            patternService.changeStatus(pattern_id, $scope).then(function () {
                $scope.pattern_id = '';
            });
        }

        $scope.apply_filter = function () {
            $scope.pattern_id = '';
            if ($scope.pattern_local_global.global == false && $scope.pattern_local_global.local == false) {
                $scope.pattern_local_global.local = true;
                //$scope.pattern_local_global.global = true;
            }
            $scope.listPatterns(1);
        }

        $scope.changeAccess = function (id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: _lang['content_global_confirm_message'],
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    patternService.makeGlobal(id, $scope).then(function (data) {
                        if (data.status == 200) {
                            $scope.listPatterns(1);
                            SweetAlert.swal("Success", "Pattern access changed successfully.", "success");
                        } else {
                            if (angular.isDefined(data.projectPermissionGranted) && data.projectPermissionGranted == 401) {
                                SweetAlert.swal("Cancelled", data.message, "warning");
                            }
                        }
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Pattern is safe :)", "error");
                }
            });
        };


        $scope.applySorting = function (field) {
            $scope.pattern_id = '';
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.listPatterns(1);
        };

        $scope.$on('apply-advaced-search', function (event, args) {
            $scope.pattern_id = '';
            $scope.listPatterns(1);
        });

        $scope.$on('clear-advaced-search', function (event, args) {
            $scope.pattern_id = '';
            $scope.listPatterns(1);
        });

        $scope.$on('apply-search', function (event, args) {
            $scope.pattern_id = '';
            $scope.listPatterns(1);
        });
        /*
         * @description add pattern in global repo
         * @author Meghamala Mukherjee
         * @param {type} size
         * @returns {undefined}
         */
        $scope.addPatternForm = function (size) {
            patternService.projecttypes().then(function (data) {
                $scope.project_types = data.data;
                selectedKey = $scope.arrayIterate($scope.project_types, 4);
                $scope.project_type = $scope.project_types[selectedKey];
                
                var modalInstance = $modal.open({
                    templateUrl: 'templates/pattern/addGlobalPattern.html',
                    controller: 'GlobalModalPatternAddCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        patternRec: function () {
                            return '';
                        },
                        modalFormText: function () {
                            return "You are trying to add a new pattern";
                        },
                        modalFormTile: function () {
                            return "Add Pattern";
                        },
                        project_id: function () {
                            return 0;
                        },
                        /*project_permissions: function () {
                            return 1;//$scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return 0;
                        }*/
                        project_types: function () {
                            return $scope.project_types;
                        },
                        project_type:function(){
                            return $scope.project_type;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    //Saving TOC    
                    $scope.pattern_id = '';
                    patternService.getPattern($scope).then(function (data) {
                        $scope.patternList = data.data;
                        $scope.totalPatterns = data.totalPatterns;
                    });
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });

            });
            //$scope.child = child;
        };
        
        /*
         * @description add pattern in global repo
         * @author Koushik Samanta
         * @param {type} size
         * @returns {undefined}
         */
        $scope.edit_button = false;
        $scope.editPattern = function (size, id) {
            $scope.edit_button = true;
            var patternRec = {};
            $scope.pattern_id = id;
            patternService.getPattern($scope).then(function (data) {
                patternRec = data.data;
                var patternTitle = patternRec[0].title;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/pattern/addGlobalPattern.html',
                    controller: 'GlobalModalPatternAddCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        patternRec: function () {
                            return patternRec;
                        },
                        modalFormText: function () {
                            return "You are trying to edit pattern [" + $filter('strLimit')(patternTitle, 20) + "]";
                        },
                        modalFormTile: function () {
                            return "Edit Pattern";
                        },
                        project_id: function () {
                            return 0;
                        },
                        project_types: function () {
                            return $scope.project_types;
                        },
                        project_type:function(){
                            return $scope.project_type;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    //Saving TOC    
                    $scope.pattern_id = '';
                    $scope.edit_button = false;
                    patternService.getPattern($scope).then(function (data) {
                        $scope.patternList = data.data;
                        $scope.totalPatterns = data.totalPatterns;
                    });
                }, function () {
                    $scope.edit_button = false;
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        /*
         * @description Array index find for selected type
         * @author Meghamala Mukherjee
         * @param {type} size
         * @returns {undefined}
         */
        var selectedKey = '';
        $scope.arrayIterate = function (lists, project_type_id) {
            angular.forEach(lists, function (value, key) {
                if (value.id == project_type_id) {
                    selectedKey = key;
                }
            });
            return selectedKey;
        }
        
        $scope.deleteGlobalPattern = function (id, project_id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this pattern!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    patternService.delPattern(id, project_id).then(function (data) {
                        if (data.status == 200) {
                            $scope.pattern_id = '';
                            $scope.listPatterns(1);
                            SweetAlert.swal("Success", "Pattern has been deleted", "success");
                        } else {
                            SweetAlert.swal({
                                title: "Cancelled",
                                html: data.message,
                                type: "error",
                            });
                        }
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Pattern is safe :)", "error");
                }
            });

        };
        
    }]);


app.controller('GlobalModalPatternAddCtrl', function ($scope, patternService, sessionService, $modalInstance, modalFormText, modalFormTile, patternRec, project_id, project_types) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    //$scope.project = {'project_permissions': project_permissions};
    $scope.pattern_classes = [{value: ''}];
    //$scope.project_theme = project_theme;
    $scope.project_types = project_types;
    $scope.project_type = '';
    $scope.pattern_id = '';
    if (patternRec.length) {
        
//        var patternContent = patternRec[0].pattern.replace(/..\/images/g, 'images');
        $scope.pattern_id = patternRec[0].id;
        $scope.pattern_title = patternRec[0].title;
        $scope.pattern_content = patternRec[0].pattern;
        $scope.pattern_classes = JSON.parse(patternRec[0].pattern_classes);
        //$scope.pattern_classes = $scope.pattern_classes.all;
        //$scope.project_permissions['pattern.update'].grant
        //console.log($scope.project.project_permissions);
        $scope.showedit = patternRec.length;

        /*$scope.$watch('pattern_classes', function(newValue, oldValue) {
         $scope.pattern_classes = newValue;
         });*/

    } else {
        $scope.showedit = 0;
    }

    $scope.add = function () {
        $scope.formError = {'pattern_title': '', 'pattern_content': ''};
        //if (angular.isUndefined($scope.pattern_title) == false && angular.isUndefined($scope.pattern_content) == false) {
        $scope.is_global=1;
        patternService.savePattern($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                patternService.getPattern($scope).then(function (data) {
                    $scope.patternList = data.data;
                    $modalInstance.close();
                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['pattern_title'] = savedResponse.error.title['0'];
                }
                if (angular.isDefined(savedResponse.error.pattern)) {
                    $scope.formError['pattern_content'] = savedResponse.error.pattern['0'];
                }
                if (angular.isDefined(savedResponse.error.project_type_id)) {
                    $scope.formError['project_type_id'] = savedResponse.error.project_type_id['0'];
                }

            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'pattern_title': '', 'pattern_content': ''};
        //if (angular.isUndefined($scope.pattern_title) == false && angular.isUndefined($scope.pattern_content) == false) {
        patternService.editPattern($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                patternService.getPattern($scope).then(function (data) {
                    $scope.patternList = data.data;
                    $modalInstance.close();
                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['pattern_title'] = savedResponse.error.title['0'];
                }
                if (angular.isDefined(savedResponse.error.pattern)) {
                    $scope.formError['pattern_content'] = savedResponse.error.pattern['0'];
                }

            }


        });
        //}
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    //Manage pattern_classes    
    $scope.addClass = function () {
        $scope.pattern_classes.push({value: ''});
    };
    $scope.removeClass = function (index) {
        // console.log(index);
        $scope.pattern_classes.splice(index, 1);
    };
    //

});


