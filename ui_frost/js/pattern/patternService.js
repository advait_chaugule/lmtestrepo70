/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';
var returnPatterns = '';
var returnPatterns = ''
app.service('patternService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        /*
         * Service to get patterns         
         */
        this.getPattern = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pattern_id)) {
                queryString += "&id=" + $scope.pattern_id;
            }
            if (angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }
            if (angular.isDefined($scope.pattern_status)) {
                queryString += '&pattern_status=' + $scope.pattern_status;
            }
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.currentOrderByField)) {
                queryString += '&orderField=' + $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.searchModel) && angular.isDefined($scope.searchModel.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.searchModel.search_text);
            }

            if (angular.isDefined($scope.search_text)) {
                queryString += '&search_text=' + window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                queryString += '&orderDirection=' + dir;
            }

            queryString += '&pattern_local_global=' + (angular.isDefined($scope.pattern_local_global) ? JSON.stringify($scope.pattern_local_global) : '{"local":true,"global":false}');

            $http.get(serviceEndPoint + 'api/v1/project-pattern?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                /*if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }*/
                //console.log(data);
                returnPatterns = data;
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {

                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //End - Method to get pattern

        /*
         * Service to create pattern         
         */
        this.savePattern = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            params = {
                project_id: $scope.project_id,
                project_type_id:(angular.isDefined($scope.project_type) && angular.isDefined($scope.project_type.id)) ? $scope.project_type.id : '',
                title: $scope.pattern_title,
                pattern: $scope.pattern_content,
                pattern_classes: angular.toJson($scope.pattern_classes),
                global:angular.isDefined($scope.is_global) ? $scope.is_global: '0',
            };
            
            $http.post(serviceEndPoint + 'api/v1/project-pattern/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), params).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /**
         * Service to edit pattern         
         */
        this.editPattern = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-pattern/pattern_id/' + $scope.pattern_id + '/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                project_id: $scope.project_id,
                title: $scope.pattern_title,
                pattern: $scope.pattern_content,
                pattern_classes: angular.toJson($scope.pattern_classes)
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /*
         * Delete Pattern         
         */
        this.delPattern = function (pattern_id, project_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/project-pattern/pattern_id/' + pattern_id + '/project_id/' + project_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (pattern_id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/project-pattern-activate-inactivate/pattern_id/' + pattern_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                project_id: $scope.project_id,
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Used to make pattern global
         */
        this.makeGlobal = function (ids, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/pattern-change-access/pattern_id/' + ids + '?access_token=' + sessionService.get('access_token'), {
                pattern_id: ids,
                project_id: $scope.project_id,
                action: 'GLOBAL'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * @author Meghamala Mukherjee
         * @dated 6-9-2017
         * @description fetches project type list 
         */
        //Start - Method to get object types
        this.projecttypes = function () {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/project-types',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        //End - Method to get object types

    }]);

