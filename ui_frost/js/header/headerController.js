/*****
 * Controller for HEADER
 * Author - Indranil Ghosh * 
 */

'use strict';

app.controller('header', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', '$modal', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, $modal) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_id = sessionService.get('uid');
        $scope.userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.location = $location;
        if (angular.isUndefined($scope.user_permissions)) {
            $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        }
        $scope.showAdvancedButton = false;
        if (angular.isDefined($scope.advanced_search)) {
            $scope.advanced_search.collapsed = false;
            $scope.showAdvancedButton = true;
        }

        /*$scope.$watch('location.path()', function (path) {            
         if(path.indexOf("asset") != -1){
         $scope.pageId = 'asset';
         $scope.search_action = $scope.pageId;
         } else {
         $scope.pageId = "All";
         }            
         });*/
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        if(userinfo){
            $scope.user_name = userinfo.username;
            $scope.firstName = userinfo.first_name;
            $scope.lastName  = userinfo.last_name;
        }

        $scope.getCall = function () {
            //console.log('fhfdhh');
        };

        $scope.search_action = '';

        $scope.do_search = function (search_action) {
            setTimeout(function () {
                // debugger;
                if ($rootScope.header_search_type == 'Asset' || $rootScope.header_search_type == 'GlobalRepo') {
                    $scope.advanced_search.active = false;
                    $scope.advanced_search.heading = 'Search Result For: ' + $scope.searchModel.search_text;
                    $scope.advanced_search.enableSearch = true;
                    $scope.advanced_search.isCollapsed = false;
                    if ($scope.searchModel.search_text == '') {
                        $scope.advanced_search.enableSearch = false;
                    }
                    if ($rootScope.header_search_type == 'GlobalRepo') { // apply search in global repo
                        var args = {};
                        args.selectedTab = angular.element('.nav-tabs .active').text();
                        $rootScope.$broadcast('apply-search', args);
                    } else {
                        $scope.getAssets(1);
                    }

                } else if ($rootScope.header_search_type == 'Project') {
                    $scope.listProjects(1, $scope.searchModel.search_text);
                } else if ($rootScope.header_search_type == 'Taxonomies') {
                    
                    $rootScope.$broadcast('apply-search',  $scope.searchModel.search_text);
                }
            }, 1000);
        }

        // Set Header Search Type
        $scope.header_search_type = 'All';
        $scope.set_header_search_type = function (header_search_type) {
            $scope.header_search_type = header_search_type;
        };
        $scope.click=function(){

            angular.element('#div1').addClass("active_nav");
          };
          
    }]);

